.class public final Lpew;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:[Llub;

.field public c:[B

.field public d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 178
    const v0, 0x466c765

    new-instance v1, Lpex;

    invoke-direct {v1}, Lpex;-><init>()V

    .line 183
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 182
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Loxq;-><init>()V

    .line 188
    sget-object v0, Llub;->a:[Llub;

    iput-object v0, p0, Lpew;->b:[Llub;

    .line 179
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 219
    .line 220
    iget-object v0, p0, Lpew;->a:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 221
    const/4 v0, 0x1

    iget-object v2, p0, Lpew;->a:Ljava/lang/Long;

    .line 222
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 224
    :goto_0
    iget-object v2, p0, Lpew;->b:[Llub;

    if-eqz v2, :cond_1

    .line 225
    iget-object v2, p0, Lpew;->b:[Llub;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 226
    if-eqz v4, :cond_0

    .line 227
    const/4 v5, 0x2

    .line 228
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 225
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 232
    :cond_1
    iget-object v1, p0, Lpew;->c:[B

    if-eqz v1, :cond_2

    .line 233
    const/4 v1, 0x3

    iget-object v2, p0, Lpew;->c:[B

    .line 234
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_2
    iget-object v1, p0, Lpew;->d:[B

    if-eqz v1, :cond_3

    .line 237
    const/4 v1, 0x4

    iget-object v2, p0, Lpew;->d:[B

    .line 238
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    :cond_3
    iget-object v1, p0, Lpew;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    iput v0, p0, Lpew;->ai:I

    .line 242
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpew;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 250
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 251
    sparse-switch v0, :sswitch_data_0

    .line 255
    iget-object v2, p0, Lpew;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 256
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpew;->ah:Ljava/util/List;

    .line 259
    :cond_1
    iget-object v2, p0, Lpew;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    :sswitch_0
    return-object p0

    .line 266
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpew;->a:Ljava/lang/Long;

    goto :goto_0

    .line 270
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 271
    iget-object v0, p0, Lpew;->b:[Llub;

    if-nez v0, :cond_3

    move v0, v1

    .line 272
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llub;

    .line 273
    iget-object v3, p0, Lpew;->b:[Llub;

    if-eqz v3, :cond_2

    .line 274
    iget-object v3, p0, Lpew;->b:[Llub;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 276
    :cond_2
    iput-object v2, p0, Lpew;->b:[Llub;

    .line 277
    :goto_2
    iget-object v2, p0, Lpew;->b:[Llub;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 278
    iget-object v2, p0, Lpew;->b:[Llub;

    new-instance v3, Llub;

    invoke-direct {v3}, Llub;-><init>()V

    aput-object v3, v2, v0

    .line 279
    iget-object v2, p0, Lpew;->b:[Llub;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 280
    invoke-virtual {p1}, Loxn;->a()I

    .line 277
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 271
    :cond_3
    iget-object v0, p0, Lpew;->b:[Llub;

    array-length v0, v0

    goto :goto_1

    .line 283
    :cond_4
    iget-object v2, p0, Lpew;->b:[Llub;

    new-instance v3, Llub;

    invoke-direct {v3}, Llub;-><init>()V

    aput-object v3, v2, v0

    .line 284
    iget-object v2, p0, Lpew;->b:[Llub;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 288
    :sswitch_3
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpew;->c:[B

    goto :goto_0

    .line 292
    :sswitch_4
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpew;->d:[B

    goto :goto_0

    .line 251
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 197
    iget-object v0, p0, Lpew;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x1

    iget-object v1, p0, Lpew;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 200
    :cond_0
    iget-object v0, p0, Lpew;->b:[Llub;

    if-eqz v0, :cond_2

    .line 201
    iget-object v1, p0, Lpew;->b:[Llub;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 202
    if-eqz v3, :cond_1

    .line 203
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 201
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lpew;->c:[B

    if-eqz v0, :cond_3

    .line 208
    const/4 v0, 0x3

    iget-object v1, p0, Lpew;->c:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 210
    :cond_3
    iget-object v0, p0, Lpew;->d:[B

    if-eqz v0, :cond_4

    .line 211
    const/4 v0, 0x4

    iget-object v1, p0, Lpew;->d:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 213
    :cond_4
    iget-object v0, p0, Lpew;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 215
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0, p1}, Lpew;->a(Loxn;)Lpew;

    move-result-object v0

    return-object v0
.end method
