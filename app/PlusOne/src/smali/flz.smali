.class public final Lflz;
.super Lhny;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private final b:I

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 1

    .prologue
    .line 22
    const-string v0, "SetAutoTaggingSettingsTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 23
    iput-object p1, p0, Lflz;->a:Landroid/content/Context;

    .line 24
    iput p2, p0, Lflz;->b:I

    .line 25
    iput-boolean p3, p0, Lflz;->c:Z

    .line 26
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 5

    .prologue
    .line 30
    new-instance v0, Ldnc;

    iget-object v1, p0, Lflz;->a:Landroid/content/Context;

    iget v2, p0, Lflz;->b:I

    iget-boolean v3, p0, Lflz;->c:Z

    invoke-direct {v0, v1, v2, v3}, Ldnc;-><init>(Landroid/content/Context;IZ)V

    .line 32
    invoke-virtual {v0}, Lkff;->l()V

    .line 33
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    .line 34
    invoke-virtual {v0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflz;->f()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a0891

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 35
    return-object v1

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Lflz;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a057b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
