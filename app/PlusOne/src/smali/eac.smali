.class public final Leac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/fragments/EditEventFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V
    .locals 0

    .prologue
    .line 1366
    iput-object p1, p0, Leac;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1369
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1373
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1377
    iget-object v0, p0, Leac;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->l(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1378
    iget-object v1, p0, Leac;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->j(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lidh;

    move-result-object v1

    invoke-virtual {v1}, Lidh;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1379
    iget-object v0, p0, Leac;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->j(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lidh;

    move-result-object v0

    iget-object v1, p0, Leac;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->l(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lidh;->b(Ljava/lang/String;)V

    .line 1380
    iget-object v0, p0, Leac;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Lcom/google/android/apps/plus/fragments/EditEventFragment;Z)Z

    .line 1382
    iget-object v0, p0, Leac;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->k(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Leae;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1383
    iget-object v0, p0, Leac;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->k(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Leae;

    .line 1386
    :cond_0
    return-void
.end method
