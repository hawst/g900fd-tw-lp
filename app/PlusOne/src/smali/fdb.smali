.class public final Lfdb;
.super Lfbx;
.source "PG"


# static fields
.field private static j:I

.field private static k:Z

.field private static p:Z

.field private static v:Landroid/text/style/StyleSpan;


# instance fields
.field private final l:I

.field private final m:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final n:Ljava/lang/String;

.field private final o:I

.field private q:Ljava/lang/String;

.field private r:I

.field private s:Lfvw;

.field private t:Z

.field private final u:Levy;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;ILjava/lang/String;Lfvw;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 95
    invoke-direct {p0, p1, p2}, Lfbx;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 66
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lfdb;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 96
    iput p3, p0, Lfdb;->l:I

    .line 97
    iput-object p4, p0, Lfdb;->n:Ljava/lang/String;

    .line 98
    iput p5, p0, Lfdb;->o:I

    .line 99
    iput-object p7, p0, Lfdb;->s:Lfvw;

    .line 100
    iput-object p6, p0, Lfdb;->x:Ljava/lang/String;

    .line 102
    sget-object v0, Lfvc;->k:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    new-instance v0, Levy;

    invoke-direct {v0, p1}, Levy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfdb;->u:Levy;

    .line 109
    :goto_0
    sget-boolean v0, Lfdb;->k:Z

    if-nez v0, :cond_0

    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 111
    const v1, 0x7f0d029e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfdb;->j:I

    .line 112
    new-instance v0, Landroid/text/style/StyleSpan;

    invoke-direct {v0, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Lfdb;->v:Landroid/text/style/StyleSpan;

    .line 113
    sput-boolean v2, Lfdb;->k:Z

    .line 115
    :cond_0
    return-void

    .line 105
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lfdb;->u:Levy;

    goto :goto_0
.end method

.method static synthetic a(Lfdb;Z)V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lfdb;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iput-boolean p1, p0, Lfdb;->t:Z

    return-void
.end method

.method private d(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 452
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Landroid/database/Cursor;)Lizu;
    .locals 8

    .prologue
    .line 456
    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 457
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 458
    const/4 v3, 0x6

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 459
    invoke-static {v0, v1}, Ljvj;->a(J)Ljac;

    move-result-object v4

    .line 463
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 464
    const/4 v0, 0x0

    .line 473
    :goto_0
    return-object v0

    .line 467
    :cond_0
    const-wide/32 v6, 0x40000

    and-long/2addr v0, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 468
    iget-object v0, p0, Lfdb;->c:Landroid/content/Context;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    goto :goto_0

    .line 470
    :cond_1
    iget-object v0, p0, Lfdb;->c:Landroid/content/Context;

    invoke-static {v0, v2, v3, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 216
    invoke-direct {p0, p2}, Lfdb;->d(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfdb;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f040040

    .line 217
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 218
    :goto_0
    return-object v0

    .line 217
    :cond_0
    iget-object v0, p0, Lfdb;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f04018e

    .line 218
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 224
    invoke-direct {p0, p3}, Lfdb;->d(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_9

    move-object v0, p1

    .line 225
    check-cast v0, Lcom/google/android/apps/plus/views/AlbumCoverView;

    invoke-direct {p0, p3}, Lfdb;->e(Landroid/database/Cursor;)Lizu;

    move-result-object v3

    iget-object v1, p0, Lfdb;->n:Ljava/lang/String;

    invoke-static {v1}, Ljvj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x4

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0xb

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/16 v1, 0xd

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v1, 0x5

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lfdb;->s:Lfvw;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AlbumCoverView;->a(Lfvw;)V

    const/4 v1, -0x1

    const/4 v2, 0x7

    invoke-interface {p3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_12

    const/4 v1, 0x7

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    move v2, v1

    :goto_0
    const-string v1, "PLUS_EVENT"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v10, "ALBUM"

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-wide/16 v10, 0x2

    and-long/2addr v8, v10

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-eqz v4, :cond_1

    :cond_0
    if-eqz v1, :cond_8

    :cond_1
    if-eqz v1, :cond_7

    const v1, 0x7f0a09a2

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lfdb;->w:Ljava/lang/String;

    :goto_2
    iget-object v1, p0, Lfdb;->w:Ljava/lang/String;

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v4, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sget-object v6, Lfdb;->v:Landroid/text/style/StyleSpan;

    const/4 v8, 0x0

    const/16 v9, 0x21

    invoke-virtual {v4, v6, v8, v1, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v6, Llju;

    const-string v8, "roster://"

    const/4 v9, 0x1

    invoke-direct {v6, v8, v9}, Llju;-><init>(Ljava/lang/String;Z)V

    const/4 v8, 0x0

    const/16 v9, 0x21

    invoke-virtual {v4, v6, v8, v1, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    if-ltz v2, :cond_4

    iget-object v1, p0, Lfdb;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f11003b

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v1, v6, v2, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, " \u2022 "

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_3
    invoke-virtual {v4, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, " \u2022 "

    invoke-virtual {v4, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_5
    invoke-virtual {v4, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_6
    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/AlbumCoverView;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/AlbumCoverView;->b(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AlbumCoverView;->invalidate()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AlbumCoverView;->g(Z)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumCoverView;->a(Lizu;)V

    const/4 v0, 0x3

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f10007f

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 226
    new-instance v0, Llka;

    const/4 v1, 0x2

    iget v2, p0, Lfdb;->r:I

    iget v3, p0, Lfdb;->o:I

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Llka;-><init>(IIII)V

    .line 237
    :goto_3
    const/4 v1, 0x1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-direct {p0, p3}, Lfdb;->e(Landroid/database/Cursor;)Lizu;

    move-result-object v2

    .line 239
    const v3, 0x7f100079

    invoke-virtual {p1, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 240
    const v1, 0x7f100092

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 241
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 242
    return-void

    .line 225
    :cond_7
    packed-switch v6, :pswitch_data_0

    const/4 v1, 0x0

    goto/16 :goto_1

    :pswitch_0
    const v1, 0x7f0a099f

    :goto_4
    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_1
    const v1, 0x7f0a09a1

    goto :goto_4

    :pswitch_2
    const-class v1, Lhei;

    invoke-static {p2, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    iget v4, p0, Lfdb;->l:I

    invoke-interface {v1, v4}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v4, "domain_name"

    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v4, 0x7f0a09a0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v6, v8

    invoke-virtual {p2, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :pswitch_3
    const v1, 0x7f0a099e

    goto :goto_4

    :pswitch_4
    const v1, 0x7f0a09a3

    goto :goto_4

    :cond_8
    const/4 v1, 0x0

    iput-object v1, p0, Lfdb;->w:Ljava/lang/String;

    goto/16 :goto_2

    :cond_9
    move-object v8, p1

    .line 230
    check-cast v8, Lcom/google/android/apps/plus/views/PhotoTileView;

    invoke-direct {p0, p3}, Lfdb;->e(Landroid/database/Cursor;)Lizu;

    move-result-object v3

    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->g(Z)V

    invoke-virtual {v8, v3}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;)V

    const/16 v0, 0x8

    invoke-interface {p3, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    :goto_5
    if-lez v0, :cond_c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Ljava/lang/Integer;)V

    :goto_6
    const/16 v0, 0x9

    invoke-interface {p3, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x0

    :goto_7
    if-lez v0, :cond_e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljava/lang/Integer;)V

    :goto_8
    const/16 v0, 0xd

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/16 v0, 0xc

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/32 v0, 0x20000000

    and-long/2addr v0, v6

    const-wide/16 v10, 0x0

    cmp-long v0, v0, v10

    if-eqz v0, :cond_f

    const-wide/16 v0, 0x4000

    and-long/2addr v0, v4

    const-wide/16 v10, 0x0

    cmp-long v0, v0, v10

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    move v9, v0

    :goto_9
    iget-object v0, p0, Lfdb;->e:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    new-instance v1, Ljug;

    iget-object v2, p0, Lfdb;->n:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljug;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljue;

    invoke-direct {v2, v3}, Ljue;-><init>(Lizu;)V

    invoke-virtual {v0, v1, v2}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v0

    check-cast v0, Ljuc;

    if-nez v0, :cond_11

    new-instance v0, Ljuc;

    iget-object v1, p0, Lfdb;->n:Ljava/lang/String;

    iget-object v2, p0, Lfdb;->n:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    move-object v1, v0

    :goto_a
    const-wide/16 v6, 0x100

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_b
    iget-object v2, p0, Lfdb;->u:Levy;

    if-eqz v2, :cond_a

    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(I)V

    iget-object v2, p0, Lfdb;->u:Levy;

    invoke-virtual {v2, v8, v3}, Levy;->a(Lcom/google/android/apps/plus/views/PhotoTileView;Lizu;)V

    :cond_a
    invoke-virtual {v8, v1}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljcl;)V

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoTileView;->n(Z)V

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->d(Z)V

    .line 231
    iget-object v0, p0, Lfdb;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    iget-object v0, p0, Lfdb;->i:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 233
    new-instance v0, Llka;

    const/4 v1, 0x2

    const/4 v2, -0x3

    invoke-direct {v0, v1, v2}, Llka;-><init>(II)V

    goto/16 :goto_3

    .line 230
    :cond_b
    const/16 v0, 0x8

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto/16 :goto_5

    :cond_c
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(Ljava/lang/Integer;)V

    goto/16 :goto_6

    :cond_d
    const/16 v0, 0x9

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto/16 :goto_7

    :cond_e
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljava/lang/Integer;)V

    goto/16 :goto_8

    :cond_f
    const/4 v0, 0x0

    move v9, v0

    goto :goto_9

    :cond_10
    const/4 v0, 0x0

    goto :goto_b

    :cond_11
    move-object v1, v0

    goto :goto_a

    :cond_12
    move v2, v1

    goto/16 :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 118
    sget-boolean v0, Lfdb;->p:Z

    if-eq v0, p1, :cond_0

    .line 119
    sput-boolean p1, Lfdb;->p:Z

    .line 120
    invoke-virtual {p0}, Lfdb;->notifyDataSetChanged()V

    .line 122
    :cond_0
    return-void
.end method

.method public b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 131
    if-nez p1, :cond_0

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    .line 132
    :goto_0
    const-string v1, "resume_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfdb;->q:Ljava/lang/String;

    .line 133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfdb;->t:Z

    .line 135
    invoke-super {p0, p1}, Lfbx;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 131
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lfdb;->w:Ljava/lang/String;

    return-object v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lfdb;->u:Levy;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lfdb;->u:Levy;

    invoke-virtual {v0}, Levy;->a()V

    .line 441
    :cond_0
    invoke-super {p0}, Lfbx;->e()V

    .line 442
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 140
    invoke-super {p0}, Lfbx;->getCount()I

    move-result v0

    .line 141
    if-nez v0, :cond_1

    .line 142
    const/4 v0, 0x0

    .line 153
    :cond_0
    :goto_0
    return v0

    .line 144
    :cond_1
    sget-boolean v1, Lfdb;->p:Z

    if-eqz v1, :cond_2

    .line 147
    add-int/lit8 v0, v0, 0x1

    .line 149
    :cond_2
    iget-object v1, p0, Lfdb;->q:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 172
    invoke-super {p0}, Lfbx;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 173
    const/4 v0, -0x1

    .line 175
    :goto_0
    return v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi",
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 181
    iget-boolean v0, p0, Lfdb;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfdb;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 182
    invoke-super {p0}, Lfbx;->getCount()I

    move-result v0

    sub-int/2addr v0, p1

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Lfdb;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Lfdc;

    iget-object v1, p0, Lfdb;->c:Landroid/content/Context;

    iget v2, p0, Lfdb;->l:I

    iget-object v4, p0, Lfdb;->n:Ljava/lang/String;

    iget-object v5, p0, Lfdb;->x:Ljava/lang/String;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lfdc;-><init>(Landroid/content/Context;ILfdb;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_3

    .line 187
    new-array v1, v7, [Ljava/lang/String;

    iget-object v2, p0, Lfdb;->q:Ljava/lang/String;

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 194
    :cond_0
    :goto_0
    if-nez p3, :cond_4

    move v0, v6

    :goto_1
    iget v1, p0, Lfdb;->r:I

    if-nez v1, :cond_2

    if-lez v0, :cond_2

    iget-object v1, p0, Lfdb;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0071

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    mul-int/lit8 v3, v0, 0x64

    div-int v2, v3, v2

    iput v2, p0, Lfdb;->r:I

    const v2, 0x7f0d02a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    if-lez v1, :cond_1

    iget v2, p0, Lfdb;->r:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lfdb;->r:I

    :cond_1
    const-string v1, "SingleAlbum"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lfdb;->r:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "albumCoverHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 196
    :cond_2
    invoke-super {p0}, Lfbx;->getCount()I

    move-result v0

    .line 197
    if-lt p1, v0, :cond_6

    .line 198
    iget-object v1, p0, Lfdb;->q:Ljava/lang/String;

    if-eqz v1, :cond_5

    if-ne p1, v0, :cond_5

    .line 199
    iget-object v0, p0, Lfdb;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f0400f8

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 211
    :goto_2
    return-object v0

    .line 189
    :cond_3
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v7, [Ljava/lang/String;

    iget-object v3, p0, Lfdb;->q:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 194
    :cond_4
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    goto :goto_1

    .line 202
    :cond_5
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lfdb;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 203
    new-instance v1, Llka;

    const/4 v2, 0x2

    const/4 v3, -0x2

    iget v4, p0, Lfdb;->o:I

    invoke-direct {v1, v2, v3, v4, v7}, Llka;-><init>(IIII)V

    .line 207
    sget v2, Lfdb;->j:I

    iput v2, v1, Llka;->height:I

    .line 208
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2

    .line 211
    :cond_6
    invoke-super {p0, p1, p2, p3}, Lfbx;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method
