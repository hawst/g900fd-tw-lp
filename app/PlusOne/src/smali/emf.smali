.class final Lemf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Leme;


# direct methods
.method constructor <init>(Leme;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lemf;->a:Leme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lemf;->a:Leme;

    invoke-virtual {v0}, Leme;->n()Lz;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 65
    const-string v2, "circle_actor_data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 66
    new-instance v2, Lfbj;

    iget-object v3, p0, Lemf;->a:Leme;

    iget-object v3, v3, Leme;->Z:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 67
    invoke-static {v1}, Ldqu;->a([B)Ljava/util/List;

    move-result-object v1

    invoke-direct {v2, v0, v3, v1}, Lfbj;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-object v2
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lemf;->a:Leme;

    const/4 v1, 0x0

    iput-boolean v1, v0, Leme;->R:Z

    .line 74
    iget-object v0, p0, Lemf;->a:Leme;

    invoke-static {v0}, Leme;->a(Leme;)Lemg;

    move-result-object v0

    invoke-virtual {v0, p1}, Lemg;->a(Landroid/database/Cursor;)V

    .line 75
    iget-object v0, p0, Lemf;->a:Leme;

    invoke-static {v0}, Leme;->a(Leme;)Lemg;

    move-result-object v0

    invoke-virtual {v0}, Lemg;->notifyDataSetChanged()V

    .line 76
    iget-object v0, p0, Lemf;->a:Leme;

    iget-object v1, p0, Lemf;->a:Leme;

    invoke-virtual {v1}, Leme;->x()Landroid/view/View;

    invoke-virtual {v0}, Leme;->aa()V

    .line 77
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 81
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 59
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lemf;->a(Landroid/database/Cursor;)V

    return-void
.end method
