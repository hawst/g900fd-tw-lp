.class final Ldsq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljpc;


# instance fields
.field final synthetic a:Landroid/os/ConditionVariable;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:I

.field private synthetic d:Ljps;

.field private synthetic e:J


# direct methods
.method constructor <init>(Ljps;Landroid/os/ConditionVariable;JLandroid/content/Context;I)V
    .locals 1

    .prologue
    .line 5126
    iput-object p1, p0, Ldsq;->d:Ljps;

    iput-object p2, p0, Ldsq;->a:Landroid/os/ConditionVariable;

    iput-wide p3, p0, Ldsq;->e:J

    iput-object p5, p0, Ldsq;->b:Landroid/content/Context;

    iput p6, p0, Ldsq;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Liqc;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 5129
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5130
    const-string v0, "Disconnecting client %s."

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Ldsq;->d:Ljps;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 5132
    :cond_0
    iget-object v0, p0, Ldsq;->d:Ljps;

    invoke-interface {v0}, Ljps;->e()V

    .line 5134
    if-nez p1, :cond_1

    .line 5135
    iget-object v0, p0, Ldsq;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 5175
    :goto_0
    return-void

    .line 5139
    :cond_1
    invoke-static {p1}, Ldsm;->a(Liqc;)J

    move-result-wide v0

    .line 5142
    iget-wide v2, p0, Ldsq;->e:J

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 5143
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5144
    const-string v0, "Fingerprint unchanged %s."

    new-array v1, v7, [Ljava/lang/Object;

    iget-wide v2, p0, Ldsq;->e:J

    .line 5145
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 5144
    invoke-static {}, Ljpe;->b()V

    .line 5147
    :cond_2
    invoke-interface {p1}, Liqc;->a()V

    .line 5148
    iget-object v0, p0, Ldsq;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto :goto_0

    .line 5152
    :cond_3
    invoke-static {}, Ljpe;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5153
    const-string v2, "currentFingerprint: %s. newFingerprint: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-wide v4, p0, Ldsq;->e:J

    .line 5154
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v7

    .line 5153
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 5159
    :cond_4
    new-instance v2, Ldsr;

    invoke-direct {v2, p0, v0, v1}, Ldsr;-><init>(Ldsq;J)V

    new-array v0, v7, [Liqc;

    aput-object p1, v0, v6

    .line 5174
    invoke-virtual {v2, v0}, Ldsr;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
