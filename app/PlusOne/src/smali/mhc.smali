.class public final Lmhc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lngp;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33289
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33292
    iput-object v0, p0, Lmhc;->apiHeader:Llyq;

    .line 33295
    iput-object v0, p0, Lmhc;->a:Lngp;

    .line 33289
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33312
    const/4 v0, 0x0

    .line 33313
    iget-object v1, p0, Lmhc;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 33314
    const/4 v0, 0x1

    iget-object v1, p0, Lmhc;->apiHeader:Llyq;

    .line 33315
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 33317
    :cond_0
    iget-object v1, p0, Lmhc;->a:Lngp;

    if-eqz v1, :cond_1

    .line 33318
    const/4 v1, 0x2

    iget-object v2, p0, Lmhc;->a:Lngp;

    .line 33319
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33321
    :cond_1
    iget-object v1, p0, Lmhc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33322
    iput v0, p0, Lmhc;->ai:I

    .line 33323
    return v0
.end method

.method public a(Loxn;)Lmhc;
    .locals 2

    .prologue
    .line 33331
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 33332
    sparse-switch v0, :sswitch_data_0

    .line 33336
    iget-object v1, p0, Lmhc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 33337
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhc;->ah:Ljava/util/List;

    .line 33340
    :cond_1
    iget-object v1, p0, Lmhc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33342
    :sswitch_0
    return-object p0

    .line 33347
    :sswitch_1
    iget-object v0, p0, Lmhc;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 33348
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmhc;->apiHeader:Llyq;

    .line 33350
    :cond_2
    iget-object v0, p0, Lmhc;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33354
    :sswitch_2
    iget-object v0, p0, Lmhc;->a:Lngp;

    if-nez v0, :cond_3

    .line 33355
    new-instance v0, Lngp;

    invoke-direct {v0}, Lngp;-><init>()V

    iput-object v0, p0, Lmhc;->a:Lngp;

    .line 33357
    :cond_3
    iget-object v0, p0, Lmhc;->a:Lngp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33332
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33300
    iget-object v0, p0, Lmhc;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 33301
    const/4 v0, 0x1

    iget-object v1, p0, Lmhc;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33303
    :cond_0
    iget-object v0, p0, Lmhc;->a:Lngp;

    if-eqz v0, :cond_1

    .line 33304
    const/4 v0, 0x2

    iget-object v1, p0, Lmhc;->a:Lngp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33306
    :cond_1
    iget-object v0, p0, Lmhc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33308
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33285
    invoke-virtual {p0, p1}, Lmhc;->a(Loxn;)Lmhc;

    move-result-object v0

    return-object v0
.end method
