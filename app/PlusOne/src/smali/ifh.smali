.class public final Lifh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private a:Ljava/io/RandomAccessFile;

.field private b:Ljava/io/RandomAccessFile;

.field private c:Ljava/io/RandomAccessFile;

.field private d:Ljava/nio/channels/FileChannel;

.field private e:Ljava/nio/MappedByteBuffer;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Ljava/io/RandomAccessFile;

.field private m:Ljava/io/RandomAccessFile;

.field private n:I

.field private o:I

.field private p:[B

.field private q:[B

.field private r:Ljava/util/zip/Adler32;

.field private s:Lifi;

.field private t:I

.field private u:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIZI)V
    .locals 10

    .prologue
    const/16 v9, 0x1c

    const/16 v8, 0x14

    const/4 v5, 0x4

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const/16 v0, 0x20

    new-array v0, v0, [B

    iput-object v0, p0, Lifh;->p:[B

    .line 123
    new-array v0, v8, [B

    iput-object v0, p0, Lifh;->q:[B

    .line 124
    new-instance v0, Ljava/util/zip/Adler32;

    invoke-direct {v0}, Ljava/util/zip/Adler32;-><init>()V

    iput-object v0, p0, Lifh;->r:Ljava/util/zip/Adler32;

    .line 410
    new-instance v0, Lifi;

    invoke-direct {v0}, Lifi;-><init>()V

    iput-object v0, p0, Lifh;->s:Lifi;

    .line 139
    new-instance v0, Ljava/io/RandomAccessFile;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".idx"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "rw"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    .line 140
    new-instance v0, Ljava/io/RandomAccessFile;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "rw"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    .line 141
    new-instance v0, Ljava/io/RandomAccessFile;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "rw"

    invoke-direct {v0, v1, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    .line 142
    iput p5, p0, Lifh;->k:I

    .line 144
    if-nez p4, :cond_1

    invoke-direct {p0}, Lifh;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    :cond_0
    return-void

    .line 148
    :cond_1
    iget-object v0, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, v6, v7}, Ljava/io/RandomAccessFile;->setLength(J)V

    iget-object v0, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    mul-int/lit8 v1, p2, 0xc

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x20

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V

    iget-object v0, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    iget-object v0, p0, Lifh;->p:[B

    const v1, -0x4cd8cfd0

    invoke-static {v0, v4, v1}, Lifh;->b([BII)V

    invoke-static {v0, v5, p2}, Lifh;->b([BII)V

    const/16 v1, 0x8

    invoke-static {v0, v1, p3}, Lifh;->b([BII)V

    const/16 v1, 0xc

    invoke-static {v0, v1, v4}, Lifh;->b([BII)V

    const/16 v1, 0x10

    invoke-static {v0, v1, v4}, Lifh;->b([BII)V

    invoke-static {v0, v8, v5}, Lifh;->b([BII)V

    const/16 v1, 0x18

    iget v2, p0, Lifh;->k:I

    invoke-static {v0, v1, v2}, Lifh;->b([BII)V

    invoke-virtual {p0, v0, v4, v9}, Lifh;->a([BII)I

    move-result v1

    invoke-static {v0, v9, v1}, Lifh;->b([BII)V

    iget-object v1, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->write([B)V

    iget-object v1, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v6, v7}, Ljava/io/RandomAccessFile;->setLength(J)V

    iget-object v1, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v6, v7}, Ljava/io/RandomAccessFile;->setLength(J)V

    iget-object v1, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    iget-object v1, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    const v1, -0x42db7af0

    invoke-static {v0, v4, v1}, Lifh;->b([BII)V

    iget-object v1, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v0, v4, v5}, Ljava/io/RandomAccessFile;->write([BII)V

    iget-object v1, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v0, v4, v5}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 150
    invoke-direct {p0}, Lifh;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    invoke-direct {p0}, Lifh;->c()V

    .line 152
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unable to load index"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static a([BI)I
    .locals 2

    .prologue
    .line 628
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method private a(J[BI)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/16 v10, 0x8

    .line 387
    iget-object v4, p0, Lifh;->q:[B

    .line 388
    invoke-virtual {p0, p3}, Lifh;->a([B)I

    move-result v5

    move v0, v1

    move-wide v2, p1

    .line 389
    :goto_0
    if-ge v0, v10, :cond_0

    add-int/lit8 v6, v0, 0x0

    const-wide/16 v8, 0xff

    and-long/2addr v8, v2

    long-to-int v7, v8

    int-to-byte v7, v7

    aput-byte v7, v4, v6

    shr-long/2addr v2, v10

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 390
    :cond_0
    invoke-static {v4, v10, v5}, Lifh;->b([BII)V

    .line 391
    const/16 v0, 0xc

    iget v2, p0, Lifh;->j:I

    invoke-static {v4, v0, v2}, Lifh;->b([BII)V

    .line 392
    const/16 v0, 0x10

    invoke-static {v4, v0, p4}, Lifh;->b([BII)V

    .line 393
    iget-object v0, p0, Lifh;->l:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, v4}, Ljava/io/RandomAccessFile;->write([B)V

    .line 394
    iget-object v0, p0, Lifh;->l:Ljava/io/RandomAccessFile;

    invoke-virtual {v0, p3, v1, p4}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 396
    iget-object v0, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    iget v1, p0, Lifh;->t:I

    invoke-virtual {v0, v1, p1, p2}, Ljava/nio/MappedByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    .line 397
    iget-object v0, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    iget v1, p0, Lifh;->t:I

    add-int/lit8 v1, v1, 0x8

    iget v2, p0, Lifh;->j:I

    invoke-virtual {v0, v1, v2}, Ljava/nio/MappedByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    .line 398
    iget v0, p0, Lifh;->j:I

    add-int/lit8 v1, p4, 0x14

    add-int/2addr v0, v1

    iput v0, p0, Lifh;->j:I

    .line 399
    iget-object v0, p0, Lifh;->p:[B

    const/16 v1, 0x14

    iget v2, p0, Lifh;->j:I

    invoke-static {v0, v1, v2}, Lifh;->b([BII)V

    .line 400
    return-void
.end method

.method static a(Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 619
    if-nez p0, :cond_0

    .line 625
    :goto_0
    return-void

    .line 621
    :cond_0
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 625
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 159
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".idx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lifh;->b(Ljava/lang/String;)V

    .line 160
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lifh;->b(Ljava/lang/String;)V

    .line 161
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lifh;->b(Ljava/lang/String;)V

    .line 162
    return-void
.end method

.method private a(JI)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 539
    iget v0, p0, Lifh;->f:I

    int-to-long v0, v0

    rem-long v0, p1, v0

    long-to-int v0, v0

    .line 540
    if-gez v0, :cond_0

    iget v1, p0, Lifh;->f:I

    add-int/2addr v0, v1

    :cond_0
    move v1, v0

    .line 543
    :cond_1
    :goto_0
    mul-int/lit8 v3, v1, 0xc

    add-int/2addr v3, p3

    .line 544
    iget-object v4, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v4, v3}, Ljava/nio/MappedByteBuffer;->getLong(I)J

    move-result-wide v4

    .line 545
    iget-object v6, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    add-int/lit8 v7, v3, 0x8

    invoke-virtual {v6, v7}, Ljava/nio/MappedByteBuffer;->getInt(I)I

    move-result v6

    .line 546
    if-nez v6, :cond_2

    .line 547
    iput v3, p0, Lifh;->t:I

    move v0, v2

    .line 552
    :goto_1
    return v0

    .line 549
    :cond_2
    cmp-long v4, v4, p1

    if-nez v4, :cond_3

    .line 550
    iput v3, p0, Lifh;->t:I

    .line 551
    iput v6, p0, Lifh;->u:I

    .line 552
    const/4 v0, 0x1

    goto :goto_1

    .line 554
    :cond_3
    add-int/lit8 v1, v1, 0x1

    iget v3, p0, Lifh;->f:I

    if-lt v1, v3, :cond_4

    move v1, v2

    .line 557
    :cond_4
    if-ne v1, v0, :cond_1

    .line 558
    iget-object v3, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    mul-int/lit8 v4, v1, 0xc

    add-int/2addr v4, p3

    add-int/lit8 v4, v4, 0x8

    invoke-virtual {v3, v4, v2}, Ljava/nio/MappedByteBuffer;->putInt(II)Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method private a(Ljava/io/RandomAccessFile;ILifi;)Z
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v0, 0x0

    .line 480
    iget-object v4, p0, Lifh;->q:[B

    .line 481
    invoke-virtual {p1}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    .line 483
    int-to-long v2, p2

    :try_start_0
    invoke-virtual {p1, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 484
    invoke-virtual {p1, v4}, Ljava/io/RandomAccessFile;->read([B)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    const/16 v2, 0x14

    if-eq v1, v2, :cond_0

    .line 485
    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 522
    :goto_0
    return v0

    .line 488
    :cond_0
    const/4 v1, 0x7

    :try_start_1
    aget-byte v1, v4, v1

    and-int/lit16 v1, v1, 0xff

    int-to-long v2, v1

    const/4 v1, 0x6

    :goto_1
    if-ltz v1, :cond_1

    shl-long/2addr v2, v10

    add-int/lit8 v5, v1, 0x0

    aget-byte v5, v4, v5

    and-int/lit16 v5, v5, 0xff

    int-to-long v8, v5

    or-long/2addr v2, v8

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 489
    :cond_1
    iget-wide v8, p3, Lifi;->a:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_2

    .line 490
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x2d

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "blob key does not match: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 491
    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    goto :goto_0

    .line 493
    :cond_2
    const/16 v1, 0x8

    :try_start_2
    invoke-static {v4, v1}, Lifh;->a([BI)I

    move-result v1

    .line 494
    const/16 v2, 0xc

    invoke-static {v4, v2}, Lifh;->a([BI)I

    move-result v2

    .line 495
    if-eq v2, p2, :cond_3

    .line 496
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "blob offset does not match: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 497
    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    goto :goto_0

    .line 499
    :cond_3
    const/16 v2, 0x10

    :try_start_3
    invoke-static {v4, v2}, Lifh;->a([BI)I

    move-result v2

    .line 500
    if-ltz v2, :cond_4

    iget v3, p0, Lifh;->g:I

    sub-int/2addr v3, p2

    add-int/lit8 v3, v3, -0x14

    if-le v2, v3, :cond_5

    .line 501
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "invalid blob length: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 502
    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    goto :goto_0

    .line 504
    :cond_5
    :try_start_4
    iget-object v3, p3, Lifi;->b:[B

    if-eqz v3, :cond_6

    iget-object v3, p3, Lifi;->b:[B

    array-length v3, v3

    if-ge v3, v2, :cond_7

    .line 505
    :cond_6
    new-array v3, v2, [B

    iput-object v3, p3, Lifi;->b:[B

    .line 508
    :cond_7
    iget-object v3, p3, Lifi;->b:[B

    .line 509
    iput v2, p3, Lifi;->c:I

    .line 511
    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4, v2}, Ljava/io/RandomAccessFile;->read([BII)I
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v4

    if-eq v4, v2, :cond_8

    .line 512
    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    goto/16 :goto_0

    .line 515
    :cond_8
    const/4 v4, 0x0

    :try_start_5
    invoke-virtual {p0, v3, v4, v2}, Lifh;->a([BII)I

    move-result v2

    if-eq v2, v1, :cond_9

    .line 516
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x29

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "blob checksum does not match: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 517
    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    goto/16 :goto_0

    .line 519
    :cond_9
    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    const/4 v0, 0x1

    goto/16 :goto_0

    .line 520
    :catch_0
    move-exception v1

    .line 521
    :try_start_6
    const-string v2, "BlobCache"

    const-string v3, "getBlob failed."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 522
    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    throw v0
.end method

.method private static b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 166
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static b([BII)V
    .locals 3

    .prologue
    .line 643
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 644
    add-int v1, p1, v0

    int-to-byte v2, p2

    aput-byte v2, p0, v1

    .line 645
    shr-int/lit8 p2, p2, 0x8

    .line 643
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 647
    :cond_0
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lifh;->d:Ljava/nio/channels/FileChannel;

    invoke-static {v0}, Lifh;->a(Ljava/io/Closeable;)V

    .line 182
    iget-object v0, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lifh;->a(Ljava/io/Closeable;)V

    .line 183
    iget-object v0, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lifh;->a(Ljava/io/Closeable;)V

    .line 184
    iget-object v0, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    invoke-static {v0}, Lifh;->a(Ljava/io/Closeable;)V

    .line 185
    return-void
.end method

.method private d()Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const v5, -0x42db7af0

    const/4 v4, 0x4

    const/4 v6, 0x0

    .line 191
    :try_start_0
    iget-object v0, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 192
    iget-object v0, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 193
    iget-object v0, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 195
    iget-object v0, p0, Lifh;->p:[B

    .line 196
    iget-object v1, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v1

    const/16 v2, 0x20

    if-eq v1, v2, :cond_0

    move v0, v6

    .line 279
    :goto_0
    return v0

    .line 201
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v1

    const v2, -0x4cd8cfd0

    if-eq v1, v2, :cond_1

    move v0, v6

    .line 202
    goto :goto_0

    .line 206
    :cond_1
    const/16 v1, 0x18

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v1

    iget v2, p0, Lifh;->k:I

    if-eq v1, v2, :cond_2

    move v0, v6

    .line 207
    goto :goto_0

    .line 211
    :cond_2
    const/4 v1, 0x4

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v1

    iput v1, p0, Lifh;->f:I

    .line 212
    const/16 v1, 0x8

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v1

    iput v1, p0, Lifh;->g:I

    .line 213
    const/16 v1, 0xc

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v1

    iput v1, p0, Lifh;->h:I

    .line 214
    const/16 v1, 0x10

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v1

    iput v1, p0, Lifh;->i:I

    .line 215
    const/16 v1, 0x14

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v1

    iput v1, p0, Lifh;->j:I

    .line 217
    const/16 v1, 0x1c

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v1

    .line 218
    const/4 v2, 0x0

    const/16 v3, 0x1c

    invoke-virtual {p0, v0, v2, v3}, Lifh;->a([BII)I

    move-result v0

    if-eq v0, v1, :cond_3

    move v0, v6

    .line 219
    goto :goto_0

    .line 224
    :cond_3
    iget v0, p0, Lifh;->f:I

    if-gtz v0, :cond_4

    move v0, v6

    .line 225
    goto :goto_0

    .line 228
    :cond_4
    iget v0, p0, Lifh;->g:I

    if-gtz v0, :cond_5

    move v0, v6

    .line 229
    goto :goto_0

    .line 232
    :cond_5
    iget v0, p0, Lifh;->h:I

    if-eqz v0, :cond_6

    iget v0, p0, Lifh;->h:I

    if-eq v0, v7, :cond_6

    move v0, v6

    .line 233
    goto :goto_0

    .line 236
    :cond_6
    iget v0, p0, Lifh;->i:I

    if-ltz v0, :cond_7

    iget v0, p0, Lifh;->i:I

    iget v1, p0, Lifh;->f:I

    if-le v0, v1, :cond_8

    :cond_7
    move v0, v6

    .line 237
    goto :goto_0

    .line 240
    :cond_8
    iget v0, p0, Lifh;->j:I

    if-lt v0, v4, :cond_9

    iget v0, p0, Lifh;->j:I

    iget v1, p0, Lifh;->g:I

    if-le v0, v1, :cond_a

    :cond_9
    move v0, v6

    .line 241
    goto :goto_0

    .line 244
    :cond_a
    iget-object v0, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    iget v2, p0, Lifh;->f:I

    mul-int/lit8 v2, v2, 0xc

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x20

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_b

    move v0, v6

    .line 246
    goto/16 :goto_0

    .line 251
    :cond_b
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 252
    iget-object v1, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v1

    if-eq v1, v4, :cond_c

    move v0, v6

    .line 253
    goto/16 :goto_0

    .line 256
    :cond_c
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v1

    if-eq v1, v5, :cond_d

    move v0, v6

    .line 257
    goto/16 :goto_0

    .line 260
    :cond_d
    iget-object v1, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v0}, Ljava/io/RandomAccessFile;->read([B)I

    move-result v1

    if-eq v1, v4, :cond_e

    move v0, v6

    .line 261
    goto/16 :goto_0

    .line 264
    :cond_e
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lifh;->a([BI)I

    move-result v0

    if-eq v0, v5, :cond_f

    move v0, v6

    .line 265
    goto/16 :goto_0

    .line 270
    :cond_f
    iget-object v0, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    iput-object v0, p0, Lifh;->d:Ljava/nio/channels/FileChannel;

    .line 271
    iget-object v0, p0, Lifh;->d:Ljava/nio/channels/FileChannel;

    sget-object v1, Ljava/nio/channels/FileChannel$MapMode;->READ_WRITE:Ljava/nio/channels/FileChannel$MapMode;

    const-wide/16 v2, 0x0

    iget-object v4, p0, Lifh;->a:Ljava/io/RandomAccessFile;

    .line 272
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v4

    .line 271
    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->map(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    .line 273
    iget-object v0, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 275
    invoke-direct {p0}, Lifh;->e()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v7

    .line 276
    goto/16 :goto_0

    .line 277
    :catch_0
    move-exception v0

    .line 278
    const-string v1, "BlobCache"

    const-string v2, "loadIndex failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v6

    .line 279
    goto/16 :goto_0
.end method

.method private e()V
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 284
    iget v0, p0, Lifh;->h:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    :goto_0
    iput-object v0, p0, Lifh;->l:Ljava/io/RandomAccessFile;

    .line 285
    iget v0, p0, Lifh;->h:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    :goto_1
    iput-object v0, p0, Lifh;->m:Ljava/io/RandomAccessFile;

    .line 286
    iget-object v0, p0, Lifh;->l:Ljava/io/RandomAccessFile;

    iget v1, p0, Lifh;->j:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 287
    iget-object v0, p0, Lifh;->l:Ljava/io/RandomAccessFile;

    iget v1, p0, Lifh;->j:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 289
    iput v4, p0, Lifh;->n:I

    .line 290
    iput v4, p0, Lifh;->o:I

    .line 292
    iget v0, p0, Lifh;->h:I

    if-nez v0, :cond_2

    .line 293
    iget v0, p0, Lifh;->o:I

    iget v1, p0, Lifh;->f:I

    mul-int/lit8 v1, v1, 0xc

    add-int/2addr v0, v1

    iput v0, p0, Lifh;->o:I

    .line 297
    :goto_2
    return-void

    .line 284
    :cond_0
    iget-object v0, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    goto :goto_0

    .line 285
    :cond_1
    iget-object v0, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    goto :goto_1

    .line 295
    :cond_2
    iget v0, p0, Lifh;->n:I

    iget v1, p0, Lifh;->f:I

    mul-int/lit8 v1, v1, 0xc

    add-int/2addr v0, v1

    iput v0, p0, Lifh;->n:I

    goto :goto_2
.end method

.method private f()V
    .locals 6

    .prologue
    const/16 v5, 0x400

    const/4 v4, 0x0

    .line 327
    iget v0, p0, Lifh;->h:I

    rsub-int/lit8 v0, v0, 0x1

    iput v0, p0, Lifh;->h:I

    .line 328
    iput v4, p0, Lifh;->i:I

    .line 329
    const/4 v0, 0x4

    iput v0, p0, Lifh;->j:I

    .line 331
    iget-object v0, p0, Lifh;->p:[B

    const/16 v1, 0xc

    iget v2, p0, Lifh;->h:I

    invoke-static {v0, v1, v2}, Lifh;->b([BII)V

    .line 332
    iget-object v0, p0, Lifh;->p:[B

    const/16 v1, 0x10

    iget v2, p0, Lifh;->i:I

    invoke-static {v0, v1, v2}, Lifh;->b([BII)V

    .line 333
    iget-object v0, p0, Lifh;->p:[B

    const/16 v1, 0x14

    iget v2, p0, Lifh;->j:I

    invoke-static {v0, v1, v2}, Lifh;->b([BII)V

    .line 334
    invoke-direct {p0}, Lifh;->g()V

    .line 336
    invoke-direct {p0}, Lifh;->e()V

    .line 337
    iget v0, p0, Lifh;->n:I

    new-array v1, v5, [B

    iget-object v2, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    iget v0, p0, Lifh;->f:I

    mul-int/lit8 v0, v0, 0xc

    :goto_0
    if-lez v0, :cond_0

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v3, v1, v4, v2}, Ljava/nio/MappedByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    sub-int/2addr v0, v2

    goto :goto_0

    .line 338
    :cond_0
    invoke-virtual {p0}, Lifh;->a()V

    .line 339
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    const/16 v3, 0x1c

    const/4 v2, 0x0

    .line 343
    iget-object v0, p0, Lifh;->p:[B

    iget-object v1, p0, Lifh;->p:[B

    .line 344
    invoke-virtual {p0, v1, v2, v3}, Lifh;->a([BII)I

    move-result v1

    .line 343
    invoke-static {v0, v3, v1}, Lifh;->b([BII)V

    .line 345
    iget-object v0, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/MappedByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 346
    iget-object v0, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    iget-object v1, p0, Lifh;->p:[B

    invoke-virtual {v0, v1}, Ljava/nio/MappedByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 347
    return-void
.end method


# virtual methods
.method a([B)I
    .locals 2

    .prologue
    .line 607
    iget-object v0, p0, Lifh;->r:Ljava/util/zip/Adler32;

    invoke-virtual {v0}, Ljava/util/zip/Adler32;->reset()V

    .line 608
    iget-object v0, p0, Lifh;->r:Ljava/util/zip/Adler32;

    invoke-virtual {v0, p1}, Ljava/util/zip/Adler32;->update([B)V

    .line 609
    iget-object v0, p0, Lifh;->r:Ljava/util/zip/Adler32;

    invoke-virtual {v0}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method a([BII)I
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lifh;->r:Ljava/util/zip/Adler32;

    invoke-virtual {v0}, Ljava/util/zip/Adler32;->reset()V

    .line 614
    iget-object v0, p0, Lifh;->r:Ljava/util/zip/Adler32;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/zip/Adler32;->update([BII)V

    .line 615
    iget-object v0, p0, Lifh;->r:Ljava/util/zip/Adler32;

    invoke-virtual {v0}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 567
    :try_start_0
    iget-object v0, p0, Lifh;->e:Ljava/nio/MappedByteBuffer;

    invoke-virtual {v0}, Ljava/nio/MappedByteBuffer;->force()Ljava/nio/MappedByteBuffer;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 570
    :goto_0
    return-void

    .line 569
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(J[B)V
    .locals 3

    .prologue
    .line 362
    array-length v0, p3

    add-int/lit8 v0, v0, 0x18

    iget v1, p0, Lifh;->g:I

    if-le v0, v1, :cond_0

    .line 363
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "blob is too large!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_0
    iget v0, p0, Lifh;->j:I

    add-int/lit8 v0, v0, 0x14

    array-length v1, p3

    add-int/2addr v0, v1

    iget v1, p0, Lifh;->g:I

    if-gt v0, v1, :cond_1

    iget v0, p0, Lifh;->i:I

    shl-int/lit8 v0, v0, 0x1

    iget v1, p0, Lifh;->f:I

    if-lt v0, v1, :cond_2

    .line 368
    :cond_1
    invoke-direct {p0}, Lifh;->f()V

    .line 371
    :cond_2
    iget v0, p0, Lifh;->n:I

    invoke-direct {p0, p1, p2, v0}, Lifh;->a(JI)Z

    move-result v0

    if-nez v0, :cond_3

    .line 374
    iget v0, p0, Lifh;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lifh;->i:I

    .line 375
    iget-object v0, p0, Lifh;->p:[B

    const/16 v1, 0x10

    iget v2, p0, Lifh;->i:I

    invoke-static {v0, v1, v2}, Lifh;->b([BII)V

    .line 378
    :cond_3
    array-length v0, p3

    invoke-direct {p0, p1, p2, p3, v0}, Lifh;->a(J[BI)V

    .line 379
    invoke-direct {p0}, Lifh;->g()V

    .line 380
    return-void
.end method

.method public a(Lifi;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 434
    iget-wide v2, p1, Lifi;->a:J

    iget v1, p0, Lifh;->n:I

    invoke-direct {p0, v2, v3, v1}, Lifh;->a(JI)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 435
    iget-object v1, p0, Lifh;->l:Ljava/io/RandomAccessFile;

    iget v2, p0, Lifh;->u:I

    invoke-direct {p0, v1, v2, p1}, Lifh;->a(Ljava/io/RandomAccessFile;ILifi;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 468
    :cond_0
    :goto_0
    return v0

    .line 443
    :cond_1
    iget v1, p0, Lifh;->t:I

    .line 446
    iget-wide v2, p1, Lifi;->a:J

    iget v4, p0, Lifh;->o:I

    invoke-direct {p0, v2, v3, v4}, Lifh;->a(JI)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 447
    iget-object v2, p0, Lifh;->m:Ljava/io/RandomAccessFile;

    iget v3, p0, Lifh;->u:I

    invoke-direct {p0, v2, v3, p1}, Lifh;->a(Ljava/io/RandomAccessFile;ILifi;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 450
    iget v2, p0, Lifh;->j:I

    add-int/lit8 v2, v2, 0x14

    iget v3, p1, Lifi;->c:I

    add-int/2addr v2, v3

    iget v3, p0, Lifh;->g:I

    if-gt v2, v3, :cond_0

    iget v2, p0, Lifh;->i:I

    shl-int/lit8 v2, v2, 0x1

    iget v3, p0, Lifh;->f:I

    if-ge v2, v3, :cond_0

    .line 455
    iput v1, p0, Lifh;->t:I

    .line 457
    :try_start_0
    iget-wide v2, p1, Lifi;->a:J

    iget-object v1, p1, Lifi;->b:[B

    iget v4, p1, Lifi;->c:I

    invoke-direct {p0, v2, v3, v1, v4}, Lifh;->a(J[BI)V

    .line 458
    iget v1, p0, Lifh;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lifh;->i:I

    .line 459
    iget-object v1, p0, Lifh;->p:[B

    const/16 v2, 0x10

    iget v3, p0, Lifh;->i:I

    invoke-static {v1, v2, v3}, Lifh;->b([BII)V

    .line 460
    invoke-direct {p0}, Lifh;->g()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 462
    :catch_0
    move-exception v1

    const-string v1, "BlobCache"

    const-string v2, "cannot copy over"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 468
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)[B
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 413
    iget-object v1, p0, Lifh;->s:Lifi;

    iput-wide p1, v1, Lifi;->a:J

    .line 414
    iget-object v1, p0, Lifh;->s:Lifi;

    iput-object v0, v1, Lifi;->b:[B

    .line 415
    iget-object v1, p0, Lifh;->s:Lifi;

    invoke-virtual {p0, v1}, Lifh;->a(Lifi;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    iget-object v0, p0, Lifh;->s:Lifi;

    iget-object v0, v0, Lifi;->b:[B

    .line 418
    :cond_0
    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 574
    invoke-virtual {p0}, Lifh;->a()V

    .line 576
    :try_start_0
    iget-object v0, p0, Lifh;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 578
    :goto_0
    :try_start_1
    iget-object v0, p0, Lifh;->c:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 584
    :goto_1
    return-void

    .line 583
    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public close()V
    .locals 0

    .prologue
    .line 176
    invoke-virtual {p0}, Lifh;->b()V

    .line 177
    invoke-direct {p0}, Lifh;->c()V

    .line 178
    return-void
.end method
