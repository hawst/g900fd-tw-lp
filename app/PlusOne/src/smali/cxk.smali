.class final Lcxk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgae;


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private final b:[I

.field private synthetic c:Lcxf;


# direct methods
.method constructor <init>(Lcxf;)V
    .locals 1

    .prologue
    .line 198
    iput-object p1, p0, Lcxk;->c:Lcxf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcxk;->a:Landroid/graphics/Rect;

    .line 200
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcxk;->b:[I

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/plus/views/PhotoView;Lkda;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 204
    iget-object v0, p0, Lcxk;->c:Lcxf;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcxf;->a(Lcxf;I)I

    .line 206
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoView;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0}, Lcxf;->d(Lcxf;)Ldgi;

    move-result-object v0

    sget-object v2, Lhmv;->em:Lhmv;

    invoke-interface {v0, v2}, Ldgi;->a(Lhmv;)V

    .line 210
    :cond_0
    iget-object v0, p0, Lcxk;->c:Lcxf;

    .line 211
    invoke-static {v0}, Lcxf;->b(Lcxf;)Ldeo;

    move-result-object v0

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->n()J

    move-result-wide v2

    const-wide/32 v4, 0x800000

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    move v0, v1

    .line 212
    :goto_0
    iget-object v2, p0, Lcxk;->c:Lcxf;

    invoke-static {v2}, Lcxf;->b(Lcxf;)Ldeo;

    move-result-object v2

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    :goto_1
    invoke-virtual {v2, v0}, Ldeo;->a(I)V

    .line 214
    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0}, Lcxf;->b(Lcxf;)Ldeo;

    move-result-object v0

    invoke-virtual {v0, v1}, Ldeo;->b(Z)V

    .line 215
    invoke-virtual {p0}, Lcxk;->c()V

    .line 217
    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0}, Lcxf;->b(Lcxf;)Ldeo;

    move-result-object v0

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    .line 218
    invoke-interface {v0}, Lddl;->F()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Lddl;->P()Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0}, Lcxf;->c(Lcxf;)Lcom/google/android/apps/plus/views/PhotoView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->j(Z)V

    .line 222
    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0, v1}, Lcxf;->a(Lcxf;Z)Z

    .line 224
    :cond_1
    return-void

    .line 211
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 212
    :cond_3
    iget-object v0, p0, Lcxk;->c:Lcxf;

    .line 213
    invoke-static {v0}, Lcxf;->c(Lcxf;)Lcom/google/android/apps/plus/views/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView;->j()I

    move-result v0

    goto :goto_1
.end method

.method public aj_()V
    .locals 0

    .prologue
    .line 228
    invoke-virtual {p0}, Lcxk;->c()V

    .line 229
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 242
    invoke-virtual {p0}, Lcxk;->c()V

    .line 243
    return-void
.end method

.method c()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 246
    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0}, Lcxf;->e(Lcxf;)Ldef;

    move-result-object v3

    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0}, Lcxf;->c(Lcxf;)Lcom/google/android/apps/plus/views/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView;->n()F

    move-result v0

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v4, 0x3d4cccc0    # 0.049999952f

    cmpl-float v0, v0, v4

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Ldef;->a(Z)V

    .line 249
    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0}, Lcxf;->c(Lcxf;)Lcom/google/android/apps/plus/views/PhotoView;

    move-result-object v0

    iget-object v3, p0, Lcxk;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotoView;->b(Landroid/graphics/Rect;)V

    .line 250
    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0}, Lcxf;->c(Lcxf;)Lcom/google/android/apps/plus/views/PhotoView;

    move-result-object v0

    iget-object v3, p0, Lcxk;->a:Landroid/graphics/Rect;

    iget-object v4, p0, Lcxk;->b:[I

    invoke-virtual {v0, v4}, Landroid/view/View;->getLocationInWindow([I)V

    iget-object v0, p0, Lcxk;->b:[I

    aget v0, v0, v2

    iget-object v2, p0, Lcxk;->b:[I

    aget v1, v2, v1

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 251
    iget-object v0, p0, Lcxk;->c:Lcxf;

    invoke-static {v0}, Lcxf;->e(Lcxf;)Ldef;

    move-result-object v0

    iget-object v1, p0, Lcxk;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Ldef;->b(Landroid/graphics/Rect;)V

    .line 252
    return-void

    :cond_0
    move v0, v2

    .line 246
    goto :goto_0
.end method
