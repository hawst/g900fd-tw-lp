.class final Ledn;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljuf;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljuf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2478
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2479
    iput-object p1, p0, Ledn;->a:Landroid/content/Context;

    .line 2480
    iput p2, p0, Ledn;->b:I

    .line 2481
    iput-object p3, p0, Ledn;->c:Ljava/lang/String;

    .line 2482
    iput-object p4, p0, Ledn;->d:Ljava/util/List;

    .line 2483
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ledn;->e:Ljava/util/List;

    .line 2484
    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Void;
    .locals 10

    .prologue
    .line 2488
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2489
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 2491
    iget-object v0, p0, Ledn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    .line 2492
    iget-object v0, p0, Ledn;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuf;

    .line 2493
    invoke-interface {v0}, Ljuf;->f()Lizu;

    move-result-object v0

    .line 2495
    invoke-virtual {v0}, Lizu;->j()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2496
    iget-object v4, p0, Ledn;->e:Ljava/util/List;

    invoke-virtual {v0}, Lizu;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2491
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 2497
    :cond_1
    invoke-virtual {v0}, Lizu;->k()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2498
    invoke-virtual {v0}, Lizu;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2499
    :cond_2
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2500
    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2504
    :cond_3
    iget v4, p0, Ledn;->b:I

    .line 2505
    iget-object v0, p0, Ledn;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, v4}, Lhei;->a(I)Lhej;

    move-result-object v5

    .line 2507
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 2508
    iget-object v0, p0, Ledn;->a:Landroid/content/Context;

    .line 2509
    invoke-static {v0, v4, v2}, Ljvj;->a(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 2510
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    .line 2511
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v6, :cond_4

    .line 2512
    iget-object v7, p0, Ledn;->e:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljad;

    invoke-virtual {v0}, Ljad;->b()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2511
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2516
    :cond_4
    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 2517
    iget-object v0, p0, Ledn;->a:Landroid/content/Context;

    invoke-static {v0}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v1

    .line 2518
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2519
    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2521
    invoke-virtual {v1, v0}, Lhrx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2520
    invoke-virtual {v2, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 2523
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2524
    new-instance v1, Ljvu;

    iget-object v6, p0, Ledn;->a:Landroid/content/Context;

    const-string v7, "gaia_id"

    .line 2525
    invoke-interface {v5, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v6, v4, v5, v0}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    .line 2526
    invoke-virtual {v1}, Ljvu;->l()V

    .line 2527
    invoke-virtual {v1}, Ljvu;->t()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2528
    const-string v0, "HostedEventFragment"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2530
    iget v0, v1, Lkff;->i:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CheckPhotosExistenceOperation error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2545
    :cond_6
    iget-object v1, p0, Ledn;->a:Landroid/content/Context;

    iget-object v2, p0, Ledn;->c:Ljava/lang/String;

    .line 2546
    invoke-virtual {v3}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    .line 2545
    invoke-virtual {v3, v0}, Ljava/util/LinkedHashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v4, v2, v0}, Leco;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V

    .line 2549
    :cond_7
    const/4 v0, 0x0

    return-object v0

    .line 2533
    :cond_8
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_9
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2537
    invoke-virtual {v1, v0}, Ljvu;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2538
    invoke-virtual {v1, v0}, Ljvu;->c(Ljava/lang/String;)J

    move-result-wide v6

    .line 2539
    iget-object v8, p0, Ledn;->e:Ljava/util/List;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2540
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 2554
    iget-object v0, p0, Ledn;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2555
    iget-object v0, p0, Ledn;->a:Landroid/content/Context;

    iget v1, p0, Ledn;->b:I

    iget-object v2, p0, Ledn;->c:Ljava/lang/String;

    iget-object v3, p0, Ledn;->e:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)I

    .line 2557
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2470
    invoke-virtual {p0}, Ledn;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2470
    invoke-virtual {p0}, Ledn;->b()V

    return-void
.end method
