.class public Lbwf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwe;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Laic;


# instance fields
.field private final c:Lbwk;

.field private final d:Lbvs;

.field private final e:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

.field private final f:Lcfq;

.field private final g:Lbvy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbvy",
            "<",
            "Laii;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lajt;

.field private final i:Lbks;

.field private final j:Lanh;

.field private final k:Lbwl;

.field private final l:Lbig;

.field private final m:Ljfb;

.field private final n:Lapo;

.field private final o:Laim;

.field private final p:Ljdw;

.field private final q:Livc;

.field private final r:Livc;

.field private final s:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 79
    const-class v0, Lbwf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbwf;->a:Ljava/lang/String;

    .line 112
    new-instance v0, Laic;

    sget-object v1, Laib;->b:Laib;

    sget-object v2, Lahx;->b:[Lahz;

    invoke-direct {v0, v1, v2}, Laic;-><init>(Laib;[Lahz;)V

    sput-object v0, Lbwf;->b:Laic;

    return-void
.end method

.method public constructor <init>(Lbwk;Lbvs;Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;Lcfq;Lbvy;Lajt;Lbks;Lanh;Lbwl;Lbit;Livc;Livc;Ljdw;Lbig;Ljava/util/concurrent/Executor;Ljfb;Lapo;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbwk;",
            "Lbvs;",
            "Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;",
            "Lcfq;",
            "Lbvy",
            "<",
            "Laii;",
            ">;",
            "Lajt;",
            "Lbks;",
            "Lanh;",
            "Lbwl;",
            "Lbit;",
            "Livc;",
            "Livc;",
            "Ljdw;",
            "Lbig;",
            "Ljava/util/concurrent/Executor;",
            "Ljfb;",
            "Lapo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    new-instance v2, Lbwh;

    invoke-direct {v2, p0}, Lbwh;-><init>(Lbwf;)V

    iput-object v2, p0, Lbwf;->s:Ljava/lang/Runnable;

    .line 243
    const-string v2, "service"

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbwk;

    iput-object v2, p0, Lbwf;->c:Lbwk;

    .line 244
    const-string v2, "backgroundServiceHelper"

    .line 245
    const/4 v3, 0x0

    invoke-static {p2, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbvs;

    iput-object v2, p0, Lbwf;->d:Lbvs;

    .line 246
    const-string v2, "applicationEnabler"

    .line 247
    const/4 v3, 0x0

    invoke-static {p3, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    iput-object v2, p0, Lbwf;->e:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    .line 248
    const-string v2, "mediaIterableFactory"

    const/4 v3, 0x0

    invoke-static {p4, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcfq;

    iput-object v2, p0, Lbwf;->f:Lcfq;

    .line 249
    const-string v2, "analyzerConnection"

    const/4 v3, 0x0

    invoke-static {p5, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbvy;

    iput-object v2, p0, Lbwf;->g:Lbvy;

    .line 250
    const-string v2, "metricsAndMetadataStore"

    .line 251
    const/4 v3, 0x0

    invoke-static {p6, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lajt;

    iput-object v2, p0, Lbwf;->h:Lajt;

    .line 252
    const-string v2, "gservicesSettings"

    const/4 v3, 0x0

    move-object/from16 v0, p8

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lanh;

    iput-object v2, p0, Lbwf;->j:Lanh;

    .line 253
    const-string v2, "settings"

    const/4 v3, 0x0

    move-object/from16 v0, p9

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbwl;

    iput-object v2, p0, Lbwf;->k:Lbwl;

    .line 254
    const-string v2, "metricsFactory"

    const/4 v3, 0x0

    invoke-static {p7, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbks;

    iput-object v2, p0, Lbwf;->i:Lbks;

    .line 255
    const-string v2, "mediaExtractorFactory"

    .line 256
    const/4 v3, 0x0

    move-object/from16 v0, p14

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbig;

    iput-object v2, p0, Lbwf;->l:Lbig;

    .line 257
    const-string v2, "movieMakerProvider"

    const/4 v3, 0x0

    move-object/from16 v0, p16

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljfb;

    iput-object v2, p0, Lbwf;->m:Ljfb;

    .line 258
    const-string v2, "movieMakerClusterFilter"

    .line 259
    const/4 v3, 0x0

    move-object/from16 v0, p17

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lapo;

    iput-object v2, p0, Lbwf;->n:Lapo;

    .line 261
    new-instance v2, Lajd;

    iget-object v3, p0, Lbwf;->i:Lbks;

    iget-object v4, p0, Lbwf;->h:Lajt;

    new-instance v5, Lajx;

    iget-object v6, p0, Lbwf;->g:Lbvy;

    invoke-direct {v5, v6}, Lajx;-><init>(Lbvy;)V

    iget-object v7, p0, Lbwf;->m:Ljfb;

    move-object/from16 v6, p10

    invoke-direct/range {v2 .. v7}, Lajd;-><init>(Lbks;Lajl;Lajv;Lbit;Ljfb;)V

    .line 267
    new-instance v3, Laim;

    move-object/from16 v0, p15

    move-object/from16 v1, p15

    invoke-direct {v3, v0, v1, v2}, Laim;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lahx;)V

    iput-object v3, p0, Lbwf;->o:Laim;

    .line 269
    const-string v2, "aamEventsLogger"

    const/4 v3, 0x0

    move-object/from16 v0, p13

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljdw;

    iput-object v2, p0, Lbwf;->p:Ljdw;

    .line 270
    const/4 v2, 0x0

    sget-object v3, Lbwf;->a:Ljava/lang/String;

    move-object/from16 v0, p11

    invoke-static {v2, v3, v0}, Livb;->a(ZLjava/lang/String;Livc;)Livc;

    move-result-object v2

    iput-object v2, p0, Lbwf;->q:Livc;

    .line 271
    const/4 v2, 0x0

    sget-object v3, Lbwf;->a:Ljava/lang/String;

    move-object/from16 v0, p12

    invoke-static {v2, v3, v0}, Livb;->a(ZLjava/lang/String;Livc;)Livc;

    move-result-object v2

    iput-object v2, p0, Lbwf;->r:Livc;

    .line 272
    return-void
.end method

.method private a(Ljava/util/List;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 512
    const/4 v1, 0x0

    .line 514
    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_0

    .line 515
    iget-object v2, p0, Lbwf;->q:Livc;

    const-string v3, "start processing: "

    const-string v0, ", "

    invoke-static {v0, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v2, v0}, Livc;->a(Ljava/lang/String;)V

    .line 518
    :cond_0
    iget-object v0, p0, Lbwf;->k:Lbwl;

    invoke-interface {v0}, Lbwl;->g()J

    move-result-wide v4

    .line 519
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v7, v1

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    .line 520
    iget-object v1, p0, Lbwf;->d:Lbvs;

    invoke-interface {v1}, Lbvs;->a()Z

    move-result v1

    const-string v2, "power disconnected: abort now"

    invoke-direct {p0, v1, v2}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 522
    const/4 v8, 0x6

    .line 566
    :cond_2
    :goto_2
    return v8

    .line 515
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 524
    :cond_4
    iget-object v1, p0, Lbwf;->d:Lbvs;

    invoke-interface {v1}, Lbvs;->c()Z

    move-result v1

    const-string v2, "battery too low: abort now"

    invoke-direct {p0, v1, v2}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 526
    const/4 v8, 0x5

    goto :goto_2

    .line 528
    :cond_5
    iget-object v1, p0, Lbwf;->d:Lbvs;

    invoke-interface {v1}, Lbvs;->b()Z

    move-result v1

    const-string v2, "screen on: abort now"

    invoke-direct {p0, v1, v2}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 529
    const/4 v8, 0x4

    goto :goto_2

    .line 532
    :cond_6
    iget-object v1, v0, Ljej;->b:Ljel;

    const-string v2, "media.type"

    sget-object v3, Ljel;->b:Ljel;

    const-string v6, "should be a video"

    invoke-static {v1, v2, v3, v6}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 533
    iget-object v1, p0, Lbwf;->q:Livc;

    if-eqz v1, :cond_7

    .line 534
    iget-object v1, p0, Lbwf;->q:Livc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x10

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "start analysis: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Livc;->a(Ljava/lang/String;)V

    .line 536
    :cond_7
    iget-object v1, p0, Lbwf;->p:Ljdw;

    iget-object v2, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1, v2}, Ljdw;->b(Landroid/net/Uri;)V

    .line 537
    iget-object v1, p0, Lbwf;->o:Laim;

    iget-object v2, v0, Ljej;->a:Landroid/net/Uri;

    sget-object v3, Lbwf;->b:Laic;

    iget-object v6, p0, Lbwf;->k:Lbwl;

    .line 542
    invoke-interface {v6}, Lbwl;->i()Z

    move-result v6

    .line 537
    invoke-static/range {v1 .. v6}, Lbww;->a(Laim;Landroid/net/Uri;Laic;JZ)Lbwz;

    move-result-object v1

    .line 543
    iget-object v2, p0, Lbwf;->p:Ljdw;

    iget-object v3, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1}, Lbwz;->b()Z

    move-result v6

    invoke-interface {v2, v3, v6}, Ljdw;->b(Landroid/net/Uri;Z)V

    .line 546
    invoke-interface {v1}, Lbwz;->f()J

    move-result-wide v2

    sub-long/2addr v4, v2

    .line 548
    invoke-interface {v1}, Lbwz;->e()Z

    move-result v2

    const-string v3, "analysis interrupted"

    invoke-direct {p0, v2, v3}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 549
    invoke-interface {v1}, Lbwz;->d()Z

    move-result v2

    const-string v3, "analysis canceled"

    invoke-direct {p0, v2, v3}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 551
    invoke-interface {v1}, Lbwz;->c()Z

    move-result v2

    const-string v3, "analysis failed"

    invoke-direct {p0, v2, v3}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 553
    iget-object v1, p0, Lbwf;->h:Lajt;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1, v0}, Lajt;->d(Landroid/net/Uri;)V

    move v7, v8

    .line 554
    goto/16 :goto_1

    .line 555
    :cond_8
    invoke-interface {v1}, Lbwz;->b()Z

    move-result v0

    const-string v1, "analysis successful"

    invoke-direct {p0, v0, v1}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 558
    const-string v0, "unexpected analysis result"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 562
    :cond_9
    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_a

    .line 563
    iget-object v1, p0, Lbwf;->q:Livc;

    const-string v2, "stop processing: "

    const-string v0, ", "

    invoke-static {v0, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-interface {v1, v0}, Livc;->a(Ljava/lang/String;)V

    .line 566
    :cond_a
    if-eqz v7, :cond_2

    const/4 v8, 0x2

    goto/16 :goto_2

    .line 563
    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 120
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    .line 121
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/moviemaker/service/PluggedInAnalyzerService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.moviemaker.analyzer.PING"

    .line 127
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_clean_up"

    .line 128
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbwf;)Lbvs;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lbwf;->d:Lbvs;

    return-object v0
.end method

.method public static a(Lanh;)Lbwl;
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lbwb;

    invoke-direct {v0, p0}, Lbwb;-><init>(Lanh;)V

    return-object v0
.end method

.method public static a(Lbwl;Landroid/content/ContentResolver;Ljfb;)Lcfq;
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lbwg;

    invoke-direct {v0, p1, p0, p2}, Lbwg;-><init>(Landroid/content/ContentResolver;Lbwl;Ljfb;)V

    return-object v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lbwf;->q:Livc;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Rescheduling in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 380
    :cond_0
    iget-object v0, p0, Lbwf;->c:Lbwk;

    const/4 v1, 0x0

    invoke-interface {v0, p1, p2, v1}, Lbwk;->a(JZ)V

    .line 381
    return-void
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 449
    iget-object v0, p0, Lbwf;->h:Lajt;

    invoke-interface {v0, p1}, Lajt;->e(Landroid/net/Uri;)J

    move-result-wide v0

    .line 450
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-nez v3, :cond_0

    .line 453
    :try_start_0
    iget-object v0, p0, Lbwf;->l:Lbig;

    invoke-interface {v0, p1}, Lbig;->a(Landroid/net/Uri;)Lbij;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 459
    :try_start_1
    invoke-interface {v3}, Lbij;->i()Lbmu;

    move-result-object v0

    check-cast v0, Lboo;

    .line 460
    iget-wide v0, v0, Lboo;->f:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 465
    invoke-interface {v3}, Lbij;->a()V

    .line 467
    iget-object v3, p0, Lbwf;->h:Lajt;

    invoke-interface {v3, p1, v0, v1}, Lajt;->a(Landroid/net/Uri;J)V

    .line 469
    :cond_0
    iget-object v3, p0, Lbwf;->j:Lanh;

    invoke-virtual {v3}, Lanh;->y()I

    move-result v3

    int-to-long v4, v3

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    .line 455
    :catch_0
    move-exception v0

    sget-object v0, Lbwf;->a:Ljava/lang/String;

    move v0, v2

    .line 456
    goto :goto_0

    .line 462
    :catch_1
    move-exception v0

    :try_start_2
    sget-object v0, Lbwf;->a:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 463
    invoke-interface {v3}, Lbij;->a()V

    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v3}, Lbij;->a()V

    throw v0

    :cond_1
    move v0, v2

    .line 469
    goto :goto_0
.end method

.method private a(ZLjava/lang/String;)Z
    .locals 1

    .prologue
    .line 391
    if-eqz p1, :cond_0

    .line 392
    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lbwf;->q:Livc;

    invoke-interface {v0, p2}, Livc;->a(Ljava/lang/String;)V

    .line 396
    :cond_0
    return p1
.end method

.method static synthetic b(Lbwf;)Laim;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lbwf;->o:Laim;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Z)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 134
    invoke-static {p0, p1}, Lbwf;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x48000000    # 131072.0f

    .line 133
    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 352
    iget-object v0, p0, Lbwf;->m:Ljfb;

    iget-object v1, p0, Lbwf;->n:Lapo;

    iget-object v2, p0, Lbwf;->l:Lbig;

    iget-object v3, p0, Lbwf;->h:Lajt;

    const/4 v4, 0x0

    iget-object v5, p0, Lbwf;->k:Lbwl;

    .line 358
    invoke-interface {v5}, Lbwl;->l()Z

    move-result v5

    iget-object v6, p0, Lbwf;->r:Livc;

    .line 352
    invoke-static/range {v0 .. v6}, Lcel;->a(Ljfb;Lapo;Lbig;Lajl;ZZLivc;)Ljava/util/List;

    .line 360
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lbwf;->q:Livc;

    const-string v1, "stopping"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 369
    :cond_0
    return-void
.end method

.method private e()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 408
    iget-object v0, p0, Lbwf;->f:Lcfq;

    invoke-interface {v0}, Lcfq;->a()Lcfp;

    move-result-object v1

    .line 410
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 411
    invoke-interface {v1}, Lcfp;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    .line 412
    iget-object v4, v0, Ljej;->b:Ljel;

    sget-object v5, Ljel;->b:Ljel;

    if-ne v4, v5, :cond_0

    .line 414
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lbwf;->k:Lbwl;

    invoke-interface {v5}, Lbwl;->h()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 417
    iget-object v4, p0, Lbwf;->h:Lajt;

    iget-object v5, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v4, v5}, Lajt;->f(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 421
    iget-object v4, p0, Lbwf;->h:Lajt;

    iget-object v5, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v4, v5}, Lajt;->c(Landroid/net/Uri;)I

    move-result v4

    iget-object v5, p0, Lbwf;->k:Lbwl;

    .line 424
    invoke-interface {v5}, Lbwl;->j()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 426
    iget-object v4, v0, Ljej;->a:Landroid/net/Uri;

    invoke-direct {p0, v4}, Lbwf;->a(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 432
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 439
    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_1
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    return-object v2
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lbwf;->g:Lbvy;

    invoke-virtual {v0}, Lbvy;->a()V

    .line 277
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 288
    iget-object v0, p0, Lbwf;->d:Lbvs;

    invoke-interface {v0}, Lbvs;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 291
    iget-object v0, p0, Lbwf;->e:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->a()V

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    const-string v0, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 296
    iget-object v0, p0, Lbwf;->c:Lbwk;

    iget-object v2, p0, Lbwf;->k:Lbwl;

    invoke-interface {v2}, Lbwl;->e()J

    move-result-wide v2

    invoke-interface {v0, v2, v3, v1}, Lbwk;->a(JZ)V

    goto :goto_0

    .line 298
    :cond_2
    const-string v0, "com.google.android.apps.moviemaker.analyzer.PING"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "is_clean_up"

    .line 299
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 300
    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbwf;->q:Livc;

    const-string v1, "start clean-up"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Ljava/util/HashSet;

    iget-object v0, p0, Lbwf;->h:Lajt;

    invoke-interface {v0}, Lajt;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbwf;->q:Livc;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x24

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "stored metrics for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " items"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Livc;->a(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lbwf;->f:Lcfq;

    invoke-interface {v0}, Lcfq;->a()Lcfp;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Lcfp;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v3}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_5
    invoke-static {v3}, Lcek;->a(Ljava/io/Closeable;)V

    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbwf;->q:Livc;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x48

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Removing "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " metrics for URIs no longer in the video media store"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Livc;->a(Ljava/lang/String;)V

    :cond_6
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v3, p0, Lbwf;->q:Livc;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lbwf;->q:Livc;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xa

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Removing: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Livc;->a(Ljava/lang/String;)V

    :cond_7
    iget-object v3, p0, Lbwf;->h:Lajt;

    invoke-interface {v3, v0}, Lajt;->b(Landroid/net/Uri;)V

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lbwf;->q:Livc;

    const-string v1, "stop clean-up"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 301
    :cond_9
    invoke-direct {p0}, Lbwf;->c()V

    .line 302
    iget-object v0, p0, Lbwf;->d:Lbvs;

    invoke-interface {v0}, Lbvs;->a()Z

    move-result v0

    const-string v1, "power disconnected: abort now"

    invoke-direct {p0, v0, v1}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 304
    invoke-direct {p0}, Lbwf;->d()V

    goto/16 :goto_0

    .line 307
    :cond_a
    iget-object v0, p0, Lbwf;->c:Lbwk;

    iget-object v1, p0, Lbwf;->k:Lbwl;

    invoke-interface {v1}, Lbwl;->f()J

    move-result-wide v4

    invoke-interface {v0, v4, v5, v2}, Lbwk;->a(JZ)V

    goto/16 :goto_0

    .line 308
    :cond_b
    const-string v0, "com.google.android.apps.moviemaker.analyzer.PING"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "is_clean_up"

    .line 309
    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_11

    .line 310
    iget-object v0, p0, Lbwf;->p:Ljdw;

    invoke-interface {v0}, Ljdw;->c()V

    .line 311
    iget-object v0, p0, Lbwf;->j:Lanh;

    invoke-virtual {v0}, Lanh;->Z()Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    :goto_3
    const-string v3, "analysis not enabled: abort now"

    invoke-direct {p0, v0, v3}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x7

    .line 312
    :goto_4
    sget-object v3, Lbwi;->a:[I

    add-int/lit8 v4, v0, -0x1

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 338
    :goto_5
    iget-object v3, p0, Lbwf;->p:Ljdw;

    if-ne v0, v1, :cond_10

    :goto_6
    invoke-interface {v3, v1}, Ljdw;->a(Z)V

    .line 339
    invoke-direct {p0}, Lbwf;->c()V

    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 311
    goto :goto_3

    :cond_d
    iget-object v0, p0, Lbwf;->d:Lbvs;

    invoke-interface {v0}, Lbvs;->a()Z

    move-result v0

    const-string v3, "power disconnected: abort now"

    invoke-direct {p0, v0, v3}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x6

    goto :goto_4

    :cond_e
    invoke-direct {p0}, Lbwf;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    const-string v4, "nothing to analyze"

    invoke-direct {p0, v3, v4}, Lbwf;->a(ZLjava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    const/4 v0, 0x3

    goto :goto_4

    :cond_f
    iget-object v3, p0, Lbwf;->d:Lbvs;

    iget-object v4, p0, Lbwf;->s:Ljava/lang/Runnable;

    invoke-interface {v3, v4}, Lbvs;->a(Ljava/lang/Runnable;)V

    invoke-direct {p0, v0}, Lbwf;->a(Ljava/util/List;)I

    move-result v0

    goto :goto_4

    .line 314
    :pswitch_0
    invoke-direct {p0}, Lbwf;->d()V

    goto :goto_5

    .line 318
    :pswitch_1
    iget-object v3, p0, Lbwf;->k:Lbwl;

    invoke-interface {v3}, Lbwl;->c()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lbwf;->a(J)V

    goto :goto_5

    .line 322
    :pswitch_2
    invoke-direct {p0}, Lbwf;->d()V

    goto :goto_5

    .line 326
    :pswitch_3
    iget-object v3, p0, Lbwf;->k:Lbwl;

    invoke-interface {v3}, Lbwl;->d()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lbwf;->a(J)V

    goto :goto_5

    .line 330
    :pswitch_4
    iget-object v3, p0, Lbwf;->k:Lbwl;

    invoke-interface {v3}, Lbwl;->b()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lbwf;->a(J)V

    goto :goto_5

    .line 335
    :pswitch_5
    iget-object v3, p0, Lbwf;->k:Lbwl;

    invoke-interface {v3}, Lbwl;->a()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lbwf;->a(J)V

    goto :goto_5

    :cond_10
    move v1, v2

    .line 338
    goto :goto_6

    .line 341
    :cond_11
    iget-object v0, p0, Lbwf;->q:Livc;

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p0, Lbwf;->q:Livc;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unexpected intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 312
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lbwf;->d:Lbvs;

    invoke-interface {v0}, Lbvs;->d()V

    .line 282
    iget-object v0, p0, Lbwf;->o:Laim;

    invoke-virtual {v0}, Laim;->a()V

    .line 283
    iget-object v0, p0, Lbwf;->g:Lbvy;

    invoke-virtual {v0}, Lbvy;->b()V

    .line 284
    return-void
.end method
