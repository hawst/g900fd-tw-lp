.class public final Libx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;
.implements Ligy;
.implements Llnx;
.implements Llrg;


# instance fields
.field a:Landroid/app/Activity;

.field b:Lhoc;

.field private c:Ligv;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-object v0, p0, Libx;->a:Landroid/app/Activity;

    invoke-static {v0}, Libq;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 63
    :goto_0
    if-eqz v0, :cond_3

    .line 64
    iget-object v2, p0, Libx;->a:Landroid/app/Activity;

    const-string v0, "connectivity"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-static {v2}, Llsa;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Libx;->b()V

    .line 68
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 62
    goto :goto_0

    .line 64
    :cond_1
    invoke-static {v0}, Ler;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Liby;

    invoke-direct {v0, p0}, Liby;-><init>(Libx;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Libx;->a:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0a0417

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0a0418

    invoke-virtual {v2, v1, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0a0419

    invoke-virtual {v2, v1, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Libx;->b:Lhoc;

    new-instance v1, Libz;

    iget-object v2, p0, Libx;->a:Landroid/app/Activity;

    const-string v3, "DownloadResourcesTask"

    invoke-direct {v1, v2, v3}, Libz;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lhoc;->c(Lhny;)V

    goto :goto_1

    .line 66
    :cond_3
    invoke-virtual {p0}, Libx;->b()V

    goto :goto_1
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Libx;->a:Landroid/app/Activity;

    .line 50
    iput-object p3, p0, Libx;->c:Ligv;

    .line 51
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 52
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 56
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Libx;->b:Lhoc;

    .line 57
    iget-object v0, p0, Libx;->b:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 58
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 106
    const-string v0, "DownloadResourcesTask"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0}, Libx;->b()V

    .line 109
    :cond_0
    return-void
.end method

.method b()V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Libx;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 113
    iget-object v1, p0, Libx;->a:Landroid/app/Activity;

    sget-object v2, Licc;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    iget-object v1, p0, Libx;->c:Ligv;

    invoke-interface {v1, v0}, Ligv;->a(Landroid/content/Intent;)V

    .line 115
    return-void
.end method
