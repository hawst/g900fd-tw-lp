.class public final Lisj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xc
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lisj;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/mtp/MtpObjectInfo;)V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-virtual {p1}, Landroid/mtp/MtpObjectInfo;->getObjectHandle()I

    move-result v0

    iput v0, p0, Lisj;->a:I

    .line 21
    invoke-virtual {p1}, Landroid/mtp/MtpObjectInfo;->getDateCreated()J

    move-result-wide v0

    iput-wide v0, p0, Lisj;->b:J

    .line 22
    invoke-virtual {p1}, Landroid/mtp/MtpObjectInfo;->getFormat()I

    move-result v0

    iput v0, p0, Lisj;->c:I

    .line 23
    invoke-virtual {p1}, Landroid/mtp/MtpObjectInfo;->getCompressedSize()I

    move-result v0

    iput v0, p0, Lisj;->d:I

    .line 24
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lisj;->d:I

    return v0
.end method

.method public a(Lisj;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 61
    invoke-virtual {p0}, Lisj;->c()J

    move-result-wide v0

    invoke-virtual {p1}, Lisj;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 62
    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    .line 63
    const/4 v0, -0x1

    .line 67
    :goto_0
    return v0

    .line 64
    :cond_0
    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 65
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Landroid/mtp/MtpDevice;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    if-eqz p1, :cond_0

    .line 51
    iget v0, p0, Lisj;->a:I

    invoke-virtual {p1, v0}, Landroid/mtp/MtpDevice;->getObjectInfo(I)Landroid/mtp/MtpObjectInfo;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0}, Landroid/mtp/MtpObjectInfo;->getName()Ljava/lang/String;

    move-result-object v0

    .line 56
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lisj;->c:I

    return v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lisj;->b:J

    return-wide v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 11
    check-cast p1, Lisj;

    invoke-virtual {p0, p1}, Lisj;->a(Lisj;)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lisj;->a:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 90
    if-ne p0, p1, :cond_1

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 94
    goto :goto_0

    .line 96
    :cond_2
    instance-of v2, p1, Lisj;

    if-nez v2, :cond_3

    move v0, v1

    .line 97
    goto :goto_0

    .line 99
    :cond_3
    check-cast p1, Lisj;

    .line 100
    iget v2, p0, Lisj;->d:I

    iget v3, p1, Lisj;->d:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 101
    goto :goto_0

    .line 103
    :cond_4
    iget-wide v2, p0, Lisj;->b:J

    iget-wide v4, p1, Lisj;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 104
    goto :goto_0

    .line 106
    :cond_5
    iget v2, p0, Lisj;->c:I

    iget v3, p1, Lisj;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 107
    goto :goto_0

    .line 109
    :cond_6
    iget v2, p0, Lisj;->a:I

    iget v3, p1, Lisj;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 110
    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 79
    iget v0, p0, Lisj;->d:I

    add-int/lit8 v0, v0, 0x1f

    .line 82
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lisj;->b:J

    iget-wide v4, p0, Lisj;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 83
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lisj;->c:I

    add-int/2addr v0, v1

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lisj;->a:I

    add-int/2addr v0, v1

    .line 85
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 73
    iget v0, p0, Lisj;->a:I

    iget-wide v2, p0, Lisj;->b:J

    iget v1, p0, Lisj;->c:I

    iget v4, p0, Lisj;->d:I

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x7b

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "IngestObjectInfo [mHandle="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", mDateCreated="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", mFormat="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCompressedSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
