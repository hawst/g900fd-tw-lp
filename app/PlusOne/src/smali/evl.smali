.class public final Levl;
.super Ljvc;
.source "PG"


# static fields
.field public static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "tile_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "parent_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "cluster_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "subtitle"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "all_tiles.image_url"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "background_color"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "cluster_count"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "plusone_count"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "duration_days"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "view_order"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "user_actions"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "media_attr"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "timestamp AS _datetaken"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "photographer_gaia_id"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "photographer_avatar_url"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "NULL AS filename"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "NULL AS _signature"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "content_url"

    aput-object v2, v0, v1

    sput-object v0, Levl;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZI)V
    .locals 10

    .prologue
    .line 97
    sget-object v4, Levl;->b:[Ljava/lang/String;

    .line 98
    const-class v0, Ldhu;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldhu;

    if-nez v0, :cond_0

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "media_attr & 4194304 == 0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_0
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v8, p5

    move/from16 v9, p6

    .line 97
    invoke-direct/range {v0 .. v9}, Ljvc;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    .line 100
    return-void

    :cond_0
    move-object v5, p4

    .line 98
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;ZI)Levl;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;ZI)",
            "Levl;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Levl;

    const-string v1, "NOT media_attr & 32"

    .line 106
    invoke-static {v1, p3}, Levl;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "media_attr & 2048 == 0"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Levl;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZI)V

    return-object v0
.end method
