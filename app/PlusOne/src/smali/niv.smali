.class public final Lniv;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lniw;

.field private b:Lnja;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 563
    invoke-direct {p0}, Loxq;-><init>()V

    .line 566
    const/4 v0, 0x0

    iput-object v0, p0, Lniv;->b:Lnja;

    .line 569
    sget-object v0, Lniw;->a:[Lniw;

    iput-object v0, p0, Lniv;->a:[Lniw;

    .line 563
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 590
    .line 591
    iget-object v0, p0, Lniv;->b:Lnja;

    if-eqz v0, :cond_2

    .line 592
    const/4 v0, 0x1

    iget-object v2, p0, Lniv;->b:Lnja;

    .line 593
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 595
    :goto_0
    iget-object v2, p0, Lniv;->a:[Lniw;

    if-eqz v2, :cond_1

    .line 596
    iget-object v2, p0, Lniv;->a:[Lniw;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 597
    if-eqz v4, :cond_0

    .line 598
    const/4 v5, 0x2

    .line 599
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 596
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 603
    :cond_1
    iget-object v1, p0, Lniv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 604
    iput v0, p0, Lniv;->ai:I

    .line 605
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lniv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 613
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 614
    sparse-switch v0, :sswitch_data_0

    .line 618
    iget-object v2, p0, Lniv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 619
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lniv;->ah:Ljava/util/List;

    .line 622
    :cond_1
    iget-object v2, p0, Lniv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 624
    :sswitch_0
    return-object p0

    .line 629
    :sswitch_1
    iget-object v0, p0, Lniv;->b:Lnja;

    if-nez v0, :cond_2

    .line 630
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lniv;->b:Lnja;

    .line 632
    :cond_2
    iget-object v0, p0, Lniv;->b:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 636
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 637
    iget-object v0, p0, Lniv;->a:[Lniw;

    if-nez v0, :cond_4

    move v0, v1

    .line 638
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lniw;

    .line 639
    iget-object v3, p0, Lniv;->a:[Lniw;

    if-eqz v3, :cond_3

    .line 640
    iget-object v3, p0, Lniv;->a:[Lniw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 642
    :cond_3
    iput-object v2, p0, Lniv;->a:[Lniw;

    .line 643
    :goto_2
    iget-object v2, p0, Lniv;->a:[Lniw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 644
    iget-object v2, p0, Lniv;->a:[Lniw;

    new-instance v3, Lniw;

    invoke-direct {v3}, Lniw;-><init>()V

    aput-object v3, v2, v0

    .line 645
    iget-object v2, p0, Lniv;->a:[Lniw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 646
    invoke-virtual {p1}, Loxn;->a()I

    .line 643
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 637
    :cond_4
    iget-object v0, p0, Lniv;->a:[Lniw;

    array-length v0, v0

    goto :goto_1

    .line 649
    :cond_5
    iget-object v2, p0, Lniv;->a:[Lniw;

    new-instance v3, Lniw;

    invoke-direct {v3}, Lniw;-><init>()V

    aput-object v3, v2, v0

    .line 650
    iget-object v2, p0, Lniv;->a:[Lniw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 614
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 574
    iget-object v0, p0, Lniv;->b:Lnja;

    if-eqz v0, :cond_0

    .line 575
    const/4 v0, 0x1

    iget-object v1, p0, Lniv;->b:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 577
    :cond_0
    iget-object v0, p0, Lniv;->a:[Lniw;

    if-eqz v0, :cond_2

    .line 578
    iget-object v1, p0, Lniv;->a:[Lniw;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 579
    if-eqz v3, :cond_1

    .line 580
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 578
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 584
    :cond_2
    iget-object v0, p0, Lniv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 586
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 559
    invoke-virtual {p0, p1}, Lniv;->a(Loxn;)Lniv;

    move-result-object v0

    return-object v0
.end method
