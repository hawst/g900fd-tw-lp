.class public final Lnot;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 665
    invoke-direct {p0}, Loxq;-><init>()V

    .line 676
    const/high16 v0, -0x80000000

    iput v0, p0, Lnot;->b:I

    .line 665
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 693
    const/4 v0, 0x0

    .line 694
    iget-object v1, p0, Lnot;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 695
    const/4 v0, 0x1

    iget-object v1, p0, Lnot;->a:Ljava/lang/Boolean;

    .line 696
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 698
    :cond_0
    iget v1, p0, Lnot;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 699
    const/4 v1, 0x2

    iget v2, p0, Lnot;->b:I

    .line 700
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 702
    :cond_1
    iget-object v1, p0, Lnot;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 703
    iput v0, p0, Lnot;->ai:I

    .line 704
    return v0
.end method

.method public a(Loxn;)Lnot;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 712
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 713
    sparse-switch v0, :sswitch_data_0

    .line 717
    iget-object v1, p0, Lnot;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 718
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnot;->ah:Ljava/util/List;

    .line 721
    :cond_1
    iget-object v1, p0, Lnot;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 723
    :sswitch_0
    return-object p0

    .line 728
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnot;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 732
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 733
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 736
    :cond_2
    iput v0, p0, Lnot;->b:I

    goto :goto_0

    .line 738
    :cond_3
    iput v2, p0, Lnot;->b:I

    goto :goto_0

    .line 713
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 681
    iget-object v0, p0, Lnot;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 682
    const/4 v0, 0x1

    iget-object v1, p0, Lnot;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 684
    :cond_0
    iget v0, p0, Lnot;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 685
    const/4 v0, 0x2

    iget v1, p0, Lnot;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 687
    :cond_1
    iget-object v0, p0, Lnot;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 689
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 661
    invoke-virtual {p0, p1}, Lnot;->a(Loxn;)Lnot;

    move-result-object v0

    return-object v0
.end method
