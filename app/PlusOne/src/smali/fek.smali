.class public final Lfek;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhfv;


# static fields
.field public static final a:[Ljava/lang/String;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Landroid/database/Cursor;


# instance fields
.field private d:Landroid/database/Cursor;

.field private e:Landroid/database/Cursor;

.field private f:Landroid/database/Cursor;

.field private g:Landroid/database/Cursor;

.field private h:Landroid/database/Cursor;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhfw;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private m:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lfen;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private o:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lfem;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/lang/String;

.field private final q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 53
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "square_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "square_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "photo_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "member_count"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "post_visibility"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "restricted_domain"

    aput-object v2, v0, v1

    sput-object v0, Lfek;->a:[Ljava/lang/String;

    .line 68
    const-string v0, "\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lfek;->b:Ljava/util/regex/Pattern;

    .line 69
    new-instance v0, Lhym;

    new-array v1, v3, [Ljava/lang/String;

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    sput-object v0, Lfek;->c:Landroid/database/Cursor;

    return-void
.end method

.method constructor <init>(Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;Landroid/database/Cursor;Ljava/util/Map;Ljava/lang/String;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Landroid/database/Cursor;",
            "Landroid/database/Cursor;",
            "Landroid/database/Cursor;",
            "Landroid/database/Cursor;",
            "Landroid/database/Cursor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lfek;->d:Landroid/database/Cursor;

    .line 114
    iput-object p2, p0, Lfek;->e:Landroid/database/Cursor;

    .line 115
    iput-object p3, p0, Lfek;->f:Landroid/database/Cursor;

    .line 116
    iput-object p7, p0, Lfek;->j:Ljava/util/Map;

    .line 118
    iput-object p8, p0, Lfek;->p:Ljava/lang/String;

    .line 119
    iput-boolean p9, p0, Lfek;->q:Z

    .line 121
    invoke-static {p1}, Lfek;->b(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lfek;->k:Ljava/util/Map;

    .line 122
    invoke-static {p2}, Lfek;->d(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lfek;->m:Ljava/util/Map;

    .line 123
    invoke-static {p4}, Lfek;->c(Landroid/database/Cursor;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lfek;->l:Ljava/util/Map;

    .line 124
    invoke-static {p6}, Lfek;->a(Landroid/database/Cursor;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfek;->n:Ljava/util/Set;

    .line 125
    iget-object v0, p0, Lfek;->k:Ljava/util/Map;

    .line 126
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lfek;->n:Ljava/util/Set;

    .line 125
    invoke-static {p5, v0, v1}, Lfek;->a(Landroid/database/Cursor;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfek;->i:Ljava/util/List;

    .line 127
    return-void
.end method

.method private static a(Landroid/database/Cursor;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lhfw;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 143
    const/4 v0, 0x0

    .line 144
    if-eqz p0, :cond_6

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 145
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 146
    if-eqz v1, :cond_6

    .line 147
    invoke-static {v1}, Lhhg;->a([B)Ljava/util/ArrayList;

    move-result-object v0

    move-object v5, v0

    .line 150
    :goto_0
    if-nez v5, :cond_0

    .line 151
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 178
    :goto_1
    return-object v0

    .line 154
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 155
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    move v1, v2

    .line 156
    :goto_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x6

    if-ge v0, v3, :cond_5

    .line 157
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 158
    invoke-virtual {v0}, Lhgw;->b()[Lhxc;

    move-result-object v7

    move v3, v2

    .line 159
    :goto_3
    array-length v8, v7

    if-ge v3, v8, :cond_2

    .line 160
    aget-object v8, v7, v3

    invoke-virtual {v8}, Lhxc;->a()Ljava/lang/String;

    move-result-object v8

    .line 161
    invoke-interface {p1, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v6, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 162
    new-instance v8, Lhfw;

    invoke-direct {v8}, Lhfw;-><init>()V

    .line 163
    aget-object v9, v7, v3

    invoke-virtual {v8, v9, v2}, Lhfw;->a(Lhxc;I)Lhfw;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    aget-object v8, v7, v3

    invoke-virtual {v8}, Lhxc;->a()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 159
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 168
    :cond_2
    invoke-virtual {v0}, Lhgw;->a()[Ljqs;

    move-result-object v3

    move v0, v2

    .line 169
    :goto_4
    array-length v7, v3

    if-ge v0, v7, :cond_4

    .line 170
    aget-object v7, v3, v0

    invoke-virtual {v7}, Ljqs;->a()Ljava/lang/String;

    move-result-object v7

    .line 171
    invoke-interface {p2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 172
    new-instance v7, Lhfw;

    invoke-direct {v7}, Lhfw;-><init>()V

    .line 173
    aget-object v8, v3, v0

    invoke-virtual {v7, v8, v2}, Lhfw;->a(Ljqs;I)Lhfw;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    aget-object v7, v3, v0

    invoke-virtual {v7}, Ljqs;->a()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 156
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    move-object v0, v4

    .line 178
    goto/16 :goto_1

    :cond_6
    move-object v5, v0

    goto/16 :goto_0
.end method

.method private static a(Landroid/database/Cursor;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 183
    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 185
    :cond_0
    const-string v1, "gaia_id"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 187
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    :cond_1
    return-object v0
.end method

.method private static a(Lhfw;Landroid/database/Cursor;I)V
    .locals 6

    .prologue
    .line 514
    new-instance v0, Ljqs;

    const-string v1, "gaia_id"

    .line 516
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "name"

    .line 518
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "avatar"

    .line 520
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "in_same_visibility_group"

    .line 522
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    invoke-direct/range {v0 .. v5}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 514
    invoke-virtual {p0, v0, p2}, Lhfw;->a(Ljqs;I)Lhfw;

    .line 527
    return-void

    .line 522
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 194
    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 196
    :cond_0
    const-string v1, "circle_id"

    .line 197
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 196
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "circle_name"

    .line 198
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 196
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 202
    :cond_1
    return-object v0
.end method

.method private static c(Landroid/database/Cursor;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 207
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 208
    if-eqz p0, :cond_2

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 211
    :cond_0
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 212
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 213
    :goto_0
    const/4 v5, 0x5

    if-ge v0, v5, :cond_1

    .line 214
    shl-int/lit8 v5, v0, 0x1

    add-int/lit8 v5, v5, 0x4

    add-int/lit8 v5, v5, 0x1

    invoke-interface {p0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 219
    invoke-static {v5}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    :cond_1
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 222
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    :cond_2
    return-object v2
.end method

.method private static d(Landroid/database/Cursor;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lfen;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 229
    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 231
    :cond_0
    const/4 v1, 0x0

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 232
    new-instance v2, Lfen;

    const/4 v3, 0x3

    .line 233
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x4

    .line 234
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getInt(I)I

    const/4 v4, 0x2

    .line 235
    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lfen;-><init>(ILjava/lang/String;)V

    .line 232
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 238
    :cond_1
    return-object v0
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 398
    packed-switch p1, :pswitch_data_0

    .line 410
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 400
    :pswitch_0
    iget-object v0, p0, Lfek;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 402
    :pswitch_1
    iget-object v0, p0, Lfek;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0

    .line 404
    :pswitch_2
    iget-object v0, p0, Lfek;->e:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0

    .line 406
    :pswitch_3
    iget-object v0, p0, Lfek;->f:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 408
    :pswitch_4
    iget-object v0, p0, Lfek;->o:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    goto :goto_0

    .line 398
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lhfw;)I
    .locals 1

    .prologue
    .line 389
    invoke-virtual {p1}, Lhfw;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 393
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 391
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 389
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lfek;->p:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Context;Lhfw;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 356
    iget-object v0, p0, Lfek;->m:Ljava/util/Map;

    invoke-virtual {p2}, Lhfw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfen;

    .line 357
    if-eqz v0, :cond_0

    .line 358
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f11001d

    iget v3, v0, Lfen;->a:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    .line 359
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v5

    iget v0, v0, Lfen;->a:I

    int-to-long v6, v0

    invoke-virtual {v5, v6, v7}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    .line 358
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 384
    :goto_0
    return-object v0

    .line 362
    :cond_0
    invoke-virtual {p2}, Lhfw;->e()Ljqs;

    move-result-object v1

    .line 363
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljqs;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 364
    invoke-virtual {v1}, Ljqs;->a()Ljava/lang/String;

    move-result-object v0

    .line 365
    if-eqz v0, :cond_3

    .line 366
    const-string v2, "g:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 367
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 369
    :cond_1
    iget-object v2, p0, Lfek;->j:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 370
    if-eqz v0, :cond_3

    .line 371
    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 372
    if-lez v1, :cond_2

    iget-object v2, p0, Lfek;->k:Ljava/util/Map;

    .line 373
    invoke-virtual {v0, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lfek;->k:Ljava/util/Map;

    .line 374
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 379
    :cond_3
    if-eqz v1, :cond_4

    .line 380
    invoke-virtual {v1}, Ljqs;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljqs;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 381
    invoke-virtual {v1}, Ljqs;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 384
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method public a(IILhfw;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 317
    packed-switch p1, :pswitch_data_0

    .line 349
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No entries in category: %s"

    new-array v2, v5, [Ljava/lang/Object;

    .line 350
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 319
    :pswitch_0
    iget-object v0, p0, Lfek;->i:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfw;

    invoke-virtual {v0, p3}, Lhfw;->a(Lhfw;)V

    .line 346
    :goto_0
    return-void

    .line 323
    :pswitch_1
    iget-object v0, p0, Lfek;->d:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 324
    iget-object v0, p0, Lfek;->d:Landroid/database/Cursor;

    const-string v1, "circle_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lhxc;

    const-string v3, "type"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const-string v4, "circle_name"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v6, "contact_count"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-direct {v2, v1, v3, v4, v0}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {p3, v2, v5}, Lhfw;->a(Lhxc;I)Lhfw;

    goto :goto_0

    .line 328
    :pswitch_2
    iget-object v0, p0, Lfek;->e:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 329
    iget-object v4, p0, Lfek;->e:Landroid/database/Cursor;

    new-instance v0, Lkxr;

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x5

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    :goto_1
    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lkxr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p3, v0}, Lhfw;->a(Lkxr;)Lhfw;

    goto :goto_0

    :cond_0
    move v5, v7

    goto :goto_1

    .line 333
    :pswitch_3
    iget-object v0, p0, Lfek;->f:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 334
    iget-object v0, p0, Lfek;->f:Landroid/database/Cursor;

    const/4 v1, 0x3

    invoke-static {p3, v0, v1}, Lfek;->a(Lhfw;Landroid/database/Cursor;I)V

    goto :goto_0

    .line 338
    :pswitch_4
    iget-object v0, p0, Lfek;->o:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfem;

    .line 339
    iget-boolean v1, v0, Lfem;->a:Z

    if-eqz v1, :cond_1

    .line 340
    iget-object v1, p0, Lfek;->g:Landroid/database/Cursor;

    iget v0, v0, Lfem;->c:I

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 341
    iget-object v0, p0, Lfek;->g:Landroid/database/Cursor;

    invoke-static {p3, v0, v8}, Lfek;->a(Lhfw;Landroid/database/Cursor;I)V

    goto/16 :goto_0

    .line 343
    :cond_1
    iget-object v1, p0, Lfek;->h:Landroid/database/Cursor;

    iget v0, v0, Lfem;->b:I

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 344
    iget-object v0, p0, Lfek;->h:Landroid/database/Cursor;

    new-instance v2, Ljqs;

    const-string v1, "display_name"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v1, "data1"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v6, v3

    invoke-direct/range {v2 .. v7}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p3, v2, v8}, Lhfw;->a(Ljqs;I)Lhfw;

    goto/16 :goto_0

    .line 317
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 131
    iput-object p1, p0, Lfek;->g:Landroid/database/Cursor;

    .line 132
    iput-object p2, p0, Lfek;->h:Landroid/database/Cursor;

    .line 133
    if-nez p1, :cond_0

    sget-object p1, Lfek;->c:Landroid/database/Cursor;

    :cond_0
    if-nez p2, :cond_1

    sget-object p2, Lfek;->c:Landroid/database/Cursor;

    :cond_1
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-interface {p2, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    move v2, v1

    move v3, v1

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_b

    :cond_2
    new-instance v7, Lfem;

    invoke-direct {v7}, Lfem;-><init>()V

    iput v2, v7, Lfem;->b:I

    iput v3, v7, Lfem;->c:I

    invoke-interface {p2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-eqz v4, :cond_3

    iput-boolean v10, v7, Lfem;->a:Z

    invoke-virtual {v6, v0, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v3, v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    const-string v4, "display_name"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v4, "data1"

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    sget-object v4, Lfek;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    add-int/lit8 v2, v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_0

    :cond_5
    iput-boolean v1, v7, Lfem;->a:Z

    const-string v4, ""

    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v9

    if-nez v9, :cond_7

    const-string v4, "name"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_6

    const-string v4, ""

    :cond_6
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v9

    invoke-virtual {v9, v5, v4}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    if-ltz v9, :cond_7

    iput-boolean v10, v7, Lfem;->a:Z

    add-int/lit8 v3, v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_7
    iget-boolean v9, v7, Lfem;->a:Z

    if-eqz v9, :cond_9

    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_a

    const-string v5, "display_name"

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-boolean v5, v7, Lfem;->a:Z

    if-nez v5, :cond_8

    const-string v5, "data1"

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    :cond_8
    add-int/lit8 v2, v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_1

    :cond_9
    move-object v4, v5

    goto :goto_1

    :cond_a
    invoke-virtual {v6, v0, v7}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_b
    iput-object v6, p0, Lfek;->o:Landroid/util/SparseArray;

    .line 134
    return-void
.end method

.method public a(Lu;ILhxc;Ljava/lang/String;Llgs;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 448
    invoke-virtual {p1}, Lu;->n()Lz;

    move-result-object v1

    .line 449
    invoke-virtual {p1}, Lu;->o()Landroid/content/res/Resources;

    move-result-object v2

    .line 452
    invoke-virtual {p1}, Lu;->n()Lz;

    move-result-object v3

    invoke-virtual {p3}, Lhxc;->c()I

    move-result v4

    .line 451
    invoke-static {v3, p2, v4}, Lhxm;->a(Landroid/content/Context;II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 453
    invoke-virtual {p1}, Lu;->n()Lz;

    move-result-object v3

    invoke-static {v3, p2}, Ldhv;->m(Landroid/content/Context;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 455
    invoke-virtual {p3}, Lhxc;->b()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a04f3

    .line 456
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0a0596

    .line 457
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0a0597

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 455
    invoke-static {v3, v4, v5, v2}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v2

    .line 459
    invoke-virtual {v2, p1, v0}, Llgr;->a(Lu;I)V

    .line 460
    invoke-virtual {p1}, Lu;->p()Lae;

    move-result-object v0

    invoke-virtual {v2, v0, p4}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 461
    new-instance v0, Lfel;

    invoke-direct {v0, v1, p2, p5}, Lfel;-><init>(Landroid/app/Activity;ILlgs;)V

    invoke-virtual {v2, v0}, Llgr;->a(Llgs;)V

    .line 492
    const/4 v0, 0x1

    .line 494
    :cond_0
    return v0
.end method

.method public b(Lhfw;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhfw;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415
    invoke-virtual {p1}, Lhfw;->d()Lhxc;

    move-result-object v0

    .line 416
    if-eqz v0, :cond_0

    iget-object v0, p0, Lfek;->l:Ljava/util/Map;

    invoke-virtual {p1}, Lhfw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 441
    iget-boolean v0, p0, Lfek;->q:Z

    return v0
.end method

.method public c(Lhfw;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 421
    invoke-virtual {p1}, Lhfw;->e()Ljqs;

    move-result-object v0

    .line 422
    if-eqz v0, :cond_0

    .line 423
    invoke-virtual {v0}, Ljqs;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 431
    :goto_0
    return-object v0

    .line 426
    :cond_0
    invoke-virtual {p1}, Lhfw;->f()Lkxr;

    move-result-object v0

    .line 427
    if-eqz v0, :cond_1

    .line 428
    iget-object v0, p0, Lfek;->m:Ljava/util/Map;

    invoke-virtual {p1}, Lhfw;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfen;

    iget-object v0, v0, Lfen;->b:Ljava/lang/String;

    goto :goto_0

    .line 431
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
