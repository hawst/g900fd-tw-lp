.class public final Lhil;
.super Lhyp;
.source "PG"


# static fields
.field static a:I

.field static b:I

.field static c:I


# instance fields
.field private d:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lhyp;-><init>()V

    .line 25
    iput-object p1, p0, Lhil;->d:Landroid/content/Context;

    .line 26
    sget v0, Lhil;->a:I

    if-nez v0, :cond_0

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 28
    const v1, 0x7f0d0239

    .line 29
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lhil;->a:I

    .line 30
    const v1, 0x7f0d0238

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lhil;->b:I

    .line 31
    const v1, 0x7f0b028a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lhil;->c:I

    .line 33
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/widget/Adapter;)V
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p0}, Lhil;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 48
    new-instance v0, Lhim;

    iget-object v1, p0, Lhil;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhim;-><init>(Landroid/content/Context;)V

    invoke-super {p0, v0}, Lhyp;->a(Landroid/widget/Adapter;)V

    .line 50
    :cond_0
    invoke-super {p0, p1}, Lhyp;->a(Landroid/widget/Adapter;)V

    .line 51
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method
