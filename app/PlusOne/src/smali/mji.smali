.class public final Lmji;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnev;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26323
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26326
    iput-object v0, p0, Lmji;->apiHeader:Llyq;

    .line 26329
    iput-object v0, p0, Lmji;->a:Lnev;

    .line 26323
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26346
    const/4 v0, 0x0

    .line 26347
    iget-object v1, p0, Lmji;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 26348
    const/4 v0, 0x1

    iget-object v1, p0, Lmji;->apiHeader:Llyq;

    .line 26349
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26351
    :cond_0
    iget-object v1, p0, Lmji;->a:Lnev;

    if-eqz v1, :cond_1

    .line 26352
    const/4 v1, 0x2

    iget-object v2, p0, Lmji;->a:Lnev;

    .line 26353
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26355
    :cond_1
    iget-object v1, p0, Lmji;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26356
    iput v0, p0, Lmji;->ai:I

    .line 26357
    return v0
.end method

.method public a(Loxn;)Lmji;
    .locals 2

    .prologue
    .line 26365
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26366
    sparse-switch v0, :sswitch_data_0

    .line 26370
    iget-object v1, p0, Lmji;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26371
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmji;->ah:Ljava/util/List;

    .line 26374
    :cond_1
    iget-object v1, p0, Lmji;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26376
    :sswitch_0
    return-object p0

    .line 26381
    :sswitch_1
    iget-object v0, p0, Lmji;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 26382
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmji;->apiHeader:Llyq;

    .line 26384
    :cond_2
    iget-object v0, p0, Lmji;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26388
    :sswitch_2
    iget-object v0, p0, Lmji;->a:Lnev;

    if-nez v0, :cond_3

    .line 26389
    new-instance v0, Lnev;

    invoke-direct {v0}, Lnev;-><init>()V

    iput-object v0, p0, Lmji;->a:Lnev;

    .line 26391
    :cond_3
    iget-object v0, p0, Lmji;->a:Lnev;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26366
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26334
    iget-object v0, p0, Lmji;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 26335
    const/4 v0, 0x1

    iget-object v1, p0, Lmji;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26337
    :cond_0
    iget-object v0, p0, Lmji;->a:Lnev;

    if-eqz v0, :cond_1

    .line 26338
    const/4 v0, 0x2

    iget-object v1, p0, Lmji;->a:Lnev;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26340
    :cond_1
    iget-object v0, p0, Lmji;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26342
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26319
    invoke-virtual {p0, p1}, Lmji;->a(Loxn;)Lmji;

    move-result-object v0

    return-object v0
.end method
