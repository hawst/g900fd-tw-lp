.class public final Lesl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Llnx;
.implements Llpu;
.implements Llqb;
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Lhms;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lesl;->a:Landroid/app/Activity;

    .line 54
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 55
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lhms;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Lesl;->b:Lhms;

    .line 61
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 62
    if-eqz v0, :cond_0

    .line 63
    invoke-interface {v0, p0}, Lhee;->a(Lheg;)Lhee;

    .line 65
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lesl;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lesl;->c:Z

    .line 97
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 69
    if-eqz p1, :cond_0

    .line 70
    const-string v0, "com.google.android.apps.plus.instrumentation.ActivityNotificationInstrumentation.EventRecorded"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lesl;->c:Z

    .line 72
    :cond_0
    return-void
.end method

.method public a(ZIIII)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 84
    const/4 v0, 0x3

    if-ne p3, v0, :cond_3

    .line 85
    iget-boolean v0, p0, Lesl;->c:Z

    if-nez v0, :cond_3

    .line 86
    iget-object v0, p0, Lesl;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_4

    const-string v0, "com.google.android.libraries.social.notifications.FROM_NOTIFICATION"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v0, "com.google.android.libraries.social.notifications.notif_id"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v6, Lhmv;->bw:Lhmv;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "extra_notification_id"

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "extra_notification_read"

    const-string v1, "com.google.android.libraries.social.notifications.NOTIFICATION_READ"

    invoke-virtual {v4, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    const-string v0, "com.google.android.libraries.social.notifications.notif_types"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "com.google.android.libraries.social.notifications.notif_types"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getIntegerArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_1
    const-string v1, "com.google.android.libraries.social.notifications.coalescing_codes"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "com.google.android.libraries.social.notifications.coalescing_codes"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ne v3, v7, :cond_1

    const-string v3, "extra_notification_types"

    invoke-virtual {v5, v3, v0}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v3, "extra_coalescing_codes"

    invoke-virtual {v5, v3, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_1
    const-string v1, "EXTRA_ACTIVITY_IS_ENTRY_POINT"

    invoke-virtual {v4, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v1, Lhmw;->y:Lhmw;

    :goto_3
    iget-object v3, p0, Lesl;->b:Lhms;

    new-instance v4, Lhmr;

    iget-object v7, p0, Lesl;->a:Landroid/app/Activity;

    invoke-direct {v4, v7}, Lhmr;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v6}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v4

    invoke-virtual {v4, v1}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v4

    invoke-virtual {v4, v5}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v4

    invoke-interface {v3, v4}, Lhms;->a(Lhmr;)V

    const/16 v3, 0x12

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lesl;->b:Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lesl;->a:Landroid/app/Activity;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->en:Lhmv;

    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    invoke-virtual {v3, v1}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    invoke-virtual {v1, v5}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 87
    :cond_2
    iput-boolean v2, p0, Lesl;->c:Z

    .line 90
    :cond_3
    return-void

    :cond_4
    move v0, v3

    .line 86
    goto/16 :goto_0

    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_1

    :cond_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_2

    :cond_7
    sget-object v1, Lhmw;->w:Lhmw;

    goto :goto_3
.end method

.method public b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 184
    const-string v0, "EXTRA_ACTIVITY_IS_ENTRY_POINT"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 185
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 76
    const-string v0, "com.google.android.apps.plus.instrumentation.ActivityNotificationInstrumentation.EventRecorded"

    iget-boolean v1, p0, Lesl;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 77
    return-void
.end method
