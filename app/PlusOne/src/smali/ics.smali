.class final Lics;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Licq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Licq;)V
    .locals 1

    .prologue
    .line 369
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 370
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lics;->a:Ljava/lang/ref/WeakReference;

    .line 371
    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 374
    invoke-virtual {p0, v2}, Lics;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    const-wide/16 v0, 0x320

    invoke-virtual {p0, v2, v0, v1}, Lics;->sendEmptyMessageDelayed(IJ)Z

    .line 377
    :cond_0
    return-void
.end method

.method b()V
    .locals 1

    .prologue
    .line 380
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lics;->removeMessages(I)V

    .line 381
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 385
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 386
    iget-object v0, p0, Lics;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Licq;

    .line 387
    if-eqz v0, :cond_0

    .line 388
    invoke-virtual {v0}, Licq;->f()V

    .line 391
    :cond_0
    return-void
.end method
