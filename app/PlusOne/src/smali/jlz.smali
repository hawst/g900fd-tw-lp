.class public Ljlz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llre;
.implements Llrf;
.implements Llrg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Ljlw",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Llnx;",
        "Llre;",
        "Llrf;",
        "Llrg;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljma;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljma",
            "<-TT;>;"
        }
    .end annotation
.end field

.field private c:Ljlx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlx",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Llqr;Ljava/lang/Class;Ljma;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Llqr;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljma",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p2, p0, Ljlz;->a:Ljava/lang/Class;

    .line 46
    iput-object p3, p0, Ljlz;->b:Ljma;

    .line 47
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 48
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Ljlz;->c:Ljlx;

    iget-object v1, p0, Ljlz;->b:Ljma;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 59
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Ljlz;->a:Ljava/lang/Class;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljlw;

    .line 53
    invoke-interface {v0}, Ljlw;->b()Ljlx;

    move-result-object v0

    iput-object v0, p0, Ljlz;->c:Ljlx;

    .line 54
    return-void
.end method

.method public aP_()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Ljlz;->c:Ljlx;

    iget-object v1, p0, Ljlz;->b:Ljma;

    invoke-interface {v0, v1}, Ljlx;->a(Ljma;)V

    .line 64
    return-void
.end method
