.class public final Lmuo;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmuo;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14300
    const/4 v0, 0x0

    new-array v0, v0, [Lmuo;

    sput-object v0, Lmuo;->a:[Lmuo;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14301
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 14332
    const/4 v0, 0x0

    .line 14333
    iget-object v1, p0, Lmuo;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 14334
    const/4 v0, 0x1

    iget-object v1, p0, Lmuo;->b:Ljava/lang/Integer;

    .line 14335
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 14337
    :cond_0
    iget-object v1, p0, Lmuo;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 14338
    const/4 v1, 0x2

    iget-object v2, p0, Lmuo;->c:Ljava/lang/String;

    .line 14339
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14341
    :cond_1
    iget-object v1, p0, Lmuo;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 14342
    const/4 v1, 0x3

    iget-object v2, p0, Lmuo;->d:Ljava/lang/Long;

    .line 14343
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 14345
    :cond_2
    iget-object v1, p0, Lmuo;->e:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 14346
    const/4 v1, 0x4

    iget-object v2, p0, Lmuo;->e:Ljava/lang/Long;

    .line 14347
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 14349
    :cond_3
    iget-object v1, p0, Lmuo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14350
    iput v0, p0, Lmuo;->ai:I

    .line 14351
    return v0
.end method

.method public a(Loxn;)Lmuo;
    .locals 2

    .prologue
    .line 14359
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 14360
    sparse-switch v0, :sswitch_data_0

    .line 14364
    iget-object v1, p0, Lmuo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 14365
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmuo;->ah:Ljava/util/List;

    .line 14368
    :cond_1
    iget-object v1, p0, Lmuo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 14370
    :sswitch_0
    return-object p0

    .line 14375
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmuo;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 14379
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmuo;->c:Ljava/lang/String;

    goto :goto_0

    .line 14383
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmuo;->d:Ljava/lang/Long;

    goto :goto_0

    .line 14387
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmuo;->e:Ljava/lang/Long;

    goto :goto_0

    .line 14360
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 14314
    iget-object v0, p0, Lmuo;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 14315
    const/4 v0, 0x1

    iget-object v1, p0, Lmuo;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 14317
    :cond_0
    iget-object v0, p0, Lmuo;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 14318
    const/4 v0, 0x2

    iget-object v1, p0, Lmuo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 14320
    :cond_1
    iget-object v0, p0, Lmuo;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 14321
    const/4 v0, 0x3

    iget-object v1, p0, Lmuo;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 14323
    :cond_2
    iget-object v0, p0, Lmuo;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 14324
    const/4 v0, 0x4

    iget-object v1, p0, Lmuo;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 14326
    :cond_3
    iget-object v0, p0, Lmuo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 14328
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 14297
    invoke-virtual {p0, p1}, Lmuo;->a(Loxn;)Lmuo;

    move-result-object v0

    return-object v0
.end method
