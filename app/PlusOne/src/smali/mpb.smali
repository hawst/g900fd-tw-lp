.class public final Lmpb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lltu;

.field public b:Locf;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 314
    invoke-direct {p0}, Loxq;-><init>()V

    .line 317
    iput-object v0, p0, Lmpb;->a:Lltu;

    .line 330
    iput-object v0, p0, Lmpb;->b:Locf;

    .line 314
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 362
    const/4 v0, 0x0

    .line 363
    iget-object v1, p0, Lmpb;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 364
    const/4 v0, 0x2

    iget-object v1, p0, Lmpb;->c:Ljava/lang/String;

    .line 365
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 367
    :cond_0
    iget-object v1, p0, Lmpb;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 368
    const/4 v1, 0x3

    iget-object v2, p0, Lmpb;->d:Ljava/lang/String;

    .line 369
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_1
    iget-object v1, p0, Lmpb;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 372
    const/4 v1, 0x4

    iget-object v2, p0, Lmpb;->e:Ljava/lang/String;

    .line 373
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_2
    iget-object v1, p0, Lmpb;->a:Lltu;

    if-eqz v1, :cond_3

    .line 376
    const/4 v1, 0x5

    iget-object v2, p0, Lmpb;->a:Lltu;

    .line 377
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_3
    iget-object v1, p0, Lmpb;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 380
    const/4 v1, 0x6

    iget-object v2, p0, Lmpb;->f:Ljava/lang/Boolean;

    .line 381
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 383
    :cond_4
    iget-object v1, p0, Lmpb;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 384
    const/4 v1, 0x7

    iget-object v2, p0, Lmpb;->g:Ljava/lang/Boolean;

    .line 385
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 387
    :cond_5
    iget-object v1, p0, Lmpb;->b:Locf;

    if-eqz v1, :cond_6

    .line 388
    const/16 v1, 0x8

    iget-object v2, p0, Lmpb;->b:Locf;

    .line 389
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 391
    :cond_6
    iget-object v1, p0, Lmpb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 392
    iput v0, p0, Lmpb;->ai:I

    .line 393
    return v0
.end method

.method public a(Loxn;)Lmpb;
    .locals 2

    .prologue
    .line 401
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 402
    sparse-switch v0, :sswitch_data_0

    .line 406
    iget-object v1, p0, Lmpb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 407
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmpb;->ah:Ljava/util/List;

    .line 410
    :cond_1
    iget-object v1, p0, Lmpb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 412
    :sswitch_0
    return-object p0

    .line 417
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpb;->c:Ljava/lang/String;

    goto :goto_0

    .line 421
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpb;->d:Ljava/lang/String;

    goto :goto_0

    .line 425
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpb;->e:Ljava/lang/String;

    goto :goto_0

    .line 429
    :sswitch_4
    iget-object v0, p0, Lmpb;->a:Lltu;

    if-nez v0, :cond_2

    .line 430
    new-instance v0, Lltu;

    invoke-direct {v0}, Lltu;-><init>()V

    iput-object v0, p0, Lmpb;->a:Lltu;

    .line 432
    :cond_2
    iget-object v0, p0, Lmpb;->a:Lltu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 436
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpb;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 440
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpb;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 444
    :sswitch_7
    iget-object v0, p0, Lmpb;->b:Locf;

    if-nez v0, :cond_3

    .line 445
    new-instance v0, Locf;

    invoke-direct {v0}, Locf;-><init>()V

    iput-object v0, p0, Lmpb;->b:Locf;

    .line 447
    :cond_3
    iget-object v0, p0, Lmpb;->b:Locf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 402
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lmpb;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 336
    const/4 v0, 0x2

    iget-object v1, p0, Lmpb;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 338
    :cond_0
    iget-object v0, p0, Lmpb;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 339
    const/4 v0, 0x3

    iget-object v1, p0, Lmpb;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 341
    :cond_1
    iget-object v0, p0, Lmpb;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 342
    const/4 v0, 0x4

    iget-object v1, p0, Lmpb;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 344
    :cond_2
    iget-object v0, p0, Lmpb;->a:Lltu;

    if-eqz v0, :cond_3

    .line 345
    const/4 v0, 0x5

    iget-object v1, p0, Lmpb;->a:Lltu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 347
    :cond_3
    iget-object v0, p0, Lmpb;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 348
    const/4 v0, 0x6

    iget-object v1, p0, Lmpb;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 350
    :cond_4
    iget-object v0, p0, Lmpb;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 351
    const/4 v0, 0x7

    iget-object v1, p0, Lmpb;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 353
    :cond_5
    iget-object v0, p0, Lmpb;->b:Locf;

    if-eqz v0, :cond_6

    .line 354
    const/16 v0, 0x8

    iget-object v1, p0, Lmpb;->b:Locf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 356
    :cond_6
    iget-object v0, p0, Lmpb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 358
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 310
    invoke-virtual {p0, p1}, Lmpb;->a(Loxn;)Lmpb;

    move-result-object v0

    return-object v0
.end method
