.class public final Loqa;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:Loqb;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput v0, p0, Loqa;->a:I

    .line 30
    iput v0, p0, Loqa;->b:I

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Loqa;->c:Loqb;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 58
    const/4 v0, 0x0

    .line 59
    iget v1, p0, Loqa;->a:I

    if-eq v1, v3, :cond_0

    .line 60
    const/4 v0, 0x1

    iget v1, p0, Loqa;->a:I

    .line 61
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 63
    :cond_0
    iget-object v1, p0, Loqa;->c:Loqb;

    if-eqz v1, :cond_1

    .line 64
    const/4 v1, 0x2

    iget-object v2, p0, Loqa;->c:Loqb;

    .line 65
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    :cond_1
    iget-object v1, p0, Loqa;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 68
    const/4 v1, 0x3

    iget-object v2, p0, Loqa;->d:Ljava/lang/String;

    .line 69
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_2
    iget v1, p0, Loqa;->b:I

    if-eq v1, v3, :cond_3

    .line 72
    const/4 v1, 0x4

    iget v2, p0, Loqa;->b:I

    .line 73
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_3
    iget-object v1, p0, Loqa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    iput v0, p0, Loqa;->ai:I

    .line 77
    return v0
.end method

.method public a(Loxn;)Loqa;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 85
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 86
    sparse-switch v0, :sswitch_data_0

    .line 90
    iget-object v1, p0, Loqa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 91
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loqa;->ah:Ljava/util/List;

    .line 94
    :cond_1
    iget-object v1, p0, Loqa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    :sswitch_0
    return-object p0

    .line 101
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 102
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 107
    :cond_2
    iput v0, p0, Loqa;->a:I

    goto :goto_0

    .line 109
    :cond_3
    iput v2, p0, Loqa;->a:I

    goto :goto_0

    .line 114
    :sswitch_2
    iget-object v0, p0, Loqa;->c:Loqb;

    if-nez v0, :cond_4

    .line 115
    new-instance v0, Loqb;

    invoke-direct {v0}, Loqb;-><init>()V

    iput-object v0, p0, Loqa;->c:Loqb;

    .line 117
    :cond_4
    iget-object v0, p0, Loqa;->c:Loqb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 121
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loqa;->d:Ljava/lang/String;

    goto :goto_0

    .line 125
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 126
    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-ne v0, v4, :cond_6

    .line 129
    :cond_5
    iput v0, p0, Loqa;->b:I

    goto :goto_0

    .line 131
    :cond_6
    iput v2, p0, Loqa;->b:I

    goto :goto_0

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 40
    iget v0, p0, Loqa;->a:I

    if-eq v0, v2, :cond_0

    .line 41
    const/4 v0, 0x1

    iget v1, p0, Loqa;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 43
    :cond_0
    iget-object v0, p0, Loqa;->c:Loqb;

    if-eqz v0, :cond_1

    .line 44
    const/4 v0, 0x2

    iget-object v1, p0, Loqa;->c:Loqb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 46
    :cond_1
    iget-object v0, p0, Loqa;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 47
    const/4 v0, 0x3

    iget-object v1, p0, Loqa;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 49
    :cond_2
    iget v0, p0, Loqa;->b:I

    if-eq v0, v2, :cond_3

    .line 50
    const/4 v0, 0x4

    iget v1, p0, Loqa;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 52
    :cond_3
    iget-object v0, p0, Loqa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 54
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loqa;->a(Loxn;)Loqa;

    move-result-object v0

    return-object v0
.end method
