.class public final Lmna;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmna;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[B

.field public d:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7166
    const/4 v0, 0x0

    new-array v0, v0, [Lmna;

    sput-object v0, Lmna;->a:[Lmna;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7167
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 7193
    const/4 v0, 0x0

    .line 7194
    iget-object v1, p0, Lmna;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 7195
    const/4 v0, 0x1

    iget-object v1, p0, Lmna;->b:Ljava/lang/Integer;

    .line 7196
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7198
    :cond_0
    iget-object v1, p0, Lmna;->c:[B

    if-eqz v1, :cond_1

    .line 7199
    const/4 v1, 0x2

    iget-object v2, p0, Lmna;->c:[B

    .line 7200
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 7202
    :cond_1
    iget-object v1, p0, Lmna;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 7203
    const/4 v1, 0x3

    iget-object v2, p0, Lmna;->d:Ljava/lang/Long;

    .line 7204
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7206
    :cond_2
    iget-object v1, p0, Lmna;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7207
    iput v0, p0, Lmna;->ai:I

    .line 7208
    return v0
.end method

.method public a(Loxn;)Lmna;
    .locals 2

    .prologue
    .line 7216
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7217
    sparse-switch v0, :sswitch_data_0

    .line 7221
    iget-object v1, p0, Lmna;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7222
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmna;->ah:Ljava/util/List;

    .line 7225
    :cond_1
    iget-object v1, p0, Lmna;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7227
    :sswitch_0
    return-object p0

    .line 7232
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmna;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 7236
    :sswitch_2
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lmna;->c:[B

    goto :goto_0

    .line 7240
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmna;->d:Ljava/lang/Long;

    goto :goto_0

    .line 7217
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 7178
    iget-object v0, p0, Lmna;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 7179
    const/4 v0, 0x1

    iget-object v1, p0, Lmna;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7181
    :cond_0
    iget-object v0, p0, Lmna;->c:[B

    if-eqz v0, :cond_1

    .line 7182
    const/4 v0, 0x2

    iget-object v1, p0, Lmna;->c:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 7184
    :cond_1
    iget-object v0, p0, Lmna;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 7185
    const/4 v0, 0x3

    iget-object v1, p0, Lmna;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 7187
    :cond_2
    iget-object v0, p0, Lmna;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7189
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7163
    invoke-virtual {p0, p1}, Lmna;->a(Loxn;)Lmna;

    move-result-object v0

    return-object v0
.end method
