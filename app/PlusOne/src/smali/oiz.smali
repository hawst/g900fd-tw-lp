.class public final Loiz;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Long;

.field private d:Loia;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5451
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5460
    const/4 v0, 0x0

    iput-object v0, p0, Loiz;->d:Loia;

    .line 5451
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 5483
    const/4 v0, 0x0

    .line 5484
    iget-object v1, p0, Loiz;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 5485
    const/4 v0, 0x1

    iget-object v1, p0, Loiz;->a:Ljava/lang/Long;

    .line 5486
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5488
    :cond_0
    iget-object v1, p0, Loiz;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5489
    const/4 v1, 0x2

    iget-object v2, p0, Loiz;->b:Ljava/lang/String;

    .line 5490
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5492
    :cond_1
    iget-object v1, p0, Loiz;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 5493
    const/4 v1, 0x3

    iget-object v2, p0, Loiz;->c:Ljava/lang/Long;

    .line 5494
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5496
    :cond_2
    iget-object v1, p0, Loiz;->d:Loia;

    if-eqz v1, :cond_3

    .line 5497
    const/4 v1, 0x4

    iget-object v2, p0, Loiz;->d:Loia;

    .line 5498
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5500
    :cond_3
    iget-object v1, p0, Loiz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5501
    iput v0, p0, Loiz;->ai:I

    .line 5502
    return v0
.end method

.method public a(Loxn;)Loiz;
    .locals 2

    .prologue
    .line 5510
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5511
    sparse-switch v0, :sswitch_data_0

    .line 5515
    iget-object v1, p0, Loiz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5516
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loiz;->ah:Ljava/util/List;

    .line 5519
    :cond_1
    iget-object v1, p0, Loiz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5521
    :sswitch_0
    return-object p0

    .line 5526
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loiz;->a:Ljava/lang/Long;

    goto :goto_0

    .line 5530
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loiz;->b:Ljava/lang/String;

    goto :goto_0

    .line 5534
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loiz;->c:Ljava/lang/Long;

    goto :goto_0

    .line 5538
    :sswitch_4
    iget-object v0, p0, Loiz;->d:Loia;

    if-nez v0, :cond_2

    .line 5539
    new-instance v0, Loia;

    invoke-direct {v0}, Loia;-><init>()V

    iput-object v0, p0, Loiz;->d:Loia;

    .line 5541
    :cond_2
    iget-object v0, p0, Loiz;->d:Loia;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5511
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 5465
    iget-object v0, p0, Loiz;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 5466
    const/4 v0, 0x1

    iget-object v1, p0, Loiz;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 5468
    :cond_0
    iget-object v0, p0, Loiz;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5469
    const/4 v0, 0x2

    iget-object v1, p0, Loiz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5471
    :cond_1
    iget-object v0, p0, Loiz;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 5472
    const/4 v0, 0x3

    iget-object v1, p0, Loiz;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 5474
    :cond_2
    iget-object v0, p0, Loiz;->d:Loia;

    if-eqz v0, :cond_3

    .line 5475
    const/4 v0, 0x4

    iget-object v1, p0, Loiz;->d:Loia;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5477
    :cond_3
    iget-object v0, p0, Loiz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5479
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5447
    invoke-virtual {p0, p1}, Loiz;->a(Loxn;)Loiz;

    move-result-object v0

    return-object v0
.end method
