.class public final Loya;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loya;


# instance fields
.field public b:[I

.field public c:Ljava/lang/String;

.field public d:Loyp;

.field private e:Ljava/lang/String;

.field private f:Lpdj;

.field private g:Ljava/lang/String;

.field private h:Lpci;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Loya;

    sput-object v0, Loya;->a:[Loya;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Loya;->b:[I

    .line 20
    iput-object v1, p0, Loya;->d:Loyp;

    .line 23
    iput-object v1, p0, Loya;->f:Lpdj;

    .line 28
    iput-object v1, p0, Loya;->h:Lpci;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 62
    .line 63
    iget-object v1, p0, Loya;->b:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Loya;->b:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 65
    iget-object v2, p0, Loya;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 67
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    iget-object v0, p0, Loya;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 72
    :cond_1
    iget-object v1, p0, Loya;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 73
    const/4 v1, 0x2

    iget-object v2, p0, Loya;->c:Ljava/lang/String;

    .line 74
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_2
    iget-object v1, p0, Loya;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 77
    const/4 v1, 0x3

    iget-object v2, p0, Loya;->e:Ljava/lang/String;

    .line 78
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_3
    iget-object v1, p0, Loya;->d:Loyp;

    if-eqz v1, :cond_4

    .line 81
    const/4 v1, 0x4

    iget-object v2, p0, Loya;->d:Loyp;

    .line 82
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_4
    iget-object v1, p0, Loya;->f:Lpdj;

    if-eqz v1, :cond_5

    .line 85
    const/4 v1, 0x5

    iget-object v2, p0, Loya;->f:Lpdj;

    .line 86
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_5
    iget-object v1, p0, Loya;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 89
    const/4 v1, 0x6

    iget-object v2, p0, Loya;->g:Ljava/lang/String;

    .line 90
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_6
    iget-object v1, p0, Loya;->h:Lpci;

    if-eqz v1, :cond_7

    .line 93
    const/4 v1, 0x7

    iget-object v2, p0, Loya;->h:Lpci;

    .line 94
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_7
    iget-object v1, p0, Loya;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    iput v0, p0, Loya;->ai:I

    .line 98
    return v0
.end method

.method public a(Loxn;)Loya;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 106
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 107
    sparse-switch v0, :sswitch_data_0

    .line 111
    iget-object v1, p0, Loya;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loya;->ah:Ljava/util/List;

    .line 115
    :cond_1
    iget-object v1, p0, Loya;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    :sswitch_0
    return-object p0

    .line 122
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 123
    iget-object v0, p0, Loya;->b:[I

    array-length v0, v0

    .line 124
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 125
    iget-object v2, p0, Loya;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    iput-object v1, p0, Loya;->b:[I

    .line 127
    :goto_1
    iget-object v1, p0, Loya;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 128
    iget-object v1, p0, Loya;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 129
    invoke-virtual {p1}, Loxn;->a()I

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 132
    :cond_2
    iget-object v1, p0, Loya;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 136
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loya;->c:Ljava/lang/String;

    goto :goto_0

    .line 140
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loya;->e:Ljava/lang/String;

    goto :goto_0

    .line 144
    :sswitch_4
    iget-object v0, p0, Loya;->d:Loyp;

    if-nez v0, :cond_3

    .line 145
    new-instance v0, Loyp;

    invoke-direct {v0}, Loyp;-><init>()V

    iput-object v0, p0, Loya;->d:Loyp;

    .line 147
    :cond_3
    iget-object v0, p0, Loya;->d:Loyp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 151
    :sswitch_5
    iget-object v0, p0, Loya;->f:Lpdj;

    if-nez v0, :cond_4

    .line 152
    new-instance v0, Lpdj;

    invoke-direct {v0}, Lpdj;-><init>()V

    iput-object v0, p0, Loya;->f:Lpdj;

    .line 154
    :cond_4
    iget-object v0, p0, Loya;->f:Lpdj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 158
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loya;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 162
    :sswitch_7
    iget-object v0, p0, Loya;->h:Lpci;

    if-nez v0, :cond_5

    .line 163
    new-instance v0, Lpci;

    invoke-direct {v0}, Lpci;-><init>()V

    iput-object v0, p0, Loya;->h:Lpci;

    .line 165
    :cond_5
    iget-object v0, p0, Loya;->h:Lpci;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 107
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Loya;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Loya;->b:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 34
    iget-object v1, p0, Loya;->b:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 35
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_0
    iget-object v0, p0, Loya;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 39
    const/4 v0, 0x2

    iget-object v1, p0, Loya;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 41
    :cond_1
    iget-object v0, p0, Loya;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 42
    const/4 v0, 0x3

    iget-object v1, p0, Loya;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 44
    :cond_2
    iget-object v0, p0, Loya;->d:Loyp;

    if-eqz v0, :cond_3

    .line 45
    const/4 v0, 0x4

    iget-object v1, p0, Loya;->d:Loyp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 47
    :cond_3
    iget-object v0, p0, Loya;->f:Lpdj;

    if-eqz v0, :cond_4

    .line 48
    const/4 v0, 0x5

    iget-object v1, p0, Loya;->f:Lpdj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 50
    :cond_4
    iget-object v0, p0, Loya;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 51
    const/4 v0, 0x6

    iget-object v1, p0, Loya;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 53
    :cond_5
    iget-object v0, p0, Loya;->h:Lpci;

    if-eqz v0, :cond_6

    .line 54
    const/4 v0, 0x7

    iget-object v1, p0, Loya;->h:Lpci;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 56
    :cond_6
    iget-object v0, p0, Loya;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 58
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loya;->a(Loxn;)Loya;

    move-result-object v0

    return-object v0
.end method
