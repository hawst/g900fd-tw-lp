.class public final Lnqn;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnqp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 156
    invoke-direct {p0}, Loxq;-><init>()V

    .line 159
    sget-object v0, Lnqp;->a:[Lnqp;

    iput-object v0, p0, Lnqn;->a:[Lnqp;

    .line 156
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 177
    .line 178
    iget-object v1, p0, Lnqn;->a:[Lnqp;

    if-eqz v1, :cond_1

    .line 179
    iget-object v2, p0, Lnqn;->a:[Lnqp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 180
    if-eqz v4, :cond_0

    .line 181
    const/4 v5, 0x1

    .line 182
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 179
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 186
    :cond_1
    iget-object v1, p0, Lnqn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    iput v0, p0, Lnqn;->ai:I

    .line 188
    return v0
.end method

.method public a(Loxn;)Lnqn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 196
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 197
    sparse-switch v0, :sswitch_data_0

    .line 201
    iget-object v2, p0, Lnqn;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 202
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnqn;->ah:Ljava/util/List;

    .line 205
    :cond_1
    iget-object v2, p0, Lnqn;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    :sswitch_0
    return-object p0

    .line 212
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 213
    iget-object v0, p0, Lnqn;->a:[Lnqp;

    if-nez v0, :cond_3

    move v0, v1

    .line 214
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnqp;

    .line 215
    iget-object v3, p0, Lnqn;->a:[Lnqp;

    if-eqz v3, :cond_2

    .line 216
    iget-object v3, p0, Lnqn;->a:[Lnqp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 218
    :cond_2
    iput-object v2, p0, Lnqn;->a:[Lnqp;

    .line 219
    :goto_2
    iget-object v2, p0, Lnqn;->a:[Lnqp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 220
    iget-object v2, p0, Lnqn;->a:[Lnqp;

    new-instance v3, Lnqp;

    invoke-direct {v3}, Lnqp;-><init>()V

    aput-object v3, v2, v0

    .line 221
    iget-object v2, p0, Lnqn;->a:[Lnqp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 222
    invoke-virtual {p1}, Loxn;->a()I

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 213
    :cond_3
    iget-object v0, p0, Lnqn;->a:[Lnqp;

    array-length v0, v0

    goto :goto_1

    .line 225
    :cond_4
    iget-object v2, p0, Lnqn;->a:[Lnqp;

    new-instance v3, Lnqp;

    invoke-direct {v3}, Lnqp;-><init>()V

    aput-object v3, v2, v0

    .line 226
    iget-object v2, p0, Lnqn;->a:[Lnqp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 197
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 164
    iget-object v0, p0, Lnqn;->a:[Lnqp;

    if-eqz v0, :cond_1

    .line 165
    iget-object v1, p0, Lnqn;->a:[Lnqp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 166
    if-eqz v3, :cond_0

    .line 167
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 165
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_1
    iget-object v0, p0, Lnqn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 173
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0, p1}, Lnqn;->a(Loxn;)Lnqn;

    move-result-object v0

    return-object v0
.end method
