.class public final Lmys;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Boolean;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 368
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 409
    const/4 v0, 0x0

    .line 410
    iget-object v1, p0, Lmys;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 411
    const/4 v0, 0x1

    iget-object v1, p0, Lmys;->a:Ljava/lang/Integer;

    .line 412
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->g(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 414
    :cond_0
    iget-object v1, p0, Lmys;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 415
    const/4 v1, 0x2

    iget-object v2, p0, Lmys;->b:Ljava/lang/String;

    .line 416
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 418
    :cond_1
    iget-object v1, p0, Lmys;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 419
    const/4 v1, 0x3

    iget-object v2, p0, Lmys;->c:Ljava/lang/Integer;

    .line 420
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 422
    :cond_2
    iget-object v1, p0, Lmys;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 423
    const/4 v1, 0x4

    iget-object v2, p0, Lmys;->e:Ljava/lang/String;

    .line 424
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 426
    :cond_3
    iget-object v1, p0, Lmys;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 427
    const/4 v1, 0x5

    iget-object v2, p0, Lmys;->d:Ljava/lang/Boolean;

    .line 428
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 430
    :cond_4
    iget-object v1, p0, Lmys;->f:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 431
    const/4 v1, 0x6

    iget-object v2, p0, Lmys;->f:Ljava/lang/Long;

    .line 432
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 434
    :cond_5
    iget-object v1, p0, Lmys;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    iput v0, p0, Lmys;->ai:I

    .line 436
    return v0
.end method

.method public a(Loxn;)Lmys;
    .locals 2

    .prologue
    .line 444
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 445
    sparse-switch v0, :sswitch_data_0

    .line 449
    iget-object v1, p0, Lmys;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 450
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmys;->ah:Ljava/util/List;

    .line 453
    :cond_1
    iget-object v1, p0, Lmys;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 455
    :sswitch_0
    return-object p0

    .line 460
    :sswitch_1
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmys;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 464
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmys;->b:Ljava/lang/String;

    goto :goto_0

    .line 468
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmys;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 472
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmys;->e:Ljava/lang/String;

    goto :goto_0

    .line 476
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmys;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 480
    :sswitch_6
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmys;->f:Ljava/lang/Long;

    goto :goto_0

    .line 445
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 385
    iget-object v0, p0, Lmys;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 386
    const/4 v0, 0x1

    iget-object v1, p0, Lmys;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 388
    :cond_0
    iget-object v0, p0, Lmys;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 389
    const/4 v0, 0x2

    iget-object v1, p0, Lmys;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 391
    :cond_1
    iget-object v0, p0, Lmys;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 392
    const/4 v0, 0x3

    iget-object v1, p0, Lmys;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 394
    :cond_2
    iget-object v0, p0, Lmys;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 395
    const/4 v0, 0x4

    iget-object v1, p0, Lmys;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 397
    :cond_3
    iget-object v0, p0, Lmys;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 398
    const/4 v0, 0x5

    iget-object v1, p0, Lmys;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 400
    :cond_4
    iget-object v0, p0, Lmys;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 401
    const/4 v0, 0x6

    iget-object v1, p0, Lmys;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 403
    :cond_5
    iget-object v0, p0, Lmys;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 405
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 364
    invoke-virtual {p0, p1}, Lmys;->a(Loxn;)Lmys;

    move-result-object v0

    return-object v0
.end method
