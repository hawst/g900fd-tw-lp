.class final Lecz;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:I

.field private synthetic e:Leco;


# direct methods
.method constructor <init>(Leco;)V
    .locals 0

    .prologue
    .line 1825
    iput-object p1, p0, Lecz;->e:Leco;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 1835
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Landroid/app/Activity;

    .line 1837
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1838
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 1850
    :goto_0
    return-object v0

    .line 1841
    :cond_0
    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhpu;

    .line 1844
    const/4 v2, 0x1

    aget-object v2, p1, v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lecz;->d:I

    .line 1845
    invoke-static {}, Lhqd;->a()Z

    move-result v2

    iput-boolean v2, p0, Lecz;->a:Z

    .line 1847
    iget v2, p0, Lecz;->d:I

    invoke-static {v0, v2}, Lhqd;->e(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lecz;->b:Z

    .line 1848
    iget v0, p0, Lecz;->d:I

    invoke-virtual {v1, v0}, Lhpu;->e(I)Z

    move-result v0

    iput-boolean v0, p0, Lecz;->c:Z

    .line 1850
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 1857
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1867
    :goto_0
    return-void

    .line 1861
    :cond_0
    iget-boolean v0, p0, Lecz;->a:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lecz;->b:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lecz;->c:Z

    if-eqz v0, :cond_1

    .line 1862
    iget-object v0, p0, Lecz;->e:Leco;

    invoke-static {v0}, Leco;->d(Leco;)V

    goto :goto_0

    .line 1864
    :cond_1
    iget-object v0, p0, Lecz;->e:Leco;

    iget-boolean v1, p0, Lecz;->a:Z

    iget-boolean v2, p0, Lecz;->b:Z

    iget-boolean v3, p0, Lecz;->c:Z

    invoke-static {v0, v1, v2, v3}, Leco;->a(Leco;ZZZ)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1825
    invoke-virtual {p0, p1}, Lecz;->a([Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1825
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lecz;->a(Ljava/lang/Boolean;)V

    return-void
.end method
