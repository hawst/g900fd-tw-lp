.class public final Ljrk;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 21
    const-string v0, "UpdateLegalNotificationsTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 22
    iput-object p1, p0, Ljrk;->a:Landroid/content/Context;

    .line 23
    iput p2, p0, Ljrk;->b:I

    .line 24
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 36
    new-instance v0, Ljrj;

    iget-object v1, p0, Ljrk;->a:Landroid/content/Context;

    iget v2, p0, Ljrk;->b:I

    invoke-direct {v0, v1, v2}, Ljrj;-><init>(Landroid/content/Context;I)V

    .line 39
    iget-boolean v1, p0, Ljrk;->c:Z

    if-eqz v1, :cond_0

    .line 40
    invoke-virtual {v0}, Ljrj;->j()V

    .line 42
    :cond_0
    iget-boolean v1, p0, Ljrk;->d:Z

    if-eqz v1, :cond_1

    .line 43
    invoke-virtual {v0}, Ljrj;->k()V

    .line 46
    :cond_1
    invoke-virtual {v0}, Ljrj;->l()V

    .line 48
    invoke-virtual {v0}, Ljrj;->t()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 49
    const-string v1, "UpdateLegalNotificationsTask"

    iget v2, v0, Lkff;->i:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Could not upload settings: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 50
    new-instance v0, Lhoz;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v5, v5}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 52
    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lhoz;

    const/16 v1, 0xc8

    invoke-direct {v0, v1, v5, v5}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljrk;->c:Z

    .line 28
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljrk;->d:Z

    .line 32
    return-void
.end method
