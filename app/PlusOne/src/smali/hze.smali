.class public final Lhze;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llql;
.implements Llrd;
.implements Llrg;
.implements Llrh;


# instance fields
.field private final a:Lae;

.field private final b:Lhzg;

.field private final c:Lhyp;

.field private final d:Lhzc;

.field private final e:Ljava/lang/String;

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lhzc;Ljava/lang/String;Lhzg;Lae;Llqr;)V
    .locals 7

    .prologue
    .line 42
    new-instance v6, Lhzh;

    invoke-direct {v6}, Lhzh;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lhze;-><init>(Lhzc;Ljava/lang/String;Lhzg;Lae;Llqr;Lhyp;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Lhzc;Ljava/lang/String;Lhzg;Lae;Llqr;Lhyp;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lhze;->d:Lhzc;

    .line 50
    iput-object p2, p0, Lhze;->e:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lhze;->b:Lhzg;

    .line 52
    iput-object p4, p0, Lhze;->a:Lae;

    .line 53
    iput-object p6, p0, Lhze;->c:Lhyp;

    .line 54
    invoke-virtual {p5, p0}, Llqr;->a(Llrg;)Llrg;

    .line 55
    return-void
.end method

.method private a(Lhyz;)V
    .locals 2

    .prologue
    .line 130
    new-instance v0, Lhza;

    iget-object v1, p0, Lhze;->f:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lhza;-><init>(Landroid/content/Context;Lhyz;)V

    .line 132
    iget-object v1, p0, Lhze;->c:Lhyp;

    invoke-virtual {v1, v0}, Lhyp;->a(Landroid/widget/Adapter;)V

    .line 133
    new-instance v1, Lhzf;

    invoke-direct {v1, v0}, Lhzf;-><init>(Lhza;)V

    invoke-interface {p1, v1}, Lhyz;->a(Lhzb;)V

    .line 139
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lhze;->c:Lhyp;

    return-object v0
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lhze;->f:Landroid/content/Context;

    .line 60
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lhze;->a:Lae;

    invoke-virtual {v1}, Lae;->f()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_0
    iget-object v1, p0, Lhze;->a:Lae;

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v2

    move v1, v0

    :goto_0
    iget-object v0, p0, Lhze;->d:Lhzc;

    invoke-virtual {v0}, Lhzc;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lhze;->d:Lhzc;

    invoke-virtual {v0, v1}, Lhzc;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhyz;

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "addDataProvider() called outside of onAddDataProviders() callback."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    instance-of v3, v0, Lu;

    if-eqz v3, :cond_2

    invoke-direct {p0, v0}, Lhze;->a(Lhyz;)V

    check-cast v0, Lu;

    iget-object v3, p0, Lhze;->e:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DataProvider must extend Fragment."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {v2}, Lat;->b()I

    .line 65
    :cond_4
    iget-object v0, p0, Lhze;->b:Lhzg;

    invoke-virtual {p0}, Lhze;->a()Landroid/widget/BaseAdapter;

    move-result-object v1

    invoke-interface {v0, v1}, Lhzg;->a(Landroid/widget/BaseAdapter;)V

    .line 66
    return-void

    :cond_5
    move v1, v0

    .line 64
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lu;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lhze;->e:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    check-cast v0, Lhyz;

    invoke-direct {p0, v0}, Lhze;->a(Lhyz;)V

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lhze;->e:Ljava/lang/String;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 71
    return-void
.end method
