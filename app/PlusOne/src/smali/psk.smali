.class public final Lpsk;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lpso;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    sget-object v0, Lpso;->a:[Lpso;

    iput-object v0, p0, Lpsk;->a:[Lpso;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 51
    .line 52
    iget-object v1, p0, Lpsk;->a:[Lpso;

    if-eqz v1, :cond_1

    .line 53
    iget-object v2, p0, Lpsk;->a:[Lpso;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 54
    if-eqz v4, :cond_0

    .line 55
    const/4 v5, 0x1

    .line 56
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 53
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, p0, Lpsk;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 61
    const/4 v1, 0x2

    iget-object v2, p0, Lpsk;->b:Ljava/lang/Boolean;

    .line 62
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 64
    :cond_2
    iget-object v1, p0, Lpsk;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 65
    const/4 v1, 0x3

    iget-object v2, p0, Lpsk;->c:Ljava/lang/Integer;

    .line 66
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_3
    iget-object v1, p0, Lpsk;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 69
    const/4 v1, 0x4

    iget-object v2, p0, Lpsk;->d:Ljava/lang/Integer;

    .line 70
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_4
    iget-object v1, p0, Lpsk;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 73
    const/4 v1, 0x5

    iget-object v2, p0, Lpsk;->e:Ljava/lang/Boolean;

    .line 74
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 76
    :cond_5
    iget-object v1, p0, Lpsk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    iput v0, p0, Lpsk;->ai:I

    .line 78
    return v0
.end method

.method public a(Loxn;)Lpsk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 86
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 87
    sparse-switch v0, :sswitch_data_0

    .line 91
    iget-object v2, p0, Lpsk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 92
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpsk;->ah:Ljava/util/List;

    .line 95
    :cond_1
    iget-object v2, p0, Lpsk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    :sswitch_0
    return-object p0

    .line 102
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 103
    iget-object v0, p0, Lpsk;->a:[Lpso;

    if-nez v0, :cond_3

    move v0, v1

    .line 104
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpso;

    .line 105
    iget-object v3, p0, Lpsk;->a:[Lpso;

    if-eqz v3, :cond_2

    .line 106
    iget-object v3, p0, Lpsk;->a:[Lpso;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 108
    :cond_2
    iput-object v2, p0, Lpsk;->a:[Lpso;

    .line 109
    :goto_2
    iget-object v2, p0, Lpsk;->a:[Lpso;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 110
    iget-object v2, p0, Lpsk;->a:[Lpso;

    new-instance v3, Lpso;

    invoke-direct {v3}, Lpso;-><init>()V

    aput-object v3, v2, v0

    .line 111
    iget-object v2, p0, Lpsk;->a:[Lpso;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 112
    invoke-virtual {p1}, Loxn;->a()I

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 103
    :cond_3
    iget-object v0, p0, Lpsk;->a:[Lpso;

    array-length v0, v0

    goto :goto_1

    .line 115
    :cond_4
    iget-object v2, p0, Lpsk;->a:[Lpso;

    new-instance v3, Lpso;

    invoke-direct {v3}, Lpso;-><init>()V

    aput-object v3, v2, v0

    .line 116
    iget-object v2, p0, Lpsk;->a:[Lpso;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 120
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpsk;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 124
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpsk;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 128
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpsk;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 132
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpsk;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 87
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 26
    iget-object v0, p0, Lpsk;->a:[Lpso;

    if-eqz v0, :cond_1

    .line 27
    iget-object v1, p0, Lpsk;->a:[Lpso;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 28
    if-eqz v3, :cond_0

    .line 29
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 27
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33
    :cond_1
    iget-object v0, p0, Lpsk;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 34
    const/4 v0, 0x2

    iget-object v1, p0, Lpsk;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 36
    :cond_2
    iget-object v0, p0, Lpsk;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 37
    const/4 v0, 0x3

    iget-object v1, p0, Lpsk;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 39
    :cond_3
    iget-object v0, p0, Lpsk;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 40
    const/4 v0, 0x4

    iget-object v1, p0, Lpsk;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 42
    :cond_4
    iget-object v0, p0, Lpsk;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 43
    const/4 v0, 0x5

    iget-object v1, p0, Lpsk;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 45
    :cond_5
    iget-object v0, p0, Lpsk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 47
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpsk;->a(Loxn;)Lpsk;

    move-result-object v0

    return-object v0
.end method
