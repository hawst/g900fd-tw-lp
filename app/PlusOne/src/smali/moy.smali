.class public final Lmoy;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmpd;

.field private b:[Lmpa;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 272
    invoke-direct {p0}, Loxq;-><init>()V

    .line 275
    sget-object v0, Lmpd;->a:[Lmpd;

    iput-object v0, p0, Lmoy;->a:[Lmpd;

    .line 278
    sget-object v0, Lmpa;->a:[Lmpa;

    iput-object v0, p0, Lmoy;->b:[Lmpa;

    .line 272
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 303
    .line 304
    iget-object v0, p0, Lmoy;->b:[Lmpa;

    if-eqz v0, :cond_1

    .line 305
    iget-object v3, p0, Lmoy;->b:[Lmpa;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 306
    if-eqz v5, :cond_0

    .line 307
    const/4 v6, 0x3

    .line 308
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 305
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 312
    :cond_2
    iget-object v2, p0, Lmoy;->a:[Lmpd;

    if-eqz v2, :cond_4

    .line 313
    iget-object v2, p0, Lmoy;->a:[Lmpd;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 314
    if-eqz v4, :cond_3

    .line 315
    const/4 v5, 0x4

    .line 316
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 313
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 320
    :cond_4
    iget-object v1, p0, Lmoy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    iput v0, p0, Lmoy;->ai:I

    .line 322
    return v0
.end method

.method public a(Loxn;)Lmoy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 330
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 331
    sparse-switch v0, :sswitch_data_0

    .line 335
    iget-object v2, p0, Lmoy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 336
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmoy;->ah:Ljava/util/List;

    .line 339
    :cond_1
    iget-object v2, p0, Lmoy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 341
    :sswitch_0
    return-object p0

    .line 346
    :sswitch_1
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 347
    iget-object v0, p0, Lmoy;->b:[Lmpa;

    if-nez v0, :cond_3

    move v0, v1

    .line 348
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmpa;

    .line 349
    iget-object v3, p0, Lmoy;->b:[Lmpa;

    if-eqz v3, :cond_2

    .line 350
    iget-object v3, p0, Lmoy;->b:[Lmpa;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 352
    :cond_2
    iput-object v2, p0, Lmoy;->b:[Lmpa;

    .line 353
    :goto_2
    iget-object v2, p0, Lmoy;->b:[Lmpa;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 354
    iget-object v2, p0, Lmoy;->b:[Lmpa;

    new-instance v3, Lmpa;

    invoke-direct {v3}, Lmpa;-><init>()V

    aput-object v3, v2, v0

    .line 355
    iget-object v2, p0, Lmoy;->b:[Lmpa;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 356
    invoke-virtual {p1}, Loxn;->a()I

    .line 353
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 347
    :cond_3
    iget-object v0, p0, Lmoy;->b:[Lmpa;

    array-length v0, v0

    goto :goto_1

    .line 359
    :cond_4
    iget-object v2, p0, Lmoy;->b:[Lmpa;

    new-instance v3, Lmpa;

    invoke-direct {v3}, Lmpa;-><init>()V

    aput-object v3, v2, v0

    .line 360
    iget-object v2, p0, Lmoy;->b:[Lmpa;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 364
    :sswitch_2
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 365
    iget-object v0, p0, Lmoy;->a:[Lmpd;

    if-nez v0, :cond_6

    move v0, v1

    .line 366
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmpd;

    .line 367
    iget-object v3, p0, Lmoy;->a:[Lmpd;

    if-eqz v3, :cond_5

    .line 368
    iget-object v3, p0, Lmoy;->a:[Lmpd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 370
    :cond_5
    iput-object v2, p0, Lmoy;->a:[Lmpd;

    .line 371
    :goto_4
    iget-object v2, p0, Lmoy;->a:[Lmpd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 372
    iget-object v2, p0, Lmoy;->a:[Lmpd;

    new-instance v3, Lmpd;

    invoke-direct {v3}, Lmpd;-><init>()V

    aput-object v3, v2, v0

    .line 373
    iget-object v2, p0, Lmoy;->a:[Lmpd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 374
    invoke-virtual {p1}, Loxn;->a()I

    .line 371
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 365
    :cond_6
    iget-object v0, p0, Lmoy;->a:[Lmpd;

    array-length v0, v0

    goto :goto_3

    .line 377
    :cond_7
    iget-object v2, p0, Lmoy;->a:[Lmpd;

    new-instance v3, Lmpd;

    invoke-direct {v3}, Lmpd;-><init>()V

    aput-object v3, v2, v0

    .line 378
    iget-object v2, p0, Lmoy;->a:[Lmpd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 331
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x22 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 283
    iget-object v1, p0, Lmoy;->b:[Lmpa;

    if-eqz v1, :cond_1

    .line 284
    iget-object v2, p0, Lmoy;->b:[Lmpa;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 285
    if-eqz v4, :cond_0

    .line 286
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 284
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 290
    :cond_1
    iget-object v1, p0, Lmoy;->a:[Lmpd;

    if-eqz v1, :cond_3

    .line 291
    iget-object v1, p0, Lmoy;->a:[Lmpd;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 292
    if-eqz v3, :cond_2

    .line 293
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 291
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 297
    :cond_3
    iget-object v0, p0, Lmoy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 299
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0, p1}, Lmoy;->a(Loxn;)Lmoy;

    move-result-object v0

    return-object v0
.end method
