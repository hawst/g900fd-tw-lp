.class public final Lnxr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnxr;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Lnxj;

.field public f:Ljava/lang/String;

.field public g:Lnzm;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Long;

.field public k:Ljava/lang/String;

.field public l:Lnxl;

.field public m:Lnzf;

.field public n:[Lnzk;

.field public o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lnxr;

    sput-object v0, Lnxr;->a:[Lnxr;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25
    const/high16 v0, -0x80000000

    iput v0, p0, Lnxr;->d:I

    .line 28
    iput-object v1, p0, Lnxr;->e:Lnxj;

    .line 33
    iput-object v1, p0, Lnxr;->g:Lnzm;

    .line 46
    iput-object v1, p0, Lnxr;->l:Lnxl;

    .line 51
    iput-object v1, p0, Lnxr;->m:Lnzf;

    .line 54
    sget-object v0, Lnzk;->a:[Lnzk;

    iput-object v0, p0, Lnxr;->n:[Lnzk;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 119
    .line 120
    iget-object v0, p0, Lnxr;->c:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 121
    const/4 v0, 0x1

    iget-object v2, p0, Lnxr;->c:Ljava/lang/String;

    .line 122
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 124
    :goto_0
    iget-object v2, p0, Lnxr;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 125
    const/4 v2, 0x2

    iget-object v3, p0, Lnxr;->h:Ljava/lang/String;

    .line 126
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 128
    :cond_0
    iget-object v2, p0, Lnxr;->i:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 129
    const/4 v2, 0x3

    iget-object v3, p0, Lnxr;->i:Ljava/lang/Long;

    .line 130
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 132
    :cond_1
    iget-object v2, p0, Lnxr;->p:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 133
    const/4 v2, 0x4

    iget-object v3, p0, Lnxr;->p:Ljava/lang/String;

    .line 134
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 136
    :cond_2
    iget v2, p0, Lnxr;->d:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    .line 137
    const/4 v2, 0x5

    iget v3, p0, Lnxr;->d:I

    .line 138
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 140
    :cond_3
    iget-object v2, p0, Lnxr;->l:Lnxl;

    if-eqz v2, :cond_4

    .line 141
    const/4 v2, 0x6

    iget-object v3, p0, Lnxr;->l:Lnxl;

    .line 142
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 144
    :cond_4
    iget-object v2, p0, Lnxr;->q:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 145
    const/4 v2, 0x7

    iget-object v3, p0, Lnxr;->q:Ljava/lang/String;

    .line 146
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 148
    :cond_5
    iget-object v2, p0, Lnxr;->e:Lnxj;

    if-eqz v2, :cond_6

    .line 149
    const/16 v2, 0x8

    iget-object v3, p0, Lnxr;->e:Lnxj;

    .line 150
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 152
    :cond_6
    iget-object v2, p0, Lnxr;->m:Lnzf;

    if-eqz v2, :cond_7

    .line 153
    const/16 v2, 0x9

    iget-object v3, p0, Lnxr;->m:Lnzf;

    .line 154
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 156
    :cond_7
    iget-object v2, p0, Lnxr;->n:[Lnzk;

    if-eqz v2, :cond_9

    .line 157
    iget-object v2, p0, Lnxr;->n:[Lnzk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 158
    if-eqz v4, :cond_8

    .line 159
    const/16 v5, 0xa

    .line 160
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 157
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 164
    :cond_9
    iget-object v1, p0, Lnxr;->j:Ljava/lang/Long;

    if-eqz v1, :cond_a

    .line 165
    const/16 v1, 0xb

    iget-object v2, p0, Lnxr;->j:Ljava/lang/Long;

    .line 166
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_a
    iget-object v1, p0, Lnxr;->b:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 169
    const/16 v1, 0xc

    iget-object v2, p0, Lnxr;->b:Ljava/lang/String;

    .line 170
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_b
    iget-object v1, p0, Lnxr;->f:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 173
    const/16 v1, 0xd

    iget-object v2, p0, Lnxr;->f:Ljava/lang/String;

    .line 174
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_c
    iget-object v1, p0, Lnxr;->k:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 177
    const/16 v1, 0xe

    iget-object v2, p0, Lnxr;->k:Ljava/lang/String;

    .line 178
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_d
    iget-object v1, p0, Lnxr;->o:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 181
    const/16 v1, 0xf

    iget-object v2, p0, Lnxr;->o:Ljava/lang/String;

    .line 182
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_e
    iget-object v1, p0, Lnxr;->g:Lnzm;

    if-eqz v1, :cond_f

    .line 185
    const/16 v1, 0x10

    iget-object v2, p0, Lnxr;->g:Lnzm;

    .line 186
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_f
    iget-object v1, p0, Lnxr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    iput v0, p0, Lnxr;->ai:I

    .line 190
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnxr;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 198
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 199
    sparse-switch v0, :sswitch_data_0

    .line 203
    iget-object v2, p0, Lnxr;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 204
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnxr;->ah:Ljava/util/List;

    .line 207
    :cond_1
    iget-object v2, p0, Lnxr;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    :sswitch_0
    return-object p0

    .line 214
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxr;->c:Ljava/lang/String;

    goto :goto_0

    .line 218
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxr;->h:Ljava/lang/String;

    goto :goto_0

    .line 222
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnxr;->i:Ljava/lang/Long;

    goto :goto_0

    .line 226
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxr;->p:Ljava/lang/String;

    goto :goto_0

    .line 230
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 231
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    .line 236
    :cond_2
    iput v0, p0, Lnxr;->d:I

    goto :goto_0

    .line 238
    :cond_3
    iput v1, p0, Lnxr;->d:I

    goto :goto_0

    .line 243
    :sswitch_6
    iget-object v0, p0, Lnxr;->l:Lnxl;

    if-nez v0, :cond_4

    .line 244
    new-instance v0, Lnxl;

    invoke-direct {v0}, Lnxl;-><init>()V

    iput-object v0, p0, Lnxr;->l:Lnxl;

    .line 246
    :cond_4
    iget-object v0, p0, Lnxr;->l:Lnxl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 250
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxr;->q:Ljava/lang/String;

    goto :goto_0

    .line 254
    :sswitch_8
    iget-object v0, p0, Lnxr;->e:Lnxj;

    if-nez v0, :cond_5

    .line 255
    new-instance v0, Lnxj;

    invoke-direct {v0}, Lnxj;-><init>()V

    iput-object v0, p0, Lnxr;->e:Lnxj;

    .line 257
    :cond_5
    iget-object v0, p0, Lnxr;->e:Lnxj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 261
    :sswitch_9
    iget-object v0, p0, Lnxr;->m:Lnzf;

    if-nez v0, :cond_6

    .line 262
    new-instance v0, Lnzf;

    invoke-direct {v0}, Lnzf;-><init>()V

    iput-object v0, p0, Lnxr;->m:Lnzf;

    .line 264
    :cond_6
    iget-object v0, p0, Lnxr;->m:Lnzf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 268
    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 269
    iget-object v0, p0, Lnxr;->n:[Lnzk;

    if-nez v0, :cond_8

    move v0, v1

    .line 270
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzk;

    .line 271
    iget-object v3, p0, Lnxr;->n:[Lnzk;

    if-eqz v3, :cond_7

    .line 272
    iget-object v3, p0, Lnxr;->n:[Lnzk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 274
    :cond_7
    iput-object v2, p0, Lnxr;->n:[Lnzk;

    .line 275
    :goto_2
    iget-object v2, p0, Lnxr;->n:[Lnzk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 276
    iget-object v2, p0, Lnxr;->n:[Lnzk;

    new-instance v3, Lnzk;

    invoke-direct {v3}, Lnzk;-><init>()V

    aput-object v3, v2, v0

    .line 277
    iget-object v2, p0, Lnxr;->n:[Lnzk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 278
    invoke-virtual {p1}, Loxn;->a()I

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 269
    :cond_8
    iget-object v0, p0, Lnxr;->n:[Lnzk;

    array-length v0, v0

    goto :goto_1

    .line 281
    :cond_9
    iget-object v2, p0, Lnxr;->n:[Lnzk;

    new-instance v3, Lnzk;

    invoke-direct {v3}, Lnzk;-><init>()V

    aput-object v3, v2, v0

    .line 282
    iget-object v2, p0, Lnxr;->n:[Lnzk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 286
    :sswitch_b
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnxr;->j:Ljava/lang/Long;

    goto/16 :goto_0

    .line 290
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxr;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 294
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxr;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 298
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxr;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 302
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxr;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 306
    :sswitch_10
    iget-object v0, p0, Lnxr;->g:Lnzm;

    if-nez v0, :cond_a

    .line 307
    new-instance v0, Lnzm;

    invoke-direct {v0}, Lnzm;-><init>()V

    iput-object v0, p0, Lnxr;->g:Lnzm;

    .line 309
    :cond_a
    iget-object v0, p0, Lnxr;->g:Lnzm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 199
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 61
    iget-object v0, p0, Lnxr;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    iget-object v1, p0, Lnxr;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 64
    :cond_0
    iget-object v0, p0, Lnxr;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 65
    const/4 v0, 0x2

    iget-object v1, p0, Lnxr;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 67
    :cond_1
    iget-object v0, p0, Lnxr;->i:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 68
    const/4 v0, 0x3

    iget-object v1, p0, Lnxr;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 70
    :cond_2
    iget-object v0, p0, Lnxr;->p:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 71
    const/4 v0, 0x4

    iget-object v1, p0, Lnxr;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 73
    :cond_3
    iget v0, p0, Lnxr;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 74
    const/4 v0, 0x5

    iget v1, p0, Lnxr;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 76
    :cond_4
    iget-object v0, p0, Lnxr;->l:Lnxl;

    if-eqz v0, :cond_5

    .line 77
    const/4 v0, 0x6

    iget-object v1, p0, Lnxr;->l:Lnxl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 79
    :cond_5
    iget-object v0, p0, Lnxr;->q:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 80
    const/4 v0, 0x7

    iget-object v1, p0, Lnxr;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 82
    :cond_6
    iget-object v0, p0, Lnxr;->e:Lnxj;

    if-eqz v0, :cond_7

    .line 83
    const/16 v0, 0x8

    iget-object v1, p0, Lnxr;->e:Lnxj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 85
    :cond_7
    iget-object v0, p0, Lnxr;->m:Lnzf;

    if-eqz v0, :cond_8

    .line 86
    const/16 v0, 0x9

    iget-object v1, p0, Lnxr;->m:Lnzf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 88
    :cond_8
    iget-object v0, p0, Lnxr;->n:[Lnzk;

    if-eqz v0, :cond_a

    .line 89
    iget-object v1, p0, Lnxr;->n:[Lnzk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 90
    if-eqz v3, :cond_9

    .line 91
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 89
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_a
    iget-object v0, p0, Lnxr;->j:Ljava/lang/Long;

    if-eqz v0, :cond_b

    .line 96
    const/16 v0, 0xb

    iget-object v1, p0, Lnxr;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 98
    :cond_b
    iget-object v0, p0, Lnxr;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 99
    const/16 v0, 0xc

    iget-object v1, p0, Lnxr;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 101
    :cond_c
    iget-object v0, p0, Lnxr;->f:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 102
    const/16 v0, 0xd

    iget-object v1, p0, Lnxr;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 104
    :cond_d
    iget-object v0, p0, Lnxr;->k:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 105
    const/16 v0, 0xe

    iget-object v1, p0, Lnxr;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 107
    :cond_e
    iget-object v0, p0, Lnxr;->o:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 108
    const/16 v0, 0xf

    iget-object v1, p0, Lnxr;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 110
    :cond_f
    iget-object v0, p0, Lnxr;->g:Lnzm;

    if-eqz v0, :cond_10

    .line 111
    const/16 v0, 0x10

    iget-object v1, p0, Lnxr;->g:Lnzm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 113
    :cond_10
    iget-object v0, p0, Lnxr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 115
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnxr;->a(Loxn;)Lnxr;

    move-result-object v0

    return-object v0
.end method
