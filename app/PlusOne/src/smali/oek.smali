.class public final Loek;
.super Loxq;
.source "PG"


# instance fields
.field private a:Loej;

.field private b:I

.field private c:Loel;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 529
    invoke-direct {p0}, Loxq;-><init>()V

    .line 597
    iput-object v1, p0, Loek;->a:Loej;

    .line 600
    const/high16 v0, -0x80000000

    iput v0, p0, Loek;->b:I

    .line 603
    iput-object v1, p0, Loek;->c:Loel;

    .line 529
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 623
    const/4 v0, 0x0

    .line 624
    iget-object v1, p0, Loek;->a:Loej;

    if-eqz v1, :cond_0

    .line 625
    const/4 v0, 0x1

    iget-object v1, p0, Loek;->a:Loej;

    .line 626
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 628
    :cond_0
    iget v1, p0, Loek;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 629
    const/4 v1, 0x2

    iget v2, p0, Loek;->b:I

    .line 630
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 632
    :cond_1
    iget-object v1, p0, Loek;->c:Loel;

    if-eqz v1, :cond_2

    .line 633
    const/4 v1, 0x3

    iget-object v2, p0, Loek;->c:Loel;

    .line 634
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 636
    :cond_2
    iget-object v1, p0, Loek;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 637
    iput v0, p0, Loek;->ai:I

    .line 638
    return v0
.end method

.method public a(Loxn;)Loek;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 646
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 647
    sparse-switch v0, :sswitch_data_0

    .line 651
    iget-object v1, p0, Loek;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 652
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loek;->ah:Ljava/util/List;

    .line 655
    :cond_1
    iget-object v1, p0, Loek;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 657
    :sswitch_0
    return-object p0

    .line 662
    :sswitch_1
    iget-object v0, p0, Loek;->a:Loej;

    if-nez v0, :cond_2

    .line 663
    new-instance v0, Loej;

    invoke-direct {v0}, Loej;-><init>()V

    iput-object v0, p0, Loek;->a:Loej;

    .line 665
    :cond_2
    iget-object v0, p0, Loek;->a:Loej;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 669
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 670
    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 672
    :cond_3
    iput v0, p0, Loek;->b:I

    goto :goto_0

    .line 674
    :cond_4
    iput v2, p0, Loek;->b:I

    goto :goto_0

    .line 679
    :sswitch_3
    iget-object v0, p0, Loek;->c:Loel;

    if-nez v0, :cond_5

    .line 680
    new-instance v0, Loel;

    invoke-direct {v0}, Loel;-><init>()V

    iput-object v0, p0, Loek;->c:Loel;

    .line 682
    :cond_5
    iget-object v0, p0, Loek;->c:Loel;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 647
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 608
    iget-object v0, p0, Loek;->a:Loej;

    if-eqz v0, :cond_0

    .line 609
    const/4 v0, 0x1

    iget-object v1, p0, Loek;->a:Loej;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 611
    :cond_0
    iget v0, p0, Loek;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 612
    const/4 v0, 0x2

    iget v1, p0, Loek;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 614
    :cond_1
    iget-object v0, p0, Loek;->c:Loel;

    if-eqz v0, :cond_2

    .line 615
    const/4 v0, 0x3

    iget-object v1, p0, Loek;->c:Loel;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 617
    :cond_2
    iget-object v0, p0, Loek;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 619
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 525
    invoke-virtual {p0, p1}, Loek;->a(Loxn;)Loek;

    move-result-object v0

    return-object v0
.end method
