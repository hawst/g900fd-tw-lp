.class public final Lldz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Llea;

.field private b:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private synthetic e:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lldz;->e:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 234
    iget v1, p0, Lldz;->c:I

    .line 239
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 240
    iget-object v2, p0, Lldz;->b:[Ljava/util/ArrayList;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 239
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 242
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 216
    if-gtz p1, :cond_0

    .line 217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Must have at least one view type ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " types reported)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_0
    iget v0, p0, Lldz;->c:I

    if-ne p1, v0, :cond_1

    .line 231
    :goto_0
    return-void

    .line 225
    :cond_1
    new-array v1, p1, [Ljava/util/ArrayList;

    .line 226
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_2

    .line 227
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v1, v0

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 229
    :cond_2
    iput p1, p0, Lldz;->c:I

    .line 230
    iput-object v1, p0, Lldz;->b:[Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 245
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lldx;

    .line 246
    iget v1, v0, Lldx;->c:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v1, p0, Lldz;->e:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v1

    .line 251
    iget v2, p0, Lldz;->d:I

    if-le v1, v2, :cond_2

    .line 252
    iput v1, p0, Lldz;->d:I

    .line 255
    :cond_2
    iget-object v1, p0, Lldz;->b:[Ljava/util/ArrayList;

    iget v0, v0, Lldx;->c:I

    aget-object v0, v1, v0

    .line 256
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lldz;->d:I

    if-ge v1, v2, :cond_3

    .line 257
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    :cond_3
    iget-object v0, p0, Lldz;->a:Llea;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lldz;->a:Llea;

    invoke-interface {v0, p1}, Llea;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public b(I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 266
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-object v0

    .line 270
    :cond_1
    iget-object v1, p0, Lldz;->b:[Ljava/util/ArrayList;

    aget-object v1, v1, p1

    .line 271
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    .line 276
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 277
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method
