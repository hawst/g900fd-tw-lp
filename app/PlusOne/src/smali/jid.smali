.class public final Ljid;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljia;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Ljid;->a:Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljhz;Ljhh;)V
    .locals 5

    .prologue
    .line 83
    iget-object v0, p0, Ljid;->a:Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;

    invoke-static {v0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->a(Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 84
    :try_start_0
    invoke-virtual {p1}, Ljhz;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Ljid;->a:Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;

    invoke-static {v0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->b(Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-interface {p2}, Ljhh;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 88
    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Ljid;->a:Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;

    invoke-static {v1}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->b(Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 89
    iget-object v0, p0, Ljid;->a:Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;

    invoke-static {v0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->b(Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 90
    iget-object v4, p0, Ljid;->a:Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;

    invoke-static {v4}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->b(Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhz;

    invoke-virtual {v0}, Ljhz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    monitor-exit v2

    .line 99
    :goto_1
    return-void

    .line 88
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 95
    :cond_2
    iget-object v0, p0, Ljid;->a:Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;->stopSelf()V

    .line 99
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
