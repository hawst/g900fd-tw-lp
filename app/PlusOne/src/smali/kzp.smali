.class public final Lkzp;
.super Lllq;
.source "PG"

# interfaces
.implements Lhtt;


# instance fields
.field private a:J

.field private b:Llao;

.field private c:Llak;

.field private d:Ljava/lang/String;

.field private e:Llab;

.field private f:Llac;

.field private g:Lkzq;

.field private h:Lkzo;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Lllq;-><init>()V

    .line 36
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkzp;->a:J

    .line 46
    return-void
.end method

.method private constructor <init>(Logr;ZZ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 49
    invoke-direct {p0}, Lllq;-><init>()V

    .line 36
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkzp;->a:J

    .line 50
    iget-object v0, p1, Logr;->ag:Lofg;

    if-eqz v0, :cond_1

    .line 51
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x100

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    .line 52
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p1, Logr;->ag:Lofg;

    iget-object v1, v1, Lofg;->a:Lpeo;

    .line 53
    sget-object v2, Lhwr;->a:Lhxa;

    if-nez v2, :cond_0

    new-instance v2, Lhwv;

    invoke-direct {v2}, Lhwv;-><init>()V

    sput-object v2, Lhwr;->a:Lhxa;

    :cond_0
    sget-object v2, Lhwr;->a:Lhxa;

    invoke-static {}, Lhwr;->d()Lhxa;

    move-result-object v3

    invoke-static {}, Lhwr;->c()Lhxa;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lhwr;->a(Lpeo;Lhxa;Lhxa;Lhxa;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 54
    new-instance v1, Lkzo;

    iget-object v2, p1, Logr;->ag:Lofg;

    iget-object v2, v2, Lofg;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lkzo;-><init>(Ljava/lang/String;Landroid/text/SpannableString;)V

    iput-object v1, p0, Lkzp;->h:Lkzo;

    .line 57
    :cond_1
    iget-object v0, p1, Logr;->A:Lofj;

    if-eqz v0, :cond_3

    .line 58
    iget-object v0, p1, Logr;->A:Lofj;

    iget v0, v0, Lofj;->a:I

    const/16 v1, 0x12c

    if-ne v0, v1, :cond_8

    .line 59
    const/4 v1, 0x0

    .line 60
    iget-object v0, p1, Logr;->U:[Lltg;

    if-eqz v0, :cond_a

    .line 61
    iget-object v0, p1, Logr;->U:[Lltg;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_a

    .line 62
    iget-object v0, p1, Logr;->U:[Lltg;

    aget-object v3, v0, v2

    .line 64
    iget v0, v3, Lltg;->b:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_7

    sget-object v0, Logr;->c:Loxr;

    .line 65
    invoke-virtual {v3, v0}, Lltg;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    sget-object v0, Logr;->c:Loxr;

    .line 66
    invoke-virtual {v3, v0}, Lltg;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpeo;

    iget-object v0, v0, Lpeo;->b:[Lpen;

    if-eqz v0, :cond_7

    sget-object v0, Logr;->c:Loxr;

    .line 67
    invoke-virtual {v3, v0}, Lltg;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpeo;

    iget-object v0, v0, Lpeo;->b:[Lpen;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 69
    sget-object v0, Logr;->c:Loxr;

    invoke-virtual {v3, v0}, Lltg;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpeo;

    iget-object v0, v0, Lpeo;->b:[Lpen;

    aget-object v0, v0, v5

    .line 70
    iget v3, v0, Lpen;->b:I

    if-nez v3, :cond_7

    .line 71
    iget-object v0, v0, Lpen;->c:Ljava/lang/String;

    .line 77
    :goto_1
    new-instance v1, Llao;

    invoke-direct {v1, v5, v0}, Llao;-><init>(ZLjava/lang/String;)V

    iput-object v1, p0, Lkzp;->b:Llao;

    .line 78
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x1

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    .line 86
    :cond_2
    :goto_2
    iget-object v0, p1, Logr;->A:Lofj;

    iget-object v0, v0, Lofj;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 87
    iget-object v0, p1, Logr;->A:Lofj;

    iget-object v0, v0, Lofj;->c:Ljava/lang/String;

    iput-object v0, p0, Lkzp;->d:Ljava/lang/String;

    .line 88
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    .line 91
    :cond_3
    iget-object v0, p1, Logr;->aa:Lofk;

    if-eqz v0, :cond_4

    .line 92
    new-instance v0, Llab;

    iget-object v1, p1, Logr;->aa:Lofk;

    invoke-direct {v0, v1}, Llab;-><init>(Lofk;)V

    iput-object v0, p0, Lkzp;->e:Llab;

    .line 93
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x8

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    .line 95
    :cond_4
    if-eqz p2, :cond_5

    .line 96
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x10

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    .line 98
    :cond_5
    if-eqz p3, :cond_6

    .line 99
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x80

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    .line 101
    :cond_6
    iget-object v0, p1, Logr;->ac:[Lofd;

    invoke-direct {p0, v0}, Lkzp;->a([Lofd;)V

    .line 102
    return-void

    .line 61
    :cond_7
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto/16 :goto_0

    .line 79
    :cond_8
    iget-object v0, p1, Logr;->A:Lofj;

    iget v0, v0, Lofj;->a:I

    const/16 v1, 0xca

    if-ne v0, v1, :cond_9

    .line 80
    new-instance v0, Llak;

    iget-object v1, p1, Logr;->A:Lofj;

    iget-object v1, v1, Lofj;->b:[Lofv;

    invoke-direct {v0, v1}, Llak;-><init>([Lofv;)V

    iput-object v0, p0, Lkzp;->c:Llak;

    .line 81
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x2

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    goto :goto_2

    .line 82
    :cond_9
    iget-object v0, p1, Logr;->A:Lofj;

    iget v0, v0, Lofj;->a:I

    const/16 v1, 0xcf

    if-ne v0, v1, :cond_2

    .line 83
    new-instance v0, Llac;

    invoke-direct {v0, v5}, Llac;-><init>(Z)V

    iput-object v0, p0, Lkzp;->f:Llac;

    .line 84
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x20

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    goto :goto_2

    :cond_a
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public static a([B)Lkzp;
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v10, 0x0

    .line 225
    if-nez p0, :cond_0

    .line 226
    const/4 v0, 0x0

    .line 253
    :goto_0
    return-object v0

    .line 229
    :cond_0
    new-instance v3, Lkzp;

    invoke-direct {v3}, Lkzp;-><init>()V

    .line 230
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 231
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v6

    iput-wide v6, v3, Lkzp;->a:J

    .line 232
    iget-wide v6, v3, Lkzp;->a:J

    const-wide/16 v8, 0x1

    and-long/2addr v6, v8

    cmp-long v0, v6, v10

    if-lez v0, :cond_1

    .line 233
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v4}, Llao;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Llao;

    invoke-direct {v6, v0, v5}, Llao;-><init>(ZLjava/lang/String;)V

    iput-object v6, v3, Lkzp;->b:Llao;

    .line 235
    :cond_1
    iget-wide v6, v3, Lkzp;->a:J

    const-wide/16 v8, 0x2

    and-long/2addr v6, v8

    cmp-long v0, v6, v10

    if-lez v0, :cond_4

    .line 236
    new-instance v5, Llak;

    invoke-direct {v5}, Llak;-><init>()V

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, v5, Llak;->a:Ljava/util/ArrayList;

    move v0, v2

    :goto_2
    if-ge v0, v6, :cond_3

    iget-object v7, v5, Llak;->a:Ljava/util/ArrayList;

    invoke-static {v4}, Llag;->a(Ljava/nio/ByteBuffer;)Llag;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 233
    goto :goto_1

    .line 236
    :cond_3
    iput-object v5, v3, Lkzp;->c:Llak;

    .line 238
    :cond_4
    iget-wide v6, v3, Lkzp;->a:J

    const-wide/16 v8, 0x4

    and-long/2addr v6, v8

    cmp-long v0, v6, v10

    if-lez v0, :cond_5

    .line 239
    invoke-static {v4}, Lkzp;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lkzp;->d:Ljava/lang/String;

    .line 241
    :cond_5
    iget-wide v6, v3, Lkzp;->a:J

    const-wide/16 v8, 0x8

    and-long/2addr v6, v8

    cmp-long v0, v6, v10

    if-lez v0, :cond_6

    .line 242
    new-instance v0, Llab;

    invoke-direct {v0}, Llab;-><init>()V

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    iput v5, v0, Llab;->a:I

    iput-object v0, v3, Lkzp;->e:Llab;

    .line 244
    :cond_6
    iget-wide v6, v3, Lkzp;->a:J

    const-wide/16 v8, 0x20

    and-long/2addr v6, v8

    cmp-long v0, v6, v10

    if-lez v0, :cond_7

    .line 245
    new-instance v0, Llac;

    invoke-direct {v0}, Llac;-><init>()V

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    if-ne v5, v1, :cond_a

    :goto_3
    iput-boolean v1, v0, Llac;->a:Z

    iput-object v0, v3, Lkzp;->f:Llac;

    .line 247
    :cond_7
    iget-wide v0, v3, Lkzp;->a:J

    const-wide/16 v6, 0x40

    and-long/2addr v0, v6

    cmp-long v0, v0, v10

    if-lez v0, :cond_8

    .line 248
    invoke-static {v4}, Lkzq;->a(Ljava/nio/ByteBuffer;)Lkzq;

    move-result-object v0

    iput-object v0, v3, Lkzp;->g:Lkzq;

    .line 250
    :cond_8
    iget-wide v0, v3, Lkzp;->a:J

    const-wide/16 v6, 0x100

    and-long/2addr v0, v6

    cmp-long v0, v0, v10

    if-lez v0, :cond_9

    .line 251
    invoke-static {v4}, Lkzo;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4}, Lkzo;->c(Ljava/nio/ByteBuffer;)[B

    move-result-object v1

    invoke-static {v1}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    new-instance v4, Landroid/text/SpannableString;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    invoke-virtual {v1, v2, v5}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Lkzo;

    invoke-direct {v1, v0, v4}, Lkzo;-><init>(Ljava/lang/String;Landroid/text/SpannableString;)V

    iput-object v1, v3, Lkzp;->h:Lkzo;

    :cond_9
    move-object v0, v3

    .line 253
    goto/16 :goto_0

    :cond_a
    move v1, v2

    .line 245
    goto :goto_3
.end method

.method private a([Lofd;)V
    .locals 6

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lkzp;->g:Lkzq;

    .line 106
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 112
    aget-object v1, p1, v0

    .line 113
    if-eqz v1, :cond_2

    iget-object v2, v1, Lofd;->b:Lofe;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lofd;->b:Lofe;

    iget v2, v2, Lofe;->a:I

    const/16 v3, 0xb

    if-ne v2, v3, :cond_2

    .line 115
    iget-wide v2, p0, Lkzp;->a:J

    const-wide/16 v4, 0x40

    or-long/2addr v2, v4

    iput-wide v2, p0, Lkzp;->a:J

    .line 119
    new-instance v0, Lkzq;

    iget-object v2, v1, Lofd;->b:Lofe;

    iget v2, v2, Lofe;->a:I

    iget-object v3, v1, Lofd;->b:Lofe;

    iget-object v3, v3, Lofe;->b:Ljava/lang/String;

    iget-object v1, v1, Lofd;->c:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lkzq;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lkzp;->g:Lkzq;

    goto :goto_0

    .line 111
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static a(Lkzp;)[B
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 189
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x400

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 190
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 191
    iget-wide v4, p0, Lkzp;->a:J

    invoke-virtual {v2, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 192
    iget-wide v4, p0, Lkzp;->a:J

    const-wide/16 v6, 0x1

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-lez v0, :cond_0

    .line 193
    iget-object v0, p0, Lkzp;->b:Llao;

    invoke-virtual {v0}, Llao;->a()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    invoke-virtual {v0}, Llao;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Llao;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 195
    :cond_0
    iget-wide v4, p0, Lkzp;->a:J

    const-wide/16 v6, 0x2

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-lez v0, :cond_1

    .line 196
    iget-object v0, p0, Lkzp;->c:Llak;

    invoke-static {v0, v2}, Llak;->a(Llak;Ljava/io/DataOutputStream;)V

    .line 198
    :cond_1
    iget-wide v4, p0, Lkzp;->a:J

    const-wide/16 v6, 0x4

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-lez v0, :cond_2

    .line 199
    iget-object v0, p0, Lkzp;->d:Ljava/lang/String;

    invoke-static {v2, v0}, Lkzp;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 201
    :cond_2
    iget-wide v4, p0, Lkzp;->a:J

    const-wide/16 v6, 0x8

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-lez v0, :cond_3

    .line 202
    iget-object v0, p0, Lkzp;->e:Llab;

    iget v0, v0, Llab;->a:I

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 204
    :cond_3
    iget-wide v4, p0, Lkzp;->a:J

    const-wide/16 v6, 0x20

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-lez v0, :cond_4

    .line 205
    iget-object v0, p0, Lkzp;->f:Llac;

    iget-boolean v0, v0, Llac;->a:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 207
    :cond_4
    iget-wide v4, p0, Lkzp;->a:J

    const-wide/16 v6, 0x40

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-lez v0, :cond_5

    .line 208
    iget-object v0, p0, Lkzp;->g:Lkzq;

    invoke-virtual {v0, v2}, Lkzq;->a(Ljava/io/DataOutputStream;)V

    .line 210
    :cond_5
    iget-wide v4, p0, Lkzp;->a:J

    const-wide/16 v6, 0x100

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-lez v0, :cond_6

    .line 211
    iget-object v0, p0, Lkzp;->h:Lkzo;

    invoke-virtual {v0}, Lkzo;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lkzo;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v0}, Lkzo;->b()Landroid/text/SpannableString;

    move-result-object v0

    invoke-static {v0}, Llht;->a(Landroid/text/Spanned;)[B

    move-result-object v0

    invoke-static {v2, v0}, Lkzo;->a(Ljava/io/DataOutputStream;[B)V

    .line 213
    :cond_6
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 214
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 215
    return-object v0

    .line 205
    :cond_7
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Logr;ZZ)[B
    .locals 1

    .prologue
    .line 221
    new-instance v0, Lkzp;

    invoke-direct {v0, p0, p1, p2}, Lkzp;-><init>(Logr;ZZ)V

    invoke-static {v0}, Lkzp;->a(Lkzp;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public synthetic a()Lhtv;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lkzp;->h()Llab;

    move-result-object v0

    return-object v0
.end method

.method public a(Llab;)V
    .locals 4

    .prologue
    .line 126
    iput-object p1, p0, Lkzp;->e:Llab;

    .line 127
    iget-object v0, p0, Lkzp;->e:Llab;

    if-nez v0, :cond_0

    .line 128
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, -0x9

    and-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x8

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    goto :goto_0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 135
    if-eqz p1, :cond_0

    .line 136
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x10

    or-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, -0x11

    and-long/2addr v0, v2

    iput-wide v0, p0, Lkzp;->a:J

    goto :goto_0
.end method

.method public synthetic b()Lhub;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lkzp;->k()Llak;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Lhud;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lkzp;->l()Llao;

    move-result-object v0

    return-object v0
.end method

.method public synthetic d()Lhtu;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lkzp;->i()Lkzq;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lhtw;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lkzp;->j()Lkzo;

    move-result-object v0

    return-object v0
.end method

.method public f()Z
    .locals 4

    .prologue
    .line 151
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x10

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 4

    .prologue
    .line 155
    iget-wide v0, p0, Lkzp;->a:J

    const-wide/16 v2, 0x80

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Llab;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lkzp;->e:Llab;

    return-object v0
.end method

.method public i()Lkzq;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lkzp;->g:Lkzq;

    return-object v0
.end method

.method public j()Lkzo;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lkzp;->h:Lkzo;

    return-object v0
.end method

.method public k()Llak;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lkzp;->c:Llak;

    return-object v0
.end method

.method public l()Llao;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lkzp;->b:Llao;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lkzp;->d:Ljava/lang/String;

    return-object v0
.end method

.method public n()Llac;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lkzp;->f:Llac;

    return-object v0
.end method
