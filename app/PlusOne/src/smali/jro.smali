.class final Ljro;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lijt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lijt",
        "<",
        "Lior;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Ljrm;


# direct methods
.method constructor <init>(Ljrm;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Ljro;->a:Ljrm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lijs;)V
    .locals 0

    .prologue
    .line 230
    check-cast p1, Lior;

    invoke-virtual {p0, p1}, Ljro;->a(Lior;)V

    return-void
.end method

.method public a(Lior;)V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 233
    iget-object v2, p0, Ljro;->a:Ljrm;

    invoke-interface {p1}, Lior;->a()Lika;

    move-result-object v0

    invoke-interface {v0}, Lika;->b()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v3, "isSuccess: %s. Circles: %s"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {p1}, Lior;->a()Lika;

    move-result-object v0

    invoke-interface {v0}, Lika;->b()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-interface {p1}, Lior;->b()Liqc;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move v2, v1

    :goto_0
    if-eqz v5, :cond_0

    invoke-interface {v5}, Liqc;->b()I

    move-result v0

    if-ge v2, v0, :cond_0

    invoke-interface {v5, v2}, Liqc;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liqb;

    const-string v7, "%s, "

    new-array v8, v9, [Ljava/lang/Object;

    invoke-interface {v0}, Liqb;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v1

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v9

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 236
    :cond_1
    :goto_1
    iget-object v0, p0, Ljro;->a:Ljrm;

    invoke-virtual {v0}, Ljrm;->e()V

    .line 237
    return-void

    .line 233
    :cond_2
    invoke-interface {p1}, Lior;->b()Liqc;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-interface {v3}, Liqc;->b()I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    :goto_2
    if-ge v1, v4, :cond_3

    invoke-interface {v3, v1}, Liqc;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liqb;

    new-instance v6, Ljrs;

    invoke-direct {v6, v0}, Ljrs;-><init>(Liqb;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    invoke-virtual {v2, v5}, Ljrm;->a(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v0

    monitor-enter v2

    :try_start_0
    iput-object v5, v2, Ljrm;->a:Ljava/util/ArrayList;

    iput-object v0, v2, Ljrm;->b:Ljava/util/HashMap;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v3}, Liqc;->a()V

    :cond_4
    invoke-virtual {v2, v7}, Ljrm;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
