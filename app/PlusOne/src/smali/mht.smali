.class public final Lmht;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmzm;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22678
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22681
    iput-object v0, p0, Lmht;->apiHeader:Llyr;

    .line 22684
    iput-object v0, p0, Lmht;->a:Lmzm;

    .line 22678
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 22701
    const/4 v0, 0x0

    .line 22702
    iget-object v1, p0, Lmht;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 22703
    const/4 v0, 0x1

    iget-object v1, p0, Lmht;->apiHeader:Llyr;

    .line 22704
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22706
    :cond_0
    iget-object v1, p0, Lmht;->a:Lmzm;

    if-eqz v1, :cond_1

    .line 22707
    const/4 v1, 0x2

    iget-object v2, p0, Lmht;->a:Lmzm;

    .line 22708
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22710
    :cond_1
    iget-object v1, p0, Lmht;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22711
    iput v0, p0, Lmht;->ai:I

    .line 22712
    return v0
.end method

.method public a(Loxn;)Lmht;
    .locals 2

    .prologue
    .line 22720
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 22721
    sparse-switch v0, :sswitch_data_0

    .line 22725
    iget-object v1, p0, Lmht;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 22726
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmht;->ah:Ljava/util/List;

    .line 22729
    :cond_1
    iget-object v1, p0, Lmht;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22731
    :sswitch_0
    return-object p0

    .line 22736
    :sswitch_1
    iget-object v0, p0, Lmht;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 22737
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmht;->apiHeader:Llyr;

    .line 22739
    :cond_2
    iget-object v0, p0, Lmht;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22743
    :sswitch_2
    iget-object v0, p0, Lmht;->a:Lmzm;

    if-nez v0, :cond_3

    .line 22744
    new-instance v0, Lmzm;

    invoke-direct {v0}, Lmzm;-><init>()V

    iput-object v0, p0, Lmht;->a:Lmzm;

    .line 22746
    :cond_3
    iget-object v0, p0, Lmht;->a:Lmzm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22721
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22689
    iget-object v0, p0, Lmht;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 22690
    const/4 v0, 0x1

    iget-object v1, p0, Lmht;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22692
    :cond_0
    iget-object v0, p0, Lmht;->a:Lmzm;

    if-eqz v0, :cond_1

    .line 22693
    const/4 v0, 0x2

    iget-object v1, p0, Lmht;->a:Lmzm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22695
    :cond_1
    iget-object v0, p0, Lmht;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 22697
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22674
    invoke-virtual {p0, p1}, Lmht;->a(Loxn;)Lmht;

    move-result-object v0

    return-object v0
.end method
