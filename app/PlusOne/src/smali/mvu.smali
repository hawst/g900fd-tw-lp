.class public final Lmvu;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lmvs;

.field private b:Lmvv;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11184
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11187
    sget-object v0, Lmvs;->a:[Lmvs;

    iput-object v0, p0, Lmvu;->a:[Lmvs;

    .line 11190
    const/4 v0, 0x0

    iput-object v0, p0, Lmvu;->b:Lmvv;

    .line 11184
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 11211
    .line 11212
    iget-object v1, p0, Lmvu;->a:[Lmvs;

    if-eqz v1, :cond_1

    .line 11213
    iget-object v2, p0, Lmvu;->a:[Lmvs;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 11214
    if-eqz v4, :cond_0

    .line 11215
    const/4 v5, 0x1

    .line 11216
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 11213
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 11220
    :cond_1
    iget-object v1, p0, Lmvu;->b:Lmvv;

    if-eqz v1, :cond_2

    .line 11221
    const/4 v1, 0x2

    iget-object v2, p0, Lmvu;->b:Lmvv;

    .line 11222
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11224
    :cond_2
    iget-object v1, p0, Lmvu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11225
    iput v0, p0, Lmvu;->ai:I

    .line 11226
    return v0
.end method

.method public a(Loxn;)Lmvu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11234
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11235
    sparse-switch v0, :sswitch_data_0

    .line 11239
    iget-object v2, p0, Lmvu;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 11240
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmvu;->ah:Ljava/util/List;

    .line 11243
    :cond_1
    iget-object v2, p0, Lmvu;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11245
    :sswitch_0
    return-object p0

    .line 11250
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 11251
    iget-object v0, p0, Lmvu;->a:[Lmvs;

    if-nez v0, :cond_3

    move v0, v1

    .line 11252
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmvs;

    .line 11253
    iget-object v3, p0, Lmvu;->a:[Lmvs;

    if-eqz v3, :cond_2

    .line 11254
    iget-object v3, p0, Lmvu;->a:[Lmvs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 11256
    :cond_2
    iput-object v2, p0, Lmvu;->a:[Lmvs;

    .line 11257
    :goto_2
    iget-object v2, p0, Lmvu;->a:[Lmvs;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 11258
    iget-object v2, p0, Lmvu;->a:[Lmvs;

    new-instance v3, Lmvs;

    invoke-direct {v3}, Lmvs;-><init>()V

    aput-object v3, v2, v0

    .line 11259
    iget-object v2, p0, Lmvu;->a:[Lmvs;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 11260
    invoke-virtual {p1}, Loxn;->a()I

    .line 11257
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 11251
    :cond_3
    iget-object v0, p0, Lmvu;->a:[Lmvs;

    array-length v0, v0

    goto :goto_1

    .line 11263
    :cond_4
    iget-object v2, p0, Lmvu;->a:[Lmvs;

    new-instance v3, Lmvs;

    invoke-direct {v3}, Lmvs;-><init>()V

    aput-object v3, v2, v0

    .line 11264
    iget-object v2, p0, Lmvu;->a:[Lmvs;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11268
    :sswitch_2
    iget-object v0, p0, Lmvu;->b:Lmvv;

    if-nez v0, :cond_5

    .line 11269
    new-instance v0, Lmvv;

    invoke-direct {v0}, Lmvv;-><init>()V

    iput-object v0, p0, Lmvu;->b:Lmvv;

    .line 11271
    :cond_5
    iget-object v0, p0, Lmvu;->b:Lmvv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11235
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 11195
    iget-object v0, p0, Lmvu;->a:[Lmvs;

    if-eqz v0, :cond_1

    .line 11196
    iget-object v1, p0, Lmvu;->a:[Lmvs;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 11197
    if-eqz v3, :cond_0

    .line 11198
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 11196
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11202
    :cond_1
    iget-object v0, p0, Lmvu;->b:Lmvv;

    if-eqz v0, :cond_2

    .line 11203
    const/4 v0, 0x2

    iget-object v1, p0, Lmvu;->b:Lmvv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11205
    :cond_2
    iget-object v0, p0, Lmvu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11207
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11180
    invoke-virtual {p0, p1}, Lmvu;->a(Loxn;)Lmvu;

    move-result-object v0

    return-object v0
.end method
