.class public final Lnvk;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:Lodc;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 1392
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1401
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnvk;->a:[Ljava/lang/String;

    .line 1404
    iput v1, p0, Lnvk;->b:I

    .line 1407
    iput v1, p0, Lnvk;->c:I

    .line 1410
    const/4 v0, 0x0

    iput-object v0, p0, Lnvk;->d:Lodc;

    .line 1392
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    const/4 v0, 0x0

    .line 1435
    .line 1436
    iget-object v1, p0, Lnvk;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnvk;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 1438
    iget-object v2, p0, Lnvk;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1440
    invoke-static {v4}, Loxo;->b(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 1438
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1443
    :cond_0
    iget-object v0, p0, Lnvk;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 1445
    :cond_1
    iget v1, p0, Lnvk;->b:I

    if-eq v1, v5, :cond_2

    .line 1446
    const/4 v1, 0x2

    iget v2, p0, Lnvk;->b:I

    .line 1447
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1449
    :cond_2
    iget v1, p0, Lnvk;->c:I

    if-eq v1, v5, :cond_3

    .line 1450
    const/4 v1, 0x3

    iget v2, p0, Lnvk;->c:I

    .line 1451
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1453
    :cond_3
    iget-object v1, p0, Lnvk;->d:Lodc;

    if-eqz v1, :cond_4

    .line 1454
    const/4 v1, 0x4

    iget-object v2, p0, Lnvk;->d:Lodc;

    .line 1455
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1457
    :cond_4
    iget-object v1, p0, Lnvk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1458
    iput v0, p0, Lnvk;->ai:I

    .line 1459
    return v0
.end method

.method public a(Loxn;)Lnvk;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1467
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1468
    sparse-switch v0, :sswitch_data_0

    .line 1472
    iget-object v1, p0, Lnvk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1473
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnvk;->ah:Ljava/util/List;

    .line 1476
    :cond_1
    iget-object v1, p0, Lnvk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1478
    :sswitch_0
    return-object p0

    .line 1483
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1484
    iget-object v0, p0, Lnvk;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 1485
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1486
    iget-object v2, p0, Lnvk;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1487
    iput-object v1, p0, Lnvk;->a:[Ljava/lang/String;

    .line 1488
    :goto_1
    iget-object v1, p0, Lnvk;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1489
    iget-object v1, p0, Lnvk;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1490
    invoke-virtual {p1}, Loxn;->a()I

    .line 1488
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1493
    :cond_2
    iget-object v1, p0, Lnvk;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1497
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1498
    if-eqz v0, :cond_3

    if-eq v0, v4, :cond_3

    if-eq v0, v5, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    .line 1507
    :cond_3
    iput v0, p0, Lnvk;->b:I

    goto :goto_0

    .line 1509
    :cond_4
    iput v3, p0, Lnvk;->b:I

    goto :goto_0

    .line 1514
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1515
    if-eqz v0, :cond_5

    if-eq v0, v4, :cond_5

    if-ne v0, v5, :cond_6

    .line 1518
    :cond_5
    iput v0, p0, Lnvk;->c:I

    goto :goto_0

    .line 1520
    :cond_6
    iput v3, p0, Lnvk;->c:I

    goto :goto_0

    .line 1525
    :sswitch_4
    iget-object v0, p0, Lnvk;->d:Lodc;

    if-nez v0, :cond_7

    .line 1526
    new-instance v0, Lodc;

    invoke-direct {v0}, Lodc;-><init>()V

    iput-object v0, p0, Lnvk;->d:Lodc;

    .line 1528
    :cond_7
    iget-object v0, p0, Lnvk;->d:Lodc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1468
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 1415
    iget-object v0, p0, Lnvk;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1416
    iget-object v1, p0, Lnvk;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1417
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 1416
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1420
    :cond_0
    iget v0, p0, Lnvk;->b:I

    if-eq v0, v5, :cond_1

    .line 1421
    const/4 v0, 0x2

    iget v1, p0, Lnvk;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1423
    :cond_1
    iget v0, p0, Lnvk;->c:I

    if-eq v0, v5, :cond_2

    .line 1424
    const/4 v0, 0x3

    iget v1, p0, Lnvk;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1426
    :cond_2
    iget-object v0, p0, Lnvk;->d:Lodc;

    if-eqz v0, :cond_3

    .line 1427
    const/4 v0, 0x4

    iget-object v1, p0, Lnvk;->d:Lodc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1429
    :cond_3
    iget-object v0, p0, Lnvk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1431
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1388
    invoke-virtual {p0, p1}, Lnvk;->a(Loxn;)Lnvk;

    move-result-object v0

    return-object v0
.end method
