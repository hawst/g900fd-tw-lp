.class public final Lpla;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lpme;

.field public b:Ljava/lang/Long;

.field public c:Lpjd;

.field public d:I

.field public e:Lplb;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20
    sget-object v0, Lpme;->a:[Lpme;

    iput-object v0, p0, Lpla;->a:[Lpme;

    .line 25
    iput-object v1, p0, Lpla;->c:Lpjd;

    .line 28
    const/high16 v0, -0x80000000

    iput v0, p0, Lpla;->d:I

    .line 31
    iput-object v1, p0, Lpla;->e:Lplb;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 61
    .line 62
    iget-object v1, p0, Lpla;->a:[Lpme;

    if-eqz v1, :cond_1

    .line 63
    iget-object v2, p0, Lpla;->a:[Lpme;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 64
    if-eqz v4, :cond_0

    .line 65
    const/4 v5, 0x1

    .line 66
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 63
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    :cond_1
    iget-object v1, p0, Lpla;->b:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 71
    const/4 v1, 0x2

    iget-object v2, p0, Lpla;->b:Ljava/lang/Long;

    .line 72
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_2
    iget-object v1, p0, Lpla;->c:Lpjd;

    if-eqz v1, :cond_3

    .line 75
    const/4 v1, 0x3

    iget-object v2, p0, Lpla;->c:Lpjd;

    .line 76
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_3
    iget v1, p0, Lpla;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 79
    const/4 v1, 0x4

    iget v2, p0, Lpla;->d:I

    .line 80
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_4
    iget-object v1, p0, Lpla;->e:Lplb;

    if-eqz v1, :cond_5

    .line 83
    const/4 v1, 0x5

    iget-object v2, p0, Lpla;->e:Lplb;

    .line 84
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_5
    iget-object v1, p0, Lpla;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    iput v0, p0, Lpla;->ai:I

    .line 88
    return v0
.end method

.method public a(Loxn;)Lpla;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 96
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 97
    sparse-switch v0, :sswitch_data_0

    .line 101
    iget-object v2, p0, Lpla;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 102
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpla;->ah:Ljava/util/List;

    .line 105
    :cond_1
    iget-object v2, p0, Lpla;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    :sswitch_0
    return-object p0

    .line 112
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 113
    iget-object v0, p0, Lpla;->a:[Lpme;

    if-nez v0, :cond_3

    move v0, v1

    .line 114
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpme;

    .line 115
    iget-object v3, p0, Lpla;->a:[Lpme;

    if-eqz v3, :cond_2

    .line 116
    iget-object v3, p0, Lpla;->a:[Lpme;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    :cond_2
    iput-object v2, p0, Lpla;->a:[Lpme;

    .line 119
    :goto_2
    iget-object v2, p0, Lpla;->a:[Lpme;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 120
    iget-object v2, p0, Lpla;->a:[Lpme;

    new-instance v3, Lpme;

    invoke-direct {v3}, Lpme;-><init>()V

    aput-object v3, v2, v0

    .line 121
    iget-object v2, p0, Lpla;->a:[Lpme;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 122
    invoke-virtual {p1}, Loxn;->a()I

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 113
    :cond_3
    iget-object v0, p0, Lpla;->a:[Lpme;

    array-length v0, v0

    goto :goto_1

    .line 125
    :cond_4
    iget-object v2, p0, Lpla;->a:[Lpme;

    new-instance v3, Lpme;

    invoke-direct {v3}, Lpme;-><init>()V

    aput-object v3, v2, v0

    .line 126
    iget-object v2, p0, Lpla;->a:[Lpme;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 130
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpla;->b:Ljava/lang/Long;

    goto :goto_0

    .line 134
    :sswitch_3
    iget-object v0, p0, Lpla;->c:Lpjd;

    if-nez v0, :cond_5

    .line 135
    new-instance v0, Lpjd;

    invoke-direct {v0}, Lpjd;-><init>()V

    iput-object v0, p0, Lpla;->c:Lpjd;

    .line 137
    :cond_5
    iget-object v0, p0, Lpla;->c:Lpjd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 141
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 142
    if-eqz v0, :cond_6

    const/4 v2, 0x1

    if-eq v0, v2, :cond_6

    const/4 v2, 0x2

    if-eq v0, v2, :cond_6

    const/4 v2, 0x3

    if-ne v0, v2, :cond_7

    .line 146
    :cond_6
    iput v0, p0, Lpla;->d:I

    goto/16 :goto_0

    .line 148
    :cond_7
    iput v1, p0, Lpla;->d:I

    goto/16 :goto_0

    .line 153
    :sswitch_5
    iget-object v0, p0, Lpla;->e:Lplb;

    if-nez v0, :cond_8

    .line 154
    new-instance v0, Lplb;

    invoke-direct {v0}, Lplb;-><init>()V

    iput-object v0, p0, Lpla;->e:Lplb;

    .line 156
    :cond_8
    iget-object v0, p0, Lpla;->e:Lplb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 97
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 36
    iget-object v0, p0, Lpla;->a:[Lpme;

    if-eqz v0, :cond_1

    .line 37
    iget-object v1, p0, Lpla;->a:[Lpme;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 38
    if-eqz v3, :cond_0

    .line 39
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_1
    iget-object v0, p0, Lpla;->b:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 44
    const/4 v0, 0x2

    iget-object v1, p0, Lpla;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 46
    :cond_2
    iget-object v0, p0, Lpla;->c:Lpjd;

    if-eqz v0, :cond_3

    .line 47
    const/4 v0, 0x3

    iget-object v1, p0, Lpla;->c:Lpjd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 49
    :cond_3
    iget v0, p0, Lpla;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 50
    const/4 v0, 0x4

    iget v1, p0, Lpla;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 52
    :cond_4
    iget-object v0, p0, Lpla;->e:Lplb;

    if-eqz v0, :cond_5

    .line 53
    const/4 v0, 0x5

    iget-object v1, p0, Lpla;->e:Lplb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 55
    :cond_5
    iget-object v0, p0, Lpla;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 57
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpla;->a(Loxn;)Lpla;

    move-result-object v0

    return-object v0
.end method
