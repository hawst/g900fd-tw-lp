.class public final Lmvq;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmvq;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Integer;

.field private d:I

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11452
    const/4 v0, 0x0

    new-array v0, v0, [Lmvq;

    sput-object v0, Lmvq;->a:[Lmvq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11453
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11467
    const/high16 v0, -0x80000000

    iput v0, p0, Lmvq;->d:I

    .line 11453
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11512
    const/4 v0, 0x0

    .line 11513
    iget-object v1, p0, Lmvq;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11514
    const/4 v0, 0x1

    iget-object v1, p0, Lmvq;->b:Ljava/lang/String;

    .line 11515
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11517
    :cond_0
    iget-object v1, p0, Lmvq;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 11518
    const/4 v1, 0x2

    iget-object v2, p0, Lmvq;->c:Ljava/lang/Integer;

    .line 11519
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11521
    :cond_1
    iget v1, p0, Lmvq;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 11522
    const/4 v1, 0x3

    iget v2, p0, Lmvq;->d:I

    .line 11523
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11525
    :cond_2
    iget-object v1, p0, Lmvq;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 11526
    const/4 v1, 0x4

    iget-object v2, p0, Lmvq;->e:Ljava/lang/Integer;

    .line 11527
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11529
    :cond_3
    iget-object v1, p0, Lmvq;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 11530
    const/4 v1, 0x5

    iget-object v2, p0, Lmvq;->f:Ljava/lang/Integer;

    .line 11531
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11533
    :cond_4
    iget-object v1, p0, Lmvq;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 11534
    const/4 v1, 0x6

    iget-object v2, p0, Lmvq;->g:Ljava/lang/Integer;

    .line 11535
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11537
    :cond_5
    iget-object v1, p0, Lmvq;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 11538
    const/4 v1, 0x7

    iget-object v2, p0, Lmvq;->h:Ljava/lang/Integer;

    .line 11539
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11541
    :cond_6
    iget-object v1, p0, Lmvq;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 11542
    const/16 v1, 0x8

    iget-object v2, p0, Lmvq;->i:Ljava/lang/Integer;

    .line 11543
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11545
    :cond_7
    iget-object v1, p0, Lmvq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11546
    iput v0, p0, Lmvq;->ai:I

    .line 11547
    return v0
.end method

.method public a(Loxn;)Lmvq;
    .locals 2

    .prologue
    .line 11555
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11556
    sparse-switch v0, :sswitch_data_0

    .line 11560
    iget-object v1, p0, Lmvq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11561
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvq;->ah:Ljava/util/List;

    .line 11564
    :cond_1
    iget-object v1, p0, Lmvq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11566
    :sswitch_0
    return-object p0

    .line 11571
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmvq;->b:Ljava/lang/String;

    goto :goto_0

    .line 11575
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvq;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 11579
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 11580
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 11584
    :cond_2
    iput v0, p0, Lmvq;->d:I

    goto :goto_0

    .line 11586
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmvq;->d:I

    goto :goto_0

    .line 11591
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvq;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 11595
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvq;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 11599
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvq;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 11603
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvq;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 11607
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvq;->i:Ljava/lang/Integer;

    goto :goto_0

    .line 11556
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11482
    iget-object v0, p0, Lmvq;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 11483
    const/4 v0, 0x1

    iget-object v1, p0, Lmvq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 11485
    :cond_0
    iget-object v0, p0, Lmvq;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 11486
    const/4 v0, 0x2

    iget-object v1, p0, Lmvq;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11488
    :cond_1
    iget v0, p0, Lmvq;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 11489
    const/4 v0, 0x3

    iget v1, p0, Lmvq;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11491
    :cond_2
    iget-object v0, p0, Lmvq;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 11492
    const/4 v0, 0x4

    iget-object v1, p0, Lmvq;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11494
    :cond_3
    iget-object v0, p0, Lmvq;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 11495
    const/4 v0, 0x5

    iget-object v1, p0, Lmvq;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11497
    :cond_4
    iget-object v0, p0, Lmvq;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 11498
    const/4 v0, 0x6

    iget-object v1, p0, Lmvq;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11500
    :cond_5
    iget-object v0, p0, Lmvq;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 11501
    const/4 v0, 0x7

    iget-object v1, p0, Lmvq;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11503
    :cond_6
    iget-object v0, p0, Lmvq;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 11504
    const/16 v0, 0x8

    iget-object v1, p0, Lmvq;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 11506
    :cond_7
    iget-object v0, p0, Lmvq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11508
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11449
    invoke-virtual {p0, p1}, Lmvq;->a(Loxn;)Lmvq;

    move-result-object v0

    return-object v0
.end method
