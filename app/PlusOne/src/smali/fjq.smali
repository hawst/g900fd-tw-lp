.class public final Lfjq;
.super Lkgn;
.source "PG"

# interfaces
.implements Lkig;


# instance fields
.field private final Q:Lkif;

.field private R:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Lkgn;-><init>()V

    .line 24
    new-instance v0, Lkif;

    iget-object v1, p0, Lfjq;->P:Llqm;

    invoke-direct {v0, p0, v1}, Lkif;-><init>(Lkgn;Llqr;)V

    iput-object v0, p0, Lfjq;->Q:Lkif;

    .line 28
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 45
    new-instance v0, Lian;

    invoke-direct {v0}, Lian;-><init>()V

    .line 46
    iget-object v1, p0, Lfjq;->N:Llnl;

    iget v2, p0, Lfjq;->R:I

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/AccountStatusActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Lian;->b(Landroid/content/Intent;)V

    .line 47
    iget-object v1, p0, Lfjq;->N:Llnl;

    iget v2, p0, Lfjq;->R:I

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/ExperimentsBrowserActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Lian;->c(Landroid/content/Intent;)V

    .line 48
    iget-object v1, p0, Lfjq;->N:Llnl;

    iget v2, p0, Lfjq;->R:I

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Lian;->d(Landroid/content/Intent;)V

    .line 49
    iget-object v1, p0, Lfjq;->N:Llnl;

    iget v2, p0, Lfjq;->R:I

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/NetworkStatisticsActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Lian;->e(Landroid/content/Intent;)V

    .line 50
    iget-object v1, p0, Lfjq;->N:Llnl;

    iget v2, p0, Lfjq;->R:I

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v3}, Lian;->f(Landroid/content/Intent;)V

    .line 51
    invoke-virtual {v0, v5}, Lian;->a(Z)V

    .line 52
    invoke-virtual {v0, v5}, Lian;->b(Z)V

    .line 54
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lfjq;->N:Llnl;

    const-class v3, Lcom/google/android/apps/plus/settings/GplusTracingSettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    const-string v2, "account_id"

    iget v3, p0, Lfjq;->R:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 56
    invoke-virtual {v0, v1}, Lian;->g(Landroid/content/Intent;)V

    .line 58
    iget-object v1, p0, Lfjq;->Q:Lkif;

    invoke-virtual {v1, v0}, Lkif;->a(Lu;)V

    .line 59
    iget-object v0, p0, Lfjq;->Q:Lkif;

    new-instance v1, Llfz;

    invoke-direct {v1}, Llfz;-><init>()V

    invoke-virtual {v0, v1}, Lkif;->a(Lu;)V

    .line 60
    iget-object v0, p0, Lfjq;->Q:Lkif;

    new-instance v1, Ljja;

    invoke-direct {v1}, Ljja;-><init>()V

    invoke-virtual {v0, v1}, Lkif;->a(Lu;)V

    .line 61
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 38
    invoke-super {p0, p1}, Lkgn;->a(Landroid/app/Activity;)V

    .line 39
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lfjq;->R:I

    .line 41
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 32
    invoke-super {p0, p1}, Lkgn;->c(Landroid/os/Bundle;)V

    .line 33
    iget-object v0, p0, Lfjq;->O:Llnh;

    const-class v1, Lkij;

    iget-object v2, p0, Lfjq;->Q:Lkif;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 34
    return-void
.end method
