.class public final Lkvy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;
.implements Lkvx;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Lu;

.field private b:Landroid/content/Context;

.field private c:Lhee;


# direct methods
.method public constructor <init>(Lu;Llqr;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lkvy;->a:Lu;

    .line 44
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 45
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 49
    iput-object p1, p0, Lkvy;->b:Landroid/content/Context;

    .line 50
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkvy;->c:Lhee;

    .line 51
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 52
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 62
    new-instance v0, Lkvp;

    iget-object v1, p0, Lkvy;->b:Landroid/content/Context;

    iget-object v2, p0, Lkvy;->c:Lhee;

    .line 63
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v4, 0x0

    const/4 v5, 0x5

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lkvp;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V

    .line 64
    iget-object v1, p0, Lkvy;->b:Landroid/content/Context;

    const-class v2, Lhoc;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 65
    iget-object v0, p0, Lkvy;->b:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lkvy;->b:Landroid/content/Context;

    iget-object v3, p0, Lkvy;->c:Lhee;

    .line 66
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->ch:Lhmv;

    .line 67
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    const-string v2, "extra_square_id"

    .line 68
    invoke-static {v2, p1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 65
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 69
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 55
    invoke-static {p1, p2}, Lkvw;->a(Ljava/lang/String;I)Lt;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lkvy;->a:Lu;

    invoke-virtual {v1}, Lu;->q()Lae;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 74
    const-string v0, "EditSquareMembershipTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 76
    :goto_0
    const-string v1, "SOLE_OWNER_LEAVING_SQUARE"

    invoke-static {v0, v1}, Lkgf;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    invoke-virtual {p3, v0}, Lhos;->a(Ljava/lang/Exception;)Z

    .line 83
    :cond_0
    :goto_1
    return-void

    .line 75
    :cond_1
    invoke-virtual {p2}, Lhoz;->b()Ljava/lang/Exception;

    move-result-object v0

    goto :goto_0

    .line 79
    :cond_2
    iget-object v1, p0, Lkvy;->a:Lu;

    iget-object v0, p0, Lkvy;->b:Landroid/content/Context;

    const-class v2, Lkte;

    .line 80
    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkte;

    iget-object v2, p0, Lkvy;->c:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    invoke-interface {v0}, Lkte;->a()Landroid/content/Intent;

    move-result-object v0

    .line 79
    invoke-virtual {v1, v0}, Lu;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method
