.class public final Lfpj;
.super Lepp;
.source "PG"


# static fields
.field private static final d:Lium;


# instance fields
.field private final e:Litz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lfpk;

    invoke-direct {v0}, Lfpk;-><init>()V

    sput-object v0, Lfpj;->d:Lium;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILept;Litz;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lepp;-><init>(Landroid/content/Context;ILept;)V

    .line 42
    iput-object p4, p0, Lfpj;->e:Litz;

    .line 43
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lfpj;->f()V

    .line 57
    iget-object v0, p0, Lfpj;->e:Litz;

    sget-object v1, Lfpj;->d:Lium;

    invoke-virtual {v0, v1}, Litz;->a(Lium;)Z

    .line 58
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public a(Lnyq;)Z
    .locals 6

    .prologue
    .line 47
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide v2, 0x9a7ec800L

    sub-long/2addr v0, v2

    .line 48
    iget-object v2, p0, Lfpj;->c:Landroid/content/Context;

    iget v3, p0, Lfpj;->b:I

    invoke-virtual {p0}, Lfpj;->e()Lepn;

    move-result-object v4

    invoke-static {v2, v3, v4}, Ldhv;->a(Landroid/content/Context;ILepn;)Ldwi;

    move-result-object v2

    .line 49
    iget-object v3, p0, Lfpj;->e:Litz;

    invoke-virtual {v3}, Litz;->g()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ldwi;->b()J

    move-result-wide v4

    cmp-long v0, v4, v0

    if-gez v0, :cond_0

    .line 50
    invoke-virtual {v2}, Ldwi;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 62
    invoke-virtual {p0}, Lfpj;->f()V

    .line 63
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 67
    const v0, 0x7f0400f9

    return v0
.end method

.method public e()Lepn;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lepn;->h:Lepn;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lfpj;->e:Litz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpj;->e:Litz;

    invoke-virtual {v0}, Litz;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lfpj;->c:Landroid/content/Context;

    iget v1, p0, Lfpj;->b:I

    sget-object v2, Lepn;->h:Lepn;

    invoke-static {v0, v1, v2}, Ldhv;->c(Landroid/content/Context;ILepn;)V

    .line 91
    return-void
.end method
