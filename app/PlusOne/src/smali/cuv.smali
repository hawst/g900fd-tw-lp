.class public final Lcuv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhei;

.field private c:Lcuw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcuv;->a:Landroid/content/Context;

    .line 34
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lcuv;->b:Lhei;

    .line 35
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Lcuv;->c:Lcuw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcuv;->c:Lcuw;

    invoke-virtual {v0}, Lcuw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v0, p0, Lcuv;->c:Lcuw;

    invoke-virtual {v0}, Lcuw;->e()V

    .line 116
    :try_start_0
    iget-object v0, p0, Lcuv;->c:Lcuw;

    invoke-virtual {v0}, Lcuw;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    const-string v1, "MediaSync"

    const-string v2, "Unable to join local media sync thread"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcuv;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    monitor-exit p0

    return-void

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ZLcus;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 44
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcuv;->b:Lhei;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "logged_in"

    aput-object v5, v3, v4

    invoke-interface {v1, v3}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 45
    iget-object v1, p0, Lcuv;->a:Landroid/content/Context;

    new-instance v4, Lcur;

    iget-object v5, p0, Lcuv;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcur;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v4, v3}, Lcut;->a(Landroid/content/Context;Lcux;Ljava/util/List;)Lcut;

    move-result-object v5

    .line 48
    iget-object v1, p0, Lcuv;->c:Lcuw;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcuv;->c:Lcuw;

    invoke-virtual {v1}, Lcuw;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcuv;->c:Lcuw;

    invoke-virtual {v1}, Lcuw;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    move v1, v0

    :goto_0
    if-eqz v1, :cond_3

    .line 49
    invoke-direct {p0}, Lcuv;->b()V

    iget-object v1, p0, Lcuv;->a:Landroid/content/Context;

    new-instance v4, Lcuq;

    iget-object v6, p0, Lcuv;->a:Landroid/content/Context;

    invoke-direct {v4, v6}, Lcuq;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v4, v3}, Lcut;->a(Landroid/content/Context;Lcux;Ljava/util/List;)Lcut;

    move-result-object v4

    if-nez p1, :cond_1

    iget-object v1, p0, Lcuv;->c:Lcuw;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcuv;->c:Lcuw;

    invoke-virtual {v1}, Lcuw;->b()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcuv;->c:Lcuw;

    invoke-virtual {v1}, Lcuw;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move v2, v0

    :cond_2
    new-instance v0, Lcuw;

    iget-object v1, p0, Lcuv;->a:Landroid/content/Context;

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcuw;-><init>(Landroid/content/Context;ZLcus;Lcut;Lcut;)V

    iput-object v0, p0, Lcuv;->c:Lcuw;

    iget-object v0, p0, Lcuv;->c:Lcuw;

    invoke-virtual {v0}, Lcuw;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    :cond_3
    monitor-exit p0

    return-void

    .line 48
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcuv;->c:Lcuw;

    invoke-virtual {v1}, Lcuw;->d()Lcut;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcut;->a(Lcut;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_5

    move v1, v0

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
