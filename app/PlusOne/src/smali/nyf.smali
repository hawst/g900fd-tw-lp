.class public final Lnyf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnyf;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:Lnyz;

.field public h:Loae;

.field private i:Ljava/lang/String;

.field private j:Loew;

.field private k:Loep;

.field private l:Lpeo;

.field private m:I

.field private n:I

.field private o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4291
    const/4 v0, 0x0

    new-array v0, v0, [Lnyf;

    sput-object v0, Lnyf;->a:[Lnyf;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 4292
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4313
    iput-object v0, p0, Lnyf;->j:Loew;

    .line 4316
    iput-object v0, p0, Lnyf;->k:Loep;

    .line 4319
    iput-object v0, p0, Lnyf;->l:Lpeo;

    .line 4328
    iput-object v0, p0, Lnyf;->g:Lnyz;

    .line 4331
    iput v1, p0, Lnyf;->m:I

    .line 4334
    iput v1, p0, Lnyf;->n:I

    .line 4339
    iput-object v0, p0, Lnyf;->h:Loae;

    .line 4292
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 4386
    const/4 v0, 0x1

    iget-object v1, p0, Lnyf;->b:Ljava/lang/String;

    .line 4388
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4389
    const/4 v1, 0x2

    iget-object v2, p0, Lnyf;->c:Ljava/lang/String;

    .line 4390
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4391
    const/4 v1, 0x3

    iget-object v2, p0, Lnyf;->d:Ljava/lang/String;

    .line 4392
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4393
    iget-object v1, p0, Lnyf;->g:Lnyz;

    if-eqz v1, :cond_0

    .line 4394
    const/4 v1, 0x4

    iget-object v2, p0, Lnyf;->g:Lnyz;

    .line 4395
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4397
    :cond_0
    iget v1, p0, Lnyf;->m:I

    if-eq v1, v3, :cond_1

    .line 4398
    const/4 v1, 0x5

    iget v2, p0, Lnyf;->m:I

    .line 4399
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4401
    :cond_1
    iget v1, p0, Lnyf;->n:I

    if-eq v1, v3, :cond_2

    .line 4402
    const/4 v1, 0x6

    iget v2, p0, Lnyf;->n:I

    .line 4403
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4405
    :cond_2
    iget-object v1, p0, Lnyf;->o:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4406
    const/4 v1, 0x7

    iget-object v2, p0, Lnyf;->o:Ljava/lang/String;

    .line 4407
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4409
    :cond_3
    iget-object v1, p0, Lnyf;->h:Loae;

    if-eqz v1, :cond_4

    .line 4410
    const/16 v1, 0x8

    iget-object v2, p0, Lnyf;->h:Loae;

    .line 4411
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4413
    :cond_4
    iget-object v1, p0, Lnyf;->i:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 4414
    const/16 v1, 0x9

    iget-object v2, p0, Lnyf;->i:Ljava/lang/String;

    .line 4415
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4417
    :cond_5
    iget-object v1, p0, Lnyf;->e:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 4418
    const/16 v1, 0xa

    iget-object v2, p0, Lnyf;->e:Ljava/lang/Long;

    .line 4419
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4421
    :cond_6
    iget-object v1, p0, Lnyf;->f:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 4422
    const/16 v1, 0xb

    iget-object v2, p0, Lnyf;->f:Ljava/lang/Long;

    .line 4423
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4425
    :cond_7
    iget-object v1, p0, Lnyf;->j:Loew;

    if-eqz v1, :cond_8

    .line 4426
    const/16 v1, 0xc

    iget-object v2, p0, Lnyf;->j:Loew;

    .line 4427
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4429
    :cond_8
    iget-object v1, p0, Lnyf;->k:Loep;

    if-eqz v1, :cond_9

    .line 4430
    const/16 v1, 0xd

    iget-object v2, p0, Lnyf;->k:Loep;

    .line 4431
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4433
    :cond_9
    iget-object v1, p0, Lnyf;->l:Lpeo;

    if-eqz v1, :cond_a

    .line 4434
    const/16 v1, 0xe

    iget-object v2, p0, Lnyf;->l:Lpeo;

    .line 4435
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4437
    :cond_a
    iget-object v1, p0, Lnyf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4438
    iput v0, p0, Lnyf;->ai:I

    .line 4439
    return v0
.end method

.method public a(Loxn;)Lnyf;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 4447
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4448
    sparse-switch v0, :sswitch_data_0

    .line 4452
    iget-object v1, p0, Lnyf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4453
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyf;->ah:Ljava/util/List;

    .line 4456
    :cond_1
    iget-object v1, p0, Lnyf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4458
    :sswitch_0
    return-object p0

    .line 4463
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyf;->b:Ljava/lang/String;

    goto :goto_0

    .line 4467
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyf;->c:Ljava/lang/String;

    goto :goto_0

    .line 4471
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyf;->d:Ljava/lang/String;

    goto :goto_0

    .line 4475
    :sswitch_4
    iget-object v0, p0, Lnyf;->g:Lnyz;

    if-nez v0, :cond_2

    .line 4476
    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    iput-object v0, p0, Lnyf;->g:Lnyz;

    .line 4478
    :cond_2
    iget-object v0, p0, Lnyf;->g:Lnyz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4482
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4483
    if-eq v0, v2, :cond_3

    if-eq v0, v3, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 4487
    :cond_3
    iput v0, p0, Lnyf;->m:I

    goto :goto_0

    .line 4489
    :cond_4
    iput v2, p0, Lnyf;->m:I

    goto :goto_0

    .line 4494
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4495
    if-eq v0, v2, :cond_5

    if-ne v0, v3, :cond_6

    .line 4497
    :cond_5
    iput v0, p0, Lnyf;->n:I

    goto :goto_0

    .line 4499
    :cond_6
    iput v2, p0, Lnyf;->n:I

    goto :goto_0

    .line 4504
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyf;->o:Ljava/lang/String;

    goto :goto_0

    .line 4508
    :sswitch_8
    iget-object v0, p0, Lnyf;->h:Loae;

    if-nez v0, :cond_7

    .line 4509
    new-instance v0, Loae;

    invoke-direct {v0}, Loae;-><init>()V

    iput-object v0, p0, Lnyf;->h:Loae;

    .line 4511
    :cond_7
    iget-object v0, p0, Lnyf;->h:Loae;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4515
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyf;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 4519
    :sswitch_a
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnyf;->e:Ljava/lang/Long;

    goto/16 :goto_0

    .line 4523
    :sswitch_b
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnyf;->f:Ljava/lang/Long;

    goto/16 :goto_0

    .line 4527
    :sswitch_c
    iget-object v0, p0, Lnyf;->j:Loew;

    if-nez v0, :cond_8

    .line 4528
    new-instance v0, Loew;

    invoke-direct {v0}, Loew;-><init>()V

    iput-object v0, p0, Lnyf;->j:Loew;

    .line 4530
    :cond_8
    iget-object v0, p0, Lnyf;->j:Loew;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4534
    :sswitch_d
    iget-object v0, p0, Lnyf;->k:Loep;

    if-nez v0, :cond_9

    .line 4535
    new-instance v0, Loep;

    invoke-direct {v0}, Loep;-><init>()V

    iput-object v0, p0, Lnyf;->k:Loep;

    .line 4537
    :cond_9
    iget-object v0, p0, Lnyf;->k:Loep;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4541
    :sswitch_e
    iget-object v0, p0, Lnyf;->l:Lpeo;

    if-nez v0, :cond_a

    .line 4542
    new-instance v0, Lpeo;

    invoke-direct {v0}, Lpeo;-><init>()V

    iput-object v0, p0, Lnyf;->l:Lpeo;

    .line 4544
    :cond_a
    iget-object v0, p0, Lnyf;->l:Lpeo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4448
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    const/high16 v2, -0x80000000

    .line 4344
    const/4 v0, 0x1

    iget-object v1, p0, Lnyf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4345
    const/4 v0, 0x2

    iget-object v1, p0, Lnyf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4346
    const/4 v0, 0x3

    iget-object v1, p0, Lnyf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4347
    iget-object v0, p0, Lnyf;->g:Lnyz;

    if-eqz v0, :cond_0

    .line 4348
    const/4 v0, 0x4

    iget-object v1, p0, Lnyf;->g:Lnyz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4350
    :cond_0
    iget v0, p0, Lnyf;->m:I

    if-eq v0, v2, :cond_1

    .line 4351
    const/4 v0, 0x5

    iget v1, p0, Lnyf;->m:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4353
    :cond_1
    iget v0, p0, Lnyf;->n:I

    if-eq v0, v2, :cond_2

    .line 4354
    const/4 v0, 0x6

    iget v1, p0, Lnyf;->n:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4356
    :cond_2
    iget-object v0, p0, Lnyf;->o:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4357
    const/4 v0, 0x7

    iget-object v1, p0, Lnyf;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4359
    :cond_3
    iget-object v0, p0, Lnyf;->h:Loae;

    if-eqz v0, :cond_4

    .line 4360
    const/16 v0, 0x8

    iget-object v1, p0, Lnyf;->h:Loae;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4362
    :cond_4
    iget-object v0, p0, Lnyf;->i:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 4363
    const/16 v0, 0x9

    iget-object v1, p0, Lnyf;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4365
    :cond_5
    iget-object v0, p0, Lnyf;->e:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 4366
    const/16 v0, 0xa

    iget-object v1, p0, Lnyf;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 4368
    :cond_6
    iget-object v0, p0, Lnyf;->f:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 4369
    const/16 v0, 0xb

    iget-object v1, p0, Lnyf;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 4371
    :cond_7
    iget-object v0, p0, Lnyf;->j:Loew;

    if-eqz v0, :cond_8

    .line 4372
    const/16 v0, 0xc

    iget-object v1, p0, Lnyf;->j:Loew;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4374
    :cond_8
    iget-object v0, p0, Lnyf;->k:Loep;

    if-eqz v0, :cond_9

    .line 4375
    const/16 v0, 0xd

    iget-object v1, p0, Lnyf;->k:Loep;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4377
    :cond_9
    iget-object v0, p0, Lnyf;->l:Lpeo;

    if-eqz v0, :cond_a

    .line 4378
    const/16 v0, 0xe

    iget-object v1, p0, Lnyf;->l:Lpeo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4380
    :cond_a
    iget-object v0, p0, Lnyf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4382
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4288
    invoke-virtual {p0, p1}, Lnyf;->a(Loxn;)Lnyf;

    move-result-object v0

    return-object v0
.end method
