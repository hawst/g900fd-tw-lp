.class public Lkki;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljby;
.implements Lkjk;
.implements Lklc;
.implements Llnx;
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Llae;

.field private h:Lkzm;

.field private i:Lkzz;

.field private j:Lhyt;

.field private k:Ljava/lang/String;

.field private l:Lklo;

.field private m:Z

.field private n:Ljbx;

.field private o:Lklb;

.field private p:Lkjj;

.field private q:Lkjd;

.field private r:Loya;

.field private s:Ljava/lang/String;

.field private t:Lklq;

.field private final u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkkj;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lkkk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkki;->u:Ljava/util/ArrayList;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkki;->v:Ljava/util/ArrayList;

    .line 112
    iput-object p1, p0, Lkki;->a:Landroid/app/Activity;

    .line 113
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 114
    return-void
.end method

.method private static a(Landroid/content/Intent;)Llae;
    .locals 15

    .prologue
    const/4 v14, 0x5

    const-wide v12, 0x416312d000000000L    # 1.0E7

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 521
    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Llae;

    .line 524
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 591
    :cond_0
    :goto_0
    return-object v1

    .line 529
    :cond_1
    const-string v0, "com.google.android.apps.plus.LOCATION_NAME"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.google.android.apps.plus.CID"

    .line 530
    invoke-virtual {p0, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 533
    :cond_2
    const/4 v0, 0x0

    .line 542
    const-string v3, "com.google.android.apps.plus.LATITUDE"

    invoke-virtual {p0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "com.google.android.apps.plus.LONGITUDE"

    .line 543
    invoke-virtual {p0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 545
    const-string v3, "com.google.android.apps.plus.LATITUDE"

    invoke-virtual {p0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 546
    const-string v3, "com.google.android.apps.plus.LONGITUDE"

    invoke-virtual {p0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 549
    :try_start_0
    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 550
    invoke-static {v5}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 552
    const-wide v10, -0x3fa9800000000000L    # -90.0

    cmpl-double v3, v6, v10

    if-ltz v3, :cond_3

    const-wide v10, 0x4056800000000000L    # 90.0

    cmpg-double v3, v6, v10

    if-gtz v3, :cond_3

    const-wide v10, -0x3f99800000000000L    # -180.0

    cmpl-double v3, v8, v10

    if-ltz v3, :cond_3

    const-wide v10, 0x4066800000000000L    # 180.0

    cmpg-double v3, v8, v10

    if-gtz v3, :cond_3

    .line 554
    mul-double/2addr v6, v12

    double-to-int v3, v6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 555
    mul-double v6, v8, v12

    double-to-int v6, v6

    :try_start_1
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    move v0, v2

    .line 571
    :goto_1
    const-string v5, "com.google.android.apps.plus.CID"

    invoke-virtual {p0, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 572
    const-string v0, "com.google.android.apps.plus.CID"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move v0, v2

    .line 576
    :goto_2
    const-string v5, "com.google.android.apps.plus.LOCATION_NAME"

    invoke-virtual {p0, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 577
    const-string v0, "com.google.android.apps.plus.LOCATION_NAME"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move v0, v2

    .line 581
    :goto_3
    const-string v6, "com.google.android.apps.plus.ADDRESS"

    invoke-virtual {p0, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 582
    const-string v0, "com.google.android.apps.plus.ADDRESS"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 586
    :goto_4
    if-eqz v2, :cond_0

    .line 587
    new-instance v1, Llae;

    const/4 v2, 0x3

    const-wide/16 v8, 0x0

    invoke-direct/range {v1 .. v9}, Llae;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    goto/16 :goto_0

    .line 558
    :cond_3
    :try_start_2
    const-string v3, "ComposedContentModel"

    const/4 v6, 0x5

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 559
    const-string v3, "Provided latitude/longitude are out of range. latitude: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xd

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", longitude: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_4
    move-object v4, v1

    move-object v3, v1

    .line 568
    goto :goto_1

    .line 564
    :catch_0
    move-exception v3

    move-object v3, v1

    :goto_5
    const-string v6, "ComposedContentModel"

    invoke-static {v6, v14}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 565
    const-string v6, "Can\'t parse latitude/longitude extras. latitude: "

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0xd

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", longitude: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    move-object v4, v1

    goto/16 :goto_1

    .line 564
    :catch_1
    move-exception v6

    goto :goto_5

    :cond_6
    move-object v6, v1

    move v2, v0

    goto/16 :goto_4

    :cond_7
    move-object v5, v1

    goto/16 :goto_3

    :cond_8
    move-object v7, v1

    goto/16 :goto_2

    :cond_9
    move-object v4, v1

    move-object v3, v1

    goto/16 :goto_1
.end method

.method private z()V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Lkki;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkkj;

    .line 434
    invoke-interface {v0}, Lkkj;->e()V

    goto :goto_0

    .line 436
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 237
    :try_start_0
    iget-object v0, p0, Lkki;->s:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    iput-object v1, p0, Lkki;->s:Ljava/lang/String;

    return-object v0

    :catchall_0
    move-exception v0

    iput-object v1, p0, Lkki;->s:Ljava/lang/String;

    throw v0
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 118
    const-class v0, Ljbx;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbx;

    iput-object v0, p0, Lkki;->n:Ljbx;

    .line 119
    const-class v0, Lklb;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lklb;

    iput-object v0, p0, Lkki;->o:Lklb;

    .line 120
    const-class v0, Lkjj;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjj;

    iput-object v0, p0, Lkki;->p:Lkjj;

    .line 121
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 125
    iget-object v0, p0, Lkki;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 126
    const-string v0, "disable_acl_modification"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lkki;->e:Z

    .line 127
    const-string v0, "disable_acl_modification_message"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkki;->f:Ljava/lang/String;

    .line 129
    if-nez p1, :cond_c

    .line 130
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkki;->b:Ljava/lang/String;

    .line 131
    const-string v0, "android.intent.extra.TEXT"

    invoke-static {v3, v0}, Lkje;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkki;->s:Ljava/lang/String;

    .line 132
    const-string v0, "activity_id"

    invoke-static {v3, v0}, Lkje;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkki;->c:Ljava/lang/String;

    .line 134
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v0, v4, :cond_7

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    const-string v0, "disable_location"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_0
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lkki;->d:Z

    .line 135
    iget-boolean v0, p0, Lkki;->d:Z

    if-nez v0, :cond_1

    .line 136
    invoke-static {v3}, Lkki;->a(Landroid/content/Intent;)Llae;

    move-result-object v0

    iput-object v0, p0, Lkki;->g:Llae;

    .line 138
    :cond_1
    const-string v0, "square_embed"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    const-string v0, "square_embed"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lkzz;

    iput-object v0, p0, Lkki;->i:Lkzz;

    .line 141
    :cond_2
    const-string v0, "embed_client_item"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 142
    const-string v0, "embed_client_item"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    iput-object v0, p0, Lkki;->j:Lhyt;

    .line 145
    :cond_3
    const-string v0, "com.google.android.apps.plus.GOOGLE_BIRTHDAY_POST"

    iget-object v4, p0, Lkki;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 147
    const-string v0, "com.google.android.apps.plus.RECIPIENT_ID"

    .line 148
    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "com.google.android.apps.plus.RECIPIENT_NAME"

    .line 149
    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "com.google.android.apps.plus.BIRTHDAY_YEAR"

    .line 150
    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    .line 152
    :goto_2
    if-eqz v0, :cond_4

    .line 153
    new-instance v0, Lkzm;

    const-string v4, "com.google.android.apps.plus.RECIPIENT_ID"

    .line 154
    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.google.android.apps.plus.RECIPIENT_NAME"

    .line 155
    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.google.android.apps.plus.BIRTHDAY_YEAR"

    .line 156
    invoke-virtual {v3, v6, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v7, "com.google.android.apps.plus.RECIPIENT_PHOTO_URL"

    .line 157
    invoke-virtual {v3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v4, v5, v6, v7}, Lkzm;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    iput-object v0, p0, Lkki;->h:Lkzm;

    .line 161
    :cond_4
    const-string v0, "com.google.android.apps.plus.SHARE_GOOGLE"

    iget-object v4, p0, Lkki;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 162
    invoke-virtual {v3}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_5

    .line 164
    iput-object v0, p0, Lkki;->k:Ljava/lang/String;

    .line 173
    :cond_5
    :goto_3
    invoke-virtual {p0}, Lkki;->t()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lkki;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Landroid/text/SpannableString;

    iget-object v3, p0, Lkki;->s:Ljava/lang/String;

    invoke-direct {v0, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v0, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    invoke-virtual {v0}, Landroid/text/SpannableString;->length()I

    move-result v2

    const-class v3, Landroid/text/style/URLSpan;

    invoke-virtual {v0, v1, v2, v3}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v2, v0

    if-lez v2, :cond_6

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkki;->k:Ljava/lang/String;

    iget-object v0, p0, Lkki;->s:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkki;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    iput-object v0, p0, Lkki;->s:Ljava/lang/String;

    .line 192
    :cond_6
    :goto_4
    iget-object v0, p0, Lkki;->n:Ljbx;

    invoke-virtual {v0, p0}, Ljbx;->a(Ljby;)V

    .line 193
    iget-object v0, p0, Lkki;->o:Lklb;

    invoke-virtual {v0, p0}, Lklb;->a(Lklc;)V

    .line 194
    return-void

    :cond_7
    move v0, v1

    .line 134
    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto/16 :goto_1

    :cond_9
    move v0, v1

    .line 150
    goto/16 :goto_2

    .line 166
    :cond_a
    const-string v0, "com.google.android.apps.plus.GOOGLE_PLUS_SHARE"

    iget-object v4, p0, Lkki;->b:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "android.intent.action.SEND"

    iget-object v4, p0, Lkki;->b:Ljava/lang/String;

    .line 167
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 168
    :cond_b
    const-string v0, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 169
    const-string v0, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkki;->k:Ljava/lang/String;

    goto :goto_3

    .line 175
    :cond_c
    const-string v0, "action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkki;->b:Ljava/lang/String;

    .line 176
    const-string v0, "activity_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkki;->c:Ljava/lang/String;

    .line 177
    const-string v0, "disable_location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lkki;->d:Z

    .line 178
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Llae;

    iput-object v0, p0, Lkki;->g:Llae;

    .line 179
    const-string v0, "birthday_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lkzm;

    iput-object v0, p0, Lkki;->h:Lkzm;

    .line 180
    const-string v0, "square_embed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lkzz;

    iput-object v0, p0, Lkki;->i:Lkzz;

    .line 181
    const-string v0, "embed_client_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    iput-object v0, p0, Lkki;->j:Lhyt;

    .line 182
    const-string v0, "preview_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkki;->k:Ljava/lang/String;

    .line 183
    const-string v0, "preview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lklo;

    iput-object v0, p0, Lkki;->l:Lklo;

    .line 184
    const-string v0, "is_link_preview_via_url_in_comment_box"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lkki;->m:Z

    .line 187
    iget-object v0, p0, Lkki;->l:Lklo;

    if-eqz v0, :cond_6

    .line 188
    new-instance v0, Lklq;

    iget-object v1, p0, Lkki;->l:Lklo;

    invoke-direct {v0, v1}, Lklq;-><init>(Lklo;)V

    iput-object v0, p0, Lkki;->t:Lklq;

    goto/16 :goto_4
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 308
    iput-object p1, p0, Lkki;->k:Ljava/lang/String;

    .line 309
    const/4 v0, 0x0

    iput-object v0, p0, Lkki;->l:Lklo;

    .line 310
    return-void
.end method

.method public a(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lizr;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 488
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 489
    iget-object v0, p0, Lkki;->l:Lklo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lkki;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 491
    :cond_0
    invoke-virtual {p0}, Lkki;->v()V

    .line 499
    :goto_0
    return-void

    .line 492
    :cond_1
    iget-object v0, p0, Lkki;->o:Lklb;

    invoke-virtual {v0}, Lklb;->b()Lkkz;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 493
    iget-object v0, p0, Lkki;->o:Lklb;

    invoke-virtual {v0}, Lklb;->a()V

    goto :goto_0

    .line 494
    :cond_2
    iget-object v0, p0, Lkki;->p:Lkjj;

    invoke-interface {v0}, Lkjj;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 495
    iget-object v0, p0, Lkki;->p:Lkjj;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lkjj;->a(Lkji;)V

    goto :goto_0

    .line 497
    :cond_3
    invoke-direct {p0}, Lkki;->z()V

    goto :goto_0
.end method

.method public a(Lkjd;Loya;)V
    .locals 0

    .prologue
    .line 361
    iput-object p1, p0, Lkki;->q:Lkjd;

    .line 362
    iput-object p2, p0, Lkki;->r:Loya;

    .line 363
    return-void
.end method

.method public a(Lkkj;)V
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lkki;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 418
    return-void
.end method

.method public a(Lkkk;)V
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lkki;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    return-void
.end method

.method public a(Lklo;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 328
    iput-object p1, p0, Lkki;->l:Lklo;

    .line 331
    invoke-virtual {p0}, Lkki;->w()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lkki;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 335
    :cond_1
    new-instance v0, Lklq;

    invoke-direct {v0, p1}, Lklq;-><init>(Lklo;)V

    iput-object v0, p0, Lkki;->t:Lklq;

    .line 336
    iget-object v0, p0, Lkki;->t:Lklq;

    invoke-virtual {v0}, Lklq;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337
    invoke-direct {p0}, Lkki;->z()V

    goto :goto_0

    .line 339
    :cond_2
    iget-object v1, p0, Lkki;->k:Ljava/lang/String;

    .line 340
    iput-object v2, p0, Lkki;->k:Ljava/lang/String;

    .line 341
    iput-object v2, p0, Lkki;->l:Lklo;

    .line 342
    iput-object v2, p0, Lkki;->t:Lklq;

    .line 343
    iget-object v0, p0, Lkki;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkkj;

    invoke-interface {v0, v1}, Lkkj;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Llae;)V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lkki;->g:Llae;

    if-eq p1, v0, :cond_0

    .line 367
    iput-object p1, p0, Lkki;->g:Llae;

    .line 368
    iget-object v0, p0, Lkki;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkkk;

    .line 369
    invoke-interface {v0}, Lkkk;->a()V

    goto :goto_0

    .line 372
    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 484
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 198
    const-string v0, "action"

    iget-object v1, p0, Lkki;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const-string v0, "activity_id"

    iget-object v1, p0, Lkki;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    const-string v0, "disable_location"

    iget-boolean v1, p0, Lkki;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 201
    const-string v0, "location"

    iget-object v1, p0, Lkki;->g:Llae;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 202
    const-string v0, "birthday_data"

    iget-object v1, p0, Lkki;->h:Lkzm;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 203
    const-string v0, "square_embed"

    iget-object v1, p0, Lkki;->i:Lkzz;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 204
    const-string v0, "embed_client_item"

    iget-object v1, p0, Lkki;->j:Lhyt;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 205
    const-string v0, "preview_url"

    iget-object v1, p0, Lkki;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v0, "preview"

    iget-object v1, p0, Lkki;->l:Lklo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 207
    const-string v0, "is_link_preview_via_url_in_comment_box"

    iget-boolean v1, p0, Lkki;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 209
    return-void
.end method

.method public b(Lkkj;)V
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lkki;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 426
    return-void
.end method

.method public b(Lkkk;)V
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lkki;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 430
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lkki;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lkki;->p:Lkjj;

    invoke-interface {v0}, Lkjj;->a()Lkji;

    move-result-object v0

    if-nez v0, :cond_0

    .line 452
    invoke-direct {p0}, Lkki;->z()V

    .line 465
    :goto_0
    return-void

    .line 456
    :cond_0
    iget-object v0, p0, Lkki;->o:Lklb;

    invoke-virtual {v0}, Lklb;->b()Lkkz;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lkki;->o:Lklb;

    invoke-virtual {v0}, Lklb;->a()V

    goto :goto_0

    .line 458
    :cond_1
    iget-object v0, p0, Lkki;->l:Lklo;

    if-nez v0, :cond_2

    iget-object v0, p0, Lkki;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 459
    :cond_2
    invoke-virtual {p0}, Lkki;->v()V

    goto :goto_0

    .line 460
    :cond_3
    iget-object v0, p0, Lkki;->n:Ljbx;

    invoke-virtual {v0}, Ljbx;->h()Z

    move-result v0

    if-nez v0, :cond_4

    .line 461
    iget-object v0, p0, Lkki;->n:Ljbx;

    invoke-virtual {v0, p0}, Ljbx;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 463
    :cond_4
    invoke-direct {p0}, Lkki;->z()V

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lkki;->d:Z

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lkki;->e:Z

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lkki;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()Llae;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lkki;->g:Llae;

    return-object v0
.end method

.method public i()Lkzm;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lkki;->h:Lkzm;

    return-object v0
.end method

.method public j()Lkzz;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lkki;->i:Lkzz;

    return-object v0
.end method

.method public k()Lhyt;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lkki;->j:Lhyt;

    return-object v0
.end method

.method public l()Lklb;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lkki;->o:Lklb;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lkki;->k:Ljava/lang/String;

    return-object v0
.end method

.method public n()Lklo;
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lkki;->l:Lklo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkki;->t:Lklq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkki;->t:Lklq;

    iget-object v0, v0, Lklq;->a:Lkko;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkki;->t:Lklq;

    iget-object v0, v0, Lklq;->a:Lkko;

    .line 285
    invoke-virtual {v0}, Lkko;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lkki;->t:Lklq;

    iget-object v0, v0, Lklq;->a:Lkko;

    iget-object v1, p0, Lkki;->l:Lklo;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lklo;->a(Ljava/lang/String;)Loya;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkko;->a(Loya;)V

    .line 288
    :cond_0
    iget-object v0, p0, Lkki;->l:Lklo;

    return-object v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 292
    iget-boolean v0, p0, Lkki;->m:Z

    return v0
.end method

.method public p()Lklq;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lkki;->t:Lklq;

    return-object v0
.end method

.method public q()Lkjd;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lkki;->q:Lkjd;

    return-object v0
.end method

.method public r()Loya;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lkki;->r:Loya;

    return-object v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lkki;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkki;->l:Lklo;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 324
    invoke-virtual {p0}, Lkki;->w()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()V
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkki;->m:Z

    .line 349
    return-void
.end method

.method public v()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 352
    iput-object v0, p0, Lkki;->k:Ljava/lang/String;

    .line 353
    iput-object v0, p0, Lkki;->l:Lklo;

    .line 354
    iput-object v0, p0, Lkki;->t:Lklq;

    .line 355
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkki;->m:Z

    .line 356
    invoke-direct {p0}, Lkki;->z()V

    .line 357
    return-void
.end method

.method public w()I
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lkki;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 379
    const/16 v0, 0x8

    .line 397
    :goto_0
    return v0

    .line 380
    :cond_0
    iget-object v0, p0, Lkki;->h:Lkzm;

    if-eqz v0, :cond_1

    .line 381
    const/4 v0, 0x5

    goto :goto_0

    .line 382
    :cond_1
    iget-object v0, p0, Lkki;->k:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lkki;->l:Lklo;

    if-eqz v0, :cond_3

    .line 383
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 384
    :cond_3
    iget-object v0, p0, Lkki;->o:Lklb;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lkki;->o:Lklb;

    invoke-virtual {v0}, Lklb;->b()Lkkz;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 385
    const/4 v0, 0x2

    goto :goto_0

    .line 386
    :cond_4
    iget-object v0, p0, Lkki;->n:Ljbx;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lkki;->n:Ljbx;

    invoke-virtual {v0}, Ljbx;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 387
    const/4 v0, 0x4

    goto :goto_0

    .line 388
    :cond_5
    iget-object v0, p0, Lkki;->n:Ljbx;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lkki;->n:Ljbx;

    invoke-virtual {v0}, Ljbx;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 389
    const/4 v0, 0x3

    goto :goto_0

    .line 390
    :cond_6
    iget-object v0, p0, Lkki;->i:Lkzz;

    if-eqz v0, :cond_7

    .line 391
    const/4 v0, 0x6

    goto :goto_0

    .line 392
    :cond_7
    iget-object v0, p0, Lkki;->q:Lkjd;

    if-eqz v0, :cond_8

    .line 393
    const/4 v0, 0x7

    goto :goto_0

    .line 394
    :cond_8
    iget-object v0, p0, Lkki;->p:Lkjj;

    invoke-interface {v0}, Lkjj;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 395
    const/16 v0, 0x9

    goto :goto_0

    .line 397
    :cond_9
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public x()Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 402
    iget-object v0, p0, Lkki;->g:Llae;

    if-eqz v0, :cond_2

    move v0, v1

    .line 403
    :goto_0
    iget-object v3, p0, Lkki;->h:Lkzm;

    if-eqz v3, :cond_3

    move v3, v1

    .line 404
    :goto_1
    iget-object v4, p0, Lkki;->i:Lkzz;

    if-eqz v4, :cond_4

    move v4, v1

    .line 405
    :goto_2
    iget-object v5, p0, Lkki;->n:Ljbx;

    invoke-virtual {v5}, Ljbx;->h()Z

    move-result v5

    if-nez v5, :cond_5

    move v5, v1

    .line 406
    :goto_3
    iget-object v6, p0, Lkki;->o:Lklb;

    invoke-virtual {v6}, Lklb;->b()Lkkz;

    move-result-object v6

    if-eqz v6, :cond_6

    move v6, v1

    .line 407
    :goto_4
    iget-object v7, p0, Lkki;->l:Lklo;

    if-eqz v7, :cond_7

    move v7, v1

    .line 408
    :goto_5
    iget-object v8, p0, Lkki;->j:Lhyt;

    if-eqz v8, :cond_8

    move v8, v1

    .line 409
    :goto_6
    iget-object v9, p0, Lkki;->c:Ljava/lang/String;

    if-eqz v9, :cond_9

    move v9, v1

    .line 410
    :goto_7
    iget-object v10, p0, Lkki;->p:Lkjj;

    invoke-interface {v10}, Lkjj;->a()Lkji;

    move-result-object v10

    if-eqz v10, :cond_a

    iget-object v10, p0, Lkki;->p:Lkjj;

    .line 411
    invoke-interface {v10}, Lkjj;->a()Lkji;

    move-result-object v10

    invoke-interface {v10}, Lkji;->f()Z

    move-result v10

    if-eqz v10, :cond_a

    move v10, v1

    .line 412
    :goto_8
    if-nez v0, :cond_0

    if-nez v3, :cond_0

    if-nez v4, :cond_0

    if-nez v8, :cond_0

    if-nez v5, :cond_0

    if-nez v6, :cond_0

    if-nez v7, :cond_0

    if-nez v9, :cond_0

    if-eqz v10, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 402
    goto :goto_0

    :cond_3
    move v3, v2

    .line 403
    goto :goto_1

    :cond_4
    move v4, v2

    .line 404
    goto :goto_2

    :cond_5
    move v5, v2

    .line 405
    goto :goto_3

    :cond_6
    move v6, v2

    .line 406
    goto :goto_4

    :cond_7
    move v7, v2

    .line 407
    goto :goto_5

    :cond_8
    move v8, v2

    .line 408
    goto :goto_6

    :cond_9
    move v9, v2

    .line 409
    goto :goto_7

    :cond_a
    move v10, v2

    .line 411
    goto :goto_8
.end method

.method public y()V
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Lkki;->o:Lklb;

    invoke-virtual {v0}, Lklb;->b()Lkkz;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 504
    iget-object v0, p0, Lkki;->l:Lklo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lkki;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 506
    :cond_0
    invoke-virtual {p0}, Lkki;->v()V

    .line 514
    :goto_0
    return-void

    .line 507
    :cond_1
    iget-object v0, p0, Lkki;->n:Ljbx;

    invoke-virtual {v0}, Ljbx;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 508
    iget-object v0, p0, Lkki;->n:Ljbx;

    invoke-virtual {v0, p0}, Ljbx;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 509
    :cond_2
    iget-object v0, p0, Lkki;->p:Lkjj;

    invoke-interface {v0}, Lkjj;->a()Lkji;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 510
    iget-object v0, p0, Lkki;->p:Lkjj;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lkjj;->a(Lkji;)V

    goto :goto_0

    .line 512
    :cond_3
    invoke-direct {p0}, Lkki;->z()V

    goto :goto_0
.end method
