.class public final Lmmc;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmmc;


# instance fields
.field public b:I

.field public c:Lmmd;

.field public d:Lmme;

.field public e:Lmmj;

.field public f:Ljava/lang/Boolean;

.field private g:Lmom;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1334
    const/4 v0, 0x0

    new-array v0, v0, [Lmmc;

    sput-object v0, Lmmc;->a:[Lmmc;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1335
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1345
    const/high16 v0, -0x80000000

    iput v0, p0, Lmmc;->b:I

    .line 1348
    iput-object v1, p0, Lmmc;->c:Lmmd;

    .line 1351
    iput-object v1, p0, Lmmc;->d:Lmme;

    .line 1354
    iput-object v1, p0, Lmmc;->g:Lmom;

    .line 1357
    iput-object v1, p0, Lmmc;->e:Lmmj;

    .line 1335
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1388
    const/4 v0, 0x0

    .line 1389
    iget v1, p0, Lmmc;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1390
    const/4 v0, 0x1

    iget v1, p0, Lmmc;->b:I

    .line 1391
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1393
    :cond_0
    iget-object v1, p0, Lmmc;->d:Lmme;

    if-eqz v1, :cond_1

    .line 1394
    const/4 v1, 0x2

    iget-object v2, p0, Lmmc;->d:Lmme;

    .line 1395
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1397
    :cond_1
    iget-object v1, p0, Lmmc;->g:Lmom;

    if-eqz v1, :cond_2

    .line 1398
    const/4 v1, 0x3

    iget-object v2, p0, Lmmc;->g:Lmom;

    .line 1399
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1401
    :cond_2
    iget-object v1, p0, Lmmc;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1402
    const/4 v1, 0x4

    iget-object v2, p0, Lmmc;->f:Ljava/lang/Boolean;

    .line 1403
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1405
    :cond_3
    iget-object v1, p0, Lmmc;->e:Lmmj;

    if-eqz v1, :cond_4

    .line 1406
    const/4 v1, 0x5

    iget-object v2, p0, Lmmc;->e:Lmmj;

    .line 1407
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1409
    :cond_4
    iget-object v1, p0, Lmmc;->c:Lmmd;

    if-eqz v1, :cond_5

    .line 1410
    const/4 v1, 0x6

    iget-object v2, p0, Lmmc;->c:Lmmd;

    .line 1411
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1413
    :cond_5
    iget-object v1, p0, Lmmc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1414
    iput v0, p0, Lmmc;->ai:I

    .line 1415
    return v0
.end method

.method public a(Loxn;)Lmmc;
    .locals 2

    .prologue
    .line 1423
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1424
    sparse-switch v0, :sswitch_data_0

    .line 1428
    iget-object v1, p0, Lmmc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1429
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmmc;->ah:Ljava/util/List;

    .line 1432
    :cond_1
    iget-object v1, p0, Lmmc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1434
    :sswitch_0
    return-object p0

    .line 1439
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1440
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 1444
    :cond_2
    iput v0, p0, Lmmc;->b:I

    goto :goto_0

    .line 1446
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmmc;->b:I

    goto :goto_0

    .line 1451
    :sswitch_2
    iget-object v0, p0, Lmmc;->d:Lmme;

    if-nez v0, :cond_4

    .line 1452
    new-instance v0, Lmme;

    invoke-direct {v0}, Lmme;-><init>()V

    iput-object v0, p0, Lmmc;->d:Lmme;

    .line 1454
    :cond_4
    iget-object v0, p0, Lmmc;->d:Lmme;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1458
    :sswitch_3
    iget-object v0, p0, Lmmc;->g:Lmom;

    if-nez v0, :cond_5

    .line 1459
    new-instance v0, Lmom;

    invoke-direct {v0}, Lmom;-><init>()V

    iput-object v0, p0, Lmmc;->g:Lmom;

    .line 1461
    :cond_5
    iget-object v0, p0, Lmmc;->g:Lmom;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1465
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmmc;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 1469
    :sswitch_5
    iget-object v0, p0, Lmmc;->e:Lmmj;

    if-nez v0, :cond_6

    .line 1470
    new-instance v0, Lmmj;

    invoke-direct {v0}, Lmmj;-><init>()V

    iput-object v0, p0, Lmmc;->e:Lmmj;

    .line 1472
    :cond_6
    iget-object v0, p0, Lmmc;->e:Lmmj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1476
    :sswitch_6
    iget-object v0, p0, Lmmc;->c:Lmmd;

    if-nez v0, :cond_7

    .line 1477
    new-instance v0, Lmmd;

    invoke-direct {v0}, Lmmd;-><init>()V

    iput-object v0, p0, Lmmc;->c:Lmmd;

    .line 1479
    :cond_7
    iget-object v0, p0, Lmmc;->c:Lmmd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1424
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1364
    iget v0, p0, Lmmc;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1365
    const/4 v0, 0x1

    iget v1, p0, Lmmc;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1367
    :cond_0
    iget-object v0, p0, Lmmc;->d:Lmme;

    if-eqz v0, :cond_1

    .line 1368
    const/4 v0, 0x2

    iget-object v1, p0, Lmmc;->d:Lmme;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1370
    :cond_1
    iget-object v0, p0, Lmmc;->g:Lmom;

    if-eqz v0, :cond_2

    .line 1371
    const/4 v0, 0x3

    iget-object v1, p0, Lmmc;->g:Lmom;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1373
    :cond_2
    iget-object v0, p0, Lmmc;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1374
    const/4 v0, 0x4

    iget-object v1, p0, Lmmc;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1376
    :cond_3
    iget-object v0, p0, Lmmc;->e:Lmmj;

    if-eqz v0, :cond_4

    .line 1377
    const/4 v0, 0x5

    iget-object v1, p0, Lmmc;->e:Lmmj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1379
    :cond_4
    iget-object v0, p0, Lmmc;->c:Lmmd;

    if-eqz v0, :cond_5

    .line 1380
    const/4 v0, 0x6

    iget-object v1, p0, Lmmc;->c:Lmmd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1382
    :cond_5
    iget-object v0, p0, Lmmc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1384
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1331
    invoke-virtual {p0, p1}, Lmmc;->a(Loxn;)Lmmc;

    move-result-object v0

    return-object v0
.end method
