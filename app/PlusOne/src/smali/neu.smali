.class public final Lneu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field public d:Lnjp;

.field public e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2045
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2056
    const/4 v0, 0x0

    iput-object v0, p0, Lneu;->d:Lnjp;

    .line 2045
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2087
    const/4 v0, 0x0

    .line 2088
    iget-object v1, p0, Lneu;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2089
    const/4 v0, 0x1

    iget-object v1, p0, Lneu;->a:Ljava/lang/String;

    .line 2090
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2092
    :cond_0
    iget-object v1, p0, Lneu;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2093
    const/4 v1, 0x2

    iget-object v2, p0, Lneu;->b:Ljava/lang/String;

    .line 2094
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2096
    :cond_1
    iget-object v1, p0, Lneu;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 2097
    const/4 v1, 0x3

    iget-object v2, p0, Lneu;->c:Ljava/lang/Boolean;

    .line 2098
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2100
    :cond_2
    iget-object v1, p0, Lneu;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2101
    const/4 v1, 0x4

    iget-object v2, p0, Lneu;->f:Ljava/lang/Integer;

    .line 2102
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2104
    :cond_3
    iget-object v1, p0, Lneu;->d:Lnjp;

    if-eqz v1, :cond_4

    .line 2105
    const/4 v1, 0x5

    iget-object v2, p0, Lneu;->d:Lnjp;

    .line 2106
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2108
    :cond_4
    iget-object v1, p0, Lneu;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 2109
    const/4 v1, 0x6

    iget-object v2, p0, Lneu;->e:Ljava/lang/Integer;

    .line 2110
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2112
    :cond_5
    iget-object v1, p0, Lneu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2113
    iput v0, p0, Lneu;->ai:I

    .line 2114
    return v0
.end method

.method public a(Loxn;)Lneu;
    .locals 2

    .prologue
    .line 2122
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2123
    sparse-switch v0, :sswitch_data_0

    .line 2127
    iget-object v1, p0, Lneu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2128
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lneu;->ah:Ljava/util/List;

    .line 2131
    :cond_1
    iget-object v1, p0, Lneu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2133
    :sswitch_0
    return-object p0

    .line 2138
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lneu;->a:Ljava/lang/String;

    goto :goto_0

    .line 2142
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lneu;->b:Ljava/lang/String;

    goto :goto_0

    .line 2146
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lneu;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 2150
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lneu;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 2154
    :sswitch_5
    iget-object v0, p0, Lneu;->d:Lnjp;

    if-nez v0, :cond_2

    .line 2155
    new-instance v0, Lnjp;

    invoke-direct {v0}, Lnjp;-><init>()V

    iput-object v0, p0, Lneu;->d:Lnjp;

    .line 2157
    :cond_2
    iget-object v0, p0, Lneu;->d:Lnjp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2161
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lneu;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 2123
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2063
    iget-object v0, p0, Lneu;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2064
    const/4 v0, 0x1

    iget-object v1, p0, Lneu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2066
    :cond_0
    iget-object v0, p0, Lneu;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2067
    const/4 v0, 0x2

    iget-object v1, p0, Lneu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2069
    :cond_1
    iget-object v0, p0, Lneu;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 2070
    const/4 v0, 0x3

    iget-object v1, p0, Lneu;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2072
    :cond_2
    iget-object v0, p0, Lneu;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2073
    const/4 v0, 0x4

    iget-object v1, p0, Lneu;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2075
    :cond_3
    iget-object v0, p0, Lneu;->d:Lnjp;

    if-eqz v0, :cond_4

    .line 2076
    const/4 v0, 0x5

    iget-object v1, p0, Lneu;->d:Lnjp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2078
    :cond_4
    iget-object v0, p0, Lneu;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 2079
    const/4 v0, 0x6

    iget-object v1, p0, Lneu;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2081
    :cond_5
    iget-object v0, p0, Lneu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2083
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2041
    invoke-virtual {p0, p1}, Lneu;->a(Loxn;)Lneu;

    move-result-object v0

    return-object v0
.end method
