.class public final Lmca;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnoa;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28915
    invoke-direct {p0}, Loxq;-><init>()V

    .line 28918
    iput-object v0, p0, Lmca;->apiHeader:Llyq;

    .line 28921
    iput-object v0, p0, Lmca;->a:Lnoa;

    .line 28915
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 28938
    const/4 v0, 0x0

    .line 28939
    iget-object v1, p0, Lmca;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 28940
    const/4 v0, 0x1

    iget-object v1, p0, Lmca;->apiHeader:Llyq;

    .line 28941
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 28943
    :cond_0
    iget-object v1, p0, Lmca;->a:Lnoa;

    if-eqz v1, :cond_1

    .line 28944
    const/4 v1, 0x2

    iget-object v2, p0, Lmca;->a:Lnoa;

    .line 28945
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28947
    :cond_1
    iget-object v1, p0, Lmca;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28948
    iput v0, p0, Lmca;->ai:I

    .line 28949
    return v0
.end method

.method public a(Loxn;)Lmca;
    .locals 2

    .prologue
    .line 28957
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 28958
    sparse-switch v0, :sswitch_data_0

    .line 28962
    iget-object v1, p0, Lmca;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 28963
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmca;->ah:Ljava/util/List;

    .line 28966
    :cond_1
    iget-object v1, p0, Lmca;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28968
    :sswitch_0
    return-object p0

    .line 28973
    :sswitch_1
    iget-object v0, p0, Lmca;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 28974
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmca;->apiHeader:Llyq;

    .line 28976
    :cond_2
    iget-object v0, p0, Lmca;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28980
    :sswitch_2
    iget-object v0, p0, Lmca;->a:Lnoa;

    if-nez v0, :cond_3

    .line 28981
    new-instance v0, Lnoa;

    invoke-direct {v0}, Lnoa;-><init>()V

    iput-object v0, p0, Lmca;->a:Lnoa;

    .line 28983
    :cond_3
    iget-object v0, p0, Lmca;->a:Lnoa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28958
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28926
    iget-object v0, p0, Lmca;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 28927
    const/4 v0, 0x1

    iget-object v1, p0, Lmca;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28929
    :cond_0
    iget-object v0, p0, Lmca;->a:Lnoa;

    if-eqz v0, :cond_1

    .line 28930
    const/4 v0, 0x2

    iget-object v1, p0, Lmca;->a:Lnoa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28932
    :cond_1
    iget-object v0, p0, Lmca;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 28934
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 28911
    invoke-virtual {p0, p1}, Lmca;->a(Loxn;)Lmca;

    move-result-object v0

    return-object v0
.end method
