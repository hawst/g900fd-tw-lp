.class public final Loci;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loci;


# instance fields
.field private b:[Loch;

.field private c:Lock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1075
    const/4 v0, 0x0

    new-array v0, v0, [Loci;

    sput-object v0, Loci;->a:[Loci;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1076
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1079
    sget-object v0, Loch;->a:[Loch;

    iput-object v0, p0, Loci;->b:[Loch;

    .line 1082
    const/4 v0, 0x0

    iput-object v0, p0, Loci;->c:Lock;

    .line 1076
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1103
    .line 1104
    iget-object v1, p0, Loci;->b:[Loch;

    if-eqz v1, :cond_1

    .line 1105
    iget-object v2, p0, Loci;->b:[Loch;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1106
    if-eqz v4, :cond_0

    .line 1107
    const/4 v5, 0x1

    .line 1108
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1105
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1112
    :cond_1
    iget-object v1, p0, Loci;->c:Lock;

    if-eqz v1, :cond_2

    .line 1113
    const/4 v1, 0x2

    iget-object v2, p0, Loci;->c:Lock;

    .line 1114
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1116
    :cond_2
    iget-object v1, p0, Loci;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1117
    iput v0, p0, Loci;->ai:I

    .line 1118
    return v0
.end method

.method public a(Loxn;)Loci;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1126
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1127
    sparse-switch v0, :sswitch_data_0

    .line 1131
    iget-object v2, p0, Loci;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1132
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loci;->ah:Ljava/util/List;

    .line 1135
    :cond_1
    iget-object v2, p0, Loci;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1137
    :sswitch_0
    return-object p0

    .line 1142
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1143
    iget-object v0, p0, Loci;->b:[Loch;

    if-nez v0, :cond_3

    move v0, v1

    .line 1144
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loch;

    .line 1145
    iget-object v3, p0, Loci;->b:[Loch;

    if-eqz v3, :cond_2

    .line 1146
    iget-object v3, p0, Loci;->b:[Loch;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1148
    :cond_2
    iput-object v2, p0, Loci;->b:[Loch;

    .line 1149
    :goto_2
    iget-object v2, p0, Loci;->b:[Loch;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1150
    iget-object v2, p0, Loci;->b:[Loch;

    new-instance v3, Loch;

    invoke-direct {v3}, Loch;-><init>()V

    aput-object v3, v2, v0

    .line 1151
    iget-object v2, p0, Loci;->b:[Loch;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1152
    invoke-virtual {p1}, Loxn;->a()I

    .line 1149
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1143
    :cond_3
    iget-object v0, p0, Loci;->b:[Loch;

    array-length v0, v0

    goto :goto_1

    .line 1155
    :cond_4
    iget-object v2, p0, Loci;->b:[Loch;

    new-instance v3, Loch;

    invoke-direct {v3}, Loch;-><init>()V

    aput-object v3, v2, v0

    .line 1156
    iget-object v2, p0, Loci;->b:[Loch;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1160
    :sswitch_2
    iget-object v0, p0, Loci;->c:Lock;

    if-nez v0, :cond_5

    .line 1161
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Loci;->c:Lock;

    .line 1163
    :cond_5
    iget-object v0, p0, Loci;->c:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1127
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1087
    iget-object v0, p0, Loci;->b:[Loch;

    if-eqz v0, :cond_1

    .line 1088
    iget-object v1, p0, Loci;->b:[Loch;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1089
    if-eqz v3, :cond_0

    .line 1090
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1088
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1094
    :cond_1
    iget-object v0, p0, Loci;->c:Lock;

    if-eqz v0, :cond_2

    .line 1095
    const/4 v0, 0x2

    iget-object v1, p0, Loci;->c:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1097
    :cond_2
    iget-object v0, p0, Loci;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1099
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1072
    invoke-virtual {p0, p1}, Loci;->a(Loxn;)Loci;

    move-result-object v0

    return-object v0
.end method
