.class public abstract Lkgg;
.super Lkfx;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RQ:",
        "Loxu;",
        "RS:",
        "Loxu;",
        ">",
        "Lkfx",
        "<TRQ;TRS;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkfo;",
            "Ljava/lang/String;",
            "TRQ;TRS;)V"
        }
    .end annotation

    .prologue
    .line 49
    const-string v6, "plusi"

    const-string v7, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lkfx;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method


# virtual methods
.method public C()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53
    invoke-super {p0}, Lkfx;->C()Ljava/lang/String;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lkgg;->g:Lkfo;

    invoke-virtual {v1}, Lkfo;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lkge;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method protected a([B)Loxu;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TRS;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-super {p0, p1}, Lkfx;->a([B)Loxu;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lkgg;->f:Landroid/content/Context;

    invoke-static {v1, v0}, Lkgi;->a(Landroid/content/Context;Loxu;)V

    .line 79
    return-object v0
.end method

.method public b([BLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 84
    .line 86
    :try_start_0
    new-instance v3, Lkgd;

    invoke-direct {v3, p1}, Lkgd;-><init>([B)V

    .line 87
    invoke-virtual {v3}, Lkgd;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 88
    new-instance v1, Lkgf;

    iget-object v0, p0, Lkgg;->g:Lkfo;

    invoke-virtual {v0}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lkgf;-><init>(Lkgd;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :try_start_1
    const-string v0, "APP_UPGRADE_REQUIRED"

    invoke-static {v1, v0}, Lkgf;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lkgg;->f:Landroid/content/Context;

    const-class v4, Lkez;

    .line 93
    invoke-static {v0, v4}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkez;

    .line 94
    if-eqz v0, :cond_0

    .line 95
    invoke-interface {v0}, Lkez;->a()V

    .line 99
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Apiary error response: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lkgg;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "   domain: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lkgd;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "   reason: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lkgd;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v4, "   message: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lkgd;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lkgd;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string v4, "\\n"

    const-string v5, "\n"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\\t"

    const-string v5, "\t"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "   debugInfo: \n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v3, 0x6

    const-string v4, "HttpOperation"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    move-object v0, v1

    .line 105
    :goto_1
    if-eqz v0, :cond_2

    .line 106
    invoke-virtual {v0}, Lkgf;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lkgg;->d([BLjava/lang/String;)V

    .line 107
    throw v0

    .line 109
    :cond_2
    invoke-virtual {p0, p1, v2}, Lkgg;->d([BLjava/lang/String;)V

    .line 110
    return-void

    :catch_0
    move-exception v0

    move-object v1, v2

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method

.method protected b(Ljava/lang/Exception;)Z
    .locals 1

    .prologue
    .line 130
    const-string v0, "INVALID_CREDENTIALS"

    invoke-static {p1, v0}, Lkgf;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x1

    .line 133
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lkfx;->b(Ljava/lang/Exception;)Z

    move-result v0

    goto :goto_0
.end method

.method protected c(Loxu;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRQ;)V"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lkgg;->g:Lkfo;

    invoke-virtual {v0}, Lkfo;->c()Lkey;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 62
    :goto_0
    iget-object v2, p0, Lkgg;->f:Landroid/content/Context;

    iget-object v1, p0, Lkgg;->g:Lkfo;

    invoke-virtual {v1}, Lkfo;->b()Ljava/lang/String;

    move-result-object v3

    .line 63
    iget-object v1, p0, Lkgg;->g:Lkfo;

    invoke-virtual {v1}, Lkfo;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x32

    .line 62
    :goto_1
    invoke-static {v2, p1, v3, v0, v1}, Lkgi;->a(Landroid/content/Context;Loxu;Ljava/lang/String;ZI)V

    .line 64
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 63
    :cond_1
    const/16 v1, 0x64

    goto :goto_1
.end method
