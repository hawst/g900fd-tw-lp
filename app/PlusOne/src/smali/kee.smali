.class public final Lkee;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:I

.field final b:I

.field final c:J

.field private final d:Lkef;


# direct methods
.method constructor <init>(IIJLkef;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput p1, p0, Lkee;->b:I

    .line 96
    iput p2, p0, Lkee;->a:I

    .line 97
    iput-wide p3, p0, Lkee;->c:J

    .line 98
    iput-object p5, p0, Lkee;->d:Lkef;

    .line 99
    return-void
.end method

.method constructor <init>(IILkef;)V
    .locals 7

    .prologue
    .line 109
    int-to-long v0, p1

    int-to-long v2, p2

    mul-long/2addr v0, v2

    const/4 v2, 0x2

    shl-long v4, v0, v2

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lkee;-><init>(IIJLkef;)V

    .line 110
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 114
    instance-of v0, p1, Lkee;

    if-eqz v0, :cond_0

    .line 115
    check-cast p1, Lkee;

    .line 116
    iget-object v0, p0, Lkee;->d:Lkef;

    invoke-interface {v0, p0, p1}, Lkef;->a(Lkee;Lkee;)Z

    move-result v0

    .line 118
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lkee;->d:Lkef;

    invoke-interface {v0, p0}, Lkef;->a(Lkee;)I

    move-result v0

    return v0
.end method
