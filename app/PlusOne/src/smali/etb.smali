.class final Letb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field private synthetic a:Landroid/widget/ViewFlipper;

.field private synthetic b:Landroid/view/View;

.field private synthetic c:Lesy;


# direct methods
.method constructor <init>(Lesy;Landroid/widget/ViewFlipper;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Letb;->c:Lesy;

    iput-object p2, p0, Letb;->a:Landroid/widget/ViewFlipper;

    iput-object p3, p0, Letb;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const-wide/16 v8, 0x1f4

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 385
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    move v0, v1

    .line 386
    :goto_0
    iget-object v3, p0, Letb;->c:Lesy;

    iget-object v3, v3, Lesy;->c:Lnhl;

    iget-object v3, v3, Lnhl;->c:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 387
    if-eqz v0, :cond_0

    iget-object v3, p0, Letb;->a:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v3

    if-eq v3, v1, :cond_1

    :cond_0
    if-nez v0, :cond_5

    iget-object v3, p0, Letb;->a:Landroid/widget/ViewFlipper;

    .line 388
    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v3

    iget-object v4, p0, Letb;->a:Landroid/widget/ViewFlipper;

    invoke-virtual {v4}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_5

    .line 410
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 385
    goto :goto_0

    .line 392
    :cond_3
    if-eqz v0, :cond_4

    iget-object v3, p0, Letb;->a:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v3

    if-eq v3, v1, :cond_1

    :cond_4
    if-nez v0, :cond_5

    iget-object v3, p0, Letb;->a:Landroid/widget/ViewFlipper;

    .line 393
    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v3

    if-eqz v3, :cond_1

    .line 398
    :cond_5
    iget-object v3, p0, Letb;->b:Landroid/view/View;

    const v4, 0x7f1002b7

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 399
    if-nez v0, :cond_7

    iget-object v4, p0, Letb;->c:Lesy;

    iget-object v4, v4, Lesy;->c:Lnhl;

    iget-object v4, v4, Lnhl;->c:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 400
    invoke-static {v3}, Leud;->a(Landroid/view/View;)V

    .line 405
    :cond_6
    :goto_2
    iget-object v3, p0, Letb;->c:Lesy;

    iget-object v3, v3, Lesy;->c:Lnhl;

    iget-object v3, v3, Lnhl;->c:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 407
    if-nez v0, :cond_8

    .line 409
    :goto_3
    iget-object v0, p0, Letb;->a:Landroid/widget/ViewFlipper;

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v2, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v7, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v2, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    goto :goto_1

    .line 401
    :cond_7
    iget-object v4, p0, Letb;->c:Lesy;

    iget-object v4, v4, Lesy;->c:Lnhl;

    iget-object v4, v4, Lnhl;->c:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 402
    new-instance v4, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v4, v7, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    invoke-virtual {v4, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v5, Leue;

    invoke-direct {v5, v3}, Leue;-><init>(Landroid/view/View;)V

    invoke-virtual {v4, v5}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    :cond_8
    move v1, v2

    .line 407
    goto :goto_3

    .line 409
    :cond_9
    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showPrevious()V

    goto/16 :goto_1

    :cond_a
    move v1, v0

    goto :goto_3
.end method
