.class public final Loip;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Float;

.field private b:Ljava/lang/Float;

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 547
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 578
    const/4 v0, 0x0

    .line 579
    iget-object v1, p0, Loip;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 580
    const/4 v0, 0x1

    iget-object v1, p0, Loip;->a:Ljava/lang/Float;

    .line 581
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 583
    :cond_0
    iget-object v1, p0, Loip;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 584
    const/4 v1, 0x2

    iget-object v2, p0, Loip;->b:Ljava/lang/Float;

    .line 585
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 587
    :cond_1
    iget-object v1, p0, Loip;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 588
    const/4 v1, 0x3

    iget-object v2, p0, Loip;->c:Ljava/lang/Float;

    .line 589
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 591
    :cond_2
    iget-object v1, p0, Loip;->d:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 592
    const/4 v1, 0x4

    iget-object v2, p0, Loip;->d:Ljava/lang/Float;

    .line 593
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 595
    :cond_3
    iget-object v1, p0, Loip;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 596
    iput v0, p0, Loip;->ai:I

    .line 597
    return v0
.end method

.method public a(Loxn;)Loip;
    .locals 2

    .prologue
    .line 605
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 606
    sparse-switch v0, :sswitch_data_0

    .line 610
    iget-object v1, p0, Loip;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 611
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loip;->ah:Ljava/util/List;

    .line 614
    :cond_1
    iget-object v1, p0, Loip;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 616
    :sswitch_0
    return-object p0

    .line 621
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loip;->a:Ljava/lang/Float;

    goto :goto_0

    .line 625
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loip;->b:Ljava/lang/Float;

    goto :goto_0

    .line 629
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loip;->c:Ljava/lang/Float;

    goto :goto_0

    .line 633
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loip;->d:Ljava/lang/Float;

    goto :goto_0

    .line 606
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 560
    iget-object v0, p0, Loip;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 561
    const/4 v0, 0x1

    iget-object v1, p0, Loip;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 563
    :cond_0
    iget-object v0, p0, Loip;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 564
    const/4 v0, 0x2

    iget-object v1, p0, Loip;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 566
    :cond_1
    iget-object v0, p0, Loip;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 567
    const/4 v0, 0x3

    iget-object v1, p0, Loip;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 569
    :cond_2
    iget-object v0, p0, Loip;->d:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 570
    const/4 v0, 0x4

    iget-object v1, p0, Loip;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 572
    :cond_3
    iget-object v0, p0, Loip;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 574
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0, p1}, Loip;->a(Loxn;)Loip;

    move-result-object v0

    return-object v0
.end method
