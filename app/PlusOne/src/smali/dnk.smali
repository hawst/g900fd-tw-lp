.class public final Ldnk;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmjm;",
        "Lmjn;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private p:Landroid/graphics/RectF;

.field private q:I

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Landroid/graphics/RectF;IZ)V
    .locals 6

    .prologue
    .line 48
    const-string v3, "setscrapbookcoverphoto"

    new-instance v4, Lmjm;

    invoke-direct {v4}, Lmjm;-><init>()V

    new-instance v5, Lmjn;

    invoke-direct {v5}, Lmjn;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 50
    iput-object p3, p0, Ldnk;->b:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Ldnk;->p:Landroid/graphics/RectF;

    .line 52
    iput p5, p0, Ldnk;->q:I

    .line 53
    iput-boolean p6, p0, Ldnk;->r:Z

    .line 54
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 55
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 56
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnk;->a:Ljava/lang/String;

    .line 57
    return-void
.end method


# virtual methods
.method protected a(Lmjm;)V
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lneu;

    invoke-direct {v0}, Lneu;-><init>()V

    iput-object v0, p1, Lmjm;->a:Lneu;

    .line 62
    iget-object v0, p1, Lmjm;->a:Lneu;

    .line 63
    iget-object v1, p0, Ldnk;->a:Ljava/lang/String;

    iput-object v1, v0, Lneu;->a:Ljava/lang/String;

    .line 64
    iget-object v1, p0, Ldnk;->b:Ljava/lang/String;

    iput-object v1, v0, Lneu;->b:Ljava/lang/String;

    .line 65
    new-instance v1, Lnjp;

    invoke-direct {v1}, Lnjp;-><init>()V

    .line 66
    iget-object v2, p0, Ldnk;->p:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lnjp;->b:Ljava/lang/Float;

    .line 67
    iget-object v2, p0, Ldnk;->p:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lnjp;->a:Ljava/lang/Float;

    .line 68
    iget-object v2, p0, Ldnk;->p:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lnjp;->d:Ljava/lang/Float;

    .line 69
    iget-object v2, p0, Ldnk;->p:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lnjp;->c:Ljava/lang/Float;

    .line 70
    iput-object v1, v0, Lneu;->d:Lnjp;

    .line 71
    iget v1, p0, Ldnk;->q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lneu;->e:Ljava/lang/Integer;

    .line 72
    iget-boolean v1, p0, Ldnk;->r:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lneu;->c:Ljava/lang/Boolean;

    .line 73
    return-void
.end method

.method protected a(Lmjn;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p1, Lmjn;->a:Lnfv;

    .line 79
    iget-object v0, v0, Lnfv;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ldnl;

    invoke-direct {v0}, Ldnl;-><init>()V

    throw v0

    .line 82
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmjm;

    invoke-virtual {p0, p1}, Ldnk;->a(Lmjm;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmjn;

    invoke-virtual {p0, p1}, Ldnk;->a(Lmjn;)V

    return-void
.end method
