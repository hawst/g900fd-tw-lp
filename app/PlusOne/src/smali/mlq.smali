.class public final Lmlq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlw;

.field public b:Lmok;

.field public c:Lmlx;

.field public d:Lnei;

.field public e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 629
    invoke-direct {p0}, Loxq;-><init>()V

    .line 632
    iput-object v0, p0, Lmlq;->a:Lmlw;

    .line 635
    iput-object v0, p0, Lmlq;->b:Lmok;

    .line 638
    iput-object v0, p0, Lmlq;->c:Lmlx;

    .line 641
    iput-object v0, p0, Lmlq;->d:Lnei;

    .line 629
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 674
    const/4 v0, 0x0

    .line 675
    iget-object v1, p0, Lmlq;->b:Lmok;

    if-eqz v1, :cond_0

    .line 676
    const/4 v0, 0x1

    iget-object v1, p0, Lmlq;->b:Lmok;

    .line 677
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 679
    :cond_0
    iget-object v1, p0, Lmlq;->c:Lmlx;

    if-eqz v1, :cond_1

    .line 680
    const/4 v1, 0x3

    iget-object v2, p0, Lmlq;->c:Lmlx;

    .line 681
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 683
    :cond_1
    iget-object v1, p0, Lmlq;->a:Lmlw;

    if-eqz v1, :cond_2

    .line 684
    const/16 v1, 0x8

    iget-object v2, p0, Lmlq;->a:Lmlw;

    .line 685
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 687
    :cond_2
    iget-object v1, p0, Lmlq;->d:Lnei;

    if-eqz v1, :cond_3

    .line 688
    const/16 v1, 0x9

    iget-object v2, p0, Lmlq;->d:Lnei;

    .line 689
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 691
    :cond_3
    iget-object v1, p0, Lmlq;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 692
    const/16 v1, 0xa

    iget-object v2, p0, Lmlq;->e:Ljava/lang/Boolean;

    .line 693
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 695
    :cond_4
    iget-object v1, p0, Lmlq;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 696
    const/16 v1, 0xb

    iget-object v2, p0, Lmlq;->f:Ljava/lang/Boolean;

    .line 697
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 699
    :cond_5
    iget-object v1, p0, Lmlq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 700
    iput v0, p0, Lmlq;->ai:I

    .line 701
    return v0
.end method

.method public a(Loxn;)Lmlq;
    .locals 2

    .prologue
    .line 709
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 710
    sparse-switch v0, :sswitch_data_0

    .line 714
    iget-object v1, p0, Lmlq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 715
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmlq;->ah:Ljava/util/List;

    .line 718
    :cond_1
    iget-object v1, p0, Lmlq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 720
    :sswitch_0
    return-object p0

    .line 725
    :sswitch_1
    iget-object v0, p0, Lmlq;->b:Lmok;

    if-nez v0, :cond_2

    .line 726
    new-instance v0, Lmok;

    invoke-direct {v0}, Lmok;-><init>()V

    iput-object v0, p0, Lmlq;->b:Lmok;

    .line 728
    :cond_2
    iget-object v0, p0, Lmlq;->b:Lmok;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 732
    :sswitch_2
    iget-object v0, p0, Lmlq;->c:Lmlx;

    if-nez v0, :cond_3

    .line 733
    new-instance v0, Lmlx;

    invoke-direct {v0}, Lmlx;-><init>()V

    iput-object v0, p0, Lmlq;->c:Lmlx;

    .line 735
    :cond_3
    iget-object v0, p0, Lmlq;->c:Lmlx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 739
    :sswitch_3
    iget-object v0, p0, Lmlq;->a:Lmlw;

    if-nez v0, :cond_4

    .line 740
    new-instance v0, Lmlw;

    invoke-direct {v0}, Lmlw;-><init>()V

    iput-object v0, p0, Lmlq;->a:Lmlw;

    .line 742
    :cond_4
    iget-object v0, p0, Lmlq;->a:Lmlw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 746
    :sswitch_4
    iget-object v0, p0, Lmlq;->d:Lnei;

    if-nez v0, :cond_5

    .line 747
    new-instance v0, Lnei;

    invoke-direct {v0}, Lnei;-><init>()V

    iput-object v0, p0, Lmlq;->d:Lnei;

    .line 749
    :cond_5
    iget-object v0, p0, Lmlq;->d:Lnei;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 753
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmlq;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 757
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmlq;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 710
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x42 -> :sswitch_3
        0x4a -> :sswitch_4
        0x50 -> :sswitch_5
        0x58 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 650
    iget-object v0, p0, Lmlq;->b:Lmok;

    if-eqz v0, :cond_0

    .line 651
    const/4 v0, 0x1

    iget-object v1, p0, Lmlq;->b:Lmok;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 653
    :cond_0
    iget-object v0, p0, Lmlq;->c:Lmlx;

    if-eqz v0, :cond_1

    .line 654
    const/4 v0, 0x3

    iget-object v1, p0, Lmlq;->c:Lmlx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 656
    :cond_1
    iget-object v0, p0, Lmlq;->a:Lmlw;

    if-eqz v0, :cond_2

    .line 657
    const/16 v0, 0x8

    iget-object v1, p0, Lmlq;->a:Lmlw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 659
    :cond_2
    iget-object v0, p0, Lmlq;->d:Lnei;

    if-eqz v0, :cond_3

    .line 660
    const/16 v0, 0x9

    iget-object v1, p0, Lmlq;->d:Lnei;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 662
    :cond_3
    iget-object v0, p0, Lmlq;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 663
    const/16 v0, 0xa

    iget-object v1, p0, Lmlq;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 665
    :cond_4
    iget-object v0, p0, Lmlq;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 666
    const/16 v0, 0xb

    iget-object v1, p0, Lmlq;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 668
    :cond_5
    iget-object v0, p0, Lmlq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 670
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 625
    invoke-virtual {p0, p1}, Lmlq;->a(Loxn;)Lmlq;

    move-result-object v0

    return-object v0
.end method
