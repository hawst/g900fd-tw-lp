.class public Lehh;
.super Llol;
.source "PG"

# interfaces
.implements Lag;
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Ldid;
.implements Lfdn;
.implements Lhjj;
.implements Lhmm;
.implements Lhmq;
.implements Lhob;
.implements Ljfl;
.implements Ljfn;
.implements Lkch;
.implements Lkxz;
.implements Llci;
.implements Llgs;
.implements Llgv;
.implements Llhc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Lag;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Ldid;",
        "Lfdn;",
        "Lhjj;",
        "Lhmm;",
        "Lhmq;",
        "Lhob;",
        "Ljfl;",
        "Ljfn;",
        "Lkch;",
        "Lkxz;",
        "Llci;",
        "Llgs;",
        "Llgv;",
        "Llhc;"
    }
.end annotation


# static fields
.field private static final N:[Ljava/lang/String;

.field private static O:I

.field private static ay:Llpa;


# instance fields
.field public final P:Lhov;

.field public final Q:Lhje;

.field public R:Lhee;

.field public S:Llhd;

.field public T:Liwk;

.field public final U:Licq;

.field public V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

.field public W:Lfdj;

.field public X:I

.field public Y:Ljava/lang/String;

.field public Z:J

.field private aA:[B

.field private aB:Landroid/widget/Toast;

.field private aC:Landroid/widget/Button;

.field private aD:Z

.field private aE:Z

.field private aF:J

.field private aG:Z

.field private aH:Z

.field private aI:Z

.field private aJ:Z

.field private aK:J

.field private aL:I

.field private aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

.field private aN:Llhj;

.field private aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

.field private aP:Llhj;

.field private aQ:Z

.field private aR:J

.field private aS:I

.field private aT:I

.field private aU:Z

.field private aV:I

.field private aW:I

.field private aX:Ldib;

.field private aY:Liax;

.field private aZ:Lfdp;

.field public aa:Ljava/lang/String;

.field public ab:Ljava/lang/String;

.field public ac:Levm;

.field public ad:Ljgf;

.field public ae:Z

.field public af:Landroid/location/Location;

.field public ag:Ljava/lang/String;

.field public ah:Liug;

.field public ai:Z

.field public aj:Landroid/view/View;

.field public ak:Z

.field public al:Z

.field public am:J

.field public an:Z

.field public ao:Ljava/lang/Integer;

.field public ap:Levp;

.field public aq:Z

.field public ar:Z

.field public as:Landroid/database/Cursor;

.field private aw:Ldxm;

.field private ax:Lehu;

.field private az:Lkci;

.field private ba:Ljpb;

.field private bb:Lknu;

.field private bc:Lita;

.field private bd:Lfhh;

.field private be:Z

.field private bf:Lehl;

.field private bg:Z

.field private bh:Lfcu;

.field private bi:Lhoc;

.field private bj:Llfn;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 244
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "circle_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "circle_name"

    aput-object v2, v0, v1

    sput-object v0, Lehh;->N:[Ljava/lang/String;

    .line 332
    new-instance v0, Llpa;

    const-string v1, "enable_popout_oneup"

    invoke-direct {v0, v1}, Llpa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lehh;->ay:Llpa;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 206
    invoke-direct {p0}, Llol;-><init>()V

    .line 314
    new-instance v0, Lhov;

    iget-object v1, p0, Lehh;->av:Llqm;

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Lehh;->P:Lhov;

    .line 315
    new-instance v0, Lhje;

    iget-object v1, p0, Lehh;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Lehh;->Q:Lhje;

    .line 318
    new-instance v0, Llhd;

    invoke-direct {v0}, Llhd;-><init>()V

    iput-object v0, p0, Lehh;->S:Llhd;

    .line 320
    new-instance v0, Licq;

    iget-object v1, p0, Lehh;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a06f6

    .line 321
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lehh;->U:Licq;

    .line 324
    new-instance v0, Lhnw;

    new-instance v1, Lehm;

    invoke-direct {v1, p0}, Lehm;-><init>(Lehh;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 334
    new-instance v0, Lkci;

    iget-object v1, p0, Lehh;->av:Llqm;

    const v2, 0x7f10005b

    invoke-direct {v0, p0, v1, v2, p0}, Lkci;-><init>(Lu;Llqr;ILkch;)V

    iput-object v0, p0, Lehh;->az:Lkci;

    .line 353
    const/4 v0, -0x1

    iput v0, p0, Lehh;->X:I

    .line 366
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->ai:Z

    .line 379
    iput-boolean v3, p0, Lehh;->aI:Z

    .line 380
    iput-boolean v3, p0, Lehh;->aJ:Z

    .line 383
    const/4 v0, 0x2

    iput v0, p0, Lehh;->aL:I

    .line 417
    new-instance v0, Lehq;

    invoke-direct {v0, p0}, Lehq;-><init>(Lehh;)V

    iput-object v0, p0, Lehh;->bd:Lfhh;

    .line 422
    iput-boolean v3, p0, Lehh;->bg:Z

    .line 3412
    return-void
.end method

.method private X()V
    .locals 4

    .prologue
    .line 1387
    iget-object v0, p0, Lehh;->ad:Ljgf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehh;->ac:Levm;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lehh;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1388
    invoke-virtual {p0}, Lehh;->as()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1389
    iget-object v1, p0, Lehh;->ad:Ljgf;

    iget-object v0, p0, Lehh;->ac:Levm;

    .line 1390
    invoke-virtual {v0}, Levm;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->h()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lehh;->ac:Levm;

    .line 1391
    invoke-virtual {v2}, Levm;->f()J

    move-result-wide v2

    .line 1389
    invoke-interface {v1, v0, v2, v3}, Ljgf;->a(Ljava/lang/String;J)Z

    .line 1394
    :cond_0
    invoke-static {p0}, Lhmc;->b(Llol;)V

    .line 1395
    invoke-static {p0}, Lhmc;->a(Llol;)V

    .line 1396
    return-void

    .line 1390
    :cond_1
    const-string v0, "circles"

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Lhmw;
    .locals 1

    .prologue
    .line 2264
    const-string v0, "v.all.circles"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2265
    sget-object v0, Lhmw;->b:Lhmw;

    .line 2275
    :goto_0
    return-object v0

    .line 2266
    :cond_0
    const-string v0, "v.whatshot"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2267
    sget-object v0, Lhmw;->e:Lhmw;

    goto :goto_0

    .line 2268
    :cond_1
    const-string v0, "v.nearby"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2269
    sget-object v0, Lhmw;->d:Lhmw;

    goto :goto_0

    .line 2270
    :cond_2
    const-string v0, "f."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2271
    sget-object v0, Lhmw;->c:Lhmw;

    goto :goto_0

    .line 2272
    :cond_3
    const-string v0, "g."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2273
    sget-object v0, Lhmw;->j:Lhmw;

    goto :goto_0

    .line 2275
    :cond_4
    sget-object v0, Lhmw;->a:Lhmw;

    goto :goto_0
.end method

.method static synthetic a(Lehh;)Llnl;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lehh;->at:Llnl;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 3403
    const-string v0, "extra_gaia_id"

    iget-object v1, p0, Lehh;->ab:Ljava/lang/String;

    .line 3404
    invoke-static {v0, v1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 3405
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehh;->R:Lhee;

    .line 3406
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v2, p1, v3}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->C:Lhmv;

    .line 3407
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 3408
    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 3405
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 3410
    return-void
.end method

.method private a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 1914
    invoke-virtual {p0}, Lehh;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lehh;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1941
    :cond_0
    :goto_0
    return-void

    .line 1921
    :cond_1
    iget-object v0, p0, Lehh;->ah:Liug;

    if-nez v0, :cond_2

    .line 1923
    new-instance v5, Lfcw;

    iget-object v0, p0, Lehh;->at:Llnl;

    iget-object v1, p0, Lehh;->R:Lhee;

    .line 1924
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    new-instance v2, Lehr;

    invoke-direct {v2, p0}, Lehr;-><init>(Lehh;)V

    invoke-direct {v5, v0, v1, v2}, Lfcw;-><init>(Landroid/content/Context;ILilo;)V

    .line 1925
    new-instance v0, Liug;

    iget-object v1, p0, Lehh;->at:Llnl;

    const-wide/16 v2, 0xbb8

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Liug;-><init>(Landroid/content/Context;JLandroid/location/Location;Lilo;)V

    iput-object v0, p0, Lehh;->ah:Liug;

    .line 1928
    :cond_2
    invoke-direct {p0}, Lehh;->ae()V

    .line 1930
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    .line 1931
    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-static {v1}, Liuo;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1932
    invoke-virtual {p0, v0}, Lehh;->d(Landroid/view/View;)V

    .line 1933
    invoke-direct {p0}, Lehh;->ad()V

    goto :goto_0

    .line 1935
    :cond_3
    iget-object v0, p0, Lehh;->ah:Liug;

    invoke-virtual {v0}, Liug;->b()V

    .line 1936
    iget-object v0, p0, Lehh;->af:Landroid/location/Location;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lehh;->az()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1937
    iget-object v0, p0, Lehh;->U:Licq;

    const v1, 0x7f0a02be

    invoke-virtual {v0, v1}, Licq;->a(I)Licq;

    .line 1938
    iget-object v0, p0, Lehh;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_0
.end method

.method static synthetic a(Lehh;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lehh;->a(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lehh;Levm;)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lehh;->a(Levm;)V

    return-void
.end method

.method static synthetic a(Lehh;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0, p1, p2, p3}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Lehu;)V
    .locals 4

    .prologue
    .line 2073
    invoke-virtual {p1}, Lehu;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 2074
    iget-object v1, p0, Lehh;->aw:Ldxm;

    invoke-interface {v1}, Ldxm;->b()I

    move-result v1

    .line 2075
    iget-object v2, p0, Lehh;->aw:Ldxm;

    invoke-interface {v2}, Ldxm;->a()I

    move-result v2

    .line 2076
    const-string v3, "popup_stream_top"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2077
    const-string v2, "popup_stream_start_height"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2078
    return-void
.end method

.method private a(Levm;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1307
    invoke-virtual {p0}, Lehh;->ad_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1384
    :goto_0
    return-void

    .line 1311
    :cond_0
    iget-object v0, p0, Lehh;->ac:Levm;

    if-nez v0, :cond_5

    move-object v1, v2

    .line 1313
    :goto_1
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1315
    const-string v0, "v.all.circles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    if-eqz v1, :cond_6

    .line 1316
    invoke-static {v1}, Lehh;->a(Ljava/lang/String;)Lhmw;

    move-result-object v0

    move-object v3, v0

    .line 1320
    :goto_2
    invoke-virtual {p1}, Levm;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lehh;->a(Ljava/lang/String;)Lhmw;

    move-result-object v4

    .line 1321
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v5, Lhms;

    invoke-virtual {v0, v5}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v5, Lhmr;

    iget-object v6, p0, Lehh;->at:Llnl;

    invoke-direct {v5, v6}, Lhmr;-><init>(Landroid/content/Context;)V

    .line 1323
    invoke-virtual {v5, v3}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v3

    .line 1324
    invoke-virtual {v3, v4}, Lhmr;->b(Lhmw;)Lhmr;

    move-result-object v3

    .line 1321
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 1328
    :cond_1
    iput-object p1, p0, Lehh;->ac:Levm;

    .line 1329
    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->e()Z

    move-result v0

    iput-boolean v0, p0, Lehh;->ae:Z

    .line 1330
    invoke-direct {p0}, Lehh;->am()V

    .line 1332
    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->a()Ljava/lang/String;

    move-result-object v0

    .line 1334
    iput-object v2, p0, Lehh;->Y:Ljava/lang/String;

    .line 1335
    iput-object v2, p0, Lehh;->aA:[B

    .line 1336
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lehh;->Z:J

    .line 1338
    iget-boolean v3, p0, Lehh;->ae:Z

    if-eqz v3, :cond_7

    .line 1339
    invoke-direct {p0, v2}, Lehh;->a(Landroid/location/Location;)V

    .line 1344
    :goto_3
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v3

    .line 1345
    if-eqz v3, :cond_2

    .line 1346
    iget-boolean v4, p0, Lehh;->ae:Z

    if-eqz v4, :cond_2

    .line 1347
    invoke-direct {p0}, Lehh;->ae()V

    .line 1348
    iget-object v4, p0, Lehh;->af:Landroid/location/Location;

    if-nez v4, :cond_8

    iget-object v4, p0, Lehh;->at:Llnl;

    invoke-static {v4}, Liuo;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1349
    invoke-virtual {p0}, Lehh;->az()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1350
    iget-object v3, p0, Lehh;->U:Licq;

    const v4, 0x7f0a02be

    invoke-virtual {v3, v4}, Licq;->a(I)Licq;

    .line 1351
    iget-object v3, p0, Lehh;->U:Licq;

    sget-object v4, Lict;->a:Lict;

    invoke-virtual {v3, v4}, Licq;->a(Lict;)V

    .line 1359
    :cond_2
    :goto_4
    invoke-virtual {p0}, Lehh;->af_()V

    .line 1361
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1364
    iput-boolean v8, p0, Lehh;->ai:Z

    .line 1366
    iput-boolean v7, p0, Lehh;->aG:Z

    .line 1367
    iput-boolean v8, p0, Lehh;->al:Z

    .line 1368
    iput-boolean v7, p0, Lehh;->an:Z

    .line 1369
    iput v7, p0, Lehh;->aS:I

    .line 1370
    iput v7, p0, Lehh;->aT:I

    .line 1371
    invoke-virtual {p0}, Lehh;->al()V

    .line 1372
    invoke-direct {p0}, Lehh;->X()V

    .line 1376
    :cond_3
    :goto_5
    invoke-direct {p0}, Lehh;->an()V

    .line 1378
    iget-object v0, p0, Lehh;->ap:Levp;

    if-eqz v0, :cond_4

    .line 1379
    iget-object v0, p0, Lehh;->ap:Levp;

    invoke-virtual {v0}, Levp;->c()V

    .line 1382
    :cond_4
    iput v9, p0, Lehh;->aL:I

    .line 1383
    invoke-virtual {p0}, Lehh;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v9, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto/16 :goto_0

    .line 1311
    :cond_5
    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    .line 1318
    :cond_6
    sget-object v0, Lhmw;->a:Lhmw;

    move-object v3, v0

    goto/16 :goto_2

    .line 1341
    :cond_7
    invoke-direct {p0}, Lehh;->ad()V

    .line 1342
    iput-object v2, p0, Lehh;->af:Landroid/location/Location;

    goto :goto_3

    .line 1354
    :cond_8
    invoke-virtual {p0, v3}, Lehh;->d(Landroid/view/View;)V

    goto :goto_4

    .line 1373
    :cond_9
    iget-boolean v0, p0, Lehh;->ai:Z

    if-eqz v0, :cond_3

    .line 1374
    invoke-virtual {p0}, Lehh;->al()V

    goto :goto_5
.end method

.method private a(Lhoz;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 684
    if-nez p1, :cond_1

    .line 713
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    invoke-virtual {p1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    .line 688
    const-string v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 689
    const-string v1, "moderation_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 691
    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Lhoz;->f()Z

    move-result v1

    if-nez v1, :cond_4

    .line 692
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, v2}, Lfdj;->d(Ljava/lang/String;)V

    .line 693
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_0

    .line 694
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_0

    .line 695
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 696
    instance-of v3, v0, Lgbz;

    if-eqz v3, :cond_3

    .line 697
    check-cast v0, Lgbz;

    .line 698
    invoke-virtual {v0}, Lgbz;->t()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 699
    invoke-virtual {v0}, Lgbz;->h()V

    goto :goto_0

    .line 701
    :cond_2
    if-nez v2, :cond_3

    .line 702
    invoke-virtual {v0}, Lgbz;->h()V

    .line 694
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 707
    :cond_4
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 710
    iput v3, p0, Lehh;->aS:I

    .line 711
    iput v3, p0, Lehh;->aT:I

    goto :goto_0
.end method

.method private a(Lhoz;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 542
    invoke-static {p1}, Lhoz;->a(Lhoz;)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->ak:Z

    .line 543
    const-string v0, "HostedStreamFrag"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 544
    iget-boolean v0, p0, Lehh;->ak:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onGetActivities - mError="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 546
    iget-boolean v0, p0, Lehh;->ak:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 547
    invoke-virtual {p1}, Lhoz;->toString()Ljava/lang/String;

    .line 551
    :cond_0
    if-eqz p2, :cond_4

    .line 552
    iget-boolean v0, p0, Lehh;->ak:Z

    if-nez v0, :cond_1

    .line 553
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lehh;->am:J

    .line 554
    iput-boolean v3, p0, Lehh;->an:Z

    .line 555
    iput-boolean v3, p0, Lehh;->aH:Z

    .line 561
    :cond_1
    :goto_0
    iput-boolean v3, p0, Lehh;->aG:Z

    .line 569
    iget-boolean v0, p0, Lehh;->ak:Z

    if-nez v0, :cond_3

    .line 570
    if-eqz p1, :cond_2

    .line 571
    invoke-virtual {p1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    .line 572
    const-string v1, "new_continuation_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lehh;->Y:Ljava/lang/String;

    .line 573
    const-string v1, "new_stream_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lehh;->aA:[B

    .line 574
    const-string v1, "new_server_timestamp"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lehh;->Z:J

    .line 577
    :cond_2
    invoke-virtual {p0}, Lehh;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 581
    :cond_3
    invoke-virtual {p0}, Lehh;->ay()V

    .line 582
    invoke-virtual {p0}, Lehh;->aD()V

    .line 583
    return-void

    .line 558
    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->aQ:Z

    .line 559
    iget-boolean v0, p0, Lehh;->ak:Z

    if-eqz v0, :cond_1

    .line 560
    iput-boolean v3, p0, Lehh;->ar:Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2574
    invoke-direct {p0, p2}, Lehh;->b(Ljava/lang/String;)V

    .line 2576
    new-instance v0, Lehl;

    invoke-direct {v0, p0, p1, p2}, Lehl;-><init>(Lehh;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lehh;->bf:Lehl;

    .line 2579
    iget-object v0, p0, Lehh;->bf:Lehl;

    const-wide/16 v2, 0xbb8

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 2581
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;ILhmv;)V
    .locals 7

    .prologue
    .line 1994
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 1996
    invoke-virtual {p0}, Lehh;->aB()V

    .line 1999
    :cond_0
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 2000
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v6

    new-instance v0, Lkyb;

    iget-object v1, p0, Lehh;->at:Llnl;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lkyb;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v6, v0}, Lhoc;->c(Lhny;)V

    .line 2003
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-direct {v1, v3, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 2005
    invoke-virtual {v1, p4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    const-string v2, "extra_activity_id"

    .line 2006
    invoke-virtual {v1, v2, p2}, Lhmr;->a(Ljava/lang/String;Ljava/lang/String;)Lhmr;

    move-result-object v1

    const-string v2, "extra_square_id"

    .line 2007
    invoke-virtual {v1, v2, p1}, Lhmr;->a(Ljava/lang/String;Ljava/lang/String;)Lhmr;

    move-result-object v1

    .line 2003
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2009
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2547
    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_0

    .line 2548
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, p1, p2}, Lfdj;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2550
    if-eqz v3, :cond_0

    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_0

    .line 2551
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_0

    .line 2552
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2553
    instance-of v0, v1, Llda;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 2554
    check-cast v0, Llch;

    .line 2555
    invoke-virtual {v0}, Llch;->t()Ljava/lang/String;

    move-result-object v0

    .line 2554
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2556
    check-cast v1, Llda;

    invoke-interface {v1}, Llda;->j()Llcu;

    move-result-object v0

    .line 2557
    if-eqz p3, :cond_1

    .line 2558
    iget-object v1, p0, Lehh;->W:Lfdj;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Lfdj;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2559
    invoke-virtual {v0, p2, v3}, Llcu;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2569
    :cond_0
    :goto_1
    return-void

    .line 2561
    :cond_1
    invoke-virtual {v0, v3}, Llcu;->a(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 2551
    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0
.end method

.method private aM()V
    .locals 2

    .prologue
    .line 3576
    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3577
    new-instance v0, Lkoe;

    const/16 v1, 0x49

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 3583
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->aI:Z

    .line 3584
    return-void

    .line 3578
    :cond_0
    invoke-direct {p0}, Lehh;->ap()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3579
    new-instance v0, Lkoe;

    const/16 v1, 0x3f

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 3581
    :cond_1
    new-instance v0, Lkoe;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private aN()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 3631
    iget-wide v0, p0, Lehh;->aK:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lehh;->an:Z

    if-eqz v0, :cond_0

    .line 3632
    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3633
    new-instance v0, Lkoe;

    const/16 v1, 0x4b

    iget-wide v2, p0, Lehh;->aK:J

    invoke-direct {v0, v1, v2, v3}, Lkoe;-><init>(IJ)V

    iget-object v1, p0, Lehh;->at:Llnl;

    .line 3634
    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 3635
    new-instance v0, Lkoe;

    const/16 v1, 0x4c

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehh;->at:Llnl;

    .line 3636
    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 3647
    :goto_0
    iput-wide v4, p0, Lehh;->aK:J

    .line 3648
    iget-object v0, p0, Lehh;->bj:Llfn;

    iget-object v1, p0, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "NewContentTooltipAfterResume"

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Llfn;->a(I[Ljava/lang/String;)V

    .line 3650
    :cond_0
    return-void

    .line 3637
    :cond_1
    invoke-direct {p0}, Lehh;->ap()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3638
    new-instance v0, Lkoe;

    const/16 v1, 0x41

    iget-wide v2, p0, Lehh;->aK:J

    invoke-direct {v0, v1, v2, v3}, Lkoe;-><init>(IJ)V

    iget-object v1, p0, Lehh;->at:Llnl;

    .line 3639
    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 3640
    new-instance v0, Lkoe;

    const/16 v1, 0x42

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehh;->at:Llnl;

    .line 3641
    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 3643
    :cond_2
    new-instance v0, Lkoe;

    const/16 v1, 0x39

    iget-wide v2, p0, Lehh;->aK:J

    invoke-direct {v0, v1, v2, v3}, Lkoe;-><init>(IJ)V

    iget-object v1, p0, Lehh;->at:Llnl;

    .line 3644
    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 3645
    new-instance v0, Lkoe;

    const/16 v1, 0x3a

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private aa()V
    .locals 2

    .prologue
    .line 1873
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v0

    const-string v1, "prefetch_newposts"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1874
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v0

    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1885
    :cond_0
    :goto_0
    return-void

    .line 1877
    :cond_1
    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->am()I

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lehh;->aE:Z

    if-nez v0, :cond_3

    .line 1878
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->aD:Z

    goto :goto_0

    .line 1881
    :cond_3
    invoke-static {}, Llay;->a()Llay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Llay;->a(Z)V

    .line 1882
    invoke-virtual {p0}, Lehh;->ax()Lhny;

    move-result-object v0

    .line 1883
    const-string v1, "prefetch_newposts"

    invoke-virtual {v0, v1}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 1884
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    goto :goto_0
.end method

.method private ad()V
    .locals 1

    .prologue
    .line 1948
    iget-object v0, p0, Lehh;->ah:Liug;

    if-eqz v0, :cond_0

    .line 1949
    iget-object v0, p0, Lehh;->ah:Liug;

    invoke-virtual {v0}, Liug;->c()V

    .line 1950
    const/4 v0, 0x0

    iput-object v0, p0, Lehh;->ah:Liug;

    .line 1952
    :cond_0
    return-void
.end method

.method private ae()V
    .locals 3

    .prologue
    .line 1959
    iget-object v0, p0, Lehh;->af:Landroid/location/Location;

    if-nez v0, :cond_3

    .line 1960
    const v0, 0x7f0a02be

    invoke-virtual {p0, v0}, Lehh;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 1970
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1971
    iget-object v1, p0, Lehh;->aB:Landroid/widget/Toast;

    if-eqz v1, :cond_1

    .line 1972
    iget-object v1, p0, Lehh;->aB:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 1975
    :cond_1
    iget-object v1, p0, Lehh;->at:Llnl;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Llih;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lehh;->aB:Landroid/widget/Toast;

    .line 1976
    iget-object v0, p0, Lehh;->aB:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1978
    :cond_2
    return-void

    .line 1962
    :cond_3
    iget-object v0, p0, Lehh;->af:Landroid/location/Location;

    invoke-static {v0}, Lfcw;->b(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    .line 1963
    if-nez v0, :cond_0

    .line 1966
    const v0, 0x7f0a07d6

    invoke-virtual {p0, v0}, Lehh;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private af()Z
    .locals 2

    .prologue
    .line 2124
    invoke-virtual {p0}, Lehh;->q()Lae;

    move-result-object v0

    const-string v1, "HOSTEDSTREAMONEUP_FRAGMENT"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ag()Z
    .locals 2

    .prologue
    .line 2128
    invoke-virtual {p0}, Lehh;->q()Lae;

    move-result-object v0

    .line 2129
    invoke-virtual {v0}, Lae;->e()I

    move-result v1

    .line 2130
    if-lez v1, :cond_0

    .line 2131
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lae;->b(I)Laf;

    move-result-object v0

    .line 2132
    invoke-interface {v0}, Laf;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HOSTEDSTREAMONEUP_FRAGMENT"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 2134
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ah()Lehu;
    .locals 2

    .prologue
    .line 2138
    invoke-virtual {p0}, Lehh;->q()Lae;

    move-result-object v0

    .line 2139
    const-string v1, "HOSTEDSTREAMONEUP_FRAGMENT"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    .line 2140
    instance-of v1, v0, Lehu;

    if-eqz v1, :cond_0

    .line 2141
    check-cast v0, Lehu;

    .line 2144
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ai()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2300
    iget-object v1, p0, Lehh;->W:Lfdj;

    if-eqz v1, :cond_0

    .line 2301
    iget-object v1, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a()I

    move-result v1

    .line 2302
    iget-object v2, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2303
    if-eqz v2, :cond_0

    .line 2304
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v1, v0, v1

    .line 2305
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lldx;

    iget v0, v0, Lldx;->topMargin:I

    sub-int v0, v1, v0

    .line 2310
    :cond_0
    return v0
.end method

.method private am()V
    .locals 2

    .prologue
    .line 2421
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    .line 2422
    if-eqz v0, :cond_0

    .line 2423
    iget-object v1, p0, Lehh;->U:Licq;

    iget-boolean v0, p0, Lehh;->ae:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0a02be

    :goto_0
    invoke-virtual {v1, v0}, Licq;->b(I)Licq;

    .line 2425
    :cond_0
    return-void

    .line 2423
    :cond_1
    const v0, 0x7f0a057b

    goto :goto_0
.end method

.method private an()V
    .locals 2

    .prologue
    .line 3269
    iget-object v0, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    if-nez v0, :cond_0

    .line 3283
    :goto_0
    return-void

    .line 3273
    :cond_0
    iget-boolean v0, p0, Lehh;->an:Z

    if-eqz v0, :cond_1

    .line 3274
    iget-object v0, p0, Lehh;->aN:Llhj;

    invoke-virtual {v0}, Llhj;->a()V

    .line 3275
    iget-object v0, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    new-instance v1, Lhmi;

    invoke-direct {v1, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3276
    iget-object v0, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    invoke-static {v0}, Lhmc;->a(Landroid/view/View;)V

    goto :goto_0

    .line 3278
    :cond_1
    iget-object v0, p0, Lehh;->aN:Llhj;

    invoke-virtual {v0}, Llhj;->d()V

    .line 3279
    iget-object v0, p0, Lehh;->aN:Llhj;

    invoke-virtual {v0}, Llhj;->b()V

    .line 3280
    iget-object v0, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3281
    iget-object v0, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    invoke-static {v0}, Lhmc;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method private ap()Z
    .locals 2

    .prologue
    .line 3562
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lkzl;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iget-object v1, p0, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-interface {v0, v1}, Lkzl;->g(I)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 3013
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3014
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3015
    const-string v1, "person_name"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3016
    const-string v1, "for_sharing"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3017
    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3018
    const-string v1, "circle_name"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3019
    const-string v1, "suggestion_id"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3020
    const-string v1, "activity_id"

    invoke-virtual {v0, v1, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3021
    const-string v1, "promo_type"

    invoke-virtual {v0, v1, p8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3022
    return-object v0
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 3486
    iget-object v0, p0, Lehh;->ac:Levm;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lehh;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lehh;->as()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lehh;->ac:Levm;

    .line 3487
    invoke-virtual {v0}, Levm;->g()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 3507
    :cond_0
    :goto_0
    return-void

    .line 3491
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3493
    :cond_2
    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3496
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3500
    :cond_3
    new-instance v1, Levm;

    iget-object v2, p0, Lehh;->at:Llnl;

    iget-object v0, p0, Lehh;->at:Llnl;

    const v3, 0x7f0a06fa

    .line 3501
    invoke-virtual {v0, v3}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "v.all.circles"

    const-wide/16 v8, 0x0

    move v6, v5

    move v7, v5

    invoke-direct/range {v1 .. v9}, Levm;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZIJ)V

    .line 3500
    invoke-direct {p0, v1}, Lehh;->a(Levm;)V

    goto :goto_0
.end method

.method static synthetic b(Lehh;)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0}, Lehh;->am()V

    return-void
.end method

.method private b(Lhoz;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 716
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p1}, Lhoz;->b()Ljava/lang/Exception;

    move-result-object v0

    if-nez v0, :cond_8

    .line 717
    invoke-virtual {p1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v6

    .line 718
    const-string v0, "extra_activity_id"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 720
    const-string v0, "extra_person_id"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 721
    invoke-static {v5}, Llcl;->b(Ljava/lang/String;)I

    move-result v7

    .line 722
    const-string v0, "extra_selected_circles"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 724
    iget-boolean v0, p0, Lehh;->be:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    if-eq v7, v0, :cond_0

    const/4 v0, 0x2

    if-ne v7, v0, :cond_5

    .line 728
    :cond_0
    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_0
    if-ltz v4, :cond_4

    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lfzf;

    if-eqz v1, :cond_2

    check-cast v0, Lfzf;

    invoke-virtual {v0}, Lfzf;->t()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    if-eqz v8, :cond_1

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhxc;

    invoke-virtual {v1}, Lhxc;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-virtual {v1, v3, v9}, Lfdj;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Lfzf;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_2
    if-ltz v2, :cond_2

    invoke-virtual {v0, v2}, Lfzf;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lgbl;

    if-eqz v1, :cond_3

    invoke-virtual {v0, v2}, Lfzf;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lgbl;

    invoke-virtual {v1}, Lgbl;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v1, v9}, Lgbl;->a(Ljava/util/List;)V

    :cond_2
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_2

    .line 729
    :cond_4
    invoke-direct {p0, v5, v3}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    :goto_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 736
    const-string v0, "extra_added_circles"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 738
    if-eqz v2, :cond_6

    .line 739
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_4
    if-ltz v1, :cond_6

    .line 740
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxc;

    invoke-virtual {v0}, Lhxc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 739
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_4

    .line 731
    :cond_5
    invoke-direct {p0, v5, v3, v11}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_3

    .line 743
    :cond_6
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 744
    const-string v0, "extra_removed_circles"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 746
    if-eqz v2, :cond_7

    .line 747
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_5
    if-ltz v1, :cond_7

    .line 748
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxc;

    invoke-virtual {v0}, Lhxc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 747
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_5

    .line 751
    :cond_7
    invoke-virtual {p0, v7, v11}, Lehh;->a(II)Ldid;

    move-result-object v6

    .line 754
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v7

    .line 755
    new-instance v0, Ldib;

    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v2, p0, Lehh;->au:Llnh;

    const-class v8, Lhms;

    invoke-virtual {v2, v8}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhms;

    invoke-direct/range {v0 .. v6}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    .line 756
    invoke-virtual {v0, v7}, Ldib;->a(I)V

    .line 758
    :cond_8
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2584
    iget-object v0, p0, Lehh;->bf:Lehl;

    if-eqz v0, :cond_1

    .line 2585
    iget-object v0, p0, Lehh;->bf:Lehl;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2586
    iget-object v0, p0, Lehh;->bf:Lehl;

    invoke-virtual {v0}, Lehl;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2587
    iget-object v0, p0, Lehh;->bf:Lehl;

    invoke-virtual {v0}, Lehl;->run()V

    .line 2589
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lehh;->bf:Lehl;

    .line 2591
    :cond_1
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 2705
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 2706
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-direct {v1, v3, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v3, Lhmv;->C:Lhmv;

    .line 2708
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2709
    invoke-static {p3}, Llcl;->a(I)I

    move-result v3

    invoke-direct {p0, v3}, Lehh;->c(I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2706
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2711
    iget-object v0, p0, Lehh;->aY:Liax;

    iget-object v1, p0, Lehh;->at:Llnl;

    const/16 v5, 0x12

    .line 2712
    invoke-virtual {p0}, Lehh;->a()I

    move-result v6

    move-object v3, p1

    move-object v4, p2

    .line 2711
    invoke-interface/range {v0 .. v6}, Liax;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II)V

    .line 2713
    return-void
.end method

.method private c(I)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 2905
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2906
    const-string v1, "extra_promo_group_id"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2907
    const-string v1, "extra_promo_type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2908
    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 3571
    iget-object v0, p0, Lehh;->bj:Llfn;

    iget-object v1, p0, Lehh;->R:Lhee;

    .line 3572
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-wide/16 v2, 0x2

    .line 3571
    invoke-interface {v0, v1, v2, v3, p1}, Llfn;->a(IJLjava/lang/String;)V

    .line 3573
    return-void
.end method

.method static synthetic c(Lehh;)Z
    .locals 1

    .prologue
    .line 206
    invoke-direct {p0}, Lehh;->ap()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lehh;)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0}, Lehh;->aa()V

    return-void
.end method

.method static synthetic e(Lehh;)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0}, Lehh;->aN()V

    return-void
.end method

.method static synthetic f(Lehh;)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0}, Lehh;->an()V

    return-void
.end method

.method static synthetic g(Lehh;)Llnl;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lehh;->at:Llnl;

    return-object v0
.end method

.method static synthetic h(Lehh;)Llnl;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lehh;->at:Llnl;

    return-object v0
.end method

.method static synthetic i(Lehh;)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0}, Lehh;->ae()V

    return-void
.end method

.method static synthetic j(Lehh;)Llnl;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lehh;->at:Llnl;

    return-object v0
.end method

.method static synthetic k(Lehh;)Llnl;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lehh;->at:Llnl;

    return-object v0
.end method

.method static synthetic l(Lehh;)Z
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, Lehh;->be:Z

    return v0
.end method

.method static synthetic m(Lehh;)Llnl;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lehh;->at:Llnl;

    return-object v0
.end method

.method static synthetic n(Lehh;)Llnl;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lehh;->at:Llnl;

    return-object v0
.end method

.method static synthetic o(Lehh;)Llnl;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lehh;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 2253
    sget-object v0, Lhmw;->a:Lhmw;

    return-object v0
.end method

.method public K_()V
    .locals 6

    .prologue
    .line 3324
    invoke-direct {p0}, Lehh;->aM()V

    .line 3325
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ar:Lhmv;

    .line 3326
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 3325
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 3327
    iget-object v0, p0, Lehh;->bj:Llfn;

    iget-object v1, p0, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "PullToRefresh"

    const-wide/16 v4, 0x3

    invoke-interface {v0, v1, v2, v4, v5}, Llfn;->a(ILjava/lang/String;J)V

    .line 3329
    iget-object v0, p0, Lehh;->az:Lkci;

    invoke-virtual {v0}, Lkci;->c()V

    .line 3330
    invoke-virtual {p0}, Lehh;->al()V

    .line 3331
    return-void
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 946
    const/4 v0, 0x1

    return v0
.end method

.method public V()Z
    .locals 2

    .prologue
    .line 1303
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 3511
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->e()Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 3292
    const/4 v0, 0x1

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 3340
    const/16 v0, 0x5d

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    .line 953
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 954
    const v1, 0x7f040213

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 955
    invoke-virtual {v10}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 956
    invoke-virtual {p0}, Lehh;->n()Lz;

    move-result-object v9

    .line 958
    sget v0, Lehh;->O:I

    if-nez v0, :cond_0

    .line 959
    invoke-static {v9}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v0}, Llsc;->b(Landroid/util/DisplayMetrics;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 960
    const/16 v0, 0x8

    sput v0, Lehh;->O:I

    .line 966
    :cond_0
    :goto_0
    new-instance v3, Llcr;

    invoke-direct {v3, v1}, Llcr;-><init>(Landroid/content/Context;)V

    .line 967
    const v0, 0x7f100304

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iput-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    .line 968
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget v2, v3, Llcr;->f:I

    const/4 v4, 0x0

    iget v5, v3, Llcr;->f:I

    const/4 v6, 0x0

    invoke-virtual {v0, v2, v4, v5, v6}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->setPadding(IIII)V

    .line 969
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {p0, v3}, Lehh;->a(Llcr;)Lldv;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Lldv;)V

    .line 971
    invoke-virtual {p0}, Lehh;->U()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 972
    const v0, 0x7f10030c

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 973
    new-instance v2, Levp;

    invoke-direct {v2, v0}, Levp;-><init>(Landroid/view/View;)V

    iput-object v2, p0, Lehh;->ap:Levp;

    .line 974
    new-instance v2, Lhmk;

    sget-object v4, Lonj;->g:Lhmn;

    invoke-direct {v2, v4}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 975
    iget-object v0, p0, Lehh;->ap:Levp;

    new-instance v2, Lhmi;

    invoke-direct {v2, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Levp;->a(Landroid/view/View$OnClickListener;)V

    .line 980
    :goto_1
    invoke-static {v1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    iget v1, v0, Llct;->aB:I

    .line 982
    const v0, 0x7f1003a9

    .line 983
    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    iput-object v0, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    .line 984
    iget-object v0, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const v2, 0x7f0a0ad8

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a(I)V

    .line 985
    iget-object v0, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const v2, 0x7f0a0ad8

    invoke-virtual {p0, v2}, Lehh;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 986
    iget-object v0, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v4, v5, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a(IIII)V

    .line 987
    new-instance v0, Llhj;

    iget-object v2, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const/4 v4, 0x0

    invoke-direct {v0, v2, v4}, Llhj;-><init>(Landroid/view/View;Z)V

    iput-object v0, p0, Lehh;->aN:Llhj;

    .line 990
    const v0, 0x7f10031b

    .line 991
    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    iput-object v0, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    .line 992
    iget-object v0, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const v2, 0x7f0a0590

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a(I)V

    .line 993
    iget-object v0, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const v2, 0x7f0a0590

    invoke-virtual {p0, v2}, Lehh;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 994
    iget-object v0, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v1, v4}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a(IIII)V

    .line 995
    iget-object v0, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a(Landroid/text/Layout$Alignment;)V

    .line 996
    new-instance v0, Llhj;

    iget-object v1, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const/4 v2, 0x1

    const-wide/16 v4, 0x1c2

    invoke-direct {v0, v1, v2, v4, v5}, Llhj;-><init>(Landroid/view/View;ZJ)V

    iput-object v0, p0, Lehh;->aP:Llhj;

    .line 999
    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v2, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v0, p0, Lehh;->R:Lhee;

    .line 1000
    invoke-interface {v0}, Lhee;->d()I

    move-result v4

    iget-object v6, p0, Lehh;->aZ:Lfdp;

    iget-object v7, p0, Lehh;->ap:Levp;

    move-object v0, p0

    move-object v5, p0

    move-object v8, p0

    .line 999
    invoke-virtual/range {v0 .. v8}, Lehh;->a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;

    move-result-object v0

    iput-object v0, p0, Lehh;->W:Lfdj;

    .line 1002
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {p0}, Lehh;->aI()Z

    move-result v1

    invoke-virtual {v0, v1}, Lfdj;->e(Z)V

    .line 1003
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, p0}, Lfdj;->a(Llhc;)V

    .line 1004
    if-eqz p3, :cond_1

    const-string v0, "stream_hash_activity_ids"

    .line 1005
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1006
    const-string v0, "stream_hash_activity_ids"

    .line 1007
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1008
    const-string v1, "stream_restore_position"

    const/4 v2, -0x1

    invoke-virtual {p3, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1010
    iget-object v2, p0, Lehh;->W:Lfdj;

    invoke-virtual {v2, v0}, Lfdj;->a(Ljava/util/ArrayList;)V

    .line 1011
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, v1}, Lfdj;->i(I)V

    .line 1013
    :cond_1
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Landroid/widget/ListAdapter;)V

    .line 1015
    iget-object v0, p0, Lehh;->S:Llhd;

    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, v1}, Llhd;->a(Llhc;)V

    .line 1016
    iget-object v0, p0, Lehh;->S:Llhd;

    invoke-virtual {v0, p0}, Llhd;->a(Llhc;)V

    .line 1018
    const v0, 0x7f1005e9

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lehh;->aj:Landroid/view/View;

    .line 1019
    const v0, 0x7f1005eb

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lehh;->aC:Landroid/widget/Button;

    .line 1020
    iget-object v0, p0, Lehh;->aC:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1022
    iget-object v0, p0, Lehh;->U:Licq;

    new-instance v1, Lehi;

    invoke-direct {v1, p0}, Lehi;-><init>(Lehh;)V

    invoke-virtual {v0, v1}, Licq;->a(Lico;)Licq;

    .line 1037
    invoke-virtual {p0}, Lehh;->aq()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lehh;->az()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1038
    iget-object v0, p0, Lehh;->U:Licq;

    const v1, 0x7f0a06f6

    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    .line 1039
    iget-object v0, p0, Lehh;->U:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 1042
    :cond_2
    invoke-virtual {p0}, Lehh;->aD()V

    .line 1043
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Llim;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llim;

    .line 1044
    if-eqz v0, :cond_3

    invoke-interface {v0}, Llim;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1045
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Llik;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llik;

    .line 1046
    if-eqz v0, :cond_3

    .line 1047
    invoke-interface {v0}, Llik;->a()Llij;

    move-result-object v0

    .line 1048
    iget-object v1, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v1, p0, Lehh;->W:Lfdj;

    .line 1049
    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-virtual {v1, v0}, Lfdj;->a(Llij;)V

    .line 1052
    :cond_3
    if-eqz p3, :cond_4

    .line 1053
    const-string v0, "popup_invisible_activity_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1054
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1055
    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-virtual {v1, v0}, Lfdj;->g(Ljava/lang/String;)V

    .line 1059
    :cond_4
    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-direct {p0}, Lehh;->ag()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Lfdj;->d(Z)V

    .line 1060
    invoke-virtual {p0}, Lehh;->q()Lae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lae;->a(Lag;)V

    .line 1062
    instance-of v0, v9, Ljgf;

    if-eqz v0, :cond_5

    move-object v0, v9

    .line 1063
    check-cast v0, Ljgf;

    iput-object v0, p0, Lehh;->ad:Ljgf;

    .line 1064
    invoke-direct {p0}, Lehh;->X()V

    .line 1067
    :cond_5
    invoke-direct {p0}, Lehh;->ah()Lehu;

    move-result-object v0

    .line 1068
    if-eqz v0, :cond_6

    .line 1069
    invoke-direct {p0, v0}, Lehh;->a(Lehu;)V

    .line 1072
    :cond_6
    invoke-direct {p0}, Lehh;->ag()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1073
    const v0, 0x7f1005f0

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1076
    :cond_7
    return-object v10

    .line 962
    :cond_8
    const/4 v0, 0x6

    sput v0, Lehh;->O:I

    goto/16 :goto_0

    .line 977
    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Lehh;->ap:Levp;

    goto/16 :goto_1

    .line 1059
    :cond_a
    const/4 v0, 0x0

    goto :goto_2
.end method

.method protected a(II)Ldid;
    .locals 2

    .prologue
    .line 3334
    .line 3335
    invoke-virtual {p0}, Lehh;->a()I

    move-result v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 3334
    invoke-static {p1, v0, v1}, Ldib;->a(IILjava/lang/Integer;)Ldid;

    move-result-object v0

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x2

    .line 1400
    const-string v1, "HostedStreamFrag"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1401
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x38

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "HostedStreamFrag onCreateLoader for loaderId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1404
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1436
    :goto_0
    return-object v0

    .line 1406
    :pswitch_0
    new-instance v0, Ldzg;

    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v2, p0, Lehh;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    sget-object v3, Lehh;->N:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v5, v3}, Ldzg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    goto :goto_0

    .line 1411
    :pswitch_1
    iget-object v0, p0, Lehh;->bc:Lita;

    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v1, p0, Lehh;->R:Lhee;

    .line 1412
    invoke-interface {v1}, Lhee;->d()I

    invoke-interface {v0}, Lita;->b()Lhye;

    move-result-object v0

    goto :goto_0

    .line 1418
    :pswitch_2
    if-ne p1, v3, :cond_2

    .line 1419
    const-string v6, "1"

    .line 1426
    :goto_1
    if-eq p1, v3, :cond_1

    iget-object v0, p0, Lehh;->aa:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1427
    invoke-virtual {p0}, Lehh;->ar()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v5, 0x1

    .line 1428
    :cond_1
    if-ne p1, v3, :cond_4

    sget-object v3, Lehk;->a:[Ljava/lang/String;

    .line 1431
    :goto_2
    new-instance v0, Llbb;

    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v2, p0, Lehh;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v4, p0, Lehh;->ag:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Llbb;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 1420
    :cond_2
    invoke-virtual {p0}, Lehh;->aq()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1421
    const-string v6, "0"

    goto :goto_1

    :cond_3
    move-object v6, v0

    .line 1423
    goto :goto_1

    .line 1428
    :cond_4
    sget-object v3, Llbf;->a:[Ljava/lang/String;

    goto :goto_2

    .line 1404
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;
    .locals 9

    .prologue
    .line 934
    new-instance v0, Lfdj;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lfdj;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)V

    .line 937
    return-object v0
.end method

.method protected a(Ljava/lang/String;[B)Lhny;
    .locals 15

    .prologue
    .line 1889
    invoke-virtual {p0}, Lehh;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pinned_activity_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 1890
    iget-object v0, p0, Lehh;->at:Llnl;

    iget-object v1, p0, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lehh;->ac:Levm;

    .line 1891
    invoke-virtual {v2}, Levm;->d()I

    move-result v2

    iget-object v3, p0, Lehh;->ac:Levm;

    invoke-virtual {v3}, Levm;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lehh;->aa:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lehh;->W:Lfdj;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lehh;->W:Lfdj;

    .line 1892
    invoke-virtual {v6}, Lfdj;->aj()[Ljava/lang/String;

    move-result-object v8

    :goto_0
    const/4 v10, 0x0

    const/4 v11, 0x0

    iget-wide v12, p0, Lehh;->Z:J

    const/4 v14, 0x0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    .line 1890
    invoke-static/range {v0 .. v14}, Ldov;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Ljava/lang/String;[Ljava/lang/String;ZZJ[Ljava/lang/String;)Ldov;

    move-result-object v0

    return-object v0

    .line 1892
    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method

.method protected a(Llcr;)Lldv;
    .locals 2

    .prologue
    .line 3515
    new-instance v0, Lldv;

    invoke-direct {v0}, Lldv;-><init>()V

    .line 3516
    iget v1, p1, Llcr;->a:I

    iput v1, v0, Lldv;->a:I

    .line 3517
    iget v1, p1, Llcr;->d:I

    iput v1, v0, Lldv;->b:I

    .line 3518
    iget v1, p1, Llcr;->c:I

    div-int/lit8 v1, v1, 0xa

    iput v1, v0, Lldv;->c:I

    .line 3519
    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v6, -0x1

    .line 2338
    iget-object v0, p0, Lehh;->at:Llnl;

    const-class v1, Litj;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Litj;

    .line 2339
    invoke-interface {v0, p1, p2}, Litj;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2403
    :cond_0
    :goto_0
    return-void

    .line 2343
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 2396
    invoke-direct {p0}, Lehh;->ah()Lehu;

    move-result-object v0

    .line 2397
    if-eqz v0, :cond_0

    .line 2398
    invoke-virtual {v0, p1, p2, p3}, Lehu;->a(IILandroid/content/Intent;)V

    goto :goto_0

    .line 2345
    :pswitch_0
    if-ne p2, v6, :cond_0

    if-eqz p3, :cond_0

    .line 2347
    const-string v0, "shareables"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2349
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2350
    iget-object v1, p0, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 2351
    iget-object v2, p0, Lehh;->at:Llnl;

    .line 2352
    const/4 v3, 0x0

    invoke-static {v2, v1, v0, v3}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;Lhgw;)Landroid/content/Intent;

    move-result-object v0

    .line 2353
    invoke-virtual {p0, v0}, Lehh;->d(Landroid/content/Intent;)V

    goto :goto_0

    .line 2360
    :pswitch_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 2361
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0}, Ldyj;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 2367
    :pswitch_2
    if-ne p2, v6, :cond_2

    .line 2368
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2369
    const-string v1, "original_circle_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2371
    const-string v2, "selected_circle_ids"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 2373
    const-string v3, "activity_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2374
    const-string v3, "person_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2375
    const-string v4, "promo_type"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 2376
    const-string v4, "display_name"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2377
    const-string v6, "suggestion_id"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2378
    const/4 v8, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v8}, Lehh;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    .line 2381
    :cond_2
    if-eqz p3, :cond_0

    .line 2382
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2383
    const-string v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2384
    const-string v2, "person_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2385
    const-string v3, "original_circle_ids"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2387
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2388
    invoke-direct {p0, v1, v2}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2343
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3054
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3058
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 848
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 850
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v0

    .line 851
    new-instance v1, Lhpf;

    iget-object v2, p0, Lehh;->at:Llnl;

    .line 852
    invoke-virtual {p0}, Lehh;->p()Lae;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lhpf;-><init>(Landroid/content/Context;Lae;)V

    .line 853
    invoke-virtual {v1, p0, v6, v5}, Lhos;->a(Lu;Ljava/lang/String;Z)V

    .line 854
    invoke-virtual {v0, v1}, Lhoc;->a(Lhos;)V

    .line 856
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 858
    new-instance v0, Lehp;

    invoke-direct {v0, p0}, Lehp;-><init>(Lehh;)V

    iput-object v0, p0, Lehh;->aZ:Lfdp;

    .line 859
    if-eqz p1, :cond_4

    .line 860
    const-string v0, "is_fetching_stream"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->aG:Z

    .line 861
    const-string v0, "fetching_newer_stream"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->aH:Z

    .line 863
    const-string v0, "scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lehh;->aS:I

    .line 864
    const-string v0, "scroll_off"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lehh;->aT:I

    .line 866
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lehh;->af:Landroid/location/Location;

    .line 868
    const-string v0, "last_deactivation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lehh;->aR:J

    .line 869
    const-string v0, "error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->ak:Z

    .line 870
    const-string v0, "reset_animation"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->al:Z

    .line 872
    const-string v0, "stream_change"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lehh;->am:J

    .line 874
    const-string v0, "notifications_change"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lehh;->aF:J

    .line 877
    const-string v0, "stream_change_flag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->an:Z

    .line 878
    const-string v0, "stream_change_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 879
    const-string v0, "stream_change_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehh;->ao:Ljava/lang/Integer;

    .line 883
    :cond_0
    const-string v0, "subscribe_visible"

    .line 884
    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->aU:Z

    .line 885
    const-string v0, "subscribe_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lehh;->aV:I

    .line 886
    const-string v0, "subscribe_icon"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lehh;->aW:I

    .line 888
    const-string v0, "stream_next_sequenced_loader_id"

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lehh;->aL:I

    .line 890
    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v2, Lhms;

    .line 891
    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    .line 890
    invoke-static {v1, v0, p1}, Ldib;->a(Landroid/content/Context;Lhms;Landroid/os/Bundle;)Ldib;

    move-result-object v0

    iput-object v0, p0, Lehh;->aX:Ldib;

    .line 892
    const-string v0, "circle_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 893
    const-string v0, "circle_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Levm;

    iput-object v0, p0, Lehh;->ac:Levm;

    .line 895
    :cond_1
    const-string v0, "first_load"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->ai:Z

    .line 904
    :goto_0
    invoke-virtual {p0}, Lehh;->af_()V

    .line 910
    invoke-virtual {p0}, Lehh;->ad_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 911
    invoke-virtual {p0}, Lehh;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v5, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 914
    :cond_2
    iget-object v0, p0, Lehh;->bc:Lita;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lehh;->bc:Lita;

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-interface {v0}, Lita;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 915
    invoke-virtual {p0}, Lehh;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 917
    :cond_3
    return-void

    .line 897
    :cond_4
    iput-boolean v4, p0, Lehh;->aG:Z

    .line 899
    iput v4, p0, Lehh;->aS:I

    .line 900
    iput v4, p0, Lehh;->aT:I

    .line 903
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Llim;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 3029
    const-string v0, "first_circle_add"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "first_circle_add_one_click"

    .line 3030
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3031
    :cond_0
    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 3032
    const-string v0, "person_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 3033
    const-string v0, "for_sharing"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 3034
    const-string v0, "circle_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 3035
    const-string v0, "circle_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3036
    const-string v0, "suggestion_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3037
    const-string v0, "activity_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 3038
    const-string v0, "promo_type"

    const/4 v8, -0x1

    invoke-virtual {p1, v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    move-object v0, p0

    .line 3039
    invoke-virtual/range {v0 .. v8}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 3042
    :cond_1
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 3258
    iget-object v0, p0, Lehh;->aN:Llhj;

    invoke-virtual {v0, p1, p2}, Llhj;->a(Landroid/view/View;I)V

    .line 3259
    iget-object v0, p0, Lehh;->aP:Llhj;

    invoke-virtual {v0, p1, p2}, Llhj;->a(Landroid/view/View;I)V

    .line 3260
    return-void
.end method

.method public a(Landroid/view/View;III)V
    .locals 1

    .prologue
    .line 3264
    iget-object v0, p0, Lehh;->aN:Llhj;

    invoke-virtual {v0, p1, p2, p3, p4}, Llhj;->a(Landroid/view/View;III)V

    .line 3265
    iget-object v0, p0, Lehh;->aP:Llhj;

    invoke-virtual {v0, p1, p2, p3, p4}, Llhj;->a(Landroid/view/View;III)V

    .line 3266
    return-void
.end method

.method protected a(Landroid/view/View;Lhtx;Z)V
    .locals 8

    .prologue
    const v7, 0x7f1005f0

    const/4 v1, 0x0

    .line 2021
    invoke-interface {p2, p3}, Lhtx;->b(Z)Landroid/content/Intent;

    move-result-object v2

    .line 2022
    if-nez v2, :cond_1

    .line 2070
    :cond_0
    :goto_0
    return-void

    .line 2026
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lehh;->X:I

    .line 2027
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_2

    .line 2028
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_2

    .line 2029
    iget-object v3, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2030
    iget-object v3, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c()I

    move-result v3

    add-int/2addr v0, v3

    iput v0, p0, Lehh;->X:I

    .line 2036
    :cond_2
    const-string v0, "event_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "owner_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2037
    const-string v0, "event_id"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2038
    const-string v3, "owner_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2039
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2040
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lehh;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->aR:Lhmv;

    .line 2041
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 2040
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 2056
    :cond_3
    :goto_2
    invoke-virtual {p0, v2}, Lehh;->e(Landroid/content/Intent;)V

    .line 2060
    sget-object v0, Lehh;->ay:Llpa;

    .line 2061
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lehh;->at:Llnl;

    .line 2062
    invoke-static {v0}, Llsc;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2063
    const-string v0, "activity_id"

    invoke-interface {p2}, Lhtx;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2064
    invoke-direct {p0}, Lehh;->af()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0}, Llsc;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HostStreamOneUpActivity;->b(I)Lu;

    move-result-object v0

    check-cast v0, Lehu;

    iput-object v0, p0, Lehh;->ax:Lehu;

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    iget-object v6, p0, Lehh;->ax:Lehu;

    invoke-virtual {v6}, Lehu;->k()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    const-string v2, "popup_start_x"

    invoke-virtual {v6, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "popup_start_y"

    invoke-virtual {v6, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "popup_start_width"

    invoke-virtual {v6, v0, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "popup_start_height"

    invoke-virtual {v6, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lehh;->ax:Lehu;

    invoke-direct {p0, v0}, Lehh;->a(Lehu;)V

    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lehh;->q()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v1

    iget-object v2, p0, Lehh;->ax:Lehu;

    const-string v3, "HOSTEDSTREAMONEUP_FRAGMENT"

    invoke-virtual {v1, v7, v2, v3}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    move-result-object v1

    const-string v2, "HOSTEDSTREAMONEUP_FRAGMENT"

    invoke-virtual {v1, v2}, Lat;->a(Ljava/lang/String;)Lat;

    move-result-object v1

    invoke-virtual {v1}, Lat;->b()I

    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-interface {p2}, Lhtx;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lfdj;->a(Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {v0}, Lae;->b()Z

    goto/16 :goto_0

    .line 2028
    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto/16 :goto_1

    .line 2043
    :cond_5
    const-string v0, "photo_ref"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2044
    const-string v0, "photo_ref"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    .line 2045
    if-eqz v0, :cond_3

    .line 2046
    invoke-virtual {v0}, Lizu;->b()Ljava/lang/String;

    move-result-object v0

    .line 2047
    const-string v3, "extra_gaia_id"

    invoke-static {v3, v0}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 2049
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, v5}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v5, Lhmv;->aa:Lhmv;

    .line 2050
    invoke-virtual {v4, v5}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v4

    .line 2051
    invoke-virtual {v4, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v3

    .line 2049
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    goto/16 :goto_2

    .line 2064
    :cond_6
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    goto/16 :goto_3

    .line 2066
    :cond_7
    const-string v0, "refresh"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2067
    invoke-virtual {p0, v2}, Lehh;->a(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1596
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1447
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    .line 1448
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onLoadFinished:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lehh;->c(Ljava/lang/String;)V

    .line 1449
    const-string v3, "HostedStreamFrag"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1450
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x38

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "HostedStreamFrag onLoadFinished for loaderId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1453
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 1589
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 1455
    :pswitch_1
    invoke-direct {p0, p2}, Lehh;->b(Landroid/database/Cursor;)V

    .line 1456
    iput-object p2, p0, Lehh;->as:Landroid/database/Cursor;

    goto :goto_0

    .line 1466
    :pswitch_2
    iget v3, p0, Lehh;->aL:I

    if-lt v3, v0, :cond_1

    .line 1467
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1470
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehh;->Y:Ljava/lang/String;

    .line 1472
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    iput-object v0, p0, Lehh;->aA:[B

    .line 1473
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lehh;->Z:J

    .line 1479
    :goto_1
    iput v7, p0, Lehh;->aL:I

    .line 1480
    invoke-virtual {p0}, Lehh;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v7, v5, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 1475
    :cond_2
    iput-object v5, p0, Lehh;->Y:Ljava/lang/String;

    .line 1476
    iput-object v5, p0, Lehh;->aA:[B

    .line 1477
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lehh;->Z:J

    goto :goto_1

    .line 1485
    :pswitch_3
    iget v3, p0, Lehh;->aL:I

    if-lt v3, v0, :cond_1

    .line 1486
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->getCount()I

    move-result v3

    .line 1490
    iget-boolean v0, p0, Lehh;->an:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lehh;->aE:Z

    if-eqz v0, :cond_4

    .line 1493
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1494
    const/16 v0, 0x24

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1496
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->al()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lehh;->W:Lfdj;

    .line 1497
    invoke-virtual {v0}, Lfdj;->al()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1498
    :cond_3
    iput-boolean v1, p0, Lehh;->an:Z

    .line 1499
    invoke-direct {p0}, Lehh;->an()V

    .line 1504
    :cond_4
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {p0}, Lehh;->ak()Lhym;

    move-result-object v4

    invoke-virtual {v0, v4}, Lfdj;->a(Landroid/database/Cursor;)V

    .line 1505
    iget-object v4, p0, Lehh;->W:Lfdj;

    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->d()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    iget-object v0, p0, Lehh;->aa:Ljava/lang/String;

    if-nez v0, :cond_f

    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    const-string v5, "com.google.android.apps.plus.search_key-"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    move v0, v2

    :goto_2
    if-eqz v0, :cond_e

    move v0, v1

    :goto_3
    invoke-virtual {v4, v0}, Lfdj;->c(Z)V

    .line 1506
    iget-object v0, p0, Lehh;->W:Lfdj;

    iget v4, p0, Lehh;->X:I

    invoke-virtual {v0, p2, v4}, Lfdj;->b(Landroid/database/Cursor;I)V

    .line 1507
    invoke-virtual {p0}, Lehh;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lehh;->af()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1508
    const/4 v0, -0x1

    iput v0, p0, Lehh;->X:I

    .line 1511
    :cond_5
    iput-boolean v2, p0, Lehh;->aE:Z

    .line 1512
    iget-boolean v0, p0, Lehh;->aD:Z

    if-eqz v0, :cond_6

    .line 1513
    iput-boolean v1, p0, Lehh;->aD:Z

    .line 1514
    invoke-direct {p0}, Lehh;->aa()V

    .line 1517
    :cond_6
    invoke-virtual {p0}, Lehh;->aA()V

    .line 1518
    invoke-virtual {p0}, Lehh;->aC()V

    .line 1519
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v4, p0, Lehh;->S:Llhd;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Llhc;)V

    .line 1521
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    .line 1523
    iput-boolean v1, p0, Lehh;->aq:Z

    .line 1524
    iput-boolean v1, p0, Lehh;->ar:Z

    .line 1525
    iget-boolean v0, p0, Lehh;->ak:Z

    if-eqz v0, :cond_10

    if-nez v4, :cond_10

    .line 1529
    invoke-virtual {p0}, Lehh;->az()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1530
    iget-object v0, p0, Lehh;->U:Licq;

    const v5, 0x7f0a058f

    invoke-virtual {v0, v5}, Licq;->b(I)Licq;

    .line 1531
    iget-object v0, p0, Lehh;->U:Licq;

    sget-object v5, Lict;->c:Lict;

    invoke-virtual {v0, v5}, Licq;->a(Lict;)V

    .line 1564
    :cond_7
    :goto_4
    iput-boolean v1, p0, Lehh;->ai:Z

    .line 1566
    const-string v0, "HostedStreamFrag"

    const/4 v5, 0x4

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1567
    iget-boolean v0, p0, Lehh;->aq:Z

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x23

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "onLoadFinished - mEndOfStream="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1572
    :cond_8
    if-nez v4, :cond_9

    .line 1573
    if-nez v3, :cond_18

    .line 1574
    invoke-virtual {p0}, Lehh;->al()V

    .line 1580
    :cond_9
    :goto_5
    iget-object v0, p0, Lehh;->at:Llnl;

    const-class v3, Lhnt;

    invoke-static {v0, v3}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnt;

    .line 1581
    if-eqz v0, :cond_a

    .line 1582
    iget-object v3, p0, Lehh;->R:Lhee;

    .line 1583
    invoke-interface {v3}, Lhee;->g()Lhej;

    move-result-object v3

    const-string v4, "account_name"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1582
    invoke-interface {v0, v3, v4, v5}, Lhnt;->a(Ljava/lang/String;J)V

    .line 1585
    :cond_a
    iget-boolean v0, p0, Lehh;->aI:Z

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v0

    if-eqz v0, :cond_19

    new-instance v0, Lkoe;

    const/16 v3, 0x4a

    invoke-direct {v0, v3}, Lkoe;-><init>(I)V

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v3}, Lkoe;->a(Landroid/content/Context;)V

    :goto_6
    iput-boolean v1, p0, Lehh;->aI:Z

    :cond_b
    iget-boolean v0, p0, Lehh;->aJ:Z

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v0

    if-eqz v0, :cond_1b

    new-instance v0, Lkoe;

    const/16 v3, 0x4e

    invoke-direct {v0, v3}, Lkoe;-><init>(I)V

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v3}, Lkoe;->a(Landroid/content/Context;)V

    :goto_7
    iput-boolean v1, p0, Lehh;->aJ:Z

    .line 1587
    :cond_c
    iget-object v0, p0, Lehh;->bj:Llfn;

    iget-object v3, p0, Lehh;->R:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "PullToRefresh"

    aput-object v5, v4, v1

    const-string v1, "TooltipStreamRefresh"

    aput-object v1, v4, v2

    const-string v1, "MenuStreamRefresh"

    aput-object v1, v4, v8

    invoke-interface {v0, v3, v4}, Llfn;->a(I[Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_4
    move v0, v1

    .line 1505
    goto/16 :goto_3

    :cond_d
    move v0, v1

    goto/16 :goto_2

    :cond_e
    move v0, v2

    goto/16 :goto_3

    :cond_f
    move v0, v2

    goto/16 :goto_3

    .line 1534
    :cond_10
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    .line 1535
    invoke-virtual {p0, p2}, Lehh;->a(Landroid/database/Cursor;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1536
    if-eqz v0, :cond_11

    .line 1537
    iget-object v0, p0, Lehh;->U:Licq;

    sget-object v5, Lict;->b:Lict;

    invoke-virtual {v0, v5}, Licq;->a(Lict;)V

    .line 1539
    :cond_11
    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    .line 1540
    const/16 v0, 0x12

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v2, :cond_12

    move v0, v2

    :goto_8
    iput-boolean v0, p0, Lehh;->aq:Z

    .line 1541
    invoke-virtual {p0}, Lehh;->ay()V

    .line 1542
    iget-boolean v0, p0, Lehh;->ai:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lehh;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "pinned_activity_ids"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1543
    invoke-virtual {p0, v2}, Lehh;->a(Z)V

    goto/16 :goto_4

    :cond_12
    move v0, v1

    .line 1540
    goto :goto_8

    .line 1545
    :cond_13
    iget-boolean v5, p0, Lehh;->ai:Z

    if-eqz v5, :cond_14

    .line 1546
    const-string v0, "no_location_stream_key"

    iget-object v5, p0, Lehh;->ag:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1547
    invoke-virtual {p0, v2}, Lehh;->a(Z)V

    goto/16 :goto_4

    .line 1550
    :cond_14
    iget-boolean v5, p0, Lehh;->ae:Z

    if-eqz v5, :cond_15

    iget-object v5, p0, Lehh;->af:Landroid/location/Location;

    if-eqz v5, :cond_7

    .line 1551
    :cond_15
    if-eqz v0, :cond_16

    invoke-virtual {p0}, Lehh;->az()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1552
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v0

    .line 1553
    const-string v5, "fetch_newer"

    .line 1554
    invoke-virtual {v0, v5}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    const v0, 0x7f0a057b

    .line 1556
    :goto_9
    iget-object v5, p0, Lehh;->U:Licq;

    invoke-virtual {v5, v0}, Licq;->b(I)Licq;

    .line 1557
    iget-object v0, p0, Lehh;->U:Licq;

    sget-object v5, Lict;->c:Lict;

    invoke-virtual {v0, v5}, Licq;->a(Lict;)V

    .line 1559
    :cond_16
    invoke-virtual {p0}, Lehh;->ay()V

    goto/16 :goto_4

    .line 1554
    :cond_17
    const v0, 0x7f0a06f6

    goto :goto_9

    .line 1575
    :cond_18
    iget-boolean v0, p0, Lehh;->bg:Z

    if-nez v0, :cond_9

    .line 1576
    iput-boolean v2, p0, Lehh;->bg:Z

    .line 1577
    invoke-virtual {p0}, Lehh;->al()V

    goto/16 :goto_5

    .line 1585
    :cond_19
    invoke-direct {p0}, Lehh;->ap()Z

    move-result v0

    if-eqz v0, :cond_1a

    new-instance v0, Lkoe;

    const/16 v3, 0x40

    invoke-direct {v0, v3}, Lkoe;-><init>(I)V

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v3}, Lkoe;->a(Landroid/content/Context;)V

    goto/16 :goto_6

    :cond_1a
    new-instance v0, Lkoe;

    const/16 v3, 0xc

    invoke-direct {v0, v3}, Lkoe;-><init>(I)V

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v3}, Lkoe;->a(Landroid/content/Context;)V

    goto/16 :goto_6

    :cond_1b
    invoke-direct {p0}, Lehh;->ap()Z

    move-result v0

    if-eqz v0, :cond_1c

    new-instance v0, Lkoe;

    const/16 v3, 0x44

    invoke-direct {v0, v3}, Lkoe;-><init>(I)V

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v3}, Lkoe;->a(Landroid/content/Context;)V

    goto/16 :goto_7

    :cond_1c
    new-instance v0, Lkoe;

    const/16 v3, 0xe

    invoke-direct {v0, v3}, Lkoe;-><init>(I)V

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v3}, Lkoe;->a(Landroid/content/Context;)V

    goto/16 :goto_7

    .line 1453
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch

    .line 1505
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 206
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lehh;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 2

    .prologue
    .line 1291
    const v0, 0x7f10067b

    .line 1292
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 1293
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 1295
    invoke-virtual {p0}, Lehh;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1296
    const v0, 0x7f10047f

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1298
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 618
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 620
    const-string v1, "fetch_newer"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 621
    invoke-direct {p0, p2, v5}, Lehh;->a(Lhoz;Z)V

    .line 653
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    const-string v1, "fetch_older"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 623
    invoke-direct {p0, p2, v4}, Lehh;->a(Lhoz;Z)V

    goto :goto_0

    .line 624
    :cond_2
    const-string v1, "prefetch_newposts"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 625
    invoke-static {}, Llay;->a()Llay;

    move-result-object v0

    invoke-virtual {v0}, Llay;->c()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    iput-boolean v0, p0, Lehh;->ak:Z

    const-string v0, "HostedStreamFrag"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lehh;->ak:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "handleCheckIfChanged - mError="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Lehh;->ak:Z

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lhoz;->toString()Ljava/lang/String;

    :cond_3
    iget-boolean v0, p0, Lehh;->ak:Z

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lehh;->am:J

    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "is_changed"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->an:Z

    :goto_1
    invoke-direct {p0}, Lehh;->aN()V

    invoke-direct {p0}, Lehh;->an()V

    goto :goto_0

    :cond_4
    iput-boolean v5, p0, Lehh;->an:Z

    goto :goto_1

    .line 626
    :cond_5
    const-string v1, "EditModerationStateTask"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 627
    invoke-direct {p0, p2}, Lehh;->a(Lhoz;)V

    goto :goto_0

    .line 628
    :cond_6
    const-string v1, "PromoModifyCircleMembershipsTask"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 629
    invoke-direct {p0, p2}, Lehh;->b(Lhoz;)V

    .line 630
    iget-object v1, p0, Lehh;->aX:Ldib;

    if-eqz v1, :cond_0

    .line 631
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 632
    iget-object v1, p0, Lehh;->aX:Ldib;

    invoke-virtual {v1, v0}, Ldib;->a(I)V

    .line 634
    :cond_7
    iput-object v2, p0, Lehh;->aX:Ldib;

    goto/16 :goto_0

    .line 636
    :cond_8
    const-string v1, "ModifyCircleMembershipsTask"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 637
    iget-object v1, p0, Lehh;->aX:Ldib;

    if-eqz v1, :cond_a

    .line 638
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 639
    iget-object v1, p0, Lehh;->aX:Ldib;

    invoke-virtual {v1, v0}, Ldib;->a(I)V

    .line 641
    :cond_9
    iput-object v2, p0, Lehh;->aX:Ldib;

    .line 643
    :cond_a
    invoke-virtual {p0}, Lehh;->ay()V

    goto/16 :goto_0

    .line 644
    :cond_b
    const-string v1, "GetRedirectUrlTask"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 645
    iget-object v0, p0, Lehh;->bh:Lfcu;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Lehh;->bh:Lfcu;

    invoke-virtual {v0, p2}, Lfcu;->a(Lhoz;)V

    goto/16 :goto_0

    .line 648
    :cond_c
    invoke-static {p1}, Lfhu;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 649
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 650
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lfhu;->a(IJ)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 2678
    invoke-static {p1}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2679
    iget-object v0, p0, Lehh;->bb:Lknu;

    iget-object v2, p0, Lehh;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-interface {v0, v2}, Lknu;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2680
    invoke-virtual {p0, p1}, Lehh;->p(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 2687
    new-instance v0, Lknt;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-direct {v0, v2}, Lknt;-><init>(Landroid/content/Context;)V

    .line 2688
    invoke-virtual {v0, v1}, Lknt;->a(Ljava/lang/String;)Lknt;

    move-result-object v0

    .line 2689
    invoke-virtual {v0}, Lknt;->a()Landroid/content/Intent;

    move-result-object v0

    .line 2691
    if-eqz v0, :cond_0

    .line 2692
    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-direct {p0, v2}, Lehh;->a(Landroid/content/Context;)V

    .line 2694
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lehh;->a(Landroid/content/Intent;I)V

    .line 2695
    invoke-direct {p0, v1, p2, p3}, Lehh;->b(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2702
    :goto_0
    return-void

    .line 2700
    :cond_0
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v2, Lhsn;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsn;

    invoke-interface {v0, p1}, Lhsn;->a(Ljava/lang/String;)V

    .line 2701
    invoke-direct {p0, v1, p2, p3}, Lehh;->b(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2481
    iget-object v2, p0, Lehh;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 2485
    packed-switch p3, :pswitch_data_0

    .line 2543
    :goto_0
    :pswitch_0
    return-void

    .line 2487
    :pswitch_1
    const/16 v2, 0x5d

    move v3, v2

    move v2, v0

    move v0, v1

    .line 2523
    :goto_1
    if-eqz v2, :cond_1

    .line 2524
    iget-object v0, p0, Lehh;->at:Llnl;

    iget-object v2, p0, Lehh;->R:Lhee;

    .line 2525
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-static {v0, v2, p1, p2, p4}, Lcom/google/android/apps/plus/service/EsService;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    .line 2527
    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_0

    .line 2528
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, p1, p2, v3}, Lfdj;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2542
    :cond_0
    :goto_2
    invoke-direct {p0, p4, p1, v1}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 2491
    :pswitch_2
    const/16 v2, 0x32

    move v3, v2

    move v2, v0

    move v0, v1

    .line 2493
    goto :goto_1

    .line 2495
    :pswitch_3
    const/16 v2, 0xe8

    move v3, v2

    move v2, v0

    move v0, v1

    .line 2497
    goto :goto_1

    .line 2499
    :pswitch_4
    const/16 v2, 0x5a

    move v3, v2

    move v2, v0

    move v0, v1

    .line 2501
    goto :goto_1

    :pswitch_5
    move v0, v1

    move v2, v1

    move v3, v1

    .line 2504
    goto :goto_1

    .line 2506
    :pswitch_6
    iget-object v0, p0, Lehh;->at:Llnl;

    iget-object v3, p0, Lehh;->R:Lhee;

    .line 2507
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-static {v0, v3, p1, p4}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    .line 2509
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lehh;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->al:Lhmv;

    .line 2511
    invoke-virtual {v3, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    const-string v3, "extra_gaia_id"

    .line 2512
    invoke-static {v3, p1}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v2

    .line 2509
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    move v0, v1

    move v2, v1

    move v3, v1

    .line 2514
    goto :goto_1

    .line 2517
    :pswitch_7
    const/16 v2, 0x6e

    move v3, v2

    move v2, v1

    .line 2518
    goto :goto_1

    .line 2530
    :cond_1
    if-eqz v0, :cond_0

    .line 2531
    iget-object v0, p0, Lehh;->at:Llnl;

    iget-object v2, p0, Lehh;->R:Lhee;

    .line 2532
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-static {v0, v2, p1, p2, p4}, Lcom/google/android/apps/plus/service/EsService;->e(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    .line 2534
    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_0

    .line 2537
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-static {p1}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p2, v3}, Lfdj;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_2

    .line 2485
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_0
        :pswitch_7
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IZ)V
    .locals 9

    .prologue
    .line 2718
    const/4 v4, 0x0

    .line 2719
    const/4 v5, 0x0

    .line 2720
    iget-boolean v0, p0, Lehh;->be:Z

    if-eqz v0, :cond_0

    if-nez p7, :cond_1

    :cond_0
    if-nez p3, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 2721
    :goto_0
    if-eqz v0, :cond_2

    .line 2722
    iget-object v0, p0, Lehh;->at:Llnl;

    iget-object v1, p0, Lehh;->as:Landroid/database/Cursor;

    invoke-static {v0, v1, p3}, Ldsm;->a(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v4

    .line 2723
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0, p3}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v5

    :cond_2
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v6, p4

    move-object v7, p5

    move v8, p6

    .line 2725
    invoke-virtual/range {v0 .. v8}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2727
    return-void

    .line 2720
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 11

    .prologue
    .line 2924
    invoke-direct {p0, p1}, Lehh;->b(Ljava/lang/String;)V

    .line 2926
    iget-object v1, p0, Lehh;->T:Liwk;

    invoke-virtual {v1}, Liwk;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2927
    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v2, p0, Lehh;->T:Liwk;

    invoke-virtual {v2}, Liwk;->b()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Llnl;->startActivity(Landroid/content/Intent;)V

    .line 3008
    :goto_0
    return-void

    .line 2931
    :cond_0
    iget-object v1, p0, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v3

    .line 2933
    iget-object v1, p0, Lehh;->ba:Ljpb;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-interface {v1, v2, v3}, Ljpb;->d(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2934
    invoke-direct/range {p0 .. p8}, Lehh;->b(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v1

    .line 2936
    iget-object v2, p0, Lehh;->ba:Ljpb;

    const-string v4, "first_circle_add"

    invoke-interface {v2, p0, v3, v4, v1}, Ljpb;->a(Lu;ILjava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 2941
    :cond_1
    const/4 v9, 0x0

    .line 2942
    iget-boolean v1, p0, Lehh;->be:Z

    if-eqz v1, :cond_2

    if-eqz p3, :cond_2

    if-eqz p4, :cond_2

    .line 2943
    const/4 v9, 0x1

    .line 2944
    iget-object v1, p0, Lehh;->ba:Ljpb;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-interface {v1, v2, v3}, Ljpb;->e(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2945
    invoke-direct/range {p0 .. p8}, Lehh;->b(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v7

    .line 2947
    iget-object v1, p0, Lehh;->ba:Ljpb;

    const-string v4, "first_circle_add_one_click"

    move-object v2, p0

    move-object v5, p2

    move-object/from16 v6, p5

    invoke-interface/range {v1 .. v7}, Ljpb;->a(Lu;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 2953
    :cond_2
    if-eqz p4, :cond_6

    .line 2954
    const/4 v1, -0x2

    move/from16 v0, p8

    if-eq v0, v1, :cond_3

    const/4 v1, -0x3

    move/from16 v0, p8

    if-ne v0, v1, :cond_4

    .line 2956
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2957
    new-instance v3, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2958
    invoke-virtual {v3, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p7

    move-object/from16 v7, p6

    move/from16 v8, p8

    .line 2959
    invoke-virtual/range {v1 .. v9}, Lehh;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    .line 2962
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2963
    invoke-virtual {v1, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2968
    new-instance v2, Ldpq;

    iget-object v4, p0, Lehh;->at:Llnl;

    invoke-direct {v2, v4}, Ldpq;-><init>(Landroid/content/Context;)V

    .line 2970
    invoke-virtual {v2, v3}, Ldpq;->a(I)Ldpq;

    move-result-object v2

    .line 2971
    invoke-virtual {v2, p1}, Ldpq;->a(Ljava/lang/String;)Ldpq;

    move-result-object v2

    .line 2972
    invoke-virtual {v2, p2}, Ldpq;->b(Ljava/lang/String;)Ldpq;

    move-result-object v2

    .line 2973
    invoke-virtual {p0}, Lehh;->a()I

    move-result v4

    invoke-virtual {v2, v4}, Ldpq;->b(I)Ldpq;

    move-result-object v2

    .line 2974
    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Ldpq;->c(Ljava/lang/String;)Ldpq;

    move-result-object v2

    .line 2975
    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Ldpq;->d(Ljava/lang/String;)Ldpq;

    move-result-object v2

    .line 2976
    invoke-virtual {v2, v1}, Ldpq;->a(Ljava/util/ArrayList;)Ldpq;

    move-result-object v2

    const/4 v4, 0x0

    .line 2977
    invoke-virtual {v2, v4}, Ldpq;->b(Ljava/util/ArrayList;)Ldpq;

    move-result-object v2

    .line 2978
    invoke-virtual {v2, v1}, Ldpq;->d(Ljava/util/ArrayList;)Ldpq;

    move-result-object v1

    const/4 v2, 0x0

    .line 2979
    invoke-virtual {v1, v2}, Ldpq;->a(Z)Ldpq;

    move-result-object v1

    .line 2980
    if-eqz v9, :cond_5

    .line 2981
    iget-object v2, p0, Lehh;->at:Llnl;

    const/4 v4, 0x1

    invoke-static {v2, v4}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    .line 2983
    const v4, 0x7f0a09d4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    .line 2985
    invoke-virtual {p0, v4, v5}, Lehh;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2984
    invoke-virtual {v1, v2}, Ldpq;->e(Ljava/lang/String;)Ldpq;

    move-result-object v2

    const v4, 0x7f0a09d5

    .line 2987
    invoke-virtual {p0, v4}, Lehh;->e_(I)Ljava/lang/String;

    move-result-object v4

    .line 2986
    invoke-virtual {v2, v4}, Ldpq;->f(Ljava/lang/String;)Ldpq;

    .line 2989
    :cond_5
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v2

    invoke-virtual {v1}, Ldpq;->a()Ldpp;

    move-result-object v1

    invoke-virtual {v2, v1}, Lhoc;->c(Lhny;)V

    .line 2991
    iget-object v1, p0, Lehh;->aY:Liax;

    iget-object v2, p0, Lehh;->at:Llnl;

    const/16 v6, 0x12

    .line 2993
    invoke-virtual {p0}, Lehh;->a()I

    move-result v7

    move-object v4, p1

    move-object/from16 v5, p6

    .line 2991
    invoke-interface/range {v1 .. v7}, Liax;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II)V

    .line 2995
    new-instance v8, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2996
    invoke-virtual {v8, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2997
    const/4 v1, 0x1

    move/from16 v0, p8

    invoke-virtual {p0, v0, v1}, Lehh;->a(II)Ldid;

    move-result-object v10

    .line 2999
    new-instance v4, Ldib;

    iget-object v5, p0, Lehh;->at:Llnl;

    iget-object v1, p0, Lehh;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v1, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lhms;

    const/4 v9, 0x0

    move-object v7, p1

    invoke-direct/range {v4 .. v10}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    .line 3000
    invoke-virtual {v4, v3}, Ldib;->a(I)V

    goto/16 :goto_0

    .line 3003
    :cond_6
    iget-object v2, p0, Lehh;->at:Llnl;

    .line 3005
    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x1

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    .line 3003
    invoke-static/range {v2 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Z)Landroid/content/Intent;

    move-result-object v1

    .line 3006
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lehh;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/16 v8, 0x12

    .line 2828
    iget-object v0, p0, Lehh;->at:Llnl;

    .line 2829
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 2832
    iget-object v1, p0, Lehh;->at:Llnl;

    const-wide/16 v6, 0x0

    move-object v3, p1

    move-object v5, v4

    invoke-static/range {v1 .. v7}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v7

    .line 2834
    const-string v0, "suggestion_id"

    invoke-virtual {v7, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2835
    const-string v0, "suggestion_ui"

    invoke-virtual {v7, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2838
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x70

    move v3, v0

    .line 2841
    :goto_0
    if-eqz p2, :cond_1

    sget-object v0, Lhmv;->ct:Lhmv;

    move-object v1, v0

    .line 2843
    :goto_1
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, v5, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 2845
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2846
    invoke-direct {p0, v3}, Lehh;->c(I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2843
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2848
    iget-object v0, p0, Lehh;->aY:Liax;

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-static {p1}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2849
    invoke-virtual {p0}, Lehh;->a()I

    move-result v6

    move-object v4, p3

    move v5, v8

    .line 2848
    invoke-interface/range {v0 .. v6}, Liax;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II)V

    .line 2850
    invoke-virtual {p0, v7}, Lehh;->a(Landroid/content/Intent;)V

    .line 2851
    return-void

    .line 2838
    :cond_0
    const/16 v0, 0x74

    move v3, v0

    goto :goto_0

    .line 2841
    :cond_1
    sget-object v0, Lhmv;->cv:Lhmv;

    move-object v1, v0

    goto :goto_1
.end method

.method protected a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 3066
    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v2, p0, Lehh;->as:Landroid/database/Cursor;

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Ldsm;->a(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v3

    .line 3068
    const/4 v2, 0x0

    .line 3070
    iget-object v1, p0, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v9

    .line 3071
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3072
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 3073
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3074
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3078
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 3079
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 3080
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 3081
    const/4 v2, 0x1

    .line 3083
    :cond_3
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 3084
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3088
    :cond_4
    const/4 v1, 0x1

    move/from16 v0, p7

    invoke-virtual {p0, v0, v1}, Lehh;->a(II)Ldid;

    move-result-object v7

    .line 3091
    iget-boolean v1, p0, Lehh;->be:Z

    if-eqz v1, :cond_e

    if-eqz v2, :cond_e

    .line 3092
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    const/4 v1, 0x1

    .line 3093
    :goto_2
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v10

    .line 3095
    if-eqz p5, :cond_5

    const/4 v2, -0x2

    move/from16 v0, p7

    if-eq v0, v2, :cond_5

    const/4 v2, -0x3

    move/from16 v0, p7

    if-ne v0, v2, :cond_12

    .line 3098
    :cond_5
    const/4 v2, -0x3

    move/from16 v0, p7

    if-ne v0, v2, :cond_f

    .line 3099
    const/16 v3, 0xd8

    .line 3104
    :goto_3
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    :cond_6
    const/4 v2, 0x1

    move v4, v2

    .line 3105
    :goto_4
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_7
    const/4 v2, 0x1

    .line 3106
    :goto_5
    new-instance v8, Ldpj;

    iget-object v11, p0, Lehh;->at:Llnl;

    invoke-direct {v8, v11}, Ldpj;-><init>(Landroid/content/Context;)V

    .line 3108
    invoke-virtual {v8, v9}, Ldpj;->a(I)Ldpj;

    move-result-object v8

    .line 3109
    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ldpj;->a(Ljava/lang/String;)Ldpj;

    move-result-object v8

    .line 3110
    move-object/from16 v0, p4

    invoke-virtual {v8, v0}, Ldpj;->b(Ljava/lang/String;)Ldpj;

    move-result-object v8

    .line 3111
    invoke-virtual {v8, v3}, Ldpj;->b(I)Ldpj;

    move-result-object v8

    .line 3112
    invoke-virtual {v8, v5}, Ldpj;->a(Ljava/util/ArrayList;)Ldpj;

    move-result-object v8

    .line 3113
    invoke-virtual {v8, v6}, Ldpj;->b(Ljava/util/ArrayList;)Ldpj;

    move-result-object v8

    .line 3114
    invoke-virtual {v8, v4}, Ldpj;->a(Z)Ldpj;

    move-result-object v4

    .line 3115
    invoke-virtual {v4, v2}, Ldpj;->b(Z)Ldpj;

    move-result-object v2

    const/4 v4, 0x0

    .line 3116
    invoke-virtual {v2, v4}, Ldpj;->c(Z)Ldpj;

    move-result-object v2

    .line 3117
    iget-object v4, p0, Lehh;->at:Llnl;

    const/4 v8, 0x1

    invoke-static {v4, v8}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v4

    .line 3118
    if-eqz p8, :cond_8

    .line 3119
    const v8, 0x7f0a09d4

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    .line 3121
    invoke-virtual {p0, v8, v11}, Lehh;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 3120
    invoke-virtual {v2, v8}, Ldpj;->d(Ljava/lang/String;)Ldpj;

    move-result-object v8

    const v11, 0x7f0a09d5

    .line 3123
    invoke-virtual {p0, v11}, Lehh;->e_(I)Ljava/lang/String;

    move-result-object v11

    .line 3122
    invoke-virtual {v8, v11}, Ldpj;->e(Ljava/lang/String;)Ldpj;

    .line 3125
    :cond_8
    if-eqz v1, :cond_9

    .line 3126
    iget-object v1, p0, Lehh;->at:Llnl;

    const v8, 0x7f0a0525

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v4, v11, v12

    .line 3127
    invoke-virtual {v1, v8, v11}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3126
    invoke-virtual {v2, v1}, Ldpj;->e(Ljava/lang/String;)Ldpj;

    .line 3129
    :cond_9
    invoke-virtual {v2}, Ldpj;->a()Ldpi;

    move-result-object v1

    invoke-virtual {v10, v1}, Lhoc;->c(Lhny;)V

    move v8, v3

    .line 3160
    :goto_6
    new-instance v1, Ldib;

    iget-object v2, p0, Lehh;->at:Llnl;

    iget-object v3, p0, Lehh;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v3, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lhms;

    move-object/from16 v4, p3

    invoke-direct/range {v1 .. v7}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    iput-object v1, p0, Lehh;->aX:Ldib;

    .line 3163
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 3165
    iget-object v1, p0, Lehh;->aY:Liax;

    iget-object v2, p0, Lehh;->at:Llnl;

    const/16 v6, 0x12

    move v3, v9

    move-object/from16 v4, p3

    move-object/from16 v5, p6

    move v7, v8

    invoke-interface/range {v1 .. v7}, Liax;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;II)V

    .line 3169
    :cond_a
    if-eqz p5, :cond_1a

    .line 3170
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    const/4 v1, 0x1

    move v2, v1

    .line 3171
    :goto_7
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_16

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_16

    const/4 v1, 0x1

    move v3, v1

    .line 3172
    :goto_8
    invoke-static/range {p3 .. p3}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3173
    if-eqz v2, :cond_17

    .line 3174
    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-virtual {v1, v6}, Lfdj;->e(Ljava/lang/String;)V

    .line 3175
    iget-object v1, p0, Lehh;->at:Llnl;

    const/4 v4, 0x0

    invoke-static {v1, v9, v6, v4}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Z)I

    .line 3180
    :cond_b
    :goto_9
    iget-object v1, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v1, :cond_1a

    .line 3181
    iget-object v1, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v5, v1

    :goto_a
    if-ltz v5, :cond_1a

    .line 3182
    iget-object v1, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v1, v5}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3183
    instance-of v4, v1, Lgbz;

    if-eqz v4, :cond_d

    .line 3184
    check-cast v1, Lgbz;

    .line 3185
    invoke-virtual {v1}, Lgbz;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 3186
    invoke-virtual {v1}, Lgbz;->s()Lkzp;

    move-result-object v4

    if-eqz v4, :cond_18

    .line 3187
    invoke-virtual {v1}, Lgbz;->s()Lkzp;

    move-result-object v4

    invoke-virtual {v4}, Lkzp;->n()Llac;

    move-result-object v4

    if-eqz v4, :cond_18

    const/4 v4, 0x1

    .line 3188
    :goto_b
    if-eqz v2, :cond_19

    .line 3189
    invoke-virtual {v1}, Lgbz;->i()V

    .line 3190
    if-eqz v4, :cond_c

    .line 3191
    new-instance v4, Ldpk;

    iget-object v7, p0, Lehh;->at:Llnl;

    .line 3192
    invoke-virtual {v1}, Lgbz;->C()Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x1

    invoke-direct {v4, v7, v9, v8, v11}, Ldpk;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 3193
    invoke-virtual {v10, v4}, Lhoc;->b(Lhny;)V

    .line 3203
    :cond_c
    :goto_c
    invoke-virtual {v1}, Lgbz;->k()V

    .line 3181
    :cond_d
    add-int/lit8 v1, v5, -0x1

    move v5, v1

    goto :goto_a

    .line 3092
    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 3101
    :cond_f
    invoke-virtual {p0}, Lehh;->a()I

    move-result v3

    goto/16 :goto_3

    .line 3104
    :cond_10
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_4

    .line 3105
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 3131
    :cond_12
    invoke-virtual {p0}, Lehh;->a()I

    move-result v2

    .line 3132
    new-instance v3, Ldpq;

    iget-object v4, p0, Lehh;->at:Llnl;

    invoke-direct {v3, v4}, Ldpq;-><init>(Landroid/content/Context;)V

    .line 3134
    invoke-virtual {v3, v9}, Ldpq;->a(I)Ldpq;

    move-result-object v3

    .line 3135
    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ldpq;->a(Ljava/lang/String;)Ldpq;

    move-result-object v3

    .line 3136
    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ldpq;->b(Ljava/lang/String;)Ldpq;

    move-result-object v3

    .line 3137
    invoke-virtual {v3, v2}, Ldpq;->b(I)Ldpq;

    move-result-object v3

    .line 3138
    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ldpq;->c(Ljava/lang/String;)Ldpq;

    move-result-object v3

    .line 3139
    move-object/from16 v0, p6

    invoke-virtual {v3, v0}, Ldpq;->d(Ljava/lang/String;)Ldpq;

    move-result-object v3

    .line 3140
    invoke-virtual {v3, v5}, Ldpq;->a(Ljava/util/ArrayList;)Ldpq;

    move-result-object v3

    .line 3141
    invoke-virtual {v3, v6}, Ldpq;->b(Ljava/util/ArrayList;)Ldpq;

    move-result-object v3

    .line 3142
    invoke-virtual {v3, p1}, Ldpq;->c(Ljava/util/ArrayList;)Ldpq;

    move-result-object v3

    .line 3143
    invoke-virtual {v3, p2}, Ldpq;->d(Ljava/util/ArrayList;)Ldpq;

    move-result-object v3

    const/4 v4, 0x0

    .line 3144
    invoke-virtual {v3, v4}, Ldpq;->a(Z)Ldpq;

    move-result-object v4

    .line 3145
    iget-object v3, p0, Lehh;->at:Llnl;

    const/4 v8, 0x1

    invoke-static {v3, v8}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    .line 3146
    if-eqz p8, :cond_13

    .line 3147
    iget-object v3, p0, Lehh;->at:Llnl;

    const/4 v8, 0x1

    invoke-static {v3, v8}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    .line 3148
    const v8, 0x7f0a09d4

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    .line 3150
    invoke-virtual {p0, v8, v11}, Lehh;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 3149
    invoke-virtual {v4, v8}, Ldpq;->e(Ljava/lang/String;)Ldpq;

    move-result-object v8

    const v11, 0x7f0a09d5

    .line 3152
    invoke-virtual {p0, v11}, Lehh;->e_(I)Ljava/lang/String;

    move-result-object v11

    .line 3151
    invoke-virtual {v8, v11}, Ldpq;->f(Ljava/lang/String;)Ldpq;

    .line 3154
    :cond_13
    if-eqz v1, :cond_14

    .line 3155
    iget-object v1, p0, Lehh;->at:Llnl;

    const v8, 0x7f0a0525

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    .line 3156
    invoke-virtual {v1, v8, v11}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3155
    invoke-virtual {v4, v1}, Ldpq;->f(Ljava/lang/String;)Ldpq;

    .line 3158
    :cond_14
    invoke-virtual {v4}, Ldpq;->a()Ldpp;

    move-result-object v1

    invoke-virtual {v10, v1}, Lhoc;->c(Lhny;)V

    move v8, v2

    goto/16 :goto_6

    .line 3170
    :cond_15
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_7

    .line 3171
    :cond_16
    const/4 v1, 0x0

    move v3, v1

    goto/16 :goto_8

    .line 3176
    :cond_17
    if-eqz v3, :cond_b

    .line 3177
    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-virtual {v1, v6}, Lfdj;->f(Ljava/lang/String;)V

    .line 3178
    iget-object v1, p0, Lehh;->at:Llnl;

    const/4 v4, 0x1

    invoke-static {v1, v9, v6, v4}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Z)I

    goto/16 :goto_9

    .line 3187
    :cond_18
    const/4 v4, 0x0

    goto/16 :goto_b

    .line 3195
    :cond_19
    if-eqz v3, :cond_c

    .line 3196
    invoke-virtual {v1}, Lgbz;->j()V

    .line 3197
    if-eqz v4, :cond_c

    .line 3198
    new-instance v4, Ldpk;

    iget-object v7, p0, Lehh;->at:Llnl;

    .line 3199
    invoke-virtual {v1}, Lgbz;->C()Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x0

    invoke-direct {v4, v7, v9, v8, v11}, Ldpk;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 3200
    invoke-virtual {v10, v4}, Lhoc;->b(Lhny;)V

    goto/16 :goto_c

    .line 3210
    :cond_1a
    invoke-virtual {p0}, Lehh;->aB()V

    .line 3211
    return-void
.end method

.method public a(Lkzm;)V
    .locals 6

    .prologue
    .line 2812
    iget-object v0, p0, Lehh;->at:Llnl;

    .line 2813
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2814
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0, v1, p1}, Leyq;->a(Landroid/content/Context;ILkzm;)Landroid/content/Intent;

    move-result-object v2

    .line 2816
    const/16 v0, 0x72

    invoke-direct {p0, v0}, Lehh;->c(I)Landroid/os/Bundle;

    move-result-object v3

    .line 2817
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, v5, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->ak:Lhmv;

    .line 2819
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2820
    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2817
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2822
    invoke-virtual {p0, v2}, Lehh;->a(Landroid/content/Intent;)V

    .line 2824
    return-void
.end method

.method public a(Llcd;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2792
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2793
    invoke-virtual {p1, v5}, Llcd;->a(Z)V

    .line 2794
    new-instance v0, Ldpr;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-direct {v0, v2, v1, p2, p1}, Ldpr;-><init>(Landroid/content/Context;ILjava/lang/String;Llcd;)V

    .line 2796
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v2

    .line 2797
    invoke-virtual {v2, v0}, Lhoc;->b(Lhny;)V

    .line 2799
    new-instance v0, Ldpo;

    iget-object v3, p0, Lehh;->at:Llnl;

    .line 2800
    invoke-virtual {p1}, Llcd;->a()I

    move-result v4

    invoke-direct {v0, v3, v1, v4, v5}, Ldpo;-><init>(Landroid/content/Context;III)V

    .line 2801
    invoke-virtual {v2, v0}, Lhoc;->b(Lhny;)V

    .line 2802
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->au:Lhmv;

    .line 2804
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    const-string v2, "user_survey_id"

    .line 2805
    invoke-virtual {p1}, Llcd;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lhmr;->a(Ljava/lang/String;I)Lhmr;

    move-result-object v1

    const-string v2, "user_survey_action_id"

    .line 2806
    invoke-virtual {v1, v2, v5}, Lhmr;->a(Ljava/lang/String;I)Lhmr;

    move-result-object v1

    .line 2802
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2808
    return-void
.end method

.method public a(Llcd;Ljava/lang/String;IZ)V
    .locals 5

    .prologue
    .line 2770
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2772
    new-instance v0, Ldpr;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-direct {v0, v2, v1, p2, p1}, Ldpr;-><init>(Landroid/content/Context;ILjava/lang/String;Llcd;)V

    .line 2774
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v2

    .line 2775
    invoke-virtual {v2, v0}, Lhoc;->b(Lhny;)V

    .line 2777
    if-eqz p4, :cond_0

    .line 2778
    new-instance v0, Ldpo;

    iget-object v3, p0, Lehh;->at:Llnl;

    .line 2779
    invoke-virtual {p1}, Llcd;->a()I

    move-result v4

    invoke-direct {v0, v3, v1, v4, p3}, Ldpo;-><init>(Landroid/content/Context;III)V

    .line 2780
    invoke-virtual {v2, v0}, Lhoc;->b(Lhny;)V

    .line 2781
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Lehh;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->av:Lhmv;

    .line 2783
    invoke-virtual {v2, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    const-string v2, "user_survey_id"

    .line 2784
    invoke-virtual {p1}, Llcd;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lhmr;->a(Ljava/lang/String;I)Lhmr;

    move-result-object v1

    const-string v2, "user_survey_action_id"

    .line 2785
    invoke-virtual {v1, v2, p3}, Lhmr;->a(Ljava/lang/String;I)Lhmr;

    move-result-object v1

    .line 2781
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2788
    :cond_0
    return-void
.end method

.method public a(Loo;)V
    .locals 2

    .prologue
    .line 3310
    iget-object v0, p0, Lehh;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 3311
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const v3, 0x7f0a057b

    .line 1767
    invoke-virtual {p0, p1}, Lehh;->i(Z)V

    .line 1770
    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1771
    const-string v0, "HostedStreamFrag"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1772
    invoke-virtual {p0}, Lehh;->az()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "fetchContent: No circles... reloading: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 1776
    :cond_0
    invoke-virtual {p0}, Lehh;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1777
    iget-object v0, p0, Lehh;->U:Licq;

    invoke-virtual {v0, v3}, Licq;->a(I)Licq;

    .line 1778
    iget-object v0, p0, Lehh;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 1781
    :cond_1
    invoke-virtual {p0}, Lehh;->ay()V

    .line 1802
    :cond_2
    :goto_0
    return-void

    .line 1784
    :cond_3
    invoke-virtual {p0}, Lehh;->aq()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1788
    if-nez p1, :cond_4

    iget-boolean v0, p0, Lehh;->aq:Z

    if-nez v0, :cond_2

    .line 1792
    :cond_4
    invoke-virtual {p0, p1}, Lehh;->j(Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1796
    invoke-virtual {p0}, Lehh;->az()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1797
    iget-boolean v0, p0, Lehh;->ae:Z

    if-eqz v0, :cond_5

    .line 1798
    iget-object v0, p0, Lehh;->U:Licq;

    invoke-virtual {v0, v3}, Licq;->a(I)Licq;

    .line 1800
    :cond_5
    iget-object v0, p0, Lehh;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_0
.end method

.method protected a(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 1442
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    .line 1276
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 1277
    const v1, 0x7f10067b

    if-ne v0, v1, :cond_0

    .line 1278
    invoke-direct {p0}, Lehh;->aM()V

    .line 1279
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ar:Lhmv;

    .line 1280
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1279
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1281
    iget-object v0, p0, Lehh;->bj:Llfn;

    iget-object v1, p0, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "MenuStreamRefresh"

    const-wide/16 v4, 0x3

    invoke-interface {v0, v1, v2, v4, v5}, Llfn;->a(ILjava/lang/String;J)V

    .line 1283
    invoke-virtual {p0}, Lehh;->al()V

    .line 1284
    const/4 v0, 0x1

    .line 1286
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;I)Z
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2732
    iget-boolean v0, p0, Lehh;->be:Z

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    move-object v6, p4

    move-object v7, p5

    move v8, p6

    .line 2733
    invoke-virtual/range {v0 .. v8}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 2735
    const/4 v0, 0x1

    .line 2737
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aA()V
    .locals 1

    .prologue
    .line 2280
    iget-boolean v0, p0, Lehh;->al:Z

    if-eqz v0, :cond_0

    .line 2281
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->ak()V

    .line 2282
    const/4 v0, 0x0

    iput-boolean v0, p0, Lehh;->al:Z

    .line 2284
    :cond_0
    return-void
.end method

.method protected aB()V
    .locals 1

    .prologue
    .line 2287
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-nez v0, :cond_0

    .line 2297
    :goto_0
    return-void

    .line 2295
    :cond_0
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c()I

    move-result v0

    iput v0, p0, Lehh;->aS:I

    .line 2296
    invoke-direct {p0}, Lehh;->ai()I

    move-result v0

    iput v0, p0, Lehh;->aT:I

    goto :goto_0
.end method

.method protected aC()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2319
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehh;->W:Lfdj;

    if-nez v0, :cond_1

    .line 2334
    :cond_0
    :goto_0
    return-void

    .line 2323
    :cond_1
    iget v0, p0, Lehh;->aS:I

    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-virtual {v1}, Lfdj;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget v0, p0, Lehh;->aT:I

    invoke-direct {p0}, Lehh;->ai()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 2324
    :cond_2
    iput v3, p0, Lehh;->aS:I

    .line 2325
    iput v3, p0, Lehh;->aT:I

    .line 2333
    :cond_3
    :goto_1
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->ao()V

    goto :goto_0

    .line 2326
    :cond_4
    iget v0, p0, Lehh;->aT:I

    if-nez v0, :cond_5

    iget v0, p0, Lehh;->aS:I

    if-eqz v0, :cond_3

    .line 2327
    :cond_5
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget v1, p0, Lehh;->aS:I

    iget v2, p0, Lehh;->aT:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(II)V

    .line 2330
    iput v3, p0, Lehh;->aS:I

    .line 2331
    iput v3, p0, Lehh;->aT:I

    goto :goto_1
.end method

.method public aD()V
    .locals 2

    .prologue
    .line 2406
    iget-object v0, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    if-nez v0, :cond_0

    .line 2418
    :goto_0
    return-void

    .line 2410
    :cond_0
    iget-boolean v0, p0, Lehh;->ak:Z

    if-eqz v0, :cond_1

    .line 2411
    iget-object v0, p0, Lehh;->aP:Llhj;

    invoke-virtual {v0}, Llhj;->a()V

    .line 2412
    iget-object v0, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2414
    :cond_1
    iget-object v0, p0, Lehh;->aP:Llhj;

    invoke-virtual {v0}, Llhj;->d()V

    .line 2415
    iget-object v0, p0, Lehh;->aP:Llhj;

    invoke-virtual {v0}, Llhj;->b()V

    .line 2416
    iget-object v0, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected aE()V
    .locals 2

    .prologue
    .line 2444
    invoke-virtual {p0}, Lehh;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 2446
    if-eqz v0, :cond_0

    .line 2447
    invoke-virtual {v0}, Lt;->a()V

    .line 2449
    :cond_0
    return-void
.end method

.method public aF()V
    .locals 6

    .prologue
    .line 2886
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2887
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0, v1}, Leyq;->d(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v2

    .line 2888
    const/16 v0, 0x71

    invoke-direct {p0, v0}, Lehh;->c(I)Landroid/os/Bundle;

    move-result-object v3

    .line 2889
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, v5, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->aZ:Lhmv;

    .line 2891
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2892
    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2889
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2894
    invoke-virtual {p0, v2}, Lehh;->a(Landroid/content/Intent;)V

    .line 2895
    return-void
.end method

.method public aG()I
    .locals 6

    .prologue
    .line 3251
    iget-object v0, p0, Lehh;->at:Llnl;

    iget-object v1, p0, Lehh;->R:Lhee;

    .line 3252
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lehh;->ac:Levm;

    .line 3253
    invoke-virtual {v2}, Levm;->d()I

    move-result v2

    iget-object v3, p0, Lehh;->ac:Levm;

    invoke-virtual {v3}, Levm;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lehh;->aa:Ljava/lang/String;

    const/4 v5, 0x0

    .line 3251
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected aH()Z
    .locals 1

    .prologue
    .line 3296
    iget-object v0, p0, Lehh;->az:Lkci;

    invoke-virtual {v0}, Lkci;->f()Z

    move-result v0

    return v0
.end method

.method public aI()Z
    .locals 1

    .prologue
    .line 3318
    invoke-virtual {p0}, Lehh;->n()Lz;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/plus/phone/HomeActivity;

    return v0
.end method

.method public aJ()Z
    .locals 2

    .prologue
    .line 3523
    invoke-virtual {p0}, Lehh;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lehh;->ac:Levm;

    if-eqz v0, :cond_0

    const-string v0, "v.all.circles"

    iget-object v1, p0, Lehh;->ac:Levm;

    .line 3524
    invoke-virtual {v1}, Levm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aK()Lhoc;
    .locals 2

    .prologue
    .line 3555
    iget-object v0, p0, Lehh;->bi:Lhoc;

    if-nez v0, :cond_0

    .line 3556
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lehh;->bi:Lhoc;

    .line 3558
    :cond_0
    iget-object v0, p0, Lehh;->bi:Lhoc;

    return-object v0
.end method

.method public aL()Z
    .locals 2

    .prologue
    .line 3566
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lkzl;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iget-object v1, p0, Lehh;->R:Lhee;

    .line 3567
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 3566
    invoke-interface {v0, v1}, Lkzl;->h(I)Z

    move-result v0

    return v0
.end method

.method public aO_()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1081
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lehh;->aK:J

    .line 1082
    const/4 v0, 0x0

    iput-boolean v0, p0, Lehh;->aQ:Z

    .line 1083
    iget-object v0, p0, Lehh;->bj:Llfn;

    iget-object v1, p0, Lehh;->R:Lhee;

    .line 1084
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "NewContentTooltipAfterResume"

    const-wide/16 v4, 0x3

    .line 1083
    invoke-interface {v0, v1, v2, v4, v5}, Llfn;->a(ILjava/lang/String;J)V

    .line 1086
    invoke-super {p0}, Llol;->aO_()V

    .line 1088
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v1

    .line 1089
    iget-boolean v0, p0, Lehh;->ar:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lehh;->aQ:Z

    if-nez v0, :cond_0

    const-string v0, "fetch_older"

    .line 1090
    invoke-virtual {v1, v0}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1092
    invoke-virtual {p0}, Lehh;->av()V

    .line 1096
    :cond_0
    iget-object v0, p0, Lehh;->at:Llnl;

    const-class v2, Lieh;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 1097
    iget-object v2, p0, Lehh;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 1098
    sget-object v3, Ldxd;->j:Lief;

    invoke-interface {v0, v3, v2}, Lieh;->b(Lief;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1099
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1100
    invoke-static {v2}, Lfhu;->b(I)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 1101
    sget-object v3, Ldxd;->k:Lief;

    invoke-interface {v0, v3, v2}, Lieh;->c(Lief;I)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 1103
    invoke-static {v2}, Lfhu;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 1102
    invoke-virtual {v1, v3}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    sget-object v3, Ldxd;->i:Lief;

    .line 1104
    invoke-interface {v0, v3, v2}, Lieh;->b(Lief;I)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1105
    new-instance v0, Ldou;

    iget-object v3, p0, Lehh;->at:Llnl;

    .line 1106
    invoke-static {v2}, Lfhu;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4, v2}, Ldou;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 1105
    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 1119
    :cond_1
    :goto_0
    iget-object v0, p0, Lehh;->bd:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 1121
    iget-boolean v0, p0, Lehh;->ae:Z

    if-eqz v0, :cond_2

    .line 1122
    invoke-direct {p0, v8}, Lehh;->a(Landroid/location/Location;)V

    .line 1125
    :cond_2
    iget-object v0, p0, Lehh;->ao:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1126
    iget-object v0, p0, Lehh;->ao:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1127
    iget-object v0, p0, Lehh;->ao:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    .line 1128
    iput-object v8, p0, Lehh;->ao:Ljava/lang/Integer;

    .line 1132
    :cond_3
    const-string v0, "HostedStreamFrag"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1133
    const-string v0, "fetch_newer"

    .line 1134
    invoke-virtual {v1, v0}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lehh;->aa:Ljava/lang/String;

    .line 1137
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lehh;->am:J

    sub-long/2addr v2, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x50

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "onResume refresh pending: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", gaia id: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", time diff (ms): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1133
    :cond_4
    iget v0, p0, Lehh;->X:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 1141
    invoke-virtual {p0}, Lehh;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v8, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1145
    :cond_5
    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_6

    .line 1146
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->ah()V

    .line 1149
    :cond_6
    iget-boolean v0, p0, Lehh;->ai:Z

    if-nez v0, :cond_7

    .line 1150
    invoke-virtual {p0}, Lehh;->ag_()V

    .line 1153
    :cond_7
    invoke-direct {p0}, Lehh;->an()V

    .line 1155
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lfdp;->b(Landroid/view/View;)V

    .line 1156
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10030c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1157
    return-void

    .line 1107
    :cond_8
    sget-object v3, Ldxd;->i:Lief;

    invoke-interface {v0, v3, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1108
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v2, "gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1109
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1110
    iget-object v0, p0, Lehh;->at:Llnl;

    const-class v4, Ljio;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljio;

    .line 1111
    invoke-interface {v0, v3, v2}, Ljio;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    sget-object v4, Ljin;->a:Ljin;

    sget-object v5, Ljix;->d:Ljix;

    invoke-interface {v0, v3, v2, v4, v5}, Ljio;->a(Ljava/lang/String;Ljava/lang/String;Ljin;Ljix;)V

    goto/16 :goto_0
.end method

.method public aQ_()Z
    .locals 1

    .prologue
    .line 3301
    invoke-virtual {p0}, Lehh;->aj()Z

    move-result v0

    return v0
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 3350
    const/4 v0, 0x0

    return-object v0
.end method

.method public ac_()Lhmk;
    .locals 3

    .prologue
    .line 3529
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 3530
    iget-object v0, p0, Lehh;->ac:Levm;

    if-eqz v0, :cond_5

    .line 3531
    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->a()Ljava/lang/String;

    move-result-object v0

    .line 3533
    :goto_0
    const-string v1, "v.all.circles"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3534
    new-instance v0, Lhmk;

    sget-object v1, Lonm;->h:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    .line 3551
    :goto_1
    return-object v0

    .line 3531
    :cond_0
    iget-object v0, p0, Lehh;->ac:Levm;

    .line 3532
    invoke-virtual {v0}, Levm;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 3535
    :cond_1
    iget-object v1, p0, Lehh;->ac:Levm;

    invoke-virtual {v1}, Levm;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3536
    new-instance v0, Lhmk;

    sget-object v1, Lonm;->q:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    goto :goto_1

    .line 3537
    :cond_2
    iget-object v1, p0, Lehh;->ac:Levm;

    invoke-virtual {v1}, Levm;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3538
    new-instance v0, Lhmk;

    sget-object v1, Lonm;->j:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    goto :goto_1

    .line 3539
    :cond_3
    if-eqz v0, :cond_5

    .line 3540
    const-string v1, "f."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 3543
    new-instance v1, Lkqu;

    sget-object v2, Lonm;->b:Lhmn;

    invoke-direct {v1, v2, v0}, Lkqu;-><init>(Lhmn;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    .line 3544
    :cond_4
    const-string v1, "g."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 3545
    new-instance v0, Lkrf;

    sget-object v1, Long;->j:Lhmn;

    iget-object v2, p0, Lehh;->aa:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lkrf;-><init>(Lhmn;Ljava/lang/String;)V

    goto :goto_1

    .line 3551
    :cond_5
    new-instance v0, Lhmk;

    sget-object v1, Lonm;->h:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    goto :goto_1
.end method

.method public ad_()Z
    .locals 1

    .prologue
    .line 792
    const/4 v0, 0x1

    return v0
.end method

.method public ae_()V
    .locals 2

    .prologue
    .line 1185
    invoke-super {p0}, Llol;->ae_()V

    .line 1186
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_0

    .line 1187
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Llhc;)V

    .line 1189
    :cond_0
    invoke-virtual {p0}, Lehh;->q()Lae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lae;->b(Lag;)V

    .line 1190
    return-void
.end method

.method public af_()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1704
    iget-boolean v0, p0, Lehh;->ae:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehh;->af:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 1706
    const-string v0, "no_location_stream_key"

    iput-object v0, p0, Lehh;->ag:Ljava/lang/String;

    .line 1717
    :goto_0
    return-void

    .line 1710
    :cond_0
    iget-object v0, p0, Lehh;->af:Landroid/location/Location;

    if-eqz v0, :cond_3

    .line 1711
    new-instance v0, Llae;

    iget-object v2, p0, Lehh;->af:Landroid/location/Location;

    invoke-direct {v0, v3, v2}, Llae;-><init>(ILandroid/location/Location;)V

    .line 1713
    :goto_1
    iget-object v4, p0, Lehh;->aa:Ljava/lang/String;

    iget-object v2, p0, Lehh;->ac:Levm;

    if-nez v2, :cond_1

    .line 1714
    :goto_2
    iget-object v2, p0, Lehh;->ac:Levm;

    if-nez v2, :cond_2

    move v2, v3

    .line 1713
    :goto_3
    invoke-static {v4, v1, v0, v3, v2}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehh;->ag:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lehh;->ac:Levm;

    .line 1714
    invoke-virtual {v1}, Levm;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_2
    iget-object v2, p0, Lehh;->ac:Levm;

    .line 1715
    invoke-virtual {v2}, Levm;->d()I

    move-result v2

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method protected ag_()V
    .locals 6

    .prologue
    .line 3214
    iget-boolean v0, p0, Lehh;->an:Z

    if-nez v0, :cond_4

    .line 3215
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lehh;->am:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    .line 3216
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v0

    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 3217
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v0

    const-string v1, "prefetch_newposts"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lehh;->ao:Ljava/lang/Integer;

    if-nez v0, :cond_4

    iget-object v0, p0, Lehh;->aa:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 3219
    iget-boolean v0, p0, Lehh;->ae:Z

    if-eqz v0, :cond_1

    .line 3220
    iget-object v0, p0, Lehh;->af:Landroid/location/Location;

    if-eqz v0, :cond_0

    .line 3221
    iget-object v0, p0, Lehh;->at:Llnl;

    iget-object v1, p0, Lehh;->R:Lhee;

    .line 3222
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lehh;->ac:Levm;

    invoke-virtual {v2}, Levm;->d()I

    move-result v2

    new-instance v3, Llae;

    const/4 v4, 0x0

    iget-object v5, p0, Lehh;->af:Landroid/location/Location;

    invoke-direct {v3, v4, v5}, Llae;-><init>(ILandroid/location/Location;)V

    .line 3221
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IILlae;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehh;->ao:Ljava/lang/Integer;

    .line 3248
    :cond_0
    :goto_0
    return-void

    .line 3228
    :cond_1
    const-string v0, "HostedStreamFrag"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3229
    invoke-direct {p0}, Lehh;->ap()Z

    move-result v0

    .line 3230
    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "CheckForNewPost v1("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") v2("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3229
    :cond_2
    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3234
    invoke-direct {p0}, Lehh;->aa()V

    goto :goto_0

    .line 3236
    :cond_3
    invoke-virtual {p0}, Lehh;->aG()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehh;->ao:Ljava/lang/Integer;

    goto :goto_0

    .line 3239
    :cond_4
    iget-boolean v0, p0, Lehh;->an:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lehh;->ap()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3240
    :cond_5
    invoke-static {}, Llay;->a()Llay;

    move-result-object v0

    invoke-virtual {v0}, Llay;->d()J

    move-result-wide v0

    .line 3241
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 3242
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/32 v2, 0x6ddd00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 3245
    invoke-direct {p0}, Lehh;->aa()V

    goto :goto_0
.end method

.method public ah_()V
    .locals 2

    .prologue
    .line 3356
    invoke-direct {p0}, Lehh;->ag()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3357
    iget-object v0, p0, Lehh;->W:Lfdj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfdj;->d(Z)V

    .line 3362
    :goto_0
    return-void

    .line 3359
    :cond_0
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1005f0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3360
    const/4 v0, 0x0

    iput-object v0, p0, Lehh;->ax:Lehu;

    goto :goto_0
.end method

.method protected ai_()Z
    .locals 2

    .prologue
    .line 1271
    invoke-virtual {p0}, Lehh;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pinned_activity_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public aj()Z
    .locals 2

    .prologue
    .line 3286
    iget-object v0, p0, Lehh;->bi:Lhoc;

    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lehh;->bi:Lhoc;

    const-string v1, "fetch_older"

    .line 3287
    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ak()Lhym;
    .locals 1

    .prologue
    .line 920
    const/4 v0, 0x0

    return-object v0
.end method

.method public al()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 2199
    const-string v0, "refresh() start"

    invoke-direct {p0, v0}, Lehh;->c(Ljava/lang/String;)V

    .line 2201
    iput-object v2, p0, Lehh;->ao:Ljava/lang/Integer;

    .line 2204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lehh;->an:Z

    .line 2205
    invoke-direct {p0}, Lehh;->an()V

    .line 2207
    iget-boolean v0, p0, Lehh;->ae:Z

    if-eqz v0, :cond_4

    .line 2209
    iget-object v0, p0, Lehh;->af:Landroid/location/Location;

    .line 2210
    iput-object v2, p0, Lehh;->af:Landroid/location/Location;

    .line 2211
    invoke-direct {p0, v0}, Lehh;->a(Landroid/location/Location;)V

    .line 2212
    invoke-virtual {p0}, Lehh;->ay()V

    .line 2213
    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_0

    .line 2214
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, v2}, Lfdj;->a(Landroid/database/Cursor;)V

    .line 2215
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, v2, v1}, Lfdj;->b(Landroid/database/Cursor;I)V

    .line 2216
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, v3, v1}, Lfdj;->a(ZI)V

    .line 2218
    :cond_0
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    .line 2219
    if-eqz v0, :cond_2

    .line 2220
    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-static {v1}, Liuo;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2221
    invoke-direct {p0}, Lehh;->ae()V

    .line 2222
    invoke-virtual {p0}, Lehh;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2223
    iget-object v0, p0, Lehh;->U:Licq;

    const v1, 0x7f0a02be

    invoke-virtual {v0, v1}, Licq;->a(I)Licq;

    .line 2224
    iget-object v0, p0, Lehh;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 2229
    :cond_1
    :goto_0
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Landroid/widget/ListAdapter;)V

    .line 2230
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v1, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Landroid/widget/ListAdapter;)V

    .line 2235
    :cond_2
    :goto_1
    const-string v0, "refresh() end"

    invoke-direct {p0, v0}, Lehh;->c(Ljava/lang/String;)V

    .line 2236
    return-void

    .line 2227
    :cond_3
    invoke-virtual {p0, v0}, Lehh;->d(Landroid/view/View;)V

    goto :goto_0

    .line 2233
    :cond_4
    invoke-virtual {p0, v3}, Lehh;->a(Z)V

    goto :goto_1
.end method

.method protected ao()Z
    .locals 1

    .prologue
    .line 2242
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public aq()Z
    .locals 3

    .prologue
    .line 800
    invoke-virtual {p0}, Lehh;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "show_empty_stream"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ar()Z
    .locals 1

    .prologue
    .line 1602
    const/4 v0, 0x0

    return v0
.end method

.method public as()Z
    .locals 1

    .prologue
    .line 1609
    const/4 v0, 0x0

    return v0
.end method

.method public at()V
    .locals 3

    .prologue
    .line 1653
    iget-object v0, p0, Lehh;->W:Lfdj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfdj;->d(Z)V

    .line 1654
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->ap()V

    .line 1655
    iget v0, p0, Lehh;->X:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1656
    invoke-virtual {p0}, Lehh;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 1658
    :cond_0
    return-void
.end method

.method public au()V
    .locals 4

    .prologue
    .line 1661
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1662
    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lehh;->ac:Levm;

    .line 1663
    invoke-virtual {v0}, Levm;->a()Ljava/lang/String;

    move-result-object v0

    .line 1665
    :goto_0
    iget-object v2, p0, Lehh;->ac:Levm;

    invoke-virtual {v2}, Levm;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1666
    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-static {v2, v1, v0}, Leyq;->e(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lehh;->a(Landroid/content/Intent;I)V

    .line 1672
    :goto_1
    return-void

    .line 1663
    :cond_0
    iget-object v0, p0, Lehh;->ac:Levm;

    .line 1664
    invoke-virtual {v0}, Levm;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1669
    :cond_1
    iget-object v2, p0, Lehh;->at:Llnl;

    iget-object v3, p0, Lehh;->ac:Levm;

    .line 1670
    invoke-virtual {v3}, Levm;->b()Ljava/lang/String;

    move-result-object v3

    .line 1669
    invoke-static {v2, v1, v0, v3}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lehh;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method protected av()V
    .locals 2

    .prologue
    .line 1734
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->ar:Z

    .line 1735
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    new-instance v1, Lehj;

    invoke-direct {v1, p0}, Lehj;-><init>(Lehh;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->post(Ljava/lang/Runnable;)Z

    .line 1750
    return-void
.end method

.method public aw()[Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1852
    iget-object v1, p0, Lehh;->W:Lfdj;

    if-eqz v1, :cond_0

    .line 1853
    iget-object v1, p0, Lehh;->W:Lfdj;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lfdj;->h(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 1854
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 1859
    :cond_0
    :goto_0
    return-object v0

    .line 1857
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public ax()Lhny;
    .locals 13

    .prologue
    const/4 v6, 0x0

    .line 1864
    invoke-virtual {p0}, Lehh;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pinned_activity_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 1865
    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v0, p0, Lehh;->R:Lhee;

    .line 1866
    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    iget-object v0, p0, Lehh;->ac:Levm;

    .line 1867
    invoke-virtual {v0}, Levm;->d()I

    move-result v3

    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lehh;->aa:Ljava/lang/String;

    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehh;->W:Lfdj;

    .line 1868
    invoke-virtual {v0}, Lfdj;->aj()[Ljava/lang/String;

    move-result-object v7

    .line 1869
    :goto_0
    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v9

    iget-wide v10, p0, Lehh;->Z:J

    invoke-virtual {p0}, Lehh;->aw()[Ljava/lang/String;

    move-result-object v12

    .line 1865
    invoke-static/range {v1 .. v12}, Ldov;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZJ[Ljava/lang/String;)Ldov;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v7, v6

    .line 1868
    goto :goto_0
.end method

.method public ay()V
    .locals 2

    .prologue
    .line 1898
    invoke-virtual {p0}, Lehh;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1899
    iget-object v0, p0, Lehh;->az:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 1901
    :cond_0
    iget-object v0, p0, Lehh;->Q:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 1902
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_1

    .line 1903
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    .line 1904
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v0

    const-string v1, "fetch_older"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    .line 1903
    :cond_1
    return-void
.end method

.method public az()Z
    .locals 1

    .prologue
    .line 2248
    invoke-virtual {p0}, Lehh;->ao()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lehh;->ai:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 3345
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2899
    iget-object v0, p0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2900
    const-string v0, "extra_circle_id"

    iget-object v1, p0, Lehh;->ac:Levm;

    invoke-virtual {v1}, Levm;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2902
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3046
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1982
    const/4 v0, 0x2

    sget-object v1, Lhmv;->ae:Lhmv;

    invoke-direct {p0, p1, p2, v0, v1}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;ILhmv;)V

    .line 1984
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 3315
    return-void
.end method

.method public b(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    .line 657
    const/4 v0, 0x0

    .line 659
    const-string v1, "circle_info"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 660
    const-string v0, "circle_info"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Levm;

    .line 661
    iget-object v1, p0, Lehh;->ac:Levm;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lehh;->ac:Levm;

    .line 662
    invoke-virtual {v1}, Levm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Levm;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 663
    :cond_0
    invoke-direct {p0, v0}, Lehh;->a(Levm;)V

    .line 665
    :cond_1
    const/4 v0, 0x1

    .line 668
    :cond_2
    return v0
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 805
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 806
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 807
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lehh;->R:Lhee;

    .line 808
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Llhe;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llhe;

    .line 809
    if-eqz v0, :cond_0

    .line 810
    iget-object v1, p0, Lehh;->S:Llhd;

    invoke-virtual {v0}, Llhe;->a()Llhc;

    move-result-object v0

    invoke-virtual {v1, v0}, Llhd;->a(Llhc;)V

    .line 812
    :cond_0
    new-instance v0, Liwk;

    iget-object v1, p0, Lehh;->at:Llnl;

    iget-object v2, p0, Lehh;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v1, Lixj;

    .line 813
    invoke-virtual {v0, v1}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lehh;->T:Liwk;

    .line 815
    new-instance v0, Lfcu;

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-direct {v0, v1}, Lfcu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lehh;->bh:Lfcu;

    .line 817
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 819
    new-instance v0, Lkvq;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-direct {v0, v2, p0, v1}, Lkvq;-><init>(Landroid/content/Context;Lu;I)V

    .line 821
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lkvq;->a(Z)V

    .line 823
    iget-object v2, p0, Lehh;->au:Llnh;

    const-class v3, Lhtp;

    new-instance v4, Lewk;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, v5, p0}, Lewk;-><init>(Landroid/content/Context;Lu;)V

    .line 824
    invoke-virtual {v2, v3, v4}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v2

    const-class v3, Lhtr;

    new-instance v4, Leht;

    invoke-direct {v4, p0}, Leht;-><init>(Lehh;)V

    .line 825
    invoke-virtual {v2, v3, v4}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v2

    const-class v3, Lhtf;

    new-instance v4, Lehs;

    invoke-direct {v4, p0}, Lehs;-><init>(Lehh;)V

    .line 826
    invoke-virtual {v2, v3, v4}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v2

    const-class v3, Litj;

    new-instance v4, Lfup;

    iget-object v5, p0, Lehh;->at:Llnl;

    const/16 v6, 0xa

    const-string v7, "HostedStreamFrag"

    invoke-direct {v4, p0, v5, v6, v7}, Lfup;-><init>(Lu;Landroid/content/Context;ILjava/lang/String;)V

    .line 827
    invoke-virtual {v2, v3, v4}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v2

    const-class v3, Lkzh;

    iget-object v4, p0, Lehh;->bh:Lfcu;

    .line 829
    invoke-virtual {v2, v3, v4}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v2

    const-class v3, Lhmm;

    .line 830
    invoke-virtual {v2, v3, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v2

    const-class v3, Lhtg;

    new-instance v4, Lfdq;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, v5}, Lfdq;-><init>(Landroid/content/Context;)V

    .line 831
    invoke-virtual {v2, v3, v4}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v2

    const-class v3, Lhsn;

    new-instance v4, Leho;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, p0, v5}, Leho;-><init>(Lehh;Landroid/content/Context;)V

    .line 832
    invoke-virtual {v2, v3, v4}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v2

    const-class v3, Lkvq;

    .line 833
    invoke-virtual {v2, v3, v0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 836
    iget-object v0, p0, Lehh;->au:Llnh;

    iget-object v0, p0, Lehh;->at:Llnl;

    const-class v2, Llfn;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llfn;

    iput-object v0, p0, Lehh;->bj:Llfn;

    .line 837
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v2, Liax;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liax;

    iput-object v0, p0, Lehh;->aY:Liax;

    .line 838
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v2, Liaw;

    .line 839
    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liaw;

    invoke-interface {v0, v1}, Liaw;->a(I)Z

    move-result v0

    iput-boolean v0, p0, Lehh;->be:Z

    .line 840
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Ljpb;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpb;

    iput-object v0, p0, Lehh;->ba:Ljpb;

    .line 841
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lknu;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lknu;

    iput-object v0, p0, Lehh;->bb:Lknu;

    .line 842
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Ldxm;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxm;

    iput-object v0, p0, Lehh;->aw:Ldxm;

    .line 843
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lita;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lita;

    iput-object v0, p0, Lehh;->bc:Lita;

    .line 844
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 3050
    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1988
    const/4 v0, 0x1

    sget-object v1, Lhmv;->af:Lhmv;

    invoke-direct {p0, p1, p2, v0, v1}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;ILhmv;)V

    .line 1990
    return-void
.end method

.method public c(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 673
    const/4 v0, 0x0

    .line 675
    const-string v1, "people_view_type"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 676
    invoke-virtual {p0}, Lehh;->au()V

    .line 677
    const/4 v0, 0x1

    .line 680
    :cond_0
    return v0
.end method

.method public d(Landroid/content/Intent;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/16 v4, 0x10

    const/4 v3, 0x0

    .line 1681
    const/4 v0, 0x0

    .line 1682
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v1

    .line 1683
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v4, :cond_0

    if-eqz v1, :cond_0

    .line 1684
    const v2, 0x7f10030c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1685
    if-eqz v1, :cond_0

    .line 1687
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 1686
    invoke-static {v1, v3, v3, v0, v2}, Li;->a(Landroid/view/View;IIII)Li;

    move-result-object v0

    .line 1687
    invoke-virtual {v0}, Li;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 1691
    :cond_0
    iget-object v1, p0, Lehh;->T:Liwk;

    invoke-virtual {v1}, Liwk;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1692
    iget-object v0, p0, Lehh;->at:Llnl;

    iget-object v1, p0, Lehh;->T:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Llnl;->startActivity(Landroid/content/Intent;)V

    .line 1698
    :goto_0
    return-void

    .line 1693
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_2

    .line 1694
    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-virtual {v1, p1, v0}, Llnl;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1696
    :cond_2
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, p1}, Llnl;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected final d(Landroid/view/View;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 2161
    iget-object v0, p0, Lehh;->U:Licq;

    invoke-virtual {v0}, Licq;->d()V

    .line 2162
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2163
    const v0, 0x7f10025f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2164
    const v0, 0x7f100260

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2165
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->setVisibility(I)V

    .line 2166
    iget-object v0, p0, Lehh;->aj:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2167
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2870
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2871
    iget-object v0, p0, Lehh;->at:Llnl;

    const/high16 v6, -0x80000000

    const/4 v9, 0x1

    move-object v2, p1

    move-object v5, v3

    move-object v7, p2

    move v8, v4

    invoke-static/range {v0 .. v9}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;ZI)Landroid/content/Intent;

    move-result-object v2

    .line 2875
    const/16 v0, 0x71

    invoke-direct {p0, v0}, Lehh;->c(I)Landroid/os/Bundle;

    move-result-object v3

    .line 2876
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, v5, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->aY:Lhmv;

    .line 2878
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2879
    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2876
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2881
    invoke-virtual {p0, v2}, Lehh;->a(Landroid/content/Intent;)V

    .line 2882
    return-void
.end method

.method public e(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2152
    const-string v0, "refresh"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2153
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1211
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 1213
    const-string v0, "is_fetching_stream"

    iget-boolean v1, p0, Lehh;->aG:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1214
    const-string v0, "fetching_newer_stream"

    iget-boolean v1, p0, Lehh;->aH:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1216
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_0

    .line 1217
    invoke-virtual {p0}, Lehh;->aB()V

    .line 1218
    const-string v0, "scroll_pos"

    iget v1, p0, Lehh;->aS:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1219
    const-string v0, "scroll_off"

    iget v1, p0, Lehh;->aT:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1223
    iput v2, p0, Lehh;->aS:I

    .line 1224
    iput v2, p0, Lehh;->aT:I

    .line 1227
    :cond_0
    iget-object v0, p0, Lehh;->af:Landroid/location/Location;

    if-eqz v0, :cond_1

    .line 1228
    const-string v0, "location"

    iget-object v1, p0, Lehh;->af:Landroid/location/Location;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1231
    :cond_1
    const-string v0, "last_deactivation"

    iget-wide v2, p0, Lehh;->aR:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1232
    const-string v0, "error"

    iget-boolean v1, p0, Lehh;->ak:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1233
    const-string v0, "reset_animation"

    iget-boolean v1, p0, Lehh;->al:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1234
    const-string v0, "stream_change"

    iget-wide v2, p0, Lehh;->am:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1235
    const-string v0, "notifications_change"

    iget-wide v2, p0, Lehh;->aF:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1236
    const-string v0, "stream_change_flag"

    iget-boolean v1, p0, Lehh;->an:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1237
    iget-object v0, p0, Lehh;->ao:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1238
    const-string v0, "stream_change_req_id"

    iget-object v1, p0, Lehh;->ao:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1240
    :cond_2
    const-string v0, "subscribe_visible"

    iget-boolean v1, p0, Lehh;->aU:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1241
    const-string v0, "subscribe_text"

    iget v1, p0, Lehh;->aV:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1242
    const-string v0, "subscribe_icon"

    iget v1, p0, Lehh;->aW:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1244
    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_4

    .line 1245
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_3

    .line 1246
    const-string v0, "stream_hash_activity_ids"

    iget-object v1, p0, Lehh;->W:Lfdj;

    .line 1247
    invoke-virtual {v1}, Lfdj;->al()Ljava/util/ArrayList;

    move-result-object v1

    .line 1246
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1248
    iget v0, p0, Lehh;->X:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c()I

    move-result v0

    .line 1250
    :goto_0
    const-string v1, "stream_restore_position"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1253
    :cond_3
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->aq()Ljava/lang/String;

    move-result-object v0

    .line 1254
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1255
    const-string v1, "popup_invisible_activity_id"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    :cond_4
    const-string v0, "stream_next_sequenced_loader_id"

    iget v1, p0, Lehh;->aL:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1260
    iget-object v0, p0, Lehh;->aX:Ldib;

    if-eqz v0, :cond_5

    .line 1261
    iget-object v0, p0, Lehh;->aX:Ldib;

    invoke-virtual {v0, p1}, Ldib;->a(Landroid/os/Bundle;)V

    .line 1263
    :cond_5
    iget-object v0, p0, Lehh;->ac:Levm;

    if-eqz v0, :cond_6

    .line 1264
    const-string v0, "circle_info"

    iget-object v1, p0, Lehh;->ac:Levm;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1266
    :cond_6
    const-string v0, "first_load"

    iget-boolean v1, p0, Lehh;->ai:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1267
    return-void

    .line 1248
    :cond_7
    iget v0, p0, Lehh;->X:I

    goto :goto_0
.end method

.method public f()V
    .locals 3

    .prologue
    .line 1194
    invoke-super {p0}, Llol;->f()V

    .line 1195
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-eqz v0, :cond_2

    .line 1199
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 1200
    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1201
    instance-of v2, v0, Lljh;

    if-eqz v2, :cond_0

    .line 1202
    check-cast v0, Lljh;

    invoke-interface {v0}, Lljh;->a()V

    .line 1199
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1205
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    .line 1207
    :cond_2
    return-void
.end method

.method public f(I)V
    .locals 2

    .prologue
    .line 1721
    iget-boolean v0, p0, Lehh;->ar:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lehh;->aq:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lehh;->ak:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lehh;->V:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    if-nez v0, :cond_1

    .line 1728
    :cond_0
    :goto_0
    return-void

    .line 1725
    :cond_1
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->getCount()I

    move-result v0

    sget v1, Lehh;->O:I

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    .line 1726
    invoke-virtual {p0}, Lehh;->av()V

    goto :goto_0
.end method

.method protected g(I)V
    .locals 3

    .prologue
    .line 2433
    invoke-virtual {p0}, Lehh;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 2435
    if-nez v0, :cond_0

    .line 2436
    const/4 v0, 0x0

    .line 2437
    invoke-virtual {p0, p1}, Lehh;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 2436
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 2439
    invoke-virtual {p0}, Lehh;->p()Lae;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 2441
    :cond_0
    return-void
.end method

.method public h(I)V
    .locals 5

    .prologue
    .line 2453
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2455
    packed-switch p1, :pswitch_data_0

    .line 2470
    :pswitch_0
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0, v1}, Leyq;->i(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 2474
    :goto_0
    invoke-virtual {p0, v0}, Lehh;->a(Landroid/content/Intent;)V

    .line 2475
    packed-switch p1, :pswitch_data_1

    .line 2476
    :goto_1
    :pswitch_1
    return-void

    .line 2457
    :pswitch_2
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0, v1}, Leyq;->m(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 2461
    :pswitch_3
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0, v1}, Leyq;->n(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 2465
    :pswitch_4
    iget-object v2, p0, Lehh;->at:Llnl;

    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/PeopleListActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "account_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "people_view_type"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 2475
    :pswitch_5
    sget-object v0, Lhmv;->bS:Lhmv;

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lehh;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-direct {p0, p1}, Lehh;->c(I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_1

    :pswitch_6
    sget-object v0, Lhmv;->bR:Lhmv;

    move-object v1, v0

    goto :goto_2

    :pswitch_7
    sget-object v0, Lhmv;->bT:Lhmv;

    move-object v1, v0

    goto :goto_2

    :pswitch_8
    sget-object v0, Lhmv;->bU:Lhmv;

    move-object v1, v0

    goto :goto_2

    :pswitch_9
    sget-object v0, Lhmv;->am:Lhmv;

    move-object v1, v0

    goto :goto_2

    :pswitch_a
    sget-object v0, Lhmv;->cu:Lhmv;

    move-object v1, v0

    goto :goto_2

    :pswitch_b
    sget-object v0, Lhmv;->cw:Lhmv;

    move-object v1, v0

    goto :goto_2

    :pswitch_c
    sget-object v0, Lhmv;->aZ:Lhmv;

    move-object v1, v0

    goto :goto_2

    .line 2455
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 2475
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_1
        :pswitch_1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_8
    .end packed-switch
.end method

.method public i(Z)V
    .locals 2

    .prologue
    .line 1756
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v0

    const-string v1, "prefetch_newposts"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1757
    invoke-static {}, Llay;->a()Llay;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Llay;->a(Z)V

    .line 1759
    :cond_0
    return-void
.end method

.method protected j(Z)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1805
    iget-boolean v0, p0, Lehh;->aG:Z

    if-eqz v0, :cond_0

    move v0, v3

    .line 1848
    :goto_0
    return v0

    .line 1809
    :cond_0
    iput-boolean p1, p0, Lehh;->aH:Z

    .line 1814
    if-eqz p1, :cond_2

    move-object v0, v1

    move-object v2, v1

    .line 1827
    :cond_1
    iget-boolean v5, p0, Lehh;->ae:Z

    if-eqz v5, :cond_5

    .line 1828
    iget-object v0, p0, Lehh;->af:Landroid/location/Location;

    if-nez v0, :cond_3

    move v0, v3

    .line 1829
    goto :goto_0

    .line 1818
    :cond_2
    iget-object v2, p0, Lehh;->Y:Ljava/lang/String;

    .line 1819
    iget-object v0, p0, Lehh;->aA:[B

    .line 1821
    iget-object v5, p0, Lehh;->Y:Ljava/lang/String;

    if-nez v5, :cond_1

    move v0, v3

    .line 1822
    goto :goto_0

    .line 1832
    :cond_3
    new-instance v0, Ldoz;

    iget-object v5, p0, Lehh;->at:Llnl;

    iget-object v6, p0, Lehh;->R:Lhee;

    invoke-interface {v6}, Lhee;->d()I

    move-result v6

    new-instance v7, Llae;

    iget-object v8, p0, Lehh;->af:Landroid/location/Location;

    invoke-direct {v7, v3, v8}, Llae;-><init>(ILandroid/location/Location;)V

    invoke-direct {v0, v5, v6, v7, v2}, Ldoz;-><init>(Landroid/content/Context;ILlae;Ljava/lang/String;)V

    .line 1839
    :goto_1
    iput-boolean v4, p0, Lehh;->aG:Z

    .line 1841
    if-eqz p1, :cond_6

    const-string v2, "fetch_newer"

    :goto_2
    invoke-virtual {v0, v2}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 1842
    invoke-virtual {p0}, Lehh;->aK()Lhoc;

    move-result-object v2

    invoke-virtual {v2, v0}, Lhoc;->b(Lhny;)V

    .line 1844
    invoke-virtual {p0}, Lehh;->ay()V

    .line 1845
    invoke-virtual {p0}, Lehh;->ad_()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1846
    invoke-virtual {p0}, Lehh;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v4, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    :cond_4
    move v0, v4

    .line 1848
    goto :goto_0

    .line 1836
    :cond_5
    invoke-virtual {p0, v2, v0}, Lehh;->a(Ljava/lang/String;[B)Lhny;

    move-result-object v0

    goto :goto_1

    .line 1841
    :cond_6
    const-string v2, "fetch_older"

    goto :goto_2
.end method

.method public k(Z)V
    .locals 7

    .prologue
    .line 2855
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 2856
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0, v2}, Leyq;->o(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v3

    .line 2857
    const/16 v0, 0x74

    invoke-direct {p0, v0}, Lehh;->c(I)Landroid/os/Bundle;

    move-result-object v4

    .line 2858
    if-eqz p1, :cond_0

    sget-object v0, Lhmv;->cu:Lhmv;

    move-object v1, v0

    .line 2860
    :goto_0
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v5, Lhms;

    invoke-virtual {v0, v5}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v5, Lhmr;

    iget-object v6, p0, Lehh;->at:Llnl;

    invoke-direct {v5, v6, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 2862
    invoke-virtual {v5, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2863
    invoke-virtual {v1, v4}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2860
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2865
    invoke-virtual {p0, v3}, Lehh;->a(Landroid/content/Intent;)V

    .line 2866
    return-void

    .line 2858
    :cond_0
    sget-object v0, Lhmv;->cw:Lhmv;

    move-object v1, v0

    goto :goto_0
.end method

.method public l(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lehh;->bh:Lfcu;

    if-eqz v0, :cond_0

    .line 514
    iget-object v0, p0, Lehh;->bh:Lfcu;

    invoke-virtual {v0, p1}, Lfcu;->l(Landroid/os/Bundle;)V

    .line 516
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1614
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1616
    iget-object v1, p0, Lehh;->aC:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    .line 1617
    iget-object v0, p0, Lehh;->at:Llnl;

    const-class v1, Limn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limn;

    iget-object v1, p0, Lehh;->at:Llnl;

    .line 1618
    invoke-interface {v0, v1}, Limn;->a(Landroid/content/Context;)V

    .line 1650
    :cond_0
    :goto_0
    return-void

    .line 1619
    :cond_1
    iget-object v1, p0, Lehh;->aO:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    if-ne p1, v1, :cond_4

    .line 1620
    iput-boolean v3, p0, Lehh;->ak:Z

    .line 1621
    invoke-virtual {p0}, Lehh;->aD()V

    .line 1625
    iget-boolean v0, p0, Lehh;->aH:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lehh;->ao()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1626
    :cond_2
    invoke-direct {p0}, Lehh;->aM()V

    .line 1627
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ar:Lhmv;

    .line 1628
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1627
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1629
    invoke-virtual {p0}, Lehh;->al()V

    goto :goto_0

    .line 1631
    :cond_3
    invoke-virtual {p0}, Lehh;->av()V

    goto :goto_0

    .line 1633
    :cond_4
    iget-object v1, p0, Lehh;->aM:Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    if-ne p1, v1, :cond_7

    .line 1634
    invoke-direct {p0}, Lehh;->aM()V

    .line 1635
    invoke-virtual {p0}, Lehh;->aL()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lkoe;

    const/16 v1, 0x4d

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehh;->aJ:Z

    .line 1636
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lehh;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ar:Lhmv;

    .line 1637
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1636
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1638
    iget-object v0, p0, Lehh;->bj:Llfn;

    iget-object v1, p0, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "TooltipStreamRefresh"

    const-wide/16 v4, 0x3

    invoke-interface {v0, v1, v2, v4, v5}, Llfn;->a(ILjava/lang/String;J)V

    .line 1640
    invoke-virtual {p0}, Lehh;->al()V

    .line 1644
    iput-boolean v3, p0, Lehh;->an:Z

    .line 1645
    invoke-direct {p0}, Lehh;->an()V

    goto/16 :goto_0

    .line 1635
    :cond_5
    invoke-direct {p0}, Lehh;->ap()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lkoe;

    const/16 v1, 0x43

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    goto :goto_1

    :cond_6
    new-instance v0, Lkoe;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    goto :goto_1

    .line 1646
    :cond_7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f10030c

    if-ne v1, v2, :cond_0

    .line 1647
    invoke-virtual {p1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 1648
    iget-object v1, p0, Lehh;->at:Llnl;

    invoke-static {v1, v0}, Leyq;->l(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lehh;->d(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected p(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 3385
    const/4 v0, 0x0

    return v0
.end method

.method public q(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2742
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2743
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0, v1, p1}, Leyq;->i(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2744
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lehh;->c(I)Landroid/os/Bundle;

    move-result-object v3

    .line 2745
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, v5, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->cD:Lhmv;

    .line 2747
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2748
    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2745
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2750
    invoke-virtual {p0, v2}, Lehh;->a(Landroid/content/Intent;)V

    .line 2751
    return-void
.end method

.method public r(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2755
    iget-object v0, p0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 2756
    iget-object v0, p0, Lehh;->at:Llnl;

    invoke-static {v0, v1, p1}, Leyq;->i(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2758
    const/16 v0, 0x8b

    invoke-direct {p0, v0}, Lehh;->c(I)Landroid/os/Bundle;

    move-result-object v3

    .line 2759
    iget-object v0, p0, Lehh;->au:Llnh;

    const-class v4, Lhms;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v4, Lhmr;

    iget-object v5, p0, Lehh;->at:Llnl;

    invoke-direct {v4, v5, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v1, Lhmv;->cF:Lhmv;

    .line 2761
    invoke-virtual {v4, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 2762
    invoke-virtual {v1, v3}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 2759
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 2764
    invoke-virtual {p0, v2}, Lehh;->a(Landroid/content/Intent;)V

    .line 2765
    return-void
.end method

.method public z()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1161
    invoke-super {p0}, Llol;->z()V

    .line 1163
    iget-object v0, p0, Lehh;->W:Lfdj;

    if-eqz v0, :cond_0

    .line 1164
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->ai()V

    .line 1165
    iget-object v0, p0, Lehh;->W:Lfdj;

    invoke-virtual {v0}, Lfdj;->an()V

    .line 1168
    :cond_0
    iget-object v0, p0, Lehh;->bd:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 1170
    invoke-direct {p0}, Lehh;->ad()V

    .line 1172
    invoke-virtual {p0}, Lehh;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lfdp;->a(Landroid/view/View;)V

    .line 1174
    iget-object v0, p0, Lehh;->bf:Lehl;

    if-eqz v0, :cond_1

    .line 1175
    iget-object v0, p0, Lehh;->bf:Lehl;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1176
    iget-object v0, p0, Lehh;->bf:Lehl;

    invoke-virtual {v0}, Lehl;->run()V

    .line 1177
    iput-object v2, p0, Lehh;->bf:Lehl;

    .line 1180
    :cond_1
    iput-object v2, p0, Lehh;->aB:Landroid/widget/Toast;

    .line 1181
    return-void
.end method
