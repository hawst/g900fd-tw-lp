.class public Lhov;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llra;
.implements Llrg;


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhox;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhov;->a:Ljava/util/ArrayList;

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhov;->b:Z

    .line 40
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 41
    return-void
.end method

.method static synthetic a(Lhov;Lhox;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lhov;->b(Lhox;)V

    return-void
.end method

.method private declared-synchronized b(Lhox;)V
    .locals 1

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhov;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized E_()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 65
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput-boolean v1, p0, Lhov;->b:Z

    move v1, v0

    .line 66
    :goto_0
    iget-object v0, p0, Lhov;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 67
    iget-object v0, p0, Lhov;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhox;

    .line 68
    invoke-interface {v0}, Lhox;->b()V

    .line 66
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 70
    :cond_0
    iget-object v0, p0, Lhov;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    monitor-exit p0

    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/Runnable;)Lhox;
    .locals 2

    .prologue
    .line 44
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, v1}, Lhov;->a(Ljava/lang/Runnable;J)Lhox;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/Runnable;J)Lhox;
    .locals 2

    .prologue
    .line 48
    monitor-enter p0

    const/4 v0, 0x0

    .line 49
    :try_start_0
    iget-boolean v1, p0, Lhov;->b:Z

    if-eqz v1, :cond_0

    .line 50
    new-instance v0, Lhoy;

    invoke-direct {v0, p0, p1, p2, p3}, Lhoy;-><init>(Lhov;Ljava/lang/Runnable;J)V

    .line 51
    iget-object v1, p0, Lhov;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    invoke-interface {v0}, Lhox;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :cond_0
    monitor-exit p0

    return-object v0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lhox;)V
    .locals 1

    .prologue
    .line 58
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 59
    :try_start_0
    invoke-interface {p1}, Lhox;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    :cond_0
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
