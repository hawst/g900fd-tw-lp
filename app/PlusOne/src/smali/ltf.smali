.class public final Lltf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lltf;


# instance fields
.field public b:I

.field private c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Lltf;

    sput-object v0, Lltf;->a:[Lltf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 18
    const/high16 v0, -0x80000000

    iput v0, p0, Lltf;->b:I

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 38
    iget v1, p0, Lltf;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 39
    const/4 v0, 0x1

    iget v1, p0, Lltf;->b:I

    .line 40
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42
    :cond_0
    iget-object v1, p0, Lltf;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 43
    const/4 v1, 0x2

    iget-object v2, p0, Lltf;->c:Ljava/lang/Long;

    .line 44
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_1
    iget-object v1, p0, Lltf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    iput v0, p0, Lltf;->ai:I

    .line 48
    return v0
.end method

.method public a(Loxn;)Lltf;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 56
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 57
    sparse-switch v0, :sswitch_data_0

    .line 61
    iget-object v1, p0, Lltf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lltf;->ah:Ljava/util/List;

    .line 65
    :cond_1
    iget-object v1, p0, Lltf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    :sswitch_0
    return-object p0

    .line 72
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 73
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 76
    :cond_2
    iput v0, p0, Lltf;->b:I

    goto :goto_0

    .line 78
    :cond_3
    iput v2, p0, Lltf;->b:I

    goto :goto_0

    .line 83
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lltf;->c:Ljava/lang/Long;

    goto :goto_0

    .line 57
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 25
    iget v0, p0, Lltf;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 26
    const/4 v0, 0x1

    iget v1, p0, Lltf;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 28
    :cond_0
    iget-object v0, p0, Lltf;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 29
    const/4 v0, 0x2

    iget-object v1, p0, Lltf;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 31
    :cond_1
    iget-object v0, p0, Lltf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lltf;->a(Loxn;)Lltf;

    move-result-object v0

    return-object v0
.end method
