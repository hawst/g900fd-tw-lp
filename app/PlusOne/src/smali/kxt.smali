.class public final Lkxt;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private Q:Landroid/widget/CheckBox;

.field private R:Landroid/widget/CheckBox;

.field private S:Landroid/view/View;

.field private T:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lloj;-><init>()V

    .line 29
    return-void
.end method

.method private U()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lkxt;->R:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 133
    :goto_0
    iget-object v1, p0, Lkxt;->Q:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 134
    iget-object v1, p0, Lkxt;->S:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 135
    return-void

    .line 132
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lkxt;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lkxt;->Q:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lkxt;
    .locals 3

    .prologue
    .line 67
    new-instance v0, Lkxt;

    invoke-direct {v0}, Lkxt;-><init>()V

    .line 68
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 69
    const-string v2, "gaia_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v2, "square_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v2, "activity_id"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-virtual {v0, v1}, Lkxt;->f(Landroid/os/Bundle;)V

    .line 73
    return-object v0
.end method

.method static synthetic b(Lkxt;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lkxt;->U()V

    return-void
.end method

.method static synthetic c(Lkxt;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lkxt;->R:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 78
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lkxt;->N:Llnl;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 80
    iget-object v0, p0, Lkxt;->N:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 81
    const v2, 0x7f040055

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 83
    const v0, 0x7f1001c4

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lkxt;->Q:Landroid/widget/CheckBox;

    .line 84
    const v0, 0x7f1001c6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lkxt;->R:Landroid/widget/CheckBox;

    .line 85
    const v0, 0x7f1001c3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lkxt;->S:Landroid/view/View;

    .line 86
    const v0, 0x7f1001c5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lkxt;->T:Landroid/view/View;

    .line 88
    if-eqz p1, :cond_0

    .line 89
    iget-object v0, p0, Lkxt;->Q:Landroid/widget/CheckBox;

    const-string v3, "also_remove"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 90
    iget-object v0, p0, Lkxt;->R:Landroid/widget/CheckBox;

    const-string v3, "also_report"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 95
    :goto_0
    invoke-direct {p0}, Lkxt;->U()V

    .line 97
    iget-object v0, p0, Lkxt;->R:Landroid/widget/CheckBox;

    new-instance v3, Lkxu;

    invoke-direct {v3, p0}, Lkxu;-><init>(Lkxt;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 108
    iget-object v0, p0, Lkxt;->S:Landroid/view/View;

    new-instance v3, Lkxv;

    invoke-direct {v3, p0}, Lkxv;-><init>(Lkxt;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lkxt;->T:Landroid/view/View;

    new-instance v3, Lkxw;

    invoke-direct {v3, p0}, Lkxw;-><init>(Lkxt;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 124
    const v0, 0x7f0a0228

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 125
    const v0, 0x104000a

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 126
    const/high16 v0, 0x1040000

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 128
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 92
    :cond_0
    iget-object v0, p0, Lkxt;->Q:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 93
    iget-object v0, p0, Lkxt;->R:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 139
    const-string v0, "also_remove"

    iget-object v1, p0, Lkxt;->Q:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 140
    const-string v0, "also_report"

    iget-object v1, p0, Lkxt;->R:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 141
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 145
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 146
    invoke-virtual {p0}, Lkxt;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-virtual {p0}, Lkxt;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "square_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-virtual {p0}, Lkxt;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "activity_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 149
    iget-object v0, p0, Lkxt;->Q:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    .line 150
    iget-object v0, p0, Lkxt;->R:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    .line 152
    iget-object v0, p0, Lkxt;->O:Llnh;

    const-class v6, Lkxx;

    invoke-virtual {v0, v6}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkxx;

    .line 153
    invoke-interface/range {v0 .. v5}, Lkxx;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 155
    :cond_0
    return-void
.end method
