.class public final Lmmv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6817
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6838
    const/4 v0, 0x0

    .line 6839
    iget-object v1, p0, Lmmv;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 6840
    const/4 v0, 0x1

    iget-object v1, p0, Lmmv;->a:Ljava/lang/Float;

    .line 6841
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 6843
    :cond_0
    iget-object v1, p0, Lmmv;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 6844
    const/4 v1, 0x2

    iget-object v2, p0, Lmmv;->b:Ljava/lang/Float;

    .line 6845
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 6847
    :cond_1
    iget-object v1, p0, Lmmv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6848
    iput v0, p0, Lmmv;->ai:I

    .line 6849
    return v0
.end method

.method public a(Loxn;)Lmmv;
    .locals 2

    .prologue
    .line 6857
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6858
    sparse-switch v0, :sswitch_data_0

    .line 6862
    iget-object v1, p0, Lmmv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6863
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmmv;->ah:Ljava/util/List;

    .line 6866
    :cond_1
    iget-object v1, p0, Lmmv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6868
    :sswitch_0
    return-object p0

    .line 6873
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmv;->a:Ljava/lang/Float;

    goto :goto_0

    .line 6877
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmmv;->b:Ljava/lang/Float;

    goto :goto_0

    .line 6858
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6826
    iget-object v0, p0, Lmmv;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 6827
    const/4 v0, 0x1

    iget-object v1, p0, Lmmv;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6829
    :cond_0
    iget-object v0, p0, Lmmv;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 6830
    const/4 v0, 0x2

    iget-object v1, p0, Lmmv;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 6832
    :cond_1
    iget-object v0, p0, Lmmv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6834
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6813
    invoke-virtual {p0, p1}, Lmmv;->a(Loxn;)Lmmv;

    move-result-object v0

    return-object v0
.end method
