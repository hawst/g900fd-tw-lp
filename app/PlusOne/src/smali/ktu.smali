.class public final Lktu;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Lktw;

.field private final c:I

.field private d:Landroid/database/sqlite/SQLiteQueryBuilder;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 50
    sget-object v0, Lktp;->a:Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 38
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    iput-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lktu;->e:Ljava/util/List;

    .line 51
    const-class v0, Lktw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktw;

    iput-object v0, p0, Lktu;->b:Lktw;

    .line 52
    iput p2, p0, Lktu;->c:I

    .line 53
    invoke-virtual {p0, p3}, Lktu;->a([Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v1, "squares"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    sget-object v1, Lktv;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 57
    iget-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v1, "1=1"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 59
    const-string v0, "square_name COLLATE NOCASE"

    invoke-virtual {p0, v0}, Lktu;->b(Ljava/lang/String;)V

    .line 60
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 64
    iget-object v0, p0, Lktu;->b:Lktw;

    iget v1, p0, Lktu;->c:I

    invoke-virtual {v0, v1}, Lktw;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lktu;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lktu;->c:I

    invoke-static {v0, v1}, Lktk;->a(Landroid/content/Context;I)Lktk;

    move-result-object v0

    .line 68
    invoke-static {v0}, Lhoc;->a(Lhny;)Lhoz;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-string v0, "SquareLoader"

    const-string v1, "Failed To sync Square list. Returning cached data."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    :cond_0
    invoke-virtual {p0}, Lktu;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lktu;->c:I

    invoke-static {v0, v1}, Lhzt;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 75
    iget-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-virtual {p0}, Lktu;->j()[Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lktu;->e:Ljava/util/List;

    iget-object v5, p0, Lktu;->e:Ljava/util/List;

    .line 76
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {p0}, Lktu;->m()Ljava/lang/String;

    move-result-object v7

    move-object v5, v3

    move-object v6, v3

    .line 75
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 78
    return-object v0
.end method

.method public a(I)Lktu;
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 83
    and-int/lit8 v0, p1, 0x1

    if-lez v0, :cond_0

    .line 84
    iget-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v1, "is_member!=0 OR "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 86
    :cond_0
    and-int/lit8 v0, p1, 0x2

    if-lez v0, :cond_1

    .line 87
    iget-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v1, "membership_status=5 OR "

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 90
    :cond_1
    iget-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v1, "0=1)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 91
    return-object p0
.end method

.method public a(Z)Lktu;
    .locals 2

    .prologue
    .line 95
    if-eqz p1, :cond_0

    .line 96
    iget-object v0, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v1, " AND restricted_domain!=\'\'"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 98
    :cond_0
    return-object p0
.end method

.method public c(Ljava/lang/String;)Lktu;
    .locals 8

    .prologue
    .line 102
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 103
    invoke-static {p1}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    const/4 v1, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lktu;->d:Landroid/database/sqlite/SQLiteQueryBuilder;

    const-string v2, " AND (square_name LIKE \'"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "square_name"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "square_name"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2e

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "%\' OR "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " LIKE \'% "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%\' OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " GLOB \'*[^a-zA-Z0-9\'\']"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "*\')"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 117
    :cond_0
    return-object p0
.end method
