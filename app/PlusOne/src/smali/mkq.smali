.class public final Lmkq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmqy;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18061
    invoke-direct {p0}, Loxq;-><init>()V

    .line 18064
    iput-object v0, p0, Lmkq;->apiHeader:Llyq;

    .line 18067
    iput-object v0, p0, Lmkq;->a:Lmqy;

    .line 18061
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 18084
    const/4 v0, 0x0

    .line 18085
    iget-object v1, p0, Lmkq;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 18086
    const/4 v0, 0x1

    iget-object v1, p0, Lmkq;->apiHeader:Llyq;

    .line 18087
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 18089
    :cond_0
    iget-object v1, p0, Lmkq;->a:Lmqy;

    if-eqz v1, :cond_1

    .line 18090
    const/4 v1, 0x2

    iget-object v2, p0, Lmkq;->a:Lmqy;

    .line 18091
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18093
    :cond_1
    iget-object v1, p0, Lmkq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18094
    iput v0, p0, Lmkq;->ai:I

    .line 18095
    return v0
.end method

.method public a(Loxn;)Lmkq;
    .locals 2

    .prologue
    .line 18103
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 18104
    sparse-switch v0, :sswitch_data_0

    .line 18108
    iget-object v1, p0, Lmkq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 18109
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkq;->ah:Ljava/util/List;

    .line 18112
    :cond_1
    iget-object v1, p0, Lmkq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18114
    :sswitch_0
    return-object p0

    .line 18119
    :sswitch_1
    iget-object v0, p0, Lmkq;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 18120
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmkq;->apiHeader:Llyq;

    .line 18122
    :cond_2
    iget-object v0, p0, Lmkq;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18126
    :sswitch_2
    iget-object v0, p0, Lmkq;->a:Lmqy;

    if-nez v0, :cond_3

    .line 18127
    new-instance v0, Lmqy;

    invoke-direct {v0}, Lmqy;-><init>()V

    iput-object v0, p0, Lmkq;->a:Lmqy;

    .line 18129
    :cond_3
    iget-object v0, p0, Lmkq;->a:Lmqy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18104
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 18072
    iget-object v0, p0, Lmkq;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 18073
    const/4 v0, 0x1

    iget-object v1, p0, Lmkq;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18075
    :cond_0
    iget-object v0, p0, Lmkq;->a:Lmqy;

    if-eqz v0, :cond_1

    .line 18076
    const/4 v0, 0x2

    iget-object v1, p0, Lmkq;->a:Lmqy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18078
    :cond_1
    iget-object v0, p0, Lmkq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 18080
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 18057
    invoke-virtual {p0, p1}, Lmkq;->a(Loxn;)Lmkq;

    move-result-object v0

    return-object v0
.end method
