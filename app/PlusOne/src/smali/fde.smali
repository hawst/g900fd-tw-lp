.class public final Lfde;
.super Lhyj;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhyj",
        "<",
        "Landroid/database/Cursor;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final e:Lizu;

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lae;Lizu;ZZZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lhyj;-><init>(Landroid/content/Context;Lae;Lhyo;)V

    .line 32
    iput-object p3, p0, Lfde;->e:Lizu;

    .line 33
    iput-boolean p4, p0, Lfde;->f:Z

    .line 34
    iput-boolean p5, p0, Lfde;->g:Z

    .line 35
    iput-boolean p6, p0, Lfde;->h:Z

    .line 36
    new-instance v0, Lhym;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    .line 37
    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    .line 38
    new-instance v1, Lhyf;

    invoke-direct {v1, v0}, Lhyf;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {p0, v1, v3}, Lfde;->a(Lhyo;I)Lhyo;

    .line 39
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lhyo;I)Lu;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lhyo",
            "<",
            "Landroid/database/Cursor;",
            "Ljava/lang/Long;",
            ">;I)",
            "Lu;"
        }
    .end annotation

    .prologue
    .line 43
    invoke-interface {p2}, Lhyo;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 44
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 45
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "photo_ref"

    iget-object v3, p0, Lfde;->e:Lizu;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "prevent_edit"

    iget-boolean v3, p0, Lfde;->f:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "prevent_share"

    iget-boolean v3, p0, Lfde;->g:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "prevent_delete"

    iget-boolean v3, p0, Lfde;->h:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "pager_identifier"

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Ldfv;->k(Landroid/os/Bundle;)Ldfv;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lu;->i_(Z)V

    return-object v0
.end method
