.class public final Ldix;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmko;",
        "Lmkp;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x3

    aput v2, v0, v1

    sput-object v0, Ldix;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 32
    const-string v3, "collectionupdate"

    new-instance v4, Lmko;

    invoke-direct {v4}, Lmko;-><init>()V

    new-instance v5, Lmkp;

    invoke-direct {v5}, Lmkp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 34
    iput-object p3, p0, Ldix;->b:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Ldix;->p:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method protected a(Lmko;)V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lnai;

    invoke-direct {v0}, Lnai;-><init>()V

    iput-object v0, p1, Lmko;->a:Lnai;

    .line 41
    iget-object v1, p1, Lmko;->a:Lnai;

    .line 42
    iget-object v0, p0, Ldix;->b:Ljava/lang/String;

    iget-object v2, p0, Ldix;->p:Ljava/lang/String;

    invoke-static {v0, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;)Lmzo;

    move-result-object v0

    iput-object v0, v1, Lnai;->a:Lmzo;

    .line 43
    sget-object v0, Ldix;->a:[I

    iput-object v0, v1, Lnai;->b:[I

    .line 44
    iget-object v0, p0, Ldix;->f:Landroid/content/Context;

    const-class v2, Lfur;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfur;

    invoke-virtual {v0}, Lfur;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnai;->h:Ljava/lang/Boolean;

    .line 47
    :cond_0
    return-void
.end method

.method protected a(Lmkp;)V
    .locals 4

    .prologue
    .line 51
    iget-object v0, p1, Lmkp;->a:Lnaj;

    .line 52
    iget-object v1, v0, Lnaj;->a:Lmzp;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lnaj;->a:Lmzp;

    iget-object v1, v1, Lmzp;->a:Lmzq;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lnaj;->a:Lmzp;

    iget-object v1, v1, Lmzp;->a:Lmzq;

    iget v1, v1, Lmzq;->a:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 54
    new-instance v1, Lkfm;

    const-string v2, "UpdateCollectionOperation failed: "

    iget-object v0, v0, Lnaj;->a:Lmzp;

    iget-object v0, v0, Lmzp;->a:Lmzq;

    iget-object v0, v0, Lmzq;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Lkfm;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p0, Ldix;->f:Landroid/content/Context;

    iget v1, p0, Ldix;->c:I

    iget-object v2, p0, Ldix;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 59
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmko;

    invoke-virtual {p0, p1}, Ldix;->a(Lmko;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmkp;

    invoke-virtual {p0, p1}, Ldix;->a(Lmkp;)V

    return-void
.end method
