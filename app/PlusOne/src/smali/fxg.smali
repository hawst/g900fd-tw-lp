.class public final Lfxg;
.super Lfwp;
.source "PG"


# static fields
.field private static a:Z

.field private static b:Landroid/graphics/drawable/Drawable;

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:I

.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:I

.field private static i:I

.field private static j:I


# instance fields
.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/TextView;

.field private m:Lfwe;

.field private n:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lfwp;-><init>(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method private a(ILjava/util/ArrayList;)Ljava/lang/String;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ldrv;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v3, 0x0

    .line 238
    invoke-virtual {p0}, Lfxg;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 243
    packed-switch p1, :pswitch_data_0

    move-object v5, v4

    .line 261
    :goto_0
    if-eqz v5, :cond_3

    .line 279
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    array-length v1, v5

    if-lt v0, v1, :cond_0

    .line 280
    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v5, v0

    .line 281
    new-array v1, v11, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 299
    :goto_1
    return-object v0

    .line 245
    :pswitch_0
    const v0, 0x7f0f0016

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 246
    goto :goto_0

    .line 250
    :pswitch_1
    const v0, 0x7f0f0018

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 251
    goto :goto_0

    .line 255
    :pswitch_2
    const v0, 0x7f0f0017

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 256
    goto :goto_0

    .line 260
    :pswitch_3
    const v0, 0x7f0f0019

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    goto :goto_0

    .line 283
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/CharSequence;

    move v2, v3

    .line 284
    :goto_2
    array-length v0, v1

    if-ge v2, v0, :cond_2

    .line 285
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldrv;

    .line 286
    iget v7, v0, Ldrv;->numAdditionalGuests:I

    if-nez v7, :cond_1

    iget-object v0, v0, Ldrv;->name:Ljava/lang/String;

    .line 287
    :goto_3
    aput-object v0, v1, v2

    .line 284
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 286
    :cond_1
    const v7, 0x7f11005f

    iget v8, v0, Ldrv;->numAdditionalGuests:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, v0, Ldrv;->name:Ljava/lang/String;

    aput-object v10, v9, v3

    iget v0, v0, Ldrv;->numAdditionalGuests:I

    .line 289
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v9, v11

    .line 287
    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 292
    :cond_2
    array-length v0, v1

    if-lez v0, :cond_3

    .line 293
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    aget-object v2, v5, v0

    move-object v0, v1

    .line 294
    check-cast v0, [Ljava/lang/Object;

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v4

    goto :goto_1

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 192
    invoke-super {p0}, Lfwp;->a()V

    .line 193
    iget-object v0, p0, Lfxg;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 194
    iget-object v0, p0, Lfxg;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v0, p0, Lfxg;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v0, p0, Lfxg;->m:Lfwe;

    invoke-virtual {v0}, Lfwe;->b()V

    .line 197
    return-void
.end method

.method public a(IJLjava/util/List;Lfxf;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/List",
            "<",
            "Ldrv;",
            ">;",
            "Lfxf;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 171
    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lfxg;->k:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 174
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 176
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v3

    .line 177
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    .line 178
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldrv;

    .line 180
    iget-object v4, v0, Ldrv;->name:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 181
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 171
    :pswitch_1
    sget-object v0, Lfxg;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lfxg;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 185
    :cond_2
    iget-object v0, p0, Lfxg;->m:Lfwe;

    invoke-virtual {v0, v2, p5, v3}, Lfwe;->a(Ljava/util/ArrayList;Lfxf;I)V

    .line 186
    iget-object v0, p0, Lfxg;->l:Landroid/widget/TextView;

    invoke-virtual {p0}, Lfxg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p2, p3}, Llhu;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    iget-object v0, p0, Lfxg;->n:Landroid/widget/TextView;

    invoke-direct {p0, p1, v2}, Lfxg;->a(ILjava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    return-void

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, -0x2

    .line 61
    invoke-super {p0, p1, p2, p3}, Lfwp;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    sget-boolean v0, Lfxg;->a:Z

    if-nez v0, :cond_0

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 66
    const v1, 0x7f0203bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfxg;->b:Landroid/graphics/drawable/Drawable;

    .line 67
    const v1, 0x7f0203bc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfxg;->c:Landroid/graphics/drawable/Drawable;

    .line 69
    const v1, 0x7f0d02d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxg;->e:I

    .line 71
    const v1, 0x7f0d02d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxg;->d:I

    .line 73
    const v1, 0x7f0d02d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxg;->f:I

    .line 75
    const v1, 0x7f0d02d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxg;->g:I

    .line 78
    const v1, 0x7f0d02c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxg;->h:I

    .line 80
    const v1, 0x7f0d02c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxg;->i:I

    .line 82
    const v1, 0x7f0d02c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lfxg;->j:I

    .line 84
    const/4 v0, 0x1

    sput-boolean v0, Lfxg;->a:Z

    .line 87
    :cond_0
    sget v0, Lfxg;->e:I

    sget v1, Lfxg;->f:I

    sget v2, Lfxg;->d:I

    sget v3, Lfxg;->g:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lfxg;->a(IIII)V

    .line 89
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxg;->l:Landroid/widget/TextView;

    .line 90
    iget-object v0, p0, Lfxg;->l:Landroid/widget/TextView;

    new-instance v1, Lfyf;

    invoke-direct {v1, v4, v4}, Lfyf;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 92
    iget-object v0, p0, Lfxg;->l:Landroid/widget/TextView;

    const/16 v1, 0xa

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 93
    iget-object v0, p0, Lfxg;->l:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfxg;->addView(Landroid/view/View;)V

    .line 95
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxg;->k:Landroid/widget/ImageView;

    .line 96
    iget-object v0, p0, Lfxg;->k:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lfxg;->addView(Landroid/view/View;)V

    .line 98
    new-instance v0, Lfwe;

    invoke-direct {v0, p1, p2, p3}, Lfwe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxg;->m:Lfwe;

    .line 99
    iget-object v0, p0, Lfxg;->m:Lfwe;

    invoke-virtual {p0, v0}, Lfxg;->addView(Landroid/view/View;)V

    .line 101
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxg;->n:Landroid/widget/TextView;

    .line 102
    iget-object v0, p0, Lfxg;->n:Landroid/widget/TextView;

    new-instance v1, Lfyf;

    invoke-direct {v1, v4, v4}, Lfyf;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    iget-object v0, p0, Lfxg;->n:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfxg;->addView(Landroid/view/View;)V

    .line 105
    return-void
.end method

.method protected measureChildren(II)V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/high16 v8, -0x80000000

    const/4 v1, 0x0

    .line 109
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 110
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 117
    iget-object v0, p0, Lfxg;->k:Landroid/widget/ImageView;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 121
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 120
    invoke-virtual {v0, v4, v5}, Landroid/widget/ImageView;->measure(II)V

    .line 123
    iget-object v0, p0, Lfxg;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    .line 125
    iget-object v4, p0, Lfxg;->k:Landroid/widget/ImageView;

    invoke-static {v4, v1, v1}, Lfxg;->a(Landroid/view/View;II)V

    .line 126
    sub-int v4, v2, v0

    .line 129
    iget-object v5, p0, Lfxg;->l:Landroid/widget/TextView;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 130
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 129
    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->measure(II)V

    .line 132
    iget-object v5, p0, Lfxg;->l:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    .line 133
    sub-int v6, v2, v5

    .line 134
    iget-object v7, p0, Lfxg;->l:Landroid/widget/TextView;

    invoke-static {v7, v6, v1}, Lfxg;->a(Landroid/view/View;II)V

    .line 136
    sub-int/2addr v4, v5

    .line 139
    sget v5, Lfxg;->h:I

    sget v6, Lfxg;->i:I

    add-int/2addr v5, v6

    sub-int/2addr v4, v5

    .line 140
    iget-object v5, p0, Lfxg;->m:Lfwe;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 141
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 140
    invoke-virtual {v5, v4, v6}, Lfwe;->measure(II)V

    .line 142
    if-lez v0, :cond_0

    sget v4, Lfxg;->h:I

    add-int/2addr v0, v4

    :goto_0
    add-int/lit8 v0, v0, 0x0

    .line 143
    iget-object v4, p0, Lfxg;->m:Lfwe;

    invoke-static {v4, v0, v1}, Lfxg;->a(Landroid/view/View;II)V

    .line 147
    new-array v0, v11, [Landroid/view/View;

    iget-object v4, p0, Lfxg;->m:Lfwe;

    aput-object v4, v0, v1

    iget-object v4, p0, Lfxg;->l:Landroid/widget/TextView;

    aput-object v4, v0, v9

    iget-object v4, p0, Lfxg;->k:Landroid/widget/ImageView;

    aput-object v4, v0, v10

    invoke-static {v0}, Lfxg;->a([Landroid/view/View;)I

    move-result v0

    .line 148
    new-array v4, v11, [Landroid/view/View;

    iget-object v5, p0, Lfxg;->m:Lfwe;

    aput-object v5, v4, v1

    iget-object v5, p0, Lfxg;->l:Landroid/widget/TextView;

    aput-object v5, v4, v9

    iget-object v5, p0, Lfxg;->k:Landroid/widget/ImageView;

    aput-object v5, v4, v10

    invoke-static {v0, v4}, Lfxg;->a(I[Landroid/view/View;)V

    .line 151
    iget-object v0, p0, Lfxg;->m:Lfwe;

    .line 152
    invoke-virtual {v0}, Lfwe;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    sget v4, Lfxg;->j:I

    add-int/2addr v0, v4

    .line 153
    iget-object v4, p0, Lfxg;->n:Landroid/widget/TextView;

    invoke-static {v4, v1, v0}, Lfxg;->a(Landroid/view/View;II)V

    .line 156
    iget-object v1, p0, Lfxg;->n:Landroid/widget/TextView;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    sub-int v0, v3, v0

    .line 158
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 157
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 156
    invoke-virtual {v1, v2, v0}, Landroid/widget/TextView;->measure(II)V

    .line 159
    return-void

    :cond_0
    move v0, v1

    .line 142
    goto :goto_0
.end method
