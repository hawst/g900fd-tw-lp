.class public final Loie;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loie;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1292
    const/4 v0, 0x0

    new-array v0, v0, [Loie;

    sput-object v0, Loie;->a:[Loie;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1293
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1319
    const/4 v0, 0x0

    .line 1320
    iget-object v1, p0, Loie;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1321
    const/4 v0, 0x1

    iget-object v1, p0, Loie;->b:Ljava/lang/String;

    .line 1322
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1324
    :cond_0
    iget-object v1, p0, Loie;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1325
    const/4 v1, 0x2

    iget-object v2, p0, Loie;->c:Ljava/lang/Integer;

    .line 1326
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1328
    :cond_1
    iget-object v1, p0, Loie;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1329
    const/4 v1, 0x3

    iget-object v2, p0, Loie;->d:Ljava/lang/String;

    .line 1330
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1332
    :cond_2
    iget-object v1, p0, Loie;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1333
    iput v0, p0, Loie;->ai:I

    .line 1334
    return v0
.end method

.method public a(Loxn;)Loie;
    .locals 2

    .prologue
    .line 1342
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1343
    sparse-switch v0, :sswitch_data_0

    .line 1347
    iget-object v1, p0, Loie;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1348
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loie;->ah:Ljava/util/List;

    .line 1351
    :cond_1
    iget-object v1, p0, Loie;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1353
    :sswitch_0
    return-object p0

    .line 1358
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loie;->b:Ljava/lang/String;

    goto :goto_0

    .line 1362
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loie;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1366
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loie;->d:Ljava/lang/String;

    goto :goto_0

    .line 1343
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1304
    iget-object v0, p0, Loie;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1305
    const/4 v0, 0x1

    iget-object v1, p0, Loie;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1307
    :cond_0
    iget-object v0, p0, Loie;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1308
    const/4 v0, 0x2

    iget-object v1, p0, Loie;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1310
    :cond_1
    iget-object v0, p0, Loie;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1311
    const/4 v0, 0x3

    iget-object v1, p0, Loie;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1313
    :cond_2
    iget-object v0, p0, Loie;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1315
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1289
    invoke-virtual {p0, p1}, Loie;->a(Loxn;)Loie;

    move-result-object v0

    return-object v0
.end method
