.class public final Loso;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Lovp;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Losv;

.field private f:[Losp;

.field private g:I

.field private h:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 362
    invoke-direct {p0}, Loxq;-><init>()V

    .line 475
    sget-object v0, Lovp;->a:[Lovp;

    iput-object v0, p0, Loso;->b:[Lovp;

    .line 482
    const/4 v0, 0x0

    iput-object v0, p0, Loso;->e:Losv;

    .line 485
    sget-object v0, Losp;->a:[Losp;

    iput-object v0, p0, Loso;->f:[Losp;

    .line 488
    const/high16 v0, -0x80000000

    iput v0, p0, Loso;->g:I

    .line 491
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Loso;->h:[Ljava/lang/Integer;

    .line 362
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 536
    .line 537
    iget-object v0, p0, Loso;->a:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 538
    const/4 v0, 0x1

    iget-object v2, p0, Loso;->a:Ljava/lang/String;

    .line 539
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 541
    :goto_0
    iget-object v2, p0, Loso;->c:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 542
    const/4 v2, 0x2

    iget-object v3, p0, Loso;->c:Ljava/lang/Long;

    .line 543
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 545
    :cond_0
    iget-object v2, p0, Loso;->d:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 546
    const/4 v2, 0x3

    iget-object v3, p0, Loso;->d:Ljava/lang/Long;

    .line 547
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 549
    :cond_1
    iget-object v2, p0, Loso;->f:[Losp;

    if-eqz v2, :cond_3

    .line 550
    iget-object v3, p0, Loso;->f:[Losp;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 551
    if-eqz v5, :cond_2

    .line 552
    const/4 v6, 0x5

    .line 553
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 550
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 557
    :cond_3
    iget v2, p0, Loso;->g:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_4

    .line 558
    const/4 v2, 0x6

    iget v3, p0, Loso;->g:I

    .line 559
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 561
    :cond_4
    iget-object v2, p0, Loso;->b:[Lovp;

    if-eqz v2, :cond_6

    .line 562
    iget-object v3, p0, Loso;->b:[Lovp;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 563
    if-eqz v5, :cond_5

    .line 564
    const/4 v6, 0x7

    .line 565
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 562
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 569
    :cond_6
    iget-object v2, p0, Loso;->e:Losv;

    if-eqz v2, :cond_7

    .line 570
    const/16 v2, 0x9

    iget-object v3, p0, Loso;->e:Losv;

    .line 571
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 573
    :cond_7
    iget-object v2, p0, Loso;->h:[Ljava/lang/Integer;

    if-eqz v2, :cond_9

    iget-object v2, p0, Loso;->h:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 575
    iget-object v3, p0, Loso;->h:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_8

    aget-object v5, v3, v1

    .line 577
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 575
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 579
    :cond_8
    add-int/2addr v0, v2

    .line 580
    iget-object v1, p0, Loso;->h:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 582
    :cond_9
    iget-object v1, p0, Loso;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    iput v0, p0, Loso;->ai:I

    .line 584
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loso;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 592
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 593
    sparse-switch v0, :sswitch_data_0

    .line 597
    iget-object v2, p0, Loso;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 598
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loso;->ah:Ljava/util/List;

    .line 601
    :cond_1
    iget-object v2, p0, Loso;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 603
    :sswitch_0
    return-object p0

    .line 608
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loso;->a:Ljava/lang/String;

    goto :goto_0

    .line 612
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loso;->c:Ljava/lang/Long;

    goto :goto_0

    .line 616
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loso;->d:Ljava/lang/Long;

    goto :goto_0

    .line 620
    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 621
    iget-object v0, p0, Loso;->f:[Losp;

    if-nez v0, :cond_3

    move v0, v1

    .line 622
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Losp;

    .line 623
    iget-object v3, p0, Loso;->f:[Losp;

    if-eqz v3, :cond_2

    .line 624
    iget-object v3, p0, Loso;->f:[Losp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 626
    :cond_2
    iput-object v2, p0, Loso;->f:[Losp;

    .line 627
    :goto_2
    iget-object v2, p0, Loso;->f:[Losp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 628
    iget-object v2, p0, Loso;->f:[Losp;

    new-instance v3, Losp;

    invoke-direct {v3}, Losp;-><init>()V

    aput-object v3, v2, v0

    .line 629
    iget-object v2, p0, Loso;->f:[Losp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 630
    invoke-virtual {p1}, Loxn;->a()I

    .line 627
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 621
    :cond_3
    iget-object v0, p0, Loso;->f:[Losp;

    array-length v0, v0

    goto :goto_1

    .line 633
    :cond_4
    iget-object v2, p0, Loso;->f:[Losp;

    new-instance v3, Losp;

    invoke-direct {v3}, Losp;-><init>()V

    aput-object v3, v2, v0

    .line 634
    iget-object v2, p0, Loso;->f:[Losp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 638
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 639
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-ne v0, v2, :cond_6

    .line 641
    :cond_5
    iput v0, p0, Loso;->g:I

    goto/16 :goto_0

    .line 643
    :cond_6
    iput v1, p0, Loso;->g:I

    goto/16 :goto_0

    .line 648
    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 649
    iget-object v0, p0, Loso;->b:[Lovp;

    if-nez v0, :cond_8

    move v0, v1

    .line 650
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lovp;

    .line 651
    iget-object v3, p0, Loso;->b:[Lovp;

    if-eqz v3, :cond_7

    .line 652
    iget-object v3, p0, Loso;->b:[Lovp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 654
    :cond_7
    iput-object v2, p0, Loso;->b:[Lovp;

    .line 655
    :goto_4
    iget-object v2, p0, Loso;->b:[Lovp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 656
    iget-object v2, p0, Loso;->b:[Lovp;

    new-instance v3, Lovp;

    invoke-direct {v3}, Lovp;-><init>()V

    aput-object v3, v2, v0

    .line 657
    iget-object v2, p0, Loso;->b:[Lovp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 658
    invoke-virtual {p1}, Loxn;->a()I

    .line 655
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 649
    :cond_8
    iget-object v0, p0, Loso;->b:[Lovp;

    array-length v0, v0

    goto :goto_3

    .line 661
    :cond_9
    iget-object v2, p0, Loso;->b:[Lovp;

    new-instance v3, Lovp;

    invoke-direct {v3}, Lovp;-><init>()V

    aput-object v3, v2, v0

    .line 662
    iget-object v2, p0, Loso;->b:[Lovp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 666
    :sswitch_7
    iget-object v0, p0, Loso;->e:Losv;

    if-nez v0, :cond_a

    .line 667
    new-instance v0, Losv;

    invoke-direct {v0}, Losv;-><init>()V

    iput-object v0, p0, Loso;->e:Losv;

    .line 669
    :cond_a
    iget-object v0, p0, Loso;->e:Losv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 673
    :sswitch_8
    const/16 v0, 0x50

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 674
    iget-object v0, p0, Loso;->h:[Ljava/lang/Integer;

    array-length v0, v0

    .line 675
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/Integer;

    .line 676
    iget-object v3, p0, Loso;->h:[Ljava/lang/Integer;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 677
    iput-object v2, p0, Loso;->h:[Ljava/lang/Integer;

    .line 678
    :goto_5
    iget-object v2, p0, Loso;->h:[Ljava/lang/Integer;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 679
    iget-object v2, p0, Loso;->h:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 680
    invoke-virtual {p1}, Loxn;->a()I

    .line 678
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 683
    :cond_b
    iget-object v2, p0, Loso;->h:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 593
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 496
    iget-object v1, p0, Loso;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 497
    const/4 v1, 0x1

    iget-object v2, p0, Loso;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 499
    :cond_0
    iget-object v1, p0, Loso;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 500
    const/4 v1, 0x2

    iget-object v2, p0, Loso;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 502
    :cond_1
    iget-object v1, p0, Loso;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 503
    const/4 v1, 0x3

    iget-object v2, p0, Loso;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 505
    :cond_2
    iget-object v1, p0, Loso;->f:[Losp;

    if-eqz v1, :cond_4

    .line 506
    iget-object v2, p0, Loso;->f:[Losp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 507
    if-eqz v4, :cond_3

    .line 508
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 506
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 512
    :cond_4
    iget v1, p0, Loso;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_5

    .line 513
    const/4 v1, 0x6

    iget v2, p0, Loso;->g:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 515
    :cond_5
    iget-object v1, p0, Loso;->b:[Lovp;

    if-eqz v1, :cond_7

    .line 516
    iget-object v2, p0, Loso;->b:[Lovp;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 517
    if-eqz v4, :cond_6

    .line 518
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 516
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 522
    :cond_7
    iget-object v1, p0, Loso;->e:Losv;

    if-eqz v1, :cond_8

    .line 523
    const/16 v1, 0x9

    iget-object v2, p0, Loso;->e:Losv;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 525
    :cond_8
    iget-object v1, p0, Loso;->h:[Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 526
    iget-object v1, p0, Loso;->h:[Ljava/lang/Integer;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 527
    const/16 v4, 0xa

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 526
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 530
    :cond_9
    iget-object v0, p0, Loso;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 532
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 358
    invoke-virtual {p0, p1}, Loso;->a(Loxn;)Loso;

    move-result-object v0

    return-object v0
.end method
