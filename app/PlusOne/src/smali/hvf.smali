.class public final Lhvf;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lkdd;
.implements Lljh;


# instance fields
.field private final a:Lhve;

.field private b:Z

.field private c:Ljava/lang/CharSequence;

.field private d:Z

.field private e:Landroid/widget/TextView;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/widget/TextView;

.field private h:Z

.field private i:I

.field private j:Ljava/lang/String;

.field private k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private l:I

.field private m:Landroid/graphics/Bitmap;

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 102
    invoke-virtual {p0}, Lhvf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhve;->a(Landroid/content/Context;)Lhve;

    move-result-object v0

    iput-object v0, p0, Lhvf;->a:Lhve;

    .line 103
    invoke-virtual {p0, v1}, Lhvf;->setWillNotDraw(Z)V

    .line 105
    iput-boolean v1, p0, Lhvf;->b:Z

    .line 88
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 464
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvf;->b:Z

    .line 465
    iput-object v1, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    .line 467
    iput-object v1, p0, Lhvf;->j:Ljava/lang/String;

    .line 468
    iput-object v1, p0, Lhvf;->c:Ljava/lang/CharSequence;

    .line 469
    iput-object v1, p0, Lhvf;->f:Ljava/lang/CharSequence;

    .line 471
    invoke-virtual {p0}, Lhvf;->c()V

    .line 472
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 192
    invoke-virtual {p0}, Lhvf;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lhvf;->e:Landroid/widget/TextView;

    invoke-static {v0, v1, p1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 193
    return-void
.end method

.method public a(Lhul;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 121
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhvf;->setVisibility(I)V

    iput-object v4, p0, Lhvf;->j:Ljava/lang/String;

    iput v2, p0, Lhvf;->i:I

    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lhvf;->c()V

    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lhvf;->removeView(Landroid/view/View;)V

    :cond_0
    iput v2, p0, Lhvf;->l:I

    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhvf;->removeView(Landroid/view/View;)V

    :cond_1
    iput-object v4, p0, Lhvf;->c:Ljava/lang/CharSequence;

    iput-boolean v2, p0, Lhvf;->d:Z

    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhvf;->removeView(Landroid/view/View;)V

    :cond_2
    iput-object v4, p0, Lhvf;->f:Ljava/lang/CharSequence;

    iput-boolean v2, p0, Lhvf;->d:Z

    iput v2, p0, Lhvf;->n:I

    iput-object v4, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    .line 123
    if-nez p1, :cond_4

    .line 182
    :cond_3
    :goto_0
    return-void

    .line 127
    :cond_4
    iput-boolean v1, p0, Lhvf;->b:Z

    .line 129
    invoke-virtual {p1}, Lhul;->c()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lhvf;->c:Ljava/lang/CharSequence;

    .line 131
    iget-object v0, p0, Lhvf;->c:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lhvf;->d:Z

    .line 132
    iget-boolean v0, p0, Lhvf;->d:Z

    if-eqz v0, :cond_6

    .line 133
    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    if-nez v0, :cond_5

    .line 134
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lhvf;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    .line 135
    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lhvf;->a:Lhve;

    iget v3, v3, Lhve;->k:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 136
    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 138
    :cond_5
    invoke-virtual {p0}, Lhvf;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lhvf;->e:Landroid/widget/TextView;

    const/16 v4, 0x15

    invoke-static {v0, v3, v4}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 140
    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lhvf;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lhvf;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhvf;->addView(Landroid/view/View;)V

    .line 146
    :cond_6
    invoke-virtual {p1}, Lhul;->d()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lhvf;->f:Ljava/lang/CharSequence;

    .line 148
    iget-object v0, p0, Lhvf;->f:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lhvf;->h:Z

    .line 149
    iget-boolean v0, p0, Lhvf;->h:Z

    if-eqz v0, :cond_8

    .line 150
    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    if-nez v0, :cond_7

    .line 151
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lhvf;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    .line 152
    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lhvf;->a:Lhve;

    iget v3, v3, Lhve;->k:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 153
    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 155
    :cond_7
    invoke-virtual {p0}, Lhvf;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lhvf;->g:Landroid/widget/TextView;

    const/16 v4, 0xa

    invoke-static {v0, v3, v4}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 157
    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lhvf;->f:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lhvf;->f:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lhvf;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhvf;->addView(Landroid/view/View;)V

    .line 163
    :cond_8
    invoke-virtual {p1}, Lhul;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhvf;->j:Ljava/lang/String;

    .line 166
    iget-object v0, p0, Lhvf;->j:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 167
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-nez v0, :cond_9

    .line 168
    new-instance v0, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0}, Lhvf;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 169
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 170
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 172
    :cond_9
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p1}, Lhul;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lhvf;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v0}, Lhvf;->addView(Landroid/view/View;)V

    .line 176
    :cond_a
    invoke-virtual {p1}, Lhul;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    .line 179
    iget-boolean v0, p0, Lhvf;->d:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lhvf;->h:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lhvf;->j:Ljava/lang/String;

    if-nez v0, :cond_b

    iget-object v0, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 180
    :cond_b
    invoke-virtual {p0, v2}, Lhvf;->setVisibility(I)V

    goto/16 :goto_0

    :cond_c
    move v0, v2

    .line 131
    goto/16 :goto_1

    :cond_d
    move v0, v2

    .line 148
    goto/16 :goto_2
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 384
    invoke-virtual {p0}, Lhvf;->invalidate()V

    .line 385
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b()V

    .line 373
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 380
    :cond_0
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 5
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 349
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 350
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lhvf;->c:Ljava/lang/CharSequence;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 351
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lhvf;->f:Ljava/lang/CharSequence;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 353
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 358
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 359
    invoke-virtual {p0}, Lhvf;->b()V

    .line 360
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 364
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 365
    invoke-virtual {p0}, Lhvf;->c()V

    .line 366
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 310
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 312
    invoke-virtual {p0}, Lhvf;->getMeasuredWidth()I

    move-result v6

    .line 313
    invoke-virtual {p0}, Lhvf;->getMeasuredHeight()I

    move-result v0

    .line 316
    if-nez v0, :cond_1

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 320
    :cond_1
    iget-object v1, p0, Lhvf;->a:Lhve;

    iget v7, v1, Lhve;->j:I

    .line 322
    int-to-float v1, v7

    int-to-float v2, v0

    sub-int v3, v6, v7

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v0, p0, Lhvf;->a:Lhve;

    iget-object v5, v0, Lhve;->m:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 325
    iget-object v0, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 326
    sub-int v0, v6, v7

    iget-object v1, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 327
    iget-object v1, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    int-to-float v0, v0

    iget v2, p0, Lhvf;->n:I

    add-int/2addr v2, v7

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 279
    iget-object v0, p0, Lhvf;->a:Lhve;

    iget v1, v0, Lhve;->j:I

    .line 282
    iget-object v0, p0, Lhvf;->j:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 283
    iget v0, p0, Lhvf;->i:I

    add-int/2addr v0, v1

    .line 284
    iget-object v2, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v2

    .line 285
    iget-object v3, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    add-int v4, v1, v2

    add-int v5, v0, v2

    invoke-virtual {v3, v1, v0, v4, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 287
    add-int v0, v2, v1

    add-int/2addr v0, v1

    .line 290
    :goto_0
    iget v2, p0, Lhvf;->l:I

    add-int/2addr v1, v2

    .line 292
    iget-boolean v2, p0, Lhvf;->d:Z

    if-eqz v2, :cond_0

    .line 293
    iget-object v2, p0, Lhvf;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lhvf;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lhvf;->e:Landroid/widget/TextView;

    .line 294
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 293
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 295
    iget-object v2, p0, Lhvf;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    .line 298
    :cond_0
    iget-boolean v2, p0, Lhvf;->h:Z

    if-eqz v2, :cond_1

    .line 299
    iget-object v2, p0, Lhvf;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lhvf;->g:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lhvf;->g:Landroid/widget/TextView;

    .line 300
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    .line 299
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 302
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 213
    iget-boolean v0, p0, Lhvf;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " expected to have been bound with valid data. Was bind() called?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_0
    invoke-virtual {p0}, Lhvf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lhvj;->a(Landroid/content/Context;I)I

    move-result v2

    .line 216
    iput v1, p0, Lhvf;->l:I

    .line 218
    iget-object v0, p0, Lhvf;->a:Lhve;

    iget v0, v0, Lhve;->j:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    .line 220
    iget-object v3, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    .line 221
    iget-object v3, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lhvf;->a:Lhve;

    iget v4, v4, Lhve;->j:I

    sub-int/2addr v3, v4

    sub-int/2addr v0, v3

    .line 224
    :cond_1
    iget-object v3, p0, Lhvf;->j:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 225
    iget-object v3, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v3

    iget-object v4, p0, Lhvf;->a:Lhve;

    iget v4, v4, Lhve;->j:I

    sub-int/2addr v3, v4

    sub-int/2addr v0, v3

    .line 228
    :cond_2
    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 229
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 232
    iget-boolean v0, p0, Lhvf;->d:Z

    if-eqz v0, :cond_b

    .line 233
    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 234
    iget-object v0, p0, Lhvf;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 237
    :goto_0
    iget-boolean v1, p0, Lhvf;->h:Z

    if-eqz v1, :cond_3

    .line 238
    iget-object v1, p0, Lhvf;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 239
    iget-object v1, p0, Lhvf;->g:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 244
    :cond_3
    iget-object v1, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_a

    .line 245
    iget-object v1, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 248
    :goto_1
    iget-object v3, p0, Lhvf;->j:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 249
    iget-object v3, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 254
    :cond_4
    iget-boolean v3, p0, Lhvf;->d:Z

    if-nez v3, :cond_5

    iget-boolean v3, p0, Lhvf;->h:Z

    if-eqz v3, :cond_6

    :cond_5
    if-eq v1, v0, :cond_6

    .line 255
    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lhvf;->l:I

    .line 258
    :cond_6
    iget-object v0, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v1, v0, :cond_7

    .line 259
    iget-object v0, p0, Lhvf;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lhvf;->n:I

    .line 262
    :cond_7
    iget-object v0, p0, Lhvf;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v0

    if-eq v1, v0, :cond_8

    .line 263
    iget-object v0, p0, Lhvf;->k:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lhvf;->i:I

    .line 267
    :cond_8
    if-lez v1, :cond_9

    .line 268
    iget-object v0, p0, Lhvf;->a:Lhve;

    iget v0, v0, Lhve;->j:I

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v1, v0

    .line 271
    :cond_9
    invoke-virtual {p0, v2, v1}, Lhvf;->setMeasuredDimension(II)V

    .line 272
    return-void

    :cond_a
    move v1, v0

    goto :goto_1

    :cond_b
    move v0, v1

    goto :goto_0
.end method
