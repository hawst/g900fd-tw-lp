.class public final Lhje;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llov;
.implements Llqz;
.implements Llrg;


# instance fields
.field private final a:Lu;

.field private final b:Lhjj;

.field private c:Lhjf;

.field private d:Z

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lu;Llqr;Lhjj;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhje;->e:Z

    .line 46
    iput-object p1, p0, Lhje;->a:Lu;

    .line 47
    iput-object p3, p0, Lhje;->b:Lhjj;

    .line 48
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 49
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lhje;->c:Lhjf;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lhje;->c:Lhjf;

    invoke-interface {v0}, Lhjf;->b()V

    .line 73
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lhjf;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjf;

    iput-object v0, p0, Lhje;->c:Lhjf;

    .line 54
    const-class v0, Llot;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llot;

    .line 55
    invoke-virtual {v0, p0}, Llot;->a(Llov;)Llov;

    .line 56
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lhje;->a:Lu;

    invoke-virtual {v0}, Lu;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ActionBarFragmentMixin.Enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lhje;->e:Z

    .line 61
    return-void
.end method

.method public a(Lhjj;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lhje;->c:Lhjf;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhje;->e:Z

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lhje;->c:Lhjf;

    invoke-interface {v0, p1}, Lhjf;->c(Lhjj;)V

    .line 79
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 65
    iput-boolean p1, p0, Lhje;->d:Z

    .line 66
    iget-boolean v0, p0, Lhje;->d:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lhje;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lhje;->f:Z

    if-eq v0, v1, :cond_0

    iput-boolean v0, p0, Lhje;->f:Z

    iget-object v1, p0, Lhje;->c:Lhjf;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhje;->c:Lhjf;

    iget-object v1, p0, Lhje;->b:Lhjj;

    invoke-interface {v0, v1}, Lhjf;->a(Lhjj;)Lhjf;

    .line 67
    :cond_0
    :goto_1
    return-void

    .line 66
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhje;->c:Lhjf;

    iget-object v1, p0, Lhje;->b:Lhjj;

    invoke-interface {v0, v1}, Lhjf;->b(Lhjj;)Lhjf;

    goto :goto_1
.end method

.method public b(Lhjj;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lhje;->c:Lhjf;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lhje;->e:Z

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lhje;->c:Lhjf;

    invoke-interface {v0, p1}, Lhjf;->d(Lhjj;)V

    .line 85
    :cond_0
    return-void
.end method
