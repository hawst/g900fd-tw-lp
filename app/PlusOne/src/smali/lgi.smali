.class public final enum Llgi;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llgi;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Llgi;

.field private static enum b:Llgi;

.field private static final synthetic d:[Llgi;


# instance fields
.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 65
    new-instance v0, Llgi;

    const-string v1, "LONG"

    const/16 v2, 0x1388

    invoke-direct {v0, v1, v3, v2}, Llgi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llgi;->a:Llgi;

    .line 66
    new-instance v0, Llgi;

    const-string v1, "SHORT"

    const/16 v2, 0x3e8

    invoke-direct {v0, v1, v4, v2}, Llgi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Llgi;->b:Llgi;

    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [Llgi;

    sget-object v1, Llgi;->a:Llgi;

    aput-object v1, v0, v3

    sget-object v1, Llgi;->b:Llgi;

    aput-object v1, v0, v4

    sput-object v0, Llgi;->d:[Llgi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 71
    iput p3, p0, Llgi;->c:I

    .line 72
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llgi;
    .locals 1

    .prologue
    .line 64
    const-class v0, Llgi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llgi;

    return-object v0
.end method

.method public static values()[Llgi;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Llgi;->d:[Llgi;

    invoke-virtual {v0}, [Llgi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llgi;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Llgi;->c:I

    return v0
.end method
