.class public final Logl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Logl;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5834
    const/4 v0, 0x0

    new-array v0, v0, [Logl;

    sput-object v0, Logl;->a:[Logl;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5835
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5856
    const/4 v0, 0x0

    .line 5857
    iget-object v1, p0, Logl;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5858
    const/4 v0, 0x1

    iget-object v1, p0, Logl;->b:Ljava/lang/String;

    .line 5859
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5861
    :cond_0
    iget-object v1, p0, Logl;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5862
    const/4 v1, 0x2

    iget-object v2, p0, Logl;->c:Ljava/lang/String;

    .line 5863
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5865
    :cond_1
    iget-object v1, p0, Logl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5866
    iput v0, p0, Logl;->ai:I

    .line 5867
    return v0
.end method

.method public a(Loxn;)Logl;
    .locals 2

    .prologue
    .line 5875
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5876
    sparse-switch v0, :sswitch_data_0

    .line 5880
    iget-object v1, p0, Logl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5881
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Logl;->ah:Ljava/util/List;

    .line 5884
    :cond_1
    iget-object v1, p0, Logl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5886
    :sswitch_0
    return-object p0

    .line 5891
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logl;->b:Ljava/lang/String;

    goto :goto_0

    .line 5895
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logl;->c:Ljava/lang/String;

    goto :goto_0

    .line 5876
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5844
    iget-object v0, p0, Logl;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 5845
    const/4 v0, 0x1

    iget-object v1, p0, Logl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5847
    :cond_0
    iget-object v0, p0, Logl;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5848
    const/4 v0, 0x2

    iget-object v1, p0, Logl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5850
    :cond_1
    iget-object v0, p0, Logl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5852
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5831
    invoke-virtual {p0, p1}, Logl;->a(Loxn;)Logl;

    move-result-object v0

    return-object v0
.end method
