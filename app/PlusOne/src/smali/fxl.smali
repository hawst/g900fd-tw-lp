.class public final Lfxl;
.super Lgbz;
.source "PG"


# instance fields
.field private A:Lpbl;

.field private y:Lfxk;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfxl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfxl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    new-instance v0, Lfxk;

    invoke-virtual {p0}, Lfxl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lfxk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfxl;->y:Lfxk;

    .line 55
    iget-object v0, p0, Lfxl;->y:Lfxk;

    invoke-virtual {v0, p0}, Lfxk;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    return-void
.end method


# virtual methods
.method protected a(III)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 89
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {p3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 90
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 91
    iget-object v2, p0, Lfxl;->y:Lfxk;

    invoke-virtual {v2, v0, v1}, Lfxk;->measure(II)V

    .line 92
    iget-object v0, p0, Lfxl;->y:Lfxk;

    iget-object v1, p0, Lfxl;->y:Lfxk;

    invoke-virtual {v1}, Lfxk;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p1

    iget-object v2, p0, Lfxl;->y:Lfxk;

    invoke-virtual {v2}, Lfxk;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {v0, p1, p2, v1, v2}, Lfxk;->layout(IIII)V

    .line 94
    iget-object v0, p0, Lfxl;->y:Lfxk;

    invoke-virtual {v0}, Lfxk;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lfxl;->y:Lfxk;

    invoke-virtual {v0}, Lfxk;->getHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Lgbz;->a()V

    .line 105
    iget-object v0, p0, Lfxl;->y:Lfxk;

    invoke-virtual {v0}, Lfxk;->a()V

    .line 106
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 60
    const/16 v0, 0x17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 63
    :try_start_0
    new-instance v1, Lpbl;

    invoke-direct {v1}, Lpbl;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lpbl;

    iput-object v0, p0, Lfxl;->A:Lpbl;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    const-string v1, "EventCardViewGroup"

    const-string v2, "Could not deserialize PlusEventV2 from Blob."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lfxl;->y:Lfxk;

    invoke-virtual {v0, p1}, Lfxk;->a(Ljava/lang/StringBuilder;)V

    .line 80
    return-void
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method protected a_(Landroid/database/Cursor;Llcr;I)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lfxl;->y:Lfxk;

    invoke-virtual {p0, v0}, Lfxl;->removeView(Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lfxl;->y:Lfxk;

    iget-object v1, p0, Lfxl;->A:Lpbl;

    invoke-virtual {v0, p0, v1}, Lfxk;->a(Lfxl;Lpbl;)V

    .line 74
    iget-object v0, p0, Lfxl;->y:Lfxk;

    invoke-virtual {p0, v0}, Lfxl;->addView(Landroid/view/View;)V

    .line 75
    return-void
.end method

.method public b(Z)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 119
    iget-object v0, p0, Lfxl;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    invoke-virtual {p0}, Lfxl;->q()I

    move-result v0

    .line 123
    invoke-virtual {p0}, Lfxl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 124
    iget-object v2, p0, Lfxl;->A:Lpbl;

    iget-object v2, v2, Lpbl;->g:Ljava/lang/String;

    iget-object v3, p0, Lfxl;->A:Lpbl;

    iget-object v3, v3, Lpbl;->d:Ljava/lang/String;

    iget-object v4, p0, Lfxl;->l:[B

    const/4 v5, 0x0

    invoke-static {v1, v0, v2, v3, v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "promoted_post_data"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 134
    :cond_0
    :goto_0
    return-object v0

    .line 127
    :cond_1
    invoke-super {p0, p1}, Lgbz;->b(Z)Landroid/content/Intent;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_0

    .line 129
    const-string v1, "event_id"

    iget-object v2, p0, Lfxl;->A:Lpbl;

    iget-object v2, v2, Lpbl;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    const-string v1, "owner_id"

    iget-object v2, p0, Lfxl;->A:Lpbl;

    iget-object v2, v2, Lpbl;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 110
    iget-object v0, p0, Lfxl;->y:Lfxk;

    if-ne p1, v0, :cond_0

    .line 111
    iget-object v0, p0, Lfxl;->i:Lfdp;

    iget-object v1, p0, Lfxl;->A:Lpbl;

    iget-object v1, v1, Lpbl;->g:Ljava/lang/String;

    iget-object v2, p0, Lfxl;->A:Lpbl;

    iget-object v2, v2, Lpbl;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lfdp;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    invoke-super {p0, p1}, Lgbz;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method
