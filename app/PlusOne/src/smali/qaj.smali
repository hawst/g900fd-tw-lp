.class public final Lqaj;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lqak;

.field private b:Ljava/lang/Integer;

.field private c:[Ljava/lang/String;

.field private d:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1445
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1534
    sget-object v0, Lqak;->a:[Lqak;

    iput-object v0, p0, Lqaj;->a:[Lqak;

    .line 1539
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqaj;->c:[Ljava/lang/String;

    .line 1542
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqaj;->d:[Ljava/lang/String;

    .line 1445
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1573
    .line 1574
    iget-object v0, p0, Lqaj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 1575
    const/4 v0, 0x1

    iget-object v2, p0, Lqaj;->b:Ljava/lang/Integer;

    .line 1576
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1578
    :goto_0
    iget-object v2, p0, Lqaj;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lqaj;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1580
    iget-object v4, p0, Lqaj;->c:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 1582
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1580
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1584
    :cond_0
    add-int/2addr v0, v3

    .line 1585
    iget-object v2, p0, Lqaj;->c:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1587
    :cond_1
    iget-object v2, p0, Lqaj;->d:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lqaj;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 1589
    iget-object v4, p0, Lqaj;->d:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 1591
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1589
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1593
    :cond_2
    add-int/2addr v0, v3

    .line 1594
    iget-object v2, p0, Lqaj;->d:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1596
    :cond_3
    iget-object v2, p0, Lqaj;->a:[Lqak;

    if-eqz v2, :cond_5

    .line 1597
    iget-object v2, p0, Lqaj;->a:[Lqak;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 1598
    if-eqz v4, :cond_4

    .line 1599
    const/4 v5, 0x4

    .line 1600
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1597
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1604
    :cond_5
    iget-object v1, p0, Lqaj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1605
    iput v0, p0, Lqaj;->ai:I

    .line 1606
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lqaj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1614
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1615
    sparse-switch v0, :sswitch_data_0

    .line 1619
    iget-object v2, p0, Lqaj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1620
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lqaj;->ah:Ljava/util/List;

    .line 1623
    :cond_1
    iget-object v2, p0, Lqaj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1625
    :sswitch_0
    return-object p0

    .line 1630
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqaj;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1634
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1635
    iget-object v0, p0, Lqaj;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 1636
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1637
    iget-object v3, p0, Lqaj;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1638
    iput-object v2, p0, Lqaj;->c:[Ljava/lang/String;

    .line 1639
    :goto_1
    iget-object v2, p0, Lqaj;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 1640
    iget-object v2, p0, Lqaj;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1641
    invoke-virtual {p1}, Loxn;->a()I

    .line 1639
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1644
    :cond_2
    iget-object v2, p0, Lqaj;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 1648
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1649
    iget-object v0, p0, Lqaj;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 1650
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1651
    iget-object v3, p0, Lqaj;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1652
    iput-object v2, p0, Lqaj;->d:[Ljava/lang/String;

    .line 1653
    :goto_2
    iget-object v2, p0, Lqaj;->d:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 1654
    iget-object v2, p0, Lqaj;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1655
    invoke-virtual {p1}, Loxn;->a()I

    .line 1653
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1658
    :cond_3
    iget-object v2, p0, Lqaj;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 1662
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1663
    iget-object v0, p0, Lqaj;->a:[Lqak;

    if-nez v0, :cond_5

    move v0, v1

    .line 1664
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lqak;

    .line 1665
    iget-object v3, p0, Lqaj;->a:[Lqak;

    if-eqz v3, :cond_4

    .line 1666
    iget-object v3, p0, Lqaj;->a:[Lqak;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1668
    :cond_4
    iput-object v2, p0, Lqaj;->a:[Lqak;

    .line 1669
    :goto_4
    iget-object v2, p0, Lqaj;->a:[Lqak;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1670
    iget-object v2, p0, Lqaj;->a:[Lqak;

    new-instance v3, Lqak;

    invoke-direct {v3}, Lqak;-><init>()V

    aput-object v3, v2, v0

    .line 1671
    iget-object v2, p0, Lqaj;->a:[Lqak;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1672
    invoke-virtual {p1}, Loxn;->a()I

    .line 1669
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1663
    :cond_5
    iget-object v0, p0, Lqaj;->a:[Lqak;

    array-length v0, v0

    goto :goto_3

    .line 1675
    :cond_6
    iget-object v2, p0, Lqaj;->a:[Lqak;

    new-instance v3, Lqak;

    invoke-direct {v3}, Lqak;-><init>()V

    aput-object v3, v2, v0

    .line 1676
    iget-object v2, p0, Lqaj;->a:[Lqak;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1615
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1547
    iget-object v1, p0, Lqaj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1548
    const/4 v1, 0x1

    iget-object v2, p0, Lqaj;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1550
    :cond_0
    iget-object v1, p0, Lqaj;->c:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1551
    iget-object v2, p0, Lqaj;->c:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1552
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1551
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1555
    :cond_1
    iget-object v1, p0, Lqaj;->d:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1556
    iget-object v2, p0, Lqaj;->d:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1557
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1556
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1560
    :cond_2
    iget-object v1, p0, Lqaj;->a:[Lqak;

    if-eqz v1, :cond_4

    .line 1561
    iget-object v1, p0, Lqaj;->a:[Lqak;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 1562
    if-eqz v3, :cond_3

    .line 1563
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1561
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1567
    :cond_4
    iget-object v0, p0, Lqaj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1569
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1441
    invoke-virtual {p0, p1}, Lqaj;->a(Loxn;)Lqaj;

    move-result-object v0

    return-object v0
.end method
