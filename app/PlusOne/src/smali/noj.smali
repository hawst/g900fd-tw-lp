.class public final Lnoj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Lnok;

.field public d:[Lnok;

.field public e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 263
    invoke-direct {p0}, Loxq;-><init>()V

    .line 270
    const/4 v0, 0x0

    iput-object v0, p0, Lnoj;->c:Lnok;

    .line 273
    sget-object v0, Lnok;->a:[Lnok;

    iput-object v0, p0, Lnoj;->d:[Lnok;

    .line 263
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 310
    .line 311
    iget-object v0, p0, Lnoj;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 312
    const/4 v0, 0x2

    iget-object v2, p0, Lnoj;->a:Ljava/lang/Boolean;

    .line 313
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 315
    :goto_0
    iget-object v2, p0, Lnoj;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 316
    const/4 v2, 0x3

    iget-object v3, p0, Lnoj;->b:Ljava/lang/Boolean;

    .line 317
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 319
    :cond_0
    iget-object v2, p0, Lnoj;->c:Lnok;

    if-eqz v2, :cond_1

    .line 320
    const/4 v2, 0x4

    iget-object v3, p0, Lnoj;->c:Lnok;

    .line 321
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 323
    :cond_1
    iget-object v2, p0, Lnoj;->d:[Lnok;

    if-eqz v2, :cond_3

    .line 324
    iget-object v2, p0, Lnoj;->d:[Lnok;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 325
    if-eqz v4, :cond_2

    .line 326
    const/4 v5, 0x5

    .line 327
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 324
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 331
    :cond_3
    iget-object v1, p0, Lnoj;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 332
    const/4 v1, 0x6

    iget-object v2, p0, Lnoj;->e:Ljava/lang/Boolean;

    .line 333
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 335
    :cond_4
    iget-object v1, p0, Lnoj;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 336
    const/4 v1, 0x7

    iget-object v2, p0, Lnoj;->f:Ljava/lang/Boolean;

    .line 337
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 339
    :cond_5
    iget-object v1, p0, Lnoj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 340
    iput v0, p0, Lnoj;->ai:I

    .line 341
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnoj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 349
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 350
    sparse-switch v0, :sswitch_data_0

    .line 354
    iget-object v2, p0, Lnoj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 355
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnoj;->ah:Ljava/util/List;

    .line 358
    :cond_1
    iget-object v2, p0, Lnoj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    :sswitch_0
    return-object p0

    .line 365
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoj;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 369
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoj;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 373
    :sswitch_3
    iget-object v0, p0, Lnoj;->c:Lnok;

    if-nez v0, :cond_2

    .line 374
    new-instance v0, Lnok;

    invoke-direct {v0}, Lnok;-><init>()V

    iput-object v0, p0, Lnoj;->c:Lnok;

    .line 376
    :cond_2
    iget-object v0, p0, Lnoj;->c:Lnok;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 380
    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 381
    iget-object v0, p0, Lnoj;->d:[Lnok;

    if-nez v0, :cond_4

    move v0, v1

    .line 382
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnok;

    .line 383
    iget-object v3, p0, Lnoj;->d:[Lnok;

    if-eqz v3, :cond_3

    .line 384
    iget-object v3, p0, Lnoj;->d:[Lnok;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 386
    :cond_3
    iput-object v2, p0, Lnoj;->d:[Lnok;

    .line 387
    :goto_2
    iget-object v2, p0, Lnoj;->d:[Lnok;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 388
    iget-object v2, p0, Lnoj;->d:[Lnok;

    new-instance v3, Lnok;

    invoke-direct {v3}, Lnok;-><init>()V

    aput-object v3, v2, v0

    .line 389
    iget-object v2, p0, Lnoj;->d:[Lnok;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 390
    invoke-virtual {p1}, Loxn;->a()I

    .line 387
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 381
    :cond_4
    iget-object v0, p0, Lnoj;->d:[Lnok;

    array-length v0, v0

    goto :goto_1

    .line 393
    :cond_5
    iget-object v2, p0, Lnoj;->d:[Lnok;

    new-instance v3, Lnok;

    invoke-direct {v3}, Lnok;-><init>()V

    aput-object v3, v2, v0

    .line 394
    iget-object v2, p0, Lnoj;->d:[Lnok;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 398
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoj;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 402
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnoj;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 350
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x38 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 282
    iget-object v0, p0, Lnoj;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 283
    const/4 v0, 0x2

    iget-object v1, p0, Lnoj;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 285
    :cond_0
    iget-object v0, p0, Lnoj;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 286
    const/4 v0, 0x3

    iget-object v1, p0, Lnoj;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 288
    :cond_1
    iget-object v0, p0, Lnoj;->c:Lnok;

    if-eqz v0, :cond_2

    .line 289
    const/4 v0, 0x4

    iget-object v1, p0, Lnoj;->c:Lnok;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 291
    :cond_2
    iget-object v0, p0, Lnoj;->d:[Lnok;

    if-eqz v0, :cond_4

    .line 292
    iget-object v1, p0, Lnoj;->d:[Lnok;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 293
    if-eqz v3, :cond_3

    .line 294
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 292
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 298
    :cond_4
    iget-object v0, p0, Lnoj;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 299
    const/4 v0, 0x6

    iget-object v1, p0, Lnoj;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 301
    :cond_5
    iget-object v0, p0, Lnoj;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 302
    const/4 v0, 0x7

    iget-object v1, p0, Lnoj;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 304
    :cond_6
    iget-object v0, p0, Lnoj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 306
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 259
    invoke-virtual {p0, p1}, Lnoj;->a(Loxn;)Lnoj;

    move-result-object v0

    return-object v0
.end method
