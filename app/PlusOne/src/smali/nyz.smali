.class public final Lnyz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnyz;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lnza;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3144
    const/4 v0, 0x0

    new-array v0, v0, [Lnyz;

    sput-object v0, Lnyz;->a:[Lnyz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3145
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3166
    const/4 v0, 0x0

    iput-object v0, p0, Lnyz;->f:Lnza;

    .line 3145
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3205
    const/4 v0, 0x1

    iget-object v1, p0, Lnyz;->c:Ljava/lang/String;

    .line 3207
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3208
    iget-object v1, p0, Lnyz;->g:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3209
    const/4 v1, 0x2

    iget-object v2, p0, Lnyz;->g:Ljava/lang/String;

    .line 3210
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3212
    :cond_0
    iget-object v1, p0, Lnyz;->h:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3213
    const/4 v1, 0x3

    iget-object v2, p0, Lnyz;->h:Ljava/lang/String;

    .line 3214
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3216
    :cond_1
    iget-object v1, p0, Lnyz;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3217
    const/4 v1, 0x4

    iget-object v2, p0, Lnyz;->d:Ljava/lang/String;

    .line 3218
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3220
    :cond_2
    iget-object v1, p0, Lnyz;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 3221
    const/4 v1, 0x5

    iget-object v2, p0, Lnyz;->e:Ljava/lang/String;

    .line 3222
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3224
    :cond_3
    iget-object v1, p0, Lnyz;->j:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3225
    const/4 v1, 0x6

    iget-object v2, p0, Lnyz;->j:Ljava/lang/String;

    .line 3226
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3228
    :cond_4
    iget-object v1, p0, Lnyz;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 3229
    const/4 v1, 0x7

    iget-object v2, p0, Lnyz;->k:Ljava/lang/Integer;

    .line 3230
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3232
    :cond_5
    iget-object v1, p0, Lnyz;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 3233
    const/16 v1, 0x8

    iget-object v2, p0, Lnyz;->i:Ljava/lang/String;

    .line 3234
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3236
    :cond_6
    iget-object v1, p0, Lnyz;->b:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 3237
    const/16 v1, 0x9

    iget-object v2, p0, Lnyz;->b:Ljava/lang/String;

    .line 3238
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3240
    :cond_7
    iget-object v1, p0, Lnyz;->f:Lnza;

    if-eqz v1, :cond_8

    .line 3241
    const/16 v1, 0xa

    iget-object v2, p0, Lnyz;->f:Lnza;

    .line 3242
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3244
    :cond_8
    iget-object v1, p0, Lnyz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3245
    iput v0, p0, Lnyz;->ai:I

    .line 3246
    return v0
.end method

.method public a(Loxn;)Lnyz;
    .locals 2

    .prologue
    .line 3254
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3255
    sparse-switch v0, :sswitch_data_0

    .line 3259
    iget-object v1, p0, Lnyz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3260
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyz;->ah:Ljava/util/List;

    .line 3263
    :cond_1
    iget-object v1, p0, Lnyz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3265
    :sswitch_0
    return-object p0

    .line 3270
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyz;->c:Ljava/lang/String;

    goto :goto_0

    .line 3274
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyz;->g:Ljava/lang/String;

    goto :goto_0

    .line 3278
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyz;->h:Ljava/lang/String;

    goto :goto_0

    .line 3282
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyz;->d:Ljava/lang/String;

    goto :goto_0

    .line 3286
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyz;->e:Ljava/lang/String;

    goto :goto_0

    .line 3290
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyz;->j:Ljava/lang/String;

    goto :goto_0

    .line 3294
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnyz;->k:Ljava/lang/Integer;

    goto :goto_0

    .line 3298
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyz;->i:Ljava/lang/String;

    goto :goto_0

    .line 3302
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnyz;->b:Ljava/lang/String;

    goto :goto_0

    .line 3306
    :sswitch_a
    iget-object v0, p0, Lnyz;->f:Lnza;

    if-nez v0, :cond_2

    .line 3307
    new-instance v0, Lnza;

    invoke-direct {v0}, Lnza;-><init>()V

    iput-object v0, p0, Lnyz;->f:Lnza;

    .line 3309
    :cond_2
    iget-object v0, p0, Lnyz;->f:Lnza;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3255
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3171
    const/4 v0, 0x1

    iget-object v1, p0, Lnyz;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3172
    iget-object v0, p0, Lnyz;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3173
    const/4 v0, 0x2

    iget-object v1, p0, Lnyz;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3175
    :cond_0
    iget-object v0, p0, Lnyz;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3176
    const/4 v0, 0x3

    iget-object v1, p0, Lnyz;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3178
    :cond_1
    iget-object v0, p0, Lnyz;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3179
    const/4 v0, 0x4

    iget-object v1, p0, Lnyz;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3181
    :cond_2
    iget-object v0, p0, Lnyz;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3182
    const/4 v0, 0x5

    iget-object v1, p0, Lnyz;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3184
    :cond_3
    iget-object v0, p0, Lnyz;->j:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3185
    const/4 v0, 0x6

    iget-object v1, p0, Lnyz;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3187
    :cond_4
    iget-object v0, p0, Lnyz;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 3188
    const/4 v0, 0x7

    iget-object v1, p0, Lnyz;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3190
    :cond_5
    iget-object v0, p0, Lnyz;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 3191
    const/16 v0, 0x8

    iget-object v1, p0, Lnyz;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3193
    :cond_6
    iget-object v0, p0, Lnyz;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 3194
    const/16 v0, 0x9

    iget-object v1, p0, Lnyz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3196
    :cond_7
    iget-object v0, p0, Lnyz;->f:Lnza;

    if-eqz v0, :cond_8

    .line 3197
    const/16 v0, 0xa

    iget-object v1, p0, Lnyz;->f:Lnza;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3199
    :cond_8
    iget-object v0, p0, Lnyz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3201
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3141
    invoke-virtual {p0, p1}, Lnyz;->a(Loxn;)Lnyz;

    move-result-object v0

    return-object v0
.end method
