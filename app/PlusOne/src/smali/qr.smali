.class public final Lqr;
.super Lxn;
.source "PG"

# interfaces
.implements Lrm;


# instance fields
.field private a:Lxo;

.field private b:Lrl;

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic d:Lqn;


# direct methods
.method public constructor <init>(Lqn;Lxo;)V
    .locals 2

    .prologue
    .line 951
    iput-object p1, p0, Lqr;->d:Lqn;

    invoke-direct {p0}, Lxn;-><init>()V

    .line 952
    iput-object p2, p0, Lqr;->a:Lxo;

    .line 953
    new-instance v0, Lrl;

    invoke-virtual {p1}, Lqn;->i()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lrl;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lrl;->a(I)Lrl;

    move-result-object v0

    iput-object v0, p0, Lqr;->b:Lrl;

    .line 955
    iget-object v0, p0, Lqr;->b:Lrl;

    invoke-virtual {v0, p0}, Lrl;->a(Lrm;)V

    .line 956
    return-void
.end method


# virtual methods
.method public a()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 960
    new-instance v0, Lqy;

    iget-object v1, p0, Lqr;->d:Lqn;

    invoke-virtual {v1}, Lqn;->i()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lqy;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 1036
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->k(Lqn;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lqr;->b(Ljava/lang/CharSequence;)V

    .line 1037
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1020
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->d(Landroid/view/View;)V

    .line 1021
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lqr;->c:Ljava/lang/ref/WeakReference;

    .line 1022
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1026
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->b(Ljava/lang/CharSequence;)V

    .line 1027
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 1056
    invoke-super {p0, p1}, Lxn;->a(Z)V

    .line 1057
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Z)V

    .line 1058
    return-void
.end method

.method public a(Lrl;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1071
    iget-object v0, p0, Lqr;->a:Lxo;

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, Lqr;->a:Lxo;

    invoke-interface {v0, p0, p2}, Lxo;->a(Lxn;Landroid/view/MenuItem;)Z

    move-result v0

    .line 1074
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lqr;->b:Lrl;

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->k(Lqn;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lqr;->a(Ljava/lang/CharSequence;)V

    .line 1042
    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1031
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Ljava/lang/CharSequence;)V

    .line 1032
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 970
    iget-object v0, p0, Lqr;->d:Lqn;

    iget-object v0, v0, Lqn;->a:Lqr;

    if-eq v0, p0, :cond_0

    .line 997
    :goto_0
    return-void

    .line 979
    :cond_0
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->g(Lqn;)Z

    move-result v0

    iget-object v1, p0, Lqr;->d:Lqn;

    invoke-static {v1}, Lqn;->h(Lqn;)Z

    move-result v1

    invoke-static {v0, v1, v2}, Lqn;->a(ZZZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 982
    iget-object v0, p0, Lqr;->d:Lqn;

    iput-object p0, v0, Lqn;->b:Lxn;

    .line 983
    iget-object v0, p0, Lqr;->d:Lqn;

    iget-object v1, p0, Lqr;->a:Lxo;

    iput-object v1, v0, Lqn;->c:Lxo;

    .line 987
    :goto_1
    iput-object v3, p0, Lqr;->a:Lxo;

    .line 988
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-virtual {v0, v2}, Lqn;->m(Z)V

    .line 991
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->d()V

    .line 992
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->j(Lqn;)Ltp;

    move-result-object v0

    invoke-interface {v0}, Ltp;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    .line 994
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->f(Lqn;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    move-result-object v0

    iget-object v1, p0, Lqr;->d:Lqn;

    iget-boolean v1, v1, Lqn;->d:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->c(Z)V

    .line 996
    iget-object v0, p0, Lqr;->d:Lqn;

    iput-object v3, v0, Lqn;->a:Lqr;

    goto :goto_0

    .line 985
    :cond_1
    iget-object v0, p0, Lqr;->a:Lxo;

    invoke-interface {v0, p0}, Lxo;->a(Lxn;)V

    goto :goto_1
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1001
    iget-object v0, p0, Lqr;->b:Lrl;

    invoke-virtual {v0}, Lrl;->g()V

    .line 1003
    :try_start_0
    iget-object v0, p0, Lqr;->a:Lxo;

    iget-object v1, p0, Lqr;->b:Lrl;

    invoke-interface {v0, p0, v1}, Lxo;->b(Lxn;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1005
    iget-object v0, p0, Lqr;->b:Lrl;

    invoke-virtual {v0}, Lrl;->h()V

    .line 1006
    return-void

    .line 1005
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lqr;->b:Lrl;

    invoke-virtual {v1}, Lrl;->h()V

    throw v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 1010
    iget-object v0, p0, Lqr;->b:Lrl;

    invoke-virtual {v0}, Lrl;->g()V

    .line 1012
    :try_start_0
    iget-object v0, p0, Lqr;->a:Lxo;

    iget-object v1, p0, Lqr;->b:Lrl;

    invoke-interface {v0, p0, v1}, Lxo;->a(Lxn;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1014
    iget-object v1, p0, Lqr;->b:Lrl;

    invoke-virtual {v1}, Lrl;->h()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lqr;->b:Lrl;

    invoke-virtual {v1}, Lrl;->h()V

    throw v0
.end method

.method public f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1046
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1051
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->c()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->f()Z

    move-result v0

    return v0
.end method

.method public i()Landroid/view/View;
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, Lqr;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqr;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, Lqr;->a:Lxo;

    if-nez v0, :cond_0

    .line 1103
    :goto_0
    return-void

    .line 1101
    :cond_0
    invoke-virtual {p0}, Lqr;->d()V

    .line 1102
    iget-object v0, p0, Lqr;->d:Lqn;

    invoke-static {v0}, Lqn;->i(Lqn;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a()Z

    goto :goto_0
.end method
