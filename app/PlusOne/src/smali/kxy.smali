.class public final Lkxy;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lt;-><init>()V

    .line 27
    return-void
.end method

.method private U()Ljava/lang/String;
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lkxy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private V()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lkxy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lkxy;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 37
    const-string v1, "square_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v1, "activity_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    new-instance v1, Lkxy;

    invoke-direct {v1}, Lkxy;-><init>()V

    .line 41
    invoke-virtual {v1, v0}, Lkxy;->f(Landroid/os/Bundle;)V

    .line 42
    return-object v1
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 47
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lkxy;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 48
    const v1, 0x7f0a022b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a022c

    .line 49
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a022d

    .line 50
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a022e

    .line 51
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0597

    .line 52
    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 53
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 55
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 68
    const/4 v1, 0x0

    .line 70
    invoke-virtual {p0}, Lkxy;->u_()Lu;

    move-result-object v0

    .line 71
    instance-of v2, v0, Lkxz;

    if-eqz v2, :cond_1

    .line 72
    check-cast v0, Lkxz;

    .line 80
    :goto_0
    packed-switch p2, :pswitch_data_0

    .line 100
    :cond_0
    :goto_1
    return-void

    .line 74
    :cond_1
    invoke-virtual {p0}, Lkxy;->n()Lz;

    move-result-object v0

    .line 75
    instance-of v2, v0, Lkxz;

    if-eqz v2, :cond_2

    .line 76
    check-cast v0, Lkxz;

    goto :goto_0

    .line 82
    :pswitch_0
    if-eqz v0, :cond_0

    .line 83
    invoke-direct {p0}, Lkxy;->U()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lkxy;->V()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lkxz;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 89
    :pswitch_1
    if-eqz v0, :cond_0

    .line 90
    invoke-direct {p0}, Lkxy;->U()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lkxy;->V()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lkxz;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 96
    :pswitch_2
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
