.class public final Lhlq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhei;

.field private final c:Lkfd;

.field private d:Ljdg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    new-instance v0, Lhlr;

    invoke-direct {v0}, Lhlr;-><init>()V

    iput-object v0, p0, Lhlq;->d:Ljdg;

    .line 34
    iput-object p1, p0, Lhlq;->a:Landroid/content/Context;

    .line 35
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhlq;->b:Lhei;

    .line 36
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lhlq;->c:Lkfd;

    .line 37
    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/String;)Lhls;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 60
    invoke-virtual {p0, p3}, Lhlq;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lhlq;->b:Lhei;

    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    sget-object v0, Lhls;->a:Lhls;

    .line 87
    :goto_0
    return-object v0

    .line 69
    :cond_0
    new-instance v2, Ljvu;

    iget-object v3, p0, Lhlq;->a:Landroid/content/Context;

    new-array v4, v5, [Ljava/lang/String;

    aput-object v0, v4, v6

    .line 70
    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, p1, v1, v4}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    .line 73
    iget-object v1, p0, Lhlq;->c:Lkfd;

    invoke-interface {v1, v2}, Lkfd;->a(Lkff;)V

    .line 75
    invoke-virtual {v2}, Ljvu;->t()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2, v0}, Ljvu;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    invoke-virtual {v2, v0}, Ljvu;->c(Ljava/lang/String;)J

    move-result-wide v2

    .line 79
    new-instance v0, Ljty;

    iget-object v1, p0, Lhlq;->a:Landroid/content/Context;

    const/4 v4, 0x0

    new-array v5, v5, [Ljava/lang/String;

    .line 80
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Ljty;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 82
    iget-object v1, p0, Lhlq;->c:Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 83
    invoke-virtual {v0}, Ljty;->t()Z

    move-result v1

    if-nez v1, :cond_1

    .line 84
    invoke-virtual {v0, v6}, Ljty;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhls;->a(Ljava/lang/String;)Lhls;

    move-result-object v0

    goto :goto_0

    .line 87
    :cond_1
    sget-object v0, Lhls;->a:Lhls;

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 44
    :try_start_0
    iget-object v0, p0, Lhlq;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 45
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 46
    invoke-static {v0}, Ljbh;->a(Ljava/io/InputStream;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 47
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 48
    const-string v2, "UploadRpcExecutor"

    const-string v3, "cannot compute fingerprint for: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 49
    const/4 v0, 0x0

    goto :goto_0

    .line 48
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public b(ILjava/lang/String;Ljava/lang/String;)Lhls;
    .locals 8

    .prologue
    .line 97
    new-instance v0, Ljct;

    iget-object v1, p0, Lhlq;->a:Landroid/content/Context;

    iget-object v2, p0, Lhlq;->d:Ljdg;

    invoke-direct {v0, v1, p1, v2}, Ljct;-><init>(Landroid/content/Context;ILjdg;)V

    .line 99
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 104
    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v3, p2

    :try_start_0
    invoke-virtual/range {v0 .. v7}, Ljct;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)Ljdb;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Ljdb;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhls;->a(Ljava/lang/String;)Lhls;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    const-string v1, "UploadRpcExecutor"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    const-string v1, "UploadRpcExecutor"

    const-string v2, "Error while uploading photo to album: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 116
    :cond_0
    sget-object v0, Lhls;->a:Lhls;

    goto :goto_0
.end method
