.class public Lcyo;
.super Llol;
.source "PG"

# interfaces
.implements Lhob;
.implements Llgs;


# static fields
.field private static final N:Ldgq;


# instance fields
.field private final O:Lhoc;

.field private P:Ldgr;

.field private Q:Ldeo;

.field private R:Ldes;

.field private S:[B

.field private T:Lhee;

.field private U:Lhkd;

.field private final V:Lhke;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ldgq;

    const v1, 0x7f1006c1

    invoke-direct {v0, v1}, Ldgq;-><init>(I)V

    sput-object v0, Lcyo;->N:Ldgq;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 46
    invoke-direct {p0}, Llol;-><init>()V

    .line 66
    new-instance v0, Lhoc;

    iget-object v1, p0, Lcyo;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    .line 67
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lcyo;->O:Lhoc;

    .line 80
    new-instance v0, Lcyp;

    invoke-direct {v0, p0}, Lcyp;-><init>(Lcyo;)V

    iput-object v0, p0, Lcyo;->U:Lhkd;

    .line 89
    new-instance v0, Lhke;

    iget-object v1, p0, Lcyo;->av:Llqm;

    invoke-direct {v0, v1}, Lhke;-><init>(Llqr;)V

    iget-object v1, p0, Lcyo;->au:Llnh;

    .line 91
    invoke-virtual {v0, v1}, Lhke;->a(Llnh;)Lhke;

    move-result-object v0

    const v1, 0x7f1000bd

    iget-object v2, p0, Lcyo;->U:Lhkd;

    .line 92
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    iput-object v0, p0, Lcyo;->V:Lhke;

    .line 96
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcyo;->av:Llqm;

    sget-object v2, Lcyo;->N:Ldgq;

    new-instance v3, Lcyq;

    invoke-direct {v3, p0}, Lcyq;-><init>(Lcyo;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 102
    new-instance v0, Ldep;

    iget-object v1, p0, Lcyo;->av:Llqm;

    new-instance v2, Lcyr;

    invoke-direct {v2, p0}, Lcyr;-><init>(Lcyo;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 108
    new-instance v0, Ldet;

    iget-object v1, p0, Lcyo;->av:Llqm;

    new-instance v2, Lcys;

    invoke-direct {v2, p0}, Lcys;-><init>(Lcyo;)V

    invoke-direct {v0, v1, v2}, Ldet;-><init>(Llqr;Ldeu;)V

    .line 114
    return-void
.end method

.method private U()Z
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcyo;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    .line 213
    invoke-interface {v0}, Lddl;->a()Lizu;

    move-result-object v1

    invoke-virtual {v1}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljbd;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    invoke-interface {v0}, Lddl;->H()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private V()V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcyo;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    .line 222
    invoke-virtual {p0}, Lcyo;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 221
    invoke-interface {v0, v1}, Lddl;->c(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcyo;->V:Lhke;

    const v2, 0x7f1000bd

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 225
    return-void
.end method

.method static synthetic a(Lcyo;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 46
    invoke-virtual {p0}, Lcyo;->o()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0}, Lcyo;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcyo;->T:Lhee;

    invoke-interface {v3}, Lhee;->f()Z

    move-result v3

    if-eqz v3, :cond_0

    const v3, 0x7f0a08f3

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcyo;->a()V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcyo;->U()Z

    move-result v3

    if-eqz v3, :cond_2

    const v3, 0x7f0a08f4

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const v1, 0x7f0a08f2

    invoke-virtual {p0, v1}, Lcyo;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Llgr;->a(Ljava/lang/String;[Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0, p0, v4}, Llgr;->a(Lu;I)V

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "operation_list"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {p0}, Lcyo;->p()Lae;

    move-result-object v1

    const-string v2, "set_photo_as"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcyo;[B)[B
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcyo;->S:[B

    return-object p1
.end method

.method static synthetic b(Lcyo;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcyo;->d()V

    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 155
    iget-object v1, p0, Lcyo;->P:Ldgr;

    sget-object v2, Lcyo;->N:Ldgq;

    iget-object v0, p0, Lcyo;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->Q()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Lddl;->I()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v0}, Lddl;->F()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcyo;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcyo;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcyo;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Ldgr;->a(Ldgu;Z)V

    .line 156
    return-void

    .line 155
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 197
    iget-object v2, p0, Lcyo;->Q:Ldeo;

    invoke-virtual {v2}, Ldeo;->a()Lddl;

    move-result-object v2

    invoke-interface {v2}, Lddl;->D()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v0

    .line 201
    :cond_1
    iget-object v2, p0, Lcyo;->T:Lhee;

    invoke-interface {v2}, Lhee;->f()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 202
    goto :goto_0

    .line 206
    :cond_2
    iget-object v2, p0, Lcyo;->T:Lhee;

    invoke-interface {v2}, Lhee;->g()Lhej;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 207
    iget-object v3, p0, Lcyo;->R:Ldes;

    invoke-virtual {v3}, Ldes;->c()Ljava/util/List;

    move-result-object v3

    .line 208
    if-eqz v3, :cond_3

    invoke-interface {v3, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi",
            "NewApi"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lcyo;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-virtual {p0}, Lcyo;->n()Lz;

    move-result-object v1

    invoke-interface {v0, v1}, Lddl;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 243
    const v1, 0x7f0a0ad7

    invoke-virtual {p0, v1}, Lcyo;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcyo;->a(Landroid/content/Intent;)V

    .line 244
    return-void
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 265
    const-string v0, "operation_list"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 266
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 268
    packed-switch v0, :pswitch_data_0

    .line 282
    const-string v0, "PhotoSetAsFragment"

    const-string v1, "Unknown operation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    :goto_0
    return-void

    .line 270
    :pswitch_0
    iget-object v0, p0, Lcyo;->Q:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    invoke-direct {p0}, Lcyo;->V()V

    goto :goto_0

    .line 273
    :cond_0
    const/4 v0, 0x0

    const v1, 0x7f0a05f3

    invoke-virtual {p0, v1}, Lcyo;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0596

    invoke-virtual {p0, v2}, Lcyo;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Lcyo;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Lcyo;->p()Lae;

    move-result-object v1

    const-string v2, "confirm_profile_photo"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 278
    :pswitch_1
    invoke-virtual {p0}, Lcyo;->a()V

    goto :goto_0

    .line 268
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 289
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 248
    const-string v0, "confirm_profile_photo"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    invoke-direct {p0}, Lcyo;->V()V

    .line 251
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 295
    const-string v0, "UploadProfilePhotoTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    invoke-virtual {p0}, Lcyo;->p()Lae;

    move-result-object v0

    invoke-static {v0}, Ldhh;->a(Lae;)V

    .line 297
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    const/4 v0, 0x0

    iput-object v0, p0, Lcyo;->S:[B

    .line 299
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 5

    .prologue
    .line 127
    invoke-super {p0}, Llol;->aO_()V

    .line 128
    invoke-direct {p0}, Lcyo;->d()V

    .line 129
    iget-object v0, p0, Lcyo;->S:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcyo;->T:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iget-object v1, p0, Lcyo;->O:Lhoc;

    new-instance v2, Ldql;

    invoke-virtual {p0}, Lcyo;->n()Lz;

    move-result-object v3

    iget-object v4, p0, Lcyo;->S:[B

    invoke-direct {v2, v3, v0, v4}, Ldql;-><init>(Landroid/content/Context;I[B)V

    invoke-virtual {v1, v2}, Lhoc;->b(Lhny;)V

    invoke-virtual {p0}, Lcyo;->p()Lae;

    move-result-object v0

    const v1, 0x7f0a08f5

    invoke-virtual {p0, v1}, Lcyo;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldhh;->a(Lae;Ljava/lang/String;)V

    .line 130
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 256
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 119
    iget-object v0, p0, Lcyo;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcyo;->P:Ldgr;

    .line 120
    iget-object v0, p0, Lcyo;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcyo;->Q:Ldeo;

    .line 121
    iget-object v0, p0, Lcyo;->au:Llnh;

    const-class v1, Ldes;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldes;

    iput-object v0, p0, Lcyo;->R:Ldes;

    .line 122
    iget-object v0, p0, Lcyo;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcyo;->T:Lhee;

    .line 123
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 261
    return-void
.end method
