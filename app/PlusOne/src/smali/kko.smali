.class public final Lkko;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lkzv;

.field private b:Lpdg;

.field private c:Lpdp;

.field private d:Loyf;

.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lozv;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z


# direct methods
.method public constructor <init>(Loyf;)V
    .locals 4

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lkko;->e:I

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkko;->f:Ljava/util/List;

    .line 62
    new-instance v0, Lkzv;

    invoke-direct {v0, p1}, Lkzv;-><init>(Loyf;)V

    iput-object v0, p0, Lkko;->a:Lkzv;

    .line 63
    iput-object p1, p0, Lkko;->d:Loyf;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Loyf;->f:[Loya;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lkko;->f:Ljava/util/List;

    .line 66
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p1, Loyf;->f:[Loya;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 67
    iget-object v0, p0, Lkko;->f:Ljava/util/List;

    iget-object v2, p1, Loyf;->f:[Loya;

    aget-object v2, v2, v1

    sget-object v3, Lozv;->a:Loxr;

    invoke-virtual {v2, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v2, p1, Loyf;->c:Ljava/lang/String;

    iget-object v0, p0, Lkko;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lozv;

    iget-object v0, v0, Lozv;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    iput v1, p0, Lkko;->e:I

    .line 66
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 72
    :cond_1
    return-void
.end method

.method public constructor <init>(Lpdd;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lkko;->e:I

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkko;->f:Ljava/util/List;

    .line 75
    new-instance v0, Lkzv;

    invoke-direct {v0, p1}, Lkzv;-><init>(Lpdd;)V

    iput-object v0, p0, Lkko;->a:Lkzv;

    .line 76
    return-void
.end method

.method public constructor <init>(Lpdg;)V
    .locals 4

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lkko;->e:I

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkko;->f:Ljava/util/List;

    .line 36
    new-instance v0, Lkzv;

    invoke-direct {v0, p1}, Lkzv;-><init>(Lpdg;)V

    iput-object v0, p0, Lkko;->a:Lkzv;

    .line 37
    iput-object p1, p0, Lkko;->b:Lpdg;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lpdg;->f:[Loya;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lkko;->f:Ljava/util/List;

    .line 40
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p1, Lpdg;->f:[Loya;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 41
    iget-object v0, p0, Lkko;->f:Ljava/util/List;

    iget-object v2, p1, Lpdg;->f:[Loya;

    aget-object v2, v2, v1

    sget-object v3, Lozv;->a:Loxr;

    invoke-virtual {v2, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    iget-object v2, p1, Lpdg;->c:Ljava/lang/String;

    iget-object v0, p0, Lkko;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lozv;

    iget-object v0, v0, Lozv;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iput v1, p0, Lkko;->e:I

    .line 40
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 46
    :cond_1
    return-void
.end method

.method public constructor <init>(Lpdl;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lkko;->e:I

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkko;->f:Ljava/util/List;

    .line 79
    new-instance v0, Lkzv;

    invoke-direct {v0, p1}, Lkzv;-><init>(Lpdl;)V

    iput-object v0, p0, Lkko;->a:Lkzv;

    .line 80
    return-void
.end method

.method public constructor <init>(Lpdp;)V
    .locals 4

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lkko;->e:I

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lkko;->f:Ljava/util/List;

    .line 49
    new-instance v0, Lkzv;

    invoke-direct {v0, p1}, Lkzv;-><init>(Lpdp;)V

    iput-object v0, p0, Lkko;->a:Lkzv;

    .line 50
    iput-object p1, p0, Lkko;->c:Lpdp;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lpdp;->e:[Loya;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lkko;->f:Ljava/util/List;

    .line 53
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p1, Lpdp;->e:[Loya;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 54
    iget-object v0, p0, Lkko;->f:Ljava/util/List;

    iget-object v2, p1, Lpdp;->e:[Loya;

    aget-object v2, v2, v1

    sget-object v3, Lozv;->a:Loxr;

    invoke-virtual {v2, v3}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v2, p1, Lpdp;->c:Ljava/lang/String;

    iget-object v0, p0, Lkko;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lozv;

    iget-object v0, v0, Lozv;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iput v1, p0, Lkko;->e:I

    .line 53
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 59
    :cond_1
    return-void
.end method

.method private static f()Loya;
    .locals 2

    .prologue
    .line 167
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    .line 168
    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iput-object v1, v0, Loya;->b:[I

    .line 170
    return-object v0

    .line 168
    :array_0
    .array-data 4
        0x153
        0x152
        0x150
        0x14f
    .end array-data
.end method


# virtual methods
.method public a(Loya;)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lkko;->b:Lpdg;

    if-eqz v0, :cond_1

    .line 96
    sget-object v0, Lpdg;->a:Loxr;

    iget-object v1, p0, Lkko;->b:Lpdg;

    invoke-virtual {p1, v0, v1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v0, p0, Lkko;->c:Lpdp;

    if-eqz v0, :cond_2

    .line 98
    sget-object v0, Lpdp;->a:Loxr;

    iget-object v1, p0, Lkko;->c:Lpdp;

    invoke-virtual {p1, v0, v1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto :goto_0

    .line 99
    :cond_2
    iget-object v0, p0, Lkko;->d:Loyf;

    if-eqz v0, :cond_0

    .line 100
    sget-object v0, Loyf;->a:Loxr;

    iget-object v1, p0, Lkko;->d:Loyf;

    invoke-virtual {p1, v0, v1}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Lkko;->g:Z

    return v0
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 87
    iget-object v1, p0, Lkko;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 91
    iget v0, p0, Lkko;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 105
    invoke-virtual {p0}, Lkko;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkko;->g:Z

    .line 111
    iget v0, p0, Lkko;->e:I

    iget-object v1, p0, Lkko;->f:Ljava/util/List;

    .line 112
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_3

    const/4 v0, -0x1

    :goto_1
    iput v0, p0, Lkko;->e:I

    .line 114
    iget v0, p0, Lkko;->e:I

    if-ltz v0, :cond_8

    .line 115
    iget-object v0, p0, Lkko;->f:Ljava/util/List;

    iget v1, p0, Lkko;->e:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lozv;

    .line 116
    iget-object v1, v0, Lozv;->c:Lpdi;

    iget-object v1, v1, Lpdi;->a:Ljava/lang/String;

    .line 117
    iget-object v2, p0, Lkko;->a:Lkzv;

    invoke-virtual {v2, v1}, Lkzv;->b(Ljava/lang/String;)V

    .line 118
    iget-object v2, p0, Lkko;->a:Lkzv;

    iget-object v3, v0, Lozv;->d:Ljava/lang/Integer;

    .line 119
    invoke-static {v3}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Lkzv;->a(S)V

    .line 120
    iget-object v2, p0, Lkko;->a:Lkzv;

    iget-object v3, v0, Lozv;->e:Ljava/lang/Integer;

    .line 121
    invoke-static {v3}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v3

    int-to-short v3, v3

    invoke-virtual {v2, v3}, Lkzv;->b(S)V

    .line 122
    iget-object v2, p0, Lkko;->b:Lpdg;

    if-eqz v2, :cond_4

    .line 123
    iget-object v2, p0, Lkko;->b:Lpdg;

    iput-object v1, v2, Lpdg;->c:Ljava/lang/String;

    .line 124
    iget-object v2, p0, Lkko;->b:Lpdg;

    iget-object v2, v2, Lpdg;->g:Loya;

    if-nez v2, :cond_2

    .line 125
    iget-object v2, p0, Lkko;->b:Lpdg;

    invoke-static {}, Lkko;->f()Loya;

    move-result-object v3

    iput-object v3, v2, Lpdg;->g:Loya;

    .line 127
    :cond_2
    iget-object v2, p0, Lkko;->b:Lpdg;

    iget-object v2, v2, Lpdg;->g:Loya;

    iput-object v1, v2, Loya;->c:Ljava/lang/String;

    .line 128
    iget-object v1, p0, Lkko;->b:Lpdg;

    iget-object v1, v1, Lpdg;->g:Loya;

    sget-object v2, Lozv;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto :goto_0

    .line 112
    :cond_3
    iget v0, p0, Lkko;->e:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 129
    :cond_4
    iget-object v2, p0, Lkko;->c:Lpdp;

    if-eqz v2, :cond_6

    .line 130
    iget-object v2, p0, Lkko;->c:Lpdp;

    iput-object v1, v2, Lpdp;->c:Ljava/lang/String;

    .line 131
    iget-object v2, p0, Lkko;->c:Lpdp;

    iget-object v2, v2, Lpdp;->f:Loya;

    if-nez v2, :cond_5

    .line 132
    iget-object v2, p0, Lkko;->c:Lpdp;

    invoke-static {}, Lkko;->f()Loya;

    move-result-object v3

    iput-object v3, v2, Lpdp;->f:Loya;

    .line 134
    :cond_5
    iget-object v2, p0, Lkko;->c:Lpdp;

    iget-object v2, v2, Lpdp;->f:Loya;

    iput-object v1, v2, Loya;->c:Ljava/lang/String;

    .line 135
    iget-object v1, p0, Lkko;->c:Lpdp;

    iget-object v1, v1, Lpdp;->f:Loya;

    sget-object v2, Lozv;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 136
    :cond_6
    iget-object v2, p0, Lkko;->d:Loyf;

    if-eqz v2, :cond_0

    .line 137
    iget-object v2, p0, Lkko;->d:Loyf;

    iput-object v1, v2, Loyf;->c:Ljava/lang/String;

    .line 138
    iget-object v2, p0, Lkko;->d:Loyf;

    iget-object v2, v2, Loyf;->g:Loya;

    if-nez v2, :cond_7

    .line 139
    iget-object v2, p0, Lkko;->d:Loyf;

    invoke-static {}, Lkko;->f()Loya;

    move-result-object v3

    iput-object v3, v2, Loyf;->g:Loya;

    .line 141
    :cond_7
    iget-object v2, p0, Lkko;->d:Loyf;

    iget-object v2, v2, Loyf;->g:Loya;

    iput-object v1, v2, Loya;->c:Ljava/lang/String;

    .line 142
    iget-object v1, p0, Lkko;->d:Loyf;

    iget-object v1, v1, Loyf;->g:Loya;

    sget-object v2, Lozv;->a:Loxr;

    invoke-virtual {v1, v2, v0}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 145
    :cond_8
    const-string v0, ""

    .line 146
    iget-object v1, p0, Lkko;->a:Lkzv;

    invoke-virtual {v1, v0}, Lkzv;->b(Ljava/lang/String;)V

    .line 147
    iget-object v1, p0, Lkko;->a:Lkzv;

    invoke-virtual {v1, v3}, Lkzv;->a(S)V

    .line 148
    iget-object v1, p0, Lkko;->a:Lkzv;

    invoke-virtual {v1, v3}, Lkzv;->b(S)V

    .line 149
    iget-object v1, p0, Lkko;->b:Lpdg;

    if-eqz v1, :cond_9

    .line 150
    iget-object v1, p0, Lkko;->b:Lpdg;

    iput-object v0, v1, Lpdg;->c:Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lkko;->b:Lpdg;

    iput-object v2, v0, Lpdg;->g:Loya;

    goto/16 :goto_0

    .line 152
    :cond_9
    iget-object v1, p0, Lkko;->c:Lpdp;

    if-eqz v1, :cond_a

    .line 153
    iget-object v1, p0, Lkko;->c:Lpdp;

    iput-object v0, v1, Lpdp;->c:Ljava/lang/String;

    .line 154
    iget-object v0, p0, Lkko;->c:Lpdp;

    iput-object v2, v0, Lpdp;->f:Loya;

    goto/16 :goto_0

    .line 155
    :cond_a
    iget-object v1, p0, Lkko;->d:Loyf;

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lkko;->d:Loyf;

    iput-object v0, v1, Loyf;->c:Ljava/lang/String;

    .line 157
    iget-object v0, p0, Lkko;->d:Loyf;

    iput-object v2, v0, Loyf;->g:Loya;

    goto/16 :goto_0
.end method

.method public e()Lkzv;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lkko;->a:Lkzv;

    return-object v0
.end method
