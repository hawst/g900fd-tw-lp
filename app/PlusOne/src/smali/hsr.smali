.class public final Lhsr;
.super Lkds;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;

.field private d:Lhsr;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lkds;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lhsr;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lhsr;->d:Lhsr;

    .line 109
    return-void
.end method

.method public a(Ljava/lang/String;III)V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0, p4}, Lkds;->a(I)V

    .line 39
    iput-object p1, p0, Lhsr;->c:Ljava/lang/String;

    .line 40
    iput p2, p0, Lhsr;->a:I

    .line 41
    iput p3, p0, Lhsr;->b:I

    .line 42
    const/4 v0, 0x0

    iput v0, p0, Lhsr;->e:I

    .line 43
    return-void
.end method

.method public aJ_()Z
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lhsr;->h:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lhsr;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lhsr;->d:Lhsr;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 65
    if-ne p1, p0, :cond_1

    .line 66
    const/4 v0, 0x1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 69
    :cond_1
    instance-of v1, p1, Lhsr;

    if-eqz v1, :cond_0

    .line 73
    check-cast p1, Lhsr;

    .line 74
    iget v1, p0, Lhsr;->a:I

    iget v2, p1, Lhsr;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhsr;->b:I

    iget v2, p1, Lhsr;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lhsr;->h:I

    iget v2, p1, Lhsr;->h:I

    if-ne v1, v2, :cond_0

    .line 78
    iget-object v1, p0, Lhsr;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lhsr;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 79
    iget-object v0, p0, Lhsr;->c:Ljava/lang/String;

    iget-object v1, p1, Lhsr;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 51
    iget v0, p0, Lhsr;->e:I

    if-nez v0, :cond_0

    .line 52
    iget-object v0, p0, Lhsr;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lhsr;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lhsr;->e:I

    .line 58
    :goto_0
    iget v0, p0, Lhsr;->e:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhsr;->a:I

    add-int/2addr v0, v1

    iget v1, p0, Lhsr;->h:I

    add-int/2addr v0, v1

    iget v1, p0, Lhsr;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lhsr;->e:I

    .line 60
    :cond_0
    iget v0, p0, Lhsr;->e:I

    return v0

    .line 55
    :cond_1
    const/16 v0, 0x1f

    iput v0, p0, Lhsr;->e:I

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 89
    iget-object v1, p0, Lhsr;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    iget v1, p0, Lhsr;->a:I

    packed-switch v1, :pswitch_data_0

    .line 97
    :goto_0
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget v1, p0, Lhsr;->b:I

    packed-switch v1, :pswitch_data_1

    .line 103
    :goto_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 92
    :pswitch_0
    const-string v1, "tiny"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 93
    :pswitch_1
    const-string v1, "small"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 94
    :pswitch_2
    const-string v1, "medium"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 95
    :pswitch_3
    const-string v1, "large"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 99
    :pswitch_4
    const-string v1, "(normal)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 100
    :pswitch_5
    const-string v1, "(rounded)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 101
    :pswitch_6
    const-string v1, "(roundedcorners)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 91
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 98
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
