.class public final Ljrm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lipf;


# instance fields
.field volatile a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field

.field volatile b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field

.field volatile c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field

.field volatile d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;>;"
        }
    .end annotation
.end field

.field e:Lipj;

.field private f:Landroid/os/Handler;

.field private g:J

.field private h:Lipf;

.field private i:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljrl;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/content/Context;

.field private k:I

.field private l:Liof;

.field private m:Lioz;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const-class v0, Lipg;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lipg;

    .line 99
    new-instance v1, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v1, p0, Ljrm;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 100
    iput-object p1, p0, Ljrm;->j:Landroid/content/Context;

    .line 101
    iput p2, p0, Ljrm;->k:I

    .line 102
    const-class v1, Liof;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liof;

    iput-object v1, p0, Ljrm;->l:Liof;

    .line 103
    const-class v1, Lioz;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lioz;

    iput-object v1, p0, Ljrm;->m:Lioz;

    .line 104
    iget-object v1, p0, Ljrm;->l:Liof;

    invoke-interface {v1}, Liof;->a()Lipj;

    move-result-object v1

    iput-object v1, p0, Ljrm;->e:Lipj;

    .line 105
    invoke-interface {v0, p0}, Lipg;->a(Lipf;)Lipf;

    move-result-object v0

    iput-object v0, p0, Ljrm;->h:Lipf;

    .line 106
    const-wide/32 v0, 0xea60

    iput-wide v0, p0, Ljrm;->g:J

    .line 107
    new-instance v0, Ljrn;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ljrn;-><init>(Ljrm;Landroid/os/Looper;)V

    iput-object v0, p0, Ljrm;->f:Landroid/os/Handler;

    .line 119
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 213
    iget-object v0, p0, Ljrm;->j:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ljrm;->k:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 215
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 216
    const-string v2, "effective_gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    new-instance v2, Lioq;

    invoke-direct {v2}, Lioq;-><init>()V

    invoke-virtual {v2, v5}, Lioq;->a(Ljava/lang/String;)Lioq;

    move-result-object v3

    const/16 v4, -0x3e7

    invoke-virtual {v3, v4}, Lioq;->a(I)Lioq;

    move-result-object v3

    invoke-virtual {v3, v5}, Lioq;->b(Ljava/lang/String;)Lioq;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lioq;->a(Z)Lioq;

    .line 220
    iget-object v3, p0, Ljrm;->l:Liof;

    iget-object v4, p0, Ljrm;->e:Lipj;

    .line 221
    invoke-interface {v3, v4, v1, v0, v2}, Liof;->a(Lipj;Ljava/lang/String;Ljava/lang/String;Lioq;)Lijq;

    move-result-object v0

    .line 223
    if-nez v0, :cond_1

    .line 224
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    invoke-static {}, Ljpe;->b()V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    new-instance v1, Ljro;

    invoke-direct {v1, p0}, Ljro;-><init>(Ljrm;)V

    invoke-interface {v0, v1}, Lijq;->a(Lijt;)V

    goto :goto_0
.end method


# virtual methods
.method a(Ljava/util/List;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljod;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljod;",
            ">;"
        }
    .end annotation

    .prologue
    .line 428
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 429
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljod;

    .line 430
    invoke-interface {v0}, Ljod;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 432
    :cond_0
    return-object v1
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Ljrm;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method a(I)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 191
    and-int/lit8 v0, p1, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    move v6, v2

    .line 193
    :goto_0
    and-int/lit8 v0, p1, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_7

    move v5, v2

    .line 195
    :goto_1
    if-nez v6, :cond_0

    if-eqz v5, :cond_8

    :cond_0
    iget-object v0, p0, Ljrm;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljrm;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    move v1, v2

    .line 198
    :goto_2
    iget-object v0, p0, Ljrm;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljrl;

    .line 199
    if-eqz v6, :cond_2

    instance-of v4, v0, Ljob;

    if-nez v4, :cond_5

    :cond_2
    if-eqz v6, :cond_3

    instance-of v4, v0, Ljoc;

    if-nez v4, :cond_5

    :cond_3
    if-eqz v5, :cond_4

    instance-of v4, v0, Ljpi;

    if-nez v4, :cond_5

    :cond_4
    if-eqz v1, :cond_9

    instance-of v4, v0, Ljqh;

    if-eqz v4, :cond_9

    :cond_5
    move v4, v2

    .line 203
    :goto_4
    if-eqz v4, :cond_1

    .line 204
    invoke-interface {v0}, Ljrl;->c()V

    goto :goto_3

    :cond_6
    move v6, v3

    .line 191
    goto :goto_0

    :cond_7
    move v5, v3

    .line 193
    goto :goto_1

    :cond_8
    move v1, v3

    .line 195
    goto :goto_2

    :cond_9
    move v4, v3

    .line 199
    goto :goto_4

    .line 207
    :cond_a
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 395
    and-int/lit8 v2, p3, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    move v2, v0

    .line 397
    :goto_0
    and-int/lit8 v3, p3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    move v1, v0

    .line 399
    :cond_0
    iget-object v0, p0, Ljrm;->j:Landroid/content/Context;

    const-class v3, Lhei;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v3, p0, Ljrm;->k:I

    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 400
    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p1}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "effective_gaia_id"

    .line 401
    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 402
    if-eqz v2, :cond_1

    .line 403
    invoke-direct {p0}, Ljrm;->f()V

    .line 405
    :cond_1
    if-eqz v1, :cond_2

    .line 406
    invoke-virtual {p0}, Ljrm;->e()V

    .line 409
    :cond_2
    return-void

    :cond_3
    move v2, v1

    .line 395
    goto :goto_0
.end method

.method public declared-synchronized a(Ljrl;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Ljrm;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    iget-object v1, p0, Ljrm;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_0
    iget-object v1, p0, Ljrm;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 131
    iget-object v1, p0, Ljrm;->m:Lioz;

    iget-object v2, p0, Ljrm;->e:Lipj;

    iget-object v3, p0, Ljrm;->h:Lipf;

    const/4 v4, 0x7

    invoke-interface {v1, v2, v3, v4}, Lioz;->a(Lipj;Lipf;I)Lijq;

    .line 132
    iget-object v1, p0, Ljrm;->f:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Ljrm;->e:Lipj;

    invoke-interface {v1}, Lipj;->c()V

    .line 133
    invoke-direct {p0}, Ljrm;->f()V

    .line 138
    :cond_1
    instance-of v1, p1, Ljob;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ljrm;->a:Ljava/util/ArrayList;

    if-nez v1, :cond_5

    :cond_2
    instance-of v1, p1, Ljoc;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ljrm;->b:Ljava/util/HashMap;

    if-nez v1, :cond_5

    :cond_3
    instance-of v1, p1, Ljpi;

    if-eqz v1, :cond_4

    iget-object v1, p0, Ljrm;->c:Ljava/util/ArrayList;

    if-nez v1, :cond_5

    :cond_4
    instance-of v1, p1, Ljqh;

    if-eqz v1, :cond_7

    iget-object v1, p0, Ljrm;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_7

    iget-object v1, p0, Ljrm;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_7

    .line 142
    :cond_5
    :goto_0
    if-eqz v0, :cond_6

    .line 143
    invoke-interface {p1}, Ljrl;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    :cond_6
    monitor-exit p0

    return-void

    .line 138
    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljod;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Ljrm;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public declared-synchronized b(Ljrl;)V
    .locals 4

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ljrm;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Ljrm;->i:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 155
    iget-object v0, p0, Ljrm;->f:Landroid/os/Handler;

    const/4 v1, 0x1

    iget-wide v2, p0, Ljrm;->g:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    :cond_0
    monitor-exit p0

    return-void

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Ljrm;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public d()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Ljrm;->d:Ljava/util/HashMap;

    return-object v0
.end method

.method e()V
    .locals 5

    .prologue
    .line 245
    iget-object v0, p0, Ljrm;->j:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ljrm;->k:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 247
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 248
    const-string v2, "effective_gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    new-instance v2, Liow;

    invoke-direct {v2}, Liow;-><init>()V

    .line 250
    iget-object v3, p0, Ljrm;->l:Liof;

    iget-object v4, p0, Ljrm;->e:Lipj;

    .line 251
    invoke-interface {v3, v4, v1, v0, v2}, Liof;->a(Lipj;Ljava/lang/String;Ljava/lang/String;Liow;)Lijq;

    move-result-object v0

    .line 253
    if-nez v0, :cond_1

    .line 254
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    invoke-static {}, Ljpe;->b()V

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    new-instance v1, Ljrp;

    invoke-direct {v1, p0}, Ljrp;-><init>(Ljrm;)V

    invoke-interface {v0, v1}, Lijq;->a(Lijt;)V

    goto :goto_0
.end method
