.class public final Llmt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/net/InetAddress;


# instance fields
.field private final b:Landroid/net/Uri;

.field private final c:Llmy;

.field private d:Ljava/net/ServerSocket;

.field private e:Ljava/util/concurrent/ExecutorService;

.field private f:Lorg/apache/http/params/HttpParams;

.field private g:Lorg/apache/http/protocol/HttpService;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 61
    const/4 v0, 0x0

    .line 63
    :try_start_0
    const-string v1, "localhost"

    const/4 v2, 0x4

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    invoke-static {v1, v2}, Ljava/net/InetAddress;->getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 67
    :goto_0
    sput-object v0, Llmt;->a:Ljava/net/InetAddress;

    .line 68
    return-void

    .line 64
    :catch_0
    move-exception v1

    .line 65
    const-string v2, "VideoServer"

    const-string v3, "Cannot find localhost"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 63
    nop

    :array_0
    .array-data 1
        0x7ft
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method private constructor <init>(Landroid/net/Uri;Llmy;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p2, p0, Llmt;->c:Llmy;

    .line 94
    iput-object p1, p0, Llmt;->b:Landroid/net/Uri;

    .line 95
    return-void
.end method

.method static synthetic a(Llmt;)Ljava/net/ServerSocket;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Llmt;->d:Ljava/net/ServerSocket;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;)Llmt;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Llme;

    invoke-direct {v0, p0, p1}, Llme;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 89
    new-instance v1, Llmt;

    invoke-direct {v1, p2, v0}, Llmt;-><init>(Landroid/net/Uri;Llmy;)V

    return-object v1
.end method

.method static synthetic b(Llmt;)Lorg/apache/http/params/HttpParams;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Llmt;->f:Lorg/apache/http/params/HttpParams;

    return-object v0
.end method

.method static synthetic c(Llmt;)Lorg/apache/http/protocol/HttpService;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Llmt;->g:Lorg/apache/http/protocol/HttpService;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 103
    new-instance v0, Ljava/net/ServerSocket;

    invoke-direct {v0}, Ljava/net/ServerSocket;-><init>()V

    iput-object v0, p0, Llmt;->d:Ljava/net/ServerSocket;

    .line 104
    iget-object v0, p0, Llmt;->d:Ljava/net/ServerSocket;

    new-instance v1, Ljava/net/InetSocketAddress;

    sget-object v2, Llmt;->a:Ljava/net/InetAddress;

    invoke-direct {v1, v2, v6}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;)V

    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x2d

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 108
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    const-string v2, "http.connection.stalecheck"

    .line 109
    invoke-virtual {v0, v2, v6}, Lorg/apache/http/params/BasicHttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v2, "http.tcp.nodelay"

    const/4 v3, 0x1

    .line 110
    invoke-interface {v0, v2, v3}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v2, "http.socket.buffer-size"

    const/16 v3, 0x2000

    .line 111
    invoke-interface {v0, v2, v3}, Lorg/apache/http/params/HttpParams;->setIntParameter(Ljava/lang/String;I)Lorg/apache/http/params/HttpParams;

    move-result-object v0

    iput-object v0, p0, Llmt;->f:Lorg/apache/http/params/HttpParams;

    .line 113
    new-instance v2, Lorg/apache/http/protocol/BasicHttpProcessor;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpProcessor;-><init>()V

    .line 114
    new-instance v0, Lorg/apache/http/protocol/ResponseContent;

    invoke-direct {v0}, Lorg/apache/http/protocol/ResponseContent;-><init>()V

    invoke-virtual {v2, v0}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 115
    new-instance v0, Lorg/apache/http/protocol/ResponseConnControl;

    invoke-direct {v0}, Lorg/apache/http/protocol/ResponseConnControl;-><init>()V

    invoke-virtual {v2, v0}, Lorg/apache/http/protocol/BasicHttpProcessor;->addInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 117
    new-instance v3, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;

    invoke-direct {v3}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;-><init>()V

    .line 118
    new-instance v4, Llmx;

    iget-object v0, p0, Llmt;->b:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llmt;->b:Landroid/net/Uri;

    .line 120
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v5, p0, Llmt;->c:Llmy;

    invoke-direct {v4, v1, v0, v5}, Llmx;-><init>(Ljava/lang/String;Ljava/lang/String;Llmy;)V

    .line 118
    invoke-virtual {v3, v1, v4}, Lorg/apache/http/protocol/HttpRequestHandlerRegistry;->register(Ljava/lang/String;Lorg/apache/http/protocol/HttpRequestHandler;)V

    .line 122
    new-instance v0, Lorg/apache/http/protocol/HttpService;

    new-instance v4, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;

    invoke-direct {v4}, Lorg/apache/http/impl/DefaultConnectionReuseStrategy;-><init>()V

    new-instance v5, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v5}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    invoke-direct {v0, v2, v4, v5}, Lorg/apache/http/protocol/HttpService;-><init>(Lorg/apache/http/protocol/HttpProcessor;Lorg/apache/http/ConnectionReuseStrategy;Lorg/apache/http/HttpResponseFactory;)V

    iput-object v0, p0, Llmt;->g:Lorg/apache/http/protocol/HttpService;

    .line 125
    iget-object v0, p0, Llmt;->g:Lorg/apache/http/protocol/HttpService;

    invoke-virtual {v0, v3}, Lorg/apache/http/protocol/HttpService;->setHandlerResolver(Lorg/apache/http/protocol/HttpRequestHandlerResolver;)V

    .line 126
    iget-object v0, p0, Llmt;->g:Lorg/apache/http/protocol/HttpService;

    iget-object v2, p0, Llmt;->f:Lorg/apache/http/params/HttpParams;

    invoke-virtual {v0, v2}, Lorg/apache/http/protocol/HttpService;->setParams(Lorg/apache/http/params/HttpParams;)V

    .line 128
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Llmt;->e:Ljava/util/concurrent/ExecutorService;

    .line 129
    iget-object v0, p0, Llmt;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Llmu;

    invoke-direct {v2, p0}, Llmu;-><init>(Llmt;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 131
    sget-object v0, Llmt;->a:Ljava/net/InetAddress;

    .line 132
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Llmt;->d:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x13

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "http://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 120
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Llmt;->e:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Llmt;->e:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 143
    :cond_0
    iget-object v0, p0, Llmt;->d:Ljava/net/ServerSocket;

    if-eqz v0, :cond_1

    .line 145
    :try_start_0
    iget-object v0, p0, Llmt;->d:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :cond_1
    :goto_0
    return-void

    .line 146
    :catch_0
    move-exception v0

    .line 147
    const-string v1, "VideoServer"

    const-string v2, "Error while closing the socket"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
