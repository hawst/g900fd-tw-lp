.class public final Lneo;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field private c:Lneh;

.field private d:Ljava/lang/Integer;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 2174
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2177
    const/4 v0, 0x0

    iput-object v0, p0, Lneo;->c:Lneh;

    .line 2180
    iput v1, p0, Lneo;->a:I

    .line 2183
    iput v1, p0, Lneo;->b:I

    .line 2188
    iput v1, p0, Lneo;->e:I

    .line 2174
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 2214
    const/4 v0, 0x0

    .line 2215
    iget-object v1, p0, Lneo;->c:Lneh;

    if-eqz v1, :cond_0

    .line 2216
    const/4 v0, 0x1

    iget-object v1, p0, Lneo;->c:Lneh;

    .line 2217
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2219
    :cond_0
    iget v1, p0, Lneo;->a:I

    if-eq v1, v3, :cond_1

    .line 2220
    const/4 v1, 0x2

    iget v2, p0, Lneo;->a:I

    .line 2221
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2223
    :cond_1
    iget v1, p0, Lneo;->b:I

    if-eq v1, v3, :cond_2

    .line 2224
    const/4 v1, 0x3

    iget v2, p0, Lneo;->b:I

    .line 2225
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2227
    :cond_2
    iget-object v1, p0, Lneo;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2228
    const/4 v1, 0x4

    iget-object v2, p0, Lneo;->d:Ljava/lang/Integer;

    .line 2229
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2231
    :cond_3
    iget v1, p0, Lneo;->e:I

    if-eq v1, v3, :cond_4

    .line 2232
    const/4 v1, 0x5

    iget v2, p0, Lneo;->e:I

    .line 2233
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2235
    :cond_4
    iget-object v1, p0, Lneo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2236
    iput v0, p0, Lneo;->ai:I

    .line 2237
    return v0
.end method

.method public a(Loxn;)Lneo;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2245
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2246
    sparse-switch v0, :sswitch_data_0

    .line 2250
    iget-object v1, p0, Lneo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2251
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lneo;->ah:Ljava/util/List;

    .line 2254
    :cond_1
    iget-object v1, p0, Lneo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2256
    :sswitch_0
    return-object p0

    .line 2261
    :sswitch_1
    iget-object v0, p0, Lneo;->c:Lneh;

    if-nez v0, :cond_2

    .line 2262
    new-instance v0, Lneh;

    invoke-direct {v0}, Lneh;-><init>()V

    iput-object v0, p0, Lneo;->c:Lneh;

    .line 2264
    :cond_2
    iget-object v0, p0, Lneo;->c:Lneh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2268
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2269
    if-eqz v0, :cond_3

    if-eq v0, v3, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    .line 2275
    :cond_3
    iput v0, p0, Lneo;->a:I

    goto :goto_0

    .line 2277
    :cond_4
    iput v2, p0, Lneo;->a:I

    goto :goto_0

    .line 2282
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2283
    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-ne v0, v4, :cond_6

    .line 2286
    :cond_5
    iput v0, p0, Lneo;->b:I

    goto :goto_0

    .line 2288
    :cond_6
    iput v2, p0, Lneo;->b:I

    goto :goto_0

    .line 2293
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lneo;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2297
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2298
    if-eqz v0, :cond_7

    if-eq v0, v3, :cond_7

    if-ne v0, v4, :cond_8

    .line 2301
    :cond_7
    iput v0, p0, Lneo;->e:I

    goto :goto_0

    .line 2303
    :cond_8
    iput v2, p0, Lneo;->e:I

    goto :goto_0

    .line 2246
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 2193
    iget-object v0, p0, Lneo;->c:Lneh;

    if-eqz v0, :cond_0

    .line 2194
    const/4 v0, 0x1

    iget-object v1, p0, Lneo;->c:Lneh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2196
    :cond_0
    iget v0, p0, Lneo;->a:I

    if-eq v0, v2, :cond_1

    .line 2197
    const/4 v0, 0x2

    iget v1, p0, Lneo;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2199
    :cond_1
    iget v0, p0, Lneo;->b:I

    if-eq v0, v2, :cond_2

    .line 2200
    const/4 v0, 0x3

    iget v1, p0, Lneo;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2202
    :cond_2
    iget-object v0, p0, Lneo;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2203
    const/4 v0, 0x4

    iget-object v1, p0, Lneo;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2205
    :cond_3
    iget v0, p0, Lneo;->e:I

    if-eq v0, v2, :cond_4

    .line 2206
    const/4 v0, 0x5

    iget v1, p0, Lneo;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2208
    :cond_4
    iget-object v0, p0, Lneo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2210
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2170
    invoke-virtual {p0, p1}, Lneo;->a(Loxn;)Lneo;

    move-result-object v0

    return-object v0
.end method
