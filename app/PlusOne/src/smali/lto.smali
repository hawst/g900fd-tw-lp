.class public final Llto;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llto;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:[Lltp;

.field public d:[Lltj;

.field private e:Lltq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Llto;

    sput-object v0, Llto;->a:[Llto;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 14
    sget-object v0, Lltp;->a:[Lltp;

    iput-object v0, p0, Llto;->c:[Lltp;

    .line 17
    sget-object v0, Lltj;->a:[Lltj;

    iput-object v0, p0, Llto;->d:[Lltj;

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Llto;->e:Lltq;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 51
    .line 52
    iget-object v0, p0, Llto;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 53
    const/4 v0, 0x1

    iget-object v2, p0, Llto;->b:Ljava/lang/Integer;

    .line 54
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 56
    :goto_0
    iget-object v2, p0, Llto;->c:[Lltp;

    if-eqz v2, :cond_1

    .line 57
    iget-object v3, p0, Llto;->c:[Lltp;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 58
    if-eqz v5, :cond_0

    .line 59
    const/4 v6, 0x2

    .line 60
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 57
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 64
    :cond_1
    iget-object v2, p0, Llto;->d:[Lltj;

    if-eqz v2, :cond_3

    .line 65
    iget-object v2, p0, Llto;->d:[Lltj;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 66
    if-eqz v4, :cond_2

    .line 67
    const/4 v5, 0x3

    .line 68
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 65
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 72
    :cond_3
    iget-object v1, p0, Llto;->e:Lltq;

    if-eqz v1, :cond_4

    .line 73
    const/4 v1, 0x4

    iget-object v2, p0, Llto;->e:Lltq;

    .line 74
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_4
    iget-object v1, p0, Llto;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    iput v0, p0, Llto;->ai:I

    .line 78
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Llto;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 86
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 87
    sparse-switch v0, :sswitch_data_0

    .line 91
    iget-object v2, p0, Llto;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 92
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llto;->ah:Ljava/util/List;

    .line 95
    :cond_1
    iget-object v2, p0, Llto;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    :sswitch_0
    return-object p0

    .line 102
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Llto;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 106
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 107
    iget-object v0, p0, Llto;->c:[Lltp;

    if-nez v0, :cond_3

    move v0, v1

    .line 108
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lltp;

    .line 109
    iget-object v3, p0, Llto;->c:[Lltp;

    if-eqz v3, :cond_2

    .line 110
    iget-object v3, p0, Llto;->c:[Lltp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    :cond_2
    iput-object v2, p0, Llto;->c:[Lltp;

    .line 113
    :goto_2
    iget-object v2, p0, Llto;->c:[Lltp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 114
    iget-object v2, p0, Llto;->c:[Lltp;

    new-instance v3, Lltp;

    invoke-direct {v3}, Lltp;-><init>()V

    aput-object v3, v2, v0

    .line 115
    iget-object v2, p0, Llto;->c:[Lltp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 116
    invoke-virtual {p1}, Loxn;->a()I

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 107
    :cond_3
    iget-object v0, p0, Llto;->c:[Lltp;

    array-length v0, v0

    goto :goto_1

    .line 119
    :cond_4
    iget-object v2, p0, Llto;->c:[Lltp;

    new-instance v3, Lltp;

    invoke-direct {v3}, Lltp;-><init>()V

    aput-object v3, v2, v0

    .line 120
    iget-object v2, p0, Llto;->c:[Lltp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 124
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 125
    iget-object v0, p0, Llto;->d:[Lltj;

    if-nez v0, :cond_6

    move v0, v1

    .line 126
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lltj;

    .line 127
    iget-object v3, p0, Llto;->d:[Lltj;

    if-eqz v3, :cond_5

    .line 128
    iget-object v3, p0, Llto;->d:[Lltj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    :cond_5
    iput-object v2, p0, Llto;->d:[Lltj;

    .line 131
    :goto_4
    iget-object v2, p0, Llto;->d:[Lltj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 132
    iget-object v2, p0, Llto;->d:[Lltj;

    new-instance v3, Lltj;

    invoke-direct {v3}, Lltj;-><init>()V

    aput-object v3, v2, v0

    .line 133
    iget-object v2, p0, Llto;->d:[Lltj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 134
    invoke-virtual {p1}, Loxn;->a()I

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 125
    :cond_6
    iget-object v0, p0, Llto;->d:[Lltj;

    array-length v0, v0

    goto :goto_3

    .line 137
    :cond_7
    iget-object v2, p0, Llto;->d:[Lltj;

    new-instance v3, Lltj;

    invoke-direct {v3}, Lltj;-><init>()V

    aput-object v3, v2, v0

    .line 138
    iget-object v2, p0, Llto;->d:[Lltj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 142
    :sswitch_4
    iget-object v0, p0, Llto;->e:Lltq;

    if-nez v0, :cond_8

    .line 143
    new-instance v0, Lltq;

    invoke-direct {v0}, Lltq;-><init>()V

    iput-object v0, p0, Llto;->e:Lltq;

    .line 145
    :cond_8
    iget-object v0, p0, Llto;->e:Lltq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 87
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 25
    iget-object v1, p0, Llto;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 26
    const/4 v1, 0x1

    iget-object v2, p0, Llto;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 28
    :cond_0
    iget-object v1, p0, Llto;->c:[Lltp;

    if-eqz v1, :cond_2

    .line 29
    iget-object v2, p0, Llto;->c:[Lltp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 30
    if-eqz v4, :cond_1

    .line 31
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 29
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 35
    :cond_2
    iget-object v1, p0, Llto;->d:[Lltj;

    if-eqz v1, :cond_4

    .line 36
    iget-object v1, p0, Llto;->d:[Lltj;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 37
    if-eqz v3, :cond_3

    .line 38
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 36
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 42
    :cond_4
    iget-object v0, p0, Llto;->e:Lltq;

    if-eqz v0, :cond_5

    .line 43
    const/4 v0, 0x4

    iget-object v1, p0, Llto;->e:Lltq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 45
    :cond_5
    iget-object v0, p0, Llto;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 47
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llto;->a(Loxn;)Llto;

    move-result-object v0

    return-object v0
.end method
