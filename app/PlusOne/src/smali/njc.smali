.class public final Lnjc;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field private b:Lnja;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1952
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1961
    const/4 v0, 0x0

    iput-object v0, p0, Lnjc;->b:Lnja;

    .line 1964
    const/high16 v0, -0x80000000

    iput v0, p0, Lnjc;->a:I

    .line 1952
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1981
    const/4 v0, 0x0

    .line 1982
    iget-object v1, p0, Lnjc;->b:Lnja;

    if-eqz v1, :cond_0

    .line 1983
    const/4 v0, 0x1

    iget-object v1, p0, Lnjc;->b:Lnja;

    .line 1984
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1986
    :cond_0
    iget v1, p0, Lnjc;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 1987
    const/4 v1, 0x2

    iget v2, p0, Lnjc;->a:I

    .line 1988
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1990
    :cond_1
    iget-object v1, p0, Lnjc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1991
    iput v0, p0, Lnjc;->ai:I

    .line 1992
    return v0
.end method

.method public a(Loxn;)Lnjc;
    .locals 2

    .prologue
    .line 2000
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2001
    sparse-switch v0, :sswitch_data_0

    .line 2005
    iget-object v1, p0, Lnjc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2006
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnjc;->ah:Ljava/util/List;

    .line 2009
    :cond_1
    iget-object v1, p0, Lnjc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2011
    :sswitch_0
    return-object p0

    .line 2016
    :sswitch_1
    iget-object v0, p0, Lnjc;->b:Lnja;

    if-nez v0, :cond_2

    .line 2017
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnjc;->b:Lnja;

    .line 2019
    :cond_2
    iget-object v0, p0, Lnjc;->b:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2023
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2024
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 2027
    :cond_3
    iput v0, p0, Lnjc;->a:I

    goto :goto_0

    .line 2029
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lnjc;->a:I

    goto :goto_0

    .line 2001
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1969
    iget-object v0, p0, Lnjc;->b:Lnja;

    if-eqz v0, :cond_0

    .line 1970
    const/4 v0, 0x1

    iget-object v1, p0, Lnjc;->b:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1972
    :cond_0
    iget v0, p0, Lnjc;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 1973
    const/4 v0, 0x2

    iget v1, p0, Lnjc;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1975
    :cond_1
    iget-object v0, p0, Lnjc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1977
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1948
    invoke-virtual {p0, p1}, Lnjc;->a(Loxn;)Lnjc;

    move-result-object v0

    return-object v0
.end method
