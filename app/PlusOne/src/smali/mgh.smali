.class public final Lmgh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnaz;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23974
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23977
    iput-object v0, p0, Lmgh;->apiHeader:Llyr;

    .line 23980
    iput-object v0, p0, Lmgh;->a:Lnaz;

    .line 23974
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 23997
    const/4 v0, 0x0

    .line 23998
    iget-object v1, p0, Lmgh;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 23999
    const/4 v0, 0x1

    iget-object v1, p0, Lmgh;->apiHeader:Llyr;

    .line 24000
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24002
    :cond_0
    iget-object v1, p0, Lmgh;->a:Lnaz;

    if-eqz v1, :cond_1

    .line 24003
    const/4 v1, 0x2

    iget-object v2, p0, Lmgh;->a:Lnaz;

    .line 24004
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24006
    :cond_1
    iget-object v1, p0, Lmgh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24007
    iput v0, p0, Lmgh;->ai:I

    .line 24008
    return v0
.end method

.method public a(Loxn;)Lmgh;
    .locals 2

    .prologue
    .line 24016
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 24017
    sparse-switch v0, :sswitch_data_0

    .line 24021
    iget-object v1, p0, Lmgh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 24022
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgh;->ah:Ljava/util/List;

    .line 24025
    :cond_1
    iget-object v1, p0, Lmgh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24027
    :sswitch_0
    return-object p0

    .line 24032
    :sswitch_1
    iget-object v0, p0, Lmgh;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 24033
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgh;->apiHeader:Llyr;

    .line 24035
    :cond_2
    iget-object v0, p0, Lmgh;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24039
    :sswitch_2
    iget-object v0, p0, Lmgh;->a:Lnaz;

    if-nez v0, :cond_3

    .line 24040
    new-instance v0, Lnaz;

    invoke-direct {v0}, Lnaz;-><init>()V

    iput-object v0, p0, Lmgh;->a:Lnaz;

    .line 24042
    :cond_3
    iget-object v0, p0, Lmgh;->a:Lnaz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24017
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23985
    iget-object v0, p0, Lmgh;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 23986
    const/4 v0, 0x1

    iget-object v1, p0, Lmgh;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23988
    :cond_0
    iget-object v0, p0, Lmgh;->a:Lnaz;

    if-eqz v0, :cond_1

    .line 23989
    const/4 v0, 0x2

    iget-object v1, p0, Lmgh;->a:Lnaz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23991
    :cond_1
    iget-object v0, p0, Lmgh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 23993
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 23970
    invoke-virtual {p0, p1}, Lmgh;->a(Loxn;)Lmgh;

    move-result-object v0

    return-object v0
.end method
