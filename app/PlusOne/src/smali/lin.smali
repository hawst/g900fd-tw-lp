.class public abstract Llin;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# static fields
.field public static a:I

.field public static final d:Landroid/text/style/StyleSpan;

.field public static e:Landroid/text/style/ForegroundColorSpan;

.field private static f:Landroid/graphics/drawable/Drawable;


# instance fields
.field public b:Z

.field public c:Landroid/widget/CheckBox;

.field private g:Z

.field private h:Llio;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    sput-object v0, Llin;->d:Landroid/text/style/StyleSpan;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    sget-object v1, Llin;->f:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 64
    const v1, 0x7f020415

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Llin;->f:Landroid/graphics/drawable/Drawable;

    .line 65
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    const v2, 0x7f0b0145

    .line 66
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v1, Llin;->e:Landroid/text/style/ForegroundColorSpan;

    .line 68
    const v1, 0x7f0d0258

    .line 69
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Llin;->a:I

    .line 71
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 168
    invoke-virtual {p0}, Llin;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Llin;->getHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 169
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 170
    return-void
.end method

.method public a(Llio;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Llin;->h:Llio;

    .line 94
    return-void
.end method

.method public g(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    iput-boolean p1, p0, Llin;->b:Z

    .line 78
    iget-boolean v0, p0, Llin;->b:Z

    if-eqz v0, :cond_2

    .line 79
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Llin;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    .line 81
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 82
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 83
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {p0, v0}, Llin;->addView(Landroid/view/View;)V

    .line 85
    :cond_0
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 89
    :cond_1
    :goto_0
    return-void

    .line 86
    :cond_2
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Llin;->b:Z

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 104
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Llin;->g:Z

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Llin;->h:Llio;

    iget-object v1, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-interface {v0, p0, v1}, Llio;->a(Llin;Z)V

    .line 151
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Llin;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Llin;->g:Z

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Llin;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1, v0}, Llin;->a(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    .line 161
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 162
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 113
    iget-boolean v0, p0, Llin;->b:Z

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    iget-boolean v0, p0, Llin;->g:Z

    if-eq p1, v0, :cond_0

    .line 116
    iput-boolean p1, p0, Llin;->g:Z

    .line 119
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Llin;->setWillNotDraw(Z)V

    .line 120
    invoke-virtual {p0}, Llin;->invalidate()V

    goto :goto_0

    .line 119
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 130
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 131
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Llin;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Llin;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-boolean v0, p0, Llin;->g:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Llin;->g:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
