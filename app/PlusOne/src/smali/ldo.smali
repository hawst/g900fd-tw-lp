.class public final Lldo;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 161
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v9, 0x2

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 166
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    if-gt v0, p1, :cond_1

    :cond_0
    move-object p2, v2

    .line 285
    :goto_0
    return-object p2

    .line 171
    :cond_1
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    if-le v0, p1, :cond_12

    .line 172
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    if-eqz v0, :cond_7

    .line 173
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    invoke-virtual {v0}, Lkzr;->a()I

    move-result v0

    if-le v0, p1, :cond_12

    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v1, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v1, v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->b:Lkzr;

    .line 174
    invoke-virtual {v1, p1}, Lkzr;->a(I)Lkzv;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;Lkzv;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 175
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    move-object v1, v0

    .line 186
    :goto_1
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 189
    instance-of v0, p2, Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v0, :cond_9

    .line 190
    check-cast p2, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 195
    :goto_2
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-boolean v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->l:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x4

    move v3, v0

    .line 197
    :goto_3
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    .line 198
    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v7, Ljaa;

    invoke-static {v0, v7}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljaa;

    invoke-interface {v0}, Ljaa;->a()Z

    move-result v7

    .line 200
    if-eqz v7, :cond_2

    .line 202
    invoke-static {v6}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    const-class v8, Llcq;

    invoke-virtual {v0, v8}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llcq;

    .line 204
    invoke-virtual {p2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 205
    iget-object v8, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v8, v8, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->g:I

    .line 206
    invoke-virtual {v0, v8}, Llcq;->a(I)I

    move-result v0

    .line 205
    invoke-virtual {p2, v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(II)V

    .line 207
    invoke-virtual {p2, v9}, Lcom/google/android/libraries/social/media/ui/MediaView;->e(I)V

    .line 209
    const-class v0, Ljau;

    invoke-static {v6, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljau;

    .line 210
    invoke-virtual {v0}, Ljau;->a()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(Z)V

    .line 213
    :cond_2
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_b

    sget-object v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    iget v0, v0, Llct;->ab:I

    :goto_4
    invoke-virtual {p2, v4, v4, v0, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->setPadding(IIII)V

    .line 217
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->e:Licj;

    if-eqz v0, :cond_c

    .line 218
    invoke-virtual {p2, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 220
    invoke-virtual {p2, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->e(Landroid/graphics/drawable/Drawable;)V

    .line 221
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->e:Licj;

    invoke-virtual {v0}, Licj;->b()Ljava/lang/String;

    move-result-object v0

    .line 240
    :goto_5
    iget-object v6, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v6, v6, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    if-ne v6, v5, :cond_10

    .line 241
    invoke-virtual {p2, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 242
    invoke-virtual {p2, v10}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(F)V

    .line 243
    invoke-virtual {p2, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 244
    invoke-virtual {p2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Z)V

    .line 253
    :goto_6
    iget-object v2, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v2, v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c:[Lppr;

    if-eqz v2, :cond_4

    .line 254
    if-nez v7, :cond_3

    .line 255
    invoke-virtual {p2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 257
    :cond_3
    invoke-virtual {p2, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 258
    invoke-virtual {p2, v10}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(F)V

    .line 260
    :cond_4
    invoke-virtual {p2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    .line 262
    iget-object v2, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v2, v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v2, v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    invoke-virtual {v2}, Lkzv;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 263
    iget-object v2, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v2, v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    invoke-virtual {v2}, Lkzv;->a()Ljava/lang/String;

    move-result-object v2

    .line 264
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 265
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    .line 266
    new-array v6, v5, [Ljava/lang/CharSequence;

    aput-object v0, v6, v4

    invoke-static {v3, v6}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 267
    new-array v0, v5, [Ljava/lang/CharSequence;

    aput-object v2, v0, v4

    invoke-static {v3, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 268
    invoke-static {v3}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    .line 272
    :cond_5
    iget-object v2, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-boolean v2, v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->m:Z

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->l(Z)V

    .line 273
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->setTag(Ljava/lang/Object;)V

    .line 274
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 275
    if-eqz v1, :cond_11

    move v0, v5

    :goto_7
    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setClickable(Z)V

    .line 276
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 277
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    aget-object v0, v0, p1

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 279
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->i:[I

    aget v0, v0, p1

    iget-object v1, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v1, v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    add-int/lit8 v1, v1, -0x1

    if-ge p1, v1, :cond_6

    sget-object v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    iget v4, v1, Llct;->ab:I

    :cond_6
    add-int/2addr v0, v4

    .line 281
    new-instance v1, Llji;

    iget-object v2, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v2, v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->j:I

    invoke-direct {v1, v0, v2}, Llji;-><init>(II)V

    .line 283
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 284
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Ljbb;)V

    goto/16 :goto_0

    .line 177
    :cond_7
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    if-eqz v0, :cond_8

    .line 178
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v1, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v1, v1, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->d:Lkzv;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;Lkzv;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 179
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    move-object v1, v0

    goto/16 :goto_1

    .line 181
    :cond_8
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->c:[Lppr;

    if-eqz v0, :cond_12

    .line 182
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    move-object v1, v0

    goto/16 :goto_1

    .line 192
    :cond_9
    new-instance p2, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {p2, v6}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    goto/16 :goto_2

    :cond_a
    move v3, v4

    .line 195
    goto/16 :goto_3

    :cond_b
    move v0, v4

    .line 213
    goto/16 :goto_4

    .line 223
    :cond_c
    if-nez v7, :cond_d

    .line 224
    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 226
    :cond_d
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    if-ne v0, v5, :cond_f

    .line 227
    iget-object v0, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->h:[Lizu;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v8, Ljac;->b:Ljac;

    if-ne v0, v8, :cond_e

    .line 228
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f110029

    new-array v8, v5, [Ljava/lang/Object;

    .line 229
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    .line 228
    invoke-virtual {v0, v6, v5, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 231
    :cond_e
    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v6, 0x7f110028

    new-array v8, v5, [Ljava/lang/Object;

    .line 232
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    .line 231
    invoke-virtual {v0, v6, v5, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 235
    :cond_f
    const v0, 0x7f0a04b2

    new-array v8, v9, [Ljava/lang/Object;

    add-int/lit8 v9, p1, 0x1

    .line 236
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    iget-object v9, p0, Lldo;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;

    iget v9, v9, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->f:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    .line 235
    invoke-virtual {v6, v0, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 246
    :cond_10
    invoke-virtual {p2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 247
    const v2, 0x3ecccccd    # 0.4f

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(F)V

    .line 248
    sget-object v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamAlbumViewGroup;->a:Llct;

    iget-object v2, v2, Llct;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 250
    invoke-virtual {p2, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->g(Z)V

    goto/16 :goto_6

    :cond_11
    move v0, v4

    .line 275
    goto/16 :goto_7

    :cond_12
    move-object v1, v2

    goto/16 :goto_1
.end method
