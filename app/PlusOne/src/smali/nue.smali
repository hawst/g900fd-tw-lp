.class public final Lnue;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lnud;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1015
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1018
    sget-object v0, Lnud;->a:[Lnud;

    iput-object v0, p0, Lnue;->a:[Lnud;

    .line 1015
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1036
    .line 1037
    iget-object v1, p0, Lnue;->a:[Lnud;

    if-eqz v1, :cond_1

    .line 1038
    iget-object v2, p0, Lnue;->a:[Lnud;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1039
    if-eqz v4, :cond_0

    .line 1040
    const/4 v5, 0x1

    .line 1041
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1038
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1045
    :cond_1
    iget-object v1, p0, Lnue;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1046
    iput v0, p0, Lnue;->ai:I

    .line 1047
    return v0
.end method

.method public a(Loxn;)Lnue;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1055
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1056
    sparse-switch v0, :sswitch_data_0

    .line 1060
    iget-object v2, p0, Lnue;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1061
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnue;->ah:Ljava/util/List;

    .line 1064
    :cond_1
    iget-object v2, p0, Lnue;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1066
    :sswitch_0
    return-object p0

    .line 1071
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1072
    iget-object v0, p0, Lnue;->a:[Lnud;

    if-nez v0, :cond_3

    move v0, v1

    .line 1073
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnud;

    .line 1074
    iget-object v3, p0, Lnue;->a:[Lnud;

    if-eqz v3, :cond_2

    .line 1075
    iget-object v3, p0, Lnue;->a:[Lnud;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1077
    :cond_2
    iput-object v2, p0, Lnue;->a:[Lnud;

    .line 1078
    :goto_2
    iget-object v2, p0, Lnue;->a:[Lnud;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1079
    iget-object v2, p0, Lnue;->a:[Lnud;

    new-instance v3, Lnud;

    invoke-direct {v3}, Lnud;-><init>()V

    aput-object v3, v2, v0

    .line 1080
    iget-object v2, p0, Lnue;->a:[Lnud;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1081
    invoke-virtual {p1}, Loxn;->a()I

    .line 1078
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1072
    :cond_3
    iget-object v0, p0, Lnue;->a:[Lnud;

    array-length v0, v0

    goto :goto_1

    .line 1084
    :cond_4
    iget-object v2, p0, Lnue;->a:[Lnud;

    new-instance v3, Lnud;

    invoke-direct {v3}, Lnud;-><init>()V

    aput-object v3, v2, v0

    .line 1085
    iget-object v2, p0, Lnue;->a:[Lnud;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1056
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1023
    iget-object v0, p0, Lnue;->a:[Lnud;

    if-eqz v0, :cond_1

    .line 1024
    iget-object v1, p0, Lnue;->a:[Lnud;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1025
    if-eqz v3, :cond_0

    .line 1026
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1024
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1030
    :cond_1
    iget-object v0, p0, Lnue;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1032
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1011
    invoke-virtual {p0, p1}, Lnue;->a(Loxn;)Lnue;

    move-result-object v0

    return-object v0
.end method
