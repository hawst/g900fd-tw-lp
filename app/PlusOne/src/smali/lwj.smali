.class public final Llwj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llwj;


# instance fields
.field private b:Lotf;

.field private c:Losl;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/Long;

.field private f:Llwk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 384
    const/4 v0, 0x0

    new-array v0, v0, [Llwj;

    sput-object v0, Llwj;->a:[Llwj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 385
    invoke-direct {p0}, Loxq;-><init>()V

    .line 388
    iput-object v0, p0, Llwj;->b:Lotf;

    .line 391
    iput-object v0, p0, Llwj;->c:Losl;

    .line 398
    iput-object v0, p0, Llwj;->f:Llwk;

    .line 385
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 424
    const/4 v0, 0x0

    .line 425
    iget-object v1, p0, Llwj;->b:Lotf;

    if-eqz v1, :cond_0

    .line 426
    const/4 v0, 0x1

    iget-object v1, p0, Llwj;->b:Lotf;

    .line 427
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 429
    :cond_0
    iget-object v1, p0, Llwj;->d:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 430
    const/4 v1, 0x2

    iget-object v2, p0, Llwj;->d:Ljava/lang/Long;

    .line 431
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 433
    :cond_1
    iget-object v1, p0, Llwj;->e:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 434
    const/4 v1, 0x3

    iget-object v2, p0, Llwj;->e:Ljava/lang/Long;

    .line 435
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 437
    :cond_2
    iget-object v1, p0, Llwj;->c:Losl;

    if-eqz v1, :cond_3

    .line 438
    const/4 v1, 0x4

    iget-object v2, p0, Llwj;->c:Losl;

    .line 439
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 441
    :cond_3
    iget-object v1, p0, Llwj;->f:Llwk;

    if-eqz v1, :cond_4

    .line 442
    const/4 v1, 0x5

    iget-object v2, p0, Llwj;->f:Llwk;

    .line 443
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 445
    :cond_4
    iget-object v1, p0, Llwj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 446
    iput v0, p0, Llwj;->ai:I

    .line 447
    return v0
.end method

.method public a(Loxn;)Llwj;
    .locals 2

    .prologue
    .line 455
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 456
    sparse-switch v0, :sswitch_data_0

    .line 460
    iget-object v1, p0, Llwj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 461
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llwj;->ah:Ljava/util/List;

    .line 464
    :cond_1
    iget-object v1, p0, Llwj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 466
    :sswitch_0
    return-object p0

    .line 471
    :sswitch_1
    iget-object v0, p0, Llwj;->b:Lotf;

    if-nez v0, :cond_2

    .line 472
    new-instance v0, Lotf;

    invoke-direct {v0}, Lotf;-><init>()V

    iput-object v0, p0, Llwj;->b:Lotf;

    .line 474
    :cond_2
    iget-object v0, p0, Llwj;->b:Lotf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 478
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llwj;->d:Ljava/lang/Long;

    goto :goto_0

    .line 482
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llwj;->e:Ljava/lang/Long;

    goto :goto_0

    .line 486
    :sswitch_4
    iget-object v0, p0, Llwj;->c:Losl;

    if-nez v0, :cond_3

    .line 487
    new-instance v0, Losl;

    invoke-direct {v0}, Losl;-><init>()V

    iput-object v0, p0, Llwj;->c:Losl;

    .line 489
    :cond_3
    iget-object v0, p0, Llwj;->c:Losl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 493
    :sswitch_5
    iget-object v0, p0, Llwj;->f:Llwk;

    if-nez v0, :cond_4

    .line 494
    new-instance v0, Llwk;

    invoke-direct {v0}, Llwk;-><init>()V

    iput-object v0, p0, Llwj;->f:Llwk;

    .line 496
    :cond_4
    iget-object v0, p0, Llwj;->f:Llwk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 456
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 403
    iget-object v0, p0, Llwj;->b:Lotf;

    if-eqz v0, :cond_0

    .line 404
    const/4 v0, 0x1

    iget-object v1, p0, Llwj;->b:Lotf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 406
    :cond_0
    iget-object v0, p0, Llwj;->d:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 407
    const/4 v0, 0x2

    iget-object v1, p0, Llwj;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 409
    :cond_1
    iget-object v0, p0, Llwj;->e:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 410
    const/4 v0, 0x3

    iget-object v1, p0, Llwj;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 412
    :cond_2
    iget-object v0, p0, Llwj;->c:Losl;

    if-eqz v0, :cond_3

    .line 413
    const/4 v0, 0x4

    iget-object v1, p0, Llwj;->c:Losl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 415
    :cond_3
    iget-object v0, p0, Llwj;->f:Llwk;

    if-eqz v0, :cond_4

    .line 416
    const/4 v0, 0x5

    iget-object v1, p0, Llwj;->f:Llwk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 418
    :cond_4
    iget-object v0, p0, Llwj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 420
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 381
    invoke-virtual {p0, p1}, Llwj;->a(Loxn;)Llwj;

    move-result-object v0

    return-object v0
.end method
