.class final Ljgl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:[Ljgk;

.field public b:I


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput v0, p0, Ljgl;->b:I

    .line 49
    new-array v1, p1, [Ljgk;

    iput-object v1, p0, Ljgl;->a:[Ljgk;

    .line 50
    :goto_0
    if-ge v0, p1, :cond_0

    .line 51
    iget-object v1, p0, Ljgl;->a:[Ljgk;

    new-instance v2, Ljgk;

    invoke-direct {v2}, Ljgk;-><init>()V

    aput-object v2, v1, v0

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljga;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 56
    move v1, v0

    .line 57
    :goto_0
    iget v2, p0, Ljgl;->b:I

    if-ge v0, v2, :cond_2

    .line 58
    iget-object v2, p0, Ljgl;->a:[Ljgk;

    aget-object v2, v2, v0

    iget-object v2, v2, Ljgk;->a:Ljava/lang/String;

    .line 59
    iget-object v3, p0, Ljgl;->a:[Ljgk;

    aget-object v3, v3, v0

    iget-wide v4, v3, Ljgk;->b:J

    .line 60
    invoke-interface {p1, v2}, Ljga;->a(Ljava/lang/String;)Ljfy;

    move-result-object v2

    .line 61
    if-eqz v2, :cond_0

    invoke-virtual {v2, v4, v5}, Ljfy;->a(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    add-int/lit8 v1, v1, 0x1

    .line 57
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    iget-object v2, p0, Ljgl;->a:[Ljgk;

    array-length v3, v2

    move v2, v0

    :goto_2
    add-int/lit8 v4, v2, 0x1

    if-ge v4, v3, :cond_1

    iget-object v4, p0, Ljgl;->a:[Ljgk;

    aget-object v4, v4, v2

    iget-object v5, p0, Ljgl;->a:[Ljgk;

    add-int/lit8 v6, v2, 0x1

    aget-object v5, v5, v6

    iget-object v5, v5, Ljgk;->a:Ljava/lang/String;

    iput-object v5, v4, Ljgk;->a:Ljava/lang/String;

    iget-object v4, p0, Ljgl;->a:[Ljgk;

    aget-object v4, v4, v2

    iget-object v5, p0, Ljgl;->a:[Ljgk;

    add-int/lit8 v6, v2, 0x1

    aget-object v5, v5, v6

    iget-wide v6, v5, Ljgk;->b:J

    iput-wide v6, v4, Ljgk;->b:J

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    iget-object v2, p0, Ljgl;->a:[Ljgk;

    iget-object v3, p0, Ljgl;->a:[Ljgk;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljgk;->a()V

    goto :goto_1

    .line 67
    :cond_2
    iput v1, p0, Ljgl;->b:I

    .line 68
    iget v0, p0, Ljgl;->b:I

    return v0
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Ljgl;->a:[Ljgk;

    array-length v2, v0

    move v0, v1

    .line 73
    :goto_0
    if-ge v0, v2, :cond_0

    .line 74
    iget-object v3, p0, Ljgl;->a:[Ljgk;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljgk;->a()V

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    iput v1, p0, Ljgl;->b:I

    .line 77
    return-void
.end method

.method a(I)V
    .locals 4

    .prologue
    .line 147
    :goto_0
    if-lez p1, :cond_0

    .line 148
    iget-object v0, p0, Ljgl;->a:[Ljgk;

    aget-object v0, v0, p1

    iget-object v1, p0, Ljgl;->a:[Ljgk;

    add-int/lit8 v2, p1, -0x1

    aget-object v1, v1, v2

    iget-object v1, v1, Ljgk;->a:Ljava/lang/String;

    iput-object v1, v0, Ljgk;->a:Ljava/lang/String;

    .line 149
    iget-object v0, p0, Ljgl;->a:[Ljgk;

    aget-object v0, v0, p1

    iget-object v1, p0, Ljgl;->a:[Ljgk;

    add-int/lit8 v2, p1, -0x1

    aget-object v1, v1, v2

    iget-wide v2, v1, Ljgk;->b:J

    iput-wide v2, v0, Ljgk;->b:J

    .line 147
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 151
    :cond_0
    iget-object v0, p0, Ljgl;->a:[Ljgk;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljgk;->a()V

    .line 152
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 104
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    const-string v0, ","

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 106
    array-length v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 107
    if-lez v0, :cond_0

    .line 108
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 109
    aget-object v2, v1, v0

    .line 110
    add-int/lit8 v3, v0, 0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 112
    iget-object v3, p0, Ljgl;->a:[Ljgk;

    div-int/lit8 v6, v0, 0x2

    new-instance v7, Ljgk;

    invoke-direct {v7}, Ljgk;-><init>()V

    aput-object v7, v3, v6

    .line 113
    iget-object v3, p0, Ljgl;->a:[Ljgk;

    div-int/lit8 v6, v0, 0x2

    aget-object v3, v3, v6

    iput-object v2, v3, Ljgk;->a:Ljava/lang/String;

    .line 114
    iget-object v2, p0, Ljgl;->a:[Ljgk;

    div-int/lit8 v3, v0, 0x2

    aget-object v2, v2, v3

    iput-wide v4, v2, Ljgk;->b:J

    .line 116
    iget v2, p0, Ljgl;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Ljgl;->b:I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Ljgl;->a()V

    .line 124
    :cond_0
    return-void
.end method

.method a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 175
    iget-object v0, p0, Ljgl;->a:[Ljgk;

    aget-object v0, v0, v1

    iput-object p1, v0, Ljgk;->a:Ljava/lang/String;

    .line 176
    iget-object v0, p0, Ljgl;->a:[Ljgk;

    aget-object v0, v0, v1

    iput-wide p2, v0, Ljgk;->b:J

    .line 177
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 86
    iget v0, p0, Ljgl;->b:I

    if-nez v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 99
    :goto_0
    return-object v0

    .line 90
    :cond_0
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 91
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, Ljgl;->b:I

    if-ge v0, v2, :cond_1

    .line 92
    iget-object v2, p0, Ljgl;->a:[Ljgk;

    aget-object v2, v2, v0

    .line 93
    iget-object v3, v2, Ljgk;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-wide v2, v2, Ljgk;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 96
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 98
    :cond_1
    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 99
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;J)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 201
    iget v0, p0, Ljgl;->b:I

    iget-object v3, p0, Ljgl;->a:[Ljgk;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v0, v1

    .line 204
    :goto_0
    iget-object v4, p0, Ljgl;->a:[Ljgk;

    array-length v4, v4

    if-ge v0, v4, :cond_6

    .line 205
    iget-object v4, p0, Ljgl;->a:[Ljgk;

    aget-object v4, v4, v0

    iget-object v4, v4, Ljgk;->a:Ljava/lang/String;

    iget-object v5, p0, Ljgl;->a:[Ljgk;

    aget-object v5, v5, v0

    iget-wide v6, v5, Ljgk;->b:J

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    move v4, v1

    :goto_1
    if-eqz v4, :cond_4

    move v3, v0

    move v0, v1

    .line 215
    :goto_2
    invoke-virtual {p0, v3}, Ljgl;->a(I)V

    .line 216
    invoke-virtual {p0, p1, p2, p3}, Ljgl;->a(Ljava/lang/String;J)V

    .line 218
    if-eqz v0, :cond_5

    .line 219
    iget v0, p0, Ljgl;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljgl;->b:I

    .line 220
    iget v0, p0, Ljgl;->b:I

    iget-object v1, p0, Ljgl;->a:[Ljgk;

    array-length v1, v1

    if-le v0, v1, :cond_1

    .line 221
    iget-object v0, p0, Ljgl;->a:[Ljgk;

    array-length v0, v0

    iput v0, p0, Ljgl;->b:I

    .line 225
    :cond_1
    :goto_3
    return v2

    .line 205
    :cond_2
    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    cmp-long v4, p2, v6

    if-nez v4, :cond_3

    move v4, v2

    goto :goto_1

    :cond_3
    move v4, v1

    goto :goto_1

    .line 204
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v2, v1

    .line 225
    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_2
.end method
