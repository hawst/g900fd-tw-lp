.class public final Lpwn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpwn;


# instance fields
.field private b:Ljava/lang/String;

.field private c:[Lpwk;

.field private d:Lpwk;

.field private e:Lpwk;

.field private f:Lppf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    new-array v0, v0, [Lpwn;

    sput-object v0, Lpwn;->a:[Lpwn;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 155
    invoke-direct {p0}, Loxq;-><init>()V

    .line 160
    sget-object v0, Lpwk;->a:[Lpwk;

    iput-object v0, p0, Lpwn;->c:[Lpwk;

    .line 163
    iput-object v1, p0, Lpwn;->d:Lpwk;

    .line 166
    iput-object v1, p0, Lpwn;->e:Lpwk;

    .line 169
    iput-object v1, p0, Lpwn;->f:Lppf;

    .line 155
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    .line 197
    const/4 v0, 0x1

    iget-object v1, p0, Lpwn;->b:Ljava/lang/String;

    .line 199
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 200
    iget-object v1, p0, Lpwn;->c:[Lpwk;

    if-eqz v1, :cond_1

    .line 201
    iget-object v2, p0, Lpwn;->c:[Lpwk;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 202
    if-eqz v4, :cond_0

    .line 203
    const/4 v5, 0x2

    .line 204
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 201
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :cond_1
    iget-object v1, p0, Lpwn;->d:Lpwk;

    if-eqz v1, :cond_2

    .line 209
    const/4 v1, 0x3

    iget-object v2, p0, Lpwn;->d:Lpwk;

    .line 210
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_2
    iget-object v1, p0, Lpwn;->e:Lpwk;

    if-eqz v1, :cond_3

    .line 213
    const/4 v1, 0x4

    iget-object v2, p0, Lpwn;->e:Lpwk;

    .line 214
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_3
    iget-object v1, p0, Lpwn;->f:Lppf;

    if-eqz v1, :cond_4

    .line 217
    const/4 v1, 0x5

    iget-object v2, p0, Lpwn;->f:Lppf;

    .line 218
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_4
    iget-object v1, p0, Lpwn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    iput v0, p0, Lpwn;->ai:I

    .line 222
    return v0
.end method

.method public a(Loxn;)Lpwn;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 230
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 231
    sparse-switch v0, :sswitch_data_0

    .line 235
    iget-object v2, p0, Lpwn;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 236
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpwn;->ah:Ljava/util/List;

    .line 239
    :cond_1
    iget-object v2, p0, Lpwn;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    :sswitch_0
    return-object p0

    .line 246
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpwn;->b:Ljava/lang/String;

    goto :goto_0

    .line 250
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 251
    iget-object v0, p0, Lpwn;->c:[Lpwk;

    if-nez v0, :cond_3

    move v0, v1

    .line 252
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpwk;

    .line 253
    iget-object v3, p0, Lpwn;->c:[Lpwk;

    if-eqz v3, :cond_2

    .line 254
    iget-object v3, p0, Lpwn;->c:[Lpwk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 256
    :cond_2
    iput-object v2, p0, Lpwn;->c:[Lpwk;

    .line 257
    :goto_2
    iget-object v2, p0, Lpwn;->c:[Lpwk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 258
    iget-object v2, p0, Lpwn;->c:[Lpwk;

    new-instance v3, Lpwk;

    invoke-direct {v3}, Lpwk;-><init>()V

    aput-object v3, v2, v0

    .line 259
    iget-object v2, p0, Lpwn;->c:[Lpwk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 260
    invoke-virtual {p1}, Loxn;->a()I

    .line 257
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 251
    :cond_3
    iget-object v0, p0, Lpwn;->c:[Lpwk;

    array-length v0, v0

    goto :goto_1

    .line 263
    :cond_4
    iget-object v2, p0, Lpwn;->c:[Lpwk;

    new-instance v3, Lpwk;

    invoke-direct {v3}, Lpwk;-><init>()V

    aput-object v3, v2, v0

    .line 264
    iget-object v2, p0, Lpwn;->c:[Lpwk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 268
    :sswitch_3
    iget-object v0, p0, Lpwn;->d:Lpwk;

    if-nez v0, :cond_5

    .line 269
    new-instance v0, Lpwk;

    invoke-direct {v0}, Lpwk;-><init>()V

    iput-object v0, p0, Lpwn;->d:Lpwk;

    .line 271
    :cond_5
    iget-object v0, p0, Lpwn;->d:Lpwk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 275
    :sswitch_4
    iget-object v0, p0, Lpwn;->e:Lpwk;

    if-nez v0, :cond_6

    .line 276
    new-instance v0, Lpwk;

    invoke-direct {v0}, Lpwk;-><init>()V

    iput-object v0, p0, Lpwn;->e:Lpwk;

    .line 278
    :cond_6
    iget-object v0, p0, Lpwn;->e:Lpwk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 282
    :sswitch_5
    iget-object v0, p0, Lpwn;->f:Lppf;

    if-nez v0, :cond_7

    .line 283
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpwn;->f:Lppf;

    .line 285
    :cond_7
    iget-object v0, p0, Lpwn;->f:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 231
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 174
    const/4 v0, 0x1

    iget-object v1, p0, Lpwn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 175
    iget-object v0, p0, Lpwn;->c:[Lpwk;

    if-eqz v0, :cond_1

    .line 176
    iget-object v1, p0, Lpwn;->c:[Lpwk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 177
    if-eqz v3, :cond_0

    .line 178
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 176
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_1
    iget-object v0, p0, Lpwn;->d:Lpwk;

    if-eqz v0, :cond_2

    .line 183
    const/4 v0, 0x3

    iget-object v1, p0, Lpwn;->d:Lpwk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 185
    :cond_2
    iget-object v0, p0, Lpwn;->e:Lpwk;

    if-eqz v0, :cond_3

    .line 186
    const/4 v0, 0x4

    iget-object v1, p0, Lpwn;->e:Lpwk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 188
    :cond_3
    iget-object v0, p0, Lpwn;->f:Lppf;

    if-eqz v0, :cond_4

    .line 189
    const/4 v0, 0x5

    iget-object v1, p0, Lpwn;->f:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 191
    :cond_4
    iget-object v0, p0, Lpwn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 193
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0, p1}, Lpwn;->a(Loxn;)Lpwn;

    move-result-object v0

    return-object v0
.end method
