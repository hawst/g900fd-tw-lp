.class public final Ldpz;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Z

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .prologue
    .line 51
    const-string v0, "RemoveReportBanTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 53
    iput p2, p0, Ldpz;->a:I

    .line 54
    iput-object p3, p0, Ldpz;->b:Ljava/lang/String;

    .line 55
    iput-object p4, p0, Ldpz;->c:Ljava/lang/String;

    .line 56
    iput-object p5, p0, Ldpz;->d:Ljava/lang/String;

    .line 57
    iput-object p6, p0, Ldpz;->e:Ljava/lang/String;

    .line 58
    iput-boolean p7, p0, Ldpz;->f:Z

    .line 59
    iput-boolean p8, p0, Ldpz;->h:Z

    .line 60
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 11

    .prologue
    .line 64
    invoke-virtual {p0}, Ldpz;->f()Landroid/content/Context;

    move-result-object v1

    .line 65
    iget v2, p0, Ldpz;->a:I

    .line 66
    new-instance v5, Lkfo;

    invoke-direct {v5, v1, v2}, Lkfo;-><init>(Landroid/content/Context;I)V

    .line 67
    invoke-static {v1, v5}, Lkgi;->a(Landroid/content/Context;Lkfo;)Lkfu;

    move-result-object v9

    .line 69
    new-instance v10, Lkvo;

    new-instance v0, Lkfo;

    invoke-direct {v0, v1, v2}, Lkfo;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Ldpz;->c:Ljava/lang/String;

    const/4 v4, 0x7

    invoke-direct {v10, v1, v0, v3, v4}, Lkvo;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;I)V

    .line 71
    iget-object v0, p0, Ldpz;->b:Ljava/lang/String;

    invoke-virtual {v10, v0}, Lkvo;->b(Ljava/lang/String;)V

    .line 72
    invoke-virtual {v9, v10}, Lkfu;->a(Lkff;)V

    .line 74
    const/4 v6, 0x0

    .line 75
    iget-boolean v0, p0, Ldpz;->h:Z

    if-eqz v0, :cond_3

    .line 77
    new-instance v0, Ldms;

    iget-object v3, p0, Ldpz;->d:Ljava/lang/String;

    iget-object v4, p0, Ldpz;->e:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Ldms;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v9, v0}, Lkfu;->a(Lkff;)V

    move-object v3, v6

    .line 84
    :goto_0
    invoke-virtual {v9}, Lkfu;->l()V

    .line 86
    invoke-virtual {v10}, Lkvo;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    const-class v0, Lktq;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iget-object v1, p0, Ldpz;->c:Ljava/lang/String;

    iget-object v4, p0, Ldpz;->b:Ljava/lang/String;

    const/4 v5, 0x7

    .line 88
    invoke-interface {v0, v2, v1, v4, v5}, Lktq;->a(ILjava/lang/String;Ljava/lang/String;I)V

    .line 91
    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lkya;->t()Z

    move-result v0

    if-nez v0, :cond_1

    .line 92
    invoke-virtual {p0}, Ldpz;->f()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Ldpz;->a:I

    iget-object v2, p0, Ldpz;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Llap;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 95
    :cond_1
    new-instance v1, Lhoz;

    iget v2, v9, Lkff;->i:I

    iget-object v3, v9, Lkff;->k:Ljava/lang/Exception;

    .line 96
    invoke-virtual {v9}, Lkfu;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Ldpz;->f()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a04c0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 97
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "remove_post"

    iget-boolean v0, p0, Ldpz;->f:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Ldpz;->h:Z

    if-eqz v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 98
    return-object v1

    .line 79
    :cond_3
    iget-boolean v0, p0, Ldpz;->f:Z

    if-eqz v0, :cond_6

    .line 80
    new-instance v3, Lkya;

    iget-object v6, p0, Ldpz;->c:Ljava/lang/String;

    iget-object v7, p0, Ldpz;->d:Ljava/lang/String;

    const/4 v8, 0x2

    move-object v4, v1

    invoke-direct/range {v3 .. v8}, Lkya;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;I)V

    .line 82
    invoke-virtual {v9, v3}, Lkfu;->a(Lkff;)V

    goto :goto_0

    .line 96
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 97
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    move-object v3, v6

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Ldpz;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a058d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
