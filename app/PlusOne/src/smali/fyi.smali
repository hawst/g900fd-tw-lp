.class public final enum Lfyi;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lfyi;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lfyi;

.field public static final enum b:Lfyi;

.field private static final synthetic c:[Lfyi;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    new-instance v0, Lfyi;

    const-string v1, "ALWAYS_VISIBLE"

    invoke-direct {v0, v1, v2}, Lfyi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfyi;->a:Lfyi;

    .line 59
    new-instance v0, Lfyi;

    const-string v1, "VISIBLE_WHEN_ACTIVE"

    invoke-direct {v0, v1, v3}, Lfyi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfyi;->b:Lfyi;

    .line 57
    const/4 v0, 0x2

    new-array v0, v0, [Lfyi;

    sget-object v1, Lfyi;->a:Lfyi;

    aput-object v1, v0, v2

    sget-object v1, Lfyi;->b:Lfyi;

    aput-object v1, v0, v3

    sput-object v0, Lfyi;->c:[Lfyi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfyi;
    .locals 1

    .prologue
    .line 57
    const-class v0, Lfyi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfyi;

    return-object v0
.end method

.method public static values()[Lfyi;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lfyi;->c:[Lfyi;

    invoke-virtual {v0}, [Lfyi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfyi;

    return-object v0
.end method
