.class public final Lool;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Double;

.field public b:Ljava/lang/Double;

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Double;

.field public e:Ljava/lang/Double;

.field public f:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 568
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 609
    const/4 v0, 0x0

    .line 610
    iget-object v1, p0, Lool;->a:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 611
    const/4 v0, 0x1

    iget-object v1, p0, Lool;->a:Ljava/lang/Double;

    .line 612
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 614
    :cond_0
    iget-object v1, p0, Lool;->b:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 615
    const/4 v1, 0x2

    iget-object v2, p0, Lool;->b:Ljava/lang/Double;

    .line 616
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 618
    :cond_1
    iget-object v1, p0, Lool;->c:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 619
    const/4 v1, 0x3

    iget-object v2, p0, Lool;->c:Ljava/lang/Double;

    .line 620
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 622
    :cond_2
    iget-object v1, p0, Lool;->d:Ljava/lang/Double;

    if-eqz v1, :cond_3

    .line 623
    const/4 v1, 0x4

    iget-object v2, p0, Lool;->d:Ljava/lang/Double;

    .line 624
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 626
    :cond_3
    iget-object v1, p0, Lool;->e:Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 627
    const/4 v1, 0x5

    iget-object v2, p0, Lool;->e:Ljava/lang/Double;

    .line 628
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 630
    :cond_4
    iget-object v1, p0, Lool;->f:Ljava/lang/Double;

    if-eqz v1, :cond_5

    .line 631
    const/4 v1, 0x6

    iget-object v2, p0, Lool;->f:Ljava/lang/Double;

    .line 632
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 634
    :cond_5
    iget-object v1, p0, Lool;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 635
    iput v0, p0, Lool;->ai:I

    .line 636
    return v0
.end method

.method public a(Loxn;)Lool;
    .locals 2

    .prologue
    .line 644
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 645
    sparse-switch v0, :sswitch_data_0

    .line 649
    iget-object v1, p0, Lool;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 650
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lool;->ah:Ljava/util/List;

    .line 653
    :cond_1
    iget-object v1, p0, Lool;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 655
    :sswitch_0
    return-object p0

    .line 660
    :sswitch_1
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lool;->a:Ljava/lang/Double;

    goto :goto_0

    .line 664
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lool;->b:Ljava/lang/Double;

    goto :goto_0

    .line 668
    :sswitch_3
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lool;->c:Ljava/lang/Double;

    goto :goto_0

    .line 672
    :sswitch_4
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lool;->d:Ljava/lang/Double;

    goto :goto_0

    .line 676
    :sswitch_5
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lool;->e:Ljava/lang/Double;

    goto :goto_0

    .line 680
    :sswitch_6
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lool;->f:Ljava/lang/Double;

    goto :goto_0

    .line 645
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x21 -> :sswitch_4
        0x29 -> :sswitch_5
        0x31 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 585
    iget-object v0, p0, Lool;->a:Ljava/lang/Double;

    if-eqz v0, :cond_0

    .line 586
    const/4 v0, 0x1

    iget-object v1, p0, Lool;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 588
    :cond_0
    iget-object v0, p0, Lool;->b:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 589
    const/4 v0, 0x2

    iget-object v1, p0, Lool;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 591
    :cond_1
    iget-object v0, p0, Lool;->c:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 592
    const/4 v0, 0x3

    iget-object v1, p0, Lool;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 594
    :cond_2
    iget-object v0, p0, Lool;->d:Ljava/lang/Double;

    if-eqz v0, :cond_3

    .line 595
    const/4 v0, 0x4

    iget-object v1, p0, Lool;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 597
    :cond_3
    iget-object v0, p0, Lool;->e:Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 598
    const/4 v0, 0x5

    iget-object v1, p0, Lool;->e:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 600
    :cond_4
    iget-object v0, p0, Lool;->f:Ljava/lang/Double;

    if-eqz v0, :cond_5

    .line 601
    const/4 v0, 0x6

    iget-object v1, p0, Lool;->f:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 603
    :cond_5
    iget-object v0, p0, Lool;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 605
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 564
    invoke-virtual {p0, p1}, Lool;->a(Loxn;)Lool;

    move-result-object v0

    return-object v0
.end method
