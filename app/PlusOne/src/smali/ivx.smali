.class public Livx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhee;
.implements Lhep;
.implements Liwd;
.implements Llnx;
.implements Llqz;
.implements Llra;
.implements Llrb;
.implements Llrc;
.implements Llrd;
.implements Llre;
.implements Llrg;


# instance fields
.field private final a:Lz;

.field private b:Lhei;

.field private c:Liwc;

.field private d:Lher;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:Liwg;

.field private j:Liwg;

.field private k:Liwg;

.field private l:I

.field private m:Z

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lheg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lz;Llqr;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const/4 v0, 0x1

    iput v0, p0, Livx;->g:I

    .line 71
    const/4 v0, -0x1

    iput v0, p0, Livx;->h:I

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Livx;->n:Ljava/util/List;

    .line 91
    iput-object p1, p0, Livx;->a:Lz;

    .line 92
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 93
    return-void
.end method

.method private a(Liwg;II)V
    .locals 7

    .prologue
    .line 294
    iget v2, p0, Livx;->g:I

    .line 295
    iget v4, p0, Livx;->h:I

    .line 296
    iput-object p1, p0, Livx;->i:Liwg;

    .line 297
    iput p2, p0, Livx;->g:I

    .line 298
    iput p3, p0, Livx;->h:I

    .line 300
    if-ne p2, v2, :cond_0

    if-eq p3, v4, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 302
    :goto_0
    iget-object v0, p0, Livx;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lheg;

    move v3, p2

    move v5, p3

    .line 303
    invoke-interface/range {v0 .. v5}, Lheg;->a(ZIIII)V

    goto :goto_1

    .line 300
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 306
    :cond_2
    return-void
.end method

.method private a(Liwg;I)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 327
    iget-object v2, p0, Livx;->b:Lhei;

    invoke-interface {v2, p2}, Lhei;->c(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 328
    iget-object v2, p0, Livx;->b:Lhei;

    invoke-interface {v2, p2}, Lhei;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Livx;->b:Lhei;

    invoke-interface {v2, p2}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Livx;->d:Lher;

    invoke-interface {v2}, Lher;->a()[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v2, v1

    :goto_1
    if-nez v2, :cond_1

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    iget-object v2, p0, Livx;->c:Liwc;

    .line 329
    invoke-interface {v2, p1, p2}, Liwc;->a(Liwg;I)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_3
    return v0

    .line 328
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 329
    goto :goto_3

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method private i()V
    .locals 4

    .prologue
    .line 223
    iget-boolean v0, p0, Livx;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Livx;->j:Liwg;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Livx;->c:Liwc;

    iget-object v1, p0, Livx;->a:Lz;

    invoke-virtual {v1}, Lz;->f()Lae;

    move-result-object v1

    iget-object v2, p0, Livx;->j:Liwg;

    iget-object v3, p0, Livx;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Liwc;->a(Lae;Liwg;Ljava/lang/String;)V

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Livx;->j:Liwg;

    .line 227
    :cond_0
    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 254
    invoke-direct {p0}, Livx;->m()V

    .line 256
    iget-object v0, p0, Livx;->a:Lz;

    invoke-virtual {v0}, Lz;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-direct {p0}, Livx;->i()V

    .line 261
    invoke-direct {p0}, Livx;->k()V

    goto :goto_0
.end method

.method private k()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x3

    const/4 v4, -0x1

    .line 265
    iget-boolean v0, p0, Livx;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Livx;->k:Liwg;

    if-eqz v0, :cond_0

    .line 266
    iget v5, p0, Livx;->l:I

    .line 267
    if-eq v5, v4, :cond_1

    move v0, v1

    .line 269
    :goto_0
    iget-object v3, p0, Livx;->k:Liwg;

    .line 272
    if-ne v5, v4, :cond_3

    iget v6, p0, Livx;->h:I

    if-eq v6, v4, :cond_3

    .line 273
    iget v0, p0, Livx;->h:I

    invoke-direct {p0, v3, v0}, Livx;->a(Liwg;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 274
    iget v3, p0, Livx;->h:I

    .line 276
    iget-object v0, p0, Livx;->i:Liwg;

    .line 284
    :goto_1
    iput v4, p0, Livx;->l:I

    .line 285
    iput-object v2, p0, Livx;->k:Liwg;

    .line 287
    invoke-direct {p0, v0, v1, v3}, Livx;->a(Liwg;II)V

    .line 289
    :cond_0
    return-void

    .line 267
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 279
    :cond_2
    const/4 v0, 0x1

    move v1, v0

    move v3, v4

    move-object v0, v2

    .line 280
    goto :goto_1

    :cond_3
    move v1, v0

    move-object v0, v3

    move v3, v5

    goto :goto_1
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 334
    invoke-virtual {p0}, Livx;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    iget v0, p0, Livx;->h:I

    if-eq v0, v2, :cond_0

    .line 336
    iget-object v0, p0, Livx;->i:Liwg;

    iget v1, p0, Livx;->h:I

    invoke-direct {p0, v0, v1}, Livx;->a(Liwg;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 337
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v2}, Livx;->a(Liwg;II)V

    .line 341
    :cond_0
    return-void
.end method


# virtual methods
.method public E_()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Livx;->b:Lhei;

    invoke-interface {v0, p0}, Lhei;->b(Lhep;)V

    .line 143
    iget-object v0, p0, Livx;->c:Liwc;

    invoke-interface {v0, p0}, Liwc;->b(Liwd;)V

    .line 144
    return-void
.end method

.method public synthetic a(Lheg;)Lhee;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Livx;
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Livx;->f:Ljava/lang/String;

    .line 197
    return-object p0
.end method

.method public a(Llnh;)Livx;
    .locals 1

    .prologue
    .line 96
    const-class v0, Lhee;

    invoke-virtual {p1, v0, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 97
    const-class v0, Livx;

    invoke-virtual {p1, v0, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 98
    return-object p0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Livx;->j()V

    .line 154
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 103
    const-class v0, Lhei;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Livx;->b:Lhei;

    .line 104
    const-class v0, Liwc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwc;

    iput-object v0, p0, Livx;->c:Liwc;

    .line 105
    const-class v0, Lher;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lher;

    iput-object v0, p0, Livx;->d:Lher;

    .line 106
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 124
    if-eqz p1, :cond_0

    .line 125
    const-string v0, "account_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Livx;->f:Ljava/lang/String;

    .line 126
    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Livx;->h:I

    .line 128
    invoke-static {}, Lhef;->a()[I

    move-result-object v0

    const-string v1, "account_handler_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    aget v0, v0, v1

    iput v0, p0, Livx;->g:I

    .line 129
    const-string v0, "completed_login_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Liwg;

    iput-object v0, p0, Livx;->i:Liwg;

    .line 130
    const-string v0, "queued_login_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Liwg;

    iput-object v0, p0, Livx;->j:Liwg;

    .line 131
    const-string v0, "pending_login_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Liwg;

    iput-object v0, p0, Livx;->k:Liwg;

    .line 132
    const-string v0, "pending_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Livx;->l:I

    .line 133
    const-string v0, "tag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Livx;->e:Ljava/lang/String;

    .line 136
    :cond_0
    iget-object v0, p0, Livx;->b:Lhei;

    invoke-interface {v0, p0}, Lhei;->a(Lhep;)V

    .line 137
    iget-object v0, p0, Livx;->c:Liwc;

    invoke-interface {v0, p0}, Liwc;->a(Liwd;)V

    .line 138
    return-void
.end method

.method public a(Liwg;)V
    .locals 3

    .prologue
    .line 207
    iget-object v0, p1, Liwg;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Livx;->f:Ljava/lang/String;

    iput-object v0, p1, Liwg;->e:Ljava/lang/String;

    .line 210
    :cond_0
    iget-object v0, p1, Liwg;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 211
    iget-object v0, p0, Livx;->a:Lz;

    const-string v1, "LoginAccountHandler.account_key"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Liwg;->e:Ljava/lang/String;

    .line 213
    :cond_1
    iget-boolean v0, p1, Liwg;->h:Z

    if-eqz v0, :cond_2

    .line 214
    iget-object v0, p0, Livx;->a:Lz;

    iget-object v1, p0, Livx;->a:Lz;

    invoke-virtual {v1}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Liwg;->a(Landroid/content/Context;Landroid/content/Intent;)Liwg;

    .line 217
    :cond_2
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Livx;->e:Ljava/lang/String;

    .line 218
    iput-object p1, p0, Livx;->j:Liwg;

    .line 219
    invoke-direct {p0}, Livx;->i()V

    .line 220
    return-void
.end method

.method public a(Liwg;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Livx;->e:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Livx;->e:Ljava/lang/String;

    .line 233
    iput-object p1, p0, Livx;->k:Liwg;

    .line 234
    iput p3, p0, Livx;->l:I

    .line 235
    invoke-direct {p0}, Livx;->k()V

    .line 237
    :cond_0
    return-void
.end method

.method public b(Lheg;)Livx;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Livx;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    return-object p0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Livx;->m:Z

    .line 159
    invoke-direct {p0}, Livx;->j()V

    .line 160
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Livx;->m:Z

    .line 112
    const-string v0, "account_key"

    iget-object v1, p0, Livx;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const-string v0, "account_id"

    iget v1, p0, Livx;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    const-string v0, "account_handler_state"

    iget v1, p0, Livx;->g:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 115
    const-string v0, "completed_login_request"

    iget-object v1, p0, Livx;->i:Liwg;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 116
    const-string v0, "queued_login_request"

    iget-object v1, p0, Livx;->j:Liwg;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 117
    const-string v0, "pending_login_request"

    iget-object v1, p0, Livx;->k:Liwg;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 118
    const-string v0, "pending_id"

    iget v1, p0, Livx;->l:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    const-string v0, "tag"

    iget-object v1, p0, Livx;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Livx;->m:Z

    .line 149
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Livx;->h:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 169
    iget v0, p0, Livx;->h:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Livx;->b:Lhei;

    iget v1, p0, Livx;->h:I

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Livx;->b:Lhei;

    iget v1, p0, Livx;->h:I

    .line 175
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lhej;
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Livx;->b:Lhei;

    iget v1, p0, Livx;->h:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Livx;->j:Liwg;

    if-nez v0, :cond_0

    iget-object v0, p0, Livx;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Livx;->k:Liwg;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, Livx;->m:Z

    if-eqz v0, :cond_0

    .line 248
    invoke-direct {p0}, Livx;->m()V

    .line 250
    :cond_0
    return-void
.end method
