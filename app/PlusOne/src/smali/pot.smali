.class public final Lpot;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpot;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lpos;

.field private c:Lpos;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 69
    const v0, 0x2e57483

    new-instance v1, Lpou;

    invoke-direct {v1}, Lpou;-><init>()V

    .line 74
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpot;->a:Loxr;

    .line 73
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 98
    iget-object v1, p0, Lpot;->b:Lpos;

    if-eqz v1, :cond_0

    .line 99
    const/4 v0, 0x1

    iget-object v1, p0, Lpot;->b:Lpos;

    .line 100
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 102
    :cond_0
    iget-object v1, p0, Lpot;->c:Lpos;

    if-eqz v1, :cond_1

    .line 103
    const/4 v1, 0x2

    iget-object v2, p0, Lpot;->c:Lpos;

    .line 104
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_1
    iget-object v1, p0, Lpot;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    iput v0, p0, Lpot;->ai:I

    .line 108
    return v0
.end method

.method public a(Loxn;)Lpot;
    .locals 2

    .prologue
    .line 116
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 117
    sparse-switch v0, :sswitch_data_0

    .line 121
    iget-object v1, p0, Lpot;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpot;->ah:Ljava/util/List;

    .line 125
    :cond_1
    iget-object v1, p0, Lpot;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    :sswitch_0
    return-object p0

    .line 132
    :sswitch_1
    iget-object v0, p0, Lpot;->b:Lpos;

    if-nez v0, :cond_2

    .line 133
    new-instance v0, Lpos;

    invoke-direct {v0}, Lpos;-><init>()V

    iput-object v0, p0, Lpot;->b:Lpos;

    .line 135
    :cond_2
    iget-object v0, p0, Lpot;->b:Lpos;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 139
    :sswitch_2
    iget-object v0, p0, Lpot;->c:Lpos;

    if-nez v0, :cond_3

    .line 140
    new-instance v0, Lpos;

    invoke-direct {v0}, Lpos;-><init>()V

    iput-object v0, p0, Lpot;->c:Lpos;

    .line 142
    :cond_3
    iget-object v0, p0, Lpot;->c:Lpos;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 117
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lpot;->b:Lpos;

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x1

    iget-object v1, p0, Lpot;->b:Lpos;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 88
    :cond_0
    iget-object v0, p0, Lpot;->c:Lpos;

    if-eqz v0, :cond_1

    .line 89
    const/4 v0, 0x2

    iget-object v1, p0, Lpot;->c:Lpos;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 91
    :cond_1
    iget-object v0, p0, Lpot;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 93
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lpot;->a(Loxn;)Lpot;

    move-result-object v0

    return-object v0
.end method
