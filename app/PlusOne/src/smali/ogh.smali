.class public final Logh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 8630
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8641
    iput v0, p0, Logh;->e:I

    .line 8644
    iput v0, p0, Logh;->f:I

    .line 8630
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 8673
    const/4 v0, 0x0

    .line 8674
    iget-object v1, p0, Logh;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8675
    const/4 v0, 0x1

    iget-object v1, p0, Logh;->a:Ljava/lang/String;

    .line 8676
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8678
    :cond_0
    iget-object v1, p0, Logh;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 8679
    const/4 v1, 0x2

    iget-object v2, p0, Logh;->b:Ljava/lang/String;

    .line 8680
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8682
    :cond_1
    iget-object v1, p0, Logh;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 8683
    const/4 v1, 0x3

    iget-object v2, p0, Logh;->c:Ljava/lang/String;

    .line 8684
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8686
    :cond_2
    iget-object v1, p0, Logh;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 8687
    const/4 v1, 0x4

    iget-object v2, p0, Logh;->d:Ljava/lang/String;

    .line 8688
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8690
    :cond_3
    iget v1, p0, Logh;->e:I

    if-eq v1, v3, :cond_4

    .line 8691
    const/4 v1, 0x5

    iget v2, p0, Logh;->e:I

    .line 8692
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8694
    :cond_4
    iget v1, p0, Logh;->f:I

    if-eq v1, v3, :cond_5

    .line 8695
    const/4 v1, 0x6

    iget v2, p0, Logh;->f:I

    .line 8696
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8698
    :cond_5
    iget-object v1, p0, Logh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8699
    iput v0, p0, Logh;->ai:I

    .line 8700
    return v0
.end method

.method public a(Loxn;)Logh;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8708
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8709
    sparse-switch v0, :sswitch_data_0

    .line 8713
    iget-object v1, p0, Logh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8714
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Logh;->ah:Ljava/util/List;

    .line 8717
    :cond_1
    iget-object v1, p0, Logh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8719
    :sswitch_0
    return-object p0

    .line 8724
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logh;->a:Ljava/lang/String;

    goto :goto_0

    .line 8728
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logh;->b:Ljava/lang/String;

    goto :goto_0

    .line 8732
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logh;->c:Ljava/lang/String;

    goto :goto_0

    .line 8736
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Logh;->d:Ljava/lang/String;

    goto :goto_0

    .line 8740
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 8741
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 8745
    :cond_2
    iput v0, p0, Logh;->e:I

    goto :goto_0

    .line 8747
    :cond_3
    iput v2, p0, Logh;->e:I

    goto :goto_0

    .line 8752
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 8753
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-ne v0, v4, :cond_5

    .line 8756
    :cond_4
    iput v0, p0, Logh;->f:I

    goto :goto_0

    .line 8758
    :cond_5
    iput v2, p0, Logh;->f:I

    goto :goto_0

    .line 8709
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 8649
    iget-object v0, p0, Logh;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 8650
    const/4 v0, 0x1

    iget-object v1, p0, Logh;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8652
    :cond_0
    iget-object v0, p0, Logh;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 8653
    const/4 v0, 0x2

    iget-object v1, p0, Logh;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8655
    :cond_1
    iget-object v0, p0, Logh;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 8656
    const/4 v0, 0x3

    iget-object v1, p0, Logh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8658
    :cond_2
    iget-object v0, p0, Logh;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 8659
    const/4 v0, 0x4

    iget-object v1, p0, Logh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 8661
    :cond_3
    iget v0, p0, Logh;->e:I

    if-eq v0, v2, :cond_4

    .line 8662
    const/4 v0, 0x5

    iget v1, p0, Logh;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8664
    :cond_4
    iget v0, p0, Logh;->f:I

    if-eq v0, v2, :cond_5

    .line 8665
    const/4 v0, 0x6

    iget v1, p0, Logh;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8667
    :cond_5
    iget-object v0, p0, Logh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8669
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8626
    invoke-virtual {p0, p1}, Logh;->a(Loxn;)Logh;

    move-result-object v0

    return-object v0
.end method
