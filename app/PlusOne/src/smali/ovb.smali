.class public final Lovb;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Loqg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 603
    invoke-direct {p0}, Loxq;-><init>()V

    .line 606
    sget-object v0, Loqg;->a:[Loqg;

    iput-object v0, p0, Lovb;->a:[Loqg;

    .line 603
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 624
    .line 625
    iget-object v1, p0, Lovb;->a:[Loqg;

    if-eqz v1, :cond_1

    .line 626
    iget-object v2, p0, Lovb;->a:[Loqg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 627
    if-eqz v4, :cond_0

    .line 628
    const/4 v5, 0x1

    .line 629
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 626
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 633
    :cond_1
    iget-object v1, p0, Lovb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 634
    iput v0, p0, Lovb;->ai:I

    .line 635
    return v0
.end method

.method public a(Loxn;)Lovb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 643
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 644
    sparse-switch v0, :sswitch_data_0

    .line 648
    iget-object v2, p0, Lovb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 649
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lovb;->ah:Ljava/util/List;

    .line 652
    :cond_1
    iget-object v2, p0, Lovb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 654
    :sswitch_0
    return-object p0

    .line 659
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 660
    iget-object v0, p0, Lovb;->a:[Loqg;

    if-nez v0, :cond_3

    move v0, v1

    .line 661
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loqg;

    .line 662
    iget-object v3, p0, Lovb;->a:[Loqg;

    if-eqz v3, :cond_2

    .line 663
    iget-object v3, p0, Lovb;->a:[Loqg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 665
    :cond_2
    iput-object v2, p0, Lovb;->a:[Loqg;

    .line 666
    :goto_2
    iget-object v2, p0, Lovb;->a:[Loqg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 667
    iget-object v2, p0, Lovb;->a:[Loqg;

    new-instance v3, Loqg;

    invoke-direct {v3}, Loqg;-><init>()V

    aput-object v3, v2, v0

    .line 668
    iget-object v2, p0, Lovb;->a:[Loqg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 669
    invoke-virtual {p1}, Loxn;->a()I

    .line 666
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 660
    :cond_3
    iget-object v0, p0, Lovb;->a:[Loqg;

    array-length v0, v0

    goto :goto_1

    .line 672
    :cond_4
    iget-object v2, p0, Lovb;->a:[Loqg;

    new-instance v3, Loqg;

    invoke-direct {v3}, Loqg;-><init>()V

    aput-object v3, v2, v0

    .line 673
    iget-object v2, p0, Lovb;->a:[Loqg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 644
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 611
    iget-object v0, p0, Lovb;->a:[Loqg;

    if-eqz v0, :cond_1

    .line 612
    iget-object v1, p0, Lovb;->a:[Loqg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 613
    if-eqz v3, :cond_0

    .line 614
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 612
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 618
    :cond_1
    iget-object v0, p0, Lovb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 620
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 599
    invoke-virtual {p0, p1}, Lovb;->a(Loxn;)Lovb;

    move-result-object v0

    return-object v0
.end method
