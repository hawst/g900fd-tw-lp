.class public final Lnly;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lnly;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:[Lnma;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 366
    const v0, 0x2971ab0

    new-instance v1, Lnlz;

    invoke-direct {v1}, Lnlz;-><init>()V

    .line 371
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lnly;->a:Loxr;

    .line 370
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 367
    invoke-direct {p0}, Loxq;-><init>()V

    .line 528
    sget-object v0, Lnma;->a:[Lnma;

    iput-object v0, p0, Lnly;->b:[Lnma;

    .line 367
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 546
    .line 547
    iget-object v1, p0, Lnly;->b:[Lnma;

    if-eqz v1, :cond_1

    .line 548
    iget-object v2, p0, Lnly;->b:[Lnma;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 549
    if-eqz v4, :cond_0

    .line 550
    const/4 v5, 0x1

    .line 551
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 548
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 555
    :cond_1
    iget-object v1, p0, Lnly;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 556
    iput v0, p0, Lnly;->ai:I

    .line 557
    return v0
.end method

.method public a(Loxn;)Lnly;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 565
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 566
    sparse-switch v0, :sswitch_data_0

    .line 570
    iget-object v2, p0, Lnly;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 571
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnly;->ah:Ljava/util/List;

    .line 574
    :cond_1
    iget-object v2, p0, Lnly;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 576
    :sswitch_0
    return-object p0

    .line 581
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 582
    iget-object v0, p0, Lnly;->b:[Lnma;

    if-nez v0, :cond_3

    move v0, v1

    .line 583
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnma;

    .line 584
    iget-object v3, p0, Lnly;->b:[Lnma;

    if-eqz v3, :cond_2

    .line 585
    iget-object v3, p0, Lnly;->b:[Lnma;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 587
    :cond_2
    iput-object v2, p0, Lnly;->b:[Lnma;

    .line 588
    :goto_2
    iget-object v2, p0, Lnly;->b:[Lnma;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 589
    iget-object v2, p0, Lnly;->b:[Lnma;

    new-instance v3, Lnma;

    invoke-direct {v3}, Lnma;-><init>()V

    aput-object v3, v2, v0

    .line 590
    iget-object v2, p0, Lnly;->b:[Lnma;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 591
    invoke-virtual {p1}, Loxn;->a()I

    .line 588
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 582
    :cond_3
    iget-object v0, p0, Lnly;->b:[Lnma;

    array-length v0, v0

    goto :goto_1

    .line 594
    :cond_4
    iget-object v2, p0, Lnly;->b:[Lnma;

    new-instance v3, Lnma;

    invoke-direct {v3}, Lnma;-><init>()V

    aput-object v3, v2, v0

    .line 595
    iget-object v2, p0, Lnly;->b:[Lnma;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 566
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 533
    iget-object v0, p0, Lnly;->b:[Lnma;

    if-eqz v0, :cond_1

    .line 534
    iget-object v1, p0, Lnly;->b:[Lnma;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 535
    if-eqz v3, :cond_0

    .line 536
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 534
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 540
    :cond_1
    iget-object v0, p0, Lnly;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 542
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Lnly;->a(Loxn;)Lnly;

    move-result-object v0

    return-object v0
.end method
