.class final Ldcb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldbr;


# instance fields
.field private synthetic a:Ldbs;


# direct methods
.method constructor <init>(Ldbs;)V
    .locals 0

    .prologue
    .line 1287
    iput-object p1, p0, Ldcb;->a:Ldbs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ldbp;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1290
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0, v4}, Ldbs;->a(Ldbs;Ljava/lang/Long;)Ljava/lang/Long;

    .line 1292
    invoke-virtual {p1}, Ldbp;->c()Ljava/lang/String;

    move-result-object v0

    .line 1293
    invoke-virtual {p1}, Ldbp;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1294
    const-string v1, "g:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1295
    :goto_0
    iget-object v1, p0, Ldcb;->a:Ldbs;

    invoke-virtual {v1}, Ldbs;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Ldcb;->a:Ldbs;

    .line 1296
    invoke-static {v2}, Ldbs;->p(Ldbs;)I

    move-result v2

    .line 1295
    invoke-static {v1, v2, v0, v4, v3}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 1297
    iget-object v1, p0, Ldcb;->a:Ldbs;

    invoke-virtual {v1, v0}, Ldbs;->a(Landroid/content/Intent;)V

    .line 1320
    :cond_0
    :goto_1
    return-void

    .line 1294
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1301
    :cond_2
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0}, Ldbs;->q(Ldbs;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1305
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0}, Ldbs;->r(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_2
    if-ltz v4, :cond_4

    .line 1306
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0}, Ldbs;->r(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbp;

    .line 1307
    if-ne v0, p1, :cond_3

    move v1, v2

    :goto_3
    invoke-virtual {v0, v1}, Ldbp;->b(Z)V

    .line 1305
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_2

    :cond_3
    move v1, v3

    .line 1307
    goto :goto_3

    .line 1310
    :cond_4
    invoke-virtual {p1}, Ldbp;->e()I

    move-result v0

    invoke-static {v0}, Lfve;->d(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1311
    invoke-virtual {p1}, Ldbp;->e()I

    move-result v0

    const/16 v1, 0x64

    if-ne v0, v1, :cond_6

    .line 1312
    :cond_5
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0, p1}, Ldbs;->a(Ldbs;Ldbp;)Ldbp;

    .line 1313
    iget-object v0, p0, Ldcb;->a:Ldbs;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Ldbs;->a(Ldbs;I)V

    .line 1316
    :cond_6
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0}, Ldbs;->r(Ldbs;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1317
    iget-object v1, p0, Ldcb;->a:Ldbs;

    invoke-static {v1}, Ldbs;->r(Ldbs;)Ljava/util/List;

    move-result-object v1

    iget-object v4, p0, Ldcb;->a:Ldbs;

    invoke-static {v4}, Ldbs;->r(Ldbs;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1318
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0, v2}, Ldbs;->b(Ldbs;Z)Z

    .line 1319
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0}, Ldbs;->i(Ldbs;)Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/shapes/PhotoShapesLayout;->requestLayout()V

    goto :goto_1
.end method

.method public a(Ldbq;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 1325
    sget-object v0, Ldbq;->a:Ldbq;

    invoke-virtual {v0, p1}, Ldbq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1326
    iget-object v0, p0, Ldcb;->a:Ldbs;

    sget-object v1, Lhmv;->ei:Lhmv;

    invoke-static {v0, v1, p3}, Ldbs;->a(Ldbs;Lhmv;Ljava/lang/String;)V

    .line 1327
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0}, Ldbs;->g(Ldbs;)Ldcg;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, Ldcg;->b(JZ)V

    .line 1343
    :cond_0
    :goto_0
    return-void

    .line 1328
    :cond_1
    sget-object v0, Ldbq;->b:Ldbq;

    invoke-virtual {v0, p1}, Ldbq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1329
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1330
    iget-object v0, p0, Ldcb;->a:Ldbs;

    sget-object v1, Lhmv;->dU:Lhmv;

    invoke-static {v0, v1, p6}, Ldbs;->a(Ldbs;Lhmv;Ljava/lang/String;)V

    .line 1338
    :goto_1
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0}, Ldbs;->g(Ldbs;)Ldcg;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3, p6, p4}, Ldcg;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1332
    :cond_2
    if-eqz p5, :cond_3

    .line 1333
    iget-object v0, p0, Ldcb;->a:Ldbs;

    sget-object v1, Lhmv;->ej:Lhmv;

    invoke-static {v0, v1, p6}, Ldbs;->a(Ldbs;Lhmv;Ljava/lang/String;)V

    goto :goto_1

    .line 1335
    :cond_3
    iget-object v0, p0, Ldcb;->a:Ldbs;

    sget-object v1, Lhmv;->dW:Lhmv;

    invoke-static {v0, v1, p6}, Ldbs;->a(Ldbs;Lhmv;Ljava/lang/String;)V

    goto :goto_1

    .line 1339
    :cond_4
    sget-object v0, Ldbq;->c:Ldbq;

    invoke-virtual {v0, p1}, Ldbq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1340
    iget-object v0, p0, Ldcb;->a:Ldbs;

    sget-object v1, Lhmv;->dV:Lhmv;

    invoke-static {v0, v1, p6}, Ldbs;->a(Ldbs;Lhmv;Ljava/lang/String;)V

    .line 1341
    iget-object v0, p0, Ldcb;->a:Ldbs;

    invoke-static {v0}, Ldbs;->g(Ldbs;)Ldcg;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3, p5}, Ldcg;->a(JZ)V

    goto :goto_0
.end method
