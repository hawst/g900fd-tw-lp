.class public final Lody;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lody;


# instance fields
.field public b:Lodz;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Ljava/lang/String;

.field public g:Lnzi;

.field public h:Loed;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 406
    const/4 v0, 0x0

    new-array v0, v0, [Lody;

    sput-object v0, Lody;->a:[Lody;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 407
    invoke-direct {p0}, Loxq;-><init>()V

    .line 494
    iput-object v0, p0, Lody;->b:Lodz;

    .line 499
    iput v1, p0, Lody;->c:I

    .line 504
    iput v1, p0, Lody;->e:I

    .line 509
    iput-object v0, p0, Lody;->g:Lnzi;

    .line 512
    iput-object v0, p0, Lody;->h:Loed;

    .line 407
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 547
    const/4 v0, 0x0

    .line 548
    iget-object v1, p0, Lody;->b:Lodz;

    if-eqz v1, :cond_0

    .line 549
    const/4 v0, 0x1

    iget-object v1, p0, Lody;->b:Lodz;

    .line 550
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 552
    :cond_0
    iget v1, p0, Lody;->c:I

    if-eq v1, v3, :cond_1

    .line 553
    const/4 v1, 0x2

    iget v2, p0, Lody;->c:I

    .line 554
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 556
    :cond_1
    iget-object v1, p0, Lody;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 557
    const/4 v1, 0x3

    iget-object v2, p0, Lody;->d:Ljava/lang/String;

    .line 558
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 560
    :cond_2
    iget v1, p0, Lody;->e:I

    if-eq v1, v3, :cond_3

    .line 561
    const/4 v1, 0x4

    iget v2, p0, Lody;->e:I

    .line 562
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 564
    :cond_3
    iget-object v1, p0, Lody;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 565
    const/4 v1, 0x5

    iget-object v2, p0, Lody;->f:Ljava/lang/String;

    .line 566
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 568
    :cond_4
    iget-object v1, p0, Lody;->g:Lnzi;

    if-eqz v1, :cond_5

    .line 569
    const/4 v1, 0x6

    iget-object v2, p0, Lody;->g:Lnzi;

    .line 570
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 572
    :cond_5
    iget-object v1, p0, Lody;->h:Loed;

    if-eqz v1, :cond_6

    .line 573
    const/4 v1, 0x7

    iget-object v2, p0, Lody;->h:Loed;

    .line 574
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 576
    :cond_6
    iget-object v1, p0, Lody;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 577
    const/16 v1, 0x8

    iget-object v2, p0, Lody;->i:Ljava/lang/String;

    .line 578
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 580
    :cond_7
    iget-object v1, p0, Lody;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 581
    iput v0, p0, Lody;->ai:I

    .line 582
    return v0
.end method

.method public a(Loxn;)Lody;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 590
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 591
    sparse-switch v0, :sswitch_data_0

    .line 595
    iget-object v1, p0, Lody;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 596
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lody;->ah:Ljava/util/List;

    .line 599
    :cond_1
    iget-object v1, p0, Lody;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 601
    :sswitch_0
    return-object p0

    .line 606
    :sswitch_1
    iget-object v0, p0, Lody;->b:Lodz;

    if-nez v0, :cond_2

    .line 607
    new-instance v0, Lodz;

    invoke-direct {v0}, Lodz;-><init>()V

    iput-object v0, p0, Lody;->b:Lodz;

    .line 609
    :cond_2
    iget-object v0, p0, Lody;->b:Lodz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 613
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 614
    if-eq v0, v2, :cond_3

    if-ne v0, v3, :cond_4

    .line 616
    :cond_3
    iput v0, p0, Lody;->c:I

    goto :goto_0

    .line 618
    :cond_4
    iput v2, p0, Lody;->c:I

    goto :goto_0

    .line 623
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lody;->d:Ljava/lang/String;

    goto :goto_0

    .line 627
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 628
    if-eqz v0, :cond_5

    if-eq v0, v2, :cond_5

    if-ne v0, v3, :cond_6

    .line 631
    :cond_5
    iput v0, p0, Lody;->e:I

    goto :goto_0

    .line 633
    :cond_6
    const/4 v0, 0x0

    iput v0, p0, Lody;->e:I

    goto :goto_0

    .line 638
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lody;->f:Ljava/lang/String;

    goto :goto_0

    .line 642
    :sswitch_6
    iget-object v0, p0, Lody;->g:Lnzi;

    if-nez v0, :cond_7

    .line 643
    new-instance v0, Lnzi;

    invoke-direct {v0}, Lnzi;-><init>()V

    iput-object v0, p0, Lody;->g:Lnzi;

    .line 645
    :cond_7
    iget-object v0, p0, Lody;->g:Lnzi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 649
    :sswitch_7
    iget-object v0, p0, Lody;->h:Loed;

    if-nez v0, :cond_8

    .line 650
    new-instance v0, Loed;

    invoke-direct {v0}, Loed;-><init>()V

    iput-object v0, p0, Lody;->h:Loed;

    .line 652
    :cond_8
    iget-object v0, p0, Lody;->h:Loed;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 656
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lody;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 591
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 517
    iget-object v0, p0, Lody;->b:Lodz;

    if-eqz v0, :cond_0

    .line 518
    const/4 v0, 0x1

    iget-object v1, p0, Lody;->b:Lodz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 520
    :cond_0
    iget v0, p0, Lody;->c:I

    if-eq v0, v2, :cond_1

    .line 521
    const/4 v0, 0x2

    iget v1, p0, Lody;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 523
    :cond_1
    iget-object v0, p0, Lody;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 524
    const/4 v0, 0x3

    iget-object v1, p0, Lody;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 526
    :cond_2
    iget v0, p0, Lody;->e:I

    if-eq v0, v2, :cond_3

    .line 527
    const/4 v0, 0x4

    iget v1, p0, Lody;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 529
    :cond_3
    iget-object v0, p0, Lody;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 530
    const/4 v0, 0x5

    iget-object v1, p0, Lody;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 532
    :cond_4
    iget-object v0, p0, Lody;->g:Lnzi;

    if-eqz v0, :cond_5

    .line 533
    const/4 v0, 0x6

    iget-object v1, p0, Lody;->g:Lnzi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 535
    :cond_5
    iget-object v0, p0, Lody;->h:Loed;

    if-eqz v0, :cond_6

    .line 536
    const/4 v0, 0x7

    iget-object v1, p0, Lody;->h:Loed;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 538
    :cond_6
    iget-object v0, p0, Lody;->i:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 539
    const/16 v0, 0x8

    iget-object v1, p0, Lody;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 541
    :cond_7
    iget-object v0, p0, Lody;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 543
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 403
    invoke-virtual {p0, p1}, Lody;->a(Loxn;)Lody;

    move-result-object v0

    return-object v0
.end method
