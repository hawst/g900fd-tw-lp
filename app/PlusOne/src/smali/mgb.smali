.class public final Lmgb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnrk;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12472
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12475
    iput-object v0, p0, Lmgb;->apiHeader:Llyr;

    .line 12478
    iput-object v0, p0, Lmgb;->a:Lnrk;

    .line 12472
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12495
    const/4 v0, 0x0

    .line 12496
    iget-object v1, p0, Lmgb;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 12497
    const/4 v0, 0x1

    iget-object v1, p0, Lmgb;->apiHeader:Llyr;

    .line 12498
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12500
    :cond_0
    iget-object v1, p0, Lmgb;->a:Lnrk;

    if-eqz v1, :cond_1

    .line 12501
    const/4 v1, 0x2

    iget-object v2, p0, Lmgb;->a:Lnrk;

    .line 12502
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12504
    :cond_1
    iget-object v1, p0, Lmgb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12505
    iput v0, p0, Lmgb;->ai:I

    .line 12506
    return v0
.end method

.method public a(Loxn;)Lmgb;
    .locals 2

    .prologue
    .line 12514
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12515
    sparse-switch v0, :sswitch_data_0

    .line 12519
    iget-object v1, p0, Lmgb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12520
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgb;->ah:Ljava/util/List;

    .line 12523
    :cond_1
    iget-object v1, p0, Lmgb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12525
    :sswitch_0
    return-object p0

    .line 12530
    :sswitch_1
    iget-object v0, p0, Lmgb;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 12531
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgb;->apiHeader:Llyr;

    .line 12533
    :cond_2
    iget-object v0, p0, Lmgb;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12537
    :sswitch_2
    iget-object v0, p0, Lmgb;->a:Lnrk;

    if-nez v0, :cond_3

    .line 12538
    new-instance v0, Lnrk;

    invoke-direct {v0}, Lnrk;-><init>()V

    iput-object v0, p0, Lmgb;->a:Lnrk;

    .line 12540
    :cond_3
    iget-object v0, p0, Lmgb;->a:Lnrk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12515
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12483
    iget-object v0, p0, Lmgb;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 12484
    const/4 v0, 0x1

    iget-object v1, p0, Lmgb;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12486
    :cond_0
    iget-object v0, p0, Lmgb;->a:Lnrk;

    if-eqz v0, :cond_1

    .line 12487
    const/4 v0, 0x2

    iget-object v1, p0, Lmgb;->a:Lnrk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12489
    :cond_1
    iget-object v0, p0, Lmgb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12491
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12468
    invoke-virtual {p0, p1}, Lmgb;->a(Loxn;)Lmgb;

    move-result-object v0

    return-object v0
.end method
