.class public final Lejr;
.super Leku;
.source "PG"


# instance fields
.field private N:Leky;

.field private O:I

.field private ad:Ljava/lang/String;

.field private ae:Ljava/lang/Integer;

.field private af:Ljava/lang/Integer;

.field private final ag:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0}, Leku;-><init>()V

    .line 40
    const/high16 v0, -0x80000000

    iput v0, p0, Lejr;->O:I

    .line 46
    new-instance v0, Lhnw;

    new-instance v1, Lejt;

    invoke-direct {v1, p0}, Lejt;-><init>(Lejr;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 61
    new-instance v0, Lejs;

    invoke-direct {v0, p0}, Lejs;-><init>(Lejr;)V

    iput-object v0, p0, Lejr;->ag:Lbc;

    .line 106
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 112
    invoke-direct {p0}, Leku;-><init>()V

    .line 40
    const/high16 v0, -0x80000000

    iput v0, p0, Lejr;->O:I

    .line 46
    new-instance v0, Lhnw;

    new-instance v1, Lejt;

    invoke-direct {v1, p0}, Lejt;-><init>(Lejr;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 61
    new-instance v0, Lejs;

    invoke-direct {v0, p0}, Lejs;-><init>(Lejr;)V

    iput-object v0, p0, Lejr;->ag:Lbc;

    .line 113
    iput p1, p0, Lejr;->O:I

    .line 114
    return-void
.end method

.method static synthetic a(Lejr;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lejr;->O:I

    return v0
.end method

.method static synthetic a(Lejr;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lejr;->ae:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Lejr;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lejr;->ad:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lejr;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lejr;->af:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic b(Lejr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lejr;->ad:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lejr;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lejr;->ae:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic d(Lejr;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lejr;->af:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lhmw;->r:Lhmw;

    return-object v0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lejr;->O:I

    if-nez v0, :cond_0

    .line 175
    const/16 v0, 0x40

    .line 177
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x41

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 139
    iget-object v1, p0, Lejr;->N:Leky;

    invoke-virtual {p0, v1}, Lejr;->a(Landroid/widget/ListAdapter;)V

    .line 140
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 120
    if-eqz p1, :cond_0

    .line 122
    const-string v0, "type"

    const/high16 v1, -0x80000000

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lejr;->O:I

    .line 125
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejr;->X:Z

    .line 126
    new-instance v0, Leky;

    invoke-direct {v0, p0}, Leky;-><init>(Leku;)V

    iput-object v0, p0, Lejr;->N:Leky;

    .line 127
    return-void
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 151
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 152
    iget-object v0, p0, Lejr;->ad:Ljava/lang/String;

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    iget-object v1, p0, Lejr;->N:Leky;

    iget v0, p0, Lejr;->O:I

    if-nez v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    invoke-virtual {v1, p1, v0}, Leky;->a(Ljava/util/ArrayList;I)V

    .line 145
    iget-object v0, p0, Lejr;->N:Leky;

    invoke-virtual {v0}, Leky;->notifyDataSetChanged()V

    .line 146
    invoke-virtual {p0}, Lejr;->x()Landroid/view/View;

    invoke-virtual {p0}, Lejr;->aa()V

    .line 147
    return-void

    .line 144
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 191
    const/16 v0, 0x67

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejr;->R:Z

    .line 163
    invoke-virtual {p0}, Lejr;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lejr;->ag:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 164
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejr;->R:Z

    .line 169
    invoke-virtual {p0}, Lejr;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lejr;->ag:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 170
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 131
    invoke-super {p0, p1}, Leku;->e(Landroid/os/Bundle;)V

    .line 132
    const-string v0, "type"

    iget v1, p0, Lejr;->O:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    return-void
.end method
