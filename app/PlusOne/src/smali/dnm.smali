.class public final Ldnm;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmjo;",
        "Lmjp;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private p:Lodw;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;Lodw;)V
    .locals 6

    .prologue
    .line 55
    const-string v3, "setvolumecontrols"

    new-instance v4, Lmjo;

    invoke-direct {v4}, Lmjo;-><init>()V

    new-instance v5, Lmjp;

    invoke-direct {v5}, Lmjp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 57
    iput p3, p0, Ldnm;->a:I

    .line 58
    iput-object p4, p0, Ldnm;->b:Ljava/lang/String;

    .line 59
    iput-object p5, p0, Ldnm;->p:Lodw;

    .line 60
    return-void
.end method


# virtual methods
.method protected a(Lmjo;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 67
    new-instance v0, Lodv;

    invoke-direct {v0}, Lodv;-><init>()V

    .line 86
    new-instance v1, Lods;

    invoke-direct {v1}, Lods;-><init>()V

    iput-object v1, v0, Lodv;->b:Lods;

    .line 87
    iget-object v1, v0, Lodv;->b:Lods;

    iget v2, p0, Ldnm;->a:I

    iput v2, v1, Lods;->b:I

    .line 88
    iget v1, p0, Ldnm;->a:I

    if-ne v1, v3, :cond_1

    .line 89
    iget-object v1, v0, Lodv;->b:Lods;

    iget-object v2, p0, Ldnm;->b:Ljava/lang/String;

    invoke-static {v2}, Lhxe;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lods;->c:Ljava/lang/String;

    .line 95
    :cond_0
    :goto_0
    iget-object v1, p0, Ldnm;->p:Lodw;

    iput-object v1, v0, Lodv;->c:Lodw;

    .line 97
    new-array v1, v3, [Lodv;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    .line 100
    new-instance v0, Lnvu;

    invoke-direct {v0}, Lnvu;-><init>()V

    iput-object v0, p1, Lmjo;->a:Lnvu;

    .line 101
    iget-object v0, p1, Lmjo;->a:Lnvu;

    new-instance v2, Lodt;

    invoke-direct {v2}, Lodt;-><init>()V

    iput-object v2, v0, Lnvu;->a:Lodt;

    .line 102
    iget-object v0, p1, Lmjo;->a:Lnvu;

    iget-object v0, v0, Lnvu;->a:Lodt;

    iput-object v1, v0, Lodt;->a:[Lodv;

    .line 103
    return-void

    .line 90
    :cond_1
    iget v1, p0, Ldnm;->a:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 91
    iget-object v1, v0, Lodv;->b:Lods;

    iget-object v2, p0, Ldnm;->b:Ljava/lang/String;

    iput-object v2, v1, Lods;->d:Ljava/lang/String;

    goto :goto_0

    .line 92
    :cond_2
    iget v1, p0, Ldnm;->a:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget v1, p0, Ldnm;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid volume control type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lmjo;

    invoke-virtual {p0, p1}, Ldnm;->a(Lmjo;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 26
    invoke-virtual {p0}, Ldnm;->d()V

    return-void
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 107
    iget v0, p0, Ldnm;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Ldnm;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 110
    :cond_0
    iget-object v0, p0, Ldnm;->f:Landroid/content/Context;

    iget v1, p0, Ldnm;->c:I

    iget-object v2, p0, Ldnm;->b:Ljava/lang/String;

    iget-object v3, p0, Ldnm;->p:Lodw;

    invoke-static {v0, v1, v2, v3}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Lodw;)V

    .line 116
    :cond_1
    :goto_0
    return-void

    .line 111
    :cond_2
    iget v0, p0, Ldnm;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 113
    iget-object v0, p0, Ldnm;->f:Landroid/content/Context;

    const-class v1, Lktq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iget v1, p0, Ldnm;->c:I

    iget-object v2, p0, Ldnm;->b:Ljava/lang/String;

    iget-object v3, p0, Ldnm;->p:Lodw;

    .line 114
    invoke-interface {v0, v1, v2, v3}, Lktq;->a(ILjava/lang/String;Lodw;)V

    goto :goto_0
.end method
