.class public final Lfbo;
.super Ljvc;
.source "PG"

# interfaces
.implements Lfdi;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljvc;",
        "Lfdi",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;


# instance fields
.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "tile_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "view_order"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "media_attr"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "photo_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "content_url"

    aput-object v2, v0, v1

    sput-object v0, Lfbo;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 10

    .prologue
    .line 78
    sget-object v4, Lfbo;->b:[Ljava/lang/String;

    const-string v5, "type == 4"

    if-eqz p6, :cond_0

    move-object v6, p5

    :goto_0
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Ljvc;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    .line 80
    iput-object p4, p0, Lfbo;->f:Ljava/lang/String;

    .line 81
    iput-object p5, p0, Lfbo;->g:Ljava/lang/String;

    .line 83
    if-eqz p6, :cond_1

    iget-object v0, p0, Lfbo;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 84
    :goto_1
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "SELECT count(*) FROM all_tiles WHERE %s AND view_order < ( %s )"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lfbo;->e:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    if-eqz v0, :cond_2

    const-string v0, "SELECT view_order FROM all_tiles WHERE media_attr & 512 != 0 AND view_id = ?  AND tile_id = ?"

    :goto_2
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->h:Ljava/lang/String;

    .line 86
    return-void

    .line 78
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 83
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 84
    :cond_2
    const-string v0, "SELECT view_order FROM all_tiles WHERE media_attr & 512 == 0 AND view_id = ?  AND tile_id = ?"

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 131
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 132
    invoke-static {v0, v1}, Ljvj;->a(J)Ljac;

    move-result-object v2

    .line 135
    const-wide/32 v4, 0x40000

    and-long/2addr v0, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 136
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 138
    invoke-static {p0, v0, v2}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    .line 140
    :cond_0
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 141
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 142
    invoke-static {p0, v0, v1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 90
    invoke-super {p0}, Ljvc;->C()Landroid/database/Cursor;

    move-result-object v1

    .line 91
    if-nez v1, :cond_1

    .line 92
    const/4 v1, 0x0

    .line 121
    :cond_0
    :goto_0
    return-object v1

    .line 95
    :cond_1
    invoke-virtual {p0}, Lfbo;->n()Landroid/content/Context;

    move-result-object v0

    iget v2, p0, Lfbo;->d:I

    invoke-static {v0, v2}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 99
    iget-object v0, p0, Lfbo;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lfbo;->g:Ljava/lang/String;

    if-nez v0, :cond_5

    :cond_2
    iget-object v0, p0, Lfbo;->f:Ljava/lang/String;

    move-object v2, v0

    .line 102
    :goto_1
    const/4 v0, 0x0

    .line 103
    if-eqz v2, :cond_6

    .line 104
    invoke-virtual {p0}, Lfbo;->l()[Ljava/lang/String;

    move-result-object v4

    .line 106
    array-length v0, v4

    add-int/lit8 v0, v0, 0x2

    .line 107
    invoke-static {v4, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 108
    array-length v5, v4

    iget-object v6, p0, Lfbo;->c:Ljava/lang/String;

    aput-object v6, v0, v5

    .line 109
    array-length v4, v4

    add-int/lit8 v4, v4, 0x1

    aput-object v2, v0, v4

    .line 110
    iget-object v2, p0, Lfbo;->h:Ljava/lang/String;

    invoke-static {v3, v2, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    long-to-int v0, v2

    move v2, v0

    .line 112
    :goto_2
    instance-of v0, v1, Lhxy;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 113
    check-cast v0, Lhxy;

    .line 114
    invoke-interface {v0}, Lhxy;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 115
    sget-object v4, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    if-eq v3, v4, :cond_3

    if-nez v3, :cond_4

    .line 116
    :cond_3
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 118
    :cond_4
    const-string v4, "start_position"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    invoke-interface {v0, v3}, Lhxy;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 99
    :cond_5
    iget-object v0, p0, Lfbo;->g:Ljava/lang/String;

    .line 100
    invoke-virtual {p0, v3, v0}, Lfbo;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_6
    move v2, v0

    goto :goto_2
.end method

.method public a(Landroid/database/Cursor;I)V
    .locals 1

    .prologue
    .line 149
    invoke-interface {p1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 150
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->f:Ljava/lang/String;

    .line 151
    return-void
.end method

.method public a(Lizu;)V
    .locals 2

    .prologue
    .line 155
    invoke-virtual {p1}, Lizu;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    invoke-virtual {p1}, Lizu;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->f:Ljava/lang/String;

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    invoke-virtual {p1}, Lizu;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p1}, Lizu;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfbo;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lfbo;->a(Landroid/database/Cursor;I)V

    return-void
.end method
