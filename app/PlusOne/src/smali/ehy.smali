.class final Lehy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field private synthetic a:Lehu;


# direct methods
.method constructor <init>(Lehu;)V
    .locals 0

    .prologue
    .line 1342
    iput-object p1, p0, Lehy;->a:Lehu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1366
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1350
    iget-object v0, p0, Lehy;->a:Lehu;

    invoke-static {v0}, Lehu;->aj(Lehu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1352
    iget-object v0, p0, Lehy;->a:Lehu;

    invoke-static {v0}, Lehu;->d(Lehu;)Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->c()Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x96

    .line 1353
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1357
    :cond_0
    iget-object v0, p0, Lehy;->a:Lehu;

    invoke-static {v0}, Lehu;->d(Lehu;)Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1358
    iget-object v0, p0, Lehy;->a:Lehu;

    invoke-static {v0}, Lehu;->d(Lehu;)Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1360
    iget-object v0, p0, Lehy;->a:Lehu;

    invoke-static {v0}, Lehu;->d(Lehu;)Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    .line 1361
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1363
    iget-object v0, p0, Lehy;->a:Lehu;

    invoke-static {v0}, Lehu;->ak(Lehu;)Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/RecyclingViewGroup;->b(Z)V

    .line 1365
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1367
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1346
    iget-object v0, p0, Lehy;->a:Lehu;

    invoke-static {v0}, Lehu;->d(Lehu;)Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpViewGroup;->d()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1347
    return-void
.end method
