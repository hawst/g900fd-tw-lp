.class public Lbwn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwe;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Laic;


# instance fields
.field private final c:Lbwp;

.field private final d:Lbvs;

.field private final e:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

.field private final f:Lbvy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbvy",
            "<",
            "Laii;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lanh;

.field private final h:Lbwm;

.field private final i:Landroid/os/PowerManager;

.field private final j:Laim;

.field private final k:Livc;

.field private final l:Ljava/lang/Runnable;

.field private m:Landroid/net/Uri;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const-class v0, Lbwn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbwn;->a:Ljava/lang/String;

    .line 53
    new-instance v0, Laic;

    sget-object v1, Laib;->a:Laib;

    sget-object v2, Lahx;->b:[Lahz;

    invoke-direct {v0, v1, v2}, Laic;-><init>(Laib;[Lahz;)V

    sput-object v0, Lbwn;->b:Laic;

    return-void
.end method

.method public constructor <init>(Lbwp;Lbvs;Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;Lbvy;Lajl;Lbks;Lbit;Ljfb;Lanh;Lbwm;Landroid/os/PowerManager;Livc;Ljava/util/concurrent/Executor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbwp;",
            "Lbvs;",
            "Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;",
            "Lbvy",
            "<",
            "Laii;",
            ">;",
            "Lajl;",
            "Lbks;",
            "Lbit;",
            "Ljfb;",
            "Lanh;",
            "Lbwm;",
            "Landroid/os/PowerManager;",
            "Livc;",
            "Ljava/util/concurrent/Executor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v2, Lbwo;

    invoke-direct {v2, p0}, Lbwo;-><init>(Lbwn;)V

    iput-object v2, p0, Lbwn;->l:Ljava/lang/Runnable;

    .line 109
    const-string v2, "service"

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbwp;

    iput-object v2, p0, Lbwn;->c:Lbwp;

    .line 110
    const-string v2, "backgroundServiceHelper"

    .line 111
    const/4 v3, 0x0

    invoke-static {p2, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbvs;

    iput-object v2, p0, Lbwn;->d:Lbvs;

    .line 112
    const-string v2, "applicationEnabler"

    .line 113
    const/4 v3, 0x0

    invoke-static {p3, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    iput-object v2, p0, Lbwn;->e:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    .line 114
    const-string v2, "analyzerConnection"

    const/4 v3, 0x0

    invoke-static {p4, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbvy;

    iput-object v2, p0, Lbwn;->f:Lbvy;

    .line 115
    const-string v2, "gservicesSettings"

    const/4 v3, 0x0

    move-object/from16 v0, p9

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lanh;

    iput-object v2, p0, Lbwn;->g:Lanh;

    .line 116
    const-string v2, "settings"

    const/4 v3, 0x0

    move-object/from16 v0, p10

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbwm;

    iput-object v2, p0, Lbwn;->h:Lbwm;

    .line 117
    const-string v2, "powerManager"

    const/4 v3, 0x0

    move-object/from16 v0, p11

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lbwn;->i:Landroid/os/PowerManager;

    .line 119
    new-instance v2, Lajd;

    new-instance v5, Lajx;

    iget-object v3, p0, Lbwn;->f:Lbvy;

    invoke-direct {v5, v3}, Lajx;-><init>(Lbvy;)V

    move-object v3, p6

    move-object v4, p5

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v2 .. v7}, Lajd;-><init>(Lbks;Lajl;Lajv;Lbit;Ljfb;)V

    .line 125
    new-instance v3, Laim;

    move-object/from16 v0, p13

    move-object/from16 v1, p13

    invoke-direct {v3, v0, v1, v2}, Laim;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lahx;)V

    iput-object v3, p0, Lbwn;->j:Laim;

    .line 128
    const/4 v2, 0x0

    sget-object v3, Lbwn;->a:Ljava/lang/String;

    move-object/from16 v0, p12

    invoke-static {v2, v3, v0}, Livb;->a(ZLjava/lang/String;Livc;)Livc;

    move-result-object v2

    iput-object v2, p0, Lbwn;->k:Livc;

    .line 129
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;I)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 65
    invoke-static {p0, p1}, Lcom/google/android/apps/moviemaker/service/PostCaptureAnalyzerService;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "retry_count"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 66
    const/4 v1, 0x0

    const/high16 v2, 0x48000000    # 131072.0f

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbwn;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lbwn;->m:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b(Lbwn;)Lbvs;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lbwn;->d:Lbvs;

    return-object v0
.end method

.method static synthetic c(Lbwn;)Livc;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lbwn;->k:Livc;

    return-object v0
.end method

.method static synthetic d(Lbwn;)Laim;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lbwn;->j:Laim;

    return-object v0
.end method

.method static synthetic e(Lbwn;)Lbwm;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lbwn;->h:Lbwm;

    return-object v0
.end method

.method static synthetic f(Lbwn;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lbwn;->n:I

    return v0
.end method

.method static synthetic g(Lbwn;)Lbwp;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lbwn;->c:Lbwp;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lbwn;->f:Lbvy;

    invoke-virtual {v0}, Lbvy;->a()V

    .line 134
    iget-object v0, p0, Lbwn;->d:Lbvs;

    iget-object v1, p0, Lbwn;->l:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lbvs;->a(Ljava/lang/Runnable;)V

    .line 136
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 146
    iget-object v0, p0, Lbwn;->d:Lbvs;

    invoke-interface {v0}, Lbvs;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 149
    iget-object v0, p0, Lbwn;->e:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->a()V

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lbwn;->k:Livc;

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lbwn;->k:Livc;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "processIntent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 157
    :cond_2
    iget-object v0, p0, Lbwn;->g:Lanh;

    invoke-virtual {v0}, Lanh;->Y()Z

    move-result v0

    if-nez v0, :cond_3

    .line 158
    iget-object v0, p0, Lbwn;->k:Livc;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lbwn;->k:Livc;

    const-string v1, "post capture analysis disabled: abort now"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 164
    :cond_3
    iget-object v0, p0, Lbwn;->d:Lbvs;

    invoke-interface {v0}, Lbvs;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 165
    iget-object v0, p0, Lbwn;->k:Livc;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lbwn;->k:Livc;

    const-string v1, "post capture analysis aborted: battery too low"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 171
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lbwn;->m:Landroid/net/Uri;

    .line 172
    const-string v0, "retry_count"

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbwn;->n:I

    .line 177
    iget-object v0, p0, Lbwn;->d:Lbvs;

    invoke-interface {v0}, Lbvs;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 178
    iget v0, p0, Lbwn;->n:I

    iget-object v1, p0, Lbwn;->h:Lbwm;

    invoke-interface {v1}, Lbwm;->a()I

    move-result v1

    if-gt v0, v1, :cond_6

    .line 179
    iget-object v0, p0, Lbwn;->k:Livc;

    if-eqz v0, :cond_5

    .line 180
    iget-object v0, p0, Lbwn;->k:Livc;

    const-string v1, "post capture analysis rescheduled: screen is currently on"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 182
    :cond_5
    iget-object v0, p0, Lbwn;->c:Lbwp;

    iget-object v1, p0, Lbwn;->h:Lbwm;

    invoke-interface {v1}, Lbwm;->b()J

    move-result-wide v2

    iget-object v1, p0, Lbwn;->m:Landroid/net/Uri;

    iget v4, p0, Lbwn;->n:I

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v0, v2, v3, v1, v4}, Lbwp;->a(JLandroid/net/Uri;I)V

    goto/16 :goto_0

    .line 184
    :cond_6
    iget-object v0, p0, Lbwn;->k:Livc;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lbwn;->k:Livc;

    const-string v1, "post capture analysis aborted: too many retries"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 191
    :cond_7
    iget-object v0, p0, Lbwn;->k:Livc;

    if-eqz v0, :cond_8

    .line 192
    iget-object v0, p0, Lbwn;->k:Livc;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "start processing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 199
    :cond_8
    iget-object v0, p0, Lbwn;->i:Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "PostCaptureAnalyzerService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v7

    .line 201
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 203
    :try_start_0
    iget-object v1, p0, Lbwn;->j:Laim;

    iget-object v2, p0, Lbwn;->m:Landroid/net/Uri;

    sget-object v3, Lbwn;->b:Laic;

    iget-object v0, p0, Lbwn;->h:Lbwm;

    .line 207
    invoke-interface {v0}, Lbwm;->c()J

    move-result-wide v4

    iget-object v0, p0, Lbwn;->h:Lbwm;

    .line 208
    invoke-interface {v0}, Lbwm;->d()Z

    move-result v6

    .line 203
    invoke-static/range {v1 .. v6}, Lbww;->a(Laim;Landroid/net/Uri;Laic;JZ)Lbwz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 211
    iput-object v9, p0, Lbwn;->m:Landroid/net/Uri;

    .line 212
    iput v8, p0, Lbwn;->n:I

    .line 215
    iget-object v0, p0, Lbwn;->k:Livc;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lbwn;->k:Livc;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "stop processing: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 210
    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 211
    iput-object v9, p0, Lbwn;->m:Landroid/net/Uri;

    .line 212
    iput v8, p0, Lbwn;->n:I

    throw v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lbwn;->f:Lbvy;

    invoke-virtual {v0}, Lbvy;->b()V

    .line 141
    iget-object v0, p0, Lbwn;->d:Lbvs;

    invoke-interface {v0}, Lbvs;->d()V

    .line 142
    return-void
.end method
