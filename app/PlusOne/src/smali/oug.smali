.class public final Loug;
.super Loxq;
.source "PG"


# instance fields
.field private a:Louj;

.field private b:[Louh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 164
    invoke-direct {p0}, Loxq;-><init>()V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Loug;->a:Louj;

    .line 170
    sget-object v0, Louh;->a:[Louh;

    iput-object v0, p0, Loug;->b:[Louh;

    .line 164
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 191
    .line 192
    iget-object v0, p0, Loug;->a:Louj;

    if-eqz v0, :cond_2

    .line 193
    const/4 v0, 0x1

    iget-object v2, p0, Loug;->a:Louj;

    .line 194
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 196
    :goto_0
    iget-object v2, p0, Loug;->b:[Louh;

    if-eqz v2, :cond_1

    .line 197
    iget-object v2, p0, Loug;->b:[Louh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 198
    if-eqz v4, :cond_0

    .line 199
    const/4 v5, 0x2

    .line 200
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 197
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 204
    :cond_1
    iget-object v1, p0, Loug;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    iput v0, p0, Loug;->ai:I

    .line 206
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loug;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 214
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 215
    sparse-switch v0, :sswitch_data_0

    .line 219
    iget-object v2, p0, Loug;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 220
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loug;->ah:Ljava/util/List;

    .line 223
    :cond_1
    iget-object v2, p0, Loug;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    :sswitch_0
    return-object p0

    .line 230
    :sswitch_1
    iget-object v0, p0, Loug;->a:Louj;

    if-nez v0, :cond_2

    .line 231
    new-instance v0, Louj;

    invoke-direct {v0}, Louj;-><init>()V

    iput-object v0, p0, Loug;->a:Louj;

    .line 233
    :cond_2
    iget-object v0, p0, Loug;->a:Louj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 237
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 238
    iget-object v0, p0, Loug;->b:[Louh;

    if-nez v0, :cond_4

    move v0, v1

    .line 239
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Louh;

    .line 240
    iget-object v3, p0, Loug;->b:[Louh;

    if-eqz v3, :cond_3

    .line 241
    iget-object v3, p0, Loug;->b:[Louh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 243
    :cond_3
    iput-object v2, p0, Loug;->b:[Louh;

    .line 244
    :goto_2
    iget-object v2, p0, Loug;->b:[Louh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 245
    iget-object v2, p0, Loug;->b:[Louh;

    new-instance v3, Louh;

    invoke-direct {v3}, Louh;-><init>()V

    aput-object v3, v2, v0

    .line 246
    iget-object v2, p0, Loug;->b:[Louh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 247
    invoke-virtual {p1}, Loxn;->a()I

    .line 244
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 238
    :cond_4
    iget-object v0, p0, Loug;->b:[Louh;

    array-length v0, v0

    goto :goto_1

    .line 250
    :cond_5
    iget-object v2, p0, Loug;->b:[Louh;

    new-instance v3, Louh;

    invoke-direct {v3}, Louh;-><init>()V

    aput-object v3, v2, v0

    .line 251
    iget-object v2, p0, Loug;->b:[Louh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 215
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 175
    iget-object v0, p0, Loug;->a:Louj;

    if-eqz v0, :cond_0

    .line 176
    const/4 v0, 0x1

    iget-object v1, p0, Loug;->a:Louj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 178
    :cond_0
    iget-object v0, p0, Loug;->b:[Louh;

    if-eqz v0, :cond_2

    .line 179
    iget-object v1, p0, Loug;->b:[Louh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 180
    if-eqz v3, :cond_1

    .line 181
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 179
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_2
    iget-object v0, p0, Loug;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 187
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0, p1}, Loug;->a(Loxn;)Loug;

    move-result-object v0

    return-object v0
.end method
