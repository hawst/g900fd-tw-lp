.class public final Ldqd;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lkfd;

.field private final b:Landroid/content/Context;

.field private final c:I

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:I

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 30
    const-string v0, "SetVolumeControlTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 31
    iput-object p1, p0, Ldqd;->b:Landroid/content/Context;

    .line 32
    iput p2, p0, Ldqd;->c:I

    .line 33
    iput p3, p0, Ldqd;->d:I

    .line 34
    iput-object p4, p0, Ldqd;->e:Ljava/lang/String;

    .line 35
    iput p5, p0, Ldqd;->f:I

    .line 36
    iput-boolean p6, p0, Ldqd;->h:Z

    .line 37
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Ldqd;->a:Lkfd;

    .line 38
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    .line 42
    new-instance v5, Lodw;

    invoke-direct {v5}, Lodw;-><init>()V

    .line 43
    iget v0, p0, Ldqd;->f:I

    iput v0, v5, Lodw;->a:I

    .line 44
    iget-boolean v0, p0, Ldqd;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, v5, Lodw;->b:I

    .line 47
    new-instance v0, Ldnm;

    iget-object v1, p0, Ldqd;->b:Landroid/content/Context;

    iget v2, p0, Ldqd;->c:I

    iget v3, p0, Ldqd;->d:I

    iget-object v4, p0, Ldqd;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ldnm;-><init>(Landroid/content/Context;IILjava/lang/String;Lodw;)V

    .line 49
    iget-object v1, p0, Ldqd;->a:Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 51
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v1

    .line 44
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
