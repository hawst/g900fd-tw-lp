.class public final Lmus;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmus;


# instance fields
.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3232
    const/4 v0, 0x0

    new-array v0, v0, [Lmus;

    sput-object v0, Lmus;->a:[Lmus;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3233
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3254
    const/4 v0, 0x0

    .line 3255
    iget-object v1, p0, Lmus;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 3256
    const/4 v0, 0x1

    iget-object v1, p0, Lmus;->b:Ljava/lang/Long;

    .line 3257
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 3259
    :cond_0
    iget-object v1, p0, Lmus;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3260
    const/4 v1, 0x2

    iget-object v2, p0, Lmus;->c:Ljava/lang/String;

    .line 3261
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3263
    :cond_1
    iget-object v1, p0, Lmus;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3264
    iput v0, p0, Lmus;->ai:I

    .line 3265
    return v0
.end method

.method public a(Loxn;)Lmus;
    .locals 2

    .prologue
    .line 3273
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3274
    sparse-switch v0, :sswitch_data_0

    .line 3278
    iget-object v1, p0, Lmus;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3279
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmus;->ah:Ljava/util/List;

    .line 3282
    :cond_1
    iget-object v1, p0, Lmus;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3284
    :sswitch_0
    return-object p0

    .line 3289
    :sswitch_1
    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmus;->b:Ljava/lang/Long;

    goto :goto_0

    .line 3293
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmus;->c:Ljava/lang/String;

    goto :goto_0

    .line 3274
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 3242
    iget-object v0, p0, Lmus;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 3243
    const/4 v0, 0x1

    iget-object v1, p0, Lmus;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->c(IJ)V

    .line 3245
    :cond_0
    iget-object v0, p0, Lmus;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3246
    const/4 v0, 0x2

    iget-object v1, p0, Lmus;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3248
    :cond_1
    iget-object v0, p0, Lmus;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3250
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3229
    invoke-virtual {p0, p1}, Lmus;->a(Loxn;)Lmus;

    move-result-object v0

    return-object v0
.end method
