.class final Lkyn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Ligy;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:Landroid/app/Activity;

.field private d:Ligv;

.field private e:Livx;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lkyn;->a:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lkyn;->b:Ljava/lang/String;

    .line 69
    return-void
.end method


# virtual methods
.method a(I)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 103
    iget-object v0, p0, Lkyn;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 104
    iget-object v0, p0, Lkyn;->c:Landroid/app/Activity;

    const-class v2, Lkte;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkte;

    iget-object v2, p0, Lkyn;->a:Ljava/lang/String;

    iget-object v3, p0, Lkyn;->b:Ljava/lang/String;

    .line 105
    invoke-interface {v0, p1, v2, v3}, Lkte;->a(ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 106
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_0

    .line 108
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 110
    :cond_0
    return-object v0
.end method

.method public a()V
    .locals 6

    .prologue
    .line 81
    iget-object v0, p0, Lkyn;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lkyn;->e:Livx;

    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 83
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Liwg;->a(I)Liwg;

    move-result-object v0

    const-class v2, Liwl;

    new-instance v3, Liwm;

    invoke-direct {v3}, Liwm;-><init>()V

    iget-object v4, p0, Lkyn;->c:Landroid/app/Activity;

    const v5, 0x7f0a04e6

    .line 85
    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v3

    .line 86
    invoke-virtual {v3}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 84
    invoke-virtual {v0, v2, v3}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v0

    .line 82
    invoke-virtual {v1, v0}, Livx;->a(Liwg;)V

    .line 89
    return-void
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 1

    .prologue
    .line 74
    iput-object p1, p0, Lkyn;->c:Landroid/app/Activity;

    .line 75
    iput-object p3, p0, Lkyn;->d:Ligv;

    .line 76
    invoke-virtual {p4, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lkyn;->e:Livx;

    .line 77
    return-void
.end method

.method public a(ZIIII)V
    .locals 2

    .prologue
    .line 95
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    .line 96
    iget-object v0, p0, Lkyn;->d:Ligv;

    invoke-virtual {p0, p5}, Lkyn;->a(I)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Ligv;->a(Landroid/content/Intent;)V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lkyn;->d:Ligv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ligv;->a(I)V

    goto :goto_0
.end method
