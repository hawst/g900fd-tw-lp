.class public final Lprh;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lprg;

.field private b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    sget-object v0, Lprg;->a:[Lprg;

    iput-object v0, p0, Lprh;->a:[Lprg;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 40
    .line 41
    iget-object v1, p0, Lprh;->a:[Lprg;

    if-eqz v1, :cond_1

    .line 42
    iget-object v2, p0, Lprh;->a:[Lprg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 43
    if-eqz v4, :cond_0

    .line 44
    const/4 v5, 0x1

    .line 45
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 42
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 49
    :cond_1
    iget-object v1, p0, Lprh;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 50
    const/4 v1, 0x2

    iget-object v2, p0, Lprh;->b:Ljava/lang/Boolean;

    .line 51
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 53
    :cond_2
    iget-object v1, p0, Lprh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    iput v0, p0, Lprh;->ai:I

    .line 55
    return v0
.end method

.method public a(Loxn;)Lprh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 64
    sparse-switch v0, :sswitch_data_0

    .line 68
    iget-object v2, p0, Lprh;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 69
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lprh;->ah:Ljava/util/List;

    .line 72
    :cond_1
    iget-object v2, p0, Lprh;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    :sswitch_0
    return-object p0

    .line 79
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 80
    iget-object v0, p0, Lprh;->a:[Lprg;

    if-nez v0, :cond_3

    move v0, v1

    .line 81
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lprg;

    .line 82
    iget-object v3, p0, Lprh;->a:[Lprg;

    if-eqz v3, :cond_2

    .line 83
    iget-object v3, p0, Lprh;->a:[Lprg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 85
    :cond_2
    iput-object v2, p0, Lprh;->a:[Lprg;

    .line 86
    :goto_2
    iget-object v2, p0, Lprh;->a:[Lprg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 87
    iget-object v2, p0, Lprh;->a:[Lprg;

    new-instance v3, Lprg;

    invoke-direct {v3}, Lprg;-><init>()V

    aput-object v3, v2, v0

    .line 88
    iget-object v2, p0, Lprh;->a:[Lprg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 89
    invoke-virtual {p1}, Loxn;->a()I

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 80
    :cond_3
    iget-object v0, p0, Lprh;->a:[Lprg;

    array-length v0, v0

    goto :goto_1

    .line 92
    :cond_4
    iget-object v2, p0, Lprh;->a:[Lprg;

    new-instance v3, Lprg;

    invoke-direct {v3}, Lprg;-><init>()V

    aput-object v3, v2, v0

    .line 93
    iget-object v2, p0, Lprh;->a:[Lprg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 97
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lprh;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 64
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 24
    iget-object v0, p0, Lprh;->a:[Lprg;

    if-eqz v0, :cond_1

    .line 25
    iget-object v1, p0, Lprh;->a:[Lprg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 26
    if-eqz v3, :cond_0

    .line 27
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 25
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 31
    :cond_1
    iget-object v0, p0, Lprh;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Lprh;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 34
    :cond_2
    iget-object v0, p0, Lprh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 36
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lprh;->a(Loxn;)Lprh;

    move-result-object v0

    return-object v0
.end method
