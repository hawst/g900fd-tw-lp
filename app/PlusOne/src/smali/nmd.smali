.class public final Lnmd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lnmd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:[Lnrp;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 189
    const v0, 0x28ccfef

    new-instance v1, Lnme;

    invoke-direct {v1}, Lnme;-><init>()V

    .line 194
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lnmd;->a:Loxr;

    .line 193
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 190
    invoke-direct {p0}, Loxq;-><init>()V

    .line 197
    sget-object v0, Lnrp;->a:[Lnrp;

    iput-object v0, p0, Lnmd;->b:[Lnrp;

    .line 190
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 215
    .line 216
    iget-object v1, p0, Lnmd;->b:[Lnrp;

    if-eqz v1, :cond_1

    .line 217
    iget-object v2, p0, Lnmd;->b:[Lnrp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 218
    if-eqz v4, :cond_0

    .line 219
    const/4 v5, 0x1

    .line 220
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 217
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 224
    :cond_1
    iget-object v1, p0, Lnmd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    iput v0, p0, Lnmd;->ai:I

    .line 226
    return v0
.end method

.method public a(Loxn;)Lnmd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 234
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 235
    sparse-switch v0, :sswitch_data_0

    .line 239
    iget-object v2, p0, Lnmd;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 240
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnmd;->ah:Ljava/util/List;

    .line 243
    :cond_1
    iget-object v2, p0, Lnmd;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    :sswitch_0
    return-object p0

    .line 250
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 251
    iget-object v0, p0, Lnmd;->b:[Lnrp;

    if-nez v0, :cond_3

    move v0, v1

    .line 252
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnrp;

    .line 253
    iget-object v3, p0, Lnmd;->b:[Lnrp;

    if-eqz v3, :cond_2

    .line 254
    iget-object v3, p0, Lnmd;->b:[Lnrp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 256
    :cond_2
    iput-object v2, p0, Lnmd;->b:[Lnrp;

    .line 257
    :goto_2
    iget-object v2, p0, Lnmd;->b:[Lnrp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 258
    iget-object v2, p0, Lnmd;->b:[Lnrp;

    new-instance v3, Lnrp;

    invoke-direct {v3}, Lnrp;-><init>()V

    aput-object v3, v2, v0

    .line 259
    iget-object v2, p0, Lnmd;->b:[Lnrp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 260
    invoke-virtual {p1}, Loxn;->a()I

    .line 257
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 251
    :cond_3
    iget-object v0, p0, Lnmd;->b:[Lnrp;

    array-length v0, v0

    goto :goto_1

    .line 263
    :cond_4
    iget-object v2, p0, Lnmd;->b:[Lnrp;

    new-instance v3, Lnrp;

    invoke-direct {v3}, Lnrp;-><init>()V

    aput-object v3, v2, v0

    .line 264
    iget-object v2, p0, Lnmd;->b:[Lnrp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 235
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 202
    iget-object v0, p0, Lnmd;->b:[Lnrp;

    if-eqz v0, :cond_1

    .line 203
    iget-object v1, p0, Lnmd;->b:[Lnrp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 204
    if-eqz v3, :cond_0

    .line 205
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 203
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_1
    iget-object v0, p0, Lnmd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 211
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0, p1}, Lnmd;->a(Loxn;)Lnmd;

    move-result-object v0

    return-object v0
.end method
