.class public final Lnin;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnin;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lnih;

.field public e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2042
    const/4 v0, 0x0

    new-array v0, v0, [Lnin;

    sput-object v0, Lnin;->a:[Lnin;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2043
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2050
    const/4 v0, 0x0

    iput-object v0, p0, Lnin;->d:Lnih;

    .line 2043
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2080
    const/4 v0, 0x0

    .line 2081
    iget-object v1, p0, Lnin;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2082
    const/4 v0, 0x1

    iget-object v1, p0, Lnin;->b:Ljava/lang/String;

    .line 2083
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2085
    :cond_0
    iget-object v1, p0, Lnin;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2086
    const/4 v1, 0x2

    iget-object v2, p0, Lnin;->c:Ljava/lang/String;

    .line 2087
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2089
    :cond_1
    iget-object v1, p0, Lnin;->d:Lnih;

    if-eqz v1, :cond_2

    .line 2090
    const/4 v1, 0x3

    iget-object v2, p0, Lnin;->d:Lnih;

    .line 2091
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2093
    :cond_2
    iget-object v1, p0, Lnin;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2094
    const/4 v1, 0x4

    iget-object v2, p0, Lnin;->f:Ljava/lang/String;

    .line 2095
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2097
    :cond_3
    iget-object v1, p0, Lnin;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2098
    const/4 v1, 0x5

    iget-object v2, p0, Lnin;->e:Ljava/lang/String;

    .line 2099
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2101
    :cond_4
    iget-object v1, p0, Lnin;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2102
    iput v0, p0, Lnin;->ai:I

    .line 2103
    return v0
.end method

.method public a(Loxn;)Lnin;
    .locals 2

    .prologue
    .line 2111
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2112
    sparse-switch v0, :sswitch_data_0

    .line 2116
    iget-object v1, p0, Lnin;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2117
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnin;->ah:Ljava/util/List;

    .line 2120
    :cond_1
    iget-object v1, p0, Lnin;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2122
    :sswitch_0
    return-object p0

    .line 2127
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnin;->b:Ljava/lang/String;

    goto :goto_0

    .line 2131
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnin;->c:Ljava/lang/String;

    goto :goto_0

    .line 2135
    :sswitch_3
    iget-object v0, p0, Lnin;->d:Lnih;

    if-nez v0, :cond_2

    .line 2136
    new-instance v0, Lnih;

    invoke-direct {v0}, Lnih;-><init>()V

    iput-object v0, p0, Lnin;->d:Lnih;

    .line 2138
    :cond_2
    iget-object v0, p0, Lnin;->d:Lnih;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2142
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnin;->f:Ljava/lang/String;

    goto :goto_0

    .line 2146
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnin;->e:Ljava/lang/String;

    goto :goto_0

    .line 2112
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2059
    iget-object v0, p0, Lnin;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2060
    const/4 v0, 0x1

    iget-object v1, p0, Lnin;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2062
    :cond_0
    iget-object v0, p0, Lnin;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2063
    const/4 v0, 0x2

    iget-object v1, p0, Lnin;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2065
    :cond_1
    iget-object v0, p0, Lnin;->d:Lnih;

    if-eqz v0, :cond_2

    .line 2066
    const/4 v0, 0x3

    iget-object v1, p0, Lnin;->d:Lnih;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2068
    :cond_2
    iget-object v0, p0, Lnin;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2069
    const/4 v0, 0x4

    iget-object v1, p0, Lnin;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2071
    :cond_3
    iget-object v0, p0, Lnin;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2072
    const/4 v0, 0x5

    iget-object v1, p0, Lnin;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2074
    :cond_4
    iget-object v0, p0, Lnin;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2076
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2039
    invoke-virtual {p0, p1}, Lnin;->a(Loxn;)Lnin;

    move-result-object v0

    return-object v0
.end method
