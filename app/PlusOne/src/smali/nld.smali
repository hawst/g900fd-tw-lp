.class public final Lnld;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnkw;

.field public b:Lnks;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 538
    invoke-direct {p0}, Loxq;-><init>()V

    .line 541
    iput-object v0, p0, Lnld;->a:Lnkw;

    .line 544
    iput-object v0, p0, Lnld;->b:Lnks;

    .line 538
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 566
    const/4 v0, 0x0

    .line 567
    iget-object v1, p0, Lnld;->a:Lnkw;

    if-eqz v1, :cond_0

    .line 568
    const/4 v0, 0x1

    iget-object v1, p0, Lnld;->a:Lnkw;

    .line 569
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 571
    :cond_0
    iget-object v1, p0, Lnld;->b:Lnks;

    if-eqz v1, :cond_1

    .line 572
    const/4 v1, 0x2

    iget-object v2, p0, Lnld;->b:Lnks;

    .line 573
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_1
    iget-object v1, p0, Lnld;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 576
    const/4 v1, 0x3

    iget-object v2, p0, Lnld;->c:Ljava/lang/String;

    .line 577
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    :cond_2
    iget-object v1, p0, Lnld;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 580
    iput v0, p0, Lnld;->ai:I

    .line 581
    return v0
.end method

.method public a(Loxn;)Lnld;
    .locals 2

    .prologue
    .line 589
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 590
    sparse-switch v0, :sswitch_data_0

    .line 594
    iget-object v1, p0, Lnld;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 595
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnld;->ah:Ljava/util/List;

    .line 598
    :cond_1
    iget-object v1, p0, Lnld;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 600
    :sswitch_0
    return-object p0

    .line 605
    :sswitch_1
    iget-object v0, p0, Lnld;->a:Lnkw;

    if-nez v0, :cond_2

    .line 606
    new-instance v0, Lnkw;

    invoke-direct {v0}, Lnkw;-><init>()V

    iput-object v0, p0, Lnld;->a:Lnkw;

    .line 608
    :cond_2
    iget-object v0, p0, Lnld;->a:Lnkw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 612
    :sswitch_2
    iget-object v0, p0, Lnld;->b:Lnks;

    if-nez v0, :cond_3

    .line 613
    new-instance v0, Lnks;

    invoke-direct {v0}, Lnks;-><init>()V

    iput-object v0, p0, Lnld;->b:Lnks;

    .line 615
    :cond_3
    iget-object v0, p0, Lnld;->b:Lnks;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 619
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnld;->c:Ljava/lang/String;

    goto :goto_0

    .line 590
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 551
    iget-object v0, p0, Lnld;->a:Lnkw;

    if-eqz v0, :cond_0

    .line 552
    const/4 v0, 0x1

    iget-object v1, p0, Lnld;->a:Lnkw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 554
    :cond_0
    iget-object v0, p0, Lnld;->b:Lnks;

    if-eqz v0, :cond_1

    .line 555
    const/4 v0, 0x2

    iget-object v1, p0, Lnld;->b:Lnks;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 557
    :cond_1
    iget-object v0, p0, Lnld;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 558
    const/4 v0, 0x3

    iget-object v1, p0, Lnld;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 560
    :cond_2
    iget-object v0, p0, Lnld;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 562
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 534
    invoke-virtual {p0, p1}, Lnld;->a(Loxn;)Lnld;

    move-result-object v0

    return-object v0
.end method
