.class public final Ldzu;
.super Lhya;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 171
    iput-object p1, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 172
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lhya;-><init>(Landroid/content/Context;B)V

    .line 173
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x5

    return v0
.end method

.method protected a(II)I
    .locals 0

    .prologue
    .line 182
    return p1
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 223
    iget-object v0, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;I)I

    move-result v0

    .line 224
    packed-switch v0, :pswitch_data_0

    .line 249
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 228
    :pswitch_0
    invoke-static {p1}, Ljpg;->a(Landroid/content/Context;)Ljpg;

    move-result-object v0

    .line 229
    iget-object v1, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, v1}, Ljpg;->a(Llio;)V

    .line 230
    invoke-virtual {v0, v2}, Ljpg;->g(Z)V

    .line 231
    iget-object v1, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lhxh;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljpg;->a(Lhxh;)V

    goto :goto_0

    .line 236
    :pswitch_1
    new-instance v0, Lhxf;

    invoke-direct {v0, p1}, Lhxf;-><init>(Landroid/content/Context;)V

    .line 237
    iget-object v1, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, v1}, Lhxf;->a(Llio;)V

    .line 238
    invoke-virtual {v0, v2}, Lhxf;->g(Z)V

    goto :goto_0

    .line 243
    :pswitch_2
    invoke-static {p1}, Ljpg;->a(Landroid/content/Context;)Ljpg;

    move-result-object v0

    .line 244
    iget-object v1, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, v1}, Ljpg;->a(Llio;)V

    .line 245
    invoke-virtual {v0, v2}, Ljpg;->g(Z)V

    goto :goto_0

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 188
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401d0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;

    .line 190
    iget-object v1, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v1, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;I)I

    move-result v1

    .line 191
    packed-switch v1, :pswitch_data_0

    .line 217
    :goto_0
    return-object v0

    .line 193
    :pswitch_0
    const v1, 0x7f0a086b

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->a(I)V

    goto :goto_0

    .line 198
    :pswitch_1
    const v1, 0x7f0a0867

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->a(I)V

    goto :goto_0

    .line 203
    :pswitch_2
    const v1, 0x7f0a0868

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->a(I)V

    goto :goto_0

    .line 208
    :pswitch_3
    const v1, 0x7f0a0869

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->a(I)V

    goto :goto_0

    .line 213
    :pswitch_4
    const v1, 0x7f0a086a

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/SectionHeaderView;->a(I)V

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 255
    iget-object v0, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;I)I

    move-result v0

    .line 256
    packed-switch v0, :pswitch_data_0

    .line 328
    :goto_0
    return-void

    .line 258
    :pswitch_0
    check-cast p1, Ljpg;

    .line 259
    iget-object v0, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lhxh;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljpg;->a(Lhxh;)V

    .line 260
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 261
    invoke-virtual {p1, v2}, Ljpg;->a(Ljava/lang/String;)V

    .line 262
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 263
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 264
    invoke-static {v4}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Ljpg;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljpg;->c(Ljava/lang/String;)V

    .line 266
    const/4 v0, 0x5

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    :goto_1
    iget-object v4, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 267
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->b(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/lang/String;

    move-result-object v4

    .line 266
    invoke-virtual {p1, v0, v4}, Ljpg;->a(ZLjava/lang/String;)V

    .line 268
    const/4 v0, 0x7

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v3, :cond_3

    move v0, v3

    :goto_2
    invoke-virtual {p1, v0}, Ljpg;->a(Z)V

    .line 269
    iget-object v0, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->c(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 270
    invoke-virtual {p1, v0}, Ljpg;->setChecked(Z)V

    .line 271
    if-eqz v0, :cond_0

    iget-object v0, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->d(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v1, v3

    :cond_1
    invoke-virtual {p1, v1}, Ljpg;->setEnabled(Z)V

    .line 272
    invoke-virtual {p1}, Ljpg;->k()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 266
    goto :goto_1

    :cond_3
    move v0, v1

    .line 268
    goto :goto_2

    .line 279
    :pswitch_1
    check-cast p1, Ljpg;

    .line 280
    iget-object v0, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->a(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lhxh;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljpg;->a(Lhxh;)V

    .line 281
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 282
    invoke-virtual {p1, v2}, Ljpg;->a(Ljava/lang/String;)V

    .line 283
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 284
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 285
    invoke-static {v4}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v0, v4}, Ljpg;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljpg;->c(Ljava/lang/String;)V

    .line 287
    const/4 v0, 0x5

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v3

    :goto_3
    iget-object v4, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 288
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->b(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/lang/String;

    move-result-object v4

    .line 287
    invoke-virtual {p1, v0, v4}, Ljpg;->a(ZLjava/lang/String;)V

    .line 289
    const/4 v0, 0x7

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v3, :cond_6

    move v0, v3

    :goto_4
    invoke-virtual {p1, v0}, Ljpg;->a(Z)V

    .line 290
    const/4 v0, 0x6

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljpg;->g(Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->c(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 292
    invoke-virtual {p1, v0}, Ljpg;->setChecked(Z)V

    .line 293
    if-eqz v0, :cond_4

    iget-object v0, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->d(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_4
    :goto_5
    invoke-virtual {p1, v3}, Ljpg;->setEnabled(Z)V

    .line 294
    invoke-virtual {p1, v1}, Ljpg;->e(Z)V

    .line 295
    invoke-virtual {p1}, Ljpg;->k()V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 287
    goto :goto_3

    :cond_6
    move v0, v1

    .line 289
    goto :goto_4

    :cond_7
    move v3, v1

    .line 293
    goto :goto_5

    :pswitch_2
    move-object v0, p1

    .line 301
    check-cast v0, Lhxf;

    .line 302
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 303
    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 306
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 307
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-object v5, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    .line 308
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->e(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Llnl;

    move-result-object v5

    iget-object v6, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v6}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->f(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)I

    move-result v6

    invoke-static {v5, v6, v2}, Lhxm;->a(Landroid/content/Context;II)Z

    move-result v5

    .line 304
    invoke-virtual/range {v0 .. v5}, Lhxf;->a(Ljava/lang/String;ILjava/lang/String;IZ)V

    .line 309
    iget-object v2, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->g(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhxf;->setChecked(Z)V

    goto/16 :goto_0

    .line 315
    :pswitch_3
    check-cast p1, Ljpg;

    .line 316
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljpg;->c(Ljava/lang/String;)V

    .line 317
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 318
    invoke-virtual {p1, v0}, Ljpg;->f(Ljava/lang/String;)V

    .line 319
    const-string v2, "e:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 320
    :goto_6
    invoke-virtual {p1, v0}, Ljpg;->a(Ljava/lang/String;)V

    .line 321
    iget-object v2, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->c(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 322
    invoke-virtual {p1, v0}, Ljpg;->setChecked(Z)V

    .line 323
    invoke-virtual {p1}, Ljpg;->k()V

    .line 324
    if-eqz v0, :cond_8

    iget-object v0, p0, Ldzu;->a:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->d(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Z

    move-result v0

    if-nez v0, :cond_a

    :cond_8
    :goto_7
    invoke-virtual {p1, v3}, Ljpg;->setEnabled(Z)V

    .line 325
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Ljpg;->a(ZLjava/lang/String;)V

    .line 326
    invoke-virtual {p1, v1}, Ljpg;->a(Z)V

    goto/16 :goto_0

    .line 319
    :cond_9
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    :cond_a
    move v3, v1

    .line 324
    goto :goto_7

    .line 256
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method
