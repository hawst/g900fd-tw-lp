.class final Lcva;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcuz;


# instance fields
.field private final a:[Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic d:Lcuy;


# direct methods
.method public constructor <init>(Lcuy;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 285
    iput-object p1, p0, Lcva;->d:Lcuy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    iput-object p2, p0, Lcva;->a:[Ljava/lang/String;

    .line 287
    iput-object p3, p0, Lcva;->b:Ljava/lang/String;

    .line 288
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcva;->c:Ljava/util/Set;

    .line 289
    return-void
.end method


# virtual methods
.method public a(II)Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 293
    iget-object v0, p0, Lcva;->d:Lcuy;

    invoke-static {v0}, Lcuy;->b(Lcuy;)Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcva;->d:Lcuy;

    invoke-static {v1}, Lcuy;->a(Lcuy;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcva;->a:[Ljava/lang/String;

    iget-object v4, p0, Lcva;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2b

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " DESC  LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OFFSET "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343
    iget-object v0, p0, Lcva;->c:Ljava/util/Set;

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 299
    move v0, v1

    .line 300
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcva;->d:Lcuy;

    invoke-static {v2}, Lcuy;->c(Lcuy;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 301
    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 302
    invoke-static {v2}, Lhsf;->a(Ljava/lang/String;)Z

    move-result v3

    .line 303
    iget-object v4, p0, Lcva;->d:Lcuy;

    invoke-static {v4}, Lcuy;->d(Lcuy;)Landroid/content/ContentValues;

    move-result-object v4

    iget-object v5, p0, Lcva;->d:Lcuy;

    .line 304
    invoke-static {v5}, Lcuy;->a(Lcuy;)Landroid/net/Uri;

    move-result-object v5

    .line 303
    invoke-static {p1, v4, v5}, Lcuy;->a(Landroid/database/Cursor;Landroid/content/ContentValues;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 307
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 308
    if-eqz v3, :cond_1

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-gez v3, :cond_2

    .line 309
    :cond_1
    const-string v3, "MediaUriSync"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 310
    const-string v3, "non media mime type or invalid id; media: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x22

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 324
    :cond_2
    const/4 v2, 0x0

    .line 325
    iget-object v3, p0, Lcva;->d:Lcuy;

    invoke-static {v3}, Lcuy;->e(Lcuy;)Lhrx;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 326
    iget-object v2, p0, Lcva;->d:Lcuy;

    invoke-static {v2}, Lcuy;->e(Lcuy;)Lhrx;

    move-result-object v2

    invoke-virtual {v2, v4}, Lhrx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 328
    :cond_3
    iget-object v3, p0, Lcva;->d:Lcuy;

    invoke-static {v3}, Lcuy;->f(Lcuy;)Landroid/content/Context;

    move-result-object v3

    iget-object v5, p0, Lcva;->d:Lcuy;

    invoke-static {v5}, Lcuy;->g(Lcuy;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    iget-object v6, p0, Lcva;->d:Lcuy;

    .line 329
    invoke-static {v6}, Lcuy;->d(Lcuy;)Landroid/content/ContentValues;

    move-result-object v6

    .line 328
    invoke-static {v3, v5, v6, v4, v2}, Ljvd;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 330
    or-int/2addr v0, v2

    .line 332
    iget-object v2, p0, Lcva;->c:Ljava/util/Set;

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 334
    iget-object v2, p0, Lcva;->c:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    rem-int/lit8 v2, v2, 0xa

    if-nez v2, :cond_0

    .line 335
    iget-object v2, p0, Lcva;->d:Lcuy;

    invoke-static {v2}, Lcuy;->g(Lcuy;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely()Z

    .line 336
    iget-object v2, p0, Lcva;->d:Lcuy;

    invoke-static {v2}, Lcuy;->i(Lcuy;)Lcus;

    move-result-object v2

    iget-object v3, p0, Lcva;->d:Lcuy;

    invoke-static {v3}, Lcuy;->h(Lcuy;)I

    move-result v3

    invoke-interface {v2, v3, v0}, Lcus;->a(IZ)V

    move v0, v1

    .line 337
    goto/16 :goto_0

    .line 340
    :cond_4
    return-void
.end method
