.class public final enum Lfyh;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lfyh;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lfyh;

.field public static final enum b:Lfyh;

.field private static final synthetic c:[Lfyh;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    new-instance v0, Lfyh;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, Lfyh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfyh;->a:Lfyh;

    .line 50
    new-instance v0, Lfyh;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, Lfyh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfyh;->b:Lfyh;

    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [Lfyh;

    sget-object v1, Lfyh;->a:Lfyh;

    aput-object v1, v0, v2

    sget-object v1, Lfyh;->b:Lfyh;

    aput-object v1, v0, v3

    sput-object v0, Lfyh;->c:[Lfyh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfyh;
    .locals 1

    .prologue
    .line 48
    const-class v0, Lfyh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfyh;

    return-object v0
.end method

.method public static values()[Lfyh;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lfyh;->c:[Lfyh;

    invoke-virtual {v0}, [Lfyh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfyh;

    return-object v0
.end method
