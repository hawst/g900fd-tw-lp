.class public final Lmye;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Integer;

.field public c:[I

.field public d:Ljava/lang/String;

.field public e:[I

.field public f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Long;

.field private h:[Ljava/lang/String;

.field private i:[B

.field private j:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Loxq;-><init>()V

    .line 190
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lmye;->h:[Ljava/lang/String;

    .line 193
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmye;->c:[I

    .line 200
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmye;->e:[I

    .line 181
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 251
    .line 252
    iget-object v0, p0, Lmye;->g:Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 253
    const/4 v0, 0x1

    iget-object v2, p0, Lmye;->g:Ljava/lang/Long;

    .line 254
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 256
    :goto_0
    iget-object v2, p0, Lmye;->a:Ljava/lang/Long;

    if-eqz v2, :cond_0

    .line 257
    const/4 v2, 0x2

    iget-object v3, p0, Lmye;->a:Ljava/lang/Long;

    .line 258
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 260
    :cond_0
    iget-object v2, p0, Lmye;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 261
    const/4 v2, 0x3

    iget-object v3, p0, Lmye;->b:Ljava/lang/Integer;

    .line 262
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 264
    :cond_1
    iget-object v2, p0, Lmye;->h:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmye;->h:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 266
    iget-object v4, p0, Lmye;->h:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 268
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 266
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 270
    :cond_2
    add-int/2addr v0, v3

    .line 271
    iget-object v2, p0, Lmye;->h:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 273
    :cond_3
    iget-object v2, p0, Lmye;->c:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Lmye;->c:[I

    array-length v2, v2

    if-lez v2, :cond_5

    .line 275
    iget-object v4, p0, Lmye;->c:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_4

    aget v6, v4, v2

    .line 277
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 275
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 279
    :cond_4
    add-int/2addr v0, v3

    .line 280
    iget-object v2, p0, Lmye;->c:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 282
    :cond_5
    iget-object v2, p0, Lmye;->i:[B

    if-eqz v2, :cond_6

    .line 283
    const/4 v2, 0x6

    iget-object v3, p0, Lmye;->i:[B

    .line 284
    invoke-static {v2, v3}, Loxo;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 286
    :cond_6
    iget-object v2, p0, Lmye;->d:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 287
    const/4 v2, 0x7

    iget-object v3, p0, Lmye;->d:Ljava/lang/String;

    .line 288
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 290
    :cond_7
    iget-object v2, p0, Lmye;->e:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Lmye;->e:[I

    array-length v2, v2

    if-lez v2, :cond_9

    .line 292
    iget-object v3, p0, Lmye;->e:[I

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_8

    aget v5, v3, v1

    .line 294
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 292
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 296
    :cond_8
    add-int/2addr v0, v2

    .line 297
    iget-object v1, p0, Lmye;->e:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 299
    :cond_9
    iget-object v1, p0, Lmye;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 300
    const/16 v1, 0x9

    iget-object v2, p0, Lmye;->f:Ljava/lang/Boolean;

    .line 301
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 303
    :cond_a
    iget-object v1, p0, Lmye;->j:Ljava/lang/Long;

    if-eqz v1, :cond_b

    .line 304
    const/16 v1, 0xa

    iget-object v2, p0, Lmye;->j:Ljava/lang/Long;

    .line 305
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 307
    :cond_b
    iget-object v1, p0, Lmye;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    iput v0, p0, Lmye;->ai:I

    .line 309
    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmye;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 317
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 318
    sparse-switch v0, :sswitch_data_0

    .line 322
    iget-object v1, p0, Lmye;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 323
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmye;->ah:Ljava/util/List;

    .line 326
    :cond_1
    iget-object v1, p0, Lmye;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    :sswitch_0
    return-object p0

    .line 333
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmye;->g:Ljava/lang/Long;

    goto :goto_0

    .line 337
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmye;->a:Ljava/lang/Long;

    goto :goto_0

    .line 341
    :sswitch_3
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmye;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 345
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 346
    iget-object v0, p0, Lmye;->h:[Ljava/lang/String;

    array-length v0, v0

    .line 347
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 348
    iget-object v2, p0, Lmye;->h:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 349
    iput-object v1, p0, Lmye;->h:[Ljava/lang/String;

    .line 350
    :goto_1
    iget-object v1, p0, Lmye;->h:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 351
    iget-object v1, p0, Lmye;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 352
    invoke-virtual {p1}, Loxn;->a()I

    .line 350
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 355
    :cond_2
    iget-object v1, p0, Lmye;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 359
    :sswitch_5
    const/16 v0, 0x28

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 360
    iget-object v0, p0, Lmye;->c:[I

    array-length v0, v0

    .line 361
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 362
    iget-object v2, p0, Lmye;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 363
    iput-object v1, p0, Lmye;->c:[I

    .line 364
    :goto_2
    iget-object v1, p0, Lmye;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 365
    iget-object v1, p0, Lmye;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 366
    invoke-virtual {p1}, Loxn;->a()I

    .line 364
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 369
    :cond_3
    iget-object v1, p0, Lmye;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 373
    :sswitch_6
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lmye;->i:[B

    goto/16 :goto_0

    .line 377
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmye;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 381
    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 382
    iget-object v0, p0, Lmye;->e:[I

    array-length v0, v0

    .line 383
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 384
    iget-object v2, p0, Lmye;->e:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 385
    iput-object v1, p0, Lmye;->e:[I

    .line 386
    :goto_3
    iget-object v1, p0, Lmye;->e:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 387
    iget-object v1, p0, Lmye;->e:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 388
    invoke-virtual {p1}, Loxn;->a()I

    .line 386
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 391
    :cond_4
    iget-object v1, p0, Lmye;->e:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 395
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmye;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 399
    :sswitch_a
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmye;->j:Ljava/lang/Long;

    goto/16 :goto_0

    .line 318
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 209
    iget-object v1, p0, Lmye;->g:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 210
    const/4 v1, 0x1

    iget-object v2, p0, Lmye;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(IJ)V

    .line 212
    :cond_0
    iget-object v1, p0, Lmye;->a:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 213
    const/4 v1, 0x2

    iget-object v2, p0, Lmye;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Loxo;->a(IJ)V

    .line 215
    :cond_1
    iget-object v1, p0, Lmye;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 216
    const/4 v1, 0x3

    iget-object v2, p0, Lmye;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->c(II)V

    .line 218
    :cond_2
    iget-object v1, p0, Lmye;->h:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 219
    iget-object v2, p0, Lmye;->h:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 220
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 219
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 223
    :cond_3
    iget-object v1, p0, Lmye;->c:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmye;->c:[I

    array-length v1, v1

    if-lez v1, :cond_4

    .line 224
    iget-object v2, p0, Lmye;->c:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget v4, v2, v1

    .line 225
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 224
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 228
    :cond_4
    iget-object v1, p0, Lmye;->i:[B

    if-eqz v1, :cond_5

    .line 229
    const/4 v1, 0x6

    iget-object v2, p0, Lmye;->i:[B

    invoke-virtual {p1, v1, v2}, Loxo;->a(I[B)V

    .line 231
    :cond_5
    iget-object v1, p0, Lmye;->d:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 232
    const/4 v1, 0x7

    iget-object v2, p0, Lmye;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 234
    :cond_6
    iget-object v1, p0, Lmye;->e:[I

    if-eqz v1, :cond_7

    iget-object v1, p0, Lmye;->e:[I

    array-length v1, v1

    if-lez v1, :cond_7

    .line 235
    iget-object v1, p0, Lmye;->e:[I

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_7

    aget v3, v1, v0

    .line 236
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 239
    :cond_7
    iget-object v0, p0, Lmye;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 240
    const/16 v0, 0x9

    iget-object v1, p0, Lmye;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 242
    :cond_8
    iget-object v0, p0, Lmye;->j:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 243
    const/16 v0, 0xa

    iget-object v1, p0, Lmye;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 245
    :cond_9
    iget-object v0, p0, Lmye;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 247
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0, p1}, Lmye;->a(Loxn;)Lmye;

    move-result-object v0

    return-object v0
.end method
