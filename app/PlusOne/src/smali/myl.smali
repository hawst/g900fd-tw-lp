.class public final Lmyl;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmym;

.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 412
    invoke-direct {p0}, Loxq;-><init>()V

    .line 531
    sget-object v0, Lmym;->a:[Lmym;

    iput-object v0, p0, Lmyl;->a:[Lmym;

    .line 534
    const/high16 v0, -0x80000000

    iput v0, p0, Lmyl;->b:I

    .line 412
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 560
    .line 561
    iget-object v1, p0, Lmyl;->a:[Lmym;

    if-eqz v1, :cond_1

    .line 562
    iget-object v2, p0, Lmyl;->a:[Lmym;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 563
    if-eqz v4, :cond_0

    .line 564
    const/4 v5, 0x1

    .line 565
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 562
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 569
    :cond_1
    iget v1, p0, Lmyl;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 570
    const/4 v1, 0x2

    iget v2, p0, Lmyl;->b:I

    .line 571
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 573
    :cond_2
    iget-object v1, p0, Lmyl;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 574
    const/4 v1, 0x3

    iget-object v2, p0, Lmyl;->c:Ljava/lang/String;

    .line 575
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 577
    :cond_3
    iget-object v1, p0, Lmyl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 578
    iput v0, p0, Lmyl;->ai:I

    .line 579
    return v0
.end method

.method public a(Loxn;)Lmyl;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 587
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 588
    sparse-switch v0, :sswitch_data_0

    .line 592
    iget-object v2, p0, Lmyl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 593
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmyl;->ah:Ljava/util/List;

    .line 596
    :cond_1
    iget-object v2, p0, Lmyl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 598
    :sswitch_0
    return-object p0

    .line 603
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 604
    iget-object v0, p0, Lmyl;->a:[Lmym;

    if-nez v0, :cond_3

    move v0, v1

    .line 605
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmym;

    .line 606
    iget-object v3, p0, Lmyl;->a:[Lmym;

    if-eqz v3, :cond_2

    .line 607
    iget-object v3, p0, Lmyl;->a:[Lmym;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 609
    :cond_2
    iput-object v2, p0, Lmyl;->a:[Lmym;

    .line 610
    :goto_2
    iget-object v2, p0, Lmyl;->a:[Lmym;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 611
    iget-object v2, p0, Lmyl;->a:[Lmym;

    new-instance v3, Lmym;

    invoke-direct {v3}, Lmym;-><init>()V

    aput-object v3, v2, v0

    .line 612
    iget-object v2, p0, Lmyl;->a:[Lmym;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 613
    invoke-virtual {p1}, Loxn;->a()I

    .line 610
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 604
    :cond_3
    iget-object v0, p0, Lmyl;->a:[Lmym;

    array-length v0, v0

    goto :goto_1

    .line 616
    :cond_4
    iget-object v2, p0, Lmyl;->a:[Lmym;

    new-instance v3, Lmym;

    invoke-direct {v3}, Lmym;-><init>()V

    aput-object v3, v2, v0

    .line 617
    iget-object v2, p0, Lmyl;->a:[Lmym;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 621
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 622
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-ne v0, v2, :cond_6

    .line 628
    :cond_5
    iput v0, p0, Lmyl;->b:I

    goto :goto_0

    .line 630
    :cond_6
    iput v1, p0, Lmyl;->b:I

    goto :goto_0

    .line 635
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmyl;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 588
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 541
    iget-object v0, p0, Lmyl;->a:[Lmym;

    if-eqz v0, :cond_1

    .line 542
    iget-object v1, p0, Lmyl;->a:[Lmym;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 543
    if-eqz v3, :cond_0

    .line 544
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 542
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 548
    :cond_1
    iget v0, p0, Lmyl;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 549
    const/4 v0, 0x2

    iget v1, p0, Lmyl;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 551
    :cond_2
    iget-object v0, p0, Lmyl;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 552
    const/4 v0, 0x3

    iget-object v1, p0, Lmyl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 554
    :cond_3
    iget-object v0, p0, Lmyl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 556
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 408
    invoke-virtual {p0, p1}, Lmyl;->a(Loxn;)Lmyl;

    move-result-object v0

    return-object v0
.end method
