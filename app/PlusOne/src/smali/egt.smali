.class final Legt;
.super Laq;
.source "PG"


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic b:Legr;


# direct methods
.method public constructor <init>(Legr;Lae;Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lae;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 334
    iput-object p1, p0, Legt;->b:Legr;

    .line 335
    invoke-direct {p0, p2}, Laq;-><init>(Lae;)V

    .line 336
    iput-object p3, p0, Legt;->a:Ljava/util/HashMap;

    .line 337
    return-void
.end method


# virtual methods
.method public a(I)Lu;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 341
    const/4 v2, 0x0

    .line 343
    iget-object v0, p0, Legt;->b:Legr;

    invoke-virtual {v0}, Legr;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/os/Bundle;

    iget-object v1, p0, Legt;->b:Legr;

    .line 344
    invoke-virtual {v1}, Legr;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    move-object v1, v0

    .line 345
    :goto_0
    const-string v0, "account_id"

    iget-object v3, p0, Legt;->b:Legr;

    iget-object v3, v3, Legr;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 346
    const-string v0, "ActionBarFragmentMixin.Enabled"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 347
    const-string v0, "external"

    iget-object v3, p0, Legt;->b:Legr;

    iget-boolean v3, v3, Legr;->ab:Z

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 348
    const-string v0, "is_for_get_content"

    iget-object v3, p0, Legt;->b:Legr;

    iget-boolean v3, v3, Legr;->ac:Z

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 349
    const-string v0, "force_return_edit_list"

    iget-object v3, p0, Legt;->b:Legr;

    iget-boolean v3, v3, Legr;->ad:Z

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 350
    const-string v0, "filter"

    iget-object v3, p0, Legt;->b:Legr;

    iget v3, v3, Legr;->aa:I

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 351
    const-string v0, "photo_picker_mode"

    iget-object v3, p0, Legt;->b:Legr;

    iget-object v3, v3, Legr;->Y:Lctq;

    invoke-virtual {v3}, Lctq;->c()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 352
    const-string v0, "external"

    iget-object v3, p0, Legt;->b:Legr;

    iget-boolean v3, v3, Legr;->ab:Z

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 353
    const-string v0, "get_content_action"

    iget-object v3, p0, Legt;->b:Legr;

    invoke-virtual {v3}, Legr;->af()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v0, "show_promos"

    iget-object v3, p0, Legt;->b:Legr;

    .line 355
    invoke-virtual {v3}, Legr;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "show_promos"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 354
    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 356
    const-string v0, "show_autobackup_status"

    iget-object v3, p0, Legt;->b:Legr;

    .line 357
    invoke-virtual {v3}, Legr;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "show_autobackup_status"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 356
    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 358
    const-string v0, "finish_on_back"

    iget-object v3, p0, Legt;->b:Legr;

    .line 359
    invoke-virtual {v3}, Legr;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "finish_on_back"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 358
    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 361
    iget-object v0, p0, Legt;->b:Legr;

    invoke-static {v0, p1}, Legr;->a(Legr;I)I

    move-result v3

    .line 363
    iget-object v0, p0, Legt;->a:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Legt;->a:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Legt;->a:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 367
    :cond_0
    sparse-switch v3, :sswitch_data_0

    move-object v0, v2

    .line 414
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 415
    invoke-virtual {v0, v1}, Lu;->f(Landroid/os/Bundle;)V

    .line 418
    :cond_2
    return-object v0

    .line 344
    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v1, v0

    goto/16 :goto_0

    .line 369
    :sswitch_0
    new-instance v0, Lecf;

    invoke-direct {v0}, Lecf;-><init>()V

    .line 370
    const-string v2, "mode"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 371
    const-string v2, "scroll_to_uri"

    iget-object v3, p0, Legt;->b:Legr;

    invoke-static {v3}, Legr;->a(Legr;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 375
    :sswitch_1
    new-instance v0, Lecf;

    invoke-direct {v0}, Lecf;-><init>()V

    .line 376
    const-string v2, "trim_remote"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 377
    iget-object v2, p0, Legt;->b:Legr;

    iget-object v2, v2, Legr;->P:Lhee;

    invoke-interface {v2}, Lhee;->f()Z

    move-result v2

    if-nez v2, :cond_1

    .line 378
    const-string v2, "mode"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 383
    :sswitch_2
    new-instance v0, Lecb;

    invoke-direct {v0}, Lecb;-><init>()V

    goto :goto_1

    .line 387
    :sswitch_3
    new-instance v0, Legc;

    invoke-direct {v0}, Legc;-><init>()V

    .line 388
    const-string v2, "query"

    const-string v3, "#videos"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    const-string v2, "hide_search_view"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 390
    const-string v2, "search_local_videos"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 394
    :sswitch_4
    iget-object v0, p0, Legt;->b:Legr;

    iget-object v0, v0, Legr;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 395
    sget-object v0, Lebv;->N:Lloz;

    .line 396
    new-instance v0, Lebv;

    invoke-direct {v0}, Lebv;-><init>()V

    .line 400
    const-string v2, "scroll_to_uri"

    iget-object v3, p0, Legt;->b:Legr;

    invoke-static {v3}, Legr;->a(Legr;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 402
    :cond_4
    new-instance v0, Lecf;

    invoke-direct {v0}, Lecf;-><init>()V

    .line 403
    const-string v2, "mode"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 404
    const-string v2, "scroll_to_uri"

    iget-object v3, p0, Legt;->b:Legr;

    invoke-static {v3}, Legr;->a(Legr;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto/16 :goto_1

    .line 409
    :sswitch_5
    new-instance v0, Lecm;

    invoke-direct {v0}, Lecm;-><init>()V

    goto/16 :goto_1

    .line 367
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_3
        0x10 -> :sswitch_2
        0x40 -> :sswitch_5
    .end sparse-switch
.end method

.method public b()I
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Legt;->b:Legr;

    invoke-static {v0}, Legr;->b(Legr;)I

    move-result v0

    return v0
.end method

.method public c(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 428
    const v0, 0x7f0a0a77

    .line 429
    iget-object v1, p0, Legt;->b:Legr;

    invoke-static {v1, p1}, Legr;->a(Legr;I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 455
    :goto_0
    iget-object v1, p0, Legt;->b:Legr;

    invoke-virtual {v1}, Legr;->o()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 431
    :sswitch_0
    const v0, 0x7f0a0a7e

    .line 432
    goto :goto_0

    .line 435
    :sswitch_1
    const v0, 0x7f0a0a7f

    .line 436
    goto :goto_0

    .line 439
    :sswitch_2
    const v0, 0x7f0a0a79

    .line 440
    goto :goto_0

    .line 443
    :sswitch_3
    const v0, 0x7f0a0a7a

    .line 444
    goto :goto_0

    .line 447
    :sswitch_4
    const v0, 0x7f0a0a81

    .line 448
    goto :goto_0

    .line 451
    :sswitch_5
    const v0, 0x7f0a0a80

    goto :goto_0

    .line 429
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_3
        0x10 -> :sswitch_2
        0x40 -> :sswitch_5
    .end sparse-switch
.end method
