.class final Lhqy;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Lhei;

.field private synthetic d:Lhqx;


# direct methods
.method public constructor <init>(Lhqx;Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 59
    iput-object p1, p0, Lhqy;->d:Lhqx;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 60
    iput-object p2, p0, Lhqy;->a:Landroid/content/Context;

    .line 61
    iput p3, p0, Lhqy;->b:I

    .line 62
    const-class v0, Lhei;

    invoke-static {p2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhqy;->c:Lhei;

    .line 63
    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Void;
    .locals 5

    .prologue
    .line 68
    :try_start_0
    iget-object v0, p0, Lhqy;->c:Lhei;

    iget v1, p0, Lhqy;->b:I

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lhqy;->c:Lhei;

    iget v1, p0, Lhqy;->b:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 70
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 71
    new-instance v1, Lkfo;

    iget-object v2, p0, Lhqy;->a:Landroid/content/Context;

    iget v3, p0, Lhqy;->b:I

    invoke-direct {v1, v2, v3}, Lkfo;-><init>(Landroid/content/Context;I)V

    .line 72
    new-instance v2, Lhqc;

    iget-object v3, p0, Lhqy;->a:Landroid/content/Context;

    .line 73
    invoke-static {}, Lhqx;->a()Lnyr;

    move-result-object v4

    invoke-direct {v2, v3, v1, v0, v4}, Lhqc;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lnyr;)V

    .line 74
    invoke-virtual {v2}, Lhqc;->l()V

    .line 75
    invoke-virtual {v2}, Lhqc;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    invoke-virtual {v2}, Lhqc;->i()Lnyq;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_0

    iget-object v1, v0, Lnyq;->e:Lnyt;

    if-eqz v1, :cond_0

    .line 79
    iget-object v0, v0, Lnyq;->e:Lnyt;

    invoke-static {v0}, Ljdn;->a(Lnyt;)Ljdn;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lhqy;->a:Landroid/content/Context;

    iget v2, p0, Lhqy;->b:I

    invoke-static {v1, v2, v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->a(Landroid/content/Context;ILjdn;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    :cond_0
    :goto_0
    iget-object v0, p0, Lhqy;->d:Lhqx;

    iget v1, p0, Lhqy;->b:I

    invoke-virtual {v0, v1}, Lhqx;->a(I)V

    const/4 v0, 0x0

    return-object v0

    .line 84
    :cond_1
    :try_start_1
    const-string v0, "QuotaTaskManager"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "QuotaTaskManager"

    iget v1, p0, Lhqy;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x32

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Account not in valid state. accountId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lhqy;->d:Lhqx;

    iget v2, p0, Lhqy;->b:I

    invoke-virtual {v1, v2}, Lhqx;->a(I)V

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lhqy;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
