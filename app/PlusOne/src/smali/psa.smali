.class public final Lpsa;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lpul;

.field private b:[Lprz;

.field private c:Lppf;

.field private d:Lpsq;

.field private e:Lppv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    iput-object v1, p0, Lpsa;->a:Lpul;

    .line 20
    sget-object v0, Lprz;->a:[Lprz;

    iput-object v0, p0, Lpsa;->b:[Lprz;

    .line 23
    iput-object v1, p0, Lpsa;->c:Lppf;

    .line 26
    iput-object v1, p0, Lpsa;->d:Lpsq;

    .line 29
    iput-object v1, p0, Lpsa;->e:Lppv;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 59
    .line 60
    iget-object v0, p0, Lpsa;->a:Lpul;

    if-eqz v0, :cond_5

    .line 61
    const/4 v0, 0x1

    iget-object v2, p0, Lpsa;->a:Lpul;

    .line 62
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 64
    :goto_0
    iget-object v2, p0, Lpsa;->b:[Lprz;

    if-eqz v2, :cond_1

    .line 65
    iget-object v2, p0, Lpsa;->b:[Lprz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 66
    if-eqz v4, :cond_0

    .line 67
    const/4 v5, 0x2

    .line 68
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 65
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 72
    :cond_1
    iget-object v1, p0, Lpsa;->c:Lppf;

    if-eqz v1, :cond_2

    .line 73
    const/4 v1, 0x3

    iget-object v2, p0, Lpsa;->c:Lppf;

    .line 74
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_2
    iget-object v1, p0, Lpsa;->d:Lpsq;

    if-eqz v1, :cond_3

    .line 77
    const/4 v1, 0x4

    iget-object v2, p0, Lpsa;->d:Lpsq;

    .line 78
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_3
    iget-object v1, p0, Lpsa;->e:Lppv;

    if-eqz v1, :cond_4

    .line 81
    const/4 v1, 0x5

    iget-object v2, p0, Lpsa;->e:Lppv;

    .line 82
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_4
    iget-object v1, p0, Lpsa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    iput v0, p0, Lpsa;->ai:I

    .line 86
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpsa;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 94
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 95
    sparse-switch v0, :sswitch_data_0

    .line 99
    iget-object v2, p0, Lpsa;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 100
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpsa;->ah:Ljava/util/List;

    .line 103
    :cond_1
    iget-object v2, p0, Lpsa;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    :sswitch_0
    return-object p0

    .line 110
    :sswitch_1
    iget-object v0, p0, Lpsa;->a:Lpul;

    if-nez v0, :cond_2

    .line 111
    new-instance v0, Lpul;

    invoke-direct {v0}, Lpul;-><init>()V

    iput-object v0, p0, Lpsa;->a:Lpul;

    .line 113
    :cond_2
    iget-object v0, p0, Lpsa;->a:Lpul;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 117
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 118
    iget-object v0, p0, Lpsa;->b:[Lprz;

    if-nez v0, :cond_4

    move v0, v1

    .line 119
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lprz;

    .line 120
    iget-object v3, p0, Lpsa;->b:[Lprz;

    if-eqz v3, :cond_3

    .line 121
    iget-object v3, p0, Lpsa;->b:[Lprz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 123
    :cond_3
    iput-object v2, p0, Lpsa;->b:[Lprz;

    .line 124
    :goto_2
    iget-object v2, p0, Lpsa;->b:[Lprz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 125
    iget-object v2, p0, Lpsa;->b:[Lprz;

    new-instance v3, Lprz;

    invoke-direct {v3}, Lprz;-><init>()V

    aput-object v3, v2, v0

    .line 126
    iget-object v2, p0, Lpsa;->b:[Lprz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 127
    invoke-virtual {p1}, Loxn;->a()I

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 118
    :cond_4
    iget-object v0, p0, Lpsa;->b:[Lprz;

    array-length v0, v0

    goto :goto_1

    .line 130
    :cond_5
    iget-object v2, p0, Lpsa;->b:[Lprz;

    new-instance v3, Lprz;

    invoke-direct {v3}, Lprz;-><init>()V

    aput-object v3, v2, v0

    .line 131
    iget-object v2, p0, Lpsa;->b:[Lprz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 135
    :sswitch_3
    iget-object v0, p0, Lpsa;->c:Lppf;

    if-nez v0, :cond_6

    .line 136
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpsa;->c:Lppf;

    .line 138
    :cond_6
    iget-object v0, p0, Lpsa;->c:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 142
    :sswitch_4
    iget-object v0, p0, Lpsa;->d:Lpsq;

    if-nez v0, :cond_7

    .line 143
    new-instance v0, Lpsq;

    invoke-direct {v0}, Lpsq;-><init>()V

    iput-object v0, p0, Lpsa;->d:Lpsq;

    .line 145
    :cond_7
    iget-object v0, p0, Lpsa;->d:Lpsq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 149
    :sswitch_5
    iget-object v0, p0, Lpsa;->e:Lppv;

    if-nez v0, :cond_8

    .line 150
    new-instance v0, Lppv;

    invoke-direct {v0}, Lppv;-><init>()V

    iput-object v0, p0, Lpsa;->e:Lppv;

    .line 152
    :cond_8
    iget-object v0, p0, Lpsa;->e:Lppv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 95
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 34
    iget-object v0, p0, Lpsa;->a:Lpul;

    if-eqz v0, :cond_0

    .line 35
    const/4 v0, 0x1

    iget-object v1, p0, Lpsa;->a:Lpul;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_0
    iget-object v0, p0, Lpsa;->b:[Lprz;

    if-eqz v0, :cond_2

    .line 38
    iget-object v1, p0, Lpsa;->b:[Lprz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 39
    if-eqz v3, :cond_1

    .line 40
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 38
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_2
    iget-object v0, p0, Lpsa;->c:Lppf;

    if-eqz v0, :cond_3

    .line 45
    const/4 v0, 0x3

    iget-object v1, p0, Lpsa;->c:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 47
    :cond_3
    iget-object v0, p0, Lpsa;->d:Lpsq;

    if-eqz v0, :cond_4

    .line 48
    const/4 v0, 0x4

    iget-object v1, p0, Lpsa;->d:Lpsq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 50
    :cond_4
    iget-object v0, p0, Lpsa;->e:Lppv;

    if-eqz v0, :cond_5

    .line 51
    const/4 v0, 0x5

    iget-object v1, p0, Lpsa;->e:Lppv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 53
    :cond_5
    iget-object v0, p0, Lpsa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 55
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpsa;->a(Loxn;)Lpsa;

    move-result-object v0

    return-object v0
.end method
