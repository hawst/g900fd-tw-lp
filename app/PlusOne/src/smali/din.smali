.class public final Ldin;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmfq;",
        "Lmfr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Llae;

.field private final b:Lkzl;

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILlae;)V
    .locals 6

    .prologue
    .line 33
    const-string v3, "nearbystream"

    new-instance v4, Lmfq;

    invoke-direct {v4}, Lmfq;-><init>()V

    new-instance v5, Lmfr;

    invoke-direct {v5}, Lmfr;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 35
    iget-object v0, p0, Ldin;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Ldin;->b:Lkzl;

    .line 36
    iput-object p3, p0, Ldin;->a:Llae;

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Lmfq;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 41
    new-instance v0, Lnno;

    invoke-direct {v0}, Lnno;-><init>()V

    iput-object v0, p1, Lmfq;->a:Lnno;

    .line 42
    iget-object v0, p1, Lmfq;->a:Lnno;

    .line 44
    new-instance v1, Lnnp;

    invoke-direct {v1}, Lnnp;-><init>()V

    iput-object v1, v0, Lnno;->a:Lnnp;

    .line 45
    iget-object v1, v0, Lnno;->a:Lnnp;

    iget-object v2, p0, Ldin;->a:Llae;

    invoke-virtual {v2}, Llae;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lnnp;->a:Ljava/lang/Integer;

    .line 46
    iget-object v1, v0, Lnno;->a:Lnnp;

    iget-object v2, p0, Ldin;->a:Llae;

    invoke-virtual {v2}, Llae;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lnnp;->b:Ljava/lang/Integer;

    .line 47
    const/4 v1, 0x0

    iput-object v1, v0, Lnno;->c:Ljava/lang/String;

    .line 48
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lnno;->b:Ljava/lang/Integer;

    .line 50
    new-instance v1, Lnmr;

    invoke-direct {v1}, Lnmr;-><init>()V

    iput-object v1, v0, Lnno;->e:Lnmr;

    .line 51
    iget-object v1, v0, Lnno;->e:Lnmr;

    new-instance v2, Lofl;

    invoke-direct {v2}, Lofl;-><init>()V

    iput-object v2, v1, Lnmr;->a:Lofl;

    .line 52
    iget-object v1, v0, Lnno;->e:Lnmr;

    iget-object v1, v1, Lnmr;->a:Lofl;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lofl;->e:Ljava/lang/Boolean;

    .line 53
    iget-object v1, v0, Lnno;->e:Lnmr;

    iget-object v1, v1, Lnmr;->a:Lofl;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lofl;->b:Ljava/lang/Boolean;

    .line 54
    iget-object v1, v0, Lnno;->e:Lnmr;

    iget-object v1, v1, Lnmr;->a:Lofl;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lofl;->c:Ljava/lang/Boolean;

    .line 55
    iget-object v1, v0, Lnno;->e:Lnmr;

    new-instance v2, Logu;

    invoke-direct {v2}, Logu;-><init>()V

    iput-object v2, v1, Lnmr;->b:Logu;

    .line 56
    iget-object v1, v0, Lnno;->e:Lnmr;

    iget-object v1, v1, Lnmr;->b:Logu;

    iget-object v2, p0, Ldin;->b:Lkzl;

    iget v3, p0, Ldin;->c:I

    .line 57
    invoke-interface {v2, v4}, Lkzl;->a(Z)[I

    move-result-object v2

    iput-object v2, v1, Logu;->a:[I

    .line 58
    const/4 v1, 0x2

    iput v1, v0, Lnno;->d:I

    .line 59
    return-void
.end method

.method protected a(Lmfr;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 63
    iget-object v0, p1, Lmfr;->a:Lnnt;

    .line 65
    iget-object v1, p0, Ldin;->a:Llae;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-static {v4, v4, v1, v2, v3}, Llbc;->a(Ljava/lang/String;Ljava/lang/String;Llae;ZI)Ljava/lang/String;

    move-result-object v1

    .line 67
    iget-object v2, p0, Ldin;->f:Landroid/content/Context;

    iget v3, p0, Ldin;->c:I

    iget-object v0, v0, Lnnt;->a:Logi;

    iget-object v0, v0, Logi;->a:[Logr;

    invoke-static {v2, v3, v1, v0}, Llap;->a(Landroid/content/Context;ILjava/lang/String;[Logr;)Z

    move-result v0

    iput-boolean v0, p0, Ldin;->p:Z

    .line 69
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lmfq;

    invoke-virtual {p0, p1}, Ldin;->a(Lmfq;)V

    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Ldin;->p:Z

    return v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lmfr;

    invoke-virtual {p0, p1}, Ldin;->a(Lmfr;)V

    return-void
.end method
