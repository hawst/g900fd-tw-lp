.class public final Lqbo;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2302
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2333
    const/4 v0, 0x0

    .line 2334
    iget-object v1, p0, Lqbo;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2335
    const/4 v0, 0x1

    iget-object v1, p0, Lqbo;->a:Ljava/lang/Integer;

    .line 2336
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2338
    :cond_0
    iget-object v1, p0, Lqbo;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2339
    const/4 v1, 0x2

    iget-object v2, p0, Lqbo;->b:Ljava/lang/Integer;

    .line 2340
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2342
    :cond_1
    iget-object v1, p0, Lqbo;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2343
    const/4 v1, 0x3

    iget-object v2, p0, Lqbo;->c:Ljava/lang/Integer;

    .line 2344
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2346
    :cond_2
    iget-object v1, p0, Lqbo;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2347
    const/4 v1, 0x4

    iget-object v2, p0, Lqbo;->d:Ljava/lang/Integer;

    .line 2348
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2350
    :cond_3
    iget-object v1, p0, Lqbo;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2351
    iput v0, p0, Lqbo;->ai:I

    .line 2352
    return v0
.end method

.method public a(Loxn;)Lqbo;
    .locals 2

    .prologue
    .line 2360
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2361
    sparse-switch v0, :sswitch_data_0

    .line 2365
    iget-object v1, p0, Lqbo;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2366
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbo;->ah:Ljava/util/List;

    .line 2369
    :cond_1
    iget-object v1, p0, Lqbo;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2371
    :sswitch_0
    return-object p0

    .line 2376
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbo;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 2380
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbo;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2384
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbo;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 2388
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbo;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2361
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2315
    iget-object v0, p0, Lqbo;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2316
    const/4 v0, 0x1

    iget-object v1, p0, Lqbo;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2318
    :cond_0
    iget-object v0, p0, Lqbo;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2319
    const/4 v0, 0x2

    iget-object v1, p0, Lqbo;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2321
    :cond_1
    iget-object v0, p0, Lqbo;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2322
    const/4 v0, 0x3

    iget-object v1, p0, Lqbo;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2324
    :cond_2
    iget-object v0, p0, Lqbo;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2325
    const/4 v0, 0x4

    iget-object v1, p0, Lqbo;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2327
    :cond_3
    iget-object v0, p0, Lqbo;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2329
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2298
    invoke-virtual {p0, p1}, Lqbo;->a(Loxn;)Lqbo;

    move-result-object v0

    return-object v0
.end method
