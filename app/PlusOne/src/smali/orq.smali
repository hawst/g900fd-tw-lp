.class public final Lorq;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Lovm;

.field private c:Lorr;

.field private d:Lorx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 147
    invoke-direct {p0}, Loxq;-><init>()V

    .line 212
    iput-object v0, p0, Lorq;->b:Lovm;

    .line 215
    iput-object v0, p0, Lorq;->c:Lorr;

    .line 218
    iput-object v0, p0, Lorq;->d:Lorx;

    .line 147
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 241
    const/4 v0, 0x0

    .line 242
    iget-object v1, p0, Lorq;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 243
    const/4 v0, 0x1

    iget-object v1, p0, Lorq;->a:Ljava/lang/Boolean;

    .line 244
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 246
    :cond_0
    iget-object v1, p0, Lorq;->b:Lovm;

    if-eqz v1, :cond_1

    .line 247
    const/4 v1, 0x2

    iget-object v2, p0, Lorq;->b:Lovm;

    .line 248
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    :cond_1
    iget-object v1, p0, Lorq;->c:Lorr;

    if-eqz v1, :cond_2

    .line 251
    const/4 v1, 0x3

    iget-object v2, p0, Lorq;->c:Lorr;

    .line 252
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    :cond_2
    iget-object v1, p0, Lorq;->d:Lorx;

    if-eqz v1, :cond_3

    .line 255
    const/4 v1, 0x4

    iget-object v2, p0, Lorq;->d:Lorx;

    .line 256
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_3
    iget-object v1, p0, Lorq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    iput v0, p0, Lorq;->ai:I

    .line 260
    return v0
.end method

.method public a(Loxn;)Lorq;
    .locals 2

    .prologue
    .line 268
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 269
    sparse-switch v0, :sswitch_data_0

    .line 273
    iget-object v1, p0, Lorq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 274
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorq;->ah:Ljava/util/List;

    .line 277
    :cond_1
    iget-object v1, p0, Lorq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 279
    :sswitch_0
    return-object p0

    .line 284
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lorq;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 288
    :sswitch_2
    iget-object v0, p0, Lorq;->b:Lovm;

    if-nez v0, :cond_2

    .line 289
    new-instance v0, Lovm;

    invoke-direct {v0}, Lovm;-><init>()V

    iput-object v0, p0, Lorq;->b:Lovm;

    .line 291
    :cond_2
    iget-object v0, p0, Lorq;->b:Lovm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 295
    :sswitch_3
    iget-object v0, p0, Lorq;->c:Lorr;

    if-nez v0, :cond_3

    .line 296
    new-instance v0, Lorr;

    invoke-direct {v0}, Lorr;-><init>()V

    iput-object v0, p0, Lorq;->c:Lorr;

    .line 298
    :cond_3
    iget-object v0, p0, Lorq;->c:Lorr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 302
    :sswitch_4
    iget-object v0, p0, Lorq;->d:Lorx;

    if-nez v0, :cond_4

    .line 303
    new-instance v0, Lorx;

    invoke-direct {v0}, Lorx;-><init>()V

    iput-object v0, p0, Lorq;->d:Lorx;

    .line 305
    :cond_4
    iget-object v0, p0, Lorq;->d:Lorx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 269
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lorq;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 224
    const/4 v0, 0x1

    iget-object v1, p0, Lorq;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 226
    :cond_0
    iget-object v0, p0, Lorq;->b:Lovm;

    if-eqz v0, :cond_1

    .line 227
    const/4 v0, 0x2

    iget-object v1, p0, Lorq;->b:Lovm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 229
    :cond_1
    iget-object v0, p0, Lorq;->c:Lorr;

    if-eqz v0, :cond_2

    .line 230
    const/4 v0, 0x3

    iget-object v1, p0, Lorq;->c:Lorr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 232
    :cond_2
    iget-object v0, p0, Lorq;->d:Lorx;

    if-eqz v0, :cond_3

    .line 233
    const/4 v0, 0x4

    iget-object v1, p0, Lorq;->d:Lorx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 235
    :cond_3
    iget-object v0, p0, Lorq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 237
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0, p1}, Lorq;->a(Loxn;)Lorq;

    move-result-object v0

    return-object v0
.end method
