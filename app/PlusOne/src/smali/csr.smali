.class public final Lcsr;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lctm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcsr;->b:Ljava/util/ArrayList;

    .line 26
    iput-object p1, p0, Lcsr;->a:Landroid/content/Context;

    .line 27
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    .line 47
    new-instance v1, Lcss;

    invoke-direct {v1}, Lcss;-><init>()V

    .line 48
    iget-object v0, p0, Lcsr;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, v1}, Lctm;->a(Lctn;)V

    .line 50
    iget-object v0, v1, Lcss;->a:Lcst;

    if-nez v0, :cond_0

    .line 51
    const/4 v0, -0x1

    .line 54
    :goto_0
    return v0

    :cond_0
    iget-object v0, v1, Lcss;->a:Lcst;

    invoke-virtual {v0}, Lcst;->a()I

    move-result v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcsr;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 31
    return-void
.end method

.method public a(III)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lcsr;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 35
    iget-object v1, p0, Lcsr;->b:Ljava/util/ArrayList;

    new-instance v2, Lcsu;

    iget-object v3, p0, Lcsr;->a:Landroid/content/Context;

    invoke-direct {v2, v3, p1, p2, v0}, Lcsu;-><init>(Landroid/content/Context;IILjava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcsr;->b:Ljava/util/ArrayList;

    new-instance v1, Lcsv;

    iget-object v2, p0, Lcsr;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcsv;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcsr;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcsr;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 80
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcsr;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0}, Lctm;->a()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcsr;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, p2, p3}, Lctm;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 90
    invoke-static {}, Lcsw;->a()[I

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcsr;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method
