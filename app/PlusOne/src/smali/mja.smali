.class public final Lmja;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvt;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9313
    invoke-direct {p0}, Loxq;-><init>()V

    .line 9316
    iput-object v0, p0, Lmja;->apiHeader:Llyq;

    .line 9319
    iput-object v0, p0, Lmja;->a:Lnvt;

    .line 9313
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 9336
    const/4 v0, 0x0

    .line 9337
    iget-object v1, p0, Lmja;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 9338
    const/4 v0, 0x1

    iget-object v1, p0, Lmja;->apiHeader:Llyq;

    .line 9339
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9341
    :cond_0
    iget-object v1, p0, Lmja;->a:Lnvt;

    if-eqz v1, :cond_1

    .line 9342
    const/4 v1, 0x2

    iget-object v2, p0, Lmja;->a:Lnvt;

    .line 9343
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9345
    :cond_1
    iget-object v1, p0, Lmja;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9346
    iput v0, p0, Lmja;->ai:I

    .line 9347
    return v0
.end method

.method public a(Loxn;)Lmja;
    .locals 2

    .prologue
    .line 9355
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 9356
    sparse-switch v0, :sswitch_data_0

    .line 9360
    iget-object v1, p0, Lmja;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 9361
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmja;->ah:Ljava/util/List;

    .line 9364
    :cond_1
    iget-object v1, p0, Lmja;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 9366
    :sswitch_0
    return-object p0

    .line 9371
    :sswitch_1
    iget-object v0, p0, Lmja;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 9372
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmja;->apiHeader:Llyq;

    .line 9374
    :cond_2
    iget-object v0, p0, Lmja;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9378
    :sswitch_2
    iget-object v0, p0, Lmja;->a:Lnvt;

    if-nez v0, :cond_3

    .line 9379
    new-instance v0, Lnvt;

    invoke-direct {v0}, Lnvt;-><init>()V

    iput-object v0, p0, Lmja;->a:Lnvt;

    .line 9381
    :cond_3
    iget-object v0, p0, Lmja;->a:Lnvt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 9356
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 9324
    iget-object v0, p0, Lmja;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 9325
    const/4 v0, 0x1

    iget-object v1, p0, Lmja;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9327
    :cond_0
    iget-object v0, p0, Lmja;->a:Lnvt;

    if-eqz v0, :cond_1

    .line 9328
    const/4 v0, 0x2

    iget-object v1, p0, Lmja;->a:Lnvt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 9330
    :cond_1
    iget-object v0, p0, Lmja;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 9332
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 9309
    invoke-virtual {p0, p1}, Lmja;->a(Loxn;)Lmja;

    move-result-object v0

    return-object v0
.end method
