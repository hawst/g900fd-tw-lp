.class public final Ldyt;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:[Ljava/lang/String;

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I[",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 38
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Ldyt;->a(Landroid/net/Uri;)V

    .line 39
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Ldyt;->b:Ldp;

    .line 40
    iput p2, p0, Ldyt;->c:I

    .line 41
    iput-object p3, p0, Ldyt;->d:[Ljava/lang/String;

    .line 42
    iput-object p4, p0, Ldyt;->e:Ljava/util/ArrayList;

    .line 43
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 50
    iget v0, p0, Ldyt;->c:I

    .line 51
    invoke-virtual {p0}, Ldyt;->n()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Ldyt;->d:[Ljava/lang/String;

    iget-object v3, p0, Ldyt;->e:Ljava/util/ArrayList;

    invoke-static {v1, v0, v2, v3}, Ldsm;->a(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)Landroid/database/Cursor;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_0

    .line 54
    iget-object v1, p0, Ldyt;->b:Ldp;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 56
    :cond_0
    return-object v0
.end method
