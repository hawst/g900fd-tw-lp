.class public final Ljwt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljxh;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/io/File;

.field private c:Lifh;

.field private d:Lcom/google/android/libraries/social/gallery3d/common/FileCache;

.field private e:Z

.field private f:Ljxm;

.field private final g:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/os/ParcelFileDescriptor;",
            "Ljava/net/Socket;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/net/ServerSocket;

.field private final i:Ljava/lang/reflect/Method;

.field private final j:Landroid/content/BroadcastReceiver;

.field private final k:Ljxg;

.field private final l:Lign;

.field private m:Ljxd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljxd",
            "<",
            "Ljxb;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljxd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljxd",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-boolean v1, p0, Ljwt;->e:Z

    .line 96
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Ljwt;->g:Ljava/util/WeakHashMap;

    .line 104
    new-instance v1, Lign;

    invoke-direct {v1}, Lign;-><init>()V

    iput-object v1, p0, Ljwt;->l:Lign;

    .line 698
    new-instance v1, Ljwv;

    invoke-direct {v1}, Ljwv;-><init>()V

    iput-object v1, p0, Ljwt;->m:Ljxd;

    .line 714
    new-instance v1, Ljww;

    invoke-direct {v1}, Ljww;-><init>()V

    iput-object v1, p0, Ljwt;->n:Ljxd;

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Ljwt;->a:Landroid/content/Context;

    .line 108
    new-instance v1, Ljxg;

    invoke-direct {v1, p0}, Ljxg;-><init>(Ljxh;)V

    iput-object v1, p0, Ljwt;->k:Ljxg;

    .line 111
    :try_start_0
    new-instance v1, Ljava/net/ServerSocket;

    invoke-direct {v1}, Ljava/net/ServerSocket;-><init>()V

    iput-object v1, p0, Ljwt;->h:Ljava/net/ServerSocket;

    .line 112
    iget-object v1, p0, Ljwt;->h:Ljava/net/ServerSocket;

    const/4 v2, 0x0

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :try_start_1
    const-class v1, Landroid/os/ParcelFileDescriptor;

    const-string v2, "createPipe"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 124
    :goto_0
    iput-object v0, p0, Ljwt;->i:Ljava/lang/reflect/Method;

    .line 136
    new-instance v0, Ljwu;

    invoke-direct {v0, p0}, Ljwu;-><init>(Ljwt;)V

    iput-object v0, p0, Ljwt;->j:Landroid/content/BroadcastReceiver;

    .line 143
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 144
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 146
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 148
    iget-object v1, p0, Ljwt;->a:Landroid/content/Context;

    iget-object v2, p0, Ljwt;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 149
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "cannot create server socket"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private static a(Landroid/net/Uri;)J
    .locals 3

    .prologue
    .line 242
    :try_start_0
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 245
    :goto_0
    return-wide v0

    .line 244
    :catch_0
    move-exception v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "cannot get id from: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ljxd",
            "<TT;>;Z)",
            "Landroid/os/ParcelFileDescriptor;"
        }
    .end annotation

    .prologue
    .line 902
    :try_start_0
    invoke-direct {p0}, Ljwt;->g()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 903
    new-instance v1, Ljwx;

    invoke-direct {v1, p3, p2, v0, p1}, Ljwx;-><init>(ZLjxd;[Landroid/os/ParcelFileDescriptor;Ljava/lang/Object;)V

    .line 915
    iget-object v2, p0, Ljwt;->l:Lign;

    invoke-virtual {v2, v1}, Lign;->a(Ligo;)Ligi;

    .line 916
    const/4 v1, 0x0

    aget-object v0, v0, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 918
    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "failure making pipe"

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 4

    .prologue
    .line 250
    invoke-static {p0}, Ljvw;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 251
    invoke-static {p0}, Ljvw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 252
    const-string v1, "I"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 253
    const-string v2, "k"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 254
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 255
    const/16 v3, 0x73

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 256
    const-string v3, "-no"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    if-eqz p2, :cond_0

    .line 258
    const-string v3, "-c"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    :cond_0
    if-eqz v1, :cond_1

    .line 261
    const-string v1, "-I"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    :cond_1
    if-eqz v0, :cond_2

    .line 264
    const-string v0, "-k"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Ljvw;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 268
    :goto_0
    return-object v0

    :cond_3
    invoke-static {p1, p0}, Ljwm;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(JILjava/lang/String;)Ljxb;
    .locals 3

    .prologue
    .line 776
    invoke-direct {p0, p1, p2, p3}, Ljwt;->a(JI)[B

    move-result-object v0

    .line 777
    invoke-virtual {p0, p4}, Ljwt;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 778
    invoke-direct {p0, v0, v1}, Ljwt;->a([B[B)Ljxb;

    move-result-object v0

    return-object v0
.end method

.method private a([B[B)Ljxb;
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 752
    :try_start_0
    invoke-direct {p0}, Ljwt;->d()Lifh;

    move-result-object v2

    .line 753
    if-nez v2, :cond_1

    .line 770
    :cond_0
    :goto_0
    return-object v0

    .line 756
    :cond_1
    invoke-static {p1}, Lifu;->a([B)J

    move-result-wide v4

    .line 758
    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 759
    :try_start_1
    invoke-virtual {v2, v4, v5}, Lifh;->a(J)[B

    move-result-object v3

    .line 760
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 761
    if-eqz v3, :cond_0

    if-nez p2, :cond_4

    :try_start_2
    array-length v2, v3

    array-length v4, p1

    if-ge v2, v4, :cond_3

    :cond_2
    :goto_1
    if-eqz v1, :cond_0

    .line 765
    array-length v1, p1

    aget-byte v1, v3, v1

    and-int/lit16 v1, v1, 0xff

    array-length v2, p1

    add-int/lit8 v2, v2, 0x1

    aget-byte v2, v3, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    add-int/2addr v2, v1

    .line 767
    array-length v1, p1

    add-int/lit8 v1, v1, 0x2

    aget-byte v1, v3, v1

    and-int/lit16 v4, v1, 0xff

    .line 768
    new-instance v1, Ljxb;

    invoke-direct {v1, v2, v4, v3}, Ljxb;-><init>(II[B)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 760
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    .line 770
    :catch_0
    move-exception v1

    goto :goto_0

    .line 761
    :cond_3
    array-length v4, p1

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-byte v5, p1, v2

    aget-byte v6, v3, v2

    if-ne v5, v6, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    array-length v2, p1

    array-length v4, p2

    add-int/2addr v2, v4

    add-int/lit8 v4, v2, 0x3

    array-length v2, v3

    if-lt v2, v4, :cond_2

    invoke-direct {p0, v4}, Ljwt;->a(I)Z

    move-result v2

    if-nez v2, :cond_2

    array-length v5, p1

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_5

    aget-byte v6, p1, v2

    aget-byte v7, v3, v2

    if-ne v6, v7, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    array-length v2, p1

    add-int/lit8 v5, v2, 0x1

    aget-byte v2, v3, v2

    int-to-byte v6, v4

    if-ne v2, v6, :cond_2

    add-int/lit8 v2, v5, 0x1

    aget-byte v5, v3, v5

    ushr-int/lit8 v4, v4, 0x8

    int-to-byte v4, v4

    if-ne v5, v4, :cond_2

    add-int/lit8 v4, v2, 0x1

    array-length v5, p2

    move v2, v1

    :goto_4
    if-ge v2, v5, :cond_6

    aget-byte v6, p2, v2

    add-int v7, v2, v4

    aget-byte v7, v3, v7
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    if-ne v6, v7, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private a(JILjava/lang/String;I[B)V
    .locals 3

    .prologue
    .line 815
    invoke-direct {p0, p1, p2, p3}, Ljwt;->a(JI)[B

    move-result-object v0

    .line 816
    invoke-virtual {p0, p4}, Ljwt;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 817
    invoke-virtual {p0, v0, v1, p5, p6}, Ljwt;->a([B[BI[B)V

    .line 818
    return-void
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 744
    const/16 v0, 0x7fff

    if-le p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 6

    .prologue
    .line 178
    iget-object v0, p0, Ljwt;->f:Ljxm;

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Ljxm;

    invoke-virtual {p0}, Ljwt;->a()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "cache_versions.info"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljxm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ljwt;->f:Ljxm;

    .line 181
    :cond_0
    iget-object v0, p0, Ljwt;->f:Ljxm;

    invoke-virtual {v0, p1}, Ljxm;->a(Ljava/lang/String;)I

    move-result v0

    if-eq v0, p2, :cond_1

    .line 182
    iget-object v0, p0, Ljwt;->f:Ljxm;

    invoke-virtual {v0, p1, p2}, Ljxm;->a(Ljava/lang/String;I)V

    .line 183
    iget-object v0, p0, Ljwt;->f:Ljxm;

    invoke-virtual {v0}, Ljxm;->a()V

    .line 184
    const/4 v0, 0x1

    .line 186
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JI)[B
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 874
    const/16 v0, 0x9

    new-array v1, v0, [B

    .line 875
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 876
    shl-int/lit8 v2, v0, 0x3

    ushr-long v2, p1, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 875
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 878
    :cond_0
    int-to-byte v0, p3

    aput-byte v0, v1, v4

    .line 879
    return-object v1
.end method

.method static synthetic a(Ljwt;JI)[B
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Ljwt;->a(JI)[B

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized b(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 405
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Ljwt;->e()Lcom/google/android/libraries/social/gallery3d/common/FileCache;

    move-result-object v1

    .line 407
    if-eqz v1, :cond_1

    .line 409
    invoke-virtual {v1, p3}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a(Ljava/lang/String;)Lifo;

    move-result-object v1

    .line 410
    if-eqz v1, :cond_1

    .line 411
    iget-object v1, v1, Lifo;->b:Ljava/io/File;

    const/high16 v2, 0x10000000

    invoke-static {v1, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 424
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v1

    .line 416
    :cond_1
    if-nez p4, :cond_0

    .line 424
    const/4 v0, 0x0

    :try_start_1
    new-instance v1, Ljxc;

    iget-object v2, p0, Ljwt;->k:Ljxg;

    .line 425
    invoke-virtual {v2, p3}, Ljxg;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v1, p1, p2, v2}, Ljxc;-><init>(JLjava/io/InputStream;)V

    const/4 v2, 0x1

    .line 424
    invoke-direct {p0, v0, v1, v2}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 427
    :catch_1
    move-exception v0

    :try_start_2
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 405
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 434
    invoke-direct {p0, p1, p2, v2, p3}, Ljwt;->a(JILjava/lang/String;)Ljxb;

    move-result-object v1

    .line 435
    if-eqz v1, :cond_1

    .line 436
    iget-object v0, p0, Ljwt;->m:Ljxd;

    invoke-direct {p0, v1, v0, v2}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 460
    :cond_0
    :goto_0
    return-object v0

    .line 441
    :cond_1
    :try_start_0
    const-string v1, ".screen"

    invoke-static {p1, p2, v1}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->a(JLjava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 442
    if-eqz v1, :cond_2

    .line 443
    const/high16 v2, 0x10000000

    invoke-static {v1, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 446
    :cond_2
    if-nez p4, :cond_0

    .line 453
    if-nez p3, :cond_3

    .line 454
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 458
    :cond_3
    :try_start_1
    new-instance v0, Ljwy;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ljwy;-><init>(Ljwt;JIILjava/lang/String;)V

    .line 460
    const/4 v1, 0x0

    new-instance v2, Ljxa;

    invoke-direct {v2, p1, p2, p3, v0}, Ljxa;-><init>(JLjava/lang/String;Ljwz;)V

    const/4 v0, 0x1

    invoke-direct {p0, v1, v2, v0}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 463
    :catch_1
    move-exception v0

    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0
.end method

.method private d(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 9

    .prologue
    .line 473
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, Ljwt;->a(JILjava/lang/String;)Ljxb;

    move-result-object v0

    .line 476
    if-eqz v0, :cond_0

    iget v1, v0, Ljxb;->b:I

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    .line 477
    iget-object v1, p0, Ljwt;->m:Ljxd;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 546
    :goto_0
    return-object v0

    .line 482
    :cond_0
    :try_start_0
    const-string v1, ".screen"

    invoke-static {p1, p2, v1}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->a(JLjava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 483
    if-eqz v1, :cond_2

    .line 484
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 485
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 486
    const/4 v4, 0x1

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 487
    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 488
    sget v4, Ljvv;->b:I

    int-to-float v4, v4

    iget v5, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 489
    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 488
    invoke-static {v4}, Lifg;->a(F)I

    move-result v4

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 491
    const/4 v4, 0x0

    iput-boolean v4, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 492
    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 494
    if-eqz v3, :cond_1

    .line 495
    sget v1, Ljvv;->b:I

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Lifg;->a(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 496
    const/16 v2, 0x5f

    .line 497
    invoke-static {v1, v2}, Lifg;->a(Landroid/graphics/Bitmap;I)[B

    move-result-object v7

    .line 498
    const/4 v4, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-direct/range {v1 .. v7}, Ljwt;->a(JILjava/lang/String;I[B)V

    .line 499
    iget-object v1, p0, Ljwt;->n:Ljxd;

    const/4 v2, 0x0

    invoke-direct {p0, v7, v1, v2}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0

    .line 501
    :cond_1
    const-string v3, "gp.PicasaStore"

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x35

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "invalid prefetch file: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", length: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 505
    :cond_2
    :goto_1
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v1, p3}, Ljwt;->a(JILjava/lang/String;)Ljxb;

    move-result-object v1

    .line 510
    if-eqz v1, :cond_3

    .line 511
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 512
    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 513
    iget-object v3, v1, Ljxb;->c:[B

    iget v4, v1, Ljxb;->a:I

    iget-object v5, v1, Ljxb;->c:[B

    array-length v5, v5

    iget v6, v1, Ljxb;->a:I

    sub-int/2addr v5, v6

    invoke-static {v3, v4, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    .line 515
    sget v3, Ljvv;->b:I

    int-to-float v3, v3

    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v5, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 516
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 515
    invoke-static {v3}, Lifg;->a(F)I

    move-result v3

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 518
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 519
    iget-object v2, v1, Ljxb;->c:[B

    iget v3, v1, Ljxb;->a:I

    iget-object v4, v1, Ljxb;->c:[B

    array-length v4, v4

    iget v1, v1, Ljxb;->a:I

    sub-int v1, v4, v1

    invoke-static {v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 521
    if-eqz v1, :cond_3

    .line 522
    sget v0, Ljvv;->b:I

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lifg;->a(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 523
    const/16 v1, 0x5f

    .line 524
    invoke-static {v0, v1}, Lifg;->a(Landroid/graphics/Bitmap;I)[B

    move-result-object v7

    .line 525
    const/4 v4, 0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v5, p3

    invoke-direct/range {v1 .. v7}, Ljwt;->a(JILjava/lang/String;I[B)V

    .line 526
    iget-object v0, p0, Ljwt;->n:Ljxd;

    const/4 v1, 0x0

    invoke-direct {p0, v7, v0, v1}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto/16 :goto_0

    .line 532
    :cond_3
    if-eqz v0, :cond_4

    .line 533
    iget-object v1, p0, Ljwt;->m:Ljxd;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto/16 :goto_0

    .line 537
    :cond_4
    if-eqz p4, :cond_5

    .line 538
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 540
    :cond_5
    if-nez p3, :cond_6

    .line 541
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 544
    :cond_6
    :try_start_1
    new-instance v0, Ljwy;

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v1, p0

    move-wide v2, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ljwy;-><init>(Ljwt;JIILjava/lang/String;)V

    .line 546
    const/4 v1, 0x0

    new-instance v2, Ljxa;

    invoke-direct {v2, p1, p2, p3, v0}, Ljxa;-><init>(JLjava/lang/String;Ljwz;)V

    const/4 v0, 0x1

    invoke-direct {p0, v1, v2, v0}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 549
    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    :catch_1
    move-exception v1

    goto/16 :goto_1
.end method

.method private declared-synchronized d()Lifh;
    .locals 6

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ljwt;->c:Lifh;

    if-nez v0, :cond_1

    .line 191
    invoke-virtual {p0}, Ljwt;->a()Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 193
    :try_start_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "picasa-cache"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 194
    const-string v0, "picasa-image-cache-version"

    const/4 v2, 0x5

    invoke-direct {p0, v0, v2}, Ljwt;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    invoke-static {v1}, Lifh;->a(Ljava/lang/String;)V

    .line 197
    :cond_0
    iget-boolean v0, p0, Ljwt;->e:Z

    if-eqz v0, :cond_2

    .line 198
    new-instance v0, Lifh;

    const/16 v2, 0x4e2

    const/high16 v3, 0x3200000

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-direct/range {v0 .. v5}, Lifh;-><init>(Ljava/lang/String;IIZI)V

    iput-object v0, p0, Ljwt;->c:Lifh;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    :cond_1
    :goto_0
    :try_start_2
    iget-object v0, p0, Ljwt;->c:Lifh;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 202
    :cond_2
    :try_start_3
    new-instance v0, Lifh;

    const/16 v2, 0x1388

    const/high16 v3, 0xc800000

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-direct/range {v0 .. v5}, Lifh;-><init>(Ljava/lang/String;IIZI)V

    iput-object v0, p0, Ljwt;->c:Lifh;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()Lcom/google/android/libraries/social/gallery3d/common/FileCache;
    .locals 6

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ljwt;->d:Lcom/google/android/libraries/social/gallery3d/common/FileCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 215
    :try_start_1
    new-instance v2, Ljava/io/File;

    .line 216
    invoke-virtual {p0}, Ljwt;->a()Ljava/io/File;

    move-result-object v0

    const-string v1, "download-cache"

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 217
    const-string v0, "picasa-download-cache-version"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Ljwt;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Ljwt;->a:Landroid/content/Context;

    const-string v1, "picasa-downloads"

    invoke-static {v0, v2, v1}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V

    .line 220
    :cond_0
    iget-boolean v0, p0, Ljwt;->e:Z

    if-nez v0, :cond_2

    .line 221
    new-instance v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;

    iget-object v1, p0, Ljwt;->a:Landroid/content/Context;

    const-string v3, "picasa-downloads"

    const-wide/32 v4, 0x6400000

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V

    iput-object v0, p0, Ljwt;->d:Lcom/google/android/libraries/social/gallery3d/common/FileCache;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    :cond_1
    :goto_0
    :try_start_2
    iget-object v0, p0, Ljwt;->d:Lcom/google/android/libraries/social/gallery3d/common/FileCache;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 224
    :cond_2
    :try_start_3
    new-instance v0, Lcom/google/android/libraries/social/gallery3d/common/FileCache;

    iget-object v1, p0, Ljwt;->a:Landroid/content/Context;

    const-string v3, "picasa-downloads"

    const-wide/32 v4, 0x1400000

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V

    iput-object v0, p0, Ljwt;->d:Lcom/google/android/libraries/social/gallery3d/common/FileCache;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f()[Landroid/os/ParcelFileDescriptor;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 927
    new-array v0, v7, [Ljava/net/Socket;

    .line 928
    monitor-enter p0

    .line 929
    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/net/Socket;

    iget-object v3, p0, Ljwt;->h:Ljava/net/ServerSocket;

    .line 930
    invoke-virtual {v3}, Ljava/net/ServerSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v3

    iget-object v4, p0, Ljwt;->h:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v4

    invoke-direct {v2, v3, v4}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    aput-object v2, v0, v1

    .line 931
    const/4 v1, 0x1

    iget-object v2, p0, Ljwt;->h:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v2

    aput-object v2, v0, v1

    .line 932
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 933
    new-array v1, v7, [Landroid/os/ParcelFileDescriptor;

    aget-object v2, v0, v5

    .line 934
    invoke-static {v2}, Landroid/os/ParcelFileDescriptor;->fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    aput-object v2, v1, v5

    aget-object v2, v0, v6

    .line 935
    invoke-static {v2}, Landroid/os/ParcelFileDescriptor;->fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    aput-object v2, v1, v6

    .line 939
    iget-object v2, p0, Ljwt;->g:Ljava/util/WeakHashMap;

    aget-object v3, v1, v5

    aget-object v4, v0, v5

    invoke-virtual {v2, v3, v4}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 940
    iget-object v2, p0, Ljwt;->g:Ljava/util/WeakHashMap;

    aget-object v3, v1, v6

    aget-object v0, v0, v6

    invoke-virtual {v2, v3, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 941
    return-object v1

    .line 932
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private g()[Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    .line 945
    iget-object v0, p0, Ljwt;->i:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 946
    invoke-direct {p0}, Ljwt;->f()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 949
    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Ljwt;->i:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 950
    :catch_0
    move-exception v0

    .line 951
    const-string v1, "gp.PicasaStore"

    const-string v2, "fail to create pipe"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 952
    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_1

    .line 953
    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 955
    :cond_1
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public a(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    .line 387
    :try_start_0
    const-string v0, ".full"

    invoke-static {p1, p2, v0}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->a(JLjava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 388
    if-eqz v0, :cond_0

    .line 389
    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 399
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    .line 392
    :cond_0
    if-nez p3, :cond_1

    .line 397
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 399
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Ljwt;->b(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x2

    const/4 v4, 0x0

    .line 284
    const-string v0, "w"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v2, "invalid mode: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 288
    :cond_1
    invoke-static {p1}, Ljwt;->a(Landroid/net/Uri;)J

    move-result-wide v2

    .line 289
    const-string v0, "content_url"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 292
    invoke-direct {p0, v2, v3, v6, v5}, Ljwt;->a(JILjava/lang/String;)Ljxb;

    move-result-object v0

    .line 293
    if-eqz v0, :cond_2

    .line 294
    iget-object v1, p0, Ljwt;->m:Ljxd;

    invoke-direct {p0, v0, v1, v4}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 329
    :goto_1
    return-object v0

    .line 296
    :cond_2
    if-nez v5, :cond_3

    .line 297
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lifu;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_3
    const-string v6, ".thumb"

    invoke-static {}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->f()Ljava/io/File;

    move-result-object v7

    if-nez v7, :cond_4

    move-object v0, v1

    :goto_2
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 302
    :goto_3
    if-eqz v0, :cond_8

    .line 303
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/32 v8, 0x80000

    cmp-long v1, v6, v8

    if-gez v1, :cond_7

    .line 304
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v1, v6

    new-array v7, v1, [B

    .line 305
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 308
    const/4 v0, 0x0

    :try_start_0
    array-length v1, v7

    invoke-virtual {v8, v7, v0, v1}, Ljava/io/FileInputStream;->read([BII)I

    move-result v0

    move v1, v4

    .line 309
    :goto_4
    if-ltz v0, :cond_6

    array-length v4, v7

    if-ge v1, v4, :cond_6

    .line 310
    add-int/2addr v1, v0

    .line 311
    array-length v0, v7

    sub-int/2addr v0, v1

    invoke-virtual {v8, v7, v1, v0}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_4

    .line 301
    :cond_4
    new-instance v0, Ljava/io/File;

    const-string v8, "picasa_covers/"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const/16 v10, 0x5f

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v5}, Lifu;->a(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x0

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v11, v12

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v7, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_5
    move-object v0, v1

    goto/16 :goto_3

    .line 313
    :cond_6
    const/4 v4, 0x2

    const/4 v6, 0x0

    move-object v1, p0

    :try_start_1
    invoke-direct/range {v1 .. v7}, Ljwt;->a(JILjava/lang/String;I[B)V

    .line 314
    iget-object v0, p0, Ljwt;->n:Ljxd;

    const/4 v1, 0x0

    invoke-direct {p0, v7, v0, v1}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 318
    invoke-static {v8}, Lifu;->a(Ljava/io/Closeable;)V

    goto/16 :goto_1

    .line 315
    :catch_0
    move-exception v0

    .line 316
    :try_start_2
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lifu;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 318
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lifu;->a(Ljava/io/Closeable;)V

    throw v0

    .line 321
    :cond_7
    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto/16 :goto_1

    .line 326
    :cond_8
    :try_start_3
    sget v0, Ljvv;->b:I

    const/4 v1, 0x1

    invoke-static {v5, v0, v1}, Ljwt;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 327
    new-instance v6, Ljwy;

    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object v7, p0

    move-wide v8, v2

    move-object v12, v5

    invoke-direct/range {v6 .. v12}, Ljwy;-><init>(Ljwt;JIILjava/lang/String;)V

    .line 329
    const/4 v1, 0x0

    new-instance v4, Ljxa;

    invoke-direct {v4, v2, v3, v0, v6}, Ljxa;-><init>(JLjava/lang/String;Ljwz;)V

    const/4 v0, 0x1

    invoke-direct {p0, v1, v4, v0}, Ljwt;->a(Ljava/lang/Object;Ljxd;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v0

    goto/16 :goto_1

    .line 332
    :catch_1
    move-exception v0

    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0
.end method

.method public declared-synchronized a()Ljava/io/File;
    .locals 3

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ljwt;->b:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Ljwt;->b:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    :goto_0
    monitor-exit p0

    return-object v0

    .line 156
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->f()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Ljwt;->b:Ljava/io/File;

    .line 157
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljwt;->e:Z

    .line 160
    iget-object v0, p0, Ljwt;->b:Ljava/io/File;

    if-nez v0, :cond_2

    .line 161
    iget-object v0, p0, Ljwt;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Ljwt;->b:Ljava/io/File;

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljwt;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 165
    :try_start_2
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Ljwt;->b:Ljava/io/File;

    const-string v2, ".nomedia"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 166
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 167
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 172
    :cond_1
    :goto_1
    :try_start_3
    iget-object v0, p0, Ljwt;->b:Ljava/io/File;

    invoke-static {v0}, Llsk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    :cond_2
    iget-object v0, p0, Ljwt;->b:Ljava/io/File;

    goto :goto_0

    .line 170
    :catch_0
    move-exception v0

    iget-object v0, p0, Ljwt;->b:Ljava/io/File;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "fail to create \'.nomedia\' in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Ljava/io/File;)V
    .locals 1

    .prologue
    .line 984
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Ljwt;->e()Lcom/google/android/libraries/social/gallery3d/common/FileCache;

    move-result-object v0

    .line 985
    if-eqz v0, :cond_0

    .line 986
    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a(Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 990
    :goto_0
    monitor-exit p0

    return-void

    .line 988
    :cond_0
    :try_start_1
    invoke-virtual {p2}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 984
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a([B[BI[B)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 782
    if-nez p2, :cond_1

    .line 783
    :goto_0
    array-length v1, p1

    add-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x3

    .line 784
    invoke-direct {p0, v1}, Ljwt;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 810
    :cond_0
    :goto_1
    return-void

    .line 782
    :cond_1
    array-length v0, p2

    goto :goto_0

    .line 788
    :cond_2
    :try_start_0
    invoke-direct {p0}, Ljwt;->d()Lifh;

    move-result-object v2

    .line 789
    if-eqz v2, :cond_0

    .line 792
    array-length v3, p4

    add-int/2addr v3, v1

    new-array v3, v3, [B

    .line 796
    const/4 v4, 0x0

    const/4 v5, 0x0

    array-length v6, p1

    invoke-static {p1, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 797
    array-length v4, p1

    int-to-byte v5, v1

    aput-byte v5, v3, v4

    .line 798
    array-length v4, p1

    add-int/lit8 v4, v4, 0x1

    ushr-int/lit8 v5, v1, 0x8

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 799
    array-length v4, p1

    add-int/lit8 v4, v4, 0x2

    int-to-byte v5, p3

    aput-byte v5, v3, v4

    .line 800
    if-lez v0, :cond_3

    .line 801
    const/4 v4, 0x0

    array-length v5, p1

    add-int/lit8 v5, v5, 0x3

    invoke-static {p2, v4, v3, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 803
    :cond_3
    const/4 v0, 0x0

    array-length v4, p4

    invoke-static {p4, v0, v3, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 805
    invoke-static {p1}, Lifu;->a([B)J

    move-result-wide v0

    .line 806
    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 807
    :try_start_1
    invoke-virtual {v2, v0, v1, v3}, Lifh;->a(J[B)V

    .line 808
    monitor-exit v2

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 810
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method a(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 884
    if-nez p1, :cond_0

    .line 885
    const/4 v0, 0x0

    .line 895
    :goto_0
    return-object v0

    .line 888
    :cond_0
    const-string v0, "https://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 889
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 895
    :cond_1
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    goto :goto_0

    .line 890
    :cond_2
    const-string v0, "http://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 891
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method public b(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 339
    const-string v0, "w"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v2, "invalid mode: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 343
    :cond_1
    const-string v0, "type"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 344
    const-string v0, "1"

    const-string v2, "cache_only"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 346
    invoke-static {p1}, Ljwt;->a(Landroid/net/Uri;)J

    move-result-wide v4

    .line 349
    const-string v0, "content_url"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 351
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    .line 354
    const-string v3, "screennail"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 355
    sget v1, Ljvv;->a:I

    .line 356
    invoke-static {v0, v1, v8}, Ljwt;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 357
    invoke-direct {p0, v4, v5, v0, v2}, Ljwt;->c(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 367
    :goto_1
    return-object v0

    .line 358
    :cond_2
    const-string v3, "thumbnail"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 359
    sget v1, Ljvv;->b:I

    invoke-static {v0, v1, v9}, Ljwt;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    .line 360
    invoke-direct {p0, v4, v5, v0, v2}, Ljwt;->d(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_1

    .line 362
    :cond_3
    invoke-virtual {p0, v4, v5, v0, v2}, Ljwt;->a(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_1

    .line 364
    :cond_4
    if-eqz v0, :cond_7

    .line 367
    const-string v3, "thumbnail"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    sget v1, Ljvv;->b:I

    invoke-static {v0, v1, v9}, Ljwt;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    :cond_5
    :goto_2
    const-wide/16 v4, -0x1

    invoke-direct {p0, v4, v5, v0, v2}, Ljwt;->b(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_1

    :cond_6
    const-string v3, "screennail"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget v1, Ljvv;->a:I

    invoke-static {v0, v1, v8}, Ljwt;->a(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 369
    :cond_7
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method declared-synchronized b()V
    .locals 1

    .prologue
    .line 960
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Ljwt;->b:Ljava/io/File;

    .line 961
    iget-object v0, p0, Ljwt;->c:Lifh;

    if-eqz v0, :cond_0

    .line 962
    iget-object v0, p0, Ljwt;->c:Lifh;

    invoke-static {v0}, Lifu;->a(Ljava/io/Closeable;)V

    .line 963
    const/4 v0, 0x0

    iput-object v0, p0, Ljwt;->c:Lifh;

    .line 965
    :cond_0
    iget-object v0, p0, Ljwt;->d:Lcom/google/android/libraries/social/gallery3d/common/FileCache;

    if-eqz v0, :cond_1

    .line 966
    iget-object v0, p0, Ljwt;->d:Lcom/google/android/libraries/social/gallery3d/common/FileCache;

    invoke-static {v0}, Lifu;->a(Ljava/io/Closeable;)V

    .line 967
    const/4 v0, 0x0

    iput-object v0, p0, Ljwt;->d:Lcom/google/android/libraries/social/gallery3d/common/FileCache;

    .line 969
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Ljwt;->f:Ljxm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 970
    monitor-exit p0

    return-void

    .line 960
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Ljava/io/File;
    .locals 2

    .prologue
    .line 975
    :try_start_0
    invoke-direct {p0}, Ljwt;->e()Lcom/google/android/libraries/social/gallery3d/common/FileCache;

    move-result-object v0

    .line 976
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/social/gallery3d/common/FileCache;->a()Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 977
    :catch_0
    move-exception v0

    .line 978
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
