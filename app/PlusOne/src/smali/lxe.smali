.class public final Llxe;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/String;

.field private b:[Ljava/lang/String;

.field private c:[Llxz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1798
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1801
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Llxe;->a:[Ljava/lang/String;

    .line 1804
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Llxe;->b:[Ljava/lang/String;

    .line 1807
    sget-object v0, Llxz;->a:[Llxz;

    iput-object v0, p0, Llxe;->c:[Llxz;

    .line 1798
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1835
    .line 1836
    iget-object v0, p0, Llxe;->a:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Llxe;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1838
    iget-object v3, p0, Llxe;->a:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 1840
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1838
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1843
    :cond_0
    iget-object v0, p0, Llxe;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 1845
    :goto_1
    iget-object v2, p0, Llxe;->b:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Llxe;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1847
    iget-object v4, p0, Llxe;->b:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 1849
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 1847
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1851
    :cond_1
    add-int/2addr v0, v3

    .line 1852
    iget-object v2, p0, Llxe;->b:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1854
    :cond_2
    iget-object v2, p0, Llxe;->c:[Llxz;

    if-eqz v2, :cond_4

    .line 1855
    iget-object v2, p0, Llxe;->c:[Llxz;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1856
    if-eqz v4, :cond_3

    .line 1857
    const/4 v5, 0x3

    .line 1858
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1855
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1862
    :cond_4
    iget-object v1, p0, Llxe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1863
    iput v0, p0, Llxe;->ai:I

    .line 1864
    return v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Llxe;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1872
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1873
    sparse-switch v0, :sswitch_data_0

    .line 1877
    iget-object v2, p0, Llxe;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1878
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llxe;->ah:Ljava/util/List;

    .line 1881
    :cond_1
    iget-object v2, p0, Llxe;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1883
    :sswitch_0
    return-object p0

    .line 1888
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1889
    iget-object v0, p0, Llxe;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 1890
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1891
    iget-object v3, p0, Llxe;->a:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1892
    iput-object v2, p0, Llxe;->a:[Ljava/lang/String;

    .line 1893
    :goto_1
    iget-object v2, p0, Llxe;->a:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 1894
    iget-object v2, p0, Llxe;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1895
    invoke-virtual {p1}, Loxn;->a()I

    .line 1893
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1898
    :cond_2
    iget-object v2, p0, Llxe;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 1902
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1903
    iget-object v0, p0, Llxe;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 1904
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 1905
    iget-object v3, p0, Llxe;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1906
    iput-object v2, p0, Llxe;->b:[Ljava/lang/String;

    .line 1907
    :goto_2
    iget-object v2, p0, Llxe;->b:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 1908
    iget-object v2, p0, Llxe;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1909
    invoke-virtual {p1}, Loxn;->a()I

    .line 1907
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1912
    :cond_3
    iget-object v2, p0, Llxe;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 1916
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1917
    iget-object v0, p0, Llxe;->c:[Llxz;

    if-nez v0, :cond_5

    move v0, v1

    .line 1918
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Llxz;

    .line 1919
    iget-object v3, p0, Llxe;->c:[Llxz;

    if-eqz v3, :cond_4

    .line 1920
    iget-object v3, p0, Llxe;->c:[Llxz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1922
    :cond_4
    iput-object v2, p0, Llxe;->c:[Llxz;

    .line 1923
    :goto_4
    iget-object v2, p0, Llxe;->c:[Llxz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1924
    iget-object v2, p0, Llxe;->c:[Llxz;

    new-instance v3, Llxz;

    invoke-direct {v3}, Llxz;-><init>()V

    aput-object v3, v2, v0

    .line 1925
    iget-object v2, p0, Llxe;->c:[Llxz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1926
    invoke-virtual {p1}, Loxn;->a()I

    .line 1923
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1917
    :cond_5
    iget-object v0, p0, Llxe;->c:[Llxz;

    array-length v0, v0

    goto :goto_3

    .line 1929
    :cond_6
    iget-object v2, p0, Llxe;->c:[Llxz;

    new-instance v3, Llxz;

    invoke-direct {v3}, Llxz;-><init>()V

    aput-object v3, v2, v0

    .line 1930
    iget-object v2, p0, Llxe;->c:[Llxz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1873
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1812
    iget-object v1, p0, Llxe;->a:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1813
    iget-object v2, p0, Llxe;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1814
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1813
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1817
    :cond_0
    iget-object v1, p0, Llxe;->b:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1818
    iget-object v2, p0, Llxe;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1819
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 1818
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1822
    :cond_1
    iget-object v1, p0, Llxe;->c:[Llxz;

    if-eqz v1, :cond_3

    .line 1823
    iget-object v1, p0, Llxe;->c:[Llxz;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1824
    if-eqz v3, :cond_2

    .line 1825
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1823
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1829
    :cond_3
    iget-object v0, p0, Llxe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1831
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1794
    invoke-virtual {p0, p1}, Llxe;->a(Loxn;)Llxe;

    move-result-object v0

    return-object v0
.end method
