.class public final Lohz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lohz;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2452
    const/4 v0, 0x0

    new-array v0, v0, [Lohz;

    sput-object v0, Lohz;->a:[Lohz;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2453
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 2469
    const/4 v0, 0x0

    .line 2470
    iget-object v1, p0, Lohz;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2471
    const/4 v0, 0x1

    iget-object v1, p0, Lohz;->b:Ljava/lang/String;

    .line 2472
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2474
    :cond_0
    iget-object v1, p0, Lohz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2475
    iput v0, p0, Lohz;->ai:I

    .line 2476
    return v0
.end method

.method public a(Loxn;)Lohz;
    .locals 2

    .prologue
    .line 2484
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2485
    sparse-switch v0, :sswitch_data_0

    .line 2489
    iget-object v1, p0, Lohz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2490
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohz;->ah:Ljava/util/List;

    .line 2493
    :cond_1
    iget-object v1, p0, Lohz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2495
    :sswitch_0
    return-object p0

    .line 2500
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohz;->b:Ljava/lang/String;

    goto :goto_0

    .line 2485
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2460
    iget-object v0, p0, Lohz;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2461
    const/4 v0, 0x1

    iget-object v1, p0, Lohz;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2463
    :cond_0
    iget-object v0, p0, Lohz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2465
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2449
    invoke-virtual {p0, p1}, Lohz;->a(Loxn;)Lohz;

    move-result-object v0

    return-object v0
.end method
