.class public final Lgbd;
.super Lgbz;
.source "PG"


# instance fields
.field private A:Z

.field private B:Lgbc;

.field private C:Lkzy;

.field private D:I

.field private y:Lgbe;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgbd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgbd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    new-instance v0, Lgbe;

    invoke-direct {v0, p0, p1, p2, p3}, Lgbe;-><init>(Lgbd;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lgbd;->y:Lgbe;

    .line 47
    iget-object v0, p0, Lgbd;->y:Lgbe;

    invoke-virtual {v0, p0}, Lgbe;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method

.method static synthetic a(Lgbd;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lgbd;->A:Z

    return v0
.end method

.method static synthetic b(Lgbd;)Lkzy;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lgbd;->C:Lkzy;

    return-object v0
.end method


# virtual methods
.method protected a(III)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 123
    const/high16 v0, -0x80000000

    invoke-static {p3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 124
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 125
    iget-object v2, p0, Lgbd;->y:Lgbe;

    invoke-virtual {v2, v0, v1}, Lgbe;->measure(II)V

    .line 126
    iget-object v0, p0, Lgbd;->y:Lgbe;

    invoke-virtual {v0}, Lgbe;->getMeasuredHeight()I

    move-result v0

    .line 127
    iget-object v1, p0, Lgbd;->y:Lgbe;

    add-int v2, p1, p3

    add-int v3, p2, v0

    invoke-virtual {v1, p1, p2, v2, v3}, Lgbe;->layout(IIII)V

    .line 128
    add-int/2addr v0, p2

    .line 129
    iget-object v1, p0, Lgbd;->B:Lgbc;

    if-eqz v1, :cond_0

    .line 130
    iput v0, p0, Lgbd;->D:I

    .line 131
    iget-object v1, p0, Lgbd;->B:Lgbc;

    iget-object v2, p0, Lgbd;->C:Lkzy;

    iget-object v3, p0, Lgbd;->r:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lgbc;->a(Lkzy;Ljava/lang/String;)V

    .line 132
    iget-object v1, p0, Lgbd;->B:Lgbc;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 133
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 132
    invoke-virtual {v1, v2, v3}, Lgbc;->measure(II)V

    .line 134
    iget-object v1, p0, Lgbd;->B:Lgbc;

    invoke-virtual {p0, v1}, Lgbd;->addView(Landroid/view/View;)V

    .line 135
    iget-object v1, p0, Lgbd;->B:Lgbc;

    invoke-virtual {v1}, Lgbc;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_0
    return v0
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lgbd;->y:Lgbe;

    invoke-virtual {v0}, Lgbe;->getHeight()I

    move-result v0

    add-int/2addr v0, p2

    .line 153
    iget-object v1, p0, Lgbd;->B:Lgbc;

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lgbd;->B:Lgbc;

    invoke-virtual {v1}, Lgbc;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    :cond_0
    return v0
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 2

    .prologue
    .line 162
    invoke-super/range {p0 .. p6}, Lgbz;->a(Landroid/graphics/Canvas;IIIII)I

    move-result v0

    .line 163
    iget-object v1, p0, Lgbd;->B:Lgbc;

    if-eqz v1, :cond_0

    .line 164
    iget-object v1, p0, Lgbd;->B:Lgbc;

    invoke-virtual {v1}, Lgbc;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    :cond_0
    return v0
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 196
    invoke-super {p0}, Lgbz;->a()V

    .line 197
    iget-object v0, p0, Lgbd;->y:Lgbe;

    invoke-virtual {v0}, Lgbe;->a()V

    .line 199
    iput-boolean v1, p0, Lgbd;->A:Z

    .line 200
    iget-object v0, p0, Lgbd;->B:Lgbc;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lgbd;->B:Lgbc;

    invoke-virtual {v0}, Lgbc;->a()V

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lgbd;->B:Lgbc;

    .line 204
    :cond_0
    iput v1, p0, Lgbd;->D:I

    .line 205
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 88
    const/16 v0, 0x17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 89
    invoke-static {v0}, Lkzy;->a([B)Lkzy;

    move-result-object v0

    iput-object v0, p0, Lgbd;->C:Lkzy;

    .line 90
    return-void
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 171
    new-array v0, v3, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lgbd;->C:Lkzy;

    invoke-virtual {v1}, Lkzy;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 172
    new-array v0, v3, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lgbd;->C:Lkzy;

    invoke-virtual {v1}, Lkzy;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 173
    new-array v0, v3, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lgbd;->C:Lkzy;

    invoke-virtual {v1}, Lkzy;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 174
    return-void
.end method

.method protected a(ZIIII)V
    .locals 6

    .prologue
    .line 142
    invoke-super/range {p0 .. p5}, Lgbz;->a(ZIIII)V

    .line 143
    iget-object v0, p0, Lgbd;->B:Lgbc;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lgbd;->B:Lgbc;

    iget-object v1, p0, Lgbd;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lgbd;->D:I

    iget-object v3, p0, Lgbd;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lgbd;->t:I

    add-int/2addr v3, v4

    iget v4, p0, Lgbd;->D:I

    iget-object v5, p0, Lgbd;->B:Lgbc;

    .line 146
    invoke-virtual {v5}, Lgbc;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 144
    invoke-virtual {v0, v1, v2, v3, v4}, Lgbc;->layout(IIII)V

    .line 148
    :cond_0
    return-void
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x1

    return v0
.end method

.method protected a_(Landroid/database/Cursor;Llcr;I)V
    .locals 4

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3}, Lgbz;->a_(Landroid/database/Cursor;Llcr;I)V

    .line 96
    iget-object v0, p0, Lgbd;->y:Lgbe;

    invoke-virtual {p0, v0}, Lgbd;->removeView(Landroid/view/View;)V

    .line 97
    iget-object v0, p0, Lgbd;->y:Lgbe;

    invoke-virtual {p0, v0}, Lgbd;->addView(Landroid/view/View;)V

    .line 98
    iget-object v0, p0, Lgbd;->C:Lkzy;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lgbd;->C:Lkzy;

    invoke-virtual {v0}, Lkzy;->g()Z

    move-result v0

    iput-boolean v0, p0, Lgbd;->A:Z

    .line 100
    iget-boolean v0, p0, Lgbd;->A:Z

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lgbc;

    invoke-virtual {p0}, Lgbd;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgbc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgbd;->B:Lgbc;

    .line 104
    :cond_0
    iget-object v0, p0, Lgbd;->C:Lkzy;

    invoke-virtual {v0}, Lkzy;->d()Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 106
    iget-object v1, p0, Lgbd;->y:Lgbe;

    .line 107
    invoke-virtual {p0}, Lgbd;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v2, v0, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    .line 106
    invoke-virtual {v1, v0}, Lgbe;->a(Lizu;)V

    .line 111
    :cond_1
    iget-object v0, p0, Lgbd;->y:Lgbe;

    invoke-virtual {v0}, Lgbe;->h()Lizu;

    move-result-object v0

    if-eqz v0, :cond_2

    iget v0, p0, Lgbd;->o:I

    .line 112
    invoke-virtual {p0, p2, v0}, Lgbd;->a(Lhuk;I)I

    move-result v0

    .line 113
    :goto_0
    iget-object v1, p0, Lgbd;->y:Lgbe;

    invoke-virtual {v1, v0}, Lgbe;->a(I)V

    .line 114
    return-void

    .line 112
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 178
    invoke-super {p0}, Lgbz;->b()V

    .line 179
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lgbd;->y:Lgbe;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lgbd;->y:Lgbe;

    invoke-virtual {v0}, Lgbe;->b()V

    .line 184
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lgbd;->y:Lgbe;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lgbd;->y:Lgbe;

    invoke-virtual {v0}, Lgbe;->c()V

    .line 191
    :cond_0
    invoke-super {p0}, Lgbz;->c()V

    .line 192
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lgbd;->y:Lgbe;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lgbd;->i:Lfdp;

    if-nez v0, :cond_2

    .line 210
    :cond_0
    invoke-super {p0, p1}, Lgbz;->onClick(Landroid/view/View;)V

    .line 218
    :cond_1
    :goto_0
    return-void

    .line 214
    :cond_2
    iget-object v0, p0, Lgbd;->C:Lkzy;

    invoke-virtual {v0}, Lkzy;->e()Ljava/lang/String;

    move-result-object v0

    .line 215
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 216
    iget-object v1, p0, Lgbd;->i:Lfdp;

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lfdp;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
