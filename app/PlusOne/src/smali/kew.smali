.class final Lkew;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;II)Lkev;
    .locals 4

    .prologue
    .line 61
    new-instance v0, Lkev;

    invoke-direct {v0}, Lkev;-><init>()V

    const v1, 0x7f0c0066

    .line 62
    invoke-direct {p0, p1, v1, p2}, Lkew;->b(Landroid/content/Context;II)I

    move-result v1

    invoke-virtual {v0, v1}, Lkev;->a(I)Lkev;

    move-result-object v0

    const v1, 0x7f0c0067

    .line 64
    invoke-direct {p0, p1, v1, p3}, Lkew;->b(Landroid/content/Context;II)I

    move-result v1

    invoke-virtual {v0, v1}, Lkev;->c(I)Lkev;

    move-result-object v0

    const v1, 0x7f0c0068

    .line 66
    invoke-direct {p0, p1, v1, p3}, Lkew;->b(Landroid/content/Context;II)I

    move-result v1

    invoke-virtual {v0, v1}, Lkev;->b(I)Lkev;

    move-result-object v0

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0}, Lkev;->b()I

    move-result v2

    if-le v2, v1, :cond_0

    int-to-float v1, v1

    invoke-virtual {v0}, Lkev;->b()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0}, Lkev;->a()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Lkev;->b(I)Lkev;

    .line 70
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 71
    const v2, 0x7f0c0069

    .line 72
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lkev;->b(J)Lkev;

    move-result-object v0

    const v2, 0x7f0c006a

    .line 74
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    .line 73
    invoke-virtual {v0, v2, v3}, Lkev;->a(J)Lkev;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;II)I
    .locals 6

    .prologue
    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 80
    invoke-static {p1}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 81
    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v0, v2

    shl-int/lit8 v2, v0, 0x2

    .line 82
    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 83
    if-nez v2, :cond_0

    .line 89
    :goto_0
    return v0

    .line 87
    :cond_0
    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 88
    int-to-long v4, v1

    int-to-long v2, v2

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x64

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 89
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Lkeu;
    .locals 1

    .prologue
    .line 20
    invoke-static {p1}, Llsg;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lkew;->a:I

    .line 21
    invoke-virtual {p0, p1}, Lkew;->b(Landroid/content/Context;)Lkeu;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Lkeu;
    .locals 5

    .prologue
    const v4, 0x7f0c0061

    .line 26
    iget v0, p0, Lkew;->a:I

    const/16 v1, 0x30

    if-ge v0, v1, :cond_0

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Lkev;

    invoke-direct {v1}, Lkev;-><init>()V

    const v2, 0x7f0c005e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lkev;->a(I)Lkev;

    move-result-object v1

    const v2, 0x7f0c005f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lkev;->c(I)Lkev;

    move-result-object v1

    const v2, 0x7f0c0060

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lkev;->b(I)Lkev;

    move-result-object v1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Lkev;->b(J)Lkev;

    move-result-object v1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Lkev;->a(J)Lkev;

    move-result-object v0

    .line 33
    :goto_0
    const/4 v1, 0x1

    .line 34
    invoke-virtual {v0, v1}, Lkev;->a(Z)Lkev;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lkev;->c()Lkeu;

    move-result-object v0

    return-object v0

    .line 28
    :cond_0
    iget v0, p0, Lkew;->a:I

    const/16 v1, 0x40

    if-ge v0, v1, :cond_1

    .line 29
    const v0, 0x7f0c0062

    const v1, 0x7f0c0063

    invoke-direct {p0, p1, v0, v1}, Lkew;->a(Landroid/content/Context;II)Lkev;

    move-result-object v0

    goto :goto_0

    .line 31
    :cond_1
    const v0, 0x7f0c0064

    const v1, 0x7f0c0065

    invoke-direct {p0, p1, v0, v1}, Lkew;->a(Landroid/content/Context;II)Lkev;

    move-result-object v0

    goto :goto_0
.end method
