.class public final Lmtw;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/String;

.field private e:Lmwl;

.field private f:Lmwy;

.field private g:Lmwk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6568
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6579
    iput-object v0, p0, Lmtw;->e:Lmwl;

    .line 6582
    iput-object v0, p0, Lmtw;->f:Lmwy;

    .line 6585
    iput-object v0, p0, Lmtw;->g:Lmwk;

    .line 6568
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6617
    const/4 v0, 0x0

    .line 6618
    iget-object v1, p0, Lmtw;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 6619
    const/4 v0, 0x1

    iget-object v1, p0, Lmtw;->a:Ljava/lang/Integer;

    .line 6620
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6622
    :cond_0
    iget-object v1, p0, Lmtw;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 6623
    const/4 v1, 0x2

    iget-object v2, p0, Lmtw;->b:Ljava/lang/Integer;

    .line 6624
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6626
    :cond_1
    iget-object v1, p0, Lmtw;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 6627
    const/4 v1, 0x3

    iget-object v2, p0, Lmtw;->c:Ljava/lang/Integer;

    .line 6628
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6630
    :cond_2
    iget-object v1, p0, Lmtw;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 6631
    const/4 v1, 0x4

    iget-object v2, p0, Lmtw;->d:Ljava/lang/String;

    .line 6632
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6634
    :cond_3
    iget-object v1, p0, Lmtw;->e:Lmwl;

    if-eqz v1, :cond_4

    .line 6635
    const/4 v1, 0x5

    iget-object v2, p0, Lmtw;->e:Lmwl;

    .line 6636
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6638
    :cond_4
    iget-object v1, p0, Lmtw;->f:Lmwy;

    if-eqz v1, :cond_5

    .line 6639
    const/4 v1, 0x6

    iget-object v2, p0, Lmtw;->f:Lmwy;

    .line 6640
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6642
    :cond_5
    iget-object v1, p0, Lmtw;->g:Lmwk;

    if-eqz v1, :cond_6

    .line 6643
    const/4 v1, 0x7

    iget-object v2, p0, Lmtw;->g:Lmwk;

    .line 6644
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6646
    :cond_6
    iget-object v1, p0, Lmtw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6647
    iput v0, p0, Lmtw;->ai:I

    .line 6648
    return v0
.end method

.method public a(Loxn;)Lmtw;
    .locals 2

    .prologue
    .line 6656
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6657
    sparse-switch v0, :sswitch_data_0

    .line 6661
    iget-object v1, p0, Lmtw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6662
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtw;->ah:Ljava/util/List;

    .line 6665
    :cond_1
    iget-object v1, p0, Lmtw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6667
    :sswitch_0
    return-object p0

    .line 6672
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtw;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 6676
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtw;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 6680
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtw;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 6684
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtw;->d:Ljava/lang/String;

    goto :goto_0

    .line 6688
    :sswitch_5
    iget-object v0, p0, Lmtw;->e:Lmwl;

    if-nez v0, :cond_2

    .line 6689
    new-instance v0, Lmwl;

    invoke-direct {v0}, Lmwl;-><init>()V

    iput-object v0, p0, Lmtw;->e:Lmwl;

    .line 6691
    :cond_2
    iget-object v0, p0, Lmtw;->e:Lmwl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6695
    :sswitch_6
    iget-object v0, p0, Lmtw;->f:Lmwy;

    if-nez v0, :cond_3

    .line 6696
    new-instance v0, Lmwy;

    invoke-direct {v0}, Lmwy;-><init>()V

    iput-object v0, p0, Lmtw;->f:Lmwy;

    .line 6698
    :cond_3
    iget-object v0, p0, Lmtw;->f:Lmwy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6702
    :sswitch_7
    iget-object v0, p0, Lmtw;->g:Lmwk;

    if-nez v0, :cond_4

    .line 6703
    new-instance v0, Lmwk;

    invoke-direct {v0}, Lmwk;-><init>()V

    iput-object v0, p0, Lmtw;->g:Lmwk;

    .line 6705
    :cond_4
    iget-object v0, p0, Lmtw;->g:Lmwk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6657
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6590
    iget-object v0, p0, Lmtw;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 6591
    const/4 v0, 0x1

    iget-object v1, p0, Lmtw;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6593
    :cond_0
    iget-object v0, p0, Lmtw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 6594
    const/4 v0, 0x2

    iget-object v1, p0, Lmtw;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6596
    :cond_1
    iget-object v0, p0, Lmtw;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 6597
    const/4 v0, 0x3

    iget-object v1, p0, Lmtw;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6599
    :cond_2
    iget-object v0, p0, Lmtw;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 6600
    const/4 v0, 0x4

    iget-object v1, p0, Lmtw;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6602
    :cond_3
    iget-object v0, p0, Lmtw;->e:Lmwl;

    if-eqz v0, :cond_4

    .line 6603
    const/4 v0, 0x5

    iget-object v1, p0, Lmtw;->e:Lmwl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6605
    :cond_4
    iget-object v0, p0, Lmtw;->f:Lmwy;

    if-eqz v0, :cond_5

    .line 6606
    const/4 v0, 0x6

    iget-object v1, p0, Lmtw;->f:Lmwy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6608
    :cond_5
    iget-object v0, p0, Lmtw;->g:Lmwk;

    if-eqz v0, :cond_6

    .line 6609
    const/4 v0, 0x7

    iget-object v1, p0, Lmtw;->g:Lmwk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6611
    :cond_6
    iget-object v0, p0, Lmtw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6613
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6564
    invoke-virtual {p0, p1}, Lmtw;->a(Loxn;)Lmtw;

    move-result-object v0

    return-object v0
.end method
