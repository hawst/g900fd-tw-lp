.class public final Lmyp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmyk;

.field public b:Lmyo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1402
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1405
    iput-object v0, p0, Lmyp;->a:Lmyk;

    .line 1408
    iput-object v0, p0, Lmyp;->b:Lmyo;

    .line 1402
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1425
    const/4 v0, 0x0

    .line 1426
    iget-object v1, p0, Lmyp;->a:Lmyk;

    if-eqz v1, :cond_0

    .line 1427
    const/4 v0, 0x1

    iget-object v1, p0, Lmyp;->a:Lmyk;

    .line 1428
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1430
    :cond_0
    iget-object v1, p0, Lmyp;->b:Lmyo;

    if-eqz v1, :cond_1

    .line 1431
    const/4 v1, 0x2

    iget-object v2, p0, Lmyp;->b:Lmyo;

    .line 1432
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1434
    :cond_1
    iget-object v1, p0, Lmyp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1435
    iput v0, p0, Lmyp;->ai:I

    .line 1436
    return v0
.end method

.method public a(Loxn;)Lmyp;
    .locals 2

    .prologue
    .line 1444
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1445
    sparse-switch v0, :sswitch_data_0

    .line 1449
    iget-object v1, p0, Lmyp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1450
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyp;->ah:Ljava/util/List;

    .line 1453
    :cond_1
    iget-object v1, p0, Lmyp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1455
    :sswitch_0
    return-object p0

    .line 1460
    :sswitch_1
    iget-object v0, p0, Lmyp;->a:Lmyk;

    if-nez v0, :cond_2

    .line 1461
    new-instance v0, Lmyk;

    invoke-direct {v0}, Lmyk;-><init>()V

    iput-object v0, p0, Lmyp;->a:Lmyk;

    .line 1463
    :cond_2
    iget-object v0, p0, Lmyp;->a:Lmyk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1467
    :sswitch_2
    iget-object v0, p0, Lmyp;->b:Lmyo;

    if-nez v0, :cond_3

    .line 1468
    new-instance v0, Lmyo;

    invoke-direct {v0}, Lmyo;-><init>()V

    iput-object v0, p0, Lmyp;->b:Lmyo;

    .line 1470
    :cond_3
    iget-object v0, p0, Lmyp;->b:Lmyo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1445
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1413
    iget-object v0, p0, Lmyp;->a:Lmyk;

    if-eqz v0, :cond_0

    .line 1414
    const/4 v0, 0x1

    iget-object v1, p0, Lmyp;->a:Lmyk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1416
    :cond_0
    iget-object v0, p0, Lmyp;->b:Lmyo;

    if-eqz v0, :cond_1

    .line 1417
    const/4 v0, 0x2

    iget-object v1, p0, Lmyp;->b:Lmyo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1419
    :cond_1
    iget-object v0, p0, Lmyp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1421
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1398
    invoke-virtual {p0, p1}, Lmyp;->a(Loxn;)Lmyp;

    move-result-object v0

    return-object v0
.end method
