.class public final Lmgn;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lndh;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28510
    invoke-direct {p0}, Loxq;-><init>()V

    .line 28513
    iput-object v0, p0, Lmgn;->apiHeader:Llyr;

    .line 28516
    iput-object v0, p0, Lmgn;->a:Lndh;

    .line 28510
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 28533
    const/4 v0, 0x0

    .line 28534
    iget-object v1, p0, Lmgn;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 28535
    const/4 v0, 0x1

    iget-object v1, p0, Lmgn;->apiHeader:Llyr;

    .line 28536
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 28538
    :cond_0
    iget-object v1, p0, Lmgn;->a:Lndh;

    if-eqz v1, :cond_1

    .line 28539
    const/4 v1, 0x2

    iget-object v2, p0, Lmgn;->a:Lndh;

    .line 28540
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28542
    :cond_1
    iget-object v1, p0, Lmgn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 28543
    iput v0, p0, Lmgn;->ai:I

    .line 28544
    return v0
.end method

.method public a(Loxn;)Lmgn;
    .locals 2

    .prologue
    .line 28552
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 28553
    sparse-switch v0, :sswitch_data_0

    .line 28557
    iget-object v1, p0, Lmgn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 28558
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgn;->ah:Ljava/util/List;

    .line 28561
    :cond_1
    iget-object v1, p0, Lmgn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28563
    :sswitch_0
    return-object p0

    .line 28568
    :sswitch_1
    iget-object v0, p0, Lmgn;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 28569
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgn;->apiHeader:Llyr;

    .line 28571
    :cond_2
    iget-object v0, p0, Lmgn;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28575
    :sswitch_2
    iget-object v0, p0, Lmgn;->a:Lndh;

    if-nez v0, :cond_3

    .line 28576
    new-instance v0, Lndh;

    invoke-direct {v0}, Lndh;-><init>()V

    iput-object v0, p0, Lmgn;->a:Lndh;

    .line 28578
    :cond_3
    iget-object v0, p0, Lmgn;->a:Lndh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 28553
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28521
    iget-object v0, p0, Lmgn;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 28522
    const/4 v0, 0x1

    iget-object v1, p0, Lmgn;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28524
    :cond_0
    iget-object v0, p0, Lmgn;->a:Lndh;

    if-eqz v0, :cond_1

    .line 28525
    const/4 v0, 0x2

    iget-object v1, p0, Lmgn;->a:Lndh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 28527
    :cond_1
    iget-object v0, p0, Lmgn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 28529
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 28506
    invoke-virtual {p0, p1}, Lmgn;->a(Loxn;)Lmgn;

    move-result-object v0

    return-object v0
.end method
