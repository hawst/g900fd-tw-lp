.class public final Lejy;
.super Leku;
.source "PG"

# interfaces
.implements Lhob;


# static fields
.field public static final N:Lief;


# instance fields
.field O:[Lois;

.field private ad:Lekc;

.field private ae:Lhoc;

.field private af:Lekb;

.field private ag:I

.field private ah:Z

.field private final ai:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Lojw;",
            ">;"
        }
    .end annotation
.end field

.field private final aj:Lhkd;

.field private final ak:Lhke;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 192
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_save_interest"

    const-string v2, "false"

    const-string v3, "f1873483"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v2, v3, v4}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lejy;->N:Lief;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 363
    invoke-direct {p0}, Leku;-><init>()V

    .line 182
    new-instance v0, Lhmg;

    sget-object v1, Lona;->e:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lejy;->au:Llnh;

    .line 183
    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 185
    new-instance v0, Lhmf;

    iget-object v1, p0, Lejy;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 204
    new-instance v0, Lekb;

    iget-object v1, p0, Lejy;->av:Llqm;

    invoke-direct {v0, v1}, Lekb;-><init>(Llqr;)V

    iput-object v0, p0, Lejy;->af:Lekb;

    .line 206
    const/4 v0, -0x1

    iput v0, p0, Lejy;->ag:I

    .line 210
    new-instance v0, Lejz;

    invoke-direct {v0, p0}, Lejz;-><init>(Lejy;)V

    iput-object v0, p0, Lejy;->ai:Lbc;

    .line 234
    new-instance v0, Leka;

    invoke-direct {v0, p0}, Leka;-><init>(Lejy;)V

    iput-object v0, p0, Lejy;->aj:Lhkd;

    .line 251
    new-instance v0, Lhke;

    iget-object v1, p0, Lejy;->av:Llqm;

    invoke-direct {v0, v1}, Lhke;-><init>(Llqr;)V

    iget-object v1, p0, Lejy;->au:Llnh;

    .line 253
    invoke-virtual {v0, v1}, Lhke;->a(Llnh;)Lhke;

    move-result-object v0

    const v1, 0x7f1000c7

    iget-object v2, p0, Lejy;->aj:Lhkd;

    .line 254
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    iput-object v0, p0, Lejy;->ak:Lhke;

    .line 364
    return-void
.end method

.method private X()Ljava/lang/String;
    .locals 3

    .prologue
    .line 725
    iget-object v0, p0, Lejy;->at:Llnl;

    iget-object v1, p0, Lejy;->T:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Ldsm;->a(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lejy;I)I
    .locals 0

    .prologue
    .line 85
    iput p1, p0, Lejy;->ag:I

    return p1
.end method

.method private a(Loiu;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Loiu;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 558
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 560
    if-eqz p1, :cond_0

    iget-object v1, p1, Loiu;->b:Lohv;

    if-eqz v1, :cond_0

    iget-object v1, p1, Loiu;->b:Lohv;

    iget-object v1, v1, Lohv;->d:[Loij;

    if-nez v1, :cond_1

    .line 567
    :cond_0
    return-object v0

    .line 564
    :cond_1
    iget-object v1, p1, Loiu;->b:Lohv;

    iget-object v2, v1, Lohv;->d:[Loij;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 565
    iget-object v4, v4, Loij;->b:Lohn;

    iget-object v4, v4, Lohn;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 564
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic a(Lejy;)Llnl;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lejy;->at:Llnl;

    return-object v0
.end method

.method private a(Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Lois;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;",
            "Lois;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 496
    .line 497
    iget-object v0, p0, Lejy;->af:Lekb;

    iget v1, p2, Lois;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lekb;->a(Ljava/lang/Integer;Ljava/util/ArrayList;)V

    .line 499
    invoke-direct {p0, p1, v2}, Lejy;->a(Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Z)V

    .line 501
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0, v2}, Lejy;->a(Lois;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    .line 502
    return-void
.end method

.method private a(Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 624
    if-nez p1, :cond_0

    .line 625
    iget-object v0, p0, Lejy;->ad:Lekc;

    invoke-virtual {v0}, Lekc;->notifyDataSetChanged()V

    .line 655
    :goto_0
    return-void

    .line 629
    :cond_0
    const v0, 0x7f1000ae

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 630
    iget-object v1, p0, Lejy;->O:[Lois;

    aget-object v0, v1, v0

    .line 632
    if-eqz p2, :cond_1

    .line 633
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 634
    iget-object v1, p0, Lejy;->at:Llnl;

    invoke-static {v1, v4}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/util/List;)V

    .line 636
    new-instance v0, Lhmk;

    sget-object v1, Lona;->k:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-static {p1, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 638
    invoke-virtual {p1, v4}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setEnabled(Z)V

    goto :goto_0

    .line 640
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/util/List;)V

    .line 642
    invoke-direct {p0, v0}, Lejy;->b(Lois;)I

    move-result v0

    .line 643
    if-ne v0, v5, :cond_2

    .line 644
    iget-object v0, p0, Lejy;->at:Llnl;

    const v1, 0x7f0a09cd

    invoke-virtual {v0, v1}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 650
    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->a(Ljava/lang/String;)V

    .line 651
    new-instance v0, Lhmk;

    sget-object v1, Lona;->r:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-static {p1, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 653
    invoke-virtual {p1, v5}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->setEnabled(Z)V

    goto :goto_0

    .line 645
    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 646
    iget-object v0, p0, Lejy;->at:Llnl;

    const v1, 0x7f0a09ce

    invoke-virtual {v0, v1}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 648
    :cond_3
    iget-object v1, p0, Lejy;->at:Llnl;

    const v2, 0x7f0a09cf

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Llnl;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(Lejy;Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Lois;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3}, Lejy;->a(Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Lois;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic a(Lejy;Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Z)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lejy;->a(Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Z)V

    return-void
.end method

.method private a(Lois;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lois;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 523
    iget-object v0, p0, Lejy;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 524
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1}, Lejy;->b(Lois;)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v4, p1, Lois;->d:[Loiu;

    aget-object v4, v4, v0

    iget-object v5, v4, Loiu;->b:Lohv;

    iget-object v5, v5, Lohv;->b:Lohp;

    iget-object v5, v5, Lohp;->d:Ljava/lang/String;

    invoke-static {v5}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v4, v4, Loiu;->b:Lohv;

    iget-object v4, v4, Lohv;->c:Lohq;

    iget-object v4, v4, Lohq;->a:Ljava/lang/String;

    new-instance v6, Ldli;

    invoke-direct {v6, v5, v4}, Ldli;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 525
    :cond_0
    new-instance v0, Ldpd;

    iget-object v1, p0, Lejy;->at:Llnl;

    .line 526
    invoke-virtual {p0}, Lejy;->Z()I

    move-result v4

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Ldpd;-><init>(Landroid/content/Context;ILjava/util/ArrayList;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 527
    iget-object v1, p0, Lejy;->ae:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 530
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldli;

    .line 531
    invoke-virtual {p0}, Lejy;->ak_()Ldid;

    move-result-object v9

    .line 532
    new-instance v3, Ldib;

    iget-object v4, p0, Lejy;->at:Llnl;

    iget-object v5, p0, Lejy;->au:Llnh;

    const-class v6, Lhms;

    invoke-virtual {v5, v6}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lhms;

    .line 533
    invoke-virtual {v0}, Ldli;->a()Ljava/lang/String;

    move-result-object v6

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v3 .. v9}, Ldib;-><init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V

    iput-object v3, p0, Lejy;->aa:Ldib;

    goto :goto_1

    .line 537
    :cond_1
    iget-object v0, p0, Lejy;->at:Llnl;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 538
    sget-object v1, Lejy;->N:Lief;

    invoke-interface {v0, v1, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 539
    new-instance v0, Ldqb;

    iget-object v1, p0, Lejy;->at:Llnl;

    iget v3, p1, Lois;->b:I

    invoke-direct {v0, v1, v2, v3, p4}, Ldqb;-><init>(Landroid/content/Context;IIZ)V

    .line 541
    iget-object v1, p0, Lejy;->ae:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 543
    :cond_2
    return-void
.end method

.method static synthetic a(Lejy;Lois;)Z
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lejy;->a(Lois;)Z

    move-result v0

    return v0
.end method

.method private a(Lois;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 591
    iget-object v0, p0, Lejy;->af:Lekb;

    iget v3, p1, Lois;->b:I

    invoke-virtual {v0, v3}, Lekb;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 592
    iget-object v0, p0, Lejy;->af:Lekb;

    iget v3, p1, Lois;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lekb;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v1, v2

    .line 620
    :cond_0
    :goto_0
    return v1

    .line 595
    :cond_1
    invoke-direct {p0}, Lejy;->X()Ljava/lang/String;

    move-result-object v4

    .line 596
    if-nez v4, :cond_6

    .line 599
    iget-object v0, p0, Lejy;->af:Lekb;

    iget v3, p1, Lois;->b:I

    invoke-virtual {v0, v3}, Lekb;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lejy;->af:Lekb;

    iget v3, p1, Lois;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lekb;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v4, p1, Lois;->d:[Loiu;

    array-length v0, v4

    if-nez v0, :cond_4

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    goto :goto_1

    :cond_4
    aget-object v0, v4, v1

    invoke-direct {p0, v0}, Lejy;->a(Loiu;)Ljava/util/ArrayList;

    move-result-object v0

    array-length v5, v4

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    if-eqz v6, :cond_5

    invoke-direct {p0, v6}, Lejy;->a(Loiu;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->retainAll(Ljava/util/Collection;)Z

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    move v0, v1

    .line 601
    :goto_3
    invoke-direct {p0, p1}, Lejy;->b(Lois;)I

    move-result v3

    if-ge v0, v3, :cond_8

    .line 602
    iget-object v3, p1, Lois;->d:[Loiu;

    aget-object v3, v3, v0

    .line 605
    iget-object v5, v3, Loiu;->b:Lohv;

    iget-object v5, v5, Lohv;->d:[Loij;

    if-eqz v5, :cond_9

    .line 606
    iget-object v3, v3, Loiu;->b:Lohv;

    iget-object v5, v3, Lohv;->d:[Loij;

    array-length v6, v5

    move v3, v1

    :goto_4
    if-ge v3, v6, :cond_9

    aget-object v7, v5, v3

    .line 607
    iget-object v7, v7, Loij;->b:Lohn;

    iget-object v7, v7, Lohn;->c:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    move v3, v2

    .line 614
    :goto_5
    if-eqz v3, :cond_0

    .line 601
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 606
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_8
    move v1, v2

    .line 620
    goto :goto_0

    :cond_9
    move v3, v1

    goto :goto_5
.end method

.method private b(Lois;)I
    .locals 2

    .prologue
    .line 709
    iget-object v0, p1, Lois;->d:[Loiu;

    array-length v0, v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lejy;)Llnl;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lejy;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Lejy;)Llnl;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lejy;->at:Llnl;

    return-object v0
.end method

.method static synthetic d(Lejy;)I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lejy;->ag:I

    return v0
.end method


# virtual methods
.method public V()V
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lejy;->O:[Lois;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lejy;->ah:Z

    if-nez v0, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    iget-object v0, p0, Lejy;->af:Lekb;

    iget-object v1, p0, Lejy;->O:[Lois;

    invoke-virtual {v0, v1}, Lekb;->a([Lois;)V

    .line 411
    iget-object v0, p0, Lejy;->ad:Lekc;

    iget-object v1, p0, Lejy;->O:[Lois;

    invoke-virtual {v0, v1}, Lekc;->a([Lois;)V

    .line 412
    iget-object v0, p0, Lejy;->ad:Lekc;

    invoke-virtual {v0}, Lekc;->notifyDataSetChanged()V

    .line 413
    iget-object v0, p0, Lejy;->Q:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 414
    invoke-virtual {p0}, Lejy;->x()Landroid/view/View;

    invoke-virtual {p0}, Lejy;->aa()V

    goto :goto_0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 671
    const/16 v0, 0x2c

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 387
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 388
    invoke-virtual {p0, v0}, Lejy;->d(Landroid/view/View;)Landroid/view/View;

    .line 389
    iget-object v1, p0, Lejy;->ad:Lekc;

    invoke-virtual {p0, v1}, Lejy;->a(Landroid/widget/ListAdapter;)V

    .line 390
    return-object v0
.end method

.method protected a(II)Ldid;
    .locals 1

    .prologue
    .line 677
    invoke-virtual {p0}, Lejy;->ak_()Ldid;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 374
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 376
    if-eqz p1, :cond_0

    .line 377
    const-string v0, "last_rendered_card"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lejy;->ag:I

    .line 380
    :cond_0
    new-instance v0, Lekc;

    invoke-direct {v0, p0}, Lekc;-><init>(Lejy;)V

    iput-object v0, p0, Lejy;->ad:Lekc;

    .line 381
    iget-object v0, p0, Lejy;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lejy;->ae:Lhoc;

    .line 382
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 734
    invoke-super {p0, p1, p2}, Leku;->a(Ldo;Landroid/database/Cursor;)V

    .line 735
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 746
    :goto_0
    return-void

    .line 737
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 740
    :goto_1
    iput-boolean v1, p0, Lejy;->ah:Z

    .line 742
    new-instance v1, Lknx;

    invoke-direct {v1, v0}, Lknx;-><init>(I)V

    iget-object v0, p0, Lejy;->at:Llnl;

    invoke-virtual {v1, v0}, Lknx;->a(Landroid/content/Context;)V

    .line 743
    invoke-virtual {p0}, Lejy;->V()V

    goto :goto_0

    .line 737
    :cond_1
    const/4 v0, 0x2

    goto :goto_1

    .line 735
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 85
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lejy;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 1

    .prologue
    .line 419
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 420
    const v0, 0x7f0a09c8

    invoke-virtual {p0, v0}, Lejy;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 421
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 698
    invoke-super {p0, p1, p2, p3}, Leku;->a(Ljava/lang/String;Lhoz;Lhos;)V

    .line 700
    const-string v0, "GroupModifyCircleMembershipsTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 701
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 702
    iget-object v0, p0, Lejy;->af:Lekb;

    invoke-virtual {v0}, Lekb;->a()V

    .line 703
    iget-object v0, p0, Lejy;->ad:Lekc;

    invoke-virtual {v0}, Lekc;->notifyDataSetChanged()V

    .line 706
    :cond_0
    return-void
.end method

.method public aR_()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 692
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected ak_()Ldid;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 681
    .line 682
    invoke-virtual {p0}, Lejy;->a()I

    move-result v0

    .line 681
    invoke-static {v0, v1, v1}, Ldib;->a(ILjava/lang/Integer;Ljava/lang/Integer;)Ldid;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 687
    const/16 v0, 0x69

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected c(I)V
    .locals 6

    .prologue
    .line 749
    iget-object v0, p0, Lejy;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 750
    iget-object v1, p0, Lejy;->ak:Lhke;

    const v2, 0x7f1000c7

    .line 752
    invoke-virtual {p0}, Lejy;->n()Lz;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;

    invoke-direct {v4, v3, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "account_id"

    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "category_index"

    invoke-virtual {v4, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "empty_selection_allowed"

    const/4 v3, 0x0

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 750
    invoke-virtual {v1, v2, v4}, Lhke;->a(ILandroid/content/Intent;)V

    .line 754
    return-void
.end method

.method protected d()V
    .locals 4

    .prologue
    .line 659
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejy;->R:Z

    .line 660
    invoke-virtual {p0}, Lejy;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lejy;->ai:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 661
    return-void
.end method

.method protected e()V
    .locals 4

    .prologue
    .line 665
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejy;->R:Z

    .line 666
    invoke-virtual {p0}, Lejy;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lejy;->ai:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 667
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 368
    invoke-super {p0, p1}, Leku;->e(Landroid/os/Bundle;)V

    .line 369
    const-string v0, "last_rendered_card"

    iget v1, p0, Lejy;->ag:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 370
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 395
    invoke-super {p0}, Leku;->g()V

    .line 398
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const v5, 0x7f1000ae

    const/4 v2, 0x0

    .line 441
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 442
    const v3, 0x7f1001ec

    if-ne v0, v3, :cond_3

    .line 443
    invoke-virtual {p1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 444
    const v0, 0x7f1000ad

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 445
    const v0, 0x7f1000af

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 446
    iget-object v5, p0, Lejy;->Z:Lhee;

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    .line 447
    invoke-virtual {p0}, Lejy;->n()Lz;

    move-result-object v6

    iget-object v7, p0, Lejy;->O:[Lois;

    aget-object v3, v7, v3

    .line 448
    iget-object v7, p0, Lejy;->af:Lekb;

    iget v8, v3, Lois;->b:I

    invoke-virtual {v7, v8}, Lekb;->a(I)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lejy;->af:Lekb;

    iget v8, v3, Lois;->b:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lekb;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_2

    .line 447
    :cond_0
    invoke-static {v6, v5, v4, v0}, Leyq;->a(Landroid/content/Context;IILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "following_preview_ids"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 449
    invoke-virtual {p0, v0}, Lejy;->a(Landroid/content/Intent;)V

    .line 462
    :cond_1
    :goto_0
    return-void

    .line 448
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v3, Lois;->d:[Loiu;

    array-length v7, v3

    :goto_1
    if-ge v2, v7, :cond_0

    aget-object v8, v3, v2

    iget-object v8, v8, Loiu;->b:Lohv;

    iget-object v8, v8, Lohv;->b:Lohp;

    iget-object v8, v8, Lohp;->d:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 450
    :cond_3
    const v3, 0x7f1001ed

    if-ne v0, v3, :cond_9

    .line 451
    invoke-virtual {p1, v5}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 452
    iget-object v3, p0, Lejy;->O:[Lois;

    aget-object v3, v3, v0

    .line 453
    check-cast p1, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;

    invoke-virtual {p0}, Lejy;->n()Lz;

    move-result-object v4

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v0, p0, Lejy;->T:Landroid/database/Cursor;

    if-nez v0, :cond_4

    const v0, 0x7f0a0592

    invoke-static {v4, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v3}, Lejy;->a(Lois;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0}, Lejy;->X()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lejy;->T:Landroid/database/Cursor;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lejy;->T:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    const/4 v0, 0x3

    :goto_2
    new-instance v2, Lknx;

    invoke-direct {v2, v0}, Lknx;-><init>(I)V

    invoke-virtual {v2, v4}, Lknx;->a(Landroid/content/Context;)V

    if-nez v1, :cond_7

    invoke-virtual {p0, v5}, Lejy;->c(I)V

    goto :goto_0

    :cond_6
    const/4 v0, 0x4

    goto :goto_2

    :cond_7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1, v3, v0}, Lejy;->a(Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Lois;Ljava/util/ArrayList;)V

    goto :goto_0

    :cond_8
    invoke-direct {p0}, Lejy;->X()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lejy;->af:Lekb;

    iget v5, v3, Lois;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v5, v6}, Lekb;->a(Ljava/lang/Integer;Ljava/util/ArrayList;)V

    invoke-direct {p0, p1, v2}, Lejy;->a(Lcom/google/android/libraries/social/circlemembership/ui/CirclesButton;Z)V

    invoke-direct {p0, v3, v1, v4, v2}, Lejy;->a(Lois;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    goto/16 :goto_0

    .line 454
    :cond_9
    instance-of v0, p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_a

    .line 455
    check-cast p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 456
    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->f()Ljava/lang/String;

    move-result-object v0

    .line 457
    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->g()Ljava/lang/String;

    move-result-object v2

    .line 458
    invoke-virtual {p0}, Lejy;->a()I

    move-result v3

    invoke-virtual {p0, v0, v2, v1, v3}, Lejy;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)V

    goto/16 :goto_0

    .line 460
    :cond_a
    invoke-super {p0, p1}, Leku;->onClick(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method public z()V
    .locals 0

    .prologue
    .line 402
    invoke-super {p0}, Leku;->z()V

    .line 403
    return-void
.end method
