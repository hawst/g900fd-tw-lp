.class public final Ldoa;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmko;",
        "Lmkp;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private q:Ljava/lang/Boolean;

.field private r:Ljava/lang/Boolean;

.field private s:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0xf

    aput v2, v0, v1

    sput-object v0, Ldoa;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 33
    const-string v3, "collectionupdate"

    new-instance v4, Lmko;

    invoke-direct {v4}, Lmko;-><init>()V

    new-instance v5, Lmkp;

    invoke-direct {v5}, Lmkp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 36
    iput-object p3, p0, Ldoa;->b:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Ldoa;->p:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method protected a(Lmko;)V
    .locals 3

    .prologue
    .line 66
    new-instance v0, Lnai;

    invoke-direct {v0}, Lnai;-><init>()V

    iput-object v0, p1, Lmko;->a:Lnai;

    .line 67
    iget-object v0, p1, Lmko;->a:Lnai;

    .line 69
    iget-object v1, p0, Ldoa;->b:Ljava/lang/String;

    iget-object v2, p0, Ldoa;->p:Ljava/lang/String;

    invoke-static {v1, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;)Lmzo;

    move-result-object v1

    iput-object v1, v0, Lnai;->a:Lmzo;

    .line 70
    sget-object v1, Ldoa;->a:[I

    iput-object v1, v0, Lnai;->b:[I

    .line 73
    new-instance v1, Lnak;

    invoke-direct {v1}, Lnak;-><init>()V

    iput-object v1, v0, Lnai;->i:Lnak;

    .line 74
    iget-object v1, v0, Lnai;->i:Lnak;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnak;->a:Ljava/lang/Boolean;

    .line 75
    new-instance v1, Lnal;

    invoke-direct {v1}, Lnal;-><init>()V

    iput-object v1, v0, Lnai;->f:Lnal;

    .line 76
    iget-object v1, v0, Lnai;->f:Lnal;

    iget-object v2, p0, Ldoa;->r:Ljava/lang/Boolean;

    iput-object v2, v1, Lnal;->b:Ljava/lang/Boolean;

    .line 77
    iget-object v0, v0, Lnai;->f:Lnal;

    iget-object v1, p0, Ldoa;->q:Ljava/lang/Boolean;

    iput-object v1, v0, Lnal;->a:Ljava/lang/Boolean;

    .line 78
    return-void
.end method

.method protected a(Lmkp;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ldoa;->r:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmkp;->a:Lnaj;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lmkp;->a:Lnaj;

    iget-object v0, v0, Lnaj;->b:Lnxr;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p1, Lmkp;->a:Lnaj;

    iget-object v0, v0, Lnaj;->b:Lnxr;

    iget-object v0, v0, Lnxr;->k:Ljava/lang/String;

    iput-object v0, p0, Ldoa;->s:Ljava/lang/String;

    .line 89
    :goto_0
    return-void

    .line 87
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Ldoa;->s:Ljava/lang/String;

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmko;

    invoke-virtual {p0, p1}, Ldoa;->a(Lmko;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 42
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldoa;->q:Ljava/lang/Boolean;

    .line 43
    iget-object v0, p0, Ldoa;->q:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldoa;->b(Z)V

    .line 47
    :cond_0
    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmkp;

    invoke-virtual {p0, p1}, Ldoa;->a(Lmkp;)V

    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 51
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ldoa;->r:Ljava/lang/Boolean;

    .line 52
    iget-object v0, p0, Ldoa;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldoa;->a(Z)V

    .line 56
    :cond_0
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Ldoa;->s:Ljava/lang/String;

    return-object v0
.end method
