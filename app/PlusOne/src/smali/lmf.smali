.class final Llmf;
.super Ljava/io/InputStream;
.source "PG"

# interfaces
.implements Lorg/chromium/net/HttpUrlRequestListener;


# instance fields
.field private final a:Lorg/chromium/net/HttpUrlRequest;

.field private final b:Ljava/io/RandomAccessFile;

.field private final c:Ljava/io/InputStream;

.field private final d:J

.field private final e:J

.field private f:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;JJ)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    .line 66
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 67
    cmp-long v0, p4, v6

    if-ltz v0, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, p6, v0

    if-eqz v0, :cond_1

    cmp-long v0, p4, p6

    if-lez v0, :cond_1

    .line 68
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid stream limits"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_1
    iput-wide p4, p0, Llmf;->f:J

    .line 71
    iput-wide p6, p0, Llmf;->d:J

    .line 72
    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Llmf;->e:J

    .line 74
    iget-wide v0, p0, Llmf;->f:J

    iget-wide v4, p0, Llmf;->e:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_3

    .line 75
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v0, p2, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Llmf;->b:Ljava/io/RandomAccessFile;

    .line 76
    iget-object v0, p0, Llmf;->b:Ljava/io/RandomAccessFile;

    iget-wide v4, p0, Llmf;->f:J

    invoke-virtual {v0, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 81
    :goto_0
    if-eqz p3, :cond_4

    .line 82
    invoke-static {}, Ljava/nio/channels/Pipe;->open()Ljava/nio/channels/Pipe;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Ljava/nio/channels/Pipe;->source()Ljava/nio/channels/Pipe$SourceChannel;

    move-result-object v1

    invoke-static {v1}, Ljava/nio/channels/Channels;->newInputStream(Ljava/nio/channels/ReadableByteChannel;)Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Llmf;->c:Ljava/io/InputStream;

    .line 84
    const/4 v2, 0x4

    .line 89
    invoke-virtual {v0}, Ljava/nio/channels/Pipe;->sink()Ljava/nio/channels/Pipe$SinkChannel;

    move-result-object v4

    move-object v0, p1

    move-object v1, p3

    move-object v5, p0

    .line 84
    invoke-static/range {v0 .. v5}, Ljgm;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    iput-object v0, p0, Llmf;->a:Lorg/chromium/net/HttpUrlRequest;

    .line 91
    iget-wide v0, p0, Llmf;->e:J

    iget-wide v2, p0, Llmf;->f:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 92
    cmp-long v2, v0, v6

    if-eqz v2, :cond_2

    .line 93
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x29

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Starting request at: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 94
    iget-object v2, p0, Llmf;->a:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v2, v0, v1}, Lorg/chromium/net/HttpUrlRequest;->a(J)V

    .line 96
    :cond_2
    iget-object v0, p0, Llmf;->a:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->g()V

    .line 101
    :goto_1
    return-void

    .line 78
    :cond_3
    iput-object v3, p0, Llmf;->b:Ljava/io/RandomAccessFile;

    goto :goto_0

    .line 98
    :cond_4
    iput-object v3, p0, Llmf;->a:Lorg/chromium/net/HttpUrlRequest;

    .line 99
    iput-object v3, p0, Llmf;->c:Ljava/io/InputStream;

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 135
    invoke-super {p0}, Ljava/io/InputStream;->close()V

    .line 136
    iget-object v0, p0, Llmf;->c:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Llmf;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 139
    :cond_0
    iget-object v0, p0, Llmf;->b:Ljava/io/RandomAccessFile;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Llmf;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 142
    :cond_1
    return-void
.end method

.method public onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public onResponseStarted(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method public read()I
    .locals 1

    .prologue
    .line 146
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public read([BII)I
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v2, -0x1

    .line 105
    iget-wide v0, p0, Llmf;->d:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    iget-wide v0, p0, Llmf;->f:J

    iget-wide v4, p0, Llmf;->d:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_1

    move v0, v2

    .line 130
    :cond_0
    :goto_0
    return v0

    .line 111
    :cond_1
    iget-wide v0, p0, Llmf;->f:J

    iget-wide v4, p0, Llmf;->e:J

    cmp-long v0, v0, v4

    if-gez v0, :cond_3

    .line 112
    iget-wide v0, p0, Llmf;->e:J

    .line 113
    iget-wide v4, p0, Llmf;->d:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_2

    iget-wide v4, p0, Llmf;->d:J

    cmp-long v3, v4, v0

    if-gez v3, :cond_2

    .line 114
    iget-wide v0, p0, Llmf;->d:J

    .line 116
    :cond_2
    iget-wide v4, p0, Llmf;->f:J

    sub-long/2addr v0, v4

    long-to-int v0, v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 117
    iget-object v1, p0, Llmf;->b:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    .line 127
    :goto_1
    if-eq v0, v2, :cond_0

    .line 128
    iget-wide v2, p0, Llmf;->f:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Llmf;->f:J

    goto :goto_0

    .line 118
    :cond_3
    iget-object v0, p0, Llmf;->c:Ljava/io/InputStream;

    if-eqz v0, :cond_5

    .line 120
    iget-wide v0, p0, Llmf;->d:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_4

    .line 121
    iget-wide v0, p0, Llmf;->d:J

    iget-wide v4, p0, Llmf;->f:J

    sub-long/2addr v0, v4

    long-to-int v0, v0

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result p3

    .line 123
    :cond_4
    iget-object v0, p0, Llmf;->c:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    goto :goto_1

    :cond_5
    move v0, v2

    .line 125
    goto :goto_1
.end method
