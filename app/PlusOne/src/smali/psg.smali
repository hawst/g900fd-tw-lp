.class public final Lpsg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpsg;


# instance fields
.field private b:Lpgx;

.field private c:[Lppg;

.field private d:Lppg;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lpsg;

    sput-object v0, Lpsg;->a:[Lpsg;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v1, p0, Lpsg;->b:Lpgx;

    .line 16
    sget-object v0, Lppg;->a:[Lppg;

    iput-object v0, p0, Lpsg;->c:[Lppg;

    .line 19
    iput-object v1, p0, Lpsg;->d:Lppg;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 53
    .line 54
    iget-object v0, p0, Lpsg;->b:Lpgx;

    if-eqz v0, :cond_5

    .line 55
    const/4 v0, 0x1

    iget-object v2, p0, Lpsg;->b:Lpgx;

    .line 56
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 58
    :goto_0
    iget-object v2, p0, Lpsg;->c:[Lppg;

    if-eqz v2, :cond_1

    .line 59
    iget-object v2, p0, Lpsg;->c:[Lppg;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 60
    if-eqz v4, :cond_0

    .line 61
    const/4 v5, 0x2

    .line 62
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 59
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 66
    :cond_1
    iget-object v1, p0, Lpsg;->d:Lppg;

    if-eqz v1, :cond_2

    .line 67
    const/4 v1, 0x3

    iget-object v2, p0, Lpsg;->d:Lppg;

    .line 68
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    :cond_2
    iget-object v1, p0, Lpsg;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 71
    const/4 v1, 0x4

    iget-object v2, p0, Lpsg;->e:Ljava/lang/String;

    .line 72
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    :cond_3
    iget-object v1, p0, Lpsg;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 75
    const/4 v1, 0x5

    iget-object v2, p0, Lpsg;->f:Ljava/lang/String;

    .line 76
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 78
    :cond_4
    iget-object v1, p0, Lpsg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    iput v0, p0, Lpsg;->ai:I

    .line 80
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpsg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 88
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 89
    sparse-switch v0, :sswitch_data_0

    .line 93
    iget-object v2, p0, Lpsg;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 94
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpsg;->ah:Ljava/util/List;

    .line 97
    :cond_1
    iget-object v2, p0, Lpsg;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    :sswitch_0
    return-object p0

    .line 104
    :sswitch_1
    iget-object v0, p0, Lpsg;->b:Lpgx;

    if-nez v0, :cond_2

    .line 105
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lpsg;->b:Lpgx;

    .line 107
    :cond_2
    iget-object v0, p0, Lpsg;->b:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 111
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 112
    iget-object v0, p0, Lpsg;->c:[Lppg;

    if-nez v0, :cond_4

    move v0, v1

    .line 113
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lppg;

    .line 114
    iget-object v3, p0, Lpsg;->c:[Lppg;

    if-eqz v3, :cond_3

    .line 115
    iget-object v3, p0, Lpsg;->c:[Lppg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 117
    :cond_3
    iput-object v2, p0, Lpsg;->c:[Lppg;

    .line 118
    :goto_2
    iget-object v2, p0, Lpsg;->c:[Lppg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 119
    iget-object v2, p0, Lpsg;->c:[Lppg;

    new-instance v3, Lppg;

    invoke-direct {v3}, Lppg;-><init>()V

    aput-object v3, v2, v0

    .line 120
    iget-object v2, p0, Lpsg;->c:[Lppg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 121
    invoke-virtual {p1}, Loxn;->a()I

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 112
    :cond_4
    iget-object v0, p0, Lpsg;->c:[Lppg;

    array-length v0, v0

    goto :goto_1

    .line 124
    :cond_5
    iget-object v2, p0, Lpsg;->c:[Lppg;

    new-instance v3, Lppg;

    invoke-direct {v3}, Lppg;-><init>()V

    aput-object v3, v2, v0

    .line 125
    iget-object v2, p0, Lpsg;->c:[Lppg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 129
    :sswitch_3
    iget-object v0, p0, Lpsg;->d:Lppg;

    if-nez v0, :cond_6

    .line 130
    new-instance v0, Lppg;

    invoke-direct {v0}, Lppg;-><init>()V

    iput-object v0, p0, Lpsg;->d:Lppg;

    .line 132
    :cond_6
    iget-object v0, p0, Lpsg;->d:Lppg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 136
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpsg;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 140
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpsg;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 89
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 28
    iget-object v0, p0, Lpsg;->b:Lpgx;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v1, p0, Lpsg;->b:Lpgx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_0
    iget-object v0, p0, Lpsg;->c:[Lppg;

    if-eqz v0, :cond_2

    .line 32
    iget-object v1, p0, Lpsg;->c:[Lppg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 33
    if-eqz v3, :cond_1

    .line 34
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 32
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_2
    iget-object v0, p0, Lpsg;->d:Lppg;

    if-eqz v0, :cond_3

    .line 39
    const/4 v0, 0x3

    iget-object v1, p0, Lpsg;->d:Lppg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 41
    :cond_3
    iget-object v0, p0, Lpsg;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 42
    const/4 v0, 0x4

    iget-object v1, p0, Lpsg;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 44
    :cond_4
    iget-object v0, p0, Lpsg;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 45
    const/4 v0, 0x5

    iget-object v1, p0, Lpsg;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 47
    :cond_5
    iget-object v0, p0, Lpsg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 49
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpsg;->a(Loxn;)Lpsg;

    move-result-object v0

    return-object v0
.end method
