.class public final Lobj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lodo;

.field private b:Lobl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 837
    invoke-direct {p0}, Loxq;-><init>()V

    .line 840
    iput-object v0, p0, Lobj;->a:Lodo;

    .line 843
    iput-object v0, p0, Lobj;->b:Lobl;

    .line 837
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 860
    const/4 v0, 0x0

    .line 861
    iget-object v1, p0, Lobj;->a:Lodo;

    if-eqz v1, :cond_0

    .line 862
    const/4 v0, 0x1

    iget-object v1, p0, Lobj;->a:Lodo;

    .line 863
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 865
    :cond_0
    iget-object v1, p0, Lobj;->b:Lobl;

    if-eqz v1, :cond_1

    .line 866
    const/4 v1, 0x2

    iget-object v2, p0, Lobj;->b:Lobl;

    .line 867
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 869
    :cond_1
    iget-object v1, p0, Lobj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 870
    iput v0, p0, Lobj;->ai:I

    .line 871
    return v0
.end method

.method public a(Loxn;)Lobj;
    .locals 2

    .prologue
    .line 879
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 880
    sparse-switch v0, :sswitch_data_0

    .line 884
    iget-object v1, p0, Lobj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 885
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lobj;->ah:Ljava/util/List;

    .line 888
    :cond_1
    iget-object v1, p0, Lobj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 890
    :sswitch_0
    return-object p0

    .line 895
    :sswitch_1
    iget-object v0, p0, Lobj;->a:Lodo;

    if-nez v0, :cond_2

    .line 896
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobj;->a:Lodo;

    .line 898
    :cond_2
    iget-object v0, p0, Lobj;->a:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 902
    :sswitch_2
    iget-object v0, p0, Lobj;->b:Lobl;

    if-nez v0, :cond_3

    .line 903
    new-instance v0, Lobl;

    invoke-direct {v0}, Lobl;-><init>()V

    iput-object v0, p0, Lobj;->b:Lobl;

    .line 905
    :cond_3
    iget-object v0, p0, Lobj;->b:Lobl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 880
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 848
    iget-object v0, p0, Lobj;->a:Lodo;

    if-eqz v0, :cond_0

    .line 849
    const/4 v0, 0x1

    iget-object v1, p0, Lobj;->a:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 851
    :cond_0
    iget-object v0, p0, Lobj;->b:Lobl;

    if-eqz v0, :cond_1

    .line 852
    const/4 v0, 0x2

    iget-object v1, p0, Lobj;->b:Lobl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 854
    :cond_1
    iget-object v0, p0, Lobj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 856
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 833
    invoke-virtual {p0, p1}, Lobj;->a(Loxn;)Lobj;

    move-result-object v0

    return-object v0
.end method
