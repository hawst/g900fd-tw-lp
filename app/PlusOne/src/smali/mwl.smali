.class public final Lmwl;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:[I

.field private e:[I

.field private f:[Lmwm;

.field private g:I

.field private h:[I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 4760
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4770
    iput v1, p0, Lmwl;->a:I

    .line 4773
    iput v1, p0, Lmwl;->b:I

    .line 4776
    iput v1, p0, Lmwl;->c:I

    .line 4779
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmwl;->d:[I

    .line 4782
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmwl;->e:[I

    .line 4785
    sget-object v0, Lmwm;->a:[Lmwm;

    iput-object v0, p0, Lmwl;->f:[Lmwm;

    .line 4788
    iput v1, p0, Lmwl;->g:I

    .line 4791
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmwl;->h:[I

    .line 4794
    iput v1, p0, Lmwl;->i:I

    .line 4797
    iput v1, p0, Lmwl;->j:I

    .line 4800
    iput v1, p0, Lmwl;->k:I

    .line 4803
    iput v1, p0, Lmwl;->l:I

    .line 4806
    iput v1, p0, Lmwl;->m:I

    .line 4760
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/high16 v7, -0x80000000

    .line 4866
    .line 4867
    iget v0, p0, Lmwl;->a:I

    if-eq v0, v7, :cond_10

    .line 4868
    const/4 v0, 0x1

    iget v2, p0, Lmwl;->a:I

    .line 4869
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4871
    :goto_0
    iget v2, p0, Lmwl;->b:I

    if-eq v2, v7, :cond_0

    .line 4872
    const/4 v2, 0x2

    iget v3, p0, Lmwl;->b:I

    .line 4873
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4875
    :cond_0
    iget v2, p0, Lmwl;->c:I

    if-eq v2, v7, :cond_1

    .line 4876
    const/4 v2, 0x3

    iget v3, p0, Lmwl;->c:I

    .line 4877
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4879
    :cond_1
    iget-object v2, p0, Lmwl;->d:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmwl;->d:[I

    array-length v2, v2

    if-lez v2, :cond_3

    .line 4881
    iget-object v4, p0, Lmwl;->d:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget v6, v4, v2

    .line 4883
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 4881
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4885
    :cond_2
    add-int/2addr v0, v3

    .line 4886
    iget-object v2, p0, Lmwl;->d:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4888
    :cond_3
    iget-object v2, p0, Lmwl;->e:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Lmwl;->e:[I

    array-length v2, v2

    if-lez v2, :cond_5

    .line 4890
    iget-object v4, p0, Lmwl;->e:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_4

    aget v6, v4, v2

    .line 4892
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 4890
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 4894
    :cond_4
    add-int/2addr v0, v3

    .line 4895
    iget-object v2, p0, Lmwl;->e:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4897
    :cond_5
    iget-object v2, p0, Lmwl;->f:[Lmwm;

    if-eqz v2, :cond_7

    .line 4898
    iget-object v3, p0, Lmwl;->f:[Lmwm;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 4899
    if-eqz v5, :cond_6

    .line 4900
    const/4 v6, 0x6

    .line 4901
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4898
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 4905
    :cond_7
    iget v2, p0, Lmwl;->g:I

    if-eq v2, v7, :cond_8

    .line 4906
    const/4 v2, 0x7

    iget v3, p0, Lmwl;->g:I

    .line 4907
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4909
    :cond_8
    iget-object v2, p0, Lmwl;->h:[I

    if-eqz v2, :cond_a

    iget-object v2, p0, Lmwl;->h:[I

    array-length v2, v2

    if-lez v2, :cond_a

    .line 4911
    iget-object v3, p0, Lmwl;->h:[I

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v1, v4, :cond_9

    aget v5, v3, v1

    .line 4913
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 4911
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 4915
    :cond_9
    add-int/2addr v0, v2

    .line 4916
    iget-object v1, p0, Lmwl;->h:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4918
    :cond_a
    iget v1, p0, Lmwl;->i:I

    if-eq v1, v7, :cond_b

    .line 4919
    const/16 v1, 0x9

    iget v2, p0, Lmwl;->i:I

    .line 4920
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4922
    :cond_b
    iget v1, p0, Lmwl;->j:I

    if-eq v1, v7, :cond_c

    .line 4923
    const/16 v1, 0xa

    iget v2, p0, Lmwl;->j:I

    .line 4924
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4926
    :cond_c
    iget v1, p0, Lmwl;->k:I

    if-eq v1, v7, :cond_d

    .line 4927
    const/16 v1, 0xb

    iget v2, p0, Lmwl;->k:I

    .line 4928
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4930
    :cond_d
    iget v1, p0, Lmwl;->l:I

    if-eq v1, v7, :cond_e

    .line 4931
    const/16 v1, 0xc

    iget v2, p0, Lmwl;->l:I

    .line 4932
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4934
    :cond_e
    iget v1, p0, Lmwl;->m:I

    if-eq v1, v7, :cond_f

    .line 4935
    const/16 v1, 0xd

    iget v2, p0, Lmwl;->m:I

    .line 4936
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4938
    :cond_f
    iget-object v1, p0, Lmwl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4939
    iput v0, p0, Lmwl;->ai:I

    .line 4940
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmwl;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 4948
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4949
    sparse-switch v0, :sswitch_data_0

    .line 4953
    iget-object v2, p0, Lmwl;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4954
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmwl;->ah:Ljava/util/List;

    .line 4957
    :cond_1
    iget-object v2, p0, Lmwl;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4959
    :sswitch_0
    return-object p0

    .line 4964
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4965
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-ne v0, v6, :cond_3

    .line 4969
    :cond_2
    iput v0, p0, Lmwl;->a:I

    goto :goto_0

    .line 4971
    :cond_3
    iput v1, p0, Lmwl;->a:I

    goto :goto_0

    .line 4976
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4977
    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-ne v0, v6, :cond_5

    .line 4981
    :cond_4
    iput v0, p0, Lmwl;->b:I

    goto :goto_0

    .line 4983
    :cond_5
    iput v1, p0, Lmwl;->b:I

    goto :goto_0

    .line 4988
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4989
    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-ne v0, v6, :cond_7

    .line 4993
    :cond_6
    iput v0, p0, Lmwl;->c:I

    goto :goto_0

    .line 4995
    :cond_7
    iput v1, p0, Lmwl;->c:I

    goto :goto_0

    .line 5000
    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5001
    iget-object v0, p0, Lmwl;->d:[I

    array-length v0, v0

    .line 5002
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 5003
    iget-object v3, p0, Lmwl;->d:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5004
    iput-object v2, p0, Lmwl;->d:[I

    .line 5005
    :goto_1
    iget-object v2, p0, Lmwl;->d:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 5006
    iget-object v2, p0, Lmwl;->d:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 5007
    invoke-virtual {p1}, Loxn;->a()I

    .line 5005
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5010
    :cond_8
    iget-object v2, p0, Lmwl;->d:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 5014
    :sswitch_5
    const/16 v0, 0x28

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5015
    iget-object v0, p0, Lmwl;->e:[I

    array-length v0, v0

    .line 5016
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 5017
    iget-object v3, p0, Lmwl;->e:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5018
    iput-object v2, p0, Lmwl;->e:[I

    .line 5019
    :goto_2
    iget-object v2, p0, Lmwl;->e:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 5020
    iget-object v2, p0, Lmwl;->e:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 5021
    invoke-virtual {p1}, Loxn;->a()I

    .line 5019
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5024
    :cond_9
    iget-object v2, p0, Lmwl;->e:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 5028
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5029
    iget-object v0, p0, Lmwl;->f:[Lmwm;

    if-nez v0, :cond_b

    move v0, v1

    .line 5030
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmwm;

    .line 5031
    iget-object v3, p0, Lmwl;->f:[Lmwm;

    if-eqz v3, :cond_a

    .line 5032
    iget-object v3, p0, Lmwl;->f:[Lmwm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5034
    :cond_a
    iput-object v2, p0, Lmwl;->f:[Lmwm;

    .line 5035
    :goto_4
    iget-object v2, p0, Lmwl;->f:[Lmwm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    .line 5036
    iget-object v2, p0, Lmwl;->f:[Lmwm;

    new-instance v3, Lmwm;

    invoke-direct {v3}, Lmwm;-><init>()V

    aput-object v3, v2, v0

    .line 5037
    iget-object v2, p0, Lmwl;->f:[Lmwm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 5038
    invoke-virtual {p1}, Loxn;->a()I

    .line 5035
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 5029
    :cond_b
    iget-object v0, p0, Lmwl;->f:[Lmwm;

    array-length v0, v0

    goto :goto_3

    .line 5041
    :cond_c
    iget-object v2, p0, Lmwl;->f:[Lmwm;

    new-instance v3, Lmwm;

    invoke-direct {v3}, Lmwm;-><init>()V

    aput-object v3, v2, v0

    .line 5042
    iget-object v2, p0, Lmwl;->f:[Lmwm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5046
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5047
    if-eqz v0, :cond_d

    if-eq v0, v4, :cond_d

    if-eq v0, v5, :cond_d

    if-ne v0, v6, :cond_e

    .line 5051
    :cond_d
    iput v0, p0, Lmwl;->g:I

    goto/16 :goto_0

    .line 5053
    :cond_e
    iput v1, p0, Lmwl;->g:I

    goto/16 :goto_0

    .line 5058
    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5059
    iget-object v0, p0, Lmwl;->h:[I

    array-length v0, v0

    .line 5060
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 5061
    iget-object v3, p0, Lmwl;->h:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5062
    iput-object v2, p0, Lmwl;->h:[I

    .line 5063
    :goto_5
    iget-object v2, p0, Lmwl;->h:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    .line 5064
    iget-object v2, p0, Lmwl;->h:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 5065
    invoke-virtual {p1}, Loxn;->a()I

    .line 5063
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 5068
    :cond_f
    iget-object v2, p0, Lmwl;->h:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 5072
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5073
    if-eqz v0, :cond_10

    if-eq v0, v4, :cond_10

    if-eq v0, v5, :cond_10

    if-ne v0, v6, :cond_11

    .line 5077
    :cond_10
    iput v0, p0, Lmwl;->i:I

    goto/16 :goto_0

    .line 5079
    :cond_11
    iput v1, p0, Lmwl;->i:I

    goto/16 :goto_0

    .line 5084
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5085
    if-eqz v0, :cond_12

    if-eq v0, v4, :cond_12

    if-eq v0, v5, :cond_12

    if-ne v0, v6, :cond_13

    .line 5089
    :cond_12
    iput v0, p0, Lmwl;->j:I

    goto/16 :goto_0

    .line 5091
    :cond_13
    iput v1, p0, Lmwl;->j:I

    goto/16 :goto_0

    .line 5096
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5097
    if-eqz v0, :cond_14

    if-eq v0, v4, :cond_14

    if-eq v0, v5, :cond_14

    if-ne v0, v6, :cond_15

    .line 5101
    :cond_14
    iput v0, p0, Lmwl;->k:I

    goto/16 :goto_0

    .line 5103
    :cond_15
    iput v1, p0, Lmwl;->k:I

    goto/16 :goto_0

    .line 5108
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5109
    if-eqz v0, :cond_16

    if-eq v0, v4, :cond_16

    if-eq v0, v5, :cond_16

    if-ne v0, v6, :cond_17

    .line 5113
    :cond_16
    iput v0, p0, Lmwl;->l:I

    goto/16 :goto_0

    .line 5115
    :cond_17
    iput v1, p0, Lmwl;->l:I

    goto/16 :goto_0

    .line 5120
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5121
    if-eqz v0, :cond_18

    if-eq v0, v4, :cond_18

    if-eq v0, v5, :cond_18

    if-ne v0, v6, :cond_19

    .line 5125
    :cond_18
    iput v0, p0, Lmwl;->m:I

    goto/16 :goto_0

    .line 5127
    :cond_19
    iput v1, p0, Lmwl;->m:I

    goto/16 :goto_0

    .line 4949
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v6, -0x80000000

    .line 4811
    iget v1, p0, Lmwl;->a:I

    if-eq v1, v6, :cond_0

    .line 4812
    const/4 v1, 0x1

    iget v2, p0, Lmwl;->a:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4814
    :cond_0
    iget v1, p0, Lmwl;->b:I

    if-eq v1, v6, :cond_1

    .line 4815
    const/4 v1, 0x2

    iget v2, p0, Lmwl;->b:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4817
    :cond_1
    iget v1, p0, Lmwl;->c:I

    if-eq v1, v6, :cond_2

    .line 4818
    const/4 v1, 0x3

    iget v2, p0, Lmwl;->c:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4820
    :cond_2
    iget-object v1, p0, Lmwl;->d:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lmwl;->d:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 4821
    iget-object v2, p0, Lmwl;->d:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget v4, v2, v1

    .line 4822
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 4821
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4825
    :cond_3
    iget-object v1, p0, Lmwl;->e:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lmwl;->e:[I

    array-length v1, v1

    if-lez v1, :cond_4

    .line 4826
    iget-object v2, p0, Lmwl;->e:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget v4, v2, v1

    .line 4827
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 4826
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4830
    :cond_4
    iget-object v1, p0, Lmwl;->f:[Lmwm;

    if-eqz v1, :cond_6

    .line 4831
    iget-object v2, p0, Lmwl;->f:[Lmwm;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 4832
    if-eqz v4, :cond_5

    .line 4833
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 4831
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4837
    :cond_6
    iget v1, p0, Lmwl;->g:I

    if-eq v1, v6, :cond_7

    .line 4838
    const/4 v1, 0x7

    iget v2, p0, Lmwl;->g:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 4840
    :cond_7
    iget-object v1, p0, Lmwl;->h:[I

    if-eqz v1, :cond_8

    iget-object v1, p0, Lmwl;->h:[I

    array-length v1, v1

    if-lez v1, :cond_8

    .line 4841
    iget-object v1, p0, Lmwl;->h:[I

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_8

    aget v3, v1, v0

    .line 4842
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 4841
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 4845
    :cond_8
    iget v0, p0, Lmwl;->i:I

    if-eq v0, v6, :cond_9

    .line 4846
    const/16 v0, 0x9

    iget v1, p0, Lmwl;->i:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4848
    :cond_9
    iget v0, p0, Lmwl;->j:I

    if-eq v0, v6, :cond_a

    .line 4849
    const/16 v0, 0xa

    iget v1, p0, Lmwl;->j:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4851
    :cond_a
    iget v0, p0, Lmwl;->k:I

    if-eq v0, v6, :cond_b

    .line 4852
    const/16 v0, 0xb

    iget v1, p0, Lmwl;->k:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4854
    :cond_b
    iget v0, p0, Lmwl;->l:I

    if-eq v0, v6, :cond_c

    .line 4855
    const/16 v0, 0xc

    iget v1, p0, Lmwl;->l:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4857
    :cond_c
    iget v0, p0, Lmwl;->m:I

    if-eq v0, v6, :cond_d

    .line 4858
    const/16 v0, 0xd

    iget v1, p0, Lmwl;->m:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4860
    :cond_d
    iget-object v0, p0, Lmwl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4862
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4756
    invoke-virtual {p0, p1}, Lmwl;->a(Loxn;)Lmwl;

    move-result-object v0

    return-object v0
.end method
