.class public Lcnt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqz;
.implements Llrg;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string v0, "Photos"

    iput-object v0, p0, Lcnt;->b:Ljava/lang/String;

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lcnt;->c:I

    .line 55
    iput-object p1, p0, Lcnt;->a:Landroid/app/Activity;

    .line 57
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 58
    return-void
.end method


# virtual methods
.method public a(Lizu;Ljava/lang/String;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 79
    iget-object v0, p0, Lcnt;->a:Landroid/app/Activity;

    .line 80
    invoke-virtual {p1}, Lizu;->c()J

    move-result-wide v2

    invoke-virtual {p1}, Lizu;->b()Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-virtual {p1}, Lizu;->d()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcnt;->c:I

    iget-object v6, p0, Lcnt;->b:Ljava/lang/String;

    iget v7, p0, Lcnt;->e:I

    .line 79
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.SEND"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v9, Landroid/content/ComponentName;

    const-string v10, "com.google.android.apps.moviemaker.MovieMakerActivity"

    invoke-direct {v9, v0, v10}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    const-string v8, "aam_photo_id"

    invoke-virtual {v0, v8, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "aadm_media_key"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "aam_owner_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source_id"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source_name"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 86
    iget-object v0, p0, Lcnt;->a:Landroid/app/Activity;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Lcnt;->e:I

    .line 87
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 88
    iget-object v0, p0, Lcnt;->a:Landroid/app/Activity;

    new-instance v1, Ljed;

    invoke-direct {v1, v2, p1}, Ljed;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget v2, p0, Lcnt;->c:I

    iget-object v3, p0, Lcnt;->b:Ljava/lang/String;

    iget v4, p0, Lcnt;->e:I

    move v5, p2

    invoke-static/range {v0 .. v5}, Ljfa;->a(Landroid/content/Context;Ljed;ILjava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 8

    .prologue
    .line 94
    iget-object v0, p0, Lcnt;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcnt;->d:Ljava/lang/String;

    iget v2, p0, Lcnt;->c:I

    iget-object v3, p0, Lcnt;->b:Ljava/lang/String;

    iget v4, p0, Lcnt;->e:I

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.google.android.apps.moviemaker.MovieMakerActivity"

    invoke-direct {v6, v0, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v0, "session_id"

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "source_id"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "source_name"

    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account_id"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 97
    iget-object v0, p0, Lcnt;->a:Landroid/app/Activity;

    const-class v1, Lctz;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    .line 98
    const-string v1, "android.intent.action.GET_CONTENT"

    const-class v2, Ljuf;

    .line 99
    invoke-virtual {v0, v2}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcnt;->a:Landroid/app/Activity;

    .line 98
    invoke-static {v5, v1, v0, v2}, Leys;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;Landroid/content/Context;)V

    .line 100
    iget-object v0, p0, Lcnt;->a:Landroid/app/Activity;

    invoke-virtual {v0, v5}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 101
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcnt;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    const-string v1, "movie_maker_session_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcnt;->d:Ljava/lang/String;

    .line 65
    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcnt;->e:I

    .line 67
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcnt;->b:Ljava/lang/String;

    .line 75
    iput p2, p0, Lcnt;->c:I

    .line 76
    return-void
.end method
