.class public final Ljav;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljaz;


# instance fields
.field private a:Ljaw;

.field private final b:Landroid/content/Context;

.field private c:Lcom/google/android/libraries/social/media/MediaResource;

.field private d:Ljava/lang/String;

.field private e:Ljaw;

.field private f:Ljaw;

.field private g:Ljaw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput-object p1, p0, Ljav;->b:Landroid/content/Context;

    .line 123
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 210
    iget-object v0, p0, Ljav;->e:Ljaw;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Ljav;->e:Ljaw;

    invoke-virtual {v0, v1}, Ljaw;->a(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Ljav;->f:Ljaw;

    invoke-virtual {v0, v1}, Ljaw;->a(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Ljav;->a:Ljaw;

    invoke-virtual {v0, v1}, Ljaw;->a(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Ljav;->g:Ljaw;

    invoke-virtual {v0, v1}, Ljaw;->a(Ljava/lang/String;)V

    .line 216
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Canvas;II)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 139
    iget-object v1, p0, Ljav;->e:Ljaw;

    if-nez v1, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v1, p0, Ljav;->e:Ljaw;

    iget-object v2, p0, Ljav;->c:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/MediaResource;->getDebugContentType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljaw;->a(Ljava/lang/String;)V

    .line 144
    iget-object v1, p0, Ljav;->e:Ljaw;

    invoke-virtual {v1, v4, v4}, Ljaw;->measure(II)V

    .line 146
    iget-object v1, p0, Ljav;->f:Ljaw;

    iget-object v2, p0, Ljav;->c:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/MediaResource;->getDebugFileSize()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljaw;->a(Ljava/lang/String;)V

    .line 147
    iget-object v1, p0, Ljav;->f:Ljaw;

    invoke-virtual {v1, v4, v4}, Ljaw;->measure(II)V

    .line 149
    iget-object v1, p0, Ljav;->a:Ljaw;

    iget-object v2, p0, Ljav;->c:Lcom/google/android/libraries/social/media/MediaResource;

    iget-object v3, p0, Ljav;->d:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v0, p0, Ljav;->d:Ljava/lang/String;

    :cond_2
    :goto_1
    invoke-virtual {v1, v0}, Ljaw;->a(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Ljav;->a:Ljaw;

    invoke-virtual {v0, v4, v4}, Ljaw;->measure(II)V

    .line 152
    iget-object v0, p0, Ljav;->g:Ljaw;

    iget-object v1, p0, Ljav;->c:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/MediaResource;->getDebugTimingInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljaw;->a(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Ljav;->g:Ljaw;

    invoke-virtual {v0, v4, v4}, Ljaw;->measure(II)V

    .line 156
    iget-object v0, p0, Ljav;->e:Ljaw;

    invoke-virtual {v0}, Ljaw;->getMeasuredHeight()I

    move-result v0

    if-gt v0, p3, :cond_0

    iget-object v0, p0, Ljav;->e:Ljaw;

    invoke-virtual {v0}, Ljaw;->getMeasuredWidth()I

    move-result v0

    if-gt v0, p2, :cond_0

    .line 157
    iget-object v0, p0, Ljav;->e:Ljaw;

    invoke-virtual {v0, p1}, Ljaw;->draw(Landroid/graphics/Canvas;)V

    .line 160
    iget-object v0, p0, Ljav;->e:Ljaw;

    invoke-virtual {v0}, Ljaw;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Ljav;->a:Ljaw;

    invoke-virtual {v1}, Ljaw;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    if-gt v0, p2, :cond_3

    .line 162
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 163
    iget-object v0, p0, Ljav;->a:Ljaw;

    invoke-virtual {v0}, Ljaw;->getMeasuredWidth()I

    move-result v0

    sub-int v0, p2, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 164
    iget-object v0, p0, Ljav;->a:Ljaw;

    invoke-virtual {v0, p1}, Ljaw;->draw(Landroid/graphics/Canvas;)V

    .line 165
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 169
    :cond_3
    iget-object v0, p0, Ljav;->e:Ljaw;

    .line 170
    invoke-virtual {v0}, Ljaw;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Ljav;->f:Ljaw;

    invoke-virtual {v1}, Ljaw;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    if-gt v0, p3, :cond_0

    iget-object v0, p0, Ljav;->f:Ljaw;

    invoke-virtual {v0}, Ljaw;->getMeasuredWidth()I

    move-result v0

    if-gt v0, p2, :cond_0

    .line 172
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 173
    iget-object v0, p0, Ljav;->f:Ljaw;

    invoke-virtual {v0}, Ljaw;->getMeasuredHeight()I

    move-result v0

    sub-int v0, p3, v0

    int-to-float v0, v0

    invoke-virtual {p1, v5, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 174
    iget-object v0, p0, Ljav;->f:Ljaw;

    invoke-virtual {v0, p1}, Ljaw;->draw(Landroid/graphics/Canvas;)V

    .line 175
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 178
    iget-object v0, p0, Ljav;->f:Ljaw;

    invoke-virtual {v0}, Ljaw;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Ljav;->g:Ljaw;

    invoke-virtual {v1}, Ljaw;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    if-gt v0, p2, :cond_0

    .line 180
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 181
    iget-object v0, p0, Ljav;->g:Ljaw;

    invoke-virtual {v0}, Ljaw;->getMeasuredWidth()I

    move-result v0

    sub-int v0, p2, v0

    int-to-float v0, v0

    iget-object v1, p0, Ljav;->g:Ljaw;

    .line 182
    invoke-virtual {v1}, Ljaw;->getMeasuredHeight()I

    move-result v1

    sub-int v1, p3, v1

    int-to-float v1, v1

    .line 181
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 183
    iget-object v0, p0, Ljav;->g:Ljaw;

    invoke-virtual {v0, p1}, Ljaw;->draw(Landroid/graphics/Canvas;)V

    .line 184
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 149
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/MediaResource;->getDebugPixelSize()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    add-int v3, p2, p3

    if-eqz v3, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " / "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljav;->d:Ljava/lang/String;

    iget-object v0, p0, Ljav;->d:Ljava/lang/String;

    goto/16 :goto_1
.end method

.method public a(Lcom/google/android/libraries/social/media/MediaResource;)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Ljav;->e:Ljaw;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Ljaw;

    iget-object v1, p0, Ljav;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Ljaw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljav;->e:Ljaw;

    .line 129
    new-instance v0, Ljaw;

    iget-object v1, p0, Ljav;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Ljaw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljav;->f:Ljaw;

    .line 130
    new-instance v0, Ljaw;

    iget-object v1, p0, Ljav;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Ljaw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljav;->a:Ljaw;

    .line 131
    new-instance v0, Ljaw;

    iget-object v1, p0, Ljav;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Ljaw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljav;->g:Ljaw;

    .line 134
    :cond_0
    iput-object p1, p0, Ljav;->c:Lcom/google/android/libraries/social/media/MediaResource;

    .line 135
    return-void
.end method
