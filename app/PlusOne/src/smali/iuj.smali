.class public abstract Liuj;
.super Lt;
.source "PG"


# instance fields
.field private N:Liun;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lt;-><init>()V

    .line 123
    return-void
.end method

.method static synthetic a(Liuj;)Liun;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Liuj;->N:Liun;

    return-object v0
.end method


# virtual methods
.method public abstract U()I
.end method

.method public abstract V()I
.end method

.method public abstract W()I
.end method

.method protected X()Landroid/content/Context;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 104
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const v0, 0x103006e

    .line 106
    :goto_0
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Liuj;->n()Lz;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v1

    .line 104
    :cond_0
    const v0, 0x103000c

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 58
    invoke-virtual {p0}, Liuj;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 60
    invoke-virtual {p0, v1}, Liuj;->k(Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 61
    const-string v0, "button_text"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Liuj;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 63
    const-string v0, "mandatory_message"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 64
    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not show Location Reporting Dialog without legal text explicitly set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    invoke-virtual {p0}, Liuj;->W()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 70
    const-string v3, "mandatory_message"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Llif;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Liuj;->c()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Window;->requestFeature(I)Z

    .line 73
    invoke-virtual {p0}, Liuj;->c()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 74
    return-object v2
.end method

.method protected a(Landroid/view/View;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p0}, Liuj;->U()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 82
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 83
    new-instance v1, Liuk;

    invoke-direct {v1, p0}, Liuk;-><init>(Liuj;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    invoke-virtual {p0}, Liuj;->V()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 93
    new-instance v1, Liul;

    invoke-direct {v1, p0}, Liul;-><init>(Liuj;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    return-void
.end method

.method public a(Liun;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Liuj;->N:Liun;

    .line 111
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 40
    invoke-virtual {p0}, Liuj;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 41
    if-nez v0, :cond_0

    .line 42
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 44
    :cond_0
    const-string v1, "mandatory_message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v1, "button_text"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0, v0}, Liuj;->f(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method

.method public abstract k(Landroid/os/Bundle;)Landroid/view/View;
.end method
