.class public final Lnpk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Locf;

.field public b:Lock;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 816
    invoke-direct {p0}, Loxq;-><init>()V

    .line 821
    iput-object v0, p0, Lnpk;->a:Locf;

    .line 824
    iput-object v0, p0, Lnpk;->b:Lock;

    .line 816
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 844
    const/4 v0, 0x0

    .line 845
    iget-object v1, p0, Lnpk;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 846
    const/4 v0, 0x1

    iget-object v1, p0, Lnpk;->c:Ljava/lang/String;

    .line 847
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 849
    :cond_0
    iget-object v1, p0, Lnpk;->a:Locf;

    if-eqz v1, :cond_1

    .line 850
    const/4 v1, 0x2

    iget-object v2, p0, Lnpk;->a:Locf;

    .line 851
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 853
    :cond_1
    iget-object v1, p0, Lnpk;->b:Lock;

    if-eqz v1, :cond_2

    .line 854
    const/4 v1, 0x3

    iget-object v2, p0, Lnpk;->b:Lock;

    .line 855
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 857
    :cond_2
    iget-object v1, p0, Lnpk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 858
    iput v0, p0, Lnpk;->ai:I

    .line 859
    return v0
.end method

.method public a(Loxn;)Lnpk;
    .locals 2

    .prologue
    .line 867
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 868
    sparse-switch v0, :sswitch_data_0

    .line 872
    iget-object v1, p0, Lnpk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 873
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnpk;->ah:Ljava/util/List;

    .line 876
    :cond_1
    iget-object v1, p0, Lnpk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 878
    :sswitch_0
    return-object p0

    .line 883
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnpk;->c:Ljava/lang/String;

    goto :goto_0

    .line 887
    :sswitch_2
    iget-object v0, p0, Lnpk;->a:Locf;

    if-nez v0, :cond_2

    .line 888
    new-instance v0, Locf;

    invoke-direct {v0}, Locf;-><init>()V

    iput-object v0, p0, Lnpk;->a:Locf;

    .line 890
    :cond_2
    iget-object v0, p0, Lnpk;->a:Locf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 894
    :sswitch_3
    iget-object v0, p0, Lnpk;->b:Lock;

    if-nez v0, :cond_3

    .line 895
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Lnpk;->b:Lock;

    .line 897
    :cond_3
    iget-object v0, p0, Lnpk;->b:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 868
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 829
    iget-object v0, p0, Lnpk;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 830
    const/4 v0, 0x1

    iget-object v1, p0, Lnpk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 832
    :cond_0
    iget-object v0, p0, Lnpk;->a:Locf;

    if-eqz v0, :cond_1

    .line 833
    const/4 v0, 0x2

    iget-object v1, p0, Lnpk;->a:Locf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 835
    :cond_1
    iget-object v0, p0, Lnpk;->b:Lock;

    if-eqz v0, :cond_2

    .line 836
    const/4 v0, 0x3

    iget-object v1, p0, Lnpk;->b:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 838
    :cond_2
    iget-object v0, p0, Lnpk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 840
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 812
    invoke-virtual {p0, p1}, Lnpk;->a(Loxn;)Lnpk;

    move-result-object v0

    return-object v0
.end method
