.class public final Lowu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lowu;


# instance fields
.field private b:I

.field private c:Losf;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Losf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 484
    const/4 v0, 0x0

    new-array v0, v0, [Lowu;

    sput-object v0, Lowu;->a:[Lowu;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 485
    invoke-direct {p0}, Loxq;-><init>()V

    .line 495
    const/high16 v0, -0x80000000

    iput v0, p0, Lowu;->b:I

    .line 498
    iput-object v1, p0, Lowu;->c:Losf;

    .line 505
    iput-object v1, p0, Lowu;->f:Losf;

    .line 485
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 531
    const/4 v0, 0x0

    .line 532
    iget v1, p0, Lowu;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 533
    const/4 v0, 0x1

    iget v1, p0, Lowu;->b:I

    .line 534
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 536
    :cond_0
    iget-object v1, p0, Lowu;->c:Losf;

    if-eqz v1, :cond_1

    .line 537
    const/4 v1, 0x2

    iget-object v2, p0, Lowu;->c:Losf;

    .line 538
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 540
    :cond_1
    iget-object v1, p0, Lowu;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 541
    const/4 v1, 0x3

    iget-object v2, p0, Lowu;->d:Ljava/lang/String;

    .line 542
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 544
    :cond_2
    iget-object v1, p0, Lowu;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 545
    const/4 v1, 0x4

    iget-object v2, p0, Lowu;->e:Ljava/lang/String;

    .line 546
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 548
    :cond_3
    iget-object v1, p0, Lowu;->f:Losf;

    if-eqz v1, :cond_4

    .line 549
    const/4 v1, 0x5

    iget-object v2, p0, Lowu;->f:Losf;

    .line 550
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 552
    :cond_4
    iget-object v1, p0, Lowu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 553
    iput v0, p0, Lowu;->ai:I

    .line 554
    return v0
.end method

.method public a(Loxn;)Lowu;
    .locals 2

    .prologue
    .line 562
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 563
    sparse-switch v0, :sswitch_data_0

    .line 567
    iget-object v1, p0, Lowu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 568
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lowu;->ah:Ljava/util/List;

    .line 571
    :cond_1
    iget-object v1, p0, Lowu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 573
    :sswitch_0
    return-object p0

    .line 578
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 579
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 583
    :cond_2
    iput v0, p0, Lowu;->b:I

    goto :goto_0

    .line 585
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lowu;->b:I

    goto :goto_0

    .line 590
    :sswitch_2
    iget-object v0, p0, Lowu;->c:Losf;

    if-nez v0, :cond_4

    .line 591
    new-instance v0, Losf;

    invoke-direct {v0}, Losf;-><init>()V

    iput-object v0, p0, Lowu;->c:Losf;

    .line 593
    :cond_4
    iget-object v0, p0, Lowu;->c:Losf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 597
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lowu;->d:Ljava/lang/String;

    goto :goto_0

    .line 601
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lowu;->e:Ljava/lang/String;

    goto :goto_0

    .line 605
    :sswitch_5
    iget-object v0, p0, Lowu;->f:Losf;

    if-nez v0, :cond_5

    .line 606
    new-instance v0, Losf;

    invoke-direct {v0}, Losf;-><init>()V

    iput-object v0, p0, Lowu;->f:Losf;

    .line 608
    :cond_5
    iget-object v0, p0, Lowu;->f:Losf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 563
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 510
    iget v0, p0, Lowu;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 511
    const/4 v0, 0x1

    iget v1, p0, Lowu;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 513
    :cond_0
    iget-object v0, p0, Lowu;->c:Losf;

    if-eqz v0, :cond_1

    .line 514
    const/4 v0, 0x2

    iget-object v1, p0, Lowu;->c:Losf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 516
    :cond_1
    iget-object v0, p0, Lowu;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 517
    const/4 v0, 0x3

    iget-object v1, p0, Lowu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 519
    :cond_2
    iget-object v0, p0, Lowu;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 520
    const/4 v0, 0x4

    iget-object v1, p0, Lowu;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 522
    :cond_3
    iget-object v0, p0, Lowu;->f:Losf;

    if-eqz v0, :cond_4

    .line 523
    const/4 v0, 0x5

    iget-object v1, p0, Lowu;->f:Losf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 525
    :cond_4
    iget-object v0, p0, Lowu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 527
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 481
    invoke-virtual {p0, p1}, Lowu;->a(Loxn;)Lowu;

    move-result-object v0

    return-object v0
.end method
