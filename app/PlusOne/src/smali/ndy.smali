.class public final Lndy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lneo;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Lner;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2317
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2326
    iput-object v0, p0, Lndy;->b:Lneo;

    .line 2329
    iput-object v0, p0, Lndy;->e:Lner;

    .line 2317
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2355
    const/4 v0, 0x0

    .line 2356
    iget-object v1, p0, Lndy;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2357
    const/4 v0, 0x1

    iget-object v1, p0, Lndy;->c:Ljava/lang/Integer;

    .line 2358
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2360
    :cond_0
    iget-object v1, p0, Lndy;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2361
    const/4 v1, 0x2

    iget-object v2, p0, Lndy;->a:Ljava/lang/String;

    .line 2362
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2364
    :cond_1
    iget-object v1, p0, Lndy;->b:Lneo;

    if-eqz v1, :cond_2

    .line 2365
    const/4 v1, 0x3

    iget-object v2, p0, Lndy;->b:Lneo;

    .line 2366
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2368
    :cond_2
    iget-object v1, p0, Lndy;->e:Lner;

    if-eqz v1, :cond_3

    .line 2369
    const/4 v1, 0x4

    iget-object v2, p0, Lndy;->e:Lner;

    .line 2370
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2372
    :cond_3
    iget-object v1, p0, Lndy;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 2373
    const/4 v1, 0x5

    iget-object v2, p0, Lndy;->d:Ljava/lang/Integer;

    .line 2374
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2376
    :cond_4
    iget-object v1, p0, Lndy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2377
    iput v0, p0, Lndy;->ai:I

    .line 2378
    return v0
.end method

.method public a(Loxn;)Lndy;
    .locals 2

    .prologue
    .line 2386
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2387
    sparse-switch v0, :sswitch_data_0

    .line 2391
    iget-object v1, p0, Lndy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2392
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lndy;->ah:Ljava/util/List;

    .line 2395
    :cond_1
    iget-object v1, p0, Lndy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2397
    :sswitch_0
    return-object p0

    .line 2402
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lndy;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 2406
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lndy;->a:Ljava/lang/String;

    goto :goto_0

    .line 2410
    :sswitch_3
    iget-object v0, p0, Lndy;->b:Lneo;

    if-nez v0, :cond_2

    .line 2411
    new-instance v0, Lneo;

    invoke-direct {v0}, Lneo;-><init>()V

    iput-object v0, p0, Lndy;->b:Lneo;

    .line 2413
    :cond_2
    iget-object v0, p0, Lndy;->b:Lneo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2417
    :sswitch_4
    iget-object v0, p0, Lndy;->e:Lner;

    if-nez v0, :cond_3

    .line 2418
    new-instance v0, Lner;

    invoke-direct {v0}, Lner;-><init>()V

    iput-object v0, p0, Lndy;->e:Lner;

    .line 2420
    :cond_3
    iget-object v0, p0, Lndy;->e:Lner;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2424
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lndy;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2387
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2334
    iget-object v0, p0, Lndy;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2335
    const/4 v0, 0x1

    iget-object v1, p0, Lndy;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2337
    :cond_0
    iget-object v0, p0, Lndy;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2338
    const/4 v0, 0x2

    iget-object v1, p0, Lndy;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2340
    :cond_1
    iget-object v0, p0, Lndy;->b:Lneo;

    if-eqz v0, :cond_2

    .line 2341
    const/4 v0, 0x3

    iget-object v1, p0, Lndy;->b:Lneo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2343
    :cond_2
    iget-object v0, p0, Lndy;->e:Lner;

    if-eqz v0, :cond_3

    .line 2344
    const/4 v0, 0x4

    iget-object v1, p0, Lndy;->e:Lner;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2346
    :cond_3
    iget-object v0, p0, Lndy;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2347
    const/4 v0, 0x5

    iget-object v1, p0, Lndy;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2349
    :cond_4
    iget-object v0, p0, Lndy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2351
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2313
    invoke-virtual {p0, p1}, Lndy;->a(Loxn;)Lndy;

    move-result-object v0

    return-object v0
.end method
