.class public final Loii;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Loih;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5195
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5198
    sget-object v0, Loih;->a:[Loih;

    iput-object v0, p0, Loii;->a:[Loih;

    .line 5195
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 5221
    .line 5222
    iget-object v1, p0, Loii;->a:[Loih;

    if-eqz v1, :cond_1

    .line 5223
    iget-object v2, p0, Loii;->a:[Loih;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 5224
    if-eqz v4, :cond_0

    .line 5225
    const/4 v5, 0x1

    .line 5226
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5223
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5230
    :cond_1
    iget-object v1, p0, Loii;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 5231
    const/4 v1, 0x2

    iget-object v2, p0, Loii;->b:Ljava/lang/Boolean;

    .line 5232
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5234
    :cond_2
    iget-object v1, p0, Loii;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5235
    iput v0, p0, Loii;->ai:I

    .line 5236
    return v0
.end method

.method public a(Loxn;)Loii;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5244
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5245
    sparse-switch v0, :sswitch_data_0

    .line 5249
    iget-object v2, p0, Loii;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 5250
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loii;->ah:Ljava/util/List;

    .line 5253
    :cond_1
    iget-object v2, p0, Loii;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5255
    :sswitch_0
    return-object p0

    .line 5260
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5261
    iget-object v0, p0, Loii;->a:[Loih;

    if-nez v0, :cond_3

    move v0, v1

    .line 5262
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loih;

    .line 5263
    iget-object v3, p0, Loii;->a:[Loih;

    if-eqz v3, :cond_2

    .line 5264
    iget-object v3, p0, Loii;->a:[Loih;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5266
    :cond_2
    iput-object v2, p0, Loii;->a:[Loih;

    .line 5267
    :goto_2
    iget-object v2, p0, Loii;->a:[Loih;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 5268
    iget-object v2, p0, Loii;->a:[Loih;

    new-instance v3, Loih;

    invoke-direct {v3}, Loih;-><init>()V

    aput-object v3, v2, v0

    .line 5269
    iget-object v2, p0, Loii;->a:[Loih;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 5270
    invoke-virtual {p1}, Loxn;->a()I

    .line 5267
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5261
    :cond_3
    iget-object v0, p0, Loii;->a:[Loih;

    array-length v0, v0

    goto :goto_1

    .line 5273
    :cond_4
    iget-object v2, p0, Loii;->a:[Loih;

    new-instance v3, Loih;

    invoke-direct {v3}, Loih;-><init>()V

    aput-object v3, v2, v0

    .line 5274
    iget-object v2, p0, Loii;->a:[Loih;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5278
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loii;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 5245
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 5205
    iget-object v0, p0, Loii;->a:[Loih;

    if-eqz v0, :cond_1

    .line 5206
    iget-object v1, p0, Loii;->a:[Loih;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 5207
    if-eqz v3, :cond_0

    .line 5208
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 5206
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5212
    :cond_1
    iget-object v0, p0, Loii;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 5213
    const/4 v0, 0x2

    iget-object v1, p0, Loii;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5215
    :cond_2
    iget-object v0, p0, Loii;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5217
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5191
    invoke-virtual {p0, p1}, Loii;->a(Loxn;)Loii;

    move-result-object v0

    return-object v0
.end method
