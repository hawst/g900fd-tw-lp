.class public final Lmax;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmys;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2428
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2431
    iput-object v0, p0, Lmax;->apiHeader:Llyr;

    .line 2434
    iput-object v0, p0, Lmax;->a:Lmys;

    .line 2428
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2451
    const/4 v0, 0x0

    .line 2452
    iget-object v1, p0, Lmax;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 2453
    const/4 v0, 0x1

    iget-object v1, p0, Lmax;->apiHeader:Llyr;

    .line 2454
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2456
    :cond_0
    iget-object v1, p0, Lmax;->a:Lmys;

    if-eqz v1, :cond_1

    .line 2457
    const/4 v1, 0x2

    iget-object v2, p0, Lmax;->a:Lmys;

    .line 2458
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2460
    :cond_1
    iget-object v1, p0, Lmax;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2461
    iput v0, p0, Lmax;->ai:I

    .line 2462
    return v0
.end method

.method public a(Loxn;)Lmax;
    .locals 2

    .prologue
    .line 2470
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2471
    sparse-switch v0, :sswitch_data_0

    .line 2475
    iget-object v1, p0, Lmax;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2476
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmax;->ah:Ljava/util/List;

    .line 2479
    :cond_1
    iget-object v1, p0, Lmax;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2481
    :sswitch_0
    return-object p0

    .line 2486
    :sswitch_1
    iget-object v0, p0, Lmax;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 2487
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmax;->apiHeader:Llyr;

    .line 2489
    :cond_2
    iget-object v0, p0, Lmax;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2493
    :sswitch_2
    iget-object v0, p0, Lmax;->a:Lmys;

    if-nez v0, :cond_3

    .line 2494
    new-instance v0, Lmys;

    invoke-direct {v0}, Lmys;-><init>()V

    iput-object v0, p0, Lmax;->a:Lmys;

    .line 2496
    :cond_3
    iget-object v0, p0, Lmax;->a:Lmys;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2471
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2439
    iget-object v0, p0, Lmax;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 2440
    const/4 v0, 0x1

    iget-object v1, p0, Lmax;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2442
    :cond_0
    iget-object v0, p0, Lmax;->a:Lmys;

    if-eqz v0, :cond_1

    .line 2443
    const/4 v0, 0x2

    iget-object v1, p0, Lmax;->a:Lmys;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2445
    :cond_1
    iget-object v0, p0, Lmax;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2447
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2424
    invoke-virtual {p0, p1}, Lmax;->a(Loxn;)Lmax;

    move-result-object v0

    return-object v0
.end method
