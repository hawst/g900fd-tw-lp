.class public final Ljjl;
.super Ljkv;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljkv",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Landroid/content/Context;

.field private final e:Ljle;

.field private final f:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljle;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Ljkv;-><init>(Landroid/content/Context;)V

    .line 36
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Ljjl;->f:Ldp;

    .line 41
    iput-object p2, p0, Ljjl;->b:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Ljjl;->c:Ljava/lang/String;

    .line 43
    iput-object p1, p0, Ljjl;->d:Landroid/content/Context;

    .line 44
    iput-object p4, p0, Ljjl;->e:Ljle;

    .line 45
    return-void
.end method


# virtual methods
.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Ljjl;->f()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 51
    iget-object v1, p0, Ljjl;->d:Landroid/content/Context;

    iget-object v2, p0, Ljjl;->b:Ljava/lang/String;

    iget-object v3, p0, Ljjl;->c:Ljava/lang/String;

    iget-object v0, p0, Ljjl;->e:Ljle;

    sget-object v4, Ljlb;->a:[I

    invoke-virtual {v0}, Ljle;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    const-string v0, "priority IN (3,4) AND read_state NOT IN (3,5,0)"

    :goto_0
    invoke-static {v1, v2, v3, v0}, Ljla;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljkx;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Ljkx;->getCount()I

    .line 56
    iget-object v1, p0, Ljjl;->f:Ldp;

    invoke-virtual {v0, v1}, Ljkx;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 57
    iget-object v1, p0, Ljjl;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Ljkw;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Ljkx;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 59
    return-object v0

    .line 51
    :pswitch_0
    const-string v0, "priority IN (3,4) AND read_state IN (1,4)"

    goto :goto_0

    :pswitch_1
    const-string v0, "priority NOT IN ( 0,1) AND read_state NOT IN (3,5,0)"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
