.class public final Ldly;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmgg;",
        "Lmgh;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:Ljava/lang/Long;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private final s:Ljava/lang/String;

.field private final t:Ljava/lang/String;

.field private final u:Ljava/lang/String;

.field private final v:Ljava/lang/String;

.field private final w:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;IJLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 7

    .prologue
    .line 49
    const-string v4, "photoscreatetag"

    new-instance v5, Lmgg;

    invoke-direct {v5}, Lmgg;-><init>()V

    new-instance v6, Lmgh;

    invoke-direct {v6}, Lmgh;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v1 .. v6}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 51
    if-eqz p5, :cond_0

    const/4 v1, 0x1

    move v2, v1

    :goto_0
    if-eqz p13, :cond_1

    const/4 v1, 0x1

    :goto_1
    xor-int/2addr v1, v2

    if-nez v1, :cond_2

    .line 52
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Only one of shapeId or relativeBounds should be supplied."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 51
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 55
    :cond_2
    iput-wide p3, p0, Ldly;->a:J

    .line 56
    iput-object p5, p0, Ldly;->b:Ljava/lang/Long;

    .line 57
    iput-object p6, p0, Ldly;->p:Ljava/lang/String;

    .line 58
    iput-object p7, p0, Ldly;->q:Ljava/lang/String;

    .line 59
    iput-object p8, p0, Ldly;->r:Ljava/lang/String;

    .line 60
    move-object/from16 v0, p9

    iput-object v0, p0, Ldly;->s:Ljava/lang/String;

    .line 61
    move-object/from16 v0, p10

    iput-object v0, p0, Ldly;->t:Ljava/lang/String;

    .line 62
    move-object/from16 v0, p11

    iput-object v0, p0, Ldly;->u:Ljava/lang/String;

    .line 63
    move-object/from16 v0, p12

    iput-object v0, p0, Ldly;->v:Ljava/lang/String;

    .line 64
    move-object/from16 v0, p13

    iput-object v0, p0, Ldly;->w:Landroid/graphics/RectF;

    .line 65
    return-void
.end method


# virtual methods
.method protected a(Lmgg;)V
    .locals 4

    .prologue
    .line 69
    new-instance v0, Lnax;

    invoke-direct {v0}, Lnax;-><init>()V

    iput-object v0, p1, Lmgg;->a:Lnax;

    .line 70
    iget-object v0, p1, Lmgg;->a:Lnax;

    .line 71
    iget-wide v2, p0, Ldly;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lnax;->b:Ljava/lang/Long;

    .line 72
    iget-object v1, p0, Ldly;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 73
    iget-object v1, p0, Ldly;->b:Ljava/lang/Long;

    iput-object v1, v0, Lnax;->d:Ljava/lang/Long;

    .line 75
    :cond_0
    iget-object v1, p0, Ldly;->w:Landroid/graphics/RectF;

    if-eqz v1, :cond_1

    .line 76
    new-instance v1, Lnyp;

    invoke-direct {v1}, Lnyp;-><init>()V

    iput-object v1, v0, Lnax;->c:Lnyp;

    .line 77
    iget-object v1, v0, Lnax;->c:Lnyp;

    iget-object v2, p0, Ldly;->w:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lnyp;->a:Ljava/lang/Double;

    .line 78
    iget-object v1, v0, Lnax;->c:Lnyp;

    iget-object v2, p0, Ldly;->w:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lnyp;->c:Ljava/lang/Double;

    .line 79
    iget-object v1, v0, Lnax;->c:Lnyp;

    iget-object v2, p0, Ldly;->w:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lnyp;->b:Ljava/lang/Double;

    .line 80
    iget-object v1, v0, Lnax;->c:Lnyp;

    iget-object v2, p0, Ldly;->w:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v1, Lnyp;->d:Ljava/lang/Double;

    .line 82
    :cond_1
    iget-object v1, p0, Ldly;->p:Ljava/lang/String;

    iput-object v1, v0, Lnax;->a:Ljava/lang/String;

    .line 83
    new-instance v1, Lnay;

    invoke-direct {v1}, Lnay;-><init>()V

    .line 84
    iget-object v2, p0, Ldly;->q:Ljava/lang/String;

    iput-object v2, v1, Lnay;->b:Ljava/lang/String;

    .line 85
    iget-object v2, p0, Ldly;->r:Ljava/lang/String;

    iput-object v2, v1, Lnay;->c:Ljava/lang/String;

    .line 86
    iget-object v2, p0, Ldly;->s:Ljava/lang/String;

    iput-object v2, v1, Lnay;->a:Ljava/lang/String;

    .line 87
    iput-object v1, v0, Lnax;->e:Lnay;

    .line 88
    return-void
.end method

.method protected a(Lmgh;)V
    .locals 6

    .prologue
    .line 92
    iget-object v1, p1, Lmgh;->a:Lnaz;

    .line 93
    iget-object v0, v1, Lnaz;->a:Lnys;

    if-nez v0, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v0, p0, Ldly;->f:Landroid/content/Context;

    iget v2, p0, Ldly;->c:I

    iget-object v3, p0, Ldly;->t:Ljava/lang/String;

    iget-object v4, p0, Ldly;->u:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v0, v2, v3, v4, v5}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    .line 103
    :try_start_0
    new-instance v2, Lnym;

    invoke-direct {v2}, Lnym;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    iget-object v2, v0, Lnym;->g:[Lnys;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lnym;->g:[Lnys;

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 123
    iget-object v2, v1, Lnaz;->a:Lnys;

    .line 124
    iget-object v1, v0, Lnym;->g:[Lnys;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_2

    .line 125
    iget-object v3, v0, Lnym;->g:[Lnys;

    aget-object v3, v3, v1

    .line 126
    iget-object v4, v3, Lnys;->b:Ljava/lang/String;

    iget-object v5, v2, Lnys;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 127
    iget-object v1, v2, Lnys;->g:Lnyz;

    iput-object v1, v3, Lnys;->g:Lnyz;

    .line 128
    iget v1, v2, Lnys;->k:I

    iput v1, v3, Lnys;->k:I

    .line 129
    iget v1, v2, Lnys;->d:I

    iput v1, v3, Lnys;->d:I

    .line 130
    iget-object v1, v2, Lnys;->c:Lnyz;

    iput-object v1, v3, Lnys;->c:Lnyz;

    .line 134
    :cond_2
    new-instance v1, Lnzx;

    invoke-direct {v1}, Lnzx;-><init>()V

    .line 135
    iget-object v2, p0, Ldly;->t:Ljava/lang/String;

    iput-object v2, v1, Lnzx;->b:Ljava/lang/String;

    .line 136
    new-instance v2, Lnzu;

    invoke-direct {v2}, Lnzu;-><init>()V

    .line 137
    iput-object v0, v2, Lnzu;->b:Lnym;

    .line 138
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v1, v0, v2}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 139
    iget-object v0, p0, Ldly;->f:Landroid/content/Context;

    iget v2, p0, Ldly;->c:I

    const/4 v3, 0x0

    invoke-static {v0, v2, v1, v3}, Ljvj;->a(Landroid/content/Context;ILnzx;I)V

    .line 141
    iget-object v0, p0, Ldly;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Ldly;->f:Landroid/content/Context;

    iget v1, p0, Ldly;->c:I

    iget-object v2, p0, Ldly;->v:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Ljvd;->a(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    const-string v1, "PhotoTagCreateOp"

    const-string v2, "Unable to parse Photo from byte array."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 124
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lmgg;

    invoke-virtual {p0, p1}, Ldly;->a(Lmgg;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lmgh;

    invoke-virtual {p0, p1}, Ldly;->a(Lmgh;)V

    return-void
.end method
