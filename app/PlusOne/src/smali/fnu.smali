.class public final Lfnu;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Z

.field private final j:Z

.field private final k:[Landroid/graphics/Point;

.field private final l:Lkfd;

.field private final m:Lhei;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;)V
    .locals 2

    .prologue
    .line 41
    const-string v0, "PhotosEditCaptionTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 42
    iput p2, p0, Lfnu;->a:I

    .line 43
    iput-object p3, p0, Lfnu;->b:Ljava/lang/String;

    .line 44
    iput-wide p4, p0, Lfnu;->c:J

    .line 45
    iput-object p6, p0, Lfnu;->d:Ljava/lang/String;

    .line 46
    iput-object p7, p0, Lfnu;->e:Ljava/lang/String;

    .line 47
    iput-object p8, p0, Lfnu;->f:Ljava/lang/String;

    .line 48
    iput-object p9, p0, Lfnu;->h:Ljava/lang/String;

    .line 49
    iput-boolean p10, p0, Lfnu;->i:Z

    .line 50
    iput-boolean p11, p0, Lfnu;->j:Z

    .line 51
    iput-object p12, p0, Lfnu;->k:[Landroid/graphics/Point;

    .line 52
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lfnu;->l:Lkfd;

    .line 53
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfnu;->m:Lhei;

    .line 54
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 58
    iget-object v0, p0, Lfnu;->m:Lhei;

    iget v1, p0, Lfnu;->a:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v6

    .line 59
    invoke-virtual {p0}, Lfnu;->f()Landroid/content/Context;

    move-result-object v1

    .line 60
    new-instance v2, Lkfo;

    const-string v0, "account_name"

    invoke-interface {v6, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v10}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    new-instance v0, Ldmc;

    iget-object v3, p0, Lfnu;->d:Ljava/lang/String;

    iget-wide v4, p0, Lfnu;->c:J

    const-string v7, "gaia_id"

    .line 64
    invoke-interface {v6, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Ldmc;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;JLjava/lang/String;)V

    .line 65
    iget-object v3, p0, Lfnu;->l:Lkfd;

    invoke-interface {v3, v0}, Lkfd;->a(Lkff;)V

    .line 66
    invoke-virtual {v0}, Ldmc;->t()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 67
    new-instance v1, Lhoz;

    .line 68
    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v1, v2, v0, v10}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    .line 84
    :goto_0
    return-object v0

    .line 70
    :cond_0
    iget v0, p0, Lfnu;->a:I

    iget-object v3, p0, Lfnu;->b:Ljava/lang/String;

    iget-object v4, p0, Lfnu;->d:Ljava/lang/String;

    iget-object v5, p0, Lfnu;->e:Ljava/lang/String;

    invoke-static {v1, v0, v3, v4, v5}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    new-instance v0, Lfnm;

    iget-object v3, p0, Lfnu;->f:Ljava/lang/String;

    iget-object v4, p0, Lfnu;->h:Ljava/lang/String;

    iget-boolean v6, p0, Lfnu;->i:Z

    iget-boolean v7, p0, Lfnu;->j:Z

    iget-object v8, p0, Lfnu;->k:[Landroid/graphics/Point;

    move-object v5, v10

    invoke-direct/range {v0 .. v9}, Lfnm;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;Z)V

    .line 76
    iget-object v2, p0, Lfnu;->l:Lkfd;

    invoke-interface {v2, v0}, Lkfd;->a(Lkff;)V

    .line 77
    invoke-virtual {v0}, Lfnm;->t()Z

    move-result v2

    if-nez v2, :cond_1

    .line 78
    iget v3, p0, Lfnu;->a:I

    .line 79
    invoke-virtual {v0}, Lfnm;->D()Loxu;

    move-result-object v2

    check-cast v2, Lmdh;

    iget-object v2, v2, Lmdh;->a:Lmlr;

    iget-object v2, v2, Lmlr;->a:Lmmq;

    .line 78
    invoke-static {v1, v3, v2}, Lfoa;->a(Landroid/content/Context;ILmmq;)V

    .line 81
    :cond_1
    new-instance v1, Lhoz;

    .line 82
    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v1, v2, v0, v10}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 83
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "edit_succeeded"

    invoke-virtual {v0, v2, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    .line 84
    goto :goto_0
.end method
