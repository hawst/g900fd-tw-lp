.class public final Lptt;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lptu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, Loxq;-><init>()V

    .line 190
    sget-object v0, Lptu;->a:[Lptu;

    iput-object v0, p0, Lptt;->a:[Lptu;

    .line 187
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 208
    .line 209
    iget-object v1, p0, Lptt;->a:[Lptu;

    if-eqz v1, :cond_1

    .line 210
    iget-object v2, p0, Lptt;->a:[Lptu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 211
    if-eqz v4, :cond_0

    .line 212
    const/4 v5, 0x1

    .line 213
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 210
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 217
    :cond_1
    iget-object v1, p0, Lptt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    iput v0, p0, Lptt;->ai:I

    .line 219
    return v0
.end method

.method public a(Loxn;)Lptt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 227
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 228
    sparse-switch v0, :sswitch_data_0

    .line 232
    iget-object v2, p0, Lptt;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 233
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lptt;->ah:Ljava/util/List;

    .line 236
    :cond_1
    iget-object v2, p0, Lptt;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    :sswitch_0
    return-object p0

    .line 243
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 244
    iget-object v0, p0, Lptt;->a:[Lptu;

    if-nez v0, :cond_3

    move v0, v1

    .line 245
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lptu;

    .line 246
    iget-object v3, p0, Lptt;->a:[Lptu;

    if-eqz v3, :cond_2

    .line 247
    iget-object v3, p0, Lptt;->a:[Lptu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 249
    :cond_2
    iput-object v2, p0, Lptt;->a:[Lptu;

    .line 250
    :goto_2
    iget-object v2, p0, Lptt;->a:[Lptu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 251
    iget-object v2, p0, Lptt;->a:[Lptu;

    new-instance v3, Lptu;

    invoke-direct {v3}, Lptu;-><init>()V

    aput-object v3, v2, v0

    .line 252
    iget-object v2, p0, Lptt;->a:[Lptu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 253
    invoke-virtual {p1}, Loxn;->a()I

    .line 250
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 244
    :cond_3
    iget-object v0, p0, Lptt;->a:[Lptu;

    array-length v0, v0

    goto :goto_1

    .line 256
    :cond_4
    iget-object v2, p0, Lptt;->a:[Lptu;

    new-instance v3, Lptu;

    invoke-direct {v3}, Lptu;-><init>()V

    aput-object v3, v2, v0

    .line 257
    iget-object v2, p0, Lptt;->a:[Lptu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 228
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 195
    iget-object v0, p0, Lptt;->a:[Lptu;

    if-eqz v0, :cond_1

    .line 196
    iget-object v1, p0, Lptt;->a:[Lptu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 197
    if-eqz v3, :cond_0

    .line 198
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 196
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 202
    :cond_1
    iget-object v0, p0, Lptt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 204
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 183
    invoke-virtual {p0, p1}, Lptt;->a(Loxn;)Lptt;

    move-result-object v0

    return-object v0
.end method
