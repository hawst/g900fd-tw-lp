.class public final Lmke;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lndt;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20329
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20332
    iput-object v0, p0, Lmke;->apiHeader:Llyq;

    .line 20335
    iput-object v0, p0, Lmke;->a:Lndt;

    .line 20329
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 20352
    const/4 v0, 0x0

    .line 20353
    iget-object v1, p0, Lmke;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 20354
    const/4 v0, 0x1

    iget-object v1, p0, Lmke;->apiHeader:Llyq;

    .line 20355
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20357
    :cond_0
    iget-object v1, p0, Lmke;->a:Lndt;

    if-eqz v1, :cond_1

    .line 20358
    const/4 v1, 0x2

    iget-object v2, p0, Lmke;->a:Lndt;

    .line 20359
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20361
    :cond_1
    iget-object v1, p0, Lmke;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20362
    iput v0, p0, Lmke;->ai:I

    .line 20363
    return v0
.end method

.method public a(Loxn;)Lmke;
    .locals 2

    .prologue
    .line 20371
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 20372
    sparse-switch v0, :sswitch_data_0

    .line 20376
    iget-object v1, p0, Lmke;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 20377
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmke;->ah:Ljava/util/List;

    .line 20380
    :cond_1
    iget-object v1, p0, Lmke;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20382
    :sswitch_0
    return-object p0

    .line 20387
    :sswitch_1
    iget-object v0, p0, Lmke;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 20388
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmke;->apiHeader:Llyq;

    .line 20390
    :cond_2
    iget-object v0, p0, Lmke;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20394
    :sswitch_2
    iget-object v0, p0, Lmke;->a:Lndt;

    if-nez v0, :cond_3

    .line 20395
    new-instance v0, Lndt;

    invoke-direct {v0}, Lndt;-><init>()V

    iput-object v0, p0, Lmke;->a:Lndt;

    .line 20397
    :cond_3
    iget-object v0, p0, Lmke;->a:Lndt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20372
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 20340
    iget-object v0, p0, Lmke;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 20341
    const/4 v0, 0x1

    iget-object v1, p0, Lmke;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20343
    :cond_0
    iget-object v0, p0, Lmke;->a:Lndt;

    if-eqz v0, :cond_1

    .line 20344
    const/4 v0, 0x2

    iget-object v1, p0, Lmke;->a:Lndt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20346
    :cond_1
    iget-object v0, p0, Lmke;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 20348
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 20325
    invoke-virtual {p0, p1}, Lmke;->a(Loxn;)Lmke;

    move-result-object v0

    return-object v0
.end method
