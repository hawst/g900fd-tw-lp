.class public final Lozb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lozb;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Double;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lpdi;

.field private j:Ljava/lang/String;

.field private k:[Loya;

.field private l:Loya;

.field private m:[Loya;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Loya;

.field private q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x2308eed

    new-instance v1, Lozc;

    invoke-direct {v1}, Lozc;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lozb;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lozb;->i:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lozb;->k:[Loya;

    .line 35
    iput-object v1, p0, Lozb;->l:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lozb;->m:[Loya;

    .line 49
    iput-object v1, p0, Lozb;->p:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 118
    .line 119
    iget-object v0, p0, Lozb;->e:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 120
    const/4 v0, 0x1

    iget-object v2, p0, Lozb;->e:Ljava/lang/String;

    .line 121
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 123
    :goto_0
    iget-object v2, p0, Lozb;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 124
    const/4 v2, 0x2

    iget-object v3, p0, Lozb;->f:Ljava/lang/String;

    .line 125
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 127
    :cond_0
    iget-object v2, p0, Lozb;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 128
    const/4 v2, 0x3

    iget-object v3, p0, Lozb;->b:Ljava/lang/String;

    .line 129
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 131
    :cond_1
    iget-object v2, p0, Lozb;->g:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 132
    const/4 v2, 0x4

    iget-object v3, p0, Lozb;->g:Ljava/lang/String;

    .line 133
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 135
    :cond_2
    iget-object v2, p0, Lozb;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 136
    const/4 v2, 0x5

    iget-object v3, p0, Lozb;->h:Ljava/lang/String;

    .line 137
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 139
    :cond_3
    iget-object v2, p0, Lozb;->i:Lpdi;

    if-eqz v2, :cond_4

    .line 140
    const/4 v2, 0x6

    iget-object v3, p0, Lozb;->i:Lpdi;

    .line 141
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 143
    :cond_4
    iget-object v2, p0, Lozb;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 144
    const/4 v2, 0x7

    iget-object v3, p0, Lozb;->j:Ljava/lang/String;

    .line 145
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 147
    :cond_5
    iget-object v2, p0, Lozb;->k:[Loya;

    if-eqz v2, :cond_7

    .line 148
    iget-object v3, p0, Lozb;->k:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 149
    if-eqz v5, :cond_6

    .line 150
    const/16 v6, 0x8

    .line 151
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 148
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 155
    :cond_7
    iget-object v2, p0, Lozb;->l:Loya;

    if-eqz v2, :cond_8

    .line 156
    const/16 v2, 0x9

    iget-object v3, p0, Lozb;->l:Loya;

    .line 157
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 159
    :cond_8
    iget-object v2, p0, Lozb;->m:[Loya;

    if-eqz v2, :cond_a

    .line 160
    iget-object v2, p0, Lozb;->m:[Loya;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 161
    if-eqz v4, :cond_9

    .line 162
    const/16 v5, 0xb

    .line 163
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 160
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 167
    :cond_a
    iget-object v1, p0, Lozb;->n:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 168
    const/16 v1, 0xc

    iget-object v2, p0, Lozb;->n:Ljava/lang/String;

    .line 169
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_b
    iget-object v1, p0, Lozb;->c:Ljava/lang/Double;

    if-eqz v1, :cond_c

    .line 172
    const/16 v1, 0x24

    iget-object v2, p0, Lozb;->c:Ljava/lang/Double;

    .line 173
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 175
    :cond_c
    iget-object v1, p0, Lozb;->d:Ljava/lang/Double;

    if-eqz v1, :cond_d

    .line 176
    const/16 v1, 0x25

    iget-object v2, p0, Lozb;->d:Ljava/lang/Double;

    .line 177
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 179
    :cond_d
    iget-object v1, p0, Lozb;->o:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 180
    const/16 v1, 0x4b

    iget-object v2, p0, Lozb;->o:Ljava/lang/String;

    .line 181
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :cond_e
    iget-object v1, p0, Lozb;->p:Loya;

    if-eqz v1, :cond_f

    .line 184
    const/16 v1, 0xb9

    iget-object v2, p0, Lozb;->p:Loya;

    .line 185
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_f
    iget-object v1, p0, Lozb;->q:Ljava/lang/String;

    if-eqz v1, :cond_10

    .line 188
    const/16 v1, 0xfe

    iget-object v2, p0, Lozb;->q:Ljava/lang/String;

    .line 189
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 191
    :cond_10
    iget-object v1, p0, Lozb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    iput v0, p0, Lozb;->ai:I

    .line 193
    return v0

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lozb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 201
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 202
    sparse-switch v0, :sswitch_data_0

    .line 206
    iget-object v2, p0, Lozb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 207
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lozb;->ah:Ljava/util/List;

    .line 210
    :cond_1
    iget-object v2, p0, Lozb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    :sswitch_0
    return-object p0

    .line 217
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozb;->e:Ljava/lang/String;

    goto :goto_0

    .line 221
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozb;->f:Ljava/lang/String;

    goto :goto_0

    .line 225
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozb;->b:Ljava/lang/String;

    goto :goto_0

    .line 229
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozb;->g:Ljava/lang/String;

    goto :goto_0

    .line 233
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozb;->h:Ljava/lang/String;

    goto :goto_0

    .line 237
    :sswitch_6
    iget-object v0, p0, Lozb;->i:Lpdi;

    if-nez v0, :cond_2

    .line 238
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lozb;->i:Lpdi;

    .line 240
    :cond_2
    iget-object v0, p0, Lozb;->i:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 244
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozb;->j:Ljava/lang/String;

    goto :goto_0

    .line 248
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 249
    iget-object v0, p0, Lozb;->k:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 250
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 251
    iget-object v3, p0, Lozb;->k:[Loya;

    if-eqz v3, :cond_3

    .line 252
    iget-object v3, p0, Lozb;->k:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 254
    :cond_3
    iput-object v2, p0, Lozb;->k:[Loya;

    .line 255
    :goto_2
    iget-object v2, p0, Lozb;->k:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 256
    iget-object v2, p0, Lozb;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 257
    iget-object v2, p0, Lozb;->k:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 258
    invoke-virtual {p1}, Loxn;->a()I

    .line 255
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 249
    :cond_4
    iget-object v0, p0, Lozb;->k:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 261
    :cond_5
    iget-object v2, p0, Lozb;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 262
    iget-object v2, p0, Lozb;->k:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 266
    :sswitch_9
    iget-object v0, p0, Lozb;->l:Loya;

    if-nez v0, :cond_6

    .line 267
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozb;->l:Loya;

    .line 269
    :cond_6
    iget-object v0, p0, Lozb;->l:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 273
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 274
    iget-object v0, p0, Lozb;->m:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 275
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 276
    iget-object v3, p0, Lozb;->m:[Loya;

    if-eqz v3, :cond_7

    .line 277
    iget-object v3, p0, Lozb;->m:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 279
    :cond_7
    iput-object v2, p0, Lozb;->m:[Loya;

    .line 280
    :goto_4
    iget-object v2, p0, Lozb;->m:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 281
    iget-object v2, p0, Lozb;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 282
    iget-object v2, p0, Lozb;->m:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 283
    invoke-virtual {p1}, Loxn;->a()I

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 274
    :cond_8
    iget-object v0, p0, Lozb;->m:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 286
    :cond_9
    iget-object v2, p0, Lozb;->m:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 287
    iget-object v2, p0, Lozb;->m:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 291
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozb;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 295
    :sswitch_c
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lozb;->c:Ljava/lang/Double;

    goto/16 :goto_0

    .line 299
    :sswitch_d
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lozb;->d:Ljava/lang/Double;

    goto/16 :goto_0

    .line 303
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozb;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 307
    :sswitch_f
    iget-object v0, p0, Lozb;->p:Loya;

    if-nez v0, :cond_a

    .line 308
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozb;->p:Loya;

    .line 310
    :cond_a
    iget-object v0, p0, Lozb;->p:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 314
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozb;->q:Ljava/lang/String;

    goto/16 :goto_0

    .line 202
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x121 -> :sswitch_c
        0x129 -> :sswitch_d
        0x25a -> :sswitch_e
        0x5ca -> :sswitch_f
        0x7f2 -> :sswitch_10
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Lozb;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 57
    const/4 v1, 0x1

    iget-object v2, p0, Lozb;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 59
    :cond_0
    iget-object v1, p0, Lozb;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 60
    const/4 v1, 0x2

    iget-object v2, p0, Lozb;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 62
    :cond_1
    iget-object v1, p0, Lozb;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 63
    const/4 v1, 0x3

    iget-object v2, p0, Lozb;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 65
    :cond_2
    iget-object v1, p0, Lozb;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 66
    const/4 v1, 0x4

    iget-object v2, p0, Lozb;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 68
    :cond_3
    iget-object v1, p0, Lozb;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 69
    const/4 v1, 0x5

    iget-object v2, p0, Lozb;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 71
    :cond_4
    iget-object v1, p0, Lozb;->i:Lpdi;

    if-eqz v1, :cond_5

    .line 72
    const/4 v1, 0x6

    iget-object v2, p0, Lozb;->i:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 74
    :cond_5
    iget-object v1, p0, Lozb;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 75
    const/4 v1, 0x7

    iget-object v2, p0, Lozb;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 77
    :cond_6
    iget-object v1, p0, Lozb;->k:[Loya;

    if-eqz v1, :cond_8

    .line 78
    iget-object v2, p0, Lozb;->k:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 79
    if-eqz v4, :cond_7

    .line 80
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 78
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_8
    iget-object v1, p0, Lozb;->l:Loya;

    if-eqz v1, :cond_9

    .line 85
    const/16 v1, 0x9

    iget-object v2, p0, Lozb;->l:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 87
    :cond_9
    iget-object v1, p0, Lozb;->m:[Loya;

    if-eqz v1, :cond_b

    .line 88
    iget-object v1, p0, Lozb;->m:[Loya;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 89
    if-eqz v3, :cond_a

    .line 90
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 88
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 94
    :cond_b
    iget-object v0, p0, Lozb;->n:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 95
    const/16 v0, 0xc

    iget-object v1, p0, Lozb;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 97
    :cond_c
    iget-object v0, p0, Lozb;->c:Ljava/lang/Double;

    if-eqz v0, :cond_d

    .line 98
    const/16 v0, 0x24

    iget-object v1, p0, Lozb;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 100
    :cond_d
    iget-object v0, p0, Lozb;->d:Ljava/lang/Double;

    if-eqz v0, :cond_e

    .line 101
    const/16 v0, 0x25

    iget-object v1, p0, Lozb;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 103
    :cond_e
    iget-object v0, p0, Lozb;->o:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 104
    const/16 v0, 0x4b

    iget-object v1, p0, Lozb;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 106
    :cond_f
    iget-object v0, p0, Lozb;->p:Loya;

    if-eqz v0, :cond_10

    .line 107
    const/16 v0, 0xb9

    iget-object v1, p0, Lozb;->p:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 109
    :cond_10
    iget-object v0, p0, Lozb;->q:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 110
    const/16 v0, 0xfe

    iget-object v1, p0, Lozb;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 112
    :cond_11
    iget-object v0, p0, Lozb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 114
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lozb;->a(Loxn;)Lozb;

    move-result-object v0

    return-object v0
.end method
