.class public final Llvn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Llvb;

.field public b:Llvg;

.field public c:Llvc;

.field public d:Llva;

.field private e:Llwa;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12
    iput-object v0, p0, Llvn;->a:Llvb;

    .line 15
    iput-object v0, p0, Llvn;->b:Llvg;

    .line 18
    iput-object v0, p0, Llvn;->c:Llvc;

    .line 21
    iput-object v0, p0, Llvn;->e:Llwa;

    .line 26
    iput-object v0, p0, Llvn;->d:Llva;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Llvn;->a:Llvb;

    if-eqz v1, :cond_0

    .line 57
    const/4 v0, 0x1

    iget-object v1, p0, Llvn;->a:Llvb;

    .line 58
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 60
    :cond_0
    iget-object v1, p0, Llvn;->b:Llvg;

    if-eqz v1, :cond_1

    .line 61
    const/4 v1, 0x2

    iget-object v2, p0, Llvn;->b:Llvg;

    .line 62
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_1
    iget-object v1, p0, Llvn;->c:Llvc;

    if-eqz v1, :cond_2

    .line 65
    const/4 v1, 0x3

    iget-object v2, p0, Llvn;->c:Llvc;

    .line 66
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_2
    iget-object v1, p0, Llvn;->e:Llwa;

    if-eqz v1, :cond_3

    .line 69
    const/4 v1, 0x4

    iget-object v2, p0, Llvn;->e:Llwa;

    .line 70
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_3
    iget-object v1, p0, Llvn;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 73
    const/4 v1, 0x5

    iget-object v2, p0, Llvn;->f:Ljava/lang/String;

    .line 74
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_4
    iget-object v1, p0, Llvn;->d:Llva;

    if-eqz v1, :cond_5

    .line 77
    const/4 v1, 0x6

    iget-object v2, p0, Llvn;->d:Llva;

    .line 78
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_5
    iget-object v1, p0, Llvn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    iput v0, p0, Llvn;->ai:I

    .line 82
    return v0
.end method

.method public a(Loxn;)Llvn;
    .locals 2

    .prologue
    .line 90
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 91
    sparse-switch v0, :sswitch_data_0

    .line 95
    iget-object v1, p0, Llvn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 96
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llvn;->ah:Ljava/util/List;

    .line 99
    :cond_1
    iget-object v1, p0, Llvn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    :sswitch_0
    return-object p0

    .line 106
    :sswitch_1
    iget-object v0, p0, Llvn;->a:Llvb;

    if-nez v0, :cond_2

    .line 107
    new-instance v0, Llvb;

    invoke-direct {v0}, Llvb;-><init>()V

    iput-object v0, p0, Llvn;->a:Llvb;

    .line 109
    :cond_2
    iget-object v0, p0, Llvn;->a:Llvb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 113
    :sswitch_2
    iget-object v0, p0, Llvn;->b:Llvg;

    if-nez v0, :cond_3

    .line 114
    new-instance v0, Llvg;

    invoke-direct {v0}, Llvg;-><init>()V

    iput-object v0, p0, Llvn;->b:Llvg;

    .line 116
    :cond_3
    iget-object v0, p0, Llvn;->b:Llvg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 120
    :sswitch_3
    iget-object v0, p0, Llvn;->c:Llvc;

    if-nez v0, :cond_4

    .line 121
    new-instance v0, Llvc;

    invoke-direct {v0}, Llvc;-><init>()V

    iput-object v0, p0, Llvn;->c:Llvc;

    .line 123
    :cond_4
    iget-object v0, p0, Llvn;->c:Llvc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 127
    :sswitch_4
    iget-object v0, p0, Llvn;->e:Llwa;

    if-nez v0, :cond_5

    .line 128
    new-instance v0, Llwa;

    invoke-direct {v0}, Llwa;-><init>()V

    iput-object v0, p0, Llvn;->e:Llwa;

    .line 130
    :cond_5
    iget-object v0, p0, Llvn;->e:Llwa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 134
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llvn;->f:Ljava/lang/String;

    goto :goto_0

    .line 138
    :sswitch_6
    iget-object v0, p0, Llvn;->d:Llva;

    if-nez v0, :cond_6

    .line 139
    new-instance v0, Llva;

    invoke-direct {v0}, Llva;-><init>()V

    iput-object v0, p0, Llvn;->d:Llva;

    .line 141
    :cond_6
    iget-object v0, p0, Llvn;->d:Llva;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 91
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Llvn;->a:Llvb;

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x1

    iget-object v1, p0, Llvn;->a:Llvb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 34
    :cond_0
    iget-object v0, p0, Llvn;->b:Llvg;

    if-eqz v0, :cond_1

    .line 35
    const/4 v0, 0x2

    iget-object v1, p0, Llvn;->b:Llvg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_1
    iget-object v0, p0, Llvn;->c:Llvc;

    if-eqz v0, :cond_2

    .line 38
    const/4 v0, 0x3

    iget-object v1, p0, Llvn;->c:Llvc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 40
    :cond_2
    iget-object v0, p0, Llvn;->e:Llwa;

    if-eqz v0, :cond_3

    .line 41
    const/4 v0, 0x4

    iget-object v1, p0, Llvn;->e:Llwa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 43
    :cond_3
    iget-object v0, p0, Llvn;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 44
    const/4 v0, 0x5

    iget-object v1, p0, Llvn;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    :cond_4
    iget-object v0, p0, Llvn;->d:Llva;

    if-eqz v0, :cond_5

    .line 47
    const/4 v0, 0x6

    iget-object v1, p0, Llvn;->d:Llva;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 49
    :cond_5
    iget-object v0, p0, Llvn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 51
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llvn;->a(Loxn;)Llvn;

    move-result-object v0

    return-object v0
.end method
