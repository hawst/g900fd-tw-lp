.class public final Ldph;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:[J

.field private final d:Z

.field private final e:Z

.field private final f:Z

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;JZZZLjava/lang/String;)V
    .locals 10

    .prologue
    .line 43
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x1

    new-array v4, v0, [J

    const/4 v0, 0x0

    aput-wide p4, v4, v0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move/from16 v5, p6

    move/from16 v6, p7

    move/from16 v7, p8

    move-object/from16 v8, p9

    invoke-direct/range {v0 .. v8}, Ldph;-><init>(Landroid/content/Context;ILjava/util/List;[JZZZLjava/lang/String;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/List;[JZZZLjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[JZZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    const-string v0, "MarkGunsNotificationsReadTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 61
    iput p2, p0, Ldph;->a:I

    .line 62
    iput-object p3, p0, Ldph;->b:Ljava/util/List;

    .line 63
    iput-object p4, p0, Ldph;->c:[J

    .line 64
    iput-boolean p5, p0, Ldph;->d:Z

    .line 65
    iput-boolean p6, p0, Ldph;->e:Z

    .line 66
    iput-boolean p7, p0, Ldph;->f:Z

    .line 67
    iput-object p8, p0, Ldph;->h:Ljava/lang/String;

    .line 68
    return-void
.end method


# virtual methods
.method public a()Lhoz;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 72
    invoke-virtual {p0}, Ldph;->f()Landroid/content/Context;

    move-result-object v1

    .line 74
    iget v0, p0, Ldph;->a:I

    invoke-static {v1, v0}, Ldsf;->b(Landroid/content/Context;I)V

    .line 76
    iget v0, p0, Ldph;->a:I

    invoke-static {v1, v0}, Lfhu;->a(Landroid/content/Context;I)V

    .line 78
    iget-object v0, p0, Ldph;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldph;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 81
    :goto_0
    if-nez v0, :cond_3

    .line 83
    iget v0, p0, Ldph;->a:I

    iget-object v2, p0, Ldph;->b:Ljava/util/List;

    iget-boolean v3, p0, Ldph;->d:Z

    iget-boolean v4, p0, Ldph;->e:Z

    invoke-static {v1, v0, v2, v3, v4}, Ldsf;->a(Landroid/content/Context;ILjava/util/List;ZZ)V

    .line 86
    new-instance v0, Ldnf;

    iget v2, p0, Ldph;->a:I

    iget-object v3, p0, Ldph;->b:Ljava/util/List;

    iget-object v4, p0, Ldph;->c:[J

    const/4 v5, 0x2

    iget-object v6, p0, Ldph;->h:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Ldnf;-><init>(Landroid/content/Context;ILjava/util/List;[JILjava/lang/String;)V

    .line 88
    invoke-virtual {v0}, Lkff;->l()V

    .line 89
    new-instance v2, Lhoz;

    iget v3, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v2, v3, v0, v7}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v2

    .line 94
    :goto_1
    iget-boolean v2, p0, Ldph;->f:Z

    if-eqz v2, :cond_1

    .line 95
    new-instance v2, Ldjp;

    iget v3, p0, Ldph;->a:I

    invoke-direct {v2, v1, v3, v7}, Ldjp;-><init>(Landroid/content/Context;ILkfp;)V

    .line 97
    invoke-virtual {v2}, Ldjp;->l()V

    .line 100
    :cond_1
    return-object v0

    .line 78
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 91
    :cond_3
    new-instance v0, Lhoz;

    invoke-direct {v0, v2}, Lhoz;-><init>(Z)V

    goto :goto_1
.end method
