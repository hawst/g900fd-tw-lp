.class public final Liuq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ling;

.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Liup;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:I

.field private final e:I

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Liup;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Linl;",
            "Liup;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Liup;",
            ">;"
        }
    .end annotation
.end field

.field private i:Limp;

.field private j:Limu;

.field private k:Landroid/graphics/Rect;

.field private l:I

.field private final m:Lius;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 42
    new-instance v0, Ling;

    invoke-direct {v0, v2, v3, v2, v3}, Ling;-><init>(DD)V

    sput-object v0, Liuq;->a:Ling;

    .line 54
    new-instance v0, Liur;

    invoke-direct {v0}, Liur;-><init>()V

    sput-object v0, Liuq;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/util/DisplayMetrics;)V
    .locals 7

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Liuq;->f:Ljava/util/List;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Liuq;->g:Ljava/util/Map;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Liuq;->h:Ljava/util/Map;

    .line 77
    new-instance v0, Lius;

    invoke-direct {v0}, Lius;-><init>()V

    iput-object v0, p0, Liuq;->m:Lius;

    .line 81
    iput-object p1, p0, Liuq;->c:Landroid/content/Context;

    .line 82
    iput p2, p0, Liuq;->d:I

    .line 85
    invoke-static {p1}, Llsg;->a(Landroid/content/Context;)I

    move-result v0

    .line 86
    const/16 v1, 0x30

    if-ge v0, v1, :cond_2

    .line 87
    const/high16 v0, 0x200000

    .line 94
    :goto_0
    invoke-static {p1}, Liut;->b(Landroid/content/Context;)V

    sget v1, Liut;->b:I

    mul-int/lit8 v1, v1, 0x4

    invoke-static {p1}, Liut;->b(Landroid/content/Context;)V

    sget v2, Liut;->c:I

    mul-int/2addr v1, v2

    .line 98
    mul-int/lit8 v2, v1, 0x2

    div-int v2, v0, v2

    iput v2, p0, Liuq;->e:I

    .line 100
    const-string v2, "MarkerClusterManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    iget v2, p0, Liuq;->e:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x53

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "totalMarkerIconBytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " markerIconBytes="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " maxMarkers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    iget v0, p3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, p3, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v2, p0, Liuq;->c:Landroid/content/Context;

    invoke-static {v2}, Liut;->b(Landroid/content/Context;)V

    sget v2, Liut;->b:I

    iget-object v3, p0, Liuq;->c:Landroid/content/Context;

    invoke-static {v3}, Liut;->b(Landroid/content/Context;)V

    sget v3, Liut;->c:I

    new-instance v4, Landroid/graphics/Rect;

    neg-int v5, v2

    neg-int v6, v3

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-direct {v4, v5, v6, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Liuq;->k:Landroid/graphics/Rect;

    const-string v0, "MarkerClusterManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Liuq;->k:Landroid/graphics/Rect;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "mPaddedMapRect = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    :cond_1
    return-void

    .line 88
    :cond_2
    const/16 v1, 0x40

    if-ge v0, v1, :cond_3

    .line 89
    const/high16 v0, 0x800000

    goto/16 :goto_0

    .line 91
    :cond_3
    const/high16 v0, 0x1400000

    goto/16 :goto_0
.end method

.method private a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Liup;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Liup;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 427
    iget-object v1, p0, Liuq;->j:Limu;

    invoke-interface {v1}, Limu;->a()Linn;

    move-result-object v1

    iget-object v2, v1, Linn;->c:Ling;

    sget-object v3, Liuq;->a:Ling;

    invoke-virtual {v2, v3}, Ling;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Linn;->d:Ling;

    sget-object v3, Liuq;->a:Ling;

    invoke-virtual {v2, v3}, Ling;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Linn;->a:Ling;

    sget-object v3, Liuq;->a:Ling;

    invoke-virtual {v2, v3}, Ling;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Linn;->b:Ling;

    sget-object v2, Liuq;->a:Ling;

    invoke-virtual {v1, v2}, Ling;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_2

    .line 428
    const/4 v0, 0x0

    .line 460
    :goto_1
    return-object v0

    :cond_1
    move v1, v0

    .line 427
    goto :goto_0

    .line 431
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    .line 432
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 434
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    .line 435
    :goto_2
    if-ge v2, v4, :cond_5

    .line 436
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 438
    iget-object v1, v0, Lnhm;->c:[Lnij;

    invoke-static {v1}, Liuo;->a([Lnij;)Lnij;

    move-result-object v1

    .line 439
    if-eqz v1, :cond_3

    iget-object v5, v1, Lnij;->c:Ljava/lang/Double;

    if-eqz v5, :cond_3

    iget-object v5, v1, Lnij;->d:Ljava/lang/Double;

    if-eqz v5, :cond_3

    .line 440
    new-instance v5, Ling;

    iget-object v6, v1, Lnij;->c:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    iget-object v1, v1, Lnij;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Ling;-><init>(DD)V

    .line 444
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liup;

    .line 445
    if-nez v1, :cond_4

    .line 446
    iget-object v1, p0, Liuq;->j:Limu;

    invoke-interface {v1, v5}, Limu;->a(Ling;)Landroid/graphics/Point;

    move-result-object v6

    .line 447
    new-instance v1, Liup;

    iget-object v7, p0, Liuq;->c:Landroid/content/Context;

    invoke-direct {v1, v7, v0, v5, v6}, Liup;-><init>(Landroid/content/Context;Lnhm;Ling;Landroid/graphics/Point;)V

    .line 448
    invoke-interface {v3, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    :goto_3
    iget-object v5, v0, Lnhm;->b:Ljava/lang/String;

    invoke-interface {p2, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    const-string v5, "MarkerClusterManager"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 455
    iget-object v0, v0, Lnhm;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Liup;->d()Ling;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 456
    invoke-virtual {v1}, Liup;->e()Landroid/graphics/Point;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x4

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_2

    .line 450
    :cond_4
    invoke-virtual {v1, v0}, Liup;->a(Lnhm;)V

    goto :goto_3

    .line 460
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/16 :goto_1
.end method

.method private a(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 333
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 334
    iget-object v0, v0, Lnhm;->c:[Lnij;

    invoke-static {v0}, Liuo;->b([Lnij;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 335
    const/4 v0, 0x0

    .line 338
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Ljava/util/List;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Liup;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 255
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 256
    if-eqz p2, :cond_0

    .line 257
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x19

    if-gt v0, v1, :cond_0

    move v2, v3

    .line 259
    :goto_0
    new-instance v5, Ljava/util/HashMap;

    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    .line 260
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 261
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    .line 262
    invoke-virtual {v0}, Liup;->g()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v5, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    move v2, v4

    .line 257
    goto :goto_0

    .line 264
    :cond_1
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 265
    iget-object v0, p0, Liuq;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 267
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    .line 268
    invoke-virtual {v0}, Liup;->g()Ljava/lang/String;

    move-result-object v1

    .line 271
    invoke-interface {v5, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 274
    invoke-interface {v5, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liup;

    .line 276
    invoke-virtual {v1}, Liup;->a()Linl;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 277
    invoke-virtual {v1}, Liup;->b()Liut;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 278
    invoke-virtual {v0, v1}, Liup;->a(Liup;)V

    .line 279
    iget-object v1, p0, Liuq;->g:Ljava/util/Map;

    invoke-virtual {v0}, Liup;->a()Linl;

    move-result-object v9

    invoke-interface {v1, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v3

    .line 287
    :goto_3
    if-eqz v2, :cond_2

    if-nez v1, :cond_2

    .line 288
    invoke-virtual {v0, v10}, Liup;->a(I)V

    .line 291
    :cond_2
    iget-object v1, p0, Liuq;->m:Lius;

    invoke-virtual {v0, v1}, Liup;->a(Ljava/util/Comparator;)V

    .line 292
    iget-object v1, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 283
    :cond_3
    invoke-virtual {v1}, Liup;->h()V

    :cond_4
    move v1, v4

    goto :goto_3

    .line 295
    :cond_5
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    .line 296
    invoke-virtual {v0}, Liup;->h()V

    goto :goto_4

    .line 299
    :cond_6
    invoke-direct {p0}, Liuq;->e()V

    .line 301
    const-string v0, "MarkerClusterManager"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 302
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v6

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "updateClusters: duration="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 304
    :cond_7
    return-void
.end method

.method private b(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 472
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_1

    .line 473
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 474
    iget-object v1, p0, Liuq;->c:Landroid/content/Context;

    const-class v3, Lhei;

    invoke-static {v1, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    iget v3, p0, Liuq;->d:I

    invoke-interface {v1, v3}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v3, "gaia_id"

    .line 475
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 476
    iget-object v0, v0, Lnhm;->b:Ljava/lang/String;

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 477
    const/4 v0, 0x1

    .line 480
    :goto_1
    return v0

    .line 472
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 480
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private e()V
    .locals 5

    .prologue
    .line 307
    iget-object v0, p0, Liuq;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 308
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    .line 309
    invoke-virtual {v0}, Liup;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnhm;

    .line 310
    iget-object v4, p0, Liuq;->h:Ljava/util/Map;

    iget-object v1, v1, Lnhm;->b:Ljava/lang/String;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 313
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Linl;)Liup;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Liuq;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Liup;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Liuq;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    return-object v0
.end method

.method public a()V
    .locals 6

    .prologue
    .line 175
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 176
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    .line 177
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Liup;->a(I)V

    goto :goto_0

    .line 180
    :cond_0
    const-string v0, "MarkerClusterManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 182
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x40

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "deselectCluster: count="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 181
    :cond_1
    return-void
.end method

.method public a(Limp;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Liuq;->i:Limp;

    .line 111
    return-void
.end method

.method public a(Liup;)V
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x1

    .line 153
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 154
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x19

    if-gt v0, v1, :cond_0

    move v1, v2

    .line 155
    :goto_0
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    .line 156
    if-ne v0, p1, :cond_1

    .line 157
    const/4 v3, 0x2

    move v8, v3

    move-object v3, v0

    move v0, v8

    .line 160
    :goto_2
    invoke-virtual {v3, v0}, Liup;->a(I)V

    goto :goto_1

    .line 154
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 160
    :cond_1
    if-eqz v1, :cond_2

    move-object v3, v0

    move v0, v4

    goto :goto_2

    :cond_2
    move-object v3, v0

    move v0, v2

    goto :goto_2

    .line 165
    :cond_3
    const-string v0, "MarkerClusterManager"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 166
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 167
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v6

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x3e

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "selectCluster: count="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 166
    :cond_4
    return-void
.end method

.method public a(Z)V
    .locals 10

    .prologue
    .line 227
    invoke-virtual {p0}, Liuq;->d()Z

    move-result v2

    .line 229
    if-eqz p1, :cond_0

    if-eqz v2, :cond_0

    .line 230
    iget-object v0, p0, Liuq;->i:Limp;

    invoke-interface {v0}, Limp;->a()Limu;

    move-result-object v0

    iput-object v0, p0, Liuq;->j:Limu;

    .line 233
    :cond_0
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 234
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    .line 235
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    .line 236
    if-eqz v2, :cond_2

    iget-object v4, p0, Liuq;->j:Limu;

    invoke-virtual {v0, v4}, Liup;->a(Limu;)V

    invoke-virtual {v0}, Liup;->e()Landroid/graphics/Point;

    move-result-object v4

    iget-object v5, p0, Liuq;->k:Landroid/graphics/Rect;

    iget v6, v4, Landroid/graphics/Point;->x:I

    iget v7, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    const-string v6, "MarkerClusterManager"

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1f

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " screenPosition="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " onScreen="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz v5, :cond_4

    .line 237
    :cond_2
    invoke-virtual {v0}, Liup;->a()Linl;

    move-result-object v4

    if-nez v4, :cond_3

    .line 238
    invoke-virtual {v0}, Liup;->f()Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v4}, Liuq;->a(Ljava/util/List;)Z

    move-result v5

    new-instance v6, Liut;

    iget-object v7, p0, Liuq;->c:Landroid/content/Context;

    invoke-direct {p0, v4}, Liuq;->b(Ljava/util/List;)Z

    move-result v8

    invoke-direct {v6, v7, v4, v5, v8}, Liut;-><init>(Landroid/content/Context;Ljava/util/List;ZZ)V

    invoke-virtual {v0}, Liup;->c()I

    move-result v4

    invoke-virtual {v6, v4}, Liut;->a(I)V

    invoke-virtual {v6}, Liut;->e()Landroid/graphics/Bitmap;

    move-result-object v4

    iget-object v5, p0, Liuq;->c:Landroid/content/Context;

    invoke-static {v5}, Liut;->b(Landroid/content/Context;)V

    sget-object v5, Liut;->d:Landroid/graphics/PointF;

    iget-object v7, p0, Liuq;->i:Limp;

    invoke-virtual {v0}, Liup;->d()Ling;

    move-result-object v8

    iget v9, v5, Landroid/graphics/PointF;->x:F

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-interface {v7, v8, v4, v9, v5}, Limp;->a(Ling;Landroid/graphics/Bitmap;FF)Linl;

    move-result-object v4

    invoke-virtual {v0, v4}, Liup;->a(Linl;)V

    invoke-virtual {v0, v6}, Liup;->a(Liut;)V

    iget-object v5, p0, Liuq;->g:Ljava/util/Map;

    invoke-interface {v5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    :cond_3
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 241
    :cond_4
    invoke-virtual {v0}, Liup;->a()Linl;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 242
    invoke-virtual {v0}, Liup;->h()V

    goto :goto_1

    .line 246
    :cond_5
    return-void
.end method

.method public a(Ljava/util/List;Z)Z
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 132
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Liuq;->l:I

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Liuq;->i:Limp;

    invoke-interface {v2}, Limp;->a()Limu;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Liuq;->j:Limu;

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Liuq;->m:Lius;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lius;->a(Ljava/util/List;)V

    .line 137
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Liuq;->c:Landroid/content/Context;

    invoke-static {v2}, Liut;->b(Landroid/content/Context;)V

    sget v11, Liut;->a:I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v10}, Liuq;->a(Ljava/util/List;Ljava/util/Map;)Ljava/util/List;

    move-result-object v12

    if-nez v12, :cond_0

    const/4 v2, 0x0

    .line 138
    :goto_0
    if-nez v2, :cond_8

    .line 139
    const/4 v2, 0x0

    .line 145
    :goto_1
    return v2

    .line 137
    :cond_0
    sget-object v2, Liuq;->b:Ljava/util/Comparator;

    invoke-static {v12, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const-string v2, "MarkerClusterManager"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "initial sorted clusters: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v13

    const/4 v2, 0x0

    move v7, v2

    :goto_2
    if-ge v7, v13, :cond_4

    invoke-interface {v12, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liup;

    invoke-virtual {v2}, Liup;->f()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnhm;

    iget-object v3, v3, Lnhm;->b:Ljava/lang/String;

    invoke-interface {v10, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Liup;

    add-int/lit8 v4, v7, 0x1

    move v6, v4

    :goto_3
    if-ge v6, v13, :cond_3

    invoke-interface {v12, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Liup;

    invoke-virtual {v2}, Liup;->e()Landroid/graphics/Point;

    move-result-object v14

    invoke-virtual {v4}, Liup;->e()Landroid/graphics/Point;

    move-result-object v15

    iget v0, v14, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    iget v0, v15, Landroid/graphics/Point;->y:I

    move/from16 v17, v0

    sub-int v16, v16, v17

    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(I)I

    move-result v16

    move/from16 v0, v16

    if-gt v0, v11, :cond_3

    iget v14, v14, Landroid/graphics/Point;->x:I

    iget v15, v15, Landroid/graphics/Point;->x:I

    sub-int/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v14

    if-gt v14, v11, :cond_2

    invoke-virtual {v4}, Liup;->f()Ljava/util/List;

    move-result-object v4

    const/4 v14, 0x0

    invoke-interface {v4, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnhm;

    iget-object v4, v4, Lnhm;->b:Ljava/lang/String;

    invoke-interface {v10, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Liup;

    invoke-virtual {v3, v4, v10}, Liup;->a(Liup;Ljava/util/Map;)V

    :cond_2
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_3

    :cond_3
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_2

    :cond_4
    new-instance v2, Ljava/util/HashSet;

    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liup;

    invoke-virtual {v2}, Liup;->i()V

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v4, "MarkerClusterManager"

    const/4 v6, 0x2

    invoke-static {v4, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xd

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Add cluster: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_6
    const-string v2, "MarkerClusterManager"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v8

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v8, 0x5b

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "calculateUserClusters: clusters="

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " users="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " duration="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_7
    move-object v2, v5

    goto/16 :goto_0

    .line 141
    :cond_8
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v2, v1}, Liuq;->b(Ljava/util/List;Z)V

    .line 143
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Liuq;->a(Z)V

    .line 145
    const/4 v2, 0x1

    goto/16 :goto_1
.end method

.method public b(Ljava/lang/String;)Liup;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 195
    if-nez p1, :cond_1

    move-object v0, v1

    .line 204
    :cond_0
    :goto_0
    return-object v0

    .line 198
    :cond_1
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_2

    .line 199
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    .line 200
    invoke-virtual {v0}, Liup;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 198
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 204
    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Liuq;->i:Limp;

    if-nez v0, :cond_0

    .line 216
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liup;

    .line 213
    invoke-virtual {v0}, Liup;->h()V

    goto :goto_1

    .line 215
    :cond_1
    iget-object v0, p0, Liuq;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Liuq;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 223
    iget v0, p0, Liuq;->l:I

    iget v1, p0, Liuq;->e:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
