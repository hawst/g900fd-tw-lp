.class public final Lqbj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 752
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 768
    const/4 v0, 0x0

    .line 769
    iget-object v1, p0, Lqbj;->a:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 770
    const/4 v0, 0x1

    iget-object v1, p0, Lqbj;->a:Ljava/lang/Double;

    .line 771
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 773
    :cond_0
    iget-object v1, p0, Lqbj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 774
    iput v0, p0, Lqbj;->ai:I

    .line 775
    return v0
.end method

.method public a(Loxn;)Lqbj;
    .locals 2

    .prologue
    .line 783
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 784
    sparse-switch v0, :sswitch_data_0

    .line 788
    iget-object v1, p0, Lqbj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 789
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbj;->ah:Ljava/util/List;

    .line 792
    :cond_1
    iget-object v1, p0, Lqbj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 794
    :sswitch_0
    return-object p0

    .line 799
    :sswitch_1
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lqbj;->a:Ljava/lang/Double;

    goto :goto_0

    .line 784
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 759
    iget-object v0, p0, Lqbj;->a:Ljava/lang/Double;

    if-eqz v0, :cond_0

    .line 760
    const/4 v0, 0x1

    iget-object v1, p0, Lqbj;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 762
    :cond_0
    iget-object v0, p0, Lqbj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 764
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 748
    invoke-virtual {p0, p1}, Lqbj;->a(Loxn;)Lqbj;

    move-result-object v0

    return-object v0
.end method
