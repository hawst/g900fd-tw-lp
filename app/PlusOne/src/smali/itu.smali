.class public final Litu;
.super Liuj;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Liuj;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Litu;
    .locals 1

    .prologue
    .line 42
    const/4 v0, -0x1

    invoke-static {p0, v0}, Litu;->a(Ljava/lang/String;I)Litu;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;I)Litu;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 23
    const-string v1, "custom_text_res_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 24
    const-string v1, "title_text"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    new-instance v1, Litu;

    invoke-direct {v1}, Litu;-><init>()V

    .line 27
    invoke-virtual {v1, v0}, Litu;->f(Landroid/os/Bundle;)V

    .line 28
    return-object v1
.end method


# virtual methods
.method public U()I
    .locals 1

    .prologue
    .line 47
    const v0, 0x7f1002a6

    return v0
.end method

.method public V()I
    .locals 1

    .prologue
    .line 52
    const v0, 0x7f1002a4

    return v0
.end method

.method public W()I
    .locals 1

    .prologue
    .line 57
    const v0, 0x7f10029c

    return v0
.end method

.method public k(Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 62
    invoke-virtual {p0}, Litu;->X()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 63
    const v0, 0x7f0400ae

    invoke-virtual {v1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 64
    const v0, 0x7f100118

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 65
    const-string v3, "title_text"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 66
    if-eqz v3, :cond_0

    .line 67
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :goto_0
    const v0, 0x7f1002a4

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 73
    const/high16 v3, 0x1040000

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(I)V

    .line 75
    const v0, 0x7f1002a3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 76
    const v3, 0x7f0400aa

    invoke-virtual {v1, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 78
    const v1, 0x7f100139

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 79
    const-string v4, "custom_text"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 80
    const-string v5, "custom_text_res_id"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 81
    if-eqz v4, :cond_1

    .line 82
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    :goto_1
    const v1, 0x7f10029c

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 90
    const/4 v4, 0x0

    invoke-virtual {v1, v6, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 92
    invoke-virtual {p0}, Litu;->X()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00f6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 91
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setLinkTextColor(I)V

    .line 94
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 95
    return-object v2

    .line 69
    :cond_0
    const v3, 0x7f0a03db

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 83
    :cond_1
    const/4 v4, -0x1

    if-eq v5, v4, :cond_2

    .line 84
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 86
    :cond_2
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
