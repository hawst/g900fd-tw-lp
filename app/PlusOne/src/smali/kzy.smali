.class public final Lkzy;
.super Lllq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    return-void
.end method

.method public constructor <init>(Lpar;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Lllq;-><init>()V

    .line 34
    iget-object v0, p1, Lpar;->d:Loya;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p1, Lpar;->d:Loya;

    sget-object v1, Lpab;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpab;

    .line 37
    if-eqz v0, :cond_1

    iget-object v0, v0, Lpab;->b:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lkzy;->b:Ljava/lang/String;

    .line 39
    :cond_0
    iget-object v0, p1, Lpar;->c:Ljava/lang/String;

    iput-object v0, p0, Lkzy;->c:Ljava/lang/String;

    .line 40
    iget-object v0, p1, Lpar;->b:Ljava/lang/String;

    iput-object v0, p0, Lkzy;->d:Ljava/lang/String;

    .line 41
    iget-object v0, p1, Lpar;->f:Ljava/lang/String;

    iput-object v0, p0, Lkzy;->e:Ljava/lang/String;

    .line 42
    iget-object v0, p1, Lpar;->e:Ljava/lang/String;

    iput-object v0, p0, Lkzy;->f:Ljava/lang/String;

    .line 43
    return-void

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lpau;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Lllq;-><init>()V

    .line 46
    iget-object v0, p1, Lpau;->d:Loya;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p1, Lpau;->d:Loya;

    sget-object v1, Lpab;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpab;

    .line 49
    if-eqz v0, :cond_2

    iget-object v0, v0, Lpab;->b:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lkzy;->b:Ljava/lang/String;

    .line 51
    :cond_0
    iget-object v0, p1, Lpau;->b:Ljava/lang/String;

    iput-object v0, p0, Lkzy;->a:Ljava/lang/String;

    .line 52
    iget-object v0, p1, Lpau;->c:Loya;

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p1, Lpau;->c:Loya;

    sget-object v1, Lpar;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpar;

    .line 55
    iget-object v1, v0, Lpar;->c:Ljava/lang/String;

    iput-object v1, p0, Lkzy;->c:Ljava/lang/String;

    .line 56
    iget-object v0, v0, Lpar;->b:Ljava/lang/String;

    iput-object v0, p0, Lkzy;->d:Ljava/lang/String;

    .line 58
    :cond_1
    iget-object v0, p1, Lpau;->e:Ljava/lang/String;

    iput-object v0, p0, Lkzy;->e:Ljava/lang/String;

    .line 59
    iget-object v0, p1, Lpau;->f:Ljava/lang/String;

    iput-object v0, p0, Lkzy;->f:Ljava/lang/String;

    .line 60
    return-void

    .line 49
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([B)Lkzy;
    .locals 3

    .prologue
    .line 87
    if-nez p0, :cond_0

    .line 88
    const/4 v0, 0x0

    .line 101
    :goto_0
    return-object v0

    .line 91
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 92
    new-instance v0, Lkzy;

    invoke-direct {v0}, Lkzy;-><init>()V

    .line 94
    invoke-static {v1}, Lkzy;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzy;->a:Ljava/lang/String;

    .line 95
    invoke-static {v1}, Lkzy;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzy;->b:Ljava/lang/String;

    .line 96
    invoke-static {v1}, Lkzy;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzy;->c:Ljava/lang/String;

    .line 97
    invoke-static {v1}, Lkzy;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzy;->d:Ljava/lang/String;

    .line 98
    invoke-static {v1}, Lkzy;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lkzy;->e:Ljava/lang/String;

    .line 99
    invoke-static {v1}, Lkzy;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lkzy;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lkzy;)[B
    .locals 3

    .prologue
    .line 71
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 72
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 74
    iget-object v2, p0, Lkzy;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lkzy;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 75
    iget-object v2, p0, Lkzy;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lkzy;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 76
    iget-object v2, p0, Lkzy;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lkzy;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 77
    iget-object v2, p0, Lkzy;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lkzy;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 78
    iget-object v2, p0, Lkzy;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lkzy;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 79
    iget-object v2, p0, Lkzy;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lkzy;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 82
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 83
    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lkzy;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lkzy;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lkzy;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lkzy;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lkzy;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lkzy;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lkzy;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
