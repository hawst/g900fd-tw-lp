.class final Lhzi;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:[Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhzi;->setOrientation(I)V

    .line 21
    return-void
.end method


# virtual methods
.method protected a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lhzi;->a:[Landroid/view/View;

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected a(ILandroid/view/View;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lhzi;->a:[Landroid/view/View;

    aput-object p2, v0, p1

    .line 63
    return-void
.end method

.method protected a(Lhyz;I)V
    .locals 3

    .prologue
    .line 44
    new-array v0, p2, [Landroid/view/View;

    iput-object v0, p0, Lhzi;->a:[Landroid/view/View;

    .line 45
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 46
    iget-object v1, p0, Lhzi;->a:[Landroid/view/View;

    invoke-virtual {p0}, Lhzi;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {p1, v2}, Lhyz;->b(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v0

    .line 47
    iget-object v1, p0, Lhzi;->a:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lhzi;->addView(Landroid/view/View;)V

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    return-void
.end method

.method protected b(I)V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lhzi;->a:[Landroid/view/View;

    aget-object v0, v0, p1

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 70
    return-void
.end method

.method protected c(I)V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lhzi;->a:[Landroid/view/View;

    aget-object v0, v0, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 78
    return-void
.end method
