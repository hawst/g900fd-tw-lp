.class public final Lmtt;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5433
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5469
    const/4 v0, 0x0

    .line 5470
    iget-object v1, p0, Lmtt;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 5471
    const/4 v0, 0x1

    iget-object v1, p0, Lmtt;->a:Ljava/lang/Boolean;

    .line 5472
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 5474
    :cond_0
    iget-object v1, p0, Lmtt;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 5475
    const/4 v1, 0x2

    iget-object v2, p0, Lmtt;->b:Ljava/lang/Boolean;

    .line 5476
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5478
    :cond_1
    iget-object v1, p0, Lmtt;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 5479
    const/4 v1, 0x3

    iget-object v2, p0, Lmtt;->c:Ljava/lang/Boolean;

    .line 5480
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5482
    :cond_2
    iget-object v1, p0, Lmtt;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 5483
    const/4 v1, 0x4

    iget-object v2, p0, Lmtt;->d:Ljava/lang/Boolean;

    .line 5484
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5486
    :cond_3
    iget-object v1, p0, Lmtt;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 5487
    const/4 v1, 0x5

    iget-object v2, p0, Lmtt;->e:Ljava/lang/Boolean;

    .line 5488
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5490
    :cond_4
    iget-object v1, p0, Lmtt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5491
    iput v0, p0, Lmtt;->ai:I

    .line 5492
    return v0
.end method

.method public a(Loxn;)Lmtt;
    .locals 2

    .prologue
    .line 5500
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5501
    sparse-switch v0, :sswitch_data_0

    .line 5505
    iget-object v1, p0, Lmtt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5506
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtt;->ah:Ljava/util/List;

    .line 5509
    :cond_1
    iget-object v1, p0, Lmtt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5511
    :sswitch_0
    return-object p0

    .line 5516
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtt;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 5520
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtt;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 5524
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtt;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 5528
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtt;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 5532
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmtt;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 5501
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5448
    iget-object v0, p0, Lmtt;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 5449
    const/4 v0, 0x1

    iget-object v1, p0, Lmtt;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5451
    :cond_0
    iget-object v0, p0, Lmtt;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 5452
    const/4 v0, 0x2

    iget-object v1, p0, Lmtt;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5454
    :cond_1
    iget-object v0, p0, Lmtt;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 5455
    const/4 v0, 0x3

    iget-object v1, p0, Lmtt;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5457
    :cond_2
    iget-object v0, p0, Lmtt;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 5458
    const/4 v0, 0x4

    iget-object v1, p0, Lmtt;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5460
    :cond_3
    iget-object v0, p0, Lmtt;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 5461
    const/4 v0, 0x5

    iget-object v1, p0, Lmtt;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 5463
    :cond_4
    iget-object v0, p0, Lmtt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5465
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5429
    invoke-virtual {p0, p1}, Lmtt;->a(Loxn;)Lmtt;

    move-result-object v0

    return-object v0
.end method
