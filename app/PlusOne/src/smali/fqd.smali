.class public final Lfqd;
.super Ldf;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldf",
        "<",
        "Lfob;",
        ">;"
    }
.end annotation


# static fields
.field private static final o:[Ljava/lang/String;

.field private static final p:[Ljava/lang/String;


# instance fields
.field private final b:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Lfob;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final c:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Lfob;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Z

.field private final i:Z

.field private final j:Landroid/net/Uri;

.field private k:[Landroid/graphics/Point;

.field private l:Lfob;

.field private m:Z

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "data"

    aput-object v1, v0, v2

    const-string v1, "refresh_timestamp"

    aput-object v1, v0, v3

    const-string v1, "requested_freeze"

    aput-object v1, v0, v4

    sput-object v0, Lfqd;->o:[Ljava/lang/String;

    .line 75
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "tile_id"

    aput-object v1, v0, v2

    const-string v1, "image_url"

    aput-object v1, v0, v3

    const-string v1, "data"

    aput-object v1, v0, v4

    sput-object v0, Lfqd;->p:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;)V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0, p1}, Ldf;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lfqd;->b:Ldp;

    .line 51
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lfqd;->c:Ldp;

    .line 88
    iput p2, p0, Lfqd;->d:I

    .line 89
    iput-object p3, p0, Lfqd;->e:Ljava/lang/String;

    .line 90
    iput-object p4, p0, Lfqd;->f:Ljava/lang/String;

    .line 91
    iput-object p5, p0, Lfqd;->g:Ljava/lang/String;

    .line 92
    iput-boolean p6, p0, Lfqd;->h:Z

    .line 93
    iput-boolean p7, p0, Lfqd;->i:Z

    .line 94
    iput-object p8, p0, Lfqd;->k:[Landroid/graphics/Point;

    .line 95
    sget-object v0, Lfoa;->a:Landroid/net/Uri;

    invoke-static {v0, p3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lfqd;->j:Landroid/net/Uri;

    .line 96
    return-void
.end method

.method private a(Ljava/lang/String;)Lfoc;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 246
    .line 249
    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfqd;->d:I

    invoke-static {v0, v1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 250
    if-nez v0, :cond_0

    .line 271
    :goto_0
    return-object v5

    .line 254
    :cond_0
    const-string v1, "stories"

    sget-object v2, Lfqd;->o:[Ljava/lang/String;

    const-string v3, "story_id=?"

    new-array v4, v8, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 257
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 258
    new-instance v1, Lfoc;

    new-instance v0, Lmmq;

    invoke-direct {v0}, Lmmq;-><init>()V

    const/4 v3, 0x0

    .line 259
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v0, v3}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmmq;

    invoke-direct {v1, v0}, Lfoc;-><init>(Lmmq;)V
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    const/4 v0, 0x1

    .line 261
    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lfoc;->a(J)Lfoc;

    .line 262
    const/4 v0, 0x2

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v8, :cond_1

    .line 263
    invoke-virtual {v1}, Lfoc;->b()Lfoc;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    move-object v5, v1

    .line 269
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 266
    :catch_0
    move-exception v0

    .line 267
    :goto_1
    :try_start_2
    const-string v1, "StoryLoader"

    const-string v3, "Unable to deserialize story"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 269
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 266
    :catch_1
    move-exception v0

    move-object v5, v1

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)Lfod;
    .locals 12

    .prologue
    .line 276
    if-nez p1, :cond_0

    .line 277
    new-instance v0, Lfod;

    invoke-direct {v0}, Lfod;-><init>()V

    .line 361
    :goto_0
    return-object v0

    .line 280
    :cond_0
    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfqd;->d:I

    invoke-static {v0, v1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 281
    if-nez v0, :cond_1

    .line 282
    const/4 v0, 0x0

    goto :goto_0

    .line 286
    :cond_1
    const-string v1, "all_tiles"

    sget-object v2, Lfqd;->p:[Ljava/lang/String;

    const-string v3, "view_id = ? AND type == 4"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 296
    new-instance v0, Lfod;

    invoke-direct {v0}, Lfod;-><init>()V

    .line 298
    :cond_2
    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_13

    .line 301
    const/4 v1, 0x2

    :try_start_1
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 302
    if-nez v1, :cond_8

    const/4 v1, 0x0

    :goto_2
    move-object v7, v1

    .line 307
    :goto_3
    const/4 v1, 0x0

    :try_start_2
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 308
    iget-object v2, v0, Lfod;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 309
    const-string v2, "Duplicate tileId: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 311
    :cond_3
    :goto_4
    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v7, :cond_a

    iget-object v3, v7, Lnym;->d:Ljava/lang/String;

    :goto_5
    if-eqz v7, :cond_b

    iget-object v4, v7, Lnym;->r:Loae;

    if-eqz v4, :cond_b

    iget-object v4, v7, Lnym;->r:Loae;

    iget-object v4, v4, Loae;->e:Ljava/lang/Integer;

    if-eqz v4, :cond_b

    iget-object v4, v7, Lnym;->r:Loae;

    iget-object v4, v4, Loae;->e:Ljava/lang/Integer;

    .line 314
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :goto_6
    if-eqz v7, :cond_c

    iget-object v5, v7, Lnym;->r:Loae;

    if-eqz v5, :cond_c

    iget-object v5, v7, Lnym;->r:Loae;

    iget-object v5, v5, Loae;->c:Ljava/lang/Boolean;

    if-eqz v5, :cond_c

    iget-object v5, v7, Lnym;->r:Loae;

    iget-object v5, v5, Loae;->c:Ljava/lang/Boolean;

    .line 316
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    :goto_7
    if-eqz v7, :cond_d

    iget-object v6, v7, Lnym;->k:Ljava/lang/Integer;

    if-eqz v6, :cond_d

    iget-object v6, v7, Lnym;->k:Ljava/lang/Integer;

    .line 317
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 311
    :goto_8
    invoke-virtual/range {v0 .. v6}, Lfod;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZI)V

    .line 318
    const-string v2, "StoryLoader"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lfqd;->l:Lfob;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lfqd;->l:Lfob;

    iget-object v2, v2, Lfob;->b:Lfod;

    if-eqz v2, :cond_2

    .line 320
    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 321
    if-eqz v7, :cond_e

    iget-object v2, v7, Lnym;->d:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, v7, Lnym;->d:Ljava/lang/String;

    move-object v5, v2

    .line 322
    :goto_9
    if-eqz v7, :cond_f

    iget-object v2, v7, Lnym;->r:Loae;

    if-eqz v2, :cond_f

    iget-object v2, v7, Lnym;->r:Loae;

    iget-object v2, v2, Loae;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_f

    iget-object v2, v7, Lnym;->r:Loae;

    iget-object v2, v2, Loae;->e:Ljava/lang/Integer;

    .line 323
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move v4, v2

    .line 324
    :goto_a
    if-eqz v7, :cond_10

    iget-object v2, v7, Lnym;->r:Loae;

    if-eqz v2, :cond_10

    iget-object v2, v7, Lnym;->r:Loae;

    iget-object v2, v2, Loae;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_10

    iget-object v2, v7, Lnym;->r:Loae;

    iget-object v2, v2, Loae;->c:Ljava/lang/Boolean;

    .line 326
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v3, v2

    .line 327
    :goto_b
    if-eqz v7, :cond_11

    iget-object v2, v7, Lnym;->k:Ljava/lang/Integer;

    if-eqz v2, :cond_11

    iget-object v2, v7, Lnym;->k:Ljava/lang/Integer;

    .line 328
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 330
    :goto_c
    if-nez v6, :cond_12

    iget-object v7, p0, Lfqd;->l:Lfob;

    iget-object v7, v7, Lfob;->b:Lfod;

    invoke-virtual {v7, v1}, Lfod;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 332
    :goto_d
    iget-object v7, p0, Lfqd;->l:Lfob;

    iget-object v7, v7, Lfob;->b:Lfod;

    .line 333
    invoke-virtual {v7, v1}, Lfod;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x17

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Image url changed: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " -> "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    :cond_4
    iget-object v6, p0, Lfqd;->l:Lfob;

    iget-object v6, v6, Lfob;->b:Lfod;

    invoke-virtual {v6, v1}, Lfod;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 336
    iget-object v6, p0, Lfqd;->l:Lfob;

    iget-object v6, v6, Lfob;->b:Lfod;

    .line 337
    invoke-virtual {v6, v1}, Lfod;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x1b

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Image caption changed: "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " -> "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    :cond_5
    iget-object v5, p0, Lfqd;->l:Lfob;

    iget-object v5, v5, Lfob;->b:Lfod;

    invoke-virtual {v5, v1}, Lfod;->c(Ljava/lang/String;)I

    move-result v5

    if-eq v4, v5, :cond_6

    .line 340
    iget-object v5, p0, Lfqd;->l:Lfob;

    iget-object v5, v5, Lfob;->b:Lfod;

    .line 341
    invoke-virtual {v5, v1}, Lfod;->c(Ljava/lang/String;)I

    move-result v5

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x32

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Plus one count changed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 340
    :cond_6
    iget-object v4, p0, Lfqd;->l:Lfob;

    iget-object v4, v4, Lfob;->b:Lfod;

    .line 345
    invoke-virtual {v4, v1}, Lfod;->e(Ljava/lang/String;)Z

    move-result v4

    if-eq v3, v4, :cond_7

    .line 346
    iget-object v4, p0, Lfqd;->l:Lfob;

    iget-object v4, v4, Lfob;->b:Lfod;

    .line 347
    invoke-virtual {v4, v1}, Lfod;->e(Ljava/lang/String;)Z

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x2b

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Plus oned by viewer changed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " -> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 346
    :cond_7
    iget-object v3, p0, Lfqd;->l:Lfob;

    iget-object v3, v3, Lfob;->b:Lfod;

    invoke-virtual {v3, v1}, Lfod;->d(Ljava/lang/String;)I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 351
    iget-object v3, p0, Lfqd;->l:Lfob;

    iget-object v3, v3, Lfob;->b:Lfod;

    .line 352
    invoke-virtual {v3, v1}, Lfod;->d(Ljava/lang/String;)I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x31

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Comment count changed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " -> "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 358
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 302
    :cond_8
    :try_start_3
    new-instance v2, Lnym;

    invoke-direct {v2}, Lnym;-><init>()V

    invoke-static {v2, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v1

    check-cast v1, Lnym;
    :try_end_3
    .catch Loxt; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 304
    :catch_0
    move-exception v1

    const/4 v1, 0x0

    move-object v7, v1

    goto/16 :goto_3

    .line 309
    :cond_9
    :try_start_4
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 311
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 314
    :cond_b
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 316
    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_7

    .line 317
    :cond_d
    const/4 v6, 0x0

    goto/16 :goto_8

    .line 321
    :cond_e
    const-string v2, ""

    move-object v5, v2

    goto/16 :goto_9

    .line 323
    :cond_f
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_a

    .line 326
    :cond_10
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_b

    .line 328
    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 330
    :cond_12
    iget-object v7, p0, Lfqd;->l:Lfob;

    iget-object v7, v7, Lfob;->b:Lfod;

    .line 331
    invoke-virtual {v7, v1}, Lfod;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v7

    if-nez v7, :cond_4

    goto/16 :goto_d

    .line 358
    :cond_13
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lfqd;->n:Z

    .line 104
    return-void
.end method

.method public a([Landroid/graphics/Point;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lfqd;->k:[Landroid/graphics/Point;

    .line 108
    return-void
.end method

.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lfqd;->j()Lfob;

    move-result-object v0

    return-object v0
.end method

.method public f()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfqd;->m:Z

    .line 113
    invoke-virtual {p0}, Lfqd;->t()V

    .line 114
    return-void
.end method

.method protected g()V
    .locals 4

    .prologue
    .line 366
    invoke-virtual {p0}, Lfqd;->y()Z

    move-result v0

    .line 367
    if-nez v0, :cond_0

    iget-object v0, p0, Lfqd;->l:Lfob;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lfqd;->l:Lfob;

    invoke-virtual {p0, v0}, Lfqd;->b(Ljava/lang/Object;)V

    .line 373
    :goto_0
    return-void

    .line 370
    :cond_0
    invoke-virtual {p0}, Lfqd;->t()V

    .line 371
    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfqd;->j:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lfqd;->b:Ldp;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 377
    invoke-virtual {p0}, Lfqd;->b()Z

    .line 378
    return-void
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 382
    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfqd;->b:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 383
    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfqd;->c:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 384
    const/4 v0, 0x0

    iput-object v0, p0, Lfqd;->l:Lfob;

    .line 385
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfqd;->m:Z

    .line 386
    invoke-virtual {p0}, Lfqd;->h()V

    .line 387
    return-void
.end method

.method public j()Lfob;
    .locals 12

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x0

    const/4 v11, 0x1

    .line 118
    :try_start_0
    iget-object v0, p0, Lfqd;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lfqf;

    invoke-direct {v0}, Lfqf;-><init>()V

    throw v0
    :try_end_0
    .catch Lfqe; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lfqf; {:try_start_0 .. :try_end_0} :catch_1

    .line 127
    :catch_0
    move-exception v0

    new-instance v0, Lfoc;

    invoke-direct {v0, v10}, Lfoc;-><init>(Lmmq;)V

    const/4 v1, 0x2

    .line 128
    invoke-virtual {v0, v1}, Lfoc;->a(I)Lfoc;

    move-result-object v0

    invoke-virtual {v0}, Lfoc;->c()Lfob;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    .line 118
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lfqd;->m:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfqd;->b:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfqd;->d:I

    iget-object v2, p0, Lfqd;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lfoa;->b(Landroid/content/Context;ILjava/lang/String;)V

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lfqd;->j:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lfqd;->b:Ldp;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfqd;->m:Z

    move-object v0, v10

    :cond_1
    :goto_1
    if-nez v0, :cond_c

    iget-boolean v1, p0, Lfqd;->h:Z

    if-eqz v1, :cond_c

    iget-object v1, p0, Lfqd;->k:[Landroid/graphics/Point;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lfqd;->k:[Landroid/graphics/Point;

    array-length v1, v1

    if-nez v1, :cond_c

    :cond_2
    new-instance v0, Lfqe;

    invoke-direct {v0, p0}, Lfqe;-><init>(Lfqd;)V

    throw v0
    :try_end_1
    .catch Lfqe; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lfqf; {:try_start_1 .. :try_end_1} :catch_1

    .line 130
    :catch_1
    move-exception v0

    new-instance v0, Lfoc;

    invoke-direct {v0, v10}, Lfoc;-><init>(Lmmq;)V

    .line 131
    invoke-virtual {v0, v11}, Lfoc;->a(I)Lfoc;

    move-result-object v0

    invoke-virtual {v0}, Lfoc;->c()Lfob;

    move-result-object v0

    goto :goto_0

    .line 118
    :cond_3
    :try_start_2
    iget-object v0, p0, Lfqd;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lfqd;->a(Ljava/lang/String;)Lfoc;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lfoc;->a()Lmmq;

    move-result-object v2

    move-object v4, v2

    :goto_2
    if-eqz v4, :cond_a

    iget-object v2, v4, Lmmq;->f:Lmoi;

    if-eqz v2, :cond_a

    iget-object v2, v4, Lmmq;->f:Lmoi;

    iget-object v2, v2, Lmoi;->a:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_a

    move v3, v11

    :goto_3
    if-eqz v4, :cond_b

    iget-object v2, v4, Lmmq;->f:Lmoi;

    if-eqz v2, :cond_b

    iget-object v2, v4, Lmmq;->f:Lmoi;

    iget-object v2, v2, Lmoi;->b:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_b

    move v2, v11

    :goto_4
    if-eqz v4, :cond_5

    iget-object v5, v4, Lmmq;->g:Lmlz;

    if-eqz v5, :cond_5

    iget-object v5, v4, Lmmq;->g:Lmlz;

    iget-object v5, v5, Lmlz;->b:Logr;

    if-nez v5, :cond_4

    iget-object v4, v4, Lmmq;->g:Lmlz;

    iget-object v4, v4, Lmlz;->c:Loya;

    if-eqz v4, :cond_5

    :cond_4
    move v1, v11

    :cond_5
    iget-boolean v4, p0, Lfqd;->h:Z

    if-eqz v4, :cond_6

    if-eqz v3, :cond_8

    :cond_6
    iget-boolean v3, p0, Lfqd;->i:Z

    if-eqz v3, :cond_7

    if-eqz v2, :cond_8

    :cond_7
    iget-boolean v2, p0, Lfqd;->n:Z

    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    :cond_8
    move-object v0, v10

    goto :goto_1

    :cond_9
    move-object v4, v10

    goto :goto_2

    :cond_a
    move v3, v1

    goto :goto_3

    :cond_b
    move v2, v1

    goto :goto_4

    :cond_c
    if-nez v0, :cond_e

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Lfqd;->d:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v3

    new-instance v0, Lfnm;

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lkfo;

    const-string v4, "account_name"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lfqd;->e:Ljava/lang/String;

    iget-object v4, p0, Lfqd;->f:Ljava/lang/String;

    iget-object v5, p0, Lfqd;->g:Ljava/lang/String;

    iget-boolean v6, p0, Lfqd;->h:Z

    iget-boolean v7, p0, Lfqd;->i:Z

    iget-object v8, p0, Lfqd;->k:[Landroid/graphics/Point;

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Lfnm;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ[Landroid/graphics/Point;Z)V

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lkfd;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    invoke-virtual {v0}, Lfnm;->t()Z

    move-result v1

    if-eqz v1, :cond_d

    new-instance v0, Lfqf;

    invoke-direct {v0}, Lfqf;-><init>()V

    throw v0

    :cond_d
    invoke-virtual {v0}, Lfnm;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmdh;

    iget-object v0, v0, Lmdh;->a:Lmlr;

    iget-object v1, v0, Lmlr;->a:Lmmq;

    new-instance v0, Lfoc;

    invoke-direct {v0, v1}, Lfoc;-><init>(Lmmq;)V

    invoke-virtual {v0}, Lfoc;->b()Lfoc;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lfoc;->a(J)Lfoc;

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lfqd;->b:Ldp;

    invoke-virtual {v2, v3}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lfqd;->d:I

    const/4 v4, 0x1

    invoke-static {v2, v3, v1, v4}, Lfoa;->a(Landroid/content/Context;ILmmq;Z)V

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lfqd;->j:Landroid/net/Uri;

    const/4 v3, 0x0

    iget-object v4, p0, Lfqd;->b:Ldp;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V
    :try_end_2
    .catch Lfqe; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lfqf; {:try_start_2 .. :try_end_2} :catch_1

    .line 134
    :cond_e
    invoke-virtual {v0}, Lfoc;->a()Lmmq;

    move-result-object v1

    .line 133
    invoke-static {v1}, Lfoa;->a(Lmmq;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lfqd;->b(Ljava/lang/String;)Lfod;

    move-result-object v2

    invoke-virtual {p0}, Lfqd;->n()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v1}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lfqd;->c:Ldp;

    invoke-virtual {v3, v1, v11, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-virtual {v0, v2}, Lfoc;->a(Lfod;)Lfoc;

    .line 135
    invoke-virtual {v0}, Lfoc;->c()Lfob;

    move-result-object v2

    .line 136
    iget-object v0, p0, Lfqd;->l:Lfob;

    invoke-virtual {v2, v0}, Lfob;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 137
    const-string v0, "StoryLoader"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 138
    iget-object v0, p0, Lfqd;->l:Lfob;

    if-eqz v0, :cond_f

    .line 139
    iget-object v0, v2, Lfob;->a:Lmmq;

    iget-object v1, p0, Lfqd;->l:Lfob;

    iget-object v1, v1, Lfob;->a:Lmmq;

    invoke-static {v0, v1}, Loxu;->a(Loxu;Loxu;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 142
    iget-object v0, v2, Lfob;->b:Lfod;

    iget-object v1, p0, Lfqd;->l:Lfob;

    iget-object v1, v1, Lfob;->b:Lfod;

    invoke-virtual {v0, v1}, Lfod;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 144
    iget-object v0, v2, Lfob;->b:Lfod;

    iget-object v0, v0, Lfod;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 145
    iget-object v1, p0, Lfqd;->l:Lfob;

    iget-object v1, v1, Lfob;->b:Lfod;

    iget-object v1, v1, Lfod;->a:Ljava/util/Map;

    .line 146
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 147
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 148
    invoke-interface {v3, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 149
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 150
    invoke-interface {v4, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 152
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "; new ids = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    :goto_5
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "; removed ids = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x25

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "loadedStoryData.photoTileData changed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_f
    :goto_7
    iput-object v2, p0, Lfqd;->l:Lfob;

    .line 163
    :cond_10
    iget-object v0, p0, Lfqd;->l:Lfob;

    goto/16 :goto_0

    .line 152
    :cond_11
    const-string v0, ""

    goto :goto_5

    .line 153
    :cond_12
    const-string v1, ""

    goto :goto_6

    .line 151
    :cond_13
    iget v0, v2, Lfob;->e:I

    iget-object v1, p0, Lfqd;->l:Lfob;

    iget v1, v1, Lfob;->e:I

    if-eq v0, v1, :cond_f

    .line 155
    iget-object v0, p0, Lfqd;->l:Lfob;

    iget v0, v0, Lfob;->e:I

    iget v1, v2, Lfob;->e:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x3e

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "loadedStoryData.loadStatus changed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " -> "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_7
.end method
