.class public final Lmqp;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Lmpm;

.field public d:Logw;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    iput v0, p0, Lmqp;->a:I

    .line 20
    iput-object v1, p0, Lmqp;->c:Lmpm;

    .line 27
    iput v0, p0, Lmqp;->h:I

    .line 36
    iput-object v1, p0, Lmqp;->d:Logw;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 80
    const/4 v0, 0x0

    .line 81
    iget-object v1, p0, Lmqp;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 82
    const/4 v0, 0x1

    iget-object v1, p0, Lmqp;->e:Ljava/lang/String;

    .line 83
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 85
    :cond_0
    iget v1, p0, Lmqp;->a:I

    if-eq v1, v3, :cond_1

    .line 86
    const/4 v1, 0x2

    iget v2, p0, Lmqp;->a:I

    .line 87
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_1
    iget-object v1, p0, Lmqp;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 90
    const/4 v1, 0x3

    iget-object v2, p0, Lmqp;->b:Ljava/lang/String;

    .line 91
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_2
    iget-object v1, p0, Lmqp;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 94
    const/4 v1, 0x4

    iget-object v2, p0, Lmqp;->f:Ljava/lang/String;

    .line 95
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_3
    iget-object v1, p0, Lmqp;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 98
    const/4 v1, 0x5

    iget-object v2, p0, Lmqp;->g:Ljava/lang/String;

    .line 99
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_4
    iget v1, p0, Lmqp;->h:I

    if-eq v1, v3, :cond_5

    .line 102
    const/4 v1, 0x6

    iget v2, p0, Lmqp;->h:I

    .line 103
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    :cond_5
    iget-object v1, p0, Lmqp;->i:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 106
    const/4 v1, 0x7

    iget-object v2, p0, Lmqp;->i:Ljava/lang/String;

    .line 107
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    :cond_6
    iget-object v1, p0, Lmqp;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 110
    const/16 v1, 0x8

    iget-object v2, p0, Lmqp;->j:Ljava/lang/Integer;

    .line 111
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 113
    :cond_7
    iget-object v1, p0, Lmqp;->c:Lmpm;

    if-eqz v1, :cond_8

    .line 114
    const/16 v1, 0x9

    iget-object v2, p0, Lmqp;->c:Lmpm;

    .line 115
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    :cond_8
    iget-object v1, p0, Lmqp;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 118
    const/16 v1, 0xa

    iget-object v2, p0, Lmqp;->k:Ljava/lang/String;

    .line 119
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 121
    :cond_9
    iget-object v1, p0, Lmqp;->d:Logw;

    if-eqz v1, :cond_a

    .line 122
    const/16 v1, 0xb

    iget-object v2, p0, Lmqp;->d:Logw;

    .line 123
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    :cond_a
    iget-object v1, p0, Lmqp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 126
    iput v0, p0, Lmqp;->ai:I

    .line 127
    return v0
.end method

.method public a(Loxn;)Lmqp;
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 135
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 136
    sparse-switch v0, :sswitch_data_0

    .line 140
    iget-object v1, p0, Lmqp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 141
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmqp;->ah:Ljava/util/List;

    .line 144
    :cond_1
    iget-object v1, p0, Lmqp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    :sswitch_0
    return-object p0

    .line 151
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqp;->e:Ljava/lang/String;

    goto :goto_0

    .line 155
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 156
    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 163
    :cond_2
    iput v0, p0, Lmqp;->a:I

    goto :goto_0

    .line 165
    :cond_3
    iput v2, p0, Lmqp;->a:I

    goto :goto_0

    .line 170
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqp;->b:Ljava/lang/String;

    goto :goto_0

    .line 174
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqp;->f:Ljava/lang/String;

    goto :goto_0

    .line 178
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqp;->g:Ljava/lang/String;

    goto :goto_0

    .line 182
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 183
    if-eqz v0, :cond_4

    if-eq v0, v2, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0xf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 200
    :cond_4
    iput v0, p0, Lmqp;->h:I

    goto/16 :goto_0

    .line 202
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Lmqp;->h:I

    goto/16 :goto_0

    .line 207
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqp;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 211
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmqp;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 215
    :sswitch_9
    iget-object v0, p0, Lmqp;->c:Lmpm;

    if-nez v0, :cond_6

    .line 216
    new-instance v0, Lmpm;

    invoke-direct {v0}, Lmpm;-><init>()V

    iput-object v0, p0, Lmqp;->c:Lmpm;

    .line 218
    :cond_6
    iget-object v0, p0, Lmqp;->c:Lmpm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 222
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqp;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 226
    :sswitch_b
    iget-object v0, p0, Lmqp;->d:Logw;

    if-nez v0, :cond_7

    .line 227
    new-instance v0, Logw;

    invoke-direct {v0}, Logw;-><init>()V

    iput-object v0, p0, Lmqp;->d:Logw;

    .line 229
    :cond_7
    iget-object v0, p0, Lmqp;->d:Logw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 136
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 41
    iget-object v0, p0, Lmqp;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x1

    iget-object v1, p0, Lmqp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 44
    :cond_0
    iget v0, p0, Lmqp;->a:I

    if-eq v0, v2, :cond_1

    .line 45
    const/4 v0, 0x2

    iget v1, p0, Lmqp;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 47
    :cond_1
    iget-object v0, p0, Lmqp;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 48
    const/4 v0, 0x3

    iget-object v1, p0, Lmqp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 50
    :cond_2
    iget-object v0, p0, Lmqp;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 51
    const/4 v0, 0x4

    iget-object v1, p0, Lmqp;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 53
    :cond_3
    iget-object v0, p0, Lmqp;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 54
    const/4 v0, 0x5

    iget-object v1, p0, Lmqp;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 56
    :cond_4
    iget v0, p0, Lmqp;->h:I

    if-eq v0, v2, :cond_5

    .line 57
    const/4 v0, 0x6

    iget v1, p0, Lmqp;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 59
    :cond_5
    iget-object v0, p0, Lmqp;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 60
    const/4 v0, 0x7

    iget-object v1, p0, Lmqp;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 62
    :cond_6
    iget-object v0, p0, Lmqp;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 63
    const/16 v0, 0x8

    iget-object v1, p0, Lmqp;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 65
    :cond_7
    iget-object v0, p0, Lmqp;->c:Lmpm;

    if-eqz v0, :cond_8

    .line 66
    const/16 v0, 0x9

    iget-object v1, p0, Lmqp;->c:Lmpm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 68
    :cond_8
    iget-object v0, p0, Lmqp;->k:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 69
    const/16 v0, 0xa

    iget-object v1, p0, Lmqp;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 71
    :cond_9
    iget-object v0, p0, Lmqp;->d:Logw;

    if-eqz v0, :cond_a

    .line 72
    const/16 v0, 0xb

    iget-object v1, p0, Lmqp;->d:Logw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 74
    :cond_a
    iget-object v0, p0, Lmqp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 76
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmqp;->a(Loxn;)Lmqp;

    move-result-object v0

    return-object v0
.end method
