.class public final Ljbd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:I

.field private static b:Ljbe;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-string v0, "https://lh3.googleusercontent.com"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    .line 84
    new-instance v0, Ljbe;

    invoke-direct {v0}, Ljbe;-><init>()V

    sput-object v0, Ljbd;->b:Ljbe;

    return-void
.end method

.method private static a(F)Ljava/lang/String;
    .locals 4

    .prologue
    .line 254
    float-to-double v0, p0

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    float-to-double v0, p0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 255
    :cond_0
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, p0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result p0

    .line 257
    :cond_1
    const v0, 0x477fff00    # 65535.0f

    mul-float/2addr v0, p0

    float-to-int v0, v0

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    .line 258
    const-string v1, "0000"

    .line 259
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    rsub-int/lit8 v3, v3, 0x4

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/graphics/RectF;I)Ljava/lang/String;
    .locals 7

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 238
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 239
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 240
    iget v2, p0, Landroid/graphics/RectF;->top:F

    .line 241
    iget v3, p0, Landroid/graphics/RectF;->right:F

    sub-float v3, v4, v3

    iput v3, v1, Landroid/graphics/RectF;->top:F

    .line 242
    iget v3, p0, Landroid/graphics/RectF;->bottom:F

    iput v3, v1, Landroid/graphics/RectF;->right:F

    .line 243
    iget v3, p0, Landroid/graphics/RectF;->left:F

    sub-float v3, v4, v3

    iput v3, v1, Landroid/graphics/RectF;->bottom:F

    .line 244
    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 239
    add-int/lit8 v0, v0, 0x1

    move-object p0, v1

    goto :goto_0

    .line 248
    :cond_0
    iget v0, p0, Landroid/graphics/RectF;->left:F

    invoke-static {v0}, Ljbd;->a(F)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Landroid/graphics/RectF;->top:F

    .line 249
    invoke-static {v1}, Ljbd;->a(F)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Landroid/graphics/RectF;->right:F

    invoke-static {v2}, Ljbd;->a(F)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Landroid/graphics/RectF;->bottom:F

    .line 250
    invoke-static {v3}, Ljbd;->a(F)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xb

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "-fcrop64=1,"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;I)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 116
    const/4 v7, 0x0

    sget v8, Ljbd;->a:I

    move-object v0, p0

    move v1, p1

    move v3, v2

    move v4, v2

    move v6, v5

    invoke-static/range {v0 .. v8}, Ljbd;->a(Ljava/lang/String;IIIIIILandroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v0

    .line 119
    if-nez v0, :cond_0

    .line 120
    invoke-static {v5, p0}, Ljbf;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;II)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 145
    const/4 v7, 0x0

    sget v8, Ljbd;->a:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move v6, v5

    invoke-static/range {v0 .. v8}, Ljbd;->a(Ljava/lang/String;IIIIIILandroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v0

    .line 147
    if-nez v0, :cond_0

    .line 148
    invoke-static {p2, p0}, Ljbf;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;III)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v5, -0x1

    .line 174
    const/4 v2, 0x0

    const/4 v7, 0x0

    sget v8, Ljbd;->a:I

    move-object v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    move v6, v5

    invoke-static/range {v0 .. v8}, Ljbd;->a(Ljava/lang/String;IIIIIILandroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v0

    .line 176
    if-eqz v0, :cond_1

    move-object p0, v0

    .line 181
    :cond_0
    :goto_0
    return-object p0

    .line 178
    :cond_1
    if-nez p2, :cond_2

    if-eqz p3, :cond_0

    .line 179
    :cond_2
    invoke-static {p2, p3, p0}, Ljbf;->a(IILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method static a(Ljava/lang/String;IIIIIILandroid/graphics/RectF;I)Ljava/lang/String;
    .locals 10

    .prologue
    .line 213
    sget-object v0, Ljbd;->b:Ljbe;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-virtual/range {v0 .. v9}, Ljbe;->a(Ljava/lang/String;IIIIIILandroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;IIIIILandroid/graphics/RectF;)Ljava/lang/String;
    .locals 9

    .prologue
    .line 192
    const/4 v2, 0x0

    sget v8, Ljbd;->a:I

    move-object v0, p0

    move v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    invoke-static/range {v0 .. v8}, Ljbd;->a(Ljava/lang/String;IIIIIILandroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_1

    move-object p0, v0

    .line 199
    :cond_0
    :goto_0
    return-object p0

    .line 196
    :cond_1
    if-nez p2, :cond_2

    if-eqz p3, :cond_0

    .line 197
    :cond_2
    invoke-static {p2, p3, p0}, Ljbf;->a(IILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IILandroid/graphics/RectF;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 159
    sget v8, Ljbd;->a:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move v6, v5

    move-object v7, p3

    invoke-static/range {v0 .. v8}, Ljbd;->a(Ljava/lang/String;IIIIIILandroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v0

    .line 161
    if-nez v0, :cond_0

    .line 162
    invoke-static {p2, p0}, Ljbf;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 164
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/lang/String;ILandroid/graphics/RectF;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 131
    sget v8, Ljbd;->a:I

    move-object v0, p0

    move v1, p1

    move v3, v2

    move v4, v2

    move v6, v5

    move-object v7, p2

    invoke-static/range {v0 .. v8}, Ljbd;->a(Ljava/lang/String;IIIIIILandroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v0

    .line 133
    if-nez v0, :cond_0

    .line 134
    invoke-static {v5, p0}, Ljbf;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 136
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 98
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 101
    sput v0, Ljbd;->a:I

    if-nez v0, :cond_0

    .line 102
    const/4 v0, 0x1

    sput v0, Ljbd;->a:I

    .line 105
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 204
    if-nez p0, :cond_0

    .line 205
    const/4 v0, 0x0

    .line 208
    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljbd;->b:Ljbe;

    invoke-virtual {v0, p0}, Ljbe;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    if-nez p0, :cond_1

    .line 222
    const/4 p0, 0x0

    .line 229
    :cond_0
    :goto_0
    return-object p0

    .line 224
    :cond_1
    invoke-static {p0}, Ljbd;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    const/4 v0, 0x2

    invoke-static {p0, v0}, Ljbd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 226
    :cond_2
    const-string v0, "https"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "http"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    :cond_3
    const/4 v0, -0x1

    invoke-static {v0, p0}, Ljbf;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
