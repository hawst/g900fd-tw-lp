.class public final Lfqm;
.super Lfqu;
.source "PG"


# static fields
.field public static final a:Lloz;


# instance fields
.field private final d:Z

.field private final e:Lfrj;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lloz;

    const-string v1, "debug.stories.wireframe"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfqm;->a:Lloz;

    return-void
.end method


# virtual methods
.method public a()Lfrj;
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lfqm;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqm;->e:Lfrj;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lfqu;->a()Lfrj;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lmnk;
    .locals 3

    .prologue
    .line 42
    iget-boolean v0, p0, Lfqm;->d:Z

    if-eqz v0, :cond_0

    .line 43
    invoke-super {p0, p1}, Lfqu;->a(I)Lmnk;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    .line 44
    :cond_0
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    .line 45
    iget-object v0, p0, Lfqm;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v1, p0, Lfqm;->c:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lmmt;->d:[Lmnk;

    div-int/lit8 v1, p1, 0x2

    aget-object v0, v0, v1

    goto :goto_0

    .line 47
    :cond_1
    const/4 v1, 0x0

    .line 49
    :try_start_0
    iget-object v0, p0, Lfqm;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v2, p0, Lfqm;->c:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lmmt;->d:[Lmnk;

    add-int/lit8 v2, p1, -0x1

    div-int/lit8 v2, v2, 0x2

    aget-object v0, v0, v2

    .line 51
    new-instance v2, Lmnk;

    invoke-direct {v2}, Lmnk;-><init>()V

    .line 52
    invoke-static {v0}, Lmnk;->a(Loxu;)[B

    move-result-object v0

    .line 51
    invoke-static {v2, v0}, Lmnk;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmnk;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_1

    .line 53
    const/4 v1, 0x2

    :try_start_1
    iput v1, v0, Lmnk;->b:I

    .line 54
    new-instance v1, Lmnj;

    invoke-direct {v1}, Lmnj;-><init>()V

    iput-object v1, v0, Lmnk;->d:Lmnj;

    .line 55
    iget-object v1, v0, Lmnk;->d:Lmnj;

    const/4 v2, 0x0

    iput v2, v1, Lmnj;->a:I

    .line 56
    iget-object v1, v0, Lmnk;->c:Lmof;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v1, Lmof;->c:Ljava/lang/Float;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 58
    :catch_0
    move-exception v1

    :goto_1
    const-string v1, "DebugStoryLayoutAdapter"

    const-string v2, "Unable to deserialize LayoutElement.  This should not happen because the object was previously serialized as a byte array."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Lfqu;->getCount()I

    move-result v1

    iget-boolean v0, p0, Lfqm;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    mul-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lfqm;->a(I)Lmnk;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 72
    iget-boolean v0, p0, Lfqm;->d:Z

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0, p1}, Lfqm;->a(I)Lmnk;

    move-result-object v1

    .line 74
    new-instance v0, Lftu;

    invoke-virtual {p0}, Lfqm;->c()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lftu;-><init>(Landroid/content/Context;Lmnk;)V

    .line 76
    invoke-virtual {p0, v1, v4}, Lfqm;->a(Lmnk;Lfrc;)Lfrc;

    move-result-object v2

    .line 77
    const v3, 0x7f100031

    invoke-virtual {v0, v3, v2}, Lftu;->setTag(ILjava/lang/Object;)V

    .line 78
    invoke-virtual {p0, v1, v4, p3}, Lfqm;->a(Lmnk;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lftu;->addView(Landroid/view/View;)V

    .line 81
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lfqu;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
