.class public final Lfzv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ljma;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;",
        "Ljma",
        "<",
        "Lctq;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View$OnClickListener;

.field private b:Z

.field private c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 749
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfzv;->b:Z

    .line 753
    iput-object p1, p0, Lfzv;->a:Landroid/view/View$OnClickListener;

    .line 754
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 757
    iput-object p1, p0, Lfzv;->c:Landroid/view/View$OnClickListener;

    .line 758
    return-void
.end method

.method public a(Lctq;)V
    .locals 1

    .prologue
    .line 762
    invoke-virtual {p1}, Lctq;->g()Z

    move-result v0

    iput-boolean v0, p0, Lfzv;->b:Z

    .line 763
    return-void
.end method

.method public synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 745
    check-cast p1, Lctq;

    invoke-virtual {p0, p1}, Lfzv;->a(Lctq;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 767
    iget-boolean v0, p0, Lfzv;->b:Z

    if-eqz v0, :cond_1

    .line 768
    iget-object v0, p0, Lfzv;->a:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 772
    :cond_0
    :goto_0
    return-void

    .line 769
    :cond_1
    iget-object v0, p0, Lfzv;->c:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 770
    iget-object v0, p0, Lfzv;->c:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method
