.class public final Losi;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1456
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1482
    const/4 v0, 0x0

    .line 1483
    iget-object v1, p0, Losi;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1484
    const/4 v0, 0x1

    iget-object v1, p0, Losi;->a:Ljava/lang/String;

    .line 1485
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1487
    :cond_0
    iget-object v1, p0, Losi;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1488
    const/4 v1, 0x2

    iget-object v2, p0, Losi;->b:Ljava/lang/Boolean;

    .line 1489
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1491
    :cond_1
    iget-object v1, p0, Losi;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1492
    const/4 v1, 0x3

    iget-object v2, p0, Losi;->c:Ljava/lang/Boolean;

    .line 1493
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1495
    :cond_2
    iget-object v1, p0, Losi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1496
    iput v0, p0, Losi;->ai:I

    .line 1497
    return v0
.end method

.method public a(Loxn;)Losi;
    .locals 2

    .prologue
    .line 1505
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1506
    sparse-switch v0, :sswitch_data_0

    .line 1510
    iget-object v1, p0, Losi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1511
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Losi;->ah:Ljava/util/List;

    .line 1514
    :cond_1
    iget-object v1, p0, Losi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1516
    :sswitch_0
    return-object p0

    .line 1521
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Losi;->a:Ljava/lang/String;

    goto :goto_0

    .line 1525
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Losi;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1529
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Losi;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1506
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1467
    iget-object v0, p0, Losi;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1468
    const/4 v0, 0x1

    iget-object v1, p0, Losi;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1470
    :cond_0
    iget-object v0, p0, Losi;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1471
    const/4 v0, 0x2

    iget-object v1, p0, Losi;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1473
    :cond_1
    iget-object v0, p0, Losi;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1474
    const/4 v0, 0x3

    iget-object v1, p0, Losi;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1476
    :cond_2
    iget-object v0, p0, Losi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1478
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1452
    invoke-virtual {p0, p1}, Losi;->a(Loxn;)Losi;

    move-result-object v0

    return-object v0
.end method
