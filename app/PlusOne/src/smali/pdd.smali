.class public final Lpdd;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpdd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Loya;

.field private g:Ljava/lang/String;

.field private h:Lpdi;

.field private i:Ljava/lang/String;

.field private j:[Loya;

.field private k:[Lozs;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x1990e8e

    new-instance v1, Lpde;

    invoke-direct {v1}, Lpde;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpdd;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lpdd;->h:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lpdd;->j:[Loya;

    .line 35
    iput-object v1, p0, Lpdd;->f:Loya;

    .line 38
    sget-object v0, Lozs;->a:[Lozs;

    iput-object v0, p0, Lpdd;->k:[Lozs;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 87
    .line 88
    iget-object v0, p0, Lpdd;->b:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 89
    const/4 v0, 0x1

    iget-object v2, p0, Lpdd;->b:Ljava/lang/String;

    .line 90
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 92
    :goto_0
    iget-object v2, p0, Lpdd;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 93
    const/4 v2, 0x2

    iget-object v3, p0, Lpdd;->c:Ljava/lang/String;

    .line 94
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 96
    :cond_0
    iget-object v2, p0, Lpdd;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 97
    const/4 v2, 0x3

    iget-object v3, p0, Lpdd;->d:Ljava/lang/String;

    .line 98
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 100
    :cond_1
    iget-object v2, p0, Lpdd;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 101
    const/4 v2, 0x4

    iget-object v3, p0, Lpdd;->e:Ljava/lang/String;

    .line 102
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 104
    :cond_2
    iget-object v2, p0, Lpdd;->h:Lpdi;

    if-eqz v2, :cond_3

    .line 105
    const/4 v2, 0x5

    iget-object v3, p0, Lpdd;->h:Lpdi;

    .line 106
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 108
    :cond_3
    iget-object v2, p0, Lpdd;->i:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 109
    const/4 v2, 0x6

    iget-object v3, p0, Lpdd;->i:Ljava/lang/String;

    .line 110
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    :cond_4
    iget-object v2, p0, Lpdd;->j:[Loya;

    if-eqz v2, :cond_6

    .line 113
    iget-object v3, p0, Lpdd;->j:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 114
    if-eqz v5, :cond_5

    .line 115
    const/4 v6, 0x7

    .line 116
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 113
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 120
    :cond_6
    iget-object v2, p0, Lpdd;->g:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 121
    const/16 v2, 0x8

    iget-object v3, p0, Lpdd;->g:Ljava/lang/String;

    .line 122
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 124
    :cond_7
    iget-object v2, p0, Lpdd;->f:Loya;

    if-eqz v2, :cond_8

    .line 125
    const/16 v2, 0x9

    iget-object v3, p0, Lpdd;->f:Loya;

    .line 126
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 128
    :cond_8
    iget-object v2, p0, Lpdd;->k:[Lozs;

    if-eqz v2, :cond_a

    .line 129
    iget-object v2, p0, Lpdd;->k:[Lozs;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 130
    if-eqz v4, :cond_9

    .line 131
    const/16 v5, 0xa

    .line 132
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 129
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 136
    :cond_a
    iget-object v1, p0, Lpdd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    iput v0, p0, Lpdd;->ai:I

    .line 138
    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lpdd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 146
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 147
    sparse-switch v0, :sswitch_data_0

    .line 151
    iget-object v2, p0, Lpdd;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 152
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpdd;->ah:Ljava/util/List;

    .line 155
    :cond_1
    iget-object v2, p0, Lpdd;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    :sswitch_0
    return-object p0

    .line 162
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdd;->b:Ljava/lang/String;

    goto :goto_0

    .line 166
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdd;->c:Ljava/lang/String;

    goto :goto_0

    .line 170
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdd;->d:Ljava/lang/String;

    goto :goto_0

    .line 174
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdd;->e:Ljava/lang/String;

    goto :goto_0

    .line 178
    :sswitch_5
    iget-object v0, p0, Lpdd;->h:Lpdi;

    if-nez v0, :cond_2

    .line 179
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lpdd;->h:Lpdi;

    .line 181
    :cond_2
    iget-object v0, p0, Lpdd;->h:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 185
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdd;->i:Ljava/lang/String;

    goto :goto_0

    .line 189
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 190
    iget-object v0, p0, Lpdd;->j:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 191
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 192
    iget-object v3, p0, Lpdd;->j:[Loya;

    if-eqz v3, :cond_3

    .line 193
    iget-object v3, p0, Lpdd;->j:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 195
    :cond_3
    iput-object v2, p0, Lpdd;->j:[Loya;

    .line 196
    :goto_2
    iget-object v2, p0, Lpdd;->j:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 197
    iget-object v2, p0, Lpdd;->j:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 198
    iget-object v2, p0, Lpdd;->j:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 199
    invoke-virtual {p1}, Loxn;->a()I

    .line 196
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 190
    :cond_4
    iget-object v0, p0, Lpdd;->j:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 202
    :cond_5
    iget-object v2, p0, Lpdd;->j:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 203
    iget-object v2, p0, Lpdd;->j:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 207
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdd;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 211
    :sswitch_9
    iget-object v0, p0, Lpdd;->f:Loya;

    if-nez v0, :cond_6

    .line 212
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpdd;->f:Loya;

    .line 214
    :cond_6
    iget-object v0, p0, Lpdd;->f:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 218
    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 219
    iget-object v0, p0, Lpdd;->k:[Lozs;

    if-nez v0, :cond_8

    move v0, v1

    .line 220
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lozs;

    .line 221
    iget-object v3, p0, Lpdd;->k:[Lozs;

    if-eqz v3, :cond_7

    .line 222
    iget-object v3, p0, Lpdd;->k:[Lozs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 224
    :cond_7
    iput-object v2, p0, Lpdd;->k:[Lozs;

    .line 225
    :goto_4
    iget-object v2, p0, Lpdd;->k:[Lozs;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 226
    iget-object v2, p0, Lpdd;->k:[Lozs;

    new-instance v3, Lozs;

    invoke-direct {v3}, Lozs;-><init>()V

    aput-object v3, v2, v0

    .line 227
    iget-object v2, p0, Lpdd;->k:[Lozs;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 228
    invoke-virtual {p1}, Loxn;->a()I

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 219
    :cond_8
    iget-object v0, p0, Lpdd;->k:[Lozs;

    array-length v0, v0

    goto :goto_3

    .line 231
    :cond_9
    iget-object v2, p0, Lpdd;->k:[Lozs;

    new-instance v3, Lozs;

    invoke-direct {v3}, Lozs;-><init>()V

    aput-object v3, v2, v0

    .line 232
    iget-object v2, p0, Lpdd;->k:[Lozs;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 147
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 43
    iget-object v1, p0, Lpdd;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 44
    const/4 v1, 0x1

    iget-object v2, p0, Lpdd;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    :cond_0
    iget-object v1, p0, Lpdd;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 47
    const/4 v1, 0x2

    iget-object v2, p0, Lpdd;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 49
    :cond_1
    iget-object v1, p0, Lpdd;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 50
    const/4 v1, 0x3

    iget-object v2, p0, Lpdd;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 52
    :cond_2
    iget-object v1, p0, Lpdd;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 53
    const/4 v1, 0x4

    iget-object v2, p0, Lpdd;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 55
    :cond_3
    iget-object v1, p0, Lpdd;->h:Lpdi;

    if-eqz v1, :cond_4

    .line 56
    const/4 v1, 0x5

    iget-object v2, p0, Lpdd;->h:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 58
    :cond_4
    iget-object v1, p0, Lpdd;->i:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 59
    const/4 v1, 0x6

    iget-object v2, p0, Lpdd;->i:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 61
    :cond_5
    iget-object v1, p0, Lpdd;->j:[Loya;

    if-eqz v1, :cond_7

    .line 62
    iget-object v2, p0, Lpdd;->j:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 63
    if-eqz v4, :cond_6

    .line 64
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 62
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    :cond_7
    iget-object v1, p0, Lpdd;->g:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 69
    const/16 v1, 0x8

    iget-object v2, p0, Lpdd;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 71
    :cond_8
    iget-object v1, p0, Lpdd;->f:Loya;

    if-eqz v1, :cond_9

    .line 72
    const/16 v1, 0x9

    iget-object v2, p0, Lpdd;->f:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 74
    :cond_9
    iget-object v1, p0, Lpdd;->k:[Lozs;

    if-eqz v1, :cond_b

    .line 75
    iget-object v1, p0, Lpdd;->k:[Lozs;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    .line 76
    if-eqz v3, :cond_a

    .line 77
    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 75
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 81
    :cond_b
    iget-object v0, p0, Lpdd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 83
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpdd;->a(Loxn;)Lpdd;

    move-result-object v0

    return-object v0
.end method
