.class public final Lnix;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lniv;

.field public b:Lnhw;

.field public c:I

.field public d:Lpmm;

.field private e:Lnji;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Lmrs;

.field private i:Lniy;

.field private j:Ljava/lang/Boolean;

.field private k:Logi;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 1062
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1077
    iput-object v0, p0, Lnix;->a:Lniv;

    .line 1080
    iput-object v0, p0, Lnix;->e:Lnji;

    .line 1083
    iput-object v0, p0, Lnix;->b:Lnhw;

    .line 1088
    iput v1, p0, Lnix;->c:I

    .line 1091
    iput-object v0, p0, Lnix;->d:Lpmm;

    .line 1094
    iput v1, p0, Lnix;->g:I

    .line 1097
    iput-object v0, p0, Lnix;->h:Lmrs;

    .line 1100
    iput-object v0, p0, Lnix;->i:Lniy;

    .line 1105
    iput-object v0, p0, Lnix;->k:Logi;

    .line 1062
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 1149
    const/4 v0, 0x0

    .line 1150
    iget-object v1, p0, Lnix;->a:Lniv;

    if-eqz v1, :cond_0

    .line 1151
    const/4 v0, 0x1

    iget-object v1, p0, Lnix;->a:Lniv;

    .line 1152
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1154
    :cond_0
    iget-object v1, p0, Lnix;->e:Lnji;

    if-eqz v1, :cond_1

    .line 1155
    const/4 v1, 0x3

    iget-object v2, p0, Lnix;->e:Lnji;

    .line 1156
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1158
    :cond_1
    iget-object v1, p0, Lnix;->b:Lnhw;

    if-eqz v1, :cond_2

    .line 1159
    const/4 v1, 0x4

    iget-object v2, p0, Lnix;->b:Lnhw;

    .line 1160
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1162
    :cond_2
    iget-object v1, p0, Lnix;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1163
    const/4 v1, 0x5

    iget-object v2, p0, Lnix;->f:Ljava/lang/String;

    .line 1164
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1166
    :cond_3
    iget v1, p0, Lnix;->c:I

    if-eq v1, v3, :cond_4

    .line 1167
    const/4 v1, 0x6

    iget v2, p0, Lnix;->c:I

    .line 1168
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1170
    :cond_4
    iget-object v1, p0, Lnix;->d:Lpmm;

    if-eqz v1, :cond_5

    .line 1171
    const/16 v1, 0x8

    iget-object v2, p0, Lnix;->d:Lpmm;

    .line 1172
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1174
    :cond_5
    iget v1, p0, Lnix;->g:I

    if-eq v1, v3, :cond_6

    .line 1175
    const/16 v1, 0x9

    iget v2, p0, Lnix;->g:I

    .line 1176
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1178
    :cond_6
    iget-object v1, p0, Lnix;->h:Lmrs;

    if-eqz v1, :cond_7

    .line 1179
    const/16 v1, 0xa

    iget-object v2, p0, Lnix;->h:Lmrs;

    .line 1180
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1182
    :cond_7
    iget-object v1, p0, Lnix;->i:Lniy;

    if-eqz v1, :cond_8

    .line 1183
    const/16 v1, 0xb

    iget-object v2, p0, Lnix;->i:Lniy;

    .line 1184
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1186
    :cond_8
    iget-object v1, p0, Lnix;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 1187
    const/16 v1, 0xc

    iget-object v2, p0, Lnix;->j:Ljava/lang/Boolean;

    .line 1188
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1190
    :cond_9
    iget-object v1, p0, Lnix;->k:Logi;

    if-eqz v1, :cond_a

    .line 1191
    const/16 v1, 0xd

    iget-object v2, p0, Lnix;->k:Logi;

    .line 1192
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1194
    :cond_a
    iget-object v1, p0, Lnix;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1195
    iput v0, p0, Lnix;->ai:I

    .line 1196
    return v0
.end method

.method public a(Loxn;)Lnix;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1204
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1205
    sparse-switch v0, :sswitch_data_0

    .line 1209
    iget-object v1, p0, Lnix;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1210
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnix;->ah:Ljava/util/List;

    .line 1213
    :cond_1
    iget-object v1, p0, Lnix;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1215
    :sswitch_0
    return-object p0

    .line 1220
    :sswitch_1
    iget-object v0, p0, Lnix;->a:Lniv;

    if-nez v0, :cond_2

    .line 1221
    new-instance v0, Lniv;

    invoke-direct {v0}, Lniv;-><init>()V

    iput-object v0, p0, Lnix;->a:Lniv;

    .line 1223
    :cond_2
    iget-object v0, p0, Lnix;->a:Lniv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1227
    :sswitch_2
    iget-object v0, p0, Lnix;->e:Lnji;

    if-nez v0, :cond_3

    .line 1228
    new-instance v0, Lnji;

    invoke-direct {v0}, Lnji;-><init>()V

    iput-object v0, p0, Lnix;->e:Lnji;

    .line 1230
    :cond_3
    iget-object v0, p0, Lnix;->e:Lnji;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1234
    :sswitch_3
    iget-object v0, p0, Lnix;->b:Lnhw;

    if-nez v0, :cond_4

    .line 1235
    new-instance v0, Lnhw;

    invoke-direct {v0}, Lnhw;-><init>()V

    iput-object v0, p0, Lnix;->b:Lnhw;

    .line 1237
    :cond_4
    iget-object v0, p0, Lnix;->b:Lnhw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1241
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnix;->f:Ljava/lang/String;

    goto :goto_0

    .line 1245
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1246
    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-ne v0, v4, :cond_6

    .line 1249
    :cond_5
    iput v0, p0, Lnix;->c:I

    goto :goto_0

    .line 1251
    :cond_6
    iput v2, p0, Lnix;->c:I

    goto :goto_0

    .line 1256
    :sswitch_6
    iget-object v0, p0, Lnix;->d:Lpmm;

    if-nez v0, :cond_7

    .line 1257
    new-instance v0, Lpmm;

    invoke-direct {v0}, Lpmm;-><init>()V

    iput-object v0, p0, Lnix;->d:Lpmm;

    .line 1259
    :cond_7
    iget-object v0, p0, Lnix;->d:Lpmm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1263
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1264
    if-eqz v0, :cond_8

    if-eq v0, v3, :cond_8

    if-ne v0, v4, :cond_9

    .line 1267
    :cond_8
    iput v0, p0, Lnix;->g:I

    goto/16 :goto_0

    .line 1269
    :cond_9
    iput v2, p0, Lnix;->g:I

    goto/16 :goto_0

    .line 1274
    :sswitch_8
    iget-object v0, p0, Lnix;->h:Lmrs;

    if-nez v0, :cond_a

    .line 1275
    new-instance v0, Lmrs;

    invoke-direct {v0}, Lmrs;-><init>()V

    iput-object v0, p0, Lnix;->h:Lmrs;

    .line 1277
    :cond_a
    iget-object v0, p0, Lnix;->h:Lmrs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1281
    :sswitch_9
    iget-object v0, p0, Lnix;->i:Lniy;

    if-nez v0, :cond_b

    .line 1282
    new-instance v0, Lniy;

    invoke-direct {v0}, Lniy;-><init>()V

    iput-object v0, p0, Lnix;->i:Lniy;

    .line 1284
    :cond_b
    iget-object v0, p0, Lnix;->i:Lniy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1288
    :sswitch_a
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnix;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1292
    :sswitch_b
    iget-object v0, p0, Lnix;->k:Logi;

    if-nez v0, :cond_c

    .line 1293
    new-instance v0, Logi;

    invoke-direct {v0}, Logi;-><init>()V

    iput-object v0, p0, Lnix;->k:Logi;

    .line 1295
    :cond_c
    iget-object v0, p0, Lnix;->k:Logi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1205
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x42 -> :sswitch_6
        0x48 -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x60 -> :sswitch_a
        0x6a -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 1110
    iget-object v0, p0, Lnix;->a:Lniv;

    if-eqz v0, :cond_0

    .line 1111
    const/4 v0, 0x1

    iget-object v1, p0, Lnix;->a:Lniv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1113
    :cond_0
    iget-object v0, p0, Lnix;->e:Lnji;

    if-eqz v0, :cond_1

    .line 1114
    const/4 v0, 0x3

    iget-object v1, p0, Lnix;->e:Lnji;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1116
    :cond_1
    iget-object v0, p0, Lnix;->b:Lnhw;

    if-eqz v0, :cond_2

    .line 1117
    const/4 v0, 0x4

    iget-object v1, p0, Lnix;->b:Lnhw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1119
    :cond_2
    iget-object v0, p0, Lnix;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1120
    const/4 v0, 0x5

    iget-object v1, p0, Lnix;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1122
    :cond_3
    iget v0, p0, Lnix;->c:I

    if-eq v0, v2, :cond_4

    .line 1123
    const/4 v0, 0x6

    iget v1, p0, Lnix;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1125
    :cond_4
    iget-object v0, p0, Lnix;->d:Lpmm;

    if-eqz v0, :cond_5

    .line 1126
    const/16 v0, 0x8

    iget-object v1, p0, Lnix;->d:Lpmm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1128
    :cond_5
    iget v0, p0, Lnix;->g:I

    if-eq v0, v2, :cond_6

    .line 1129
    const/16 v0, 0x9

    iget v1, p0, Lnix;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1131
    :cond_6
    iget-object v0, p0, Lnix;->h:Lmrs;

    if-eqz v0, :cond_7

    .line 1132
    const/16 v0, 0xa

    iget-object v1, p0, Lnix;->h:Lmrs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1134
    :cond_7
    iget-object v0, p0, Lnix;->i:Lniy;

    if-eqz v0, :cond_8

    .line 1135
    const/16 v0, 0xb

    iget-object v1, p0, Lnix;->i:Lniy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1137
    :cond_8
    iget-object v0, p0, Lnix;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 1138
    const/16 v0, 0xc

    iget-object v1, p0, Lnix;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1140
    :cond_9
    iget-object v0, p0, Lnix;->k:Logi;

    if-eqz v0, :cond_a

    .line 1141
    const/16 v0, 0xd

    iget-object v1, p0, Lnix;->k:Logi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1143
    :cond_a
    iget-object v0, p0, Lnix;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1145
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1058
    invoke-virtual {p0, p1}, Lnix;->a(Loxn;)Lnix;

    move-result-object v0

    return-object v0
.end method
