.class public final Lmyg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 971
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 992
    const/4 v0, 0x0

    .line 993
    iget-object v1, p0, Lmyg;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 994
    const/4 v0, 0x1

    iget-object v1, p0, Lmyg;->a:Ljava/lang/Long;

    .line 995
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 997
    :cond_0
    iget-object v1, p0, Lmyg;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 998
    const/4 v1, 0x2

    iget-object v2, p0, Lmyg;->b:Ljava/lang/String;

    .line 999
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1001
    :cond_1
    iget-object v1, p0, Lmyg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1002
    iput v0, p0, Lmyg;->ai:I

    .line 1003
    return v0
.end method

.method public a(Loxn;)Lmyg;
    .locals 2

    .prologue
    .line 1011
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1012
    sparse-switch v0, :sswitch_data_0

    .line 1016
    iget-object v1, p0, Lmyg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1017
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmyg;->ah:Ljava/util/List;

    .line 1020
    :cond_1
    iget-object v1, p0, Lmyg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1022
    :sswitch_0
    return-object p0

    .line 1027
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmyg;->a:Ljava/lang/Long;

    goto :goto_0

    .line 1031
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmyg;->b:Ljava/lang/String;

    goto :goto_0

    .line 1012
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 980
    iget-object v0, p0, Lmyg;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 981
    const/4 v0, 0x1

    iget-object v1, p0, Lmyg;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 983
    :cond_0
    iget-object v0, p0, Lmyg;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 984
    const/4 v0, 0x2

    iget-object v1, p0, Lmyg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 986
    :cond_1
    iget-object v0, p0, Lmyg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 988
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 967
    invoke-virtual {p0, p1}, Lmyg;->a(Loxn;)Lmyg;

    move-result-object v0

    return-object v0
.end method
