.class public Lhpu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheo;
.implements Liwq;
.implements Ljlw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lheo;",
        "Liwq;",
        "Ljlw",
        "<",
        "Lhpu;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Llpa;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lhei;

.field private final d:Ljlx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlx",
            "<",
            "Lhpu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Llpa;

    const-string v1, ""

    invoke-direct {v0, v1}, Llpa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhpu;->a:Llpa;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljlv;

    invoke-direct {v0, p0}, Ljlv;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lhpu;->d:Ljlx;

    .line 46
    iput-object p1, p0, Lhpu;->b:Landroid/content/Context;

    .line 47
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhpu;->c:Lhei;

    .line 48
    return-void
.end method

.method private a(IZ)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lhpu;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.autobackup.AutobackupAccountSettingsManager"

    invoke-interface {v0, v1}, Lhek;->f(Ljava/lang/String;)Lhek;

    move-result-object v0

    const-string v1, "auto_backup_enabled"

    invoke-interface {v0, v1, p2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    .line 80
    iget-object v0, p0, Lhpu;->d:Ljlx;

    invoke-interface {v0}, Ljlx;->a()V

    .line 81
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 89
    iget-object v0, p0, Lhpu;->c:Lhei;

    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 90
    iget-object v3, p0, Lhpu;->c:Lhei;

    invoke-interface {v3, v0}, Lhei;->a(I)Lhej;

    move-result-object v3

    .line 91
    invoke-interface {v3}, Lhej;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "is_plus_page"

    .line 92
    invoke-interface {v3, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 93
    const-string v4, "account_name"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "@youtube.com"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 94
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 98
    :cond_1
    return-object v1
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Lhpu;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 176
    const-string v1, "logged_out"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lhpu;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.autobackup.AutobackupAccountSettingsManager"

    .line 178
    invoke-interface {v0, v1}, Lhek;->f(Ljava/lang/String;)Lhek;

    move-result-object v0

    const-string v1, "auto_backup_enabled"

    const/4 v2, 0x0

    .line 179
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 180
    invoke-interface {v0}, Lhek;->c()I

    .line 182
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Lhem;)V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lheq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    new-instance v0, Lhpv;

    invoke-direct {v0}, Lhpv;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    return-void
.end method

.method public b()Ljlx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljlx",
            "<",
            "Lhpu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lhpu;->d:Ljlx;

    return-object v0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 62
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 63
    sget-object v0, Lhpu;->a:Llpa;

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempting to enable autobackup for INVALID_ID"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lhpu;->a(IZ)V

    .line 69
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhpu;->a(IZ)V

    .line 76
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lhpu;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 2

    .prologue
    .line 113
    invoke-virtual {p0}, Lhpu;->e()Ljava/util/List;

    move-result-object v0

    .line 114
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 115
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public d(I)Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lhpu;->b:Landroid/content/Context;

    invoke-static {v0}, Lhqd;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 143
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lhpu;->e(I)Z

    move-result v0

    goto :goto_0
.end method

.method public e()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 127
    invoke-virtual {p0}, Lhpu;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 128
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p0, v3}, Lhpu;->e(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 129
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 132
    :cond_1
    return-object v1
.end method

.method public e(I)Z
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lhpu;->c:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "com.google.android.libraries.social.autobackup.AutobackupAccountSettingsManager"

    invoke-interface {v0, v1}, Lhej;->d(Ljava/lang/String;)Lhej;

    move-result-object v0

    const-string v1, "auto_backup_enabled"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
