.class public final Ljqx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljpb;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 46
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 47
    invoke-interface {v0, p2}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "add_circle_notice_shown"

    const/4 v2, 0x1

    .line 48
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 49
    invoke-interface {v0}, Lhek;->c()I

    .line 50
    return-void
.end method

.method public a(Lhem;Loif;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 133
    if-eqz p2, :cond_1

    .line 134
    iget-object v0, p2, Loif;->a:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    const-string v0, "add_circle_notice_shown"

    invoke-interface {p1, v0, v1}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    .line 136
    const-string v0, "add_circle_notice_one_time_sync"

    invoke-interface {p1, v0, v1}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    .line 138
    :cond_0
    iget-object v0, p2, Loif;->b:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    const-string v0, "one_click_add_circle_notice_shown"

    invoke-interface {p1, v0, v1}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    .line 140
    const-string v0, "one_click_add_circle_notice_one_time_sync"

    invoke-interface {p1, v0, v1}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    .line 143
    :cond_1
    return-void
.end method

.method public a(Lu;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 179
    invoke-static {p1, p2, p3, p4}, Ljqv;->a(Lu;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 180
    return-void
.end method

.method public a(Lu;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 189
    invoke-static/range {p1 .. p6}, Ljqw;->a(Lu;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 191
    return-void
.end method

.method public a(Lhej;)Z
    .locals 1

    .prologue
    .line 66
    const-string v0, "add_circle_notice_one_time_sync"

    invoke-interface {p1, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 95
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 96
    invoke-interface {v0, p2}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "one_click_add_circle_notice_shown"

    const/4 v2, 0x1

    .line 97
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 98
    invoke-interface {v0}, Lhek;->c()I

    .line 99
    return-void
.end method

.method public b(Lhej;)Z
    .locals 1

    .prologue
    .line 116
    const-string v0, "one_click_add_circle_notice_one_time_sync"

    invoke-interface {p1, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c(Landroid/content/Context;I)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 147
    invoke-virtual {p0, p1, p2}, Ljqx;->g(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_5

    move v1, v2

    .line 149
    :goto_0
    invoke-virtual {p0, p1, p2}, Ljqx;->j(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 150
    invoke-virtual {p0, p1, p2}, Ljqx;->i(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_6

    move v4, v2

    .line 152
    :goto_1
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v5

    const/4 v0, 0x0

    if-eqz v1, :cond_1

    const-string v6, "is_plus_page"

    invoke-interface {v5, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "first_circle_picker"

    invoke-interface {v5, v6, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_7

    :goto_2
    invoke-virtual {p0, p1, p2}, Ljqx;->f(Landroid/content/Context;I)Z

    move-result v3

    if-nez v2, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    new-instance v0, Ljrj;

    invoke-direct {v0, p1, p2}, Ljrj;-><init>(Landroid/content/Context;I)V

    if-eqz v2, :cond_8

    invoke-virtual {v0}, Ljrj;->i()V

    :cond_1
    :goto_3
    if-eqz v4, :cond_3

    if-nez v0, :cond_2

    new-instance v0, Ljrj;

    invoke-direct {v0, p1, p2}, Ljrj;-><init>(Landroid/content/Context;I)V

    :cond_2
    invoke-virtual {v0}, Ljrj;->k()V

    .line 154
    :cond_3
    if-eqz v0, :cond_4

    .line 155
    invoke-virtual {v0}, Ljrj;->l()V

    .line 157
    invoke-virtual {v0}, Ljrj;->t()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 158
    const-string v1, "LegalNotifications"

    iget v2, v0, Lkff;->i:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Could not upload settings: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 169
    :cond_4
    :goto_4
    return-void

    :cond_5
    move v1, v3

    .line 147
    goto :goto_0

    :cond_6
    move v4, v3

    .line 150
    goto :goto_1

    :cond_7
    move v2, v3

    .line 152
    goto :goto_2

    :cond_8
    invoke-virtual {v0}, Ljrj;->j()V

    goto :goto_3

    .line 160
    :cond_9
    if-eqz v1, :cond_a

    .line 161
    invoke-virtual {p0, p1, p2}, Ljqx;->a(Landroid/content/Context;I)V

    .line 162
    invoke-virtual {p0, p1, p2}, Ljqx;->h(Landroid/content/Context;I)V

    .line 164
    :cond_a
    if-eqz v4, :cond_4

    .line 165
    invoke-virtual {p0, p1, p2}, Ljqx;->k(Landroid/content/Context;I)V

    goto :goto_4
.end method

.method public d(Landroid/content/Context;I)Z
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0, p1, p2}, Ljqx;->f(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Landroid/content/Context;I)Z
    .locals 1

    .prologue
    .line 184
    invoke-virtual {p0, p1, p2}, Ljqx;->i(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 36
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 37
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "add_circle_notice_shown"

    .line 38
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public g(Landroid/content/Context;I)Z
    .locals 1

    .prologue
    .line 57
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 58
    invoke-virtual {p0, v0}, Ljqx;->a(Lhej;)Z

    move-result v0

    return v0
.end method

.method public h(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 74
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 75
    invoke-interface {v0, p2}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "add_circle_notice_one_time_sync"

    const/4 v2, 0x1

    .line 76
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 77
    invoke-interface {v0}, Lhek;->c()I

    .line 78
    return-void
.end method

.method public i(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 85
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 86
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "one_click_add_circle_notice_shown"

    .line 87
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public j(Landroid/content/Context;I)Z
    .locals 1

    .prologue
    .line 107
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 108
    invoke-virtual {p0, v0}, Ljqx;->b(Lhej;)Z

    move-result v0

    return v0
.end method

.method public k(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 124
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 125
    invoke-interface {v0, p2}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "one_click_add_circle_notice_one_time_sync"

    const/4 v2, 0x1

    .line 126
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 127
    invoke-interface {v0}, Lhek;->c()I

    .line 128
    return-void
.end method
