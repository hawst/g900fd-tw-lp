.class public final Legu;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private N:Lcom/google/android/apps/plus/views/BestPhotosTileListView;

.field private ah:Lfca;

.field private ai:Z

.field private aj:Ljava/lang/Integer;

.field private ak:Ljava/lang/String;

.field private al:Z

.field private final am:Licq;

.field private final an:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Legi;-><init>()V

    .line 61
    new-instance v0, Licq;

    iget-object v1, p0, Legu;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064c

    .line 62
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Legu;->am:Licq;

    .line 65
    new-instance v0, Lhnw;

    new-instance v1, Legw;

    invoke-direct {v1, p0}, Legw;-><init>(Legu;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 78
    new-instance v0, Legv;

    invoke-direct {v0, p0}, Legv;-><init>(Legu;)V

    iput-object v0, p0, Legu;->an:Lfhh;

    .line 310
    return-void
.end method

.method static synthetic a(Legu;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Legu;->ak:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Legu;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Legu;->ak:Ljava/lang/String;

    return-object p1
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 294
    iget-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    .line 299
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Legu;->Z:Z

    .line 300
    iget-boolean v0, p0, Legu;->Z:Z

    if-eqz v0, :cond_3

    .line 301
    invoke-virtual {p0}, Legu;->n()Lz;

    move-result-object v0

    const v2, 0x7f0a07ac

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 302
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 303
    invoke-virtual {p0}, Legu;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Legu;->d(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 299
    goto :goto_1

    .line 305
    :cond_3
    invoke-virtual {p0}, Legu;->Z_()V

    goto :goto_0
.end method

.method static synthetic a(Legu;ILfib;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Legu;->a(ILfib;)V

    return-void
.end method

.method private d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 276
    if-nez p1, :cond_0

    .line 291
    :goto_0
    return-void

    .line 280
    :cond_0
    invoke-virtual {p0}, Legu;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 281
    iget-boolean v0, p0, Legu;->ai:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 282
    iget-object v0, p0, Legu;->am:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 289
    :goto_1
    invoke-virtual {p0}, Legu;->Z_()V

    .line 290
    invoke-virtual {p0}, Legu;->ag()V

    goto :goto_0

    .line 284
    :cond_1
    iget-object v0, p0, Legu;->am:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 287
    :cond_2
    iget-object v0, p0, Legu;->am:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 199
    sget-object v0, Lhmw;->aq:Lhmw;

    return-object v0
.end method

.method public J_()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 3

    .prologue
    .line 204
    invoke-super {p0}, Legi;->L_()V

    .line 206
    iget-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 210
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Legu;->Z:Z

    .line 211
    invoke-virtual {p0}, Legu;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Legu;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Legu;->ak:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->g(Landroid/content/Context;ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    .line 213
    const/4 v0, 0x1

    iput-boolean v0, p0, Legu;->al:Z

    .line 214
    invoke-virtual {p0}, Legu;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Legu;->d(Landroid/view/View;)V

    .line 215
    iget-object v0, p0, Legu;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Legu;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ek:Lhmv;

    .line 216
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 215
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Legu;->ah:Lfca;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 189
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 188
    :cond_1
    iget-object v0, p0, Legu;->ah:Lfca;

    invoke-virtual {v0}, Lfca;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 189
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 102
    iget-object v0, p0, Legu;->at:Llnl;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400d5

    invoke-super {p0, v0, p2, p3, v1}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 104
    iget-object v0, p0, Legu;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0291

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 107
    new-instance v2, Lfca;

    iget-object v3, p0, Legu;->at:Llnl;

    iget-object v4, p0, Legu;->P:Lhee;

    .line 108
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    iget-object v5, p0, Legu;->ak:Ljava/lang/String;

    .line 109
    invoke-static {v5}, Ljvj;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v6, v4, v5}, Lfca;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;)V

    iput-object v2, p0, Legu;->ah:Lfca;

    .line 110
    iget-object v2, p0, Legu;->ah:Lfca;

    invoke-virtual {v2, p0}, Lfca;->a(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v2, p0, Legu;->ah:Lfca;

    invoke-virtual {v2, v0}, Lfca;->a(I)V

    .line 113
    const v0, 0x7f100306

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/BestPhotosTileListView;

    iput-object v0, p0, Legu;->N:Lcom/google/android/apps/plus/views/BestPhotosTileListView;

    .line 114
    iget-object v0, p0, Legu;->N:Lcom/google/android/apps/plus/views/BestPhotosTileListView;

    new-instance v2, Legx;

    invoke-direct {v2}, Legx;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 115
    iget-object v0, p0, Legu;->N:Lcom/google/android/apps/plus/views/BestPhotosTileListView;

    iget-object v2, p0, Legu;->ah:Lfca;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/BestPhotosTileListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 117
    invoke-virtual {p0}, Legu;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 119
    invoke-direct {p0, v1}, Legu;->d(Landroid/view/View;)V

    .line 120
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    new-instance v0, Lfcc;

    iget-object v1, p0, Legu;->at:Llnl;

    iget-object v2, p0, Legu;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Legu;->ak:Ljava/lang/String;

    .line 257
    invoke-static {v3}, Ljvj;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lfcc;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 262
    const/4 v0, 0x1

    iput-boolean v0, p0, Legu;->ai:Z

    .line 263
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Legu;->al:Z

    if-nez v0, :cond_1

    .line 264
    invoke-virtual {p0}, Legu;->L_()V

    .line 267
    :cond_1
    iget-object v0, p0, Legu;->ah:Lfca;

    invoke-virtual {v0, p1}, Lfca;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 268
    invoke-virtual {p0}, Legu;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Legu;->d(Landroid/view/View;)V

    .line 269
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 90
    if-eqz p1, :cond_1

    .line 91
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    .line 95
    :cond_0
    const-string v0, "refreshed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Legu;->al:Z

    .line 97
    :cond_1
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 273
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 47
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Legu;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 183
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0}, Legi;->aO_()V

    .line 140
    iget-object v0, p0, Legu;->an:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 142
    iget-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    invoke-virtual {p0}, Legu;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Legu;->am:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 149
    iget-object v1, p0, Legu;->aj:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Legu;->a(ILfib;)V

    goto :goto_0
.end method

.method protected b(Lhjk;)V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 173
    const v0, 0x7f0a0a7b

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 174
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Legu;->a(Lhjk;I)V

    .line 176
    const v0, 0x7f10067b

    .line 177
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 178
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 179
    return-void
.end method

.method public c(Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 221
    const v0, 0x7f100079

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 222
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    const/4 v0, 0x0

    .line 251
    :goto_0
    return v0

    .line 226
    :cond_0
    const v1, 0x7f10007a

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 228
    iget-object v3, p0, Legu;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 229
    if-ne v1, v2, :cond_1

    .line 230
    new-instance v1, Ldew;

    invoke-virtual {p0}, Legu;->n()Lz;

    move-result-object v4

    invoke-direct {v1, v4, v3}, Ldew;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Legu;->Y:Lctq;

    .line 231
    invoke-virtual {v3}, Lctq;->c()I

    move-result v3

    invoke-virtual {v1, v3}, Ldew;->b(I)Ldew;

    move-result-object v1

    .line 232
    invoke-virtual {v1, v0}, Ldew;->a(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legu;->ak:Ljava/lang/String;

    .line 233
    invoke-static {v1}, Ljvj;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legu;->X:Lctz;

    .line 234
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legu;->Y:Lctq;

    .line 235
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v1

    .line 237
    iget-object v0, p0, Legu;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Legu;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->dZ:Lhmv;

    .line 238
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 237
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    move-object v0, v1

    .line 250
    :goto_1
    invoke-virtual {p0, v0}, Legu;->b(Landroid/content/Intent;)V

    move v0, v2

    .line 251
    goto :goto_0

    .line 240
    :cond_1
    const v0, 0x7f10007f

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 241
    invoke-virtual {p0}, Legu;->n()Lz;

    move-result-object v1

    invoke-static {v1, v3}, Leyq;->g(Landroid/content/Context;I)Ljuj;

    move-result-object v1

    .line 242
    invoke-virtual {v1, v0}, Ljuj;->a(Ljava/lang/String;)Ljuj;

    move-result-object v0

    const/4 v1, 0x5

    .line 243
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->a(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget-object v1, p0, Legu;->X:Lctz;

    .line 244
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->a(Ljcn;)Ljuj;

    move-result-object v0

    .line 245
    invoke-virtual {v0, v2}, Ljuj;->e(Z)Ljuj;

    move-result-object v0

    .line 246
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v1

    .line 247
    iget-object v0, p0, Legu;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Legu;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->eb:Lhmv;

    .line 248
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 247
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 162
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 163
    iget-object v0, p0, Legu;->aj:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 164
    const-string v0, "refresh_request"

    iget-object v1, p0, Legu;->aj:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 167
    :cond_0
    const-string v0, "refreshed"

    iget-boolean v1, p0, Legu;->al:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 168
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Legi;->g()V

    .line 133
    iget-object v0, p0, Legu;->N:Lcom/google/android/apps/plus/views/BestPhotosTileListView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 134
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Legi;->h()V

    .line 157
    iget-object v0, p0, Legu;->N:Lcom/google/android/apps/plus/views/BestPhotosTileListView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 158
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 125
    invoke-super {p0}, Legi;->z()V

    .line 127
    iget-object v0, p0, Legu;->an:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 128
    return-void
.end method
