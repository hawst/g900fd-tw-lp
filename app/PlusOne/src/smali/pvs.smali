.class public final Lpvs;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:[Lpvo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 681
    invoke-direct {p0}, Loxq;-><init>()V

    .line 692
    const/high16 v0, -0x80000000

    iput v0, p0, Lpvs;->a:I

    .line 695
    sget-object v0, Lpvo;->a:[Lpvo;

    iput-object v0, p0, Lpvs;->b:[Lpvo;

    .line 681
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 716
    .line 717
    iget v0, p0, Lpvs;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_2

    .line 718
    const/4 v0, 0x1

    iget v2, p0, Lpvs;->a:I

    .line 719
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 721
    :goto_0
    iget-object v2, p0, Lpvs;->b:[Lpvo;

    if-eqz v2, :cond_1

    .line 722
    iget-object v2, p0, Lpvs;->b:[Lpvo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 723
    if-eqz v4, :cond_0

    .line 724
    const/4 v5, 0x2

    .line 725
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 722
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 729
    :cond_1
    iget-object v1, p0, Lpvs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 730
    iput v0, p0, Lpvs;->ai:I

    .line 731
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpvs;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 739
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 740
    sparse-switch v0, :sswitch_data_0

    .line 744
    iget-object v2, p0, Lpvs;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 745
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpvs;->ah:Ljava/util/List;

    .line 748
    :cond_1
    iget-object v2, p0, Lpvs;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 750
    :sswitch_0
    return-object p0

    .line 755
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 756
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-ne v0, v2, :cond_3

    .line 761
    :cond_2
    iput v0, p0, Lpvs;->a:I

    goto :goto_0

    .line 763
    :cond_3
    iput v1, p0, Lpvs;->a:I

    goto :goto_0

    .line 768
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 769
    iget-object v0, p0, Lpvs;->b:[Lpvo;

    if-nez v0, :cond_5

    move v0, v1

    .line 770
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpvo;

    .line 771
    iget-object v3, p0, Lpvs;->b:[Lpvo;

    if-eqz v3, :cond_4

    .line 772
    iget-object v3, p0, Lpvs;->b:[Lpvo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 774
    :cond_4
    iput-object v2, p0, Lpvs;->b:[Lpvo;

    .line 775
    :goto_2
    iget-object v2, p0, Lpvs;->b:[Lpvo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 776
    iget-object v2, p0, Lpvs;->b:[Lpvo;

    new-instance v3, Lpvo;

    invoke-direct {v3}, Lpvo;-><init>()V

    aput-object v3, v2, v0

    .line 777
    iget-object v2, p0, Lpvs;->b:[Lpvo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 778
    invoke-virtual {p1}, Loxn;->a()I

    .line 775
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 769
    :cond_5
    iget-object v0, p0, Lpvs;->b:[Lpvo;

    array-length v0, v0

    goto :goto_1

    .line 781
    :cond_6
    iget-object v2, p0, Lpvs;->b:[Lpvo;

    new-instance v3, Lpvo;

    invoke-direct {v3}, Lpvo;-><init>()V

    aput-object v3, v2, v0

    .line 782
    iget-object v2, p0, Lpvs;->b:[Lpvo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 740
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 700
    iget v0, p0, Lpvs;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 701
    const/4 v0, 0x1

    iget v1, p0, Lpvs;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 703
    :cond_0
    iget-object v0, p0, Lpvs;->b:[Lpvo;

    if-eqz v0, :cond_2

    .line 704
    iget-object v1, p0, Lpvs;->b:[Lpvo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 705
    if-eqz v3, :cond_1

    .line 706
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 704
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 710
    :cond_2
    iget-object v0, p0, Lpvs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 712
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 677
    invoke-virtual {p0, p1}, Lpvs;->a(Loxn;)Lpvs;

    move-result-object v0

    return-object v0
.end method
