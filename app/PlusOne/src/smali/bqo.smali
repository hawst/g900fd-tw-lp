.class public Lbqo;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbqk;

.field private final c:Lbpz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpz",
            "<",
            "Lbqp;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lbpz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbpz",
            "<",
            "Lbmm;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lbqp;

.field private final f:Ljava/lang/Object;

.field private g:Lbgf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lbqo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbqo;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbqk;Lbpz;Lbpz;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbqk;",
            "Lbpz",
            "<",
            "Lbqp;",
            ">;",
            "Lbpz",
            "<",
            "Lbmm;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbqo;->f:Ljava/lang/Object;

    .line 94
    const-string v0, "posterExtractor"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqk;

    iput-object v0, p0, Lbqo;->b:Lbqk;

    .line 95
    const-string v0, "posterCache"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpz;

    iput-object v0, p0, Lbqo;->c:Lbpz;

    .line 96
    const-string v0, "lowResPosterCache"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpz;

    iput-object v0, p0, Lbqo;->d:Lbpz;

    .line 97
    new-instance v0, Lbqp;

    invoke-direct {v0}, Lbqp;-><init>()V

    iput-object v0, p0, Lbqo;->e:Lbqp;

    .line 98
    return-void
.end method

.method private a(Lbmm;IILbqp;)V
    .locals 6

    .prologue
    .line 292
    const-string v0, "posterInfoOut"

    const/4 v1, 0x0

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 294
    iget-object v0, p0, Lbqo;->b:Lbqk;

    iget-object v1, p1, Lbmm;->a:Lbml;

    invoke-virtual {v0, v1, p2, p3}, Lbqk;->a(Lbml;II)F

    move-result v0

    .line 296
    iget-object v1, p1, Lbmm;->a:Lbml;

    invoke-static {v1}, Lbqu;->a(Lbml;)I

    move-result v1

    .line 297
    iget-object v2, p1, Lbmm;->a:Lbml;

    invoke-static {v2}, Lbqu;->b(Lbml;)I

    move-result v2

    .line 298
    int-to-float v1, v1

    mul-float/2addr v1, v0

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v1, v4

    .line 299
    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 301
    iput-object p1, p4, Lbqp;->a:Lbmm;

    .line 302
    iput v1, p4, Lbqp;->b:I

    .line 303
    iput v0, p4, Lbqp;->c:I

    .line 304
    return-void
.end method

.method private b()Lbgf;
    .locals 2

    .prologue
    .line 308
    iget-object v1, p0, Lbqo;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 309
    :try_start_0
    iget-object v0, p0, Lbqo;->g:Lbgf;

    monitor-exit v1

    return-object v0

    .line 310
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lbmm;)Lbqt;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lbqo;->d:Lbpz;

    invoke-virtual {v0, p1}, Lbpz;->a(Ljava/lang/Object;)Lbqq;

    move-result-object v0

    check-cast v0, Lbqt;

    return-object v0
.end method

.method public a(Lbmm;II)Lbqt;
    .locals 7

    .prologue
    const/high16 v6, 0x43000000    # 128.0f

    const/4 v1, 0x0

    .line 144
    invoke-direct {p0}, Lbqo;->b()Lbgf;

    move-result-object v0

    .line 145
    if-nez v0, :cond_0

    .line 146
    sget-object v0, Lbqo;->a:Ljava/lang/String;

    move-object v0, v1

    .line 167
    :goto_0
    return-object v0

    .line 154
    :cond_0
    iget-object v2, p0, Lbqo;->e:Lbqp;

    monitor-enter v2

    .line 155
    :try_start_0
    iget-object v0, p0, Lbqo;->e:Lbqp;

    invoke-direct {p0, p1, p2, p3, v0}, Lbqo;->a(Lbmm;IILbqp;)V

    .line 156
    iget-object v0, p0, Lbqo;->c:Lbpz;

    iget-object v3, p0, Lbqo;->e:Lbqp;

    invoke-virtual {v0, v3}, Lbpz;->a(Ljava/lang/Object;)Lbqq;

    move-result-object v0

    check-cast v0, Lbqt;

    .line 157
    if-eqz v0, :cond_1

    .line 158
    monitor-exit v2

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 160
    :cond_1
    :try_start_1
    new-instance v3, Lbqp;

    iget-object v0, p0, Lbqo;->e:Lbqp;

    invoke-direct {v3, v0}, Lbqp;-><init>(Lbqp;)V

    .line 161
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    const-string v0, "extractPoster"

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V

    .line 165
    :try_start_2
    invoke-direct {p0}, Lbqo;->b()Lbgf;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 167
    :cond_2
    :goto_1
    invoke-static {}, Laep;->a()V

    goto :goto_0

    .line 165
    :cond_3
    :try_start_3
    iget-object v2, p0, Lbqo;->b:Lbqk;

    invoke-virtual {v2, v0, v3}, Lbqk;->a(Lbgf;Lbqp;)Lbqt;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lbqo;->c:Lbpz;

    invoke-virtual {v1, v3, v0}, Lbpz;->a(Ljava/lang/Object;Lbqq;)V

    invoke-virtual {v0}, Lbqt;->d()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Lbqt;->d()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-lez v1, :cond_2

    if-lez v2, :cond_2

    int-to-float v4, v1

    div-float v4, v6, v4

    int-to-float v5, v2

    div-float v5, v6, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-virtual {v0}, Lbqt;->d()Landroid/graphics/Bitmap;

    move-result-object v5

    int-to-float v1, v1

    mul-float/2addr v1, v4

    float-to-int v1, v1

    int-to-float v2, v2

    mul-float/2addr v2, v4

    float-to-int v2, v2

    invoke-static {v5, v1, v2}, Lceh;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Lbqt;

    invoke-direct {v2, v1}, Lbqt;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lbqo;->d:Lbpz;

    iget-object v3, v3, Lbqp;->a:Lbmm;

    invoke-virtual {v1, v3, v2}, Lbpz;->a(Ljava/lang/Object;Lbqq;)V

    invoke-virtual {v2}, Lbqt;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 167
    :catchall_1
    move-exception v0

    invoke-static {}, Laep;->a()V

    throw v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 114
    iget-object v1, p0, Lbqo;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lbqo;->g:Lbgf;

    .line 116
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lbgf;)V
    .locals 2

    .prologue
    .line 104
    const-string v0, "renderContext"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 105
    iget-object v1, p0, Lbqo;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 106
    :try_start_0
    iput-object p1, p0, Lbqo;->g:Lbgf;

    .line 107
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public varargs a(Lbqm;Lbon;II[J)V
    .locals 7

    .prologue
    .line 190
    invoke-direct {p0}, Lbqo;->b()Lbgf;

    move-result-object v1

    .line 191
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "PosterStore not initialized"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 193
    const-string v0, "callback"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lbqo;->b:Lbqk;

    invoke-virtual {v0, p2, p3, p4}, Lbqk;->a(Lbml;II)F

    move-result v0

    .line 197
    invoke-static {p2}, Lbqu;->a(Lbml;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v4, v2

    .line 199
    invoke-static {p2}, Lbqu;->b(Lbml;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v5, v2

    .line 200
    iget-object v0, p0, Lbqo;->b:Lbqk;

    move-object v2, p1

    move-object v3, p2

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lbqk;->a(Lbgf;Lbqm;Lbon;II[J)V

    .line 202
    return-void

    .line 191
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lbmm;II)Lbqt;
    .locals 3

    .prologue
    .line 223
    iget-object v1, p0, Lbqo;->e:Lbqp;

    monitor-enter v1

    .line 224
    :try_start_0
    iget-object v0, p0, Lbqo;->e:Lbqp;

    invoke-direct {p0, p1, p2, p3, v0}, Lbqo;->a(Lbmm;IILbqp;)V

    .line 225
    iget-object v0, p0, Lbqo;->c:Lbpz;

    iget-object v2, p0, Lbqo;->e:Lbqp;

    invoke-virtual {v0, v2}, Lbpz;->a(Ljava/lang/Object;)Lbqq;

    move-result-object v0

    check-cast v0, Lbqt;

    .line 226
    monitor-exit v1

    .line 227
    return-object v0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
