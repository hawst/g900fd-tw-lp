.class public final Lpsz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpsz;


# instance fields
.field public b:Ljava/lang/String;

.field private c:Lpgx;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Integer;

.field private f:[Lpsz;

.field private g:[Lpsy;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x0

    new-array v0, v0, [Lpsz;

    sput-object v0, Lpsz;->a:[Lpsz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0}, Loxq;-><init>()V

    .line 244
    const/4 v0, 0x0

    iput-object v0, p0, Lpsz;->c:Lpgx;

    .line 253
    sget-object v0, Lpsz;->a:[Lpsz;

    iput-object v0, p0, Lpsz;->f:[Lpsz;

    .line 256
    sget-object v0, Lpsy;->a:[Lpsy;

    iput-object v0, p0, Lpsz;->g:[Lpsy;

    .line 259
    const/high16 v0, -0x80000000

    iput v0, p0, Lpsz;->h:I

    .line 235
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 299
    .line 300
    iget-object v0, p0, Lpsz;->c:Lpgx;

    if-eqz v0, :cond_8

    .line 301
    const/4 v0, 0x1

    iget-object v2, p0, Lpsz;->c:Lpgx;

    .line 302
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 304
    :goto_0
    iget-object v2, p0, Lpsz;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 305
    const/4 v2, 0x2

    iget-object v3, p0, Lpsz;->b:Ljava/lang/String;

    .line 306
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 308
    :cond_0
    iget-object v2, p0, Lpsz;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 309
    const/4 v2, 0x3

    iget-object v3, p0, Lpsz;->e:Ljava/lang/Integer;

    .line 310
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 312
    :cond_1
    iget-object v2, p0, Lpsz;->f:[Lpsz;

    if-eqz v2, :cond_3

    .line 313
    iget-object v3, p0, Lpsz;->f:[Lpsz;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 314
    if-eqz v5, :cond_2

    .line 315
    const/4 v6, 0x4

    .line 316
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 313
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 320
    :cond_3
    iget-object v2, p0, Lpsz;->g:[Lpsy;

    if-eqz v2, :cond_5

    .line 321
    iget-object v2, p0, Lpsz;->g:[Lpsy;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 322
    if-eqz v4, :cond_4

    .line 323
    const/4 v5, 0x5

    .line 324
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 321
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 328
    :cond_5
    iget v1, p0, Lpsz;->h:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_6

    .line 329
    const/4 v1, 0x6

    iget v2, p0, Lpsz;->h:I

    .line 330
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 332
    :cond_6
    iget-object v1, p0, Lpsz;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 333
    const/4 v1, 0x7

    iget-object v2, p0, Lpsz;->d:Ljava/lang/Boolean;

    .line 334
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 336
    :cond_7
    iget-object v1, p0, Lpsz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 337
    iput v0, p0, Lpsz;->ai:I

    .line 338
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpsz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 346
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 347
    sparse-switch v0, :sswitch_data_0

    .line 351
    iget-object v2, p0, Lpsz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 352
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpsz;->ah:Ljava/util/List;

    .line 355
    :cond_1
    iget-object v2, p0, Lpsz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 357
    :sswitch_0
    return-object p0

    .line 362
    :sswitch_1
    iget-object v0, p0, Lpsz;->c:Lpgx;

    if-nez v0, :cond_2

    .line 363
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lpsz;->c:Lpgx;

    .line 365
    :cond_2
    iget-object v0, p0, Lpsz;->c:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 369
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpsz;->b:Ljava/lang/String;

    goto :goto_0

    .line 373
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpsz;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 377
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 378
    iget-object v0, p0, Lpsz;->f:[Lpsz;

    if-nez v0, :cond_4

    move v0, v1

    .line 379
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpsz;

    .line 380
    iget-object v3, p0, Lpsz;->f:[Lpsz;

    if-eqz v3, :cond_3

    .line 381
    iget-object v3, p0, Lpsz;->f:[Lpsz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 383
    :cond_3
    iput-object v2, p0, Lpsz;->f:[Lpsz;

    .line 384
    :goto_2
    iget-object v2, p0, Lpsz;->f:[Lpsz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 385
    iget-object v2, p0, Lpsz;->f:[Lpsz;

    new-instance v3, Lpsz;

    invoke-direct {v3}, Lpsz;-><init>()V

    aput-object v3, v2, v0

    .line 386
    iget-object v2, p0, Lpsz;->f:[Lpsz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 387
    invoke-virtual {p1}, Loxn;->a()I

    .line 384
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 378
    :cond_4
    iget-object v0, p0, Lpsz;->f:[Lpsz;

    array-length v0, v0

    goto :goto_1

    .line 390
    :cond_5
    iget-object v2, p0, Lpsz;->f:[Lpsz;

    new-instance v3, Lpsz;

    invoke-direct {v3}, Lpsz;-><init>()V

    aput-object v3, v2, v0

    .line 391
    iget-object v2, p0, Lpsz;->f:[Lpsz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 395
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 396
    iget-object v0, p0, Lpsz;->g:[Lpsy;

    if-nez v0, :cond_7

    move v0, v1

    .line 397
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpsy;

    .line 398
    iget-object v3, p0, Lpsz;->g:[Lpsy;

    if-eqz v3, :cond_6

    .line 399
    iget-object v3, p0, Lpsz;->g:[Lpsy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 401
    :cond_6
    iput-object v2, p0, Lpsz;->g:[Lpsy;

    .line 402
    :goto_4
    iget-object v2, p0, Lpsz;->g:[Lpsy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 403
    iget-object v2, p0, Lpsz;->g:[Lpsy;

    new-instance v3, Lpsy;

    invoke-direct {v3}, Lpsy;-><init>()V

    aput-object v3, v2, v0

    .line 404
    iget-object v2, p0, Lpsz;->g:[Lpsy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 405
    invoke-virtual {p1}, Loxn;->a()I

    .line 402
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 396
    :cond_7
    iget-object v0, p0, Lpsz;->g:[Lpsy;

    array-length v0, v0

    goto :goto_3

    .line 408
    :cond_8
    iget-object v2, p0, Lpsz;->g:[Lpsy;

    new-instance v3, Lpsy;

    invoke-direct {v3}, Lpsy;-><init>()V

    aput-object v3, v2, v0

    .line 409
    iget-object v2, p0, Lpsz;->g:[Lpsy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 413
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 414
    if-eqz v0, :cond_9

    const/4 v2, 0x1

    if-eq v0, v2, :cond_9

    const/4 v2, 0x2

    if-ne v0, v2, :cond_a

    .line 417
    :cond_9
    iput v0, p0, Lpsz;->h:I

    goto/16 :goto_0

    .line 419
    :cond_a
    iput v1, p0, Lpsz;->h:I

    goto/16 :goto_0

    .line 424
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpsz;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 347
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 264
    iget-object v1, p0, Lpsz;->c:Lpgx;

    if-eqz v1, :cond_0

    .line 265
    const/4 v1, 0x1

    iget-object v2, p0, Lpsz;->c:Lpgx;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 267
    :cond_0
    iget-object v1, p0, Lpsz;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 268
    const/4 v1, 0x2

    iget-object v2, p0, Lpsz;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 270
    :cond_1
    iget-object v1, p0, Lpsz;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 271
    const/4 v1, 0x3

    iget-object v2, p0, Lpsz;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 273
    :cond_2
    iget-object v1, p0, Lpsz;->f:[Lpsz;

    if-eqz v1, :cond_4

    .line 274
    iget-object v2, p0, Lpsz;->f:[Lpsz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 275
    if-eqz v4, :cond_3

    .line 276
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 274
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 280
    :cond_4
    iget-object v1, p0, Lpsz;->g:[Lpsy;

    if-eqz v1, :cond_6

    .line 281
    iget-object v1, p0, Lpsz;->g:[Lpsy;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 282
    if-eqz v3, :cond_5

    .line 283
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 281
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 287
    :cond_6
    iget v0, p0, Lpsz;->h:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_7

    .line 288
    const/4 v0, 0x6

    iget v1, p0, Lpsz;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 290
    :cond_7
    iget-object v0, p0, Lpsz;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 291
    const/4 v0, 0x7

    iget-object v1, p0, Lpsz;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 293
    :cond_8
    iget-object v0, p0, Lpsz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 295
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 231
    invoke-virtual {p0, p1}, Lpsz;->a(Loxn;)Lpsz;

    move-result-object v0

    return-object v0
.end method
