.class public final Lpfk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Llug;

.field public c:Ljava/lang/Integer;

.field public d:[I

.field public e:[B

.field private f:[I

.field private g:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x4902a31

    new-instance v1, Lpfl;

    invoke-direct {v1}, Lpfl;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lpfk;->b:Llug;

    .line 24
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lpfk;->f:[I

    .line 27
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lpfk;->d:[I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 67
    .line 68
    iget-object v0, p0, Lpfk;->a:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 69
    const/4 v0, 0x1

    iget-object v2, p0, Lpfk;->a:Ljava/lang/String;

    .line 70
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 72
    :goto_0
    iget-object v2, p0, Lpfk;->b:Llug;

    if-eqz v2, :cond_0

    .line 73
    const/4 v2, 0x2

    iget-object v3, p0, Lpfk;->b:Llug;

    .line 74
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 76
    :cond_0
    iget-object v2, p0, Lpfk;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 77
    const/4 v2, 0x3

    iget-object v3, p0, Lpfk;->c:Ljava/lang/Integer;

    .line 78
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 80
    :cond_1
    iget-object v2, p0, Lpfk;->f:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lpfk;->f:[I

    array-length v2, v2

    if-lez v2, :cond_3

    .line 82
    iget-object v4, p0, Lpfk;->f:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_2

    aget v6, v4, v2

    .line 84
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 82
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 86
    :cond_2
    add-int/2addr v0, v3

    .line 87
    iget-object v2, p0, Lpfk;->f:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 89
    :cond_3
    iget-object v2, p0, Lpfk;->d:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Lpfk;->d:[I

    array-length v2, v2

    if-lez v2, :cond_5

    .line 91
    iget-object v3, p0, Lpfk;->d:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_4

    aget v5, v3, v1

    .line 93
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 95
    :cond_4
    add-int/2addr v0, v2

    .line 96
    iget-object v1, p0, Lpfk;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 98
    :cond_5
    iget-object v1, p0, Lpfk;->e:[B

    if-eqz v1, :cond_6

    .line 99
    const/4 v1, 0x6

    iget-object v2, p0, Lpfk;->e:[B

    .line 100
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_6
    iget-object v1, p0, Lpfk;->g:[B

    if-eqz v1, :cond_7

    .line 103
    const/4 v1, 0x7

    iget-object v2, p0, Lpfk;->g:[B

    .line 104
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_7
    iget-object v1, p0, Lpfk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    iput v0, p0, Lpfk;->ai:I

    .line 108
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpfk;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 116
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 117
    sparse-switch v0, :sswitch_data_0

    .line 121
    iget-object v1, p0, Lpfk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpfk;->ah:Ljava/util/List;

    .line 125
    :cond_1
    iget-object v1, p0, Lpfk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    :sswitch_0
    return-object p0

    .line 132
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpfk;->a:Ljava/lang/String;

    goto :goto_0

    .line 136
    :sswitch_2
    iget-object v0, p0, Lpfk;->b:Llug;

    if-nez v0, :cond_2

    .line 137
    new-instance v0, Llug;

    invoke-direct {v0}, Llug;-><init>()V

    iput-object v0, p0, Lpfk;->b:Llug;

    .line 139
    :cond_2
    iget-object v0, p0, Lpfk;->b:Llug;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 143
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpfk;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 147
    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 148
    iget-object v0, p0, Lpfk;->f:[I

    array-length v0, v0

    .line 149
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 150
    iget-object v2, p0, Lpfk;->f:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 151
    iput-object v1, p0, Lpfk;->f:[I

    .line 152
    :goto_1
    iget-object v1, p0, Lpfk;->f:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 153
    iget-object v1, p0, Lpfk;->f:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 154
    invoke-virtual {p1}, Loxn;->a()I

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 157
    :cond_3
    iget-object v1, p0, Lpfk;->f:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 161
    :sswitch_5
    const/16 v0, 0x28

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 162
    iget-object v0, p0, Lpfk;->d:[I

    array-length v0, v0

    .line 163
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 164
    iget-object v2, p0, Lpfk;->d:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    iput-object v1, p0, Lpfk;->d:[I

    .line 166
    :goto_2
    iget-object v1, p0, Lpfk;->d:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 167
    iget-object v1, p0, Lpfk;->d:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 168
    invoke-virtual {p1}, Loxn;->a()I

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 171
    :cond_4
    iget-object v1, p0, Lpfk;->d:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 175
    :sswitch_6
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpfk;->e:[B

    goto/16 :goto_0

    .line 179
    :sswitch_7
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lpfk;->g:[B

    goto/16 :goto_0

    .line 117
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 36
    iget-object v1, p0, Lpfk;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 37
    const/4 v1, 0x1

    iget-object v2, p0, Lpfk;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 39
    :cond_0
    iget-object v1, p0, Lpfk;->b:Llug;

    if-eqz v1, :cond_1

    .line 40
    const/4 v1, 0x2

    iget-object v2, p0, Lpfk;->b:Llug;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 42
    :cond_1
    iget-object v1, p0, Lpfk;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 43
    const/4 v1, 0x3

    iget-object v2, p0, Lpfk;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 45
    :cond_2
    iget-object v1, p0, Lpfk;->f:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lpfk;->f:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 46
    iget-object v2, p0, Lpfk;->f:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget v4, v2, v1

    .line 47
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 46
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50
    :cond_3
    iget-object v1, p0, Lpfk;->d:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lpfk;->d:[I

    array-length v1, v1

    if-lez v1, :cond_4

    .line 51
    iget-object v1, p0, Lpfk;->d:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget v3, v1, v0

    .line 52
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 55
    :cond_4
    iget-object v0, p0, Lpfk;->e:[B

    if-eqz v0, :cond_5

    .line 56
    const/4 v0, 0x6

    iget-object v1, p0, Lpfk;->e:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 58
    :cond_5
    iget-object v0, p0, Lpfk;->g:[B

    if-eqz v0, :cond_6

    .line 59
    const/4 v0, 0x7

    iget-object v1, p0, Lpfk;->g:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 61
    :cond_6
    iget-object v0, p0, Lpfk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 63
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpfk;->a(Loxn;)Lpfk;

    move-result-object v0

    return-object v0
.end method
