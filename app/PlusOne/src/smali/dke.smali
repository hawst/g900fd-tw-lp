.class public final Ldke;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmjq;",
        "Lmjr;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[I


# instance fields
.field private a:Ldvq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ldke;->b:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 6

    .prologue
    .line 52
    const-string v3, "settingsfetch"

    new-instance v4, Lmjq;

    invoke-direct {v4}, Lmjq;-><init>()V

    new-instance v5, Lmjr;

    invoke-direct {v5}, Lmjr;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 54
    return-void
.end method


# virtual methods
.method protected a(Lmjq;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 58
    new-instance v0, Lnps;

    invoke-direct {v0}, Lnps;-><init>()V

    iput-object v0, p1, Lmjq;->a:Lnps;

    .line 59
    iget-object v0, p1, Lmjq;->a:Lnps;

    .line 60
    new-instance v1, Lnpb;

    invoke-direct {v1}, Lnpb;-><init>()V

    iput-object v1, v0, Lnps;->a:Lnpb;

    .line 61
    iget-object v1, v0, Lnps;->a:Lnpb;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnpb;->d:Ljava/lang/Boolean;

    .line 62
    iget-object v1, v0, Lnps;->a:Lnpb;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnpb;->a:Ljava/lang/Boolean;

    .line 63
    iget-object v1, v0, Lnps;->a:Lnpb;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnpb;->b:Ljava/lang/Boolean;

    .line 64
    iget-object v1, v0, Lnps;->a:Lnpb;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnpb;->c:Ljava/lang/Boolean;

    .line 65
    iget-object v0, v0, Lnps;->a:Lnpb;

    sget-object v1, Ldke;->b:[I

    iput-object v1, v0, Lnpb;->e:[I

    .line 66
    return-void
.end method

.method protected a(Lmjr;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-object v0, p1, Lmjr;->a:Lnpm;

    iget-object v3, v0, Lnpm;->a:Lnpf;

    .line 71
    if-eqz v3, :cond_0

    iget-object v0, v3, Lnpf;->a:Lnox;

    if-nez v0, :cond_1

    .line 72
    :cond_0
    new-instance v0, Lkfm;

    const-string v1, "Notification settings missing from response"

    invoke-direct {v0, v1}, Lkfm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1
    iget-object v4, v3, Lnpf;->a:Lnox;

    .line 75
    iget-object v5, v3, Lnpf;->b:Lnou;

    .line 76
    iget-object v0, v4, Lnox;->a:[Lnoy;

    if-eqz v0, :cond_2

    iget-object v0, v4, Lnox;->c:[Lnpa;

    if-eqz v0, :cond_2

    if-nez v5, :cond_3

    .line 79
    :cond_2
    new-instance v0, Lkfm;

    const-string v1, "Invalid notification settings response"

    invoke-direct {v0, v1}, Lkfm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_3
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 84
    iget-object v7, v4, Lnox;->a:[Lnoy;

    .line 86
    array-length v8, v7

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_6

    .line 87
    aget-object v9, v7, v2

    .line 88
    iget v10, v9, Lnoy;->c:I

    .line 89
    iget-object v0, v9, Lnoy;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 94
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 95
    if-nez v0, :cond_4

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 99
    :cond_4
    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 103
    :cond_6
    iget-object v2, v4, Lnox;->c:[Lnpa;

    .line 104
    array-length v0, v2

    new-array v7, v0, [Ldvo;

    .line 106
    array-length v8, v2

    :goto_1
    if-ge v1, v8, :cond_8

    .line 107
    aget-object v9, v2, v1

    .line 108
    iget-object v0, v9, Lnpa;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 110
    iget v0, v9, Lnpa;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 114
    new-instance v10, Ldvo;

    iget-object v9, v9, Lnpa;->c:Ljava/lang/String;

    .line 115
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    new-array v11, v11, [Lnoy;

    invoke-interface {v0, v11}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnoy;

    invoke-direct {v10, v9, v0}, Ldvo;-><init>(Ljava/lang/String;[Lnoy;)V

    aput-object v10, v7, v1

    .line 106
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 118
    :cond_8
    invoke-static {}, Ldvs;->a()Ldvs;

    move-result-object v0

    .line 119
    iget-object v1, v4, Lnox;->b:Ljava/lang/String;

    .line 120
    invoke-virtual {v0, v1}, Ldvs;->a(Ljava/lang/String;)Ldvs;

    move-result-object v0

    iget v1, v5, Lnou;->a:I

    .line 121
    invoke-virtual {v0, v1}, Ldvs;->a(I)Ldvs;

    move-result-object v0

    .line 122
    invoke-virtual {v0, v7}, Ldvs;->a([Ldvo;)Ldvs;

    move-result-object v0

    iget-object v1, p0, Ldke;->f:Landroid/content/Context;

    iget v2, p0, Ldke;->c:I

    iget-object v3, v3, Lnpf;->c:Lnpk;

    .line 123
    invoke-virtual {v0, v1, v2, v3}, Ldvs;->a(Landroid/content/Context;ILnpk;)Ldvs;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Ldvs;->b()Ldvq;

    move-result-object v0

    iput-object v0, p0, Ldke;->a:Ldvq;

    .line 125
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Lmjq;

    invoke-virtual {p0, p1}, Ldke;->a(Lmjq;)V

    return-void
.end method

.method public b()Ldvq;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Ldke;->a:Ldvq;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 29
    check-cast p1, Lmjr;

    invoke-virtual {p0, p1}, Ldke;->a(Lmjr;)V

    return-void
.end method
