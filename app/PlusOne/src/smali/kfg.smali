.class public final Lkfg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lkfh;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lkfh;

.field private c:J

.field private d:J

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lkfg;->d:J

    .line 63
    return-void
.end method

.method public a(JJ)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iput-wide p1, v0, Lkfh;->b:J

    .line 98
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iput-wide p3, v0, Lkfh;->c:J

    .line 99
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 112
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 113
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 114
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 115
    iget-object v2, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 117
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfh;

    iput-object v0, p0, Lkfg;->b:Lkfh;

    .line 48
    iget-object v0, p0, Lkfg;->b:Lkfh;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lkfh;

    invoke-direct {v0}, Lkfh;-><init>()V

    iput-object v0, p0, Lkfg;->b:Lkfh;

    .line 50
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iput-object p1, v0, Lkfh;->a:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iput-object p2, v0, Lkfh;->h:[Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    iget-object v1, p0, Lkfg;->b:Lkfh;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lkfg;->c:J

    .line 55
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lkfg;->e:J

    .line 56
    return-void
.end method

.method public a(Lkfk;)V
    .locals 6

    .prologue
    .line 102
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iget-wide v2, v0, Lkfh;->e:J

    invoke-virtual {p1}, Lkfk;->a()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v0, Lkfh;->e:J

    .line 103
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iget-wide v2, v0, Lkfh;->f:J

    invoke-virtual {p1}, Lkfk;->b()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v0, Lkfh;->f:J

    .line 104
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iget-wide v2, v0, Lkfh;->d:J

    invoke-virtual {p1}, Lkfk;->e()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, v0, Lkfh;->d:J

    .line 105
    iget-object v0, p0, Lkfg;->b:Lkfh;

    invoke-virtual {p1}, Lkfk;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lkfh;->g:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public b()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 67
    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfh;

    .line 68
    iget-object v0, v0, Lkfh;->h:[Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 74
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lkfg;->e:J

    .line 75
    return-void
.end method

.method public d()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 78
    iget-wide v0, p0, Lkfg;->e:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iget-wide v2, v0, Lkfh;->c:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lkfg;->e:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, v0, Lkfh;->c:J

    .line 80
    iput-wide v8, p0, Lkfg;->e:J

    .line 82
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lkfg;->f:J

    .line 83
    return-void
.end method

.method public e()V
    .locals 8

    .prologue
    .line 89
    invoke-virtual {p0}, Lkfg;->d()V

    .line 90
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iget-wide v2, v0, Lkfh;->b:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lkfg;->c:J

    sub-long/2addr v4, v6

    add-long/2addr v2, v4

    iput-wide v2, v0, Lkfh;->b:J

    .line 91
    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 126
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Unknown"

    goto :goto_0
.end method

.method public g()J
    .locals 5

    .prologue
    .line 136
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 137
    const-wide/16 v0, 0x0

    .line 138
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 139
    iget-object v1, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfh;

    iget-wide v0, v0, Lkfh;->b:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 140
    goto :goto_0

    .line 141
    :cond_0
    return-wide v2
.end method

.method public h()J
    .locals 5

    .prologue
    .line 150
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 151
    const-wide/16 v0, 0x0

    .line 152
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 153
    iget-object v1, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfh;

    iget-wide v0, v0, Lkfh;->c:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 154
    goto :goto_0

    .line 155
    :cond_0
    return-wide v2
.end method

.method public i()J
    .locals 5

    .prologue
    .line 164
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 165
    const-wide/16 v0, 0x0

    .line 166
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 167
    iget-object v1, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfh;

    iget-wide v0, v0, Lkfh;->d:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 168
    goto :goto_0

    .line 170
    :cond_0
    return-wide v2
.end method

.method public j()J
    .locals 5

    .prologue
    .line 179
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 180
    const-wide/16 v0, 0x0

    .line 181
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 182
    iget-object v1, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfh;

    iget-wide v0, v0, Lkfh;->e:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 183
    goto :goto_0

    .line 184
    :cond_0
    return-wide v2
.end method

.method public k()J
    .locals 2

    .prologue
    .line 193
    iget-wide v0, p0, Lkfg;->c:J

    return-wide v0
.end method

.method public l()J
    .locals 2

    .prologue
    .line 202
    iget-wide v0, p0, Lkfg;->f:J

    return-wide v0
.end method

.method public m()J
    .locals 2

    .prologue
    .line 211
    iget-wide v0, p0, Lkfg;->d:J

    return-wide v0
.end method

.method public n()J
    .locals 5

    .prologue
    .line 220
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 221
    const-wide/16 v0, 0x0

    .line 222
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 223
    iget-object v1, p0, Lkfg;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfh;

    iget-wide v0, v0, Lkfh;->f:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 224
    goto :goto_0

    .line 225
    :cond_0
    return-wide v2
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lkfg;->b:Lkfh;

    iget-object v0, v0, Lkfh;->g:Ljava/lang/String;

    return-object v0
.end method
