.class final Leub;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lilo;


# instance fields
.field private synthetic a:Letn;


# direct methods
.method constructor <init>(Letn;)V
    .locals 0

    .prologue
    .line 2134
    iput-object p1, p0, Leub;->a:Letn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 2137
    new-instance v0, Ling;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Ling;-><init>(DD)V

    .line 2138
    const-string v1, "FriendLocations"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2139
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "onLocationChanged: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2141
    :cond_0
    iget-object v1, p0, Leub;->a:Letn;

    invoke-static {v1}, Letn;->r(Letn;)Ling;

    move-result-object v1

    invoke-virtual {v0, v1}, Ling;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2142
    iget-object v1, p0, Leub;->a:Letn;

    invoke-static {v1, v0}, Letn;->a(Letn;Ling;)Ling;

    .line 2143
    iget-object v0, p0, Leub;->a:Letn;

    invoke-static {v0}, Letn;->k(Letn;)V

    .line 2145
    :cond_1
    return-void
.end method
