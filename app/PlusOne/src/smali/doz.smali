.class public final Ldoz;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Llae;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILlae;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 30
    const-string v0, "GetNearbyActivitiesTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 32
    iput p2, p0, Ldoz;->a:I

    .line 33
    iput-object p3, p0, Ldoz;->b:Llae;

    .line 34
    iput-object p4, p0, Ldoz;->c:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    .line 39
    invoke-virtual {p0}, Ldoz;->f()Landroid/content/Context;

    move-result-object v0

    .line 40
    new-instance v5, Lkfp;

    invoke-direct {v5}, Lkfp;-><init>()V

    .line 41
    const-string v1, "Get nearby activities"

    invoke-virtual {v5, v1}, Lkfp;->b(Ljava/lang/String;)V

    .line 42
    const-string v1, "Activities:SyncNearby"

    invoke-virtual {v5, v1}, Lkfp;->c(Ljava/lang/String;)V

    .line 43
    iget v1, p0, Ldoz;->a:I

    .line 45
    :try_start_0
    iget-object v2, p0, Ldoz;->b:Llae;

    iget-object v3, p0, Ldoz;->c:Ljava/lang/String;

    .line 46
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->d()I

    move-result v4

    .line 45
    invoke-static/range {v0 .. v5}, Llap;->a(Landroid/content/Context;ILlae;Ljava/lang/String;ILkfp;)V

    .line 47
    new-instance v0, Lhoz;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lhoz;-><init>(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    invoke-virtual {v5}, Lkfp;->f()V

    .line 52
    invoke-virtual {v5}, Lkfp;->g()V

    :goto_0
    return-object v0

    .line 48
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 49
    :try_start_1
    new-instance v0, Lhoz;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 51
    invoke-virtual {v5}, Lkfp;->f()V

    .line 52
    invoke-virtual {v5}, Lkfp;->g()V

    goto :goto_0

    .line 51
    :catchall_0
    move-exception v0

    invoke-virtual {v5}, Lkfp;->f()V

    .line 52
    invoke-virtual {v5}, Lkfp;->g()V

    throw v0
.end method
