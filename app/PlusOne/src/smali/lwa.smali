.class public final Llwa;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Llwc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 305
    sget-object v0, Llwc;->a:[Llwc;

    iput-object v0, p0, Llwa;->a:[Llwc;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 323
    .line 324
    iget-object v1, p0, Llwa;->a:[Llwc;

    if-eqz v1, :cond_1

    .line 325
    iget-object v2, p0, Llwa;->a:[Llwc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 326
    if-eqz v4, :cond_0

    .line 327
    const/4 v5, 0x1

    .line 328
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 325
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 332
    :cond_1
    iget-object v1, p0, Llwa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 333
    iput v0, p0, Llwa;->ai:I

    .line 334
    return v0
.end method

.method public a(Loxn;)Llwa;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 342
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 343
    sparse-switch v0, :sswitch_data_0

    .line 347
    iget-object v2, p0, Llwa;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 348
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llwa;->ah:Ljava/util/List;

    .line 351
    :cond_1
    iget-object v2, p0, Llwa;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 353
    :sswitch_0
    return-object p0

    .line 358
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 359
    iget-object v0, p0, Llwa;->a:[Llwc;

    if-nez v0, :cond_3

    move v0, v1

    .line 360
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llwc;

    .line 361
    iget-object v3, p0, Llwa;->a:[Llwc;

    if-eqz v3, :cond_2

    .line 362
    iget-object v3, p0, Llwa;->a:[Llwc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 364
    :cond_2
    iput-object v2, p0, Llwa;->a:[Llwc;

    .line 365
    :goto_2
    iget-object v2, p0, Llwa;->a:[Llwc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 366
    iget-object v2, p0, Llwa;->a:[Llwc;

    new-instance v3, Llwc;

    invoke-direct {v3}, Llwc;-><init>()V

    aput-object v3, v2, v0

    .line 367
    iget-object v2, p0, Llwa;->a:[Llwc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 368
    invoke-virtual {p1}, Loxn;->a()I

    .line 365
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 359
    :cond_3
    iget-object v0, p0, Llwa;->a:[Llwc;

    array-length v0, v0

    goto :goto_1

    .line 371
    :cond_4
    iget-object v2, p0, Llwa;->a:[Llwc;

    new-instance v3, Llwc;

    invoke-direct {v3}, Llwc;-><init>()V

    aput-object v3, v2, v0

    .line 372
    iget-object v2, p0, Llwa;->a:[Llwc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 343
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 310
    iget-object v0, p0, Llwa;->a:[Llwc;

    if-eqz v0, :cond_1

    .line 311
    iget-object v1, p0, Llwa;->a:[Llwc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 312
    if-eqz v3, :cond_0

    .line 313
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 311
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317
    :cond_1
    iget-object v0, p0, Llwa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 319
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llwa;->a(Loxn;)Llwa;

    move-result-object v0

    return-object v0
.end method
