.class public final Liup;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liuu;


# instance fields
.field private a:Linl;

.field private b:Liut;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ling;

.field private e:Landroid/graphics/Point;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lnhm;Ling;Landroid/graphics/Point;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Liup;->h:Landroid/content/Context;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Liup;->c:Ljava/util/List;

    .line 81
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iput-object p3, p0, Liup;->d:Ling;

    .line 83
    iput-object p4, p0, Liup;->e:Landroid/graphics/Point;

    .line 84
    const/4 v0, 0x1

    iput v0, p0, Liup;->g:I

    .line 85
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Liup;->b:Liut;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Liup;->b:Liut;

    invoke-virtual {v0}, Liut;->e()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 151
    iget-object v1, p0, Liup;->a:Linl;

    invoke-interface {v1, v0}, Linl;->a(Landroid/graphics/Bitmap;)V

    .line 153
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Linl;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Liup;->a:Linl;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 141
    iput p1, p0, Liup;->g:I

    .line 142
    iget-object v0, p0, Liup;->b:Liut;

    if-eqz v0, :cond_0

    iget-object v0, p0, Liup;->b:Liut;

    invoke-virtual {v0}, Liut;->d()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 143
    iget-object v0, p0, Liup;->b:Liut;

    invoke-virtual {v0, p1}, Liut;->a(I)V

    .line 144
    invoke-direct {p0}, Liup;->j()V

    .line 146
    :cond_0
    return-void
.end method

.method public a(Limu;)V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Liup;->d:Ling;

    invoke-interface {p1, v0}, Limu;->a(Ling;)Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Liup;->e:Landroid/graphics/Point;

    .line 258
    return-void
.end method

.method public a(Linl;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Liup;->a:Linl;

    .line 130
    return-void
.end method

.method public a(Liup;)V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p1, Liup;->a:Linl;

    invoke-virtual {p0, v0}, Liup;->a(Linl;)V

    .line 163
    iget-object v0, p1, Liup;->b:Liut;

    invoke-virtual {p0, v0}, Liup;->a(Liut;)V

    .line 164
    iget-object v0, p0, Liup;->d:Ling;

    iget-object v1, p1, Liup;->d:Ling;

    invoke-virtual {v0, v1}, Ling;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Liup;->a:Linl;

    iget-object v1, p0, Liup;->d:Ling;

    invoke-interface {v0, v1}, Linl;->a(Ling;)V

    .line 167
    :cond_0
    return-void
.end method

.method public a(Liup;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Liup;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Liup;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 197
    if-ne p0, p1, :cond_0

    .line 209
    :goto_0
    return-void

    .line 200
    :cond_0
    const-string v0, "MarkerCluster"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "merge "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " into "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    :cond_1
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    iget-object v1, p1, Liup;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 204
    iget-object v0, p1, Liup;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 205
    iget-object v0, v0, Lnhm;->b:Ljava/lang/String;

    invoke-interface {p2, v0, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 208
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Liup;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Liut;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Liup;->b:Liut;

    .line 134
    invoke-virtual {p1, p0}, Liut;->a(Liuu;)V

    .line 135
    return-void
.end method

.method public a(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<",
            "Lnhm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 223
    return-void
.end method

.method public a(Lnhm;)V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    return-void
.end method

.method public b()Liut;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Liup;->b:Liut;

    return-object v0
.end method

.method public b(Liut;)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Liup;->b:Liut;

    if-ne v0, p1, :cond_0

    .line 172
    invoke-direct {p0}, Liup;->j()V

    .line 174
    :cond_0
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Liup;->g:I

    return v0
.end method

.method public d()Ling;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Liup;->d:Ling;

    return-object v0
.end method

.method public e()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Liup;->e:Landroid/graphics/Point;

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 4

    .prologue
    .line 115
    iget-object v0, p0, Liup;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 116
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 117
    new-array v3, v2, [Ljava/lang/String;

    .line 118
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 119
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 120
    iget-object v0, v0, Lnhm;->b:Ljava/lang/String;

    aput-object v0, v3, v1

    .line 118
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 122
    :cond_0
    invoke-static {v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 123
    const-string v0, ","

    invoke-static {v0, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liup;->f:Ljava/lang/String;

    .line 125
    :cond_1
    iget-object v0, p0, Liup;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 180
    iget-object v0, p0, Liup;->a:Linl;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Liup;->a:Linl;

    invoke-interface {v0}, Linl;->a()V

    .line 182
    iput-object v1, p0, Liup;->a:Linl;

    .line 184
    :cond_0
    iget-object v0, p0, Liup;->b:Liut;

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Liup;->b:Liut;

    invoke-virtual {v0}, Liut;->a()V

    .line 186
    iput-object v1, p0, Liup;->b:Liut;

    .line 188
    :cond_1
    return-void
.end method

.method public i()V
    .locals 12

    .prologue
    const-wide v10, 0x4076800000000000L    # 360.0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 229
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 254
    :goto_0
    return-void

    .line 232
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 233
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 234
    iget-object v0, v0, Lnhm;->c:[Lnij;

    invoke-static {v0}, Liuo;->a([Lnij;)Lnij;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_1

    iget-object v3, v0, Lnij;->c:Ljava/lang/Double;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lnij;->d:Ljava/lang/Double;

    if-eqz v3, :cond_1

    .line 236
    new-instance v3, Ling;

    iget-object v4, v0, Lnij;->c:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v0, v0, Lnij;->d:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Ling;-><init>(DD)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 240
    :cond_2
    iget-object v0, p0, Liup;->h:Landroid/content/Context;

    const-class v2, Lini;

    .line 241
    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lini;

    invoke-interface {v0, v1}, Lini;->a(Ljava/util/List;)Linh;

    move-result-object v0

    .line 242
    invoke-interface {v0}, Linh;->a()Ling;

    move-result-object v1

    iget-wide v2, v1, Ling;->a:D

    invoke-interface {v0}, Linh;->b()Ling;

    move-result-object v1

    iget-wide v4, v1, Ling;->a:D

    add-double/2addr v2, v4

    div-double/2addr v2, v8

    .line 244
    invoke-interface {v0}, Linh;->a()Ling;

    move-result-object v1

    iget-wide v4, v1, Ling;->b:D

    invoke-interface {v0}, Linh;->b()Ling;

    move-result-object v1

    iget-wide v6, v1, Ling;->b:D

    cmpl-double v1, v4, v6

    if-lez v1, :cond_4

    .line 246
    invoke-interface {v0}, Linh;->a()Ling;

    move-result-object v1

    iget-wide v4, v1, Ling;->b:D

    invoke-interface {v0}, Linh;->b()Ling;

    move-result-object v0

    iget-wide v0, v0, Ling;->b:D

    add-double/2addr v0, v4

    add-double/2addr v0, v10

    div-double/2addr v0, v8

    .line 247
    const-wide v4, 0x4066800000000000L    # 180.0

    cmpl-double v4, v0, v4

    if-lez v4, :cond_3

    .line 248
    sub-double/2addr v0, v10

    .line 253
    :cond_3
    :goto_2
    new-instance v4, Ling;

    invoke-direct {v4, v2, v3, v0, v1}, Ling;-><init>(DD)V

    iput-object v4, p0, Liup;->d:Ling;

    goto/16 :goto_0

    .line 251
    :cond_4
    invoke-interface {v0}, Linh;->a()Ling;

    move-result-object v1

    iget-wide v4, v1, Ling;->b:D

    invoke-interface {v0}, Linh;->b()Ling;

    move-result-object v0

    iget-wide v0, v0, Ling;->b:D

    add-double/2addr v0, v4

    div-double/2addr v0, v8

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 262
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 263
    const-string v0, "[MarkerCluster: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    iget-object v0, p0, Liup;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 265
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 266
    if-lez v1, :cond_0

    .line 267
    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :cond_0
    iget-object v0, p0, Liup;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    iget-object v0, v0, Lnhm;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 271
    :cond_1
    const/16 v0, 0x5d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 272
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
