.class public final Llhx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Landroid/database/ContentObserver;

.field private static b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Llia;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Llhz;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Landroid/graphics/Typeface;

.field private static e:Landroid/graphics/Typeface;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 104
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Llhx;->b:Landroid/util/SparseArray;

    .line 107
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Llhx;->c:Landroid/util/SparseArray;

    .line 110
    const-string v0, "sans-serif-light"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Llhx;->d:Landroid/graphics/Typeface;

    .line 112
    const-string v0, "sans-serif-medium"

    .line 113
    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Llhx;->e:Landroid/graphics/Typeface;

    .line 112
    return-void
.end method

.method public static a(Landroid/content/Context;I)Landroid/text/TextPaint;
    .locals 4

    .prologue
    .line 499
    sget-object v0, Llhx;->a:Landroid/database/ContentObserver;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Llhy;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Llhy;-><init>(Landroid/os/Handler;Landroid/content/res/Resources;)V

    sput-object v1, Llhx;->a:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "font_scale"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Llhx;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 501
    :cond_0
    const/4 v1, 0x0

    .line 502
    sget-object v0, Llhx;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llhz;

    .line 504
    if-eqz v0, :cond_2

    .line 505
    iget-object v0, v0, Llhz;->a:Landroid/text/TextPaint;

    .line 508
    :goto_0
    if-nez v0, :cond_1

    .line 509
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 510
    invoke-static {p0, p1}, Llhx;->b(Landroid/content/Context;I)Llia;

    move-result-object v2

    .line 511
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    .line 512
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 513
    iget v3, v2, Llia;->b:I

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 514
    iget v3, v2, Llia;->a:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 515
    iget v1, v2, Llia;->d:I

    iput v1, v0, Landroid/text/TextPaint;->linkColor:I

    .line 517
    iget v1, v2, Llia;->c:I

    packed-switch v1, :pswitch_data_0

    .line 532
    :goto_1
    sget-object v1, Llhx;->c:Landroid/util/SparseArray;

    new-instance v3, Llhz;

    invoke-direct {v3, v0, v2}, Llhz;-><init>(Landroid/text/TextPaint;Llia;)V

    invoke-virtual {v1, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 535
    :cond_1
    return-object v0

    .line 519
    :pswitch_0
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_1

    .line 523
    :pswitch_1
    sget-object v1, Llhx;->d:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_1

    .line 527
    :pswitch_2
    sget-object v1, Llhx;->e:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 517
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a()Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Llhx;->c:Landroid/util/SparseArray;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 548
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 550
    invoke-static {p0, v0, p3}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 552
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/widget/TextView;I)V
    .locals 4

    .prologue
    .line 562
    invoke-static {p0, p2}, Llhx;->b(Landroid/content/Context;I)Llia;

    move-result-object v0

    .line 564
    if-nez p1, :cond_1

    .line 593
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    if-eqz v0, :cond_0

    .line 569
    const/4 v1, 0x0

    .line 570
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v0, Llia;->a:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 569
    invoke-virtual {p1, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 571
    iget v1, v0, Llia;->b:I

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 573
    iget v0, v0, Llia;->c:I

    packed-switch v0, :pswitch_data_0

    .line 588
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 575
    :pswitch_0
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 579
    :pswitch_1
    sget-object v0, Llhx;->d:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 583
    :pswitch_2
    sget-object v0, Llhx;->e:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 573
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Landroid/content/Context;I)Llia;
    .locals 10

    .prologue
    const v8, 0x7f0b013a

    const/4 v1, 0x1

    const v3, 0x7f0d024c

    const/4 v2, 0x2

    const v7, 0x7f0b0141

    .line 148
    sget-object v0, Llhx;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llia;

    .line 150
    if-nez v0, :cond_0

    .line 151
    new-instance v4, Llia;

    invoke-direct {v4}, Llia;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v0, 0x7f0b00d6

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    .line 153
    :goto_0
    sget-object v1, Llhx;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 156
    :cond_0
    return-object v0

    .line 151
    :pswitch_0
    const v2, 0x7f0d024b

    const v1, 0x7f0b013f

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_1
    iput v2, v4, Llia;->a:I

    iput v1, v4, Llia;->b:I

    iput v6, v4, Llia;->d:I

    iput v0, v4, Llia;->c:I

    move-object v0, v4

    goto :goto_0

    :pswitch_1
    const v2, 0x7f0d024e

    const v0, 0x7f0b00d6

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    :pswitch_2
    const v2, 0x7f0d0055

    const v0, 0x7f0b00d6

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    :pswitch_3
    const v1, 0x7f0b00d6

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v2, v3

    goto :goto_1

    :pswitch_4
    const v2, 0x7f0d024d

    const v1, 0x7f0b00d6

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1

    :pswitch_5
    const v0, 0x7f0b013d

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const v1, 0x7f0d024b

    move v9, v2

    move v2, v1

    move v1, v0

    move v0, v9

    goto :goto_1

    :pswitch_6
    const v0, 0x7f0b013d

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    move v0, v2

    move v2, v3

    goto :goto_1

    :pswitch_7
    const v0, 0x7f0b013d

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v2, v3

    move v9, v0

    move v0, v1

    move v1, v9

    goto :goto_1

    :pswitch_8
    const v2, 0x7f0d024b

    const v1, 0x7f0b013d

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1

    :pswitch_9
    const v1, 0x7f0b013d

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v2, v3

    goto :goto_1

    :pswitch_a
    const v2, 0x7f0d024d

    const v1, 0x7f0b013d

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1

    :pswitch_b
    const v2, 0x7f0d0055

    const v1, 0x7f0b013d

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_1

    :pswitch_c
    const v2, 0x7f0d0055

    const v0, 0x7f0b013d

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_d
    const v2, 0x7f0d024e

    const v0, 0x7f0b013d

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_e
    const v1, 0x7f0d024b

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v2

    move v2, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_f
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    move v0, v2

    move v2, v3

    goto/16 :goto_1

    :pswitch_10
    const v1, 0x7f0d024d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v2

    move v2, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_11
    const v1, 0x7f0d0055

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v2

    move v2, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_12
    const v2, 0x7f0d024b

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_13
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v2, v3

    move v9, v0

    move v0, v1

    move v1, v9

    goto/16 :goto_1

    :pswitch_14
    const v2, 0x7f0d024d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_15
    const v2, 0x7f0d0055

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_16
    const v2, 0x7f0d024e

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_17
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v0, 0x3

    move v2, v3

    goto/16 :goto_1

    :pswitch_18
    const v2, 0x7f0d024b

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_1

    :pswitch_19
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v2, v3

    goto/16 :goto_1

    :pswitch_1a
    const v2, 0x7f0d024d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_1

    :pswitch_1b
    const v2, 0x7f0d0055

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_1

    :pswitch_1c
    const v0, 0x7f0b0142

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    move v0, v2

    move v2, v3

    goto/16 :goto_1

    :pswitch_1d
    const v1, 0x7f0d024d

    const v0, 0x7f0b0142

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v2

    move v2, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_1e
    const v1, 0x7f0d024b

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v2

    move v2, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_1f
    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v1, v0

    move v0, v2

    move v2, v3

    goto/16 :goto_1

    :pswitch_20
    const v1, 0x7f0d024d

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v2

    move v2, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_21
    const v1, 0x7f0d0055

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v2

    move v2, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_22
    const v2, 0x7f0d0055

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_23
    const v2, 0x7f0d024e

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_24
    const v2, 0x7f0d024b

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_1

    :pswitch_25
    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v2, v3

    goto/16 :goto_1

    :pswitch_26
    const v2, 0x7f0d024d

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_1

    :pswitch_27
    const v2, 0x7f0d0055

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto/16 :goto_1

    :pswitch_28
    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const v2, 0x7f0d024e

    goto/16 :goto_1

    :pswitch_29
    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const v2, 0x7f0d024f

    goto/16 :goto_1

    :pswitch_2a
    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v0, 0x3

    move v2, v3

    goto/16 :goto_1

    :pswitch_2b
    const v2, 0x7f0d024d

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v0, 0x3

    goto/16 :goto_1

    :pswitch_2c
    const v0, 0x7f0b013f

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    const/4 v0, 0x3

    move v2, v3

    goto/16 :goto_1

    :pswitch_2d
    const v1, 0x7f0d024d

    const v0, 0x7f0b013f

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v9, v2

    move v2, v1

    move v1, v0

    move v0, v9

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_c
        :pswitch_d
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2c
        :pswitch_2a
        :pswitch_2b
        :pswitch_2d
    .end packed-switch
.end method
