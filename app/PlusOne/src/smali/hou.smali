.class final Lhou;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lhou;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/PowerManager$WakeLock;",
            ">;"
        }
    .end annotation
.end field

.field private d:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lhou;->c:Landroid/util/SparseArray;

    .line 26
    iput-object p1, p0, Lhou;->b:Landroid/content/Context;

    .line 27
    return-void
.end method

.method static declared-synchronized a(Landroid/content/Context;)Lhou;
    .locals 2

    .prologue
    .line 19
    const-class v1, Lhou;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhou;->a:Lhou;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lhou;

    invoke-direct {v0, p0}, Lhou;-><init>(Landroid/content/Context;)V

    sput-object v0, Lhou;->a:Lhou;

    .line 22
    :cond_0
    sget-object v0, Lhou;->a:Lhou;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 19
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method a()I
    .locals 4

    .prologue
    .line 30
    iget-object v1, p0, Lhou;->c:Landroid/util/SparseArray;

    monitor-enter v1

    .line 31
    :try_start_0
    iget v0, p0, Lhou;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhou;->d:I

    if-gtz v0, :cond_0

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lhou;->d:I

    .line 35
    :cond_0
    iget-object v0, p0, Lhou;->b:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 36
    const/4 v2, 0x1

    const-string v3, "BackgroundTaskService"

    .line 37
    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 38
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 39
    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 40
    iget-object v2, p0, Lhou;->c:Landroid/util/SparseArray;

    iget v3, p0, Lhou;->d:I

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 41
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    iget v0, p0, Lhou;->d:I

    return v0

    .line 41
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method a(I)V
    .locals 2

    .prologue
    .line 46
    iget-object v1, p0, Lhou;->c:Landroid/util/SparseArray;

    monitor-enter v1

    .line 47
    :try_start_0
    iget-object v0, p0, Lhou;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager$WakeLock;

    .line 48
    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 50
    iget-object v0, p0, Lhou;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 52
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
