.class public abstract Leah;
.super Lu;
.source "PG"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lenc;


# instance fields
.field private N:Ljava/lang/Integer;

.field private O:Ljava/lang/CharSequence;

.field private P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

.field private Q:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lu;-><init>()V

    .line 67
    new-instance v0, Leai;

    invoke-direct {v0, p0}, Leai;-><init>(Leah;)V

    iput-object v0, p0, Leah;->Q:Lfhh;

    return-void
.end method

.method private a(ILfib;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 245
    iget-object v0, p0, Leah;->N:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leah;->N:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    iput-object v4, p0, Leah;->N:Ljava/lang/Integer;

    .line 249
    invoke-virtual {p0}, Leah;->n()Lz;

    move-result-object v0

    .line 250
    if-eqz v0, :cond_0

    .line 251
    const v1, 0x48ba7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V

    .line 252
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 253
    invoke-virtual {p2}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v1

    .line 254
    const-string v2, "INVALID_ACL_EXPANSION"

    invoke-static {v1, v2}, Lkgf;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 255
    invoke-virtual {p0}, Leah;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_square_post"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 257
    const v1, 0x7f0a02c8

    .line 258
    invoke-virtual {p0, v1}, Leah;->e_(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_2

    const v0, 0x7f0a02c7

    .line 259
    :goto_1
    invoke-virtual {p0, v0}, Leah;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0a0596

    .line 261
    invoke-virtual {p0, v2}, Leah;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 257
    invoke-static {v1, v0, v2, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 263
    invoke-virtual {p0}, Leah;->u_()Lu;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Llgr;->a(Lu;I)V

    .line 264
    invoke-virtual {p0}, Leah;->p()Lae;

    move-result-object v1

    const-string v2, "StreamPostRestrictionsNotSupported"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 258
    :cond_2
    const v0, 0x7f0a02c6

    goto :goto_1

    .line 267
    :cond_3
    invoke-virtual {p0}, Leah;->c()I

    move-result v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 269
    :cond_4
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method static synthetic a(Leah;ILfib;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Leah;->a(ILfib;)V

    return-void
.end method


# virtual methods
.method public U()Z
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 60
    iget-object v1, p0, Leah;->O:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public V()V
    .locals 4

    .prologue
    .line 194
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 196
    invoke-virtual {p0}, Leah;->n()Lz;

    move-result-object v0

    .line 197
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 198
    const-string v2, "account_id"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 201
    invoke-virtual {p0}, Leah;->U()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 202
    const v1, 0xdc073

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 207
    :goto_0
    return-void

    .line 204
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 205
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method protected W()V
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Leah;->n()Lz;

    move-result-object v0

    check-cast v0, Lewu;

    .line 214
    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {v0}, Lewu;->l()V

    .line 217
    :cond_0
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    .line 102
    const v0, 0x7f040088

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 104
    const v0, 0x7f10013b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    .line 106
    invoke-virtual {p0}, Leah;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 107
    const-string v2, "account_id"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 109
    const-string v3, "activity_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 111
    iget-object v4, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, p0, v2, v3, v5}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Lu;ILjava/lang/String;Lkjm;)V

    .line 112
    iget-object v2, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Z)V

    .line 114
    if-nez p3, :cond_0

    .line 116
    invoke-virtual {p0}, Leah;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 115
    invoke-static {v0}, Llht;->a([B)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 117
    if-nez v0, :cond_1

    .line 118
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Ljava/lang/String;)V

    .line 122
    :goto_0
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v2, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setSelection(I)V

    .line 123
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leah;->O:Ljava/lang/CharSequence;

    .line 126
    :cond_0
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    new-instance v2, Leaj;

    invoke-direct {v2, p0}, Leaj;-><init>(Leah;)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 142
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 144
    return-object v1

    .line 120
    :cond_1
    iget-object v2, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Landroid/text/SpannableStringBuilder;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0, p1}, Lu;->a(Landroid/os/Bundle;)V

    .line 90
    if-eqz p1, :cond_0

    .line 91
    const-string v0, "original_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Leah;->O:Ljava/lang/CharSequence;

    .line 93
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leah;->N:Ljava/lang/Integer;

    .line 97
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0}, Lu;->aO_()V

    .line 150
    iget-object v0, p0, Leah;->Q:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 152
    iget-object v0, p0, Leah;->N:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leah;->N:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Leah;->N:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Leah;->N:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Leah;->a(ILfib;)V

    .line 155
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 224
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 225
    invoke-virtual {p0}, Leah;->n()Lz;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 227
    const-string v2, "account_id"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 230
    invoke-virtual {p0}, Leah;->U()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    const v1, 0x48ba7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 236
    :goto_0
    return-void

    .line 233
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 234
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public abstract c()I
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 64
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leah;->N:Ljava/lang/Integer;

    .line 65
    return-void
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public e()Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 165
    invoke-super {p0, p1}, Lu;->e(Landroid/os/Bundle;)V

    .line 167
    const-string v0, "original_text"

    iget-object v1, p0, Leah;->O:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Leah;->N:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 170
    const-string v0, "request_id"

    iget-object v1, p0, Leah;->N:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 172
    :cond_0
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Leah;->P:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    if-ne p1, v0, :cond_0

    .line 177
    packed-switch p2, :pswitch_data_0

    .line 186
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 179
    :pswitch_0
    invoke-static {p1}, Llsn;->b(Landroid/view/View;)V

    .line 180
    invoke-virtual {p0}, Leah;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    invoke-virtual {p0}, Leah;->b()V

    .line 183
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public z()V
    .locals 1

    .prologue
    .line 159
    invoke-super {p0}, Lu;->z()V

    .line 160
    iget-object v0, p0, Leah;->Q:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 161
    return-void
.end method
