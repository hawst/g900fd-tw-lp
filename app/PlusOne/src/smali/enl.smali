.class public Lenl;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Leqi;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Leqi;",
        "Llgs;"
    }
.end annotation


# static fields
.field public static N:I

.field public static O:F

.field public static P:I

.field public static Q:Landroid/graphics/drawable/Drawable;

.field public static R:I

.field public static S:I


# instance fields
.field public T:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public U:Z

.field public V:Z

.field public W:Lnjs;

.field public X:Locn;

.field public Y:Lhgw;

.field public Z:[B

.field public aa:[B

.field public ab:Z

.field public ac:Ljava/lang/String;

.field public ad:Ljava/lang/String;

.field public ae:Z

.field public af:I

.field public ag:I

.field public ah:Landroid/view/View;

.field ai:Landroid/widget/LinearLayout;

.field public aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

.field public ak:Lcom/google/android/apps/plus/views/ImageTextButton;

.field public al:Ljava/lang/Integer;

.field public am:Lhee;

.field private an:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ao:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private ap:Lhgw;

.field private aq:Lfhh;

.field private ar:[B

.field private as:Z

.field private aw:Ljava/lang/String;

.field private ax:Ljava/lang/String;

.field private ay:Landroid/widget/ScrollView;

.field private az:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Llol;-><init>()V

    .line 125
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lenl;->an:Ljava/util/HashSet;

    .line 127
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lenl;->ao:Ljava/util/HashSet;

    .line 128
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lenl;->T:Ljava/util/HashSet;

    .line 132
    invoke-virtual {p0}, Lenl;->W()Lfhh;

    move-result-object v0

    iput-object v0, p0, Lenl;->aq:Lfhh;

    .line 1195
    return-void
.end method

.method static synthetic a(Lenl;)Llnl;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lenl;->at:Llnl;

    return-object v0
.end method

.method private a(Lhgw;)V
    .locals 1

    .prologue
    .line 579
    invoke-virtual {p1}, Lhgw;->n()V

    .line 580
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Lhgw;)V

    .line 581
    iget-object v0, p0, Lenl;->Y:Lhgw;

    invoke-virtual {v0, p1}, Lhgw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {p0, v0}, Lenl;->c(Landroid/view/View;)V

    .line 586
    :goto_0
    return-void

    .line 584
    :cond_0
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {p0, v0}, Lenl;->d(Landroid/view/View;)V

    goto :goto_0
.end method

.method private al()Z
    .locals 1

    .prologue
    .line 1028
    iget-object v0, p0, Lenl;->ao:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private am()Z
    .locals 1

    .prologue
    .line 1035
    iget-object v0, p0, Lenl;->T:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected U()V
    .locals 2

    .prologue
    .line 473
    invoke-virtual {p0}, Lenl;->ai()V

    .line 475
    iget-boolean v0, p0, Lenl;->U:Z

    if-nez v0, :cond_0

    .line 476
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    new-instance v1, Leno;

    invoke-direct {v1, p0}, Leno;-><init>(Lenl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Ljava/lang/Runnable;)V

    .line 487
    :cond_0
    return-void
.end method

.method protected V()V
    .locals 0

    .prologue
    .line 942
    invoke-virtual {p0}, Lenl;->ae()V

    .line 943
    return-void
.end method

.method protected W()Lfhh;
    .locals 1

    .prologue
    .line 135
    new-instance v0, Lenm;

    invoke-direct {v0, p0}, Lenm;-><init>(Lenl;)V

    return-object v0
.end method

.method protected X()V
    .locals 0

    .prologue
    .line 162
    return-void
.end method

.method protected Y()[B
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x0

    return-object v0
.end method

.method protected Z()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 540
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h()Lhgw;

    move-result-object v3

    .line 541
    invoke-virtual {v3}, Lhgw;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    const/4 v3, 0x0

    .line 544
    :cond_0
    iget-object v0, p0, Lenl;->am:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 545
    invoke-virtual {p0}, Lenl;->n()Lz;

    move-result-object v0

    const v2, 0x7f0a0377

    .line 546
    invoke-virtual {p0, v2}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x5

    const/4 v5, 0x0

    move v7, v6

    move v8, v6

    .line 545
    invoke-static/range {v0 .. v8}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Lhgw;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lenl;->x()Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Llsn;->b(Landroid/view/View;)V

    invoke-virtual {p0, v0, v6}, Lenl;->a(Landroid/content/Intent;I)V

    .line 554
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 401
    const v0, 0x7f0401be

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 403
    const v0, 0x7f100319

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lenl;->az:Landroid/view/View;

    .line 404
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lenl;->n()Lz;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lenl;->ah:Landroid/view/View;

    .line 406
    const v0, 0x7f1001da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageTextButton;

    .line 407
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    const v0, 0x7f1001fa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageTextButton;

    iput-object v0, p0, Lenl;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    .line 409
    iget-object v0, p0, Lenl;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 411
    const v0, 0x7f1001fb

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lenl;->ay:Landroid/widget/ScrollView;

    .line 412
    const v0, 0x7f1001e9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lenl;->ai:Landroid/widget/LinearLayout;

    .line 414
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 415
    iget-object v0, p0, Lenl;->ai:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    new-instance v2, Lenn;

    invoke-direct {v2, p0}, Lenn;-><init>(Lenl;)V

    invoke-virtual {v0, v2}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 432
    :cond_0
    iget-boolean v0, p0, Lenl;->U:Z

    if-nez v0, :cond_4

    .line 433
    const v0, 0x7f100541

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iput-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    .line 434
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iget-object v2, p0, Lenl;->am:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->c(I)V

    .line 435
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 436
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    const v2, 0x7f0a057b

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(I)V

    .line 437
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->b(I)V

    .line 438
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    iget-boolean v2, p0, Lenl;->as:Z

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->setEnabled(Z)V

    .line 439
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->c()V

    .line 446
    :goto_0
    if-nez p3, :cond_1

    .line 447
    iget-object v0, p0, Lenl;->az:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 450
    :cond_1
    iget-boolean v0, p0, Lenl;->ab:Z

    if-eqz v0, :cond_2

    .line 451
    invoke-virtual {p0}, Lenl;->e()V

    .line 452
    invoke-virtual {p0}, Lenl;->U()V

    .line 456
    :cond_2
    iget-object v0, p0, Lenl;->aw:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 457
    const v0, 0x7f10053d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 458
    iget-object v2, p0, Lenl;->aw:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 459
    const v0, 0x7f10053e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 460
    iget-object v2, p0, Lenl;->ax:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 461
    const v0, 0x7f10053c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 462
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 465
    :cond_3
    invoke-virtual {p0, v1}, Lenl;->i(Landroid/view/View;)V

    .line 466
    return-object v1

    .line 441
    :cond_4
    const v0, 0x7f10053f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 442
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 344
    packed-switch p1, :pswitch_data_0

    .line 352
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 346
    :pswitch_0
    new-instance v0, Lhxg;

    iget-object v1, p0, Lenl;->at:Llnl;

    iget-object v2, p0, Lenl;->am:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "circle_id"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const-string v5, "circle_name"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "type"

    aput-object v5, v3, v4

    invoke-direct {v0, v1, v2, v6, v3}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;)V

    goto :goto_0

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Lnja;)Lhgw;
    .locals 13

    .prologue
    const/16 v12, 0x9

    const/16 v11, 0x8

    const/4 v10, 0x5

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 644
    if-nez p1, :cond_2

    .line 646
    iget-boolean v0, p0, Lenl;->ae:Z

    if-eqz v0, :cond_0

    .line 647
    new-instance v0, Lhxc;

    const-string v2, "0"

    const v3, 0x7f0a04a3

    .line 648
    invoke-virtual {p0, v3}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v12, v3, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 659
    :goto_0
    new-instance v1, Lhgw;

    invoke-direct {v1, v0}, Lhgw;-><init>(Lhxc;)V

    move-object v0, v1

    .line 768
    :goto_1
    return-object v0

    .line 650
    :cond_0
    iget-object v0, p0, Lenl;->ac:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 651
    new-instance v0, Lhxc;

    const-string v2, "v.domain"

    iget-object v3, p0, Lenl;->ac:Ljava/lang/String;

    invoke-direct {v0, v2, v11, v3, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 655
    :cond_1
    new-instance v0, Lhxc;

    const-string v2, "1c"

    const v3, 0x7f0a04a5

    .line 657
    invoke-virtual {p0, v3}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v10, v3, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_0

    .line 663
    :cond_2
    iget-object v0, p1, Lnja;->a:Lock;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lnja;->a:Lock;

    iget-object v0, v0, Lock;->a:[Locn;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lnja;->a:Lock;

    iget-object v0, v0, Lock;->a:[Locn;

    array-length v0, v0

    if-eqz v0, :cond_6

    .line 666
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    move v2, v1

    .line 667
    :goto_2
    iget-object v5, p1, Lnja;->a:Lock;

    iget-object v5, v5, Lock;->a:[Locn;

    array-length v5, v5

    if-ge v0, v5, :cond_5

    .line 668
    iget-object v5, p1, Lnja;->a:Lock;

    iget-object v5, v5, Lock;->a:[Locn;

    aget-object v5, v5, v0

    iget-object v5, v5, Locn;->c:Ljava/lang/String;

    .line 669
    if-eqz v5, :cond_3

    .line 670
    invoke-static {v5}, Lhxe;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 673
    iget-object v6, p0, Lenl;->an:Ljava/util/HashSet;

    invoke-virtual {v6, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 674
    iget-object v5, p1, Lnja;->a:Lock;

    iget-object v5, v5, Lock;->a:[Locn;

    aget-object v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 667
    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v2, v3

    .line 676
    goto :goto_3

    .line 679
    :cond_5
    if-eqz v2, :cond_6

    .line 680
    iget-object v2, p1, Lnja;->a:Lock;

    .line 681
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Locn;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Locn;

    iput-object v0, v2, Lock;->a:[Locn;

    .line 686
    :cond_6
    iget-object v0, p1, Lnja;->a:Lock;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lnja;->a:Lock;

    iget-object v0, v0, Lock;->a:[Locn;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lnja;->a:Lock;

    iget-object v0, v0, Lock;->a:[Locn;

    array-length v0, v0

    if-nez v0, :cond_e

    .line 688
    :cond_7
    iget v0, p1, Lnja;->b:I

    .line 689
    if-ne v0, v3, :cond_8

    .line 690
    new-instance v0, Lhgw;

    new-instance v2, Lhxc;

    const-string v3, "0"

    const v4, 0x7f0a04a3

    .line 692
    invoke-virtual {p0, v4}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v12, v4, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v0, v2}, Lhgw;-><init>(Lhxc;)V

    goto/16 :goto_1

    .line 694
    :cond_8
    const/4 v2, 0x2

    if-ne v0, v2, :cond_9

    .line 695
    new-instance v0, Lhgw;

    new-instance v2, Lhxc;

    iget-object v3, p0, Lenl;->ad:Ljava/lang/String;

    iget-object v4, p0, Lenl;->ac:Ljava/lang/String;

    invoke-direct {v2, v3, v11, v4, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v0, v2}, Lhgw;-><init>(Lhxc;)V

    goto/16 :goto_1

    .line 698
    :cond_9
    const/4 v2, 0x3

    if-ne v0, v2, :cond_a

    .line 699
    new-instance v0, Lhgw;

    new-instance v2, Lhxc;

    const-string v3, "1f"

    const/4 v4, 0x7

    const v5, 0x7f0a04a4

    .line 701
    invoke-virtual {p0, v5}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v0, v2}, Lhgw;-><init>(Lhxc;)V

    goto/16 :goto_1

    .line 703
    :cond_a
    const/4 v2, 0x4

    if-ne v0, v2, :cond_b

    .line 704
    new-instance v0, Lhgw;

    new-instance v2, Lhxc;

    const-string v3, "1c"

    const v4, 0x7f0a04a5

    .line 706
    invoke-virtual {p0, v4}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v10, v4, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v0, v2}, Lhgw;-><init>(Lhxc;)V

    goto/16 :goto_1

    .line 708
    :cond_b
    const/4 v2, 0x6

    if-eq v0, v2, :cond_c

    if-ne v0, v10, :cond_d

    .line 710
    :cond_c
    new-instance v0, Lhgw;

    new-instance v2, Lhxc;

    const-string v3, "v.private"

    const/16 v4, 0x65

    const v5, 0x7f0a04a1

    .line 712
    invoke-virtual {p0, v5}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v0, v2}, Lhgw;-><init>(Lhxc;)V

    goto/16 :goto_1

    .line 715
    :cond_d
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lenl;->a(Lnja;)Lhgw;

    move-result-object v0

    goto/16 :goto_1

    .line 720
    :cond_e
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 721
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 723
    iget-object v0, p1, Lnja;->a:Lock;

    iget-object v5, v0, Lock;->a:[Locn;

    move v0, v1

    .line 724
    :goto_4
    array-length v6, v5

    if-ge v0, v6, :cond_15

    .line 725
    aget-object v6, v5, v0

    .line 728
    iget v7, v6, Locn;->d:I

    const/high16 v8, -0x80000000

    if-eq v7, v8, :cond_13

    .line 729
    iget v7, v6, Locn;->d:I

    if-ne v7, v3, :cond_10

    .line 730
    new-instance v6, Lhxc;

    const-string v7, "0"

    const v8, 0x7f0a04a3

    .line 732
    invoke-virtual {p0, v8}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v12, v8, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 730
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 724
    :cond_f
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 734
    :cond_10
    iget v7, v6, Locn;->d:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_11

    .line 735
    new-instance v6, Lhxc;

    iget-object v7, p0, Lenl;->ad:Ljava/lang/String;

    iget-object v8, p0, Lenl;->ac:Ljava/lang/String;

    invoke-direct {v6, v7, v11, v8, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 738
    :cond_11
    iget v7, v6, Locn;->d:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_12

    .line 739
    new-instance v6, Lhxc;

    const-string v7, "1f"

    const/4 v8, 0x7

    const v9, 0x7f0a04a4

    .line 741
    invoke-virtual {p0, v9}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v8, v9, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 739
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 743
    :cond_12
    iget v6, v6, Locn;->d:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_f

    .line 744
    new-instance v6, Lhxc;

    const-string v7, "1c"

    const v8, 0x7f0a04a5

    .line 746
    invoke-virtual {p0, v8}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v10, v8, v1}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 744
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 749
    :cond_13
    iget-object v7, v6, Locn;->c:Ljava/lang/String;

    if-eqz v7, :cond_14

    .line 750
    iget-object v7, v6, Locn;->c:Ljava/lang/String;

    invoke-virtual {p0, v7}, Lenl;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 751
    new-instance v8, Lhxc;

    iget-object v6, v6, Locn;->c:Ljava/lang/String;

    invoke-static {v6}, Lhxe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v8, v6, v3, v7, v3}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 754
    :cond_14
    iget-object v7, v6, Locn;->b:Lohp;

    if-eqz v7, :cond_f

    .line 755
    iget-object v7, v6, Locn;->b:Lohp;

    iget-object v7, v7, Lohp;->d:Ljava/lang/String;

    if-eqz v7, :cond_f

    .line 756
    iget-object v7, v6, Locn;->b:Lohp;

    iget-object v7, v7, Lohp;->d:Ljava/lang/String;

    invoke-virtual {p0, v7}, Lenl;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 759
    new-instance v8, Ljqs;

    iget-object v6, v6, Locn;->b:Lohp;

    iget-object v6, v6, Lohp;->d:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct {v8, v6, v7, v9}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 764
    :cond_15
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 765
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lenl;->a(Lnja;)Lhgw;

    move-result-object v0

    goto/16 :goto_1

    .line 768
    :cond_16
    new-instance v0, Lhgw;

    invoke-direct {v0, v2, v4}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto/16 :goto_1
.end method

.method a(ILjava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 597
    iget-object v0, p0, Lenl;->W:Lnjs;

    if-nez v0, :cond_0

    .line 598
    iget-object v0, p0, Lenl;->ar:[B

    if-eqz v0, :cond_0

    .line 600
    :try_start_0
    new-instance v0, Lnjs;

    invoke-direct {v0}, Lnjs;-><init>()V

    iget-object v1, p0, Lenl;->ar:[B

    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnjs;

    iput-object v0, p0, Lenl;->W:Lnjs;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 608
    :cond_0
    :goto_0
    iget-object v0, p0, Lenl;->W:Lnjs;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lenl;->W:Lnjs;

    iget-object v0, v0, Lnjs;->a:[Locm;

    if-eqz v0, :cond_2

    .line 609
    iget-object v0, p0, Lenl;->W:Lnjs;

    iget-object v0, v0, Lnjs;->a:[Locm;

    array-length v1, v0

    .line 610
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    .line 611
    iget-object v2, p0, Lenl;->W:Lnjs;

    iget-object v2, v2, Lnjs;->a:[Locm;

    aget-object v2, v2, v0

    .line 612
    iget-object v3, v2, Locm;->b:Locn;

    if-eqz v3, :cond_1

    .line 613
    packed-switch p1, :pswitch_data_0

    .line 610
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 617
    :pswitch_0
    iget-object v3, v2, Locm;->b:Locn;

    iget-object v3, v3, Locn;->c:Ljava/lang/String;

    invoke-static {v3, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 618
    iget-object v0, v2, Locm;->c:Ljava/lang/String;

    .line 639
    :goto_2
    return-object v0

    .line 623
    :pswitch_1
    iget-object v3, v2, Locm;->b:Locn;

    iget-object v3, v3, Locn;->b:Lohp;

    if-eqz v3, :cond_1

    iget-object v3, v2, Locm;->b:Locn;

    iget-object v3, v3, Locn;->b:Lohp;

    iget-object v3, v3, Lohp;->d:Ljava/lang/String;

    .line 624
    invoke-static {v3, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 625
    iget-object v0, v2, Locm;->c:Ljava/lang/String;

    goto :goto_2

    .line 630
    :pswitch_2
    iget-object v3, v2, Locm;->b:Locn;

    iget v3, v3, Locn;->d:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 631
    iget-object v0, v2, Locm;->c:Ljava/lang/String;

    goto :goto_2

    .line 639
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_0

    .line 613
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 589
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lenl;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 558
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 576
    :cond_0
    :goto_0
    return-void

    .line 561
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 563
    :pswitch_0
    if-eqz p3, :cond_0

    .line 564
    const-string v0, "extra_acl"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 566
    iget-boolean v1, p0, Lenl;->as:Z

    if-eqz v1, :cond_2

    .line 567
    invoke-direct {p0, v0}, Lenl;->a(Lhgw;)V

    goto :goto_0

    .line 570
    :cond_2
    iput-object v0, p0, Lenl;->ap:Lhgw;

    goto :goto_0

    .line 561
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 928
    return-void
.end method

.method protected a(ILfib;)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lenl;->al:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lenl;->al:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    invoke-virtual {p0}, Lenl;->ab()V

    .line 150
    invoke-virtual {p0, p2}, Lenl;->a(Lfib;)Z

    move-result v0

    .line 152
    const/4 v1, 0x0

    iput-object v1, p0, Lenl;->al:Ljava/lang/Integer;

    .line 154
    if-nez v0, :cond_0

    .line 155
    invoke-virtual {p0}, Lenl;->X()V

    .line 156
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lenl;->d(I)V

    goto :goto_0
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 932
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 247
    invoke-super {p0, p1}, Llol;->a(Landroid/app/Activity;)V

    .line 248
    sget v0, Lenl;->N:I

    if-nez v0, :cond_0

    .line 249
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 250
    const v1, 0x7f0d0135

    .line 251
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lenl;->N:I

    .line 252
    const v1, 0x7f0d024d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lenl;->O:F

    .line 253
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lenl;->P:I

    .line 254
    const v1, 0x7f02008b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lenl;->Q:Landroid/graphics/drawable/Drawable;

    .line 255
    const v1, 0x7f0d0127

    .line 256
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lenl;->R:I

    .line 257
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lenl;->S:I

    .line 259
    :cond_0
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 396
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 263
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 265
    invoke-virtual {p0}, Lenl;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 266
    const-string v1, "help_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lenl;->aw:Ljava/lang/String;

    .line 267
    const-string v1, "help_desc"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lenl;->ax:Ljava/lang/String;

    .line 268
    const-string v1, "profile_edit_roster_proto"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lenl;->ar:[B

    .line 269
    iput-boolean v2, p0, Lenl;->as:Z

    .line 271
    invoke-virtual {p0, p1}, Lenl;->k(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 272
    iget-object v0, p0, Lenl;->Z:[B

    if-nez v0, :cond_0

    .line 273
    iget-object v0, p0, Lenl;->aa:[B

    iput-object v0, p0, Lenl;->Z:[B

    .line 276
    :cond_0
    invoke-virtual {p0}, Lenl;->c()V

    .line 277
    invoke-virtual {p0}, Lenl;->d()V

    .line 279
    iput-boolean v3, p0, Lenl;->ab:Z

    .line 280
    iget-boolean v0, p0, Lenl;->V:Z

    if-nez v0, :cond_1

    .line 281
    invoke-virtual {p0}, Lenl;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 289
    :cond_1
    :goto_0
    return-void

    .line 285
    :cond_2
    iput-boolean v2, p0, Lenl;->ab:Z

    .line 286
    new-instance v0, Lens;

    invoke-direct {v0, p0}, Lens;-><init>(Lenl;)V

    .line 287
    invoke-virtual {p0}, Lenl;->w()Lbb;

    move-result-object v1

    invoke-virtual {v1, v3, v4, v0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 913
    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 914
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lenl;->d(I)V

    .line 916
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1345
    iget-object v0, p0, Lenl;->ay:Landroid/widget/ScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1347
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1348
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1349
    const v0, 0x7f10025f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1350
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1351
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1352
    const v0, 0x7f100260

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1353
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 359
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 363
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 393
    :goto_0
    return-void

    .line 365
    :pswitch_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    :cond_0
    iget-object v0, p0, Lenl;->an:Ljava/util/HashSet;

    .line 368
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 367
    invoke-static {v1}, Lhxe;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 369
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 370
    packed-switch v0, :pswitch_data_1

    .line 379
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    :cond_1
    invoke-virtual {p0, p2}, Lenl;->a(Landroid/database/Cursor;)V

    .line 384
    iget-object v0, p0, Lenl;->ap:Lhgw;

    if-eqz v0, :cond_2

    .line 385
    iget-object v0, p0, Lenl;->ap:Lhgw;

    invoke-direct {p0, v0}, Lenl;->a(Lhgw;)V

    .line 386
    const/4 v0, 0x0

    iput-object v0, p0, Lenl;->ap:Lhgw;

    .line 389
    :cond_2
    iput-boolean v2, p0, Lenl;->as:Z

    goto :goto_0

    .line 372
    :pswitch_1
    iput-boolean v2, p0, Lenl;->ae:Z

    goto :goto_1

    .line 375
    :pswitch_2
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lenl;->ac:Ljava/lang/String;

    .line 376
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lenl;->ad:Ljava/lang/String;

    goto :goto_1

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    .line 370
    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 88
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lenl;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method protected a(Ldsx;)V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1304
    if-lez p2, :cond_1

    .line 1305
    new-instance v0, Lhgw;

    new-instance v1, Lhxc;

    const/4 v2, 0x1

    invoke-direct {v1, p1, p2, p3, v2}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v0, v1}, Lhgw;-><init>(Lhxc;)V

    .line 1307
    iget-object v1, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->a(Lhgw;)V

    .line 1308
    iget-object v1, p0, Lenl;->Y:Lhgw;

    invoke-virtual {v1, v0}, Lhgw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1309
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {p0, v0}, Lenl;->c(Landroid/view/View;)V

    .line 1316
    :goto_0
    return-void

    .line 1311
    :cond_0
    iget-object v0, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {p0, v0}, Lenl;->d(Landroid/view/View;)V

    goto :goto_0

    .line 1314
    :cond_1
    invoke-virtual {p0}, Lenl;->Z()V

    goto :goto_0
.end method

.method protected a(Lnjt;)V
    .locals 2

    .prologue
    .line 946
    iget-object v0, p0, Lenl;->at:Llnl;

    iget-object v1, p0, Lenl;->am:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILnjt;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lenl;->al:Ljava/lang/Integer;

    .line 948
    const v0, 0x7f0a0378

    invoke-virtual {p0, v0}, Lenl;->c(I)V

    .line 949
    return-void
.end method

.method protected a(Lfib;)Z
    .locals 5

    .prologue
    const v3, 0x7f0a0592

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 857
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lfib;->e()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 859
    :cond_0
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 860
    invoke-virtual {p1}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 861
    if-eqz v2, :cond_2

    .line 862
    const v3, 0x7f0a0379

    new-array v4, v1, [Ljava/lang/Object;

    aput-object v2, v4, v0

    invoke-virtual {p0, v3, v4}, Lenl;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 869
    :goto_0
    invoke-virtual {p0, v0}, Lenl;->c(Ljava/lang/String;)V

    move v0, v1

    .line 873
    :cond_1
    return v0

    .line 864
    :cond_2
    invoke-virtual {p0, v3}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 867
    :cond_3
    invoke-virtual {p0, v3}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a()[B
    .locals 1

    .prologue
    .line 504
    const/4 v0, 0x0

    return-object v0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 308
    invoke-super {p0}, Llol;->aO_()V

    .line 310
    iget-object v0, p0, Lenl;->aq:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 312
    iget-object v0, p0, Lenl;->al:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lenl;->al:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    iget-object v0, p0, Lenl;->al:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 315
    const/4 v1, 0x0

    iput-object v1, p0, Lenl;->al:Ljava/lang/Integer;

    .line 316
    invoke-virtual {p0}, Lenl;->ab()V

    .line 317
    invoke-virtual {p0, v0}, Lenl;->a(Lfib;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lenl;->d(I)V

    .line 322
    :cond_0
    return-void
.end method

.method protected aa()Lnja;
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v0, 0x0

    const/4 v10, 0x1

    .line 772
    iget-object v1, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;->h()Lhgw;

    move-result-object v3

    .line 773
    new-instance v1, Lnja;

    invoke-direct {v1}, Lnja;-><init>()V

    .line 776
    invoke-virtual {v3}, Lhgw;->h()I

    move-result v4

    .line 777
    invoke-virtual {v3}, Lhgw;->g()I

    move-result v5

    .line 778
    if-ne v4, v10, :cond_5

    if-nez v5, :cond_5

    .line 779
    invoke-virtual {v3, v0}, Lhgw;->b(I)Lhxc;

    move-result-object v2

    invoke-virtual {v2}, Lhxc;->c()I

    move-result v2

    .line 780
    if-eq v2, v10, :cond_5

    .line 781
    const/16 v0, 0x9

    if-ne v2, v0, :cond_1

    .line 782
    iput v10, v1, Lnja;->b:I

    :cond_0
    :goto_0
    move-object v0, v1

    .line 838
    :goto_1
    return-object v0

    .line 783
    :cond_1
    const/16 v0, 0x8

    if-ne v2, v0, :cond_2

    .line 784
    iput v11, v1, Lnja;->b:I

    goto :goto_0

    .line 785
    :cond_2
    const/4 v0, 0x7

    if-ne v2, v0, :cond_3

    .line 786
    iput v12, v1, Lnja;->b:I

    goto :goto_0

    .line 787
    :cond_3
    const/4 v0, 0x5

    if-ne v2, v0, :cond_4

    .line 788
    iput v13, v1, Lnja;->b:I

    goto :goto_0

    .line 789
    :cond_4
    const/16 v0, 0x65

    if-ne v2, v0, :cond_0

    .line 790
    const/4 v0, 0x6

    iput v0, v1, Lnja;->b:I

    goto :goto_0

    .line 797
    :cond_5
    const/4 v2, 0x5

    iput v2, v1, Lnja;->b:I

    .line 798
    new-instance v2, Lock;

    invoke-direct {v2}, Lock;-><init>()V

    iput-object v2, v1, Lnja;->a:Lock;

    .line 799
    iget-object v2, v1, Lnja;->a:Lock;

    iget-object v6, p0, Lenl;->X:Locn;

    iput-object v6, v2, Lock;->b:Locn;

    .line 800
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v2, v0

    .line 802
    :goto_2
    if-ge v2, v4, :cond_6

    .line 803
    invoke-virtual {v3, v2}, Lhgw;->b(I)Lhxc;

    move-result-object v7

    .line 804
    new-instance v8, Locn;

    invoke-direct {v8}, Locn;-><init>()V

    .line 805
    invoke-virtual {v7}, Lhxc;->c()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    .line 822
    :goto_3
    :pswitch_0
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 802
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 807
    :pswitch_1
    iput v10, v8, Locn;->d:I

    goto :goto_3

    .line 810
    :pswitch_2
    iput v11, v8, Locn;->d:I

    goto :goto_3

    .line 813
    :pswitch_3
    iput v13, v8, Locn;->d:I

    goto :goto_3

    .line 816
    :pswitch_4
    iput v12, v8, Locn;->d:I

    goto :goto_3

    .line 819
    :pswitch_5
    invoke-virtual {v7}, Lhxc;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lhxe;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v8, Locn;->c:Ljava/lang/String;

    goto :goto_3

    .line 825
    :cond_6
    :goto_4
    if-ge v0, v5, :cond_8

    .line 826
    invoke-virtual {v3, v0}, Lhgw;->a(I)Ljqs;

    move-result-object v2

    .line 827
    invoke-virtual {v2}, Ljqs;->a()Ljava/lang/String;

    move-result-object v2

    .line 828
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 829
    new-instance v4, Locn;

    invoke-direct {v4}, Locn;-><init>()V

    .line 830
    new-instance v7, Lohp;

    invoke-direct {v7}, Lohp;-><init>()V

    iput-object v7, v4, Locn;->b:Lohp;

    .line 831
    iget-object v7, v4, Locn;->b:Lohp;

    iput-object v2, v7, Lohp;->d:Ljava/lang/String;

    .line 832
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 825
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 836
    :cond_8
    iget-object v2, v1, Lnja;->a:Lock;

    .line 837
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Locn;

    .line 836
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Locn;

    iput-object v0, v2, Lock;->a:[Locn;

    move-object v0, v1

    .line 838
    goto/16 :goto_1

    .line 805
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected ab()V
    .locals 2

    .prologue
    .line 849
    invoke-virtual {p0}, Lenl;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 851
    if-eqz v0, :cond_0

    .line 852
    invoke-virtual {v0}, Lt;->a()V

    .line 854
    :cond_0
    return-void
.end method

.method protected ac()V
    .locals 4

    .prologue
    .line 881
    iget-object v0, p0, Lenl;->az:Landroid/view/View;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 883
    const v0, 0x7f0a024d

    .line 884
    invoke-virtual {p0, v0}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a037a

    .line 885
    invoke-virtual {p0, v1}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a07fa

    .line 886
    invoke-virtual {p0, v2}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a07fd

    invoke-virtual {p0, v3}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 883
    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 887
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    .line 888
    invoke-virtual {p0}, Lenl;->p()Lae;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 889
    return-void
.end method

.method protected ad()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 892
    const/4 v0, 0x0

    .line 893
    invoke-virtual {p0}, Lenl;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 894
    const-string v2, "profile_edit_return_json"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 895
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 896
    const-string v1, "profile_edit_items_proto"

    invoke-virtual {p0}, Lenl;->a()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 902
    :cond_0
    :goto_0
    return-object v0

    .line 897
    :cond_1
    const-string v2, "profile_data_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 898
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 899
    const-string v2, "profile_data_id"

    const-string v3, "profile_data_id"

    const/4 v4, 0x0

    .line 900
    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 899
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected ae()V
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lenl;->az:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 936
    iget-object v0, p0, Lenl;->az:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 938
    :cond_0
    invoke-virtual {p0}, Lenl;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 939
    return-void
.end method

.method public af()V
    .locals 0

    .prologue
    .line 955
    invoke-virtual {p0}, Lenl;->ag()V

    .line 956
    return-void
.end method

.method protected ag()V
    .locals 1

    .prologue
    .line 962
    iget-object v0, p0, Lenl;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageTextButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 963
    invoke-virtual {p0}, Lenl;->ac()V

    .line 967
    :goto_0
    return-void

    .line 965
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lenl;->d(I)V

    goto :goto_0
.end method

.method protected ah()I
    .locals 2

    .prologue
    .line 970
    iget-object v0, p0, Lenl;->ai:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    iget v1, p0, Lenl;->ag:I

    sub-int/2addr v0, v1

    return v0
.end method

.method protected ai()V
    .locals 2

    .prologue
    .line 974
    iget-object v0, p0, Lenl;->ao:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 975
    iget-object v0, p0, Lenl;->T:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 976
    iget-object v0, p0, Lenl;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    .line 977
    return-void
.end method

.method public aj()V
    .locals 0

    .prologue
    .line 1325
    invoke-virtual {p0}, Lenl;->Z()V

    .line 1326
    return-void
.end method

.method protected ak()Z
    .locals 1

    .prologue
    .line 1329
    iget-boolean v0, p0, Lenl;->ab:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 593
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lenl;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 920
    return-void
.end method

.method public b(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1320
    invoke-virtual {p0, p1, p2, p3}, Lenl;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 1321
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 493
    return-void
.end method

.method protected c(I)V
    .locals 3

    .prologue
    .line 842
    const/4 v0, 0x0

    .line 843
    invoke-virtual {p0, p1}, Lenl;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 842
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 845
    invoke-virtual {p0}, Lenl;->p()Lae;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 846
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 241
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 242
    iget-object v0, p0, Lenl;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lenl;->am:Lhee;

    .line 243
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 924
    return-void
.end method

.method protected c(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 980
    iget-object v0, p0, Lenl;->ao:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 982
    iget v0, p0, Lenl;->af:I

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lenl;->ah()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 983
    :goto_0
    iget-object v3, p0, Lenl;->ao:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    if-ne v3, v1, :cond_3

    iget-object v3, p0, Lenl;->ao:Ljava/util/HashSet;

    iget-object v4, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    .line 984
    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 985
    :goto_1
    invoke-direct {p0}, Lenl;->al()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 986
    :cond_0
    iget-object v0, p0, Lenl;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    .line 988
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 982
    goto :goto_0

    :cond_3
    move v1, v2

    .line 984
    goto :goto_1
.end method

.method protected c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 877
    invoke-virtual {p0}, Lenl;->n()Lz;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Llih;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 878
    return-void
.end method

.method protected d()V
    .locals 1

    .prologue
    .line 496
    const/4 v0, 0x0

    iput v0, p0, Lenl;->af:I

    .line 497
    return-void
.end method

.method protected d(I)V
    .locals 2

    .prologue
    .line 906
    invoke-virtual {p0}, Lenl;->ad()Landroid/content/Intent;

    move-result-object v0

    .line 907
    invoke-virtual {p0}, Lenl;->n()Lz;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 908
    invoke-virtual {p0}, Lenl;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 909
    return-void
.end method

.method protected d(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 991
    iget-object v0, p0, Lenl;->ao:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 992
    iget v0, p0, Lenl;->af:I

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lenl;->ah()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 993
    :goto_0
    iget-object v3, p0, Lenl;->ao:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    iget-object v3, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    if-ne p1, v3, :cond_0

    move v2, v1

    .line 994
    :cond_0
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 1000
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 992
    goto :goto_0

    .line 997
    :cond_3
    invoke-direct {p0}, Lenl;->am()Z

    move-result v0

    if-nez v0, :cond_1

    .line 998
    iget-object v0, p0, Lenl;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    goto :goto_1
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 470
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lenl;->al:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 333
    const-string v0, "profile_request_id"

    iget-object v1, p0, Lenl;->al:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 335
    :cond_0
    invoke-virtual {p0}, Lenl;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "profile_edit_items_proto"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 336
    const-string v0, "original_items_proto"

    invoke-virtual {p0}, Lenl;->Y()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 338
    :cond_1
    const-string v0, "items_proto"

    invoke-virtual {p0}, Lenl;->a()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 339
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 340
    return-void
.end method

.method protected e(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1003
    iget-object v0, p0, Lenl;->T:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1005
    iget v0, p0, Lenl;->af:I

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lenl;->ah()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 1006
    :goto_0
    iget-object v3, p0, Lenl;->T:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    iget-object v3, p0, Lenl;->T:Ljava/util/HashSet;

    iget-object v4, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    .line 1007
    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    .line 1008
    :cond_0
    invoke-direct {p0}, Lenl;->am()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0}, Lenl;->al()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    .line 1010
    :cond_2
    iget-object v0, p0, Lenl;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    .line 1012
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 1005
    goto :goto_0
.end method

.method protected f(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1015
    iget-object v0, p0, Lenl;->T:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1016
    iget v0, p0, Lenl;->af:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lenl;->ah()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1017
    :goto_0
    iget-object v3, p0, Lenl;->T:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    if-ne v3, v1, :cond_1

    iget-object v3, p0, Lenl;->aj:Lcom/google/android/libraries/social/acl/TextOnlyAudienceView;

    if-ne p1, v3, :cond_1

    .line 1018
    :goto_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 1022
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 1016
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1017
    goto :goto_1

    .line 1021
    :cond_2
    iget-object v0, p0, Lenl;->ak:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    goto :goto_2
.end method

.method protected g(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1333
    iget-object v0, p0, Lenl;->ay:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 1334
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1335
    return-void
.end method

.method protected h(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1338
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1339
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1340
    const v1, 0x7f10025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1341
    const v1, 0x7f100260

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1342
    return-void
.end method

.method protected i(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1356
    invoke-virtual {p0}, Lenl;->ak()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1357
    invoke-virtual {p0, p1}, Lenl;->h(Landroid/view/View;)V

    .line 1361
    :goto_0
    return-void

    .line 1359
    :cond_0
    invoke-virtual {p0, p1}, Lenl;->g(Landroid/view/View;)V

    goto :goto_0
.end method

.method public k(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 293
    invoke-virtual {p0}, Lenl;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "profile_edit_items_proto"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lenl;->aa:[B

    .line 294
    if-eqz p1, :cond_1

    .line 295
    const-string v0, "items_proto"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lenl;->Z:[B

    .line 296
    const-string v0, "profile_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 297
    const-string v0, "profile_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lenl;->al:Ljava/lang/Integer;

    .line 299
    :cond_0
    iget-object v0, p0, Lenl;->aa:[B

    if-nez v0, :cond_1

    .line 300
    const-string v0, "original_items_proto"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lenl;->aa:[B

    .line 303
    :cond_1
    iget-object v0, p0, Lenl;->aa:[B

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1365
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 1366
    const v1, 0x7f1001da

    if-ne v0, v1, :cond_1

    .line 1367
    invoke-virtual {p0}, Lenl;->af()V

    .line 1379
    :cond_0
    :goto_0
    return-void

    .line 1369
    :cond_1
    const v1, 0x7f1001fa

    if-ne v0, v1, :cond_2

    .line 1370
    invoke-virtual {p0}, Lenl;->V()V

    goto :goto_0

    .line 1372
    :cond_2
    const v1, 0x7f100541

    if-ne v0, v1, :cond_0

    .line 1373
    invoke-virtual {p0}, Lenl;->ae()V

    .line 1374
    const v0, 0x7f0a0376

    iget-object v1, p0, Lenl;->ac:Ljava/lang/String;

    iget-object v2, p0, Lenl;->ad:Ljava/lang/String;

    iget-boolean v3, p0, Lenl;->ae:Z

    invoke-static {v0, v1, v2, v3}, Leqf;->a(ILjava/lang/String;Ljava/lang/String;Z)Leqf;

    move-result-object v0

    .line 1376
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Leqf;->a(Lu;I)V

    .line 1377
    invoke-virtual {p0}, Lenl;->p()Lae;

    move-result-object v1

    const-string v2, "simple_audience"

    invoke-virtual {v0, v1, v2}, Leqf;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lenl;->aq:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 327
    invoke-super {p0}, Llol;->z()V

    .line 328
    return-void
.end method
