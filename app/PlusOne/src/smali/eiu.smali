.class public final Leiu;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private N:Landroid/widget/GridView;

.field private ah:Lfdv;

.field private ai:Z

.field private final aj:Licq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Legi;-><init>()V

    .line 53
    new-instance v0, Licq;

    iget-object v1, p0, Leiu;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064c

    .line 54
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Leiu;->aj:Licq;

    .line 221
    return-void
.end method

.method static synthetic a(Leiu;)Llnl;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Leiu;->at:Llnl;

    return-object v0
.end method

.method static synthetic a(Leiu;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Leiu;->d(Landroid/view/View;)V

    return-void
.end method

.method private ac()Z
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Leiu;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 174
    const-string v1, "GetTrashPhotosTask"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Leiu;)Lfdv;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Leiu;->ah:Lfdv;

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 204
    if-nez p1, :cond_0

    .line 219
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-virtual {p0}, Leiu;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 209
    invoke-direct {p0}, Leiu;->ac()Z

    move-result v0

    if-nez v0, :cond_1

    .line 210
    iget-object v0, p0, Leiu;->aj:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 217
    :goto_1
    invoke-virtual {p0}, Leiu;->Z_()V

    .line 218
    invoke-virtual {p0}, Leiu;->ag()V

    goto :goto_0

    .line 212
    :cond_1
    iget-object v0, p0, Leiu;->aj:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 215
    :cond_2
    iget-object v0, p0, Leiu;->aj:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lhmw;->ap:Lhmw;

    return-object v0
.end method

.method public L_()V
    .locals 4

    .prologue
    .line 179
    invoke-super {p0}, Legi;->L_()V

    .line 180
    invoke-virtual {p0}, Leiu;->n()Lz;

    move-result-object v1

    .line 181
    iget-object v0, p0, Leiu;->au:Llnh;

    const-class v2, Lhoc;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 183
    if-eqz v1, :cond_0

    invoke-direct {p0}, Leiu;->ac()Z

    move-result v2

    if-nez v2, :cond_0

    .line 184
    new-instance v2, Ldpc;

    iget-object v3, p0, Leiu;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {v2, v1, v3}, Ldpc;-><init>(Landroid/content/Context;I)V

    .line 185
    invoke-virtual {v0, v2}, Lhoc;->b(Lhny;)V

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Leiu;->ai:Z

    .line 188
    invoke-virtual {p0}, Leiu;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Leiu;->d(Landroid/view/View;)V

    .line 190
    :cond_0
    return-void
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Leiu;->ah:Lfdv;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 195
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 194
    :cond_1
    iget-object v0, p0, Leiu;->ah:Lfdv;

    invoke-virtual {v0}, Lfdv;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 195
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Leiu;->N:Landroid/widget/GridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a(Landroid/widget/AbsListView;)Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 280
    invoke-super {p0}, Legi;->Z()Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 112
    iget-object v0, p0, Leiu;->at:Llnl;

    .line 113
    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e7

    .line 112
    invoke-super {p0, v0, p2, p3, v1}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoAlbumView;

    .line 116
    iget-object v1, p0, Leiu;->at:Llnl;

    invoke-virtual {v1}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0291

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 119
    new-instance v1, Lfdv;

    iget-object v3, p0, Leiu;->at:Llnl;

    invoke-direct {v1, v3, v5}, Lfdv;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Leiu;->ah:Lfdv;

    .line 120
    iget-object v1, p0, Leiu;->ah:Lfdv;

    invoke-virtual {v1, p0}, Lfdv;->a(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v1, p0, Leiu;->ah:Lfdv;

    invoke-virtual {v1, p0}, Lfdv;->a(Landroid/view/View$OnLongClickListener;)V

    .line 123
    const v1, 0x7f100304

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Leiu;->N:Landroid/widget/GridView;

    .line 124
    iget-object v1, p0, Leiu;->N:Landroid/widget/GridView;

    new-instance v3, Ljvl;

    iget-object v4, p0, Leiu;->at:Llnl;

    invoke-direct {v3, v4}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v3, v3, Ljvl;->a:I

    invoke-virtual {v1, v3}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 125
    iget-object v1, p0, Leiu;->N:Landroid/widget/GridView;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 126
    iget-object v1, p0, Leiu;->N:Landroid/widget/GridView;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 127
    iget-object v1, p0, Leiu;->N:Landroid/widget/GridView;

    iget-object v2, p0, Leiu;->ah:Lfdv;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 128
    iget-object v1, p0, Leiu;->N:Landroid/widget/GridView;

    const v2, 0x7f020415

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setSelector(I)V

    .line 130
    if-eqz p3, :cond_0

    .line 131
    const-string v1, "refresh_complete"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Leiu;->ai:Z

    .line 134
    :cond_0
    iget-boolean v1, p0, Leiu;->ai:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Leiu;->P:Lhee;

    invoke-interface {v1}, Lhee;->f()Z

    move-result v1

    if-nez v1, :cond_2

    .line 135
    :cond_1
    invoke-virtual {p0}, Leiu;->w()Lbb;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Leiv;

    invoke-direct {v3, p0}, Leiv;-><init>(Leiu;)V

    invoke-virtual {v1, v2, v5, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 138
    :cond_2
    invoke-direct {p0, v0}, Leiu;->d(Landroid/view/View;)V

    .line 139
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 98
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 99
    if-nez p1, :cond_0

    iget-object v0, p0, Leiu;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {p0}, Leiu;->L_()V

    .line 104
    :cond_0
    iget-object v0, p0, Leiu;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 105
    new-instance v1, Lhpf;

    iget-object v2, p0, Leiu;->at:Llnl;

    .line 106
    invoke-virtual {p0}, Leiu;->p()Lae;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lhpf;-><init>(Landroid/content/Context;Lae;)V

    .line 105
    invoke-virtual {v0, v1}, Lhoc;->a(Lhos;)V

    .line 107
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 144
    const-string v0, "empty_trash"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "fingerprints"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 146
    new-instance v1, Ldpy;

    iget-object v2, p0, Leiu;->at:Llnl;

    iget-object v3, p0, Leiu;->P:Lhee;

    .line 147
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v0, v4}, Ldpy;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Z)V

    .line 148
    invoke-virtual {p0}, Leiu;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0875

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldpy;->a(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Leiu;->au:Llnh;

    const-class v2, Lhoc;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 150
    invoke-virtual {v0, v1}, Lhoc;->c(Lhny;)V

    .line 154
    :goto_0
    return-void

    .line 152
    :cond_0
    invoke-super {p0, p1, p2}, Legi;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 59
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Leiu;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Leiu;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    const-string v0, "GetTrashPhotosTask"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    iput-boolean v1, p0, Leiu;->Z:Z

    .line 68
    :cond_2
    iput-boolean v1, p0, Leiu;->ai:Z

    iget-boolean v0, p0, Leiu;->Z:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Leiu;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a07ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Leiu;->n()Lz;

    move-result-object v1

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Leiu;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Leiu;->d(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Leiu;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Leiv;

    invoke-direct {v2, p0}, Leiv;-><init>(Leiu;)V

    invoke-virtual {v0, v3, v1, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const v2, 0x7f0a0873

    .line 248
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f100697

    if-ne v0, v1, :cond_1

    .line 249
    invoke-virtual {p0}, Leiu;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Leiu;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0874

    invoke-virtual {p0, v1}, Leiu;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2}, Leiu;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0597

    invoke-virtual {p0, v3}, Leiu;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0}, Llgr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "fingerprints"

    iget-object v3, p0, Leiu;->ah:Lfdv;

    invoke-virtual {v3}, Lfdv;->c()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Leiu;->p()Lae;

    move-result-object v1

    const-string v2, "empty_trash"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 250
    :cond_0
    const/4 v0, 0x1

    .line 252
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected b(Lhjk;)V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 165
    const v0, 0x7f0a0a7d

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 167
    iget-object v0, p0, Leiu;->ah:Lfdv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leiu;->ah:Lfdv;

    invoke-virtual {v0}, Lfdv;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 168
    const v0, 0x7f100697

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 170
    :cond_0
    return-void
.end method

.method protected c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 88
    instance-of v0, p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 93
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 159
    const-string v0, "refresh_complete"

    iget-boolean v1, p0, Leiu;->ai:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 160
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 241
    check-cast p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    invoke-virtual {p0, p1}, Leiu;->b(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    .line 242
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Leiu;->e(I)V

    .line 243
    const/4 v0, 0x1

    return v0
.end method
