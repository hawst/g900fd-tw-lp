.class public final Ldov;
.super Lhny;
.source "PG"


# instance fields
.field final a:I

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final h:[B

.field private final i:[Ljava/lang/String;

.field private final j:[Ljava/lang/String;

.field private final k:Z

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:J

.field private final p:[Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Ljava/lang/String;[Ljava/lang/String;ZZZZJ[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 129
    const-string v2, "GetActivityStreamTask"

    invoke-direct {p0, p1, v2}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 131
    iput p2, p0, Ldov;->a:I

    .line 132
    iput p3, p0, Ldov;->b:I

    .line 133
    iput-object p4, p0, Ldov;->c:Ljava/lang/String;

    .line 134
    iput-object p5, p0, Ldov;->d:Ljava/lang/String;

    .line 135
    iput-object p6, p0, Ldov;->e:Ljava/lang/String;

    .line 136
    iput-object p7, p0, Ldov;->f:Ljava/lang/String;

    .line 137
    iput-object p8, p0, Ldov;->h:[B

    .line 138
    iput-object p9, p0, Ldov;->i:[Ljava/lang/String;

    .line 139
    iput-boolean p11, p0, Ldov;->k:Z

    .line 140
    iput-boolean p12, p0, Ldov;->l:Z

    .line 141
    iput-object p10, p0, Ldov;->j:[Ljava/lang/String;

    .line 142
    move/from16 v0, p14

    iput-boolean v0, p0, Ldov;->m:Z

    .line 143
    move/from16 v0, p13

    iput-boolean v0, p0, Ldov;->n:Z

    .line 144
    move-wide/from16 v0, p15

    iput-wide v0, p0, Ldov;->o:J

    .line 145
    move-object/from16 v0, p17

    iput-object v0, p0, Ldov;->p:[Ljava/lang/String;

    .line 146
    return-void
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Ljava/lang/String;[Ljava/lang/String;ZZJ[Ljava/lang/String;)Ldov;
    .locals 20

    .prologue
    .line 71
    new-instance v1, Ldov;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v2, p0

    move/from16 v3, p1

    move/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move-wide/from16 v16, p12

    move-object/from16 v18, p14

    invoke-direct/range {v1 .. v18}, Ldov;-><init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Ljava/lang/String;[Ljava/lang/String;ZZZZJ[Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZJ[Ljava/lang/String;)Ldov;
    .locals 19

    .prologue
    .line 99
    new-instance v1, Ldov;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    move-object/from16 v2, p0

    move/from16 v3, p1

    move/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    move/from16 v15, p8

    move-wide/from16 v16, p9

    move-object/from16 v18, p11

    invoke-direct/range {v1 .. v18}, Ldov;-><init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[Ljava/lang/String;[Ljava/lang/String;ZZZZJ[Ljava/lang/String;)V

    return-object v1
.end method

.method private d()Llaz;
    .locals 8

    .prologue
    .line 202
    const/4 v0, 0x0

    .line 203
    iget-object v1, p0, Ldov;->f:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p0, Ldov;->h:[B

    if-nez v1, :cond_2

    .line 204
    iget v0, p0, Ldov;->b:I

    iget-object v1, p0, Ldov;->d:Ljava/lang/String;

    iget-object v2, p0, Ldov;->e:Ljava/lang/String;

    iget-object v3, p0, Ldov;->c:Ljava/lang/String;

    iget-boolean v4, p0, Ldov;->k:Z

    invoke-static {v0, v1, v2, v3, v4}, Llap;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 206
    invoke-static {}, Llay;->a()Llay;

    move-result-object v1

    iget v2, p0, Ldov;->a:I

    invoke-virtual {v1, v2, v0}, Llay;->a(ILjava/lang/String;)Llaz;

    move-result-object v2

    .line 207
    if-eqz v2, :cond_1

    .line 208
    invoke-virtual {v2}, Llaz;->b()Loda;

    move-result-object v3

    .line 209
    iget-object v0, v3, Loda;->a:Logi;

    iget-object v0, v0, Logi;->b:[Lpyk;

    array-length v0, v0

    new-array v4, v0, [Ljava/lang/String;

    .line 210
    invoke-virtual {p0}, Ldov;->f()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    .line 211
    const/4 v1, 0x0

    :goto_0
    iget-object v5, v3, Loda;->a:Logi;

    iget-object v5, v5, Logi;->b:[Lpyk;

    array-length v5, v5

    if-ge v1, v5, :cond_0

    .line 212
    iget-object v5, v3, Loda;->a:Logi;

    iget-object v5, v5, Logi;->b:[Lpyk;

    aget-object v5, v5, v1

    .line 213
    iget v6, v5, Lpyk;->b:I

    .line 214
    invoke-interface {v0, v6}, Lkzl;->a(I)Lkzk;

    move-result-object v6

    .line 215
    iget v7, p0, Ldov;->a:I

    invoke-interface {v6, v7, v5}, Lkzk;->a(ILpyk;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 211
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    :cond_0
    iget-object v0, v3, Loda;->c:Lodc;

    if-eqz v0, :cond_3

    .line 219
    iget-object v0, v3, Loda;->c:Lodc;

    iget-object v0, v0, Lodc;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 224
    :goto_1
    new-instance v3, Ldow;

    invoke-direct {v3, p0, v0, v1, v4}, Ldow;-><init>(Ldov;J[Ljava/lang/String;)V

    invoke-static {v3}, Llsx;->a(Ljava/lang/Runnable;)V

    :cond_1
    move-object v0, v2

    .line 233
    :cond_2
    return-object v0

    .line 221
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 22

    .prologue
    .line 150
    invoke-virtual/range {p0 .. p0}, Ldov;->f()Landroid/content/Context;

    move-result-object v2

    .line 151
    new-instance v14, Lkfp;

    invoke-direct {v14}, Lkfp;-><init>()V

    .line 153
    move-object/from16 v0, p0

    iget-boolean v3, v0, Ldov;->k:Z

    invoke-virtual {v14, v3}, Lkfp;->a(Z)V

    .line 154
    move-object/from16 v0, p0

    iget-object v3, v0, Ldov;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Ldov;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Ldov;->b:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x38

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Get activities for circleId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " userId: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v3}, Lkfp;->b(Ljava/lang/String;)V

    .line 156
    const-string v3, "Activities:SyncStream"

    invoke-virtual {v14, v3}, Lkfp;->c(Ljava/lang/String;)V

    .line 159
    move-object/from16 v0, p0

    iget-boolean v3, v0, Ldov;->k:Z

    if-eqz v3, :cond_1

    .line 160
    const/16 v11, 0x14

    .line 167
    :goto_0
    new-instance v20, Lhoz;

    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Lhoz;-><init>(Z)V

    .line 168
    invoke-virtual/range {v20 .. v20}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v21

    .line 171
    :try_start_0
    move-object/from16 v0, p0

    iget v3, v0, Ldov;->a:I

    .line 172
    move-object/from16 v0, p0

    iget-boolean v4, v0, Ldov;->m:Z

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-boolean v4, v0, Ldov;->n:Z

    if-eqz v4, :cond_3

    .line 173
    :cond_0
    move-object/from16 v0, p0

    iget v4, v0, Ldov;->b:I

    move-object/from16 v0, p0

    iget-object v5, v0, Ldov;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Ldov;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Ldov;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Ldov;->k:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Ldov;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Ldov;->h:[B

    move-object/from16 v0, p0

    iget-object v12, v0, Ldov;->i:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Ldov;->j:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v15, v0, Ldov;->m:Z

    move-object/from16 v0, p0

    iget-wide v0, v0, Ldov;->o:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ldov;->p:[Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v2 .. v18}, Llap;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZJ[Ljava/lang/String;)Z

    move-result v2

    .line 177
    const-string v3, "is_changed"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    :goto_1
    invoke-virtual {v14}, Lkfp;->f()V

    .line 195
    invoke-virtual {v14}, Lkfp;->g()V

    move-object/from16 v2, v20

    :goto_2
    return-object v2

    .line 161
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Ldov;->f:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 162
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->e()I

    move-result v11

    goto :goto_0

    .line 164
    :cond_2
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->d()I

    move-result v11

    goto :goto_0

    .line 179
    :cond_3
    :try_start_1
    invoke-direct/range {p0 .. p0}, Ldov;->d()Llaz;

    move-result-object v16

    .line 180
    move-object/from16 v0, p0

    iget v4, v0, Ldov;->b:I

    move-object/from16 v0, p0

    iget-object v5, v0, Ldov;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Ldov;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Ldov;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Ldov;->k:Z

    move-object/from16 v0, p0

    iget-object v9, v0, Ldov;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Ldov;->h:[B

    move-object/from16 v0, p0

    iget-object v12, v0, Ldov;->i:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Ldov;->j:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v15, v0, Ldov;->l:Z

    move-object/from16 v0, p0

    iget-wide v0, v0, Ldov;->o:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ldov;->p:[Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v2 .. v19}, Llap;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[BI[Ljava/lang/String;[Ljava/lang/String;Lkfp;ZLlaz;J[Ljava/lang/String;)Llau;

    move-result-object v2

    .line 185
    const-string v3, "new_continuation_token"

    invoke-virtual {v2}, Llau;->a()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v3, "new_stream_token"

    invoke-virtual {v2}, Llau;->b()[B

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 187
    const-string v3, "new_server_timestamp"

    invoke-virtual {v2}, Llau;->c()J

    move-result-wide v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 190
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 191
    :try_start_2
    const-string v2, "GetActivityStreamTask"

    const-string v4, "Exception: "

    invoke-static {v2, v4, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 192
    new-instance v2, Lhoz;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v2, v4, v3, v5}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 194
    invoke-virtual {v14}, Lkfp;->f()V

    .line 195
    invoke-virtual {v14}, Lkfp;->g()V

    goto/16 :goto_2

    .line 194
    :catchall_0
    move-exception v2

    invoke-virtual {v14}, Lkfp;->f()V

    .line 195
    invoke-virtual {v14}, Lkfp;->g()V

    throw v2
.end method
