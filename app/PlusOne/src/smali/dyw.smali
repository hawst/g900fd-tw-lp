.class public final Ldyw;
.super Llgr;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private Q:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Llgr;-><init>()V

    .line 37
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ldyw;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Ldyw;

    invoke-direct {v0}, Ldyw;-><init>()V

    .line 62
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 63
    const-string v2, "circle_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v2, "name"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {v0, v1}, Ldyw;->f(Landroid/os/Bundle;)V

    .line 66
    return-object v0
.end method


# virtual methods
.method public U()V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Ldyw;->c()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 147
    if-nez v0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 151
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 152
    iget-object v0, p0, Ldyw;->Q:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 153
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 135
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 71
    invoke-virtual {p0}, Ldyw;->W_()Landroid/content/Context;

    move-result-object v0

    .line 72
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 74
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 75
    const v2, 0x7f040063

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 77
    const v0, 0x7f10013b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyw;->Q:Landroid/widget/TextView;

    .line 78
    iget-object v0, p0, Ldyw;->Q:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 79
    iget-object v0, p0, Ldyw;->Q:Landroid/widget/TextView;

    const v3, 0x7f0a0836

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setHint(I)V

    .line 82
    const v0, 0x7f10016d

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 83
    const v0, 0x7f1001f3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 85
    if-eqz p1, :cond_0

    .line 86
    iget-object v0, p0, Ldyw;->Q:Landroid/widget/TextView;

    const-string v3, "name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    :goto_0
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 94
    const v0, 0x7f0a0834

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 95
    const v0, 0x7f0a0596

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 96
    const v0, 0x7f0a0597

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 98
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 88
    :cond_0
    invoke-virtual {p0}, Ldyw;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 89
    iget-object v3, p0, Ldyw;->Q:Landroid/widget/TextView;

    const-string v4, "name"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 103
    const-string v0, "name"

    iget-object v1, p0, Ldyw;->Q:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 104
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 125
    invoke-super {p0}, Llgr;->g()V

    .line 126
    invoke-virtual {p0}, Ldyw;->U()V

    .line 127
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 108
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 109
    invoke-virtual {p0}, Ldyw;->u_()Lu;

    move-result-object v0

    .line 110
    check-cast v0, Ldyx;

    .line 111
    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p0}, Ldyw;->n()Lz;

    move-result-object v0

    check-cast v0, Ldyx;

    .line 115
    :cond_0
    invoke-virtual {p0}, Ldyw;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "circle_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    iget-object v2, p0, Ldyw;->Q:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ldyx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_1
    iget-object v0, p0, Ldyw;->Q:Landroid/widget/TextView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 120
    invoke-super {p0, p1, p2}, Llgr;->onClick(Landroid/content/DialogInterface;I)V

    .line 121
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 139
    invoke-virtual {p0}, Ldyw;->U()V

    .line 140
    return-void
.end method
