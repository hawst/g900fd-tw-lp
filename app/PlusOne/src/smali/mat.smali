.class public final Lmat;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmqq;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17818
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17821
    iput-object v0, p0, Lmat;->apiHeader:Llyr;

    .line 17824
    iput-object v0, p0, Lmat;->a:Lmqq;

    .line 17818
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 17841
    const/4 v0, 0x0

    .line 17842
    iget-object v1, p0, Lmat;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 17843
    const/4 v0, 0x1

    iget-object v1, p0, Lmat;->apiHeader:Llyr;

    .line 17844
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17846
    :cond_0
    iget-object v1, p0, Lmat;->a:Lmqq;

    if-eqz v1, :cond_1

    .line 17847
    const/4 v1, 0x2

    iget-object v2, p0, Lmat;->a:Lmqq;

    .line 17848
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17850
    :cond_1
    iget-object v1, p0, Lmat;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17851
    iput v0, p0, Lmat;->ai:I

    .line 17852
    return v0
.end method

.method public a(Loxn;)Lmat;
    .locals 2

    .prologue
    .line 17860
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 17861
    sparse-switch v0, :sswitch_data_0

    .line 17865
    iget-object v1, p0, Lmat;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 17866
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmat;->ah:Ljava/util/List;

    .line 17869
    :cond_1
    iget-object v1, p0, Lmat;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17871
    :sswitch_0
    return-object p0

    .line 17876
    :sswitch_1
    iget-object v0, p0, Lmat;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 17877
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmat;->apiHeader:Llyr;

    .line 17879
    :cond_2
    iget-object v0, p0, Lmat;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17883
    :sswitch_2
    iget-object v0, p0, Lmat;->a:Lmqq;

    if-nez v0, :cond_3

    .line 17884
    new-instance v0, Lmqq;

    invoke-direct {v0}, Lmqq;-><init>()V

    iput-object v0, p0, Lmat;->a:Lmqq;

    .line 17886
    :cond_3
    iget-object v0, p0, Lmat;->a:Lmqq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17861
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 17829
    iget-object v0, p0, Lmat;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 17830
    const/4 v0, 0x1

    iget-object v1, p0, Lmat;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17832
    :cond_0
    iget-object v0, p0, Lmat;->a:Lmqq;

    if-eqz v0, :cond_1

    .line 17833
    const/4 v0, 0x2

    iget-object v1, p0, Lmat;->a:Lmqq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17835
    :cond_1
    iget-object v0, p0, Lmat;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 17837
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 17814
    invoke-virtual {p0, p1}, Lmat;->a(Loxn;)Lmat;

    move-result-object v0

    return-object v0
.end method
