.class public final Lmhp;
.super Loxq;
.source "PG"


# instance fields
.field private a:Llte;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34018
    invoke-direct {p0}, Loxq;-><init>()V

    .line 34021
    iput-object v0, p0, Lmhp;->apiHeader:Llyr;

    .line 34024
    iput-object v0, p0, Lmhp;->a:Llte;

    .line 34018
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 34041
    const/4 v0, 0x0

    .line 34042
    iget-object v1, p0, Lmhp;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 34043
    const/4 v0, 0x1

    iget-object v1, p0, Lmhp;->apiHeader:Llyr;

    .line 34044
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 34046
    :cond_0
    iget-object v1, p0, Lmhp;->a:Llte;

    if-eqz v1, :cond_1

    .line 34047
    const/4 v1, 0x2

    iget-object v2, p0, Lmhp;->a:Llte;

    .line 34048
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34050
    :cond_1
    iget-object v1, p0, Lmhp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34051
    iput v0, p0, Lmhp;->ai:I

    .line 34052
    return v0
.end method

.method public a(Loxn;)Lmhp;
    .locals 2

    .prologue
    .line 34060
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 34061
    sparse-switch v0, :sswitch_data_0

    .line 34065
    iget-object v1, p0, Lmhp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 34066
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhp;->ah:Ljava/util/List;

    .line 34069
    :cond_1
    iget-object v1, p0, Lmhp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 34071
    :sswitch_0
    return-object p0

    .line 34076
    :sswitch_1
    iget-object v0, p0, Lmhp;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 34077
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmhp;->apiHeader:Llyr;

    .line 34079
    :cond_2
    iget-object v0, p0, Lmhp;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 34083
    :sswitch_2
    iget-object v0, p0, Lmhp;->a:Llte;

    if-nez v0, :cond_3

    .line 34084
    new-instance v0, Llte;

    invoke-direct {v0}, Llte;-><init>()V

    iput-object v0, p0, Lmhp;->a:Llte;

    .line 34086
    :cond_3
    iget-object v0, p0, Lmhp;->a:Llte;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 34061
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 34029
    iget-object v0, p0, Lmhp;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 34030
    const/4 v0, 0x1

    iget-object v1, p0, Lmhp;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 34032
    :cond_0
    iget-object v0, p0, Lmhp;->a:Llte;

    if-eqz v0, :cond_1

    .line 34033
    const/4 v0, 0x2

    iget-object v1, p0, Lmhp;->a:Llte;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 34035
    :cond_1
    iget-object v0, p0, Lmhp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 34037
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 34014
    invoke-virtual {p0, p1}, Lmhp;->a(Loxn;)Lmhp;

    move-result-object v0

    return-object v0
.end method
