.class public Lkmz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lkmz;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:[I

.field public static final e:[I


# instance fields
.field public a:Locz;

.field public b:Landroid/os/Bundle;

.field public c:Lkng;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    new-array v0, v1, [I

    aput v2, v0, v2

    sput-object v0, Lkmz;->d:[I

    .line 57
    new-array v0, v1, [I

    const/16 v1, 0x16d

    aput v1, v0, v2

    sput-object v0, Lkmz;->e:[I

    .line 213
    new-instance v0, Lkna;

    invoke-direct {v0}, Lkna;-><init>()V

    sput-object v0, Lkmz;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lkmz;->a:Locz;

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lkmz;->b:Landroid/os/Bundle;

    iget-object v0, p0, Lkmz;->b:Landroid/os/Bundle;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "No metadata."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    iget-object v0, p0, Lkmz;->a:Locz;

    iget-object v0, v0, Locz;->b:[Loft;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lkmz;->a(Loft;)V

    .line 183
    :cond_1
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 74
    iget-object v1, p0, Lkmz;->a:Locz;

    if-nez v1, :cond_1

    move-object v1, v0

    .line 76
    :goto_0
    if-eqz v1, :cond_0

    array-length v2, v1

    if-nez v2, :cond_2

    .line 90
    :cond_0
    :goto_1
    return-object v0

    .line 74
    :cond_1
    iget-object v1, p0, Lkmz;->a:Locz;

    iget-object v1, v1, Locz;->d:[Ljava/lang/String;

    goto :goto_0

    .line 80
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 82
    array-length v3, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    .line 83
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 87
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 88
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lkmz;->b:Landroid/os/Bundle;

    .line 168
    invoke-direct {p0}, Lkmz;->b()V

    .line 169
    return-void
.end method

.method public a(Lkng;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lkmz;->c:Lkng;

    .line 265
    return-void
.end method

.method public a(Locz;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lkmz;->a:Locz;

    .line 157
    invoke-direct {p0}, Lkmz;->b()V

    .line 158
    return-void
.end method

.method protected a(Loft;)V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lkmz;->a:Locz;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No metadata."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lkmz;->a:Locz;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lkmz;->a:Locz;

    invoke-static {v0}, Locz;->a(Loxu;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 209
    :goto_0
    iget-object v0, p0, Lkmz;->b:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 210
    iget-object v0, p0, Lkmz;->c:Lkng;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 211
    return-void

    .line 207
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
