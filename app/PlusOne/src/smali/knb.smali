.class public final Lknb;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/os/Bundle;Lkng;)Lkmz;
    .locals 2

    .prologue
    .line 91
    if-nez p0, :cond_0

    .line 94
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Content deep-link metadata must not be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_0
    const-string v0, "ApiaryActivityFactory"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    invoke-virtual {p0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    .line 102
    :cond_1
    new-instance v0, Lkmz;

    invoke-direct {v0}, Lkmz;-><init>()V

    .line 105
    :try_start_0
    invoke-virtual {v0, p0}, Lkmz;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    if-eqz p1, :cond_2

    .line 110
    invoke-virtual {v0, p1}, Lkmz;->a(Lkng;)V

    .line 113
    :cond_2
    :goto_0
    return-object v0

    .line 107
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Locz;)Lkmz;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 29
    if-nez p0, :cond_0

    .line 32
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    const-string v1, "ApiaryActivityFactory"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36
    invoke-virtual {p0}, Locz;->toString()Ljava/lang/String;

    .line 39
    :cond_1
    iget-object v1, p0, Locz;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 77
    :goto_0
    return-object v0

    .line 43
    :cond_2
    iget-object v1, p0, Locz;->b:[Loft;

    if-eqz v1, :cond_3

    iget-object v1, p0, Locz;->b:[Loft;

    array-length v1, v1

    if-nez v1, :cond_4

    .line 44
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Media layout must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_4
    iget-object v1, p0, Locz;->b:[Loft;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    .line 51
    iget v1, v1, Loft;->b:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 53
    :sswitch_0
    new-instance v1, Lknc;

    invoke-direct {v1}, Lknc;-><init>()V

    .line 72
    :goto_1
    :try_start_0
    invoke-virtual {v1, p0}, Lkmz;->a(Locz;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 77
    goto :goto_0

    .line 56
    :sswitch_1
    new-instance v1, Lknf;

    invoke-direct {v1}, Lknf;-><init>()V

    goto :goto_1

    .line 59
    :sswitch_2
    new-instance v1, Lkne;

    invoke-direct {v1}, Lkne;-><init>()V

    goto :goto_1

    .line 62
    :sswitch_3
    new-instance v1, Lkne;

    invoke-direct {v1}, Lkne;-><init>()V

    goto :goto_1

    .line 65
    :sswitch_4
    new-instance v1, Lknd;

    invoke-direct {v1}, Lknd;-><init>()V

    goto :goto_1

    .line 74
    :catch_0
    move-exception v1

    goto :goto_0

    .line 51
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_4
        0xb -> :sswitch_2
        0xd -> :sswitch_3
    .end sparse-switch
.end method
