.class public final Lmpx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lmpm;

.field public c:Ljava/lang/Boolean;

.field private d:I

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput-object v0, p0, Lmpx;->b:Lmpm;

    .line 18
    const/high16 v0, -0x80000000

    iput v0, p0, Lmpx;->d:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 73
    const/4 v0, 0x0

    .line 74
    iget-object v1, p0, Lmpx;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 75
    const/4 v0, 0x1

    iget-object v1, p0, Lmpx;->a:Ljava/lang/String;

    .line 76
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 78
    :cond_0
    iget v1, p0, Lmpx;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 79
    const/4 v1, 0x2

    iget v2, p0, Lmpx;->d:I

    .line 80
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_1
    iget-object v1, p0, Lmpx;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 83
    const/4 v1, 0x3

    iget-object v2, p0, Lmpx;->c:Ljava/lang/Boolean;

    .line 84
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 86
    :cond_2
    iget-object v1, p0, Lmpx;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 87
    const/4 v1, 0x4

    iget-object v2, p0, Lmpx;->f:Ljava/lang/String;

    .line 88
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_3
    iget-object v1, p0, Lmpx;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 91
    const/4 v1, 0x5

    iget-object v2, p0, Lmpx;->h:Ljava/lang/Boolean;

    .line 92
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 94
    :cond_4
    iget-object v1, p0, Lmpx;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 95
    const/4 v1, 0x6

    iget-object v2, p0, Lmpx;->i:Ljava/lang/Boolean;

    .line 96
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 98
    :cond_5
    iget-object v1, p0, Lmpx;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 99
    const/4 v1, 0x7

    iget-object v2, p0, Lmpx;->e:Ljava/lang/Integer;

    .line 100
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_6
    iget-object v1, p0, Lmpx;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 103
    const/16 v1, 0x8

    iget-object v2, p0, Lmpx;->g:Ljava/lang/String;

    .line 104
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_7
    iget-object v1, p0, Lmpx;->b:Lmpm;

    if-eqz v1, :cond_8

    .line 107
    const/16 v1, 0x9

    iget-object v2, p0, Lmpx;->b:Lmpm;

    .line 108
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_8
    iget-object v1, p0, Lmpx;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 111
    const/16 v1, 0xa

    iget-object v2, p0, Lmpx;->j:Ljava/lang/Boolean;

    .line 112
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 114
    :cond_9
    iget-object v1, p0, Lmpx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    iput v0, p0, Lmpx;->ai:I

    .line 116
    return v0
.end method

.method public a(Loxn;)Lmpx;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 124
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 125
    sparse-switch v0, :sswitch_data_0

    .line 129
    iget-object v1, p0, Lmpx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 130
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmpx;->ah:Ljava/util/List;

    .line 133
    :cond_1
    iget-object v1, p0, Lmpx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    :sswitch_0
    return-object p0

    .line 140
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpx;->a:Ljava/lang/String;

    goto :goto_0

    .line 144
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 145
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 152
    :cond_2
    iput v0, p0, Lmpx;->d:I

    goto :goto_0

    .line 154
    :cond_3
    iput v2, p0, Lmpx;->d:I

    goto :goto_0

    .line 159
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpx;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 163
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpx;->f:Ljava/lang/String;

    goto :goto_0

    .line 167
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpx;->h:Ljava/lang/Boolean;

    goto :goto_0

    .line 171
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpx;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 175
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmpx;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 179
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpx;->g:Ljava/lang/String;

    goto :goto_0

    .line 183
    :sswitch_9
    iget-object v0, p0, Lmpx;->b:Lmpm;

    if-nez v0, :cond_4

    .line 184
    new-instance v0, Lmpm;

    invoke-direct {v0}, Lmpm;-><init>()V

    iput-object v0, p0, Lmpx;->b:Lmpm;

    .line 186
    :cond_4
    iget-object v0, p0, Lmpx;->b:Lmpm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 190
    :sswitch_a
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpx;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 125
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lmpx;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    iget-object v1, p0, Lmpx;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 40
    :cond_0
    iget v0, p0, Lmpx;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 41
    const/4 v0, 0x2

    iget v1, p0, Lmpx;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 43
    :cond_1
    iget-object v0, p0, Lmpx;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 44
    const/4 v0, 0x3

    iget-object v1, p0, Lmpx;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 46
    :cond_2
    iget-object v0, p0, Lmpx;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 47
    const/4 v0, 0x4

    iget-object v1, p0, Lmpx;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 49
    :cond_3
    iget-object v0, p0, Lmpx;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 50
    const/4 v0, 0x5

    iget-object v1, p0, Lmpx;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 52
    :cond_4
    iget-object v0, p0, Lmpx;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 53
    const/4 v0, 0x6

    iget-object v1, p0, Lmpx;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 55
    :cond_5
    iget-object v0, p0, Lmpx;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 56
    const/4 v0, 0x7

    iget-object v1, p0, Lmpx;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 58
    :cond_6
    iget-object v0, p0, Lmpx;->g:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 59
    const/16 v0, 0x8

    iget-object v1, p0, Lmpx;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 61
    :cond_7
    iget-object v0, p0, Lmpx;->b:Lmpm;

    if-eqz v0, :cond_8

    .line 62
    const/16 v0, 0x9

    iget-object v1, p0, Lmpx;->b:Lmpm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 64
    :cond_8
    iget-object v0, p0, Lmpx;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 65
    const/16 v0, 0xa

    iget-object v1, p0, Lmpx;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 67
    :cond_9
    iget-object v0, p0, Lmpx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 69
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmpx;->a(Loxn;)Lmpx;

    move-result-object v0

    return-object v0
.end method
