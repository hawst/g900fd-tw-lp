.class public final Lhrf;
.super Lhxz;
.source "PG"

# interfaces
.implements Ljma;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lhrh;",
        ">;",
        "Ljma",
        "<",
        "Lhpu;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:[Ljava/lang/String;

.field private final d:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Lhrh;",
            ">.dp;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 195
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 192
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lhrf;->d:Ldp;

    .line 196
    iput-object p3, p0, Lhrf;->b:Ljava/lang/String;

    .line 197
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    new-array v0, v3, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, p0, Lhrf;->c:[Ljava/lang/String;

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    aput-object p3, v0, v3

    iput-object v0, p0, Lhrf;->c:[Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected D_()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 206
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 207
    sget-object v1, Lhqv;->b:Landroid/net/Uri;

    iget-object v2, p0, Lhrf;->d:Ldp;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 209
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lhqv;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lhrf;->d:Ldp;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 211
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lhrf;->d:Ldp;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 213
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 214
    invoke-virtual {v0}, Lhpu;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0, p0, v3}, Ljlx;->a(Ljma;Z)V

    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method public synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 29
    invoke-virtual {p0}, Lhrf;->l()V

    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lhrf;->m()Lhrh;

    move-result-object v0

    return-object v0
.end method

.method protected k()Z
    .locals 2

    .prologue
    .line 221
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lhrf;->d:Ldp;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 222
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 223
    invoke-virtual {v0}, Lhpu;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0, p0}, Ljlx;->a(Ljma;)V

    .line 224
    const/4 v0, 0x1

    return v0
.end method

.method public l()V
    .locals 0

    .prologue
    .line 34
    invoke-virtual {p0}, Lhrf;->t()V

    .line 35
    return-void
.end method

.method public m()Lhrh;
    .locals 14

    .prologue
    .line 229
    new-instance v6, Lhrh;

    invoke-direct {v6}, Lhrh;-><init>()V

    .line 230
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhpu;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    .line 233
    invoke-virtual {v0}, Lhpu;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 234
    const/4 v0, 0x0

    iput-boolean v0, v6, Lhrh;->e:Z

    move-object v0, v6

    .line 314
    :goto_0
    return-object v0

    .line 237
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v6, Lhrh;->e:Z

    .line 241
    invoke-virtual {v0}, Lhpu;->d()I

    move-result v0

    iput v0, v6, Lhrh;->d:I

    .line 242
    iget-object v0, p0, Lhrf;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 244
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhqo;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqo;

    iget-object v1, p0, Lhrf;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lhqo;->c(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v6, Lhrh;->f:Z

    .line 246
    :cond_1
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 247
    const/4 v7, 0x0

    .line 250
    :try_start_0
    iget-object v1, p0, Lhrf;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 251
    sget-object v3, Lhrg;->d:Ljava/lang/String;

    .line 255
    :goto_1
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lhqv;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lhrg;->a:[Ljava/lang/String;

    iget-object v4, p0, Lhrf;->c:[Ljava/lang/String;

    const-string v5, "upload_state DESC, _id DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 257
    if-nez v1, :cond_4

    .line 258
    if-eqz v1, :cond_2

    .line 310
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 253
    :cond_3
    :try_start_1
    sget-object v3, Lhrg;->e:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 261
    :cond_4
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 262
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 263
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v3, v6, Lhrh;->a:Ljava/util/Map;

    .line 264
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, v6, Lhrh;->b:Ljava/util/Map;

    .line 265
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v3, v6, Lhrh;->c:Ljava/util/Map;

    .line 266
    const/4 v2, 0x0

    iput-boolean v2, v6, Lhrh;->m:Z

    .line 267
    :cond_5
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 268
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 269
    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 270
    const/4 v7, 0x4

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 271
    const/4 v8, 0x6

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    sub-long v8, v4, v8

    .line 272
    iget-object v10, v6, Lhrh;->c:Ljava/util/Map;

    const/4 v11, 0x6

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-interface {v10, v3, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    invoke-static {v2, v7, v8, v9}, Lhri;->a(IIJ)Lhri;

    move-result-object v7

    .line 275
    const/16 v2, 0x8

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    .line 276
    :goto_3
    iget-object v8, v6, Lhrh;->a:Ljava/util/Map;

    invoke-interface {v8, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 277
    sget-object v8, Lhri;->b:Lhri;

    if-ne v7, v8, :cond_a

    .line 278
    const/4 v8, 0x2

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lhrh;->n:Ljava/lang/String;

    .line 279
    if-eqz v2, :cond_9

    .line 280
    iget v2, v6, Lhrh;->l:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lhrh;->l:I

    .line 284
    :goto_4
    iget v2, v6, Lhrh;->g:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lhrh;->g:I

    .line 301
    :cond_6
    :goto_5
    iget-object v2, v6, Lhrh;->a:Ljava/util/Map;

    invoke-interface {v2, v3, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    sget-object v2, Lhri;->c:Lhri;

    if-ne v7, v2, :cond_5

    .line 303
    const/4 v2, 0x7

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    .line 304
    iget-object v7, v6, Lhrh;->b:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v7, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 309
    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v1, :cond_7

    .line 310
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v0

    .line 275
    :cond_8
    const/4 v2, 0x0

    goto :goto_3

    .line 282
    :cond_9
    :try_start_3
    iget v2, v6, Lhrh;->k:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lhrh;->k:I

    goto :goto_4

    .line 285
    :cond_a
    sget-object v2, Lhri;->c:Lhri;

    if-ne v7, v2, :cond_b

    .line 286
    const/4 v2, 0x2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v6, Lhrh;->n:Ljava/lang/String;

    .line 287
    iget v2, v6, Lhrh;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lhrh;->h:I

    goto :goto_5

    .line 288
    :cond_b
    sget-object v2, Lhri;->e:Lhri;

    if-ne v7, v2, :cond_c

    .line 289
    iget v2, v6, Lhrh;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lhrh;->j:I

    goto :goto_5

    .line 290
    :cond_c
    sget-object v2, Lhri;->d:Lhri;

    if-ne v7, v2, :cond_d

    .line 291
    iget v2, v6, Lhrh;->j:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lhrh;->j:I

    .line 292
    const/4 v2, 0x1

    iput-boolean v2, v6, Lhrh;->m:Z

    goto :goto_5

    .line 293
    :cond_d
    sget-object v2, Lhri;->f:Lhri;

    if-ne v7, v2, :cond_6

    .line 294
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lhsf;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-gtz v2, :cond_e

    .line 295
    invoke-virtual {p0}, Lhrf;->n()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lhqv;->a(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    const-string v8, "media_url = ?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v3, v9, v10

    invoke-virtual {v0, v2, v8, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_5

    .line 298
    :cond_e
    iget v2, v6, Lhrh;->i:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v6, Lhrh;->i:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    .line 309
    :cond_f
    if-eqz v1, :cond_10

    .line 310
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_10
    move-object v0, v6

    .line 314
    goto/16 :goto_0

    .line 309
    :catchall_1
    move-exception v0

    move-object v1, v7

    goto :goto_6
.end method
