.class public Llrk;
.super Landroid/preference/PreferenceActivity;
.source "PG"


# instance fields
.field private a:I

.field public final b:Llqc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 18
    new-instance v0, Llqc;

    invoke-direct {v0}, Llqc;-><init>()V

    iput-object v0, p0, Llrk;->b:Llqc;

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Llrk;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Llrk;->a:I

    .line 166
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 159
    iget v0, p0, Llrk;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Llrk;->a:I

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->b(Landroid/content/Intent;)V

    .line 162
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    const/4 v0, 0x1

    .line 205
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->e()V

    .line 183
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->finish()V

    .line 184
    return-void
.end method

.method public onActionModeFinished(Landroid/view/ActionMode;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->g()V

    .line 197
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onActionModeFinished(Landroid/view/ActionMode;)V

    .line 198
    return-void
.end method

.method public onActionModeStarted(Landroid/view/ActionMode;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->f()V

    .line 190
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onActionModeStarted(Landroid/view/ActionMode;)V

    .line 191
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1, p2, p3}, Llqc;->a(IILandroid/content/Intent;)V

    .line 98
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 99
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 235
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onBackPressed()V

    .line 237
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->c(Landroid/os/Bundle;)V

    .line 28
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const/4 v0, 0x1

    .line 213
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->c()V

    .line 86
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    .line 87
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    const/4 v0, 0x1

    .line 244
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    const/4 v0, 0x1

    .line 252
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->m()V

    .line 104
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onLowMemory()V

    .line 105
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/content/Intent;)V

    .line 171
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 172
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    const/4 v0, 0x1

    .line 229
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->b()V

    .line 68
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 69
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Landroid/os/Bundle;)V

    .line 34
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 35
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->a()V

    .line 62
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPostResume()V

    .line 63
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    const/4 v0, 0x1

    .line 221
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->b(Landroid/os/Bundle;)V

    .line 50
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->k()V

    .line 56
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 57
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->d(Landroid/os/Bundle;)V

    .line 80
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method protected onStart()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 40
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 41
    invoke-virtual {p0}, Llrk;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Llri;->a(Landroid/app/FragmentManager;)V

    .line 43
    :cond_0
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->j()V

    .line 44
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 45
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->l()V

    .line 74
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    .line 75
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0}, Llqc;->d()V

    .line 177
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onUserLeaveHint()V

    .line 178
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Llrk;->b:Llqc;

    invoke-virtual {v0, p1}, Llqc;->a(Z)V

    .line 92
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onWindowFocusChanged(Z)V

    .line 93
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Llrk;->a(Landroid/content/Intent;)V

    .line 110
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->startActivity(Landroid/content/Intent;)V

    .line 111
    invoke-direct {p0}, Llrk;->a()V

    .line 112
    return-void
.end method

.method public startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 117
    invoke-direct {p0, p1}, Llrk;->a(Landroid/content/Intent;)V

    .line 118
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 119
    invoke-direct {p0}, Llrk;->a()V

    .line 120
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0, p1}, Llrk;->a(Landroid/content/Intent;)V

    .line 142
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 143
    invoke-direct {p0}, Llrk;->a()V

    .line 144
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0, p1}, Llrk;->a(Landroid/content/Intent;)V

    .line 150
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 151
    invoke-direct {p0}, Llrk;->a()V

    .line 152
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p2}, Llrk;->a(Landroid/content/Intent;)V

    .line 126
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;I)V

    .line 127
    invoke-direct {p0}, Llrk;->a()V

    .line 128
    return-void
.end method

.method public startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p2}, Llrk;->a(Landroid/content/Intent;)V

    .line 135
    invoke-super {p0, p1, p2, p3, p4}, Landroid/preference/PreferenceActivity;->startActivityFromFragment(Landroid/app/Fragment;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 136
    invoke-direct {p0}, Llrk;->a()V

    .line 137
    return-void
.end method
