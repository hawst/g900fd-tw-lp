.class public final Lelw;
.super Lelj;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static l:Lhym;


# instance fields
.field private m:Lfze;

.field private n:Landroid/view/View$OnClickListener;

.field private o:Lfuw;

.field private p:Lknu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lae;Lbb;ILfuw;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 56
    invoke-direct {p0, p1, p2, p3, p4}, Lelj;-><init>(Landroid/content/Context;Lae;Lbb;I)V

    .line 58
    sget-object v0, Lelw;->l:Lhym;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lhym;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v3

    invoke-direct {v0, v1, v4}, Lhym;-><init>([Ljava/lang/String;I)V

    .line 60
    sput-object v0, Lelw;->l:Lhym;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    .line 63
    :cond_0
    iput-object p5, p0, Lelw;->o:Lfuw;

    .line 64
    const-class v0, Lknu;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lknu;

    iput-object v0, p0, Lelw;->p:Lknu;

    .line 65
    return-void
.end method

.method static synthetic a(Lelw;)Lfze;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lelw;->m:Lfze;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x4

    return v0
.end method

.method protected a(II)I
    .locals 1

    .prologue
    .line 104
    sparse-switch p1, :sswitch_data_0

    .line 118
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 106
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 110
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 114
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 104
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x2 -> :sswitch_0
        0x6 -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 126
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 127
    sparse-switch p2, :sswitch_data_0

    .line 145
    const v1, 0x7f040164

    .line 146
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    .line 147
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 148
    iget-object v2, p0, Lelw;->p:Lknu;

    iget v3, p0, Lelw;->d:I

    invoke-interface {v2, v3}, Lknu;->a(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 149
    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Z)V

    .line 151
    :cond_0
    :goto_0
    return-object v0

    .line 129
    :sswitch_0
    const v1, 0x7f040170

    invoke-virtual {v0, v1, p5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 133
    :sswitch_1
    const v1, 0x7f04016d

    .line 134
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    .line 135
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 136
    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Z)V

    goto :goto_0

    .line 141
    :sswitch_2
    const v1, 0x7f040165

    invoke-virtual {v0, v1, p5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 127
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x2 -> :sswitch_1
        0x6 -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 15

    .prologue
    .line 159
    const/4 v4, 0x0

    .line 160
    packed-switch p2, :pswitch_data_0

    :goto_0
    :pswitch_0
    move-object/from16 p1, v4

    .line 305
    :cond_0
    :goto_1
    if-eqz p1, :cond_1

    .line 311
    move/from16 v0, p2

    invoke-virtual {p0, v0}, Lelw;->o(I)I

    .line 312
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c()V

    .line 314
    :cond_1
    return-void

    .line 164
    :pswitch_1
    check-cast p1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    .line 165
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 166
    iget-object v2, p0, Lelw;->m:Lfze;

    iget-object v3, p0, Lelw;->c:Lhxh;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lfze;Lhxh;Z)V

    .line 168
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 169
    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 170
    const/4 v2, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 172
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->b(Z)V

    .line 174
    const-string v3, "f:"

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    iget-object v12, p0, Lelw;->o:Lfuw;

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZLfuw;)V

    .line 179
    const v2, 0x7f1000ab

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setTag(ILjava/lang/Object;)V

    .line 180
    const v2, 0x7f1000b1

    invoke-virtual {v1, v2, v13}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setTag(ILjava/lang/Object;)V

    .line 181
    const v2, 0x7f1000b2

    invoke-virtual {v1, v2, v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setTag(ILjava/lang/Object;)V

    .line 182
    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 174
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 188
    :pswitch_2
    const v1, 0x7f10047e

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 189
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a081a

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lelw;->g:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v2, p0, Lelw;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 p1, v4

    .line 192
    goto/16 :goto_1

    .line 196
    :pswitch_3
    check-cast p1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    .line 197
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 198
    iget-object v2, p0, Lelw;->m:Lfze;

    iget-object v3, p0, Lelw;->c:Lhxh;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lfze;Lhxh;Z)V

    .line 199
    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 200
    const/16 v2, 0x8

    .line 201
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_3

    const/4 v11, 0x1

    .line 202
    :goto_3
    const/16 v2, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_4

    const/4 v12, 0x1

    .line 203
    :goto_4
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x4

    .line 206
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 205
    invoke-static {v4}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    .line 207
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v11, :cond_5

    iget-object v6, p0, Lelw;->h:Ljava/lang/String;

    :goto_5
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x5

    .line 211
    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    .line 215
    move-object/from16 v0, p3

    invoke-virtual {p0, v0}, Lelw;->a(Landroid/database/Cursor;)Landroid/os/Bundle;

    move-result-object v13

    iget-object v14, p0, Lelw;->o:Lfuw;

    .line 203
    invoke-virtual/range {v1 .. v14}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLjava/lang/String;Ljava/lang/String;ZZLandroid/os/Bundle;Lfuw;)V

    .line 217
    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 201
    :cond_3
    const/4 v11, 0x0

    goto :goto_3

    .line 202
    :cond_4
    const/4 v12, 0x0

    goto :goto_4

    .line 207
    :cond_5
    const/4 v6, 0x0

    goto :goto_5

    .line 222
    :pswitch_4
    check-cast p1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    .line 223
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 225
    const/4 v2, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 227
    iget-object v3, p0, Lelw;->m:Lfze;

    iget-object v4, p0, Lelw;->c:Lhxh;

    const/4 v5, 0x2

    if-eq v2, v5, :cond_9

    const/4 v2, 0x1

    :goto_6
    invoke-virtual {v1, v3, v4, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lfze;Lhxh;Z)V

    .line 230
    const/16 v2, 0xb

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 231
    if-nez v2, :cond_11

    .line 232
    const/4 v2, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 233
    if-nez v2, :cond_11

    .line 234
    const/16 v2, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v6, v2

    .line 238
    :goto_7
    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 240
    iget-object v2, p0, Lelw;->j:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 241
    const-string v9, "15"

    .line 246
    :goto_8
    const/16 v2, 0xc

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_b

    const/4 v11, 0x1

    .line 248
    :goto_9
    const/16 v2, 0xd

    .line 249
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_c

    const/4 v12, 0x1

    .line 251
    :goto_a
    iget-object v2, p0, Lelw;->b:Lhei;

    iget v4, p0, Lelw;->d:I

    invoke-interface {v2, v4}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v4, "gaia_id"

    invoke-interface {v2, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 253
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x1

    :goto_b
    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->b(Z)V

    .line 254
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x5

    .line 257
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 256
    invoke-static {v4}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    .line 258
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v11, :cond_6

    iget-object v6, p0, Lelw;->h:Ljava/lang/String;

    :cond_6
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 263
    move-object/from16 v0, p3

    invoke-virtual {p0, v0}, Lelw;->a(Landroid/database/Cursor;)Landroid/os/Bundle;

    move-result-object v13

    iget-object v14, p0, Lelw;->o:Lfuw;

    .line 254
    invoke-virtual/range {v1 .. v14}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZLjava/lang/String;Ljava/lang/String;ZZLandroid/os/Bundle;Lfuw;)V

    .line 265
    if-nez v3, :cond_7

    if-eqz v9, :cond_e

    :cond_7
    move-object v2, p0

    :goto_c
    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    if-nez v3, :cond_8

    if-eqz v9, :cond_f

    :cond_8
    const/4 v2, 0x1

    :goto_d
    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setClickable(Z)V

    .line 269
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x5

    move/from16 v0, p4

    if-lt v0, v1, :cond_0

    .line 270
    invoke-virtual {p0}, Lelw;->e()V

    goto/16 :goto_1

    .line 227
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_6

    .line 243
    :cond_a
    const/4 v2, 0x6

    .line 244
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_8

    .line 246
    :cond_b
    const/4 v11, 0x0

    goto :goto_9

    .line 249
    :cond_c
    const/4 v12, 0x0

    goto :goto_a

    .line 253
    :cond_d
    const/4 v2, 0x0

    goto :goto_b

    .line 265
    :cond_e
    const/4 v2, 0x0

    goto :goto_c

    .line 266
    :cond_f
    const/4 v2, 0x0

    goto :goto_d

    .line 277
    :pswitch_5
    check-cast p1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    .line 278
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 279
    iget-object v2, p0, Lelw;->m:Lfze;

    iget-object v3, p0, Lelw;->c:Lhxh;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lfze;Lhxh;Z)V

    .line 280
    const-string v3, "e:"

    iget-object v2, p0, Lelw;->g:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_10

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_e
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v1 .. v12}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZLfuw;)V

    .line 282
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setClickable(Z)V

    goto/16 :goto_1

    .line 280
    :cond_10
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_e

    .line 288
    :pswitch_6
    const/16 v3, 0x8

    .line 289
    const/16 v2, 0x8

    .line 290
    const/16 v1, 0x8

    .line 291
    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    packed-switch v5, :pswitch_data_1

    .line 302
    :goto_f
    const v5, 0x7f100286

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 303
    const v3, 0x7f100485

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 304
    const v2, 0x7f100486

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 293
    :pswitch_7
    const/4 v3, 0x0

    .line 294
    goto :goto_f

    .line 296
    :pswitch_8
    const/4 v2, 0x0

    .line 297
    goto :goto_f

    .line 299
    :pswitch_9
    const/4 v1, 0x0

    goto :goto_f

    :cond_11
    move-object v6, v2

    goto/16 :goto_7

    .line 160
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_6
    .end packed-switch

    .line 291
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public a(Lfze;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lelw;->m:Lfze;

    .line 69
    return-void
.end method

.method protected b(II)Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 318
    const/4 v0, 0x0

    return v0
.end method

.method public j(Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 75
    if-eqz p1, :cond_0

    .line 76
    sget-object v0, Lelw;->l:Lhym;

    invoke-virtual {p0, v1, v0}, Lelw;->a(ILandroid/database/Cursor;)V

    .line 77
    new-instance v0, Lelx;

    invoke-direct {v0, p0}, Lelx;-><init>(Lelw;)V

    iput-object v0, p0, Lelw;->n:Landroid/view/View$OnClickListener;

    .line 90
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-virtual {p0, v1, v0}, Lelw;->a(ILandroid/database/Cursor;)V

    .line 88
    iput-object v0, p0, Lelw;->n:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 323
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 324
    const v1, 0x7f100183

    if-ne v0, v1, :cond_0

    .line 325
    const v0, 0x7f1000ab

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    .line 326
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 327
    const v0, 0x7f1000b1

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 328
    const v1, 0x7f1000b2

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 329
    iget v2, p0, Lelw;->d:I

    .line 330
    iget-object v3, p0, Lelw;->k:Landroid/content/Context;

    invoke-static {v3, v2, v0, v1}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 332
    iget-object v1, p0, Lelw;->k:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 333
    :cond_1
    iget-object v0, p0, Lelw;->m:Lfze;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    if-eqz v0, :cond_0

    .line 334
    check-cast p1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 335
    iget-object v0, p0, Lelw;->m:Lfze;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->d()Ljava/lang/String;

    move-result-object v2

    .line 336
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 338
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->e()I

    move-result v4

    .line 337
    invoke-static {v4}, Ldib;->b(I)I

    move-result v4

    .line 335
    invoke-interface {v0, v1, v2, v3, v4}, Lfze;->a(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;I)V

    goto :goto_0
.end method
