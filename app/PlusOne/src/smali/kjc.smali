.class public final Lkjc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Lhmr;

.field private d:Lkey;

.field private e:Ljava/lang/String;

.field private f:Loya;

.field private g:Lhgw;

.field private h:Lhgw;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lizr;",
            ">;"
        }
    .end annotation
.end field

.field private n:Llae;

.field private o:Z

.field private p:Lkzm;

.field private q:Loya;

.field private r:Lkzz;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Lkji;

.field private z:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public A()[B
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lkjc;->z:[B

    return-object v0
.end method

.method public a()Lkjc;
    .locals 0

    .prologue
    .line 159
    return-object p0
.end method

.method public a(I)Lkjc;
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lkjc;->b:I

    .line 70
    return-object p0
.end method

.method public a(Landroid/content/Context;)Lkjc;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lkjc;->a:Landroid/content/Context;

    .line 65
    return-object p0
.end method

.method public a(Lhgw;)Lkjc;
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lkjc;->g:Lhgw;

    .line 95
    return-object p0
.end method

.method public a(Lhmr;)Lkjc;
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lkjc;->c:Lhmr;

    .line 75
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lkjc;
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lkjc;->e:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Lkjc;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lizr;",
            ">;)",
            "Lkjc;"
        }
    .end annotation

    .prologue
    .line 124
    iput-object p1, p0, Lkjc;->m:Ljava/util/ArrayList;

    .line 125
    return-object p0
.end method

.method public a(Lkey;)Lkjc;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lkjc;->d:Lkey;

    .line 80
    return-object p0
.end method

.method public a(Lkji;)Lkjc;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lkjc;->y:Lkji;

    .line 175
    return-object p0
.end method

.method public a(Lkzm;)Lkjc;
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lkjc;->p:Lkzm;

    .line 140
    return-object p0
.end method

.method public a(Lkzz;)Lkjc;
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lkjc;->r:Lkzz;

    .line 150
    return-object p0
.end method

.method public a(Llae;)Lkjc;
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lkjc;->n:Llae;

    .line 130
    return-object p0
.end method

.method public a(Loya;)Lkjc;
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lkjc;->f:Loya;

    .line 90
    return-object p0
.end method

.method public a(Z)Lkjc;
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Lkjc;->v:Z

    .line 50
    return-object p0
.end method

.method public a([B)Lkjc;
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lkjc;->z:[B

    .line 180
    return-object p0
.end method

.method public b(Lhgw;)Lkjc;
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lkjc;->h:Lhgw;

    .line 100
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lkjc;
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lkjc;->i:Ljava/lang/String;

    .line 105
    return-object p0
.end method

.method public b(Loya;)Lkjc;
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lkjc;->q:Loya;

    .line 145
    return-object p0
.end method

.method public b(Z)Lkjc;
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Lkjc;->w:Z

    .line 55
    return-object p0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lkjc;->w:Z

    return v0
.end method

.method public c(Ljava/lang/String;)Lkjc;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lkjc;->j:Ljava/lang/String;

    .line 110
    return-object p0
.end method

.method public c(Z)Lkjc;
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Lkjc;->x:Z

    .line 60
    return-object p0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lkjc;->v:Z

    return v0
.end method

.method public d(Ljava/lang/String;)Lkjc;
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lkjc;->k:Ljava/lang/String;

    .line 115
    return-object p0
.end method

.method public d(Z)Lkjc;
    .locals 0

    .prologue
    .line 134
    iput-boolean p1, p0, Lkjc;->o:Z

    .line 135
    return-object p0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lkjc;->x:Z

    return v0
.end method

.method public e()Landroid/content/Context;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lkjc;->a:Landroid/content/Context;

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lkjc;
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lkjc;->l:Ljava/lang/String;

    .line 120
    return-object p0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lkjc;->b:I

    return v0
.end method

.method public f(Ljava/lang/String;)Lkjc;
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lkjc;->s:Ljava/lang/String;

    .line 155
    return-object p0
.end method

.method public g()Lhmr;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lkjc;->c:Lhmr;

    return-object v0
.end method

.method public g(Ljava/lang/String;)Lkjc;
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lkjc;->t:Ljava/lang/String;

    .line 165
    return-object p0
.end method

.method public h()Lkey;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lkjc;->d:Lkey;

    return-object v0
.end method

.method public h(Ljava/lang/String;)Lkjc;
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lkjc;->u:Ljava/lang/String;

    .line 170
    return-object p0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lkjc;->e:Ljava/lang/String;

    return-object v0
.end method

.method public j()Loya;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lkjc;->f:Loya;

    return-object v0
.end method

.method public k()Lhgw;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lkjc;->g:Lhgw;

    return-object v0
.end method

.method public l()Lhgw;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lkjc;->h:Lhgw;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lkjc;->i:Ljava/lang/String;

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lkjc;->j:Ljava/lang/String;

    return-object v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lkjc;->k:Ljava/lang/String;

    return-object v0
.end method

.method public p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lkjc;->l:Ljava/lang/String;

    return-object v0
.end method

.method public q()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lizr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lkjc;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method public r()Llae;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lkjc;->n:Llae;

    return-object v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lkjc;->o:Z

    return v0
.end method

.method public t()Lkzm;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lkjc;->p:Lkzm;

    return-object v0
.end method

.method public u()Loya;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lkjc;->q:Loya;

    return-object v0
.end method

.method public v()Lkzz;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lkjc;->r:Lkzz;

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lkjc;->s:Ljava/lang/String;

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lkjc;->t:Ljava/lang/String;

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lkjc;->u:Ljava/lang/String;

    return-object v0
.end method

.method public z()Lkji;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lkjc;->y:Lkji;

    return-object v0
.end method
