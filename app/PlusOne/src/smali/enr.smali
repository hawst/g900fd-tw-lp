.class public final Lenr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final a:Landroid/widget/TextView;

.field private synthetic b:Lenl;


# direct methods
.method public constructor <init>(Lenl;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 1075
    iput-object p1, p0, Lenr;->b:Lenl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1076
    iput-object p2, p0, Lenr;->a:Landroid/widget/TextView;

    .line 1077
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1094
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1081
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1085
    iget-object v0, p0, Lenr;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 1086
    iget-object v0, p0, Lenr;->b:Lenl;

    iget-object v1, p0, Lenr;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lenl;->f(Landroid/view/View;)V

    .line 1090
    :goto_0
    return-void

    .line 1088
    :cond_0
    iget-object v0, p0, Lenr;->b:Lenl;

    iget-object v1, p0, Lenr;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lenl;->e(Landroid/view/View;)V

    goto :goto_0
.end method
