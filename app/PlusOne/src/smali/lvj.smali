.class public final Llvj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[Llvk;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 87
    sget-object v0, Llvk;->a:[Llvk;

    iput-object v0, p0, Llvj;->b:[Llvk;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 113
    .line 114
    iget-object v0, p0, Llvj;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 115
    const/4 v0, 0x1

    iget-object v2, p0, Llvj;->a:Ljava/lang/String;

    .line 116
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 118
    :goto_0
    iget-object v2, p0, Llvj;->b:[Llvk;

    if-eqz v2, :cond_1

    .line 119
    iget-object v2, p0, Llvj;->b:[Llvk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 120
    if-eqz v4, :cond_0

    .line 121
    const/4 v5, 0x2

    .line 122
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 119
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 126
    :cond_1
    iget-object v1, p0, Llvj;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 127
    const/4 v1, 0x3

    iget-object v2, p0, Llvj;->c:Ljava/lang/Boolean;

    .line 128
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 130
    :cond_2
    iget-object v1, p0, Llvj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    iput v0, p0, Llvj;->ai:I

    .line 132
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Llvj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 140
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 141
    sparse-switch v0, :sswitch_data_0

    .line 145
    iget-object v2, p0, Llvj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 146
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llvj;->ah:Ljava/util/List;

    .line 149
    :cond_1
    iget-object v2, p0, Llvj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 151
    :sswitch_0
    return-object p0

    .line 156
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llvj;->a:Ljava/lang/String;

    goto :goto_0

    .line 160
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 161
    iget-object v0, p0, Llvj;->b:[Llvk;

    if-nez v0, :cond_3

    move v0, v1

    .line 162
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llvk;

    .line 163
    iget-object v3, p0, Llvj;->b:[Llvk;

    if-eqz v3, :cond_2

    .line 164
    iget-object v3, p0, Llvj;->b:[Llvk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 166
    :cond_2
    iput-object v2, p0, Llvj;->b:[Llvk;

    .line 167
    :goto_2
    iget-object v2, p0, Llvj;->b:[Llvk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 168
    iget-object v2, p0, Llvj;->b:[Llvk;

    new-instance v3, Llvk;

    invoke-direct {v3}, Llvk;-><init>()V

    aput-object v3, v2, v0

    .line 169
    iget-object v2, p0, Llvj;->b:[Llvk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 170
    invoke-virtual {p1}, Loxn;->a()I

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 161
    :cond_3
    iget-object v0, p0, Llvj;->b:[Llvk;

    array-length v0, v0

    goto :goto_1

    .line 173
    :cond_4
    iget-object v2, p0, Llvj;->b:[Llvk;

    new-instance v3, Llvk;

    invoke-direct {v3}, Llvk;-><init>()V

    aput-object v3, v2, v0

    .line 174
    iget-object v2, p0, Llvj;->b:[Llvk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 178
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llvj;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 141
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 94
    iget-object v0, p0, Llvj;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    iget-object v1, p0, Llvj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 97
    :cond_0
    iget-object v0, p0, Llvj;->b:[Llvk;

    if-eqz v0, :cond_2

    .line 98
    iget-object v1, p0, Llvj;->b:[Llvk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 99
    if-eqz v3, :cond_1

    .line 100
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 98
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104
    :cond_2
    iget-object v0, p0, Llvj;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 105
    const/4 v0, 0x3

    iget-object v1, p0, Llvj;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 107
    :cond_3
    iget-object v0, p0, Llvj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 109
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llvj;->a(Loxn;)Llvj;

    move-result-object v0

    return-object v0
.end method
