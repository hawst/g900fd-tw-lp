.class public final Losy;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Losy;


# instance fields
.field private b:Lovk;

.field private c:Losf;

.field private d:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1237
    const/4 v0, 0x0

    new-array v0, v0, [Losy;

    sput-object v0, Losy;->a:[Losy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1238
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1241
    iput-object v0, p0, Losy;->b:Lovk;

    .line 1244
    iput-object v0, p0, Losy;->c:Losf;

    .line 1238
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1266
    const/4 v0, 0x0

    .line 1267
    iget-object v1, p0, Losy;->b:Lovk;

    if-eqz v1, :cond_0

    .line 1268
    const/4 v0, 0x1

    iget-object v1, p0, Losy;->b:Lovk;

    .line 1269
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1271
    :cond_0
    iget-object v1, p0, Losy;->c:Losf;

    if-eqz v1, :cond_1

    .line 1272
    const/4 v1, 0x2

    iget-object v2, p0, Losy;->c:Losf;

    .line 1273
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1275
    :cond_1
    iget-object v1, p0, Losy;->d:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 1276
    const/4 v1, 0x3

    iget-object v2, p0, Losy;->d:Ljava/lang/Float;

    .line 1277
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1279
    :cond_2
    iget-object v1, p0, Losy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1280
    iput v0, p0, Losy;->ai:I

    .line 1281
    return v0
.end method

.method public a(Loxn;)Losy;
    .locals 2

    .prologue
    .line 1289
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1290
    sparse-switch v0, :sswitch_data_0

    .line 1294
    iget-object v1, p0, Losy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1295
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Losy;->ah:Ljava/util/List;

    .line 1298
    :cond_1
    iget-object v1, p0, Losy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1300
    :sswitch_0
    return-object p0

    .line 1305
    :sswitch_1
    iget-object v0, p0, Losy;->b:Lovk;

    if-nez v0, :cond_2

    .line 1306
    new-instance v0, Lovk;

    invoke-direct {v0}, Lovk;-><init>()V

    iput-object v0, p0, Losy;->b:Lovk;

    .line 1308
    :cond_2
    iget-object v0, p0, Losy;->b:Lovk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1312
    :sswitch_2
    iget-object v0, p0, Losy;->c:Losf;

    if-nez v0, :cond_3

    .line 1313
    new-instance v0, Losf;

    invoke-direct {v0}, Losf;-><init>()V

    iput-object v0, p0, Losy;->c:Losf;

    .line 1315
    :cond_3
    iget-object v0, p0, Losy;->c:Losf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1319
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Losy;->d:Ljava/lang/Float;

    goto :goto_0

    .line 1290
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1251
    iget-object v0, p0, Losy;->b:Lovk;

    if-eqz v0, :cond_0

    .line 1252
    const/4 v0, 0x1

    iget-object v1, p0, Losy;->b:Lovk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1254
    :cond_0
    iget-object v0, p0, Losy;->c:Losf;

    if-eqz v0, :cond_1

    .line 1255
    const/4 v0, 0x2

    iget-object v1, p0, Losy;->c:Losf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1257
    :cond_1
    iget-object v0, p0, Losy;->d:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 1258
    const/4 v0, 0x3

    iget-object v1, p0, Losy;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1260
    :cond_2
    iget-object v0, p0, Losy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1262
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1234
    invoke-virtual {p0, p1}, Losy;->a(Loxn;)Losy;

    move-result-object v0

    return-object v0
.end method
