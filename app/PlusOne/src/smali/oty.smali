.class public final Loty;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Lopf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1130
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1153
    const/high16 v0, -0x80000000

    iput v0, p0, Loty;->a:I

    .line 1156
    const/4 v0, 0x0

    iput-object v0, p0, Loty;->b:Lopf;

    .line 1130
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1173
    const/4 v0, 0x0

    .line 1174
    iget v1, p0, Loty;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1175
    const/4 v0, 0x1

    iget v1, p0, Loty;->a:I

    .line 1176
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1178
    :cond_0
    iget-object v1, p0, Loty;->b:Lopf;

    if-eqz v1, :cond_1

    .line 1179
    const/4 v1, 0x2

    iget-object v2, p0, Loty;->b:Lopf;

    .line 1180
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1182
    :cond_1
    iget-object v1, p0, Loty;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1183
    iput v0, p0, Loty;->ai:I

    .line 1184
    return v0
.end method

.method public a(Loxn;)Loty;
    .locals 2

    .prologue
    .line 1192
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1193
    sparse-switch v0, :sswitch_data_0

    .line 1197
    iget-object v1, p0, Loty;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1198
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loty;->ah:Ljava/util/List;

    .line 1201
    :cond_1
    iget-object v1, p0, Loty;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1203
    :sswitch_0
    return-object p0

    .line 1208
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1209
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 1226
    :cond_2
    iput v0, p0, Loty;->a:I

    goto :goto_0

    .line 1228
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Loty;->a:I

    goto :goto_0

    .line 1233
    :sswitch_2
    iget-object v0, p0, Loty;->b:Lopf;

    if-nez v0, :cond_4

    .line 1234
    new-instance v0, Lopf;

    invoke-direct {v0}, Lopf;-><init>()V

    iput-object v0, p0, Loty;->b:Lopf;

    .line 1236
    :cond_4
    iget-object v0, p0, Loty;->b:Lopf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1193
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1161
    iget v0, p0, Loty;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1162
    const/4 v0, 0x1

    iget v1, p0, Loty;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1164
    :cond_0
    iget-object v0, p0, Loty;->b:Lopf;

    if-eqz v0, :cond_1

    .line 1165
    const/4 v0, 0x2

    iget-object v1, p0, Loty;->b:Lopf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1167
    :cond_1
    iget-object v0, p0, Loty;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1169
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1126
    invoke-virtual {p0, p1}, Loty;->a(Loxn;)Loty;

    move-result-object v0

    return-object v0
.end method
