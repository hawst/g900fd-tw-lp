.class public Lhke;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhkk;
.implements Llnx;
.implements Llrb;
.implements Llrc;
.implements Llrd;
.implements Llrg;


# instance fields
.field private a:Lhkp;

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lhkd;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lhkh;

.field private d:Lhkj;

.field private e:Lhko;


# direct methods
.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhke;-><init>(Llqr;B)V

    .line 92
    return-void
.end method

.method constructor <init>(Llqr;B)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lhke;->b:Landroid/util/SparseArray;

    .line 95
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 96
    return-void
.end method

.method private a(ILhkb;)V
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lhke;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkd;

    .line 214
    if-eqz v0, :cond_0

    .line 215
    iget v1, p2, Lhkb;->b:I

    iget-object v2, p2, Lhkb;->c:Landroid/content/Intent;

    invoke-interface {v0, v1, v2}, Lhkd;->a(ILandroid/content/Intent;)V

    .line 217
    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILhkd;)Lhke;
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Lhke;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot register more than one handler for a given  id: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xb

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_0
    iget-object v0, p0, Lhke;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 116
    return-object p0
.end method

.method public a(Llnh;)Lhke;
    .locals 1

    .prologue
    .line 99
    const-class v0, Lhke;

    invoke-virtual {p1, v0, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 100
    return-object p0
.end method

.method public a(ILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 137
    iget-object v0, p0, Lhke;->e:Lhko;

    invoke-virtual {v0, p1}, Lhko;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 139
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must start an activity for result using a resource id as the request code to guarantee overlap does not occur"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_0
    if-nez p2, :cond_1

    .line 145
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Intent must not be null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_1
    iget-object v0, p0, Lhke;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkd;

    .line 149
    if-nez v0, :cond_2

    .line 150
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x71

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "You must register a result handler for request code"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " before starting an activity for result with that request code"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_2
    iget-object v0, p0, Lhke;->c:Lhkh;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhkh;->a(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    .line 155
    if-nez v0, :cond_3

    .line 156
    iget-object v0, p0, Lhke;->a:Lhkp;

    invoke-virtual {v0}, Lhkp;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 157
    iget-object v1, p0, Lhke;->c:Lhkh;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lhkh;->a(Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 159
    :cond_3
    iget-object v1, p0, Lhke;->d:Lhkj;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0, p2}, Lhkj;->a(ILandroid/content/Intent;)V

    .line 160
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 164
    const-class v0, Lhkj;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkj;

    iput-object v0, p0, Lhke;->d:Lhkj;

    .line 165
    const-class v0, Lhkp;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkp;

    iput-object v0, p0, Lhke;->a:Lhkp;

    .line 166
    const-class v0, Lhko;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhko;

    iput-object v0, p0, Lhke;->e:Lhko;

    .line 167
    if-eqz p3, :cond_0

    .line 168
    const-string v0, "pending_requests"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhkh;

    iput-object v0, p0, Lhke;->c:Lhkh;

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    new-instance v0, Lhkh;

    invoke-direct {v0}, Lhkh;-><init>()V

    iput-object v0, p0, Lhke;->c:Lhkh;

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lhke;->a:Lhkp;

    invoke-virtual {v0}, Lhkp;->a()I

    move-result v0

    .line 128
    iget-object v1, p0, Lhke;->d:Lhkj;

    invoke-virtual {v1, v0, p1}, Lhkj;->a(ILandroid/content/Intent;)V

    .line 129
    return-void
.end method

.method public a(Lhkb;)Z
    .locals 4

    .prologue
    .line 199
    iget-object v0, p0, Lhke;->c:Lhkh;

    invoke-virtual {v0}, Lhkh;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 200
    iget-object v2, p0, Lhke;->c:Lhkh;

    invoke-virtual {v2, v0}, Lhkh;->a(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v2

    .line 201
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget v3, p1, Lhkb;->a:I

    if-ne v2, v3, :cond_0

    .line 202
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0, p1}, Lhke;->a(ILhkb;)V

    .line 203
    const/4 v0, 0x1

    .line 206
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 5

    .prologue
    .line 176
    iget-object v0, p0, Lhke;->c:Lhkh;

    invoke-virtual {v0}, Lhkh;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 177
    iget-object v1, p0, Lhke;->c:Lhkh;

    invoke-virtual {v1, v0}, Lhkh;->a(Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v1

    .line 178
    iget-object v3, p0, Lhke;->d:Lhkj;

    .line 179
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1}, Lhkj;->a(I)Ljava/util/List;

    move-result-object v1

    .line 183
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 184
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhkb;

    .line 185
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {p0, v4, v1}, Lhke;->a(ILhkb;)V

    goto :goto_0

    .line 189
    :cond_1
    iget-object v0, p0, Lhke;->d:Lhkj;

    invoke-virtual {v0, p0}, Lhkj;->a(Lhkk;)V

    .line 190
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 226
    const-string v0, "pending_requests"

    iget-object v1, p0, Lhke;->c:Lhkh;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 227
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lhke;->d:Lhkj;

    invoke-virtual {v0, p0}, Lhkj;->b(Lhkk;)V

    .line 222
    return-void
.end method
