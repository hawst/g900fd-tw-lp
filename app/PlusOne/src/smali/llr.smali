.class final Lllr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[B


# instance fields
.field private b:[B

.field private transient c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lllr;->a:[B

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Lllr;->a:[B

    iput-object v0, p0, Lllr;->b:[B

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lllr;->c:I

    .line 38
    return-void
.end method

.method private constructor <init>([B)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-string v0, "map cannot be null"

    invoke-static {p1, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lllr;->b:[B

    .line 42
    invoke-static {p1}, Lllr;->b([B)I

    move-result v0

    iput v0, p0, Lllr;->c:I

    .line 43
    return-void
.end method

.method public static a()Lllr;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lllr;

    invoke-direct {v0}, Lllr;-><init>()V

    return-object v0
.end method

.method public static a([B)Lllr;
    .locals 1

    .prologue
    .line 24
    array-length v0, p0

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lllr;

    invoke-direct {v0}, Lllr;-><init>()V

    .line 27
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lllr;

    invoke-direct {v0, p0}, Lllr;-><init>([B)V

    goto :goto_0
.end method

.method static b([B)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 149
    const-string v1, "array cannot be null"

    invoke-static {p0, v1}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-byte v3, p0, v0

    .line 152
    and-int/lit16 v3, v3, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->bitCount(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_0
    return v1
.end method


# virtual methods
.method public a(IZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    if-eqz p2, :cond_3

    .line 54
    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "bit index must be non-negative"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lllr;->b:[B

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x3

    if-lt p1, v0, :cond_1

    div-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    iget-object v1, p0, Lllr;->b:[B

    iget-object v2, p0, Lllr;->b:[B

    array-length v2, v2

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v0, p0, Lllr;->b:[B

    :cond_1
    div-int/lit8 v0, p1, 0x8

    rem-int/lit8 v1, p1, 0x8

    shl-int v1, v4, v1

    int-to-byte v1, v1

    iget-object v2, p0, Lllr;->b:[B

    aget-byte v2, v2, v0

    and-int/2addr v2, v1

    if-nez v2, :cond_2

    iget-object v2, p0, Lllr;->b:[B

    aget-byte v3, v2, v0

    or-int/2addr v1, v3

    int-to-byte v1, v1

    aput-byte v1, v2, v0

    iget v0, p0, Lllr;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lllr;->c:I

    .line 58
    :cond_2
    :goto_0
    return-void

    .line 56
    :cond_3
    if-gez p1, :cond_4

    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "bit index must be non-negative"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    div-int/lit8 v0, p1, 0x8

    iget-object v1, p0, Lllr;->b:[B

    array-length v1, v1

    if-ge v0, v1, :cond_2

    rem-int/lit8 v1, p1, 0x8

    shl-int v1, v4, v1

    int-to-byte v1, v1

    iget-object v2, p0, Lllr;->b:[B

    aget-byte v2, v2, v0

    and-int/2addr v2, v1

    if-eqz v2, :cond_2

    iget-object v2, p0, Lllr;->b:[B

    aget-byte v3, v2, v0

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v1, v3

    int-to-byte v1, v1

    aput-byte v1, v2, v0

    iget v0, p0, Lllr;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lllr;->c:I

    goto :goto_0
.end method

.method public a(I)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 66
    if-gez p1, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "bit index must be non-negative"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    div-int/lit8 v2, p1, 0x8

    .line 70
    iget-object v3, p0, Lllr;->b:[B

    array-length v3, v3

    if-lt v2, v3, :cond_2

    .line 75
    :cond_1
    :goto_0
    return v0

    .line 73
    :cond_2
    rem-int/lit8 v3, p1, 0x8

    .line 74
    shl-int v3, v1, v3

    int-to-byte v3, v3

    .line 75
    iget-object v4, p0, Lllr;->b:[B

    aget-byte v2, v4, v2

    and-int/2addr v2, v3

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public b()[B
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lllr;->b:[B

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lllr;->c:I

    return v0
.end method
