.class public final Lmvk;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;

.field private e:I

.field private f:Ljava/lang/Boolean;

.field private g:[I

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10829
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10874
    const/high16 v0, -0x80000000

    iput v0, p0, Lmvk;->e:I

    .line 10879
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmvk;->g:[I

    .line 10829
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 10923
    .line 10924
    iget-object v0, p0, Lmvk;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 10925
    const/4 v0, 0x1

    iget-object v2, p0, Lmvk;->a:Ljava/lang/Integer;

    .line 10926
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10928
    :goto_0
    iget-object v2, p0, Lmvk;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 10929
    const/4 v2, 0x2

    iget-object v3, p0, Lmvk;->b:Ljava/lang/Integer;

    .line 10930
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 10932
    :cond_0
    iget-object v2, p0, Lmvk;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 10933
    const/4 v2, 0x3

    iget-object v3, p0, Lmvk;->c:Ljava/lang/Boolean;

    .line 10934
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 10936
    :cond_1
    iget-object v2, p0, Lmvk;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 10937
    const/4 v2, 0x4

    iget-object v3, p0, Lmvk;->d:Ljava/lang/Boolean;

    .line 10938
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 10940
    :cond_2
    iget v2, p0, Lmvk;->e:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    .line 10941
    const/4 v2, 0x5

    iget v3, p0, Lmvk;->e:I

    .line 10942
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 10944
    :cond_3
    iget-object v2, p0, Lmvk;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 10945
    const/4 v2, 0x6

    iget-object v3, p0, Lmvk;->f:Ljava/lang/Boolean;

    .line 10946
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 10948
    :cond_4
    iget-object v2, p0, Lmvk;->g:[I

    if-eqz v2, :cond_6

    iget-object v2, p0, Lmvk;->g:[I

    array-length v2, v2

    if-lez v2, :cond_6

    .line 10950
    iget-object v3, p0, Lmvk;->g:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_5

    aget v5, v3, v1

    .line 10952
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 10950
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 10954
    :cond_5
    add-int/2addr v0, v2

    .line 10955
    iget-object v1, p0, Lmvk;->g:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10957
    :cond_6
    iget-object v1, p0, Lmvk;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 10958
    const/16 v1, 0x8

    iget-object v2, p0, Lmvk;->h:Ljava/lang/Integer;

    .line 10959
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10961
    :cond_7
    iget-object v1, p0, Lmvk;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 10962
    const/16 v1, 0x9

    iget-object v2, p0, Lmvk;->i:Ljava/lang/Boolean;

    .line 10963
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 10965
    :cond_8
    iget-object v1, p0, Lmvk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10966
    iput v0, p0, Lmvk;->ai:I

    .line 10967
    return v0

    :cond_9
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmvk;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 10975
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10976
    sparse-switch v0, :sswitch_data_0

    .line 10980
    iget-object v1, p0, Lmvk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10981
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmvk;->ah:Ljava/util/List;

    .line 10984
    :cond_1
    iget-object v1, p0, Lmvk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10986
    :sswitch_0
    return-object p0

    .line 10991
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvk;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 10995
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvk;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 10999
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmvk;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 11003
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmvk;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 11007
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 11008
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-ne v0, v1, :cond_3

    .line 11030
    :cond_2
    iput v0, p0, Lmvk;->e:I

    goto/16 :goto_0

    .line 11032
    :cond_3
    iput v3, p0, Lmvk;->e:I

    goto/16 :goto_0

    .line 11037
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmvk;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 11041
    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 11042
    iget-object v0, p0, Lmvk;->g:[I

    array-length v0, v0

    .line 11043
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 11044
    iget-object v2, p0, Lmvk;->g:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 11045
    iput-object v1, p0, Lmvk;->g:[I

    .line 11046
    :goto_1
    iget-object v1, p0, Lmvk;->g:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 11047
    iget-object v1, p0, Lmvk;->g:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 11048
    invoke-virtual {p1}, Loxn;->a()I

    .line 11046
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 11051
    :cond_4
    iget-object v1, p0, Lmvk;->g:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 11055
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmvk;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 11059
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmvk;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 10976
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 10888
    iget-object v0, p0, Lmvk;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 10889
    const/4 v0, 0x1

    iget-object v1, p0, Lmvk;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10891
    :cond_0
    iget-object v0, p0, Lmvk;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 10892
    const/4 v0, 0x2

    iget-object v1, p0, Lmvk;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10894
    :cond_1
    iget-object v0, p0, Lmvk;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 10895
    const/4 v0, 0x3

    iget-object v1, p0, Lmvk;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 10897
    :cond_2
    iget-object v0, p0, Lmvk;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 10898
    const/4 v0, 0x4

    iget-object v1, p0, Lmvk;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 10900
    :cond_3
    iget v0, p0, Lmvk;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 10901
    const/4 v0, 0x5

    iget v1, p0, Lmvk;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10903
    :cond_4
    iget-object v0, p0, Lmvk;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 10904
    const/4 v0, 0x6

    iget-object v1, p0, Lmvk;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 10906
    :cond_5
    iget-object v0, p0, Lmvk;->g:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lmvk;->g:[I

    array-length v0, v0

    if-lez v0, :cond_6

    .line 10907
    iget-object v1, p0, Lmvk;->g:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget v3, v1, v0

    .line 10908
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 10907
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10911
    :cond_6
    iget-object v0, p0, Lmvk;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 10912
    const/16 v0, 0x8

    iget-object v1, p0, Lmvk;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10914
    :cond_7
    iget-object v0, p0, Lmvk;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 10915
    const/16 v0, 0x9

    iget-object v1, p0, Lmvk;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 10917
    :cond_8
    iget-object v0, p0, Lmvk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10919
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10825
    invoke-virtual {p0, p1}, Lmvk;->a(Loxn;)Lmvk;

    move-result-object v0

    return-object v0
.end method
