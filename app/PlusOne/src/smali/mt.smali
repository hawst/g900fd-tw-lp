.class final Lmt;
.super Lgw;
.source "PG"


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private synthetic b:Lms;


# direct methods
.method constructor <init>(Lms;)V
    .locals 1

    .prologue
    .line 1799
    iput-object p1, p0, Lmt;->b:Lms;

    invoke-direct {p0}, Lgw;-><init>()V

    .line 1800
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lmt;->a:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Llh;)V
    .locals 4

    .prologue
    .line 1804
    invoke-static {}, Lms;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1805
    invoke-super {p0, p1, p2}, Lgw;->a(Landroid/view/View;Llh;)V

    .line 1824
    :cond_0
    const-class v0, Lms;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Llh;->b(Ljava/lang/CharSequence;)V

    .line 1825
    return-void

    .line 1809
    :cond_1
    invoke-static {p2}, Llh;->a(Llh;)Llh;

    move-result-object v1

    .line 1811
    invoke-super {p0, p1, v1}, Lgw;->a(Landroid/view/View;Llh;)V

    .line 1813
    invoke-virtual {p2, p1}, Llh;->a(Landroid/view/View;)V

    .line 1814
    invoke-static {p1}, Liu;->f(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    .line 1815
    instance-of v2, v0, Landroid/view/View;

    if-eqz v2, :cond_2

    .line 1816
    check-cast v0, Landroid/view/View;

    invoke-virtual {p2, v0}, Llh;->c(Landroid/view/View;)V

    .line 1818
    :cond_2
    iget-object v0, p0, Lmt;->a:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Llh;->a(Landroid/graphics/Rect;)V

    invoke-virtual {p2, v0}, Llh;->b(Landroid/graphics/Rect;)V

    invoke-virtual {v1, v0}, Llh;->c(Landroid/graphics/Rect;)V

    invoke-virtual {p2, v0}, Llh;->d(Landroid/graphics/Rect;)V

    invoke-virtual {v1}, Llh;->g()Z

    move-result v0

    invoke-virtual {p2, v0}, Llh;->c(Z)V

    invoke-virtual {v1}, Llh;->o()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Llh;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Llh;->p()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Llh;->b(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Llh;->r()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Llh;->c(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Llh;->l()Z

    move-result v0

    invoke-virtual {p2, v0}, Llh;->h(Z)V

    invoke-virtual {v1}, Llh;->j()Z

    move-result v0

    invoke-virtual {p2, v0}, Llh;->f(Z)V

    invoke-virtual {v1}, Llh;->e()Z

    move-result v0

    invoke-virtual {p2, v0}, Llh;->a(Z)V

    invoke-virtual {v1}, Llh;->f()Z

    move-result v0

    invoke-virtual {p2, v0}, Llh;->b(Z)V

    invoke-virtual {v1}, Llh;->h()Z

    move-result v0

    invoke-virtual {p2, v0}, Llh;->d(Z)V

    invoke-virtual {v1}, Llh;->i()Z

    move-result v0

    invoke-virtual {p2, v0}, Llh;->e(Z)V

    invoke-virtual {v1}, Llh;->k()Z

    move-result v0

    invoke-virtual {p2, v0}, Llh;->g(Z)V

    invoke-virtual {v1}, Llh;->b()I

    move-result v0

    invoke-virtual {p2, v0}, Llh;->a(I)V

    .line 1819
    invoke-virtual {v1}, Llh;->s()V

    .line 1821
    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lms;->l(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p2, v2}, Llh;->b(Landroid/view/View;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1861
    invoke-static {}, Lms;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lms;->l(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1862
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lgw;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 1864
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 3

    .prologue
    .line 1841
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_1

    .line 1842
    invoke-virtual {p2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    .line 1843
    iget-object v1, p0, Lmt;->b:Lms;

    invoke-static {v1}, Lms;->a(Lms;)Landroid/view/View;

    move-result-object v1

    .line 1844
    if-eqz v1, :cond_0

    .line 1845
    iget-object v2, p0, Lmt;->b:Lms;

    invoke-virtual {v2, v1}, Lms;->e(Landroid/view/View;)I

    move-result v1

    .line 1846
    iget-object v2, p0, Lmt;->b:Lms;

    invoke-virtual {v2, v1}, Lms;->b(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1847
    if-eqz v1, :cond_0

    .line 1848
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1852
    :cond_0
    const/4 v0, 0x1

    .line 1855
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Lgw;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 1829
    invoke-super {p0, p1, p2}, Lgw;->d(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1831
    const-class v0, Lms;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1832
    return-void
.end method
