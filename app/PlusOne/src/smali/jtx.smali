.class public final Ljtx;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljtz;

.field private final e:Lhlf;


# direct methods
.method protected constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljtz;)V
    .locals 1

    .prologue
    .line 45
    const-string v0, "CopyPhotosToAlbumTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 46
    iput p2, p0, Ljtx;->a:I

    .line 47
    iput-object p3, p0, Ljtx;->b:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Ljtx;->c:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Ljtx;->d:Ljtz;

    .line 50
    const-class v0, Lhlf;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlf;

    iput-object v0, p0, Ljtx;->e:Lhlf;

    .line 51
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;Ljtz;)Ljtx;
    .locals 6

    .prologue
    .line 32
    new-instance v0, Ljtx;

    const/4 v3, 0x0

    move-object v1, p0

    move v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ljtx;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljtz;)V

    return-object v0
.end method

.method public static b(Landroid/content/Context;ILjava/lang/String;Ljtz;)Ljtx;
    .locals 6

    .prologue
    .line 39
    new-instance v0, Ljtx;

    const/4 v4, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ljtx;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljtz;)V

    return-object v0
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 14

    .prologue
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v9, 0x0

    .line 55
    invoke-virtual {p0}, Ljtx;->f()Landroid/content/Context;

    move-result-object v1

    .line 56
    const-class v0, Lkfd;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lkfd;

    .line 58
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 59
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 62
    iget-object v0, p0, Ljtx;->d:Ljtz;

    invoke-virtual {p0}, Ljtx;->f()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2}, Ljtz;->a(Landroid/content/Context;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljua;

    .line 63
    invoke-interface {v0}, Ljua;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    invoke-interface {v0}, Ljua;->a()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 66
    :cond_0
    invoke-interface {v0}, Ljua;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 70
    :cond_1
    invoke-virtual {p0}, Ljtx;->d()Z

    move-result v0

    if-nez v0, :cond_2

    move v7, v8

    .line 72
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 74
    new-instance v0, Ljty;

    iget v2, p0, Ljtx;->a:I

    iget-object v3, p0, Ljtx;->b:Ljava/lang/String;

    iget-object v4, p0, Ljtx;->c:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ljty;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/util/Collection;)V

    .line 77
    invoke-interface {v6, v0}, Lkfd;->a(Lkff;)V

    .line 79
    invoke-virtual {v0}, Ljty;->t()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 80
    new-instance v1, Lhoz;

    iget v0, v0, Lkff;->i:I

    invoke-direct {v1, v0, v11, v11}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    .line 98
    :goto_2
    return-object v0

    :cond_2
    move v7, v9

    .line 70
    goto :goto_1

    .line 82
    :cond_3
    invoke-virtual {v0}, Ljty;->i()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 83
    invoke-virtual {v0}, Ljty;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljtx;->b:Ljava/lang/String;

    move v0, v1

    .line 87
    :goto_3
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 89
    iget-object v1, p0, Ljtx;->e:Lhlf;

    iget v2, p0, Ljtx;->a:I

    iget-object v3, p0, Ljtx;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v10, v11}, Lhlf;->a(ILjava/lang/String;Ljava/util/Collection;Lhle;)J

    .line 90
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 94
    new-instance v0, Lhoz;

    invoke-direct {v0, v9}, Lhoz;-><init>(Z)V

    goto :goto_2

    .line 96
    :cond_5
    new-instance v1, Lhoz;

    invoke-direct {v1, v8}, Lhoz;-><init>(Z)V

    .line 97
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Ljtx;->b:Ljava/lang/String;

    const-string v4, "is_new_album"

    invoke-virtual {v2, v4, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "num_photos_added"

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "album_id"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 98
    goto :goto_2

    :cond_6
    move v0, v9

    goto :goto_3
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Ljtx;->f()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Ljtx;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a02db

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f0a02dc

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Ljtx;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
