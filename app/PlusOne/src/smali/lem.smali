.class public final Llem;
.super Llch;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Llda;


# instance fields
.field private c:Llcu;

.field private d:Lide;

.field private e:Llcj;

.field private f:Landroid/widget/TextView;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Llem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Llem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Llch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Llcj;

    invoke-direct {v0, p1, p2, p3}, Llcj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Llem;->e:Llcj;

    .line 49
    const/16 v0, 0xb

    invoke-static {p1, p2, p3, v0}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Llem;->f:Landroid/widget/TextView;

    .line 51
    iget-object v0, p0, Llem;->f:Landroid/widget/TextView;

    const v1, 0x7f0a0486

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 52
    iget-object v0, p0, Llem;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Llem;->f:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020416

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 55
    iget-object v0, p0, Llem;->f:Landroid/widget/TextView;

    sget-object v1, Llem;->z:Llct;

    iget v1, v1, Llct;->m:I

    sget-object v2, Llem;->z:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Llem;->z:Llct;

    iget v3, v3, Llct;->m:I

    sget-object v4, Llem;->z:Llct;

    iget v4, v4, Llct;->m:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 57
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Llem;->setClickable(Z)V

    .line 59
    new-instance v0, Llcu;

    invoke-direct {v0, p0}, Llcu;-><init>(Llda;)V

    iput-object v0, p0, Llem;->c:Llcu;

    .line 60
    return-void
.end method


# virtual methods
.method protected a(IIII)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 111
    iget v1, p0, Llem;->t:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 114
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 116
    iget-object v1, p0, Llem;->e:Llcj;

    invoke-virtual {v1, v2, v3}, Llcj;->measure(II)V

    .line 117
    iget-object v1, p0, Llem;->e:Llcj;

    invoke-virtual {v1}, Llcj;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, p2

    .line 119
    :goto_0
    iget v4, p0, Llem;->g:I

    if-ge v0, v4, :cond_0

    .line 120
    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0, v4}, Llem;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 121
    invoke-virtual {v4, v2, v3}, Landroid/view/View;->measure(II)V

    .line 122
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v1, v4

    .line 119
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    :cond_0
    iget-object v0, p0, Llem;->f:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Llem;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->measure(II)V

    .line 127
    iget-object v0, p0, Llem;->f:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v1, v0

    .line 130
    :cond_1
    return v1
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 1

    .prologue
    .line 154
    invoke-virtual {p0}, Llem;->getHeight()I

    move-result v0

    add-int/2addr v0, p6

    return v0
.end method

.method public a(IZ)Landroid/view/View;
    .locals 4

    .prologue
    .line 95
    new-instance v0, Llel;

    .line 96
    invoke-virtual {p0}, Llem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Llel;-><init>(Landroid/content/Context;)V

    .line 97
    iget-object v1, p0, Llem;->a:Llci;

    iget-object v2, p0, Llem;->d:Lide;

    .line 98
    invoke-virtual {v2, p1}, Lide;->a(I)Lidf;

    move-result-object v2

    iget-object v3, p0, Llem;->r:Ljava/lang/String;

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Llel;->a(Llci;Lidf;Ljava/lang/String;)V

    .line 100
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 159
    invoke-super {p0}, Llch;->a()V

    .line 161
    invoke-virtual {p0}, Llem;->m()V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Llem;->d:Lide;

    .line 164
    const/4 v0, 0x0

    iput v0, p0, Llem;->g:I

    .line 165
    iget-object v0, p0, Llem;->c:Llcu;

    invoke-virtual {v0}, Llcu;->a()V

    .line 166
    return-void
.end method

.method protected a(Landroid/database/Cursor;Llcr;I)V
    .locals 19

    .prologue
    .line 64
    invoke-super/range {p0 .. p3}, Llch;->a(Landroid/database/Cursor;Llcr;I)V

    .line 66
    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 67
    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iput-object v2, v0, Llem;->d:Lide;

    .line 69
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v3, v0, Llem;->d:Lide;

    .line 70
    invoke-virtual {v3}, Lide;->b()I

    move-result v3

    .line 69
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Llem;->g:I

    .line 72
    move-object/from16 v0, p0

    iget-object v2, v0, Llem;->c:Llcu;

    move-object/from16 v0, p0

    iget-object v3, v0, Llem;->d:Lide;

    invoke-virtual {v3}, Lide;->a()Ljava/util/ArrayList;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Llem;->g:I

    invoke-virtual {v2, v3, v4}, Llcu;->a(Ljava/util/List;I)V

    .line 73
    return-void

    .line 67
    :cond_0
    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v16

    new-instance v14, Lide;

    invoke-direct {v14}, Lide;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v17

    new-instance v2, Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, v14, Lide;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    move v15, v2

    :goto_1
    move/from16 v0, v17

    if-ge v15, v0, :cond_3

    iget-object v0, v14, Lide;->a:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-static/range {v16 .. v16}, Lidf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    invoke-static/range {v16 .. v16}, Lidf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v2, v6, v8

    if-nez v2, :cond_1

    const/4 v5, 0x0

    :goto_2
    invoke-static/range {v16 .. v16}, Lidf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v16 .. v16}, Lidf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v16 .. v16}, Lidf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v16 .. v16}, Lidf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v16 .. v16}, Lidf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v11

    invoke-static/range {v16 .. v16}, Lidf;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v16 .. v16}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    const/4 v13, 0x1

    if-ne v2, v13, :cond_2

    const/4 v13, 0x1

    :goto_3
    new-instance v2, Lidf;

    invoke-direct/range {v2 .. v13}, Lidf;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto :goto_1

    :cond_1
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    goto :goto_2

    :cond_2
    const/4 v13, 0x0

    goto :goto_3

    :cond_3
    move-object v2, v14

    goto/16 :goto_0
.end method

.method protected a(ZIIII)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 135
    invoke-super/range {p0 .. p5}, Llch;->a(ZIIII)V

    .line 137
    iget-object v1, p0, Llem;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 138
    iget v2, p0, Llem;->t:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 140
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 141
    invoke-virtual {p0}, Llem;->getChildCount()I

    move-result v4

    :goto_0
    if-ge v0, v4, :cond_0

    .line 142
    invoke-virtual {p0, v0}, Llem;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 143
    invoke-virtual {v5, v2, v3}, Landroid/view/View;->measure(II)V

    .line 144
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 145
    iget-object v7, p0, Llem;->q:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Llem;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget v9, p0, Llem;->t:I

    add-int/2addr v8, v9

    add-int v9, v1, v6

    invoke-virtual {v5, v7, v1, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 147
    add-int/2addr v1, v6

    .line 141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    :cond_0
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    .line 77
    iget-object v0, p0, Llem;->e:Llcj;

    sget-object v1, Llem;->z:Llct;

    iget v1, v1, Llct;->at:I

    .line 78
    invoke-virtual {p0}, Llem;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0495

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Llem;->z:Llct;

    iget-object v3, v3, Llct;->au:Landroid/graphics/Bitmap;

    .line 77
    invoke-virtual {v0, v1, v2, v3}, Llcj;->a(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 80
    iget-object v0, p0, Llem;->e:Llcj;

    invoke-virtual {p0, v0}, Llem;->addView(Landroid/view/View;)V

    .line 81
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Llem;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Llem;->addView(Landroid/view/View;)V

    .line 86
    return-void
.end method

.method public h()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Llem;->g:I

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Llem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llel;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public j()Llcu;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Llem;->c:Llcu;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Llem;->f:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Llem;->a:Llci;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Llem;->a:Llci;

    invoke-interface {v0}, Llci;->aF()V

    .line 173
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Llem;->c:Llcu;

    invoke-virtual {v0, p1}, Llcu;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Llem;->c:Llcu;

    invoke-virtual {v0, p1}, Llcu;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
