.class public final Ldoc;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmko;",
        "Lmkp;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 21
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x10

    aput v2, v0, v1

    sput-object v0, Ldoc;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 29
    const-string v3, "collectionupdate"

    new-instance v4, Lmko;

    invoke-direct {v4}, Lmko;-><init>()V

    new-instance v5, Lmkp;

    invoke-direct {v5}, Lmkp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 32
    iput-object p3, p0, Ldoc;->b:Ljava/lang/String;

    .line 33
    iput-object p4, p0, Ldoc;->p:Ljava/lang/String;

    .line 34
    iput-boolean p5, p0, Ldoc;->q:Z

    .line 35
    return-void
.end method


# virtual methods
.method protected a(Lmko;)V
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lnai;

    invoke-direct {v0}, Lnai;-><init>()V

    iput-object v0, p1, Lmko;->a:Lnai;

    .line 40
    iget-object v0, p1, Lmko;->a:Lnai;

    .line 42
    iget-object v1, p0, Ldoc;->b:Ljava/lang/String;

    iget-object v2, p0, Ldoc;->p:Ljava/lang/String;

    invoke-static {v1, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;)Lmzo;

    move-result-object v1

    iput-object v1, v0, Lnai;->a:Lmzo;

    .line 43
    sget-object v1, Ldoc;->a:[I

    iput-object v1, v0, Lnai;->b:[I

    .line 44
    new-instance v1, Lnam;

    invoke-direct {v1}, Lnam;-><init>()V

    iput-object v1, v0, Lnai;->g:Lnam;

    .line 45
    iget-object v0, v0, Lnai;->g:Lnam;

    iget-boolean v1, p0, Ldoc;->q:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnam;->a:Ljava/lang/Boolean;

    .line 46
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmko;

    invoke-virtual {p0, p1}, Ldoc;->a(Lmko;)V

    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 18
    return-void
.end method
