.class public final Lgag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/apps/plus/views/PhotoView;

.field private b:Z

.field private c:F

.field private d:F

.field private e:F

.field private f:J

.field private g:Z

.field private h:Z

.field private i:J

.field private j:Z

.field private synthetic k:Lcom/google/android/apps/plus/views/PhotoView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/PhotoView;Lcom/google/android/apps/plus/views/PhotoView;)V
    .locals 1

    .prologue
    .line 1769
    iput-object p1, p0, Lgag;->k:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1767
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgag;->j:Z

    .line 1770
    iput-object p2, p0, Lgag;->a:Lcom/google/android/apps/plus/views/PhotoView;

    .line 1771
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 1805
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lgag;->a(Z)V

    .line 1806
    return-void
.end method

.method public a(Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 1813
    iget-wide v0, p0, Lgag;->i:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    if-nez p1, :cond_0

    .line 1822
    :goto_0
    return-void

    .line 1816
    :cond_0
    iget-object v0, p0, Lgag;->a:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1817
    iput-boolean v2, p0, Lgag;->g:Z

    .line 1818
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgag;->h:Z

    .line 1819
    iput-boolean v2, p0, Lgag;->j:Z

    .line 1820
    iput-wide v4, p0, Lgag;->i:J

    .line 1821
    iget-object v0, p0, Lgag;->k:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/PhotoView;->a(Lcom/google/android/apps/plus/views/PhotoView;)V

    goto :goto_0
.end method

.method public a(FF)Z
    .locals 2

    .prologue
    .line 1798
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lgag;->a(FFJ)Z

    move-result v0

    return v0
.end method

.method public a(FFJ)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1777
    iget-boolean v3, p0, Lgag;->g:Z

    .line 1778
    iget-boolean v0, p0, Lgag;->g:Z

    if-eqz v0, :cond_1

    iget-wide v4, p0, Lgag;->i:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgag;->j:Z

    if-eqz v0, :cond_1

    .line 1794
    :cond_0
    :goto_0
    return v2

    .line 1783
    :cond_1
    iput p2, p0, Lgag;->c:F

    .line 1784
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lgag;->f:J

    .line 1785
    iput p1, p0, Lgag;->d:F

    .line 1786
    iget v0, p0, Lgag;->c:F

    iget v4, p0, Lgag;->d:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lgag;->b:Z

    .line 1787
    iget v0, p0, Lgag;->c:F

    iget v4, p0, Lgag;->d:F

    sub-float/2addr v0, v4

    const/high16 v4, 0x437a0000    # 250.0f

    div-float/2addr v0, v4

    iput v0, p0, Lgag;->e:F

    .line 1788
    iput-boolean v1, p0, Lgag;->g:Z

    .line 1789
    iput-boolean v2, p0, Lgag;->h:Z

    .line 1790
    iput-wide p3, p0, Lgag;->i:J

    .line 1791
    if-nez v3, :cond_2

    .line 1792
    iget-object v0, p0, Lgag;->a:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, p0, p3, p4}, Lcom/google/android/apps/plus/views/PhotoView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    move v2, v1

    .line 1794
    goto :goto_0

    :cond_3
    move v0, v2

    .line 1786
    goto :goto_1
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1826
    iget-boolean v0, p0, Lgag;->h:Z

    if-eqz v0, :cond_1

    .line 1860
    :cond_0
    :goto_0
    return-void

    .line 1832
    :cond_1
    iget-boolean v0, p0, Lgag;->b:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lgag;->k:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/PhotoView;->b(Lcom/google/android/apps/plus/views/PhotoView;)F

    move-result v0

    iget v1, p0, Lgag;->c:F

    cmpg-float v0, v0, v1

    if-lez v0, :cond_3

    :cond_2
    iget-boolean v0, p0, Lgag;->b:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lgag;->k:Lcom/google/android/apps/plus/views/PhotoView;

    .line 1833
    invoke-static {v0}, Lcom/google/android/apps/plus/views/PhotoView;->b(Lcom/google/android/apps/plus/views/PhotoView;)F

    move-result v0

    iget v1, p0, Lgag;->c:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_4

    .line 1834
    :cond_3
    invoke-virtual {p0, v4}, Lgag;->a(Z)V

    goto :goto_0

    .line 1838
    :cond_4
    iget-wide v0, p0, Lgag;->f:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 1839
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lgag;->f:J

    .line 1842
    :cond_5
    iput-boolean v4, p0, Lgag;->j:Z

    .line 1845
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 1846
    iget-wide v2, p0, Lgag;->f:J

    sub-long/2addr v0, v2

    .line 1847
    iget v2, p0, Lgag;->d:F

    iget v3, p0, Lgag;->e:F

    long-to-float v0, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    .line 1850
    iget-boolean v1, p0, Lgag;->b:Z

    if-nez v1, :cond_6

    iget v1, p0, Lgag;->c:F

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_7

    :cond_6
    iget-boolean v1, p0, Lgag;->b:Z

    if-eqz v1, :cond_8

    iget v1, p0, Lgag;->c:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_8

    .line 1852
    :cond_7
    iget v0, p0, Lgag;->c:F

    .line 1855
    :cond_8
    iget-object v1, p0, Lgag;->a:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v2, p0, Lgag;->k:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-static {v2}, Lcom/google/android/apps/plus/views/PhotoView;->c(Lcom/google/android/apps/plus/views/PhotoView;)F

    move-result v2

    iget-object v3, p0, Lgag;->k:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-static {v3}, Lcom/google/android/apps/plus/views/PhotoView;->d(Lcom/google/android/apps/plus/views/PhotoView;)F

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/plus/views/PhotoView;->b(FFF)V

    .line 1857
    iget-boolean v0, p0, Lgag;->h:Z

    if-nez v0, :cond_0

    .line 1858
    iget-object v0, p0, Lgag;->a:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
