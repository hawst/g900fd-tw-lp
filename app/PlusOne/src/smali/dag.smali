.class final Ldag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lldh;


# instance fields
.field private synthetic a:Ldac;


# direct methods
.method constructor <init>(Ldac;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Ldag;->a:Ldac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/text/style/URLSpan;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 345
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v4

    .line 346
    invoke-static {v4}, Ljpf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Ldag;->a:Ldac;

    invoke-static {v0}, Ldac;->g(Ldac;)Ldeo;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ldeo;->c(Z)V

    .line 348
    invoke-static {v4}, Ljpf;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 349
    const-string v2, "extra_gaia_id"

    .line 350
    invoke-static {v2, v0}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 351
    iget-object v2, p0, Ldag;->a:Ldac;

    invoke-static {v2}, Ldac;->k(Ldac;)Ldgi;

    move-result-object v2

    sget-object v3, Lhmv;->C:Lhmv;

    invoke-interface {v2, v3, v0}, Ldgi;->a(Lhmv;Landroid/os/Bundle;)V

    .line 353
    :cond_0
    iget-object v0, p0, Ldag;->a:Ldac;

    .line 354
    invoke-static {v0}, Ldac;->l(Ldac;)Llnh;

    move-result-object v0

    const-class v2, Lkzh;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfcu;

    move-object v2, v1

    move-object v3, v1

    move-object v5, v1

    .line 355
    invoke-virtual/range {v0 .. v5}, Lfcu;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkzs;)V

    .line 357
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;)V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Ldag;->a:Ldac;

    invoke-static {v0}, Ldac;->b(Ldac;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Ldag;->a:Ldac;

    invoke-static {v0}, Ldac;->j(Ldac;)Lczz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lczz;->a(Lcom/google/android/libraries/social/stream/legacy/views/OneUpCommentViewGroup;)V

    .line 328
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 332
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    :goto_0
    return-void

    .line 336
    :cond_0
    iget-object v0, p0, Ldag;->a:Ldac;

    invoke-static {v0}, Ldac;->i(Ldac;)Lhee;

    move-result-object v0

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 337
    const-string v2, "g:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 338
    :goto_1
    iget-object v2, p0, Ldag;->a:Ldac;

    invoke-virtual {v2}, Ldac;->n()Lz;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v1, v0, v3, v4}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 340
    iget-object v1, p0, Ldag;->a:Ldac;

    invoke-virtual {v1, v0}, Ldac;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 337
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
