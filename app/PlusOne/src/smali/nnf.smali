.class public final Lnnf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnnf;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 929
    const/4 v0, 0x0

    new-array v0, v0, [Lnnf;

    sput-object v0, Lnnf;->a:[Lnnf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 930
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 956
    const/4 v0, 0x0

    .line 957
    iget-object v1, p0, Lnnf;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 958
    const/4 v0, 0x1

    iget-object v1, p0, Lnnf;->b:Ljava/lang/String;

    .line 959
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 961
    :cond_0
    iget-object v1, p0, Lnnf;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 962
    const/4 v1, 0x2

    iget-object v2, p0, Lnnf;->c:Ljava/lang/String;

    .line 963
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 965
    :cond_1
    iget-object v1, p0, Lnnf;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 966
    const/4 v1, 0x3

    iget-object v2, p0, Lnnf;->d:Ljava/lang/String;

    .line 967
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 969
    :cond_2
    iget-object v1, p0, Lnnf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 970
    iput v0, p0, Lnnf;->ai:I

    .line 971
    return v0
.end method

.method public a(Loxn;)Lnnf;
    .locals 2

    .prologue
    .line 979
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 980
    sparse-switch v0, :sswitch_data_0

    .line 984
    iget-object v1, p0, Lnnf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 985
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnnf;->ah:Ljava/util/List;

    .line 988
    :cond_1
    iget-object v1, p0, Lnnf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 990
    :sswitch_0
    return-object p0

    .line 995
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnnf;->b:Ljava/lang/String;

    goto :goto_0

    .line 999
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnnf;->c:Ljava/lang/String;

    goto :goto_0

    .line 1003
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnnf;->d:Ljava/lang/String;

    goto :goto_0

    .line 980
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 941
    iget-object v0, p0, Lnnf;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 942
    const/4 v0, 0x1

    iget-object v1, p0, Lnnf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 944
    :cond_0
    iget-object v0, p0, Lnnf;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 945
    const/4 v0, 0x2

    iget-object v1, p0, Lnnf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 947
    :cond_1
    iget-object v0, p0, Lnnf;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 948
    const/4 v0, 0x3

    iget-object v1, p0, Lnnf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 950
    :cond_2
    iget-object v0, p0, Lnnf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 952
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 926
    invoke-virtual {p0, p1}, Lnnf;->a(Loxn;)Lnnf;

    move-result-object v0

    return-object v0
.end method
