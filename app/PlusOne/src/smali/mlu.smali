.class public final Lmlu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmok;

.field public b:Llwr;

.field public c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1369
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1372
    iput-object v0, p0, Lmlu;->a:Lmok;

    .line 1375
    iput-object v0, p0, Lmlu;->b:Llwr;

    .line 1369
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1397
    const/4 v0, 0x0

    .line 1398
    iget-object v1, p0, Lmlu;->a:Lmok;

    if-eqz v1, :cond_0

    .line 1399
    const/4 v0, 0x1

    iget-object v1, p0, Lmlu;->a:Lmok;

    .line 1400
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1402
    :cond_0
    iget-object v1, p0, Lmlu;->b:Llwr;

    if-eqz v1, :cond_1

    .line 1403
    const/4 v1, 0x2

    iget-object v2, p0, Lmlu;->b:Llwr;

    .line 1404
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1406
    :cond_1
    iget-object v1, p0, Lmlu;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1407
    const/4 v1, 0x3

    iget-object v2, p0, Lmlu;->c:Ljava/lang/Boolean;

    .line 1408
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1410
    :cond_2
    iget-object v1, p0, Lmlu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1411
    iput v0, p0, Lmlu;->ai:I

    .line 1412
    return v0
.end method

.method public a(Loxn;)Lmlu;
    .locals 2

    .prologue
    .line 1420
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1421
    sparse-switch v0, :sswitch_data_0

    .line 1425
    iget-object v1, p0, Lmlu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1426
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmlu;->ah:Ljava/util/List;

    .line 1429
    :cond_1
    iget-object v1, p0, Lmlu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1431
    :sswitch_0
    return-object p0

    .line 1436
    :sswitch_1
    iget-object v0, p0, Lmlu;->a:Lmok;

    if-nez v0, :cond_2

    .line 1437
    new-instance v0, Lmok;

    invoke-direct {v0}, Lmok;-><init>()V

    iput-object v0, p0, Lmlu;->a:Lmok;

    .line 1439
    :cond_2
    iget-object v0, p0, Lmlu;->a:Lmok;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1443
    :sswitch_2
    iget-object v0, p0, Lmlu;->b:Llwr;

    if-nez v0, :cond_3

    .line 1444
    new-instance v0, Llwr;

    invoke-direct {v0}, Llwr;-><init>()V

    iput-object v0, p0, Lmlu;->b:Llwr;

    .line 1446
    :cond_3
    iget-object v0, p0, Lmlu;->b:Llwr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1450
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmlu;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1421
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1382
    iget-object v0, p0, Lmlu;->a:Lmok;

    if-eqz v0, :cond_0

    .line 1383
    const/4 v0, 0x1

    iget-object v1, p0, Lmlu;->a:Lmok;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1385
    :cond_0
    iget-object v0, p0, Lmlu;->b:Llwr;

    if-eqz v0, :cond_1

    .line 1386
    const/4 v0, 0x2

    iget-object v1, p0, Lmlu;->b:Llwr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1388
    :cond_1
    iget-object v0, p0, Lmlu;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1389
    const/4 v0, 0x3

    iget-object v1, p0, Lmlu;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1391
    :cond_2
    iget-object v0, p0, Lmlu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1393
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1365
    invoke-virtual {p0, p1}, Lmlu;->a(Loxn;)Lmlu;

    move-result-object v0

    return-object v0
.end method
