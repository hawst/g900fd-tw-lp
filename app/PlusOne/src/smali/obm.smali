.class public final Lobm;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lodo;

.field private c:Lobn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 253
    invoke-direct {p0}, Loxq;-><init>()V

    .line 258
    iput-object v0, p0, Lobm;->b:Lodo;

    .line 261
    iput-object v0, p0, Lobm;->c:Lobn;

    .line 253
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 281
    const/4 v0, 0x0

    .line 282
    iget-object v1, p0, Lobm;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 283
    const/4 v0, 0x2

    iget-object v1, p0, Lobm;->a:Ljava/lang/String;

    .line 284
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 286
    :cond_0
    iget-object v1, p0, Lobm;->b:Lodo;

    if-eqz v1, :cond_1

    .line 287
    const/4 v1, 0x3

    iget-object v2, p0, Lobm;->b:Lodo;

    .line 288
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 290
    :cond_1
    iget-object v1, p0, Lobm;->c:Lobn;

    if-eqz v1, :cond_2

    .line 291
    const/4 v1, 0x4

    iget-object v2, p0, Lobm;->c:Lobn;

    .line 292
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    :cond_2
    iget-object v1, p0, Lobm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    iput v0, p0, Lobm;->ai:I

    .line 296
    return v0
.end method

.method public a(Loxn;)Lobm;
    .locals 2

    .prologue
    .line 304
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 305
    sparse-switch v0, :sswitch_data_0

    .line 309
    iget-object v1, p0, Lobm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 310
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lobm;->ah:Ljava/util/List;

    .line 313
    :cond_1
    iget-object v1, p0, Lobm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    :sswitch_0
    return-object p0

    .line 320
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lobm;->a:Ljava/lang/String;

    goto :goto_0

    .line 324
    :sswitch_2
    iget-object v0, p0, Lobm;->b:Lodo;

    if-nez v0, :cond_2

    .line 325
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobm;->b:Lodo;

    .line 327
    :cond_2
    iget-object v0, p0, Lobm;->b:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 331
    :sswitch_3
    iget-object v0, p0, Lobm;->c:Lobn;

    if-nez v0, :cond_3

    .line 332
    new-instance v0, Lobn;

    invoke-direct {v0}, Lobn;-><init>()V

    iput-object v0, p0, Lobm;->c:Lobn;

    .line 334
    :cond_3
    iget-object v0, p0, Lobm;->c:Lobn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 305
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lobm;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 267
    const/4 v0, 0x2

    iget-object v1, p0, Lobm;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 269
    :cond_0
    iget-object v0, p0, Lobm;->b:Lodo;

    if-eqz v0, :cond_1

    .line 270
    const/4 v0, 0x3

    iget-object v1, p0, Lobm;->b:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 272
    :cond_1
    iget-object v0, p0, Lobm;->c:Lobn;

    if-eqz v0, :cond_2

    .line 273
    const/4 v0, 0x4

    iget-object v1, p0, Lobm;->c:Lobn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 275
    :cond_2
    iget-object v0, p0, Lobm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 277
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0, p1}, Lobm;->a(Loxn;)Lobm;

    move-result-object v0

    return-object v0
.end method
