.class public final Lntk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1395
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1402
    const/high16 v0, -0x80000000

    iput v0, p0, Lntk;->c:I

    .line 1395
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1422
    const/4 v0, 0x0

    .line 1423
    iget-object v1, p0, Lntk;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1424
    const/4 v0, 0x1

    iget-object v1, p0, Lntk;->a:Ljava/lang/String;

    .line 1425
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1427
    :cond_0
    iget-object v1, p0, Lntk;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1428
    const/4 v1, 0x2

    iget-object v2, p0, Lntk;->b:Ljava/lang/String;

    .line 1429
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1431
    :cond_1
    iget v1, p0, Lntk;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 1432
    const/4 v1, 0x3

    iget v2, p0, Lntk;->c:I

    .line 1433
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1435
    :cond_2
    iget-object v1, p0, Lntk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1436
    iput v0, p0, Lntk;->ai:I

    .line 1437
    return v0
.end method

.method public a(Loxn;)Lntk;
    .locals 2

    .prologue
    .line 1445
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1446
    sparse-switch v0, :sswitch_data_0

    .line 1450
    iget-object v1, p0, Lntk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1451
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lntk;->ah:Ljava/util/List;

    .line 1454
    :cond_1
    iget-object v1, p0, Lntk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1456
    :sswitch_0
    return-object p0

    .line 1461
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lntk;->a:Ljava/lang/String;

    goto :goto_0

    .line 1465
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lntk;->b:Ljava/lang/String;

    goto :goto_0

    .line 1469
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1470
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1473
    :cond_2
    iput v0, p0, Lntk;->c:I

    goto :goto_0

    .line 1475
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lntk;->c:I

    goto :goto_0

    .line 1446
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1407
    iget-object v0, p0, Lntk;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1408
    const/4 v0, 0x1

    iget-object v1, p0, Lntk;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1410
    :cond_0
    iget-object v0, p0, Lntk;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1411
    const/4 v0, 0x2

    iget-object v1, p0, Lntk;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1413
    :cond_1
    iget v0, p0, Lntk;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 1414
    const/4 v0, 0x3

    iget v1, p0, Lntk;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1416
    :cond_2
    iget-object v0, p0, Lntk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1418
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1391
    invoke-virtual {p0, p1}, Lntk;->a(Loxn;)Lntk;

    move-result-object v0

    return-object v0
.end method
