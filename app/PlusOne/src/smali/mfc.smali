.class public final Lmfc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojp;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11743
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11746
    iput-object v0, p0, Lmfc;->apiHeader:Llyq;

    .line 11749
    iput-object v0, p0, Lmfc;->a:Lojp;

    .line 11743
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11766
    const/4 v0, 0x0

    .line 11767
    iget-object v1, p0, Lmfc;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 11768
    const/4 v0, 0x1

    iget-object v1, p0, Lmfc;->apiHeader:Llyq;

    .line 11769
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11771
    :cond_0
    iget-object v1, p0, Lmfc;->a:Lojp;

    if-eqz v1, :cond_1

    .line 11772
    const/4 v1, 0x2

    iget-object v2, p0, Lmfc;->a:Lojp;

    .line 11773
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11775
    :cond_1
    iget-object v1, p0, Lmfc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11776
    iput v0, p0, Lmfc;->ai:I

    .line 11777
    return v0
.end method

.method public a(Loxn;)Lmfc;
    .locals 2

    .prologue
    .line 11785
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11786
    sparse-switch v0, :sswitch_data_0

    .line 11790
    iget-object v1, p0, Lmfc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11791
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfc;->ah:Ljava/util/List;

    .line 11794
    :cond_1
    iget-object v1, p0, Lmfc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11796
    :sswitch_0
    return-object p0

    .line 11801
    :sswitch_1
    iget-object v0, p0, Lmfc;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 11802
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfc;->apiHeader:Llyq;

    .line 11804
    :cond_2
    iget-object v0, p0, Lmfc;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11808
    :sswitch_2
    iget-object v0, p0, Lmfc;->a:Lojp;

    if-nez v0, :cond_3

    .line 11809
    new-instance v0, Lojp;

    invoke-direct {v0}, Lojp;-><init>()V

    iput-object v0, p0, Lmfc;->a:Lojp;

    .line 11811
    :cond_3
    iget-object v0, p0, Lmfc;->a:Lojp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11786
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11754
    iget-object v0, p0, Lmfc;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 11755
    const/4 v0, 0x1

    iget-object v1, p0, Lmfc;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11757
    :cond_0
    iget-object v0, p0, Lmfc;->a:Lojp;

    if-eqz v0, :cond_1

    .line 11758
    const/4 v0, 0x2

    iget-object v1, p0, Lmfc;->a:Lojp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11760
    :cond_1
    iget-object v0, p0, Lmfc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11762
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11739
    invoke-virtual {p0, p1}, Lmfc;->a(Loxn;)Lmfc;

    move-result-object v0

    return-object v0
.end method
