.class public final Looi;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Looi;


# instance fields
.field public b:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    new-array v0, v0, [Looi;

    sput-object v0, Looi;->a:[Looi;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 192
    const/4 v0, 0x0

    .line 193
    iget-object v1, p0, Looi;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 194
    const/4 v0, 0x1

    iget-object v1, p0, Looi;->b:Ljava/lang/Integer;

    .line 195
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 197
    :cond_0
    iget-object v1, p0, Looi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    iput v0, p0, Looi;->ai:I

    .line 199
    return v0
.end method

.method public a(Loxn;)Looi;
    .locals 2

    .prologue
    .line 207
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 208
    sparse-switch v0, :sswitch_data_0

    .line 212
    iget-object v1, p0, Looi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 213
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Looi;->ah:Ljava/util/List;

    .line 216
    :cond_1
    iget-object v1, p0, Looi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    :sswitch_0
    return-object p0

    .line 223
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Looi;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 208
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Looi;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x1

    iget-object v1, p0, Looi;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 186
    :cond_0
    iget-object v0, p0, Looi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 188
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Looi;->a(Loxn;)Looi;

    move-result-object v0

    return-object v0
.end method
