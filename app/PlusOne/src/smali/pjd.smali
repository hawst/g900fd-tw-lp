.class public final Lpjd;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpsi;

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lpjd;->a:Lpsi;

    .line 22
    const/high16 v0, -0x80000000

    iput v0, p0, Lpjd;->b:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x0

    .line 40
    iget-object v1, p0, Lpjd;->a:Lpsi;

    if-eqz v1, :cond_0

    .line 41
    const/4 v0, 0x1

    iget-object v1, p0, Lpjd;->a:Lpsi;

    .line 42
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 44
    :cond_0
    iget v1, p0, Lpjd;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 45
    const/4 v1, 0x2

    iget v2, p0, Lpjd;->b:I

    .line 46
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 48
    :cond_1
    iget-object v1, p0, Lpjd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49
    iput v0, p0, Lpjd;->ai:I

    .line 50
    return v0
.end method

.method public a(Loxn;)Lpjd;
    .locals 2

    .prologue
    .line 58
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 59
    sparse-switch v0, :sswitch_data_0

    .line 63
    iget-object v1, p0, Lpjd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 64
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjd;->ah:Ljava/util/List;

    .line 67
    :cond_1
    iget-object v1, p0, Lpjd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    :sswitch_0
    return-object p0

    .line 74
    :sswitch_1
    iget-object v0, p0, Lpjd;->a:Lpsi;

    if-nez v0, :cond_2

    .line 75
    new-instance v0, Lpsi;

    invoke-direct {v0}, Lpsi;-><init>()V

    iput-object v0, p0, Lpjd;->a:Lpsi;

    .line 77
    :cond_2
    iget-object v0, p0, Lpjd;->a:Lpsi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 81
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 82
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 85
    :cond_3
    iput v0, p0, Lpjd;->b:I

    goto :goto_0

    .line 87
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lpjd;->b:I

    goto :goto_0

    .line 59
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lpjd;->a:Lpsi;

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-object v1, p0, Lpjd;->a:Lpsi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 30
    :cond_0
    iget v0, p0, Lpjd;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 31
    const/4 v0, 0x2

    iget v1, p0, Lpjd;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 33
    :cond_1
    iget-object v0, p0, Lpjd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 35
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpjd;->a(Loxn;)Lpjd;

    move-result-object v0

    return-object v0
.end method
