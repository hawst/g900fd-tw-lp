.class public final Ljrq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljob;
.implements Ljrl;


# instance fields
.field private final a:Ljrm;

.field private final b:Ljqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljqc",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljoy;

.field private d:Z

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljrm;Ljoy;Ljqc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljrm;",
            "Ljoy;",
            "Ljqc",
            "<",
            "Ljod;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Ljrq;->a:Ljrm;

    .line 24
    iput-object p3, p0, Ljrq;->b:Ljqc;

    .line 25
    iput-object p2, p0, Ljrq;->c:Ljoy;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljrq;->d:Z

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljrq;->e:Ljava/util/List;

    .line 28
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, Ljrq;->e:Ljava/util/List;

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ljrq;->a:Ljrm;

    invoke-virtual {v0, p0}, Ljrm;->b(Ljrl;)V

    .line 38
    return-void
.end method

.method public c()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljrq;->e:Ljava/util/List;

    iget-object v0, p0, Ljrq;->a:Ljrm;

    invoke-virtual {v0}, Ljrm;->a()Ljava/util/List;

    move-result-object v3

    move v1, v2

    :goto_0
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljod;

    iget-object v4, p0, Ljrq;->b:Ljqc;

    invoke-interface {v4, v0}, Ljqc;->a(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Ljrq;->e:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 59
    :cond_1
    iget-boolean v0, p0, Ljrq;->d:Z

    if-eqz v0, :cond_2

    .line 60
    iput-boolean v2, p0, Ljrq;->d:Z

    .line 61
    iget-object v0, p0, Ljrq;->c:Ljoy;

    invoke-interface {v0, p0}, Ljoy;->a(Ljob;)V

    .line 65
    :goto_1
    return-void

    .line 63
    :cond_2
    iget-object v0, p0, Ljrq;->c:Ljoy;

    invoke-interface {v0, p0}, Ljoy;->b(Ljob;)V

    goto :goto_1
.end method
