.class public final Lizj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llpw;
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field a:Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;

.field private final b:Landroid/app/Activity;

.field private final c:I

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:Landroid/widget/FrameLayout;

.field private g:Lhee;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;I)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lizj;->d:Ljava/util/List;

    .line 54
    iput-object p1, p0, Lizj;->b:Landroid/app/Activity;

    .line 55
    iput p3, p0, Lizj;->c:I

    .line 56
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 57
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lizj;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lizj;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    return-object p0
.end method

.method public a()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 104
    iget-object v0, p0, Lizj;->g:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 105
    if-ne v2, v6, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    iput-boolean v7, p0, Lizj;->e:Z

    .line 110
    iget-object v3, p0, Lizj;->b:Landroid/app/Activity;

    .line 112
    iget-object v0, p0, Lizj;->f:Landroid/widget/FrameLayout;

    if-nez v0, :cond_2

    .line 114
    iget-object v0, p0, Lizj;->b:Landroid/app/Activity;

    iget v1, p0, Lizj;->c:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 119
    instance-of v1, v0, Landroid/widget/FrameLayout;

    if-eqz v1, :cond_4

    .line 120
    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lizj;->f:Landroid/widget/FrameLayout;

    .line 132
    :cond_2
    :goto_1
    iget-object v0, p0, Lizj;->a:Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;

    if-nez v0, :cond_3

    .line 134
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 135
    const v1, 0x7f040079

    iget-object v3, p0, Lizj;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;

    iput-object v0, p0, Lizj;->a:Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;

    .line 137
    iget-object v0, p0, Lizj;->a:Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lizj;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lizj;->a:Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 142
    :cond_3
    iget-object v0, p0, Lizj;->a:Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;->a(I)V

    .line 144
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 145
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 146
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 147
    iget-object v1, p0, Lizj;->a:Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 148
    iget-object v0, p0, Lizj;->a:Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/login/ui/CurrentAccountBannerView;->setVisibility(I)V

    .line 150
    new-instance v0, Lizk;

    invoke-direct {v0, p0}, Lizk;-><init>(Lizj;)V

    const-wide/16 v2, 0xbb8

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 122
    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 123
    new-instance v4, Landroid/widget/FrameLayout;

    invoke-direct {v4, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lizj;->f:Landroid/widget/FrameLayout;

    .line 124
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 125
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 126
    iget-object v5, p0, Lizj;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 127
    iget-object v1, p0, Lizj;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0, v6, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lizj;->g:Lhee;

    .line 78
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 82
    if-nez p1, :cond_2

    .line 83
    iget-object v0, p0, Lizj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 84
    iget-boolean v3, p0, Lizj;->e:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lizj;->b:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lizj;->e:Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 87
    :cond_2
    const-string v0, "show_on_start"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lizj;->e:Z

    .line 89
    :cond_3
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 100
    const-string v0, "show_on_start"

    iget-boolean v1, p0, Lizj;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 101
    return-void
.end method

.method public b_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lizj;->e:Z

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lizj;->a()V

    .line 96
    :cond_0
    return-void
.end method
