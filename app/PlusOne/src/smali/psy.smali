.class public final Lpsy;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpsy;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 436
    const/4 v0, 0x0

    new-array v0, v0, [Lpsy;

    sput-object v0, Lpsy;->a:[Lpsy;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 437
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 463
    const/4 v0, 0x0

    .line 464
    iget-object v1, p0, Lpsy;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 465
    const/4 v0, 0x1

    iget-object v1, p0, Lpsy;->b:Ljava/lang/String;

    .line 466
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 468
    :cond_0
    iget-object v1, p0, Lpsy;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 469
    const/4 v1, 0x2

    iget-object v2, p0, Lpsy;->c:Ljava/lang/Integer;

    .line 470
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 472
    :cond_1
    iget-object v1, p0, Lpsy;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 473
    const/4 v1, 0x3

    iget-object v2, p0, Lpsy;->d:Ljava/lang/Integer;

    .line 474
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 476
    :cond_2
    iget-object v1, p0, Lpsy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 477
    iput v0, p0, Lpsy;->ai:I

    .line 478
    return v0
.end method

.method public a(Loxn;)Lpsy;
    .locals 2

    .prologue
    .line 486
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 487
    sparse-switch v0, :sswitch_data_0

    .line 491
    iget-object v1, p0, Lpsy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 492
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpsy;->ah:Ljava/util/List;

    .line 495
    :cond_1
    iget-object v1, p0, Lpsy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 497
    :sswitch_0
    return-object p0

    .line 502
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpsy;->b:Ljava/lang/String;

    goto :goto_0

    .line 506
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpsy;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 510
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpsy;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 487
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, Lpsy;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 449
    const/4 v0, 0x1

    iget-object v1, p0, Lpsy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 451
    :cond_0
    iget-object v0, p0, Lpsy;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 452
    const/4 v0, 0x2

    iget-object v1, p0, Lpsy;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 454
    :cond_1
    iget-object v0, p0, Lpsy;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 455
    const/4 v0, 0x3

    iget-object v1, p0, Lpsy;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 457
    :cond_2
    iget-object v0, p0, Lpsy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 459
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0, p1}, Lpsy;->a(Loxn;)Lpsy;

    move-result-object v0

    return-object v0
.end method
