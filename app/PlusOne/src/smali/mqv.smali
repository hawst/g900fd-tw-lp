.class public final Lmqv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lmqv;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:[Lpaz;

.field public c:[Lpaf;

.field private d:[Loya;

.field private e:Ljava/lang/Boolean;

.field private f:[Lpaz;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x2ab8837

    new-instance v1, Lmqw;

    invoke-direct {v1}, Lmqw;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lmqv;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    sget-object v0, Lpaz;->a:[Lpaz;

    iput-object v0, p0, Lmqv;->b:[Lpaz;

    .line 20
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lmqv;->d:[Loya;

    .line 23
    sget-object v0, Lpaf;->a:[Lpaf;

    iput-object v0, p0, Lmqv;->c:[Lpaf;

    .line 28
    sget-object v0, Lpaz;->a:[Lpaz;

    iput-object v0, p0, Lmqv;->f:[Lpaz;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 70
    .line 71
    iget-object v0, p0, Lmqv;->b:[Lpaz;

    if-eqz v0, :cond_1

    .line 72
    iget-object v3, p0, Lmqv;->b:[Lpaz;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 73
    if-eqz v5, :cond_0

    .line 74
    const/4 v6, 0x1

    .line 75
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 72
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 79
    :cond_2
    iget-object v2, p0, Lmqv;->f:[Lpaz;

    if-eqz v2, :cond_4

    .line 80
    iget-object v3, p0, Lmqv;->f:[Lpaz;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 81
    if-eqz v5, :cond_3

    .line 82
    const/4 v6, 0x2

    .line 83
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 80
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 87
    :cond_4
    iget-object v2, p0, Lmqv;->d:[Loya;

    if-eqz v2, :cond_6

    .line 88
    iget-object v3, p0, Lmqv;->d:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 89
    if-eqz v5, :cond_5

    .line 90
    const/4 v6, 0x3

    .line 91
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 88
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 95
    :cond_6
    iget-object v2, p0, Lmqv;->c:[Lpaf;

    if-eqz v2, :cond_8

    .line 96
    iget-object v2, p0, Lmqv;->c:[Lpaf;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 97
    if-eqz v4, :cond_7

    .line 98
    const/4 v5, 0x4

    .line 99
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 96
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 103
    :cond_8
    iget-object v1, p0, Lmqv;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 104
    const/4 v1, 0x5

    iget-object v2, p0, Lmqv;->e:Ljava/lang/Boolean;

    .line 105
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 107
    :cond_9
    iget-object v1, p0, Lmqv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    iput v0, p0, Lmqv;->ai:I

    .line 109
    return v0
.end method

.method public a(Loxn;)Lmqv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 117
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 118
    sparse-switch v0, :sswitch_data_0

    .line 122
    iget-object v2, p0, Lmqv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 123
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmqv;->ah:Ljava/util/List;

    .line 126
    :cond_1
    iget-object v2, p0, Lmqv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    :sswitch_0
    return-object p0

    .line 133
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 134
    iget-object v0, p0, Lmqv;->b:[Lpaz;

    if-nez v0, :cond_3

    move v0, v1

    .line 135
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpaz;

    .line 136
    iget-object v3, p0, Lmqv;->b:[Lpaz;

    if-eqz v3, :cond_2

    .line 137
    iget-object v3, p0, Lmqv;->b:[Lpaz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 139
    :cond_2
    iput-object v2, p0, Lmqv;->b:[Lpaz;

    .line 140
    :goto_2
    iget-object v2, p0, Lmqv;->b:[Lpaz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 141
    iget-object v2, p0, Lmqv;->b:[Lpaz;

    new-instance v3, Lpaz;

    invoke-direct {v3}, Lpaz;-><init>()V

    aput-object v3, v2, v0

    .line 142
    iget-object v2, p0, Lmqv;->b:[Lpaz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 143
    invoke-virtual {p1}, Loxn;->a()I

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 134
    :cond_3
    iget-object v0, p0, Lmqv;->b:[Lpaz;

    array-length v0, v0

    goto :goto_1

    .line 146
    :cond_4
    iget-object v2, p0, Lmqv;->b:[Lpaz;

    new-instance v3, Lpaz;

    invoke-direct {v3}, Lpaz;-><init>()V

    aput-object v3, v2, v0

    .line 147
    iget-object v2, p0, Lmqv;->b:[Lpaz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 151
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 152
    iget-object v0, p0, Lmqv;->f:[Lpaz;

    if-nez v0, :cond_6

    move v0, v1

    .line 153
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpaz;

    .line 154
    iget-object v3, p0, Lmqv;->f:[Lpaz;

    if-eqz v3, :cond_5

    .line 155
    iget-object v3, p0, Lmqv;->f:[Lpaz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    :cond_5
    iput-object v2, p0, Lmqv;->f:[Lpaz;

    .line 158
    :goto_4
    iget-object v2, p0, Lmqv;->f:[Lpaz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 159
    iget-object v2, p0, Lmqv;->f:[Lpaz;

    new-instance v3, Lpaz;

    invoke-direct {v3}, Lpaz;-><init>()V

    aput-object v3, v2, v0

    .line 160
    iget-object v2, p0, Lmqv;->f:[Lpaz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 161
    invoke-virtual {p1}, Loxn;->a()I

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 152
    :cond_6
    iget-object v0, p0, Lmqv;->f:[Lpaz;

    array-length v0, v0

    goto :goto_3

    .line 164
    :cond_7
    iget-object v2, p0, Lmqv;->f:[Lpaz;

    new-instance v3, Lpaz;

    invoke-direct {v3}, Lpaz;-><init>()V

    aput-object v3, v2, v0

    .line 165
    iget-object v2, p0, Lmqv;->f:[Lpaz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 169
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 170
    iget-object v0, p0, Lmqv;->d:[Loya;

    if-nez v0, :cond_9

    move v0, v1

    .line 171
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 172
    iget-object v3, p0, Lmqv;->d:[Loya;

    if-eqz v3, :cond_8

    .line 173
    iget-object v3, p0, Lmqv;->d:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 175
    :cond_8
    iput-object v2, p0, Lmqv;->d:[Loya;

    .line 176
    :goto_6
    iget-object v2, p0, Lmqv;->d:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 177
    iget-object v2, p0, Lmqv;->d:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 178
    iget-object v2, p0, Lmqv;->d:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 179
    invoke-virtual {p1}, Loxn;->a()I

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 170
    :cond_9
    iget-object v0, p0, Lmqv;->d:[Loya;

    array-length v0, v0

    goto :goto_5

    .line 182
    :cond_a
    iget-object v2, p0, Lmqv;->d:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 183
    iget-object v2, p0, Lmqv;->d:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 187
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 188
    iget-object v0, p0, Lmqv;->c:[Lpaf;

    if-nez v0, :cond_c

    move v0, v1

    .line 189
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lpaf;

    .line 190
    iget-object v3, p0, Lmqv;->c:[Lpaf;

    if-eqz v3, :cond_b

    .line 191
    iget-object v3, p0, Lmqv;->c:[Lpaf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    :cond_b
    iput-object v2, p0, Lmqv;->c:[Lpaf;

    .line 194
    :goto_8
    iget-object v2, p0, Lmqv;->c:[Lpaf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 195
    iget-object v2, p0, Lmqv;->c:[Lpaf;

    new-instance v3, Lpaf;

    invoke-direct {v3}, Lpaf;-><init>()V

    aput-object v3, v2, v0

    .line 196
    iget-object v2, p0, Lmqv;->c:[Lpaf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 197
    invoke-virtual {p1}, Loxn;->a()I

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 188
    :cond_c
    iget-object v0, p0, Lmqv;->c:[Lpaf;

    array-length v0, v0

    goto :goto_7

    .line 200
    :cond_d
    iget-object v2, p0, Lmqv;->c:[Lpaf;

    new-instance v3, Lpaf;

    invoke-direct {v3}, Lpaf;-><init>()V

    aput-object v3, v2, v0

    .line 201
    iget-object v2, p0, Lmqv;->c:[Lpaf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 205
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmqv;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 118
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-object v1, p0, Lmqv;->b:[Lpaz;

    if-eqz v1, :cond_1

    .line 34
    iget-object v2, p0, Lmqv;->b:[Lpaz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 35
    if-eqz v4, :cond_0

    .line 36
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 34
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    :cond_1
    iget-object v1, p0, Lmqv;->f:[Lpaz;

    if-eqz v1, :cond_3

    .line 41
    iget-object v2, p0, Lmqv;->f:[Lpaz;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 42
    if-eqz v4, :cond_2

    .line 43
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 41
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 47
    :cond_3
    iget-object v1, p0, Lmqv;->d:[Loya;

    if-eqz v1, :cond_5

    .line 48
    iget-object v2, p0, Lmqv;->d:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 49
    if-eqz v4, :cond_4

    .line 50
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 48
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 54
    :cond_5
    iget-object v1, p0, Lmqv;->c:[Lpaf;

    if-eqz v1, :cond_7

    .line 55
    iget-object v1, p0, Lmqv;->c:[Lpaf;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 56
    if-eqz v3, :cond_6

    .line 57
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 55
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 61
    :cond_7
    iget-object v0, p0, Lmqv;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 62
    const/4 v0, 0x5

    iget-object v1, p0, Lmqv;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 64
    :cond_8
    iget-object v0, p0, Lmqv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 66
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmqv;->a(Loxn;)Lmqv;

    move-result-object v0

    return-object v0
.end method
