.class public final Lnil;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnil;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lnih;

.field public e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2258
    const/4 v0, 0x0

    new-array v0, v0, [Lnil;

    sput-object v0, Lnil;->a:[Lnil;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2259
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2266
    const/4 v0, 0x0

    iput-object v0, p0, Lnil;->d:Lnih;

    .line 2259
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2296
    const/4 v0, 0x0

    .line 2297
    iget-object v1, p0, Lnil;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2298
    const/4 v0, 0x1

    iget-object v1, p0, Lnil;->b:Ljava/lang/String;

    .line 2299
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2301
    :cond_0
    iget-object v1, p0, Lnil;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2302
    const/4 v1, 0x2

    iget-object v2, p0, Lnil;->c:Ljava/lang/String;

    .line 2303
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2305
    :cond_1
    iget-object v1, p0, Lnil;->d:Lnih;

    if-eqz v1, :cond_2

    .line 2306
    const/4 v1, 0x3

    iget-object v2, p0, Lnil;->d:Lnih;

    .line 2307
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2309
    :cond_2
    iget-object v1, p0, Lnil;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2310
    const/4 v1, 0x4

    iget-object v2, p0, Lnil;->f:Ljava/lang/String;

    .line 2311
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2313
    :cond_3
    iget-object v1, p0, Lnil;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2314
    const/4 v1, 0x5

    iget-object v2, p0, Lnil;->e:Ljava/lang/String;

    .line 2315
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2317
    :cond_4
    iget-object v1, p0, Lnil;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2318
    iput v0, p0, Lnil;->ai:I

    .line 2319
    return v0
.end method

.method public a(Loxn;)Lnil;
    .locals 2

    .prologue
    .line 2327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2328
    sparse-switch v0, :sswitch_data_0

    .line 2332
    iget-object v1, p0, Lnil;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2333
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnil;->ah:Ljava/util/List;

    .line 2336
    :cond_1
    iget-object v1, p0, Lnil;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2338
    :sswitch_0
    return-object p0

    .line 2343
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnil;->b:Ljava/lang/String;

    goto :goto_0

    .line 2347
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnil;->c:Ljava/lang/String;

    goto :goto_0

    .line 2351
    :sswitch_3
    iget-object v0, p0, Lnil;->d:Lnih;

    if-nez v0, :cond_2

    .line 2352
    new-instance v0, Lnih;

    invoke-direct {v0}, Lnih;-><init>()V

    iput-object v0, p0, Lnil;->d:Lnih;

    .line 2354
    :cond_2
    iget-object v0, p0, Lnil;->d:Lnih;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2358
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnil;->f:Ljava/lang/String;

    goto :goto_0

    .line 2362
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnil;->e:Ljava/lang/String;

    goto :goto_0

    .line 2328
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2275
    iget-object v0, p0, Lnil;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2276
    const/4 v0, 0x1

    iget-object v1, p0, Lnil;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2278
    :cond_0
    iget-object v0, p0, Lnil;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2279
    const/4 v0, 0x2

    iget-object v1, p0, Lnil;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2281
    :cond_1
    iget-object v0, p0, Lnil;->d:Lnih;

    if-eqz v0, :cond_2

    .line 2282
    const/4 v0, 0x3

    iget-object v1, p0, Lnil;->d:Lnih;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2284
    :cond_2
    iget-object v0, p0, Lnil;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2285
    const/4 v0, 0x4

    iget-object v1, p0, Lnil;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2287
    :cond_3
    iget-object v0, p0, Lnil;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2288
    const/4 v0, 0x5

    iget-object v1, p0, Lnil;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2290
    :cond_4
    iget-object v0, p0, Lnil;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2292
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2255
    invoke-virtual {p0, p1}, Lnil;->a(Loxn;)Lnil;

    move-result-object v0

    return-object v0
.end method
