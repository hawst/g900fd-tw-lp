.class public final Lmko;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnai;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22435
    invoke-direct {p0}, Loxq;-><init>()V

    .line 22438
    iput-object v0, p0, Lmko;->apiHeader:Llyq;

    .line 22441
    iput-object v0, p0, Lmko;->a:Lnai;

    .line 22435
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 22458
    const/4 v0, 0x0

    .line 22459
    iget-object v1, p0, Lmko;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 22460
    const/4 v0, 0x1

    iget-object v1, p0, Lmko;->apiHeader:Llyq;

    .line 22461
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22463
    :cond_0
    iget-object v1, p0, Lmko;->a:Lnai;

    if-eqz v1, :cond_1

    .line 22464
    const/4 v1, 0x2

    iget-object v2, p0, Lmko;->a:Lnai;

    .line 22465
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22467
    :cond_1
    iget-object v1, p0, Lmko;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22468
    iput v0, p0, Lmko;->ai:I

    .line 22469
    return v0
.end method

.method public a(Loxn;)Lmko;
    .locals 2

    .prologue
    .line 22477
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 22478
    sparse-switch v0, :sswitch_data_0

    .line 22482
    iget-object v1, p0, Lmko;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 22483
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmko;->ah:Ljava/util/List;

    .line 22486
    :cond_1
    iget-object v1, p0, Lmko;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22488
    :sswitch_0
    return-object p0

    .line 22493
    :sswitch_1
    iget-object v0, p0, Lmko;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 22494
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmko;->apiHeader:Llyq;

    .line 22496
    :cond_2
    iget-object v0, p0, Lmko;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22500
    :sswitch_2
    iget-object v0, p0, Lmko;->a:Lnai;

    if-nez v0, :cond_3

    .line 22501
    new-instance v0, Lnai;

    invoke-direct {v0}, Lnai;-><init>()V

    iput-object v0, p0, Lmko;->a:Lnai;

    .line 22503
    :cond_3
    iget-object v0, p0, Lmko;->a:Lnai;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 22478
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 22446
    iget-object v0, p0, Lmko;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 22447
    const/4 v0, 0x1

    iget-object v1, p0, Lmko;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22449
    :cond_0
    iget-object v0, p0, Lmko;->a:Lnai;

    if-eqz v0, :cond_1

    .line 22450
    const/4 v0, 0x2

    iget-object v1, p0, Lmko;->a:Lnai;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 22452
    :cond_1
    iget-object v0, p0, Lmko;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 22454
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 22431
    invoke-virtual {p0, p1}, Lmko;->a(Loxn;)Lmko;

    move-result-object v0

    return-object v0
.end method
