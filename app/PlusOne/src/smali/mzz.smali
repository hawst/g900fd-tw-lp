.class public final Lmzz;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lnaa;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1240
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1369
    sget-object v0, Lnaa;->a:[Lnaa;

    iput-object v0, p0, Lmzz;->a:[Lnaa;

    .line 1240
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1387
    .line 1388
    iget-object v1, p0, Lmzz;->a:[Lnaa;

    if-eqz v1, :cond_1

    .line 1389
    iget-object v2, p0, Lmzz;->a:[Lnaa;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1390
    if-eqz v4, :cond_0

    .line 1391
    const/4 v5, 0x4

    .line 1392
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1389
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1396
    :cond_1
    iget-object v1, p0, Lmzz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1397
    iput v0, p0, Lmzz;->ai:I

    .line 1398
    return v0
.end method

.method public a(Loxn;)Lmzz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1406
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1407
    sparse-switch v0, :sswitch_data_0

    .line 1411
    iget-object v2, p0, Lmzz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1412
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmzz;->ah:Ljava/util/List;

    .line 1415
    :cond_1
    iget-object v2, p0, Lmzz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1417
    :sswitch_0
    return-object p0

    .line 1422
    :sswitch_1
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1423
    iget-object v0, p0, Lmzz;->a:[Lnaa;

    if-nez v0, :cond_3

    move v0, v1

    .line 1424
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnaa;

    .line 1425
    iget-object v3, p0, Lmzz;->a:[Lnaa;

    if-eqz v3, :cond_2

    .line 1426
    iget-object v3, p0, Lmzz;->a:[Lnaa;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1428
    :cond_2
    iput-object v2, p0, Lmzz;->a:[Lnaa;

    .line 1429
    :goto_2
    iget-object v2, p0, Lmzz;->a:[Lnaa;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 1430
    iget-object v2, p0, Lmzz;->a:[Lnaa;

    new-instance v3, Lnaa;

    invoke-direct {v3}, Lnaa;-><init>()V

    aput-object v3, v2, v0

    .line 1431
    iget-object v2, p0, Lmzz;->a:[Lnaa;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1432
    invoke-virtual {p1}, Loxn;->a()I

    .line 1429
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1423
    :cond_3
    iget-object v0, p0, Lmzz;->a:[Lnaa;

    array-length v0, v0

    goto :goto_1

    .line 1435
    :cond_4
    iget-object v2, p0, Lmzz;->a:[Lnaa;

    new-instance v3, Lnaa;

    invoke-direct {v3}, Lnaa;-><init>()V

    aput-object v3, v2, v0

    .line 1436
    iget-object v2, p0, Lmzz;->a:[Lnaa;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1407
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1374
    iget-object v0, p0, Lmzz;->a:[Lnaa;

    if-eqz v0, :cond_1

    .line 1375
    iget-object v1, p0, Lmzz;->a:[Lnaa;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1376
    if-eqz v3, :cond_0

    .line 1377
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1375
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1381
    :cond_1
    iget-object v0, p0, Lmzz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1383
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1236
    invoke-virtual {p0, p1}, Lmzz;->a(Loxn;)Lmzz;

    move-result-object v0

    return-object v0
.end method
