.class public final Lezh;
.super Landroid/database/CursorWrapper;
.source "PG"


# static fields
.field private static a:Ljava/text/DateFormat;

.field private static b:Ljava/text/DateFormat;


# instance fields
.field private c:Landroid/content/Context;

.field private d:J

.field private e:J

.field private f:Ljava/lang/String;

.field private g:Lhyx;

.field private h:[Landroid/net/Uri;

.field private i:[Z

.field private final j:I

.field private final k:I

.field private final l:Ljem;

.field private final m:Lezi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhyx;ILjem;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0, p2}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 44
    new-instance v0, Lezi;

    invoke-direct {v0}, Lezi;-><init>()V

    iput-object v0, p0, Lezh;->m:Lezi;

    .line 49
    iput-object p1, p0, Lezh;->c:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lezh;->g:Lhyx;

    .line 51
    invoke-static {}, Ljvj;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lezh;->d:J

    .line 52
    const-wide/32 v0, 0x40000

    iput-wide v0, p0, Lezh;->e:J

    .line 54
    invoke-static {}, Ljvj;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lezh;->f:Ljava/lang/String;

    .line 55
    invoke-virtual {p2}, Lhyx;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "media_uris"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Landroid/net/Uri;

    iput-object v0, p0, Lezh;->h:[Landroid/net/Uri;

    .line 57
    invoke-virtual {p2}, Lhyx;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "media_is_video"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    iput-object v0, p0, Lezh;->i:[Z

    .line 59
    iput p3, p0, Lezh;->j:I

    .line 61
    const-string v0, "_id"

    invoke-virtual {p2, v0}, Lhyx;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lezh;->k:I

    .line 62
    iput-object p4, p0, Lezh;->l:Ljem;

    .line 64
    sget-object v0, Lezh;->a:Ljava/text/DateFormat;

    if-nez v0, :cond_0

    .line 65
    invoke-static {p1}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lezh;->a:Ljava/text/DateFormat;

    .line 66
    invoke-static {p1}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lezh;->b:Ljava/text/DateFormat;

    .line 68
    :cond_0
    return-void
.end method


# virtual methods
.method public getColumnCount()I
    .locals 1

    .prologue
    .line 72
    sget-object v0, Levl;->b:[Ljava/lang/String;

    const/16 v0, 0x16

    return v0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Levl;->b:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Levl;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public getInt(I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 120
    sparse-switch p1, :sswitch_data_0

    .line 134
    const-string v1, "EsTile"

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x3d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "LocalBestPhotosCursorWrapper#getInt - bad column: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :goto_0
    :sswitch_0
    return v0

    .line 122
    :sswitch_1
    iget v0, p0, Lezh;->k:I

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->getInt(I)I

    move-result v0

    goto :goto_0

    .line 128
    :sswitch_2
    iget v0, p0, Lezh;->j:I

    iget-object v1, p0, Lezh;->g:Lhyx;

    invoke-virtual {v1}, Lhyx;->getPosition()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    .line 131
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 120
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_3
        0x9 -> :sswitch_0
        0xd -> :sswitch_2
    .end sparse-switch
.end method

.method public getLong(I)J
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 87
    sparse-switch p1, :sswitch_data_0

    .line 112
    const-string v0, "EsTile"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x3e

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "LocalBestPhotosCursorWrapper#getLong - bad column: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :goto_0
    return-wide v2

    .line 89
    :sswitch_0
    iget v0, p0, Lezh;->k:I

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->getLong(I)J

    move-result-wide v2

    goto :goto_0

    .line 92
    :sswitch_1
    iget-object v4, p0, Lezh;->g:Lhyx;

    invoke-virtual {v4}, Lhyx;->a()I

    move-result v4

    .line 94
    iget-object v5, p0, Lezh;->l:Ljem;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lezh;->i:[Z

    aget-boolean v5, v5, v4

    if-eqz v5, :cond_0

    .line 95
    iget-object v5, p0, Lezh;->g:Lhyx;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lhyx;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 97
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lezh;->l:Ljem;

    .line 98
    invoke-virtual {v6, v5}, Ljem;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    move v1, v0

    .line 101
    :cond_0
    iget-wide v6, p0, Lezh;->e:J

    iget-object v0, p0, Lezh;->i:[Z

    aget-boolean v0, v0, v4

    if-eqz v0, :cond_2

    const-wide/16 v4, 0x20

    :goto_2
    or-long/2addr v4, v6

    if-eqz v1, :cond_3

    const-wide/16 v0, 0x100

    :goto_3
    or-long v2, v4, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 98
    goto :goto_1

    :cond_2
    move-wide v4, v2

    .line 101
    goto :goto_2

    :cond_3
    move-wide v0, v2

    goto :goto_3

    .line 106
    :sswitch_2
    iget-wide v2, p0, Lezh;->d:J

    goto :goto_0

    .line 109
    :sswitch_3
    iget-object v1, p0, Lezh;->g:Lhyx;

    invoke-virtual {v1, v0}, Lhyx;->getLong(I)J

    move-result-wide v2

    goto :goto_0

    .line 87
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xe -> :sswitch_2
        0xf -> :sswitch_1
        0x10 -> :sswitch_3
    .end sparse-switch
.end method

.method public getString(I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 161
    iget-object v1, p0, Lezh;->m:Lezi;

    invoke-virtual {p0}, Lezh;->getPosition()I

    move-result v2

    invoke-virtual {v1, v2, p1}, Lezi;->a(II)Ljava/lang/String;

    move-result-object v1

    .line 162
    if-nez v1, :cond_1

    .line 163
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string v1, "EsTile"

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x40

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "LocalBestPhotosCursorWrapper#getString - bad column: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :goto_0
    :pswitch_1
    iget-object v1, p0, Lezh;->m:Lezi;

    invoke-virtual {p0}, Lezh;->getPosition()I

    move-result v2

    invoke-virtual {v1, v2, p1, v0}, Lezi;->a(IILjava/lang/String;)V

    .line 166
    :goto_1
    return-object v0

    .line 163
    :pswitch_2
    iget v0, p0, Lezh;->k:I

    invoke-super {p0, v0}, Landroid/database/CursorWrapper;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lezh;->f:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lezh;->g:Lhyx;

    invoke-virtual {v0, v3}, Lhyx;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljvj;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lezh;->g:Lhyx;

    invoke-virtual {v0, v3}, Lhyx;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljvj;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    const-string v0, "~local"

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lezh;->g:Lhyx;

    invoke-virtual {v0}, Lhyx;->a()I

    move-result v0

    iget-object v1, p0, Lezh;->h:[Landroid/net/Uri;

    aget-object v0, v1, v0

    iget-object v1, p0, Lezh;->g:Lhyx;

    invoke-virtual {v1, v5}, Lhyx;->getLong(I)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lezh;->g:Lhyx;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lhyx;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lezh;->g:Lhyx;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lhyx;->getInt(I)I

    move-result v1

    iget-object v2, p0, Lezh;->g:Lhyx;

    invoke-virtual {v2, v4}, Lhyx;->getLong(I)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lhsf;->a(Ljava/lang/String;IJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    new-instance v0, Ljava/util/Date;

    iget-object v1, p0, Lezh;->g:Lhyx;

    invoke-virtual {v1, v4}, Lhyx;->getLong(I)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iget-object v1, p0, Lezh;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0658

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    sget-object v3, Lezh;->a:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    sget-object v4, Lezh;->b:Ljava/text/DateFormat;

    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    move-object v0, v1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_7
        :pswitch_1
    .end packed-switch
.end method

.method public isNull(I)Z
    .locals 1

    .prologue
    .line 142
    packed-switch p1, :pswitch_data_0

    .line 155
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 152
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
