.class public final Ljhz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;
.implements Ljgq;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:[I

.field private static e:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private final c:Ljava/util/Random;

.field private d:Landroid/content/Context;

.field private f:Ljhh;

.field private g:Landroid/os/Handler;

.field private h:Ljgn;

.field private i:Ljia;

.field private volatile j:Z

.field private k:Z

.field private final l:Landroid/content/SyncResult;

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Ljhz;->a:Ljava/lang/Object;

    .line 50
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Ljhz;->b:[I

    return-void

    :array_0
    .array-data 4
        0x7530
        0x1d4c0
        0x3a980
        0x927c0
        0x124f80
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljhh;Ljia;)V
    .locals 4

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 114
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 118
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ljhz;->d:Landroid/content/Context;

    .line 120
    iput-object p2, p0, Ljhz;->f:Ljhh;

    .line 122
    iput-object p3, p0, Ljhz;->i:Ljia;

    .line 125
    const-class v0, Ljgn;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    iput-object v0, p0, Ljhz;->h:Ljgn;

    .line 127
    new-instance v0, Landroid/content/SyncResult;

    invoke-direct {v0}, Landroid/content/SyncResult;-><init>()V

    iput-object v0, p0, Ljhz;->l:Landroid/content/SyncResult;

    .line 129
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Ljhz;->c:Ljava/util/Random;

    .line 131
    sget-object v1, Ljhz;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 132
    :try_start_0
    sget-object v0, Ljhz;->e:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_2

    .line 133
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 134
    const/4 v2, 0x1

    const-string v3, "network_queue_process_wakelock"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Ljhz;->e:Landroid/os/PowerManager$WakeLock;

    .line 137
    :cond_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(J)V
    .locals 5

    .prologue
    const-wide/16 v0, 0x1388

    .line 290
    const/4 v2, 0x1

    iput-boolean v2, p0, Ljhz;->j:Z

    .line 291
    const-wide/16 v2, 0x1

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    .line 292
    invoke-direct {p0}, Ljhz;->b()V

    .line 304
    :goto_0
    return-void

    .line 294
    :cond_0
    cmp-long v2, p1, v0

    if-gez v2, :cond_1

    move-wide p1, v0

    .line 298
    :cond_1
    const-string v0, "NetworkQueueProcessJob"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Setting alarm for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    :cond_2
    iget-object v0, p0, Ljhz;->d:Landroid/content/Context;

    iget-object v1, p0, Ljhz;->f:Ljhh;

    invoke-interface {v1}, Ljhh;->b()I

    move-result v1

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueAlarmReceiver;->a(Landroid/content/Context;IJ)V

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljhz;->j:Z

    .line 308
    iget-object v0, p0, Ljhz;->g:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 309
    return-void
.end method


# virtual methods
.method public declared-synchronized a(JZ)V
    .locals 5

    .prologue
    .line 245
    monitor-enter p0

    :try_start_0
    const-string v0, "NetworkQueueProcessJob"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Scheduling in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1c

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Retry on IOExceptions: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 250
    :cond_0
    iget-object v0, p0, Ljhz;->g:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 251
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "NetworkQueueProcessor"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 257
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 258
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Ljhz;->g:Landroid/os/Handler;

    .line 262
    :cond_1
    iget-object v0, p0, Ljhz;->d:Landroid/content/Context;

    invoke-virtual {p0, v0}, Ljhz;->b(Landroid/content/Context;)V

    .line 264
    iput-boolean p3, p0, Ljhz;->k:Z

    .line 266
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 267
    invoke-direct {p0}, Ljhz;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    :goto_0
    monitor-exit p0

    return-void

    .line 269
    :cond_2
    :try_start_1
    invoke-direct {p0, p1, p2}, Ljhz;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 317
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ljhz;->j:Z

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Ljhz;->h:Ljgn;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Ljgn;->b(Landroid/content/Context;Ljgq;)V

    .line 322
    invoke-direct {p0}, Ljhz;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    :cond_0
    monitor-exit p0

    return-void

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Ljhz;->f:Ljhh;

    invoke-interface {v0}, Ljhh;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 279
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Ljhz;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 287
    :goto_0
    monitor-exit p0

    return-void

    .line 283
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Ljhz;->j:Z

    .line 284
    iget-object v0, p0, Ljhz;->h:Ljgn;

    invoke-interface {v0, p1, p0}, Ljgn;->b(Landroid/content/Context;Ljgq;)V

    .line 285
    iget-object v0, p0, Ljhz;->f:Ljhh;

    invoke-interface {v0}, Ljhh;->b()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueAlarmReceiver;->a(Landroid/content/Context;I)V

    .line 286
    iget-object v0, p0, Ljhz;->g:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 279
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 145
    :try_start_0
    sget-object v1, Ljhz;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 155
    iget-object v1, p0, Ljhz;->h:Ljgn;

    invoke-interface {v1}, Ljgn;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 156
    const/4 v1, 0x0

    iput-boolean v1, p0, Ljhz;->j:Z

    .line 163
    new-instance v1, Lkfp;

    invoke-direct {v1}, Lkfp;-><init>()V

    .line 164
    iget-object v2, p0, Ljhz;->f:Ljhh;

    iget-object v3, p0, Ljhz;->l:Landroid/content/SyncResult;

    invoke-interface {v2, v1, v3}, Ljhh;->a(Lkfp;Landroid/content/SyncResult;)V

    .line 166
    iget-object v1, p0, Ljhz;->l:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 167
    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    iget-boolean v1, p0, Ljhz;->k:Z

    if-eqz v1, :cond_2

    .line 170
    iget v0, p0, Ljhz;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljhz;->m:I

    .line 171
    iget v0, p0, Ljhz;->m:I

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Ljhz;->b:[I

    const/4 v1, 0x4

    .line 172
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 173
    sget-object v1, Ljhz;->b:[I

    aget v0, v1, v0

    .line 174
    iget-object v1, p0, Ljhz;->c:Ljava/util/Random;

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    .line 175
    add-int/2addr v0, v1

    .line 177
    const-string v1, "NetworkQueueProcessJob"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Process had an IOException.  Retrying again in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    :cond_0
    :goto_0
    if-lez v0, :cond_1

    .line 200
    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Ljhz;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 203
    :cond_1
    sget-object v0, Ljhz;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 204
    return-void

    .line 183
    :cond_2
    :try_start_1
    iget-object v1, p0, Ljhz;->l:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    iget-boolean v1, p0, Ljhz;->k:Z

    .line 184
    :cond_3
    iget-object v1, p0, Ljhz;->h:Ljgn;

    iget-object v2, p0, Ljhz;->d:Landroid/content/Context;

    invoke-interface {v1, v2, p0}, Ljgn;->b(Landroid/content/Context;Ljgq;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 190
    const-wide/16 v2, 0x32

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    :try_start_3
    monitor-enter p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-boolean v1, p0, Ljhz;->j:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Ljhz;->h:Ljgn;

    iget-object v2, p0, Ljhz;->d:Landroid/content/Context;

    invoke-interface {v1, v2, p0}, Ljgn;->b(Landroid/content/Context;Ljgq;)V

    iget-object v1, p0, Ljhz;->g:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    const/4 v1, 0x0

    iput-object v1, p0, Ljhz;->g:Landroid/os/Handler;

    :cond_4
    iget-object v1, p0, Ljhz;->i:Ljia;

    iget-object v2, p0, Ljhz;->f:Ljhh;

    invoke-interface {v1, p0, v2}, Ljia;->a(Ljhz;Ljhh;)V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 203
    :catchall_1
    move-exception v0

    sget-object v1, Ljhz;->e:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    .line 190
    :catch_0
    move-exception v1

    :try_start_6
    monitor-enter p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    iget-boolean v1, p0, Ljhz;->j:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Ljhz;->h:Ljgn;

    iget-object v2, p0, Ljhz;->d:Landroid/content/Context;

    invoke-interface {v1, v2, p0}, Ljgn;->b(Landroid/content/Context;Ljgq;)V

    iget-object v1, p0, Ljhz;->g:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    const/4 v1, 0x0

    iput-object v1, p0, Ljhz;->g:Landroid/os/Handler;

    :cond_5
    iget-object v1, p0, Ljhz;->i:Ljia;

    iget-object v2, p0, Ljhz;->f:Ljhh;

    invoke-interface {v1, p0, v2}, Ljia;->a(Ljhz;Ljhh;)V

    monitor-exit p0

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v0

    :catchall_3
    move-exception v0

    monitor-enter p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    iget-boolean v1, p0, Ljhz;->j:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Ljhz;->h:Ljgn;

    iget-object v2, p0, Ljhz;->d:Landroid/content/Context;

    invoke-interface {v1, v2, p0}, Ljgn;->b(Landroid/content/Context;Ljgq;)V

    iget-object v1, p0, Ljhz;->g:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    const/4 v1, 0x0

    iput-object v1, p0, Ljhz;->g:Landroid/os/Handler;

    :cond_6
    iget-object v1, p0, Ljhz;->i:Ljia;

    iget-object v2, p0, Ljhz;->f:Ljhh;

    invoke-interface {v1, p0, v2}, Ljia;->a(Ljhz;Ljhh;)V

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    :try_start_a
    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :catchall_4
    move-exception v0

    :try_start_b
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :try_start_c
    throw v0

    .line 192
    :cond_7
    iget-object v1, p0, Ljhz;->h:Ljgn;

    iget-object v2, p0, Ljhz;->d:Landroid/content/Context;

    invoke-interface {v1, v2, p0}, Ljgn;->a(Landroid/content/Context;Ljgq;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_0
.end method
