.class public final Lmjh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnft;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26890
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26893
    iput-object v0, p0, Lmjh;->apiHeader:Llyr;

    .line 26896
    iput-object v0, p0, Lmjh;->a:Lnft;

    .line 26890
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26913
    const/4 v0, 0x0

    .line 26914
    iget-object v1, p0, Lmjh;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 26915
    const/4 v0, 0x1

    iget-object v1, p0, Lmjh;->apiHeader:Llyr;

    .line 26916
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26918
    :cond_0
    iget-object v1, p0, Lmjh;->a:Lnft;

    if-eqz v1, :cond_1

    .line 26919
    const/4 v1, 0x2

    iget-object v2, p0, Lmjh;->a:Lnft;

    .line 26920
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26922
    :cond_1
    iget-object v1, p0, Lmjh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26923
    iput v0, p0, Lmjh;->ai:I

    .line 26924
    return v0
.end method

.method public a(Loxn;)Lmjh;
    .locals 2

    .prologue
    .line 26932
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26933
    sparse-switch v0, :sswitch_data_0

    .line 26937
    iget-object v1, p0, Lmjh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26938
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjh;->ah:Ljava/util/List;

    .line 26941
    :cond_1
    iget-object v1, p0, Lmjh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26943
    :sswitch_0
    return-object p0

    .line 26948
    :sswitch_1
    iget-object v0, p0, Lmjh;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 26949
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjh;->apiHeader:Llyr;

    .line 26951
    :cond_2
    iget-object v0, p0, Lmjh;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26955
    :sswitch_2
    iget-object v0, p0, Lmjh;->a:Lnft;

    if-nez v0, :cond_3

    .line 26956
    new-instance v0, Lnft;

    invoke-direct {v0}, Lnft;-><init>()V

    iput-object v0, p0, Lmjh;->a:Lnft;

    .line 26958
    :cond_3
    iget-object v0, p0, Lmjh;->a:Lnft;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26933
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26901
    iget-object v0, p0, Lmjh;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 26902
    const/4 v0, 0x1

    iget-object v1, p0, Lmjh;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26904
    :cond_0
    iget-object v0, p0, Lmjh;->a:Lnft;

    if-eqz v0, :cond_1

    .line 26905
    const/4 v0, 0x2

    iget-object v1, p0, Lmjh;->a:Lnft;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26907
    :cond_1
    iget-object v0, p0, Lmjh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26909
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26886
    invoke-virtual {p0, p1}, Lmjh;->a(Loxn;)Lmjh;

    move-result-object v0

    return-object v0
.end method
