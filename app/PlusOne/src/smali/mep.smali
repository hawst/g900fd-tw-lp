.class public final Lmep;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lokc;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11500
    invoke-direct {p0}, Loxq;-><init>()V

    .line 11503
    iput-object v0, p0, Lmep;->apiHeader:Llyr;

    .line 11506
    iput-object v0, p0, Lmep;->a:Lokc;

    .line 11500
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 11523
    const/4 v0, 0x0

    .line 11524
    iget-object v1, p0, Lmep;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 11525
    const/4 v0, 0x1

    iget-object v1, p0, Lmep;->apiHeader:Llyr;

    .line 11526
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11528
    :cond_0
    iget-object v1, p0, Lmep;->a:Lokc;

    if-eqz v1, :cond_1

    .line 11529
    const/4 v1, 0x2

    iget-object v2, p0, Lmep;->a:Lokc;

    .line 11530
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11532
    :cond_1
    iget-object v1, p0, Lmep;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11533
    iput v0, p0, Lmep;->ai:I

    .line 11534
    return v0
.end method

.method public a(Loxn;)Lmep;
    .locals 2

    .prologue
    .line 11542
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 11543
    sparse-switch v0, :sswitch_data_0

    .line 11547
    iget-object v1, p0, Lmep;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 11548
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmep;->ah:Ljava/util/List;

    .line 11551
    :cond_1
    iget-object v1, p0, Lmep;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11553
    :sswitch_0
    return-object p0

    .line 11558
    :sswitch_1
    iget-object v0, p0, Lmep;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 11559
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmep;->apiHeader:Llyr;

    .line 11561
    :cond_2
    iget-object v0, p0, Lmep;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11565
    :sswitch_2
    iget-object v0, p0, Lmep;->a:Lokc;

    if-nez v0, :cond_3

    .line 11566
    new-instance v0, Lokc;

    invoke-direct {v0}, Lokc;-><init>()V

    iput-object v0, p0, Lmep;->a:Lokc;

    .line 11568
    :cond_3
    iget-object v0, p0, Lmep;->a:Lokc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 11543
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 11511
    iget-object v0, p0, Lmep;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 11512
    const/4 v0, 0x1

    iget-object v1, p0, Lmep;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11514
    :cond_0
    iget-object v0, p0, Lmep;->a:Lokc;

    if-eqz v0, :cond_1

    .line 11515
    const/4 v0, 0x2

    iget-object v1, p0, Lmep;->a:Lokc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 11517
    :cond_1
    iget-object v0, p0, Lmep;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 11519
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 11496
    invoke-virtual {p0, p1}, Lmep;->a(Loxn;)Lmep;

    move-result-object v0

    return-object v0
.end method
