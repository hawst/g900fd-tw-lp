.class public final Lqal;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field private c:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 481
    invoke-direct {p0}, Loxq;-><init>()V

    .line 484
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqal;->c:[Ljava/lang/String;

    .line 487
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqal;->a:[Ljava/lang/String;

    .line 490
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqal;->b:[Ljava/lang/String;

    .line 481
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 516
    .line 517
    iget-object v0, p0, Lqal;->c:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lqal;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 519
    iget-object v3, p0, Lqal;->c:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 521
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 519
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 524
    :cond_0
    iget-object v0, p0, Lqal;->c:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 526
    :goto_1
    iget-object v2, p0, Lqal;->a:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lqal;->a:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 528
    iget-object v4, p0, Lqal;->a:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 530
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 528
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 532
    :cond_1
    add-int/2addr v0, v3

    .line 533
    iget-object v2, p0, Lqal;->a:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 535
    :cond_2
    iget-object v2, p0, Lqal;->b:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lqal;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 537
    iget-object v3, p0, Lqal;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 539
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 537
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 541
    :cond_3
    add-int/2addr v0, v2

    .line 542
    iget-object v1, p0, Lqal;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 544
    :cond_4
    iget-object v1, p0, Lqal;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 545
    iput v0, p0, Lqal;->ai:I

    .line 546
    return v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lqal;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 554
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 555
    sparse-switch v0, :sswitch_data_0

    .line 559
    iget-object v1, p0, Lqal;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 560
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqal;->ah:Ljava/util/List;

    .line 563
    :cond_1
    iget-object v1, p0, Lqal;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 565
    :sswitch_0
    return-object p0

    .line 570
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 571
    iget-object v0, p0, Lqal;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 572
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 573
    iget-object v2, p0, Lqal;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 574
    iput-object v1, p0, Lqal;->c:[Ljava/lang/String;

    .line 575
    :goto_1
    iget-object v1, p0, Lqal;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 576
    iget-object v1, p0, Lqal;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 577
    invoke-virtual {p1}, Loxn;->a()I

    .line 575
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 580
    :cond_2
    iget-object v1, p0, Lqal;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 584
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 585
    iget-object v0, p0, Lqal;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 586
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 587
    iget-object v2, p0, Lqal;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 588
    iput-object v1, p0, Lqal;->a:[Ljava/lang/String;

    .line 589
    :goto_2
    iget-object v1, p0, Lqal;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 590
    iget-object v1, p0, Lqal;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 591
    invoke-virtual {p1}, Loxn;->a()I

    .line 589
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 594
    :cond_3
    iget-object v1, p0, Lqal;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 598
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 599
    iget-object v0, p0, Lqal;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 600
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 601
    iget-object v2, p0, Lqal;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 602
    iput-object v1, p0, Lqal;->b:[Ljava/lang/String;

    .line 603
    :goto_3
    iget-object v1, p0, Lqal;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 604
    iget-object v1, p0, Lqal;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 605
    invoke-virtual {p1}, Loxn;->a()I

    .line 603
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 608
    :cond_4
    iget-object v1, p0, Lqal;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 555
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 495
    iget-object v1, p0, Lqal;->c:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 496
    iget-object v2, p0, Lqal;->c:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 497
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 496
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 500
    :cond_0
    iget-object v1, p0, Lqal;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 501
    iget-object v2, p0, Lqal;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 502
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 501
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 505
    :cond_1
    iget-object v1, p0, Lqal;->b:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 506
    iget-object v1, p0, Lqal;->b:[Ljava/lang/String;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 507
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 506
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 510
    :cond_2
    iget-object v0, p0, Lqal;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 512
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 477
    invoke-virtual {p0, p1}, Lqal;->a(Loxn;)Lqal;

    move-result-object v0

    return-object v0
.end method
