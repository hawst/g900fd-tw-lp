.class public final Lmto;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:I

.field private k:Ljava/lang/Integer;

.field private l:I

.field private m:I

.field private n:Ljava/lang/Integer;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/Integer;

.field private q:I

.field private r:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 2148
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2163
    iput v0, p0, Lmto;->j:I

    .line 2170
    iput v0, p0, Lmto;->l:I

    .line 2173
    iput v0, p0, Lmto;->m:I

    .line 2186
    iput v0, p0, Lmto;->q:I

    .line 2148
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 2253
    const/4 v0, 0x0

    .line 2254
    iget-object v1, p0, Lmto;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 2255
    const/4 v0, 0x1

    iget-object v1, p0, Lmto;->d:Ljava/lang/Boolean;

    .line 2256
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 2258
    :cond_0
    iget-object v1, p0, Lmto;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 2259
    const/4 v1, 0x2

    iget-object v2, p0, Lmto;->e:Ljava/lang/Boolean;

    .line 2260
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2262
    :cond_1
    iget-object v1, p0, Lmto;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 2263
    const/4 v1, 0x3

    iget-object v2, p0, Lmto;->f:Ljava/lang/Boolean;

    .line 2264
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2266
    :cond_2
    iget-object v1, p0, Lmto;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2267
    const/4 v1, 0x4

    iget-object v2, p0, Lmto;->g:Ljava/lang/Integer;

    .line 2268
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2270
    :cond_3
    iget-object v1, p0, Lmto;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 2271
    const/4 v1, 0x5

    iget-object v2, p0, Lmto;->h:Ljava/lang/Integer;

    .line 2272
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2274
    :cond_4
    iget-object v1, p0, Lmto;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 2275
    const/4 v1, 0x6

    iget-object v2, p0, Lmto;->i:Ljava/lang/Integer;

    .line 2276
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2278
    :cond_5
    iget v1, p0, Lmto;->j:I

    if-eq v1, v3, :cond_6

    .line 2279
    const/4 v1, 0x7

    iget v2, p0, Lmto;->j:I

    .line 2280
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2282
    :cond_6
    iget-object v1, p0, Lmto;->a:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 2283
    const/16 v1, 0x8

    iget-object v2, p0, Lmto;->a:Ljava/lang/String;

    .line 2284
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2286
    :cond_7
    iget-object v1, p0, Lmto;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 2287
    const/16 v1, 0x9

    iget-object v2, p0, Lmto;->k:Ljava/lang/Integer;

    .line 2288
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2290
    :cond_8
    iget v1, p0, Lmto;->l:I

    if-eq v1, v3, :cond_9

    .line 2291
    const/16 v1, 0xa

    iget v2, p0, Lmto;->l:I

    .line 2292
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2294
    :cond_9
    iget v1, p0, Lmto;->m:I

    if-eq v1, v3, :cond_a

    .line 2295
    const/16 v1, 0xb

    iget v2, p0, Lmto;->m:I

    .line 2296
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2298
    :cond_a
    iget-object v1, p0, Lmto;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 2299
    const/16 v1, 0xc

    iget-object v2, p0, Lmto;->n:Ljava/lang/Integer;

    .line 2300
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2302
    :cond_b
    iget-object v1, p0, Lmto;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 2303
    const/16 v1, 0xd

    iget-object v2, p0, Lmto;->o:Ljava/lang/Integer;

    .line 2304
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2306
    :cond_c
    iget-object v1, p0, Lmto;->b:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 2307
    const/16 v1, 0xe

    iget-object v2, p0, Lmto;->b:Ljava/lang/String;

    .line 2308
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2310
    :cond_d
    iget-object v1, p0, Lmto;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 2311
    const/16 v1, 0xf

    iget-object v2, p0, Lmto;->c:Ljava/lang/Integer;

    .line 2312
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2314
    :cond_e
    iget-object v1, p0, Lmto;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 2315
    const/16 v1, 0x10

    iget-object v2, p0, Lmto;->p:Ljava/lang/Integer;

    .line 2316
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2318
    :cond_f
    iget v1, p0, Lmto;->q:I

    if-eq v1, v3, :cond_10

    .line 2319
    const/16 v1, 0x11

    iget v2, p0, Lmto;->q:I

    .line 2320
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2322
    :cond_10
    iget-object v1, p0, Lmto;->r:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    .line 2323
    const/16 v1, 0x12

    iget-object v2, p0, Lmto;->r:Ljava/lang/Integer;

    .line 2324
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2326
    :cond_11
    iget-object v1, p0, Lmto;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2327
    iput v0, p0, Lmto;->ai:I

    .line 2328
    return v0
.end method

.method public a(Loxn;)Lmto;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2336
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2337
    sparse-switch v0, :sswitch_data_0

    .line 2341
    iget-object v1, p0, Lmto;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2342
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmto;->ah:Ljava/util/List;

    .line 2345
    :cond_1
    iget-object v1, p0, Lmto;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2347
    :sswitch_0
    return-object p0

    .line 2352
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmto;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 2356
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmto;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 2360
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmto;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 2364
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmto;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 2368
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmto;->h:Ljava/lang/Integer;

    goto :goto_0

    .line 2372
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmto;->i:Ljava/lang/Integer;

    goto :goto_0

    .line 2376
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2377
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 2383
    :cond_2
    iput v0, p0, Lmto;->j:I

    goto :goto_0

    .line 2385
    :cond_3
    iput v2, p0, Lmto;->j:I

    goto :goto_0

    .line 2390
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmto;->a:Ljava/lang/String;

    goto :goto_0

    .line 2394
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmto;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2398
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2399
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/16 v1, 0x64

    if-eq v0, v1, :cond_4

    const/16 v1, 0x65

    if-eq v0, v1, :cond_4

    const/16 v1, 0x66

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xca

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xcd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xce

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x191

    if-eq v0, v1, :cond_4

    const/16 v1, 0x192

    if-eq v0, v1, :cond_4

    const/16 v1, 0x1f4

    if-eq v0, v1, :cond_4

    const/16 v1, 0x258

    if-eq v0, v1, :cond_4

    const/16 v1, 0x259

    if-eq v0, v1, :cond_4

    const/16 v1, 0x25a

    if-eq v0, v1, :cond_4

    const/16 v1, 0x25b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x25c

    if-eq v0, v1, :cond_4

    const/16 v1, 0x321

    if-eq v0, v1, :cond_4

    const/16 v1, 0x322

    if-eq v0, v1, :cond_4

    const/16 v1, 0x323

    if-eq v0, v1, :cond_4

    const/16 v1, 0x384

    if-ne v0, v1, :cond_5

    .line 2428
    :cond_4
    iput v0, p0, Lmto;->l:I

    goto/16 :goto_0

    .line 2430
    :cond_5
    iput v2, p0, Lmto;->l:I

    goto/16 :goto_0

    .line 2435
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2436
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-eq v0, v6, :cond_6

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    .line 2442
    :cond_6
    iput v0, p0, Lmto;->m:I

    goto/16 :goto_0

    .line 2444
    :cond_7
    iput v2, p0, Lmto;->m:I

    goto/16 :goto_0

    .line 2449
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmto;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2453
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmto;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2457
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmto;->b:Ljava/lang/String;

    goto/16 :goto_0

    .line 2461
    :sswitch_f
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmto;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2465
    :sswitch_10
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmto;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2469
    :sswitch_11
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2470
    if-eqz v0, :cond_8

    if-eq v0, v3, :cond_8

    if-eq v0, v4, :cond_8

    const/16 v1, 0x9

    if-eq v0, v1, :cond_8

    const/16 v1, 0xa

    if-eq v0, v1, :cond_8

    const/16 v1, 0xb

    if-eq v0, v1, :cond_8

    const/16 v1, 0xc

    if-eq v0, v1, :cond_8

    const/16 v1, 0xd

    if-eq v0, v1, :cond_8

    const/16 v1, 0xe

    if-eq v0, v1, :cond_8

    const/16 v1, 0x14

    if-eq v0, v1, :cond_8

    const/16 v1, 0x18

    if-eq v0, v1, :cond_8

    const/16 v1, 0x19

    if-eq v0, v1, :cond_8

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_8

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_8

    const/16 v1, 0x20

    if-eq v0, v1, :cond_8

    const/16 v1, 0x21

    if-eq v0, v1, :cond_8

    const/16 v1, 0x22

    if-eq v0, v1, :cond_8

    const/16 v1, 0x32

    if-eq v0, v1, :cond_8

    const/16 v1, 0x33

    if-eq v0, v1, :cond_8

    const/16 v1, 0x34

    if-eq v0, v1, :cond_8

    const/16 v1, 0x35

    if-eq v0, v1, :cond_8

    const/16 v1, 0x3c

    if-eq v0, v1, :cond_8

    const/16 v1, 0x46

    if-eq v0, v1, :cond_8

    const/16 v1, 0x47

    if-eq v0, v1, :cond_8

    const/16 v1, 0x50

    if-eq v0, v1, :cond_8

    const/16 v1, 0x29

    if-eq v0, v1, :cond_8

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_8

    const/16 v1, 0x5b

    if-eq v0, v1, :cond_8

    const/16 v1, 0x5c

    if-eq v0, v1, :cond_8

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_8

    const/16 v1, 0x5e

    if-eq v0, v1, :cond_8

    const/16 v1, 0x5f

    if-eq v0, v1, :cond_8

    const/16 v1, 0x60

    if-eq v0, v1, :cond_8

    const/16 v1, 0x61

    if-eq v0, v1, :cond_8

    const/16 v1, 0x62

    if-eq v0, v1, :cond_8

    const/16 v1, 0x64

    if-eq v0, v1, :cond_8

    const/16 v1, 0x65

    if-eq v0, v1, :cond_8

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_8

    const/16 v1, 0x78

    if-eq v0, v1, :cond_8

    const/16 v1, 0x79

    if-eq v0, v1, :cond_8

    const/16 v1, 0x82

    if-eq v0, v1, :cond_8

    const/16 v1, 0x83

    if-eq v0, v1, :cond_8

    const/16 v1, 0x84

    if-eq v0, v1, :cond_8

    const/16 v1, 0x85

    if-eq v0, v1, :cond_8

    const/16 v1, 0x86

    if-eq v0, v1, :cond_8

    const/16 v1, 0x87

    if-ne v0, v1, :cond_9

    .line 2516
    :cond_8
    iput v0, p0, Lmto;->q:I

    goto/16 :goto_0

    .line 2518
    :cond_9
    iput v2, p0, Lmto;->q:I

    goto/16 :goto_0

    .line 2523
    :sswitch_12
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmto;->r:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 2337
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x90 -> :sswitch_12
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 2193
    iget-object v0, p0, Lmto;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 2194
    const/4 v0, 0x1

    iget-object v1, p0, Lmto;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2196
    :cond_0
    iget-object v0, p0, Lmto;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 2197
    const/4 v0, 0x2

    iget-object v1, p0, Lmto;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2199
    :cond_1
    iget-object v0, p0, Lmto;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 2200
    const/4 v0, 0x3

    iget-object v1, p0, Lmto;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2202
    :cond_2
    iget-object v0, p0, Lmto;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2203
    const/4 v0, 0x4

    iget-object v1, p0, Lmto;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2205
    :cond_3
    iget-object v0, p0, Lmto;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2206
    const/4 v0, 0x5

    iget-object v1, p0, Lmto;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2208
    :cond_4
    iget-object v0, p0, Lmto;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 2209
    const/4 v0, 0x6

    iget-object v1, p0, Lmto;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2211
    :cond_5
    iget v0, p0, Lmto;->j:I

    if-eq v0, v2, :cond_6

    .line 2212
    const/4 v0, 0x7

    iget v1, p0, Lmto;->j:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2214
    :cond_6
    iget-object v0, p0, Lmto;->a:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2215
    const/16 v0, 0x8

    iget-object v1, p0, Lmto;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2217
    :cond_7
    iget-object v0, p0, Lmto;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 2218
    const/16 v0, 0x9

    iget-object v1, p0, Lmto;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2220
    :cond_8
    iget v0, p0, Lmto;->l:I

    if-eq v0, v2, :cond_9

    .line 2221
    const/16 v0, 0xa

    iget v1, p0, Lmto;->l:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2223
    :cond_9
    iget v0, p0, Lmto;->m:I

    if-eq v0, v2, :cond_a

    .line 2224
    const/16 v0, 0xb

    iget v1, p0, Lmto;->m:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2226
    :cond_a
    iget-object v0, p0, Lmto;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 2227
    const/16 v0, 0xc

    iget-object v1, p0, Lmto;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2229
    :cond_b
    iget-object v0, p0, Lmto;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 2230
    const/16 v0, 0xd

    iget-object v1, p0, Lmto;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2232
    :cond_c
    iget-object v0, p0, Lmto;->b:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 2233
    const/16 v0, 0xe

    iget-object v1, p0, Lmto;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2235
    :cond_d
    iget-object v0, p0, Lmto;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 2236
    const/16 v0, 0xf

    iget-object v1, p0, Lmto;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2238
    :cond_e
    iget-object v0, p0, Lmto;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 2239
    const/16 v0, 0x10

    iget-object v1, p0, Lmto;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2241
    :cond_f
    iget v0, p0, Lmto;->q:I

    if-eq v0, v2, :cond_10

    .line 2242
    const/16 v0, 0x11

    iget v1, p0, Lmto;->q:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2244
    :cond_10
    iget-object v0, p0, Lmto;->r:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 2245
    const/16 v0, 0x12

    iget-object v1, p0, Lmto;->r:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2247
    :cond_11
    iget-object v0, p0, Lmto;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2249
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2144
    invoke-virtual {p0, p1}, Lmto;->a(Loxn;)Lmto;

    move-result-object v0

    return-object v0
.end method
