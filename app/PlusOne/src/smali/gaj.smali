.class public final Lgaj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/views/PhotoViewPager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/PhotoViewPager;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 56
    :try_start_0
    iget-object v0, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(Lcom/google/android/apps/plus/views/PhotoViewPager;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 57
    iget-object v0, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(Lcom/google/android/apps/plus/views/PhotoViewPager;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    .line 59
    iget-object v1, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(Lcom/google/android/apps/plus/views/PhotoViewPager;)Landroid/widget/Scroller;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    iget-object v2, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-static {v2}, Lcom/google/android/apps/plus/views/PhotoViewPager;->b(Lcom/google/android/apps/plus/views/PhotoViewPager;)I

    move-result v2

    sub-int v2, v0, v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PhotoViewPager;->b(F)V

    .line 61
    iget-object v1, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->a(Lcom/google/android/apps/plus/views/PhotoViewPager;I)I

    .line 62
    iget-object v0, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    iget-object v1, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->c(Lcom/google/android/apps/plus/views/PhotoViewPager;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/PhotoViewPager;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 72
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->m()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 66
    :catch_0
    move-exception v0

    .line 67
    const-string v1, "PhotoViewPager"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 68
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "NPE when fake dragging\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    :cond_1
    iget-object v0, p0, Lgaj;->a:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->m()V

    goto :goto_0
.end method
