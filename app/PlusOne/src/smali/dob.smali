.class public final Ldob;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmko;",
        "Lmkp;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x5

    aput v2, v0, v1

    sput-object v0, Ldob;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 41
    const-string v3, "collectionupdate"

    new-instance v4, Lmko;

    invoke-direct {v4}, Lmko;-><init>()V

    new-instance v5, Lmkp;

    invoke-direct {v5}, Lmkp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 44
    iput-object p3, p0, Ldob;->r:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Ldob;->b:Ljava/lang/String;

    .line 46
    iput-object p5, p0, Ldob;->p:Ljava/lang/String;

    .line 47
    iput-object p6, p0, Ldob;->q:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method protected a(Lmko;)V
    .locals 3

    .prologue
    .line 52
    new-instance v0, Lnai;

    invoke-direct {v0}, Lnai;-><init>()V

    iput-object v0, p1, Lmko;->a:Lnai;

    .line 53
    iget-object v0, p1, Lmko;->a:Lnai;

    .line 54
    iget-object v1, p0, Ldob;->b:Ljava/lang/String;

    iget-object v2, p0, Ldob;->p:Ljava/lang/String;

    invoke-static {v1, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;)Lmzo;

    move-result-object v1

    iput-object v1, v0, Lnai;->a:Lmzo;

    .line 55
    sget-object v1, Ldob;->a:[I

    iput-object v1, v0, Lnai;->b:[I

    .line 56
    new-instance v1, Lnac;

    invoke-direct {v1}, Lnac;-><init>()V

    iput-object v1, v0, Lnai;->d:Lnac;

    .line 57
    iget-object v0, v0, Lnai;->d:Lnac;

    iget-object v1, p0, Ldob;->q:Ljava/lang/String;

    iput-object v1, v0, Lnac;->a:Ljava/lang/String;

    .line 58
    return-void
.end method

.method protected a(Lmkp;)V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p1, Lmkp;->a:Lnaj;

    .line 63
    iget-object v1, v0, Lnaj;->a:Lmzp;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lnaj;->a:Lmzp;

    iget-object v1, v1, Lmzp;->a:Lmzq;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lnaj;->a:Lmzp;

    iget-object v1, v1, Lmzp;->a:Lmzq;

    iget v1, v1, Lmzq;->a:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 65
    new-instance v1, Lkfm;

    const-string v2, "RenameAlbum failed: "

    iget-object v0, v0, Lnaj;->a:Lmzp;

    iget-object v0, v0, Lmzp;->a:Lmzq;

    iget-object v0, v0, Lmzq;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Lkfm;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_1
    iget-object v0, p0, Ldob;->f:Landroid/content/Context;

    iget v1, p0, Ldob;->c:I

    iget-object v2, p0, Ldob;->r:Ljava/lang/String;

    iget-object v3, p0, Ldob;->q:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmko;

    invoke-virtual {p0, p1}, Ldob;->a(Lmko;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmkp;

    invoke-virtual {p0, p1}, Ldob;->a(Lmkp;)V

    return-void
.end method
