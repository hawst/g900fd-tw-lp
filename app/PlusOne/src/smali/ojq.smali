.class public final Lojq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lohu;

.field public b:[Lohn;

.field public c:[Lohn;

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1309
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1312
    const/4 v0, 0x0

    iput-object v0, p0, Lojq;->a:Lohu;

    .line 1315
    sget-object v0, Lohn;->a:[Lohn;

    iput-object v0, p0, Lojq;->b:[Lohn;

    .line 1318
    sget-object v0, Lohn;->a:[Lohn;

    iput-object v0, p0, Lojq;->c:[Lohn;

    .line 1309
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1351
    .line 1352
    iget-object v0, p0, Lojq;->a:Lohu;

    if-eqz v0, :cond_5

    .line 1353
    const/4 v0, 0x1

    iget-object v2, p0, Lojq;->a:Lohu;

    .line 1354
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1356
    :goto_0
    iget-object v2, p0, Lojq;->b:[Lohn;

    if-eqz v2, :cond_1

    .line 1357
    iget-object v3, p0, Lojq;->b:[Lohn;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 1358
    if-eqz v5, :cond_0

    .line 1359
    const/4 v6, 0x2

    .line 1360
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1357
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1364
    :cond_1
    iget-object v2, p0, Lojq;->c:[Lohn;

    if-eqz v2, :cond_3

    .line 1365
    iget-object v2, p0, Lojq;->c:[Lohn;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 1366
    if-eqz v4, :cond_2

    .line 1367
    const/4 v5, 0x3

    .line 1368
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1365
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1372
    :cond_3
    iget-object v1, p0, Lojq;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1373
    const/4 v1, 0x4

    iget-object v2, p0, Lojq;->d:Ljava/lang/Boolean;

    .line 1374
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1376
    :cond_4
    iget-object v1, p0, Lojq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1377
    iput v0, p0, Lojq;->ai:I

    .line 1378
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lojq;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1386
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1387
    sparse-switch v0, :sswitch_data_0

    .line 1391
    iget-object v2, p0, Lojq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1392
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lojq;->ah:Ljava/util/List;

    .line 1395
    :cond_1
    iget-object v2, p0, Lojq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1397
    :sswitch_0
    return-object p0

    .line 1402
    :sswitch_1
    iget-object v0, p0, Lojq;->a:Lohu;

    if-nez v0, :cond_2

    .line 1403
    new-instance v0, Lohu;

    invoke-direct {v0}, Lohu;-><init>()V

    iput-object v0, p0, Lojq;->a:Lohu;

    .line 1405
    :cond_2
    iget-object v0, p0, Lojq;->a:Lohu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1409
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1410
    iget-object v0, p0, Lojq;->b:[Lohn;

    if-nez v0, :cond_4

    move v0, v1

    .line 1411
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohn;

    .line 1412
    iget-object v3, p0, Lojq;->b:[Lohn;

    if-eqz v3, :cond_3

    .line 1413
    iget-object v3, p0, Lojq;->b:[Lohn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1415
    :cond_3
    iput-object v2, p0, Lojq;->b:[Lohn;

    .line 1416
    :goto_2
    iget-object v2, p0, Lojq;->b:[Lohn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1417
    iget-object v2, p0, Lojq;->b:[Lohn;

    new-instance v3, Lohn;

    invoke-direct {v3}, Lohn;-><init>()V

    aput-object v3, v2, v0

    .line 1418
    iget-object v2, p0, Lojq;->b:[Lohn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1419
    invoke-virtual {p1}, Loxn;->a()I

    .line 1416
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1410
    :cond_4
    iget-object v0, p0, Lojq;->b:[Lohn;

    array-length v0, v0

    goto :goto_1

    .line 1422
    :cond_5
    iget-object v2, p0, Lojq;->b:[Lohn;

    new-instance v3, Lohn;

    invoke-direct {v3}, Lohn;-><init>()V

    aput-object v3, v2, v0

    .line 1423
    iget-object v2, p0, Lojq;->b:[Lohn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1427
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1428
    iget-object v0, p0, Lojq;->c:[Lohn;

    if-nez v0, :cond_7

    move v0, v1

    .line 1429
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lohn;

    .line 1430
    iget-object v3, p0, Lojq;->c:[Lohn;

    if-eqz v3, :cond_6

    .line 1431
    iget-object v3, p0, Lojq;->c:[Lohn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1433
    :cond_6
    iput-object v2, p0, Lojq;->c:[Lohn;

    .line 1434
    :goto_4
    iget-object v2, p0, Lojq;->c:[Lohn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 1435
    iget-object v2, p0, Lojq;->c:[Lohn;

    new-instance v3, Lohn;

    invoke-direct {v3}, Lohn;-><init>()V

    aput-object v3, v2, v0

    .line 1436
    iget-object v2, p0, Lojq;->c:[Lohn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1437
    invoke-virtual {p1}, Loxn;->a()I

    .line 1434
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1428
    :cond_7
    iget-object v0, p0, Lojq;->c:[Lohn;

    array-length v0, v0

    goto :goto_3

    .line 1440
    :cond_8
    iget-object v2, p0, Lojq;->c:[Lohn;

    new-instance v3, Lohn;

    invoke-direct {v3}, Lohn;-><init>()V

    aput-object v3, v2, v0

    .line 1441
    iget-object v2, p0, Lojq;->c:[Lohn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1445
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lojq;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1387
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1325
    iget-object v1, p0, Lojq;->a:Lohu;

    if-eqz v1, :cond_0

    .line 1326
    const/4 v1, 0x1

    iget-object v2, p0, Lojq;->a:Lohu;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1328
    :cond_0
    iget-object v1, p0, Lojq;->b:[Lohn;

    if-eqz v1, :cond_2

    .line 1329
    iget-object v2, p0, Lojq;->b:[Lohn;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1330
    if-eqz v4, :cond_1

    .line 1331
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1329
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1335
    :cond_2
    iget-object v1, p0, Lojq;->c:[Lohn;

    if-eqz v1, :cond_4

    .line 1336
    iget-object v1, p0, Lojq;->c:[Lohn;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 1337
    if-eqz v3, :cond_3

    .line 1338
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1336
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1342
    :cond_4
    iget-object v0, p0, Lojq;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 1343
    const/4 v0, 0x4

    iget-object v1, p0, Lojq;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1345
    :cond_5
    iget-object v0, p0, Lojq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1347
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1305
    invoke-virtual {p0, p1}, Lojq;->a(Loxn;)Lojq;

    move-result-object v0

    return-object v0
.end method
