.class public abstract Ljfb;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljfb;

.field public static b:Ljfb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    new-instance v0, Ljfc;

    invoke-direct {v0}, Ljfc;-><init>()V

    sput-object v0, Ljfb;->a:Ljfb;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 435
    return-void
.end method

.method public static f()Ljfb;
    .locals 1

    .prologue
    .line 425
    sget-object v0, Ljfb;->b:Ljfb;

    if-eqz v0, :cond_0

    sget-object v0, Ljfb;->b:Ljfb;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljfb;->a:Ljfb;

    goto :goto_0
.end method


# virtual methods
.method public abstract a(I)Landroid/content/Intent;
.end method

.method public abstract a(Landroid/net/Uri;I)Landroid/content/Intent;
.end method

.method public abstract a(Ljdz;Landroid/net/Uri;I)Landroid/content/Intent;
.end method

.method public abstract a(Z)Landroid/content/Intent;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/Long;)Landroid/graphics/Bitmap;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract a(Landroid/content/Intent;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljeb;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Ljed;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljed;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/Iterable;Lkfd;)Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljdz;",
            ">;",
            "Lkfd;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljdz;",
            "Ljej;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;J)Ljfd;
.end method

.method public abstract a(Ljdz;Lllx;)Lllt;
.end method

.method public abstract a(ILjeo;)V
.end method

.method public abstract a(Ljdz;)V
.end method

.method public abstract a(Ljed;Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract a(Landroid/net/Uri;)Z
.end method

.method public abstract b()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Ljed;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljed;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract b(I)Z
.end method

.method public abstract b(Landroid/net/Uri;)Z
.end method

.method public abstract c()Z
.end method

.method public abstract c(Landroid/net/Uri;)Z
.end method

.method public abstract d(Landroid/net/Uri;)Landroid/net/Uri;
.end method

.method public abstract d()Z
.end method

.method public abstract e(Landroid/net/Uri;)Landroid/net/Uri;
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f(Landroid/net/Uri;)J
.end method
