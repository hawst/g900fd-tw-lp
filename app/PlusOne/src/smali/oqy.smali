.class public final Loqy;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:[Losf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 739
    invoke-direct {p0}, Loxq;-><init>()V

    .line 744
    sget-object v0, Losf;->a:[Losf;

    iput-object v0, p0, Loqy;->b:[Losf;

    .line 739
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 765
    .line 766
    iget-object v0, p0, Loqy;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 767
    const/4 v0, 0x1

    iget-object v2, p0, Loqy;->a:Ljava/lang/Integer;

    .line 768
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 770
    :goto_0
    iget-object v2, p0, Loqy;->b:[Losf;

    if-eqz v2, :cond_1

    .line 771
    iget-object v2, p0, Loqy;->b:[Losf;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 772
    if-eqz v4, :cond_0

    .line 773
    const/4 v5, 0x2

    .line 774
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 771
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 778
    :cond_1
    iget-object v1, p0, Loqy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 779
    iput v0, p0, Loqy;->ai:I

    .line 780
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loqy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 788
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 789
    sparse-switch v0, :sswitch_data_0

    .line 793
    iget-object v2, p0, Loqy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 794
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loqy;->ah:Ljava/util/List;

    .line 797
    :cond_1
    iget-object v2, p0, Loqy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 799
    :sswitch_0
    return-object p0

    .line 804
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loqy;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 808
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 809
    iget-object v0, p0, Loqy;->b:[Losf;

    if-nez v0, :cond_3

    move v0, v1

    .line 810
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Losf;

    .line 811
    iget-object v3, p0, Loqy;->b:[Losf;

    if-eqz v3, :cond_2

    .line 812
    iget-object v3, p0, Loqy;->b:[Losf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 814
    :cond_2
    iput-object v2, p0, Loqy;->b:[Losf;

    .line 815
    :goto_2
    iget-object v2, p0, Loqy;->b:[Losf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 816
    iget-object v2, p0, Loqy;->b:[Losf;

    new-instance v3, Losf;

    invoke-direct {v3}, Losf;-><init>()V

    aput-object v3, v2, v0

    .line 817
    iget-object v2, p0, Loqy;->b:[Losf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 818
    invoke-virtual {p1}, Loxn;->a()I

    .line 815
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 809
    :cond_3
    iget-object v0, p0, Loqy;->b:[Losf;

    array-length v0, v0

    goto :goto_1

    .line 821
    :cond_4
    iget-object v2, p0, Loqy;->b:[Losf;

    new-instance v3, Losf;

    invoke-direct {v3}, Losf;-><init>()V

    aput-object v3, v2, v0

    .line 822
    iget-object v2, p0, Loqy;->b:[Losf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 789
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 749
    iget-object v0, p0, Loqy;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 750
    const/4 v0, 0x1

    iget-object v1, p0, Loqy;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 752
    :cond_0
    iget-object v0, p0, Loqy;->b:[Losf;

    if-eqz v0, :cond_2

    .line 753
    iget-object v1, p0, Loqy;->b:[Losf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 754
    if-eqz v3, :cond_1

    .line 755
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 753
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 759
    :cond_2
    iget-object v0, p0, Loqy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 761
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 735
    invoke-virtual {p0, p1}, Loqy;->a(Loxn;)Loqy;

    move-result-object v0

    return-object v0
.end method
