.class public final Lodt;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lodv;

.field private b:[Lodu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 313
    invoke-direct {p0}, Loxq;-><init>()V

    .line 316
    sget-object v0, Lodu;->a:[Lodu;

    iput-object v0, p0, Lodt;->b:[Lodu;

    .line 319
    sget-object v0, Lodv;->a:[Lodv;

    iput-object v0, p0, Lodt;->a:[Lodv;

    .line 313
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 344
    .line 345
    iget-object v0, p0, Lodt;->b:[Lodu;

    if-eqz v0, :cond_1

    .line 346
    iget-object v3, p0, Lodt;->b:[Lodu;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 347
    if-eqz v5, :cond_0

    .line 348
    const/4 v6, 0x1

    .line 349
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 346
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 353
    :cond_2
    iget-object v2, p0, Lodt;->a:[Lodv;

    if-eqz v2, :cond_4

    .line 354
    iget-object v2, p0, Lodt;->a:[Lodv;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 355
    if-eqz v4, :cond_3

    .line 356
    const/4 v5, 0x2

    .line 357
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 354
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 361
    :cond_4
    iget-object v1, p0, Lodt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    iput v0, p0, Lodt;->ai:I

    .line 363
    return v0
.end method

.method public a(Loxn;)Lodt;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 371
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 372
    sparse-switch v0, :sswitch_data_0

    .line 376
    iget-object v2, p0, Lodt;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 377
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lodt;->ah:Ljava/util/List;

    .line 380
    :cond_1
    iget-object v2, p0, Lodt;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 382
    :sswitch_0
    return-object p0

    .line 387
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 388
    iget-object v0, p0, Lodt;->b:[Lodu;

    if-nez v0, :cond_3

    move v0, v1

    .line 389
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lodu;

    .line 390
    iget-object v3, p0, Lodt;->b:[Lodu;

    if-eqz v3, :cond_2

    .line 391
    iget-object v3, p0, Lodt;->b:[Lodu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 393
    :cond_2
    iput-object v2, p0, Lodt;->b:[Lodu;

    .line 394
    :goto_2
    iget-object v2, p0, Lodt;->b:[Lodu;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 395
    iget-object v2, p0, Lodt;->b:[Lodu;

    new-instance v3, Lodu;

    invoke-direct {v3}, Lodu;-><init>()V

    aput-object v3, v2, v0

    .line 396
    iget-object v2, p0, Lodt;->b:[Lodu;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 397
    invoke-virtual {p1}, Loxn;->a()I

    .line 394
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 388
    :cond_3
    iget-object v0, p0, Lodt;->b:[Lodu;

    array-length v0, v0

    goto :goto_1

    .line 400
    :cond_4
    iget-object v2, p0, Lodt;->b:[Lodu;

    new-instance v3, Lodu;

    invoke-direct {v3}, Lodu;-><init>()V

    aput-object v3, v2, v0

    .line 401
    iget-object v2, p0, Lodt;->b:[Lodu;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 405
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 406
    iget-object v0, p0, Lodt;->a:[Lodv;

    if-nez v0, :cond_6

    move v0, v1

    .line 407
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lodv;

    .line 408
    iget-object v3, p0, Lodt;->a:[Lodv;

    if-eqz v3, :cond_5

    .line 409
    iget-object v3, p0, Lodt;->a:[Lodv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 411
    :cond_5
    iput-object v2, p0, Lodt;->a:[Lodv;

    .line 412
    :goto_4
    iget-object v2, p0, Lodt;->a:[Lodv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 413
    iget-object v2, p0, Lodt;->a:[Lodv;

    new-instance v3, Lodv;

    invoke-direct {v3}, Lodv;-><init>()V

    aput-object v3, v2, v0

    .line 414
    iget-object v2, p0, Lodt;->a:[Lodv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 415
    invoke-virtual {p1}, Loxn;->a()I

    .line 412
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 406
    :cond_6
    iget-object v0, p0, Lodt;->a:[Lodv;

    array-length v0, v0

    goto :goto_3

    .line 418
    :cond_7
    iget-object v2, p0, Lodt;->a:[Lodv;

    new-instance v3, Lodv;

    invoke-direct {v3}, Lodv;-><init>()V

    aput-object v3, v2, v0

    .line 419
    iget-object v2, p0, Lodt;->a:[Lodv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 372
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 324
    iget-object v1, p0, Lodt;->b:[Lodu;

    if-eqz v1, :cond_1

    .line 325
    iget-object v2, p0, Lodt;->b:[Lodu;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 326
    if-eqz v4, :cond_0

    .line 327
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 325
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 331
    :cond_1
    iget-object v1, p0, Lodt;->a:[Lodv;

    if-eqz v1, :cond_3

    .line 332
    iget-object v1, p0, Lodt;->a:[Lodv;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 333
    if-eqz v3, :cond_2

    .line 334
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 332
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 338
    :cond_3
    iget-object v0, p0, Lodt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 340
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0, p1}, Lodt;->a(Loxn;)Lodt;

    move-result-object v0

    return-object v0
.end method
