.class public final Lofp;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Lofn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2205
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2208
    const/high16 v0, -0x80000000

    iput v0, p0, Lofp;->a:I

    .line 2211
    const/4 v0, 0x0

    iput-object v0, p0, Lofp;->b:Lofn;

    .line 2205
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2228
    const/4 v0, 0x0

    .line 2229
    iget v1, p0, Lofp;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 2230
    const/4 v0, 0x1

    iget v1, p0, Lofp;->a:I

    .line 2231
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2233
    :cond_0
    iget-object v1, p0, Lofp;->b:Lofn;

    if-eqz v1, :cond_1

    .line 2234
    const/4 v1, 0x2

    iget-object v2, p0, Lofp;->b:Lofn;

    .line 2235
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2237
    :cond_1
    iget-object v1, p0, Lofp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2238
    iput v0, p0, Lofp;->ai:I

    .line 2239
    return v0
.end method

.method public a(Loxn;)Lofp;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2247
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2248
    sparse-switch v0, :sswitch_data_0

    .line 2252
    iget-object v1, p0, Lofp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofp;->ah:Ljava/util/List;

    .line 2256
    :cond_1
    iget-object v1, p0, Lofp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2258
    :sswitch_0
    return-object p0

    .line 2263
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2264
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 2268
    :cond_2
    iput v0, p0, Lofp;->a:I

    goto :goto_0

    .line 2270
    :cond_3
    iput v2, p0, Lofp;->a:I

    goto :goto_0

    .line 2275
    :sswitch_2
    iget-object v0, p0, Lofp;->b:Lofn;

    if-nez v0, :cond_4

    .line 2276
    new-instance v0, Lofn;

    invoke-direct {v0}, Lofn;-><init>()V

    iput-object v0, p0, Lofp;->b:Lofn;

    .line 2278
    :cond_4
    iget-object v0, p0, Lofp;->b:Lofn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2248
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2216
    iget v0, p0, Lofp;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2217
    const/4 v0, 0x1

    iget v1, p0, Lofp;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2219
    :cond_0
    iget-object v0, p0, Lofp;->b:Lofn;

    if-eqz v0, :cond_1

    .line 2220
    const/4 v0, 0x2

    iget-object v1, p0, Lofp;->b:Lofn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2222
    :cond_1
    iget-object v0, p0, Lofp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2224
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2201
    invoke-virtual {p0, p1}, Lofp;->a(Loxn;)Lofp;

    move-result-object v0

    return-object v0
.end method
