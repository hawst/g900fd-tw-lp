.class public final enum Ldsk;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ldsk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ldsk;

.field public static final enum b:Ldsk;

.field public static final enum c:Ldsk;

.field public static final enum d:Ldsk;

.field public static final enum e:Ldsk;

.field public static final enum f:Ldsk;

.field public static final enum g:Ldsk;

.field public static final enum h:Ldsk;

.field private static final synthetic i:[Ldsk;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 102
    new-instance v0, Ldsk;

    const-string v1, "FETCH_UNREAD_HIGH_MORE"

    invoke-direct {v0, v1, v3}, Ldsk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsk;->a:Ldsk;

    .line 103
    new-instance v0, Ldsk;

    const-string v1, "FETCH_UNREAD_LOW_MORE"

    invoke-direct {v0, v1, v4}, Ldsk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsk;->b:Ldsk;

    .line 104
    new-instance v0, Ldsk;

    const-string v1, "FETCH_READ_HIGH_MORE"

    invoke-direct {v0, v1, v5}, Ldsk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsk;->c:Ldsk;

    .line 105
    new-instance v0, Ldsk;

    const-string v1, "FETCH_READ_LOW_MORE"

    invoke-direct {v0, v1, v6}, Ldsk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsk;->d:Ldsk;

    .line 106
    new-instance v0, Ldsk;

    const-string v1, "FETCH_UNREAD_HIGH"

    invoke-direct {v0, v1, v7}, Ldsk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsk;->e:Ldsk;

    .line 107
    new-instance v0, Ldsk;

    const-string v1, "FETCH_UNREAD_LOW"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldsk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsk;->f:Ldsk;

    .line 108
    new-instance v0, Ldsk;

    const-string v1, "FETCH_READ_HIGH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ldsk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsk;->g:Ldsk;

    .line 109
    new-instance v0, Ldsk;

    const-string v1, "FETCH_READ_LOW"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ldsk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldsk;->h:Ldsk;

    .line 101
    const/16 v0, 0x8

    new-array v0, v0, [Ldsk;

    sget-object v1, Ldsk;->a:Ldsk;

    aput-object v1, v0, v3

    sget-object v1, Ldsk;->b:Ldsk;

    aput-object v1, v0, v4

    sget-object v1, Ldsk;->c:Ldsk;

    aput-object v1, v0, v5

    sget-object v1, Ldsk;->d:Ldsk;

    aput-object v1, v0, v6

    sget-object v1, Ldsk;->e:Ldsk;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldsk;->f:Ldsk;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldsk;->g:Ldsk;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldsk;->h:Ldsk;

    aput-object v2, v0, v1

    sput-object v0, Ldsk;->i:[Ldsk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldsk;
    .locals 1

    .prologue
    .line 101
    const-class v0, Ldsk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldsk;

    return-object v0
.end method

.method public static values()[Ldsk;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Ldsk;->i:[Ldsk;

    invoke-virtual {v0}, [Ldsk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldsk;

    return-object v0
.end method
