.class public final Lohc;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lohb;

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 41
    iput v1, p0, Lohc;->a:I

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lohc;->b:Lohb;

    .line 49
    iput v1, p0, Lohc;->d:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 72
    const/4 v0, 0x0

    .line 73
    iget v1, p0, Lohc;->a:I

    if-eq v1, v3, :cond_0

    .line 74
    const/4 v0, 0x1

    iget v1, p0, Lohc;->a:I

    .line 75
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 77
    :cond_0
    iget-object v1, p0, Lohc;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 78
    const/4 v1, 0x2

    iget-object v2, p0, Lohc;->c:Ljava/lang/String;

    .line 79
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    :cond_1
    iget-object v1, p0, Lohc;->b:Lohb;

    if-eqz v1, :cond_2

    .line 82
    const/4 v1, 0x3

    iget-object v2, p0, Lohc;->b:Lohb;

    .line 83
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_2
    iget v1, p0, Lohc;->d:I

    if-eq v1, v3, :cond_3

    .line 86
    const/4 v1, 0x4

    iget v2, p0, Lohc;->d:I

    .line 87
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_3
    iget-object v1, p0, Lohc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    iput v0, p0, Lohc;->ai:I

    .line 91
    return v0
.end method

.method public a(Loxn;)Lohc;
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 99
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 100
    sparse-switch v0, :sswitch_data_0

    .line 104
    iget-object v1, p0, Lohc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohc;->ah:Ljava/util/List;

    .line 108
    :cond_1
    iget-object v1, p0, Lohc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    :sswitch_0
    return-object p0

    .line 115
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 116
    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x16

    if-eq v0, v1, :cond_2

    const/16 v1, 0x17

    if-eq v0, v1, :cond_2

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    const/16 v1, 0x19

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_3

    .line 141
    :cond_2
    iput v0, p0, Lohc;->a:I

    goto :goto_0

    .line 143
    :cond_3
    iput v2, p0, Lohc;->a:I

    goto :goto_0

    .line 148
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohc;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 152
    :sswitch_3
    iget-object v0, p0, Lohc;->b:Lohb;

    if-nez v0, :cond_4

    .line 153
    new-instance v0, Lohb;

    invoke-direct {v0}, Lohb;-><init>()V

    iput-object v0, p0, Lohc;->b:Lohb;

    .line 155
    :cond_4
    iget-object v0, p0, Lohc;->b:Lohb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 159
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 160
    if-eq v0, v2, :cond_5

    if-eq v0, v3, :cond_5

    if-eq v0, v4, :cond_5

    if-eq v0, v5, :cond_5

    if-eq v0, v6, :cond_5

    const/4 v1, 0x6

    if-eq v0, v1, :cond_5

    const/4 v1, 0x7

    if-eq v0, v1, :cond_5

    const/16 v1, 0x8

    if-eq v0, v1, :cond_5

    const/16 v1, 0x9

    if-eq v0, v1, :cond_5

    const/16 v1, 0xa

    if-eq v0, v1, :cond_5

    const/16 v1, 0xb

    if-eq v0, v1, :cond_5

    const/16 v1, 0xc

    if-eq v0, v1, :cond_5

    const/16 v1, 0xd

    if-eq v0, v1, :cond_5

    const/16 v1, 0xe

    if-eq v0, v1, :cond_5

    const/16 v1, 0xf

    if-eq v0, v1, :cond_5

    const/16 v1, 0x11

    if-eq v0, v1, :cond_5

    const/16 v1, 0x12

    if-eq v0, v1, :cond_5

    const/16 v1, 0x13

    if-eq v0, v1, :cond_5

    const/16 v1, 0x14

    if-eq v0, v1, :cond_5

    const/16 v1, 0x15

    if-eq v0, v1, :cond_5

    const/16 v1, 0x16

    if-eq v0, v1, :cond_5

    const/16 v1, 0x17

    if-eq v0, v1, :cond_5

    const/16 v1, 0x18

    if-eq v0, v1, :cond_5

    const/16 v1, 0x19

    if-eq v0, v1, :cond_5

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_6

    .line 185
    :cond_5
    iput v0, p0, Lohc;->d:I

    goto/16 :goto_0

    .line 187
    :cond_6
    iput v2, p0, Lohc;->d:I

    goto/16 :goto_0

    .line 100
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 54
    iget v0, p0, Lohc;->a:I

    if-eq v0, v2, :cond_0

    .line 55
    const/4 v0, 0x1

    iget v1, p0, Lohc;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 57
    :cond_0
    iget-object v0, p0, Lohc;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 58
    const/4 v0, 0x2

    iget-object v1, p0, Lohc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 60
    :cond_1
    iget-object v0, p0, Lohc;->b:Lohb;

    if-eqz v0, :cond_2

    .line 61
    const/4 v0, 0x3

    iget-object v1, p0, Lohc;->b:Lohb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 63
    :cond_2
    iget v0, p0, Lohc;->d:I

    if-eq v0, v2, :cond_3

    .line 64
    const/4 v0, 0x4

    iget v1, p0, Lohc;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 66
    :cond_3
    iget-object v0, p0, Lohc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 68
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lohc;->a(Loxn;)Lohc;

    move-result-object v0

    return-object v0
.end method
