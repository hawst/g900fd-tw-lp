.class public final Lnes;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lnzi;

.field public d:Ljava/lang/Boolean;

.field public e:Lnea;

.field public f:Ljava/lang/Boolean;

.field private g:I

.field private h:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2950
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2957
    iput-object v1, p0, Lnes;->c:Lnzi;

    .line 2962
    const/high16 v0, -0x80000000

    iput v0, p0, Lnes;->g:I

    .line 2967
    iput-object v1, p0, Lnes;->e:Lnea;

    .line 2950
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3004
    const/4 v0, 0x0

    .line 3005
    iget-object v1, p0, Lnes;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3006
    const/4 v0, 0x1

    iget-object v1, p0, Lnes;->a:Ljava/lang/String;

    .line 3007
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3009
    :cond_0
    iget-object v1, p0, Lnes;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3010
    const/4 v1, 0x2

    iget-object v2, p0, Lnes;->b:Ljava/lang/String;

    .line 3011
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3013
    :cond_1
    iget-object v1, p0, Lnes;->c:Lnzi;

    if-eqz v1, :cond_2

    .line 3014
    const/4 v1, 0x3

    iget-object v2, p0, Lnes;->c:Lnzi;

    .line 3015
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3017
    :cond_2
    iget-object v1, p0, Lnes;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 3018
    const/4 v1, 0x4

    iget-object v2, p0, Lnes;->d:Ljava/lang/Boolean;

    .line 3019
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3021
    :cond_3
    iget v1, p0, Lnes;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 3022
    const/4 v1, 0x5

    iget v2, p0, Lnes;->g:I

    .line 3023
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3025
    :cond_4
    iget-object v1, p0, Lnes;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 3026
    const/4 v1, 0x6

    iget-object v2, p0, Lnes;->h:Ljava/lang/Boolean;

    .line 3027
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3029
    :cond_5
    iget-object v1, p0, Lnes;->e:Lnea;

    if-eqz v1, :cond_6

    .line 3030
    const/4 v1, 0x7

    iget-object v2, p0, Lnes;->e:Lnea;

    .line 3031
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3033
    :cond_6
    iget-object v1, p0, Lnes;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 3034
    const/16 v1, 0x8

    iget-object v2, p0, Lnes;->f:Ljava/lang/Boolean;

    .line 3035
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3037
    :cond_7
    iget-object v1, p0, Lnes;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3038
    iput v0, p0, Lnes;->ai:I

    .line 3039
    return v0
.end method

.method public a(Loxn;)Lnes;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3047
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3048
    sparse-switch v0, :sswitch_data_0

    .line 3052
    iget-object v1, p0, Lnes;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3053
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnes;->ah:Ljava/util/List;

    .line 3056
    :cond_1
    iget-object v1, p0, Lnes;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3058
    :sswitch_0
    return-object p0

    .line 3063
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnes;->a:Ljava/lang/String;

    goto :goto_0

    .line 3067
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnes;->b:Ljava/lang/String;

    goto :goto_0

    .line 3071
    :sswitch_3
    iget-object v0, p0, Lnes;->c:Lnzi;

    if-nez v0, :cond_2

    .line 3072
    new-instance v0, Lnzi;

    invoke-direct {v0}, Lnzi;-><init>()V

    iput-object v0, p0, Lnes;->c:Lnzi;

    .line 3074
    :cond_2
    iget-object v0, p0, Lnes;->c:Lnzi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3078
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnes;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 3082
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3083
    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 3085
    :cond_3
    iput v0, p0, Lnes;->g:I

    goto :goto_0

    .line 3087
    :cond_4
    iput v2, p0, Lnes;->g:I

    goto :goto_0

    .line 3092
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnes;->h:Ljava/lang/Boolean;

    goto :goto_0

    .line 3096
    :sswitch_7
    iget-object v0, p0, Lnes;->e:Lnea;

    if-nez v0, :cond_5

    .line 3097
    new-instance v0, Lnea;

    invoke-direct {v0}, Lnea;-><init>()V

    iput-object v0, p0, Lnes;->e:Lnea;

    .line 3099
    :cond_5
    iget-object v0, p0, Lnes;->e:Lnea;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3103
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnes;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 3048
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2974
    iget-object v0, p0, Lnes;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2975
    const/4 v0, 0x1

    iget-object v1, p0, Lnes;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2977
    :cond_0
    iget-object v0, p0, Lnes;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2978
    const/4 v0, 0x2

    iget-object v1, p0, Lnes;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2980
    :cond_1
    iget-object v0, p0, Lnes;->c:Lnzi;

    if-eqz v0, :cond_2

    .line 2981
    const/4 v0, 0x3

    iget-object v1, p0, Lnes;->c:Lnzi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2983
    :cond_2
    iget-object v0, p0, Lnes;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2984
    const/4 v0, 0x4

    iget-object v1, p0, Lnes;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2986
    :cond_3
    iget v0, p0, Lnes;->g:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 2987
    const/4 v0, 0x5

    iget v1, p0, Lnes;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2989
    :cond_4
    iget-object v0, p0, Lnes;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 2990
    const/4 v0, 0x6

    iget-object v1, p0, Lnes;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2992
    :cond_5
    iget-object v0, p0, Lnes;->e:Lnea;

    if-eqz v0, :cond_6

    .line 2993
    const/4 v0, 0x7

    iget-object v1, p0, Lnes;->e:Lnea;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2995
    :cond_6
    iget-object v0, p0, Lnes;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 2996
    const/16 v0, 0x8

    iget-object v1, p0, Lnes;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2998
    :cond_7
    iget-object v0, p0, Lnes;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3000
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2946
    invoke-virtual {p0, p1}, Lnes;->a(Loxn;)Lnes;

    move-result-object v0

    return-object v0
.end method
