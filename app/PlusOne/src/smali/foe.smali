.class final Lfoe;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public c:I

.field public d:Z

.field public final e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZI)V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    iput-object p1, p0, Lfoe;->a:Ljava/lang/String;

    .line 211
    if-nez p2, :cond_0

    const-string p2, ""

    :cond_0
    iput-object p2, p0, Lfoe;->b:Ljava/lang/String;

    .line 212
    iput p3, p0, Lfoe;->c:I

    .line 213
    iput-boolean p4, p0, Lfoe;->d:Z

    .line 214
    iput p5, p0, Lfoe;->e:I

    .line 215
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 228
    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 219
    instance-of v0, p1, Lfoe;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lfoe;->a:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lfoe;

    iget-object v0, v0, Lfoe;->a:Ljava/lang/String;

    .line 220
    invoke-static {v1, v0}, Lfoe;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lfoe;->b:Ljava/lang/String;

    move-object v0, p1

    check-cast v0, Lfoe;

    iget-object v0, v0, Lfoe;->b:Ljava/lang/String;

    .line 221
    invoke-static {v1, v0}, Lfoe;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lfoe;->c:I

    move-object v0, p1

    check-cast v0, Lfoe;

    iget v0, v0, Lfoe;->c:I

    if-ne v1, v0, :cond_0

    iget-boolean v1, p0, Lfoe;->d:Z

    move-object v0, p1

    check-cast v0, Lfoe;

    iget-boolean v0, v0, Lfoe;->d:Z

    if-ne v1, v0, :cond_0

    iget v0, p0, Lfoe;->e:I

    check-cast p1, Lfoe;

    iget v1, p1, Lfoe;->e:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lfoe;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, Lfoe;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget v1, p0, Lfoe;->e:I

    xor-int/2addr v0, v1

    return v0
.end method
