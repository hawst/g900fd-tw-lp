.class public final Lhwb;
.super Landroid/widget/TextView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lhve;

.field private b:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {p0}, Lhwb;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 49
    invoke-static {v0}, Lhve;->a(Landroid/content/Context;)Lhve;

    move-result-object v0

    iput-object v0, p0, Lhwb;->a:Lhve;

    .line 51
    const v0, 0x7f020416

    invoke-virtual {p0, v0}, Lhwb;->setBackgroundResource(I)V

    .line 54
    invoke-virtual {p0, v1}, Lhwb;->setMinimumHeight(I)V

    .line 55
    invoke-virtual {p0, v1}, Lhwb;->setMinimumWidth(I)V

    .line 56
    invoke-virtual {p0, v1}, Lhwb;->setMinHeight(I)V

    .line 57
    invoke-virtual {p0, v1}, Lhwb;->setMinWidth(I)V

    .line 34
    return-void
.end method


# virtual methods
.method public a(Lhux;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 75
    iput-object v3, p0, Lhwb;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lhwb;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x19

    invoke-static {v0, p0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    invoke-virtual {p0, v2, v2, v2, v2}, Lhwb;->setPadding(IIII)V

    invoke-virtual {p0, v3}, Lhwb;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v3}, Lhwb;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v3}, Lhwb;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v2}, Lhwb;->setClickable(Z)V

    .line 77
    if-eqz p1, :cond_0

    .line 78
    invoke-virtual {p1}, Lhux;->a()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lhwb;->b:Ljava/lang/CharSequence;

    .line 81
    :cond_0
    iget-object v0, p0, Lhwb;->b:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lhwb;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lhwb;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lhwb;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lhwb;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lhwb;->a:Lhve;

    iget v0, v0, Lhve;->j:I

    .line 86
    invoke-virtual {p0, v0, v0, v0, v0}, Lhwb;->setPadding(IIII)V

    .line 88
    invoke-virtual {p0, p0}, Lhwb;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-virtual {p0, v2}, Lhwb;->setHeight(I)V

    .line 92
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhwb;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 114
    invoke-virtual {p0}, Lhwb;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "TextFooterView clicked!"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 115
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 103
    iget-object v0, p0, Lhwb;->b:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lhwb;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v0, p0, Lhwb;->a:Lhve;

    iget-object v5, v0, Lhve;->m:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 106
    :cond_0
    return-void
.end method
