.class final Ldti;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Ldvz;

.field private d:I

.field private e:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lnzx;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lnzx;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;ILdvz;)V
    .locals 1

    .prologue
    .line 1249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1237
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Ldti;->e:Ljava/util/LinkedHashMap;

    .line 1239
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldti;->f:Ljava/util/HashMap;

    .line 1246
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Ldti;->g:Ljava/util/LinkedHashMap;

    .line 1250
    iput-object p1, p0, Ldti;->a:Landroid/content/Context;

    .line 1251
    iput p2, p0, Ldti;->b:I

    .line 1252
    iput-object p3, p0, Ldti;->c:Ldvz;

    .line 1255
    invoke-static {p1, p2}, Lcuj;->b(Landroid/content/Context;I)Lnyj;

    move-result-object v0

    .line 1256
    if-nez v0, :cond_0

    const/16 v0, 0x32

    .line 1257
    :goto_0
    iput v0, p0, Ldti;->d:I

    .line 1258
    return-void

    .line 1256
    :cond_0
    iget-object v0, v0, Lnyj;->c:Ljava/lang/Integer;

    .line 1257
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lnzx;
    .locals 1

    .prologue
    .line 1441
    iget-object v0, p0, Ldti;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzx;

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lnzx;
    .locals 1

    .prologue
    .line 1451
    iget-object v0, p0, Ldti;->g:Ljava/util/LinkedHashMap;

    .line 1452
    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedHashMap;

    .line 1453
    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzx;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1377
    iget v1, p0, Ldti;->b:I

    .line 1379
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1380
    iget-object v0, p0, Ldti;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1381
    iget-object v2, p0, Ldti;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lnzx;

    .line 1382
    sget-object v2, Lnzr;->a:Loxr;

    .line 1383
    invoke-virtual {v8, v2}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzr;

    iget-object v2, v2, Lnzr;->b:Lnxr;

    .line 1386
    new-array v3, v4, [Lnzx;

    aput-object v8, v3, v5

    .line 1387
    iget-object v7, v2, Lnxr;->f:Ljava/lang/String;

    iget-object v11, v2, Lnxr;->c:Ljava/lang/String;

    iget v2, v2, Lnxr;->d:I

    invoke-static {v0, v7, v11, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 1389
    const/4 v7, 0x3

    new-array v11, v4, [Ljava/lang/String;

    aput-object v2, v11, v5

    invoke-static {v7, v11}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1390
    iget-object v7, p0, Ldti;->a:Landroid/content/Context;

    iget-object v11, p0, Ldti;->f:Ljava/util/HashMap;

    .line 1391
    invoke-virtual {v11, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1390
    invoke-static {v7, v1, v2, v0, v4}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 1393
    iget-object v0, p0, Ldti;->a:Landroid/content/Context;

    move v7, v4

    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    move v2, v5

    .line 1397
    :goto_0
    iget-object v0, v8, Lnzx;->j:[Lnzx;

    array-length v0, v0

    if-ge v2, v0, :cond_0

    .line 1398
    iget-object v0, v8, Lnzx;->j:[Lnzx;

    aget-object v0, v0, v2

    sget-object v3, Lnzu;->a:Loxr;

    .line 1399
    invoke-virtual {v0, v3}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 1400
    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->m:Lnzb;

    if-eqz v0, :cond_1

    .line 1401
    iget-object v0, v8, Lnzx;->j:[Lnzx;

    aget-object v0, v0, v2

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1397
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1409
    :cond_2
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 1410
    const/4 v0, 0x5

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "#videos"

    aput-object v3, v2, v5

    invoke-static {v0, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1412
    iget-object v0, p0, Ldti;->a:Landroid/content/Context;

    new-array v3, v5, [Lnzx;

    .line 1413
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lnzx;

    move v7, v4

    .line 1412
    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 1416
    :cond_3
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v12, 0x5

    const/4 v5, 0x0

    .line 1267
    iget-object v0, p0, Ldti;->c:Ldvz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldti;->c:Ldvz;

    invoke-virtual {v0}, Ldvz;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Ldti;->d:I

    if-eqz v0, :cond_1

    .line 1269
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1369
    :cond_1
    return-void

    .line 1273
    :cond_2
    const/4 v7, 0x0

    .line 1276
    iget v3, p0, Ldti;->b:I

    .line 1282
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    move v8, v5

    .line 1285
    :goto_0
    iget-object v0, p0, Ldti;->c:Ldvz;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldti;->c:Ldvz;

    invoke-virtual {v0}, Ldvz;->c()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1286
    :cond_3
    const-string v0, "EsTileSync"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1289
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Getting collections by ID, request "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1292
    :cond_4
    iget-object v0, p0, Ldti;->c:Ldvz;

    if-eqz v0, :cond_5

    .line 1293
    iget-object v0, p0, Ldti;->c:Ldvz;

    iget v1, p0, Ldti;->d:I

    invoke-virtual {v0, v1}, Ldvz;->d(I)V

    .line 1296
    :cond_5
    new-instance v0, Ldmn;

    iget-object v1, p0, Ldti;->a:Landroid/content/Context;

    new-instance v2, Lkfo;

    iget-object v4, p0, Ldti;->a:Landroid/content/Context;

    iget-object v6, p0, Ldti;->c:Ldvz;

    invoke-direct {v2, v4, v3, v6}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    iget v4, p0, Ldti;->d:I

    .line 1298
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Ldmn;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/Iterable;ZLjava/lang/Integer;Ljava/lang/String;)V

    .line 1300
    invoke-virtual {v0}, Ldmn;->l()V

    .line 1301
    const-string v1, "EsTileSync"

    invoke-virtual {v0, v1}, Ldmn;->e(Ljava/lang/String;)V

    .line 1304
    invoke-virtual {v0}, Ldmn;->e()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 1308
    invoke-virtual {v0}, Ldmn;->c()[Lnzx;

    move-result-object v4

    .line 1310
    iget-object v1, p0, Ldti;->c:Ldvz;

    if-eqz v1, :cond_6

    .line 1311
    iget-object v1, p0, Ldti;->c:Ldvz;

    array-length v2, v4

    invoke-virtual {v1, v2}, Ldvz;->f(I)V

    :cond_6
    move v2, v5

    .line 1314
    :goto_1
    array-length v1, v4

    if-ge v2, v1, :cond_9

    .line 1315
    aget-object v1, v4, v2

    sget-object v6, Lnzu;->a:Loxr;

    invoke-virtual {v1, v6}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzu;

    iget-object v6, v1, Lnzu;->b:Lnym;

    .line 1316
    iget-object v1, v6, Lnym;->z:[Lnxt;

    array-length v1, v1

    if-lez v1, :cond_8

    .line 1317
    iget-object v1, v6, Lnym;->z:[Lnxt;

    aget-object v1, v1, v5

    iget-object v10, v1, Lnxt;->b:Ljava/lang/String;

    .line 1318
    iget-object v1, p0, Ldti;->g:Ljava/util/LinkedHashMap;

    .line 1319
    invoke-virtual {v1, v10}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedHashMap;

    .line 1320
    if-nez v1, :cond_7

    .line 1321
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1322
    iget-object v11, p0, Ldti;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v11, v10, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1324
    :cond_7
    iget-object v6, v6, Lnym;->f:Ljava/lang/String;

    aget-object v10, v4, v2

    invoke-virtual {v1, v6, v10}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1314
    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1330
    :cond_9
    invoke-virtual {v0}, Ldmn;->d()[Lndp;

    move-result-object v2

    .line 1331
    array-length v4, v2

    move v1, v5

    :goto_2
    if-ge v1, v4, :cond_a

    aget-object v6, v2, v1

    .line 1332
    iget-object v10, v6, Lndp;->b:Lotc;

    iget-object v10, v10, Lotc;->a:Ljava/lang/String;

    .line 1333
    iget-object v11, p0, Ldti;->f:Ljava/util/HashMap;

    iget-object v6, v6, Lndp;->c:Ljava/lang/String;

    invoke-virtual {v11, v10, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1337
    :cond_a
    invoke-virtual {v0}, Ldmn;->b()Ljava/lang/String;

    move-result-object v1

    .line 1338
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 1339
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1341
    const-string v0, "EsTileSync"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1357
    :cond_b
    iget-object v0, p0, Ldti;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_c
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1358
    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzx;

    .line 1359
    if-eqz v1, :cond_10

    .line 1360
    iget-object v2, p0, Ldti;->g:Ljava/util/LinkedHashMap;

    .line 1361
    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    new-array v4, v5, [Lnzx;

    .line 1362
    invoke-interface {v2, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lnzx;

    .line 1360
    iput-object v2, v1, Lnzx;->j:[Lnzx;

    iget-object v4, v1, Lnzx;->f:Lnyl;

    if-nez v4, :cond_f

    array-length v4, v2

    if-lez v4, :cond_f

    aget-object v2, v2, v5

    iget-object v2, v2, Lnzx;->f:Lnyl;

    iput-object v2, v1, Lnzx;->f:Lnyl;

    .line 1364
    :cond_d
    :goto_4
    iget-object v2, p0, Ldti;->e:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1349
    :cond_e
    add-int/lit8 v0, v8, 0x1

    .line 1351
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_b

    move v8, v0

    move-object v7, v1

    goto/16 :goto_0

    .line 1360
    :cond_f
    array-length v2, v2

    if-nez v2, :cond_d

    const-string v2, "EsTileSync"

    invoke-static {v2, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_d

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x4f

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "processCollectionTile: Collection ID "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " has no children - This should not happen!"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1365
    :cond_10
    const-string v1, "EsTileSync"

    invoke-static {v1, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1366
    const-string v1, "CollectionSyncer.sync: No collection found for id "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_11

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_3

    :cond_11
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3
.end method
