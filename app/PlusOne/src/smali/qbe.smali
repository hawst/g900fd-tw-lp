.class public final Lqbe;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lqbg;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 423
    invoke-direct {p0}, Loxq;-><init>()V

    .line 629
    sget-object v0, Lqbg;->a:[Lqbg;

    iput-object v0, p0, Lqbe;->b:[Lqbg;

    .line 423
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 656
    .line 657
    iget-object v0, p0, Lqbe;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 658
    const/4 v0, 0x1

    iget-object v2, p0, Lqbe;->a:Ljava/lang/String;

    .line 659
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 661
    :goto_0
    iget-object v2, p0, Lqbe;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 662
    const/4 v2, 0x2

    iget-object v3, p0, Lqbe;->c:Ljava/lang/Integer;

    .line 663
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 665
    :cond_0
    iget-object v2, p0, Lqbe;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 666
    const/4 v2, 0x3

    iget-object v3, p0, Lqbe;->d:Ljava/lang/Integer;

    .line 667
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 669
    :cond_1
    iget-object v2, p0, Lqbe;->b:[Lqbg;

    if-eqz v2, :cond_3

    .line 670
    iget-object v2, p0, Lqbe;->b:[Lqbg;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 671
    if-eqz v4, :cond_2

    .line 672
    const/4 v5, 0x4

    .line 673
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 670
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 677
    :cond_3
    iget-object v1, p0, Lqbe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 678
    iput v0, p0, Lqbe;->ai:I

    .line 679
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lqbe;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 687
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 688
    sparse-switch v0, :sswitch_data_0

    .line 692
    iget-object v2, p0, Lqbe;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 693
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lqbe;->ah:Ljava/util/List;

    .line 696
    :cond_1
    iget-object v2, p0, Lqbe;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 698
    :sswitch_0
    return-object p0

    .line 703
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqbe;->a:Ljava/lang/String;

    goto :goto_0

    .line 707
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbe;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 711
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbe;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 715
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 716
    iget-object v0, p0, Lqbe;->b:[Lqbg;

    if-nez v0, :cond_3

    move v0, v1

    .line 717
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lqbg;

    .line 718
    iget-object v3, p0, Lqbe;->b:[Lqbg;

    if-eqz v3, :cond_2

    .line 719
    iget-object v3, p0, Lqbe;->b:[Lqbg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 721
    :cond_2
    iput-object v2, p0, Lqbe;->b:[Lqbg;

    .line 722
    :goto_2
    iget-object v2, p0, Lqbe;->b:[Lqbg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 723
    iget-object v2, p0, Lqbe;->b:[Lqbg;

    new-instance v3, Lqbg;

    invoke-direct {v3}, Lqbg;-><init>()V

    aput-object v3, v2, v0

    .line 724
    iget-object v2, p0, Lqbe;->b:[Lqbg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 725
    invoke-virtual {p1}, Loxn;->a()I

    .line 722
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 716
    :cond_3
    iget-object v0, p0, Lqbe;->b:[Lqbg;

    array-length v0, v0

    goto :goto_1

    .line 728
    :cond_4
    iget-object v2, p0, Lqbe;->b:[Lqbg;

    new-instance v3, Lqbg;

    invoke-direct {v3}, Lqbg;-><init>()V

    aput-object v3, v2, v0

    .line 729
    iget-object v2, p0, Lqbe;->b:[Lqbg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 688
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 634
    iget-object v0, p0, Lqbe;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 635
    const/4 v0, 0x1

    iget-object v1, p0, Lqbe;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 637
    :cond_0
    iget-object v0, p0, Lqbe;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 638
    const/4 v0, 0x2

    iget-object v1, p0, Lqbe;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 640
    :cond_1
    iget-object v0, p0, Lqbe;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 641
    const/4 v0, 0x3

    iget-object v1, p0, Lqbe;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 643
    :cond_2
    iget-object v0, p0, Lqbe;->b:[Lqbg;

    if-eqz v0, :cond_4

    .line 644
    iget-object v1, p0, Lqbe;->b:[Lqbg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 645
    if-eqz v3, :cond_3

    .line 646
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 644
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 650
    :cond_4
    iget-object v0, p0, Lqbe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 652
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 419
    invoke-virtual {p0, p1}, Lqbe;->a(Loxn;)Lqbe;

    move-result-object v0

    return-object v0
.end method
