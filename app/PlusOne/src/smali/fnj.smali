.class public final Lfnj;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Llzs;",
        "Llzt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 32
    const-string v3, "deletestory"

    new-instance v4, Llzs;

    invoke-direct {v4}, Llzs;-><init>()V

    new-instance v5, Llzt;

    invoke-direct {v5}, Llzt;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 33
    iput-object p3, p0, Lfnj;->a:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lfnj;->b:Ljava/lang/String;

    .line 35
    iput p5, p0, Lfnj;->c:I

    .line 36
    return-void
.end method


# virtual methods
.method protected a(Llzs;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 40
    new-instance v0, Lmlk;

    invoke-direct {v0}, Lmlk;-><init>()V

    iput-object v0, p1, Llzs;->a:Lmlk;

    .line 41
    iget-object v0, p1, Llzs;->a:Lmlk;

    .line 42
    new-instance v1, Lmlw;

    invoke-direct {v1}, Lmlw;-><init>()V

    iput-object v1, v0, Lmlk;->a:Lmlw;

    .line 43
    iget-object v1, v0, Lmlk;->a:Lmlw;

    iget-object v2, p0, Lfnj;->b:Ljava/lang/String;

    iput-object v2, v1, Lmlw;->b:Ljava/lang/String;

    .line 44
    iget-object v1, v0, Lmlk;->a:Lmlw;

    new-instance v2, Lmlh;

    invoke-direct {v2}, Lmlh;-><init>()V

    iput-object v2, v1, Lmlw;->a:Lmlh;

    .line 45
    iget-object v1, v0, Lmlk;->a:Lmlw;

    iget-object v1, v1, Lmlw;->a:Lmlh;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lmlh;->b:Ljava/lang/Boolean;

    .line 47
    new-instance v1, Lmok;

    invoke-direct {v1}, Lmok;-><init>()V

    iput-object v1, v0, Lmlk;->b:Lmok;

    .line 48
    iget-object v1, v0, Lmlk;->b:Lmok;

    iget-object v2, p0, Lfnj;->a:Ljava/lang/String;

    iput-object v2, v1, Lmok;->a:Ljava/lang/String;

    .line 50
    new-instance v1, Lmlj;

    invoke-direct {v1}, Lmlj;-><init>()V

    iput-object v1, v0, Lmlk;->c:Lmlj;

    .line 51
    iget v1, p0, Lfnj;->c:I

    packed-switch v1, :pswitch_data_0

    .line 61
    iget v0, p0, Lfnj;->c:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x25

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unrecognized delete type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 65
    :goto_0
    return-void

    .line 53
    :pswitch_0
    iget-object v0, v0, Lmlk;->c:Lmlj;

    iput v3, v0, Lmlj;->a:I

    goto :goto_0

    .line 57
    :pswitch_1
    iget-object v0, v0, Lmlk;->c:Lmlj;

    const/4 v1, 0x3

    iput v1, v0, Lmlj;->a:I

    goto :goto_0

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Llzs;

    invoke-virtual {p0, p1}, Lfnj;->a(Llzs;)V

    return-void
.end method
