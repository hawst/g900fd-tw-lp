.class public final Lmkc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojs;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12229
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12232
    iput-object v0, p0, Lmkc;->apiHeader:Llyq;

    .line 12235
    iput-object v0, p0, Lmkc;->a:Lojs;

    .line 12229
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12252
    const/4 v0, 0x0

    .line 12253
    iget-object v1, p0, Lmkc;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 12254
    const/4 v0, 0x1

    iget-object v1, p0, Lmkc;->apiHeader:Llyq;

    .line 12255
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12257
    :cond_0
    iget-object v1, p0, Lmkc;->a:Lojs;

    if-eqz v1, :cond_1

    .line 12258
    const/4 v1, 0x2

    iget-object v2, p0, Lmkc;->a:Lojs;

    .line 12259
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12261
    :cond_1
    iget-object v1, p0, Lmkc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12262
    iput v0, p0, Lmkc;->ai:I

    .line 12263
    return v0
.end method

.method public a(Loxn;)Lmkc;
    .locals 2

    .prologue
    .line 12271
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12272
    sparse-switch v0, :sswitch_data_0

    .line 12276
    iget-object v1, p0, Lmkc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12277
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkc;->ah:Ljava/util/List;

    .line 12280
    :cond_1
    iget-object v1, p0, Lmkc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12282
    :sswitch_0
    return-object p0

    .line 12287
    :sswitch_1
    iget-object v0, p0, Lmkc;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 12288
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmkc;->apiHeader:Llyq;

    .line 12290
    :cond_2
    iget-object v0, p0, Lmkc;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12294
    :sswitch_2
    iget-object v0, p0, Lmkc;->a:Lojs;

    if-nez v0, :cond_3

    .line 12295
    new-instance v0, Lojs;

    invoke-direct {v0}, Lojs;-><init>()V

    iput-object v0, p0, Lmkc;->a:Lojs;

    .line 12297
    :cond_3
    iget-object v0, p0, Lmkc;->a:Lojs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 12272
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 12240
    iget-object v0, p0, Lmkc;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 12241
    const/4 v0, 0x1

    iget-object v1, p0, Lmkc;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12243
    :cond_0
    iget-object v0, p0, Lmkc;->a:Lojs;

    if-eqz v0, :cond_1

    .line 12244
    const/4 v0, 0x2

    iget-object v1, p0, Lmkc;->a:Lojs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 12246
    :cond_1
    iget-object v0, p0, Lmkc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12248
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12225
    invoke-virtual {p0, p1}, Lmkc;->a(Loxn;)Lmkc;

    move-result-object v0

    return-object v0
.end method
