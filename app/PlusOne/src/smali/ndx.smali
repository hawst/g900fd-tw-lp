.class public final Lndx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lndx;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3542
    const/4 v0, 0x0

    new-array v0, v0, [Lndx;

    sput-object v0, Lndx;->a:[Lndx;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3543
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 3559
    const/4 v0, 0x0

    .line 3560
    iget-object v1, p0, Lndx;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3561
    const/4 v0, 0x1

    iget-object v1, p0, Lndx;->b:Ljava/lang/String;

    .line 3562
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3564
    :cond_0
    iget-object v1, p0, Lndx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3565
    iput v0, p0, Lndx;->ai:I

    .line 3566
    return v0
.end method

.method public a(Loxn;)Lndx;
    .locals 2

    .prologue
    .line 3574
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3575
    sparse-switch v0, :sswitch_data_0

    .line 3579
    iget-object v1, p0, Lndx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3580
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lndx;->ah:Ljava/util/List;

    .line 3583
    :cond_1
    iget-object v1, p0, Lndx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3585
    :sswitch_0
    return-object p0

    .line 3590
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lndx;->b:Ljava/lang/String;

    goto :goto_0

    .line 3575
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3550
    iget-object v0, p0, Lndx;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3551
    const/4 v0, 0x1

    iget-object v1, p0, Lndx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3553
    :cond_0
    iget-object v0, p0, Lndx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3555
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3539
    invoke-virtual {p0, p1}, Lndx;->a(Loxn;)Lndx;

    move-result-object v0

    return-object v0
.end method
