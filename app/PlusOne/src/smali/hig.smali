.class public final Lhig;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/os/Bundle;

.field private b:Lhiq;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lhig;->a:Landroid/os/Bundle;

    .line 52
    new-instance v0, Lhiq;

    iget-object v1, p0, Lhig;->a:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lhiq;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lhig;->b:Lhiq;

    return-void
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lhig;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method public a(Ljava/util/ArrayList;)Lhig;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lhig;"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lhig;->a:Landroid/os/Bundle;

    const-string v1, "AclPickerFragment.SHAREOUSEL_ORDER"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 78
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;Ljava/lang/String;)Lhig;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lhig;"
        }
    .end annotation

    .prologue
    .line 67
    if-eqz p1, :cond_0

    .line 68
    iget-object v0, p0, Lhig;->a:Landroid/os/Bundle;

    const-string v1, "AclPickerFragment.INITIAL_SELECTION"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 70
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 71
    iget-object v0, p0, Lhig;->a:Landroid/os/Bundle;

    const-string v1, "AclPickerFragment.INITIAL_SELECTION_SLIDE"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_1
    return-object p0
.end method

.method public a(Z)Lhig;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lhig;->b:Lhiq;

    invoke-virtual {v0, p1}, Lhiq;->a(Z)Lhiq;

    .line 57
    return-object p0
.end method

.method public b(Z)Lhig;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lhig;->b:Lhiq;

    invoke-virtual {v0, p1}, Lhiq;->b(Z)Lhiq;

    .line 62
    return-object p0
.end method
