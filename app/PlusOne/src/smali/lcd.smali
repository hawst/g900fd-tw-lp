.class public final Llcd;
.super Lllq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Z

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Llce;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lllq;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(Lpyc;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lllq;-><init>()V

    .line 30
    iget-object v1, p1, Lpyc;->b:Lpxy;

    iget v1, v1, Lpxy;->a:I

    iput v1, p0, Llcd;->a:I

    .line 31
    iget-object v1, p1, Lpyc;->b:Lpxy;

    iget-object v1, v1, Lpxy;->b:Ljava/lang/String;

    iput-object v1, p0, Llcd;->b:Ljava/lang/String;

    .line 32
    iget-object v1, p1, Lpyc;->b:Lpxy;

    iget-object v1, v1, Lpxy;->c:Ljava/lang/String;

    iput-object v1, p0, Llcd;->c:Ljava/lang/String;

    .line 33
    iput v0, p0, Llcd;->e:I

    .line 34
    iput-boolean v0, p0, Llcd;->f:Z

    .line 35
    iget-object v1, p1, Lpyc;->b:Lpxy;

    iget-object v1, v1, Lpxy;->d:Ljava/lang/String;

    iput-object v1, p0, Llcd;->d:Ljava/lang/String;

    .line 36
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lpyc;->b:Lpxy;

    iget-object v2, v2, Lpxy;->e:[Lpxz;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Llcd;->g:Ljava/util/ArrayList;

    .line 37
    :goto_0
    iget-object v1, p1, Lpyc;->b:Lpxy;

    iget-object v1, v1, Lpxy;->e:[Lpxz;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 38
    iget-object v1, p0, Llcd;->g:Ljava/util/ArrayList;

    new-instance v2, Llce;

    iget-object v3, p1, Lpyc;->b:Lpxy;

    iget-object v3, v3, Lpxy;->e:[Lpxz;

    aget-object v3, v3, v0

    iget v3, v3, Lpxz;->b:I

    iget-object v4, p1, Lpyc;->b:Lpxy;

    iget-object v4, v4, Lpxy;->e:[Lpxz;

    aget-object v4, v4, v0

    iget-object v4, v4, Lpxz;->c:Ljava/lang/String;

    iget-object v5, p1, Lpyc;->b:Lpxy;

    iget-object v5, v5, Lpxy;->e:[Lpxz;

    aget-object v5, v5, v0

    iget v5, v5, Lpxz;->d:I

    iget-object v6, p1, Lpyc;->b:Lpxy;

    iget-object v6, v6, Lpxy;->e:[Lpxz;

    aget-object v6, v6, v0

    iget v6, v6, Lpxz;->e:I

    invoke-direct {v2, v3, v4, v5, v6}, Llce;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    return-void
.end method

.method public static a(Llcd;)[B
    .locals 6

    .prologue
    .line 95
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x100

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 96
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 98
    :try_start_0
    iget v0, p0, Llcd;->a:I

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 101
    iget-object v0, p0, Llcd;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Llcd;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Llcd;->c:Ljava/lang/String;

    invoke-static {v2, v0}, Llcd;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Llcd;->d:Ljava/lang/String;

    invoke-static {v2, v0}, Llcd;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 104
    iget v0, p0, Llcd;->e:I

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 105
    iget-boolean v0, p0, Llcd;->f:Z

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    .line 106
    invoke-virtual {p0}, Llcd;->e()I

    move-result v3

    .line 107
    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 108
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 109
    invoke-virtual {p0, v0}, Llcd;->b(I)Llce;

    move-result-object v4

    iget v5, v4, Llce;->a:I

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v5, v4, Llce;->b:Ljava/lang/String;

    invoke-static {v2, v5}, Llce;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget v5, v4, Llce;->c:I

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v4, v4, Llce;->d:I

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 113
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    .line 115
    return-object v0

    .line 113
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Llcd;->a:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Llcd;->e:I

    .line 72
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Llcd;->f:Z

    .line 80
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Llcd;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Llce;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Llcd;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llce;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Llcd;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Llcd;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Llcd;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Llcd;->e:I

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Llcd;->f:Z

    return v0
.end method
