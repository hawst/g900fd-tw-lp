.class public final Lfrb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lmmt;

.field private final b:I

.field private final c:[[Lhng;

.field private final d:[Lhng;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lfsc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lmmt;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2049
    iput-object p1, p0, Lfrb;->a:Lmmt;

    .line 2050
    iput p2, p0, Lfrb;->b:I

    .line 2052
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    if-nez v0, :cond_0

    move v0, v1

    .line 2054
    :goto_0
    new-array v2, v0, [[Lhng;

    iput-object v2, p0, Lfrb;->c:[[Lhng;

    .line 2055
    :goto_1
    if-ge v1, v0, :cond_1

    .line 2056
    iget-object v2, p0, Lfrb;->c:[[Lhng;

    iget-object v3, p0, Lfrb;->a:Lmmt;

    iget-object v3, v3, Lmmt;->f:Lmnx;

    iget-object v3, v3, Lmnx;->b:[Lmns;

    aget-object v3, v3, v1

    invoke-direct {p0, v3}, Lfrb;->a(Lmns;)[Lhng;

    move-result-object v3

    aput-object v3, v2, v1

    .line 2055
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2052
    :cond_0
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    iget-object v0, v0, Lmnx;->b:[Lmns;

    array-length v0, v0

    goto :goto_0

    .line 2059
    :cond_1
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->c:Lmob;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->c:Lmob;

    iget-object v0, v0, Lmob;->b:Lmns;

    :goto_2
    invoke-direct {p0, v0}, Lfrb;->a(Lmns;)[Lhng;

    move-result-object v0

    iput-object v0, p0, Lfrb;->d:[Lhng;

    .line 2062
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfrb;->e:Ljava/util/Map;

    .line 2063
    invoke-direct {p0}, Lfrb;->l()V

    .line 2064
    return-void

    .line 2059
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(F)I
    .locals 1

    .prologue
    .line 2286
    iget v0, p0, Lfrb;->b:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private static a(Lmmw;)Landroid/view/animation/Interpolator;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2266
    if-nez p0, :cond_0

    .line 2279
    :goto_0
    return-object v0

    .line 2270
    :cond_0
    iget v1, p0, Lmmw;->a:I

    packed-switch v1, :pswitch_data_0

    .line 2278
    iget v1, p0, Lmmw;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown interpolator type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2272
    :pswitch_0
    new-instance v0, Lfsg;

    iget-object v1, p0, Lmmw;->b:[Ljava/lang/Float;

    invoke-direct {v0, v1}, Lfsg;-><init>([Ljava/lang/Float;)V

    goto :goto_0

    .line 2275
    :pswitch_1
    new-instance v0, Lfsd;

    iget-object v1, p0, Lmmw;->c:[Lmnf;

    invoke-static {v1}, Lfss;->a([Lmnf;)[Lhng;

    move-result-object v1

    invoke-direct {v0, v1}, Lfsd;-><init>([Lhng;)V

    goto :goto_0

    .line 2270
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lmns;)[Lhng;
    .locals 5

    .prologue
    .line 2258
    if-eqz p1, :cond_0

    iget-object v0, p1, Lmns;->b:[Lmnf;

    :goto_0
    invoke-static {v0}, Lfss;->a([Lmnf;)[Lhng;

    move-result-object v1

    .line 2259
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2260
    iget v4, p0, Lfrb;->b:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lhng;->f(F)V

    .line 2259
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2258
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2262
    :cond_1
    return-object v1
.end method

.method private l()V
    .locals 13

    .prologue
    .line 2067
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->g:[Lmmu;

    if-eqz v0, :cond_1

    .line 2068
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v10, v0, Lmmt;->g:[Lmmu;

    array-length v11, v10

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v11, :cond_1

    aget-object v12, v10, v0

    .line 2070
    iget v1, v12, Lmmu;->c:I

    packed-switch v1, :pswitch_data_0

    .line 2121
    const/4 v1, 0x0

    .line 2122
    iget v2, v12, Lmmu;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x23

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unknown animation type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2125
    :goto_1
    if-eqz v1, :cond_0

    .line 2126
    iget-object v2, p0, Lfrb;->e:Ljava/util/Map;

    iget-object v3, v12, Lmmu;->b:Ljava/lang/Long;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2068
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2072
    :pswitch_0
    iget-object v1, v12, Lmmu;->f:Lmmy;

    iget-object v1, v1, Lmmy;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1}, Lfrb;->a(F)I

    move-result v6

    .line 2073
    iget-object v1, v12, Lmmu;->f:Lmmy;

    iget-object v1, v1, Lmmy;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1}, Lfrb;->a(F)I

    move-result v7

    .line 2074
    iget-object v1, v12, Lmmu;->f:Lmmy;

    iget-object v1, v1, Lmmy;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1}, Lfrb;->a(F)I

    move-result v8

    .line 2075
    iget-object v1, v12, Lmmu;->f:Lmmy;

    iget-object v1, v1, Lmmy;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-direct {p0, v1}, Lfrb;->a(F)I

    move-result v9

    .line 2076
    new-instance v1, Lfsn;

    iget-object v2, v12, Lmmu;->d:Ljava/lang/Integer;

    .line 2077
    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    int-to-long v2, v2

    iget-object v4, v12, Lmmu;->e:Lmmw;

    .line 2078
    invoke-static {v4}, Lfrb;->a(Lmmw;)Landroid/view/animation/Interpolator;

    move-result-object v4

    iget-object v5, v12, Lmmu;->i:Ljava/lang/Boolean;

    .line 2079
    invoke-static {v5}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v5

    invoke-direct/range {v1 .. v9}, Lfsn;-><init>(JLandroid/view/animation/Interpolator;ZIIII)V

    goto :goto_1

    .line 2087
    :pswitch_1
    new-instance v1, Lfsb;

    iget-object v2, v12, Lmmu;->d:Ljava/lang/Integer;

    .line 2088
    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    int-to-long v2, v2

    iget-object v4, v12, Lmmu;->e:Lmmw;

    .line 2089
    invoke-static {v4}, Lfrb;->a(Lmmw;)Landroid/view/animation/Interpolator;

    move-result-object v4

    iget-object v5, v12, Lmmu;->i:Ljava/lang/Boolean;

    .line 2090
    invoke-static {v5}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v5

    iget-object v6, v12, Lmmu;->g:Lmmv;

    iget-object v6, v6, Lmmv;->a:Ljava/lang/Float;

    .line 2091
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget-object v7, v12, Lmmu;->g:Lmmv;

    iget-object v7, v7, Lmmv;->b:Ljava/lang/Float;

    .line 2092
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-direct/range {v1 .. v7}, Lfsb;-><init>(JLandroid/view/animation/Interpolator;ZFF)V

    goto :goto_1

    .line 2096
    :pswitch_2
    new-instance v1, Lfsh;

    iget-object v2, v12, Lmmu;->d:Ljava/lang/Integer;

    .line 2097
    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    int-to-long v2, v2

    iget-object v4, v12, Lmmu;->e:Lmmw;

    .line 2098
    invoke-static {v4}, Lfrb;->a(Lmmw;)Landroid/view/animation/Interpolator;

    move-result-object v4

    iget-object v5, v12, Lmmu;->i:Ljava/lang/Boolean;

    .line 2099
    invoke-static {v5}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v5

    iget-object v6, v12, Lmmu;->h:Lmmx;

    iget-object v6, v6, Lmmx;->a:Ljava/lang/Float;

    .line 2100
    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget-object v7, v12, Lmmu;->h:Lmmx;

    iget-object v7, v7, Lmmx;->c:Ljava/lang/Float;

    .line 2101
    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iget-object v8, v12, Lmmu;->h:Lmmx;

    iget-object v8, v8, Lmmx;->b:Ljava/lang/Float;

    .line 2102
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    iget-object v9, v12, Lmmu;->h:Lmmx;

    iget-object v9, v9, Lmmx;->d:Ljava/lang/Float;

    .line 2103
    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-direct/range {v1 .. v9}, Lfsh;-><init>(JLandroid/view/animation/Interpolator;ZFFFF)V

    goto/16 :goto_1

    .line 2107
    :pswitch_3
    new-instance v1, Lfso;

    iget-object v2, v12, Lmmu;->d:Ljava/lang/Integer;

    .line 2108
    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    int-to-long v2, v2

    iget-object v4, v12, Lmmu;->e:Lmmw;

    .line 2109
    invoke-static {v4}, Lfrb;->a(Lmmw;)Landroid/view/animation/Interpolator;

    move-result-object v4

    iget-object v5, v12, Lmmu;->i:Ljava/lang/Boolean;

    .line 2110
    invoke-static {v5}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lfso;-><init>(JLandroid/view/animation/Interpolator;Z)V

    goto/16 :goto_1

    .line 2114
    :pswitch_4
    new-instance v1, Lfse;

    iget-object v2, v12, Lmmu;->d:Ljava/lang/Integer;

    .line 2115
    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    int-to-long v2, v2

    iget-object v4, v12, Lmmu;->e:Lmmw;

    .line 2116
    invoke-static {v4}, Lfrb;->a(Lmmw;)Landroid/view/animation/Interpolator;

    move-result-object v4

    iget-object v5, v12, Lmmu;->i:Ljava/lang/Boolean;

    .line 2117
    invoke-static {v5}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Lfse;-><init>(JLandroid/view/animation/Interpolator;Z)V

    goto/16 :goto_1

    .line 2130
    :cond_1
    return-void

    .line 2070
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 2133
    iget v0, p0, Lfrb;->b:I

    return v0
.end method

.method public a(J)Lfsc;
    .locals 3

    .prologue
    .line 2243
    iget-object v0, p0, Lfrb;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsc;

    return-object v0
.end method

.method public b()F
    .locals 2

    .prologue
    .line 2144
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->e:Lmoa;

    .line 2145
    if-eqz v0, :cond_0

    iget-object v1, v0, Lmoa;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v0, v0, Lmoa;->b:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float v0, v1, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Lmmz;
    .locals 7

    .prologue
    .line 2247
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->h:[Lmmz;

    if-eqz v0, :cond_1

    .line 2248
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v2, v0, Lmmt;->h:[Lmmz;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2249
    iget-object v4, v0, Lmmz;->b:Ljava/lang/Long;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lmmz;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    .line 2254
    :goto_1
    return-object v0

    .line 2248
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2254
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c()[[Landroid/graphics/Point;
    .locals 15

    .prologue
    .line 2153
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    iget-object v0, v0, Lmnx;->a:[Lmny;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    iget-object v0, v0, Lmnx;->a:[Lmny;

    array-length v0, v0

    if-nez v0, :cond_2

    .line 2155
    :cond_0
    const/4 v0, 0x0

    .line 2172
    :cond_1
    return-object v0

    .line 2158
    :cond_2
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    iget-object v0, v0, Lmnx;->a:[Lmny;

    array-length v0, v0

    iget-object v1, p0, Lfrb;->a:Lmmt;

    iget-object v1, v1, Lmmt;->f:Lmnx;

    iget-object v1, v1, Lmnx;->a:[Lmny;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v1, v1, Lmny;->b:[Lmnz;

    array-length v1, v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Landroid/graphics/Point;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Landroid/graphics/Point;

    .line 2162
    const/4 v2, 0x0

    .line 2163
    iget-object v1, p0, Lfrb;->a:Lmmt;

    iget-object v1, v1, Lmmt;->f:Lmnx;

    iget-object v6, v1, Lmnx;->a:[Lmny;

    array-length v7, v6

    const/4 v1, 0x0

    move v4, v1

    move v5, v2

    :goto_0
    if-ge v4, v7, :cond_1

    aget-object v1, v6, v4

    .line 2164
    const/4 v2, 0x0

    .line 2165
    iget-object v8, v1, Lmny;->b:[Lmnz;

    array-length v9, v8

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v9, :cond_3

    aget-object v10, v8, v1

    .line 2166
    aget-object v11, v0, v5

    add-int/lit8 v3, v2, 0x1

    new-instance v12, Landroid/graphics/Point;

    iget-object v13, v10, Lmnz;->b:Lmnu;

    iget-object v13, v13, Lmnu;->a:Ljava/lang/Float;

    .line 2167
    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v13

    iget v14, p0, Lfrb;->b:I

    int-to-float v14, v14

    mul-float/2addr v13, v14

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v13

    iget-object v10, v10, Lmnz;->b:Lmnu;

    iget-object v10, v10, Lmnu;->b:Ljava/lang/Float;

    .line 2168
    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    iget v14, p0, Lfrb;->b:I

    int-to-float v14, v14

    mul-float/2addr v10, v14

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    invoke-direct {v12, v13, v10}, Landroid/graphics/Point;-><init>(II)V

    aput-object v12, v11, v2

    .line 2165
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_1

    .line 2170
    :cond_3
    add-int/lit8 v2, v5, 0x1

    .line 2163
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v5, v2

    goto :goto_0
.end method

.method public d()[[F
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 2180
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    iget-object v0, v0, Lmnx;->a:[Lmny;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    iget-object v0, v0, Lmnx;->a:[Lmny;

    array-length v0, v0

    if-nez v0, :cond_2

    .line 2182
    :cond_0
    const/4 v0, 0x0

    .line 2197
    :cond_1
    return-object v0

    .line 2185
    :cond_2
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    iget-object v0, v0, Lmnx;->a:[Lmny;

    array-length v0, v0

    iget-object v1, p0, Lfrb;->a:Lmmt;

    iget-object v1, v1, Lmmt;->f:Lmnx;

    iget-object v1, v1, Lmnx;->a:[Lmny;

    aget-object v1, v1, v2

    iget-object v1, v1, Lmny;->b:[Lmnz;

    array-length v1, v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    .line 2190
    iget-object v1, p0, Lfrb;->a:Lmmt;

    iget-object v1, v1, Lmmt;->f:Lmnx;

    iget-object v7, v1, Lmnx;->a:[Lmny;

    array-length v8, v7

    move v5, v2

    move v6, v2

    :goto_0
    if-ge v5, v8, :cond_1

    aget-object v1, v7, v5

    .line 2192
    iget-object v9, v1, Lmny;->b:[Lmnz;

    array-length v10, v9

    move v1, v2

    move v3, v2

    :goto_1
    if-ge v1, v10, :cond_3

    aget-object v11, v9, v1

    .line 2193
    aget-object v12, v0, v6

    add-int/lit8 v4, v3, 0x1

    iget-object v11, v11, Lmnz;->c:Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    aput v11, v12, v3

    .line 2192
    add-int/lit8 v1, v1, 0x1

    move v3, v4

    goto :goto_1

    .line 2195
    :cond_3
    add-int/lit8 v3, v6, 0x1

    .line 2190
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v6, v3

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 2202
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->f:Lmnx;

    iget-object v0, v0, Lmnx;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public f()[[Lhng;
    .locals 1

    .prologue
    .line 2210
    iget-object v0, p0, Lfrb;->c:[[Lhng;

    return-object v0
.end method

.method public g()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2215
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->c:Lmob;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->c:Lmob;

    iget-object v0, v0, Lmob;->a:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->c:Lmob;

    iget-object v0, v0, Lmob;->a:Lmof;

    iget-object v0, v0, Lmof;->u:Ljava/lang/Float;

    .line 2217
    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->c:Lmob;

    iget-object v0, v0, Lmob;->a:Lmof;

    iget-object v0, v0, Lmof;->g:Lmne;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 2223
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->c:Lmob;

    iget-object v0, v0, Lmob;->a:Lmof;

    iget-object v0, v0, Lmof;->g:Lmne;

    iget-object v0, v0, Lmne;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 2

    .prologue
    .line 2228
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->c:Lmob;

    iget-object v0, v0, Lmob;->a:Lmof;

    iget-object v0, v0, Lmof;->u:Ljava/lang/Float;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    iget v1, p0, Lfrb;->b:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public j()[Lhng;
    .locals 1

    .prologue
    .line 2234
    iget-object v0, p0, Lfrb;->d:[Lhng;

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 2239
    iget-object v0, p0, Lfrb;->a:Lmmt;

    iget-object v0, v0, Lmmt;->b:Lmof;

    iget-object v0, v0, Lmof;->h:Lmne;

    iget-object v0, v0, Lmne;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
