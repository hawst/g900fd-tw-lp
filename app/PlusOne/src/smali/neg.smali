.class public final Lneg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lnyr;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1642
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1649
    const/4 v0, 0x0

    iput-object v0, p0, Lneg;->b:Lnyr;

    .line 1642
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1669
    const/4 v0, 0x0

    .line 1670
    iget-object v1, p0, Lneg;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1671
    const/4 v0, 0x1

    iget-object v1, p0, Lneg;->a:Ljava/lang/String;

    .line 1672
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1674
    :cond_0
    iget-object v1, p0, Lneg;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1675
    const/4 v1, 0x2

    iget-object v2, p0, Lneg;->c:Ljava/lang/Integer;

    .line 1676
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1678
    :cond_1
    iget-object v1, p0, Lneg;->b:Lnyr;

    if-eqz v1, :cond_2

    .line 1679
    const/4 v1, 0x3

    iget-object v2, p0, Lneg;->b:Lnyr;

    .line 1680
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1682
    :cond_2
    iget-object v1, p0, Lneg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1683
    iput v0, p0, Lneg;->ai:I

    .line 1684
    return v0
.end method

.method public a(Loxn;)Lneg;
    .locals 2

    .prologue
    .line 1692
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1693
    sparse-switch v0, :sswitch_data_0

    .line 1697
    iget-object v1, p0, Lneg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1698
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lneg;->ah:Ljava/util/List;

    .line 1701
    :cond_1
    iget-object v1, p0, Lneg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1703
    :sswitch_0
    return-object p0

    .line 1708
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lneg;->a:Ljava/lang/String;

    goto :goto_0

    .line 1712
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lneg;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1716
    :sswitch_3
    iget-object v0, p0, Lneg;->b:Lnyr;

    if-nez v0, :cond_2

    .line 1717
    new-instance v0, Lnyr;

    invoke-direct {v0}, Lnyr;-><init>()V

    iput-object v0, p0, Lneg;->b:Lnyr;

    .line 1719
    :cond_2
    iget-object v0, p0, Lneg;->b:Lnyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1693
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1654
    iget-object v0, p0, Lneg;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1655
    const/4 v0, 0x1

    iget-object v1, p0, Lneg;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1657
    :cond_0
    iget-object v0, p0, Lneg;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1658
    const/4 v0, 0x2

    iget-object v1, p0, Lneg;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1660
    :cond_1
    iget-object v0, p0, Lneg;->b:Lnyr;

    if-eqz v0, :cond_2

    .line 1661
    const/4 v0, 0x3

    iget-object v1, p0, Lneg;->b:Lnyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1663
    :cond_2
    iget-object v0, p0, Lneg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1665
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1638
    invoke-virtual {p0, p1}, Lneg;->a(Loxn;)Lneg;

    move-result-object v0

    return-object v0
.end method
