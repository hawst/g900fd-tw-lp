.class public final Lnip;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnja;

.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3649
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3659
    const/4 v0, 0x0

    iput-object v0, p0, Lnip;->a:Lnja;

    .line 3662
    const/high16 v0, -0x80000000

    iput v0, p0, Lnip;->b:I

    .line 3649
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3684
    const/4 v0, 0x0

    .line 3685
    iget-object v1, p0, Lnip;->a:Lnja;

    if-eqz v1, :cond_0

    .line 3686
    const/4 v0, 0x1

    iget-object v1, p0, Lnip;->a:Lnja;

    .line 3687
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3689
    :cond_0
    iget v1, p0, Lnip;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 3690
    const/4 v1, 0x2

    iget v2, p0, Lnip;->b:I

    .line 3691
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3693
    :cond_1
    iget-object v1, p0, Lnip;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3694
    const/4 v1, 0x3

    iget-object v2, p0, Lnip;->c:Ljava/lang/String;

    .line 3695
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3697
    :cond_2
    iget-object v1, p0, Lnip;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3698
    iput v0, p0, Lnip;->ai:I

    .line 3699
    return v0
.end method

.method public a(Loxn;)Lnip;
    .locals 2

    .prologue
    .line 3707
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3708
    sparse-switch v0, :sswitch_data_0

    .line 3712
    iget-object v1, p0, Lnip;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3713
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnip;->ah:Ljava/util/List;

    .line 3716
    :cond_1
    iget-object v1, p0, Lnip;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3718
    :sswitch_0
    return-object p0

    .line 3723
    :sswitch_1
    iget-object v0, p0, Lnip;->a:Lnja;

    if-nez v0, :cond_2

    .line 3724
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnip;->a:Lnja;

    .line 3726
    :cond_2
    iget-object v0, p0, Lnip;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3730
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3731
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 3735
    :cond_3
    iput v0, p0, Lnip;->b:I

    goto :goto_0

    .line 3737
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lnip;->b:I

    goto :goto_0

    .line 3742
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnip;->c:Ljava/lang/String;

    goto :goto_0

    .line 3708
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3669
    iget-object v0, p0, Lnip;->a:Lnja;

    if-eqz v0, :cond_0

    .line 3670
    const/4 v0, 0x1

    iget-object v1, p0, Lnip;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3672
    :cond_0
    iget v0, p0, Lnip;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 3673
    const/4 v0, 0x2

    iget v1, p0, Lnip;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3675
    :cond_1
    iget-object v0, p0, Lnip;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3676
    const/4 v0, 0x3

    iget-object v1, p0, Lnip;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3678
    :cond_2
    iget-object v0, p0, Lnip;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3680
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3645
    invoke-virtual {p0, p1}, Lnip;->a(Loxn;)Lnip;

    move-result-object v0

    return-object v0
.end method
