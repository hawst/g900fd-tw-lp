.class public final Lfmw;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmjq;",
        "Lmjr;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 6

    .prologue
    .line 33
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v3, "settingsfetch"

    new-instance v4, Lmjq;

    invoke-direct {v4}, Lmjq;-><init>()V

    new-instance v5, Lmjr;

    invoke-direct {v5}, Lmjr;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected a(Lmjq;)V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lnps;

    invoke-direct {v0}, Lnps;-><init>()V

    iput-object v0, p1, Lmjq;->a:Lnps;

    .line 44
    iget-object v0, p1, Lmjq;->a:Lnps;

    .line 45
    new-instance v1, Lnpg;

    invoke-direct {v1}, Lnpg;-><init>()V

    iput-object v1, v0, Lnps;->b:Lnpg;

    .line 46
    iget-object v0, v0, Lnps;->b:Lnpg;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnpg;->a:Ljava/lang/Boolean;

    .line 47
    return-void
.end method

.method protected a(Lmjr;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p1, Lmjr;->a:Lnpm;

    iget-object v0, v0, Lnpm;->a:Lnpf;

    .line 52
    if-eqz v0, :cond_0

    iget-object v1, v0, Lnpf;->d:Lnpd;

    if-nez v1, :cond_1

    .line 53
    :cond_0
    new-instance v0, Lkfm;

    const-string v1, "Profile settings missing from response"

    invoke-direct {v0, v1}, Lkfm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_1
    iget-object v0, v0, Lnpf;->d:Lnpd;

    .line 56
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmjq;

    invoke-virtual {p0, p1}, Lfmw;->a(Lmjq;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lmjr;

    invoke-virtual {p0, p1}, Lfmw;->a(Lmjr;)V

    return-void
.end method
