.class public final Lmnk;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmnk;


# instance fields
.field public b:I

.field public c:Lmof;

.field public d:Lmnj;

.field public e:[Lmnk;

.field public f:Ljava/lang/Integer;

.field public g:[B

.field public h:Ljava/lang/String;

.field public i:Lmnl;

.field public j:[I

.field public k:Lmnk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6025
    const/4 v0, 0x0

    new-array v0, v0, [Lmnk;

    sput-object v0, Lmnk;->a:[Lmnk;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6026
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6035
    const/high16 v0, -0x80000000

    iput v0, p0, Lmnk;->b:I

    .line 6038
    iput-object v1, p0, Lmnk;->c:Lmof;

    .line 6041
    iput-object v1, p0, Lmnk;->d:Lmnj;

    .line 6044
    sget-object v0, Lmnk;->a:[Lmnk;

    iput-object v0, p0, Lmnk;->e:[Lmnk;

    .line 6053
    iput-object v1, p0, Lmnk;->i:Lmnl;

    .line 6056
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmnk;->j:[I

    .line 6059
    iput-object v1, p0, Lmnk;->k:Lmnk;

    .line 6026
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 6106
    .line 6107
    iget v0, p0, Lmnk;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_b

    .line 6108
    const/4 v0, 0x1

    iget v2, p0, Lmnk;->b:I

    .line 6109
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6111
    :goto_0
    iget-object v2, p0, Lmnk;->c:Lmof;

    if-eqz v2, :cond_0

    .line 6112
    const/4 v2, 0x2

    iget-object v3, p0, Lmnk;->c:Lmof;

    .line 6113
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6115
    :cond_0
    iget-object v2, p0, Lmnk;->d:Lmnj;

    if-eqz v2, :cond_1

    .line 6116
    const/4 v2, 0x3

    iget-object v3, p0, Lmnk;->d:Lmnj;

    .line 6117
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6119
    :cond_1
    iget-object v2, p0, Lmnk;->e:[Lmnk;

    if-eqz v2, :cond_3

    .line 6120
    iget-object v3, p0, Lmnk;->e:[Lmnk;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 6121
    if-eqz v5, :cond_2

    .line 6122
    const/4 v6, 0x4

    .line 6123
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 6120
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 6127
    :cond_3
    iget-object v2, p0, Lmnk;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 6128
    const/4 v2, 0x5

    iget-object v3, p0, Lmnk;->f:Ljava/lang/Integer;

    .line 6129
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 6131
    :cond_4
    iget-object v2, p0, Lmnk;->g:[B

    if-eqz v2, :cond_5

    .line 6132
    const/4 v2, 0x6

    iget-object v3, p0, Lmnk;->g:[B

    .line 6133
    invoke-static {v2, v3}, Loxo;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 6135
    :cond_5
    iget-object v2, p0, Lmnk;->h:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 6136
    const/4 v2, 0x7

    iget-object v3, p0, Lmnk;->h:Ljava/lang/String;

    .line 6137
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6139
    :cond_6
    iget-object v2, p0, Lmnk;->i:Lmnl;

    if-eqz v2, :cond_7

    .line 6140
    const/16 v2, 0x8

    iget-object v3, p0, Lmnk;->i:Lmnl;

    .line 6141
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6143
    :cond_7
    iget-object v2, p0, Lmnk;->j:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Lmnk;->j:[I

    array-length v2, v2

    if-lez v2, :cond_9

    .line 6145
    iget-object v3, p0, Lmnk;->j:[I

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_8

    aget v5, v3, v1

    .line 6147
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 6145
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 6149
    :cond_8
    add-int/2addr v0, v2

    .line 6150
    iget-object v1, p0, Lmnk;->j:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6152
    :cond_9
    iget-object v1, p0, Lmnk;->k:Lmnk;

    if-eqz v1, :cond_a

    .line 6153
    const/16 v1, 0xa

    iget-object v2, p0, Lmnk;->k:Lmnk;

    .line 6154
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6156
    :cond_a
    iget-object v1, p0, Lmnk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6157
    iput v0, p0, Lmnk;->ai:I

    .line 6158
    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmnk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6166
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6167
    sparse-switch v0, :sswitch_data_0

    .line 6171
    iget-object v2, p0, Lmnk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 6172
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmnk;->ah:Ljava/util/List;

    .line 6175
    :cond_1
    iget-object v2, p0, Lmnk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6177
    :sswitch_0
    return-object p0

    .line 6182
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6183
    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 6186
    :cond_2
    iput v0, p0, Lmnk;->b:I

    goto :goto_0

    .line 6188
    :cond_3
    iput v1, p0, Lmnk;->b:I

    goto :goto_0

    .line 6193
    :sswitch_2
    iget-object v0, p0, Lmnk;->c:Lmof;

    if-nez v0, :cond_4

    .line 6194
    new-instance v0, Lmof;

    invoke-direct {v0}, Lmof;-><init>()V

    iput-object v0, p0, Lmnk;->c:Lmof;

    .line 6196
    :cond_4
    iget-object v0, p0, Lmnk;->c:Lmof;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6200
    :sswitch_3
    iget-object v0, p0, Lmnk;->d:Lmnj;

    if-nez v0, :cond_5

    .line 6201
    new-instance v0, Lmnj;

    invoke-direct {v0}, Lmnj;-><init>()V

    iput-object v0, p0, Lmnk;->d:Lmnj;

    .line 6203
    :cond_5
    iget-object v0, p0, Lmnk;->d:Lmnj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6207
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6208
    iget-object v0, p0, Lmnk;->e:[Lmnk;

    if-nez v0, :cond_7

    move v0, v1

    .line 6209
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmnk;

    .line 6210
    iget-object v3, p0, Lmnk;->e:[Lmnk;

    if-eqz v3, :cond_6

    .line 6211
    iget-object v3, p0, Lmnk;->e:[Lmnk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6213
    :cond_6
    iput-object v2, p0, Lmnk;->e:[Lmnk;

    .line 6214
    :goto_2
    iget-object v2, p0, Lmnk;->e:[Lmnk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 6215
    iget-object v2, p0, Lmnk;->e:[Lmnk;

    new-instance v3, Lmnk;

    invoke-direct {v3}, Lmnk;-><init>()V

    aput-object v3, v2, v0

    .line 6216
    iget-object v2, p0, Lmnk;->e:[Lmnk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6217
    invoke-virtual {p1}, Loxn;->a()I

    .line 6214
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6208
    :cond_7
    iget-object v0, p0, Lmnk;->e:[Lmnk;

    array-length v0, v0

    goto :goto_1

    .line 6220
    :cond_8
    iget-object v2, p0, Lmnk;->e:[Lmnk;

    new-instance v3, Lmnk;

    invoke-direct {v3}, Lmnk;-><init>()V

    aput-object v3, v2, v0

    .line 6221
    iget-object v2, p0, Lmnk;->e:[Lmnk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6225
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmnk;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 6229
    :sswitch_6
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lmnk;->g:[B

    goto/16 :goto_0

    .line 6233
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmnk;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 6237
    :sswitch_8
    iget-object v0, p0, Lmnk;->i:Lmnl;

    if-nez v0, :cond_9

    .line 6238
    new-instance v0, Lmnl;

    invoke-direct {v0}, Lmnl;-><init>()V

    iput-object v0, p0, Lmnk;->i:Lmnl;

    .line 6240
    :cond_9
    iget-object v0, p0, Lmnk;->i:Lmnl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6244
    :sswitch_9
    const/16 v0, 0x48

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6245
    iget-object v0, p0, Lmnk;->j:[I

    array-length v0, v0

    .line 6246
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 6247
    iget-object v3, p0, Lmnk;->j:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6248
    iput-object v2, p0, Lmnk;->j:[I

    .line 6249
    :goto_3
    iget-object v2, p0, Lmnk;->j:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 6250
    iget-object v2, p0, Lmnk;->j:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 6251
    invoke-virtual {p1}, Loxn;->a()I

    .line 6249
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6254
    :cond_a
    iget-object v2, p0, Lmnk;->j:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 6258
    :sswitch_a
    iget-object v0, p0, Lmnk;->k:Lmnk;

    if-nez v0, :cond_b

    .line 6259
    new-instance v0, Lmnk;

    invoke-direct {v0}, Lmnk;-><init>()V

    iput-object v0, p0, Lmnk;->k:Lmnk;

    .line 6261
    :cond_b
    iget-object v0, p0, Lmnk;->k:Lmnk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 6167
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6064
    iget v1, p0, Lmnk;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 6065
    const/4 v1, 0x1

    iget v2, p0, Lmnk;->b:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6067
    :cond_0
    iget-object v1, p0, Lmnk;->c:Lmof;

    if-eqz v1, :cond_1

    .line 6068
    const/4 v1, 0x2

    iget-object v2, p0, Lmnk;->c:Lmof;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 6070
    :cond_1
    iget-object v1, p0, Lmnk;->d:Lmnj;

    if-eqz v1, :cond_2

    .line 6071
    const/4 v1, 0x3

    iget-object v2, p0, Lmnk;->d:Lmnj;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 6073
    :cond_2
    iget-object v1, p0, Lmnk;->e:[Lmnk;

    if-eqz v1, :cond_4

    .line 6074
    iget-object v2, p0, Lmnk;->e:[Lmnk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 6075
    if-eqz v4, :cond_3

    .line 6076
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 6074
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6080
    :cond_4
    iget-object v1, p0, Lmnk;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 6081
    const/4 v1, 0x5

    iget-object v2, p0, Lmnk;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 6083
    :cond_5
    iget-object v1, p0, Lmnk;->g:[B

    if-eqz v1, :cond_6

    .line 6084
    const/4 v1, 0x6

    iget-object v2, p0, Lmnk;->g:[B

    invoke-virtual {p1, v1, v2}, Loxo;->a(I[B)V

    .line 6086
    :cond_6
    iget-object v1, p0, Lmnk;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 6087
    const/4 v1, 0x7

    iget-object v2, p0, Lmnk;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 6089
    :cond_7
    iget-object v1, p0, Lmnk;->i:Lmnl;

    if-eqz v1, :cond_8

    .line 6090
    const/16 v1, 0x8

    iget-object v2, p0, Lmnk;->i:Lmnl;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 6092
    :cond_8
    iget-object v1, p0, Lmnk;->j:[I

    if-eqz v1, :cond_9

    iget-object v1, p0, Lmnk;->j:[I

    array-length v1, v1

    if-lez v1, :cond_9

    .line 6093
    iget-object v1, p0, Lmnk;->j:[I

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_9

    aget v3, v1, v0

    .line 6094
    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 6093
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6097
    :cond_9
    iget-object v0, p0, Lmnk;->k:Lmnk;

    if-eqz v0, :cond_a

    .line 6098
    const/16 v0, 0xa

    iget-object v1, p0, Lmnk;->k:Lmnk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6100
    :cond_a
    iget-object v0, p0, Lmnk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6102
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6022
    invoke-virtual {p0, p1}, Lmnk;->a(Loxn;)Lmnk;

    move-result-object v0

    return-object v0
.end method
