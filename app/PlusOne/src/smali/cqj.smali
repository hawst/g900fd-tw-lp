.class public final Lcqj;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Landroid/widget/SectionIndexer;
.implements Lenf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/BaseAdapter;",
        "Landroid/widget/SectionIndexer;",
        "Lenf",
        "<",
        "Lizu;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/View$OnLongClickListener;

.field private b:Levy;

.field private c:Lcqm;

.field private d:Z

.field private e:Landroid/database/Cursor;

.field private final f:Landroid/content/Context;

.field private final g:Lcqn;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private final j:I

.field private final k:Ljava/lang/String;

.field private l:Lctm;

.field private final m:Lhmw;

.field private n:I

.field private o:Z

.field private final p:Lcqo;

.field private q:Leqt;

.field private r:Lfcd;

.field private s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lctm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lcqn;Lcqm;Lhmw;)V
    .locals 2

    .prologue
    .line 142
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcqj;->o:Z

    .line 120
    new-instance v0, Lcqo;

    invoke-direct {v0, p0}, Lcqo;-><init>(Lcqj;)V

    iput-object v0, p0, Lcqj;->p:Lcqo;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcqj;->s:Ljava/util/List;

    .line 143
    iput-object p1, p0, Lcqj;->f:Landroid/content/Context;

    .line 144
    iput-object p4, p0, Lcqj;->g:Lcqn;

    .line 145
    iput p2, p0, Lcqj;->j:I

    .line 146
    iput-object p3, p0, Lcqj;->k:Ljava/lang/String;

    .line 147
    iput-object p6, p0, Lcqj;->m:Lhmw;

    .line 148
    new-instance v0, Lfcd;

    new-instance v1, Lcqk;

    invoke-direct {v1, p0}, Lcqk;-><init>(Lcqj;)V

    invoke-direct {v0, v1}, Lfcd;-><init>(Lfcf;)V

    iput-object v0, p0, Lcqj;->r:Lfcd;

    .line 155
    iput-object p5, p0, Lcqj;->c:Lcqm;

    .line 157
    sget-object v0, Lfvc;->k:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    new-instance v0, Levy;

    invoke-direct {v0, p1}, Levy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcqj;->b:Levy;

    .line 160
    :cond_0
    return-void
.end method

.method static synthetic a(Lcqj;)Levy;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcqj;->b:Levy;

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 623
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcqn;Lcra;ZLjava/util/List;Lcqv;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcqn;",
            "Lcra;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcrb;",
            ">;",
            "Lcqv;",
            ")",
            "Ljava/util/List",
            "<",
            "Lctm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 425
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 426
    new-instance v8, Ljvl;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Ljvl;-><init>(Landroid/content/Context;)V

    .line 427
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v3

    .line 430
    move-object/from16 v0, p1

    iget v2, v0, Lcqn;->a:I

    if-lez v2, :cond_1

    move-object/from16 v0, p1

    iget v2, v0, Lcqn;->a:I

    iget v4, v8, Ljvl;->a:I

    mul-int/2addr v2, v4

    .line 431
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 434
    :goto_0
    move-object/from16 v0, p2

    iget v4, v0, Lcra;->d:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-ge v2, v4, :cond_2

    const/4 v4, 0x1

    .line 435
    :goto_1
    iget v9, v8, Ljvl;->a:I

    .line 437
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 441
    const/4 v5, 0x0

    move v6, v5

    move v5, v4

    :goto_2
    if-ge v6, v2, :cond_5

    .line 442
    new-instance v11, Lcrd;

    const/4 v4, 0x0

    const/4 v12, 0x0

    invoke-direct {v11, v4, v12}, Lcrd;-><init>(ZZ)V

    .line 443
    add-int v4, v6, v9

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 445
    new-instance v12, Ljava/util/ArrayList;

    .line 446
    move-object/from16 v0, p4

    invoke-interface {v0, v6, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-direct {v12, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 449
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcqn;->b:Z

    if-eqz v4, :cond_0

    if-eqz v6, :cond_0

    .line 450
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-eqz v5, :cond_3

    const/4 v4, 0x1

    :goto_3
    add-int/2addr v4, v13

    iget v13, v8, Ljvl;->a:I

    if-lt v4, v13, :cond_4

    .line 454
    :cond_0
    new-instance v4, Lcrc;

    add-int/lit8 v13, v6, 0x1

    int-to-float v13, v13

    add-int/lit8 v14, v3, 0x1

    int-to-float v14, v14

    div-float/2addr v13, v14

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v12, v11, v13}, Lcrc;-><init>(Lcra;Ljava/util/List;Lcrd;F)V

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v5

    .line 441
    :goto_4
    add-int v5, v6, v9

    move v6, v5

    move v5, v4

    goto :goto_2

    :cond_1
    move v2, v3

    .line 431
    goto :goto_0

    .line 434
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    .line 450
    :cond_3
    const/4 v4, 0x0

    goto :goto_3

    .line 458
    :cond_4
    const/4 v4, 0x1

    goto :goto_4

    .line 462
    :cond_5
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 464
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 465
    const/4 v2, 0x0

    move v4, v2

    :goto_5
    if-ge v4, v6, :cond_b

    .line 466
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcrc;

    .line 468
    new-instance v11, Lcqs;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v11, v0, v2, v8, v1}, Lcqs;-><init>(Landroid/content/Context;Lcrc;Ljvl;Lcqv;)V

    .line 470
    new-instance v3, Lcti;

    const/4 v12, 0x1

    move-object/from16 v0, p1

    iget v13, v0, Lcqn;->c:I

    iget v14, v8, Ljvl;->a:I

    invoke-direct {v3, v11, v12, v13, v14}, Lcti;-><init>(Lcsq;III)V

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 473
    add-int/lit8 v3, v4, 0x1

    if-ne v3, v6, :cond_8

    .line 475
    if-eqz v5, :cond_7

    invoke-virtual {v2}, Lcrc;->c()Z

    move-result v3

    if-nez v3, :cond_7

    .line 476
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcqn;->b:Z

    if-nez v3, :cond_6

    invoke-virtual {v2}, Lcrc;->d()I

    move-result v3

    iget v12, v8, Ljvl;->a:I

    if-ge v3, v12, :cond_9

    .line 477
    :cond_6
    iget-object v3, v2, Lcrc;->b:Lcrd;

    const/4 v12, 0x1

    iput-boolean v12, v3, Lcrd;->a:Z

    .line 491
    :cond_7
    :goto_6
    if-eqz p3, :cond_8

    .line 492
    invoke-virtual {v2}, Lcrc;->d()I

    move-result v3

    iget v12, v8, Ljvl;->a:I

    if-ge v3, v12, :cond_a

    .line 494
    iget-object v2, v2, Lcrc;->b:Lcrd;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcrd;->b:Z

    .line 511
    :cond_8
    :goto_7
    const-wide/32 v2, 0x20000000

    invoke-virtual {v11, v2, v3}, Lcqs;->a(J)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 465
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_5

    .line 480
    :cond_9
    new-instance v3, Lcrc;

    const/4 v12, 0x0

    new-instance v13, Lcrd;

    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-direct {v13, v14, v15}, Lcrd;-><init>(ZZ)V

    iget v2, v2, Lcrc;->d:F

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v12, v13, v2}, Lcrc;-><init>(Lcra;Ljava/util/List;Lcrd;F)V

    .line 483
    new-instance v2, Lcqs;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v2, v0, v3, v8, v1}, Lcqs;-><init>(Landroid/content/Context;Lcrc;Ljvl;Lcqv;)V

    .line 486
    new-instance v12, Lcti;

    const/4 v13, 0x1

    move-object/from16 v0, p1

    iget v14, v0, Lcqn;->c:I

    iget v15, v8, Ljvl;->a:I

    invoke-direct {v12, v2, v13, v14, v15}, Lcti;-><init>(Lcsq;III)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v3

    goto :goto_6

    .line 497
    :cond_a
    new-instance v3, Lcrc;

    const/4 v12, 0x0

    new-instance v13, Lcrd;

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-direct {v13, v14, v15}, Lcrd;-><init>(ZZ)V

    iget v2, v2, Lcrc;->d:F

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v12, v13, v2}, Lcrc;-><init>(Lcra;Ljava/util/List;Lcrd;F)V

    .line 500
    new-instance v2, Lcqs;

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v2, v0, v3, v8, v1}, Lcqs;-><init>(Landroid/content/Context;Lcrc;Ljvl;Lcqv;)V

    .line 503
    new-instance v3, Lcti;

    const/4 v12, 0x1

    move-object/from16 v0, p1

    iget v13, v0, Lcqn;->c:I

    iget v14, v8, Ljvl;->a:I

    invoke-direct {v3, v2, v12, v13, v14}, Lcti;-><init>(Lcsq;III)V

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 514
    :cond_b
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Lcra;->a(Ljava/util/List;)V

    .line 516
    return-object v7
.end method

.method private b(Landroid/database/Cursor;)I
    .locals 8

    .prologue
    const/4 v1, 0x4

    const/4 v0, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 662
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-ne v4, v1, :cond_2

    move v4, v3

    :goto_0
    if-nez v4, :cond_0

    invoke-static {p1}, Lcqj;->c(Landroid/database/Cursor;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    move v0, v3

    .line 671
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v4, v2

    .line 662
    goto :goto_0

    .line 664
    :cond_3
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v5, 0x6e

    if-ne v4, v5, :cond_4

    move v4, v3

    :goto_2
    if-nez v4, :cond_1

    .line 666
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v5, 0x29a

    if-ne v4, v5, :cond_5

    move v4, v3

    :goto_3
    if-eqz v4, :cond_6

    .line 667
    const/4 v0, 0x3

    goto :goto_1

    :cond_4
    move v4, v2

    .line 664
    goto :goto_2

    :cond_5
    move v4, v2

    .line 666
    goto :goto_3

    .line 668
    :cond_6
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-ne v4, v0, :cond_7

    move v0, v3

    :goto_4
    if-eqz v0, :cond_8

    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v4, v0

    const-wide/32 v6, 0x400000

    and-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-eqz v0, :cond_8

    :goto_5
    if-eqz v3, :cond_9

    move v0, v1

    .line 669
    goto :goto_1

    :cond_7
    move v0, v2

    .line 668
    goto :goto_4

    :cond_8
    move v3, v2

    goto :goto_5

    :cond_9
    move v0, v2

    .line 671
    goto :goto_1
.end method

.method static synthetic b(Lcqj;)Leqt;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcqj;->q:Leqt;

    return-object v0
.end method

.method private static c(Landroid/database/Cursor;)Z
    .locals 2

    .prologue
    .line 700
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcqj;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcqj;->d:Z

    return v0
.end method

.method private d()V
    .locals 14

    .prologue
    const-wide/16 v6, 0x0

    .line 361
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 362
    const/4 v1, 0x0

    .line 363
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 364
    new-instance v9, Lcqr;

    invoke-direct {v9}, Lcqr;-><init>()V

    .line 367
    const/4 v0, 0x0

    move v5, v0

    move-wide v2, v6

    :goto_0
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_2

    .line 368
    iput-wide v6, v9, Lcqr;->a:J

    .line 369
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, v9}, Lctm;->a(Lctn;)V

    .line 370
    iget-wide v10, v9, Lcqr;->a:J

    cmp-long v0, v10, v6

    if-lez v0, :cond_3

    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    iget-wide v10, v9, Lcqr;->a:J

    cmp-long v0, v10, v2

    if-gez v0, :cond_3

    .line 372
    :cond_0
    iget-wide v10, v9, Lcqr;->a:J

    invoke-virtual {v4, v10, v11}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 373
    if-eqz v1, :cond_1

    invoke-virtual {v1, v4}, Lfce;->a(Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 374
    :cond_1
    iget-wide v0, v9, Lcqr;->a:J

    .line 375
    new-instance v3, Lfce;

    iget-object v2, p0, Lcqj;->f:Landroid/content/Context;

    invoke-direct {v3, v2, v4}, Lfce;-><init>(Landroid/content/Context;Ljava/util/Calendar;)V

    .line 376
    invoke-virtual {v8, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 377
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 367
    :goto_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-object v4, v2

    move-wide v12, v0

    move-object v1, v3

    move-wide v2, v12

    goto :goto_0

    .line 382
    :cond_2
    iget-object v0, p0, Lcqj;->r:Lfcd;

    invoke-virtual {v0, v8}, Lfcd;->a(Landroid/util/SparseArray;)V

    .line 383
    return-void

    :cond_3
    move-wide v12, v2

    move-object v2, v4

    move-object v3, v1

    move-wide v0, v12

    goto :goto_1
.end method


# virtual methods
.method public a(F)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 389
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 390
    new-instance v4, Lcqq;

    invoke-direct {v4}, Lcqq;-><init>()V

    move v2, v1

    .line 393
    :goto_0
    if-ge v1, v3, :cond_1

    .line 394
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    .line 395
    invoke-interface {v0, v4}, Lctm;->a(Lctn;)V

    .line 397
    const/4 v0, 0x0

    iget v5, v4, Lcqq;->a:F

    cmpg-float v0, v0, v5

    if-gtz v0, :cond_0

    iget v0, v4, Lcqq;->a:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_0

    move v0, v1

    .line 400
    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_0

    .line 399
    :cond_0
    iget v0, v4, Lcqq;->a:F

    cmpl-float v0, v0, p1

    if-gtz v0, :cond_1

    move v0, v2

    goto :goto_1

    .line 404
    :cond_1
    return v2
.end method

.method public a(Landroid/net/Uri;)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 520
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    .line 537
    :goto_0
    return v0

    .line 523
    :cond_1
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 524
    new-instance v4, Lcqp;

    invoke-direct {v4}, Lcqp;-><init>()V

    move v1, v2

    .line 525
    :goto_1
    if-ge v1, v3, :cond_4

    .line 526
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, v4}, Lctm;->a(Lctn;)V

    .line 528
    iget-object v0, v4, Lcqp;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 529
    iget-object v0, v4, Lcqp;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 530
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 531
    goto :goto_0

    .line 525
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 537
    goto :goto_0
.end method

.method public a(Lctm;)Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lctm;",
            ")",
            "Ljava/util/ArrayList",
            "<+",
            "Ljuf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    new-instance v0, Lcql;

    invoke-direct {v0}, Lcql;-><init>()V

    .line 260
    invoke-interface {p1, v0}, Lctm;->a(Lctn;)V

    .line 262
    iget-object v1, v0, Lcql;->a:Lcra;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcql;->a:Lcra;

    invoke-virtual {v0}, Lcra;->b()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(II)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 165
    new-instance v2, Lcqp;

    invoke-direct {v2}, Lcqp;-><init>()V

    .line 166
    :goto_0
    if-ge p1, p2, :cond_1

    .line 167
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, v2}, Lctm;->a(Lctn;)V

    .line 168
    iget-object v0, v2, Lcqp;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcqp;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    iget-object v0, v2, Lcqp;->a:Ljava/util/ArrayList;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 166
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 173
    :cond_1
    return-object v1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcqj;->i:Ljava/lang/String;

    .line 252
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 247
    iput p1, p0, Lcqj;->n:I

    .line 248
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 38

    .prologue
    .line 266
    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->e:Landroid/database/Cursor;

    move-object/from16 v0, p1

    if-ne v2, v0, :cond_0

    .line 358
    :goto_0
    return-void

    .line 270
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcqj;->e:Landroid/database/Cursor;

    .line 271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 273
    if-nez p1, :cond_1

    .line 274
    invoke-virtual/range {p0 .. p0}, Lcqj;->notifyDataSetChanged()V

    goto :goto_0

    .line 278
    :cond_1
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 279
    const-string v3, "resume_token"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcqj;->h:Ljava/lang/String;

    .line 281
    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->l:Lctm;

    if-eqz v2, :cond_2

    .line 282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->s:Ljava/util/List;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcqj;->l:Lctm;

    invoke-interface {v2, v3, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 285
    :cond_2
    const/16 v21, 0x0

    .line 287
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 288
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 289
    const/4 v4, 0x0

    .line 290
    const/4 v3, 0x0

    .line 292
    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->f:Landroid/content/Context;

    const-class v5, Lcrg;

    .line 293
    invoke-static {v2, v5}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v37, v2

    check-cast v37, Lcrg;

    move-object v2, v3

    .line 296
    :cond_3
    const/4 v9, 0x0

    .line 297
    const/4 v8, 0x1

    .line 298
    invoke-direct/range {p0 .. p1}, Lcqj;->b(Landroid/database/Cursor;)I

    move-result v3

    .line 300
    const/4 v5, 0x1

    if-eq v3, v5, :cond_13

    .line 301
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 302
    if-eqz v2, :cond_9

    .line 303
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, Lcrh;->a(Ljava/util/List;)V

    .line 308
    :cond_4
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 310
    :cond_5
    const/4 v4, 0x0

    .line 311
    const/16 v34, 0x0

    .line 314
    :goto_2
    invoke-direct/range {p0 .. p1}, Lcqj;->b(Landroid/database/Cursor;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move-object v2, v4

    move-object/from16 v4, v34

    .line 340
    :goto_3
    if-eqz v9, :cond_6

    if-eqz v8, :cond_6

    .line 341
    move-object/from16 v0, p0

    iget-object v3, v0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    :cond_6
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 345
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_8

    .line 346
    if-eqz v2, :cond_10

    .line 347
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, Lcrh;->a(Ljava/util/List;)V

    .line 352
    :cond_7
    :goto_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 356
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcqj;->d()V

    .line 357
    invoke-virtual/range {p0 .. p0}, Lcqj;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 304
    :cond_9
    if-eqz v4, :cond_4

    .line 305
    move-object/from16 v0, p0

    iget-object v10, v0, Lcqj;->s:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcqj;->g:Lcqn;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcqj;->p:Lcqo;

    invoke-static/range {v2 .. v7}, Lcqj;->a(Landroid/content/Context;Lcqn;Lcra;ZLjava/util/List;Lcqv;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v10, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 316
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->g:Lcqn;

    iget-boolean v2, v2, Lcqn;->d:Z

    .line 317
    move-object/from16 v0, p0

    iget-object v3, v0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v20

    new-instance v7, Lcra;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcqj;->k:Ljava/lang/String;

    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    invoke-direct/range {v7 .. v21}, Lcra;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JJJII)V

    .line 320
    new-instance v3, Lcri;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcqj;->f:Landroid/content/Context;

    const/4 v8, 0x0

    invoke-direct {v3, v5, v7, v8}, Lcri;-><init>(Landroid/content/Context;Lcra;I)V

    move-object v14, v4

    move-object/from16 v34, v7

    .line 335
    :goto_5
    add-int/lit8 v21, v21, 0x1

    move v8, v2

    move-object v9, v3

    move-object/from16 v4, v34

    move-object v2, v14

    goto/16 :goto_3

    .line 325
    :pswitch_2
    new-instance v22, Lcrb;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcqj;->k:Ljava/lang/String;

    move-object/from16 v23, v0

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {p1 .. p1}, Lcqj;->c(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v25, 0x0

    :goto_6
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcqj;->a(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v26

    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcqj;->a(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v27

    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v32

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v35

    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcqj;->a(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v36

    invoke-direct/range {v22 .. v36}, Lcrb;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;Ljava/lang/Integer;Ljava/lang/Integer;JJJLcra;ILjava/lang/Integer;)V

    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v4

    move-object/from16 v4, v34

    .line 326
    goto/16 :goto_3

    .line 325
    :cond_a
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_b

    const/4 v2, 0x0

    move-object v3, v2

    :goto_7
    invoke-static {v10, v11}, Ljvj;->a(J)Ljac;

    move-result-object v12

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string v2, "content"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcqj;->f:Landroid/content/Context;

    invoke-static {v3, v5, v7, v2, v12}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v2

    :goto_9
    move-object/from16 v25, v2

    goto/16 :goto_6

    :cond_b
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto :goto_7

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->f:Landroid/content/Context;

    const-class v10, Lkdv;

    invoke-static {v2, v10}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkdv;

    invoke-interface {v2}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v2

    new-instance v10, Ljava/io/File;

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v10, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v10}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_8

    :cond_d
    const-wide/32 v2, 0x40000

    and-long/2addr v2, v10

    const-wide/16 v10, 0x0

    cmp-long v2, v2, v10

    if-eqz v2, :cond_e

    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcqj;->f:Landroid/content/Context;

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v3, v5, v12, v2}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v2

    goto :goto_9

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->f:Landroid/content/Context;

    invoke-static {v2, v5, v7, v12}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    goto :goto_9

    .line 329
    :pswitch_3
    if-eqz v37, :cond_12

    .line 330
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcqj;->f:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Ljava/util/Calendar;->get(I)I

    move-result v7

    if-ne v2, v7, :cond_f

    const v2, 0x10018

    :goto_a
    invoke-static {v3, v4, v5, v2}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v18

    new-instance v14, Lcrh;

    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    invoke-direct/range {v14 .. v21}, Lcrh;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V

    .line 332
    move-object/from16 v0, p0

    iget v15, v0, Lcqj;->j:I

    const/16 v16, 0x4

    const/16 v17, 0x4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->g:Lcqn;

    iget-boolean v0, v2, Lcqn;->d:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcqj;->m:Lhmw;

    move-object/from16 v19, v0

    move-object/from16 v13, v37

    invoke-virtual/range {v13 .. v19}, Lcrg;->a(Lcrh;IIIZLhmw;)Lctm;

    move-result-object v2

    move-object v3, v2

    move v2, v8

    goto/16 :goto_5

    .line 330
    :cond_f
    const v2, 0x10010

    goto :goto_a

    .line 348
    :cond_10
    if-eqz v4, :cond_7

    .line 349
    move-object/from16 v0, p0

    iget-object v8, v0, Lcqj;->s:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcqj;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcqj;->g:Lcqn;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcqj;->h:Ljava/lang/String;

    if-eqz v5, :cond_11

    const/4 v5, 0x1

    :goto_b
    move-object/from16 v0, p0

    iget-object v7, v0, Lcqj;->p:Lcqo;

    invoke-static/range {v2 .. v7}, Lcqj;->a(Landroid/content/Context;Lcqn;Lcra;ZLjava/util/List;Lcqv;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_11
    const/4 v5, 0x0

    goto :goto_b

    :cond_12
    move v2, v8

    move-object v3, v9

    move-object v14, v4

    goto/16 :goto_5

    :cond_13
    move-object/from16 v34, v4

    move-object v4, v2

    goto/16 :goto_2

    .line 314
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/view/View$OnLongClickListener;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcqj;->a:Landroid/view/View$OnLongClickListener;

    .line 222
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 547
    iget-object v0, p0, Lcqj;->l:Lctm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 548
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    iget-object v1, p0, Lcqj;->l:Lctm;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 551
    :cond_0
    if-eqz p1, :cond_2

    .line 552
    new-instance v0, Lcpb;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, Lcpb;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcqj;->l:Lctm;

    .line 554
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 555
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    const/4 v1, 0x0

    iget-object v2, p0, Lcqj;->l:Lctm;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 561
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcqj;->notifyDataSetChanged()V

    .line 562
    return-void

    .line 558
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcqj;->l:Lctm;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/plus/views/FastScrollContainer;)V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcqj;->r:Lfcd;

    invoke-virtual {v0, p1}, Lfcd;->a(Lcom/google/android/apps/plus/views/FastScrollContainer;)V

    .line 229
    return-void
.end method

.method public a(Leqt;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcqj;->q:Leqt;

    .line 244
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 236
    iput-boolean p1, p0, Lcqj;->d:Z

    .line 237
    return-void
.end method

.method public b(I)F
    .locals 2

    .prologue
    .line 412
    new-instance v1, Lcqq;

    invoke-direct {v1}, Lcqq;-><init>()V

    .line 413
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    .line 414
    invoke-interface {v0, v1}, Lctm;->a(Lctn;)V

    .line 415
    iget v0, v1, Lcqq;->a:F

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcqj;->o:Z

    .line 256
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcqj;->o:Z

    .line 542
    const/4 v0, 0x0

    iput-object v0, p0, Lcqj;->i:Ljava/lang/String;

    .line 543
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 188
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0}, Lctm;->a()I

    move-result v0

    return v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcqj;->r:Lfcd;

    invoke-virtual {v0, p1}, Lfcd;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcqj;->r:Lfcd;

    invoke-virtual {v0, p1}, Lfcd;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lcqj;->r:Lfcd;

    invoke-virtual {v0}, Lfcd;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 203
    iget-boolean v0, p0, Lcqj;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqj;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    .line 205
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v1, v0, p1

    iget v0, p0, Lcqj;->n:I

    if-lez v0, :cond_1

    iget v0, p0, Lcqj;->n:I

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcqj;->i:Ljava/lang/String;

    iget-object v1, p0, Lcqj;->h:Ljava/lang/String;

    .line 207
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcqj;->h:Ljava/lang/String;

    iput-object v0, p0, Lcqj;->i:Ljava/lang/String;

    .line 209
    iget-object v0, p0, Lcqj;->c:Lcqm;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcqj;->c:Lcqm;

    iget-object v1, p0, Lcqj;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcqm;->a(Ljava/lang/String;)V

    .line 214
    :cond_0
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, p2, p3}, Lctm;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 205
    :cond_1
    const/16 v0, 0x64

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x5

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqj;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
