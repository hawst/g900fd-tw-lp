.class final Ljxl;
.super Ljava/io/InputStream;
.source "PG"


# instance fields
.field private a:Ljxi;

.field private b:J

.field private synthetic c:Ljxg;


# direct methods
.method public constructor <init>(Ljxg;Ljxi;)V
    .locals 2

    .prologue
    .line 238
    iput-object p1, p0, Ljxl;->c:Ljxg;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 236
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljxl;->b:J

    .line 239
    iput-object p2, p0, Ljxl;->a:Ljxi;

    .line 240
    iget-object v0, p0, Ljxl;->a:Ljxi;

    invoke-virtual {v0}, Ljxi;->a()V

    .line 241
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 283
    monitor-enter p0

    .line 284
    :try_start_0
    iget-object v0, p0, Ljxl;->a:Ljxi;

    if-nez v0, :cond_0

    monitor-exit p0

    .line 289
    :goto_0
    return-void

    .line 285
    :cond_0
    iget-object v0, p0, Ljxl;->a:Ljxi;

    .line 286
    const/4 v1, 0x0

    iput-object v1, p0, Ljxl;->a:Ljxi;

    .line 287
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 288
    invoke-virtual {v0}, Ljxi;->b()V

    goto :goto_0

    .line 287
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 294
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    iget-object v0, p0, Ljxl;->a:Ljxi;

    .line 297
    invoke-static {p0}, Lifu;->a(Ljava/io/Closeable;)V

    .line 300
    return-void

    .line 296
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ljxl;->a:Ljxi;

    .line 297
    invoke-static {p0}, Lifu;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public read()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 245
    new-array v0, v2, [B

    .line 246
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Ljxl;->read([BII)I

    move-result v1

    .line 247
    if-lez v1, :cond_0

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public read([BII)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 256
    if-nez p3, :cond_0

    .line 276
    :goto_0
    return v0

    .line 258
    :cond_0
    iget-object v4, p0, Ljxl;->a:Ljxi;

    .line 260
    iget-object v5, p0, Ljxl;->c:Ljxg;

    monitor-enter v5

    .line 261
    :try_start_0
    iget-wide v2, v4, Ljxi;->b:J

    .line 262
    :goto_1
    iget-wide v6, p0, Ljxl;->b:J

    cmp-long v1, v2, v6

    if-gtz v1, :cond_2

    iget v1, v4, Ljxi;->c:I

    and-int/lit8 v1, v1, 0x3

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_2

    .line 263
    iget-object v1, p0, Ljxl;->c:Ljxg;

    invoke-static {v1}, Lifu;->a(Ljava/lang/Object;)V

    .line 264
    iget-wide v2, v4, Ljxi;->b:J

    goto :goto_1

    :cond_1
    move v1, v0

    .line 262
    goto :goto_2

    .line 266
    :cond_2
    iget v0, v4, Ljxi;->c:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 267
    new-instance v0, Ljava/io/IOException;

    const-string v1, "download fail!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    int-to-long v0, p3

    iget-wide v6, p0, Ljxl;->b:J

    sub-long/2addr v2, v6

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 271
    if-nez v0, :cond_4

    const/4 v0, -0x1

    goto :goto_0

    .line 272
    :cond_4
    monitor-enter v4

    .line 273
    :try_start_2
    iget-object v1, v4, Ljxi;->a:Ljava/io/RandomAccessFile;

    iget-wide v2, p0, Ljxl;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 274
    iget-object v1, v4, Ljxi;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    .line 275
    iget-wide v2, p0, Ljxl;->b:J

    int-to-long v6, v0

    add-long/2addr v2, v6

    iput-wide v2, p0, Ljxl;->b:J

    .line 276
    monitor-exit v4

    goto :goto_0

    .line 277
    :catchall_1
    move-exception v0

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
