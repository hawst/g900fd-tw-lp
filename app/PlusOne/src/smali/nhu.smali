.class public final Lnhu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Lnja;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4709
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4712
    const/4 v0, 0x0

    iput-object v0, p0, Lnhu;->b:Lnja;

    .line 4709
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4751
    const/4 v0, 0x0

    .line 4752
    iget-object v1, p0, Lnhu;->b:Lnja;

    if-eqz v1, :cond_0

    .line 4753
    const/4 v0, 0x1

    iget-object v1, p0, Lnhu;->b:Lnja;

    .line 4754
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4756
    :cond_0
    iget-object v1, p0, Lnhu;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4757
    const/4 v1, 0x2

    iget-object v2, p0, Lnhu;->a:Ljava/lang/String;

    .line 4758
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4760
    :cond_1
    iget-object v1, p0, Lnhu;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 4761
    const/4 v1, 0x3

    iget-object v2, p0, Lnhu;->c:Ljava/lang/Boolean;

    .line 4762
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4764
    :cond_2
    iget-object v1, p0, Lnhu;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 4765
    const/4 v1, 0x4

    iget-object v2, p0, Lnhu;->d:Ljava/lang/Integer;

    .line 4766
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4768
    :cond_3
    iget-object v1, p0, Lnhu;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 4769
    const/4 v1, 0x5

    iget-object v2, p0, Lnhu;->e:Ljava/lang/Integer;

    .line 4770
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4772
    :cond_4
    iget-object v1, p0, Lnhu;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 4773
    const/4 v1, 0x6

    iget-object v2, p0, Lnhu;->f:Ljava/lang/Integer;

    .line 4774
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4776
    :cond_5
    iget-object v1, p0, Lnhu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4777
    iput v0, p0, Lnhu;->ai:I

    .line 4778
    return v0
.end method

.method public a(Loxn;)Lnhu;
    .locals 2

    .prologue
    .line 4786
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4787
    sparse-switch v0, :sswitch_data_0

    .line 4791
    iget-object v1, p0, Lnhu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4792
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnhu;->ah:Ljava/util/List;

    .line 4795
    :cond_1
    iget-object v1, p0, Lnhu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4797
    :sswitch_0
    return-object p0

    .line 4802
    :sswitch_1
    iget-object v0, p0, Lnhu;->b:Lnja;

    if-nez v0, :cond_2

    .line 4803
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnhu;->b:Lnja;

    .line 4805
    :cond_2
    iget-object v0, p0, Lnhu;->b:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4809
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnhu;->a:Ljava/lang/String;

    goto :goto_0

    .line 4813
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnhu;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 4817
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnhu;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 4821
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnhu;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 4825
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnhu;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 4787
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4727
    iget-object v0, p0, Lnhu;->b:Lnja;

    if-eqz v0, :cond_0

    .line 4728
    const/4 v0, 0x1

    iget-object v1, p0, Lnhu;->b:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4730
    :cond_0
    iget-object v0, p0, Lnhu;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4731
    const/4 v0, 0x2

    iget-object v1, p0, Lnhu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4733
    :cond_1
    iget-object v0, p0, Lnhu;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 4734
    const/4 v0, 0x3

    iget-object v1, p0, Lnhu;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 4736
    :cond_2
    iget-object v0, p0, Lnhu;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 4737
    const/4 v0, 0x4

    iget-object v1, p0, Lnhu;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4739
    :cond_3
    iget-object v0, p0, Lnhu;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 4740
    const/4 v0, 0x5

    iget-object v1, p0, Lnhu;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4742
    :cond_4
    iget-object v0, p0, Lnhu;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 4743
    const/4 v0, 0x6

    iget-object v1, p0, Lnhu;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4745
    :cond_5
    iget-object v0, p0, Lnhu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4747
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4705
    invoke-virtual {p0, p1}, Lnhu;->a(Loxn;)Lnhu;

    move-result-object v0

    return-object v0
.end method
