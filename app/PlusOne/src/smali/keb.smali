.class final Lkeb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# instance fields
.field private final a:Lkeh;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkec;",
            ">;"
        }
    .end annotation
.end field

.field private volatile c:Z

.field private d:I


# direct methods
.method public constructor <init>(Lkeu;Lkeh;II)V
    .locals 3

    .prologue
    const v2, 0x3fa66666    # 1.3f

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lkeb;->b:Ljava/util/List;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkeb;->c:Z

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lkeb;->d:I

    .line 47
    iput-object p2, p0, Lkeb;->a:Lkeh;

    .line 49
    int-to-float v0, p3

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-direct {p0, p1, p3, v0}, Lkeb;->a(Lkeu;II)Lkec;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_0

    .line 52
    iget-object v1, p0, Lkeb;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    :cond_0
    int-to-float v0, p3

    mul-float/2addr v0, v2

    float-to-int v0, v0

    invoke-direct {p0, p1, v0, p3}, Lkeb;->a(Lkeu;II)Lkec;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_1

    .line 58
    iget-object v1, p0, Lkeb;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_1
    iget-object v0, p0, Lkeb;->b:Ljava/util/List;

    new-instance v1, Lkec;

    const/4 v2, -0x1

    invoke-direct {v1, p4, p4, v2}, Lkec;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method

.method private a(Lkeu;II)Lkec;
    .locals 2

    .prologue
    .line 68
    iget v0, p1, Lkeu;->b:I

    .line 69
    mul-int v1, p2, p3

    shl-int/lit8 v1, v1, 0x2

    mul-int/lit8 v1, v1, 0x3

    div-int v1, v0, v1

    .line 71
    const/4 v0, 0x0

    .line 72
    if-lez v1, :cond_0

    .line 73
    new-instance v0, Lkec;

    invoke-direct {v0, p2, p3, v1}, Lkec;-><init>(III)V

    .line 75
    :cond_0
    return-object v0
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 79
    iget-boolean v0, p0, Lkeb;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkeb;->a:Lkeh;

    invoke-virtual {v0}, Lkeh;->c()F

    move-result v0

    const v1, 0x3f59999a    # 0.85f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkeb;->c:Z

    .line 82
    :cond_0
    return-void
.end method

.method public queueIdle()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 86
    iget-boolean v0, p0, Lkeb;->c:Z

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lkeb;->a:Lkeh;

    invoke-virtual {v0}, Lkeh;->c()F

    move-result v0

    const v2, 0x3f59999a    # 0.85f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 92
    iget v0, p0, Lkeb;->d:I

    iget-object v2, p0, Lkeb;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    move-object v0, v1

    .line 93
    :goto_0
    if-eqz v0, :cond_0

    .line 94
    iget-object v1, p0, Lkeb;->a:Lkeh;

    invoke-virtual {v1, v0}, Lkeh;->a(Landroid/graphics/Bitmap;)V

    .line 98
    :cond_0
    iget-boolean v0, p0, Lkeb;->c:Z

    return v0

    .line 92
    :cond_1
    iget-object v0, p0, Lkeb;->b:Ljava/util/List;

    iget v2, p0, Lkeb;->d:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkec;

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    iget v1, v0, Lkec;->c:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lkec;->c:I

    if-nez v1, :cond_3

    iget v1, p0, Lkeb;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lkeb;->d:I

    :cond_3
    iget v1, v0, Lkec;->a:I

    iget v0, v0, Lkec;->b:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method
