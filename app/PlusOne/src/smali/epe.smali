.class public final Lepe;
.super Llol;
.source "PG"

# interfaces
.implements Lhjj;
.implements Lhmq;
.implements Lkch;
.implements Lkut;


# instance fields
.field private N:Lhee;

.field private O:Lkci;

.field private P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private Q:Ljava/lang/String;

.field private final R:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<[",
            "Lnsr;",
            ">;"
        }
    .end annotation
.end field

.field private S:Lepj;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 45
    invoke-direct {p0}, Llol;-><init>()V

    .line 53
    new-instance v0, Lhje;

    iget-object v1, p0, Lepe;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    .line 58
    new-instance v0, Lkci;

    iget-object v1, p0, Lepe;->av:Llqm;

    const v2, 0x7f10005b

    invoke-direct {v0, p0, v1, v2, p0}, Lkci;-><init>(Lu;Llqr;ILkch;)V

    iput-object v0, p0, Lepe;->O:Lkci;

    .line 62
    new-instance v0, Lhnw;

    new-instance v1, Lepi;

    invoke-direct {v1, p0}, Lepi;-><init>(Lepe;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 69
    new-instance v0, Lepf;

    invoke-direct {v0, p0}, Lepf;-><init>(Lepe;)V

    iput-object v0, p0, Lepe;->R:Lbc;

    .line 234
    return-void
.end method

.method static synthetic a(Lepe;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lepe;->Q:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lepe;)Llnl;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lepe;->at:Llnl;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lu;
    .locals 3

    .prologue
    .line 165
    new-instance v0, Lepe;

    invoke-direct {v0}, Lepe;-><init>()V

    .line 166
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 167
    const-string v2, "person_id"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-virtual {v0, v1}, Lepe;->f(Landroid/os/Bundle;)V

    .line 169
    return-object v0
.end method

.method static synthetic b(Lepe;)Lhee;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lepe;->N:Lhee;

    return-object v0
.end method

.method static synthetic c(Lepe;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lepe;->Q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lepe;)Lepj;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lepe;->S:Lepj;

    return-object v0
.end method

.method static synthetic e(Lepe;)Llnh;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lepe;->au:Llnh;

    return-object v0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lepe;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lepe;->O:Lkci;

    invoke-virtual {v0}, Lkci;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lepe;->O:Lkci;

    invoke-virtual {v0}, Lkci;->d()V

    .line 115
    :cond_0
    return-void
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 287
    sget-object v0, Lhmw;->g:Lhmw;

    return-object v0
.end method

.method public K_()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lepe;->O:Lkci;

    invoke-virtual {v0}, Lkci;->c()V

    .line 108
    invoke-direct {p0}, Lepe;->e()V

    .line 109
    return-void
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 198
    const v0, 0x7f0400e2

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 188
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 190
    if-eqz p1, :cond_0

    .line 191
    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lepe;->Q:Ljava/lang/String;

    .line 193
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 203
    invoke-super {p0, p1, p2}, Llol;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 205
    const v0, 0x7f100304

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Lepe;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 206
    iget-object v0, p0, Lepe;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const v1, 0x7f020415

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 207
    iget-object v0, p0, Lepe;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->c(I)V

    .line 208
    iget-object v0, p0, Lepe;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v1, p0, Lepe;->at:Llnl;

    invoke-static {v1}, Llcm;->b(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 210
    iget-object v0, p0, Lepe;->at:Llnl;

    invoke-static {v0}, Llcm;->a(Landroid/content/Context;)I

    move-result v0

    .line 211
    iget-object v1, p0, Lepe;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 212
    iget-object v1, p0, Lepe;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v1, v0, v2, v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setPadding(IIII)V

    .line 214
    iget-object v0, p0, Lepe;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v1, Leph;

    invoke-direct {v1}, Leph;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 224
    return-void
.end method

.method public a(Lhjk;)V
    .locals 0

    .prologue
    .line 138
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 299
    invoke-virtual {p0}, Lepe;->n()Lz;

    move-result-object v1

    .line 300
    iget-object v0, p0, Lepe;->N:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 301
    const-wide/16 v6, 0x0

    move-object v3, p1

    move-object v5, v4

    invoke-static/range {v1 .. v7}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    .line 305
    const-string v1, "suggestion_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    const-string v1, "suggestion_ui"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 308
    invoke-virtual {p0, v0}, Lepe;->a(Landroid/content/Intent;)V

    .line 309
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 283
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 142
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 143
    const v1, 0x7f10067b

    if-ne v0, v1, :cond_0

    .line 144
    invoke-direct {p0}, Lepe;->e()V

    .line 145
    const/4 v0, 0x1

    .line 147
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aQ_()Z
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 178
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 179
    iget-object v0, p0, Lepe;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 180
    iget-object v0, p0, Lepe;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lepe;->N:Lhee;

    .line 181
    iget-object v0, p0, Lepe;->au:Llnh;

    const-class v1, Lkvq;

    new-instance v2, Lkvq;

    iget-object v3, p0, Lepe;->at:Llnl;

    iget-object v4, p0, Lepe;->N:Lhee;

    .line 182
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v2, v3, p0, v4}, Lkvq;-><init>(Landroid/content/Context;Lu;I)V

    .line 181
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 183
    iget-object v0, p0, Lepe;->au:Llnh;

    const-class v1, Lkut;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 184
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 228
    invoke-super {p0, p1}, Llol;->d(Landroid/os/Bundle;)V

    .line 229
    new-instance v0, Lepj;

    iget-object v1, p0, Lepe;->at:Llnl;

    invoke-direct {v0, p0, v1}, Lepj;-><init>(Lepe;Landroid/content/Context;)V

    iput-object v0, p0, Lepe;->S:Lepj;

    .line 230
    iget-object v0, p0, Lepe;->P:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v1, p0, Lepe;->S:Lepj;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 231
    invoke-virtual {p0}, Lepe;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lepe;->R:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 232
    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 291
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 294
    return-void
.end method
