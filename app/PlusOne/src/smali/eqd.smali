.class public final Leqd;
.super Lepp;
.source "PG"


# instance fields
.field private final d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILept;Z)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lepp;-><init>(Landroid/content/Context;ILept;Z)V

    .line 27
    iput-boolean p4, p0, Leqd;->d:Z

    .line 28
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Leqe;

    invoke-direct {v0, p0}, Leqe;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 75
    invoke-virtual {v0, v1}, Leqe;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 76
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Leqd;->c:Landroid/content/Context;

    const-class v1, Livx;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livx;

    .line 38
    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    const-class v2, Lixj;

    .line 39
    invoke-virtual {v1, v2}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Liwg;->c()Liwg;

    move-result-object v1

    const-class v2, Liwl;

    .line 41
    invoke-virtual {v1, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 43
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 52
    iget-boolean v0, p0, Leqd;->d:Z

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Leqd;->c:Landroid/content/Context;

    const v1, 0x7f0a05fb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Leqd;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 55
    :cond_0
    return-void
.end method

.method public a(Lnyq;)Z
    .locals 2

    .prologue
    .line 32
    iget v0, p0, Leqd;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Leqd;->d:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lepp;->a(Lnyq;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 47
    invoke-virtual {p0}, Leqd;->f()V

    .line 48
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Leqd;->d:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0400ba

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0401e8

    goto :goto_0
.end method

.method public e()Lepn;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lepn;->b:Lepn;

    return-object v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x2

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    const-string v0, "signin_promo_stats"

    return-object v0
.end method
