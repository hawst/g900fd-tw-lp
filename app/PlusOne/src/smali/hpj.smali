.class final Lhpj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lhpl;

.field b:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

.field private final c:Landroid/content/Context;

.field private final d:I

.field private final e:Ljct;


# direct methods
.method constructor <init>(Landroid/content/Context;ILhpl;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lhpj;->c:Landroid/content/Context;

    .line 46
    iput p2, p0, Lhpj;->d:I

    .line 47
    iput-object p3, p0, Lhpj;->a:Lhpl;

    .line 48
    new-instance v0, Lhpk;

    invoke-direct {v0, p0}, Lhpk;-><init>(Lhpj;)V

    .line 58
    new-instance v1, Ljct;

    invoke-direct {v1, p1, p2, v0}, Ljct;-><init>(Landroid/content/Context;ILjdg;)V

    iput-object v1, p0, Lhpj;->e:Ljct;

    .line 59
    iget-object v0, p0, Lhpj;->c:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v1, Lhpi;->c:Lief;

    invoke-interface {v0, v1, p2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhpj;->e:Ljct;

    const-string v1, "uploadmediabackground"

    invoke-virtual {v0, v1}, Ljct;->a(Ljava/lang/String;)V

    .line 60
    :cond_0
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhpj;->e:Ljct;

    invoke-virtual {v0}, Ljct;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    monitor-exit p0

    return-void

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;ZI)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 66
    iput-object p1, p0, Lhpj;->b:Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 67
    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    if-eqz p2, :cond_4

    const-string v0, ""

    .line 69
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x30

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "upload full size; task: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", remaining: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 68
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->h()Ljava/lang/String;

    move-result-object v0

    .line 73
    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->j()I

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_5

    const/4 v8, 0x1

    .line 74
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 75
    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->v()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 76
    iget-object v2, p0, Lhpj;->c:Landroid/content/Context;

    invoke-static {v2, v1}, Ljdv;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e(Ljava/lang/String;)V

    .line 78
    :cond_1
    iget-object v2, p0, Lhpj;->c:Landroid/content/Context;

    invoke-static {v1}, Llsb;->b(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_6

    move-object v2, v9

    .line 80
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 81
    iget-object v0, p0, Lhpj;->e:Ljct;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->v()Ljava/lang/String;

    move-result-object v3

    .line 82
    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->i()Ljava/lang/String;

    move-result-object v5

    move v6, p2

    move v7, p3

    .line 81
    invoke-virtual/range {v0 .. v9}, Ljct;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLopf;)Ljdb;

    move-result-object v0

    .line 87
    :goto_3
    invoke-virtual {v0}, Ljdb;->a()Ljdn;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lhpj;->c:Landroid/content/Context;

    iget v3, p0, Lhpj;->d:I

    invoke-static {v2, v3, v1}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->a(Landroid/content/Context;ILjdn;)V

    :cond_2
    invoke-virtual {v0}, Ljdb;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljdb;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    :cond_3
    invoke-virtual {v0}, Ljdb;->e()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    invoke-virtual {v0}, Ljdb;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    invoke-virtual {v0}, Ljdb;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v1

    invoke-virtual {v0}, Ljdb;->c()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(J)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 88
    return-void

    .line 68
    :cond_4
    const-string v0, "Don\'t "

    goto/16 :goto_0

    :cond_5
    move v8, v10

    .line 73
    goto/16 :goto_1

    .line 78
    :cond_6
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "_camera_roll_"

    invoke-static {v2, v1, v3}, Llsb;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 85
    :cond_7
    iget-object v3, p0, Lhpj;->e:Ljct;

    invoke-virtual {v3, v1, v2, v0}, Ljct;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljdb;

    move-result-object v0

    goto :goto_3
.end method
