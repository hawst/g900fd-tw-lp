.class public final Lohr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lohr;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1494
    const/4 v0, 0x0

    new-array v0, v0, [Lohr;

    sput-object v0, Lohr;->a:[Lohr;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1495
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1556
    const/4 v0, 0x0

    .line 1557
    iget-object v1, p0, Lohr;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1558
    const/4 v0, 0x1

    iget-object v1, p0, Lohr;->b:Ljava/lang/String;

    .line 1559
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1561
    :cond_0
    iget-object v1, p0, Lohr;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1562
    const/4 v1, 0x2

    iget-object v2, p0, Lohr;->e:Ljava/lang/String;

    .line 1563
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1565
    :cond_1
    iget-object v1, p0, Lohr;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1566
    const/4 v1, 0x3

    iget-object v2, p0, Lohr;->f:Ljava/lang/String;

    .line 1567
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1569
    :cond_2
    iget-object v1, p0, Lohr;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1570
    const/4 v1, 0x4

    iget-object v2, p0, Lohr;->g:Ljava/lang/String;

    .line 1571
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1573
    :cond_3
    iget-object v1, p0, Lohr;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1574
    const/4 v1, 0x5

    iget-object v2, p0, Lohr;->h:Ljava/lang/String;

    .line 1575
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1577
    :cond_4
    iget-object v1, p0, Lohr;->i:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1578
    const/4 v1, 0x6

    iget-object v2, p0, Lohr;->i:Ljava/lang/String;

    .line 1579
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1581
    :cond_5
    iget-object v1, p0, Lohr;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 1582
    const/4 v1, 0x7

    iget-object v2, p0, Lohr;->j:Ljava/lang/String;

    .line 1583
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1585
    :cond_6
    iget-object v1, p0, Lohr;->k:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1586
    const/16 v1, 0x8

    iget-object v2, p0, Lohr;->k:Ljava/lang/String;

    .line 1587
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1589
    :cond_7
    iget-object v1, p0, Lohr;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 1590
    const/16 v1, 0x9

    iget-object v2, p0, Lohr;->c:Ljava/lang/Integer;

    .line 1591
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1593
    :cond_8
    iget-object v1, p0, Lohr;->d:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1594
    const/16 v1, 0xa

    iget-object v2, p0, Lohr;->d:Ljava/lang/String;

    .line 1595
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1597
    :cond_9
    iget-object v1, p0, Lohr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1598
    iput v0, p0, Lohr;->ai:I

    .line 1599
    return v0
.end method

.method public a(Loxn;)Lohr;
    .locals 2

    .prologue
    .line 1607
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1608
    sparse-switch v0, :sswitch_data_0

    .line 1612
    iget-object v1, p0, Lohr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1613
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohr;->ah:Ljava/util/List;

    .line 1616
    :cond_1
    iget-object v1, p0, Lohr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1618
    :sswitch_0
    return-object p0

    .line 1623
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohr;->b:Ljava/lang/String;

    goto :goto_0

    .line 1627
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohr;->e:Ljava/lang/String;

    goto :goto_0

    .line 1631
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohr;->f:Ljava/lang/String;

    goto :goto_0

    .line 1635
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohr;->g:Ljava/lang/String;

    goto :goto_0

    .line 1639
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohr;->h:Ljava/lang/String;

    goto :goto_0

    .line 1643
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohr;->i:Ljava/lang/String;

    goto :goto_0

    .line 1647
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohr;->j:Ljava/lang/String;

    goto :goto_0

    .line 1651
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohr;->k:Ljava/lang/String;

    goto :goto_0

    .line 1655
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohr;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1659
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohr;->d:Ljava/lang/String;

    goto :goto_0

    .line 1608
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1520
    iget-object v0, p0, Lohr;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1521
    const/4 v0, 0x1

    iget-object v1, p0, Lohr;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1523
    :cond_0
    iget-object v0, p0, Lohr;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1524
    const/4 v0, 0x2

    iget-object v1, p0, Lohr;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1526
    :cond_1
    iget-object v0, p0, Lohr;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1527
    const/4 v0, 0x3

    iget-object v1, p0, Lohr;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1529
    :cond_2
    iget-object v0, p0, Lohr;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1530
    const/4 v0, 0x4

    iget-object v1, p0, Lohr;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1532
    :cond_3
    iget-object v0, p0, Lohr;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1533
    const/4 v0, 0x5

    iget-object v1, p0, Lohr;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1535
    :cond_4
    iget-object v0, p0, Lohr;->i:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1536
    const/4 v0, 0x6

    iget-object v1, p0, Lohr;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1538
    :cond_5
    iget-object v0, p0, Lohr;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1539
    const/4 v0, 0x7

    iget-object v1, p0, Lohr;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1541
    :cond_6
    iget-object v0, p0, Lohr;->k:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1542
    const/16 v0, 0x8

    iget-object v1, p0, Lohr;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1544
    :cond_7
    iget-object v0, p0, Lohr;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 1545
    const/16 v0, 0x9

    iget-object v1, p0, Lohr;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1547
    :cond_8
    iget-object v0, p0, Lohr;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 1548
    const/16 v0, 0xa

    iget-object v1, p0, Lohr;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1550
    :cond_9
    iget-object v0, p0, Lohr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1552
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1491
    invoke-virtual {p0, p1}, Lohr;->a(Loxn;)Lohr;

    move-result-object v0

    return-object v0
.end method
