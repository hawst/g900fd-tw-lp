.class public final Lmky;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lncm;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25675
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25678
    iput-object v0, p0, Lmky;->apiHeader:Llyq;

    .line 25681
    iput-object v0, p0, Lmky;->a:Lncm;

    .line 25675
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 25698
    const/4 v0, 0x0

    .line 25699
    iget-object v1, p0, Lmky;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 25700
    const/4 v0, 0x1

    iget-object v1, p0, Lmky;->apiHeader:Llyq;

    .line 25701
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 25703
    :cond_0
    iget-object v1, p0, Lmky;->a:Lncm;

    if-eqz v1, :cond_1

    .line 25704
    const/4 v1, 0x2

    iget-object v2, p0, Lmky;->a:Lncm;

    .line 25705
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25707
    :cond_1
    iget-object v1, p0, Lmky;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 25708
    iput v0, p0, Lmky;->ai:I

    .line 25709
    return v0
.end method

.method public a(Loxn;)Lmky;
    .locals 2

    .prologue
    .line 25717
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 25718
    sparse-switch v0, :sswitch_data_0

    .line 25722
    iget-object v1, p0, Lmky;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 25723
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmky;->ah:Ljava/util/List;

    .line 25726
    :cond_1
    iget-object v1, p0, Lmky;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25728
    :sswitch_0
    return-object p0

    .line 25733
    :sswitch_1
    iget-object v0, p0, Lmky;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 25734
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmky;->apiHeader:Llyq;

    .line 25736
    :cond_2
    iget-object v0, p0, Lmky;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25740
    :sswitch_2
    iget-object v0, p0, Lmky;->a:Lncm;

    if-nez v0, :cond_3

    .line 25741
    new-instance v0, Lncm;

    invoke-direct {v0}, Lncm;-><init>()V

    iput-object v0, p0, Lmky;->a:Lncm;

    .line 25743
    :cond_3
    iget-object v0, p0, Lmky;->a:Lncm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 25718
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 25686
    iget-object v0, p0, Lmky;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 25687
    const/4 v0, 0x1

    iget-object v1, p0, Lmky;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25689
    :cond_0
    iget-object v0, p0, Lmky;->a:Lncm;

    if-eqz v0, :cond_1

    .line 25690
    const/4 v0, 0x2

    iget-object v1, p0, Lmky;->a:Lncm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 25692
    :cond_1
    iget-object v0, p0, Lmky;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 25694
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 25671
    invoke-virtual {p0, p1}, Lmky;->a(Loxn;)Lmky;

    move-result-object v0

    return-object v0
.end method
