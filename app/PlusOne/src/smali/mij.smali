.class public final Lmij;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmzi;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1942
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1945
    iput-object v0, p0, Lmij;->apiHeader:Llyr;

    .line 1948
    iput-object v0, p0, Lmij;->a:Lmzi;

    .line 1942
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1965
    const/4 v0, 0x0

    .line 1966
    iget-object v1, p0, Lmij;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 1967
    const/4 v0, 0x1

    iget-object v1, p0, Lmij;->apiHeader:Llyr;

    .line 1968
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1970
    :cond_0
    iget-object v1, p0, Lmij;->a:Lmzi;

    if-eqz v1, :cond_1

    .line 1971
    const/4 v1, 0x2

    iget-object v2, p0, Lmij;->a:Lmzi;

    .line 1972
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1974
    :cond_1
    iget-object v1, p0, Lmij;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1975
    iput v0, p0, Lmij;->ai:I

    .line 1976
    return v0
.end method

.method public a(Loxn;)Lmij;
    .locals 2

    .prologue
    .line 1984
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1985
    sparse-switch v0, :sswitch_data_0

    .line 1989
    iget-object v1, p0, Lmij;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1990
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmij;->ah:Ljava/util/List;

    .line 1993
    :cond_1
    iget-object v1, p0, Lmij;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1995
    :sswitch_0
    return-object p0

    .line 2000
    :sswitch_1
    iget-object v0, p0, Lmij;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 2001
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmij;->apiHeader:Llyr;

    .line 2003
    :cond_2
    iget-object v0, p0, Lmij;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2007
    :sswitch_2
    iget-object v0, p0, Lmij;->a:Lmzi;

    if-nez v0, :cond_3

    .line 2008
    new-instance v0, Lmzi;

    invoke-direct {v0}, Lmzi;-><init>()V

    iput-object v0, p0, Lmij;->a:Lmzi;

    .line 2010
    :cond_3
    iget-object v0, p0, Lmij;->a:Lmzi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1985
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1953
    iget-object v0, p0, Lmij;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 1954
    const/4 v0, 0x1

    iget-object v1, p0, Lmij;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1956
    :cond_0
    iget-object v0, p0, Lmij;->a:Lmzi;

    if-eqz v0, :cond_1

    .line 1957
    const/4 v0, 0x2

    iget-object v1, p0, Lmij;->a:Lmzi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1959
    :cond_1
    iget-object v0, p0, Lmij;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1961
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1938
    invoke-virtual {p0, p1}, Lmij;->a(Loxn;)Lmij;

    move-result-object v0

    return-object v0
.end method
