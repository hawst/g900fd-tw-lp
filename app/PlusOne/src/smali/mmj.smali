.class public final Lmmj;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmon;

.field public b:Lmme;

.field public c:Lmme;

.field private d:[Lmmk;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 969
    invoke-direct {p0}, Loxq;-><init>()V

    .line 978
    iput-object v0, p0, Lmmj;->a:Lmon;

    .line 981
    iput-object v0, p0, Lmmj;->b:Lmme;

    .line 984
    iput-object v0, p0, Lmmj;->c:Lmme;

    .line 987
    sget-object v0, Lmmk;->a:[Lmmk;

    iput-object v0, p0, Lmmj;->d:[Lmmk;

    .line 990
    const/high16 v0, -0x80000000

    iput v0, p0, Lmmj;->e:I

    .line 969
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1020
    .line 1021
    iget-object v0, p0, Lmmj;->a:Lmon;

    if-eqz v0, :cond_5

    .line 1022
    const/4 v0, 0x1

    iget-object v2, p0, Lmmj;->a:Lmon;

    .line 1023
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1025
    :goto_0
    iget-object v2, p0, Lmmj;->b:Lmme;

    if-eqz v2, :cond_0

    .line 1026
    const/4 v2, 0x2

    iget-object v3, p0, Lmmj;->b:Lmme;

    .line 1027
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1029
    :cond_0
    iget-object v2, p0, Lmmj;->d:[Lmmk;

    if-eqz v2, :cond_2

    .line 1030
    iget-object v2, p0, Lmmj;->d:[Lmmk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 1031
    if-eqz v4, :cond_1

    .line 1032
    const/4 v5, 0x3

    .line 1033
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1030
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1037
    :cond_2
    iget v1, p0, Lmmj;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 1038
    const/4 v1, 0x4

    iget v2, p0, Lmmj;->e:I

    .line 1039
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1041
    :cond_3
    iget-object v1, p0, Lmmj;->c:Lmme;

    if-eqz v1, :cond_4

    .line 1042
    const/4 v1, 0x5

    iget-object v2, p0, Lmmj;->c:Lmme;

    .line 1043
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1045
    :cond_4
    iget-object v1, p0, Lmmj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1046
    iput v0, p0, Lmmj;->ai:I

    .line 1047
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmmj;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1055
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1056
    sparse-switch v0, :sswitch_data_0

    .line 1060
    iget-object v2, p0, Lmmj;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1061
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmmj;->ah:Ljava/util/List;

    .line 1064
    :cond_1
    iget-object v2, p0, Lmmj;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1066
    :sswitch_0
    return-object p0

    .line 1071
    :sswitch_1
    iget-object v0, p0, Lmmj;->a:Lmon;

    if-nez v0, :cond_2

    .line 1072
    new-instance v0, Lmon;

    invoke-direct {v0}, Lmon;-><init>()V

    iput-object v0, p0, Lmmj;->a:Lmon;

    .line 1074
    :cond_2
    iget-object v0, p0, Lmmj;->a:Lmon;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1078
    :sswitch_2
    iget-object v0, p0, Lmmj;->b:Lmme;

    if-nez v0, :cond_3

    .line 1079
    new-instance v0, Lmme;

    invoke-direct {v0}, Lmme;-><init>()V

    iput-object v0, p0, Lmmj;->b:Lmme;

    .line 1081
    :cond_3
    iget-object v0, p0, Lmmj;->b:Lmme;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1085
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1086
    iget-object v0, p0, Lmmj;->d:[Lmmk;

    if-nez v0, :cond_5

    move v0, v1

    .line 1087
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmmk;

    .line 1088
    iget-object v3, p0, Lmmj;->d:[Lmmk;

    if-eqz v3, :cond_4

    .line 1089
    iget-object v3, p0, Lmmj;->d:[Lmmk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1091
    :cond_4
    iput-object v2, p0, Lmmj;->d:[Lmmk;

    .line 1092
    :goto_2
    iget-object v2, p0, Lmmj;->d:[Lmmk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1093
    iget-object v2, p0, Lmmj;->d:[Lmmk;

    new-instance v3, Lmmk;

    invoke-direct {v3}, Lmmk;-><init>()V

    aput-object v3, v2, v0

    .line 1094
    iget-object v2, p0, Lmmj;->d:[Lmmk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1095
    invoke-virtual {p1}, Loxn;->a()I

    .line 1092
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1086
    :cond_5
    iget-object v0, p0, Lmmj;->d:[Lmmk;

    array-length v0, v0

    goto :goto_1

    .line 1098
    :cond_6
    iget-object v2, p0, Lmmj;->d:[Lmmk;

    new-instance v3, Lmmk;

    invoke-direct {v3}, Lmmk;-><init>()V

    aput-object v3, v2, v0

    .line 1099
    iget-object v2, p0, Lmmj;->d:[Lmmk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1103
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1104
    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-ne v0, v2, :cond_8

    .line 1107
    :cond_7
    iput v0, p0, Lmmj;->e:I

    goto/16 :goto_0

    .line 1109
    :cond_8
    iput v1, p0, Lmmj;->e:I

    goto/16 :goto_0

    .line 1114
    :sswitch_5
    iget-object v0, p0, Lmmj;->c:Lmme;

    if-nez v0, :cond_9

    .line 1115
    new-instance v0, Lmme;

    invoke-direct {v0}, Lmme;-><init>()V

    iput-object v0, p0, Lmmj;->c:Lmme;

    .line 1117
    :cond_9
    iget-object v0, p0, Lmmj;->c:Lmme;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1056
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 995
    iget-object v0, p0, Lmmj;->a:Lmon;

    if-eqz v0, :cond_0

    .line 996
    const/4 v0, 0x1

    iget-object v1, p0, Lmmj;->a:Lmon;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 998
    :cond_0
    iget-object v0, p0, Lmmj;->b:Lmme;

    if-eqz v0, :cond_1

    .line 999
    const/4 v0, 0x2

    iget-object v1, p0, Lmmj;->b:Lmme;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1001
    :cond_1
    iget-object v0, p0, Lmmj;->d:[Lmmk;

    if-eqz v0, :cond_3

    .line 1002
    iget-object v1, p0, Lmmj;->d:[Lmmk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1003
    if-eqz v3, :cond_2

    .line 1004
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1002
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1008
    :cond_3
    iget v0, p0, Lmmj;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 1009
    const/4 v0, 0x4

    iget v1, p0, Lmmj;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1011
    :cond_4
    iget-object v0, p0, Lmmj;->c:Lmme;

    if-eqz v0, :cond_5

    .line 1012
    const/4 v0, 0x5

    iget-object v1, p0, Lmmj;->c:Lmme;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1014
    :cond_5
    iget-object v0, p0, Lmmj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1016
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 965
    invoke-virtual {p0, p1}, Lmmj;->a(Loxn;)Lmmj;

    move-result-object v0

    return-object v0
.end method
