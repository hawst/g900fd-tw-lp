.class public final Leqj;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lt;-><init>()V

    .line 30
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Lt;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 34
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    const-string v1, "plus_page"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 36
    invoke-virtual {p0, v0}, Leqj;->f(Landroid/os/Bundle;)V

    .line 37
    return-void
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 44
    invoke-virtual {p0}, Leqj;->n()Lz;

    move-result-object v0

    .line 45
    invoke-virtual {p0}, Leqj;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "plus_page"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 46
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 47
    if-eqz v1, :cond_0

    const v0, 0x7f0a0850

    :goto_0
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 50
    if-eqz v1, :cond_1

    const v0, 0x7f0a0852

    :goto_1
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 54
    const v0, 0x104000a

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 55
    const/high16 v0, 0x1040000

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 56
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 57
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 47
    :cond_0
    const v0, 0x7f0a084f

    goto :goto_0

    .line 50
    :cond_1
    const v0, 0x7f0a0851

    goto :goto_1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 65
    packed-switch p2, :pswitch_data_0

    .line 80
    :goto_0
    return-void

    .line 67
    :pswitch_0
    invoke-virtual {p0}, Leqj;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "person_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 68
    invoke-virtual {p0}, Leqj;->u_()Lu;

    move-result-object v0

    instance-of v0, v0, Leqk;

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p0}, Leqj;->u_()Lu;

    move-result-object v0

    check-cast v0, Leqk;

    invoke-interface {v0, v1}, Leqk;->o(Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_0
    invoke-virtual {p0}, Leqj;->n()Lz;

    move-result-object v0

    check-cast v0, Leqk;

    invoke-interface {v0, v1}, Leqk;->o(Ljava/lang/String;)V

    goto :goto_0

    .line 76
    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 65
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
