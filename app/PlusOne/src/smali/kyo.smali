.class public final Lkyo;
.super Llch;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Libe;
.implements Llda;


# instance fields
.field private c:Llca;

.field private d:Llcj;

.field private e:Landroid/widget/TextView;

.field private f:Z

.field private g:Llcu;

.field private h:I

.field private i:Liwk;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lkyo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lkyo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Llch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkyo;->f:Z

    .line 71
    new-instance v0, Llcj;

    invoke-direct {v0, p1, p2, p3}, Llcj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lkyo;->d:Llcj;

    .line 73
    const/16 v0, 0xb

    invoke-static {p1, p2, p3, v0}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lkyo;->e:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lkyo;->e:Landroid/widget/TextView;

    const v1, 0x7f0a0486

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 76
    iget-object v0, p0, Lkyo;->e:Landroid/widget/TextView;

    new-instance v1, Lhmk;

    sget-object v2, Lomv;->ag:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 78
    iget-object v0, p0, Lkyo;->e:Landroid/widget/TextView;

    new-instance v1, Lhmi;

    invoke-direct {v1, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v0, p0, Lkyo;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020416

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 81
    iget-object v0, p0, Lkyo;->e:Landroid/widget/TextView;

    sget-object v1, Lkyo;->z:Llct;

    iget v1, v1, Llct;->m:I

    sget-object v2, Lkyo;->z:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Lkyo;->z:Llct;

    iget v3, v3, Llct;->m:I

    sget-object v4, Lkyo;->z:Llct;

    iget v4, v4, Llct;->m:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 83
    new-instance v0, Llcu;

    invoke-direct {v0, p0}, Llcu;-><init>(Llda;)V

    iput-object v0, p0, Lkyo;->g:Llcu;

    .line 85
    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 87
    new-instance v1, Liwk;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-direct {v1, p1, v0}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v0, Lixj;

    .line 88
    invoke-virtual {v1, v0}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lkyo;->i:Liwk;

    .line 89
    return-void
.end method


# virtual methods
.method protected a(IIII)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 141
    .line 142
    iget v1, p0, Lkyo;->t:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 144
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 146
    invoke-virtual {p0}, Lkyo;->getChildCount()I

    move-result v3

    .line 147
    :goto_0
    if-ge v0, v3, :cond_0

    .line 148
    invoke-virtual {p0, v0}, Lkyo;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 149
    invoke-virtual {v4, v1, v2}, Landroid/view/View;->measure(II)V

    .line 150
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr p2, v4

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    :cond_0
    return p2
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lkyo;->getHeight()I

    move-result v0

    add-int/2addr v0, p6

    return v0
.end method

.method public a(IZ)Landroid/view/View;
    .locals 5

    .prologue
    .line 127
    new-instance v0, Lkye;

    invoke-virtual {p0}, Lkyo;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lkye;-><init>(Landroid/content/Context;)V

    .line 128
    iget-object v1, p0, Lkyo;->r:Ljava/lang/String;

    iget-object v2, p0, Lkyo;->a:Llci;

    iget-object v3, p0, Lkyo;->c:Llca;

    invoke-virtual {v3, p1}, Llca;->a(I)Llbz;

    move-result-object v3

    iget v4, p0, Lkyo;->b:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lkye;->a(Ljava/lang/String;Llci;Llbz;I)V

    .line 130
    return-object v0
.end method

.method public a(Landroid/view/View;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/List",
            "<",
            "Libd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 241
    invoke-virtual {p0}, Lkyo;->getChildCount()I

    move-result v4

    .line 242
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    .line 243
    invoke-virtual {p0, v2}, Lkyo;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 244
    instance-of v1, v0, Lkye;

    if-eqz v1, :cond_0

    .line 245
    invoke-static {v0, p1}, Llii;->a(Landroid/view/View;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 246
    check-cast v0, Lkye;

    .line 247
    new-instance v5, Libd;

    const-string v6, "g:"

    .line 248
    invoke-virtual {v0}, Lkye;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0}, Lkye;->c()Ljava/lang/String;

    move-result-object v0

    const/16 v6, 0x6e

    invoke-direct {v5, v1, v0, v6}, Libd;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 247
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 248
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 252
    :cond_2
    return-object v3
.end method

.method public a()V
    .locals 1

    .prologue
    .line 181
    invoke-super {p0}, Llch;->a()V

    .line 182
    invoke-virtual {p0}, Lkyo;->m()V

    .line 183
    iget-object v0, p0, Lkyo;->g:Llcu;

    invoke-virtual {v0}, Llcu;->a()V

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lkyo;->c:Llca;

    .line 185
    return-void
.end method

.method protected a(Landroid/database/Cursor;Llcr;I)V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0, p1, p2, p3}, Llch;->a(Landroid/database/Cursor;Llcr;I)V

    .line 95
    const/16 v0, 0x1b

    .line 96
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Llca;->a([B)Llca;

    move-result-object v0

    iput-object v0, p0, Lkyo;->c:Llca;

    .line 97
    const/4 v0, 0x2

    iget-object v1, p0, Lkyo;->c:Llca;

    invoke-virtual {v1}, Llca;->a()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lkyo;->h:I

    .line 99
    iget-object v0, p0, Lkyo;->g:Llcu;

    iget-object v1, p0, Lkyo;->c:Llca;

    invoke-virtual {v1}, Llca;->b()Ljava/util/ArrayList;

    move-result-object v1

    iget v2, p0, Lkyo;->h:I

    invoke-virtual {v0, v1, v2}, Llcu;->a(Ljava/util/List;I)V

    .line 101
    iget v0, p0, Lkyo;->b:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lkyo;->f:Z

    .line 102
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(ZIIII)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 157
    invoke-super/range {p0 .. p5}, Llch;->a(ZIIII)V

    .line 159
    iget-object v1, p0, Lkyo;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 160
    iget v2, p0, Lkyo;->t:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 162
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 163
    invoke-virtual {p0}, Lkyo;->getChildCount()I

    move-result v4

    :goto_0
    if-ge v0, v4, :cond_0

    .line 164
    invoke-virtual {p0, v0}, Lkyo;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 165
    invoke-virtual {v5, v2, v3}, Landroid/view/View;->measure(II)V

    .line 166
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 167
    iget-object v7, p0, Lkyo;->q:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Lkyo;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget v9, p0, Lkyo;->t:I

    add-int/2addr v8, v9

    add-int v9, v1, v6

    invoke-virtual {v5, v7, v1, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 169
    add-int/2addr v1, v6

    .line 163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_0
    return-void
.end method

.method public f()V
    .locals 4

    .prologue
    .line 106
    invoke-virtual {p0}, Lkyo;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v0, p0, Lkyo;->b:I

    const/4 v2, 0x7

    if-ne v0, v2, :cond_0

    const v0, 0x7f0a0488

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lkyo;->d:Llcj;

    sget-object v2, Lkyo;->z:Llct;

    iget v2, v2, Llct;->an:I

    sget-object v3, Lkyo;->z:Llct;

    iget-object v3, v3, Llct;->ap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v0, v3}, Llcj;->a(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 112
    iget-object v0, p0, Lkyo;->d:Llcj;

    invoke-virtual {p0, v0}, Lkyo;->addView(Landroid/view/View;)V

    .line 113
    return-void

    .line 106
    :cond_0
    const v0, 0x7f0a0487

    goto :goto_0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lkyo;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lkyo;->addView(Landroid/view/View;)V

    .line 118
    return-void
.end method

.method public h()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lkyo;->h:I

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Lkyo;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lkye;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public j()Llcu;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lkyo;->g:Llcu;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lkyo;->e:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lkyo;->a:Llci;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lkyo;->i:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    invoke-virtual {p0}, Lkyo;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lkyo;->i:Liwk;

    invoke-virtual {v1}, Liwk;->b()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    iget-object v0, p0, Lkyo;->a:Llci;

    iget-boolean v1, p0, Lkyo;->f:Z

    invoke-interface {v0, v1}, Llci;->k(Z)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lkyo;->g:Llcu;

    invoke-virtual {v0, p1}, Llcu;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lkyo;->g:Llcu;

    invoke-virtual {v0, p1}, Llcu;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
