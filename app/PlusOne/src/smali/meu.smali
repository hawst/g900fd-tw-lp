.class public final Lmeu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Llyl;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10771
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10774
    iput-object v0, p0, Lmeu;->apiHeader:Llyq;

    .line 10777
    iput-object v0, p0, Lmeu;->a:Llyl;

    .line 10771
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 10794
    const/4 v0, 0x0

    .line 10795
    iget-object v1, p0, Lmeu;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 10796
    const/4 v0, 0x1

    iget-object v1, p0, Lmeu;->apiHeader:Llyq;

    .line 10797
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10799
    :cond_0
    iget-object v1, p0, Lmeu;->a:Llyl;

    if-eqz v1, :cond_1

    .line 10800
    const/4 v1, 0x2

    iget-object v2, p0, Lmeu;->a:Llyl;

    .line 10801
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10803
    :cond_1
    iget-object v1, p0, Lmeu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10804
    iput v0, p0, Lmeu;->ai:I

    .line 10805
    return v0
.end method

.method public a(Loxn;)Lmeu;
    .locals 2

    .prologue
    .line 10813
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10814
    sparse-switch v0, :sswitch_data_0

    .line 10818
    iget-object v1, p0, Lmeu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 10819
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmeu;->ah:Ljava/util/List;

    .line 10822
    :cond_1
    iget-object v1, p0, Lmeu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10824
    :sswitch_0
    return-object p0

    .line 10829
    :sswitch_1
    iget-object v0, p0, Lmeu;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 10830
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmeu;->apiHeader:Llyq;

    .line 10832
    :cond_2
    iget-object v0, p0, Lmeu;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10836
    :sswitch_2
    iget-object v0, p0, Lmeu;->a:Llyl;

    if-nez v0, :cond_3

    .line 10837
    new-instance v0, Llyl;

    invoke-direct {v0}, Llyl;-><init>()V

    iput-object v0, p0, Lmeu;->a:Llyl;

    .line 10839
    :cond_3
    iget-object v0, p0, Lmeu;->a:Llyl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10814
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 10782
    iget-object v0, p0, Lmeu;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 10783
    const/4 v0, 0x1

    iget-object v1, p0, Lmeu;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10785
    :cond_0
    iget-object v0, p0, Lmeu;->a:Llyl;

    if-eqz v0, :cond_1

    .line 10786
    const/4 v0, 0x2

    iget-object v1, p0, Lmeu;->a:Llyl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10788
    :cond_1
    iget-object v0, p0, Lmeu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10790
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10767
    invoke-virtual {p0, p1}, Lmeu;->a(Loxn;)Lmeu;

    move-result-object v0

    return-object v0
.end method
