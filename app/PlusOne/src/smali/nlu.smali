.class public final Lnlu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 492
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 518
    const/4 v0, 0x0

    .line 519
    iget-object v1, p0, Lnlu;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 520
    const/4 v0, 0x1

    iget-object v1, p0, Lnlu;->b:Ljava/lang/String;

    .line 521
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 523
    :cond_0
    iget-object v1, p0, Lnlu;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 524
    const/4 v1, 0x2

    iget-object v2, p0, Lnlu;->a:Ljava/lang/Boolean;

    .line 525
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 527
    :cond_1
    iget-object v1, p0, Lnlu;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 528
    const/4 v1, 0x3

    iget-object v2, p0, Lnlu;->c:Ljava/lang/String;

    .line 529
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 531
    :cond_2
    iget-object v1, p0, Lnlu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 532
    iput v0, p0, Lnlu;->ai:I

    .line 533
    return v0
.end method

.method public a(Loxn;)Lnlu;
    .locals 2

    .prologue
    .line 541
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 542
    sparse-switch v0, :sswitch_data_0

    .line 546
    iget-object v1, p0, Lnlu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 547
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnlu;->ah:Ljava/util/List;

    .line 550
    :cond_1
    iget-object v1, p0, Lnlu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    :sswitch_0
    return-object p0

    .line 557
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnlu;->b:Ljava/lang/String;

    goto :goto_0

    .line 561
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnlu;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 565
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnlu;->c:Ljava/lang/String;

    goto :goto_0

    .line 542
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Lnlu;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 504
    const/4 v0, 0x1

    iget-object v1, p0, Lnlu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 506
    :cond_0
    iget-object v0, p0, Lnlu;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 507
    const/4 v0, 0x2

    iget-object v1, p0, Lnlu;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 509
    :cond_1
    iget-object v0, p0, Lnlu;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 510
    const/4 v0, 0x3

    iget-object v1, p0, Lnlu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 512
    :cond_2
    iget-object v0, p0, Lnlu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 514
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 488
    invoke-virtual {p0, p1}, Lnlu;->a(Loxn;)Lnlu;

    move-result-object v0

    return-object v0
.end method
