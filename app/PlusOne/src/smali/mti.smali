.class public final Lmti;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmth;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2426
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2429
    sget-object v0, Lmth;->a:[Lmth;

    iput-object v0, p0, Lmti;->a:[Lmth;

    .line 2438
    const/high16 v0, -0x80000000

    iput v0, p0, Lmti;->e:I

    .line 2426
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2468
    .line 2469
    iget-object v1, p0, Lmti;->a:[Lmth;

    if-eqz v1, :cond_1

    .line 2470
    iget-object v2, p0, Lmti;->a:[Lmth;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2471
    if-eqz v4, :cond_0

    .line 2472
    const/4 v5, 0x1

    .line 2473
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2470
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2477
    :cond_1
    iget-object v1, p0, Lmti;->b:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2478
    const/4 v1, 0x2

    iget-object v2, p0, Lmti;->b:Ljava/lang/Long;

    .line 2479
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2481
    :cond_2
    iget-object v1, p0, Lmti;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2482
    const/4 v1, 0x3

    iget-object v2, p0, Lmti;->c:Ljava/lang/String;

    .line 2483
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2485
    :cond_3
    iget-object v1, p0, Lmti;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2486
    const/4 v1, 0x4

    iget-object v2, p0, Lmti;->d:Ljava/lang/String;

    .line 2487
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2489
    :cond_4
    iget v1, p0, Lmti;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_5

    .line 2490
    const/4 v1, 0x5

    iget v2, p0, Lmti;->e:I

    .line 2491
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2493
    :cond_5
    iget-object v1, p0, Lmti;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2494
    iput v0, p0, Lmti;->ai:I

    .line 2495
    return v0
.end method

.method public a(Loxn;)Lmti;
    .locals 5

    .prologue
    const/16 v4, 0xa

    const/4 v1, 0x0

    .line 2503
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2504
    sparse-switch v0, :sswitch_data_0

    .line 2508
    iget-object v2, p0, Lmti;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2509
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmti;->ah:Ljava/util/List;

    .line 2512
    :cond_1
    iget-object v2, p0, Lmti;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2514
    :sswitch_0
    return-object p0

    .line 2519
    :sswitch_1
    invoke-static {p1, v4}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2520
    iget-object v0, p0, Lmti;->a:[Lmth;

    if-nez v0, :cond_3

    move v0, v1

    .line 2521
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmth;

    .line 2522
    iget-object v3, p0, Lmti;->a:[Lmth;

    if-eqz v3, :cond_2

    .line 2523
    iget-object v3, p0, Lmti;->a:[Lmth;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2525
    :cond_2
    iput-object v2, p0, Lmti;->a:[Lmth;

    .line 2526
    :goto_2
    iget-object v2, p0, Lmti;->a:[Lmth;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 2527
    iget-object v2, p0, Lmti;->a:[Lmth;

    new-instance v3, Lmth;

    invoke-direct {v3}, Lmth;-><init>()V

    aput-object v3, v2, v0

    .line 2528
    iget-object v2, p0, Lmti;->a:[Lmth;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2529
    invoke-virtual {p1}, Loxn;->a()I

    .line 2526
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2520
    :cond_3
    iget-object v0, p0, Lmti;->a:[Lmth;

    array-length v0, v0

    goto :goto_1

    .line 2532
    :cond_4
    iget-object v2, p0, Lmti;->a:[Lmth;

    new-instance v3, Lmth;

    invoke-direct {v3}, Lmth;-><init>()V

    aput-object v3, v2, v0

    .line 2533
    iget-object v2, p0, Lmti;->a:[Lmth;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2537
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmti;->b:Ljava/lang/Long;

    goto :goto_0

    .line 2541
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmti;->c:Ljava/lang/String;

    goto :goto_0

    .line 2545
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmti;->d:Ljava/lang/String;

    goto :goto_0

    .line 2549
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2550
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_5

    const/4 v2, 0x6

    if-eq v0, v2, :cond_5

    const/4 v2, 0x7

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8

    if-eq v0, v2, :cond_5

    const/16 v2, 0x9

    if-eq v0, v2, :cond_5

    if-eq v0, v4, :cond_5

    const/16 v2, 0xb

    if-eq v0, v2, :cond_5

    const/16 v2, 0xc

    if-eq v0, v2, :cond_5

    const/16 v2, 0xd

    if-eq v0, v2, :cond_5

    const/16 v2, 0xe

    if-ne v0, v2, :cond_6

    .line 2565
    :cond_5
    iput v0, p0, Lmti;->e:I

    goto/16 :goto_0

    .line 2567
    :cond_6
    iput v1, p0, Lmti;->e:I

    goto/16 :goto_0

    .line 2504
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2443
    iget-object v0, p0, Lmti;->a:[Lmth;

    if-eqz v0, :cond_1

    .line 2444
    iget-object v1, p0, Lmti;->a:[Lmth;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2445
    if-eqz v3, :cond_0

    .line 2446
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2444
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2450
    :cond_1
    iget-object v0, p0, Lmti;->b:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2451
    const/4 v0, 0x2

    iget-object v1, p0, Lmti;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2453
    :cond_2
    iget-object v0, p0, Lmti;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2454
    const/4 v0, 0x3

    iget-object v1, p0, Lmti;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2456
    :cond_3
    iget-object v0, p0, Lmti;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2457
    const/4 v0, 0x4

    iget-object v1, p0, Lmti;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2459
    :cond_4
    iget v0, p0, Lmti;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_5

    .line 2460
    const/4 v0, 0x5

    iget v1, p0, Lmti;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2462
    :cond_5
    iget-object v0, p0, Lmti;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2464
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2422
    invoke-virtual {p0, p1}, Lmti;->a(Loxn;)Lmti;

    move-result-object v0

    return-object v0
.end method
