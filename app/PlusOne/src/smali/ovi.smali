.class public final Lovi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lovf;

.field public b:Lovd;

.field private c:Lovb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 108
    invoke-direct {p0}, Loxq;-><init>()V

    .line 111
    iput-object v0, p0, Lovi;->a:Lovf;

    .line 114
    iput-object v0, p0, Lovi;->b:Lovd;

    .line 117
    iput-object v0, p0, Lovi;->c:Lovb;

    .line 108
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 137
    const/4 v0, 0x0

    .line 138
    iget-object v1, p0, Lovi;->a:Lovf;

    if-eqz v1, :cond_0

    .line 139
    const/4 v0, 0x1

    iget-object v1, p0, Lovi;->a:Lovf;

    .line 140
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 142
    :cond_0
    iget-object v1, p0, Lovi;->b:Lovd;

    if-eqz v1, :cond_1

    .line 143
    const/4 v1, 0x2

    iget-object v2, p0, Lovi;->b:Lovd;

    .line 144
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    :cond_1
    iget-object v1, p0, Lovi;->c:Lovb;

    if-eqz v1, :cond_2

    .line 147
    const/4 v1, 0x3

    iget-object v2, p0, Lovi;->c:Lovb;

    .line 148
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    :cond_2
    iget-object v1, p0, Lovi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    iput v0, p0, Lovi;->ai:I

    .line 152
    return v0
.end method

.method public a(Loxn;)Lovi;
    .locals 2

    .prologue
    .line 160
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 161
    sparse-switch v0, :sswitch_data_0

    .line 165
    iget-object v1, p0, Lovi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 166
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lovi;->ah:Ljava/util/List;

    .line 169
    :cond_1
    iget-object v1, p0, Lovi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    :sswitch_0
    return-object p0

    .line 176
    :sswitch_1
    iget-object v0, p0, Lovi;->a:Lovf;

    if-nez v0, :cond_2

    .line 177
    new-instance v0, Lovf;

    invoke-direct {v0}, Lovf;-><init>()V

    iput-object v0, p0, Lovi;->a:Lovf;

    .line 179
    :cond_2
    iget-object v0, p0, Lovi;->a:Lovf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 183
    :sswitch_2
    iget-object v0, p0, Lovi;->b:Lovd;

    if-nez v0, :cond_3

    .line 184
    new-instance v0, Lovd;

    invoke-direct {v0}, Lovd;-><init>()V

    iput-object v0, p0, Lovi;->b:Lovd;

    .line 186
    :cond_3
    iget-object v0, p0, Lovi;->b:Lovd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 190
    :sswitch_3
    iget-object v0, p0, Lovi;->c:Lovb;

    if-nez v0, :cond_4

    .line 191
    new-instance v0, Lovb;

    invoke-direct {v0}, Lovb;-><init>()V

    iput-object v0, p0, Lovi;->c:Lovb;

    .line 193
    :cond_4
    iget-object v0, p0, Lovi;->c:Lovb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 161
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lovi;->a:Lovf;

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x1

    iget-object v1, p0, Lovi;->a:Lovf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 125
    :cond_0
    iget-object v0, p0, Lovi;->b:Lovd;

    if-eqz v0, :cond_1

    .line 126
    const/4 v0, 0x2

    iget-object v1, p0, Lovi;->b:Lovd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 128
    :cond_1
    iget-object v0, p0, Lovi;->c:Lovb;

    if-eqz v0, :cond_2

    .line 129
    const/4 v0, 0x3

    iget-object v1, p0, Lovi;->c:Lovb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 131
    :cond_2
    iget-object v0, p0, Lovi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 133
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lovi;->a(Loxn;)Lovi;

    move-result-object v0

    return-object v0
.end method
