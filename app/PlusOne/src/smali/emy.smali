.class final Lemy;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Lemx;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;ILemx;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 195
    iput-object p1, p0, Lemy;->a:Landroid/content/Context;

    .line 196
    iput-object p3, p0, Lemy;->c:Lemx;

    .line 197
    iput-object p4, p0, Lemy;->d:Ljava/lang/String;

    .line 198
    iput p2, p0, Lemy;->b:I

    .line 199
    iput-object p5, p0, Lemy;->e:Ljava/lang/String;

    .line 200
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 204
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 205
    :cond_0
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 213
    :goto_0
    return-object v0

    .line 208
    :cond_1
    iget-object v0, p0, Lemy;->a:Landroid/content/Context;

    iget v1, p0, Lemy;->b:I

    iget-object v2, p0, Lemy;->d:Ljava/lang/String;

    aget-object v3, p1, v4

    iget-object v5, p0, Lemy;->e:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Ldtc;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lkff;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lkff;->l()V

    .line 213
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v4, v6

    :cond_3
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lemy;->c:Lemx;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lemx;->a(Lemx;Z)V

    .line 219
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 186
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lemy;->a([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 186
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lemy;->a(Ljava/lang/Boolean;)V

    return-void
.end method
