.class final Lfkg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkhp;


# instance fields
.field final synthetic a:Lfjx;


# direct methods
.method constructor <init>(Lfjx;)V
    .locals 0

    .prologue
    .line 964
    iput-object p1, p0, Lfkg;->a:Lfjx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkhl;Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 967
    invoke-virtual {p1}, Lkhl;->u()Ljava/lang/String;

    move-result-object v2

    .line 968
    iget-object v0, p0, Lfkg;->a:Lfjx;

    .line 969
    invoke-static {v0}, Lfjx;->n(Lfjx;)Llnl;

    move-result-object v0

    const-class v3, Lhpu;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpu;

    invoke-virtual {v0}, Lhpu;->e()Ljava/util/List;

    move-result-object v3

    .line 971
    iget-object v0, p0, Lfkg;->a:Lfjx;

    const v4, 0x7f0a05b9

    invoke-virtual {v0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 972
    check-cast p2, Ljava/lang/Boolean;

    .line 973
    iget-object v0, p0, Lfkg;->a:Lfjx;

    .line 974
    invoke-static {v0}, Lfjx;->o(Lfjx;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->a(I)Lkhl;

    move-result-object v0

    check-cast v0, Lfiz;

    .line 976
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 977
    invoke-virtual {v0}, Lfiz;->aB_()V

    .line 1146
    :cond_0
    :goto_0
    return v5

    .line 978
    :cond_1
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 979
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lfkg;->a:Lfjx;

    invoke-static {v2}, Lfjx;->p(Lfjx;)Llnl;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0610

    .line 980
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0a0583

    .line 981
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 982
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 983
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 985
    new-instance v0, Lfkh;

    invoke-direct {v0, p0, v3}, Lfkh;-><init>(Lfkg;Ljava/util/List;)V

    .line 994
    invoke-virtual {v0, v1}, Lfkh;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 997
    :cond_2
    instance-of v0, p1, Lfiz;

    if-eqz v0, :cond_4

    .line 998
    check-cast p1, Lfjh;

    .line 999
    check-cast p2, Ljava/lang/String;

    .line 1001
    invoke-virtual {p1}, Lfjh;->b()Ljava/lang/String;

    move-result-object v0

    .line 1003
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1007
    :cond_3
    new-instance v0, Lfki;

    invoke-direct {v0, p0, v3, p2}, Lfki;-><init>(Lfkg;Ljava/util/List;Ljava/lang/String;)V

    .line 1044
    invoke-virtual {v0, v1}, Lfki;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1046
    :cond_4
    iget-object v0, p0, Lfkg;->a:Lfjx;

    const v4, 0x7f0a05bd

    invoke-virtual {v0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1047
    check-cast p2, Ljava/lang/Boolean;

    .line 1049
    iget-object v2, p0, Lfkg;->a:Lfjx;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lhmv;->r:Lhmv;

    :goto_1
    invoke-static {v2, v3, v0}, Lfjx;->a(Lfjx;Ljava/util/List;Lhmv;)V

    .line 1052
    new-instance v0, Lfkk;

    invoke-direct {v0, p0, p2}, Lfkk;-><init>(Lfkg;Ljava/lang/Boolean;)V

    .line 1058
    invoke-virtual {v0, v1}, Lfkk;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1049
    :cond_5
    sget-object v0, Lhmv;->s:Lhmv;

    goto :goto_1

    .line 1059
    :cond_6
    iget-object v0, p0, Lfkg;->a:Lfjx;

    const v4, 0x7f0a05be

    invoke-virtual {v0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1060
    check-cast p2, Ljava/lang/Boolean;

    .line 1062
    new-instance v0, Lfkl;

    invoke-direct {v0, p0, p2}, Lfkl;-><init>(Lfkg;Ljava/lang/Boolean;)V

    .line 1070
    invoke-virtual {v0, v1}, Lfkl;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 1071
    :cond_7
    iget-object v0, p0, Lfkg;->a:Lfjx;

    const v4, 0x7f0a05bb

    invoke-virtual {v0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1072
    check-cast p2, Ljava/lang/String;

    .line 1075
    const-string v0, "WIFI_ONLY"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1076
    iget-object v0, p0, Lfkg;->a:Lfjx;

    sget-object v2, Lhmv;->q:Lhmv;

    invoke-static {v0, v3, v2}, Lfjx;->a(Lfjx;Ljava/util/List;Lhmv;)V

    .line 1077
    const v0, 0x7f0a06ca

    invoke-virtual {p1, v0}, Lkhl;->i(I)V

    .line 1078
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1087
    :goto_2
    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1088
    new-instance v2, Lfkm;

    invoke-direct {v2, p0, v0}, Lfkm;-><init>(Lfkg;Ljava/lang/Boolean;)V

    .line 1094
    invoke-virtual {v2, v1}, Lfkm;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 1079
    :cond_8
    const-string v0, "WIFI_OR_MOBILE"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1080
    iget-object v0, p0, Lfkg;->a:Lfjx;

    sget-object v2, Lhmv;->p:Lhmv;

    invoke-static {v0, v3, v2}, Lfjx;->a(Lfjx;Ljava/util/List;Lhmv;)V

    .line 1081
    const v0, 0x7f0a06cb

    invoke-virtual {p1, v0}, Lkhl;->i(I)V

    .line 1082
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2

    :cond_9
    move-object v0, v1

    .line 1084
    goto :goto_2

    .line 1096
    :cond_a
    iget-object v0, p0, Lfkg;->a:Lfjx;

    const v4, 0x7f0a05bf

    invoke-virtual {v0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1097
    check-cast p2, Ljava/lang/String;

    .line 1100
    const-string v0, "WIFI_ONLY"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1101
    iget-object v0, p0, Lfkg;->a:Lfjx;

    sget-object v2, Lhmv;->o:Lhmv;

    invoke-static {v0, v3, v2}, Lfjx;->a(Lfjx;Ljava/util/List;Lhmv;)V

    .line 1102
    const v0, 0x7f0a06d0

    invoke-virtual {p1, v0}, Lkhl;->i(I)V

    .line 1103
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1104
    iget-object v2, p0, Lfkg;->a:Lfjx;

    invoke-static {v2}, Lfjx;->A(Lfjx;)Llnl;

    move-result-object v2

    invoke-static {v2}, Lhqd;->c(Landroid/content/Context;)V

    .line 1113
    :goto_3
    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1114
    new-instance v2, Lfkn;

    invoke-direct {v2, p0, v0}, Lfkn;-><init>(Lfkg;Ljava/lang/Boolean;)V

    .line 1120
    invoke-virtual {v2, v1}, Lfkn;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 1105
    :cond_b
    const-string v0, "WIFI_OR_MOBILE"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1106
    iget-object v0, p0, Lfkg;->a:Lfjx;

    sget-object v2, Lhmv;->n:Lhmv;

    invoke-static {v0, v3, v2}, Lfjx;->a(Lfjx;Ljava/util/List;Lhmv;)V

    .line 1107
    const v0, 0x7f0a06d1

    invoke-virtual {p1, v0}, Lkhl;->i(I)V

    .line 1108
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_3

    :cond_c
    move-object v0, v1

    .line 1110
    goto :goto_3

    .line 1122
    :cond_d
    iget-object v0, p0, Lfkg;->a:Lfjx;

    const v4, 0x7f0a05c0

    invoke-virtual {v0, v4}, Lfjx;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1123
    check-cast p2, Ljava/lang/String;

    .line 1126
    const-string v0, "FULL"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1127
    iget-object v0, p0, Lfkg;->a:Lfjx;

    sget-object v2, Lhmv;->u:Lhmv;

    invoke-static {v0, v3, v2}, Lfjx;->a(Lfjx;Ljava/util/List;Lhmv;)V

    .line 1128
    const v0, 0x7f0a06d9

    invoke-virtual {p1, v0}, Lkhl;->i(I)V

    .line 1129
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1136
    :goto_4
    new-instance v2, Lfko;

    invoke-direct {v2, p0, v3, v0}, Lfko;-><init>(Lfkg;Ljava/util/List;Ljava/lang/Boolean;)V

    .line 1144
    invoke-virtual {v2, v1}, Lfko;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 1131
    :cond_e
    iget-object v0, p0, Lfkg;->a:Lfjx;

    sget-object v2, Lhmv;->t:Lhmv;

    invoke-static {v0, v3, v2}, Lfjx;->a(Lfjx;Ljava/util/List;Lhmv;)V

    .line 1132
    const v0, 0x7f0a06da

    invoke-virtual {p1, v0}, Lkhl;->i(I)V

    .line 1133
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_4
.end method
