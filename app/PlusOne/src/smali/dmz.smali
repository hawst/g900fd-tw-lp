.class public final Ldmz;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmgu;",
        "Lmgv;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:I

.field private final q:Z

.field private final r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 32
    const-string v3, "photossearch"

    new-instance v4, Lmgu;

    invoke-direct {v4}, Lmgu;-><init>()V

    new-instance v5, Lmgv;

    invoke-direct {v5}, Lmgv;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 34
    iput-object p5, p0, Ldmz;->a:Ljava/lang/String;

    .line 35
    iput-boolean p7, p0, Ldmz;->q:Z

    .line 36
    iput-object p3, p0, Ldmz;->b:Ljava/lang/String;

    .line 37
    iput p4, p0, Ldmz;->p:I

    .line 38
    iput-object p6, p0, Ldmz;->r:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method protected a(Lmgu;)V
    .locals 3

    .prologue
    .line 43
    new-instance v0, Lnen;

    invoke-direct {v0}, Lnen;-><init>()V

    iput-object v0, p1, Lmgu;->a:Lnen;

    .line 44
    iget-object v0, p1, Lmgu;->a:Lnen;

    .line 45
    iget-object v1, p0, Ldmz;->a:Ljava/lang/String;

    iput-object v1, v0, Lnen;->b:Ljava/lang/String;

    .line 46
    new-instance v1, Lner;

    invoke-direct {v1}, Lner;-><init>()V

    iput-object v1, v0, Lnen;->c:Lner;

    .line 47
    iget-object v1, v0, Lnen;->c:Lner;

    iget v2, p0, Ldmz;->p:I

    iput v2, v1, Lner;->b:I

    .line 48
    iget-object v1, v0, Lnen;->c:Lner;

    iget-object v2, p0, Ldmz;->b:Ljava/lang/String;

    iput-object v2, v1, Lner;->a:Ljava/lang/String;

    .line 49
    const/16 v1, 0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lnen;->a:Ljava/lang/Integer;

    .line 50
    new-instance v1, Lnei;

    invoke-direct {v1}, Lnei;-><init>()V

    iput-object v1, v0, Lnen;->d:Lnei;

    .line 51
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->m:Ljava/lang/Boolean;

    .line 52
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->g:Ljava/lang/Boolean;

    .line 53
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->i:Ljava/lang/Boolean;

    .line 54
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->a:Ljava/lang/Boolean;

    .line 55
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->l:Ljava/lang/Boolean;

    .line 56
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->h:Ljava/lang/Boolean;

    .line 57
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->k:Ljava/lang/Boolean;

    .line 58
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->o:Ljava/lang/Boolean;

    .line 59
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->e:Ljava/lang/Boolean;

    .line 60
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->f:Ljava/lang/Boolean;

    .line 61
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->d:Ljava/lang/Boolean;

    .line 62
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->b:Ljava/lang/Boolean;

    .line 63
    iget-object v1, v0, Lnen;->d:Lnei;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lnei;->c:Ljava/lang/Boolean;

    .line 64
    iget-object v0, v0, Lnen;->d:Lnei;

    const/4 v1, 0x2

    iput v1, v0, Lnei;->j:I

    .line 65
    return-void
.end method

.method protected a(Lmgv;)V
    .locals 8

    .prologue
    .line 69
    iget-object v3, p1, Lmgv;->a:Lnfn;

    .line 70
    iget-object v0, p0, Ldmz;->f:Landroid/content/Context;

    iget v1, p0, Ldmz;->c:I

    iget-object v2, p0, Ldmz;->r:Ljava/lang/String;

    iget-object v4, v3, Lnfn;->b:Ljava/lang/String;

    iget-boolean v5, p0, Ldmz;->q:Z

    invoke-static {v0, v1, v2, v4, v5}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 72
    iget-object v0, p0, Ldmz;->f:Landroid/content/Context;

    iget v1, p0, Ldmz;->c:I

    iget-object v2, p0, Ldmz;->r:Ljava/lang/String;

    iget-object v3, v3, Lnfn;->a:[Lnzx;

    iget-boolean v4, p0, Ldmz;->q:Z

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 74
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmgu;

    invoke-virtual {p0, p1}, Ldmz;->a(Lmgu;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmgv;

    invoke-virtual {p0, p1}, Ldmz;->a(Lmgv;)V

    return-void
.end method
