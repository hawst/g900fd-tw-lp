.class public final Lnto;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnrx;

.field public b:[Lnsq;

.field public c:[Lnru;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 423
    invoke-direct {p0}, Loxq;-><init>()V

    .line 426
    sget-object v0, Lnrx;->a:[Lnrx;

    iput-object v0, p0, Lnto;->a:[Lnrx;

    .line 429
    sget-object v0, Lnsq;->a:[Lnsq;

    iput-object v0, p0, Lnto;->b:[Lnsq;

    .line 432
    sget-object v0, Lnru;->a:[Lnru;

    iput-object v0, p0, Lnto;->c:[Lnru;

    .line 423
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 464
    .line 465
    iget-object v0, p0, Lnto;->a:[Lnrx;

    if-eqz v0, :cond_1

    .line 466
    iget-object v3, p0, Lnto;->a:[Lnrx;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 467
    if-eqz v5, :cond_0

    .line 468
    const/4 v6, 0x2

    .line 469
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 466
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 473
    :cond_2
    iget-object v2, p0, Lnto;->b:[Lnsq;

    if-eqz v2, :cond_4

    .line 474
    iget-object v3, p0, Lnto;->b:[Lnsq;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 475
    if-eqz v5, :cond_3

    .line 476
    const/4 v6, 0x3

    .line 477
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 474
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 481
    :cond_4
    iget-object v2, p0, Lnto;->c:[Lnru;

    if-eqz v2, :cond_6

    .line 482
    iget-object v2, p0, Lnto;->c:[Lnru;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 483
    if-eqz v4, :cond_5

    .line 484
    const/4 v5, 0x4

    .line 485
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 482
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 489
    :cond_6
    iget-object v1, p0, Lnto;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    iput v0, p0, Lnto;->ai:I

    .line 491
    return v0
.end method

.method public a(Loxn;)Lnto;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 499
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 500
    sparse-switch v0, :sswitch_data_0

    .line 504
    iget-object v2, p0, Lnto;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 505
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnto;->ah:Ljava/util/List;

    .line 508
    :cond_1
    iget-object v2, p0, Lnto;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 510
    :sswitch_0
    return-object p0

    .line 515
    :sswitch_1
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 516
    iget-object v0, p0, Lnto;->a:[Lnrx;

    if-nez v0, :cond_3

    move v0, v1

    .line 517
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnrx;

    .line 518
    iget-object v3, p0, Lnto;->a:[Lnrx;

    if-eqz v3, :cond_2

    .line 519
    iget-object v3, p0, Lnto;->a:[Lnrx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 521
    :cond_2
    iput-object v2, p0, Lnto;->a:[Lnrx;

    .line 522
    :goto_2
    iget-object v2, p0, Lnto;->a:[Lnrx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 523
    iget-object v2, p0, Lnto;->a:[Lnrx;

    new-instance v3, Lnrx;

    invoke-direct {v3}, Lnrx;-><init>()V

    aput-object v3, v2, v0

    .line 524
    iget-object v2, p0, Lnto;->a:[Lnrx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 525
    invoke-virtual {p1}, Loxn;->a()I

    .line 522
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 516
    :cond_3
    iget-object v0, p0, Lnto;->a:[Lnrx;

    array-length v0, v0

    goto :goto_1

    .line 528
    :cond_4
    iget-object v2, p0, Lnto;->a:[Lnrx;

    new-instance v3, Lnrx;

    invoke-direct {v3}, Lnrx;-><init>()V

    aput-object v3, v2, v0

    .line 529
    iget-object v2, p0, Lnto;->a:[Lnrx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 533
    :sswitch_2
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 534
    iget-object v0, p0, Lnto;->b:[Lnsq;

    if-nez v0, :cond_6

    move v0, v1

    .line 535
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsq;

    .line 536
    iget-object v3, p0, Lnto;->b:[Lnsq;

    if-eqz v3, :cond_5

    .line 537
    iget-object v3, p0, Lnto;->b:[Lnsq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 539
    :cond_5
    iput-object v2, p0, Lnto;->b:[Lnsq;

    .line 540
    :goto_4
    iget-object v2, p0, Lnto;->b:[Lnsq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 541
    iget-object v2, p0, Lnto;->b:[Lnsq;

    new-instance v3, Lnsq;

    invoke-direct {v3}, Lnsq;-><init>()V

    aput-object v3, v2, v0

    .line 542
    iget-object v2, p0, Lnto;->b:[Lnsq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 543
    invoke-virtual {p1}, Loxn;->a()I

    .line 540
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 534
    :cond_6
    iget-object v0, p0, Lnto;->b:[Lnsq;

    array-length v0, v0

    goto :goto_3

    .line 546
    :cond_7
    iget-object v2, p0, Lnto;->b:[Lnsq;

    new-instance v3, Lnsq;

    invoke-direct {v3}, Lnsq;-><init>()V

    aput-object v3, v2, v0

    .line 547
    iget-object v2, p0, Lnto;->b:[Lnsq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 551
    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 552
    iget-object v0, p0, Lnto;->c:[Lnru;

    if-nez v0, :cond_9

    move v0, v1

    .line 553
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lnru;

    .line 554
    iget-object v3, p0, Lnto;->c:[Lnru;

    if-eqz v3, :cond_8

    .line 555
    iget-object v3, p0, Lnto;->c:[Lnru;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 557
    :cond_8
    iput-object v2, p0, Lnto;->c:[Lnru;

    .line 558
    :goto_6
    iget-object v2, p0, Lnto;->c:[Lnru;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 559
    iget-object v2, p0, Lnto;->c:[Lnru;

    new-instance v3, Lnru;

    invoke-direct {v3}, Lnru;-><init>()V

    aput-object v3, v2, v0

    .line 560
    iget-object v2, p0, Lnto;->c:[Lnru;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 561
    invoke-virtual {p1}, Loxn;->a()I

    .line 558
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 552
    :cond_9
    iget-object v0, p0, Lnto;->c:[Lnru;

    array-length v0, v0

    goto :goto_5

    .line 564
    :cond_a
    iget-object v2, p0, Lnto;->c:[Lnru;

    new-instance v3, Lnru;

    invoke-direct {v3}, Lnru;-><init>()V

    aput-object v3, v2, v0

    .line 565
    iget-object v2, p0, Lnto;->c:[Lnru;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 500
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 437
    iget-object v1, p0, Lnto;->a:[Lnrx;

    if-eqz v1, :cond_1

    .line 438
    iget-object v2, p0, Lnto;->a:[Lnrx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 439
    if-eqz v4, :cond_0

    .line 440
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 438
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 444
    :cond_1
    iget-object v1, p0, Lnto;->b:[Lnsq;

    if-eqz v1, :cond_3

    .line 445
    iget-object v2, p0, Lnto;->b:[Lnsq;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 446
    if-eqz v4, :cond_2

    .line 447
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 445
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 451
    :cond_3
    iget-object v1, p0, Lnto;->c:[Lnru;

    if-eqz v1, :cond_5

    .line 452
    iget-object v1, p0, Lnto;->c:[Lnru;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 453
    if-eqz v3, :cond_4

    .line 454
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 452
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 458
    :cond_5
    iget-object v0, p0, Lnto;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 460
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 419
    invoke-virtual {p0, p1}, Lnto;->a(Loxn;)Lnto;

    move-result-object v0

    return-object v0
.end method
