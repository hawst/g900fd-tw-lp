.class public final Loic;
.super Loxq;
.source "PG"


# instance fields
.field private a:Loio;

.field private b:Ljava/lang/String;

.field private c:[Ljava/lang/String;

.field private d:Loid;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 870
    invoke-direct {p0}, Loxq;-><init>()V

    .line 955
    iput-object v1, p0, Loic;->a:Loio;

    .line 960
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loic;->c:[Ljava/lang/String;

    .line 963
    iput-object v1, p0, Loic;->d:Loid;

    .line 870
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 988
    .line 989
    iget-object v0, p0, Loic;->a:Loio;

    if-eqz v0, :cond_4

    .line 990
    const/4 v0, 0x1

    iget-object v2, p0, Loic;->a:Loio;

    .line 991
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 993
    :goto_0
    iget-object v2, p0, Loic;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 994
    const/4 v2, 0x2

    iget-object v3, p0, Loic;->b:Ljava/lang/String;

    .line 995
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 997
    :cond_0
    iget-object v2, p0, Loic;->c:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Loic;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 999
    iget-object v3, p0, Loic;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 1001
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 999
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1003
    :cond_1
    add-int/2addr v0, v2

    .line 1004
    iget-object v1, p0, Loic;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1006
    :cond_2
    iget-object v1, p0, Loic;->d:Loid;

    if-eqz v1, :cond_3

    .line 1007
    const/4 v1, 0x4

    iget-object v2, p0, Loic;->d:Loid;

    .line 1008
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1010
    :cond_3
    iget-object v1, p0, Loic;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1011
    iput v0, p0, Loic;->ai:I

    .line 1012
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loic;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1020
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1021
    sparse-switch v0, :sswitch_data_0

    .line 1025
    iget-object v1, p0, Loic;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1026
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loic;->ah:Ljava/util/List;

    .line 1029
    :cond_1
    iget-object v1, p0, Loic;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1031
    :sswitch_0
    return-object p0

    .line 1036
    :sswitch_1
    iget-object v0, p0, Loic;->a:Loio;

    if-nez v0, :cond_2

    .line 1037
    new-instance v0, Loio;

    invoke-direct {v0}, Loio;-><init>()V

    iput-object v0, p0, Loic;->a:Loio;

    .line 1039
    :cond_2
    iget-object v0, p0, Loic;->a:Loio;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1043
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loic;->b:Ljava/lang/String;

    goto :goto_0

    .line 1047
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1048
    iget-object v0, p0, Loic;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 1049
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1050
    iget-object v2, p0, Loic;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1051
    iput-object v1, p0, Loic;->c:[Ljava/lang/String;

    .line 1052
    :goto_1
    iget-object v1, p0, Loic;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 1053
    iget-object v1, p0, Loic;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1054
    invoke-virtual {p1}, Loxn;->a()I

    .line 1052
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1057
    :cond_3
    iget-object v1, p0, Loic;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1061
    :sswitch_4
    iget-object v0, p0, Loic;->d:Loid;

    if-nez v0, :cond_4

    .line 1062
    new-instance v0, Loid;

    invoke-direct {v0}, Loid;-><init>()V

    iput-object v0, p0, Loic;->d:Loid;

    .line 1064
    :cond_4
    iget-object v0, p0, Loic;->d:Loid;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1021
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 968
    iget-object v0, p0, Loic;->a:Loio;

    if-eqz v0, :cond_0

    .line 969
    const/4 v0, 0x1

    iget-object v1, p0, Loic;->a:Loio;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 971
    :cond_0
    iget-object v0, p0, Loic;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 972
    const/4 v0, 0x2

    iget-object v1, p0, Loic;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 974
    :cond_1
    iget-object v0, p0, Loic;->c:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 975
    iget-object v1, p0, Loic;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 976
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 975
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 979
    :cond_2
    iget-object v0, p0, Loic;->d:Loid;

    if-eqz v0, :cond_3

    .line 980
    const/4 v0, 0x4

    iget-object v1, p0, Loic;->d:Loid;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 982
    :cond_3
    iget-object v0, p0, Loic;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 984
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 866
    invoke-virtual {p0, p1}, Loic;->a(Loxn;)Loic;

    move-result-object v0

    return-object v0
.end method
