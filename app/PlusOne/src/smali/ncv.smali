.class public final Lncv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lncv;


# instance fields
.field public b:Lnww;

.field public c:Lnzx;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/String;

.field private f:Lncx;

.field private g:Lncx;

.field private h:Lorf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 556
    const/4 v0, 0x0

    new-array v0, v0, [Lncv;

    sput-object v0, Lncv;->a:[Lncv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 557
    invoke-direct {p0}, Loxq;-><init>()V

    .line 560
    iput-object v0, p0, Lncv;->f:Lncx;

    .line 563
    iput-object v0, p0, Lncv;->g:Lncx;

    .line 566
    iput-object v0, p0, Lncv;->b:Lnww;

    .line 569
    iput-object v0, p0, Lncv;->c:Lnzx;

    .line 572
    iput-object v0, p0, Lncv;->h:Lorf;

    .line 557
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 608
    const/4 v0, 0x0

    .line 609
    iget-object v1, p0, Lncv;->f:Lncx;

    if-eqz v1, :cond_0

    .line 610
    const/4 v0, 0x1

    iget-object v1, p0, Lncv;->f:Lncx;

    .line 611
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 613
    :cond_0
    iget-object v1, p0, Lncv;->g:Lncx;

    if-eqz v1, :cond_1

    .line 614
    const/4 v1, 0x2

    iget-object v2, p0, Lncv;->g:Lncx;

    .line 615
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 617
    :cond_1
    iget-object v1, p0, Lncv;->b:Lnww;

    if-eqz v1, :cond_2

    .line 618
    const/4 v1, 0x3

    iget-object v2, p0, Lncv;->b:Lnww;

    .line 619
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 621
    :cond_2
    iget-object v1, p0, Lncv;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 622
    const/4 v1, 0x4

    iget-object v2, p0, Lncv;->d:Ljava/lang/Boolean;

    .line 623
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 625
    :cond_3
    iget-object v1, p0, Lncv;->c:Lnzx;

    if-eqz v1, :cond_4

    .line 626
    const/4 v1, 0x5

    iget-object v2, p0, Lncv;->c:Lnzx;

    .line 627
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 629
    :cond_4
    iget-object v1, p0, Lncv;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 630
    const/4 v1, 0x6

    iget-object v2, p0, Lncv;->e:Ljava/lang/String;

    .line 631
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 633
    :cond_5
    iget-object v1, p0, Lncv;->h:Lorf;

    if-eqz v1, :cond_6

    .line 634
    const/4 v1, 0x7

    iget-object v2, p0, Lncv;->h:Lorf;

    .line 635
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 637
    :cond_6
    iget-object v1, p0, Lncv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 638
    iput v0, p0, Lncv;->ai:I

    .line 639
    return v0
.end method

.method public a(Loxn;)Lncv;
    .locals 2

    .prologue
    .line 647
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 648
    sparse-switch v0, :sswitch_data_0

    .line 652
    iget-object v1, p0, Lncv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 653
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lncv;->ah:Ljava/util/List;

    .line 656
    :cond_1
    iget-object v1, p0, Lncv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 658
    :sswitch_0
    return-object p0

    .line 663
    :sswitch_1
    iget-object v0, p0, Lncv;->f:Lncx;

    if-nez v0, :cond_2

    .line 664
    new-instance v0, Lncx;

    invoke-direct {v0}, Lncx;-><init>()V

    iput-object v0, p0, Lncv;->f:Lncx;

    .line 666
    :cond_2
    iget-object v0, p0, Lncv;->f:Lncx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 670
    :sswitch_2
    iget-object v0, p0, Lncv;->g:Lncx;

    if-nez v0, :cond_3

    .line 671
    new-instance v0, Lncx;

    invoke-direct {v0}, Lncx;-><init>()V

    iput-object v0, p0, Lncv;->g:Lncx;

    .line 673
    :cond_3
    iget-object v0, p0, Lncv;->g:Lncx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 677
    :sswitch_3
    iget-object v0, p0, Lncv;->b:Lnww;

    if-nez v0, :cond_4

    .line 678
    new-instance v0, Lnww;

    invoke-direct {v0}, Lnww;-><init>()V

    iput-object v0, p0, Lncv;->b:Lnww;

    .line 680
    :cond_4
    iget-object v0, p0, Lncv;->b:Lnww;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 684
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lncv;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 688
    :sswitch_5
    iget-object v0, p0, Lncv;->c:Lnzx;

    if-nez v0, :cond_5

    .line 689
    new-instance v0, Lnzx;

    invoke-direct {v0}, Lnzx;-><init>()V

    iput-object v0, p0, Lncv;->c:Lnzx;

    .line 691
    :cond_5
    iget-object v0, p0, Lncv;->c:Lnzx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 695
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncv;->e:Ljava/lang/String;

    goto :goto_0

    .line 699
    :sswitch_7
    iget-object v0, p0, Lncv;->h:Lorf;

    if-nez v0, :cond_6

    .line 700
    new-instance v0, Lorf;

    invoke-direct {v0}, Lorf;-><init>()V

    iput-object v0, p0, Lncv;->h:Lorf;

    .line 702
    :cond_6
    iget-object v0, p0, Lncv;->h:Lorf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 648
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 581
    iget-object v0, p0, Lncv;->f:Lncx;

    if-eqz v0, :cond_0

    .line 582
    const/4 v0, 0x1

    iget-object v1, p0, Lncv;->f:Lncx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 584
    :cond_0
    iget-object v0, p0, Lncv;->g:Lncx;

    if-eqz v0, :cond_1

    .line 585
    const/4 v0, 0x2

    iget-object v1, p0, Lncv;->g:Lncx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 587
    :cond_1
    iget-object v0, p0, Lncv;->b:Lnww;

    if-eqz v0, :cond_2

    .line 588
    const/4 v0, 0x3

    iget-object v1, p0, Lncv;->b:Lnww;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 590
    :cond_2
    iget-object v0, p0, Lncv;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 591
    const/4 v0, 0x4

    iget-object v1, p0, Lncv;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 593
    :cond_3
    iget-object v0, p0, Lncv;->c:Lnzx;

    if-eqz v0, :cond_4

    .line 594
    const/4 v0, 0x5

    iget-object v1, p0, Lncv;->c:Lnzx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 596
    :cond_4
    iget-object v0, p0, Lncv;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 597
    const/4 v0, 0x6

    iget-object v1, p0, Lncv;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 599
    :cond_5
    iget-object v0, p0, Lncv;->h:Lorf;

    if-eqz v0, :cond_6

    .line 600
    const/4 v0, 0x7

    iget-object v1, p0, Lncv;->h:Lorf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 602
    :cond_6
    iget-object v0, p0, Lncv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 604
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 553
    invoke-virtual {p0, p1}, Lncv;->a(Loxn;)Lncv;

    move-result-object v0

    return-object v0
.end method
