.class public final Lpzu;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 253
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 274
    const/4 v0, 0x0

    .line 275
    iget-object v1, p0, Lpzu;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 276
    const/4 v0, 0x1

    iget-object v1, p0, Lpzu;->a:Ljava/lang/Long;

    .line 277
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 279
    :cond_0
    iget-object v1, p0, Lpzu;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 280
    const/4 v1, 0x2

    iget-object v2, p0, Lpzu;->b:Ljava/lang/Long;

    .line 281
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 283
    :cond_1
    iget-object v1, p0, Lpzu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    iput v0, p0, Lpzu;->ai:I

    .line 285
    return v0
.end method

.method public a(Loxn;)Lpzu;
    .locals 2

    .prologue
    .line 293
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 294
    sparse-switch v0, :sswitch_data_0

    .line 298
    iget-object v1, p0, Lpzu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 299
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpzu;->ah:Ljava/util/List;

    .line 302
    :cond_1
    iget-object v1, p0, Lpzu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 304
    :sswitch_0
    return-object p0

    .line 309
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpzu;->a:Ljava/lang/Long;

    goto :goto_0

    .line 313
    :sswitch_2
    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpzu;->b:Ljava/lang/Long;

    goto :goto_0

    .line 294
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Lpzu;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 263
    const/4 v0, 0x1

    iget-object v1, p0, Lpzu;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 265
    :cond_0
    iget-object v0, p0, Lpzu;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 266
    const/4 v0, 0x2

    iget-object v1, p0, Lpzu;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->c(IJ)V

    .line 268
    :cond_1
    iget-object v0, p0, Lpzu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 270
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 249
    invoke-virtual {p0, p1}, Lpzu;->a(Loxn;)Lpzu;

    move-result-object v0

    return-object v0
.end method
