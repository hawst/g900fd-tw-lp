.class public final Lhft;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public static a(Landroid/content/Context;ILocf;ZZ)Lhgw;
    .locals 25

    .prologue
    .line 75
    if-nez p2, :cond_0

    .line 76
    const/4 v2, 0x0

    .line 137
    :goto_0
    return-object v2

    .line 79
    :cond_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 80
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 81
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 83
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "circle_id"

    aput-object v3, v6, v2

    const/4 v2, 0x1

    const-string v3, "type"

    aput-object v3, v6, v2

    const/4 v2, 0x2

    const-string v3, "contact_count"

    aput-object v3, v6, v2

    .line 85
    const-class v2, Lhxk;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhxk;

    const/4 v5, 0x5

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v9, p4

    invoke-interface/range {v2 .. v9}, Lhxk;->a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;IZ)Landroid/database/Cursor;

    move-result-object v2

    .line 87
    if-nez v2, :cond_1

    .line 88
    const/4 v2, 0x0

    goto :goto_0

    .line 94
    :cond_1
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 95
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 96
    const/4 v3, -0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 97
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 98
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 99
    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 100
    const/4 v5, 0x2

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 102
    const/4 v6, 0x1

    if-eq v4, v6, :cond_2

    .line 103
    invoke-virtual {v8, v4, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 105
    :cond_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 107
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 109
    move-object/from16 v0, p2

    iget-object v2, v0, Locf;->c:[Locm;

    if-eqz v2, :cond_2a

    move-object/from16 v0, p2

    iget-object v2, v0, Locf;->a:[Locg;

    if-eqz v2, :cond_2a

    .line 110
    move-object/from16 v0, p2

    iget-object v13, v0, Locf;->a:[Locg;

    array-length v14, v13

    const/4 v2, 0x0

    move v6, v2

    :goto_2
    if-ge v6, v14, :cond_2a

    aget-object v3, v13, v6

    .line 111
    if-eqz v3, :cond_7

    iget-object v2, v3, Locg;->c:Lock;

    if-eqz v2, :cond_7

    iget-object v2, v3, Locg;->c:Lock;

    iget-object v2, v2, Lock;->a:[Locn;

    if-eqz v2, :cond_7

    if-nez p3, :cond_4

    iget-object v2, v3, Locg;->b:Locj;

    if-eqz v2, :cond_7

    iget-object v2, v3, Locg;->b:Locj;

    iget-object v2, v2, Locj;->a:Ljava/lang/String;

    const-string v4, "pref:DEFAULT"

    invoke-static {v2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_29

    .line 112
    const/4 v2, 0x0

    .line 115
    iget-object v4, v3, Locg;->c:Lock;

    iget-object v4, v4, Lock;->b:Locn;

    if-eqz v4, :cond_2b

    iget-object v4, v3, Locg;->c:Lock;

    iget-object v4, v4, Lock;->b:Locn;

    iget v4, v4, Locn;->d:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2b

    .line 117
    const/4 v2, 0x1

    move v5, v2

    .line 119
    :goto_4
    iget-object v2, v3, Locg;->c:Lock;

    iget-object v15, v2, Lock;->a:[Locn;

    array-length v0, v15

    move/from16 v16, v0

    const/4 v2, 0x0

    move v7, v2

    :goto_5
    move/from16 v0, v16

    if-ge v7, v0, :cond_29

    aget-object v17, v15, v7

    .line 120
    const/4 v2, 0x0

    .line 121
    move-object/from16 v0, p2

    iget-object v0, v0, Locf;->c:[Locm;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v19, v0

    const/4 v3, 0x0

    :goto_6
    move/from16 v0, v19

    if-ge v3, v0, :cond_6

    aget-object v20, v18, v3

    .line 122
    move-object/from16 v0, v20

    iget-object v0, v0, Locm;->b:Locn;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->c:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v0, v0, Locn;->c:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    const/4 v4, 0x0

    :goto_7
    if-eqz v4, :cond_27

    .line 123
    move-object/from16 v0, v20

    iget-object v0, v0, Locm;->c:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v20

    iget-object v2, v0, Locm;->b:Locn;

    if-nez v2, :cond_1c

    const-string v2, "AclDataUtils"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "AclDataUtils"

    const-string v3, "null SharingTargetId"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_5
    :goto_8
    const/4 v2, 0x1

    .line 129
    :cond_6
    if-nez v2, :cond_28

    .line 130
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "sharingRoster contains a sharingTargetId that is not in targets"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 111
    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    .line 122
    :cond_8
    move-object/from16 v0, v21

    iget v4, v0, Locn;->d:I

    move-object/from16 v0, v17

    iget v0, v0, Locn;->d:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-eq v4, v0, :cond_9

    const/4 v4, 0x0

    goto :goto_7

    :cond_9
    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->f:Loce;

    if-nez v4, :cond_a

    move-object/from16 v0, v17

    iget-object v4, v0, Locn;->f:Loce;

    if-eqz v4, :cond_b

    :cond_a
    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->f:Loce;

    iget-object v4, v4, Loce;->a:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v0, v0, Locn;->f:Loce;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Loce;->a:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    const/4 v4, 0x0

    goto :goto_7

    :cond_b
    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->e:Loco;

    if-nez v4, :cond_c

    move-object/from16 v0, v17

    iget-object v4, v0, Locn;->e:Loco;

    if-eqz v4, :cond_d

    :cond_c
    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->e:Loco;

    iget-object v4, v4, Loco;->a:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v0, v0, Locn;->e:Loco;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Loco;->a:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    const/4 v4, 0x0

    goto/16 :goto_7

    :cond_d
    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->b:Lohp;

    move-object/from16 v0, v17

    iget-object v0, v0, Locn;->b:Lohp;

    move-object/from16 v22, v0

    if-nez v4, :cond_e

    if-eqz v22, :cond_16

    :cond_e
    if-eqz v4, :cond_f

    if-nez v22, :cond_10

    :cond_f
    const/4 v4, 0x0

    :goto_9
    if-nez v4, :cond_17

    const/4 v4, 0x0

    goto/16 :goto_7

    :cond_10
    iget-object v0, v4, Lohp;->c:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lohp;->c:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_11

    const/4 v4, 0x0

    goto :goto_9

    :cond_11
    iget-object v0, v4, Lohp;->b:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lohp;->b:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_12

    const/4 v4, 0x0

    goto :goto_9

    :cond_12
    iget-object v0, v4, Lohp;->d:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_13

    const/4 v4, 0x0

    goto :goto_9

    :cond_13
    iget-object v0, v4, Lohp;->e:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lohp;->e:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_14

    const/4 v4, 0x0

    goto :goto_9

    :cond_14
    iget-object v0, v4, Lohp;->g:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lohp;->g:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v23 .. v24}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_15

    const/4 v4, 0x0

    goto :goto_9

    :cond_15
    iget-object v4, v4, Lohp;->f:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v0, v0, Lohp;->f:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_16

    const/4 v4, 0x0

    goto :goto_9

    :cond_16
    const/4 v4, 0x1

    goto :goto_9

    :cond_17
    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->g:Locp;

    if-nez v4, :cond_18

    move-object/from16 v0, v17

    iget-object v4, v0, Locn;->g:Locp;

    if-eqz v4, :cond_19

    :cond_18
    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->g:Locp;

    iget-object v4, v4, Locp;->b:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v0, v0, Locn;->g:Locp;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Locp;->b:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_19

    const/4 v4, 0x0

    goto/16 :goto_7

    :cond_19
    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->g:Locp;

    if-nez v4, :cond_1a

    move-object/from16 v0, v17

    iget-object v4, v0, Locn;->g:Locp;

    if-eqz v4, :cond_1b

    :cond_1a
    move-object/from16 v0, v21

    iget-object v4, v0, Locn;->g:Locp;

    iget-object v4, v4, Locp;->a:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v0, v0, Locn;->g:Locp;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Locp;->a:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1b

    const/4 v4, 0x0

    goto/16 :goto_7

    :cond_1b
    const/4 v4, 0x1

    goto/16 :goto_7

    .line 123
    :cond_1c
    iget v3, v2, Locn;->d:I

    const/high16 v4, -0x80000000

    if-ne v3, v4, :cond_1d

    iget-object v3, v2, Locn;->c:Ljava/lang/String;

    if-eqz v3, :cond_20

    :cond_1d
    iget v3, v2, Locn;->d:I

    const/high16 v4, -0x80000000

    if-eq v3, v4, :cond_1e

    iget v2, v2, Locn;->d:I

    invoke-static {v2}, Lhxl;->a(I)I

    move-result v3

    invoke-virtual {v8, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_2c

    const-string v2, "AclDataUtils"

    const/4 v4, 0x6

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "AclDataUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v17, 0x29

    move/from16 v0, v17

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v17, "Circle ID not found for type: "

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    :cond_1e
    const/4 v3, 0x1

    iget-object v2, v2, Locn;->c:Ljava/lang/String;

    invoke-static {v2}, Lhxe;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move v4, v3

    move-object v3, v2

    :goto_a
    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-nez v2, :cond_1f

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :cond_1f
    new-instance v18, Lhxc;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v3, v4, v1, v2}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    :cond_20
    iget-object v3, v2, Locn;->b:Lohp;

    if-eqz v3, :cond_24

    iget-object v3, v2, Locn;->b:Lohp;

    iget-object v2, v3, Lohp;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_21

    move-object/from16 v0, v20

    iget-object v2, v0, Locm;->d:Ljava/lang/String;

    :cond_21
    iget-object v4, v3, Lohp;->d:Ljava/lang/String;

    if-nez v4, :cond_22

    iget-object v4, v3, Lohp;->b:Ljava/lang/String;

    if-eqz v4, :cond_23

    :cond_22
    new-instance v4, Ljqs;

    iget-object v3, v3, Lohp;->d:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v4, v3, v0, v2}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    :cond_23
    const-string v2, "AclDataUtils"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "AclDataUtils"

    const-string v3, "Invalid user from roster: "

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    :cond_24
    iget-object v3, v2, Locn;->g:Locp;

    if-eqz v3, :cond_26

    iget-object v3, v2, Locn;->g:Locp;

    const-class v2, Lhxn;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhxn;

    if-eqz v2, :cond_25

    iget-object v4, v3, Locp;->b:Ljava/lang/String;

    invoke-interface {v2}, Lhxn;->c()Z

    move-result v2

    if-eqz v2, :cond_25

    new-instance v2, Lhxs;

    iget-object v3, v3, Locp;->a:Ljava/lang/String;

    move-object/from16 v0, v20

    iget-object v4, v0, Locm;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Lhxs;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_8

    :cond_25
    const-string v2, "AclDataUtils"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "AclDataUtils"

    const-string v3, "Invalid SharingTargetId"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    :cond_26
    const-string v2, "AclDataUtils"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "AclDataUtils"

    const-string v3, "Invalid SharingTargetId"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_8

    .line 121
    :cond_27
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    .line 119
    :cond_28
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto/16 :goto_5

    .line 110
    :cond_29
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto/16 :goto_2

    .line 137
    :cond_2a
    new-instance v2, Lhgw;

    invoke-direct {v2, v10, v11, v12}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_2b
    move v5, v2

    goto/16 :goto_4

    :cond_2c
    move v4, v3

    move-object v3, v2

    goto/16 :goto_a
.end method

.method public static a(Lhgw;Lhgw;)Lock;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 308
    invoke-static {p0}, Lhft;->a(Lhgw;)[Locn;

    move-result-object v0

    .line 310
    invoke-static {p1}, Lhft;->a(Lhgw;)[Locn;

    move-result-object v1

    .line 312
    array-length v2, v1

    if-le v2, v3, :cond_0

    .line 313
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "restrictAudience"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 316
    :cond_0
    new-instance v2, Lock;

    invoke-direct {v2}, Lock;-><init>()V

    .line 317
    iput-object v0, v2, Lock;->a:[Locn;

    .line 318
    array-length v0, v1

    if-ne v0, v3, :cond_1

    const/4 v0, 0x0

    aget-object v0, v1, v0

    :goto_0
    iput-object v0, v2, Lock;->b:Locn;

    .line 321
    return-object v2

    .line 318
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILhgw;)V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 393
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move v0, v9

    move v5, v9

    :goto_0
    invoke-virtual {p2}, Lhgw;->g()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p2, v0}, Lhgw;->a(I)Ljqs;

    move-result-object v2

    invoke-virtual {v2}, Ljqs;->f()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v2, v5, 0x1

    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v5, v2

    goto :goto_0

    :cond_0
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v5

    goto :goto_1

    :cond_1
    new-instance v0, Lhgw;

    invoke-virtual {p2}, Lhgw;->b()[Lhxc;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p2}, Lhgw;->c()[Lkxr;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {p2}, Lhgw;->d()[Lhxs;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {p2}, Lhgw;->e()I

    move-result v6

    sub-int v5, v6, v5

    invoke-direct/range {v0 .. v5}, Lhgw;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    .line 396
    const-class v1, Lhzr;

    invoke-static {p0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhzr;

    .line 397
    invoke-virtual {v1, p0, p1}, Lhzr;->b(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 399
    const-string v2, "account_status"

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "audience_history"

    aput-object v4, v3, v9

    move-object v4, v10

    move-object v5, v10

    move-object v6, v10

    move-object v7, v10

    move-object v8, v10

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 405
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 406
    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    .line 407
    if-eqz v2, :cond_7

    .line 408
    invoke-static {v2}, Lhhg;->a([B)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 412
    :goto_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 414
    if-nez v2, :cond_6

    .line 415
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object v3, v2

    .line 420
    :goto_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 421
    invoke-static {v0}, Lhft;->b(Lhgw;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 422
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 426
    :goto_4
    if-ge v9, v5, :cond_4

    .line 427
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhgw;

    .line 428
    invoke-static {v0, v2}, Lhgw;->a(Lhgw;Lhgw;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 429
    invoke-static {v2}, Lhft;->b(Lhgw;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 430
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 426
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 412
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 414
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    throw v0

    .line 435
    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 436
    add-int/lit8 v0, v0, -0x1

    :goto_5
    const/4 v2, 0x5

    if-lt v0, v2, :cond_5

    .line 437
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 436
    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    .line 442
    :cond_5
    :try_start_1
    invoke-static {v4}, Lhhg;->a(Ljava/util/ArrayList;)[B

    move-result-object v0

    .line 443
    new-instance v2, Landroid/content/ContentValues;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 444
    const-string v3, "audience_history"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 445
    const-string v0, "account_status"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 451
    :goto_6
    return-void

    .line 449
    :catch_0
    move-exception v0

    const/4 v0, 0x6

    const-string v1, "AclDataUtils"

    const-string v2, "Error saving audience history"

    invoke-static {v0, v1, v2}, Llse;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :cond_6
    move-object v3, v2

    goto :goto_3

    :cond_7
    move-object v2, v10

    goto :goto_2
.end method

.method private static a(Lhgw;)[Locn;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v0, 0x0

    .line 325
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 327
    if-eqz p0, :cond_c

    .line 328
    invoke-virtual {p0}, Lhgw;->b()[Lhxc;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 329
    new-instance v6, Locn;

    invoke-direct {v6}, Locn;-><init>()V

    .line 331
    invoke-virtual {v5}, Lhxc;->c()I

    move-result v7

    .line 332
    const/4 v8, 0x5

    if-ne v7, v8, :cond_0

    .line 333
    const/4 v5, 0x3

    iput v5, v6, Locn;->d:I

    .line 348
    :goto_1
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 334
    :cond_0
    const/4 v8, 0x7

    if-ne v7, v8, :cond_1

    .line 335
    const/4 v5, 0x4

    iput v5, v6, Locn;->d:I

    goto :goto_1

    .line 336
    :cond_1
    const/16 v8, 0x8

    if-ne v7, v8, :cond_2

    .line 337
    iput v9, v6, Locn;->d:I

    goto :goto_1

    .line 338
    :cond_2
    const/16 v8, 0x9

    if-ne v7, v8, :cond_3

    .line 339
    const/4 v5, 0x1

    iput v5, v6, Locn;->d:I

    goto :goto_1

    .line 341
    :cond_3
    invoke-virtual {v5}, Lhxc;->a()Ljava/lang/String;

    move-result-object v5

    .line 342
    const-string v7, "f."

    invoke-virtual {v5, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 343
    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v6, Locn;->c:Ljava/lang/String;

    goto :goto_1

    .line 345
    :cond_4
    iput-object v5, v6, Locn;->c:Ljava/lang/String;

    goto :goto_1

    .line 351
    :cond_5
    invoke-virtual {p0}, Lhgw;->a()[Ljqs;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_a

    aget-object v5, v3, v1

    .line 352
    new-instance v6, Locn;

    invoke-direct {v6}, Locn;-><init>()V

    .line 353
    new-instance v7, Lohp;

    invoke-direct {v7}, Lohp;-><init>()V

    .line 355
    invoke-virtual {v5}, Ljqs;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 356
    invoke-virtual {v5}, Ljqs;->a()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v7, Lohp;->d:Ljava/lang/String;

    .line 367
    :goto_3
    iput-object v7, v6, Locn;->b:Lohp;

    .line 368
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    :cond_6
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 357
    :cond_7
    invoke-virtual {v5}, Ljqs;->c()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_8

    .line 358
    invoke-virtual {v5}, Ljqs;->c()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v7, Lohp;->b:Ljava/lang/String;

    goto :goto_3

    .line 361
    :cond_8
    const-string v6, "AclDataUtils"

    const/4 v7, 0x6

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 362
    const-string v6, "Invalid user: "

    invoke-virtual {v5}, Ljqs;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {v6, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_4

    :cond_9
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 371
    :cond_a
    invoke-virtual {p0}, Lhgw;->c()[Lkxr;

    move-result-object v1

    array-length v3, v1

    :goto_5
    if-ge v0, v3, :cond_c

    aget-object v4, v1, v0

    .line 372
    new-instance v5, Locn;

    invoke-direct {v5}, Locn;-><init>()V

    .line 373
    new-instance v6, Loco;

    invoke-direct {v6}, Loco;-><init>()V

    .line 374
    invoke-virtual {v4}, Lkxr;->a()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Loco;->a:Ljava/lang/String;

    .line 375
    iput-object v6, v5, Locn;->e:Loco;

    .line 376
    invoke-virtual {v4}, Lkxr;->a()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_b

    .line 377
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing Square stream id."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 379
    :cond_b
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 371
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 384
    :cond_c
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Locn;

    .line 383
    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Locn;

    return-object v0
.end method

.method private static b(Lhgw;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 484
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lhgw;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 521
    :cond_0
    :goto_0
    return v2

    .line 489
    :cond_1
    invoke-virtual {p0}, Lhgw;->h()I

    move-result v3

    .line 490
    invoke-virtual {p0}, Lhgw;->g()I

    move-result v0

    .line 491
    invoke-virtual {p0}, Lhgw;->i()I

    move-result v4

    .line 492
    invoke-virtual {p0}, Lhgw;->j()I

    move-result v5

    .line 495
    if-lez v3, :cond_4

    .line 496
    if-nez v0, :cond_3

    if-nez v4, :cond_3

    if-nez v5, :cond_3

    move v0, v1

    .line 505
    :goto_1
    if-eqz v0, :cond_0

    .line 509
    if-ne v3, v1, :cond_2

    .line 510
    invoke-virtual {p0, v2}, Lhgw;->b(I)Lhxc;

    move-result-object v0

    invoke-virtual {v0}, Lhxc;->c()I

    move-result v0

    .line 511
    const/4 v3, 0x5

    if-eq v0, v3, :cond_0

    const/16 v3, 0x8

    if-eq v0, v3, :cond_0

    const/16 v3, 0x9

    if-eq v0, v3, :cond_0

    :cond_2
    move v2, v1

    .line 521
    goto :goto_0

    :cond_3
    move v0, v2

    .line 496
    goto :goto_1

    .line 497
    :cond_4
    if-lez v0, :cond_6

    .line 498
    if-nez v4, :cond_5

    if-nez v5, :cond_5

    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    .line 499
    :cond_6
    if-lez v4, :cond_8

    .line 500
    if-nez v5, :cond_7

    move v0, v1

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_1

    .line 502
    :cond_8
    if-lez v5, :cond_9

    move v0, v1

    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_1
.end method
