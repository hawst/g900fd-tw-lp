.class public final Ldlf;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmes;",
        "Lmet;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:[Ljava/lang/String;

.field private final b:Z

.field private final p:I

.field private final q:J


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;ZIJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I[",
            "Ljava/lang/String;",
            "ZIJ)V"
        }
    .end annotation

    .prologue
    .line 38
    const-string v3, "markitemread"

    new-instance v4, Lmes;

    invoke-direct {v4}, Lmes;-><init>()V

    new-instance v5, Lmet;

    invoke-direct {v5}, Lmet;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 40
    iput-object p3, p0, Ldlf;->a:[Ljava/lang/String;

    .line 41
    iput-boolean p4, p0, Ldlf;->b:Z

    .line 43
    iput p5, p0, Ldlf;->p:I

    .line 44
    iput-wide p6, p0, Ldlf;->q:J

    .line 45
    return-void
.end method


# virtual methods
.method protected a(Lmes;)V
    .locals 6

    .prologue
    .line 61
    new-instance v0, Lnvk;

    invoke-direct {v0}, Lnvk;-><init>()V

    iput-object v0, p1, Lmes;->a:Lnvk;

    .line 62
    iget-object v0, p1, Lmes;->a:Lnvk;

    .line 63
    iget v1, p0, Ldlf;->p:I

    iput v1, v0, Lnvk;->c:I

    .line 64
    iget-wide v2, p0, Ldlf;->q:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 65
    new-instance v1, Lodc;

    invoke-direct {v1}, Lodc;-><init>()V

    iput-object v1, v0, Lnvk;->d:Lodc;

    .line 66
    iget-object v1, v0, Lnvk;->d:Lodc;

    iget-wide v2, p0, Ldlf;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lodc;->a:Ljava/lang/Long;

    .line 68
    :cond_0
    iget-boolean v1, p0, Ldlf;->b:Z

    if-eqz v1, :cond_1

    .line 69
    const/4 v1, 0x4

    iput v1, v0, Lnvk;->b:I

    .line 73
    :goto_0
    iget-object v1, p0, Ldlf;->a:[Ljava/lang/String;

    iput-object v1, v0, Lnvk;->a:[Ljava/lang/String;

    .line 74
    return-void

    .line 71
    :cond_1
    const/4 v1, 0x3

    iput v1, v0, Lnvk;->b:I

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmes;

    invoke-virtual {p0, p1}, Ldlf;->a(Lmes;)V

    return-void
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 80
    iget-boolean v0, p0, Ldlf;->b:Z

    if-nez v0, :cond_0

    iget v0, p0, Ldlf;->p:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 82
    iget-object v0, p0, Ldlf;->f:Landroid/content/Context;

    iget v1, p0, Ldlf;->c:I

    iget-object v2, p0, Ldlf;->a:[Ljava/lang/String;

    invoke-static {v0, v1, v2}, Llap;->a(Landroid/content/Context;I[Ljava/lang/String;)V

    .line 84
    :cond_0
    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0}, Ldlf;->b()V

    return-void
.end method
