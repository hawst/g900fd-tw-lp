.class public final Lctt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljma;
.implements Llnx;
.implements Llre;
.implements Llrg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljma",
        "<",
        "Lctz;",
        ">;",
        "Llnx;",
        "Llre;",
        "Llrg;"
    }
.end annotation


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Lcnc;

.field private c:Lctq;

.field private d:Landroid/os/Bundle;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lctt;->a:Landroid/app/Activity;

    .line 37
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 38
    return-void
.end method

.method private b(Lctz;)V
    .locals 5

    .prologue
    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 68
    invoke-virtual {p1}, Lctz;->a()Ljcn;

    move-result-object v0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_8

    .line 69
    iget-object v0, p0, Lctt;->b:Lcnc;

    invoke-virtual {v0}, Lcnc;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    iget-object v0, p0, Lctt;->b:Lcnc;

    invoke-virtual {v0}, Lcnc;->d()V

    .line 77
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 68
    goto :goto_0

    .line 72
    :cond_2
    iget-object v0, p0, Lctt;->c:Lctq;

    invoke-virtual {v0}, Lctq;->c()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lctt;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lctt;->d:Landroid/os/Bundle;

    const-string v3, "button_title_res_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-object v3, p0, Lctt;->d:Landroid/os/Bundle;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lctt;->d:Landroid/os/Bundle;

    const-string v4, "min_selection_count"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    :cond_3
    iget-object v3, p0, Lctt;->d:Landroid/os/Bundle;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lctt;->d:Landroid/os/Bundle;

    const-string v4, "max_selection_count"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    :cond_4
    invoke-static {v0, v1, v2}, Lcug;->a(Ljava/lang/String;II)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lctt;->b:Lcnc;

    const-string v2, "com.google.android.apps.photos.actionbar.modes.multi_select_mode"

    invoke-virtual {v1, v2, v0}, Lcnc;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    iget-boolean v0, p0, Lctt;->e:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lctt;->b:Lcnc;

    const-string v1, "com.google.android.apps.photos.actionbar.modes.multi_select_share_only"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcnc;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lctt;->b:Lcnc;

    const-string v1, "com.google.android.apps.photos.actionbar.modes.contextual_multi_select_mode"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcnc;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    .line 74
    :cond_8
    iget-object v0, p0, Lctt;->b:Lcnc;

    invoke-virtual {v0}, Lcnc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lctt;->b:Lcnc;

    invoke-virtual {v0}, Lcnc;->e()V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lctt;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lctt;->d:Landroid/os/Bundle;

    .line 51
    iget-object v0, p0, Lctt;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lctt;->d:Landroid/os/Bundle;

    const-string v1, "share_only"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lctt;->e:Z

    .line 55
    :cond_0
    iget-object v0, p0, Lctt;->a:Landroid/app/Activity;

    const-class v1, Lctz;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    invoke-direct {p0, v0}, Lctt;->b(Lctz;)V

    .line 56
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 42
    const-class v0, Lcnc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnc;

    iput-object v0, p0, Lctt;->b:Lcnc;

    .line 43
    const-class v0, Lctq;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    iput-object v0, p0, Lctt;->c:Lctq;

    .line 44
    const-class v0, Lctz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    .line 45
    invoke-virtual {v0}, Lctz;->b()Ljlx;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Ljlx;->a(Ljma;Z)V

    .line 46
    return-void
.end method

.method public a(Lctz;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lctt;->b(Lctz;)V

    .line 61
    return-void
.end method

.method public synthetic a_(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lctz;

    invoke-virtual {p0, p1}, Lctt;->a(Lctz;)V

    return-void
.end method
