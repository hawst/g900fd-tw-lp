.class public Llhe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llhh;
.implements Llpw;
.implements Llrg;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Llhd;

.field private c:Llhc;

.field private d:Llhc;

.field private e:Llhg;

.field private f:Z

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llhh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Llhd;

    invoke-direct {v0}, Llhd;-><init>()V

    iput-object v0, p0, Llhe;->b:Llhd;

    .line 29
    new-instance v0, Llhf;

    invoke-direct {v0, p0}, Llhf;-><init>(Llhe;)V

    iput-object v0, p0, Llhe;->c:Llhc;

    .line 41
    iget-object v0, p0, Llhe;->b:Llhd;

    iput-object v0, p0, Llhe;->d:Llhc;

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Llhe;->f:Z

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llhe;->g:Ljava/util/List;

    .line 49
    iput-object p1, p0, Llhe;->a:Landroid/app/Activity;

    .line 50
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 51
    return-void
.end method

.method static synthetic a(Llhe;)Llhc;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Llhe;->d:Llhc;

    return-object v0
.end method


# virtual methods
.method public a()Llhc;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Llhe;->c:Llhc;

    return-object v0
.end method

.method public a(Llnh;)Llhe;
    .locals 1

    .prologue
    .line 54
    const-class v0, Llhe;

    invoke-virtual {p1, v0, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 55
    return-object p0
.end method

.method public a(Llhc;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Llhe;->b:Llhd;

    invoke-virtual {v0, p1}, Llhd;->a(Llhc;)V

    .line 73
    return-void
.end method

.method public a(Llhh;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Llhe;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Llhe;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llhh;

    .line 98
    invoke-interface {v0, p1}, Llhh;->a(Z)V

    goto :goto_0

    .line 100
    :cond_0
    return-void
.end method

.method public b_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 81
    iget-boolean v0, p0, Llhe;->f:Z

    if-nez v0, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Llhe;->a:Landroid/app/Activity;

    instance-of v0, v0, Los;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Llhe;->a:Landroid/app/Activity;

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    .line 89
    new-instance v1, Llhg;

    iget-object v2, p0, Llhe;->b:Llhd;

    invoke-direct {v1, v0, v2}, Llhg;-><init>(Loo;Llhc;)V

    iput-object v1, p0, Llhe;->e:Llhg;

    .line 91
    iget-object v0, p0, Llhe;->e:Llhg;

    invoke-virtual {v0, p0}, Llhg;->a(Llhh;)V

    .line 92
    iget-object v0, p0, Llhe;->e:Llhg;

    iput-object v0, p0, Llhe;->d:Llhc;

    goto :goto_0
.end method
