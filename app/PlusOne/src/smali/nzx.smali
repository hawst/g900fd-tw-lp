.class public final Lnzx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnzx;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lnyl;

.field public g:Ljava/lang/String;

.field public h:Lnyz;

.field public i:Lnzm;

.field public j:[Lnzx;

.field public k:I

.field private l:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lnzx;

    sput-object v0, Lnzx;->a:[Lnzx;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnzx;->d:[Ljava/lang/String;

    .line 22
    iput-object v1, p0, Lnzx;->f:Lnyl;

    .line 27
    iput-object v1, p0, Lnzx;->h:Lnyz;

    .line 30
    iput-object v1, p0, Lnzx;->i:Lnzm;

    .line 35
    sget-object v0, Lnzx;->a:[Lnzx;

    iput-object v0, p0, Lnzx;->j:[Lnzx;

    .line 38
    const/high16 v0, -0x80000000

    iput v0, p0, Lnzx;->k:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 88
    .line 89
    iget-object v0, p0, Lnzx;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 90
    const/4 v0, 0x1

    iget-object v2, p0, Lnzx;->b:Ljava/lang/String;

    .line 91
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 93
    :goto_0
    iget-object v2, p0, Lnzx;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 94
    const/4 v2, 0x2

    iget-object v3, p0, Lnzx;->c:Ljava/lang/String;

    .line 95
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 97
    :cond_0
    iget-object v2, p0, Lnzx;->d:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnzx;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 99
    iget-object v4, p0, Lnzx;->d:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 101
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 99
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 103
    :cond_1
    add-int/2addr v0, v3

    .line 104
    iget-object v2, p0, Lnzx;->d:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 106
    :cond_2
    iget-object v2, p0, Lnzx;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 107
    const/4 v2, 0x4

    iget-object v3, p0, Lnzx;->e:Ljava/lang/String;

    .line 108
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 110
    :cond_3
    iget-object v2, p0, Lnzx;->f:Lnyl;

    if-eqz v2, :cond_4

    .line 111
    const/4 v2, 0x5

    iget-object v3, p0, Lnzx;->f:Lnyl;

    .line 112
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 114
    :cond_4
    iget-object v2, p0, Lnzx;->g:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 115
    const/4 v2, 0x6

    iget-object v3, p0, Lnzx;->g:Ljava/lang/String;

    .line 116
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 118
    :cond_5
    iget-object v2, p0, Lnzx;->l:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 119
    const/4 v2, 0x7

    iget-object v3, p0, Lnzx;->l:Ljava/lang/Boolean;

    .line 120
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 122
    :cond_6
    iget-object v2, p0, Lnzx;->j:[Lnzx;

    if-eqz v2, :cond_8

    .line 123
    iget-object v2, p0, Lnzx;->j:[Lnzx;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 124
    if-eqz v4, :cond_7

    .line 125
    const/16 v5, 0x8

    .line 126
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 123
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 130
    :cond_8
    iget v1, p0, Lnzx;->k:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_9

    .line 131
    const/16 v1, 0x9

    iget v2, p0, Lnzx;->k:I

    .line 132
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    :cond_9
    iget-object v1, p0, Lnzx;->h:Lnyz;

    if-eqz v1, :cond_a

    .line 135
    const/16 v1, 0xa

    iget-object v2, p0, Lnzx;->h:Lnyz;

    .line 136
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 138
    :cond_a
    iget-object v1, p0, Lnzx;->i:Lnzm;

    if-eqz v1, :cond_b

    .line 139
    const/16 v1, 0xb

    iget-object v2, p0, Lnzx;->i:Lnzm;

    .line 140
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    :cond_b
    iget-object v1, p0, Lnzx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    iput v0, p0, Lnzx;->ai:I

    .line 144
    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnzx;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 152
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 153
    sparse-switch v0, :sswitch_data_0

    .line 157
    iget-object v2, p0, Lnzx;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 158
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnzx;->ah:Ljava/util/List;

    .line 161
    :cond_1
    iget-object v2, p0, Lnzx;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    :sswitch_0
    return-object p0

    .line 168
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzx;->b:Ljava/lang/String;

    goto :goto_0

    .line 172
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzx;->c:Ljava/lang/String;

    goto :goto_0

    .line 176
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 177
    iget-object v0, p0, Lnzx;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 178
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 179
    iget-object v3, p0, Lnzx;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 180
    iput-object v2, p0, Lnzx;->d:[Ljava/lang/String;

    .line 181
    :goto_1
    iget-object v2, p0, Lnzx;->d:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 182
    iget-object v2, p0, Lnzx;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 183
    invoke-virtual {p1}, Loxn;->a()I

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 186
    :cond_2
    iget-object v2, p0, Lnzx;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto :goto_0

    .line 190
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzx;->e:Ljava/lang/String;

    goto :goto_0

    .line 194
    :sswitch_5
    iget-object v0, p0, Lnzx;->f:Lnyl;

    if-nez v0, :cond_3

    .line 195
    new-instance v0, Lnyl;

    invoke-direct {v0}, Lnyl;-><init>()V

    iput-object v0, p0, Lnzx;->f:Lnyl;

    .line 197
    :cond_3
    iget-object v0, p0, Lnzx;->f:Lnyl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 201
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnzx;->g:Ljava/lang/String;

    goto :goto_0

    .line 205
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnzx;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 209
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 210
    iget-object v0, p0, Lnzx;->j:[Lnzx;

    if-nez v0, :cond_5

    move v0, v1

    .line 211
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lnzx;

    .line 212
    iget-object v3, p0, Lnzx;->j:[Lnzx;

    if-eqz v3, :cond_4

    .line 213
    iget-object v3, p0, Lnzx;->j:[Lnzx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 215
    :cond_4
    iput-object v2, p0, Lnzx;->j:[Lnzx;

    .line 216
    :goto_3
    iget-object v2, p0, Lnzx;->j:[Lnzx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 217
    iget-object v2, p0, Lnzx;->j:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 218
    iget-object v2, p0, Lnzx;->j:[Lnzx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 219
    invoke-virtual {p1}, Loxn;->a()I

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 210
    :cond_5
    iget-object v0, p0, Lnzx;->j:[Lnzx;

    array-length v0, v0

    goto :goto_2

    .line 222
    :cond_6
    iget-object v2, p0, Lnzx;->j:[Lnzx;

    new-instance v3, Lnzx;

    invoke-direct {v3}, Lnzx;-><init>()V

    aput-object v3, v2, v0

    .line 223
    iget-object v2, p0, Lnzx;->j:[Lnzx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 227
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 228
    if-eqz v0, :cond_7

    const/4 v2, 0x1

    if-eq v0, v2, :cond_7

    const/4 v2, 0x2

    if-eq v0, v2, :cond_7

    const/4 v2, 0x3

    if-eq v0, v2, :cond_7

    const/4 v2, 0x4

    if-eq v0, v2, :cond_7

    const/4 v2, 0x5

    if-eq v0, v2, :cond_7

    const/4 v2, 0x6

    if-eq v0, v2, :cond_7

    const/16 v2, 0x64

    if-eq v0, v2, :cond_7

    const/16 v2, 0x65

    if-eq v0, v2, :cond_7

    const/16 v2, 0x6e

    if-ne v0, v2, :cond_8

    .line 238
    :cond_7
    iput v0, p0, Lnzx;->k:I

    goto/16 :goto_0

    .line 240
    :cond_8
    iput v1, p0, Lnzx;->k:I

    goto/16 :goto_0

    .line 245
    :sswitch_a
    iget-object v0, p0, Lnzx;->h:Lnyz;

    if-nez v0, :cond_9

    .line 246
    new-instance v0, Lnyz;

    invoke-direct {v0}, Lnyz;-><init>()V

    iput-object v0, p0, Lnzx;->h:Lnyz;

    .line 248
    :cond_9
    iget-object v0, p0, Lnzx;->h:Lnyz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 252
    :sswitch_b
    iget-object v0, p0, Lnzx;->i:Lnzm;

    if-nez v0, :cond_a

    .line 253
    new-instance v0, Lnzm;

    invoke-direct {v0}, Lnzm;-><init>()V

    iput-object v0, p0, Lnzx;->i:Lnzm;

    .line 255
    :cond_a
    iget-object v0, p0, Lnzx;->i:Lnzm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 153
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 43
    iget-object v1, p0, Lnzx;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 44
    const/4 v1, 0x1

    iget-object v2, p0, Lnzx;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    :cond_0
    iget-object v1, p0, Lnzx;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 47
    const/4 v1, 0x2

    iget-object v2, p0, Lnzx;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 49
    :cond_1
    iget-object v1, p0, Lnzx;->d:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 50
    iget-object v2, p0, Lnzx;->d:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 51
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 50
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54
    :cond_2
    iget-object v1, p0, Lnzx;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 55
    const/4 v1, 0x4

    iget-object v2, p0, Lnzx;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 57
    :cond_3
    iget-object v1, p0, Lnzx;->f:Lnyl;

    if-eqz v1, :cond_4

    .line 58
    const/4 v1, 0x5

    iget-object v2, p0, Lnzx;->f:Lnyl;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 60
    :cond_4
    iget-object v1, p0, Lnzx;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 61
    const/4 v1, 0x6

    iget-object v2, p0, Lnzx;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 63
    :cond_5
    iget-object v1, p0, Lnzx;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 64
    const/4 v1, 0x7

    iget-object v2, p0, Lnzx;->l:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 66
    :cond_6
    iget-object v1, p0, Lnzx;->j:[Lnzx;

    if-eqz v1, :cond_8

    .line 67
    iget-object v1, p0, Lnzx;->j:[Lnzx;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    .line 68
    if-eqz v3, :cond_7

    .line 69
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 67
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 73
    :cond_8
    iget v0, p0, Lnzx;->k:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_9

    .line 74
    const/16 v0, 0x9

    iget v1, p0, Lnzx;->k:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 76
    :cond_9
    iget-object v0, p0, Lnzx;->h:Lnyz;

    if-eqz v0, :cond_a

    .line 77
    const/16 v0, 0xa

    iget-object v1, p0, Lnzx;->h:Lnyz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 79
    :cond_a
    iget-object v0, p0, Lnzx;->i:Lnzm;

    if-eqz v0, :cond_b

    .line 80
    const/16 v0, 0xb

    iget-object v1, p0, Lnzx;->i:Lnzm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 82
    :cond_b
    iget-object v0, p0, Lnzx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 84
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnzx;->a(Loxn;)Lnzx;

    move-result-object v0

    return-object v0
.end method
