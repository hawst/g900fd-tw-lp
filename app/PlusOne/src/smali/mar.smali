.class public final Lmar;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmqk;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18304
    invoke-direct {p0}, Loxq;-><init>()V

    .line 18307
    iput-object v0, p0, Lmar;->apiHeader:Llyr;

    .line 18310
    iput-object v0, p0, Lmar;->a:Lmqk;

    .line 18304
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 18327
    const/4 v0, 0x0

    .line 18328
    iget-object v1, p0, Lmar;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 18329
    const/4 v0, 0x1

    iget-object v1, p0, Lmar;->apiHeader:Llyr;

    .line 18330
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 18332
    :cond_0
    iget-object v1, p0, Lmar;->a:Lmqk;

    if-eqz v1, :cond_1

    .line 18333
    const/4 v1, 0x2

    iget-object v2, p0, Lmar;->a:Lmqk;

    .line 18334
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18336
    :cond_1
    iget-object v1, p0, Lmar;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18337
    iput v0, p0, Lmar;->ai:I

    .line 18338
    return v0
.end method

.method public a(Loxn;)Lmar;
    .locals 2

    .prologue
    .line 18346
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 18347
    sparse-switch v0, :sswitch_data_0

    .line 18351
    iget-object v1, p0, Lmar;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 18352
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmar;->ah:Ljava/util/List;

    .line 18355
    :cond_1
    iget-object v1, p0, Lmar;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18357
    :sswitch_0
    return-object p0

    .line 18362
    :sswitch_1
    iget-object v0, p0, Lmar;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 18363
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmar;->apiHeader:Llyr;

    .line 18365
    :cond_2
    iget-object v0, p0, Lmar;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18369
    :sswitch_2
    iget-object v0, p0, Lmar;->a:Lmqk;

    if-nez v0, :cond_3

    .line 18370
    new-instance v0, Lmqk;

    invoke-direct {v0}, Lmqk;-><init>()V

    iput-object v0, p0, Lmar;->a:Lmqk;

    .line 18372
    :cond_3
    iget-object v0, p0, Lmar;->a:Lmqk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18347
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 18315
    iget-object v0, p0, Lmar;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 18316
    const/4 v0, 0x1

    iget-object v1, p0, Lmar;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18318
    :cond_0
    iget-object v0, p0, Lmar;->a:Lmqk;

    if-eqz v0, :cond_1

    .line 18319
    const/4 v0, 0x2

    iget-object v1, p0, Lmar;->a:Lmqk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18321
    :cond_1
    iget-object v0, p0, Lmar;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 18323
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 18300
    invoke-virtual {p0, p1}, Lmar;->a(Loxn;)Lmar;

    move-result-object v0

    return-object v0
.end method
