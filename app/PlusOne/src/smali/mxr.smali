.class public final Lmxr;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 630
    invoke-direct {p0}, Loxq;-><init>()V

    .line 640
    const/high16 v0, -0x80000000

    iput v0, p0, Lmxr;->a:I

    .line 630
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 664
    const/4 v0, 0x0

    .line 665
    iget v1, p0, Lmxr;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 666
    const/4 v0, 0x1

    iget v1, p0, Lmxr;->a:I

    .line 667
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 669
    :cond_0
    iget-object v1, p0, Lmxr;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 670
    const/4 v1, 0x2

    iget-object v2, p0, Lmxr;->b:Ljava/lang/Boolean;

    .line 671
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 673
    :cond_1
    iget-object v1, p0, Lmxr;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 674
    const/4 v1, 0x3

    iget-object v2, p0, Lmxr;->c:Ljava/lang/Long;

    .line 675
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 677
    :cond_2
    iget-object v1, p0, Lmxr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 678
    iput v0, p0, Lmxr;->ai:I

    .line 679
    return v0
.end method

.method public a(Loxn;)Lmxr;
    .locals 2

    .prologue
    .line 687
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 688
    sparse-switch v0, :sswitch_data_0

    .line 692
    iget-object v1, p0, Lmxr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 693
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmxr;->ah:Ljava/util/List;

    .line 696
    :cond_1
    iget-object v1, p0, Lmxr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 698
    :sswitch_0
    return-object p0

    .line 703
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 704
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 708
    :cond_2
    iput v0, p0, Lmxr;->a:I

    goto :goto_0

    .line 710
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmxr;->a:I

    goto :goto_0

    .line 715
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmxr;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 719
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmxr;->c:Ljava/lang/Long;

    goto :goto_0

    .line 688
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 649
    iget v0, p0, Lmxr;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 650
    const/4 v0, 0x1

    iget v1, p0, Lmxr;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 652
    :cond_0
    iget-object v0, p0, Lmxr;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 653
    const/4 v0, 0x2

    iget-object v1, p0, Lmxr;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 655
    :cond_1
    iget-object v0, p0, Lmxr;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 656
    const/4 v0, 0x3

    iget-object v1, p0, Lmxr;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 658
    :cond_2
    iget-object v0, p0, Lmxr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 660
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 626
    invoke-virtual {p0, p1}, Lmxr;->a(Loxn;)Lmxr;

    move-result-object v0

    return-object v0
.end method
