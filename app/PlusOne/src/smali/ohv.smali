.class public final Lohv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lohv;


# instance fields
.field public b:Lohp;

.field public c:Lohq;

.field public d:[Loij;

.field private e:[Lohp;

.field private f:Lohy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2608
    const/4 v0, 0x0

    new-array v0, v0, [Lohv;

    sput-object v0, Lohv;->a:[Lohv;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2609
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2612
    iput-object v1, p0, Lohv;->b:Lohp;

    .line 2615
    sget-object v0, Lohp;->a:[Lohp;

    iput-object v0, p0, Lohv;->e:[Lohp;

    .line 2618
    iput-object v1, p0, Lohv;->c:Lohq;

    .line 2621
    sget-object v0, Loij;->a:[Loij;

    iput-object v0, p0, Lohv;->d:[Loij;

    .line 2624
    iput-object v1, p0, Lohv;->f:Lohy;

    .line 2609
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2658
    .line 2659
    iget-object v0, p0, Lohv;->b:Lohp;

    if-eqz v0, :cond_6

    .line 2660
    const/4 v0, 0x1

    iget-object v2, p0, Lohv;->b:Lohp;

    .line 2661
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2663
    :goto_0
    iget-object v2, p0, Lohv;->e:[Lohp;

    if-eqz v2, :cond_1

    .line 2664
    iget-object v3, p0, Lohv;->e:[Lohp;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 2665
    if-eqz v5, :cond_0

    .line 2666
    const/4 v6, 0x2

    .line 2667
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 2664
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2671
    :cond_1
    iget-object v2, p0, Lohv;->c:Lohq;

    if-eqz v2, :cond_2

    .line 2672
    const/4 v2, 0x3

    iget-object v3, p0, Lohv;->c:Lohq;

    .line 2673
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2675
    :cond_2
    iget-object v2, p0, Lohv;->d:[Loij;

    if-eqz v2, :cond_4

    .line 2676
    iget-object v2, p0, Lohv;->d:[Loij;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 2677
    if-eqz v4, :cond_3

    .line 2678
    const/4 v5, 0x4

    .line 2679
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2676
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2683
    :cond_4
    iget-object v1, p0, Lohv;->f:Lohy;

    if-eqz v1, :cond_5

    .line 2684
    const/4 v1, 0x5

    iget-object v2, p0, Lohv;->f:Lohy;

    .line 2685
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2687
    :cond_5
    iget-object v1, p0, Lohv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2688
    iput v0, p0, Lohv;->ai:I

    .line 2689
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lohv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2697
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2698
    sparse-switch v0, :sswitch_data_0

    .line 2702
    iget-object v2, p0, Lohv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2703
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lohv;->ah:Ljava/util/List;

    .line 2706
    :cond_1
    iget-object v2, p0, Lohv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2708
    :sswitch_0
    return-object p0

    .line 2713
    :sswitch_1
    iget-object v0, p0, Lohv;->b:Lohp;

    if-nez v0, :cond_2

    .line 2714
    new-instance v0, Lohp;

    invoke-direct {v0}, Lohp;-><init>()V

    iput-object v0, p0, Lohv;->b:Lohp;

    .line 2716
    :cond_2
    iget-object v0, p0, Lohv;->b:Lohp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2720
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2721
    iget-object v0, p0, Lohv;->e:[Lohp;

    if-nez v0, :cond_4

    move v0, v1

    .line 2722
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohp;

    .line 2723
    iget-object v3, p0, Lohv;->e:[Lohp;

    if-eqz v3, :cond_3

    .line 2724
    iget-object v3, p0, Lohv;->e:[Lohp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2726
    :cond_3
    iput-object v2, p0, Lohv;->e:[Lohp;

    .line 2727
    :goto_2
    iget-object v2, p0, Lohv;->e:[Lohp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2728
    iget-object v2, p0, Lohv;->e:[Lohp;

    new-instance v3, Lohp;

    invoke-direct {v3}, Lohp;-><init>()V

    aput-object v3, v2, v0

    .line 2729
    iget-object v2, p0, Lohv;->e:[Lohp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2730
    invoke-virtual {p1}, Loxn;->a()I

    .line 2727
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2721
    :cond_4
    iget-object v0, p0, Lohv;->e:[Lohp;

    array-length v0, v0

    goto :goto_1

    .line 2733
    :cond_5
    iget-object v2, p0, Lohv;->e:[Lohp;

    new-instance v3, Lohp;

    invoke-direct {v3}, Lohp;-><init>()V

    aput-object v3, v2, v0

    .line 2734
    iget-object v2, p0, Lohv;->e:[Lohp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2738
    :sswitch_3
    iget-object v0, p0, Lohv;->c:Lohq;

    if-nez v0, :cond_6

    .line 2739
    new-instance v0, Lohq;

    invoke-direct {v0}, Lohq;-><init>()V

    iput-object v0, p0, Lohv;->c:Lohq;

    .line 2741
    :cond_6
    iget-object v0, p0, Lohv;->c:Lohq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2745
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2746
    iget-object v0, p0, Lohv;->d:[Loij;

    if-nez v0, :cond_8

    move v0, v1

    .line 2747
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loij;

    .line 2748
    iget-object v3, p0, Lohv;->d:[Loij;

    if-eqz v3, :cond_7

    .line 2749
    iget-object v3, p0, Lohv;->d:[Loij;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2751
    :cond_7
    iput-object v2, p0, Lohv;->d:[Loij;

    .line 2752
    :goto_4
    iget-object v2, p0, Lohv;->d:[Loij;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 2753
    iget-object v2, p0, Lohv;->d:[Loij;

    new-instance v3, Loij;

    invoke-direct {v3}, Loij;-><init>()V

    aput-object v3, v2, v0

    .line 2754
    iget-object v2, p0, Lohv;->d:[Loij;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2755
    invoke-virtual {p1}, Loxn;->a()I

    .line 2752
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2746
    :cond_8
    iget-object v0, p0, Lohv;->d:[Loij;

    array-length v0, v0

    goto :goto_3

    .line 2758
    :cond_9
    iget-object v2, p0, Lohv;->d:[Loij;

    new-instance v3, Loij;

    invoke-direct {v3}, Loij;-><init>()V

    aput-object v3, v2, v0

    .line 2759
    iget-object v2, p0, Lohv;->d:[Loij;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2763
    :sswitch_5
    iget-object v0, p0, Lohv;->f:Lohy;

    if-nez v0, :cond_a

    .line 2764
    new-instance v0, Lohy;

    invoke-direct {v0}, Lohy;-><init>()V

    iput-object v0, p0, Lohv;->f:Lohy;

    .line 2766
    :cond_a
    iget-object v0, p0, Lohv;->f:Lohy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2698
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2629
    iget-object v1, p0, Lohv;->b:Lohp;

    if-eqz v1, :cond_0

    .line 2630
    const/4 v1, 0x1

    iget-object v2, p0, Lohv;->b:Lohp;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2632
    :cond_0
    iget-object v1, p0, Lohv;->e:[Lohp;

    if-eqz v1, :cond_2

    .line 2633
    iget-object v2, p0, Lohv;->e:[Lohp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 2634
    if-eqz v4, :cond_1

    .line 2635
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2633
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2639
    :cond_2
    iget-object v1, p0, Lohv;->c:Lohq;

    if-eqz v1, :cond_3

    .line 2640
    const/4 v1, 0x3

    iget-object v2, p0, Lohv;->c:Lohq;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 2642
    :cond_3
    iget-object v1, p0, Lohv;->d:[Loij;

    if-eqz v1, :cond_5

    .line 2643
    iget-object v1, p0, Lohv;->d:[Loij;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 2644
    if-eqz v3, :cond_4

    .line 2645
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2643
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2649
    :cond_5
    iget-object v0, p0, Lohv;->f:Lohy;

    if-eqz v0, :cond_6

    .line 2650
    const/4 v0, 0x5

    iget-object v1, p0, Lohv;->f:Lohy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2652
    :cond_6
    iget-object v0, p0, Lohv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2654
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2605
    invoke-virtual {p0, p1}, Lohv;->a(Loxn;)Lohv;

    move-result-object v0

    return-object v0
.end method
