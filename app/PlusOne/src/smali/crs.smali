.class final Lcrs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Lcrp;

.field private synthetic b:Landroid/view/ViewTreeObserver;

.field private synthetic c:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcrp;Landroid/view/ViewTreeObserver;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcrs;->a:Lcrp;

    iput-object p2, p0, Lcrs;->b:Landroid/view/ViewTreeObserver;

    iput-object p3, p0, Lcrs;->c:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 260
    iget-object v0, p0, Lcrs;->b:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 262
    iget-object v0, p0, Lcrs;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v10

    move v7, v8

    move v3, v9

    .line 263
    :goto_0
    iget-object v0, p0, Lcrs;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge v7, v0, :cond_3

    .line 264
    iget-object v0, p0, Lcrs;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 265
    add-int v0, v10, v7

    .line 266
    iget-object v4, p0, Lcrs;->a:Lcrp;

    invoke-static {v4}, Lcrp;->b(Lcrp;)Lcry;

    move-result-object v4

    invoke-interface {v4, v0}, Lcry;->getItemId(I)J

    move-result-wide v4

    .line 267
    iget-object v0, p0, Lcrs;->a:Lcrp;

    invoke-static {v0}, Lcrp;->c(Lcrp;)Ljava/util/Map;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 268
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    .line 269
    if-nez v0, :cond_0

    .line 273
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v5, p0, Lcrs;->c:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v5

    add-int/2addr v0, v5

    .line 274
    if-lez v7, :cond_1

    :goto_1
    add-int/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 276
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v4, v0, v4

    .line 277
    if-eqz v4, :cond_4

    .line 278
    if-eqz v3, :cond_2

    new-instance v6, Lcrt;

    invoke-direct {v6, p0}, Lcrt;-><init>(Lcrs;)V

    .line 288
    :goto_2
    iget-object v0, p0, Lcrs;->a:Lcrp;

    int-to-float v4, v4

    move v3, v2

    move v5, v2

    invoke-static/range {v0 .. v6}, Lcrp;->a(Lcrp;Landroid/view/View;FFFFLjava/lang/Runnable;)V

    move v1, v8

    .line 263
    :goto_3
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v3, v1

    goto :goto_0

    .line 274
    :cond_1
    neg-int v0, v0

    goto :goto_1

    .line 278
    :cond_2
    const/4 v6, 0x0

    goto :goto_2

    .line 291
    :cond_3
    iget-object v0, p0, Lcrs;->a:Lcrp;

    invoke-static {v0}, Lcrp;->c(Lcrp;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 292
    return v9

    :cond_4
    move v1, v3

    goto :goto_3
.end method
