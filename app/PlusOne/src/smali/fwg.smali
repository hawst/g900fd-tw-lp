.class public final Lfwg;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private a:Lfwj;

.field private b:Landroid/text/StaticLayout;

.field private c:Landroid/text/StaticLayout;

.field private final d:Landroid/graphics/Rect;

.field private e:J

.field private f:Ljava/lang/Runnable;

.field private synthetic g:Lcom/google/android/apps/plus/views/BarGraphListView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/BarGraphListView;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 248
    iput-object p1, p0, Lfwg;->g:Lcom/google/android/apps/plus/views/BarGraphListView;

    .line 249
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 232
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    .line 241
    new-instance v0, Lfwh;

    invoke-direct {v0, p0}, Lfwh;-><init>(Lfwg;)V

    iput-object v0, p0, Lfwg;->f:Ljava/lang/Runnable;

    .line 250
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 437
    iput-object v0, p0, Lfwg;->a:Lfwj;

    .line 438
    iput-object v0, p0, Lfwg;->b:Landroid/text/StaticLayout;

    .line 439
    iput-object v0, p0, Lfwg;->c:Landroid/text/StaticLayout;

    .line 440
    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 441
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lfwg;->e:J

    .line 442
    iget-object v0, p0, Lfwg;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lfwg;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 443
    return-void
.end method

.method public a(Lfwj;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lfwg;->a:Lfwj;

    .line 267
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x10

    const-wide/16 v8, 0x0

    .line 369
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 371
    iget-object v0, p0, Lfwg;->a:Lfwj;

    if-nez v0, :cond_1

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 375
    :cond_1
    iget-object v0, p0, Lfwg;->g:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->d(Lcom/google/android/apps/plus/views/BarGraphListView;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lfwg;->a:Lfwj;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 376
    iget-object v0, p0, Lfwg;->g:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->d(Lcom/google/android/apps/plus/views/BarGraphListView;)Ljava/util/HashSet;

    move-result-object v0

    iget-object v1, p0, Lfwg;->a:Lfwj;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 377
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lfwg;->e:J

    .line 378
    iget-object v0, p0, Lfwg;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lfwg;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 379
    iget-object v0, p0, Lfwg;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, v12, v13}, Lfwg;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 382
    :cond_2
    invoke-virtual {p0}, Lfwg;->getPaddingLeft()I

    move-result v1

    .line 383
    invoke-virtual {p0}, Lfwg;->getPaddingTop()I

    move-result v0

    .line 385
    iget-object v2, p0, Lfwg;->b:Landroid/text/StaticLayout;

    if-eqz v2, :cond_3

    .line 386
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 387
    iget-object v2, p0, Lfwg;->b:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 388
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 389
    iget-object v2, p0, Lfwg;->b:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->c()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 392
    :cond_3
    iget-object v2, p0, Lfwg;->c:Landroid/text/StaticLayout;

    if-eqz v2, :cond_4

    .line 393
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 394
    iget-object v2, p0, Lfwg;->c:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 395
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 396
    iget-object v0, p0, Lfwg;->c:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->c()I

    .line 399
    :cond_4
    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 400
    iget-wide v0, p0, Lfwg;->e:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_6

    .line 401
    iget-wide v0, p0, Lfwg;->e:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 402
    cmp-long v2, v0, v8

    if-gez v2, :cond_7

    move-wide v6, v8

    .line 407
    :goto_1
    const/high16 v0, 0x3f800000    # 1.0f

    long-to-float v1, v6

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 409
    iget-object v1, p0, Lfwg;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lfwg;->d:Landroid/graphics/Rect;

    .line 410
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    add-int v10, v1, v0

    .line 413
    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    int-to-float v3, v10

    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    .line 414
    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->g()Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    .line 413
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 415
    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    int-to-float v3, v10

    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    .line 416
    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->h()Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    .line 415
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 418
    cmp-long v0, v6, v8

    if-nez v0, :cond_5

    .line 419
    iput-wide v8, p0, Lfwg;->e:J

    .line 431
    :goto_2
    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->e()I

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->f()I

    goto/16 :goto_0

    .line 424
    :cond_5
    iget-object v0, p0, Lfwg;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lfwg;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 425
    iget-object v0, p0, Lfwg;->f:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, v12, v13}, Lfwg;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2

    .line 428
    :cond_6
    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->g()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 429
    iget-object v0, p0, Lfwg;->d:Landroid/graphics/Rect;

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->h()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_2

    :cond_7
    move-wide v6, v0

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    .line 279
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v1, v2, :cond_0

    move v8, v0

    .line 280
    :goto_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    if-ne v1, v2, :cond_2

    .line 281
    :goto_1
    invoke-virtual {p0, v8, v0}, Lfwg;->setMeasuredDimension(II)V

    .line 282
    return-void

    .line 279
    :cond_0
    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_1

    const/16 v1, 0x1e0

    if-ge v0, v1, :cond_1

    move v8, v0

    goto :goto_0

    :cond_1
    const/16 v0, 0x1e0

    move v8, v0

    goto :goto_0

    .line 280
    :cond_2
    invoke-virtual {p0}, Lfwg;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lfwg;->getPaddingBottom()I

    move-result v1

    add-int v9, v0, v1

    iget-object v0, p0, Lfwg;->a:Lfwj;

    if-nez v0, :cond_3

    move v0, v9

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lfwg;->getPaddingLeft()I

    move-result v10

    invoke-virtual {p0}, Lfwg;->getPaddingRight()I

    move-result v0

    add-int/2addr v0, v10

    sub-int v3, v8, v0

    invoke-virtual {p0}, Lfwg;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lfwg;->a:Lfwj;

    invoke-virtual {v1}, Lfwj;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->b()Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lfwg;->b:Landroid/text/StaticLayout;

    iget-object v0, p0, Lfwg;->b:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->c()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v9, v0

    iget-object v0, p0, Lfwg;->g:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->a(Lcom/google/android/apps/plus/views/BarGraphListView;)J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    const v1, 0x7f0a0900

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lfwg;->a:Lfwj;

    invoke-virtual {v5}, Lfwj;->b()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lfwg;->g:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v5}, Lcom/google/android/apps/plus/views/BarGraphListView;->b(Lcom/google/android/apps/plus/views/BarGraphListView;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {v11, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/text/StaticLayout;

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->d()Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lfwg;->c:Landroid/text/StaticLayout;

    iget-object v0, p0, Lfwg;->c:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->c()I

    move-result v1

    add-int/2addr v0, v1

    add-int v1, v9, v0

    iget-object v0, p0, Lfwg;->g:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/BarGraphListView;->c(Lcom/google/android/apps/plus/views/BarGraphListView;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_5

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_3
    iget-object v2, p0, Lfwg;->d:Landroid/graphics/Rect;

    int-to-float v3, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    add-int/2addr v0, v10

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->e()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v2, v10, v1, v0, v3}, Landroid/graphics/Rect;->set(IIII)V

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->e()I

    move-result v0

    invoke-static {}, Lcom/google/android/apps/plus/views/BarGraphListView;->f()I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    goto/16 :goto_1

    :cond_4
    const/high16 v0, 0x42c80000    # 100.0f

    iget-object v1, p0, Lfwg;->a:Lfwj;

    invoke-virtual {v1}, Lfwj;->b()J

    move-result-wide v4

    long-to-float v1, v4

    mul-float/2addr v0, v1

    iget-object v1, p0, Lfwg;->g:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/BarGraphListView;->a(Lcom/google/android/apps/plus/views/BarGraphListView;)J

    move-result-wide v4

    long-to-float v1, v4

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto/16 :goto_2

    :cond_5
    iget-object v0, p0, Lfwg;->a:Lfwj;

    invoke-virtual {v0}, Lfwj;->b()J

    move-result-wide v4

    long-to-float v0, v4

    iget-object v2, p0, Lfwg;->g:Lcom/google/android/apps/plus/views/BarGraphListView;

    invoke-static {v2}, Lcom/google/android/apps/plus/views/BarGraphListView;->c(Lcom/google/android/apps/plus/views/BarGraphListView;)J

    move-result-wide v4

    long-to-float v2, v4

    div-float/2addr v0, v2

    goto :goto_3
.end method
