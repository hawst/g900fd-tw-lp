.class public final Lent;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lnjb;

.field public b:Lnjc;

.field public c:Lnjd;

.field public d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1289
    new-instance v0, Lenu;

    invoke-direct {v0}, Lenu;-><init>()V

    sput-object v0, Lent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 1211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1201
    iput-boolean v2, p0, Lent;->d:Z

    .line 1212
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1213
    if-eqz v0, :cond_0

    .line 1214
    new-array v0, v0, [B

    .line 1215
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 1217
    :try_start_0
    new-instance v3, Lnjb;

    invoke-direct {v3}, Lnjb;-><init>()V

    invoke-static {v3, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnjb;

    iput-object v0, p0, Lent;->a:Lnjb;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 1225
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1226
    if-eqz v0, :cond_1

    .line 1227
    new-array v0, v0, [B

    .line 1228
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 1230
    :try_start_1
    new-instance v3, Lnjc;

    invoke-direct {v3}, Lnjc;-><init>()V

    invoke-static {v3, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnjc;

    iput-object v0, p0, Lent;->b:Lnjc;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_1

    .line 1239
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1240
    if-eqz v0, :cond_2

    .line 1241
    new-array v0, v0, [B

    .line 1242
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 1244
    :try_start_2
    new-instance v3, Lnjd;

    invoke-direct {v3}, Lnjd;-><init>()V

    invoke-static {v3, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnjd;

    iput-object v0, p0, Lent;->c:Lnjd;
    :try_end_2
    .catch Loxt; {:try_start_2 .. :try_end_2} :catch_2

    .line 1252
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lent;->d:Z

    .line 1253
    return-void

    .line 1218
    :catch_0
    move-exception v0

    .line 1220
    const-string v3, "NameEditInfo"

    const-string v4, "Unable to create MessageNano object while reading parcel."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1223
    :cond_0
    iput-object v5, p0, Lent;->a:Lnjb;

    goto :goto_0

    .line 1232
    :catch_1
    move-exception v0

    .line 1234
    const-string v3, "NameEditInfo"

    const-string v4, "Unable to create MessageNano object while reading parcel."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1237
    :cond_1
    iput-object v5, p0, Lent;->b:Lnjc;

    goto :goto_1

    .line 1245
    :catch_2
    move-exception v0

    .line 1247
    const-string v3, "NameEditInfo"

    const-string v4, "Unable to create MessageNano object while reading parcel."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 1250
    :cond_2
    iput-object v5, p0, Lent;->c:Lnjd;

    goto :goto_2

    :cond_3
    move v0, v2

    .line 1252
    goto :goto_3
.end method

.method public constructor <init>(Lnjb;Lnjc;Lnjd;Z)V
    .locals 1

    .prologue
    .line 1204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lent;->d:Z

    .line 1205
    iput-object p1, p0, Lent;->a:Lnjb;

    .line 1206
    iput-object p2, p0, Lent;->b:Lnjc;

    .line 1207
    iput-object p3, p0, Lent;->c:Lnjd;

    .line 1208
    iput-boolean p4, p0, Lent;->d:Z

    .line 1209
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1283
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1257
    iget-object v1, p0, Lent;->a:Lnjb;

    if-eqz v1, :cond_1

    .line 1258
    iget-object v1, p0, Lent;->a:Lnjb;

    invoke-static {v1}, Loxu;->a(Loxu;)[B

    move-result-object v1

    .line 1259
    array-length v2, v1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1260
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 1264
    :goto_0
    iget-object v1, p0, Lent;->b:Lnjc;

    if-eqz v1, :cond_2

    .line 1265
    iget-object v1, p0, Lent;->b:Lnjc;

    invoke-static {v1}, Loxu;->a(Loxu;)[B

    move-result-object v1

    .line 1266
    array-length v2, v1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1267
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 1271
    :goto_1
    iget-object v1, p0, Lent;->c:Lnjd;

    if-eqz v1, :cond_3

    .line 1272
    iget-object v1, p0, Lent;->c:Lnjd;

    invoke-static {v1}, Loxu;->a(Loxu;)[B

    move-result-object v1

    .line 1273
    array-length v2, v1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1274
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 1278
    :goto_2
    iget-boolean v1, p0, Lent;->d:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1279
    return-void

    .line 1262
    :cond_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 1269
    :cond_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 1276
    :cond_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2
.end method
