.class final Lfei;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Ligy;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Ligv;

.field private c:Livx;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 95
    iget-object v0, p0, Lfei;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lfei;->c:Livx;

    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    const-class v3, Lixj;

    .line 97
    invoke-virtual {v2, v3}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v2

    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 98
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Liwg;->a(I)Liwg;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Liwg;->d()Liwg;

    move-result-object v0

    const-class v2, Liwl;

    new-instance v3, Liwm;

    invoke-direct {v3}, Liwm;-><init>()V

    iget-object v4, p0, Lfei;->a:Landroid/app/Activity;

    const v5, 0x7f0a0b58

    .line 101
    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v3

    .line 102
    invoke-virtual {v3}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 100
    invoke-virtual {v0, v2, v3}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v0

    .line 96
    invoke-virtual {v1, v0}, Livx;->a(Liwg;)V

    .line 105
    return-void
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 1

    .prologue
    .line 88
    iput-object p1, p0, Lfei;->a:Landroid/app/Activity;

    .line 89
    iput-object p3, p0, Lfei;->b:Ligv;

    .line 90
    invoke-virtual {p4, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lfei;->c:Livx;

    .line 91
    return-void
.end method

.method public a(ZIIII)V
    .locals 5

    .prologue
    .line 111
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    .line 112
    new-instance v0, Lkoe;

    const/16 v1, 0x53

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Lfei;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 113
    iget-object v0, p0, Lfei;->b:Ligv;

    iget-object v1, p0, Lfei;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    new-instance v1, Landroid/content/ComponentName;

    iget-object v3, p0, Lfei;->a:Landroid/app/Activity;

    const-class v4, Lcom/google/android/apps/plus/phone/sharebox/PlusShareboxActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v2, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "from_url_gateway"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-interface {v0, v2}, Ligv;->a(Landroid/content/Intent;)V

    .line 117
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lfei;->b:Ligv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ligv;->a(I)V

    goto :goto_0
.end method
