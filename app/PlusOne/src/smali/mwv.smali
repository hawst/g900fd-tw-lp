.class public final Lmwv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmwv;


# instance fields
.field private b:Ljava/lang/Long;

.field private c:[Lmww;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5718
    const/4 v0, 0x0

    new-array v0, v0, [Lmwv;

    sput-object v0, Lmwv;->a:[Lmwv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5719
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5724
    sget-object v0, Lmww;->a:[Lmww;

    iput-object v0, p0, Lmwv;->c:[Lmww;

    .line 5719
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5745
    .line 5746
    iget-object v0, p0, Lmwv;->b:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 5747
    const/4 v0, 0x1

    iget-object v2, p0, Lmwv;->b:Ljava/lang/Long;

    .line 5748
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5750
    :goto_0
    iget-object v2, p0, Lmwv;->c:[Lmww;

    if-eqz v2, :cond_1

    .line 5751
    iget-object v2, p0, Lmwv;->c:[Lmww;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 5752
    if-eqz v4, :cond_0

    .line 5753
    const/4 v5, 0x2

    .line 5754
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5751
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5758
    :cond_1
    iget-object v1, p0, Lmwv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5759
    iput v0, p0, Lmwv;->ai:I

    .line 5760
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmwv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5768
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5769
    sparse-switch v0, :sswitch_data_0

    .line 5773
    iget-object v2, p0, Lmwv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 5774
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmwv;->ah:Ljava/util/List;

    .line 5777
    :cond_1
    iget-object v2, p0, Lmwv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5779
    :sswitch_0
    return-object p0

    .line 5784
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwv;->b:Ljava/lang/Long;

    goto :goto_0

    .line 5788
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5789
    iget-object v0, p0, Lmwv;->c:[Lmww;

    if-nez v0, :cond_3

    move v0, v1

    .line 5790
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmww;

    .line 5791
    iget-object v3, p0, Lmwv;->c:[Lmww;

    if-eqz v3, :cond_2

    .line 5792
    iget-object v3, p0, Lmwv;->c:[Lmww;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5794
    :cond_2
    iput-object v2, p0, Lmwv;->c:[Lmww;

    .line 5795
    :goto_2
    iget-object v2, p0, Lmwv;->c:[Lmww;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 5796
    iget-object v2, p0, Lmwv;->c:[Lmww;

    new-instance v3, Lmww;

    invoke-direct {v3}, Lmww;-><init>()V

    aput-object v3, v2, v0

    .line 5797
    iget-object v2, p0, Lmwv;->c:[Lmww;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 5798
    invoke-virtual {p1}, Loxn;->a()I

    .line 5795
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5789
    :cond_3
    iget-object v0, p0, Lmwv;->c:[Lmww;

    array-length v0, v0

    goto :goto_1

    .line 5801
    :cond_4
    iget-object v2, p0, Lmwv;->c:[Lmww;

    new-instance v3, Lmww;

    invoke-direct {v3}, Lmww;-><init>()V

    aput-object v3, v2, v0

    .line 5802
    iget-object v2, p0, Lmwv;->c:[Lmww;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5769
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 5729
    iget-object v0, p0, Lmwv;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 5730
    const/4 v0, 0x1

    iget-object v1, p0, Lmwv;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 5732
    :cond_0
    iget-object v0, p0, Lmwv;->c:[Lmww;

    if-eqz v0, :cond_2

    .line 5733
    iget-object v1, p0, Lmwv;->c:[Lmww;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 5734
    if-eqz v3, :cond_1

    .line 5735
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 5733
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5739
    :cond_2
    iget-object v0, p0, Lmwv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5741
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5715
    invoke-virtual {p0, p1}, Lmwv;->a(Loxn;)Lmwv;

    move-result-object v0

    return-object v0
.end method
