.class public final Lmjd;
.super Loxq;
.source "PG"


# instance fields
.field private a:Llte;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29158
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29161
    iput-object v0, p0, Lmjd;->apiHeader:Llyr;

    .line 29164
    iput-object v0, p0, Lmjd;->a:Llte;

    .line 29158
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29181
    const/4 v0, 0x0

    .line 29182
    iget-object v1, p0, Lmjd;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 29183
    const/4 v0, 0x1

    iget-object v1, p0, Lmjd;->apiHeader:Llyr;

    .line 29184
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29186
    :cond_0
    iget-object v1, p0, Lmjd;->a:Llte;

    if-eqz v1, :cond_1

    .line 29187
    const/4 v1, 0x2

    iget-object v2, p0, Lmjd;->a:Llte;

    .line 29188
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29190
    :cond_1
    iget-object v1, p0, Lmjd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29191
    iput v0, p0, Lmjd;->ai:I

    .line 29192
    return v0
.end method

.method public a(Loxn;)Lmjd;
    .locals 2

    .prologue
    .line 29200
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 29201
    sparse-switch v0, :sswitch_data_0

    .line 29205
    iget-object v1, p0, Lmjd;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 29206
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjd;->ah:Ljava/util/List;

    .line 29209
    :cond_1
    iget-object v1, p0, Lmjd;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29211
    :sswitch_0
    return-object p0

    .line 29216
    :sswitch_1
    iget-object v0, p0, Lmjd;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 29217
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjd;->apiHeader:Llyr;

    .line 29219
    :cond_2
    iget-object v0, p0, Lmjd;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29223
    :sswitch_2
    iget-object v0, p0, Lmjd;->a:Llte;

    if-nez v0, :cond_3

    .line 29224
    new-instance v0, Llte;

    invoke-direct {v0}, Llte;-><init>()V

    iput-object v0, p0, Lmjd;->a:Llte;

    .line 29226
    :cond_3
    iget-object v0, p0, Lmjd;->a:Llte;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29201
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29169
    iget-object v0, p0, Lmjd;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 29170
    const/4 v0, 0x1

    iget-object v1, p0, Lmjd;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29172
    :cond_0
    iget-object v0, p0, Lmjd;->a:Llte;

    if-eqz v0, :cond_1

    .line 29173
    const/4 v0, 0x2

    iget-object v1, p0, Lmjd;->a:Llte;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29175
    :cond_1
    iget-object v0, p0, Lmjd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29177
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29154
    invoke-virtual {p0, p1}, Lmjd;->a(Loxn;)Lmjd;

    move-result-object v0

    return-object v0
.end method
