.class public final Lptp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lptp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:[Lptr;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x13122db

    new-instance v1, Lptq;

    invoke-direct {v1}, Lptq;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lptp;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 133
    sget-object v0, Lptr;->a:[Lptr;

    iput-object v0, p0, Lptp;->b:[Lptr;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 151
    .line 152
    iget-object v1, p0, Lptp;->b:[Lptr;

    if-eqz v1, :cond_1

    .line 153
    iget-object v2, p0, Lptp;->b:[Lptr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 154
    if-eqz v4, :cond_0

    .line 155
    const/4 v5, 0x1

    .line 156
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 153
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    :cond_1
    iget-object v1, p0, Lptp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    iput v0, p0, Lptp;->ai:I

    .line 162
    return v0
.end method

.method public a(Loxn;)Lptp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 170
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 171
    sparse-switch v0, :sswitch_data_0

    .line 175
    iget-object v2, p0, Lptp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 176
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lptp;->ah:Ljava/util/List;

    .line 179
    :cond_1
    iget-object v2, p0, Lptp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    :sswitch_0
    return-object p0

    .line 186
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 187
    iget-object v0, p0, Lptp;->b:[Lptr;

    if-nez v0, :cond_3

    move v0, v1

    .line 188
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lptr;

    .line 189
    iget-object v3, p0, Lptp;->b:[Lptr;

    if-eqz v3, :cond_2

    .line 190
    iget-object v3, p0, Lptp;->b:[Lptr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 192
    :cond_2
    iput-object v2, p0, Lptp;->b:[Lptr;

    .line 193
    :goto_2
    iget-object v2, p0, Lptp;->b:[Lptr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 194
    iget-object v2, p0, Lptp;->b:[Lptr;

    new-instance v3, Lptr;

    invoke-direct {v3}, Lptr;-><init>()V

    aput-object v3, v2, v0

    .line 195
    iget-object v2, p0, Lptp;->b:[Lptr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 196
    invoke-virtual {p1}, Loxn;->a()I

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 187
    :cond_3
    iget-object v0, p0, Lptp;->b:[Lptr;

    array-length v0, v0

    goto :goto_1

    .line 199
    :cond_4
    iget-object v2, p0, Lptp;->b:[Lptr;

    new-instance v3, Lptr;

    invoke-direct {v3}, Lptr;-><init>()V

    aput-object v3, v2, v0

    .line 200
    iget-object v2, p0, Lptp;->b:[Lptr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 171
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 138
    iget-object v0, p0, Lptp;->b:[Lptr;

    if-eqz v0, :cond_1

    .line 139
    iget-object v1, p0, Lptp;->b:[Lptr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 140
    if-eqz v3, :cond_0

    .line 141
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 139
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_1
    iget-object v0, p0, Lptp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 147
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lptp;->a(Loxn;)Lptp;

    move-result-object v0

    return-object v0
.end method
