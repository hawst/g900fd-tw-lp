.class public final Lkbv;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmcu;",
        "Lmcv;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lnjt;

.field private c:Lhei;

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 33
    const-string v3, "getsimpleprofile"

    new-instance v4, Lmcu;

    invoke-direct {v4}, Lmcu;-><init>()V

    new-instance v5, Lmcv;

    invoke-direct {v5}, Lmcv;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 35
    iput-object p4, p0, Lkbv;->a:Ljava/lang/String;

    .line 36
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lkbv;->c:Lhei;

    .line 37
    iget-object v0, p0, Lkbv;->c:Lhei;

    invoke-interface {v0, p3}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 38
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lkbv;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Llsu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lkbv;->p:Z

    .line 40
    return-void
.end method


# virtual methods
.method protected a(Lmcu;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 44
    new-instance v1, Lnle;

    invoke-direct {v1}, Lnle;-><init>()V

    iput-object v1, p1, Lmcu;->a:Lnle;

    .line 45
    iget-object v1, p1, Lmcu;->a:Lnle;

    iget-object v2, p0, Lkbv;->a:Ljava/lang/String;

    iput-object v2, v1, Lnle;->a:Ljava/lang/String;

    .line 46
    iget-object v1, p1, Lmcu;->a:Lnle;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnle;->c:Ljava/lang/Boolean;

    .line 47
    iget-object v1, p1, Lmcu;->a:Lnle;

    iget-boolean v2, p0, Lkbv;->p:Z

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnle;->b:Ljava/lang/Boolean;

    .line 48
    iget-object v0, p1, Lmcu;->a:Lnle;

    iget-boolean v1, p0, Lkbv;->p:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnle;->d:Ljava/lang/Boolean;

    .line 49
    return-void
.end method

.method protected a(Lmcv;)V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p1, Lmcv;->a:Lnlq;

    iget-object v0, v0, Lnlq;->a:Lnjt;

    iput-object v0, p0, Lkbv;->b:Lnjt;

    .line 55
    iget-object v0, p0, Lkbv;->b:Lnjt;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lkbv;->p:Z

    if-eqz v0, :cond_2

    .line 56
    iget-object v1, p0, Lkbv;->b:Lnjt;

    iget-object v0, p0, Lkbv;->f:Landroid/content/Context;

    const-class v2, Liuy;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liuy;

    invoke-interface {v0}, Liuy;->a()Liuw;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, v1, Lnjt;->e:Lnkd;

    if-nez v2, :cond_0

    new-instance v2, Lnkd;

    invoke-direct {v2}, Lnkd;-><init>()V

    iput-object v2, v1, Lnjt;->e:Lnkd;

    :cond_0
    iget-object v2, v1, Lnjt;->e:Lnkd;

    iget-object v2, v2, Lnkd;->m:Lnik;

    if-nez v2, :cond_1

    iget-object v2, v1, Lnjt;->e:Lnkd;

    new-instance v3, Lnik;

    invoke-direct {v3}, Lnik;-><init>()V

    iput-object v3, v2, Lnkd;->m:Lnik;

    :cond_1
    iget-object v1, v1, Lnjt;->e:Lnkd;

    iget-object v1, v1, Lnkd;->m:Lnik;

    invoke-virtual {v0}, Liuw;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lnik;->b:Ljava/lang/Boolean;

    .line 58
    :cond_2
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lmcu;

    invoke-virtual {p0, p1}, Lkbv;->a(Lmcu;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lmcv;

    invoke-virtual {p0, p1}, Lkbv;->a(Lmcv;)V

    return-void
.end method

.method public i()Lnjt;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lkbv;->b:Lnjt;

    return-object v0
.end method
