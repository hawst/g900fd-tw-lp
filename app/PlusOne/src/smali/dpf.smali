.class public final Ldpf;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Lkfd;

.field private final e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 39
    const-string v0, "LocationSharingSettingsDeltaTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 41
    iput p2, p0, Ldpf;->a:I

    .line 42
    iput-object p3, p0, Ldpf;->b:Ljava/lang/String;

    .line 43
    iput p4, p0, Ldpf;->c:I

    .line 44
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Ldpf;->d:Lkfd;

    .line 45
    iput-boolean p5, p0, Ldpf;->e:Z

    .line 46
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    .line 59
    invoke-virtual {p0}, Ldpf;->f()Landroid/content/Context;

    move-result-object v1

    .line 60
    new-instance v0, Ldla;

    iget v2, p0, Ldpf;->a:I

    iget-object v3, p0, Ldpf;->b:Ljava/lang/String;

    iget v4, p0, Ldpf;->c:I

    iget-boolean v5, p0, Ldpf;->e:Z

    invoke-direct/range {v0 .. v5}, Ldla;-><init>(Landroid/content/Context;ILjava/lang/String;IZ)V

    .line 64
    iget-object v1, p0, Ldpf;->d:Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 66
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    .line 67
    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    .line 68
    invoke-virtual {v0}, Ldla;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldpf;->f()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a0592

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 70
    return-object v1

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
