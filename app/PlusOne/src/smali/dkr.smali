.class public final Ldkr;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmdm;",
        "Lmdn;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private p:Lnce;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;IZ)V
    .locals 6

    .prologue
    .line 31
    if-eqz p5, :cond_0

    const-string v3, "getuseritemsbackground"

    :goto_0
    new-instance v4, Lmdm;

    invoke-direct {v4}, Lmdm;-><init>()V

    new-instance v5, Lmdn;

    invoke-direct {v5}, Lmdn;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 33
    iput-object p3, p0, Ldkr;->a:Ljava/lang/String;

    .line 34
    iput p4, p0, Ldkr;->b:I

    .line 35
    return-void

    .line 31
    :cond_0
    const-string v3, "getuseritems"

    goto :goto_0
.end method


# virtual methods
.method protected a(Lmdm;)V
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lncd;

    invoke-direct {v0}, Lncd;-><init>()V

    iput-object v0, p1, Lmdm;->a:Lncd;

    .line 40
    iget-object v0, p1, Lmdm;->a:Lncd;

    .line 41
    new-instance v1, Lndj;

    invoke-direct {v1}, Lndj;-><init>()V

    iput-object v1, v0, Lncd;->b:Lndj;

    .line 42
    iget-object v1, v0, Lncd;->b:Lndj;

    iget-object v2, p0, Ldkr;->a:Ljava/lang/String;

    iput-object v2, v1, Lndj;->a:Ljava/lang/String;

    .line 43
    iget v1, p0, Ldkr;->b:I

    iput v1, v0, Lncd;->a:I

    .line 44
    return-void
.end method

.method protected a(Lmdn;)V
    .locals 7

    .prologue
    .line 48
    iget-object v0, p1, Lmdn;->a:Lnce;

    iput-object v0, p0, Ldkr;->p:Lnce;

    .line 49
    const-string v0, "GetUserItems"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "Response:  resumeToken: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldkr;->p:Lnce;

    iget-object v1, v1, Lnce;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldkr;->p:Lnce;

    iget-object v2, v2, Lnce;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldkr;->p:Lnce;

    iget-object v3, v3, Lnce;->c:[Lnzx;

    array-length v3, v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x23

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " syncToken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " num tiles: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 55
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmdm;

    invoke-virtual {p0, p1}, Ldkr;->a(Lmdm;)V

    return-void
.end method

.method public b()Lnyz;
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Ldkr;->p:Lnce;

    iget-object v0, v0, Lnce;->d:[Lnyz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkr;->p:Lnce;

    iget-object v0, v0, Lnce;->d:[Lnyz;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Ldkr;->p:Lnce;

    iget-object v0, v0, Lnce;->d:[Lnyz;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmdn;

    invoke-virtual {p0, p1}, Ldkr;->a(Lmdn;)V

    return-void
.end method

.method public c()[Lnzx;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ldkr;->p:Lnce;

    iget-object v0, v0, Lnce;->c:[Lnzx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkr;->p:Lnce;

    iget-object v0, v0, Lnce;->c:[Lnzx;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Lnzx;

    goto :goto_0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Ldkr;->p:Lnce;

    iget-object v0, v0, Lnce;->a:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ldkr;->p:Lnce;

    iget-object v0, v0, Lnce;->b:Ljava/lang/String;

    return-object v0
.end method
