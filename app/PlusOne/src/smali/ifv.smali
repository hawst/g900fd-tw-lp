.class public abstract Lifv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ligf;


# static fields
.field private static i:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lifv;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static j:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Class",
            "<",
            "Lifv;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field private e:I

.field private f:I

.field private g:Z

.field private h:Lifx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lifv;->i:Ljava/util/WeakHashMap;

    .line 56
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lifv;->j:Ljava/lang/ThreadLocal;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lifv;-><init>(Lifx;II)V

    .line 70
    return-void
.end method

.method protected constructor <init>(Lifx;II)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput v0, p0, Lifv;->a:I

    .line 45
    iput v0, p0, Lifv;->c:I

    .line 46
    iput v0, p0, Lifv;->d:I

    .line 53
    iput-object v1, p0, Lifv;->h:Lifx;

    .line 60
    invoke-virtual {p0, p1}, Lifv;->a(Lifx;)V

    .line 61
    iput p2, p0, Lifv;->a:I

    .line 62
    iput p3, p0, Lifv;->b:I

    .line 63
    sget-object v1, Lifv;->i:Ljava/util/WeakHashMap;

    monitor-enter v1

    .line 64
    :try_start_0
    sget-object v0, Lifv;->i:Ljava/util/WeakHashMap;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static k()V
    .locals 4

    .prologue
    .line 207
    sget-object v1, Lifv;->i:Ljava/util/WeakHashMap;

    monitor-enter v1

    .line 208
    :try_start_0
    sget-object v0, Lifv;->i:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lifv;

    .line 209
    const/4 v3, 0x0

    iput v3, v0, Lifv;->b:I

    .line 210
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lifv;->a(Lifx;)V

    goto :goto_0

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 4

    .prologue
    const/16 v2, 0x1000

    const/4 v1, 0x0

    .line 81
    iput p1, p0, Lifv;->c:I

    .line 82
    iput p2, p0, Lifv;->d:I

    .line 83
    if-lez p1, :cond_2

    invoke-static {p1}, Lifu;->a(I)I

    move-result v0

    :goto_0
    iput v0, p0, Lifv;->e:I

    .line 84
    if-lez p2, :cond_3

    invoke-static {p2}, Lifu;->a(I)I

    move-result v0

    :goto_1
    iput v0, p0, Lifv;->f:I

    .line 85
    iget v0, p0, Lifv;->e:I

    if-gt v0, v2, :cond_0

    iget v0, p0, Lifv;->f:I

    if-le v0, v2, :cond_1

    .line 86
    :cond_0
    const-string v0, "texture is too large: %d x %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lifv;->e:I

    .line 87
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    const/4 v1, 0x1

    iget v3, p0, Lifv;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    .line 86
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    .line 89
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 83
    goto :goto_0

    :cond_3
    move v0, v1

    .line 84
    goto :goto_1
.end method

.method protected a(Lifx;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lifv;->h:Lifx;

    .line 74
    return-void
.end method

.method public a(Lifx;IIII)V
    .locals 6

    .prologue
    .line 145
    move-object v0, p1

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lifx;->a(Lifv;IIII)V

    .line 146
    return-void
.end method

.method protected a(Z)V
    .locals 0

    .prologue
    .line 135
    iput-boolean p1, p0, Lifv;->g:Z

    .line 136
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lifv;->a:I

    return v0
.end method

.method protected abstract b(Lifx;)Z
.end method

.method public c()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lifv;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lifv;->d:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lifv;->e:I

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lifv;->f:I

    return v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 186
    sget-object v0, Lifv;->j:Ljava/lang/ThreadLocal;

    const-class v1, Lifv;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 187
    invoke-virtual {p0}, Lifv;->j()V

    .line 188
    sget-object v0, Lifv;->j:Ljava/lang/ThreadLocal;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 189
    return-void
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lifv;->g:Z

    return v0
.end method

.method protected abstract h()I
.end method

.method public i()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 156
    iget v1, p0, Lifv;->b:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 162
    iget-object v0, p0, Lifv;->h:Lifx;

    if-eqz v0, :cond_0

    iget v1, p0, Lifv;->a:I

    if-eq v1, v2, :cond_0

    invoke-interface {v0, p0}, Lifx;->a(Lifv;)Z

    iput v2, p0, Lifv;->a:I

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lifv;->b:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lifv;->a(Lifx;)V

    .line 163
    return-void
.end method
