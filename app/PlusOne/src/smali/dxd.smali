.class public final Ldxd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final A:Lief;

.field public static final B:Lief;

.field public static final C:Lief;

.field public static final D:Lief;

.field public static final E:Lief;

.field public static final F:Lief;

.field private static G:Lief;

.field private static H:Lief;

.field private static I:Lief;

.field private static J:Lief;

.field public static final a:Lief;

.field public static final b:Lief;

.field public static final c:Lief;

.field public static final d:Lief;

.field public static final e:Lief;

.field public static final f:Lief;

.field public static final g:Lief;

.field public static final h:Lief;

.field public static final i:Lief;

.field public static final j:Lief;

.field public static final k:Lief;

.field public static final l:Lief;

.field public static final m:Lief;

.field public static final n:Lief;

.field public static final o:Lief;

.field public static final p:Lief;

.field public static final q:Lief;

.field public static final r:Lief;

.field public static final s:Lief;

.field public static final t:Lief;

.field public static final u:Lief;

.field public static final v:Lief;

.field public static final w:Lief;

.field public static final x:Lief;

.field public static final y:Lief;

.field public static final z:Lief;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 25
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_3g_stream_gif"

    const-string v2, "false"

    const-string v3, "16650d86"

    const/4 v4, 0x3

    invoke-direct {v0, v1, v2, v3, v4}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->a:Lief;

    .line 37
    new-instance v0, Lief;

    const-string v1, "debug.plus.aspen_settings_v2"

    const-string v2, "false"

    const-string v3, "54100b94"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->b:Lief;

    .line 46
    new-instance v0, Lief;

    const-string v1, "debug.plus.location_sharing"

    const-string v2, "false"

    const-string v3, "8d04e73"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->G:Lief;

    .line 55
    new-instance v0, Lief;

    const-string v1, "debug.plus.loc_sharing_actions"

    const-string v2, "false"

    const-string v3, "6e3dc74c"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->c:Lief;

    .line 64
    new-instance v0, Lief;

    const-string v1, "debug.plus.letterman_watchpage"

    const-string v2, "false"

    const-string v3, "5d6aa522"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->d:Lief;

    .line 73
    new-instance v0, Lief;

    const-string v1, "debug.plus.activity_posts"

    const-string v2, "false"

    const-string v3, "fc9a51be"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->e:Lief;

    .line 82
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_photo_editor"

    const-string v2, "true"

    const-string v3, "58bc1fbe"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->H:Lief;

    .line 92
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_photos_sync"

    const-string v2, "true"

    const-string v3, "89fa7b0d"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->f:Lief;

    .line 101
    new-instance v0, Lief;

    const-string v1, "debug.plus.social_reviews"

    const-string v2, "true"

    const-string v3, "de815509"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->I:Lief;

    .line 110
    new-instance v0, Lief;

    const-string v1, "debug.plus.guns_v2"

    const-string v2, "false"

    const-string v3, "c620d3de"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->g:Lief;

    .line 123
    new-instance v0, Lief;

    const-string v1, "debug.plus.guns_staleness"

    const-string v2, "15000"

    const-string v3, "32a36dc7"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->h:Lief;

    .line 132
    new-instance v0, Lief;

    const-string v1, "debug.plus.guns_components"

    const-string v2, "false"

    const-string v3, "4be7e68b"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->i:Lief;

    .line 141
    new-instance v0, Lief;

    const-string v1, "debug.plus.guns_prefetch"

    const-string v2, "false"

    const-string v3, "127c6d0c"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->j:Lief;

    .line 154
    new-instance v0, Lief;

    const-string v1, "debug.plus.guns_prefetch_ms"

    const-wide/32 v2, 0x493e0

    .line 155
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "8f83c63f"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->k:Lief;

    .line 163
    new-instance v0, Lief;

    const-string v1, "debug.plus.guns_fetch_on_login"

    const-string v2, "false"

    const-string v3, "2a6983cf"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->l:Lief;

    .line 172
    new-instance v0, Lief;

    const-string v1, "debug.plus.chromecast"

    const-string v2, "true"

    const-string v3, "f5c943ea"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->m:Lief;

    .line 182
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_springboard"

    const-string v2, "true"

    const-string v3, "11aa8a01"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->n:Lief;

    .line 196
    new-instance v0, Lief;

    const-string v1, "debug.plus.springboard_dur_i"

    const-wide/32 v2, 0x240c8400

    .line 197
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "f1b4186c"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->o:Lief;

    .line 211
    new-instance v0, Lief;

    const-string v1, "debug.plus.springboard_dur_s"

    const-wide v2, 0x9a7ec800L

    .line 213
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, "527011c3"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->p:Lief;

    .line 220
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable.gms.people"

    const-string v2, "false"

    const-string v3, "8698ab36"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->q:Lief;

    .line 228
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_crowdsurf"

    const-string v2, "false"

    const-string v3, "a0245361"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->J:Lief;

    .line 236
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_hoa_broadcast"

    const-string v2, "false"

    const-string v3, "6477d8b"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->r:Lief;

    .line 244
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_close_ties"

    const-string v2, "false"

    const-string v3, "c1a9f079"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->s:Lief;

    .line 252
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_onb_ww"

    const-string v2, "false"

    const-string v3, "c2562c0"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->t:Lief;

    .line 260
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_onb_cs"

    const-string v2, "false"

    const-string v3, "9a2e2672"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->u:Lief;

    .line 269
    new-instance v0, Lief;

    const-string v1, "debug.plus.remove_ab_wifi_only"

    const-string v2, "false"

    const-string v3, "e0c626d4"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->v:Lief;

    .line 277
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_new_showtime"

    const-string v2, "false"

    const-string v3, "b365f857"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->w:Lief;

    .line 285
    new-instance v0, Lief;

    const-string v1, "debug.plus.photos_app_url"

    const-string v2, ""

    const-string v3, "92722ec3"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->x:Lief;

    .line 293
    new-instance v0, Lief;

    const-string v1, "debug.plus.disable_promo_btn"

    const-string v2, "false"

    const-string v3, "5bf2afed"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->y:Lief;

    .line 302
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_add_btn_posts"

    const-string v2, "false"

    const-string v3, "17181959"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->z:Lief;

    .line 310
    new-instance v0, Lief;

    const-string v1, "debug.plus.silent_ab_notifs"

    const-string v2, "false"

    const-string v3, "34ad8e51"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->A:Lief;

    .line 319
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_gaiaonly"

    const-string v2, "false"

    const-string v3, "b1f03ec2"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->B:Lief;

    .line 327
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_hun_notifs"

    const-string v2, "false"

    const-string v3, "fe219274"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->C:Lief;

    .line 335
    new-instance v0, Lief;

    const-string v1, "debug.plus.enable_aam_notifs"

    const-string v2, "false"

    const-string v3, "f1f30164"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->D:Lief;

    .line 345
    new-instance v0, Lief;

    const-string v1, "debug.deleteaccountv2.enabled"

    const-string v2, "false"

    const-string v3, "ac035941"

    invoke-direct {v0, v1, v2, v3, v5}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->E:Lief;

    .line 353
    new-instance v0, Lief;

    const-string v1, "debug.plus.settings_poll_int"

    const-string v2, "3600"

    const-string v3, "5ae2ea30"

    invoke-direct {v0, v1, v2, v3, v6}, Lief;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Ldxd;->F:Lief;

    return-void
.end method

.method public static a()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lief;",
            ">;"
        }
    .end annotation

    .prologue
    .line 358
    const/16 v0, 0x33

    new-array v0, v0, [Lief;

    const/4 v1, 0x0

    sget-object v2, Ldxd;->a:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ldxd;->b:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Ldxd;->G:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Ldxd;->c:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Ldxd;->d:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Ldxd;->e:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldxd;->H:Lief;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldxd;->f:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldxd;->I:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Ldxd;->g:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Ldxd;->h:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Ldxd;->m:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Ldxd;->n:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Ldxd;->o:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Ldxd;->p:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Ldxd;->q:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Ldxd;->J:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Ldxd;->r:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Ldxd;->s:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Ldxd;->v:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Ldxd;->i:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Ldxd;->j:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Ldxd;->k:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Ldxd;->l:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Ldxd;->w:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Ldxd;->z:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Ldxd;->t:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Ldxd;->u:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Ldxd;->D:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcuk;->g:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcuk;->a:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcuk;->f:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcuk;->e:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcuk;->d:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcuk;->c:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcuk;->b:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcul;->d:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcul;->e:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcul;->c:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcul;->a:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcul;->b:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcuj;->a:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcum;->a:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcum;->b:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Ldxd;->x:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Ldxd;->y:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Ldxd;->A:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Ldxd;->B:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Ldxd;->C:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Ldxd;->F:Lief;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Ldxd;->E:Lief;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
