.class public final Lezm;
.super Lfbx;
.source "PG"


# instance fields
.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lfbx;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 38
    iput-object p4, p0, Lezm;->j:Ljava/lang/String;

    .line 41
    new-instance v0, Lezn;

    invoke-direct {v0}, Lezn;-><init>()V

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 56
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lezm;->f:Landroid/view/LayoutInflater;

    const v1, 0x7f04018e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    .line 111
    move-object v8, p1

    check-cast v8, Lcom/google/android/apps/plus/views/PhotoTileView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p3}, Lezk;->a(Landroid/content/Context;Landroid/database/Cursor;)Lizu;

    move-result-object v3

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoTileView;->b(I)V

    iget-object v0, p0, Lezm;->e:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    new-instance v1, Ljug;

    iget-object v2, p0, Lezm;->j:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljug;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljue;

    invoke-direct {v2, v3}, Ljue;-><init>(Lizu;)V

    invoke-virtual {v0, v1, v2}, Ljcn;->a(Ljcj;Ljcm;)Ljcl;

    move-result-object v0

    check-cast v0, Ljuc;

    if-nez v0, :cond_0

    new-instance v0, Ljuc;

    iget-object v1, p0, Lezm;->j:Ljava/lang/String;

    iget-object v2, p0, Lezm;->j:Ljava/lang/String;

    const-wide/32 v4, 0x40000

    invoke-static {}, Ljvj;->c()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v8, v3, v1}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Lizu;Lizo;)V

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->a(Ljcl;)V

    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->n(Z)V

    iget-object v0, p0, Lezm;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lezm;->i:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 112
    new-instance v0, Llka;

    const/4 v1, -0x3

    invoke-direct {v0, v9, v1}, Llka;-><init>(II)V

    .line 114
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 115
    return-void
.end method

.method public getItemId(I)J
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 67
    invoke-super {p0}, Lfbx;->getCount()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 68
    const-wide/high16 v0, -0x8000000000000000L

    .line 79
    :goto_0
    return-wide v0

    .line 70
    :cond_0
    invoke-virtual {p0}, Lezm;->a()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lhyx;

    .line 71
    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lhyx;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 76
    invoke-virtual {v0}, Lhyx;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "media_is_video"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v1

    invoke-virtual {v0}, Lhyx;->a()I

    move-result v2

    aget-boolean v1, v1, v2

    if-eqz v1, :cond_1

    sget-object v1, Ljac;->b:Ljac;

    :goto_1
    sget-object v2, Ljac;->b:Ljac;

    if-ne v1, v2, :cond_2

    .line 77
    invoke-virtual {v0, v3}, Lhyx;->getLong(I)J

    move-result-wide v0

    neg-long v0, v0

    goto :goto_0

    .line 76
    :cond_1
    sget-object v1, Ljac;->a:Ljac;

    goto :goto_1

    .line 79
    :cond_2
    invoke-virtual {v0, v3}, Lhyx;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    .line 83
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method
