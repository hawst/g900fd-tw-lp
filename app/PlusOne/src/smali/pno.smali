.class public final Lpno;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 506
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 537
    const/4 v0, 0x0

    .line 538
    iget-object v1, p0, Lpno;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 539
    const/4 v0, 0x1

    iget-object v1, p0, Lpno;->a:Ljava/lang/String;

    .line 540
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 542
    :cond_0
    iget-object v1, p0, Lpno;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 543
    const/4 v1, 0x2

    iget-object v2, p0, Lpno;->b:Ljava/lang/String;

    .line 544
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 546
    :cond_1
    iget-object v1, p0, Lpno;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 547
    const/4 v1, 0x3

    iget-object v2, p0, Lpno;->c:Ljava/lang/String;

    .line 548
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 550
    :cond_2
    iget-object v1, p0, Lpno;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 551
    const/4 v1, 0x4

    iget-object v2, p0, Lpno;->d:Ljava/lang/String;

    .line 552
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 554
    :cond_3
    iget-object v1, p0, Lpno;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 555
    iput v0, p0, Lpno;->ai:I

    .line 556
    return v0
.end method

.method public a(Loxn;)Lpno;
    .locals 2

    .prologue
    .line 564
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 565
    sparse-switch v0, :sswitch_data_0

    .line 569
    iget-object v1, p0, Lpno;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 570
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpno;->ah:Ljava/util/List;

    .line 573
    :cond_1
    iget-object v1, p0, Lpno;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 575
    :sswitch_0
    return-object p0

    .line 580
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpno;->a:Ljava/lang/String;

    goto :goto_0

    .line 584
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpno;->b:Ljava/lang/String;

    goto :goto_0

    .line 588
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpno;->c:Ljava/lang/String;

    goto :goto_0

    .line 592
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpno;->d:Ljava/lang/String;

    goto :goto_0

    .line 565
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 519
    iget-object v0, p0, Lpno;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 520
    const/4 v0, 0x1

    iget-object v1, p0, Lpno;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 522
    :cond_0
    iget-object v0, p0, Lpno;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 523
    const/4 v0, 0x2

    iget-object v1, p0, Lpno;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 525
    :cond_1
    iget-object v0, p0, Lpno;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 526
    const/4 v0, 0x3

    iget-object v1, p0, Lpno;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 528
    :cond_2
    iget-object v0, p0, Lpno;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 529
    const/4 v0, 0x4

    iget-object v1, p0, Lpno;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 531
    :cond_3
    iget-object v0, p0, Lpno;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 533
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 502
    invoke-virtual {p0, p1}, Lpno;->a(Loxn;)Lpno;

    move-result-object v0

    return-object v0
.end method
