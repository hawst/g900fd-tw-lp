.class public final Lkyr;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lkfd;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/Integer;

.field private final e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .locals 1

    .prologue
    .line 47
    const-string v0, "SetSquareVolumeControlsTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 48
    iput p2, p0, Lkyr;->b:I

    .line 49
    iput-object p3, p0, Lkyr;->c:Ljava/lang/String;

    .line 50
    iput-object p5, p0, Lkyr;->d:Ljava/lang/Integer;

    .line 51
    iput-object p4, p0, Lkyr;->e:Ljava/lang/Boolean;

    .line 52
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lkyr;->a:Lkfd;

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 6

    .prologue
    .line 35
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lkyr;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/Boolean;Ljava/lang/Integer;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    .line 57
    new-instance v1, Lodw;

    invoke-direct {v1}, Lodw;-><init>()V

    .line 58
    iget-object v0, p0, Lkyr;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lkyr;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Lodw;->a:I

    .line 62
    :cond_0
    iget-object v0, p0, Lkyr;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lkyr;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    :goto_0
    iput v0, v1, Lodw;->b:I

    .line 67
    :cond_1
    new-instance v2, Lkyq;

    invoke-virtual {p0}, Lkyr;->f()Landroid/content/Context;

    move-result-object v0

    new-instance v3, Lkfo;

    .line 68
    invoke-virtual {p0}, Lkyr;->f()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lkyr;->b:I

    invoke-direct {v3, v4, v5}, Lkfo;-><init>(Landroid/content/Context;I)V

    iget-object v4, p0, Lkyr;->c:Ljava/lang/String;

    invoke-direct {v2, v0, v3, v4, v1}, Lkyq;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lodw;)V

    .line 69
    iget-object v0, p0, Lkyr;->a:Lkfd;

    invoke-interface {v0, v2}, Lkfd;->a(Lkff;)V

    .line 71
    invoke-virtual {v2}, Lkff;->t()Z

    move-result v0

    if-nez v0, :cond_2

    .line 72
    invoke-virtual {p0}, Lkyr;->f()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lktq;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iget v3, p0, Lkyr;->b:I

    iget-object v4, p0, Lkyr;->c:Ljava/lang/String;

    .line 73
    invoke-interface {v0, v3, v4, v1}, Lktq;->a(ILjava/lang/String;Lodw;)V

    .line 76
    :cond_2
    new-instance v1, Lhoz;

    iget v3, v2, Lkff;->i:I

    iget-object v4, v2, Lkff;->k:Ljava/lang/Exception;

    invoke-virtual {v2}, Lkff;->t()Z

    move-result v0

    iget-object v2, p0, Lkyr;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lkyr;->d:Ljava/lang/Integer;

    if-nez v2, :cond_7

    iget-object v2, p0, Lkyr;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    if-eqz v0, :cond_4

    const v0, 0x7f0a0213

    :goto_1
    invoke-virtual {p0}, Lkyr;->f()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v4, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v1

    .line 63
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 76
    :cond_4
    const v0, 0x7f0a020f

    goto :goto_1

    :cond_5
    if-eqz v0, :cond_6

    const v0, 0x7f0a0214

    goto :goto_1

    :cond_6
    const v0, 0x7f0a0210

    goto :goto_1

    :cond_7
    if-eqz v0, :cond_8

    const v0, 0x7f0a020c

    goto :goto_1

    :cond_8
    const v0, 0x7f0a020b

    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lkyr;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkyr;->d:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 82
    invoke-virtual {p0}, Lkyr;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lkyr;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0211

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 85
    :goto_1
    return-object v0

    .line 82
    :cond_0
    const v0, 0x7f0a0212

    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {p0}, Lkyr;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a020a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
