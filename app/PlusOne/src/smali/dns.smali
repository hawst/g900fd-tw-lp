.class public final Ldns;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmke;",
        "Lmkf;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lndu;

.field private b:Ljava/lang/String;

.field private p:Z

.field private q:Z

.field private r:Ljava/lang/Integer;

.field private s:Ljava/lang/Integer;

.field private t:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;I)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 45
    const-string v4, "syncuserhighlights"

    new-instance v5, Lmke;

    invoke-direct {v5}, Lmke;-><init>()V

    new-instance v6, Lmkf;

    invoke-direct {v6}, Lmkf;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 33
    iput-object v7, p0, Ldns;->b:Ljava/lang/String;

    .line 34
    iput-boolean v8, p0, Ldns;->p:Z

    .line 35
    iput-boolean v8, p0, Ldns;->q:Z

    .line 36
    iput-object v7, p0, Ldns;->r:Ljava/lang/Integer;

    .line 37
    iput-object v7, p0, Ldns;->s:Ljava/lang/Integer;

    .line 38
    iput-object v7, p0, Ldns;->t:Ljava/lang/Long;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Integer;)Ldns;
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Ldns;->r:Ljava/lang/Integer;

    .line 87
    return-object p0
.end method

.method public a(Ljava/lang/Long;)Ldns;
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Ldns;->t:Ljava/lang/Long;

    .line 108
    return-object p0
.end method

.method public a(Z)Ldns;
    .locals 0

    .prologue
    .line 65
    iput-boolean p1, p0, Ldns;->p:Z

    .line 66
    return-object p0
.end method

.method protected a(Lmke;)V
    .locals 3

    .prologue
    .line 113
    new-instance v0, Lndt;

    invoke-direct {v0}, Lndt;-><init>()V

    iput-object v0, p1, Lmke;->a:Lndt;

    .line 114
    iget-object v0, p1, Lmke;->a:Lndt;

    .line 115
    iget-object v1, p0, Ldns;->r:Ljava/lang/Integer;

    iput-object v1, v0, Lndt;->c:Ljava/lang/Integer;

    .line 116
    iget-object v1, p0, Ldns;->b:Ljava/lang/String;

    iput-object v1, v0, Lndt;->d:Ljava/lang/String;

    .line 119
    new-instance v1, Lotq;

    invoke-direct {v1}, Lotq;-><init>()V

    iput-object v1, v0, Lndt;->e:Lotq;

    .line 120
    iget-object v1, v0, Lndt;->e:Lotq;

    new-instance v2, Loto;

    invoke-direct {v2}, Loto;-><init>()V

    iput-object v2, v1, Lotq;->a:Loto;

    .line 121
    iget-object v1, v0, Lndt;->e:Lotq;

    iget-object v1, v1, Lotq;->a:Loto;

    new-instance v2, Lorn;

    invoke-direct {v2}, Lorn;-><init>()V

    iput-object v2, v1, Loto;->a:Lorn;

    .line 123
    iget-boolean v1, p0, Ldns;->p:Z

    if-eqz v1, :cond_0

    .line 124
    new-instance v1, Loqv;

    invoke-direct {v1}, Loqv;-><init>()V

    iput-object v1, v0, Lndt;->f:Loqv;

    .line 125
    iget-object v1, v0, Lndt;->f:Loqv;

    new-instance v2, Loqu;

    invoke-direct {v2}, Loqu;-><init>()V

    iput-object v2, v1, Loqv;->a:Loqu;

    .line 128
    :cond_0
    iget-boolean v1, p0, Ldns;->q:Z

    if-eqz v1, :cond_1

    .line 129
    iget-object v1, v0, Lndt;->e:Lotq;

    new-instance v2, Lory;

    invoke-direct {v2}, Lory;-><init>()V

    iput-object v2, v1, Lotq;->b:Lory;

    .line 130
    iget-object v1, v0, Lndt;->e:Lotq;

    new-instance v2, Loro;

    invoke-direct {v2}, Loro;-><init>()V

    iput-object v2, v1, Lotq;->c:Loro;

    .line 133
    :cond_1
    iget-object v1, p0, Ldns;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 134
    iget-object v1, p0, Ldns;->s:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lndt;->a:I

    .line 137
    :cond_2
    iget-object v1, p0, Ldns;->t:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 138
    iget-object v1, p0, Ldns;->t:Ljava/lang/Long;

    iput-object v1, v0, Lndt;->b:Ljava/lang/Long;

    .line 140
    :cond_3
    return-void
.end method

.method protected a(Lmkf;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p1, Lmkf;->a:Lndu;

    iput-object v0, p0, Ldns;->a:Lndu;

    .line 145
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lmke;

    invoke-virtual {p0, p1}, Ldns;->a(Lmke;)V

    return-void
.end method

.method public a_(Ljava/lang/String;)Ldns;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Ldns;->b:Ljava/lang/String;

    .line 56
    return-object p0
.end method

.method public b(Ljava/lang/Integer;)Ldns;
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Ldns;->s:Ljava/lang/Integer;

    .line 98
    return-object p0
.end method

.method public b(Z)Ldns;
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Ldns;->q:Z

    .line 77
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 200
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ldns;->a:Lndu;

    iget-object v1, v1, Lndu;->d:[Lorf;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 201
    iget-object v1, p0, Ldns;->a:Lndu;

    iget-object v1, v1, Lndu;->d:[Lorf;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorf;->b:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldns;->a:Lndu;

    iget-object v1, v1, Lndu;->d:[Lorf;

    aget-object v1, v1, v0

    iget-object v1, v1, Lorf;->c:Losn;

    if-eqz v1, :cond_0

    .line 203
    iget-object v1, p0, Ldns;->a:Lndu;

    iget-object v1, v1, Lndu;->d:[Lorf;

    aget-object v0, v1, v0

    iget-object v0, v0, Lorf;->c:Losn;

    iget-object v0, v0, Losn;->a:Ljava/lang/String;

    .line 206
    :goto_1
    return-object v0

    .line 200
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Lmkf;

    invoke-virtual {p0, p1}, Ldns;->a(Lmkf;)V

    return-void
.end method

.method public c(Ljava/lang/String;)Ljava/util/LinkedHashSet;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/LinkedHashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 218
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    move v0, v1

    .line 219
    :goto_0
    iget-object v3, p0, Ldns;->a:Lndu;

    iget-object v3, v3, Lndu;->c:[Loud;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 220
    iget-object v3, p0, Ldns;->a:Lndu;

    iget-object v3, v3, Lndu;->c:[Loud;

    aget-object v3, v3, v0

    .line 221
    iget-object v4, v3, Loud;->c:Loua;

    if-eqz v4, :cond_0

    iget-object v4, v3, Loud;->c:Loua;

    iget-object v4, v4, Loua;->a:[Losl;

    array-length v4, v4

    if-lez v4, :cond_0

    iget-object v4, p0, Ldns;->a:Lndu;

    iget-object v4, v4, Lndu;->c:[Loud;

    aget-object v4, v4, v0

    iget-object v4, v4, Loud;->c:Loua;

    iget-object v4, v4, Loua;->a:[Losl;

    aget-object v4, v4, v1

    iget-object v4, v4, Losl;->b:Ljava/lang/String;

    .line 223
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 224
    iget-object v3, v3, Loud;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_1
    return-object v2
.end method

.method public d()Lndu;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Ldns;->a:Lndu;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Ldns;->a:Lndu;

    iget v0, v0, Lndu;->a:I

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Ldns;->a:Lndu;

    iget-object v0, v0, Lndu;->b:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Ldns;->a:Lndu;

    iget-object v0, v0, Lndu;->c:[Loud;

    array-length v0, v0

    return v0
.end method

.method public h()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    new-instance v0, Ldnt;

    invoke-direct {v0, p0}, Ldnt;-><init>(Ldns;)V

    return-object v0
.end method
