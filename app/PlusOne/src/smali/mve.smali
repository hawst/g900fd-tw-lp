.class public final Lmve;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lmvg;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:[Lmvf;

.field private e:[Lmvg;

.field private f:Lmvv;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10112
    invoke-direct {p0}, Loxq;-><init>()V

    .line 10115
    iput-object v1, p0, Lmve;->a:Lmvg;

    .line 10122
    sget-object v0, Lmvf;->a:[Lmvf;

    iput-object v0, p0, Lmve;->d:[Lmvf;

    .line 10125
    sget-object v0, Lmvg;->a:[Lmvg;

    iput-object v0, p0, Lmve;->e:[Lmvg;

    .line 10128
    iput-object v1, p0, Lmve;->f:Lmvv;

    .line 10131
    const/high16 v0, -0x80000000

    iput v0, p0, Lmve;->g:I

    .line 10112
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 10171
    .line 10172
    iget-object v0, p0, Lmve;->a:Lmvg;

    if-eqz v0, :cond_8

    .line 10173
    const/4 v0, 0x1

    iget-object v2, p0, Lmve;->a:Lmvg;

    .line 10174
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 10176
    :goto_0
    iget-object v2, p0, Lmve;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 10177
    const/4 v2, 0x2

    iget-object v3, p0, Lmve;->b:Ljava/lang/Integer;

    .line 10178
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 10180
    :cond_0
    iget-object v2, p0, Lmve;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 10181
    const/4 v2, 0x3

    iget-object v3, p0, Lmve;->c:Ljava/lang/Integer;

    .line 10182
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 10184
    :cond_1
    iget-object v2, p0, Lmve;->d:[Lmvf;

    if-eqz v2, :cond_3

    .line 10185
    iget-object v3, p0, Lmve;->d:[Lmvf;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 10186
    if-eqz v5, :cond_2

    .line 10187
    const/4 v6, 0x4

    .line 10188
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 10185
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 10192
    :cond_3
    iget-object v2, p0, Lmve;->e:[Lmvg;

    if-eqz v2, :cond_5

    .line 10193
    iget-object v2, p0, Lmve;->e:[Lmvg;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 10194
    if-eqz v4, :cond_4

    .line 10195
    const/4 v5, 0x5

    .line 10196
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 10193
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 10200
    :cond_5
    iget-object v1, p0, Lmve;->f:Lmvv;

    if-eqz v1, :cond_6

    .line 10201
    const/4 v1, 0x6

    iget-object v2, p0, Lmve;->f:Lmvv;

    .line 10202
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10204
    :cond_6
    iget v1, p0, Lmve;->g:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_7

    .line 10205
    const/4 v1, 0x7

    iget v2, p0, Lmve;->g:I

    .line 10206
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10208
    :cond_7
    iget-object v1, p0, Lmve;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10209
    iput v0, p0, Lmve;->ai:I

    .line 10210
    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmve;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 10218
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 10219
    sparse-switch v0, :sswitch_data_0

    .line 10223
    iget-object v2, p0, Lmve;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 10224
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmve;->ah:Ljava/util/List;

    .line 10227
    :cond_1
    iget-object v2, p0, Lmve;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10229
    :sswitch_0
    return-object p0

    .line 10234
    :sswitch_1
    iget-object v0, p0, Lmve;->a:Lmvg;

    if-nez v0, :cond_2

    .line 10235
    new-instance v0, Lmvg;

    invoke-direct {v0}, Lmvg;-><init>()V

    iput-object v0, p0, Lmve;->a:Lmvg;

    .line 10237
    :cond_2
    iget-object v0, p0, Lmve;->a:Lmvg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 10241
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmve;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 10245
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmve;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 10249
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 10250
    iget-object v0, p0, Lmve;->d:[Lmvf;

    if-nez v0, :cond_4

    move v0, v1

    .line 10251
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmvf;

    .line 10252
    iget-object v3, p0, Lmve;->d:[Lmvf;

    if-eqz v3, :cond_3

    .line 10253
    iget-object v3, p0, Lmve;->d:[Lmvf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 10255
    :cond_3
    iput-object v2, p0, Lmve;->d:[Lmvf;

    .line 10256
    :goto_2
    iget-object v2, p0, Lmve;->d:[Lmvf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 10257
    iget-object v2, p0, Lmve;->d:[Lmvf;

    new-instance v3, Lmvf;

    invoke-direct {v3}, Lmvf;-><init>()V

    aput-object v3, v2, v0

    .line 10258
    iget-object v2, p0, Lmve;->d:[Lmvf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 10259
    invoke-virtual {p1}, Loxn;->a()I

    .line 10256
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 10250
    :cond_4
    iget-object v0, p0, Lmve;->d:[Lmvf;

    array-length v0, v0

    goto :goto_1

    .line 10262
    :cond_5
    iget-object v2, p0, Lmve;->d:[Lmvf;

    new-instance v3, Lmvf;

    invoke-direct {v3}, Lmvf;-><init>()V

    aput-object v3, v2, v0

    .line 10263
    iget-object v2, p0, Lmve;->d:[Lmvf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 10267
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 10268
    iget-object v0, p0, Lmve;->e:[Lmvg;

    if-nez v0, :cond_7

    move v0, v1

    .line 10269
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmvg;

    .line 10270
    iget-object v3, p0, Lmve;->e:[Lmvg;

    if-eqz v3, :cond_6

    .line 10271
    iget-object v3, p0, Lmve;->e:[Lmvg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 10273
    :cond_6
    iput-object v2, p0, Lmve;->e:[Lmvg;

    .line 10274
    :goto_4
    iget-object v2, p0, Lmve;->e:[Lmvg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 10275
    iget-object v2, p0, Lmve;->e:[Lmvg;

    new-instance v3, Lmvg;

    invoke-direct {v3}, Lmvg;-><init>()V

    aput-object v3, v2, v0

    .line 10276
    iget-object v2, p0, Lmve;->e:[Lmvg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 10277
    invoke-virtual {p1}, Loxn;->a()I

    .line 10274
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 10268
    :cond_7
    iget-object v0, p0, Lmve;->e:[Lmvg;

    array-length v0, v0

    goto :goto_3

    .line 10280
    :cond_8
    iget-object v2, p0, Lmve;->e:[Lmvg;

    new-instance v3, Lmvg;

    invoke-direct {v3}, Lmvg;-><init>()V

    aput-object v3, v2, v0

    .line 10281
    iget-object v2, p0, Lmve;->e:[Lmvg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 10285
    :sswitch_6
    iget-object v0, p0, Lmve;->f:Lmvv;

    if-nez v0, :cond_9

    .line 10286
    new-instance v0, Lmvv;

    invoke-direct {v0}, Lmvv;-><init>()V

    iput-object v0, p0, Lmve;->f:Lmvv;

    .line 10288
    :cond_9
    iget-object v0, p0, Lmve;->f:Lmvv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 10292
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 10293
    if-eqz v0, :cond_a

    const/4 v2, 0x1

    if-eq v0, v2, :cond_a

    const/4 v2, 0x2

    if-eq v0, v2, :cond_a

    const/4 v2, 0x3

    if-eq v0, v2, :cond_a

    const/4 v2, 0x4

    if-eq v0, v2, :cond_a

    const/4 v2, 0x5

    if-eq v0, v2, :cond_a

    const/4 v2, 0x6

    if-eq v0, v2, :cond_a

    const/4 v2, 0x7

    if-eq v0, v2, :cond_a

    const/16 v2, 0x8

    if-eq v0, v2, :cond_a

    const/16 v2, 0x9

    if-ne v0, v2, :cond_b

    .line 10303
    :cond_a
    iput v0, p0, Lmve;->g:I

    goto/16 :goto_0

    .line 10305
    :cond_b
    iput v1, p0, Lmve;->g:I

    goto/16 :goto_0

    .line 10219
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 10136
    iget-object v1, p0, Lmve;->a:Lmvg;

    if-eqz v1, :cond_0

    .line 10137
    const/4 v1, 0x1

    iget-object v2, p0, Lmve;->a:Lmvg;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 10139
    :cond_0
    iget-object v1, p0, Lmve;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 10140
    const/4 v1, 0x2

    iget-object v2, p0, Lmve;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 10142
    :cond_1
    iget-object v1, p0, Lmve;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 10143
    const/4 v1, 0x3

    iget-object v2, p0, Lmve;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 10145
    :cond_2
    iget-object v1, p0, Lmve;->d:[Lmvf;

    if-eqz v1, :cond_4

    .line 10146
    iget-object v2, p0, Lmve;->d:[Lmvf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 10147
    if-eqz v4, :cond_3

    .line 10148
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 10146
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 10152
    :cond_4
    iget-object v1, p0, Lmve;->e:[Lmvg;

    if-eqz v1, :cond_6

    .line 10153
    iget-object v1, p0, Lmve;->e:[Lmvg;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 10154
    if-eqz v3, :cond_5

    .line 10155
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 10153
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 10159
    :cond_6
    iget-object v0, p0, Lmve;->f:Lmvv;

    if-eqz v0, :cond_7

    .line 10160
    const/4 v0, 0x6

    iget-object v1, p0, Lmve;->f:Lmvv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 10162
    :cond_7
    iget v0, p0, Lmve;->g:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_8

    .line 10163
    const/4 v0, 0x7

    iget v1, p0, Lmve;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 10165
    :cond_8
    iget-object v0, p0, Lmve;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 10167
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10108
    invoke-virtual {p0, p1}, Lmve;->a(Loxn;)Lmve;

    move-result-object v0

    return-object v0
.end method
