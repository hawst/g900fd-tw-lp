.class public final Lmwm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmwm;


# instance fields
.field private b:I

.field private c:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4671
    const/4 v0, 0x0

    new-array v0, v0, [Lmwm;

    sput-object v0, Lmwm;->a:[Lmwm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4672
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4675
    const/high16 v0, -0x80000000

    iput v0, p0, Lmwm;->b:I

    .line 4672
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4694
    const/4 v0, 0x0

    .line 4695
    iget v1, p0, Lmwm;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 4696
    const/4 v0, 0x1

    iget v1, p0, Lmwm;->b:I

    .line 4697
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4699
    :cond_0
    iget-object v1, p0, Lmwm;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 4700
    const/4 v1, 0x2

    iget-object v2, p0, Lmwm;->c:Ljava/lang/Boolean;

    .line 4701
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4703
    :cond_1
    iget-object v1, p0, Lmwm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4704
    iput v0, p0, Lmwm;->ai:I

    .line 4705
    return v0
.end method

.method public a(Loxn;)Lmwm;
    .locals 2

    .prologue
    .line 4713
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4714
    sparse-switch v0, :sswitch_data_0

    .line 4718
    iget-object v1, p0, Lmwm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4719
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwm;->ah:Ljava/util/List;

    .line 4722
    :cond_1
    iget-object v1, p0, Lmwm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4724
    :sswitch_0
    return-object p0

    .line 4729
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 4730
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 4740
    :cond_2
    iput v0, p0, Lmwm;->b:I

    goto :goto_0

    .line 4742
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmwm;->b:I

    goto :goto_0

    .line 4747
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwm;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 4714
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4682
    iget v0, p0, Lmwm;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 4683
    const/4 v0, 0x1

    iget v1, p0, Lmwm;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4685
    :cond_0
    iget-object v0, p0, Lmwm;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 4686
    const/4 v0, 0x2

    iget-object v1, p0, Lmwm;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 4688
    :cond_1
    iget-object v0, p0, Lmwm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4690
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4668
    invoke-virtual {p0, p1}, Lmwm;->a(Loxn;)Lmwm;

    move-result-object v0

    return-object v0
.end method
