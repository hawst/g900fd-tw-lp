.class public final Llxj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxj;


# instance fields
.field private b:Llxp;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5212
    const/4 v0, 0x0

    new-array v0, v0, [Llxj;

    sput-object v0, Llxj;->a:[Llxj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5213
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5225
    const/4 v0, 0x0

    iput-object v0, p0, Llxj;->b:Llxp;

    .line 5228
    const/high16 v0, -0x80000000

    iput v0, p0, Llxj;->c:I

    .line 5213
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5245
    const/4 v0, 0x0

    .line 5246
    iget-object v1, p0, Llxj;->b:Llxp;

    if-eqz v1, :cond_0

    .line 5247
    const/4 v0, 0x1

    iget-object v1, p0, Llxj;->b:Llxp;

    .line 5248
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5250
    :cond_0
    iget v1, p0, Llxj;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 5251
    const/4 v1, 0x4

    iget v2, p0, Llxj;->c:I

    .line 5252
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5254
    :cond_1
    iget-object v1, p0, Llxj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5255
    iput v0, p0, Llxj;->ai:I

    .line 5256
    return v0
.end method

.method public a(Loxn;)Llxj;
    .locals 2

    .prologue
    .line 5264
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5265
    sparse-switch v0, :sswitch_data_0

    .line 5269
    iget-object v1, p0, Llxj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5270
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxj;->ah:Ljava/util/List;

    .line 5273
    :cond_1
    iget-object v1, p0, Llxj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5275
    :sswitch_0
    return-object p0

    .line 5280
    :sswitch_1
    iget-object v0, p0, Llxj;->b:Llxp;

    if-nez v0, :cond_2

    .line 5281
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxj;->b:Llxp;

    .line 5283
    :cond_2
    iget-object v0, p0, Llxj;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5287
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5288
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 5294
    :cond_3
    iput v0, p0, Llxj;->c:I

    goto :goto_0

    .line 5296
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Llxj;->c:I

    goto :goto_0

    .line 5265
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5233
    iget-object v0, p0, Llxj;->b:Llxp;

    if-eqz v0, :cond_0

    .line 5234
    const/4 v0, 0x1

    iget-object v1, p0, Llxj;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5236
    :cond_0
    iget v0, p0, Llxj;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 5237
    const/4 v0, 0x4

    iget v1, p0, Llxj;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5239
    :cond_1
    iget-object v0, p0, Llxj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5241
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5209
    invoke-virtual {p0, p1}, Llxj;->a(Loxn;)Llxj;

    move-result-object v0

    return-object v0
.end method
