.class public final Lkrq;
.super Lvq;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/socialcast/impl/CastService;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V
    .locals 0

    .prologue
    .line 543
    iput-object p1, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {p0}, Lvq;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lvp;Lvy;)V
    .locals 2

    .prologue
    .line 549
    iget-object v0, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Liic;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->b(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 550
    invoke-virtual {p2}, Lvy;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->b(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 553
    iget-object v0, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0, p2}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->a(Lcom/google/android/libraries/social/socialcast/impl/CastService;Lvy;)V

    .line 558
    iget-object v0, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->c(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    .line 560
    :cond_0
    return-void
.end method

.method public c(Lvp;Lvy;)V
    .locals 4

    .prologue
    .line 567
    const-string v0, "CastService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onRouteSelected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 571
    :cond_0
    invoke-virtual {p2}, Lvy;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->b(Lcom/google/android/libraries/social/socialcast/impl/CastService;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 586
    :goto_0
    return-void

    .line 582
    :cond_1
    iget-object v0, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->c(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    .line 585
    iget-object v0, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    const-class v3, Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public d(Lvp;Lvy;)V
    .locals 3

    .prologue
    .line 594
    const-string v0, "CastService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x13

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onRouteUnselected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 597
    :cond_0
    iget-object v0, p0, Lkrq;->a:Lcom/google/android/libraries/social/socialcast/impl/CastService;

    invoke-static {v0}, Lcom/google/android/libraries/social/socialcast/impl/CastService;->d(Lcom/google/android/libraries/social/socialcast/impl/CastService;)V

    .line 598
    return-void
.end method
