.class public final Lkwj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;
.implements Lkvr;


# instance fields
.field private final a:I

.field private final b:Lhoc;

.field private c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lkwk;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/content/Context;

.field private e:Lu;

.field private f:Lkvq;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lu;ILkvq;)V
    .locals 4

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lkwj;->d:Landroid/content/Context;

    .line 52
    iput p3, p0, Lkwj;->a:I

    .line 53
    iput-object p2, p0, Lkwj;->e:Lu;

    .line 54
    const-class v0, Lhoc;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lkwj;->b:Lhoc;

    .line 55
    iget-object v0, p0, Lkwj;->b:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 56
    iput-object p4, p0, Lkwj;->f:Lkvq;

    .line 58
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lkwj;->c:Landroid/util/SparseArray;

    .line 59
    const-class v0, Lkwk;

    invoke-static {p1, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkwk;

    .line 60
    iget-object v2, p0, Lkwj;->c:Landroid/util/SparseArray;

    invoke-interface {v0}, Lkwk;->a()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 62
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lhos;)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lkwj;->c:Landroid/util/SparseArray;

    iget v1, p0, Lkwj;->g:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkwk;

    .line 100
    if-eqz v0, :cond_0

    .line 101
    iget-object v1, p0, Lkwj;->e:Lu;

    invoke-virtual {v1}, Lu;->q()Lae;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lkwk;->a(Lhos;Lae;)V

    .line 103
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 77
    invoke-static {p2}, Lkvu;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Action is not a join action"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    invoke-virtual {p0}, Lkwj;->a()[I

    move-result-object v4

    .line 81
    new-instance v0, Lkwc;

    iget-object v1, p0, Lkwj;->d:Landroid/content/Context;

    iget v2, p0, Lkwj;->a:I

    const-string v3, "squares_promo_task"

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lkwc;-><init>(Landroid/content/Context;ILjava/lang/String;[ILjava/lang/String;)V

    .line 83
    iget-object v1, p0, Lkwj;->b:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 84
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 89
    const-string v0, "squares_promo_task"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 90
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "squares_promo_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lkwj;->g:I

    .line 92
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_id_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lkwj;->f:Lkvq;

    invoke-virtual {v1, v0}, Lkvq;->b(Ljava/lang/String;)V

    .line 95
    :cond_0
    return-void
.end method

.method public a()[I
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lkwj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v1, v0, [I

    .line 66
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 67
    iget-object v2, p0, Lkwj;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    aput v2, v1, v0

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_0
    return-object v1
.end method
