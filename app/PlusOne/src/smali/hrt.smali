.class public Lhrt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:I


# instance fields
.field private final b:Lhrw;

.field private final c:Lhru;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const v0, 0x7f100054

    sput v0, Lhrt;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const-class v0, Lhrw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrw;

    iput-object v0, p0, Lhrt;->b:Lhrw;

    .line 47
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 48
    const-class v0, Lhru;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhru;

    iput-object v0, p0, Lhrt;->c:Lhru;

    .line 49
    return-void
.end method

.method private a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 12

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 111
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 112
    if-eqz p3, :cond_5

    const/4 v2, 0x1

    .line 113
    :goto_0
    invoke-static {p1}, Lhqd;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 114
    invoke-static {v7, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    .line 116
    if-eqz v8, :cond_0

    .line 117
    invoke-static {p1}, Lhqd;->f(Landroid/content/Context;)J

    move-result-wide v4

    .line 119
    const-wide/16 v10, -0x1

    cmp-long v9, v4, v10

    if-eqz v9, :cond_0

    move-wide/from16 p6, v4

    .line 124
    :cond_0
    if-eqz v2, :cond_1

    .line 125
    const-string v4, "auto_upload_account_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 126
    const-string v4, "auto_upload_account_type"

    const-string v5, "com.google"

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v4, "auto_upload_enabled"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 131
    :cond_1
    const-string v4, "instant_share_eventid"

    invoke-virtual {v6, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v4, "instant_share_account_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 133
    const-string v4, "instant_share_starttime"

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 135
    const-wide/32 v4, 0x1b77400

    add-long v4, v4, p6

    move-wide/from16 v0, p8

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 138
    new-instance v9, Landroid/content/Intent;

    const-string v10, "action_instant_share_end_time_changed"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v10, "account_id"

    invoke-virtual {v9, v10, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v10, "event_id"

    invoke-virtual {v9, v10, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v10, "end_time"

    invoke-virtual {v9, v10, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p1}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v10

    invoke-virtual {v10, v9}, Ldr;->b(Landroid/content/Intent;)V

    .line 140
    const-string v9, "instant_share_endtime"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v6, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 141
    invoke-static {p1}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v9, 0x0

    invoke-virtual {v3, v4, v6, v5, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 143
    if-eqz v2, :cond_6

    .line 144
    if-nez v8, :cond_2

    .line 145
    iget-object v2, p0, Lhrt;->b:Lhrw;

    .line 146
    move-object/from16 v0, p4

    invoke-interface {v2, p1, p2, p3, v0}, Lhrw;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 147
    move-object/from16 v0, p5

    invoke-virtual {p0, p1, v0, v2}, Lhrt;->a(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 152
    :cond_2
    :goto_1
    invoke-static {p1}, Lhqd;->i(Landroid/content/Context;)V

    .line 154
    invoke-static {p3, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 156
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 157
    iget-object v2, p0, Lhrt;->c:Lhru;

    invoke-interface {v2, p1, p2}, Lhru;->b(Landroid/content/Context;I)V

    .line 159
    :cond_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 160
    iget-object v2, p0, Lhrt;->c:Lhru;

    invoke-interface {v2, p1, p2}, Lhru;->a(Landroid/content/Context;I)V

    .line 163
    :cond_4
    return-void

    .line 112
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 150
    :cond_6
    invoke-virtual {p0, p1}, Lhrt;->a(Landroid/content/Context;)V

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 182
    const-string v0, "notification"

    .line 183
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 184
    const-string v1, "InstantShare"

    const v2, 0x7f100054

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 186
    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 10

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    .line 55
    const-string v0, "InstantShare"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "#disableInstantShare; now: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, v3

    move-object v5, v3

    move-wide v8, v6

    .line 59
    invoke-direct/range {v0 .. v9}, Lhrt;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 61
    return-void
.end method

.method public a(Landroid/content/Context;IZLhrv;)V
    .locals 12

    .prologue
    .line 69
    const-string v0, "InstantShare"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-string v0, "#enableInstantShare; event: "

    invoke-interface/range {p4 .. p4}, Lhrv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 73
    :cond_0
    :goto_0
    const-string v0, "alarm"

    .line 74
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/app/AlarmManager;

    .line 75
    iget-object v0, p0, Lhrt;->b:Lhrw;

    .line 76
    invoke-interface/range {p4 .. p4}, Lhrv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lhrw;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v11

    .line 77
    invoke-interface/range {p4 .. p4}, Lhrv;->d()J

    move-result-wide v8

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 80
    invoke-virtual {v10, v11}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 81
    if-eqz p3, :cond_3

    const-wide/16 v0, 0x1388

    add-long/2addr v0, v6

    cmp-long v0, v0, v8

    if-gez v0, :cond_3

    .line 82
    const-string v0, "InstantShare"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    sub-long v0, v8, v6

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x72

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "#enableInstantShare; start IS; now: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", end: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", wake in: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 87
    :cond_1
    invoke-interface/range {p4 .. p4}, Lhrv;->a()Ljava/lang/String;

    move-result-object v3

    .line 88
    invoke-interface/range {p4 .. p4}, Lhrv;->c()Ljava/lang/String;

    move-result-object v4

    .line 89
    invoke-interface/range {p4 .. p4}, Lhrv;->b()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    .line 87
    invoke-direct/range {v0 .. v9}, Lhrt;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 91
    const/4 v0, 0x0

    invoke-virtual {v10, v0, v8, v9, v11}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 100
    :goto_1
    return-void

    .line 70
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_3
    const-string v0, "InstantShare"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x55

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "#enableInstantShare; event over; now: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", end: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 98
    :cond_4
    invoke-virtual {p0, p1, p2}, Lhrt;->a(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method public a(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lhrt;->a(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;Z)V

    .line 198
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;Z)V
    .locals 6

    .prologue
    .line 210
    const v0, 0x7f0a0433

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 211
    if-eqz p4, :cond_0

    move-object v0, v1

    .line 213
    :goto_0
    new-instance v2, Lbs;

    invoke-direct {v2, p1}, Lbs;-><init>(Landroid/content/Context;)V

    .line 214
    const v3, 0x7f020364

    invoke-virtual {v2, v3}, Lbs;->a(I)Lbs;

    .line 215
    invoke-virtual {v2, v0}, Lbs;->e(Ljava/lang/CharSequence;)Lbs;

    .line 216
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lbs;->a(J)Lbs;

    .line 217
    invoke-virtual {v2, p2}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    .line 218
    invoke-virtual {v2, v1}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 219
    invoke-virtual {v2, p3}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    .line 220
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lbs;->a(Z)Lbs;

    .line 222
    const-string v0, "notification"

    .line 223
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 224
    const-string v1, "InstantShare"

    const v3, 0x7f100054

    .line 225
    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    .line 224
    invoke-virtual {v0, v1, v3, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 226
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
