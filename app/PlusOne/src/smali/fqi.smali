.class public final Lfqi;
.super Lepp;
.source "PG"


# instance fields
.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILept;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lepp;-><init>(Landroid/content/Context;ILept;)V

    .line 27
    return-void
.end method

.method static synthetic a(Lfqi;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lfqi;->d:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic a(Lfqi;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lfqi;->d:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Lfqi;Lfhh;)V
    .locals 0

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lfqi;->a(Lfhh;)V

    return-void
.end method

.method public static a(Landroid/content/Context;ILnyq;)Z
    .locals 4

    .prologue
    .line 35
    sget-object v0, Lepn;->g:Lepn;

    .line 36
    invoke-static {p0, p1, v0}, Ldhv;->a(Landroid/content/Context;ILepn;)Ldwi;

    move-result-object v0

    .line 37
    if-eqz p2, :cond_0

    iget-object v1, p2, Lnyq;->m:Lnyu;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lnyq;->m:Lnyu;

    iget-object v1, v1, Lnyu;->b:Ljava/lang/Boolean;

    .line 39
    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    invoke-virtual {v0}, Ldwi;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 45
    invoke-virtual {p0}, Lfqi;->f()V

    .line 46
    iget-object v0, p0, Lfqi;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    new-instance v0, Lnyq;

    invoke-direct {v0}, Lnyq;-><init>()V

    new-instance v1, Lnyu;

    invoke-direct {v1}, Lnyu;-><init>()V

    iput-object v1, v0, Lnyq;->m:Lnyu;

    iget-object v1, v0, Lnyq;->m:Lnyu;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnyu;->b:Ljava/lang/Boolean;

    new-instance v1, Lfqj;

    invoke-direct {v1, p0}, Lfqj;-><init>(Lfqi;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    iget-object v1, p0, Lfqi;->c:Landroid/content/Context;

    iget v2, p0, Lfqi;->b:I

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILnyq;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lfqi;->d:Ljava/lang/Integer;

    .line 47
    :cond_0
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 60
    const v0, 0x7f0a01e8

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfqi;->c:Landroid/content/Context;

    const-string v4, "ulr_googlelocation"

    .line 61
    const-string v5, "https://www.google.com/support/mobile/?hl=%locale%"

    invoke-static {v3, v4, v5}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 60
    invoke-virtual {p0, p1, v0, v1}, Lfqi;->a(Landroid/view/View;I[Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.method public a(Lnyq;)Z
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lfqi;->c:Landroid/content/Context;

    iget v1, p0, Lfqi;->b:I

    invoke-static {v0, v1, p1}, Lfqi;->a(Landroid/content/Context;ILnyq;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 55
    const v0, 0x7f04020f

    return v0
.end method

.method public e()Lepn;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lepn;->g:Lepn;

    return-object v0
.end method
