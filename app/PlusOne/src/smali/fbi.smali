.class final Lfbi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Ligy;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Ligv;

.field private c:Livx;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput p1, p0, Lfbi;->d:I

    .line 73
    iput-object p2, p0, Lfbi;->e:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lfbi;->f:Ljava/lang/String;

    .line 75
    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    .line 87
    iget-object v0, p0, Lfbi;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lfbi;->c:Livx;

    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 90
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 89
    invoke-virtual {v2, v0}, Liwg;->a(I)Liwg;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Liwg;->d()Liwg;

    move-result-object v0

    const-class v2, Liwl;

    .line 92
    invoke-virtual {v0, v2}, Liwg;->b(Ljava/lang/Class;)Liwg;

    move-result-object v0

    .line 88
    invoke-virtual {v1, v0}, Livx;->a(Liwg;)V

    .line 93
    return-void
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 1

    .prologue
    .line 80
    iput-object p1, p0, Lfbi;->a:Landroid/app/Activity;

    .line 81
    iput-object p3, p0, Lfbi;->b:Ligv;

    .line 82
    invoke-virtual {p4, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lfbi;->c:Livx;

    .line 83
    return-void
.end method

.method public a(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 99
    const/4 v0, -0x1

    if-eq p5, v0, :cond_1

    .line 100
    iget-object v1, p0, Lfbi;->b:Ligv;

    iget v0, p0, Lfbi;->d:I

    if-ne v0, v5, :cond_0

    iget-object v0, p0, Lfbi;->a:Landroid/app/Activity;

    iget-object v2, p0, Lfbi;->f:Ljava/lang/String;

    iget-object v3, p0, Lfbi;->e:Ljava/lang/String;

    invoke-static {v0, p5, v2, v3, v4}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "disable_up_button"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Ligv;->a(Landroid/content/Intent;)V

    .line 104
    :goto_1
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lfbi;->a:Landroid/app/Activity;

    iget-object v2, p0, Lfbi;->f:Ljava/lang/String;

    iget-object v3, p0, Lfbi;->e:Ljava/lang/String;

    invoke-static {v0, p5, v2, v3, v4}, Leyq;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "disable_up_button"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 102
    :cond_1
    iget-object v0, p0, Lfbi;->b:Ligv;

    invoke-interface {v0, v4}, Ligv;->a(I)V

    goto :goto_1
.end method
