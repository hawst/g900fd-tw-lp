.class public Lloj;
.super Llrm;
.source "PG"

# interfaces
.implements Llnk;


# instance fields
.field public final N:Llnl;

.field public final O:Llnh;

.field private Q:Llqw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Llrm;-><init>()V

    .line 22
    new-instance v0, Llnl;

    invoke-direct {v0}, Llnl;-><init>()V

    iput-object v0, p0, Lloj;->N:Llnl;

    .line 23
    iget-object v0, p0, Lloj;->N:Llnl;

    invoke-virtual {v0}, Llnl;->h_()Llnh;

    move-result-object v0

    iput-object v0, p0, Lloj;->O:Llnh;

    return-void
.end method

.method static synthetic a(Lloj;)Llqm;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lloj;->P:Llqm;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lloj;->P:Llqm;

    iget-object v1, p0, Lloj;->Q:Llqw;

    invoke-virtual {v0, v1}, Llqm;->b(Llqw;)V

    .line 62
    invoke-super {p0}, Llrm;->A()V

    .line 63
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lloj;->r()Lu;

    move-result-object v0

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Lu;)Llnh;

    move-result-object v0

    .line 34
    iget-object v1, p0, Lloj;->N:Llnl;

    invoke-virtual {v1, p1}, Llnl;->a(Landroid/content/Context;)V

    .line 35
    iget-object v1, p0, Lloj;->N:Llnl;

    invoke-virtual {v1, v0}, Llnl;->a(Llnh;)V

    .line 36
    iget-object v0, p0, Lloj;->O:Llnh;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/String;)V

    .line 37
    invoke-super {p0, p1}, Llrm;->a(Landroid/app/Activity;)V

    .line 38
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 43
    iget-object v0, p0, Lloj;->O:Llnh;

    const-class v1, Llos;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    iget-object v1, p0, Lloj;->P:Llqm;

    iget-object v1, p0, Lloj;->O:Llnh;

    goto :goto_0

    .line 45
    :cond_0
    iget-object v0, p0, Lloj;->O:Llnh;

    invoke-virtual {v0}, Llnh;->a()V

    .line 46
    iget-object v0, p0, Lloj;->P:Llqm;

    new-instance v1, Llok;

    invoke-direct {v1, p0, p1}, Llok;-><init>(Lloj;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Llqm;->a(Llqw;)Llqw;

    move-result-object v0

    iput-object v0, p0, Lloj;->Q:Llqw;

    .line 56
    invoke-super {p0, p1}, Llrm;->a(Landroid/os/Bundle;)V

    .line 57
    return-void
.end method

.method public a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 67
    invoke-super {p0, p1}, Llrm;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    .line 68
    iget-object v0, p0, Lloj;->N:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public h_()Llnh;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lloj;->O:Llnh;

    return-object v0
.end method

.method public k(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lloj;->O:Llnh;

    new-instance v1, Llor;

    iget-object v2, p0, Lloj;->P:Llqm;

    invoke-direct {v1, p0, v2}, Llor;-><init>(Lu;Llqr;)V

    invoke-virtual {v0, v1}, Llnh;->a(Llnq;)Llnh;

    .line 73
    return-void
.end method
