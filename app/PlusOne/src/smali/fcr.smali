.class final Lfcr;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I


# direct methods
.method constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 25
    const-string v0, "ProfileNameEditBackgroundTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Lfcr;->a:Landroid/content/Context;

    .line 27
    iput p2, p0, Lfcr;->b:I

    .line 28
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 4

    .prologue
    .line 31
    iget-object v0, p0, Lfcr;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Lfcr;->b:I

    .line 32
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    invoke-static {v0}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lfcr;->a:Landroid/content/Context;

    iget v2, p0, Lfcr;->b:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;Z)Ldsx;

    move-result-object v0

    .line 38
    iget-object v0, v0, Ldsx;->h:Lnjt;

    invoke-static {v0}, Ldsm;->a(Lnjt;)[B

    move-result-object v0

    .line 40
    new-instance v1, Lhoz;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lhoz;-><init>(Z)V

    .line 41
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    .line 42
    const-string v3, "name_edit_info_bytes"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 43
    return-object v1
.end method
