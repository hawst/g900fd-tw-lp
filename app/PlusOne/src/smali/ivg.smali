.class public Livg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Livc;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/Object;

.field private final c:Ljava/io/File;

.field private final d:I

.field private final e:I

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:Livj;

.field private h:Ljava/io/PrintWriter;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Livg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Livg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;IILjava/util/concurrent/Executor;)V
    .locals 6

    .prologue
    .line 54
    new-instance v5, Livk;

    invoke-direct {v5}, Livk;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Livg;-><init>(Ljava/io/File;IILjava/util/concurrent/Executor;Livj;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/io/File;IILjava/util/concurrent/Executor;Livj;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Livg;->b:Ljava/lang/Object;

    .line 59
    iput-object p1, p0, Livg;->c:Ljava/io/File;

    .line 60
    iput p2, p0, Livg;->d:I

    .line 61
    add-int/lit8 v0, p3, -0x1

    iput v0, p0, Livg;->e:I

    .line 62
    iput-object p4, p0, Livg;->f:Ljava/util/concurrent/Executor;

    .line 63
    iput-object p5, p0, Livg;->g:Livj;

    .line 64
    return-void
.end method

.method static a(Ljava/io/File;)I
    .locals 4

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    const/4 v2, 0x0

    .line 154
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, p0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    if-eqz v2, :cond_0

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 159
    :cond_0
    invoke-static {v1}, Llrz;->a(Ljava/io/Closeable;)V

    .line 161
    return v0

    .line 159
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    invoke-static {v1}, Llrz;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Livg;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 20
    const-string v0, "%s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "yyyy-MM-dd kk:mm:ss"

    iget-object v4, p0, Livg;->g:Livj;

    invoke-interface {v4}, Livj;->a()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Livg;)V
    .locals 3

    .prologue
    .line 20
    iget-object v1, p0, Livg;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Livg;->h:Ljava/io/PrintWriter;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Livg;->i:I

    iget-object v0, p0, Livg;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Livg;->c:Ljava/io/File;

    invoke-static {v0}, Livg;->a(Ljava/io/File;)I

    move-result v0

    iput v0, p0, Livg;->i:I

    iget v0, p0, Livg;->i:I

    iget v2, p0, Livg;->d:I

    if-lt v0, v2, :cond_0

    invoke-direct {p0}, Livg;->c()V

    const/4 v0, 0x0

    iput v0, p0, Livg;->i:I

    :cond_0
    invoke-direct {p0}, Livg;->d()Ljava/io/PrintWriter;

    move-result-object v0

    iput-object v0, p0, Livg;->h:Ljava/io/PrintWriter;

    :cond_1
    iget v0, p0, Livg;->i:I

    iget v2, p0, Livg;->d:I

    if-lt v0, v2, :cond_2

    iget-object v0, p0, Livg;->h:Ljava/io/PrintWriter;

    invoke-static {v0}, Llrz;->a(Ljava/io/Closeable;)V

    invoke-direct {p0}, Livg;->c()V

    const/4 v0, 0x0

    iput v0, p0, Livg;->i:I

    invoke-direct {p0}, Livg;->d()Ljava/io/PrintWriter;

    move-result-object v0

    iput-object v0, p0, Livg;->h:Ljava/io/PrintWriter;

    :cond_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Livg;)Ljava/io/PrintWriter;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Livg;->h:Ljava/io/PrintWriter;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Livg;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Livg;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Livg;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Livg;->i:I

    return v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 118
    invoke-virtual {p0}, Livg;->a()I

    move-result v1

    .line 119
    iget v0, p0, Livg;->e:I

    if-ne v1, v0, :cond_1

    .line 122
    const/4 v0, 0x1

    :goto_0
    iget v2, p0, Livg;->e:I

    if-ge v0, v2, :cond_0

    .line 123
    add-int/lit8 v2, v0, -0x1

    invoke-virtual {p0, v2}, Livg;->a(I)Ljava/io/File;

    move-result-object v2

    .line 124
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 125
    invoke-virtual {p0, v0}, Livg;->a(I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_0
    add-int/lit8 v0, v1, -0x1

    .line 130
    :goto_1
    invoke-virtual {p0, v0}, Livg;->a(I)Ljava/io/File;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 132
    iget-object v1, p0, Livg;->c:Ljava/io/File;

    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 133
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private d()Ljava/io/PrintWriter;
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Livg;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Livg;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 138
    iget-object v0, p0, Livg;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 140
    :cond_0
    new-instance v0, Ljava/io/PrintWriter;

    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Livg;->c:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method


# virtual methods
.method a()I
    .locals 2

    .prologue
    .line 165
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Livg;->e:I

    if-ge v0, v1, :cond_1

    .line 166
    invoke-virtual {p0, v0}, Livg;->a(I)Ljava/io/File;

    move-result-object v1

    .line 167
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 172
    :goto_1
    return v0

    .line 165
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_1
    iget v0, p0, Livg;->e:I

    goto :goto_1
.end method

.method a(I)Ljava/io/File;
    .locals 2

    .prologue
    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    iget-object v1, p0, Livg;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 179
    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 180
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Livg;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Livh;

    invoke-direct {v1, p0, p1}, Livh;-><init>(Livg;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 82
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Livg;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Livi;

    invoke-direct {v1, p0}, Livi;-><init>(Livg;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 92
    return-void
.end method
