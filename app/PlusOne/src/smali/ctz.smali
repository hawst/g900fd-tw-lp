.class public Lctz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljlw;
.implements Llnx;
.implements Llqz;
.implements Llrd;
.implements Llrg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljlw",
        "<",
        "Lctz;",
        ">;",
        "Llnx;",
        "Llqz;",
        "Llrd;",
        "Llrg;"
    }
.end annotation


# instance fields
.field private a:Lhjf;

.field private final b:Ljlx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlx",
            "<",
            "Lctz;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/os/Bundle;

.field private d:Ljcn;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Ljlv;

    invoke-direct {v0, p0}, Ljlv;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lctz;->b:Ljlx;

    .line 83
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lctz;->c:Landroid/os/Bundle;

    .line 85
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 86
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lctz;->a:Lhjf;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lctz;->a:Lhjf;

    invoke-interface {v0}, Lhjf;->b()V

    .line 174
    :cond_0
    iget-object v0, p0, Lctz;->b:Ljlx;

    invoke-interface {v0}, Ljlx;->a()V

    .line 175
    return-void
.end method


# virtual methods
.method public a()Ljcn;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lctz;->d:Ljcn;

    return-object v0
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 90
    const-class v0, Lhjf;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjf;

    iput-object v0, p0, Lctz;->a:Lhjf;

    .line 91
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 95
    if-eqz p1, :cond_1

    .line 96
    const-string v0, "com.google.android.apps.photos.selection.SelectionModel.MediaSelection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljcn;

    .line 97
    if-eqz v0, :cond_0

    .line 98
    iput-object v0, p0, Lctz;->d:Ljcn;

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lctz;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p0, Lctz;->c:Landroid/os/Bundle;

    const-string v1, "photo_picker_selected"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljcn;

    iput-object v0, p0, Lctz;->d:Ljcn;

    .line 105
    :cond_2
    iget-object v0, p0, Lctz;->d:Ljcn;

    if-nez v0, :cond_0

    .line 106
    new-instance v0, Ljcn;

    invoke-direct {v0}, Ljcn;-><init>()V

    iput-object v0, p0, Lctz;->d:Ljcn;

    goto :goto_0
.end method

.method a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljcl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcl;

    .line 150
    iget-object v2, p0, Lctz;->d:Ljcn;

    invoke-virtual {v2, v0}, Ljcn;->a(Ljcl;)V

    goto :goto_0

    .line 153
    :cond_0
    invoke-direct {p0}, Lctz;->d()V

    .line 154
    return-void
.end method

.method a(Ljcn;)V
    .locals 2

    .prologue
    .line 140
    if-nez p1, :cond_0

    .line 141
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Cannot set a null media selection"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    iput-object p1, p0, Lctz;->d:Ljcn;

    .line 145
    invoke-direct {p0}, Lctz;->d()V

    .line 146
    return-void
.end method

.method public a(Ljcl;)Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lctz;->d:Ljcn;

    invoke-virtual {v0, p1}, Ljcn;->c(Ljcl;)Z

    move-result v0

    return v0
.end method

.method public b()Ljlx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljlx",
            "<",
            "Lctz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lctz;->b:Ljlx;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    const-string v0, "com.google.android.apps.photos.selection.SelectionModel.MediaSelection"

    iget-object v1, p0, Lctz;->d:Ljcn;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 114
    return-void
.end method

.method b(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Ljcl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcl;

    .line 158
    iget-object v2, p0, Lctz;->d:Ljcn;

    invoke-virtual {v2, v0}, Ljcn;->b(Ljcl;)Z

    goto :goto_0

    .line 161
    :cond_0
    invoke-direct {p0}, Lctz;->d()V

    .line 162
    return-void
.end method

.method b(Ljcl;)V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lctz;->d:Ljcn;

    invoke-virtual {v0, p1}, Ljcn;->a(Ljcl;)V

    .line 167
    invoke-direct {p0}, Lctz;->d()V

    .line 168
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lctz;->d:Ljcn;

    invoke-virtual {v0}, Ljcn;->j()V

    .line 136
    invoke-direct {p0}, Lctz;->d()V

    .line 137
    return-void
.end method
