.class public final Laph;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lapp;


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lbco;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lae;

.field private final d:Lcdu;

.field private final e:Los;

.field private final f:Lag;

.field private final g:Lcgn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgn",
            "<",
            "Lu;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lavx;

.field private i:Lapq;

.field private j:Z

.field private k:Z

.field private l:Landroid/view/MenuItem;

.field private m:Landroid/view/MenuItem;

.field private n:Landroid/view/MenuItem;

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Laph;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laph;->a:Ljava/lang/String;

    .line 67
    const-class v0, Lbco;

    sput-object v0, Laph;->b:Ljava/lang/Class;

    return-void
.end method

.method private constructor <init>(Los;Lae;Lcdu;Lavx;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Lapi;

    invoke-direct {v0, p0}, Lapi;-><init>(Laph;)V

    iput-object v0, p0, Laph;->f:Lag;

    .line 118
    new-instance v0, Lcgn;

    new-instance v1, Lapj;

    invoke-direct {v1}, Lapj;-><init>()V

    invoke-direct {v0, v1}, Lcgn;-><init>(Lcgo;)V

    iput-object v0, p0, Laph;->g:Lcgn;

    .line 140
    iput-boolean v3, p0, Laph;->j:Z

    .line 147
    iput-boolean v3, p0, Laph;->k:Z

    .line 177
    const-string v0, "activity"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Los;

    iput-object v0, p0, Laph;->e:Los;

    .line 178
    const-string v0, "fragmentManager"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lae;

    iput-object v0, p0, Laph;->c:Lae;

    .line 179
    const-string v0, "analyticsSession"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Laph;->d:Lcdu;

    .line 180
    const-string v0, "displayState"

    invoke-static {p4, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavx;

    iput-object v0, p0, Laph;->h:Lavx;

    .line 181
    iget-object v0, p0, Laph;->h:Lavx;

    new-instance v1, Lapk;

    invoke-direct {v1, p0}, Lapk;-><init>(Laph;)V

    invoke-interface {v0, v1}, Lavx;->m(Lawk;)V

    .line 188
    iget-object v0, p0, Laph;->c:Lae;

    iget-object v1, p0, Laph;->f:Lag;

    invoke-virtual {v0, v1}, Lae;->a(Lag;)V

    .line 189
    return-void
.end method

.method public static a(Los;Lae;Lcdu;Lavx;)Laph;
    .locals 1

    .prologue
    .line 166
    new-instance v0, Laph;

    invoke-direct {v0, p0, p1, p2, p3}, Laph;-><init>(Los;Lae;Lcdu;Lavx;)V

    .line 168
    invoke-direct {v0}, Laph;->t()V

    .line 169
    return-object v0
.end method

.method static a(Ljava/lang/Class;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lu;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 494
    sget-object v0, Laph;->b:Ljava/lang/Class;

    if-ne p0, v0, :cond_0

    .line 495
    const-string v0, "mm_first_fragment"

    .line 499
    :goto_0
    return-object v0

    .line 497
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "#"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 499
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Laph;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Laph;->t()V

    return-void
.end method

.method static synthetic a(Laph;Z)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Laph;->b(Z)V

    return-void
.end method

.method private a(Lt;)V
    .locals 2

    .prologue
    .line 481
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Laph;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 482
    iget-object v1, p0, Laph;->c:Lae;

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    invoke-virtual {v1, v0}, Lat;->a(Ljava/lang/String;)Lat;

    move-result-object v1

    .line 483
    invoke-virtual {p1, v1, v0}, Lt;->a(Lat;Ljava/lang/String;)I

    .line 484
    return-void
.end method

.method private a(Lu;II)V
    .locals 3

    .prologue
    .line 470
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Laph;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    .line 471
    iget-object v1, p0, Laph;->c:Lae;

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const v2, 0x7f100143

    .line 472
    invoke-virtual {v1, v2, p1, v0}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    move-result-object v1

    .line 473
    invoke-virtual {v1, p2, p3}, Lat;->a(II)Lat;

    move-result-object v1

    .line 474
    invoke-virtual {v1, v0}, Lat;->a(Ljava/lang/String;)Lat;

    move-result-object v0

    .line 475
    invoke-virtual {v0}, Lat;->b()I

    .line 476
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 522
    iget-boolean v0, p0, Laph;->k:Z

    if-nez v0, :cond_0

    .line 523
    const/4 p1, 0x0

    .line 526
    :cond_0
    invoke-direct {p0}, Laph;->s()Lu;

    move-result-object v0

    .line 527
    instance-of v1, v0, Lcdw;

    if-eqz v1, :cond_2

    if-eqz p1, :cond_1

    .line 528
    invoke-virtual {v0}, Lu;->u()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 529
    :cond_1
    iget-object v1, p0, Laph;->d:Lcdu;

    check-cast v0, Lcdw;

    .line 530
    invoke-interface {v0}, Lcdw;->d()Lcdz;

    move-result-object v0

    .line 529
    invoke-virtual {v1, v0}, Lcdu;->a(Lcdz;)V

    .line 532
    :cond_2
    return-void
.end method

.method static synthetic b(Laph;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Laph;->j:Z

    return v0
.end method

.method static synthetic b(Laph;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Laph;->j:Z

    return p1
.end method

.method static synthetic c(Laph;)Lapq;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laph;->i:Lapq;

    return-object v0
.end method

.method private s()Lu;
    .locals 2

    .prologue
    .line 430
    const/4 v0, 0x0

    .line 431
    iget-object v1, p0, Laph;->c:Lae;

    invoke-virtual {v1}, Lae;->e()I

    move-result v1

    .line 432
    if-lez v1, :cond_0

    .line 433
    iget-object v0, p0, Laph;->c:Lae;

    add-int/lit8 v1, v1, -0x1

    .line 434
    invoke-virtual {v0, v1}, Lae;->b(I)Laf;

    move-result-object v0

    .line 435
    iget-object v1, p0, Laph;->c:Lae;

    invoke-interface {v0}, Laf;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    .line 438
    :cond_0
    if-nez v0, :cond_1

    .line 440
    iget-object v0, p0, Laph;->c:Lae;

    const-string v1, "mm_first_fragment"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    .line 443
    :cond_1
    if-nez v0, :cond_2

    .line 444
    sget-object v1, Laph;->a:Ljava/lang/String;

    .line 447
    :cond_2
    return-object v0
.end method

.method private t()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 535
    iget-object v0, p0, Laph;->c:Lae;

    invoke-virtual {v0}, Lae;->e()I

    move-result v2

    .line 547
    iget-object v0, p0, Laph;->g:Lcgn;

    invoke-virtual {v0}, Lcgn;->a()Ljava/util/Set;

    move-result-object v3

    .line 548
    invoke-interface {v3}, Ljava/util/Set;->clear()V

    move v0, v1

    .line 549
    :goto_0
    if-ge v0, v2, :cond_1

    .line 550
    iget-object v4, p0, Laph;->c:Lae;

    invoke-virtual {v4, v0}, Lae;->b(I)Laf;

    move-result-object v4

    .line 551
    iget-object v5, p0, Laph;->c:Lae;

    invoke-interface {v4}, Laf;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v4

    .line 552
    if-eqz v4, :cond_0

    .line 553
    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 549
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 556
    :cond_1
    iget-object v0, p0, Laph;->g:Lcgn;

    invoke-virtual {v0}, Lcgn;->b()V

    .line 560
    iget-object v0, p0, Laph;->e:Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Laph;->e:Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    invoke-virtual {v0, v1}, Loo;->e(Z)V

    .line 563
    :cond_2
    invoke-direct {p0}, Laph;->s()Lu;

    move-result-object v0

    .line 564
    instance-of v1, v0, Lbfs;

    if-eqz v1, :cond_3

    .line 565
    check-cast v0, Lbfs;

    invoke-interface {v0}, Lbfs;->V()V

    .line 567
    :cond_3
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 318
    new-instance v0, Lbce;

    invoke-direct {v0}, Lbce;-><init>()V

    invoke-direct {p0, v0}, Laph;->a(Lt;)V

    .line 319
    return-void
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 328
    invoke-static {p1, p2}, Lbbt;->a(IZ)Lbbt;

    move-result-object v0

    invoke-direct {p0, v0}, Laph;->a(Lt;)V

    .line 329
    return-void
.end method

.method public a(Landroid/graphics/Rect;Lbmd;Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 290
    new-instance v0, Lbem;

    invoke-direct {v0}, Lbem;-><init>()V

    .line 291
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 292
    invoke-virtual {v0, p1, p2, p3}, Lbem;->a(Landroid/graphics/Rect;Lbmd;Landroid/graphics/RectF;)V

    .line 294
    :cond_0
    const/high16 v1, 0x7f100000

    const v2, 0x7f100001

    invoke-direct {p0, v0, v1, v2}, Laph;->a(Lu;II)V

    .line 297
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 383
    const v0, 0x7f1006eb

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Laph;->l:Landroid/view/MenuItem;

    .line 384
    const v0, 0x7f1006ea

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Laph;->m:Landroid/view/MenuItem;

    .line 385
    const v0, 0x7f1006ec

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Laph;->n:Landroid/view/MenuItem;

    .line 386
    const/4 v0, 0x1

    iput-boolean v0, p0, Laph;->o:Z

    .line 387
    return-void
.end method

.method public a(Lanm;)V
    .locals 1

    .prologue
    .line 323
    invoke-static {p1}, Lbcm;->b(Lanm;)Lbcm;

    move-result-object v0

    invoke-direct {p0, v0}, Laph;->a(Lt;)V

    .line 324
    return-void
.end method

.method public a(Lapq;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 193
    iget-object v0, p0, Laph;->i:Lapq;

    const-string v1, "mListener"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 194
    const-string v0, "listener"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapq;

    iput-object v0, p0, Laph;->i:Lapq;

    .line 195
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 254
    invoke-direct {p0}, Laph;->s()Lu;

    move-result-object v0

    .line 255
    instance-of v1, v0, Lbft;

    if-eqz v1, :cond_0

    .line 256
    check-cast v0, Lbft;

    invoke-interface {v0, p1}, Lbft;->a(Z)V

    .line 258
    :cond_0
    iget-object v0, p0, Laph;->c:Lae;

    invoke-virtual {v0}, Lae;->c()V

    .line 259
    return-void
.end method

.method public a([Latu;)V
    .locals 1

    .prologue
    .line 313
    invoke-static {p1}, Lbdw;->a([Latu;)Lbdw;

    move-result-object v0

    invoke-direct {p0, v0}, Laph;->a(Lt;)V

    .line 314
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 353
    new-instance v0, Lbch;

    invoke-direct {v0}, Lbch;-><init>()V

    invoke-direct {p0, v0}, Laph;->a(Lt;)V

    .line 354
    return-void
.end method

.method public b(Lapq;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 199
    const-string v0, "listener"

    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 200
    const-string v0, "listener"

    iget-object v1, p0, Laph;->i:Lapq;

    const-string v2, "detaching wrong listener"

    invoke-static {p1, v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 201
    iput-object v3, p0, Laph;->i:Lapq;

    .line 202
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 280
    new-instance v0, Lbco;

    invoke-direct {v0}, Lbco;-><init>()V

    iget-object v1, p0, Laph;->c:Lae;

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const v2, 0x7f100143

    const-string v3, "mm_first_fragment"

    invoke-virtual {v1, v2, v0, v3}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    move-result-object v0

    const/high16 v1, 0x7f100000

    const v2, 0x7f100001

    invoke-virtual {v0, v1, v2}, Lat;->a(II)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    iget-object v0, p0, Laph;->i:Lapq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laph;->i:Lapq;

    invoke-interface {v0}, Lapq;->a()V

    .line 283
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 348
    new-instance v0, Lbff;

    invoke-direct {v0}, Lbff;-><init>()V

    invoke-direct {p0, v0}, Laph;->a(Lt;)V

    .line 349
    return-void
.end method

.method public e()V
    .locals 4

    .prologue
    .line 308
    new-instance v0, Lbbv;

    invoke-direct {v0}, Lbbv;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Laph;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Laph;->c:Lae;

    invoke-virtual {v2}, Lae;->a()Lat;

    move-result-object v2

    const v3, 0x7f100143

    invoke-virtual {v2, v3, v0, v1}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    move-result-object v0

    invoke-virtual {v0, v1}, Lat;->a(Ljava/lang/String;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    .line 309
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 333
    invoke-static {}, Lbfm;->U()Lbfm;

    move-result-object v0

    invoke-direct {p0, v0}, Laph;->a(Lt;)V

    .line 334
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 338
    invoke-static {}, Lbck;->U()Lbck;

    move-result-object v0

    invoke-direct {p0, v0}, Laph;->a(Lt;)V

    .line 339
    return-void
.end method

.method public h()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 211
    invoke-direct {p0}, Laph;->s()Lu;

    move-result-object v0

    .line 212
    if-nez v0, :cond_0

    move v0, v1

    .line 228
    :goto_0
    return v0

    .line 217
    :cond_0
    instance-of v3, v0, Lbfq;

    if-eqz v3, :cond_1

    check-cast v0, Lbfq;

    .line 218
    invoke-interface {v0}, Lbfq;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 220
    goto :goto_0

    .line 223
    :cond_1
    invoke-virtual {p0}, Laph;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 225
    invoke-virtual {p0, v1}, Laph;->a(Z)V

    move v0, v2

    .line 226
    goto :goto_0

    :cond_2
    move v0, v1

    .line 228
    goto :goto_0
.end method

.method public i()V
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Laph;->g:Lcgn;

    invoke-virtual {v0}, Lcgn;->c()V

    .line 235
    iget-object v0, p0, Laph;->c:Lae;

    invoke-virtual {v0}, Lae;->e()I

    move-result v1

    .line 236
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 237
    iget-object v2, p0, Laph;->c:Lae;

    invoke-virtual {v2}, Lae;->c()V

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    :cond_0
    invoke-virtual {p0}, Laph;->j()V

    .line 241
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Laph;->c:Lae;

    invoke-virtual {v0}, Lae;->b()Z

    .line 250
    return-void
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Laph;->c:Lae;

    invoke-virtual {v0}, Lae;->e()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x0

    iput-boolean v0, p0, Laph;->j:Z

    .line 276
    return-void
.end method

.method public m()V
    .locals 3

    .prologue
    .line 301
    new-instance v0, Lbeb;

    invoke-direct {v0}, Lbeb;-><init>()V

    .line 302
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbeb;->c(Z)V

    .line 303
    const/4 v1, 0x0

    const v2, 0x7f100001

    invoke-direct {p0, v0, v1, v2}, Laph;->a(Lu;II)V

    .line 304
    return-void
.end method

.method public n()V
    .locals 1

    .prologue
    .line 343
    invoke-static {}, Lbfj;->U()Lbfj;

    move-result-object v0

    invoke-direct {p0, v0}, Laph;->a(Lt;)V

    .line 344
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Laph;->b(Z)V

    .line 364
    return-void
.end method

.method public p()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Laph;->e:Los;

    invoke-virtual {v0}, Los;->finish()V

    .line 369
    return-void
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 378
    invoke-direct {p0}, Laph;->s()Lu;

    move-result-object v0

    instance-of v0, v0, Lbeb;

    return v0
.end method

.method public r()V
    .locals 10

    .prologue
    const v9, 0x7f0204be

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 392
    invoke-virtual {p0}, Laph;->k()Z

    move-result v5

    .line 393
    invoke-static {}, Lapm;->c()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 394
    :goto_0
    iget-object v3, p0, Laph;->e:Los;

    invoke-virtual {v3}, Los;->h()Loo;

    move-result-object v6

    .line 395
    if-nez v5, :cond_0

    .line 396
    invoke-static {}, Lapm;->c()Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_0
    move v3, v1

    .line 395
    :goto_1
    invoke-virtual {v6, v3}, Loo;->c(Z)V

    .line 397
    if-nez v5, :cond_6

    iget-object v3, p0, Laph;->h:Lavx;

    .line 398
    invoke-interface {v3}, Lavx;->az()Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Laph;->h:Lavx;

    .line 399
    invoke-interface {v3}, Lavx;->E()Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v1

    .line 400
    :goto_2
    iget-object v4, p0, Laph;->h:Lavx;

    invoke-interface {v4}, Lavx;->V()Z

    move-result v4

    .line 402
    iget-object v7, p0, Laph;->h:Lavx;

    invoke-interface {v7}, Lavx;->bd()Lawg;

    move-result-object v7

    sget-object v8, Lawg;->c:Lawg;

    if-ne v7, v8, :cond_1

    .line 403
    if-eqz v3, :cond_7

    iget-object v3, p0, Laph;->h:Lavx;

    invoke-interface {v3}, Lavx;->bi()Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v1

    .line 406
    :cond_1
    :goto_3
    iget-boolean v7, p0, Laph;->o:Z

    if-eqz v7, :cond_3

    .line 407
    iget-object v7, p0, Laph;->l:Landroid/view/MenuItem;

    if-nez v3, :cond_2

    if-eqz v4, :cond_8

    :cond_2
    move v4, v1

    :goto_4
    invoke-interface {v7, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 408
    iget-object v4, p0, Laph;->m:Landroid/view/MenuItem;

    if-eqz v3, :cond_9

    if-eqz v0, :cond_9

    :goto_5
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 409
    iget-object v1, p0, Laph;->n:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 411
    if-nez v5, :cond_a

    .line 412
    invoke-virtual {v6, v9}, Loo;->f(I)V

    .line 413
    iget-object v0, p0, Laph;->l:Landroid/view/MenuItem;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    .line 415
    iget-object v0, p0, Laph;->l:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 427
    :cond_3
    :goto_6
    return-void

    :cond_4
    move v0, v2

    .line 393
    goto :goto_0

    :cond_5
    move v3, v2

    .line 396
    goto :goto_1

    :cond_6
    move v3, v2

    .line 399
    goto :goto_2

    :cond_7
    move v3, v2

    .line 403
    goto :goto_3

    :cond_8
    move v4, v2

    .line 407
    goto :goto_4

    :cond_9
    move v1, v2

    .line 408
    goto :goto_5

    .line 417
    :cond_a
    iget-object v0, p0, Laph;->l:Landroid/view/MenuItem;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    .line 418
    iget-object v0, p0, Laph;->l:Landroid/view/MenuItem;

    const v1, 0x7f0204d4

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 420
    iget-object v0, p0, Laph;->l:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 421
    const v0, 0x7f0204e6

    invoke-virtual {v6, v0}, Loo;->f(I)V

    goto :goto_6

    .line 423
    :cond_b
    invoke-virtual {v6, v9}, Loo;->f(I)V

    goto :goto_6
.end method
