.class public final Lhvk;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lhvd;
.implements Lkdd;
.implements Lljh;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:Lhwd;

.field private static p:Landroid/view/animation/Interpolator;


# instance fields
.field private b:Landroid/widget/Button;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/ImageView;

.field private f:Lhtz;

.field private g:I

.field private h:Lhts;

.field private i:Lhvl;

.field private j:[Lkda;

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lhvk;->p:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/16 v6, 0x9

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115
    sget-object v0, Lhvk;->a:Lhwd;

    if-nez v0, :cond_0

    .line 116
    invoke-static {p1}, Lhwd;->a(Landroid/content/Context;)Lhwd;

    move-result-object v0

    sput-object v0, Lhvk;->a:Lhwd;

    .line 120
    :cond_0
    invoke-virtual {p0}, Lhvk;->i()I

    move-result v3

    invoke-virtual {p0}, Lhvk;->k()I

    move-result v4

    invoke-virtual {p0}, Lhvk;->l()I

    move-result v5

    move-object v0, p1

    .line 119
    invoke-static/range {v0 .. v5}, Lkcr;->a(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lhvk;->b:Landroid/widget/Button;

    .line 121
    iget-object v0, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    invoke-direct {p0}, Lhvk;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lhvk;->k()I

    move-result v4

    invoke-virtual {p0}, Lhvk;->l()I

    move-result v5

    move v3, v6

    invoke-static/range {v0 .. v5}, Lkcr;->a(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p0}, Lhvk;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0474

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setSingleLine(Z)V

    iput-object v0, p0, Lhvk;->c:Landroid/view/View;

    :goto_0
    iget-object v0, p0, Lhvk;->c:Landroid/view/View;

    new-instance v3, Lhmk;

    sget-object v4, Lonm;->n:Lhmn;

    invoke-direct {v3, v4}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    iget-object v0, p0, Lhvk;->c:Landroid/view/View;

    invoke-virtual {p0}, Lhvk;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0589

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lhvk;->c:Landroid/view/View;

    new-instance v3, Lhmi;

    invoke-direct {v3, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    invoke-direct {p0}, Lhvk;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    move v3, v6

    :goto_1
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lhvk;->k()I

    move-result v4

    invoke-virtual {p0}, Lhvk;->l()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lkcr;->a(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lhvk;->d:Landroid/widget/Button;

    iget-object v0, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setSingleLine(Z)V

    iget-object v0, p0, Lhvk;->d:Landroid/widget/Button;

    new-instance v1, Lhmk;

    sget-object v2, Lonm;->d:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    iget-object v0, p0, Lhvk;->d:Landroid/widget/Button;

    new-instance v1, Lhmi;

    invoke-direct {v1, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lhvk;->e:Landroid/widget/ImageView;

    .line 127
    iget-object v0, p0, Lhvk;->e:Landroid/widget/ImageView;

    const v1, 0x7f0200cf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 128
    return-void

    .line 123
    :cond_1
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lhvk;->i()I

    move-result v3

    invoke-virtual {p0}, Lhvk;->k()I

    move-result v4

    invoke-virtual {p0}, Lhvk;->l()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lkcr;->b(Landroid/content/Context;Landroid/util/AttributeSet;IIII)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-static {v0}, Llii;->g(Landroid/view/View;)V

    invoke-virtual {p0}, Lhvk;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020347

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/16 v4, 0x8a

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lhvk;->c:Landroid/view/View;

    goto/16 :goto_0

    .line 124
    :cond_2
    invoke-virtual {p0}, Lhvk;->i()I

    move-result v3

    goto :goto_1
.end method

.method private b(II)I
    .locals 4

    .prologue
    .line 439
    if-ltz p1, :cond_0

    if-lt p1, p2, :cond_1

    .line 440
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x4c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "avatarIndex is out of range. avatarIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", numAvatars="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 443
    :cond_1
    iget v0, p0, Lhvk;->m:I

    sget-object v1, Lhvk;->a:Lhwd;

    iget v1, v1, Lhwd;->g:I

    sub-int/2addr v0, v1

    .line 444
    sub-int v1, p2, p1

    .line 445
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lhss;->a(Landroid/content/Context;)I

    move-result v2

    mul-int/2addr v2, v1

    add-int/lit8 v1, v1, -0x1

    sget-object v3, Lhvk;->a:Lhwd;

    iget v3, v3, Lhwd;->h:I

    mul-int/2addr v1, v3

    add-int/2addr v1, v2

    .line 447
    sub-int/2addr v0, v1

    return v0
.end method

.method private m()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 240
    iget-object v0, p0, Lhvk;->f:Lhtz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhvk;->f:Lhtz;

    invoke-interface {v0}, Lhtz;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    invoke-virtual {p0}, Lhvk;->j()I

    move-result v0

    .line 242
    :goto_0
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lhvk;->b:Landroid/widget/Button;

    .line 243
    invoke-virtual {p0}, Lhvk;->k()I

    move-result v4

    invoke-virtual {p0}, Lhvk;->l()I

    move-result v5

    .line 242
    invoke-static {v2, v3, v0, v4, v5}, Lkcr;->a(Landroid/content/Context;Landroid/widget/Button;III)V

    .line 245
    iget-object v0, p0, Lhvk;->f:Lhtz;

    if-nez v0, :cond_2

    move v0, v1

    .line 246
    :goto_1
    invoke-virtual {p0}, Lhvk;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a059c

    new-array v4, v1, [Ljava/lang/Object;

    .line 248
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    .line 246
    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 249
    iget-object v1, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v1, p0, Lhvk;->b:Landroid/widget/Button;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextDirection(I)V

    .line 253
    :cond_0
    iget-object v1, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v0, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 255
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 254
    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->measure(II)V

    .line 256
    return-void

    .line 241
    :cond_1
    invoke-virtual {p0}, Lhvk;->i()I

    move-result v0

    goto :goto_0

    .line 245
    :cond_2
    iget-object v0, p0, Lhvk;->f:Lhtz;

    invoke-interface {v0}, Lhtz;->b()I

    move-result v0

    goto :goto_1
.end method

.method private n()V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lhvk;->j:[Lkda;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lhvk;->j:[Lkda;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 511
    iget-object v1, p0, Lhvk;->j:[Lkda;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Lkda;->unregister(Lkdd;)V

    .line 510
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 514
    :cond_0
    return-void
.end method

.method private o()Z
    .locals 4

    .prologue
    .line 591
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 592
    const-class v0, Lieh;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 593
    sget-object v2, Lhvt;->a:Lief;

    const-class v3, Lhee;

    .line 594
    invoke-static {v1, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 593
    invoke-interface {v0, v2, v1}, Lieh;->b(Lief;I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(I)I
    .locals 1

    .prologue
    .line 358
    invoke-virtual {p0}, Lhvk;->g()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    return v0
.end method

.method public a(ILhtz;ZZZZILhts;Lhvl;Z)I
    .locals 6

    .prologue
    .line 133
    iput p1, p0, Lhvk;->k:I

    .line 134
    iput-object p2, p0, Lhvk;->f:Lhtz;

    .line 135
    iput p7, p0, Lhvk;->g:I

    .line 136
    iput-object p9, p0, Lhvk;->i:Lhvl;

    .line 137
    iput-object p8, p0, Lhvk;->h:Lhts;

    .line 138
    const/4 v0, 0x0

    iput-object v0, p0, Lhvk;->j:[Lkda;

    .line 140
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 141
    invoke-virtual {p0}, Lhvk;->removeAllViews()V

    .line 143
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 144
    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 146
    const/4 v0, 0x0

    .line 148
    if-eqz p4, :cond_0

    .line 149
    iget-object v0, p0, Lhvk;->c:Landroid/view/View;

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    .line 150
    const/4 v0, 0x0

    iget-object v4, p0, Lhvk;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 151
    iget-object v4, p0, Lhvk;->c:Landroid/view/View;

    invoke-virtual {p0, v4}, Lhvk;->addView(Landroid/view/View;)V

    .line 154
    :cond_0
    iget v4, p0, Lhvk;->g:I

    if-gtz v4, :cond_1

    if-eqz p5, :cond_3

    .line 155
    :cond_1
    iget-object v4, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {p0}, Lhvk;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v4, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {p0}, Lhvk;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v4, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {v4, p5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 159
    invoke-direct {p0}, Lhvk;->o()Z

    move-result v4

    if-nez v4, :cond_2

    .line 160
    invoke-virtual {p0}, Lhvk;->f()V

    .line 163
    :cond_2
    iget-object v4, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {v4, v2, v3}, Landroid/widget/Button;->measure(II)V

    .line 164
    iget-object v2, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 165
    iget-object v2, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {p0, v2}, Lhvk;->addView(Landroid/view/View;)V

    .line 168
    :cond_3
    iget-object v2, p0, Lhvk;->h:Lhts;

    if-eqz v2, :cond_8

    .line 169
    iget-object v2, p0, Lhvk;->h:Lhts;

    invoke-interface {v2}, Lhts;->a()I

    move-result v2

    .line 170
    invoke-direct {p0}, Lhvk;->o()Z

    move-result v3

    if-nez v3, :cond_7

    if-lez v2, :cond_7

    .line 171
    invoke-static {v1}, Llhn;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_7

    if-nez p10, :cond_7

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lhvk;->o:Z

    .line 173
    iget-boolean v1, p0, Lhvk;->o:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    if-le v2, v1, :cond_4

    .line 174
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lhss;->a(Landroid/content/Context;)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 176
    iget-object v2, p0, Lhvk;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v1, v1}, Landroid/widget/ImageView;->measure(II)V

    .line 177
    iget-object v1, p0, Lhvk;->e:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lhvk;->addView(Landroid/view/View;)V

    .line 186
    :cond_4
    :goto_1
    if-eqz p3, :cond_5

    .line 187
    invoke-direct {p0}, Lhvk;->m()V

    .line 188
    iget-object v1, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 189
    iget-object v1, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-virtual {p0, v1}, Lhvk;->addView(Landroid/view/View;)V

    .line 192
    :cond_5
    if-eqz p6, :cond_6

    .line 193
    iget-object v1, p0, Lhvk;->c:Landroid/view/View;

    new-instance v2, Lhmj;

    invoke-direct {v2, p0}, Lhmj;-><init>(Landroid/view/View$OnLongClickListener;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 196
    :cond_6
    invoke-virtual {p0, v0}, Lhvk;->a(I)I

    move-result v0

    iput v0, p0, Lhvk;->l:I

    .line 197
    iget v0, p0, Lhvk;->l:I

    return v0

    .line 171
    :cond_7
    const/4 v1, 0x0

    goto :goto_0

    .line 180
    :cond_8
    const/4 v1, 0x0

    iput-boolean v1, p0, Lhvk;->o:Z

    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 523
    invoke-virtual {p0}, Lhvk;->c()V

    .line 524
    invoke-virtual {p0}, Lhvk;->removeAllViews()V

    .line 525
    iput-object v0, p0, Lhvk;->f:Lhtz;

    .line 526
    iput-object v0, p0, Lhvk;->h:Lhts;

    .line 527
    iput-object v0, p0, Lhvk;->i:Lhvl;

    .line 528
    iget-object v0, p0, Lhvk;->e:Landroid/widget/ImageView;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 529
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhvk;->o:Z

    .line 530
    return-void
.end method

.method public a(II)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 417
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvk;->e:Landroid/widget/ImageView;

    .line 418
    invoke-static {v0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvk;->d:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lhvk;->e:Landroid/widget/ImageView;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    .line 420
    invoke-direct {p0, p1, p2}, Lhvk;->b(II)I

    move-result v0

    .line 421
    iget-object v1, p0, Lhvk;->e:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 422
    iget-object v1, p0, Lhvk;->e:Landroid/widget/ImageView;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setX(F)V

    .line 423
    iget-object v0, p0, Lhvk;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 424
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x190

    .line 425
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lhvk;->p:Landroid/view/animation/Interpolator;

    .line 426
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 427
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 429
    :cond_0
    return-void
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 518
    invoke-virtual {p0}, Lhvk;->invalidate()V

    .line 519
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 479
    invoke-static {p0}, Llii;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lhvk;->o:Z

    if-eqz v0, :cond_3

    .line 480
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhso;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    .line 481
    iget-object v1, p0, Lhvk;->h:Lhts;

    invoke-interface {v1}, Lhts;->a()I

    move-result v3

    .line 482
    invoke-direct {p0}, Lhvk;->n()V

    .line 483
    new-array v1, v3, [Lkda;

    iput-object v1, p0, Lhvk;->j:[Lkda;

    move v1, v2

    .line 484
    :goto_0
    if-ge v1, v3, :cond_2

    .line 485
    iget-object v4, p0, Lhvk;->h:Lhts;

    invoke-interface {v4, v1}, Lhts;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 486
    iget-object v5, p0, Lhvk;->h:Lhts;

    invoke-interface {v5, v1}, Lhts;->b(I)Ljava/lang/String;

    move-result-object v5

    .line 487
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 488
    iget-object v5, p0, Lhvk;->j:[Lkda;

    invoke-interface {v0, v4, v2, v7, p0}, Lhso;->a(Ljava/lang/String;IILkdd;)Lkda;

    move-result-object v4

    aput-object v4, v5, v1

    .line 484
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 490
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 491
    iget-object v4, p0, Lhvk;->j:[Lkda;

    invoke-interface {v0, v2, v7, p0}, Lhso;->a(IILkdd;)Lkda;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_1

    .line 495
    :cond_2
    invoke-virtual {p0, v2}, Lhvk;->setWillNotDraw(Z)V

    .line 497
    :cond_3
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lhvk;->j:[Lkda;

    if-eqz v0, :cond_0

    .line 502
    invoke-direct {p0}, Lhvk;->n()V

    .line 503
    const/4 v0, 0x0

    iput-object v0, p0, Lhvk;->j:[Lkda;

    .line 504
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhvk;->setWillNotDraw(Z)V

    .line 506
    :cond_0
    return-void
.end method

.method protected d()Ljava/lang/String;
    .locals 5

    .prologue
    .line 206
    invoke-direct {p0}, Lhvk;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    iget v0, p0, Lhvk;->g:I

    if-lez v0, :cond_0

    .line 208
    invoke-virtual {p0}, Lhvk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0473

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lhvk;->g:I

    .line 209
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 208
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 214
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 217
    :goto_1
    return-object v0

    .line 210
    :cond_0
    invoke-virtual {p0}, Lhvk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0472

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 217
    :cond_1
    iget v0, p0, Lhvk;->g:I

    if-lez v0, :cond_2

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iget v1, p0, Lhvk;->g:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method protected e()Ljava/lang/String;
    .locals 6

    .prologue
    .line 221
    iget v0, p0, Lhvk;->g:I

    if-lez v0, :cond_0

    .line 222
    invoke-virtual {p0}, Lhvk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110037

    iget v2, p0, Lhvk;->g:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lhvk;->g:I

    .line 224
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 222
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    .line 226
    :cond_0
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a058a

    .line 227
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected f()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 232
    invoke-virtual {p0}, Lhvk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02018f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 233
    const/16 v1, 0x8a

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 234
    iget-object v1, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {v1, v0, v2, v2, v2}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 235
    iget-object v1, p0, Lhvk;->d:Landroid/widget/Button;

    iget v0, p0, Lhvk;->g:I

    if-lez v0, :cond_0

    sget-object v0, Lhvk;->a:Lhwd;

    iget v0, v0, Lhwd;->p:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setCompoundDrawablePadding(I)V

    .line 237
    return-void

    .line 235
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 350
    sget-object v0, Lhvk;->a:Lhwd;

    iget v0, v0, Lhwd;->d:I

    return v0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 456
    iget-object v0, p0, Lhvk;->f:Lhtz;

    if-nez v0, :cond_0

    .line 463
    :goto_0
    return-void

    .line 460
    :cond_0
    iget-object v1, p0, Lhvk;->f:Lhtz;

    iget-object v0, p0, Lhvk;->f:Lhtz;

    invoke-interface {v0}, Lhtz;->c()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, Lhtz;->a(Z)V

    .line 461
    invoke-direct {p0}, Lhvk;->m()V

    .line 462
    invoke-virtual {p0}, Lhvk;->requestLayout()V

    goto :goto_0

    .line 460
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected i()I
    .locals 1

    .prologue
    .line 598
    const/16 v0, 0xe

    return v0
.end method

.method protected j()I
    .locals 1

    .prologue
    .line 606
    const/4 v0, 0x2

    return v0
.end method

.method protected k()I
    .locals 1

    .prologue
    .line 611
    sget-object v0, Lhvk;->a:Lhwd;

    iget v0, v0, Lhwd;->d:I

    return v0
.end method

.method protected l()I
    .locals 1

    .prologue
    .line 616
    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 473
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 474
    invoke-virtual {p0}, Lhvk;->b()V

    .line 475
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 375
    iget-object v2, p0, Lhvk;->i:Lhvl;

    if-nez v2, :cond_1

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    iget-object v2, p0, Lhvk;->b:Landroid/widget/Button;

    if-ne p1, v2, :cond_5

    .line 379
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Llhn;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 380
    iget-object v2, p0, Lhvk;->f:Lhtz;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhvk;->f:Lhtz;

    .line 381
    invoke-interface {v2}, Lhtz;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x1

    .line 382
    :cond_2
    if-eqz v0, :cond_4

    const v0, 0x7f0a058c

    .line 385
    :goto_1
    const/16 v2, 0x4000

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v2

    .line 387
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    .line 388
    invoke-virtual {p0}, Lhvk;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 387
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_3

    invoke-virtual {p0, v2}, Lhvk;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {v2, v1}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p0, v2}, Lhvk;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 391
    :cond_3
    iget-object v0, p0, Lhvk;->i:Lhvl;

    iget-object v1, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-interface {v0, v1}, Lhvl;->a(Landroid/widget/Button;)V

    goto :goto_0

    .line 382
    :cond_4
    const v0, 0x7f0a058b

    goto :goto_1

    .line 392
    :cond_5
    iget-object v2, p0, Lhvk;->c:Landroid/view/View;

    if-ne p1, v2, :cond_6

    .line 394
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_7

    .line 396
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-static {p1, v0, v0, v1, v2}, Li;->a(Landroid/view/View;IIII)Li;

    move-result-object v0

    .line 397
    invoke-virtual {v0}, Li;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 399
    :goto_2
    iget-object v1, p0, Lhvk;->i:Lhvl;

    invoke-interface {v1, v0}, Lhvl;->a(Landroid/os/Bundle;)V

    goto :goto_0

    .line 400
    :cond_6
    iget-object v0, p0, Lhvk;->d:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 401
    iget-object v0, p0, Lhvk;->i:Lhvl;

    invoke-interface {v0}, Lhvl;->r()V

    goto :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_2
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 467
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 468
    invoke-virtual {p0}, Lhvk;->c()V

    .line 469
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0xff

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 314
    invoke-direct {p0}, Lhvk;->o()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lhvk;->j:[Lkda;

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lhvk;->o:Z

    if-eqz v0, :cond_5

    .line 315
    iget-object v0, p0, Lhvk;->h:Lhts;

    invoke-interface {v0}, Lhts;->a()I

    move-result v0

    .line 316
    if-lez v0, :cond_5

    .line 317
    iget v1, p0, Lhvk;->n:I

    sget-object v6, Lhvk;->a:Lhwd;

    iget v6, v6, Lhwd;->i:I

    add-int v7, v1, v6

    .line 318
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lhss;->a(Landroid/content/Context;)I

    move-result v1

    .line 319
    sget-object v6, Lhvk;->a:Lhwd;

    iget v6, v6, Lhwd;->h:I

    add-int v8, v1, v6

    .line 321
    add-int/lit8 v6, v0, -0x1

    .line 322
    invoke-direct {p0, v6, v0}, Lhvk;->b(II)I

    move-result v6

    .line 323
    sget-object v9, Lhvk;->a:Lhwd;

    iget-object v9, v9, Lhwd;->l:Landroid/graphics/Rect;

    sget-object v10, Lhvk;->a:Lhwd;

    iget v10, v10, Lhwd;->f:I

    add-int v11, v6, v1

    sget-object v12, Lhvk;->a:Lhwd;

    iget v12, v12, Lhwd;->f:I

    add-int/2addr v1, v12

    invoke-virtual {v9, v6, v10, v11, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 327
    add-int/lit8 v0, v0, -0x1

    move v6, v0

    :goto_0
    if-ltz v6, :cond_4

    .line 328
    iget-object v0, p0, Lhvk;->j:[Lkda;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhvk;->j:[Lkda;

    aget-object v0, v0, v6

    .line 329
    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 330
    :goto_1
    if-nez v0, :cond_0

    .line 332
    invoke-virtual {p0}, Lhvk;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2}, Lhss;->b(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 334
    :cond_0
    sget-object v1, Lhvk;->a:Lhwd;

    iget-object v1, v1, Lhwd;->l:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-lt v1, v7, :cond_2

    move v1, v2

    .line 336
    :goto_2
    sget-object v9, Lhvk;->a:Lhwd;

    iget-object v9, v9, Lhwd;->A:Landroid/graphics/Paint;

    if-eqz v1, :cond_3

    move v1, v4

    :goto_3
    invoke-virtual {v9, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 338
    sget-object v1, Lhvk;->a:Lhwd;

    iget-object v1, v1, Lhwd;->l:Landroid/graphics/Rect;

    sget-object v9, Lhvk;->a:Lhwd;

    iget-object v9, v9, Lhwd;->A:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v5, v1, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 340
    sget-object v0, Lhvk;->a:Lhwd;

    iget-object v0, v0, Lhwd;->l:Landroid/graphics/Rect;

    neg-int v1, v8

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 327
    add-int/lit8 v0, v6, -0x1

    move v6, v0

    goto :goto_0

    :cond_1
    move-object v0, v5

    .line 329
    goto :goto_1

    :cond_2
    move v1, v3

    .line 334
    goto :goto_2

    .line 336
    :cond_3
    const/16 v1, 0x20

    goto :goto_3

    .line 342
    :cond_4
    sget-object v0, Lhvk;->a:Lhwd;

    iget-object v0, v0, Lhwd;->A:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 345
    :cond_5
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 346
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 280
    sget-object v0, Lhvk;->a:Lhwd;

    iget v0, v0, Lhwd;->k:I

    iput v0, p0, Lhvk;->n:I

    .line 281
    invoke-virtual {p0}, Lhvk;->g()I

    move-result v1

    .line 283
    iget-object v2, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_0

    .line 284
    iget-object v2, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    .line 285
    iget-object v3, p0, Lhvk;->b:Landroid/widget/Button;

    iget-object v4, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/widget/Button;->layout(IIII)V

    .line 286
    iput v2, p0, Lhvk;->n:I

    .line 287
    sget-object v0, Lhvk;->a:Lhwd;

    iget v0, v0, Lhwd;->e:I

    add-int/2addr v0, v2

    .line 290
    :cond_0
    iget-object v2, p0, Lhvk;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_1

    .line 291
    iget-object v2, p0, Lhvk;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    .line 292
    iget-object v3, p0, Lhvk;->c:Landroid/view/View;

    iget-object v4, p0, Lhvk;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/view/View;->layout(IIII)V

    .line 293
    iput v2, p0, Lhvk;->n:I

    .line 296
    :cond_1
    iget-object v0, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_2

    .line 297
    invoke-virtual {p0}, Lhvk;->getMeasuredWidth()I

    move-result v0

    sget-object v2, Lhvk;->a:Lhwd;

    iget v2, v2, Lhwd;->k:I

    sub-int/2addr v0, v2

    .line 298
    iget-object v2, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v0, v2

    iput v2, p0, Lhvk;->m:I

    .line 299
    iget-object v2, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v1

    .line 300
    iget-object v3, p0, Lhvk;->d:Landroid/widget/Button;

    iget v4, p0, Lhvk;->m:I

    invoke-virtual {v3, v4, v1, v0, v2}, Landroid/widget/Button;->layout(IIII)V

    .line 301
    iget-object v0, p0, Lhvk;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_2

    .line 302
    const/4 v0, 0x0

    iget-object v1, p0, Lhvk;->h:Lhts;

    .line 303
    invoke-interface {v1}, Lhts;->a()I

    move-result v1

    .line 302
    invoke-direct {p0, v0, v1}, Lhvk;->b(II)I

    move-result v0

    .line 304
    iget-object v1, p0, Lhvk;->e:Landroid/widget/ImageView;

    sget-object v2, Lhvk;->a:Lhwd;

    iget v2, v2, Lhwd;->f:I

    iget-object v3, p0, Lhvk;->e:Landroid/widget/ImageView;

    .line 305
    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    sget-object v4, Lhvk;->a:Lhwd;

    iget v4, v4, Lhwd;->f:I

    iget-object v5, p0, Lhvk;->e:Landroid/widget/ImageView;

    .line 307
    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 304
    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    .line 310
    :cond_2
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 363
    iget-object v1, p0, Lhvk;->c:Landroid/view/View;

    if-ne p1, v1, :cond_0

    .line 365
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-static {p1, v0, v0, v1, v2}, Li;->a(Landroid/view/View;IIII)Li;

    move-result-object v0

    .line 366
    invoke-virtual {v0}, Li;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 367
    iget-object v1, p0, Lhvk;->i:Lhvl;

    invoke-interface {v1, v0}, Lhvl;->b(Landroid/os/Bundle;)V

    .line 368
    const/4 v0, 0x1

    .line 370
    :cond_0
    return v0
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 268
    iget-object v0, p0, Lhvk;->b:Landroid/widget/Button;

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 269
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 268
    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->measure(II)V

    .line 271
    iget-object v0, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 272
    iget-object v0, p0, Lhvk;->d:Landroid/widget/Button;

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 273
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 272
    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->measure(II)V

    .line 275
    :cond_0
    iget v0, p0, Lhvk;->k:I

    iget v1, p0, Lhvk;->l:I

    invoke-virtual {p0, v0, v1}, Lhvk;->setMeasuredDimension(II)V

    .line 276
    return-void
.end method
