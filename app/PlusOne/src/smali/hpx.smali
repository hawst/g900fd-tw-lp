.class public final Lhpx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[Landroid/net/Uri;

.field private static final b:[Ljava/lang/String;

.field private static c:Lhpx;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lhrj;

.field private final f:Lhrx;

.field private final g:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

.field private final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lhpy;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lhei;

.field private final j:Lhrb;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sget-object v1, Lhsf;->a:Landroid/net/Uri;

    aput-object v1, v0, v5

    const/4 v1, 0x3

    sget-object v2, Lhsf;->b:Landroid/net/Uri;

    aput-object v2, v0, v1

    sput-object v0, Lhpx;->a:[Landroid/net/Uri;

    .line 59
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "mime_type"

    aput-object v1, v0, v4

    sput-object v0, Lhpx;->b:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lhpx;->d:Landroid/content/Context;

    .line 76
    const-class v0, Lhrj;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrj;

    iput-object v0, p0, Lhpx;->e:Lhrj;

    .line 77
    invoke-static {p1}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v0

    iput-object v0, p0, Lhpx;->f:Lhrx;

    .line 78
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    move-result-object v0

    iput-object v0, p0, Lhpx;->g:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhpx;->h:Ljava/util/Set;

    .line 80
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhpx;->i:Lhei;

    .line 81
    iget-object v0, p0, Lhpx;->d:Landroid/content/Context;

    const-class v1, Lhrb;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhrb;

    iput-object v0, p0, Lhpx;->j:Lhrb;

    .line 82
    return-void
.end method

.method private a(ILjava/util/List;Ljava/util/List;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 268
    iget-object v1, p0, Lhpx;->i:Lhei;

    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 271
    new-instance v4, Ljvu;

    iget-object v2, p0, Lhpx;->d:Landroid/content/Context;

    const-string v3, "gaia_id"

    .line 272
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v2, p1, v1, p2}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    .line 273
    invoke-virtual {v4}, Ljvu;->l()V

    .line 275
    iget-object v1, p0, Lhpx;->e:Lhrj;

    invoke-virtual {v1}, Lhrj;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 276
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v6

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v6, :cond_1

    .line 277
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 278
    invoke-interface {p3, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 279
    invoke-virtual {v4, v0}, Ljvu;->b(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 281
    const-wide/16 v8, 0x0

    iput-wide v8, v1, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->id:J

    .line 282
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 283
    invoke-virtual {v1, p1}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->e(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 284
    const/16 v0, 0x190

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->b(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v0

    const/16 v7, 0x22

    .line 285
    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->c(I)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    .line 286
    sget-object v0, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a:Lifm;

    invoke-virtual {v0, v5, v1}, Lifm;->a(Landroid/database/sqlite/SQLiteDatabase;Lifj;)J

    .line 287
    add-int/lit8 v0, v2, 0x1

    .line 289
    const-string v2, "iu.FingerprintManager"

    const/4 v7, 0x4

    invoke-static {v2, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 290
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x25

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "+++ Found previously uploaded media: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 276
    :cond_0
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v2, v0

    goto :goto_0

    .line 294
    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lhpx;
    .locals 2

    .prologue
    .line 85
    const-class v1, Lhpx;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lhpx;->c:Lhpx;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lhpx;

    invoke-direct {v0, p0}, Lhpx;-><init>(Landroid/content/Context;)V

    sput-object v0, Lhpx;->c:Lhpx;

    .line 88
    :cond_0
    sget-object v0, Lhpx;->c:Lhpx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized a()I
    .locals 21

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lhpx;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 103
    const/4 v10, 0x0

    .line 104
    const/4 v9, 0x0

    .line 105
    const/4 v8, 0x0

    .line 107
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 109
    move-object/from16 v0, p0

    iget-object v3, v0, Lhpx;->f:Lhrx;

    .line 114
    invoke-virtual {v3}, Lhrx;->a()Ljava/util/Set;

    move-result-object v14

    .line 116
    sget-object v15, Lhpx;->a:[Landroid/net/Uri;

    const/4 v3, 0x0

    move v11, v3

    :goto_0
    const/4 v3, 0x4

    if-ge v11, v3, :cond_9

    aget-object v3, v15, v11

    .line 117
    const-string v4, "iu.FingerprintManager"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 118
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x22

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Start processing media store URI: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    sget-object v4, Lhpx;->b:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v7

    .line 123
    if-eqz v7, :cond_e

    move v4, v8

    move v5, v9

    move v6, v10

    .line 124
    :goto_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 129
    const/4 v8, 0x0

    invoke-interface {v7, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 131
    invoke-static {v3, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 133
    const-string v9, "iu.FingerprintManager"

    const/4 v10, 0x2

    invoke-static {v9, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 134
    const-string v9, "processing media: "

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v16

    if-eqz v16, :cond_6

    invoke-virtual {v9, v10}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 137
    :cond_1
    :goto_2
    const/4 v9, 0x1

    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 138
    invoke-static {v9}, Lhsf;->a(Ljava/lang/String;)Z

    move-result v10

    .line 140
    if-eqz v10, :cond_4

    invoke-interface {v14, v8}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_4

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lhpx;->f:Lhrx;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, Lhrx;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v16

    .line 146
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_2

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 147
    :cond_2
    add-int/lit8 v4, v4, 0x1

    .line 148
    const-string v17, "iu.FingerprintManager"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 149
    const-string v17, "Not inserting fingerprint into all photos because has empty content uri or fingerprint. uri: "

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, 0xe

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int v19, v19, v20

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int v19, v19, v20

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " fingerprint: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_3
    add-int/lit8 v5, v5, 0x1

    .line 156
    const-string v16, "iu.FingerprintManager"

    const/16 v17, 0x3

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 157
    const-string v16, "generated fingerprint for: "

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v18

    if-eqz v18, :cond_7

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 160
    :cond_4
    :goto_3
    if-nez v10, :cond_5

    const-string v10, "iu.FingerprintManager"

    const/16 v16, 0x3

    move/from16 v0, v16

    invoke-static {v10, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 161
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, 0x24

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int v16, v16, v17

    move/from16 v0, v16

    invoke-direct {v10, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v16, "non media mime type; media: "

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ", type: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_5
    add-int/lit8 v6, v6, 0x1

    .line 164
    goto/16 :goto_1

    .line 134
    :cond_6
    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 166
    :catchall_0
    move-exception v2

    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 102
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 157
    :cond_7
    :try_start_3
    new-instance v17, Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 166
    :cond_8
    :try_start_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move v3, v4

    move v4, v5

    move v5, v6

    .line 116
    :goto_4
    add-int/lit8 v6, v11, 0x1

    move v11, v6

    move v8, v3

    move v9, v4

    move v10, v5

    goto/16 :goto_0

    .line 172
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lhpx;->f:Lhrx;

    invoke-virtual {v2, v14}, Lhrx;->a(Ljava/util/Collection;)V

    .line 173
    invoke-interface {v14}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v3, v2, 0x0

    .line 175
    move-object/from16 v0, p0

    iget-object v2, v0, Lhpx;->h:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhpy;

    .line 176
    invoke-interface {v2}, Lhpy;->a()V

    goto :goto_5

    .line 179
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lhpx;->d:Landroid/content/Context;

    const-class v4, Lhqo;

    invoke-static {v2, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhqo;

    invoke-interface {v2}, Lhqo;->c()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 180
    invoke-virtual/range {p0 .. p0}, Lhpx;->c()V

    .line 183
    :cond_b
    const-string v2, "iu.FingerprintManager"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "Finished generating fingerprints; "

    invoke-static {v12, v13}, Llse;->a(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_d

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_6
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x5b

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "  numSeen="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " numGenerated="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " numDeleted="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " numFailed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 185
    :cond_c
    monitor-exit p0

    return v10

    .line 183
    :cond_d
    :try_start_5
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_6

    :cond_e
    move v3, v8

    move v4, v9

    move v5, v10

    goto/16 :goto_4
.end method

.method public a(Lhpy;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lhpx;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 93
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 192
    iget-object v0, p0, Lhpx;->i:Lhei;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "logged_in"

    aput-object v2, v1, v4

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 193
    iget-object v2, p0, Lhpx;->j:Lhrb;

    iget-object v3, p0, Lhpx;->d:Landroid/content/Context;

    invoke-virtual {v2, v3, v0, v4}, Lhrb;->a(Landroid/content/Context;IZ)V

    goto :goto_0

    .line 195
    :cond_0
    return-void
.end method

.method public c()V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 201
    iget-object v0, p0, Lhpx;->i:Lhei;

    new-array v1, v1, [Ljava/lang/String;

    const-string v3, "logged_in"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 203
    :try_start_0
    iget-object v0, p0, Lhpx;->g:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 213
    :cond_1
    return-void

    .line 205
    :cond_2
    iget-object v0, p0, Lhpx;->j:Lhrb;

    iget-object v1, p0, Lhpx;->d:Landroid/content/Context;

    invoke-virtual {v0, v1, v4}, Lhrb;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lhpx;->e:Lhrj;

    invoke-static {v0, v4}, Lhqn;->e(Lhrj;I)Landroid/database/Cursor;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Lhel; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    :cond_3
    :goto_1
    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-static {v5}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->a(Landroid/database/Cursor;)Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->o()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    iget-object v9, p0, Lhpx;->d:Landroid/content/Context;

    invoke-virtual {v8}, Lcom/google/android/libraries/social/autobackup/MediaRecordEntry;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljdv;->a(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    const-string v10, "file://"

    invoke-static {v9, v0}, Ljdv;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_6

    invoke-virtual {v10, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    :goto_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v9, p0, Lhpx;->f:Lhrx;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lhrx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_5
    if-eqz v0, :cond_3

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    const/16 v8, 0x64

    if-ne v0, v8, :cond_b

    invoke-direct {p0, v4, v6, v7}, Lhpx;->a(ILjava/util/List;Ljava/util/List;)I

    move-result v0

    add-int/2addr v0, v1

    invoke-interface {v6}, Ljava/util/List;->clear()V

    invoke-interface {v7}, Ljava/util/List;->clear()V

    :goto_3
    move v1, v0

    goto :goto_1

    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v0

    .line 210
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 206
    :cond_7
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    invoke-direct {p0, v4, v6, v7}, Lhpx;->a(ILjava/util/List;Ljava/util/List;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_8
    const-string v0, "iu.FingerprintManager"

    const/4 v5, 0x4

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v5, 0x45

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Synced photo uploads, account="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", matched photos="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_9
    if-lez v1, :cond_a

    iget-object v0, p0, Lhpx;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lhqv;->b:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 207
    :cond_a
    iget-object v0, p0, Lhpx;->j:Lhrb;

    iget-object v1, p0, Lhpx;->d:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-virtual {v0, v1, v4, v5}, Lhrb;->a(Landroid/content/Context;IZ)V
    :try_end_2
    .catch Lhel; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto :goto_3
.end method
