.class public final Lnnq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Loej;

.field public b:Lnmw;

.field public c:Lnmt;

.field public d:I

.field public e:Loxz;

.field public f:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v1, p0, Lnnq;->a:Loej;

    .line 16
    iput-object v1, p0, Lnnq;->b:Lnmw;

    .line 19
    iput-object v1, p0, Lnnq;->c:Lnmt;

    .line 22
    const/high16 v0, -0x80000000

    iput v0, p0, Lnnq;->d:I

    .line 25
    iput-object v1, p0, Lnnq;->e:Loxz;

    .line 28
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnnq;->f:[I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 59
    .line 60
    iget-object v0, p0, Lnnq;->a:Loej;

    if-eqz v0, :cond_6

    .line 61
    const/4 v0, 0x1

    iget-object v2, p0, Lnnq;->a:Loej;

    .line 62
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 64
    :goto_0
    iget-object v2, p0, Lnnq;->b:Lnmw;

    if-eqz v2, :cond_0

    .line 65
    const/4 v2, 0x2

    iget-object v3, p0, Lnnq;->b:Lnmw;

    .line 66
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 68
    :cond_0
    iget-object v2, p0, Lnnq;->c:Lnmt;

    if-eqz v2, :cond_1

    .line 69
    const/4 v2, 0x3

    iget-object v3, p0, Lnnq;->c:Lnmt;

    .line 70
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 72
    :cond_1
    iget v2, p0, Lnnq;->d:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_2

    .line 73
    const/4 v2, 0x4

    iget v3, p0, Lnnq;->d:I

    .line 74
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 76
    :cond_2
    iget-object v2, p0, Lnnq;->e:Loxz;

    if-eqz v2, :cond_3

    .line 77
    const/4 v2, 0x5

    iget-object v3, p0, Lnnq;->e:Loxz;

    .line 78
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 80
    :cond_3
    iget-object v2, p0, Lnnq;->f:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Lnnq;->f:[I

    array-length v2, v2

    if-lez v2, :cond_5

    .line 82
    iget-object v3, p0, Lnnq;->f:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget v5, v3, v1

    .line 84
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 86
    :cond_4
    add-int/2addr v0, v2

    .line 87
    iget-object v1, p0, Lnnq;->f:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 89
    :cond_5
    iget-object v1, p0, Lnnq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    iput v0, p0, Lnnq;->ai:I

    .line 91
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnnq;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 99
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 100
    sparse-switch v0, :sswitch_data_0

    .line 104
    iget-object v1, p0, Lnnq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnnq;->ah:Ljava/util/List;

    .line 108
    :cond_1
    iget-object v1, p0, Lnnq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    :sswitch_0
    return-object p0

    .line 115
    :sswitch_1
    iget-object v0, p0, Lnnq;->a:Loej;

    if-nez v0, :cond_2

    .line 116
    new-instance v0, Loej;

    invoke-direct {v0}, Loej;-><init>()V

    iput-object v0, p0, Lnnq;->a:Loej;

    .line 118
    :cond_2
    iget-object v0, p0, Lnnq;->a:Loej;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 122
    :sswitch_2
    iget-object v0, p0, Lnnq;->b:Lnmw;

    if-nez v0, :cond_3

    .line 123
    new-instance v0, Lnmw;

    invoke-direct {v0}, Lnmw;-><init>()V

    iput-object v0, p0, Lnnq;->b:Lnmw;

    .line 125
    :cond_3
    iget-object v0, p0, Lnnq;->b:Lnmw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 129
    :sswitch_3
    iget-object v0, p0, Lnnq;->c:Lnmt;

    if-nez v0, :cond_4

    .line 130
    new-instance v0, Lnmt;

    invoke-direct {v0}, Lnmt;-><init>()V

    iput-object v0, p0, Lnnq;->c:Lnmt;

    .line 132
    :cond_4
    iget-object v0, p0, Lnnq;->c:Lnmt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 136
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 137
    if-eqz v0, :cond_5

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 140
    :cond_5
    iput v0, p0, Lnnq;->d:I

    goto :goto_0

    .line 142
    :cond_6
    iput v3, p0, Lnnq;->d:I

    goto :goto_0

    .line 147
    :sswitch_5
    iget-object v0, p0, Lnnq;->e:Loxz;

    if-nez v0, :cond_7

    .line 148
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, p0, Lnnq;->e:Loxz;

    .line 150
    :cond_7
    iget-object v0, p0, Lnnq;->e:Loxz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 154
    :sswitch_6
    const/16 v0, 0x30

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 155
    iget-object v0, p0, Lnnq;->f:[I

    array-length v0, v0

    .line 156
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 157
    iget-object v2, p0, Lnnq;->f:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 158
    iput-object v1, p0, Lnnq;->f:[I

    .line 159
    :goto_1
    iget-object v1, p0, Lnnq;->f:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_8

    .line 160
    iget-object v1, p0, Lnnq;->f:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 161
    invoke-virtual {p1}, Loxn;->a()I

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 164
    :cond_8
    iget-object v1, p0, Lnnq;->f:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    .line 100
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lnnq;->a:Loej;

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x1

    iget-object v1, p0, Lnnq;->a:Loej;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 36
    :cond_0
    iget-object v0, p0, Lnnq;->b:Lnmw;

    if-eqz v0, :cond_1

    .line 37
    const/4 v0, 0x2

    iget-object v1, p0, Lnnq;->b:Lnmw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 39
    :cond_1
    iget-object v0, p0, Lnnq;->c:Lnmt;

    if-eqz v0, :cond_2

    .line 40
    const/4 v0, 0x3

    iget-object v1, p0, Lnnq;->c:Lnmt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 42
    :cond_2
    iget v0, p0, Lnnq;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 43
    const/4 v0, 0x4

    iget v1, p0, Lnnq;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 45
    :cond_3
    iget-object v0, p0, Lnnq;->e:Loxz;

    if-eqz v0, :cond_4

    .line 46
    const/4 v0, 0x5

    iget-object v1, p0, Lnnq;->e:Loxz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 48
    :cond_4
    iget-object v0, p0, Lnnq;->f:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lnnq;->f:[I

    array-length v0, v0

    if-lez v0, :cond_5

    .line 49
    iget-object v1, p0, Lnnq;->f:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget v3, v1, v0

    .line 50
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_5
    iget-object v0, p0, Lnnq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 55
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnnq;->a(Loxn;)Lnnq;

    move-result-object v0

    return-object v0
.end method
