.class public final Lhss;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:I

.field private static b:I

.field private static c:I

.field private static d:I

.field private static e:Landroid/graphics/Bitmap;

.field private static f:Landroid/graphics/Bitmap;

.field private static g:Landroid/graphics/Bitmap;

.field private static h:Landroid/graphics/Bitmap;

.field private static i:Landroid/graphics/Bitmap;

.field private static j:Landroid/graphics/Bitmap;

.field private static k:Landroid/graphics/Bitmap;

.field private static l:Landroid/graphics/Bitmap;

.field private static m:Landroid/graphics/Bitmap;

.field private static n:Landroid/graphics/Bitmap;

.field private static o:Landroid/graphics/Bitmap;

.field private static p:Landroid/graphics/Bitmap;

.field private static q:F


# direct methods
.method public static a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 73
    sget v0, Lhss;->a:I

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0210

    .line 75
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lhss;->a:I

    .line 77
    :cond_0
    sget v0, Lhss;->a:I

    return v0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 1

    .prologue
    .line 57
    packed-switch p1, :pswitch_data_0

    .line 64
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 58
    :pswitch_0
    invoke-static {p0}, Lhss;->a(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    .line 59
    :pswitch_1
    invoke-static {p0}, Lhss;->c(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    .line 60
    :pswitch_2
    invoke-static {p0}, Lhss;->e(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    .line 61
    :pswitch_3
    invoke-static {p0}, Lhss;->g(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 276
    const-string v0, "tiny"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    const/4 v0, 0x0

    .line 286
    :goto_0
    return v0

    .line 279
    :cond_0
    const-string v0, "small"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    const/4 v0, 0x1

    goto :goto_0

    .line 282
    :cond_1
    const-string v0, "medium"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 283
    const/4 v0, 0x2

    goto :goto_0

    .line 285
    :cond_2
    const-string v0, "large"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 286
    const/4 v0, 0x3

    goto :goto_0

    .line 288
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid avatar size: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private static a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 323
    if-nez p0, :cond_0

    .line 324
    const/4 v0, 0x0

    .line 328
    :goto_0
    return-object v0

    .line 326
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 327
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 328
    div-int/lit8 v2, v0, 0x4

    const/4 v3, 0x0

    div-int/lit8 v0, v0, 0x2

    invoke-static {p0, v2, v3, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 348
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 349
    :cond_0
    const/4 v0, 0x0

    .line 361
    :goto_0
    return-object v0

    .line 350
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 351
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0

    .line 354
    :cond_2
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 355
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 356
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 357
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 359
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 360
    invoke-static {v1, p0, v2, v2}, Lhss;->a(Landroid/graphics/Canvas;Ljava/util/List;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Canvas;Ljava/util/List;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Canvas;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Paint;",
            "Landroid/graphics/Paint;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 374
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 379
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 380
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 382
    if-ne v1, v4, :cond_2

    .line 383
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0, v2, v2, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 384
    :cond_2
    if-ne v1, v5, :cond_3

    .line 386
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lhss;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 387
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lhss;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 388
    invoke-virtual {p0, v1, v2, v2, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 389
    div-int/lit8 v1, v6, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1, v2, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 390
    if-eqz p3, :cond_0

    .line 391
    div-int/lit8 v0, v6, 0x2

    int-to-float v1, v0

    div-int/lit8 v0, v6, 0x2

    int-to-float v3, v0

    int-to-float v4, v7

    move-object v0, p0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 393
    :cond_3
    if-ne v1, v8, :cond_4

    .line 395
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lhss;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 396
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lhss;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 397
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lhss;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 398
    invoke-virtual {p0, v1, v2, v2, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 399
    div-int/lit8 v1, v6, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v3, v1, v2, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 400
    div-int/lit8 v1, v6, 0x2

    int-to-float v1, v1

    div-int/lit8 v3, v7, 0x2

    int-to-float v3, v3

    invoke-virtual {p0, v0, v1, v3, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 401
    if-eqz p3, :cond_0

    .line 402
    div-int/lit8 v0, v6, 0x2

    int-to-float v1, v0

    div-int/lit8 v0, v6, 0x2

    int-to-float v3, v0

    int-to-float v4, v7

    move-object v0, p0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 403
    div-int/lit8 v0, v6, 0x2

    int-to-float v1, v0

    div-int/lit8 v0, v7, 0x2

    int-to-float v2, v0

    int-to-float v3, v6

    div-int/lit8 v0, v7, 0x2

    int-to-float v4, v0

    move-object v0, p0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 405
    :cond_4
    const/4 v0, 0x4

    if-lt v1, v0, :cond_0

    .line 407
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lhss;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 408
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lhss;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 409
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lhss;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 410
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lhss;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 411
    invoke-virtual {p0, v1, v2, v2, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 412
    div-int/lit8 v1, v6, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v3, v1, v2, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 413
    div-int/lit8 v1, v7, 0x2

    int-to-float v1, v1

    invoke-virtual {p0, v4, v2, v1, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 414
    div-int/lit8 v1, v6, 0x2

    int-to-float v1, v1

    div-int/lit8 v3, v7, 0x2

    int-to-float v3, v3

    invoke-virtual {p0, v0, v1, v3, p2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 415
    if-eqz p3, :cond_0

    .line 416
    div-int/lit8 v0, v6, 0x2

    int-to-float v1, v0

    div-int/lit8 v0, v6, 0x2

    int-to-float v3, v0

    int-to-float v4, v7

    move-object v0, p0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 417
    div-int/lit8 v0, v7, 0x2

    int-to-float v3, v0

    int-to-float v4, v6

    div-int/lit8 v0, v7, 0x2

    int-to-float v5, v0

    move-object v1, p0

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public static b(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 295
    const-string v0, "normal"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    const/4 v0, 0x0

    .line 302
    :goto_0
    return v0

    .line 298
    :cond_0
    const-string v0, "round"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    const/4 v0, 0x1

    goto :goto_0

    .line 301
    :cond_1
    const-string v0, "rounded_corners"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 302
    const/4 v0, 0x2

    goto :goto_0

    .line 304
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid avatar shape: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 86
    sget-object v0, Lhss;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 87
    invoke-static {p0}, Lhss;->f(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 88
    invoke-static {p0}, Lhss;->a(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    .line 87
    invoke-static {v0, v1, v2}, Llrw;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->e:Landroid/graphics/Bitmap;

    .line 90
    :cond_0
    sget-object v0, Lhss;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static b(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 99
    packed-switch p1, :pswitch_data_0

    .line 114
    invoke-static {p0}, Lhss;->b(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    .line 101
    :pswitch_0
    sget-object v0, Lhss;->f:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 103
    invoke-static {p0}, Lhss;->b(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 102
    invoke-static {p0, v0}, Llho;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->f:Landroid/graphics/Bitmap;

    .line 105
    :cond_0
    sget-object v0, Lhss;->f:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 107
    :pswitch_1
    sget-object v0, Lhss;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 109
    invoke-static {p0}, Lhss;->b(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0}, Lhss;->i(Landroid/content/Context;)F

    move-result v1

    .line 108
    invoke-static {v0, v1}, Llho;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->g:Landroid/graphics/Bitmap;

    .line 111
    :cond_1
    sget-object v0, Lhss;->g:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 99
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 332
    if-nez p0, :cond_0

    .line 333
    const/4 v0, 0x0

    .line 337
    :goto_0
    return-object v0

    .line 335
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 336
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 337
    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v1, 0x2

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 124
    sget v0, Lhss;->b:I

    if-nez v0, :cond_0

    .line 125
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0211

    .line 126
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lhss;->b:I

    .line 128
    :cond_0
    sget v0, Lhss;->b:I

    return v0
.end method

.method public static c(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 150
    packed-switch p1, :pswitch_data_0

    .line 165
    invoke-static {p0}, Lhss;->d(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    .line 152
    :pswitch_0
    sget-object v0, Lhss;->i:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 154
    invoke-static {p0}, Lhss;->d(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 153
    invoke-static {p0, v0}, Llho;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->i:Landroid/graphics/Bitmap;

    .line 156
    :cond_0
    sget-object v0, Lhss;->i:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 158
    :pswitch_1
    sget-object v0, Lhss;->j:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 160
    invoke-static {p0}, Lhss;->d(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0}, Lhss;->i(Landroid/content/Context;)F

    move-result v1

    .line 159
    invoke-static {v0, v1}, Llho;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->j:Landroid/graphics/Bitmap;

    .line 162
    :cond_1
    sget-object v0, Lhss;->j:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static d(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 137
    sget-object v0, Lhss;->h:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 138
    invoke-static {p0}, Lhss;->f(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 139
    invoke-static {p0}, Lhss;->c(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    .line 138
    invoke-static {v0, v1, v2}, Llrw;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->h:Landroid/graphics/Bitmap;

    .line 141
    :cond_0
    sget-object v0, Lhss;->h:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static d(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 201
    packed-switch p1, :pswitch_data_0

    .line 216
    invoke-static {p0}, Lhss;->f(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    .line 203
    :pswitch_0
    sget-object v0, Lhss;->l:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 205
    invoke-static {p0}, Lhss;->f(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 204
    invoke-static {p0, v0}, Llho;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->l:Landroid/graphics/Bitmap;

    .line 207
    :cond_0
    sget-object v0, Lhss;->l:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 209
    :pswitch_1
    sget-object v0, Lhss;->m:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 211
    invoke-static {p0}, Lhss;->f(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0}, Lhss;->i(Landroid/content/Context;)F

    move-result v1

    .line 210
    invoke-static {v0, v1}, Llho;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->m:Landroid/graphics/Bitmap;

    .line 213
    :cond_1
    sget-object v0, Lhss;->m:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static e(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 175
    sget v0, Lhss;->c:I

    if-nez v0, :cond_0

    .line 176
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0212

    .line 177
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lhss;->c:I

    .line 179
    :cond_0
    sget v0, Lhss;->c:I

    return v0
.end method

.method public static e(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 253
    packed-switch p1, :pswitch_data_0

    .line 268
    invoke-static {p0}, Lhss;->h(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    .line 255
    :pswitch_0
    sget-object v0, Lhss;->o:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 257
    invoke-static {p0}, Lhss;->h(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 256
    invoke-static {p0, v0}, Llho;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->o:Landroid/graphics/Bitmap;

    .line 259
    :cond_0
    sget-object v0, Lhss;->o:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 261
    :pswitch_1
    sget-object v0, Lhss;->p:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 263
    invoke-static {p0}, Lhss;->h(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0}, Lhss;->i(Landroid/content/Context;)F

    move-result v1

    .line 262
    invoke-static {v0, v1}, Llho;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->p:Landroid/graphics/Bitmap;

    .line 265
    :cond_1
    sget-object v0, Lhss;->p:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 253
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static f(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 188
    sget-object v0, Lhss;->k:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 189
    invoke-static {p0}, Lhss;->h(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 190
    invoke-static {p0}, Lhss;->e(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    .line 189
    invoke-static {v0, v1, v2}, Llrw;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->k:Landroid/graphics/Bitmap;

    .line 192
    :cond_0
    sget-object v0, Lhss;->k:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static g(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 226
    sget v0, Lhss;->d:I

    if-nez v0, :cond_0

    .line 227
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0213

    .line 228
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lhss;->d:I

    .line 230
    :cond_0
    sget v0, Lhss;->d:I

    return v0
.end method

.method public static h(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 239
    sget-object v0, Lhss;->n:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 240
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 241
    const v1, 0x7f02005d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 242
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lhss;->n:Landroid/graphics/Bitmap;

    .line 244
    :cond_0
    sget-object v0, Lhss;->n:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static i(Landroid/content/Context;)F
    .locals 2

    .prologue
    .line 308
    sget v0, Lhss;->q:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 309
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0214

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lhss;->q:F

    .line 312
    :cond_0
    sget v0, Lhss;->q:F

    return v0
.end method
