.class public final Lmde;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlo;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32641
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32644
    iput-object v0, p0, Lmde;->apiHeader:Llyq;

    .line 32647
    iput-object v0, p0, Lmde;->a:Lmlo;

    .line 32641
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32664
    const/4 v0, 0x0

    .line 32665
    iget-object v1, p0, Lmde;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 32666
    const/4 v0, 0x1

    iget-object v1, p0, Lmde;->apiHeader:Llyq;

    .line 32667
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32669
    :cond_0
    iget-object v1, p0, Lmde;->a:Lmlo;

    if-eqz v1, :cond_1

    .line 32670
    const/4 v1, 0x2

    iget-object v2, p0, Lmde;->a:Lmlo;

    .line 32671
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32673
    :cond_1
    iget-object v1, p0, Lmde;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32674
    iput v0, p0, Lmde;->ai:I

    .line 32675
    return v0
.end method

.method public a(Loxn;)Lmde;
    .locals 2

    .prologue
    .line 32683
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32684
    sparse-switch v0, :sswitch_data_0

    .line 32688
    iget-object v1, p0, Lmde;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32689
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmde;->ah:Ljava/util/List;

    .line 32692
    :cond_1
    iget-object v1, p0, Lmde;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32694
    :sswitch_0
    return-object p0

    .line 32699
    :sswitch_1
    iget-object v0, p0, Lmde;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 32700
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmde;->apiHeader:Llyq;

    .line 32702
    :cond_2
    iget-object v0, p0, Lmde;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32706
    :sswitch_2
    iget-object v0, p0, Lmde;->a:Lmlo;

    if-nez v0, :cond_3

    .line 32707
    new-instance v0, Lmlo;

    invoke-direct {v0}, Lmlo;-><init>()V

    iput-object v0, p0, Lmde;->a:Lmlo;

    .line 32709
    :cond_3
    iget-object v0, p0, Lmde;->a:Lmlo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32684
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32652
    iget-object v0, p0, Lmde;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 32653
    const/4 v0, 0x1

    iget-object v1, p0, Lmde;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32655
    :cond_0
    iget-object v0, p0, Lmde;->a:Lmlo;

    if-eqz v0, :cond_1

    .line 32656
    const/4 v0, 0x2

    iget-object v1, p0, Lmde;->a:Lmlo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32658
    :cond_1
    iget-object v0, p0, Lmde;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32660
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 32637
    invoke-virtual {p0, p1}, Lmde;->a(Loxn;)Lmde;

    move-result-object v0

    return-object v0
.end method
