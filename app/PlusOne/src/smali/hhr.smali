.class public final Lhhr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    .line 148
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a(I)Lhhr;
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 171
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;Ljava/lang/String;)Lhhr;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lhhr;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.SELECTION"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 163
    if-eqz p2, :cond_0

    .line 164
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.SELECTION_SLIDE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    :cond_0
    return-object p0
.end method

.method public a(Z)Lhhr;
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.RESTRICT_TO_DOMAIN"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 152
    return-object p0
.end method

.method public b(Z)Lhhr;
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.DISABLE_DOMAIN_RESTRICTION_TOGGLE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 157
    return-object p0
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "circle_usage_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 176
    return-void
.end method

.method public c(I)V
    .locals 3

    .prologue
    .line 191
    packed-switch p1, :pswitch_data_0

    .line 202
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.SHAREOUSEL_ORDER"

    invoke-static {}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->n()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 206
    :goto_0
    return-void

    .line 193
    :pswitch_0
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.SHAREOUSEL_ORDER"

    invoke-static {}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->k()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    .line 196
    :pswitch_1
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.SHAREOUSEL_ORDER"

    invoke-static {}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    .line 199
    :pswitch_2
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.SHAREOUSEL_ORDER"

    invoke-static {}, Lcom/google/android/libraries/social/acl/PlusAclPickerActivity;->m()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public c(Z)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.ALLOW_EMPTY_AUDIENCE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 180
    return-void
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.FILTER_NULL_GAIA_IDS"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 184
    return-void
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lhhr;->a:Landroid/content/Intent;

    const-string v1, "acl.PlusAclPickerActivity.INCLUDE_PLUS_PAGES"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 188
    return-void
.end method
