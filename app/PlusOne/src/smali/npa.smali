.class public final Lnpa;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnpa;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1512
    const/4 v0, 0x0

    new-array v0, v0, [Lnpa;

    sput-object v0, Lnpa;->a:[Lnpa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1513
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1516
    const/high16 v0, -0x80000000

    iput v0, p0, Lnpa;->b:I

    .line 1513
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1535
    const/4 v0, 0x0

    .line 1536
    iget v1, p0, Lnpa;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1537
    const/4 v0, 0x1

    iget v1, p0, Lnpa;->b:I

    .line 1538
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1540
    :cond_0
    iget-object v1, p0, Lnpa;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1541
    const/4 v1, 0x2

    iget-object v2, p0, Lnpa;->c:Ljava/lang/String;

    .line 1542
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1544
    :cond_1
    iget-object v1, p0, Lnpa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1545
    iput v0, p0, Lnpa;->ai:I

    .line 1546
    return v0
.end method

.method public a(Loxn;)Lnpa;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1554
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1555
    sparse-switch v0, :sswitch_data_0

    .line 1559
    iget-object v1, p0, Lnpa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1560
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnpa;->ah:Ljava/util/List;

    .line 1563
    :cond_1
    iget-object v1, p0, Lnpa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1565
    :sswitch_0
    return-object p0

    .line 1570
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1571
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 1580
    :cond_2
    iput v0, p0, Lnpa;->b:I

    goto :goto_0

    .line 1582
    :cond_3
    iput v2, p0, Lnpa;->b:I

    goto :goto_0

    .line 1587
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnpa;->c:Ljava/lang/String;

    goto :goto_0

    .line 1555
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1523
    iget v0, p0, Lnpa;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1524
    const/4 v0, 0x1

    iget v1, p0, Lnpa;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1526
    :cond_0
    iget-object v0, p0, Lnpa;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1527
    const/4 v0, 0x2

    iget-object v1, p0, Lnpa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1529
    :cond_1
    iget-object v0, p0, Lnpa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1531
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1509
    invoke-virtual {p0, p1}, Lnpa;->a(Loxn;)Lnpa;

    move-result-object v0

    return-object v0
.end method
