.class public final Ldmg;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmgs;",
        "Lmgt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Z

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;IJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 35
    const-string v3, "photosplusone"

    new-instance v4, Lmgs;

    invoke-direct {v4}, Lmgs;-><init>()V

    new-instance v5, Lmgt;

    invoke-direct {v5}, Lmgt;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 38
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmg;->a:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Ldmg;->b:Ljava/lang/String;

    .line 40
    iput-boolean p8, p0, Ldmg;->p:Z

    .line 41
    iput-object p6, p0, Ldmg;->q:Ljava/lang/String;

    .line 42
    iput-object p7, p0, Ldmg;->r:Ljava/lang/String;

    .line 43
    return-void
.end method

.method private a(ZLoae;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 80
    iget-object v0, p0, Ldmg;->f:Landroid/content/Context;

    iget v1, p0, Ldmg;->c:I

    iget-object v2, p0, Ldmg;->q:Ljava/lang/String;

    iget-object v3, p0, Ldmg;->r:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v0

    .line 82
    if-nez v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 85
    :cond_0
    :try_start_0
    new-instance v1, Lnym;

    invoke-direct {v1}, Lnym;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    if-eqz p2, :cond_2

    .line 93
    :cond_1
    :goto_1
    iput-object p2, v0, Lnym;->r:Loae;

    .line 94
    new-instance v1, Lnzx;

    invoke-direct {v1}, Lnzx;-><init>()V

    .line 95
    iget-object v2, p0, Ldmg;->q:Ljava/lang/String;

    iput-object v2, v1, Lnzx;->b:Ljava/lang/String;

    .line 96
    new-instance v2, Lnzu;

    invoke-direct {v2}, Lnzu;-><init>()V

    .line 97
    iput-object v0, v2, Lnzu;->b:Lnym;

    .line 98
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v1, v0, v2}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 99
    iget-object v0, p0, Ldmg;->f:Landroid/content/Context;

    iget v2, p0, Ldmg;->c:I

    invoke-static {v0, v2, v1, v4}, Ljvj;->a(Landroid/content/Context;ILnzx;I)V

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    const-string v1, "HttpOperation"

    const-string v2, "Unable to parse Photo from byte array."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 92
    :cond_2
    iget-object p2, v0, Lnym;->r:Loae;

    .line 93
    if-nez p2, :cond_4

    if-eqz p1, :cond_3

    new-instance p2, Loae;

    invoke-direct {p2}, Loae;-><init>()V

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p2, Loae;->c:Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, Loae;->e:Ljava/lang/Integer;

    goto :goto_1

    :cond_3
    const/4 p2, 0x0

    goto :goto_1

    :cond_4
    if-eqz p1, :cond_5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p2, Loae;->c:Ljava/lang/Boolean;

    iget-object v1, p2, Loae;->e:Ljava/lang/Integer;

    iget-object v1, p2, Loae;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, Loae;->e:Ljava/lang/Integer;

    goto :goto_1

    :cond_5
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p2, Loae;->c:Ljava/lang/Boolean;

    iget-object v1, p2, Loae;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p2, Loae;->e:Ljava/lang/Integer;

    iget-object v1, p2, Loae;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, Loae;->e:Ljava/lang/Integer;

    goto :goto_1
.end method


# virtual methods
.method public M_()V
    .locals 2

    .prologue
    .line 47
    iget-boolean v0, p0, Ldmg;->p:Z

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldmg;->a(ZLoae;)V

    .line 48
    return-void
.end method

.method protected a(Lmgs;)V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lndl;

    invoke-direct {v0}, Lndl;-><init>()V

    iput-object v0, p1, Lmgs;->a:Lndl;

    .line 53
    iget-object v0, p1, Lmgs;->a:Lndl;

    .line 54
    iget-object v1, p0, Ldmg;->b:Ljava/lang/String;

    iput-object v1, v0, Lndl;->a:Ljava/lang/String;

    .line 55
    iget-object v1, p0, Ldmg;->a:Ljava/lang/String;

    iput-object v1, v0, Lndl;->b:Ljava/lang/String;

    .line 56
    iget-boolean v1, p0, Ldmg;->p:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lndl;->c:Ljava/lang/Boolean;

    .line 57
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lndl;->d:Ljava/lang/Boolean;

    .line 58
    return-void
.end method

.method protected a(Lmgt;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 62
    iget-object v1, p1, Lmgt;->a:Lndm;

    .line 64
    iget-object v2, v1, Lndm;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    .line 66
    iget-boolean v1, p0, Ldmg;->p:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldmg;->a(ZLoae;)V

    .line 70
    :cond_1
    :goto_0
    return-void

    .line 67
    :cond_2
    iget-object v2, v1, Lndm;->b:Loae;

    if-eqz v2, :cond_1

    .line 68
    iget-object v1, v1, Lndm;->b:Loae;

    invoke-direct {p0, v0, v1}, Ldmg;->a(ZLoae;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmgs;

    invoke-virtual {p0, p1}, Ldmg;->a(Lmgs;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmgt;

    invoke-virtual {p0, p1}, Ldmg;->a(Lmgt;)V

    return-void
.end method
