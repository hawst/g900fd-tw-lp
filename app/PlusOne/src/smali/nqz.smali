.class public final Lnqz;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 376
    invoke-direct {p0}, Loxq;-><init>()V

    .line 381
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnqz;->b:[I

    .line 376
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 400
    .line 401
    iget-object v0, p0, Lnqz;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 402
    const/4 v0, 0x1

    iget-object v2, p0, Lnqz;->a:Ljava/lang/String;

    .line 403
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 405
    :goto_0
    iget-object v2, p0, Lnqz;->b:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lnqz;->b:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 407
    iget-object v3, p0, Lnqz;->b:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 409
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 407
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 411
    :cond_0
    add-int/2addr v0, v2

    .line 412
    iget-object v1, p0, Lnqz;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 414
    :cond_1
    iget-object v1, p0, Lnqz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 415
    iput v0, p0, Lnqz;->ai:I

    .line 416
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnqz;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 424
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 425
    sparse-switch v0, :sswitch_data_0

    .line 429
    iget-object v1, p0, Lnqz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 430
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnqz;->ah:Ljava/util/List;

    .line 433
    :cond_1
    iget-object v1, p0, Lnqz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    :sswitch_0
    return-object p0

    .line 440
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqz;->a:Ljava/lang/String;

    goto :goto_0

    .line 444
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 445
    iget-object v0, p0, Lnqz;->b:[I

    array-length v0, v0

    .line 446
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 447
    iget-object v2, p0, Lnqz;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 448
    iput-object v1, p0, Lnqz;->b:[I

    .line 449
    :goto_1
    iget-object v1, p0, Lnqz;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 450
    iget-object v1, p0, Lnqz;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 451
    invoke-virtual {p1}, Loxn;->a()I

    .line 449
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 454
    :cond_2
    iget-object v1, p0, Lnqz;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 425
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 386
    iget-object v0, p0, Lnqz;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 387
    const/4 v0, 0x1

    iget-object v1, p0, Lnqz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 389
    :cond_0
    iget-object v0, p0, Lnqz;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnqz;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 390
    iget-object v1, p0, Lnqz;->b:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 391
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 390
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 394
    :cond_1
    iget-object v0, p0, Lnqz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 396
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 372
    invoke-virtual {p0, p1}, Lnqz;->a(Loxn;)Lnqz;

    move-result-object v0

    return-object v0
.end method
