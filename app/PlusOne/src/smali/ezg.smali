.class public final Lezg;
.super Lhya;
.source "PG"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:I

.field private final c:I

.field private d:Lhym;

.field private e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1, v1}, Lhya;-><init>(Landroid/content/Context;B)V

    move v0, v1

    .line 57
    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    .line 58
    invoke-virtual {p0, v1, v1}, Lezg;->b(ZZ)V

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    new-instance v0, Ljvl;

    invoke-direct {v0, p1}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v0, v0, Ljvl;->a:I

    iput v0, p0, Lezg;->c:I

    .line 61
    const v0, 0x7f0a0b08

    iput v0, p0, Lezg;->b:I

    .line 62
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lezg;->a:Landroid/view/LayoutInflater;

    .line 64
    new-instance v0, Lhym;

    new-array v2, v1, [Ljava/lang/String;

    invoke-direct {v0, v2}, Lhym;-><init>([Ljava/lang/String;)V

    .line 68
    new-array v2, v1, [Ljava/lang/Object;

    .line 69
    invoke-virtual {v0, v2}, Lhym;->a([Ljava/lang/Object;)V

    .line 70
    invoke-virtual {p0, v1, v0}, Lezg;->a(ILandroid/database/Cursor;)V

    .line 71
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 100
    packed-switch p1, :pswitch_data_0

    .line 114
    :cond_0
    :goto_0
    return v0

    .line 102
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 106
    :pswitch_1
    iget-object v1, p0, Lezg;->d:Lhym;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lezg;->d:Lhym;

    invoke-virtual {v0}, Lhym;->getCount()I

    move-result v0

    goto :goto_0

    .line 110
    :pswitch_2
    invoke-super {p0, p1}, Lhya;->a(I)I

    move-result v0

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(II)I
    .locals 1

    .prologue
    .line 175
    packed-switch p1, :pswitch_data_0

    .line 189
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 177
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 181
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 185
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 175
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 122
    packed-switch p2, :pswitch_data_0

    .line 143
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 124
    :pswitch_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040121

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 128
    :pswitch_1
    iget-object v0, p0, Lezg;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f040075

    .line 129
    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 130
    const v1, 0x7f1001c9

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 131
    const v1, 0x7f100237

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 132
    new-instance v1, Llka;

    const/4 v2, 0x2

    const/4 v3, -0x2

    iget v4, p0, Lezg;->c:I

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Llka;-><init>(IIII)V

    .line 134
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 139
    :pswitch_2
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04004e

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 83
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lezg;->a(ILandroid/database/Cursor;)V

    .line 84
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lezg;->d:Lhym;

    :cond_1
    :goto_0
    const/4 v0, 0x1

    iget-object v1, p0, Lezg;->d:Lhym;

    invoke-virtual {p0, v0, v1}, Lezg;->a(ILandroid/database/Cursor;)V

    .line 85
    return-void

    .line 84
    :cond_2
    iget-object v0, p0, Lezg;->d:Lhym;

    if-nez v0, :cond_1

    new-instance v0, Lhym;

    new-array v1, v2, [Ljava/lang/String;

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lezg;->d:Lhym;

    new-array v0, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lezg;->d:Lhym;

    invoke-virtual {v1, v0}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lezg;->e:Landroid/view/View$OnClickListener;

    .line 75
    return-void
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 10

    .prologue
    const/4 v9, -0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 150
    packed-switch p2, :pswitch_data_0

    .line 166
    :goto_0
    return-void

    .line 152
    :pswitch_0
    check-cast p1, Lcom/google/android/apps/plus/views/NewAutoAwesomeMovieTileView;

    new-instance v0, Llka;

    iget v1, p0, Lezg;->c:I

    invoke-direct {v0, v8, v9, v1, v7}, Llka;-><init>(IIII)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/NewAutoAwesomeMovieTileView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lezg;->as()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0b07

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/NewAutoAwesomeMovieTileView;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lezg;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/NewAutoAwesomeMovieTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 157
    :pswitch_1
    const v0, 0x7f1001c7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0a0b08

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 162
    :pswitch_2
    check-cast p1, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;

    const/4 v0, 0x3

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x5

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3}, Ljvj;->a(J)Ljac;

    move-result-object v5

    iget-object v6, p0, Lezg;->k:Landroid/content/Context;

    invoke-static {v6, v1, v4, v5}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f10007f

    invoke-virtual {p1, v5, v4}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->setTag(ILjava/lang/Object;)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->a(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->a(Lizu;)V

    iget-object v0, p0, Lezg;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100093

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->setTag(ILjava/lang/Object;)V

    new-instance v0, Llka;

    iget v1, p0, Lezg;->c:I

    invoke-direct {v0, v8, v9, v1, v7}, Llka;-><init>(IIII)V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/AutoAwesomeMovieTileView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x3

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method
