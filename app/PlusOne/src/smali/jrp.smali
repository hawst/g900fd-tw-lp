.class final Ljrp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lijt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lijt",
        "<",
        "Liox;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Ljrm;


# direct methods
.method constructor <init>(Ljrm;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Ljrp;->a:Ljrm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lijs;)V
    .locals 0

    .prologue
    .line 260
    check-cast p1, Liox;

    invoke-virtual {p0, p1}, Ljrp;->a(Liox;)V

    return-void
.end method

.method public a(Liox;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 263
    iget-object v2, p0, Ljrp;->a:Ljrm;

    invoke-interface {p1}, Liox;->a()Lika;

    move-result-object v1

    invoke-interface {v1}, Lika;->b()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Ljpe;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "isSuccess: %s. People count: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p1}, Liox;->a()Lika;

    move-result-object v3

    invoke-interface {v3}, Lika;->b()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    invoke-interface {p1}, Liox;->b()Liqi;

    move-result-object v3

    invoke-interface {v3}, Liqi;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Ljpe;->b()V

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    invoke-interface {p1}, Liox;->b()Liqi;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3}, Liqi;->b()I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_4

    invoke-interface {v3, v1}, Liqi;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liqh;

    new-instance v7, Ljry;

    iget-object v8, v2, Ljrm;->b:Ljava/util/HashMap;

    invoke-direct {v7, v0, v8}, Ljry;-><init>(Liqh;Ljava/util/HashMap;)V

    invoke-interface {v7}, Ljpv;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljod;

    invoke-interface {v0}, Ljod;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v6, v9, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    invoke-interface {v3}, Liqi;->a()V

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    sget-object v3, Ljru;->b:Ljava/util/Comparator;

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_3

    :cond_5
    monitor-enter v2

    :try_start_0
    iput-object v5, v2, Ljrm;->c:Ljava/util/ArrayList;

    iput-object v6, v2, Ljrm;->d:Ljava/util/HashMap;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x4

    invoke-virtual {v2, v0}, Ljrm;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
