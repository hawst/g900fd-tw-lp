.class final Ljfm;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Ljgf;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljfy;",
            ">;"
        }
    .end annotation
.end field

.field private b:[I

.field private c:I

.field private d:Ljfn;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljge;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:J


# direct methods
.method public constructor <init>(Ljfn;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 50
    iput-object p1, p0, Ljfm;->d:Ljfn;

    .line 51
    return-void
.end method

.method private a(I)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 103
    if-ltz p1, :cond_0

    iget v0, p0, Ljfm;->c:I

    if-lt p1, v0, :cond_2

    .line 104
    :cond_0
    const/4 v0, -0x1

    .line 115
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 110
    :goto_1
    iget-object v2, p0, Ljfm;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 111
    iget-object v2, p0, Ljfm;->b:[I

    aget v2, v2, v0

    add-int/lit8 v2, v2, -0x1

    if-ge v2, p1, :cond_1

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 115
    goto :goto_0
.end method

.method private a(II)I
    .locals 2

    .prologue
    .line 124
    if-nez p1, :cond_0

    :goto_0
    return p2

    :cond_0
    iget-object v0, p0, Ljfm;->b:[I

    add-int/lit8 v1, p1, -0x1

    aget v0, v0, v1

    sub-int/2addr p2, v0

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 87
    iput v0, p0, Ljfm;->c:I

    .line 89
    iget-object v1, p0, Ljfm;->a:Ljava/util/List;

    if-eqz v1, :cond_0

    move v1, v0

    .line 90
    :goto_0
    iget-object v0, p0, Ljfm;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 91
    iget v2, p0, Ljfm;->c:I

    iget-object v0, p0, Ljfm;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    invoke-virtual {v0}, Ljfy;->a()I

    move-result v0

    add-int/2addr v0, v2

    iput v0, p0, Ljfm;->c:I

    .line 92
    iget-object v0, p0, Ljfm;->b:[I

    iget v2, p0, Ljfm;->c:I

    aput v2, v0, v1

    .line 90
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 95
    :cond_0
    invoke-virtual {p0}, Ljfm;->notifyDataSetChanged()V

    .line 96
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljfy;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 58
    iput-object p1, p0, Ljfm;->a:Ljava/util/List;

    .line 60
    iget-object v0, p0, Ljfm;->a:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 61
    iget-object v0, p0, Ljfm;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 62
    iget-object v0, p0, Ljfm;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljfm;->b:[I

    array-length v0, v0

    if-eq v0, v2, :cond_1

    .line 63
    :cond_0
    new-array v0, v2, [I

    iput-object v0, p0, Ljfm;->b:[I

    .line 66
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljfm;->e:Ljava/util/List;

    .line 67
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 68
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 69
    instance-of v3, v0, Ljge;

    if-eqz v3, :cond_2

    .line 70
    check-cast v0, Ljge;

    .line 71
    iget-object v3, p0, Ljfm;->e:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 75
    :cond_3
    invoke-virtual {p0}, Ljfm;->a()V

    .line 76
    return-void
.end method

.method public a(Ljava/lang/String;J)Z
    .locals 2

    .prologue
    .line 267
    iget-wide v0, p0, Ljfm;->g:J

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljfm;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 268
    :cond_0
    iput-wide p2, p0, Ljfm;->g:J

    .line 269
    iput-object p1, p0, Ljfm;->f:Ljava/lang/String;

    .line 270
    invoke-virtual {p0}, Ljfm;->notifyDataSetChanged()V

    .line 272
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljfy;
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Ljfm;->c:I

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Ljfm;->b()Ljfy;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 159
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3

    .prologue
    .line 245
    invoke-direct {p0, p1}, Ljfm;->a(I)I

    move-result v0

    .line 246
    invoke-direct {p0, v0, p1}, Ljfm;->a(II)I

    move-result v1

    .line 247
    iget-object v2, p0, Ljfm;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 248
    invoke-virtual {v0, v1}, Ljfy;->d(I)I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 249
    const/4 v0, 0x1

    .line 251
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    .prologue
    .line 177
    invoke-direct/range {p0 .. p1}, Ljfm;->a(I)I

    move-result v6

    .line 178
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v6, v1}, Ljfm;->a(II)I

    move-result v8

    .line 179
    move-object/from16 v0, p0

    iget-object v2, v0, Ljfm;->a:Ljava/util/List;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljfy;

    .line 180
    invoke-virtual {v2, v8}, Ljfy;->d(I)I

    move-result v9

    .line 181
    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v10

    .line 183
    const/4 v3, 0x6

    if-ne v9, v3, :cond_1

    .line 185
    if-nez p2, :cond_0

    .line 186
    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f0400ef

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v3, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 187
    const v3, 0x7f10033e

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 188
    const/4 v4, 0x5

    invoke-static {v10, v3, v4}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 189
    const/4 v4, 0x5

    invoke-static {v10, v4}, Llib;->a(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMinimumHeight(I)V

    .line 191
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 195
    :goto_0
    invoke-virtual {v2}, Ljfy;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    :goto_1
    return-object p2

    .line 193
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    goto :goto_0

    .line 198
    :cond_1
    if-nez p2, :cond_4

    .line 199
    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f0400ee

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v3, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 200
    new-instance v3, Ljgc;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Ljgc;-><init>(Landroid/view/View;)V

    .line 201
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v4, v3

    .line 206
    :goto_2
    invoke-virtual {v2, v8}, Ljfy;->e(I)J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljfm;->g:J

    cmp-long v3, v12, v14

    if-nez v3, :cond_5

    .line 207
    invoke-virtual {v2}, Ljfy;->w()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Ljfm;->f:Ljava/lang/String;

    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    move v5, v3

    .line 212
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Ljfm;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v6, v3, :cond_6

    const/4 v3, 0x1

    move v7, v3

    .line 213
    :goto_4
    if-eqz v7, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Ljfm;->a:Ljava/util/List;

    add-int/lit8 v6, v6, 0x1

    .line 214
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljfy;

    .line 216
    :goto_5
    const/4 v6, 0x0

    .line 217
    invoke-virtual {v2}, Ljfy;->a()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    if-ne v8, v11, :cond_3

    .line 218
    invoke-virtual {v2}, Ljfy;->q()Z

    move-result v11

    if-nez v11, :cond_2

    if-eqz v7, :cond_3

    .line 219
    invoke-virtual {v3}, Ljfy;->q()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 220
    :cond_2
    const/4 v6, 0x1

    .line 224
    :cond_3
    if-eqz v6, :cond_9

    .line 225
    if-eqz v7, :cond_9

    .line 226
    invoke-virtual {v3}, Ljfy;->r()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual {v3}, Ljfy;->m()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 227
    invoke-virtual {v2}, Ljfy;->r()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2}, Ljfy;->m()Z

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, 0x1

    .line 232
    :goto_6
    invoke-virtual {v4, v10, v9, v5, v3}, Ljgc;->a(Landroid/content/Context;IZZ)V

    .line 233
    invoke-virtual {v2, v8, v4}, Ljfy;->a(ILjgb;)V

    goto/16 :goto_1

    .line 203
    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljgc;

    move-object v4, v3

    goto :goto_2

    .line 207
    :cond_5
    const/4 v3, 0x0

    move v5, v3

    goto :goto_3

    .line 212
    :cond_6
    const/4 v3, 0x0

    move v7, v3

    goto :goto_4

    .line 214
    :cond_7
    const/4 v3, 0x0

    goto :goto_5

    .line 227
    :cond_8
    const/4 v3, 0x0

    goto :goto_6

    :cond_9
    move v3, v6

    goto :goto_6
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Ljfm;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 134
    invoke-direct {p0, p1}, Ljfm;->a(I)I

    move-result v1

    .line 135
    iget-object v0, p0, Ljfm;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    invoke-direct {p0, v1, p1}, Ljfm;->a(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljfy;->c(I)Z

    move-result v0

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 285
    const/4 v0, 0x4

    invoke-static {p2, v0}, Lhly;->a(Landroid/view/View;I)V

    .line 286
    invoke-direct {p0, p3}, Ljfm;->a(I)I

    move-result v5

    .line 287
    invoke-direct {p0, v5, p3}, Ljfm;->a(II)I

    move-result v6

    .line 288
    iget-object v0, p0, Ljfm;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 289
    invoke-virtual {v0, v6}, Ljfy;->d(I)I

    move-result v1

    .line 290
    const/4 v4, 0x6

    if-ne v1, v4, :cond_1

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 296
    :cond_1
    packed-switch v1, :pswitch_data_0

    move v1, v3

    :goto_1
    if-eqz v1, :cond_3

    .line 297
    invoke-virtual {v0}, Ljfy;->x()V

    move v0, v2

    .line 309
    :goto_2
    iget-object v1, p0, Ljfm;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 310
    :goto_3
    if-ge v3, v1, :cond_4

    .line 311
    if-eq v3, v5, :cond_2

    .line 312
    iget-object v0, p0, Ljfm;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfy;

    .line 313
    invoke-virtual {v0}, Ljfy;->o()V

    move v0, v2

    .line 310
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :pswitch_0
    move v1, v2

    .line 296
    goto :goto_1

    .line 299
    :cond_3
    iget-object v1, p0, Ljfm;->d:Ljfn;

    if-eqz v1, :cond_5

    .line 300
    iget-object v1, p0, Ljfm;->d:Ljfn;

    invoke-virtual {v0, v6}, Ljfy;->a(I)Landroid/content/Intent;

    move-result-object v4

    invoke-interface {v1, v4}, Ljfn;->b(Landroid/content/Intent;)Z

    .line 302
    iget-object v1, p0, Ljfm;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    move v4, v3

    .line 303
    :goto_4
    if-ge v4, v7, :cond_5

    .line 304
    iget-object v1, p0, Ljfm;->e:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljge;

    .line 305
    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v8, v6}, Ljge;->a(Ljava/lang/String;I)V

    .line 303
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_4

    .line 318
    :cond_4
    if-eqz v0, :cond_0

    .line 319
    invoke-virtual {p0}, Ljfm;->a()V

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_2

    .line 296
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
