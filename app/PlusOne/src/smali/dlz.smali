.class public final Ldlz;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmgi;",
        "Lmgj;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private final b:J

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:Ljava/lang/String;

.field private final s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 36
    const-string v4, "photosdeletetag"

    new-instance v5, Lmgi;

    invoke-direct {v5}, Lmgi;-><init>()V

    new-instance v6, Lmgj;

    invoke-direct {v6}, Lmgj;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v1 .. v6}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 39
    iput-wide p3, p0, Ldlz;->a:J

    .line 40
    iput-wide p5, p0, Ldlz;->b:J

    .line 41
    iput-object p7, p0, Ldlz;->p:Ljava/lang/String;

    .line 42
    iput-object p8, p0, Ldlz;->q:Ljava/lang/String;

    .line 43
    move-object/from16 v0, p9

    iput-object v0, p0, Ldlz;->r:Ljava/lang/String;

    .line 44
    move/from16 v0, p10

    iput-boolean v0, p0, Ldlz;->s:Z

    .line 45
    return-void
.end method


# virtual methods
.method protected a(Lmgi;)V
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lned;

    invoke-direct {v0}, Lned;-><init>()V

    iput-object v0, p1, Lmgi;->a:Lned;

    .line 50
    iget-object v0, p1, Lmgi;->a:Lned;

    .line 51
    iget-wide v2, p0, Ldlz;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lned;->a:Ljava/lang/String;

    .line 52
    iget-wide v2, p0, Ldlz;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lned;->b:Ljava/lang/String;

    .line 53
    iget-object v1, p0, Ldlz;->p:Ljava/lang/String;

    iput-object v1, v0, Lned;->d:Ljava/lang/String;

    .line 54
    iget-boolean v1, p0, Ldlz;->s:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lned;->c:Ljava/lang/Boolean;

    .line 55
    return-void
.end method

.method protected a(Lmgj;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v0, p1, Lmgj;->a:Lnfh;

    .line 60
    iget-object v0, v0, Lnfh;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Ldlz;->f:Landroid/content/Context;

    iget v1, p0, Ldlz;->c:I

    iget-object v3, p0, Ldlz;->r:Ljava/lang/String;

    iget-object v4, p0, Ldlz;->q:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v0, v1, v3, v4, v5}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    .line 69
    :try_start_0
    new-instance v1, Lnym;

    invoke-direct {v1}, Lnym;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    iget-object v1, v0, Lnym;->g:[Lnys;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lnym;->g:[Lnys;

    array-length v1, v1

    if-eqz v1, :cond_0

    move v1, v2

    .line 84
    :goto_1
    iget-object v3, v0, Lnym;->g:[Lnys;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 85
    iget-wide v4, p0, Ldlz;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lnym;->g:[Lnys;

    aget-object v4, v4, v1

    iget-object v4, v4, Lnys;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 86
    iget-object v3, v0, Lnym;->g:[Lnys;

    .line 87
    iget-boolean v4, p0, Ldlz;->s:Z

    if-eqz v4, :cond_3

    .line 88
    iget-object v3, v0, Lnym;->g:[Lnys;

    aget-object v3, v3, v1

    const/4 v4, 0x0

    iput-object v4, v3, Lnys;->c:Lnyz;

    .line 89
    iget-object v3, v0, Lnym;->g:[Lnys;

    aget-object v1, v3, v1

    const/4 v3, 0x4

    iput v3, v1, Lnys;->d:I

    .line 99
    :cond_2
    :goto_2
    new-instance v1, Lnzx;

    invoke-direct {v1}, Lnzx;-><init>()V

    .line 100
    iget-object v3, p0, Ldlz;->r:Ljava/lang/String;

    iput-object v3, v1, Lnzx;->b:Ljava/lang/String;

    .line 101
    new-instance v3, Lnzu;

    invoke-direct {v3}, Lnzu;-><init>()V

    .line 102
    iput-object v0, v3, Lnzu;->b:Lnym;

    .line 103
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v1, v0, v3}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 104
    iget-object v0, p0, Ldlz;->f:Landroid/content/Context;

    iget v3, p0, Ldlz;->c:I

    invoke-static {v0, v3, v1, v2}, Ljvj;->a(Landroid/content/Context;ILnzx;I)V

    goto :goto_0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    const-string v1, "HttpOperation"

    const-string v2, "Unable to parse Photo from byte array."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 91
    :cond_3
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    new-array v4, v4, [Lnys;

    iput-object v4, v0, Lnym;->g:[Lnys;

    .line 92
    iget-object v4, v0, Lnym;->g:[Lnys;

    invoke-static {v3, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    add-int/lit8 v4, v1, 0x1

    iget-object v5, v0, Lnym;->g:[Lnys;

    iget-object v6, v0, Lnym;->g:[Lnys;

    array-length v6, v6

    sub-int/2addr v6, v1

    invoke-static {v3, v4, v5, v1, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_2

    .line 84
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmgi;

    invoke-virtual {p0, p1}, Ldlz;->a(Lmgi;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmgj;

    invoke-virtual {p0, p1}, Ldlz;->a(Lmgj;)V

    return-void
.end method
