.class Lxf;
.super Lxb;
.source "PG"

# interfaces
.implements Lwk;


# instance fields
.field private o:Lwj;

.field private p:Lwm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lxk;)V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0, p1, p2}, Lxb;-><init>(Landroid/content/Context;Lxk;)V

    .line 717
    return-void
.end method


# virtual methods
.method protected a(Lxd;Lvd;)V
    .locals 1

    .prologue
    .line 742
    invoke-super {p0, p1, p2}, Lxb;->a(Lxd;Lvd;)V

    .line 744
    iget-object v0, p1, Lxd;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 745
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lvd;->a(Z)Lvd;

    .line 748
    :cond_0
    invoke-virtual {p0, p1}, Lxf;->b(Lxd;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lvd;->b(Z)Lvd;

    .line 752
    :cond_1
    iget-object v0, p1, Lxd;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    .line 754
    if-eqz v0, :cond_2

    .line 755
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    invoke-virtual {p2, v0}, Lvd;->f(I)Lvd;

    .line 757
    :cond_2
    return-void
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 761
    invoke-super {p0}, Lxb;->b()V

    .line 763
    iget-object v0, p0, Lxf;->o:Lwj;

    if-nez v0, :cond_0

    .line 764
    new-instance v0, Lwj;

    iget-object v1, p0, Lvf;->a:Landroid/content/Context;

    iget-object v2, p0, Lvf;->c:Lvh;

    invoke-direct {v0, v1, v2}, Lwj;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lxf;->o:Lwj;

    .line 767
    :cond_0
    iget-object v1, p0, Lxf;->o:Lwj;

    iget-boolean v0, p0, Lxf;->l:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lxf;->k:I

    :goto_0
    invoke-virtual {v1, v0}, Lwj;->a(I)V

    .line 768
    return-void

    .line 767
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Lxd;)Z
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Lxf;->p:Lwm;

    if-nez v0, :cond_0

    .line 777
    new-instance v0, Lwm;

    invoke-direct {v0}, Lwm;-><init>()V

    iput-object v0, p0, Lxf;->p:Lwm;

    .line 779
    :cond_0
    iget-object v0, p0, Lxf;->p:Lwm;

    iget-object v1, p1, Lxd;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lwm;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 772
    new-instance v0, Lwl;

    invoke-direct {v0, p0}, Lwl;-><init>(Lwk;)V

    return-object v0
.end method

.method public f(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 721
    invoke-virtual {p0, p1}, Lxf;->g(Ljava/lang/Object;)I

    move-result v0

    .line 722
    if-ltz v0, :cond_0

    .line 723
    iget-object v1, p0, Lxf;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lxd;

    .line 724
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v1

    .line 726
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    .line 728
    :goto_0
    iget-object v2, v0, Lxd;->c:Lvc;

    invoke-virtual {v2}, Lvc;->m()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 730
    new-instance v2, Lvd;

    iget-object v3, v0, Lxd;->c:Lvc;

    invoke-direct {v2, v3}, Lvd;-><init>(Lvc;)V

    invoke-virtual {v2, v1}, Lvd;->f(I)Lvd;

    move-result-object v1

    invoke-virtual {v1}, Lvd;->a()Lvc;

    move-result-object v1

    iput-object v1, v0, Lxd;->c:Lvc;

    .line 734
    invoke-virtual {p0}, Lxf;->a()V

    .line 737
    :cond_0
    return-void

    .line 726
    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method
