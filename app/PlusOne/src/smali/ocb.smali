.class public final Locb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Locb;


# instance fields
.field public b:I

.field public c:[I

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 584
    const/4 v0, 0x0

    new-array v0, v0, [Locb;

    sput-object v0, Locb;->a:[Locb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 585
    invoke-direct {p0}, Loxq;-><init>()V

    .line 588
    const/high16 v0, -0x80000000

    iput v0, p0, Locb;->b:I

    .line 591
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Locb;->c:[I

    .line 585
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 620
    .line 621
    iget-object v1, p0, Locb;->c:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Locb;->c:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 623
    iget-object v2, p0, Locb;->c:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 625
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 623
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 628
    :cond_0
    iget-object v0, p0, Locb;->c:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 630
    :cond_1
    iget-object v1, p0, Locb;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 631
    const/4 v1, 0x2

    iget-object v2, p0, Locb;->d:Ljava/lang/Boolean;

    .line 632
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 634
    :cond_2
    iget-object v1, p0, Locb;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 635
    const/4 v1, 0x3

    iget-object v2, p0, Locb;->e:Ljava/lang/Boolean;

    .line 636
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 638
    :cond_3
    iget v1, p0, Locb;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 639
    const/4 v1, 0x4

    iget v2, p0, Locb;->b:I

    .line 640
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 642
    :cond_4
    iget-object v1, p0, Locb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 643
    iput v0, p0, Locb;->ai:I

    .line 644
    return v0
.end method

.method public a(Loxn;)Locb;
    .locals 6

    .prologue
    const/16 v5, 0x12

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 652
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 653
    sparse-switch v0, :sswitch_data_0

    .line 657
    iget-object v1, p0, Locb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 658
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Locb;->ah:Ljava/util/List;

    .line 661
    :cond_1
    iget-object v1, p0, Locb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 663
    :sswitch_0
    return-object p0

    .line 668
    :sswitch_1
    invoke-static {p1, v4}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 669
    iget-object v0, p0, Locb;->c:[I

    array-length v0, v0

    .line 670
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 671
    iget-object v2, p0, Locb;->c:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 672
    iput-object v1, p0, Locb;->c:[I

    .line 673
    :goto_1
    iget-object v1, p0, Locb;->c:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 674
    iget-object v1, p0, Locb;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 675
    invoke-virtual {p1}, Loxn;->a()I

    .line 673
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 678
    :cond_2
    iget-object v1, p0, Locb;->c:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 682
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Locb;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 686
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Locb;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 690
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 691
    if-eq v0, v5, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/4 v1, 0x7

    if-eq v0, v1, :cond_3

    const/16 v1, 0x13

    if-eq v0, v1, :cond_3

    if-eq v0, v4, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0x10

    if-eq v0, v1, :cond_3

    const/16 v1, 0x11

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_3

    const/16 v1, 0x14

    if-eq v0, v1, :cond_3

    const/16 v1, 0xe

    if-eq v0, v1, :cond_3

    const/16 v1, 0xf

    if-eq v0, v1, :cond_3

    const/16 v1, 0x19

    if-eq v0, v1, :cond_3

    const/16 v1, 0x20

    if-eq v0, v1, :cond_3

    const/16 v1, 0x15

    if-eq v0, v1, :cond_3

    const/16 v1, 0x16

    if-eq v0, v1, :cond_3

    const/16 v1, 0x17

    if-eq v0, v1, :cond_3

    const/16 v1, 0x18

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x1f

    if-ne v0, v1, :cond_4

    .line 723
    :cond_3
    iput v0, p0, Locb;->b:I

    goto/16 :goto_0

    .line 725
    :cond_4
    iput v5, p0, Locb;->b:I

    goto/16 :goto_0

    .line 653
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 600
    iget-object v0, p0, Locb;->c:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Locb;->c:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 601
    iget-object v1, p0, Locb;->c:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 602
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 601
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 605
    :cond_0
    iget-object v0, p0, Locb;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 606
    const/4 v0, 0x2

    iget-object v1, p0, Locb;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 608
    :cond_1
    iget-object v0, p0, Locb;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 609
    const/4 v0, 0x3

    iget-object v1, p0, Locb;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 611
    :cond_2
    iget v0, p0, Locb;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 612
    const/4 v0, 0x4

    iget v1, p0, Locb;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 614
    :cond_3
    iget-object v0, p0, Locb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 616
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 581
    invoke-virtual {p0, p1}, Locb;->a(Loxn;)Locb;

    move-result-object v0

    return-object v0
.end method
