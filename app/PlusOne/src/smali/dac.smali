.class public Ldac;
.super Llol;
.source "PG"


# instance fields
.field private final N:Ldao;

.field private final O:Ldag;

.field private final P:Ldah;

.field private final Q:Ldai;

.field private final R:Ldal;

.field private final S:Ldak;

.field private final T:Ldaj;

.field private final U:Ldam;

.field private V:Lhee;

.field private W:Lely;

.field private X:Ldgr;

.field private Y:Ldgm;

.field private Z:Ldgi;

.field private aa:Lczv;

.field private ab:Ldeo;

.field private ac:Landroid/view/View;

.field private ad:Landroid/view/View;

.field private ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

.field private af:Ljava/lang/Integer;

.field private ag:Lczz;

.field private ah:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 59
    invoke-direct {p0}, Llol;-><init>()V

    .line 66
    new-instance v0, Ldao;

    invoke-direct {v0, p0}, Ldao;-><init>(Ldac;)V

    iput-object v0, p0, Ldac;->N:Ldao;

    .line 67
    new-instance v0, Ldag;

    invoke-direct {v0, p0}, Ldag;-><init>(Ldac;)V

    iput-object v0, p0, Ldac;->O:Ldag;

    .line 68
    new-instance v0, Ldah;

    invoke-direct {v0, p0}, Ldah;-><init>(Ldac;)V

    iput-object v0, p0, Ldac;->P:Ldah;

    .line 70
    new-instance v0, Ldai;

    invoke-direct {v0}, Ldai;-><init>()V

    iput-object v0, p0, Ldac;->Q:Ldai;

    .line 72
    new-instance v0, Ldal;

    invoke-direct {v0}, Ldal;-><init>()V

    iput-object v0, p0, Ldac;->R:Ldal;

    .line 73
    new-instance v0, Ldak;

    invoke-direct {v0, p0}, Ldak;-><init>(Ldac;)V

    iput-object v0, p0, Ldac;->S:Ldak;

    .line 75
    new-instance v0, Ldaj;

    invoke-direct {v0, p0}, Ldaj;-><init>(Ldac;)V

    iput-object v0, p0, Ldac;->T:Ldaj;

    .line 77
    new-instance v0, Ldam;

    invoke-direct {v0, p0}, Ldam;-><init>(Ldac;)V

    iput-object v0, p0, Ldac;->U:Ldam;

    .line 100
    new-instance v0, Ldgn;

    iget-object v1, p0, Ldac;->av:Llqm;

    sget-object v2, Ldgx;->R:Ldgo;

    new-instance v3, Ldad;

    invoke-direct {v3, p0}, Ldad;-><init>(Ldac;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 106
    new-instance v0, Ldep;

    iget-object v1, p0, Ldac;->av:Llqm;

    new-instance v2, Ldae;

    invoke-direct {v2, p0}, Ldae;-><init>(Ldac;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 476
    return-void
.end method

.method static synthetic a(Ldac;)Ldgm;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->Y:Ldgm;

    return-object v0
.end method

.method static synthetic a(Ldac;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Ldac;->af:Ljava/lang/Integer;

    return-object p1
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Ldac;->ab:Ldeo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldac;->ab:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldac;->ab:Ldeo;

    .line 195
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ldac;Z)Z
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Ldac;->ah:Z

    return p1
.end method

.method static synthetic b(Ldac;)Z
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ldac;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Ldac;)Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Ldac;->ah:Z

    return v0
.end method

.method static synthetic d(Ldac;)V
    .locals 4

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->ac:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Ldac;->a()Z

    move-result v1

    iget-object v0, p0, Ldac;->ab:Ldeo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldac;->ab:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldac;->ab:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->x()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Ldac;->Y:Ldgm;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldac;->ac:Landroid/view/View;

    :goto_1
    iget-object v3, p0, Ldac;->Q:Ldai;

    invoke-interface {v2, v0, v3}, Ldgm;->a(Landroid/view/View;Llix;)V

    iget-object v0, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    iget-object v2, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    if-eqz v1, :cond_4

    const v0, 0x7f0a077b

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setHint(I)V

    :cond_0
    iget-object v0, p0, Ldac;->X:Ldgr;

    sget-object v2, Ldgx;->R:Ldgo;

    invoke-virtual {v0, v2, v1}, Ldgr;->a(Ldgu;Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const v0, 0x7f0a077d

    goto :goto_2
.end method

.method static synthetic e(Ldac;)Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->ad:Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Ldac;)Ldgr;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->X:Ldgr;

    return-object v0
.end method

.method static synthetic g(Ldac;)Ldeo;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->ab:Ldeo;

    return-object v0
.end method

.method static synthetic h(Ldac;)Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic i(Ldac;)Lhee;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->V:Lhee;

    return-object v0
.end method

.method static synthetic j(Ldac;)Lczz;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->ag:Lczz;

    return-object v0
.end method

.method static synthetic k(Ldac;)Ldgi;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->Z:Ldgi;

    return-object v0
.end method

.method static synthetic l(Ldac;)Llnh;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->au:Llnh;

    return-object v0
.end method

.method static synthetic m(Ldac;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldac;->af:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 137
    const v0, 0x7f040180

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldac;->ac:Landroid/view/View;

    .line 139
    iget-object v0, p0, Ldac;->ac:Landroid/view/View;

    const v1, 0x102000a

    .line 140
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photos/viewer/components/comments/DetailsListView;

    .line 141
    iget-object v1, p0, Ldac;->aa:Lczv;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 142
    iget-object v1, p0, Ldac;->R:Ldal;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/comments/DetailsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 144
    iget-object v0, p0, Ldac;->V:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v4

    .line 146
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Ldac;->n()Lz;

    move-result-object v0

    const v2, 0x7f0901dd

    invoke-direct {v1, v0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 148
    new-instance v0, Lely;

    invoke-virtual {p0}, Ldac;->p()Lae;

    move-result-object v2

    .line 149
    invoke-virtual {p0}, Ldac;->w()Lbb;

    move-result-object v3

    invoke-direct/range {v0 .. v5}, Lely;-><init>(Landroid/content/Context;Lae;Lbb;II)V

    iput-object v0, p0, Ldac;->W:Lely;

    .line 151
    iget-object v0, p0, Ldac;->ac:Landroid/view/View;

    const v1, 0x7f10029a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    .line 153
    iget-object v0, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    .line 154
    iget-object v0, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    const v1, 0x7f0a077d

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setHint(I)V

    .line 155
    iget-object v0, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Ldac;->W:Lely;

    invoke-virtual {v0, v6, v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Lkjm;Ljpj;)V

    .line 156
    iget-object v0, p0, Ldac;->ac:Landroid/view/View;

    const v1, 0x7f10029b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldac;->ad:Landroid/view/View;

    .line 157
    iget-object v0, p0, Ldac;->ad:Landroid/view/View;

    iget-object v1, p0, Ldac;->P:Ldah;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    iget-object v0, p0, Ldac;->ad:Landroid/view/View;

    invoke-direct {p0}, Ldac;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v5, 0x1

    :cond_0
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 159
    iget-object v0, p0, Ldac;->ae:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    new-instance v1, Ldaf;

    invoke-direct {v1, p0}, Ldaf;-><init>(Ldac;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 174
    invoke-virtual {p0}, Ldac;->q()Lae;

    move-result-object v1

    .line 175
    const-class v0, Lczz;

    .line 176
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lczz;

    iput-object v0, p0, Ldac;->ag:Lczz;

    .line 178
    iget-object v0, p0, Ldac;->ag:Lczz;

    if-nez v0, :cond_1

    .line 179
    new-instance v0, Lczz;

    invoke-direct {v0}, Lczz;-><init>()V

    iput-object v0, p0, Ldac;->ag:Lczz;

    .line 180
    invoke-virtual {p0}, Ldac;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 181
    iget-object v2, p0, Ldac;->ag:Lczz;

    invoke-virtual {v2, v0}, Lczz;->f(Landroid/os/Bundle;)V

    .line 182
    iget-object v0, p0, Ldac;->ag:Lczz;

    iget-object v2, p0, Ldac;->T:Ldaj;

    invoke-virtual {v0, v2}, Lczz;->a(Ldab;)V

    .line 183
    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v0

    iget-object v1, p0, Ldac;->ag:Lczz;

    const-class v2, Lczz;

    .line 184
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v0

    .line 185
    invoke-virtual {v0}, Lat;->b()I

    .line 190
    :cond_1
    return-object v6
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 234
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 235
    if-nez p1, :cond_1

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    const-string v0, "comment_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    const-string v0, "comment_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldac;->af:Ljava/lang/Integer;

    .line 242
    :cond_2
    const-string v0, "launched_comments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    const-string v0, "launched_comments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldac;->ah:Z

    goto :goto_0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 211
    invoke-super {p0}, Llol;->aO_()V

    .line 213
    iget-object v0, p0, Ldac;->Z:Ldgi;

    iget-object v1, p0, Ldac;->S:Ldak;

    invoke-interface {v0, v1}, Ldgi;->a(Ldgl;)V

    .line 214
    iget-object v0, p0, Ldac;->U:Ldam;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 215
    return-void
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 205
    invoke-super {p0}, Llol;->ae_()V

    .line 206
    const/4 v0, 0x0

    iput-object v0, p0, Ldac;->ac:Landroid/view/View;

    .line 207
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 122
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 124
    iget-object v0, p0, Ldac;->au:Llnh;

    const-class v1, Ldgm;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgm;

    iput-object v0, p0, Ldac;->Y:Ldgm;

    .line 125
    iget-object v0, p0, Ldac;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Ldac;->X:Ldgr;

    .line 126
    iget-object v0, p0, Ldac;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Ldac;->Z:Ldgi;

    .line 127
    iget-object v0, p0, Ldac;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Ldac;->ab:Ldeo;

    .line 128
    iget-object v0, p0, Ldac;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Ldac;->V:Lhee;

    .line 130
    new-instance v0, Lczv;

    iget-object v1, p0, Ldac;->at:Llnl;

    iget-object v2, p0, Ldac;->N:Ldao;

    iget-object v3, p0, Ldac;->O:Ldag;

    invoke-direct {v0, v1, v2, v3}, Lczv;-><init>(Landroid/content/Context;Ldax;Lldh;)V

    iput-object v0, p0, Ldac;->aa:Lczv;

    .line 132
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 249
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 250
    const-string v0, "launched_comments"

    iget-boolean v1, p0, Ldac;->ah:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 251
    iget-object v0, p0, Ldac;->af:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 252
    const-string v0, "comment_request_id"

    iget-object v1, p0, Ldac;->af:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 254
    :cond_0
    return-void
.end method

.method public z()V
    .locals 3

    .prologue
    .line 219
    invoke-super {p0}, Llol;->z()V

    .line 221
    iget-object v0, p0, Ldac;->Z:Ldgi;

    iget-object v1, p0, Ldac;->S:Ldak;

    invoke-interface {v0, v1}, Ldgi;->b(Ldgl;)V

    .line 223
    iget-object v0, p0, Ldac;->U:Ldam;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 224
    iget-object v0, p0, Ldac;->af:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Ldac;->af:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    iget-object v0, p0, Ldac;->af:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 227
    iget-object v1, p0, Ldac;->U:Ldam;

    iget-object v2, p0, Ldac;->af:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Ldam;->d(ILfib;)Z

    .line 230
    :cond_0
    return-void
.end method
