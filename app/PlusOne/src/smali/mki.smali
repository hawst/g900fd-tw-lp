.class public final Lmki;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvv;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8341
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8344
    iput-object v0, p0, Lmki;->apiHeader:Llyq;

    .line 8347
    iput-object v0, p0, Lmki;->a:Lnvv;

    .line 8341
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8364
    const/4 v0, 0x0

    .line 8365
    iget-object v1, p0, Lmki;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 8366
    const/4 v0, 0x1

    iget-object v1, p0, Lmki;->apiHeader:Llyq;

    .line 8367
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8369
    :cond_0
    iget-object v1, p0, Lmki;->a:Lnvv;

    if-eqz v1, :cond_1

    .line 8370
    const/4 v1, 0x2

    iget-object v2, p0, Lmki;->a:Lnvv;

    .line 8371
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8373
    :cond_1
    iget-object v1, p0, Lmki;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8374
    iput v0, p0, Lmki;->ai:I

    .line 8375
    return v0
.end method

.method public a(Loxn;)Lmki;
    .locals 2

    .prologue
    .line 8383
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8384
    sparse-switch v0, :sswitch_data_0

    .line 8388
    iget-object v1, p0, Lmki;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8389
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmki;->ah:Ljava/util/List;

    .line 8392
    :cond_1
    iget-object v1, p0, Lmki;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8394
    :sswitch_0
    return-object p0

    .line 8399
    :sswitch_1
    iget-object v0, p0, Lmki;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 8400
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmki;->apiHeader:Llyq;

    .line 8402
    :cond_2
    iget-object v0, p0, Lmki;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8406
    :sswitch_2
    iget-object v0, p0, Lmki;->a:Lnvv;

    if-nez v0, :cond_3

    .line 8407
    new-instance v0, Lnvv;

    invoke-direct {v0}, Lnvv;-><init>()V

    iput-object v0, p0, Lmki;->a:Lnvv;

    .line 8409
    :cond_3
    iget-object v0, p0, Lmki;->a:Lnvv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8384
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8352
    iget-object v0, p0, Lmki;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 8353
    const/4 v0, 0x1

    iget-object v1, p0, Lmki;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8355
    :cond_0
    iget-object v0, p0, Lmki;->a:Lnvv;

    if-eqz v0, :cond_1

    .line 8356
    const/4 v0, 0x2

    iget-object v1, p0, Lmki;->a:Lnvv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8358
    :cond_1
    iget-object v0, p0, Lmki;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8360
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8337
    invoke-virtual {p0, p1}, Lmki;->a(Loxn;)Lmki;

    move-result-object v0

    return-object v0
.end method
