.class public final Lmaf;
.super Loxq;
.source "PG"


# instance fields
.field private a:Llte;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4696
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4699
    iput-object v0, p0, Lmaf;->apiHeader:Llyr;

    .line 4702
    iput-object v0, p0, Lmaf;->a:Llte;

    .line 4696
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4719
    const/4 v0, 0x0

    .line 4720
    iget-object v1, p0, Lmaf;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 4721
    const/4 v0, 0x1

    iget-object v1, p0, Lmaf;->apiHeader:Llyr;

    .line 4722
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4724
    :cond_0
    iget-object v1, p0, Lmaf;->a:Llte;

    if-eqz v1, :cond_1

    .line 4725
    const/4 v1, 0x2

    iget-object v2, p0, Lmaf;->a:Llte;

    .line 4726
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4728
    :cond_1
    iget-object v1, p0, Lmaf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4729
    iput v0, p0, Lmaf;->ai:I

    .line 4730
    return v0
.end method

.method public a(Loxn;)Lmaf;
    .locals 2

    .prologue
    .line 4738
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4739
    sparse-switch v0, :sswitch_data_0

    .line 4743
    iget-object v1, p0, Lmaf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4744
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmaf;->ah:Ljava/util/List;

    .line 4747
    :cond_1
    iget-object v1, p0, Lmaf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4749
    :sswitch_0
    return-object p0

    .line 4754
    :sswitch_1
    iget-object v0, p0, Lmaf;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 4755
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmaf;->apiHeader:Llyr;

    .line 4757
    :cond_2
    iget-object v0, p0, Lmaf;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4761
    :sswitch_2
    iget-object v0, p0, Lmaf;->a:Llte;

    if-nez v0, :cond_3

    .line 4762
    new-instance v0, Llte;

    invoke-direct {v0}, Llte;-><init>()V

    iput-object v0, p0, Lmaf;->a:Llte;

    .line 4764
    :cond_3
    iget-object v0, p0, Lmaf;->a:Llte;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4739
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4707
    iget-object v0, p0, Lmaf;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 4708
    const/4 v0, 0x1

    iget-object v1, p0, Lmaf;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4710
    :cond_0
    iget-object v0, p0, Lmaf;->a:Llte;

    if-eqz v0, :cond_1

    .line 4711
    const/4 v0, 0x2

    iget-object v1, p0, Lmaf;->a:Llte;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4713
    :cond_1
    iget-object v0, p0, Lmaf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4715
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4692
    invoke-virtual {p0, p1}, Lmaf;->a(Loxn;)Lmaf;

    move-result-object v0

    return-object v0
.end method
