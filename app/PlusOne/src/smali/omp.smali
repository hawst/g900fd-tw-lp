.class public final Lomp;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    iget-object v1, p0, Lomp;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 124
    const/4 v0, 0x1

    iget-object v1, p0, Lomp;->a:Ljava/lang/Integer;

    .line 125
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 127
    :cond_0
    iget-object v1, p0, Lomp;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 128
    const/4 v1, 0x2

    iget-object v2, p0, Lomp;->b:Ljava/lang/Integer;

    .line 129
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_1
    iget-object v1, p0, Lomp;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 132
    const/4 v1, 0x4

    iget-object v2, p0, Lomp;->c:Ljava/lang/Float;

    .line 133
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 135
    :cond_2
    iget-object v1, p0, Lomp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    iput v0, p0, Lomp;->ai:I

    .line 137
    return v0
.end method

.method public a(Loxn;)Lomp;
    .locals 2

    .prologue
    .line 145
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 146
    sparse-switch v0, :sswitch_data_0

    .line 150
    iget-object v1, p0, Lomp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 151
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lomp;->ah:Ljava/util/List;

    .line 154
    :cond_1
    iget-object v1, p0, Lomp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    :sswitch_0
    return-object p0

    .line 161
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lomp;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 165
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lomp;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 169
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lomp;->c:Ljava/lang/Float;

    goto :goto_0

    .line 146
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x25 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lomp;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 108
    const/4 v0, 0x1

    iget-object v1, p0, Lomp;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 110
    :cond_0
    iget-object v0, p0, Lomp;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 111
    const/4 v0, 0x2

    iget-object v1, p0, Lomp;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 113
    :cond_1
    iget-object v0, p0, Lomp;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 114
    const/4 v0, 0x4

    iget-object v1, p0, Lomp;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 116
    :cond_2
    iget-object v0, p0, Lomp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 118
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lomp;->a(Loxn;)Lomp;

    move-result-object v0

    return-object v0
.end method
