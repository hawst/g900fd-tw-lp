.class public final Lkwc;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lkfo;

.field private final b:Ljava/lang/String;

.field private final c:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;[ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1, p3}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 28
    new-instance v0, Lkfo;

    invoke-direct {v0, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkwc;->a:Lkfo;

    .line 29
    iput-object p4, p0, Lkwc;->c:[I

    .line 30
    iput-object p5, p0, Lkwc;->b:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 5

    .prologue
    .line 35
    new-instance v0, Lkwb;

    invoke-virtual {p0}, Lkwc;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lkwc;->a:Lkfo;

    iget-object v3, p0, Lkwc;->c:[I

    iget-object v4, p0, Lkwc;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lkwb;-><init>(Landroid/content/Context;Lkfo;[ILjava/lang/String;)V

    .line 37
    invoke-virtual {v0}, Lkwb;->l()V

    .line 38
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0}, Lkwb;->t()Z

    move-result v2

    if-nez v2, :cond_0

    .line 41
    invoke-virtual {v0}, Lkwb;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmdb;

    iget-object v0, v0, Lmdb;->a:Lntp;

    .line 42
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "squares_promo_type"

    iget v0, v0, Lntp;->a:I

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 45
    :cond_0
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "square_id_key"

    iget-object v3, p0, Lkwc;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-object v1
.end method
