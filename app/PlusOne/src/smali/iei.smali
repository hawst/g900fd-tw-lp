.class public abstract Liei;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lieh;


# instance fields
.field public final a:Landroid/content/Context;

.field private b:Lhei;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Liei;->a:Landroid/content/Context;

    .line 21
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Liei;->b:Lhei;

    .line 22
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Liei;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 46
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    .line 47
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lief;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Liei;->b:Lhei;

    invoke-interface {v0, p2}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    invoke-direct {p0, p2}, Liei;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Liei;->a(Lief;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(Lief;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public b(Lief;I)Z
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Liei;->b:Lhei;

    invoke-interface {v0, p2}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-direct {p0, p2}, Liei;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Liei;->b(Lief;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lief;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 55
    const-string v0, "true"

    invoke-virtual {p0, p1, p2}, Liei;->a(Lief;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c(Lief;I)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Liei;->b:Lhei;

    invoke-interface {v0, p2}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-direct {p0, p2}, Liei;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Liei;->c(Lief;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lief;Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 61
    :try_start_0
    invoke-virtual {p0, p1, p2}, Liei;->a(Lief;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 65
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
