.class public final Ljjx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjm;
.implements Ljjp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const-string v0, "com.google.android.libraries.social.notifications.NOTIFICATION_SELECTED"

    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 24
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->a(Landroid/content/Intent;)Lkfo;

    move-result-object v2

    .line 25
    if-nez v2, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    const-string v0, "notification_key_list"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 33
    invoke-virtual {v2}, Lkfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lkfo;->b()Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-static {p2, v0, v1, v3}, Ljju;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)[Ljiu;

    move-result-object v4

    .line 35
    const-string v0, "notification_event_type"

    .line 36
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljir;

    .line 39
    array-length v1, v3

    const/4 v5, 0x1

    if-ne v1, v5, :cond_2

    .line 41
    const/4 v1, 0x2

    .line 47
    :goto_1
    invoke-static {p2, v2, v3, v1, p0}, Ljkh;->a(Landroid/content/Context;Lkfo;[Ljava/lang/String;ILjjp;)V

    .line 49
    invoke-static {p2, v4, v0}, Ljkh;->a(Landroid/content/Context;[Ljiu;Ljir;)V

    .line 53
    const-class v0, Ljiv;

    invoke-static {p2, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljiv;

    .line 55
    if-eqz v0, :cond_0

    .line 56
    const-string v0, "effective_gaia_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    .line 45
    :cond_2
    const/4 v1, 0x4

    goto :goto_1
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Ljjx;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
