.class public final Lmud;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmud;


# instance fields
.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3305
    const/4 v0, 0x0

    new-array v0, v0, [Lmud;

    sput-object v0, Lmud;->a:[Lmud;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3306
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3325
    const/high16 v0, -0x80000000

    iput v0, p0, Lmud;->f:I

    .line 3306
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 3351
    const/4 v0, 0x0

    .line 3352
    iget-object v1, p0, Lmud;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 3353
    const/4 v0, 0x1

    iget-object v1, p0, Lmud;->b:Ljava/lang/Long;

    .line 3354
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3356
    :cond_0
    iget-object v1, p0, Lmud;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 3357
    const/4 v1, 0x2

    iget-object v2, p0, Lmud;->c:Ljava/lang/Long;

    .line 3358
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3360
    :cond_1
    iget-object v1, p0, Lmud;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 3361
    const/4 v1, 0x3

    iget-object v2, p0, Lmud;->d:Ljava/lang/Integer;

    .line 3362
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3364
    :cond_2
    iget-object v1, p0, Lmud;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 3365
    const/4 v1, 0x4

    iget-object v2, p0, Lmud;->e:Ljava/lang/Integer;

    .line 3366
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3368
    :cond_3
    iget v1, p0, Lmud;->f:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 3369
    const/4 v1, 0x5

    iget v2, p0, Lmud;->f:I

    .line 3370
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3372
    :cond_4
    iget-object v1, p0, Lmud;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3373
    iput v0, p0, Lmud;->ai:I

    .line 3374
    return v0
.end method

.method public a(Loxn;)Lmud;
    .locals 2

    .prologue
    .line 3382
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3383
    sparse-switch v0, :sswitch_data_0

    .line 3387
    iget-object v1, p0, Lmud;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3388
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmud;->ah:Ljava/util/List;

    .line 3391
    :cond_1
    iget-object v1, p0, Lmud;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3393
    :sswitch_0
    return-object p0

    .line 3398
    :sswitch_1
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmud;->b:Ljava/lang/Long;

    goto :goto_0

    .line 3402
    :sswitch_2
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmud;->c:Ljava/lang/Long;

    goto :goto_0

    .line 3406
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmud;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 3410
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmud;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 3414
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3415
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 3420
    :cond_2
    iput v0, p0, Lmud;->f:I

    goto :goto_0

    .line 3422
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmud;->f:I

    goto :goto_0

    .line 3383
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 3330
    iget-object v0, p0, Lmud;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 3331
    const/4 v0, 0x1

    iget-object v1, p0, Lmud;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 3333
    :cond_0
    iget-object v0, p0, Lmud;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 3334
    const/4 v0, 0x2

    iget-object v1, p0, Lmud;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 3336
    :cond_1
    iget-object v0, p0, Lmud;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 3337
    const/4 v0, 0x3

    iget-object v1, p0, Lmud;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3339
    :cond_2
    iget-object v0, p0, Lmud;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 3340
    const/4 v0, 0x4

    iget-object v1, p0, Lmud;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3342
    :cond_3
    iget v0, p0, Lmud;->f:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 3343
    const/4 v0, 0x5

    iget v1, p0, Lmud;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3345
    :cond_4
    iget-object v0, p0, Lmud;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3347
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3302
    invoke-virtual {p0, p1}, Lmud;->a(Loxn;)Lmud;

    move-result-object v0

    return-object v0
.end method
