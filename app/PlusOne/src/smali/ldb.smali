.class public final Lldb;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Lkzm;

.field private c:Ljava/lang/String;

.field private d:Lldc;

.field private e:Z

.field private f:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lldb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lldb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    sget-object v0, Lldb;->a:Llct;

    if-nez v0, :cond_0

    .line 58
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lldb;->a:Llct;

    .line 60
    :cond_0
    invoke-virtual {p0, p0}, Lldb;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 134
    iput-object v0, p0, Lldb;->f:Landroid/text/StaticLayout;

    .line 135
    iput-object v0, p0, Lldb;->b:Lkzm;

    .line 136
    iput-object v0, p0, Lldb;->d:Lldc;

    .line 137
    iput-object v0, p0, Lldb;->c:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public a(Lldc;ILkzm;)V
    .locals 6

    .prologue
    .line 65
    invoke-virtual {p0}, Lldb;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 66
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 67
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    iput-object p3, p0, Lldb;->b:Lkzm;

    .line 70
    iput-object p1, p0, Lldb;->d:Lldc;

    .line 71
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 72
    iget-object v2, p0, Lldb;->b:Lkzm;

    invoke-virtual {v2}, Lkzm;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lldb;->e:Z

    .line 74
    iget-boolean v0, p0, Lldb;->e:Z

    if-eqz v0, :cond_1

    .line 75
    invoke-virtual {p0}, Lldb;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0a0480

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 91
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lldb;->c:Ljava/lang/String;

    .line 92
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lldb;->b:Lkzm;

    invoke-virtual {v0}, Lkzm;->b()Ljava/lang/String;

    move-result-object v0

    .line 80
    invoke-virtual {p0}, Lldb;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a047f

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 79
    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 81
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 82
    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 83
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 85
    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v3, v2, :cond_0

    .line 86
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v3

    .line 87
    sget-object v2, Lldb;->a:Llct;

    iget-object v2, v2, Llct;->af:Landroid/text/style/StyleSpan;

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 127
    iget-boolean v0, p0, Lldb;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lldb;->d:Lldc;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lldb;->d:Lldc;

    iget-object v1, p0, Lldb;->b:Lkzm;

    invoke-interface {v0, v1}, Lldc;->a(Lkzm;)V

    .line 130
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 110
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 112
    invoke-virtual {p0}, Lldb;->getWidth()I

    move-result v0

    .line 113
    invoke-virtual {p0}, Lldb;->getHeight()I

    move-result v1

    .line 114
    sget-object v2, Lldb;->a:Llct;

    iget-object v2, v2, Llct;->aD:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    .line 115
    sget-object v0, Lldb;->a:Llct;

    iget-object v0, v0, Llct;->aD:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 117
    iget-object v0, p0, Lldb;->f:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lldb;->f:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    .line 119
    sget-object v1, Lldb;->a:Llct;

    iget v1, v1, Llct;->m:I

    int-to-float v1, v1

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 120
    iget-object v1, p0, Lldb;->f:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 121
    sget-object v1, Lldb;->a:Llct;

    iget v1, v1, Llct;->m:I

    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 123
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    .line 96
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 97
    sget-object v0, Lldb;->a:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v3, v8, v0

    .line 100
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lldb;->c:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lldb;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v4, 0x25

    invoke-static {v2, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lldb;->f:Landroid/text/StaticLayout;

    .line 104
    sget-object v0, Lldb;->a:Llct;

    iget v0, v0, Llct;->ao:I

    iget-object v1, p0, Lldb;->f:Landroid/text/StaticLayout;

    .line 105
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget-object v2, Lldb;->a:Llct;

    iget v2, v2, Llct;->m:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 104
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v8, v0}, Lldb;->setMeasuredDimension(II)V

    .line 106
    return-void
.end method
