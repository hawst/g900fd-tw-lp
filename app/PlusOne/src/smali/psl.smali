.class public final Lpsl;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lpsl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:Lpsk;

.field private c:Lpsk;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 144
    const v0, 0x2edfbe9

    new-instance v1, Lpsm;

    invoke-direct {v1}, Lpsm;-><init>()V

    .line 149
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lpsl;->a:Loxr;

    .line 148
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 173
    iget-object v1, p0, Lpsl;->b:Lpsk;

    if-eqz v1, :cond_0

    .line 174
    const/4 v0, 0x1

    iget-object v1, p0, Lpsl;->b:Lpsk;

    .line 175
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 177
    :cond_0
    iget-object v1, p0, Lpsl;->c:Lpsk;

    if-eqz v1, :cond_1

    .line 178
    const/4 v1, 0x2

    iget-object v2, p0, Lpsl;->c:Lpsk;

    .line 179
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_1
    iget-object v1, p0, Lpsl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    iput v0, p0, Lpsl;->ai:I

    .line 183
    return v0
.end method

.method public a(Loxn;)Lpsl;
    .locals 2

    .prologue
    .line 191
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 192
    sparse-switch v0, :sswitch_data_0

    .line 196
    iget-object v1, p0, Lpsl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 197
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpsl;->ah:Ljava/util/List;

    .line 200
    :cond_1
    iget-object v1, p0, Lpsl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    :sswitch_0
    return-object p0

    .line 207
    :sswitch_1
    iget-object v0, p0, Lpsl;->b:Lpsk;

    if-nez v0, :cond_2

    .line 208
    new-instance v0, Lpsk;

    invoke-direct {v0}, Lpsk;-><init>()V

    iput-object v0, p0, Lpsl;->b:Lpsk;

    .line 210
    :cond_2
    iget-object v0, p0, Lpsl;->b:Lpsk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 214
    :sswitch_2
    iget-object v0, p0, Lpsl;->c:Lpsk;

    if-nez v0, :cond_3

    .line 215
    new-instance v0, Lpsk;

    invoke-direct {v0}, Lpsk;-><init>()V

    iput-object v0, p0, Lpsl;->c:Lpsk;

    .line 217
    :cond_3
    iget-object v0, p0, Lpsl;->c:Lpsk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 192
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lpsl;->b:Lpsk;

    if-eqz v0, :cond_0

    .line 161
    const/4 v0, 0x1

    iget-object v1, p0, Lpsl;->b:Lpsk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 163
    :cond_0
    iget-object v0, p0, Lpsl;->c:Lpsk;

    if-eqz v0, :cond_1

    .line 164
    const/4 v0, 0x2

    iget-object v1, p0, Lpsl;->c:Lpsk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 166
    :cond_1
    iget-object v0, p0, Lpsl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 168
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0, p1}, Lpsl;->a(Loxn;)Lpsl;

    move-result-object v0

    return-object v0
.end method
