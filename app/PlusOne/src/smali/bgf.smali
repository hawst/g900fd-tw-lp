.class public Lbgf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[I

.field private static final d:[I

.field private static final e:[I


# instance fields
.field private final c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/nio/ByteBuffer;",
            "[I>;>;"
        }
    .end annotation
.end field

.field private final f:Lchk;

.field private final g:Ljava/lang/Runnable;

.field private final h:[I

.field private final i:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lbgk;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lbgl;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/lang/Object;

.field private volatile l:Ljava/util/concurrent/ExecutorService;

.field private m:I

.field private volatile n:Ljava/util/concurrent/CountDownLatch;

.field private volatile o:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/util/concurrent/FutureTask",
            "<*>;>;"
        }
    .end annotation
.end field

.field private p:Landroid/opengl/EGLDisplay;

.field private q:Landroid/opengl/EGLContext;

.field private r:Lbgk;

.field private s:Lbgr;

.field private t:Lbgk;

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    const-class v0, Lbgf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbgf;->a:Ljava/lang/String;

    .line 72
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x3038

    aput v2, v0, v1

    sput-object v0, Lbgf;->b:[I

    .line 213
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbgf;->d:[I

    .line 222
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lbgf;->e:[I

    return-void

    .line 213
    nop

    :array_0
    .array-data 4
        0x3040
        0x4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3025
        0x0
        0x3026
        0x0
        0x3038
    .end array-data

    .line 222
    :array_1
    .array-data 4
        0x3040
        0x4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3025
        0x0
        0x3026
        0x0
        0x3142
        0x1
        0x3038
    .end array-data
.end method

.method public constructor <init>(Lchk;)V
    .locals 2

    .prologue
    .line 286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lbgf;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 235
    new-instance v0, Lbgh;

    invoke-direct {v0, p0}, Lbgh;-><init>(Lbgf;)V

    iput-object v0, p0, Lbgf;->g:Ljava/lang/Runnable;

    .line 257
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbgf;->k:Ljava/lang/Object;

    .line 287
    const-string v0, "executorFactory"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchk;

    iput-object v0, p0, Lbgf;->f:Lchk;

    .line 289
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lbgf;->n:Ljava/util/concurrent/CountDownLatch;

    .line 290
    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lbgf;->h:[I

    .line 291
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbgf;->i:Ljava/util/LinkedList;

    .line 292
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbgf;->j:Ljava/util/LinkedList;

    .line 293
    return-void
.end method

.method static a(I)I
    .locals 2

    .prologue
    .line 195
    ushr-int/lit8 v0, p0, 0x8

    shl-int/lit8 v1, p0, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method static synthetic a(Landroid/opengl/EGLDisplay;[I)Landroid/opengl/EGLConfig;
    .locals 1

    .prologue
    .line 62
    invoke-static {p0, p1}, Lbgf;->b(Landroid/opengl/EGLDisplay;[I)Landroid/opengl/EGLConfig;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbgf;I)Landroid/util/Pair;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 62
    iget-object v0, p0, Lbgf;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, [I

    array-length v1, v1

    :goto_0
    if-eqz v0, :cond_1

    if-lt v1, p1, :cond_1

    :goto_1
    return-object v0

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    new-array v1, p1, [I

    shl-int/lit8 v3, p1, 0x2

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    iget-object v1, p0, Lbgf;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v3}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    if-eq v1, v0, :cond_2

    if-nez v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_2
    const-string v1, "getAuxiliaryBuffers called from two different threads. Expecting only one."

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    move-object v0, v3

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method static synthetic a(Lbgf;Lbgk;)Lbgk;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lbgf;->t:Lbgk;

    return-object p1
.end method

.method static synthetic a(Lbgf;Lbgr;)Lbgr;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lbgf;->s:Lbgr;

    return-object p1
.end method

.method static synthetic a(Lbgf;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lbgf;->n:Ljava/util/concurrent/CountDownLatch;

    return-object p1
.end method

.method static synthetic a(IIII)V
    .locals 10

    .prologue
    const/16 v2, 0x1908

    const/4 v1, 0x0

    .line 62
    invoke-static {p1, p0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    mul-int v0, p2, p3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-static {p2}, Lbgf;->c(I)I

    move-result v9

    const/16 v7, 0x1401

    move v0, p1

    move v3, p2

    move v4, p3

    move v5, v1

    move v6, v2

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    const-string v0, "glTexImage2D"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    invoke-static {v9}, Lbgf;->b(I)Z

    move-result v0

    const-string v1, "couldn\'t reset row byte alignment"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(IILandroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-static {p1, p0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-static {v0}, Lbgf;->c(I)I

    move-result v0

    invoke-static {p1, v1, p2, v1}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    const-string v1, "glTexImage2D"

    invoke-static {v1}, Lbgc;->a(Ljava/lang/String;)V

    invoke-static {v0}, Lbgf;->b(I)Z

    move-result v0

    const-string v1, "couldn\'t reset row byte alignment"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic a(Lbgf;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lbgf;->n()V

    return-void
.end method

.method static synthetic a(Lbgf;Z)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lbgf;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 590
    iget-object v0, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    const-string v3, "mDisplay"

    invoke-static {v0, v3, v4}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 591
    iget-object v0, p0, Lbgf;->q:Landroid/opengl/EGLContext;

    const-string v3, "mContext"

    invoke-static {v0, v3, v4}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 593
    invoke-static {v2}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v3

    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v3, v0, :cond_2

    move v0, v1

    :goto_0
    const-string v4, "eglGetDisplay"

    invoke-static {v0, v4}, Lbgf;->b(ZLjava/lang/String;)V

    new-array v0, v1, [I

    new-array v4, v1, [I

    invoke-static {v3, v0, v2, v4, v2}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v0

    const-string v4, "eglInitialize"

    invoke-static {v0, v4}, Lbgf;->b(ZLjava/lang/String;)V

    iput-object v3, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    .line 594
    iget-object v3, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    iget-object v4, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    if-eqz p1, :cond_3

    sget-object v0, Lbgf;->e:[I

    :goto_1
    invoke-static {v4, v0}, Lbgf;->b(Landroid/opengl/EGLDisplay;[I)Landroid/opengl/EGLConfig;

    move-result-object v0

    const/4 v4, 0x3

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    sget-object v5, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v3, v0, v5, v4, v2}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v0

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-eq v0, v3, :cond_0

    move v2, v1

    :cond_0
    const-string v3, "eglCreateContext"

    invoke-static {v2, v3}, Lbgf;->b(ZLjava/lang/String;)V

    iput-object v0, p0, Lbgf;->q:Landroid/opengl/EGLContext;

    .line 598
    if-nez p1, :cond_1

    .line 599
    new-instance v0, Lbgk;

    invoke-direct {v0, p0, v1, v1}, Lbgk;-><init>(Lbgf;II)V

    iput-object v0, p0, Lbgf;->r:Lbgk;

    .line 600
    iget-object v0, p0, Lbgf;->r:Lbgk;

    invoke-virtual {v0}, Lbgk;->g()V

    .line 603
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v6, v6, v6, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 605
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 593
    goto :goto_0

    .line 594
    :cond_3
    sget-object v0, Lbgf;->d:[I

    goto :goto_1

    :array_0
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method static synthetic a(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-static {p0, p1}, Lbgf;->b(ZLjava/lang/String;)V

    return-void
.end method

.method static a([III)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 175
    move v0, v1

    :goto_0
    add-int/lit8 v2, p2, 0x1

    div-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_1

    move v2, v1

    .line 176
    :goto_1
    if-ge v2, p1, :cond_0

    .line 177
    mul-int v3, v0, p1

    add-int/2addr v3, v2

    .line 178
    sub-int v4, p2, v0

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v4, p1

    add-int/2addr v4, v2

    .line 179
    aget v5, p0, v3

    .line 180
    aget v6, p0, v4

    .line 181
    invoke-static {v5}, Lbgf;->a(I)I

    move-result v5

    aput v5, p0, v4

    .line 182
    invoke-static {v6}, Lbgf;->a(I)I

    move-result v4

    aput v4, p0, v3

    .line 176
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 175
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_1
    return-void
.end method

.method static synthetic b(Lbgf;I)I
    .locals 0

    .prologue
    .line 62
    iput p1, p0, Lbgf;->u:I

    return p1
.end method

.method private static b(Landroid/opengl/EGLDisplay;[I)Landroid/opengl/EGLConfig;
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 663
    new-array v6, v5, [I

    .line 664
    new-array v3, v5, [Landroid/opengl/EGLConfig;

    move-object v0, p0

    move-object v1, p1

    move v4, v2

    move v7, v2

    .line 665
    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    const-string v1, "eglChooseConfig"

    invoke-static {v0, v1}, Lbgf;->b(ZLjava/lang/String;)V

    .line 667
    aget-object v0, v3, v2

    return-object v0
.end method

.method static synthetic b(Lbgf;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->n:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private static b(ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 778
    if-nez p0, :cond_0

    .line 780
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1b

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "EGL Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed. Reason:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 779
    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 782
    :cond_0
    return-void
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 721
    const/16 v0, 0xcf5

    invoke-static {v0, p0}, Landroid/opengl/GLES20;->glPixelStorei(II)V

    .line 722
    invoke-static {}, Lbgf;->o()I

    move-result v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(I)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 752
    invoke-static {}, Lbgf;->o()I

    move-result v2

    .line 753
    if-eq v2, v1, :cond_0

    const/4 v0, 0x2

    if-eq v2, v0, :cond_0

    const/4 v0, 0x4

    if-eq v2, v0, :cond_0

    const/16 v0, 0x8

    if-ne v2, v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v3, "unexpected row byte alignment"

    invoke-static {v0, v3}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 757
    rem-int v0, p0, v2

    .line 758
    if-lez v0, :cond_3

    .line 761
    :goto_1
    and-int v3, v0, v1

    if-nez v3, :cond_2

    .line 762
    shl-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 753
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 764
    :cond_2
    invoke-static {v1}, Lbgf;->b(I)Z

    move-result v0

    const-string v1, "couldn\'t set row byte alignment"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 767
    :cond_3
    return v2
.end method

.method static synthetic c(Lbgf;)Landroid/opengl/EGLDisplay;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    return-object v0
.end method

.method static synthetic d(Lbgf;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->i:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic e(Lbgf;)Lbgk;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->t:Lbgk;

    return-object v0
.end method

.method static synthetic f(Lbgf;)Landroid/opengl/EGLContext;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->q:Landroid/opengl/EGLContext;

    return-object v0
.end method

.method static synthetic g(Lbgf;)[I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->h:[I

    return-object v0
.end method

.method static synthetic h(Lbgf;)Lbgk;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->r:Lbgk;

    return-object v0
.end method

.method static synthetic h()[I
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lbgf;->e:[I

    return-object v0
.end method

.method static synthetic i(Lbgf;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->j:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic i()[I
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lbgf;->d:[I

    return-object v0
.end method

.method static synthetic j(Lbgf;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->k:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic j()[I
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lbgf;->b:[I

    return-object v0
.end method

.method static synthetic k()I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 62
    new-array v0, v1, [I

    invoke-static {v1, v0, v4}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    const-string v1, "glGenTextures"

    invoke-static {v1}, Lbgc;->a(Ljava/lang/String;)V

    aget v1, v0, v4

    const-string v2, "textures[0]"

    const-string v3, "could not create texture"

    invoke-static {v1, v2, v4, v3}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    aget v0, v0, v4

    return v0
.end method

.method static synthetic k(Lbgf;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic l()I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 62
    new-array v0, v1, [I

    invoke-static {v1, v0, v4}, Landroid/opengl/GLES20;->glGenFramebuffers(I[II)V

    const-string v1, "glGenFramebuffers"

    invoke-static {v1}, Lbgc;->a(Ljava/lang/String;)V

    aget v1, v0, v4

    const-string v2, "fbos[0]"

    const-string v3, "could not create frame buffer"

    invoke-static {v1, v2, v4, v3}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    aget v0, v0, v4

    return v0
.end method

.method static synthetic m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lbgf;->a:Ljava/lang/String;

    return-object v0
.end method

.method private n()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 612
    :goto_0
    iget-object v0, p0, Lbgf;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 613
    iget-object v0, p0, Lbgf;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgl;

    .line 614
    sget-object v1, Lbgf;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "context released texture: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 615
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_0

    .line 617
    :cond_0
    iget-object v0, p0, Lbgf;->i:Ljava/util/LinkedList;

    iget-object v1, p0, Lbgf;->r:Lbgk;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 618
    :goto_1
    iget-object v0, p0, Lbgf;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 619
    iget-object v0, p0, Lbgf;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgk;

    .line 620
    sget-object v1, Lbgf;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "context released sink: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_1

    .line 623
    :cond_1
    iget-object v0, p0, Lbgf;->r:Lbgk;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 624
    iput-object v4, p0, Lbgf;->r:Lbgk;

    .line 626
    iget-object v0, p0, Lbgf;->s:Lbgr;

    if-eqz v0, :cond_2

    .line 627
    iget-object v0, p0, Lbgf;->s:Lbgr;

    invoke-virtual {v0}, Lbgr;->a()V

    .line 630
    :cond_2
    iput-object v4, p0, Lbgf;->t:Lbgk;

    .line 631
    iget-object v0, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v0, v1, :cond_3

    .line 632
    iget-object v0, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    const-string v1, "eglMakeCurrent"

    invoke-static {v0, v1}, Lbgf;->b(ZLjava/lang/String;)V

    .line 641
    :cond_3
    iget-object v0, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Lbgf;->q:Landroid/opengl/EGLContext;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    move-result v0

    const-string v1, "eglDestroyContext"

    invoke-static {v0, v1}, Lbgf;->b(ZLjava/lang/String;)V

    .line 642
    iget-object v0, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    invoke-static {v0}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    move-result v0

    const-string v1, "eglTerminate"

    invoke-static {v0, v1}, Lbgf;->b(ZLjava/lang/String;)V

    .line 644
    iput-object v4, p0, Lbgf;->p:Landroid/opengl/EGLDisplay;

    .line 645
    iput-object v4, p0, Lbgf;->q:Landroid/opengl/EGLContext;

    .line 646
    return-void
.end method

.method private static o()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 714
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 715
    const/16 v1, 0xcf5

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 716
    const-string v1, "glGetIntegerv"

    invoke-static {v1}, Lbgc;->a(Ljava/lang/String;)V

    .line 717
    aget v0, v0, v2

    return v0
.end method


# virtual methods
.method public a(Lbgn;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 88
    new-instance v0, Lbgg;

    invoke-direct {v0, p0, p1, p2}, Lbgg;-><init>(Lbgf;Lbgn;Landroid/graphics/Bitmap;)V

    invoke-interface {p1, v0}, Lbgn;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public a(III)Lbfw;
    .locals 1

    .prologue
    .line 549
    new-instance v0, Lbgl;

    invoke-direct {v0, p0, p1, p2, p3}, Lbgl;-><init>(Lbgf;III)V

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)Lbfw;
    .locals 1

    .prologue
    .line 560
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lbgf;->a(Landroid/graphics/Bitmap;I)Lbfw;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;I)Lbfw;
    .locals 1

    .prologue
    .line 571
    new-instance v0, Lbgl;

    invoke-direct {v0, p0, p1, p2}, Lbgl;-><init>(Lbgf;Landroid/graphics/Bitmap;I)V

    return-object v0
.end method

.method public a(II)Lbge;
    .locals 1

    .prologue
    .line 507
    new-instance v0, Lbgk;

    invoke-direct {v0, p0, p1, p2}, Lbgk;-><init>(Lbgf;II)V

    return-object v0
.end method

.method public a(Ljava/lang/Object;IIIIZ)Lbge;
    .locals 11

    .prologue
    .line 454
    invoke-direct {p0}, Lbgf;->n()V

    .line 455
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbgf;->a(Z)V

    .line 458
    new-instance v0, Lbgk;

    const/4 v3, 0x1

    const/4 v8, 0x1

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, p4

    move/from16 v5, p5

    move v6, p2

    move v7, p3

    move/from16 v9, p6

    invoke-direct/range {v0 .. v10}, Lbgk;-><init>(Lbgf;Ljava/lang/Object;ZIIIIZZB)V

    iput-object v0, p0, Lbgf;->r:Lbgk;

    .line 460
    iget-object v0, p0, Lbgf;->r:Lbgk;

    invoke-virtual {v0}, Lbgk;->g()V

    .line 463
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 465
    iget-object v0, p0, Lbgf;->r:Lbgk;

    return-object v0
.end method

.method public a(Ljava/lang/Object;IIZ)Lbge;
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 495
    new-instance v0, Lbgk;

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move v5, p3

    move v6, p2

    move v7, p3

    move v8, p4

    move v9, v3

    move v10, v3

    invoke-direct/range {v0 .. v10}, Lbgk;-><init>(Lbgf;Ljava/lang/Object;ZIIIIZZB)V

    return-object v0
.end method

.method public a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 328
    iget-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 329
    new-instance v0, Lbgm;

    invoke-direct {v0}, Lbgm;-><init>()V

    throw v0

    .line 331
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 332
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 335
    :cond_1
    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 337
    :try_start_0
    iget-object v0, p0, Lbgf;->o:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 338
    iget-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 339
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 347
    iget-object v2, p0, Lbgf;->o:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    :goto_0
    return-object v0

    .line 340
    :catch_0
    move-exception v0

    .line 341
    const/4 v2, 0x1

    :try_start_1
    invoke-virtual {v1, v2}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 342
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 345
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lbgf;->o:Ljava/util/Queue;

    invoke-interface {v2, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    throw v0

    .line 344
    :catch_1
    move-exception v0

    :try_start_2
    sget-object v0, Lbgf;->a:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 345
    iget-object v0, p0, Lbgf;->o:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lbgf;->c:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 138
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 380
    iget-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    const-string v1, "mExecutorService"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 382
    iget-object v1, p0, Lbgf;->k:Ljava/lang/Object;

    monitor-enter v1

    .line 383
    :try_start_0
    iget-object v0, p0, Lbgf;->f:Lchk;

    const-class v2, Lbgf;

    iget v3, p0, Lbgf;->m:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lbgf;->m:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0xd

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "gl"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 384
    invoke-interface {v0, v2, v3}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    .line 387
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lbgf;->o:Ljava/util/Queue;

    .line 388
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :try_start_1
    new-instance v0, Lbgj;

    iget-object v1, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lbgf;->o:Ljava/util/Queue;

    invoke-direct {v0, p0, v1, v2, p1}, Lbgj;-><init>(Lbgf;Ljava/util/concurrent/ExecutorService;Ljava/util/Queue;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lbgf;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Lbgm; {:try_start_1 .. :try_end_1} :catch_0

    .line 395
    return-void

    .line 388
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 394
    :catch_0
    move-exception v0

    const-string v0, "RenderContext release()d during initialize()"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 305
    new-instance v0, Lbgi;

    invoke-direct {v0, p1}, Lbgi;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {p0, v0}, Lbgf;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    .line 314
    return-void
.end method

.method public b(II)Lbfw;
    .locals 1

    .prologue
    .line 537
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lbgf;->a(III)Lbfw;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 409
    sget-object v0, Lbgf;->a:Ljava/lang/String;

    .line 423
    :goto_0
    return-void

    .line 414
    :cond_0
    :try_start_0
    iget-object v0, p0, Lbgf;->g:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lbgf;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lbgm; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    iget-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 422
    const/4 v0, 0x0

    iput-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    goto :goto_0

    .line 416
    :catch_0
    move-exception v0

    const-string v0, "RenderContext initialize()d during release()"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public b(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 361
    new-instance v0, Lbgm;

    invoke-direct {v0}, Lbgm;-><init>()V

    throw v0

    .line 364
    :cond_0
    iget-object v0, p0, Lbgf;->l:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 365
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 479
    invoke-direct {p0}, Lbgf;->n()V

    .line 480
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbgf;->a(Z)V

    .line 481
    return-void
.end method

.method public d()Lbfw;
    .locals 2

    .prologue
    .line 516
    new-instance v0, Lbgl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbgl;-><init>(Lbgf;Z)V

    return-object v0
.end method

.method public e()Lbfw;
    .locals 2

    .prologue
    .line 525
    new-instance v0, Lbgl;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lbgl;-><init>(Lbgf;Z)V

    return-object v0
.end method

.method public f()Lbgr;
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lbgf;->s:Lbgr;

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 582
    iget v0, p0, Lbgf;->u:I

    return v0
.end method
