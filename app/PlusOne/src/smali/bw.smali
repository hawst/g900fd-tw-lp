.class Lbw;
.super Lcd;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 689
    invoke-direct {p0}, Lcd;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lbs;)Landroid/app/Notification;
    .locals 25

    .prologue
    .line 692
    new-instance v1, Lch;

    move-object/from16 v0, p1

    iget-object v2, v0, Lbs;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lbs;->u:Landroid/app/Notification;

    move-object/from16 v0, p1

    iget-object v4, v0, Lbs;->b:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v5, v0, Lbs;->c:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    iget-object v6, v0, Lbs;->f:Ljava/lang/CharSequence;

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget v8, v0, Lbs;->g:I

    move-object/from16 v0, p1

    iget-object v9, v0, Lbs;->d:Landroid/app/PendingIntent;

    const/4 v10, 0x0

    move-object/from16 v0, p1

    iget-object v11, v0, Lbs;->e:Landroid/graphics/Bitmap;

    move-object/from16 v0, p1

    iget v12, v0, Lbs;->l:I

    move-object/from16 v0, p1

    iget v13, v0, Lbs;->m:I

    move-object/from16 v0, p1

    iget-boolean v14, v0, Lbs;->n:Z

    move-object/from16 v0, p1

    iget-boolean v15, v0, Lbs;->i:Z

    const/16 v16, 0x0

    move-object/from16 v0, p1

    iget v0, v0, Lbs;->h:I

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lbs;->k:Ljava/lang/CharSequence;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lbs;->v:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lbs;->q:Landroid/os/Bundle;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    invoke-direct/range {v1 .. v24}, Lch;-><init>(Landroid/content/Context;Landroid/app/Notification;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/graphics/Bitmap;IIZZZILjava/lang/CharSequence;ZLjava/util/ArrayList;Landroid/os/Bundle;Ljava/lang/String;ZLjava/lang/String;)V

    .line 698
    move-object/from16 v0, p1

    iget-object v2, v0, Lbs;->o:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lbm;->a(Lbk;Ljava/util/ArrayList;)V

    .line 699
    move-object/from16 v0, p1

    iget-object v2, v0, Lbs;->j:Lce;

    invoke-static {v1, v2}, Lbm;->a(Lbl;Lce;)V

    .line 700
    invoke-virtual {v1}, Lch;->b()Landroid/app/Notification;

    move-result-object v1

    return-object v1
.end method

.method public a([Lbn;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lbn;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 719
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    invoke-static {v3}, Lcg;->a(Lcl;)Landroid/app/Notification$Action;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
