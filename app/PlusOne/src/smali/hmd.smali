.class public Lhmd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llqz;
.implements Llrd;
.implements Llrg;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z


# direct methods
.method constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhmd;->c:Z

    .line 44
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 45
    return-void
.end method

.method constructor <init>(Llqr;B)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhmd;->c:Z

    .line 48
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 49
    return-void
.end method

.method private c(Landroid/view/View;)Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 129
    invoke-static {p1}, Lhmo;->a(Landroid/view/View;)Lhmk;

    move-result-object v0

    .line 130
    if-eqz v0, :cond_0

    .line 131
    invoke-virtual {v0}, Lhmk;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 133
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x7f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must either implement the VisualElementProvider interface or have a VisualElement attached to it in order to be impressionable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 82
    iget-boolean v0, p0, Lhmd;->c:Z

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lhmd;->a:Landroid/content/Context;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lhly;->a(Landroid/content/Context;I)V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhmd;->c:Z

    .line 86
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lhmd;->a:Landroid/content/Context;

    .line 65
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    if-eqz p1, :cond_0

    .line 54
    const-string v0, "analytics_log_impression"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lhmd;->c:Z

    .line 55
    const-string v0, "analytics_log_impression_views"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "analytics_log_impression_views"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 57
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lhmd;->b:Ljava/util/HashSet;

    .line 60
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lhmd;->c(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v0

    .line 97
    iget-object v1, p0, Lhmd;->b:Ljava/util/HashSet;

    if-nez v1, :cond_0

    .line 98
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lhmd;->b:Ljava/util/HashSet;

    .line 100
    :cond_0
    iget-object v1, p0, Lhmd;->b:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 101
    const/4 v1, -0x1

    invoke-static {p1, v1}, Lhly;->a(Landroid/view/View;I)V

    .line 102
    iget-object v1, p0, Lhmd;->b:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 104
    :cond_1
    return-void
.end method

.method public b()Lhmd;
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhmd;->c:Z

    .line 113
    return-object p0
.end method

.method public b(Landroid/view/View;)Lhmd;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lhmd;->b:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lhmd;->b:Ljava/util/HashSet;

    invoke-direct {p0, p1}, Lhmd;->c(Landroid/view/View;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 125
    :cond_0
    return-object p0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 69
    const-string v0, "analytics_log_impression"

    iget-boolean v1, p0, Lhmd;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 70
    iget-object v0, p0, Lhmd;->b:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhmd;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lhmd;->b:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 72
    const-string v1, "analytics_log_impression_views"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 74
    :cond_0
    return-void
.end method
