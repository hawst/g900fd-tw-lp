.class public abstract Lelj;
.super Lhya;
.source "PG"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lbc;
.implements Llgs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhya;",
        "Landroid/widget/Filterable;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Llgs;"
    }
.end annotation


# static fields
.field private static final l:[Ljava/lang/String;

.field private static final m:[Ljava/lang/String;

.field private static final n:[Ljava/lang/String;

.field private static final o:[Ljava/lang/String;

.field private static final p:[Ljava/lang/String;

.field private static final q:[Ljava/lang/String;

.field private static r:[Ljava/lang/String;

.field private static s:Ljava/util/regex/Pattern;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Ljava/lang/String;

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final K:I

.field private final L:I

.field private final M:I

.field private final N:I

.field private final O:I

.field private final P:I

.field private final Q:I

.field private R:Ljava/lang/Runnable;

.field private final S:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final T:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final U:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private V:Ljpl;

.field private W:Z

.field private X:Landroid/database/Cursor;

.field private Y:Landroid/database/Cursor;

.field private Z:Z

.field public final a:Lbb;

.field private final aa:Landroid/os/Handler;

.field private ab:I

.field private ac:Landroid/widget/Filter;

.field private volatile ad:Ljava/util/concurrent/CountDownLatch;

.field private final ae:Landroid/database/DataSetObserver;

.field public final b:Lhei;

.field public final c:Lhxh;

.field public final d:I

.field public e:Z

.field public f:Lelt;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lae;

.field private u:I

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "circle_id"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "circle_name"

    aput-object v1, v0, v6

    const-string v1, "contact_count"

    aput-object v1, v0, v7

    sput-object v0, Lelj;->l:[Ljava/lang/String;

    .line 99
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "person_id"

    aput-object v1, v0, v4

    const-string v1, "avatar"

    aput-object v1, v0, v5

    const-string v1, "interaction_sort_key"

    aput-object v1, v0, v6

    .line 109
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "person_id"

    aput-object v1, v0, v4

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "avatar"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "in_same_visibility_group"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "verified"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "interaction_sort_key"

    aput-object v2, v0, v1

    sput-object v0, Lelj;->m:[Ljava/lang/String;

    .line 133
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "lookup_key"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "email"

    aput-object v1, v0, v6

    sput-object v0, Lelj;->n:[Ljava/lang/String;

    .line 140
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "lookup_key"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "email"

    aput-object v1, v0, v6

    const-string v1, "phone"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "phone_type"

    aput-object v2, v0, v1

    sput-object v0, Lelj;->o:[Ljava/lang/String;

    .line 156
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v3

    const-string v1, "packed_circle_ids"

    aput-object v1, v0, v4

    sput-object v0, Lelj;->p:[Ljava/lang/String;

    .line 164
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "gaia_id"

    aput-object v1, v0, v4

    const-string v1, "person_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "avatar"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "snippet"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "in_same_visibility_group"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "verified"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "auto_complete_index"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "auto_complete_suggestion"

    aput-object v2, v0, v1

    sput-object v0, Lelj;->q:[Ljava/lang/String;

    .line 209
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "gaia_id"

    aput-object v1, v0, v4

    sput-object v0, Lelj;->r:[Ljava/lang/String;

    .line 218
    const-string v0, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@([a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})*)?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lelj;->s:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lae;Lbb;I)V
    .locals 6

    .prologue
    .line 423
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lelj;-><init>(Landroid/content/Context;Lae;Lbb;II)V

    .line 424
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lae;Lbb;II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 434
    invoke-direct {p0, p1, v1}, Lhya;-><init>(Landroid/content/Context;B)V

    .line 245
    const/4 v0, -0x1

    iput v0, p0, Lelj;->u:I

    .line 259
    iput-boolean v3, p0, Lelj;->E:Z

    .line 262
    iput-boolean v3, p0, Lelj;->F:Z

    .line 270
    iput-boolean v3, p0, Lelj;->H:Z

    .line 272
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lelj;->J:Ljava/util/Set;

    .line 287
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lelj;->S:Ljava/util/HashMap;

    .line 293
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lelj;->T:Ljava/util/HashSet;

    .line 298
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lelj;->U:Ljava/util/HashSet;

    .line 300
    new-instance v0, Ljpl;

    invoke-direct {v0}, Ljpl;-><init>()V

    iput-object v0, p0, Lelj;->V:Ljpl;

    .line 307
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lelj;->j:Ljava/util/Set;

    .line 309
    new-instance v0, Lelk;

    invoke-direct {v0, p0}, Lelk;-><init>(Lelj;)V

    iput-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    .line 378
    new-instance v0, Lell;

    invoke-direct {v0, p0}, Lell;-><init>(Lelj;)V

    iput-object v0, p0, Lelj;->ae:Landroid/database/DataSetObserver;

    move v0, v1

    .line 435
    :goto_0
    const/4 v2, 0x7

    if-ge v0, v2, :cond_0

    .line 436
    invoke-virtual {p0, v1, v1}, Lelj;->b(ZZ)V

    .line 435
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 439
    :cond_0
    iput-boolean v3, p0, Lelj;->I:Z

    .line 440
    mul-int/lit8 v0, p5, 0xa

    add-int/lit16 v0, v0, 0x400

    .line 441
    add-int/lit8 v1, v0, 0x1

    iput v0, p0, Lelj;->K:I

    .line 442
    add-int/lit8 v0, v1, 0x1

    iput v1, p0, Lelj;->L:I

    .line 443
    add-int/lit8 v1, v0, 0x1

    iput v0, p0, Lelj;->M:I

    .line 444
    add-int/lit8 v0, v1, 0x1

    iput v1, p0, Lelj;->N:I

    .line 445
    add-int/lit8 v1, v0, 0x1

    iput v0, p0, Lelj;->O:I

    .line 446
    add-int/lit8 v0, v1, 0x1

    iput v1, p0, Lelj;->P:I

    .line 447
    iput v0, p0, Lelj;->Q:I

    .line 449
    const-string v0, "people_search_results"

    .line 450
    invoke-virtual {p2, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lelu;

    .line 451
    if-nez v0, :cond_2

    .line 452
    new-instance v0, Lelu;

    invoke-direct {v0}, Lelu;-><init>()V

    .line 453
    invoke-virtual {p2}, Lae;->a()Lat;

    move-result-object v1

    const-string v2, "people_search_results"

    invoke-virtual {v1, v0, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v1

    .line 454
    invoke-virtual {v1}, Lat;->c()I

    .line 464
    :cond_1
    :goto_1
    iget-object v1, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0, v1}, Lelu;->a(Ljpl;)V

    .line 466
    iput-object p2, p0, Lelj;->t:Lae;

    .line 467
    iput-object p3, p0, Lelj;->a:Lbb;

    .line 468
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lelj;->b:Lhei;

    .line 469
    iput p4, p0, Lelj;->d:I

    .line 470
    iget-object v0, p0, Lelj;->V:Ljpl;

    iget-boolean v1, p0, Lelj;->H:Z

    invoke-virtual {v0, v1}, Ljpl;->a(Z)V

    .line 471
    new-instance v0, Lhxh;

    iget v1, p0, Lelj;->d:I

    invoke-direct {v0, p1, p3, v1, p5}, Lhxh;-><init>(Landroid/content/Context;Lbb;II)V

    iput-object v0, p0, Lelj;->c:Lhxh;

    .line 472
    iget-object v0, p0, Lelj;->c:Lhxh;

    iget-object v1, p0, Lelj;->ae:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lhxh;->a(Landroid/database/DataSetObserver;)V

    .line 473
    return-void

    .line 456
    :cond_2
    invoke-virtual {v0}, Lelu;->a()Ljpl;

    move-result-object v1

    .line 457
    if-eqz v1, :cond_1

    .line 458
    iput-object v1, p0, Lelj;->V:Ljpl;

    .line 459
    iget-object v1, p0, Lelj;->V:Ljpl;

    invoke-virtual {v1}, Ljpl;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lelj;->g:Ljava/lang/String;

    .line 460
    iput-boolean v3, p0, Lelj;->W:Z

    goto :goto_1
.end method

.method static synthetic a(Lelj;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lelj;->ad:Ljava/util/concurrent/CountDownLatch;

    return-object p1
.end method

.method static synthetic a(Lelj;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lelj;->j(Z)V

    iget-object v0, p0, Lelj;->f:Lelt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lelj;->f:Lelt;

    invoke-interface {v0}, Lelt;->an_()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lelj;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lelj;->O:I

    return v0
.end method

.method private b(ILandroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 926
    iget-object v0, p0, Lelj;->S:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 927
    if-eqz v0, :cond_0

    if-eq v0, p2, :cond_0

    .line 928
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 929
    iget-object v1, p0, Lelj;->S:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 931
    :cond_0
    if-eqz p2, :cond_1

    if-eq v0, p2, :cond_1

    .line 932
    iget-object v0, p0, Lelj;->S:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 934
    :cond_1
    return-void
.end method

.method private c(ILandroid/database/Cursor;)I
    .locals 13

    .prologue
    .line 1040
    iget v0, p0, Lelj;->O:I

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    move v12, v0

    .line 1041
    :goto_0
    if-nez v12, :cond_2

    iget v0, p0, Lelj;->P:I

    if-eq p1, v0, :cond_2

    .line 1042
    const-string v0, "PeopleSearchAdapter"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1043
    const-string v0, "PeopleSearchAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unexpected loader "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1045
    :cond_0
    const/4 v11, 0x0

    .line 1107
    :goto_1
    return v11

    .line 1040
    :cond_1
    const/4 v0, 0x0

    move v12, v0

    goto :goto_0

    .line 1048
    :cond_2
    if-eqz p2, :cond_3

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1049
    :cond_3
    iget-object v0, p0, Lelj;->U:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1050
    iget-object v0, p0, Lelj;->T:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1051
    const/4 v11, 0x0

    goto :goto_1

    .line 1053
    :cond_4
    iget-object v0, p0, Lelj;->U:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1055
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1058
    iget-object v1, p0, Lelj;->V:Ljpl;

    invoke-virtual {v1}, Ljpl;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1059
    const/4 v11, 0x0

    goto :goto_1

    .line 1062
    :cond_5
    iget-object v0, p0, Lelj;->T:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1064
    const/4 v0, 0x0

    .line 1065
    const/4 v1, 0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1066
    iget-object v2, p0, Lelj;->V:Ljpl;

    invoke-virtual {v2, v1}, Ljpl;->a(Ljava/lang/String;)V

    move v11, v0

    .line 1068
    :cond_6
    :goto_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1069
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1070
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1071
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1072
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1073
    const-string v0, "PeopleSearchAdapter"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1074
    if-eqz v12, :cond_7

    const-string v0, "PublicProfileSearch"

    :goto_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x27

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "empty personId for gaiaId/name "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_7
    const-string v0, "AutocompleteMergedPeople"

    goto :goto_3

    .line 1080
    :cond_8
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lelj;->J:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1081
    :cond_9
    if-eqz v12, :cond_c

    .line 1085
    iget-object v0, p0, Lelj;->V:Ljpl;

    const/4 v4, 0x5

    .line 1086
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    .line 1087
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x6

    .line 1088
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x7

    .line 1089
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_a

    const/4 v7, 0x1

    :goto_4
    const/16 v8, 0x8

    .line 1090
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_b

    const/4 v8, 0x1

    .line 1085
    :goto_5
    invoke-virtual/range {v0 .. v8}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZ)Z

    move-result v0

    .line 1101
    :goto_6
    if-eqz v0, :cond_10

    .line 1102
    add-int/lit8 v0, v11, 0x1

    :goto_7
    move v11, v0

    .line 1104
    goto/16 :goto_2

    .line 1089
    :cond_a
    const/4 v7, 0x0

    goto :goto_4

    .line 1090
    :cond_b
    const/4 v8, 0x0

    goto :goto_5

    .line 1092
    :cond_c
    iget-object v0, p0, Lelj;->V:Ljpl;

    const/4 v4, 0x5

    .line 1093
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    .line 1094
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x6

    .line 1095
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x7

    .line 1096
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-eqz v7, :cond_d

    const/4 v7, 0x1

    :goto_8
    const/16 v8, 0x8

    .line 1097
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_e

    const/4 v8, 0x1

    :goto_9
    const/16 v9, 0x9

    .line 1098
    invoke-interface {p2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/16 v10, 0xa

    .line 1099
    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1092
    invoke-virtual/range {v0 .. v10}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZILjava/lang/String;)Z

    move-result v0

    goto :goto_6

    .line 1096
    :cond_d
    const/4 v7, 0x0

    goto :goto_8

    .line 1097
    :cond_e
    const/4 v8, 0x0

    goto :goto_9

    .line 1106
    :cond_f
    invoke-direct {p0}, Lelj;->l()V

    goto/16 :goto_1

    :cond_10
    move v0, v11

    goto :goto_7
.end method

.method static synthetic c(Lelj;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lelj;->T:Ljava/util/HashSet;

    return-object v0
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 576
    iget-object v0, p0, Lelj;->R:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lelj;->R:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 580
    :cond_0
    new-instance v0, Lelm;

    invoke-direct {v0, p0, p1, p0}, Lelm;-><init>(Lelj;Landroid/os/Bundle;Lbc;)V

    iput-object v0, p0, Lelj;->R:Ljava/lang/Runnable;

    .line 587
    iget-object v0, p0, Lelj;->R:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 589
    return-void
.end method

.method static synthetic d(Lelj;)Z
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lelj;->j()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lelj;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lelj;->j(Z)V

    return-void
.end method

.method static synthetic f(Lelj;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lelj;->m()V

    return-void
.end method

.method static synthetic g(Lelj;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lelj;->B:Z

    return v0
.end method

.method static synthetic h(Lelj;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    return-object v0
.end method

.method private j(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1229
    iget-boolean v0, p0, Lelj;->D:Z

    if-nez v0, :cond_0

    .line 1255
    :goto_0
    return-void

    .line 1233
    :cond_0
    new-instance v0, Lhym;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    .line 1234
    iget-object v1, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lelj;->g:Ljava/lang/String;

    .line 1235
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v5, :cond_1

    iget-boolean v1, p0, Lelj;->x:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lelj;->z:Z

    if-eqz v1, :cond_1

    .line 1238
    iget-object v1, p0, Lelj;->U:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1239
    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    .line 1250
    :cond_1
    :goto_1
    invoke-virtual {v0}, Lhym;->getCount()I

    move-result v1

    if-eqz v1, :cond_2

    .line 1251
    invoke-virtual {p0}, Lelj;->h()V

    .line 1254
    :cond_2
    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Lelj;->a(ILandroid/database/Cursor;)V

    goto :goto_0

    .line 1240
    :cond_3
    iget-boolean v1, p0, Lelj;->B:Z

    if-nez v1, :cond_1

    .line 1241
    if-nez p1, :cond_4

    iget-object v1, p0, Lelj;->T:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    :cond_4
    iget-boolean v1, p0, Lelj;->E:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lelj;->V:Ljpl;

    .line 1242
    invoke-virtual {v1}, Ljpl;->d()I

    move-result v1

    if-lez v1, :cond_6

    .line 1243
    :cond_5
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_1

    .line 1244
    :cond_6
    iget-object v1, p0, Lelj;->V:Ljpl;

    invoke-virtual {v1}, Ljpl;->l()I

    move-result v1

    if-nez v1, :cond_1

    .line 1245
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private j()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 695
    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0}, Ljpl;->c()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 713
    :goto_0
    return v0

    .line 699
    :cond_0
    iget-object v0, p0, Lelj;->T:Ljava/util/HashSet;

    iget v2, p0, Lelj;->O:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 700
    goto :goto_0

    .line 703
    :cond_1
    iget-object v0, p0, Lelj;->a:Lbb;

    iget v2, p0, Lelj;->O:I

    .line 704
    invoke-virtual {v0, v2}, Lbb;->b(I)Ldo;

    move-result-object v0

    check-cast v0, Lepu;

    .line 705
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lepu;->p()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 706
    goto :goto_0

    .line 709
    :cond_3
    iget-object v2, p0, Lelj;->V:Ljpl;

    invoke-virtual {v2}, Ljpl;->b()Ljava/lang/String;

    move-result-object v2

    .line 710
    invoke-virtual {v0}, Lepu;->D()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 711
    goto :goto_0

    .line 713
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private k()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 759
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 760
    const-string v1, "query"

    iget-object v2, p0, Lelj;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    return-object v0
.end method

.method private l()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1115
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1116
    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0}, Ljpl;->m()Landroid/database/Cursor;

    move-result-object v0

    .line 1117
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 1118
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1123
    :goto_0
    return-void

    .line 1121
    :cond_0
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lelj;->a(ILandroid/database/Cursor;)V

    goto :goto_0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 1137
    iget-object v0, p0, Lelj;->ad:Ljava/util/concurrent/CountDownLatch;

    .line 1138
    if-eqz v0, :cond_0

    .line 1139
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1141
    :cond_0
    return-void
.end method

.method private n()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1176
    iget-object v1, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1187
    :cond_0
    :goto_0
    return-object v0

    .line 1180
    :cond_1
    iget-object v1, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v1

    .line 1181
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 1182
    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 1183
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lelj;->s:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1184
    goto :goto_0
.end method

.method private o()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1195
    iget-object v1, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1211
    :cond_0
    :goto_0
    return-object v0

    .line 1198
    :cond_1
    iget-object v1, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isWellFormedSmsAddress(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1201
    iget-object v1, p0, Lelj;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    .line 1202
    const/4 v1, 0x1

    move v3, v1

    move v1, v2

    .line 1203
    :goto_1
    if-ge v1, v4, :cond_3

    .line 1204
    iget-object v5, p0, Lelj;->g:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 1205
    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    move-result v6

    if-eqz v6, :cond_0

    const/16 v6, 0x2b

    if-ne v5, v6, :cond_2

    if-eqz v3, :cond_0

    .line 1203
    :cond_2
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    goto :goto_1

    .line 1211
    :cond_3
    iget-object v0, p0, Lelj;->g:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 685
    const/4 v0, 0x7

    return v0
.end method

.method protected a(II)I
    .locals 0

    .prologue
    .line 690
    return p1
.end method

.method protected a(Landroid/database/Cursor;)Landroid/os/Bundle;
    .locals 6

    .prologue
    const/16 v4, 0xe

    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 1389
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1391
    :goto_0
    const/16 v3, 0xf

    .line 1392
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1393
    invoke-interface {p1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, -0x1

    move v4, v3

    .line 1396
    :goto_1
    if-nez v0, :cond_2

    if-gez v4, :cond_2

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1397
    const/4 v0, 0x0

    .line 1414
    :goto_2
    return-object v0

    .line 1389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1394
    :cond_1
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move v4, v3

    goto :goto_1

    .line 1400
    :cond_2
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v1, :cond_3

    move v3, v1

    .line 1403
    :goto_3
    if-eqz v0, :cond_4

    const/4 v0, 0x3

    .line 1409
    :goto_4
    const-string v1, "extra_search_query"

    iget-object v2, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lhmt;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1410
    const-string v2, "extra_search_type"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1411
    const-string v2, "extra_search_personalization_type"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1412
    const-string v0, "extra_search_selected_text"

    invoke-virtual {v1, v0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    const-string v0, "extra_search_selected_index"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v0, v1

    .line 1414
    goto :goto_2

    .line 1400
    :cond_3
    const/4 v1, 0x2

    move v3, v1

    goto :goto_3

    :cond_4
    move v0, v2

    .line 1403
    goto :goto_4
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 820
    iget v0, p0, Lelj;->K:I

    if-ne p1, v0, :cond_0

    .line 821
    new-instance v0, Lhxg;

    invoke-virtual {p0}, Lelj;->as()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lelj;->d:I

    iget v3, p0, Lelj;->u:I

    sget-object v4, Lelj;->l:[Ljava/lang/String;

    iget-object v5, p0, Lelj;->g:Ljava/lang/String;

    .line 822
    invoke-static {v5}, Ldsm;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0xa

    invoke-direct/range {v0 .. v6}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)V

    .line 854
    :goto_0
    return-object v0

    .line 824
    :cond_0
    iget v0, p0, Lelj;->L:I

    if-ne p1, v0, :cond_1

    .line 825
    new-instance v0, Lekz;

    invoke-virtual {p0}, Lelj;->as()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lelj;->d:I

    sget-object v3, Lelj;->p:[Ljava/lang/String;

    iget-boolean v4, p0, Lelj;->G:Z

    iget-boolean v5, p0, Lelj;->Z:Z

    invoke-direct/range {v0 .. v5}, Lekz;-><init>(Landroid/content/Context;I[Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 828
    :cond_1
    iget v0, p0, Lelj;->N:I

    if-ne p1, v0, :cond_3

    .line 829
    new-instance v0, Ldxk;

    invoke-virtual {p0}, Lelj;->as()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lelj;->d:I

    iget-boolean v2, p0, Lelj;->i:Z

    if-eqz v2, :cond_2

    sget-object v2, Lelj;->o:[Ljava/lang/String;

    :goto_1
    iget-object v3, p0, Lelj;->g:Ljava/lang/String;

    const/4 v4, 0x2

    iget-boolean v5, p0, Lelj;->i:Z

    invoke-direct/range {v0 .. v5}, Ldxk;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    sget-object v2, Lelj;->n:[Ljava/lang/String;

    goto :goto_1

    .line 835
    :cond_3
    iget v0, p0, Lelj;->M:I

    if-ne p1, v0, :cond_4

    .line 836
    new-instance v0, Lemd;

    invoke-virtual {p0}, Lelj;->as()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lelj;->d:I

    sget-object v3, Lelj;->m:[Ljava/lang/String;

    iget-object v4, p0, Lelj;->g:Ljava/lang/String;

    iget-boolean v5, p0, Lelj;->G:Z

    iget-boolean v6, p0, Lelj;->H:Z

    iget-boolean v7, p0, Lelj;->Z:Z

    iget-object v8, p0, Lelj;->C:Ljava/lang/String;

    const/16 v9, 0xa

    invoke-direct/range {v0 .. v9}, Lemd;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;I)V

    goto :goto_0

    .line 840
    :cond_4
    iget v0, p0, Lelj;->O:I

    if-ne p1, v0, :cond_5

    .line 841
    new-instance v0, Lepu;

    invoke-virtual {p0}, Lelj;->as()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lelj;->d:I

    sget-object v3, Lelj;->q:[Ljava/lang/String;

    iget-object v4, p0, Lelj;->g:Ljava/lang/String;

    const/4 v5, 0x2

    iget-boolean v6, p0, Lelj;->G:Z

    iget-boolean v7, p0, Lelj;->Z:Z

    iget-object v8, p0, Lelj;->V:Ljpl;

    .line 843
    invoke-virtual {v8}, Ljpl;->b()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lepu;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;)V

    goto :goto_0

    .line 845
    :cond_5
    iget v0, p0, Lelj;->P:I

    if-ne p1, v0, :cond_6

    .line 846
    new-instance v0, Ldyg;

    invoke-virtual {p0}, Lelj;->as()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lelj;->d:I

    sget-object v3, Lelj;->q:[Ljava/lang/String;

    iget-object v4, p0, Lelj;->g:Ljava/lang/String;

    const/4 v5, 0x2

    iget-boolean v6, p0, Lelj;->Z:Z

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Ldyg;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;)V

    goto/16 :goto_0

    .line 850
    :cond_6
    iget v0, p0, Lelj;->Q:I

    if-ne p1, v0, :cond_7

    .line 851
    new-instance v0, Ldyt;

    invoke-virtual {p0}, Lelj;->as()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lelj;->d:I

    sget-object v3, Lelj;->r:[Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Ldyt;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 854
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1484
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1488
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 739
    if-eqz p1, :cond_0

    .line 740
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 741
    const-string v0, "search_list_adapter.query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lelj;->g:Ljava/lang/String;

    .line 742
    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lelj;->W:Z

    if-nez v0, :cond_0

    .line 743
    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljpl;

    iput-object v0, p0, Lelj;->V:Ljpl;

    .line 746
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1445
    const-string v0, "add_email_dialog"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1446
    const-string v0, "message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1447
    invoke-direct {p0}, Lelj;->n()Ljava/lang/String;

    move-result-object v2

    .line 1448
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1449
    const-string v3, "e:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1450
    :goto_0
    iget-boolean v3, p0, Lelj;->e:Z

    if-eqz v3, :cond_2

    .line 1451
    iget-object v2, p0, Lelj;->f:Lelt;

    invoke-interface {v2, v0, v1, v5}, Lelt;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1472
    :cond_0
    :goto_1
    return-void

    .line 1449
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 1453
    :cond_2
    iget-object v3, p0, Lelj;->f:Lelt;

    new-instance v4, Ljqs;

    invoke-direct {v4, v5, v1, v2}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0, v5, v4, v5}, Lelt;->a(Ljava/lang/String;Ljava/lang/String;Ljqs;Landroid/os/Bundle;)V

    goto :goto_1

    .line 1458
    :cond_3
    const-string v0, "add_sms_dialog"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1459
    const-string v0, "message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1460
    invoke-direct {p0}, Lelj;->o()Ljava/lang/String;

    move-result-object v0

    .line 1461
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1462
    const-string v2, "p:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1463
    :goto_2
    iget-boolean v2, p0, Lelj;->e:Z

    if-eqz v2, :cond_5

    .line 1464
    iget-object v2, p0, Lelj;->f:Lelt;

    invoke-interface {v2, v0, v1, v5}, Lelt;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1462
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 1466
    :cond_5
    iget-object v2, p0, Lelj;->f:Lelt;

    new-instance v3, Ljqs;

    invoke-direct {v3, v5, v1, v0}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0, v5, v3, v5}, Lelt;->a(Ljava/lang/String;Ljava/lang/String;Ljqs;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1216
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 859
    invoke-virtual {p1}, Ldo;->o()I

    move-result v1

    .line 860
    iget v0, p0, Lelj;->K:I

    if-ne v1, v0, :cond_4

    .line 861
    if-nez p2, :cond_3

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lelj;->w:Z

    .line 862
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelj;->v:Z

    .line 863
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p2}, Lelj;->a(ILandroid/database/Cursor;)V

    .line 909
    :cond_0
    :goto_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lelj;->j(Z)V

    .line 910
    iget-object v0, p0, Lelj;->f:Lelt;

    if-eqz v0, :cond_1

    .line 911
    iget-object v0, p0, Lelj;->f:Lelt;

    invoke-interface {v0}, Lelt;->an_()V

    .line 913
    :cond_1
    iget v0, p0, Lelj;->ab:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lelj;->ab:I

    .line 914
    iget v0, p0, Lelj;->ab:I

    if-gtz v0, :cond_2

    .line 915
    invoke-direct {p0}, Lelj;->m()V

    .line 917
    :cond_2
    return-void

    .line 861
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 864
    :cond_4
    iget v0, p0, Lelj;->L:I

    if-ne v1, v0, :cond_9

    .line 865
    iget-object v0, p0, Lelj;->X:Landroid/database/Cursor;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lelj;->X:Landroid/database/Cursor;

    if-eq v0, p2, :cond_5

    iget-object v0, p0, Lelj;->X:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    iput-object p2, p0, Lelj;->X:Landroid/database/Cursor;

    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0}, Ljpl;->f()V

    if-eqz p2, :cond_8

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lelj;->J:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lelj;->V:Ljpl;

    const/4 v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_8
    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0}, Ljpl;->g()V

    invoke-direct {p0}, Lelj;->l()V

    goto :goto_1

    .line 866
    :cond_9
    iget v0, p0, Lelj;->N:I

    if-ne v1, v0, :cond_12

    .line 867
    if-nez p2, :cond_f

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lelj;->A:Z

    .line 868
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelj;->z:Z

    .line 869
    iget-object v0, p0, Lelj;->Y:Landroid/database/Cursor;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lelj;->Y:Landroid/database/Cursor;

    if-eq v0, p2, :cond_a

    iget-object v0, p0, Lelj;->Y:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_a
    iput-object p2, p0, Lelj;->Y:Landroid/database/Cursor;

    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0}, Ljpl;->j()V

    if-eqz p2, :cond_c

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_b
    iget-boolean v0, p0, Lelj;->i:Z

    if-eqz v0, :cond_10

    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_3
    iget-boolean v0, p0, Lelj;->i:Z

    if-eqz v0, :cond_11

    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_4
    iget-object v0, p0, Lelj;->V:Ljpl;

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v6}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_c
    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0}, Ljpl;->k()V

    invoke-direct {p0}, Lelj;->l()V

    .line 870
    new-instance v0, Lhym;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "address"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    if-eqz p2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_d

    invoke-direct {p0}, Lelj;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lhym;->a([Ljava/lang/Object;)V

    :cond_d
    const/4 v1, 0x3

    invoke-virtual {p0, v1, v0}, Lelj;->a(ILandroid/database/Cursor;)V

    .line 871
    iget-boolean v0, p0, Lelj;->i:Z

    if-eqz v0, :cond_0

    .line 872
    new-instance v0, Lhym;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "address"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lhym;-><init>([Ljava/lang/String;)V

    if-eqz p2, :cond_e

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_e

    invoke-direct {p0}, Lelj;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lhym;->a([Ljava/lang/Object;)V

    :cond_e
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lelj;->a(ILandroid/database/Cursor;)V

    goto/16 :goto_1

    .line 867
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 869
    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_11
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 874
    :cond_12
    iget v0, p0, Lelj;->M:I

    if-ne v1, v0, :cond_1a

    .line 875
    if-nez p2, :cond_17

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p0, Lelj;->y:Z

    .line 876
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelj;->x:Z

    .line 877
    invoke-direct {p0, v1, p2}, Lelj;->b(ILandroid/database/Cursor;)V

    .line 878
    iget-boolean v0, p0, Lelj;->I:Z

    iget-object v1, p0, Lelj;->V:Ljpl;

    invoke-virtual {v1}, Ljpl;->h()V

    iget-object v1, p0, Lelj;->b:Lhei;

    iget v2, p0, Lelj;->d:I

    invoke-interface {v1, v2}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_13

    iget-object v1, p0, Lelj;->b:Lhei;

    iget v2, p0, Lelj;->d:I

    invoke-interface {v1, v2}, Lhei;->a(I)Lhej;

    move-result-object v4

    const-string v1, "gaia_id"

    invoke-interface {v4, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "display_name"

    invoke-interface {v4, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "profile_photo_url"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "is_dasher_account"

    invoke-interface {v4, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v10

    if-eqz v0, :cond_13

    if-eqz v3, :cond_13

    iget-object v0, p0, Lelj;->g:Ljava/lang/String;

    if-eqz v0, :cond_13

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lelj;->V:Ljpl;

    const/4 v4, 0x1

    invoke-static {v5}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v0 .. v11}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    :cond_13
    if-eqz p2, :cond_16

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_14
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lelj;->J:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    iget-object v0, p0, Lelj;->V:Ljpl;

    const/4 v1, 0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x5

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x6

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x8

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-eqz v10, :cond_18

    const/4 v10, 0x1

    :goto_6
    const/16 v11, 0x9

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    if-eqz v11, :cond_19

    const/4 v11, 0x1

    :goto_7
    invoke-virtual/range {v0 .. v11}, Ljpl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    :cond_15
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_14

    :cond_16
    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0}, Ljpl;->i()V

    invoke-direct {p0}, Lelj;->l()V

    goto/16 :goto_1

    .line 875
    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 878
    :cond_18
    const/4 v10, 0x0

    goto :goto_6

    :cond_19
    const/4 v11, 0x0

    goto :goto_7

    .line 879
    :cond_1a
    iget v0, p0, Lelj;->O:I

    if-ne v1, v0, :cond_1c

    .line 880
    sget-object v0, Lepu;->b:Landroid/database/MatrixCursor;

    if-eq p2, v0, :cond_1b

    .line 881
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 882
    invoke-direct {p0, v1, p2}, Lelj;->b(ILandroid/database/Cursor;)V

    .line 883
    invoke-direct {p0, v1, p2}, Lelj;->c(ILandroid/database/Cursor;)I

    .line 885
    :cond_1b
    const/4 v0, 0x0

    iput-object v0, p0, Lelj;->R:Ljava/lang/Runnable;

    .line 886
    iget-object v0, p0, Lelj;->T:Ljava/util/HashSet;

    iget v1, p0, Lelj;->O:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 887
    :cond_1c
    iget v0, p0, Lelj;->P:I

    if-ne v1, v0, :cond_1e

    .line 888
    const/4 v0, 0x0

    .line 889
    sget-object v2, Ldyg;->b:Landroid/database/MatrixCursor;

    if-eq p2, v2, :cond_1d

    .line 890
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 891
    invoke-direct {p0, v1, p2}, Lelj;->b(ILandroid/database/Cursor;)V

    .line 892
    invoke-direct {p0, v1, p2}, Lelj;->c(ILandroid/database/Cursor;)I

    move-result v0

    .line 894
    :cond_1d
    iget-object v1, p0, Lelj;->T:Ljava/util/HashSet;

    iget v2, p0, Lelj;->P:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 897
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lelj;->R:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 898
    iget-object v0, p0, Lelj;->R:Ljava/lang/Runnable;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Llsx;->a(Ljava/lang/Runnable;Z)V

    goto/16 :goto_1

    .line 901
    :cond_1e
    iget v0, p0, Lelj;->Q:I

    if-ne v1, v0, :cond_0

    .line 902
    iget-object v0, p0, Lelj;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 903
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 905
    :cond_1f
    iget-object v0, p0, Lelj;->j:Ljava/util/Set;

    const/4 v1, 0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 906
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1f

    goto/16 :goto_1
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 60
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lelj;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lelt;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lelj;->f:Lelt;

    .line 573
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 564
    iput-object p1, p0, Lelj;->C:Ljava/lang/String;

    .line 565
    const/4 v0, 0x1

    iput-boolean v0, p0, Lelj;->B:Z

    .line 566
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 486
    iget-object v0, p0, Lelj;->J:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 487
    if-eqz p1, :cond_0

    .line 488
    iget-object v0, p0, Lelj;->J:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 490
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 479
    iput-boolean p1, p0, Lelj;->I:Z

    .line 480
    return-void
.end method

.method public b(I)V
    .locals 13

    .prologue
    const/4 v1, 0x2

    const/4 v8, 0x0

    const/4 v12, 0x3

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1262
    invoke-virtual {p0, p1}, Lelj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/database/Cursor;

    .line 1263
    if-nez v6, :cond_1

    .line 1375
    :cond_0
    :goto_0
    return-void

    .line 1267
    :cond_1
    iget v5, p0, Lelj;->d:I

    .line 1269
    invoke-virtual {p0, p1}, Lelj;->m(I)I

    move-result v0

    .line 1270
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1272
    :pswitch_0
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1273
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1274
    invoke-interface {v6, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1275
    const/16 v0, 0x8

    .line 1276
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v5, v7

    .line 1277
    :goto_1
    new-instance v0, Ljqs;

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1278
    iget-object v1, p0, Lelj;->f:Lelt;

    if-eqz v1, :cond_0

    .line 1279
    iget-object v1, p0, Lelj;->f:Lelt;

    .line 1280
    invoke-virtual {p0, v6}, Lelj;->a(Landroid/database/Cursor;)Landroid/os/Bundle;

    move-result-object v2

    .line 1279
    invoke-interface {v1, v9, v3, v0, v2}, Lelt;->a(Ljava/lang/String;Ljava/lang/String;Ljqs;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    move v5, v8

    .line 1276
    goto :goto_1

    .line 1286
    :pswitch_1
    invoke-virtual {p0}, Lelj;->as()Landroid/content/Context;

    move-result-object v4

    .line 1287
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1288
    invoke-interface {v6, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1289
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1290
    const/4 v3, 0x4

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1291
    new-instance v3, Lhxc;

    invoke-direct {v3, v2, v1, v0, v7}, Lhxc;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 1293
    iget v0, p0, Lelj;->d:I

    invoke-static {v4, v0, v1}, Lhxm;->a(Landroid/content/Context;II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1294
    invoke-static {v4, v5}, Ldhv;->m(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1295
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1296
    invoke-interface {v6, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1297
    const v0, 0x7f0a04f3

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1299
    const v6, 0x7f0a0596

    new-instance v0, Lelo;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lelo;-><init>(Lelj;Ljava/lang/String;Lhxc;Landroid/content/Context;I)V

    invoke-virtual {v7, v6, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1310
    const v0, 0x7f0a0597

    new-instance v1, Lelp;

    invoke-direct {v1}, Lelp;-><init>()V

    invoke-virtual {v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1319
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1321
    :cond_3
    iget-object v0, p0, Lelj;->f:Lelt;

    if-eqz v0, :cond_0

    .line 1322
    iget-object v0, p0, Lelj;->f:Lelt;

    invoke-interface {v0, v2, v3}, Lelt;->a(Ljava/lang/String;Lhxc;)V

    goto/16 :goto_0

    .line 1328
    :pswitch_2
    iget-object v0, p0, Lelj;->f:Lelt;

    if-eqz v0, :cond_0

    .line 1329
    iget-object v9, p0, Lelj;->f:Lelt;

    .line 1330
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1331
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1332
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x5

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v0, 0xc

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v5, v7

    :goto_2
    iget-boolean v0, p0, Lelj;->i:Z

    if-eqz v0, :cond_d

    const/16 v0, 0x9

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_d

    const-string v7, "p:"

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_4

    const/4 v0, 0x7

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_c

    const/16 v0, 0x8

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_b

    :goto_4
    new-instance v0, Ljqs;

    invoke-direct/range {v0 .. v5}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1333
    invoke-virtual {p0, v6}, Lelj;->a(Landroid/database/Cursor;)Landroid/os/Bundle;

    move-result-object v1

    .line 1329
    invoke-interface {v9, v10, v11, v0, v1}, Lelt;->a(Ljava/lang/String;Ljava/lang/String;Ljqs;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_5
    move v5, v8

    .line 1332
    goto :goto_2

    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 1338
    :pswitch_3
    iget-boolean v0, p0, Lelj;->e:Z

    if-nez v0, :cond_0

    .line 1340
    iget-boolean v0, p0, Lelj;->F:Z

    if-eqz v0, :cond_7

    .line 1342
    const-string v0, "add_email_dialog"

    invoke-virtual {p0, v0}, Lelj;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1345
    :cond_7
    invoke-direct {p0}, Lelj;->n()Ljava/lang/String;

    move-result-object v1

    .line 1346
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1347
    const-string v2, "e:"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1348
    :goto_5
    new-instance v2, Ljqs;

    invoke-direct {v2, v3, v1, v1}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1349
    iget-object v1, p0, Lelj;->f:Lelt;

    if-eqz v1, :cond_0

    .line 1350
    iget-object v1, p0, Lelj;->f:Lelt;

    invoke-interface {v1, v0, v3, v2, v3}, Lelt;->a(Ljava/lang/String;Ljava/lang/String;Ljqs;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1347
    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 1358
    :pswitch_4
    iget-boolean v0, p0, Lelj;->e:Z

    if-nez v0, :cond_0

    .line 1360
    iget-boolean v0, p0, Lelj;->F:Z

    if-eqz v0, :cond_9

    .line 1362
    const-string v0, "add_sms_dialog"

    invoke-virtual {p0, v0}, Lelj;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1365
    :cond_9
    invoke-direct {p0}, Lelj;->o()Ljava/lang/String;

    move-result-object v1

    .line 1366
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1367
    const-string v2, "p:"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1368
    :goto_6
    new-instance v2, Ljqs;

    invoke-direct {v2, v3, v1, v0}, Ljqs;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1369
    iget-object v1, p0, Lelj;->f:Lelt;

    if-eqz v1, :cond_0

    .line 1370
    iget-object v1, p0, Lelj;->f:Lelt;

    invoke-interface {v1, v0, v3, v2, v3}, Lelt;->a(Ljava/lang/String;Ljava/lang/String;Ljqs;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1367
    :cond_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    :cond_b
    move-object v3, v0

    goto/16 :goto_4

    :cond_c
    move-object v3, v0

    goto/16 :goto_4

    :cond_d
    move-object v0, v3

    goto/16 :goto_3

    .line 1270
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 752
    const-string v0, "search_list_adapter.query"

    iget-object v1, p0, Lelj;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0}, Ljpl;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 754
    const-string v0, "search_list_adapter.results"

    iget-object v1, p0, Lelj;->V:Ljpl;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 756
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1476
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 592
    iget-object v0, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 593
    invoke-direct {p0}, Lelj;->m()V

    .line 654
    :cond_0
    :goto_0
    return-void

    .line 597
    :cond_1
    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0, p1}, Ljpl;->c(Ljava/lang/String;)V

    .line 598
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 599
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 601
    iput-object p1, p0, Lelj;->g:Ljava/lang/String;

    .line 602
    iput v4, p0, Lelj;->ab:I

    .line 603
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 604
    iget-object v0, p0, Lelj;->a:Lbb;

    iget v1, p0, Lelj;->K:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 605
    iget-object v0, p0, Lelj;->a:Lbb;

    iget v1, p0, Lelj;->M:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 606
    iget-object v0, p0, Lelj;->a:Lbb;

    iget v1, p0, Lelj;->N:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 607
    iget-object v0, p0, Lelj;->a:Lbb;

    iget v1, p0, Lelj;->O:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 608
    iget-object v0, p0, Lelj;->a:Lbb;

    iget v1, p0, Lelj;->P:I

    invoke-virtual {v0, v1}, Lbb;->a(I)V

    .line 609
    iget-object v0, p0, Lelj;->T:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 610
    invoke-virtual {p0}, Lelj;->at()V

    .line 611
    invoke-direct {p0}, Lelj;->m()V

    .line 612
    iget-object v0, p0, Lelj;->f:Lelt;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lelj;->f:Lelt;

    invoke-interface {v0}, Lelt;->an_()V

    goto :goto_0

    .line 616
    :cond_2
    invoke-direct {p0}, Lelj;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 618
    iget v1, p0, Lelj;->u:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 619
    iget v1, p0, Lelj;->ab:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lelj;->ab:I

    .line 620
    iget-object v1, p0, Lelj;->a:Lbb;

    iget v2, p0, Lelj;->K:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 623
    :cond_3
    iget v1, p0, Lelj;->ab:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lelj;->ab:I

    .line 624
    iget-object v1, p0, Lelj;->a:Lbb;

    iget v2, p0, Lelj;->M:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 626
    iget-object v1, p0, Lelj;->b:Lhei;

    iget v2, p0, Lelj;->d:I

    invoke-interface {v1, v2}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 627
    const-string v2, "is_dasher_account"

    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    .line 629
    iget-boolean v2, p0, Lelj;->Z:Z

    if-nez v2, :cond_4

    if-nez v1, :cond_4

    .line 630
    iget v1, p0, Lelj;->ab:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lelj;->ab:I

    .line 631
    iget-object v1, p0, Lelj;->a:Lbb;

    iget v2, p0, Lelj;->N:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 634
    :cond_4
    iget-boolean v1, p0, Lelj;->D:Z

    if-eqz v1, :cond_0

    .line 635
    iget-object v1, p0, Lelj;->U:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 636
    iget-object v1, p0, Lelj;->T:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 638
    iget-object v1, p0, Lelj;->aa:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 644
    iget-object v1, p0, Lelj;->a:Lbb;

    iget v2, p0, Lelj;->O:I

    invoke-virtual {v1, v2}, Lbb;->a(I)V

    .line 645
    iget-object v1, p0, Lelj;->a:Lbb;

    iget v2, p0, Lelj;->P:I

    invoke-virtual {v1, v2}, Lbb;->a(I)V

    .line 647
    iget-object v1, p0, Lelj;->T:Ljava/util/HashSet;

    iget v2, p0, Lelj;->P:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 648
    iget-object v1, p0, Lelj;->a:Lbb;

    iget v2, p0, Lelj;->P:I

    invoke-virtual {v1, v2, v0, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 649
    invoke-direct {p0, v0}, Lelj;->c(Landroid/os/Bundle;)V

    .line 651
    invoke-direct {p0, v4}, Lelj;->j(Z)V

    goto/16 :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 503
    iput-boolean p1, p0, Lelj;->Z:Z

    .line 504
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lelj;->V:Ljpl;

    .line 658
    invoke-virtual {v0}, Ljpl;->d()I

    move-result v0

    if-nez v0, :cond_1

    .line 659
    invoke-virtual {p0}, Lelj;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lelj;->T:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1480
    return-void
.end method

.method protected c(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1421
    iget-object v0, p0, Lelj;->t:Lae;

    const-string v1, "add_person_dialog_listener"

    .line 1422
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lels;

    .line 1423
    if-nez v0, :cond_0

    .line 1424
    new-instance v0, Lels;

    invoke-direct {v0}, Lels;-><init>()V

    .line 1425
    iget-object v1, p0, Lelj;->t:Lae;

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const-string v2, "add_person_dialog_listener"

    invoke-virtual {v1, v0, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v1

    .line 1426
    invoke-virtual {v1}, Lat;->b()I

    move-object v6, v0

    .line 1428
    :goto_0
    invoke-virtual {v6, p0}, Lels;->a(Lelj;)V

    .line 1430
    invoke-virtual {p0}, Lelj;->as()Landroid/content/Context;

    move-result-object v4

    .line 1431
    const v0, 0x7f0a087d

    .line 1432
    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f0a087e

    .line 1434
    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x104000a

    .line 1435
    invoke-virtual {v4, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/high16 v7, 0x1040000

    .line 1436
    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1431
    invoke-static/range {v0 .. v5}, Leag;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Leag;

    move-result-object v0

    .line 1439
    invoke-virtual {v0, v6, v5}, Leag;->a(Lu;I)V

    .line 1440
    iget-object v1, p0, Lelj;->t:Lae;

    invoke-virtual {v0, v1, p1}, Leag;->a(Lae;Ljava/lang/String;)V

    .line 1441
    return-void

    :cond_0
    move-object v6, v0

    goto :goto_0
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 511
    iput-boolean p1, p0, Lelj;->e:Z

    .line 512
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 671
    iget-boolean v0, p0, Lelj;->w:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lelj;->y:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lelj;->A:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 519
    iput-boolean p1, p0, Lelj;->F:Z

    .line 520
    return-void
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 678
    iget-boolean v0, p0, Lelj;->x:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lelj;->z:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lelj;->u:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lelj;->v:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lelj;->c:Lhxh;

    .line 680
    invoke-virtual {v0}, Lhxh;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()V
    .locals 2

    .prologue
    .line 720
    invoke-direct {p0}, Lelj;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 736
    :goto_0
    return-void

    .line 725
    :cond_0
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    new-instance v1, Leln;

    invoke-direct {v1, p0, p0}, Leln;-><init>(Lelj;Lbc;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public e(Z)V
    .locals 0

    .prologue
    .line 526
    iput-boolean p1, p0, Lelj;->D:Z

    .line 527
    return-void
.end method

.method public f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 765
    iget-object v0, p0, Lelj;->b:Lhei;

    iget v1, p0, Lelj;->d:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 767
    iget-boolean v1, p0, Lelj;->I:Z

    if-nez v1, :cond_0

    .line 768
    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 769
    invoke-static {v1}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 770
    iget-object v2, p0, Lelj;->V:Ljpl;

    invoke-virtual {v2, v1}, Ljpl;->b(Ljava/lang/String;)V

    .line 772
    :cond_0
    invoke-direct {p0}, Lelj;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 774
    iget-object v2, p0, Lelj;->c:Lhxh;

    invoke-virtual {v2}, Lhxh;->b()V

    .line 775
    iget-object v2, p0, Lelj;->a:Lbb;

    iget v3, p0, Lelj;->L:I

    invoke-virtual {v2, v3, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 776
    iget-object v2, p0, Lelj;->a:Lbb;

    iget v3, p0, Lelj;->Q:I

    invoke-virtual {v2, v3, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 777
    iget v2, p0, Lelj;->u:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lelj;->g:Ljava/lang/String;

    .line 778
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 779
    iget-object v2, p0, Lelj;->a:Lbb;

    iget v3, p0, Lelj;->K:I

    invoke-virtual {v2, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 781
    :cond_1
    iget-object v2, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 782
    iget-object v2, p0, Lelj;->a:Lbb;

    iget v3, p0, Lelj;->M:I

    invoke-virtual {v2, v3, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 785
    :cond_2
    const-string v2, "domain_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lelj;->h:Ljava/lang/String;

    .line 786
    iget-boolean v2, p0, Lelj;->Z:Z

    if-nez v2, :cond_3

    const-string v2, "is_dasher_account"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 787
    iget-object v0, p0, Lelj;->a:Lbb;

    iget v2, p0, Lelj;->N:I

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 789
    :cond_3
    iget-boolean v0, p0, Lelj;->D:Z

    if-eqz v0, :cond_4

    .line 790
    iget-object v0, p0, Lelj;->T:Ljava/util/HashSet;

    iget v2, p0, Lelj;->P:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 791
    iget-object v0, p0, Lelj;->a:Lbb;

    iget v2, p0, Lelj;->P:I

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 792
    invoke-direct {p0, v1}, Lelj;->c(Landroid/os/Bundle;)V

    .line 794
    :cond_4
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lelj;->j(Z)V

    .line 796
    iget-object v0, p0, Lelj;->t:Lae;

    const-string v1, "add_person_dialog_listener"

    .line 797
    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lels;

    .line 798
    if-eqz v0, :cond_5

    .line 799
    invoke-virtual {v0, p0}, Lels;->a(Lelj;)V

    .line 801
    :cond_5
    return-void
.end method

.method public f(Z)V
    .locals 0

    .prologue
    .line 533
    iput-boolean p1, p0, Lelj;->i:Z

    .line 534
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 804
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 805
    return-void
.end method

.method public g(Z)V
    .locals 0

    .prologue
    .line 540
    iput-boolean p1, p0, Lelj;->G:Z

    .line 541
    return-void
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1534
    iget-object v0, p0, Lelj;->ac:Landroid/widget/Filter;

    if-nez v0, :cond_0

    .line 1535
    new-instance v0, Lelq;

    invoke-direct {v0, p0}, Lelq;-><init>(Lelj;)V

    iput-object v0, p0, Lelj;->ac:Landroid/widget/Filter;

    .line 1537
    :cond_0
    iget-object v0, p0, Lelj;->ac:Landroid/widget/Filter;

    return-object v0
.end method

.method protected h()V
    .locals 2

    .prologue
    .line 1129
    iget-object v0, p0, Lelj;->aa:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1130
    iget-object v0, p0, Lelj;->V:Ljpl;

    invoke-virtual {v0}, Ljpl;->m()Landroid/database/Cursor;

    move-result-object v0

    .line 1131
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 1132
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lelj;->a(ILandroid/database/Cursor;)V

    .line 1134
    :cond_0
    return-void
.end method

.method public h(Z)V
    .locals 2

    .prologue
    .line 547
    iput-boolean p1, p0, Lelj;->H:Z

    .line 548
    iget-object v0, p0, Lelj;->V:Ljpl;

    iget-boolean v1, p0, Lelj;->H:Z

    invoke-virtual {v0, v1}, Ljpl;->a(Z)V

    .line 549
    return-void
.end method

.method public i(Z)V
    .locals 0

    .prologue
    .line 557
    iput-boolean p1, p0, Lelj;->E:Z

    .line 558
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 664
    iget-object v0, p0, Lelj;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lelj;->c:Lhxh;

    invoke-virtual {v0}, Lhxh;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j_(I)V
    .locals 0

    .prologue
    .line 496
    iput p1, p0, Lelj;->u:I

    .line 497
    return-void
.end method
