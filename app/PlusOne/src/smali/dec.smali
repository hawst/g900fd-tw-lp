.class public Ldec;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljlw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljlw",
        "<",
        "Ldec;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljlx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlx",
            "<",
            "Ldec;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:I

.field private h:Ljava/lang/String;

.field private i:I

.field private j:Z


# direct methods
.method public constructor <init>(Lhov;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljls;

    invoke-direct {v0, p0, p1}, Ljls;-><init>(Ljava/lang/Object;Lhov;)V

    iput-object v0, p0, Ldec;->a:Ljlx;

    .line 47
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Ldec;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 104
    iput p1, p0, Ldec;->g:I

    .line 105
    invoke-virtual {p0}, Ldec;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 106
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 95
    iput-wide p1, p0, Ldec;->f:J

    .line 96
    invoke-virtual {p0}, Ldec;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 97
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    iput-object p1, p0, Ldec;->b:Ljava/lang/String;

    .line 60
    invoke-virtual {p0}, Ldec;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 61
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 131
    iput-boolean p1, p0, Ldec;->j:Z

    .line 132
    invoke-virtual {p0}, Ldec;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 133
    return-void
.end method

.method public b()Ljlx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljlx",
            "<",
            "Ldec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Ldec;->a:Ljlx;

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 122
    iput p1, p0, Ldec;->i:I

    .line 123
    invoke-virtual {p0}, Ldec;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 124
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 68
    iput-object p1, p0, Ldec;->c:Ljava/lang/String;

    .line 69
    invoke-virtual {p0}, Ldec;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 70
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Ldec;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Ldec;->d:Ljava/lang/String;

    .line 78
    invoke-virtual {p0}, Ldec;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 79
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ldec;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 86
    iput-object p1, p0, Ldec;->e:Ljava/lang/String;

    .line 87
    invoke-virtual {p0}, Ldec;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 88
    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ldec;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 113
    iput-object p1, p0, Ldec;->h:Ljava/lang/String;

    .line 114
    invoke-virtual {p0}, Ldec;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 115
    return-void
.end method

.method public f()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Ldec;->f:J

    return-wide v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Ldec;->g:I

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ldec;->h:Ljava/lang/String;

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Ldec;->i:I

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Ldec;->j:Z

    return v0
.end method
