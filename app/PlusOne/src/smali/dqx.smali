.class public final Ldqx;
.super Ldxc;
.source "PG"

# interfaces
.implements Lkdd;


# static fields
.field static d:Lizs;


# instance fields
.field final a:Landroid/content/Context;

.field b:Ldxb;

.field c:Landroid/content/Intent;

.field e:Lnzi;

.field f:I

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field private j:Landroid/app/Activity;

.field private k:Lcom/google/android/libraries/social/media/MediaResource;

.field private l:Ljava/lang/Integer;

.field private m:Z

.field private n:I

.field private o:Landroid/content/DialogInterface$OnClickListener;

.field private p:Landroid/content/DialogInterface$OnCancelListener;

.field private final q:Lfhh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0}, Ldxc;-><init>()V

    .line 110
    const/4 v0, 0x0

    iput v0, p0, Ldqx;->n:I

    .line 111
    const/4 v0, -0x1

    iput v0, p0, Ldqx;->f:I

    .line 114
    new-instance v0, Ldrc;

    invoke-direct {v0, p0}, Ldrc;-><init>(Ldqx;)V

    iput-object v0, p0, Ldqx;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 115
    new-instance v0, Ldrb;

    invoke-direct {v0, p0}, Ldrb;-><init>(Ldqx;)V

    iput-object v0, p0, Ldqx;->p:Landroid/content/DialogInterface$OnCancelListener;

    .line 117
    new-instance v0, Ldqy;

    invoke-direct {v0, p0}, Ldqy;-><init>(Ldqx;)V

    iput-object v0, p0, Ldqx;->q:Lfhh;

    .line 163
    iput-object p1, p0, Ldqx;->a:Landroid/content/Context;

    .line 164
    return-void
.end method

.method private a([B)Lnzi;
    .locals 3

    .prologue
    .line 1126
    :try_start_0
    new-instance v0, Lnzi;

    invoke-direct {v0}, Lnzi;-><init>()V

    invoke-static {v0, p1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzi;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 1129
    :goto_0
    return-object v0

    .line 1127
    :catch_0
    move-exception v0

    .line 1128
    const-string v1, "DefaultEditorProvider"

    const-string v2, "Failed to deserialize EditInfo."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1129
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 3

    .prologue
    .line 501
    iget-object v0, p0, Ldqx;->c:Landroid/content/Intent;

    const-string v1, "save_photo_edits"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private m()I
    .locals 3

    .prologue
    .line 505
    iget-object v0, p0, Ldqx;->c:Landroid/content/Intent;

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 531
    new-instance v0, Ldra;

    const/4 v1, 0x3

    invoke-direct {v0, p0, v1}, Ldra;-><init>(Ldqx;I)V

    .line 532
    invoke-virtual {v0}, Ldra;->a()Ljava/lang/String;

    move-result-object v0

    .line 533
    new-instance v1, Ldra;

    const/4 v2, 0x2

    invoke-direct {v1, p0, v2}, Ldra;-><init>(Ldqx;I)V

    .line 534
    invoke-virtual {v1}, Ldra;->a()Ljava/lang/String;

    move-result-object v1

    .line 536
    new-instance v2, Ldre;

    invoke-direct {v2, p0, v0, v1}, Ldre;-><init>(Ldqx;Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Ldre;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 539
    return-void
.end method


# virtual methods
.method protected a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 1085
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1086
    iget-object v1, p0, Ldqx;->j:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 1087
    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1088
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1089
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1090
    if-ge v0, v1, :cond_0

    .line 1091
    add-int/lit8 v0, v0, -0x28

    .line 1092
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    mul-int/2addr v2, v0

    div-int/2addr v2, v1

    .line 1093
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v0, v3

    div-int/2addr v0, v1

    .line 1094
    const/4 v1, 0x1

    invoke-static {p1, v2, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 1096
    :cond_0
    return-object p1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Ldqx;->q:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 223
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 282
    if-nez p1, :cond_1

    .line 283
    iget-object v0, p0, Ldqx;->j:Landroid/app/Activity;

    invoke-static {v0}, Liew;->a(Landroid/app/Activity;)V

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 285
    iget-object v0, p0, Ldqx;->j:Landroid/app/Activity;

    const-string v1, "plus_stream"

    const-string v2, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v0, v1, v2}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 286
    iget-object v1, p0, Ldqx;->j:Landroid/app/Activity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method a(ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Ldqx;->q:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 515
    iget-object v0, p0, Ldqx;->b:Ldxb;

    invoke-interface {v0, p1, p2}, Ldxb;->a(ILandroid/content/Intent;)V

    .line 516
    return-void
.end method

.method protected a(ILfib;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 414
    iget-object v0, p0, Ldqx;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqx;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 418
    :cond_1
    iput-object v1, p0, Ldqx;->l:Ljava/lang/Integer;

    .line 421
    instance-of v0, p2, Lffn;

    if-eqz v0, :cond_7

    move-object v0, p2

    .line 422
    check-cast v0, Lffn;

    invoke-virtual {v0}, Lffn;->a()[B

    move-result-object v0

    .line 423
    if-eqz v0, :cond_2

    .line 425
    :try_start_0
    new-instance v2, Lnzi;

    invoke-direct {v2}, Lnzi;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzi;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v0

    .line 434
    :goto_1
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 437
    if-eqz v2, :cond_6

    iget-object v0, v2, Lnzi;->b:Lpla;

    if-eqz v0, :cond_6

    .line 438
    iget-object v0, v2, Lnzi;->b:Lpla;

    iget-object v0, v0, Lpla;->b:Ljava/lang/Long;

    .line 442
    :goto_2
    iget-object v1, p0, Ldqx;->e:Lnzi;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ldqx;->e:Lnzi;

    iget-object v1, v1, Lnzi;->b:Lpla;

    if-eqz v1, :cond_3

    iget v1, p0, Ldqx;->n:I

    if-gtz v1, :cond_3

    .line 443
    iget-object v1, p0, Ldqx;->e:Lnzi;

    iget-object v1, v1, Lnzi;->b:Lpla;

    iget-object v1, v1, Lpla;->b:Ljava/lang/Long;

    .line 445
    invoke-static {v1}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v4

    .line 446
    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_3

    .line 447
    iget-object v1, p0, Ldqx;->e:Lnzi;

    iget-object v1, v1, Lnzi;->b:Lpla;

    iput-object v0, v1, Lpla;->b:Ljava/lang/Long;

    .line 448
    invoke-virtual {p0}, Ldqx;->i()Lizu;

    move-result-object v4

    .line 449
    iget-object v0, p0, Ldqx;->e:Lnzi;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v5

    .line 451
    iget v0, p0, Ldqx;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldqx;->n:I

    .line 452
    iget-object v0, p0, Ldqx;->a:Landroid/content/Context;

    invoke-direct {p0}, Ldqx;->m()I

    move-result v1

    .line 453
    invoke-virtual {v4}, Lizu;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lizu;->b()Ljava/lang/String;

    move-result-object v3

    .line 454
    invoke-virtual {v4}, Lizu;->a()Ljava/lang/String;

    move-result-object v4

    iget-boolean v6, p0, Ldqx;->m:Z

    .line 452
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[BZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqx;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 426
    :catch_0
    move-exception v0

    .line 427
    const-string v2, "DefaultEditorProvider"

    const-string v3, "Failed to deserialize EditInfo."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v2, v1

    .line 428
    goto :goto_1

    :cond_2
    move-object v2, v1

    .line 430
    goto :goto_1

    .line 459
    :cond_3
    iput v8, p0, Ldqx;->n:I

    .line 461
    iget-object v0, p0, Ldqx;->a:Landroid/content/Context;

    const v1, 0x7f0a058e

    const/4 v3, 0x1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 463
    if-eqz v2, :cond_4

    iget-object v0, p0, Ldqx;->b:Ldxb;

    .line 464
    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Ldxb;->a([B)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 465
    iput-object v2, p0, Ldqx;->e:Lnzi;

    .line 466
    invoke-direct {p0}, Ldqx;->n()V

    .line 473
    :cond_4
    :goto_3
    iput-boolean v8, p0, Ldqx;->m:Z

    goto/16 :goto_0

    .line 469
    :cond_5
    iput-object v2, p0, Ldqx;->e:Lnzi;

    .line 470
    invoke-direct {p0}, Ldqx;->n()V

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto/16 :goto_2

    :cond_7
    move-object v2, v1

    goto/16 :goto_1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Ldqx;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "pending_request_id"

    iget-object v1, p0, Ldqx;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 218
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;Landroid/app/Activity;Landroid/content/Intent;Ldxb;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 169
    iput-object p2, p0, Ldqx;->j:Landroid/app/Activity;

    .line 170
    iput-object p3, p0, Ldqx;->c:Landroid/content/Intent;

    .line 171
    iput-object p4, p0, Ldqx;->b:Ldxb;

    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldqx;->m:Z

    .line 174
    iput-object v1, p0, Ldqx;->e:Lnzi;

    .line 175
    const/4 v0, -0x1

    iput v0, p0, Ldqx;->f:I

    .line 176
    iput-object v1, p0, Ldqx;->g:Ljava/lang/String;

    .line 177
    iput-object v1, p0, Ldqx;->h:Ljava/lang/String;

    .line 179
    sget-object v0, Ldqx;->d:Lizs;

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Ldqx;->a:Landroid/content/Context;

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Ldqx;->d:Lizs;

    .line 183
    :cond_0
    invoke-virtual {p0}, Ldqx;->j()Lizu;

    move-result-object v0

    .line 184
    if-nez v0, :cond_1

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A source URI must be specified via the Intent\'s data field."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :cond_1
    if-eqz p1, :cond_2

    const-string v0, "pending_request_id"

    .line 190
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqx;->l:Ljava/lang/Integer;

    .line 194
    :cond_2
    iget-object v0, p0, Ldqx;->c:Landroid/content/Intent;

    const-string v1, "edit_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 195
    iget-object v0, p0, Ldqx;->c:Landroid/content/Intent;

    const-string v1, "edit_info"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Ldqx;->a([B)Lnzi;

    move-result-object v0

    iput-object v0, p0, Ldqx;->e:Lnzi;

    .line 199
    :cond_3
    iget-object v0, p0, Ldqx;->e:Lnzi;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldqx;->b:Ldxb;

    iget-object v1, p0, Ldqx;->e:Lnzi;

    .line 200
    invoke-static {v1}, Loxu;->a(Loxu;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Ldxb;->a([B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 201
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Ldqx;->j:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0ac7

    .line 202
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0ac8

    .line 203
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0ac9

    iget-object v2, p0, Ldqx;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 204
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0597

    iget-object v2, p0, Ldqx;->o:Landroid/content/DialogInterface$OnClickListener;

    .line 205
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Ldqx;->p:Landroid/content/DialogInterface$OnCancelListener;

    .line 206
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 211
    :goto_0
    return-void

    .line 209
    :cond_4
    invoke-virtual {p0}, Ldqx;->b()V

    goto :goto_0
.end method

.method public a(Ldxa;)V
    .locals 10

    .prologue
    const/high16 v9, -0x80000000

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 243
    invoke-virtual {p1}, Ldxa;->b()[B

    move-result-object v2

    invoke-direct {p0, v2}, Ldqx;->a([B)Lnzi;

    move-result-object v3

    iget-object v2, p0, Ldqx;->e:Lnzi;

    if-eqz v2, :cond_3

    iget-object v2, p0, Ldqx;->e:Lnzi;

    iget-object v2, v2, Lnzi;->b:Lpla;

    if-eqz v2, :cond_3

    iget-object v2, v3, Lnzi;->b:Lpla;

    iget-object v4, p0, Ldqx;->e:Lnzi;

    iget-object v4, v4, Lnzi;->b:Lpla;

    iget-object v4, v4, Lpla;->b:Ljava/lang/Long;

    iput-object v4, v2, Lpla;->b:Ljava/lang/Long;

    .line 244
    :goto_0
    iget-object v4, p0, Ldqx;->e:Lnzi;

    invoke-static {v3}, Ljub;->c(Lnzi;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v3}, Ljub;->c(Lnzi;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, v3, Lnzi;->b:Lpla;

    iget-object v5, v2, Lpla;->a:[Lpme;

    array-length v6, v5

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_5

    aget-object v7, v5, v2

    invoke-static {v7, v0}, Ljub;->a(Lpme;I)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v8, 0x3

    invoke-static {v7, v8}, Ljub;->a(Lpme;I)Z

    move-result v7

    if-nez v7, :cond_4

    move v2, v1

    :goto_2
    if-nez v2, :cond_0

    iget-object v2, v3, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->e:Lplb;

    if-eqz v2, :cond_0

    iget-object v2, v3, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->e:Lplb;

    iget-object v2, v2, Lplb;->a:Lphc;

    if-eqz v2, :cond_0

    iget-object v2, v3, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->e:Lplb;

    iget-object v2, v2, Lplb;->a:Lphc;

    iget v2, v2, Lphc;->a:I

    if-ne v2, v9, :cond_6

    :cond_0
    move v0, v1

    :cond_1
    :goto_3
    iput-boolean v0, p0, Ldqx;->m:Z

    .line 245
    iput-object v3, p0, Ldqx;->e:Lnzi;

    .line 247
    invoke-direct {p0}, Ldqx;->l()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 248
    iget-object v0, p0, Ldqx;->e:Lnzi;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v5

    .line 250
    invoke-virtual {p0}, Ldqx;->i()Lizu;

    move-result-object v4

    .line 251
    iget-object v0, p0, Ldqx;->a:Landroid/content/Context;

    invoke-direct {p0}, Ldqx;->m()I

    move-result v1

    .line 252
    invoke-virtual {v4}, Lizu;->c()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lizu;->b()Ljava/lang/String;

    move-result-object v3

    .line 253
    invoke-virtual {v4}, Lizu;->a()Ljava/lang/String;

    move-result-object v4

    iget-boolean v6, p0, Ldqx;->m:Z

    .line 251
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[BZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ldqx;->l:Ljava/lang/Integer;

    .line 263
    :cond_2
    :goto_4
    return-void

    .line 243
    :cond_3
    iget-object v2, v3, Lnzi;->b:Lpla;

    const/4 v4, 0x0

    iput-object v4, v2, Lpla;->b:Ljava/lang/Long;

    goto :goto_0

    .line 244
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v2, v0

    goto :goto_2

    :cond_6
    invoke-static {v4}, Ljub;->c(Lnzi;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v4, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->e:Lplb;

    if-eqz v2, :cond_1

    iget-object v2, v4, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->e:Lplb;

    iget-object v2, v2, Lplb;->a:Lphc;

    if-eqz v2, :cond_1

    iget-object v2, v4, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->e:Lplb;

    iget-object v2, v2, Lplb;->a:Lphc;

    iget v2, v2, Lphc;->a:I

    if-eq v2, v9, :cond_1

    iget-object v2, v3, Lnzi;->b:Lpla;

    iget-object v2, v2, Lpla;->e:Lplb;

    iget-object v2, v2, Lplb;->a:Lphc;

    iget v2, v2, Lphc;->a:I

    iget-object v4, v4, Lnzi;->b:Lpla;

    iget-object v4, v4, Lpla;->e:Lplb;

    iget-object v4, v4, Lplb;->a:Lphc;

    iget v4, v4, Lphc;->a:I

    if-ne v2, v4, :cond_1

    move v0, v1

    goto :goto_3

    .line 255
    :cond_7
    iget-object v0, p0, Ldqx;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 256
    if-eqz v0, :cond_8

    const-string v2, "force_return_edit_list"

    .line 257
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 258
    invoke-direct {p0}, Ldqx;->n()V

    goto :goto_4

    .line 260
    :cond_8
    invoke-virtual {p0}, Ldqx;->j()Lizu;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v2, Ldrd;

    invoke-direct {v2, p0, p1, v0}, Ldrd;-><init>(Ldqx;Ldxa;Lizu;)V

    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Ldrd;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_4
.end method

.method public a(Lkda;)V
    .locals 3

    .prologue
    .line 372
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    .line 373
    packed-switch v0, :pswitch_data_0

    .line 406
    :pswitch_0
    const-string v1, "DefaultEditorProvider"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 407
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "A unhandled case: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 375
    :pswitch_1
    invoke-virtual {p1}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    .line 376
    instance-of v1, v0, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 377
    check-cast v0, Landroid/graphics/Bitmap;

    .line 378
    new-instance v1, Ldqz;

    invoke-direct {v1, p0, v0}, Ldqz;-><init>(Ldqx;Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    .line 392
    invoke-virtual {v1, v0}, Ldqz;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 402
    :pswitch_2
    iget-object v0, p0, Ldqx;->b:Ldxb;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ldxb;->a(Ldxa;)V

    goto :goto_0

    .line 373
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v3, 0x5

    .line 335
    invoke-virtual {p0}, Ldqx;->j()Lizu;

    move-result-object v4

    .line 336
    if-eqz v4, :cond_1

    .line 343
    iget-object v0, p0, Ldqx;->a:Landroid/content/Context;

    const-class v1, Ljgn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    .line 344
    invoke-interface {v0}, Ljgn;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x8000

    .line 347
    :goto_0
    invoke-direct {p0}, Ldqx;->l()Z

    move-result v1

    if-eqz v1, :cond_3

    move v2, v3

    .line 349
    :goto_1
    const/16 v1, 0x10

    .line 350
    if-ne v2, v3, :cond_4

    .line 351
    or-int/lit8 v0, v0, 0x20

    or-int/lit8 v0, v0, 0x10

    .line 354
    :goto_2
    iget-object v1, p0, Ldqx;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 355
    if-eqz v1, :cond_0

    iget-object v1, p0, Ldqx;->c:Landroid/content/Intent;

    const-string v3, "base_photo_media_ref"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 356
    or-int/lit8 v0, v0, 0x1

    .line 358
    :cond_0
    sget-object v1, Ldqx;->d:Lizs;

    invoke-virtual {v1, v4, v2, v0, p0}, Lizs;->a(Lizu;IILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Ldqx;->k:Lcom/google/android/libraries/social/media/MediaResource;

    .line 360
    :cond_1
    return-void

    .line 344
    :cond_2
    const/16 v0, 0x1000

    goto :goto_0

    .line 347
    :cond_3
    const/4 v1, 0x1

    move v2, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public c()V
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Ldqx;->k:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Ldqx;->k:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 366
    const/4 v0, 0x0

    iput-object v0, p0, Ldqx;->k:Lcom/google/android/libraries/social/media/MediaResource;

    .line 368
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Ldqx;->q:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 229
    iget-object v0, p0, Ldqx;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldqx;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Ldqx;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 231
    iget-object v1, p0, Ldqx;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Ldqx;->a(ILfib;)V

    .line 233
    :cond_0
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 237
    invoke-virtual {p0}, Ldqx;->c()V

    .line 238
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 267
    iget v0, p0, Ldqx;->f:I

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Ldqx;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Ldqx;->h:Ljava/lang/String;

    return-object v0
.end method

.method i()Lizu;
    .locals 2

    .prologue
    .line 477
    iget-object v0, p0, Ldqx;->c:Landroid/content/Intent;

    const-string v1, "photo_ref"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    return-object v0
.end method

.method j()Lizu;
    .locals 4

    .prologue
    .line 481
    iget-object v0, p0, Ldqx;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 482
    if-eqz v0, :cond_0

    const-string v1, "base_photo_media_ref"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 483
    const-string v1, "base_photo_media_ref"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    .line 493
    :goto_0
    return-object v0

    .line 486
    :cond_0
    iget-object v0, p0, Ldqx;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 487
    if-eqz v0, :cond_1

    .line 490
    iget-object v1, p0, Ldqx;->a:Landroid/content/Context;

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v0

    goto :goto_0

    .line 493
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
