.class public final Lfba;
.super Lfdj;
.source "PG"


# static fields
.field private static l:I

.field private static o:I

.field private static p:Landroid/graphics/drawable/Drawable;

.field private static q:Landroid/graphics/drawable/LayerDrawable;

.field private static final r:[Ljava/lang/String;

.field private static final s:[Ljava/lang/String;

.field private static final t:[Ljava/lang/String;


# instance fields
.field private A:Ljms;

.field private B:Ljmx;

.field private C:Lgas;

.field private D:Ljnj;

.field private E:Ljns;

.field private F:Ljmi;

.field private G:Ljmy;

.field private H:Ljnk;

.field private I:Lfzc;

.field private J:Lfyv;

.field private K:Lfys;

.field private L:Ljava/lang/String;

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:Ldsx;

.field private Q:Lnjt;

.field private R:Lnkm;

.field private S:Lmcf;

.field private T:Lmdp;

.field private U:Lnlp;

.field private V:Lnts;

.field private W:Lnto;

.field private X:Lnso;

.field private Y:Z

.field private Z:Z

.field public a:Levi;

.field private aA:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

.field private aB:I

.field private aC:Loo;

.field private aD:Lhym;

.field private aE:Ljava/lang/String;

.field private aF:Lhym;

.field private aG:Lhym;

.field private aH:Ljava/lang/String;

.field private aI:Z

.field private aJ:Ljava/lang/Runnable;

.field private aK:Z

.field private aL:Lhxh;

.field private aM:Z

.field private aN:Z

.field private aO:Z

.field private aP:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private aQ:Z

.field private aR:I

.field private aS:Lgcs;

.field private aa:Z

.field private ab:Z

.field private ac:Ljava/lang/String;

.field private ad:Z

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private ah:I

.field private ai:Ljava/lang/String;

.field private aj:Ljava/lang/String;

.field private ak:Ljava/lang/String;

.field private al:Ljava/lang/String;

.field private am:Ljava/lang/String;

.field private an:Ljava/lang/String;

.field private ao:I

.field private ap:Z

.field private aq:Z

.field private ar:Z

.field private as:I

.field private at:Lnjp;

.field private au:I

.field private av:Z

.field private aw:Ljava/lang/String;

.field private ax:Ljava/lang/String;

.field private ay:I

.field private az:Z

.field b:Llea;

.field c:Lcom/google/android/apps/plus/views/OneProfileHeader;

.field d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

.field e:Z

.field f:I

.field private final m:Ljava/lang/String;

.field private final n:Ljava/lang/String;

.field private u:Landroid/view/LayoutInflater;

.field private v:Lfyz;

.field private w:Llhl;

.field private x:Ljnt;

.field private y:Ljni;

.field private z:Ljmv;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 154
    const/4 v0, -0x1

    sput v0, Lfba;->l:I

    .line 162
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "type"

    aput-object v1, v0, v2

    const-string v1, "reviewId"

    aput-object v1, v0, v3

    sput-object v0, Lfba;->r:[Ljava/lang/String;

    .line 166
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "type"

    aput-object v1, v0, v2

    const-string v1, "index"

    aput-object v1, v0, v3

    sput-object v0, Lfba;->s:[Ljava/lang/String;

    .line 170
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "type"

    aput-object v1, v0, v2

    const-string v1, "index"

    aput-object v1, v0, v3

    sput-object v0, Lfba;->t:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)V
    .locals 10

    .prologue
    .line 288
    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lfdj;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)V

    .line 246
    const/4 v1, 0x1

    iput-boolean v1, p0, Lfba;->az:Z

    .line 278
    const/4 v1, -0x1

    iput v1, p0, Lfba;->aR:I

    .line 291
    const-class v1, Lhei;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhei;

    .line 292
    invoke-interface {v1, p4}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 293
    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lfba;->m:Ljava/lang/String;

    .line 294
    const-string v2, "domain_name"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfba;->n:Ljava/lang/String;

    .line 296
    invoke-virtual {p0}, Lfba;->as()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lgcs;

    invoke-static {v1, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgcs;

    iput-object v1, p0, Lfba;->aS:Lgcs;

    .line 298
    sget v1, Lfba;->l:I

    if-gez v1, :cond_0

    .line 299
    invoke-super {p0}, Lfdj;->getViewTypeCount()I

    move-result v1

    sput v1, Lfba;->l:I

    .line 302
    :cond_0
    sget v1, Lfba;->o:I

    if-nez v1, :cond_1

    .line 303
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 304
    const v2, 0x7f0d032f

    .line 305
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lfba;->o:I

    .line 306
    const v2, 0x7f02003f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lfba;->p:Landroid/graphics/drawable/Drawable;

    .line 307
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    const v5, 0x7f020041

    .line 308
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    sget-object v4, Lfba;->p:Landroid/graphics/drawable/Drawable;

    aput-object v4, v3, v1

    invoke-direct {v2, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    sput-object v2, Lfba;->q:Landroid/graphics/drawable/LayerDrawable;

    .line 313
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b()Llea;

    move-result-object v1

    iput-object v1, p0, Lfba;->b:Llea;

    .line 314
    new-instance v1, Lfbb;

    invoke-direct {v1, p0}, Lfbb;-><init>(Lfba;)V

    .line 329
    invoke-virtual {p2, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Llea;)V

    .line 331
    iput-object p2, p0, Lfba;->aA:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    .line 332
    move-object/from16 v0, p7

    iput-object v0, p0, Lfba;->h:Levp;

    .line 334
    new-instance v1, Lfbc;

    invoke-direct {v1, p0}, Lfbc;-><init>(Lfba;)V

    iput-object v1, p0, Lfba;->aJ:Ljava/lang/Runnable;

    .line 342
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lfba;->u:Landroid/view/LayoutInflater;

    .line 344
    sget-object v1, Lcom/google/android/apps/plus/phone/OneProfileActivity;->e:Lloy;

    .line 345
    return-void
.end method

.method static synthetic a(Lfba;)Z
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lfba;->ax()Z

    move-result v0

    return v0
.end method

.method private a(Lnfq;)Z
    .locals 1

    .prologue
    .line 675
    iget-object v0, p1, Lnfq;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lpvl;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 593
    iget-object v1, p1, Lpvl;->c:Lpvm;

    .line 594
    iget-object v2, p1, Lpvl;->d:Lpvn;

    .line 595
    if-eqz v2, :cond_0

    .line 596
    iget-object v2, v2, Lpvn;->a:Ljava/lang/String;

    .line 597
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    .line 599
    :cond_0
    return v0
.end method

.method private ax()Z
    .locals 2

    .prologue
    .line 2360
    sget-object v0, Leet;->N:Llpa;

    iget v0, p0, Lfba;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lnfq;)I
    .locals 1

    .prologue
    .line 679
    iget-object v0, p1, Lnfq;->a:[Lnym;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lnfq;->a:[Lnym;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lpvl;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 603
    const/4 v1, 0x0

    .line 604
    iget-object v2, p1, Lpvl;->b:[Lpvt;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lpvl;->b:[Lpvt;

    array-length v2, v2

    if-eqz v2, :cond_0

    .line 605
    iget-object v1, p1, Lpvl;->b:[Lpvt;

    aget-object v1, v1, v0

    .line 606
    iget-object v1, v1, Lpvt;->d:[Lpvi;

    .line 608
    :cond_0
    if-eqz v1, :cond_1

    array-length v0, v1

    :cond_1
    return v0
.end method

.method static synthetic b(Lfba;)Lfyv;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lfba;->J:Lfyv;

    return-object v0
.end method

.method static synthetic c(Lfba;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lfba;->k:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lfba;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lfba;->k:Landroid/content/Context;

    return-object v0
.end method

.method private p(I)I
    .locals 3

    .prologue
    .line 2342
    const/16 v0, 0x19

    .line 2343
    invoke-direct {p0}, Lfba;->ax()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2344
    iget-object v0, p0, Lfba;->a:Levi;

    invoke-virtual {v0}, Levi;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x19

    .line 2346
    :cond_0
    if-ltz p1, :cond_1

    if-lt p1, v0, :cond_2

    .line 2347
    :cond_1
    const-string v0, "OneProfileStreamAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x21

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected view type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2348
    const/4 v0, 0x0

    .line 2350
    :goto_0
    return v0

    :cond_2
    sget v0, Lfba;->l:I

    add-int/2addr v0, p1

    goto :goto_0
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1997
    invoke-virtual {p0}, Lfba;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    if-nez v0, :cond_1

    .line 1998
    :cond_0
    const/4 v0, 0x0

    .line 2000
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 2004
    iget-boolean v0, p0, Lfba;->ag:Z

    return v0
.end method

.method public C()Z
    .locals 1

    .prologue
    .line 2042
    iget-object v0, p0, Lfba;->al:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfba;->aj:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2046
    iget-object v0, p0, Lfba;->ai:Ljava/lang/String;

    return-object v0
.end method

.method public E()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2056
    iget v0, p0, Lfba;->ah:I

    packed-switch v0, :pswitch_data_0

    .line 2063
    const-string v0, "COVER"

    :goto_0
    return-object v0

    .line 2058
    :pswitch_0
    const-string v0, "FULL_BLEED"

    goto :goto_0

    .line 2060
    :pswitch_1
    const-string v0, "COVER"

    goto :goto_0

    .line 2056
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public F()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2068
    iget v2, p0, Lfba;->ao:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    iget-boolean v2, p0, Lfba;->ap:Z

    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public G()I
    .locals 1

    .prologue
    .line 2093
    iget v0, p0, Lfba;->ao:I

    return v0
.end method

.method public H()I
    .locals 1

    .prologue
    .line 2097
    iget v0, p0, Lfba;->as:I

    return v0
.end method

.method public I()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2101
    iget v0, p0, Lfba;->ao:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 2102
    const-string v0, "115239603441691718952"

    .line 2104
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfba;->m:Ljava/lang/String;

    goto :goto_0
.end method

.method public J()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 2109
    iget-object v0, p0, Lfba;->aj:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2110
    iget-object v0, p0, Lfba;->aj:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2112
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public K()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2116
    iget-object v0, p0, Lfba;->aj:Ljava/lang/String;

    return-object v0
.end method

.method public L()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2120
    iget-object v0, p0, Lfba;->ak:Ljava/lang/String;

    return-object v0
.end method

.method public M()Z
    .locals 1

    .prologue
    .line 2124
    iget-object v0, p0, Lfba;->al:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2128
    iget-object v0, p0, Lfba;->al:Ljava/lang/String;

    return-object v0
.end method

.method public O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2132
    iget-object v0, p0, Lfba;->am:Ljava/lang/String;

    return-object v0
.end method

.method public P()Lnjp;
    .locals 1

    .prologue
    .line 2136
    iget-object v0, p0, Lfba;->at:Lnjp;

    return-object v0
.end method

.method public Q()Z
    .locals 1

    .prologue
    .line 2140
    iget-boolean v0, p0, Lfba;->av:Z

    return v0
.end method

.method public R()[B
    .locals 1

    .prologue
    .line 2173
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->f:Lnio;

    if-nez v0, :cond_1

    .line 2174
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2176
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->f:Lnio;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public S()[B
    .locals 1

    .prologue
    .line 2180
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->g:Lnim;

    if-nez v0, :cond_1

    .line 2181
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2183
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->g:Lnim;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public T()[B
    .locals 1

    .prologue
    .line 2187
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->h:Lniz;

    if-nez v0, :cond_1

    .line 2188
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2190
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->h:Lniz;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public U()[B
    .locals 1

    .prologue
    .line 2194
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->c:Lnif;

    if-nez v0, :cond_1

    .line 2195
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2197
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->c:Lnif;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public V()[B
    .locals 1

    .prologue
    .line 2201
    iget-object v0, p0, Lfba;->S:Lmcf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->S:Lmcf;

    iget-object v0, v0, Lmcf;->a:Lnlm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->S:Lmcf;

    iget-object v0, v0, Lmcf;->a:Lnlm;

    iget-object v0, v0, Lnlm;->b:Lnkf;

    if-nez v0, :cond_1

    .line 2203
    :cond_0
    const/4 v0, 0x0

    .line 2205
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->S:Lmcf;

    iget-object v0, v0, Lmcf;->a:Lnlm;

    iget-object v0, v0, Lnlm;->b:Lnkf;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public W()[B
    .locals 1

    .prologue
    .line 2209
    iget-object v0, p0, Lfba;->X:Lnso;

    if-eqz v0, :cond_0

    .line 2210
    iget-object v0, p0, Lfba;->X:Lnso;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 2212
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public X()[B
    .locals 1

    .prologue
    .line 2216
    iget-object v0, p0, Lfba;->W:Lnto;

    if-eqz v0, :cond_0

    .line 2217
    iget-object v0, p0, Lfba;->W:Lnto;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 2219
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Y()[B
    .locals 1

    .prologue
    .line 2223
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->h:Lnjs;

    if-nez v0, :cond_1

    .line 2224
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2226
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->h:Lnjs;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public Z()[B
    .locals 1

    .prologue
    .line 2234
    iget-object v0, p0, Lfba;->Q:Lnjt;

    invoke-static {v0}, Ldsm;->a(Lnjt;)[B

    move-result-object v0

    return-object v0
.end method

.method protected a(ILandroid/database/Cursor;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1491
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lfba;->ax()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1492
    new-instance v1, Lldx;

    const/4 v0, -0x2

    invoke-direct {v1, v0}, Lldx;-><init>(I)V

    .line 1494
    iget-object v0, p0, Lfba;->j:Llcr;

    iget v0, v0, Llcr;->a:I

    iput v0, v1, Lldx;->a:I

    .line 1495
    iget-object v0, p0, Lfba;->j:Llcr;

    iget v0, v0, Llcr;->f:I

    neg-int v0, v0

    iget-object v2, p0, Lfba;->j:Llcr;

    iget v2, v2, Llcr;->d:I

    neg-int v2, v2

    iget-object v3, p0, Lfba;->j:Llcr;

    iget v3, v3, Llcr;->f:I

    neg-int v3, v3

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lldx;->setMargins(IIII)V

    .line 1498
    iget-object v0, p0, Lfba;->a:Levi;

    invoke-virtual {v0, p3, p4, p5}, Levi;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1499
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1502
    :goto_0
    return-object v0

    :cond_0
    invoke-super/range {p0 .. p5}, Lfdj;->a(ILandroid/database/Cursor;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;
    .locals 6

    .prologue
    .line 715
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040144

    const/4 v2, 0x0

    .line 716
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OneProfileHeader;

    .line 720
    new-instance v1, Lldx;

    const/4 v2, -0x2

    invoke-direct {v1, v2}, Lldx;-><init>(I)V

    .line 722
    iget-object v2, p0, Lfba;->j:Llcr;

    iget v2, v2, Llcr;->a:I

    iput v2, v1, Lldx;->a:I

    .line 723
    iget-object v2, p0, Lfba;->j:Llcr;

    iget v2, v2, Llcr;->f:I

    neg-int v2, v2

    iget-object v3, p0, Lfba;->j:Llcr;

    iget v3, v3, Llcr;->d:I

    neg-int v3, v3

    iget-object v4, p0, Lfba;->j:Llcr;

    iget v4, v4, Llcr;->f:I

    neg-int v4, v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Lldx;->setMargins(IIII)V

    .line 725
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 727
    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const v6, 0x7f04013d

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1149
    .line 1150
    new-instance v2, Lldx;

    const/4 v0, -0x2

    invoke-direct {v2, v0}, Lldx;-><init>(I)V

    .line 1152
    iput v5, v2, Lldx;->a:I

    .line 1153
    iget v0, p0, Lfba;->f:I

    packed-switch v0, :pswitch_data_0

    .line 1285
    invoke-super {p0, p1, p2, p3}, Lfdj;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    return-object v1

    .line 1155
    :pswitch_0
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1156
    packed-switch v3, :pswitch_data_1

    .line 1215
    :goto_1
    :pswitch_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1216
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1158
    :pswitch_2
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040140

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1161
    :pswitch_3
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f04013b

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1164
    :pswitch_4
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040141

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1167
    :pswitch_5
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040142

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1170
    :pswitch_6
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040134

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1174
    :pswitch_7
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f04013a

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1178
    :pswitch_8
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f04013c

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1181
    :pswitch_9
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040130

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1184
    :pswitch_a
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040131

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1188
    :pswitch_b
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f04013e

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1192
    :pswitch_c
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040143

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1

    .line 1196
    :pswitch_d
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040135

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_1

    .line 1199
    :pswitch_e
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f04012e

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_1

    .line 1202
    :pswitch_f
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040139

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_1

    .line 1206
    :pswitch_10
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v6, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1207
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;

    .line 1208
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1211
    :pswitch_11
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040137

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_1

    .line 1221
    :pswitch_12
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040145

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1227
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1232
    :pswitch_13
    invoke-interface {p2}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 1233
    iget-object v0, p0, Lfba;->aS:Lgcs;

    invoke-interface {v0}, Lgcs;->d()I

    move-result v0

    .line 1234
    iget-object v1, p0, Lfba;->j:Llcr;

    iget v1, v1, Llcr;->a:I

    iput v1, v2, Lldx;->a:I

    .line 1238
    :goto_2
    iget-object v1, p0, Lfba;->u:Landroid/view/LayoutInflater;

    invoke-virtual {v1, v0, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1239
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1236
    :cond_0
    iget-object v0, p0, Lfba;->aS:Lgcs;

    invoke-interface {v0}, Lgcs;->c()I

    move-result v0

    goto :goto_2

    .line 1243
    :pswitch_14
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1244
    packed-switch v0, :pswitch_data_2

    .line 1255
    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1256
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1246
    :pswitch_15
    iget-object v1, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v3, 0x7f040147

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1248
    iget-object v3, p0, Lfba;->j:Llcr;

    iget v3, v3, Llcr;->a:I

    iput v3, v2, Lldx;->a:I

    goto :goto_3

    .line 1251
    :pswitch_16
    iget-object v1, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v3, 0x7f040148

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_3

    .line 1261
    :pswitch_17
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1262
    packed-switch v3, :pswitch_data_3

    goto/16 :goto_0

    .line 1264
    :pswitch_18
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f040146

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1278
    :goto_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1279
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v1, v0

    .line 1281
    goto/16 :goto_0

    .line 1268
    :pswitch_19
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v6, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_4

    .line 1271
    :pswitch_1a
    iget-object v0, p0, Lfba;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f0400f8

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1272
    iget-object v1, p0, Lfba;->j:Llcr;

    iget v1, v1, Llcr;->a:I

    iput v1, v2, Lldx;->a:I

    goto :goto_4

    .line 1153
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_12
        :pswitch_14
        :pswitch_17
        :pswitch_13
    .end packed-switch

    .line 1156
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_11
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_10
        :pswitch_e
        :pswitch_f
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1244
    :pswitch_data_2
    .packed-switch 0x14
        :pswitch_15
        :pswitch_16
    .end packed-switch

    .line 1262
    :pswitch_data_3
    .packed-switch 0x11
        :pswitch_18
        :pswitch_19
        :pswitch_1a
    .end packed-switch
.end method

.method varargs a(I[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1642
    iget-object v0, p0, Lfba;->k:Landroid/content/Context;

    invoke-virtual {v0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;I)V
    .locals 1

    .prologue
    .line 1614
    iget v0, p0, Lfba;->f:I

    packed-switch v0, :pswitch_data_0

    .line 1632
    invoke-super {p0, p1, p2}, Lfdj;->a(Landroid/database/Cursor;I)V

    .line 1635
    :pswitch_0
    return-void

    .line 1614
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/view/View;III)V
    .locals 0

    .prologue
    .line 2355
    invoke-super {p0, p1, p2, p3, p4}, Lfdj;->a(Landroid/view/View;III)V

    .line 2356
    invoke-virtual {p0}, Lfba;->b()V

    .line 2357
    return-void
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 10

    .prologue
    const v6, 0x7f0a02e2

    const/4 v9, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 732
    invoke-virtual {p0}, Lfba;->as()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 734
    check-cast p1, Lcom/google/android/apps/plus/views/OneProfileHeader;

    iput-object p1, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    .line 736
    iget-object v0, p0, Lfba;->K:Lfys;

    if-eqz v0, :cond_0

    .line 737
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-object v4, p0, Lfba;->K:Lfys;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Lfys;)V

    .line 740
    :cond_0
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-object v4, p0, Lfba;->v:Lfyz;

    iget-object v5, p0, Lfba;->w:Llhl;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Lfyz;Llhl;)V

    .line 751
    iget-boolean v0, p0, Lfba;->ab:Z

    if-eqz v0, :cond_9

    .line 752
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {p0, v6}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v2, v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;ILhmn;)V

    .line 788
    :cond_1
    :goto_0
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget v4, p0, Lfba;->f:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(I)V

    .line 789
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-object v4, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Lcom/google/android/libraries/social/ui/tabbar/TabBar;)V

    .line 793
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_25

    .line 794
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-object v4, p0, Lfba;->an:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;)V

    .line 795
    invoke-virtual {p0}, Lfba;->j()V

    .line 798
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->f:Ljava/lang/String;

    .line 799
    iget-object v4, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->c(Ljava/lang/String;)V

    .line 800
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 801
    iget-object v4, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-boolean v0, p0, Lfba;->ab:Z

    if-nez v0, :cond_11

    move v0, v2

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->b(Z)V

    .line 803
    :cond_2
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-boolean v4, p0, Lfba;->az:Z

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->e(Z)V

    .line 806
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->a:Lnjb;

    if-eqz v0, :cond_34

    .line 807
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->a:Lnjb;

    iget-object v0, v0, Lnjb;->d:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    .line 812
    :goto_2
    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->c:Lnia;

    if-eqz v4, :cond_33

    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->c:Lnia;

    iget-object v4, v4, Lnia;->e:Lnju;

    if-eqz v4, :cond_33

    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->c:Lnia;

    iget-object v4, v4, Lnia;->e:Lnju;

    iget-object v4, v4, Lnju;->b:Ljava/lang/Boolean;

    .line 814
    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    if-nez v4, :cond_33

    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->c:Lnia;

    iget-object v4, v4, Lnia;->e:Lnju;

    iget-object v4, v4, Lnju;->a:Lohv;

    if-eqz v4, :cond_33

    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->c:Lnia;

    iget-object v4, v4, Lnia;->e:Lnju;

    iget-object v4, v4, Lnju;->a:Lohv;

    iget-object v4, v4, Lohv;->d:[Loij;

    if-eqz v4, :cond_33

    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->c:Lnia;

    iget-object v4, v4, Lnia;->e:Lnju;

    iget-object v4, v4, Lnju;->a:Lohv;

    iget-object v4, v4, Lohv;->c:Lohq;

    if-eqz v4, :cond_33

    .line 819
    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->c:Lnia;

    iget-object v4, v4, Lnia;->e:Lnju;

    iget-object v4, v4, Lnju;->a:Lohv;

    iget-object v4, v4, Lohv;->c:Lohq;

    iget-object v4, v4, Lohq;->h:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v5

    .line 821
    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->c:Lnia;

    iget-object v4, v4, Lnia;->e:Lnju;

    iget-object v4, v4, Lnju;->a:Lohv;

    iget-object v4, v4, Lohv;->d:[Loij;

    array-length v4, v4

    if-lez v4, :cond_12

    move v4, v2

    .line 825
    :goto_3
    iget-object v6, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-object v7, p0, Lfba;->aw:Ljava/lang/String;

    invoke-virtual {v6, v7, v0, v5, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;ZZZ)V

    .line 826
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-boolean v4, p0, Lfba;->aQ:Z

    iget-object v5, p0, Lfba;->n:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(ZLjava/lang/String;)V

    .line 828
    iget-boolean v0, p0, Lfba;->aa:Z

    if-eqz v0, :cond_3

    .line 829
    iget-object v4, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->k:Lptg;

    if-eqz v0, :cond_32

    iget-object v0, v0, Lptg;->a:Lpgg;

    if-eqz v0, :cond_32

    iget-object v0, v0, Lpgg;->a:Ljava/lang/Integer;

    :goto_4
    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/Integer;)V

    .line 837
    :cond_3
    iget-boolean v0, p0, Lfba;->Z:Z

    if-eqz v0, :cond_14

    .line 838
    iget-object v0, p0, Lfba;->Q:Lnjt;

    invoke-static {v0}, Ljnu;->b(Lnjt;)Ljava/lang/String;

    move-result-object v5

    .line 840
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 841
    iget-object v0, p0, Lfba;->Q:Lnjt;

    invoke-static {v0}, Ljnu;->a(Lnjt;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_4
    move-object v0, v1

    .line 842
    :goto_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 843
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 845
    :cond_5
    iget-object v0, p0, Lfba;->Q:Lnjt;

    invoke-static {v0}, Ljnu;->a(Lnjt;)I

    move-result v6

    packed-switch v6, :pswitch_data_1

    move-object v0, v1

    .line 846
    :goto_6
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 847
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_6

    .line 848
    const-string v6, " \u2014 "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 850
    :cond_6
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 852
    :cond_7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_31

    .line 853
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_7
    move-object v4, v1

    move-object v6, v5

    move-object v5, v0

    move v0, v3

    .line 969
    :goto_8
    iget-object v7, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-boolean v8, p0, Lfba;->Y:Z

    if-nez v8, :cond_24

    :goto_9
    iget-boolean v3, p0, Lfba;->Y:Z

    invoke-virtual {v7, v2, v3}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(ZZ)V

    .line 971
    iget-object v2, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v2, v6}, Lcom/google/android/apps/plus/views/OneProfileHeader;->d(Ljava/lang/String;)V

    .line 972
    iget-object v2, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/OneProfileHeader;->e(Ljava/lang/String;)V

    .line 973
    iget-object v2, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/OneProfileHeader;->f(Ljava/lang/String;)V

    .line 974
    iget-object v2, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->c(Z)V

    .line 976
    invoke-virtual {p0}, Lfba;->r()V

    .line 979
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->b:Lnis;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->b:Lnis;

    iget-object v0, v0, Lnis;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_26

    .line 982
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->b:Lnis;

    iget-object v0, v0, Lnis;->a:Ljava/lang/Integer;

    .line 986
    :goto_a
    iget-object v2, p0, Lfba;->R:Lnkm;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lfba;->R:Lnkm;

    iget-object v2, v2, Lnkm;->a:Lnkn;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lfba;->R:Lnkm;

    iget-object v2, v2, Lnkm;->a:Lnkn;

    iget-object v2, v2, Lnkn;->a:Ljava/lang/Long;

    if-eqz v2, :cond_8

    .line 989
    iget-object v1, p0, Lfba;->R:Lnkm;

    iget-object v1, v1, Lnkm;->a:Lnkn;

    iget-object v1, v1, Lnkn;->a:Ljava/lang/Long;

    .line 992
    :cond_8
    iget-object v2, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/Integer;Ljava/lang/Long;)V

    .line 994
    sget-object v0, Lcom/google/android/apps/plus/phone/OneProfileActivity;->e:Lloy;

    .line 1001
    :goto_b
    invoke-virtual {p0}, Lfba;->h()V

    .line 1002
    return-void

    .line 757
    :cond_9
    iget-object v4, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {p0, v6}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    iget-boolean v0, p0, Lfba;->N:Z

    if-eqz v0, :cond_10

    sget-object v0, Long;->a:Lhmn;

    :goto_c
    invoke-virtual {v4, v5, v2, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;ILhmn;)V

    .line 760
    iget-boolean v0, p0, Lfba;->M:Z

    if-nez v0, :cond_a

    invoke-virtual {p0}, Lfba;->m()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 761
    :cond_a
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    const v4, 0x7f0a02e1

    invoke-virtual {p0, v4}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Long;->d:Lhmn;

    invoke-virtual {v0, v4, v3, v5}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;ILhmn;)V

    .line 765
    :cond_b
    iget-object v0, p0, Lfba;->aS:Lgcs;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lfba;->aS:Lgcs;

    iget v4, p0, Lfba;->g:I

    .line 766
    invoke-interface {v0}, Lgcs;->g()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 767
    iget-object v0, p0, Lfba;->aS:Lgcs;

    invoke-interface {v0}, Lgcs;->b()Ljava/lang/String;

    move-result-object v0

    .line 768
    iget-object v4, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x5

    sget-object v6, Long;->b:Lhmn;

    invoke-virtual {v4, v0, v5, v6}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;ILhmn;)V

    .line 771
    :cond_c
    iget-boolean v0, p0, Lfba;->M:Z

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lfba;->n()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 772
    :cond_d
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    const v4, 0x7f0a02e3

    invoke-virtual {p0, v4}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Long;->c:Lhmn;

    invoke-virtual {v0, v4, v9, v5}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;ILhmn;)V

    .line 776
    :cond_e
    invoke-virtual {p0}, Lfba;->p()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lfba;->aq:Z

    if-eqz v0, :cond_f

    .line 777
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    const v4, 0x7f0a02e5

    invoke-virtual {p0, v4}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    sget-object v6, Long;->m:Lhmn;

    invoke-virtual {v0, v4, v5, v6}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;ILhmn;)V

    .line 781
    :cond_f
    invoke-virtual {p0}, Lfba;->o()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lfba;->ar:Z

    if-eqz v0, :cond_1

    .line 782
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    const v4, 0x7f0a02e4

    invoke-virtual {p0, v4}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    sget-object v6, Long;->l:Lhmn;

    invoke-virtual {v0, v4, v5, v6}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;ILhmn;)V

    goto/16 :goto_0

    :cond_10
    move-object v0, v1

    .line 757
    goto/16 :goto_c

    :cond_11
    move v0, v3

    .line 801
    goto/16 :goto_1

    :cond_12
    move v4, v3

    .line 821
    goto/16 :goto_3

    .line 841
    :pswitch_0
    iget-object v6, v0, Lnjt;->f:Lnjg;

    iget-object v6, v6, Lnjg;->b:Lnix;

    iget-object v6, v6, Lnix;->d:Lpmm;

    iget-object v6, v6, Lpmm;->g:Lphv;

    if-eqz v6, :cond_4

    iget-object v6, v0, Lnjt;->f:Lnjg;

    iget-object v6, v6, Lnjg;->b:Lnix;

    iget-object v6, v6, Lnix;->d:Lpmm;

    iget-object v6, v6, Lpmm;->g:Lphv;

    iget-object v6, v6, Lphv;->a:[Lphw;

    if-eqz v6, :cond_4

    iget-object v6, v0, Lnjt;->f:Lnjg;

    iget-object v6, v6, Lnjg;->b:Lnix;

    iget-object v6, v6, Lnix;->d:Lpmm;

    iget-object v6, v6, Lpmm;->g:Lphv;

    iget-object v6, v6, Lphv;->a:[Lphw;

    array-length v6, v6

    if-lez v6, :cond_4

    iget-object v0, v0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->g:Lphv;

    iget-object v0, v0, Lphv;->a:[Lphw;

    aget-object v0, v0, v3

    iget-object v0, v0, Lphw;->b:Ljava/lang/String;

    goto/16 :goto_5

    :pswitch_1
    iget-object v6, v0, Lnjt;->f:Lnjg;

    iget-object v6, v6, Lnjg;->b:Lnix;

    iget-object v6, v6, Lnix;->a:Lniv;

    if-eqz v6, :cond_4

    iget-object v6, v0, Lnjt;->f:Lnjg;

    iget-object v6, v6, Lnjg;->b:Lnix;

    iget-object v6, v6, Lnix;->a:Lniv;

    iget-object v6, v6, Lniv;->a:[Lniw;

    if-eqz v6, :cond_4

    iget-object v6, v0, Lnjt;->f:Lnjg;

    iget-object v6, v6, Lnjg;->b:Lnix;

    iget-object v6, v6, Lnix;->a:Lniv;

    iget-object v6, v6, Lniv;->a:[Lniw;

    array-length v6, v6

    if-lez v6, :cond_4

    iget-object v0, v0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->a:Lniv;

    iget-object v0, v0, Lniv;->a:[Lniw;

    aget-object v0, v0, v3

    iget-object v0, v0, Lniw;->b:Ljava/lang/String;

    goto/16 :goto_5

    .line 845
    :pswitch_2
    iget-object v0, v0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    iget-object v0, v0, Lpmm;->e:Lpgq;

    if-eqz v0, :cond_13

    iget-object v0, v0, Lpgq;->b:Lpgr;

    iget-object v0, v0, Lpgr;->a:Ljava/lang/String;

    goto/16 :goto_6

    :cond_13
    move-object v0, v1

    goto/16 :goto_6

    .line 856
    :cond_14
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_30

    .line 857
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->f:Lnio;

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->f:Lnio;

    iget-object v0, v0, Lnio;->b:[Lnin;

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->f:Lnio;

    iget-object v0, v0, Lnio;->b:[Lnin;

    array-length v0, v0

    if-eqz v0, :cond_2f

    .line 864
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->f:Lnio;

    iget-object v0, v0, Lnio;->b:[Lnin;

    aget-object v0, v0, v3

    .line 865
    if-eqz v0, :cond_2e

    .line 866
    iget-object v5, v0, Lnin;->c:Ljava/lang/String;

    .line 867
    iget-object v4, v0, Lnin;->b:Ljava/lang/String;

    .line 868
    iget-object v6, v0, Lnin;->d:Lnih;

    if-eqz v6, :cond_2d

    .line 869
    iget-object v0, v0, Lnin;->d:Lnih;

    iget-object v0, v0, Lnih;->c:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    .line 873
    :goto_d
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_16

    move v6, v2

    .line 874
    :goto_e
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_17

    move v7, v2

    .line 876
    :goto_f
    if-eqz v6, :cond_18

    if-eqz v7, :cond_18

    if-eqz v0, :cond_18

    .line 877
    const v0, 0x7f0a0358

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v5, v6, v3

    aput-object v4, v6, v2

    invoke-virtual {v8, v0, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 894
    :cond_15
    :goto_10
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->g:Lnim;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->g:Lnim;

    iget-object v0, v0, Lnim;->b:[Lnil;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->g:Lnim;

    iget-object v0, v0, Lnim;->b:[Lnil;

    array-length v0, v0

    if-eqz v0, :cond_2c

    .line 900
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->g:Lnim;

    iget-object v0, v0, Lnim;->b:[Lnil;

    aget-object v0, v0, v3

    .line 901
    if-eqz v0, :cond_2b

    .line 902
    iget-object v4, v0, Lnil;->b:Ljava/lang/String;

    .line 903
    iget-object v6, v0, Lnil;->d:Lnih;

    if-eqz v6, :cond_2a

    .line 904
    iget-object v0, v0, Lnil;->d:Lnih;

    iget-object v0, v0, Lnih;->c:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    .line 908
    :goto_11
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1b

    move v6, v2

    .line 909
    :goto_12
    if-eqz v6, :cond_1d

    .line 910
    if-eqz v0, :cond_1c

    .line 911
    const v0, 0x7f0a035b

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v4, v6, v3

    invoke-virtual {v8, v0, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 925
    :goto_13
    iget-boolean v4, p0, Lfba;->Y:Z

    if-nez v4, :cond_29

    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->e:Lnkd;

    iget-object v4, v4, Lnkd;->m:Lnik;

    if-eqz v4, :cond_29

    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->e:Lnkd;

    iget-object v4, v4, Lnkd;->m:Lnik;

    iget-object v4, v4, Lnik;->a:[Lnij;

    if-eqz v4, :cond_29

    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->e:Lnkd;

    iget-object v4, v4, Lnkd;->m:Lnik;

    iget-object v4, v4, Lnik;->a:[Lnij;

    array-length v4, v4

    if-eqz v4, :cond_29

    .line 930
    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->e:Lnkd;

    iget-object v4, v4, Lnkd;->m:Lnik;

    iget-object v4, v4, Lnik;->a:[Lnij;

    aget-object v4, v4, v3

    iget-object v4, v4, Lnij;->g:Ljava/lang/String;

    move v6, v2

    .line 934
    :goto_14
    if-eqz v6, :cond_1f

    .line 938
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1e

    .line 939
    const v6, 0x7f0a035f

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v4, v7, v3

    invoke-virtual {v8, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_15
    move-object v6, v5

    move-object v5, v0

    move v0, v2

    .line 944
    goto/16 :goto_8

    :cond_16
    move v6, v3

    .line 873
    goto/16 :goto_e

    :cond_17
    move v7, v3

    .line 874
    goto/16 :goto_f

    .line 879
    :cond_18
    if-eqz v7, :cond_1a

    .line 880
    if-eqz v0, :cond_19

    .line 881
    const v0, 0x7f0a0359

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v4, v5, v3

    invoke-virtual {v8, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_10

    .line 884
    :cond_19
    const v0, 0x7f0a035a

    new-array v5, v2, [Ljava/lang/Object;

    aput-object v4, v5, v3

    invoke-virtual {v8, v0, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_10

    .line 887
    :cond_1a
    if-nez v6, :cond_15

    move-object v5, v1

    .line 890
    goto/16 :goto_10

    :cond_1b
    move v6, v3

    .line 908
    goto :goto_12

    .line 914
    :cond_1c
    const v0, 0x7f0a035c

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v4, v6, v3

    invoke-virtual {v8, v0, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_13

    :cond_1d
    move-object v0, v1

    .line 917
    goto :goto_13

    .line 942
    :cond_1e
    const v4, 0x7f0a0360

    invoke-virtual {v8, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_15

    .line 946
    :cond_1f
    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->e:Lnkd;

    iget-object v4, v4, Lnkd;->h:Lniz;

    if-eqz v4, :cond_28

    .line 947
    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->e:Lnkd;

    iget-object v4, v4, Lnkd;->h:Lniz;

    iget-object v9, v4, Lniz;->b:Ljava/lang/String;

    .line 950
    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->e:Lnkd;

    iget-object v4, v4, Lnkd;->h:Lniz;

    iget-object v4, v4, Lniz;->c:[Ljava/lang/String;

    if-eqz v4, :cond_27

    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->e:Lnkd;

    iget-object v4, v4, Lnkd;->h:Lniz;

    iget-object v4, v4, Lniz;->c:[Ljava/lang/String;

    array-length v4, v4

    if-eqz v4, :cond_27

    .line 952
    iget-object v4, p0, Lfba;->Q:Lnjt;

    iget-object v4, v4, Lnjt;->e:Lnkd;

    iget-object v4, v4, Lnkd;->h:Lniz;

    iget-object v4, v4, Lniz;->c:[Ljava/lang/String;

    aget-object v4, v4, v3

    .line 955
    :goto_16
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_20

    move v6, v2

    .line 956
    :goto_17
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_21

    move v7, v2

    .line 957
    :goto_18
    if-eqz v6, :cond_22

    .line 958
    const v4, 0x7f0a035d

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v9, v6, v3

    invoke-virtual {v8, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v5, v0

    move v0, v3

    goto/16 :goto_8

    :cond_20
    move v6, v3

    .line 955
    goto :goto_17

    :cond_21
    move v7, v3

    .line 956
    goto :goto_18

    .line 960
    :cond_22
    if-eqz v7, :cond_23

    .line 961
    const v6, 0x7f0a035e

    new-array v7, v2, [Ljava/lang/Object;

    aput-object v4, v7, v3

    invoke-virtual {v8, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v6, v5

    move-object v5, v0

    move v0, v3

    goto/16 :goto_8

    :cond_23
    move-object v4, v1

    move-object v6, v5

    move-object v5, v0

    move v0, v3

    .line 964
    goto/16 :goto_8

    :cond_24
    move v2, v3

    .line 969
    goto/16 :goto_9

    .line 998
    :cond_25
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Z)V

    goto/16 :goto_b

    :cond_26
    move-object v0, v1

    goto/16 :goto_a

    :cond_27
    move-object v4, v1

    goto :goto_16

    :cond_28
    move-object v4, v1

    move-object v6, v5

    move-object v5, v0

    move v0, v3

    goto/16 :goto_8

    :cond_29
    move-object v4, v1

    move v6, v3

    goto/16 :goto_14

    :cond_2a
    move v0, v3

    goto/16 :goto_11

    :cond_2b
    move v0, v3

    move-object v4, v1

    goto/16 :goto_11

    :cond_2c
    move-object v0, v1

    goto/16 :goto_13

    :cond_2d
    move v0, v3

    goto/16 :goto_d

    :cond_2e
    move v0, v3

    move-object v4, v1

    move-object v5, v1

    goto/16 :goto_d

    :cond_2f
    move-object v5, v1

    goto/16 :goto_10

    :cond_30
    move v0, v3

    move-object v4, v1

    move-object v5, v1

    move-object v6, v1

    goto/16 :goto_8

    :cond_31
    move-object v0, v1

    goto/16 :goto_7

    :cond_32
    move-object v0, v1

    goto/16 :goto_4

    :cond_33
    move v4, v3

    move v5, v3

    goto/16 :goto_3

    :cond_34
    move v0, v3

    goto/16 :goto_2

    .line 841
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 845
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1292
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 1293
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lfba;->o(I)I

    move-result v1

    add-int v6, v0, v1

    .line 1294
    iget v1, p0, Lfba;->f:I

    packed-switch v1, :pswitch_data_0

    .line 1483
    invoke-super {p0, p1, p2, p3}, Lfdj;->a(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V

    .line 1486
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p1

    .line 1296
    check-cast v0, Ljnl;

    .line 1297
    iget-boolean v3, p0, Lfba;->M:Z

    iget-boolean v7, p0, Lfba;->Y:Z

    iget-boolean v8, p0, Lfba;->Z:Z

    iget-boolean v1, p0, Lfba;->M:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfba;->x:Ljnt;

    :goto_1
    invoke-virtual {v0, v3, v7, v8, v1}, Ljnl;->a(ZZZLjnt;)V

    .line 1301
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1302
    sparse-switch v7, :sswitch_data_0

    .line 1320
    :goto_2
    iget-object v1, p0, Lfba;->Q:Lnjt;

    invoke-virtual {v0, v1}, Ljnl;->a(Lnjt;)V

    .line 1323
    sparse-switch v7, :sswitch_data_1

    .line 1379
    :cond_0
    :goto_3
    invoke-virtual {p0, p1, v6}, Lfba;->b(Landroid/view/View;I)V

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 1297
    goto :goto_1

    :sswitch_0
    move-object v1, p1

    .line 1304
    check-cast v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;

    .line 1305
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Ljava/lang/String;)V

    .line 1306
    iget-object v3, p0, Lfba;->H:Ljnk;

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Ljnk;)V

    goto :goto_2

    :sswitch_1
    move-object v1, p1

    .line 1309
    check-cast v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;

    .line 1311
    iget-boolean v3, p0, Lfba;->ap:Z

    if-nez v3, :cond_2

    move v3, v4

    :goto_4
    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a(Z)V

    goto :goto_2

    :cond_2
    move v3, v5

    goto :goto_4

    :sswitch_2
    move-object v1, p1

    .line 1314
    check-cast v1, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;

    .line 1316
    iget-object v3, p0, Lfba;->A:Ljms;

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->a(Ljms;)V

    goto :goto_2

    :sswitch_3
    move-object v0, p1

    .line 1325
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;

    .line 1326
    iget-object v1, p0, Lfba;->S:Lmcf;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfba;->S:Lmcf;

    iget-object v1, v1, Lmcf;->a:Lnlm;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfba;->S:Lmcf;

    iget-object v1, v1, Lmcf;->a:Lnlm;

    iget-object v2, v1, Lnlm;->a:Lofz;

    :cond_3
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Lofz;)V

    .line 1328
    iget-object v1, p0, Lfba;->y:Ljni;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->a(Ljni;)V

    goto :goto_3

    :sswitch_4
    move-object v0, p1

    .line 1331
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;

    .line 1333
    iget-object v1, p0, Lfba;->z:Ljmv;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->a(Ljmv;)V

    goto :goto_3

    :sswitch_5
    move-object v0, p1

    .line 1336
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;

    .line 1338
    iget-object v1, p0, Lfba;->G:Ljmy;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a(Ljmy;)V

    .line 1339
    iget-boolean v1, p0, Lfba;->ap:Z

    if-nez v1, :cond_4

    :goto_5
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->a(Z)V

    goto :goto_3

    :cond_4
    move v4, v5

    goto :goto_5

    :sswitch_6
    move-object v0, p1

    .line 1342
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;

    .line 1344
    iget-object v1, p0, Lfba;->D:Ljnj;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->a(Ljnj;)V

    goto :goto_3

    :sswitch_7
    move-object v0, p1

    .line 1347
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;

    .line 1349
    iget-object v1, p0, Lfba;->E:Ljns;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->a(Ljns;)V

    goto :goto_3

    :sswitch_8
    move-object v0, p1

    .line 1352
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;

    .line 1353
    iget-object v1, p0, Lfba;->B:Ljmx;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->a(Ljmx;)V

    goto :goto_3

    :sswitch_9
    move-object v0, p1

    .line 1356
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;

    .line 1357
    iget-object v1, p0, Lfba;->F:Ljmi;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->a(Ljmi;)V

    goto/16 :goto_3

    :sswitch_a
    move-object v0, p1

    .line 1360
    check-cast v0, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;

    .line 1361
    iget-object v1, p0, Lfba;->V:Lnts;

    iget-object v2, p0, Lfba;->W:Lnto;

    iget-object v3, p0, Lfba;->X:Lnso;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a(Lnts;Lnto;Lnso;)V

    goto/16 :goto_3

    .line 1366
    :sswitch_b
    invoke-virtual {p0}, Lfba;->as()Landroid/content/Context;

    move-result-object v0

    const-class v1, Ljnv;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljnv;

    .line 1368
    if-eqz v0, :cond_5

    .line 1370
    invoke-virtual {p0}, Lfba;->as()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Lfba;->g:I

    .line 1369
    invoke-interface {v0, v1, v3}, Ljnv;->a(Landroid/content/Context;I)Z

    move-result v1

    .line 1371
    if-eqz v1, :cond_5

    .line 1372
    invoke-virtual {p0}, Lfba;->as()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Ljnv;->a(Landroid/content/Context;)Ljnw;

    move-result-object v2

    .line 1375
    :cond_5
    if-eqz v2, :cond_0

    move-object v0, p1

    .line 1376
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;

    .line 1378
    invoke-interface {v2}, Ljnw;->h()Ljava/lang/String;

    move-result-object v1

    .line 1377
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->a(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1388
    :pswitch_1
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-object v0, p1

    .line 1389
    check-cast v0, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;

    .line 1405
    iget-object v1, p0, Lfba;->C:Lgas;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/plus/views/ProfileAlbumTileView;->a(Landroid/database/Cursor;Lgas;)V

    .line 1407
    invoke-virtual {p0, p1, v6}, Lfba;->b(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 1411
    :pswitch_2
    invoke-interface {p2}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    if-ne v0, v4, :cond_6

    .line 1412
    new-instance v0, Lfbd;

    invoke-direct {v0, p0}, Lfbd;-><init>(Lfba;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1424
    :goto_6
    invoke-virtual {p0, p1, v6}, Lfba;->b(Landroid/view/View;I)V

    goto/16 :goto_0

    :cond_6
    move-object v0, p1

    .line 1420
    check-cast v0, Lfyt;

    .line 1421
    iget-object v1, p0, Lfba;->J:Lfyv;

    invoke-virtual {v0, v1}, Lfyt;->a(Lfyv;)V

    .line 1422
    invoke-virtual {v0, p2}, Lfyt;->a(Landroid/database/Cursor;)V

    goto :goto_6

    .line 1428
    :pswitch_3
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1429
    packed-switch v0, :pswitch_data_1

    .line 1442
    :goto_7
    invoke-virtual {p0, p1, v6}, Lfba;->b(Landroid/view/View;I)V

    goto/16 :goto_0

    :pswitch_4
    move-object v0, p1

    .line 1431
    check-cast v0, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;

    .line 1432
    iget-object v1, p0, Lfba;->ax:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lfba;->ax:Ljava/lang/String;

    .line 1433
    :goto_8
    iget-object v2, p0, Lfba;->T:Lmdp;

    iget-object v2, v2, Lmdp;->a:Lnlr;

    iget-object v2, v2, Lnlr;->b:Lnfq;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->a(Lnfq;Ljava/lang/String;)V

    .line 1434
    iget-object v1, p0, Lfba;->I:Lfzc;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneProfileVideosChannelView;->a(Lfzc;)V

    goto :goto_7

    .line 1432
    :cond_7
    iget-object v1, p0, Lfba;->aw:Ljava/lang/String;

    goto :goto_8

    :pswitch_5
    move-object v0, p1

    .line 1437
    check-cast v0, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;

    .line 1438
    iget-object v1, p0, Lfba;->T:Lmdp;

    iget-object v1, v1, Lmdp;->a:Lnlr;

    iget-object v1, v1, Lnlr;->b:Lnfq;

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->a(Lnfq;I)V

    .line 1439
    iget-object v1, p0, Lfba;->I:Lfzc;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneProfileVideosVideoView;->a(Lfzc;)V

    goto :goto_7

    .line 1446
    :pswitch_6
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 1447
    packed-switch v1, :pswitch_data_2

    .line 1472
    :goto_9
    invoke-virtual {p0, p1, v6}, Lfba;->b(Landroid/view/View;I)V

    goto/16 :goto_0

    :pswitch_7
    move-object v0, p1

    .line 1449
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;

    .line 1450
    iget-object v1, p0, Lfba;->U:Lnlp;

    iget-object v1, v1, Lnlp;->a:Lpvl;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileReviewsSummaryView;->a(Lpvl;)V

    goto :goto_9

    .line 1453
    :pswitch_8
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 1454
    sub-int v0, v1, v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_8

    .line 1455
    iget-object v0, p0, Lfba;->aH:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lfba;->e:Z

    if-nez v0, :cond_8

    .line 1456
    iput-boolean v4, p0, Lfba;->e:Z

    .line 1457
    new-instance v0, Lfbe;

    iget-object v1, p0, Lfba;->k:Landroid/content/Context;

    iget-object v1, p0, Lfba;->L:Ljava/lang/String;

    .line 1459
    invoke-static {v1}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lfbe;-><init>(Lfba;Ljava/lang/String;)V

    .line 1460
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_9

    .line 1461
    new-array v1, v4, [Ljava/lang/String;

    iget-object v2, p0, Lfba;->aH:Ljava/lang/String;

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_8
    :goto_a
    move-object v0, p1

    .line 1468
    check-cast v0, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;

    .line 1469
    iget-object v1, p0, Lfba;->U:Lnlp;

    iget-object v1, v1, Lnlp;->a:Lpvl;

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lfba;->j:Llcr;

    .line 1470
    invoke-virtual {v3, v4}, Llcr;->a(I)I

    move-result v3

    .line 1469
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Lpvl;II)V

    .line 1471
    iget-object v1, p0, Lfba;->H:Ljnk;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->a(Ljnk;)V

    goto :goto_9

    .line 1463
    :cond_9
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v4, [Ljava/lang/String;

    iget-object v3, p0, Lfba;->aH:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_a

    .line 1294
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_2
    .end packed-switch

    .line 1302
    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_2
        0xc -> :sswitch_0
        0xe -> :sswitch_1
    .end sparse-switch

    .line 1323
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_3
        0x4 -> :sswitch_4
        0x6 -> :sswitch_b
        0x8 -> :sswitch_8
        0x9 -> :sswitch_6
        0xa -> :sswitch_7
        0xd -> :sswitch_9
        0xe -> :sswitch_5
        0x16 -> :sswitch_a
    .end sparse-switch

    .line 1429
    :pswitch_data_1
    .packed-switch 0x14
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1447
    :pswitch_data_2
    .packed-switch 0x11
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public a(Lcom/google/android/libraries/social/ui/tabbar/TabBar;)V
    .locals 0

    .prologue
    .line 472
    iput-object p1, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    .line 473
    return-void
.end method

.method public a(Ldsx;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const v6, 0x7f04021c

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1646
    if-nez p1, :cond_1

    .line 1855
    :cond_0
    :goto_0
    return-void

    .line 1650
    :cond_1
    iput-object p1, p0, Lfba;->P:Ldsx;

    .line 1651
    iget-object v0, p1, Ldsx;->h:Lnjt;

    iput-object v0, p0, Lfba;->Q:Lnjt;

    .line 1652
    iget-object v0, p1, Ldsx;->i:Lnkm;

    iput-object v0, p0, Lfba;->R:Lnkm;

    .line 1653
    iget-object v0, p1, Ldsx;->j:Lmcf;

    iput-object v0, p0, Lfba;->S:Lmcf;

    .line 1654
    iget-object v0, p1, Ldsx;->k:Lmdp;

    iput-object v0, p0, Lfba;->T:Lmdp;

    .line 1655
    iget-object v0, p1, Ldsx;->l:Lnlp;

    iput-object v0, p0, Lfba;->U:Lnlp;

    .line 1656
    iget-object v0, p1, Ldsx;->p:Lnts;

    iput-object v0, p0, Lfba;->V:Lnts;

    .line 1657
    iget-object v0, p1, Ldsx;->n:Lnto;

    iput-object v0, p0, Lfba;->W:Lnto;

    .line 1658
    iget-object v0, p1, Ldsx;->o:Lnso;

    iput-object v0, p0, Lfba;->X:Lnso;

    .line 1662
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-nez v0, :cond_2

    .line 1663
    new-instance v0, Lnjt;

    invoke-direct {v0}, Lnjt;-><init>()V

    iput-object v0, p0, Lfba;->Q:Lnjt;

    .line 1664
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iput v1, v0, Lnjt;->b:I

    .line 1666
    iget-object v0, p0, Lfba;->L:Ljava/lang/String;

    const-string v3, "e:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1667
    iget-object v0, p0, Lfba;->Q:Lnjt;

    new-instance v3, Lnib;

    invoke-direct {v3}, Lnib;-><init>()V

    iput-object v3, v0, Lnjt;->d:Lnib;

    .line 1668
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    new-instance v3, Lnif;

    invoke-direct {v3}, Lnif;-><init>()V

    iput-object v3, v0, Lnib;->c:Lnif;

    .line 1669
    new-instance v0, Lnjy;

    invoke-direct {v0}, Lnjy;-><init>()V

    .line 1670
    iget-object v3, p0, Lfba;->L:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lnjy;->d:Ljava/lang/String;

    .line 1671
    new-instance v3, Lnie;

    invoke-direct {v3}, Lnie;-><init>()V

    iput-object v3, v0, Lnjy;->c:Lnie;

    .line 1672
    iget-object v3, v0, Lnjy;->c:Lnie;

    iput v5, v3, Lnie;->a:I

    .line 1673
    iget-object v3, p0, Lfba;->Q:Lnjt;

    iget-object v3, v3, Lnjt;->d:Lnib;

    iget-object v3, v3, Lnib;->c:Lnif;

    new-array v4, v1, [Lnjy;

    aput-object v0, v4, v2

    iput-object v4, v3, Lnif;->b:[Lnjy;

    .line 1687
    :cond_2
    :goto_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget v0, v0, Lnjt;->b:I

    const/high16 v3, -0x80000000

    if-eq v0, v3, :cond_4

    .line 1688
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget v0, v0, Lnjt;->b:I

    if-ne v0, v1, :cond_e

    .line 1689
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->a:Lnjb;

    if-eqz v0, :cond_3

    .line 1690
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->a:Lnjb;

    iget-object v0, v0, Lnjb;->a:Ljava/lang/String;

    iput-object v0, p0, Lfba;->ax:Ljava/lang/String;

    .line 1692
    :cond_3
    iput-boolean v2, p0, Lfba;->Y:Z

    .line 1703
    :cond_4
    :goto_2
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 1704
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->g:Ljava/lang/String;

    iput-object v0, p0, Lfba;->aw:Ljava/lang/String;

    .line 1710
    :goto_3
    iget-boolean v0, p1, Ldsx;->e:Z

    iput-boolean v0, p0, Lfba;->ad:Z

    .line 1711
    iget-object v0, p1, Ldsx;->d:Ljava/lang/String;

    iput-object v0, p0, Lfba;->ac:Ljava/lang/String;

    .line 1712
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->e:Lnju;

    if-eqz v0, :cond_19

    move v0, v1

    .line 1714
    :goto_4
    if-eqz v0, :cond_5

    .line 1715
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->e:Lnju;

    iget-object v0, v0, Lnju;->c:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    iput-boolean v0, p0, Lfba;->ae:Z

    .line 1716
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->e:Lnju;

    iget-object v0, v0, Lnju;->a:Lohv;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->e:Lnju;

    iget-object v0, v0, Lnju;->a:Lohv;

    iget-object v0, v0, Lohv;->c:Lohq;

    if-eqz v0, :cond_5

    .line 1718
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->e:Lnju;

    iget-object v0, v0, Lnju;->a:Lohv;

    iget-object v0, v0, Lohv;->c:Lohq;

    iget-object v0, v0, Lohq;->f:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    iput-boolean v0, p0, Lfba;->aQ:Z

    .line 1723
    :cond_5
    invoke-virtual {p0}, Lfba;->s()V

    .line 1725
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    if-eqz v0, :cond_b

    .line 1726
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v3, v0, Lnib;->i:Lnjm;

    .line 1727
    const/4 v0, 0x0

    .line 1728
    if-eqz v3, :cond_7

    .line 1729
    iget-object v0, v3, Lnjm;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1730
    iget-object v0, v3, Lnjm;->a:Ljava/lang/String;

    iput-object v0, p0, Lfba;->ai:Ljava/lang/String;

    .line 1732
    :cond_6
    iget-object v0, v3, Lnjm;->b:Lnjn;

    .line 1733
    iget-object v3, v3, Lnjm;->c:Ljava/lang/String;

    iput-object v3, p0, Lfba;->an:Ljava/lang/String;

    .line 1736
    :cond_7
    if-eqz v0, :cond_1a

    .line 1737
    iget-object v3, v0, Lnjn;->d:Ljava/lang/String;

    iput-object v3, p0, Lfba;->aj:Ljava/lang/String;

    .line 1738
    iget-object v3, v0, Lnjn;->b:Ljava/lang/String;

    iput-object v3, p0, Lfba;->ak:Ljava/lang/String;

    .line 1739
    iget-object v3, v0, Lnjn;->c:Ljava/lang/String;

    iput-object v3, p0, Lfba;->al:Ljava/lang/String;

    .line 1740
    iget-object v3, v0, Lnjn;->f:Ljava/lang/String;

    iput-object v3, p0, Lfba;->am:Ljava/lang/String;

    .line 1741
    iget-object v0, v0, Lnjn;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lfba;->av:Z

    .line 1749
    :cond_8
    :goto_5
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->h:Lnjo;

    if-eqz v0, :cond_a

    .line 1750
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->h:Lnjo;

    iget v0, v0, Lnjo;->a:I

    iput v0, p0, Lfba;->ah:I

    .line 1751
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->h:Lnjo;

    iget-object v0, v0, Lnjo;->b:Lnjq;

    if-eqz v0, :cond_a

    .line 1752
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->h:Lnjo;

    iget-object v0, v0, Lnjo;->b:Lnjq;

    iget-object v0, v0, Lnjq;->d:Lnjp;

    iput-object v0, p0, Lfba;->at:Lnjp;

    .line 1754
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->h:Lnjo;

    iget-object v0, v0, Lnjo;->b:Lnjq;

    iget-object v0, v0, Lnjq;->b:Lnjr;

    if-eqz v0, :cond_9

    .line 1755
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->h:Lnjo;

    iget-object v0, v0, Lnjo;->b:Lnjq;

    iget-object v0, v0, Lnjq;->b:Lnjr;

    iget-object v0, v0, Lnjr;->a:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    iput v0, p0, Lfba;->au:I

    .line 1758
    :cond_9
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->h:Lnjo;

    iget-object v0, v0, Lnjo;->b:Lnjq;

    iget v0, v0, Lnjq;->c:I

    iput v0, p0, Lfba;->ao:I

    .line 1760
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->h:Lnjo;

    iget-object v0, v0, Lnjo;->b:Lnjq;

    iget-object v0, v0, Lnjq;->e:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    iput v0, p0, Lfba;->as:I

    .line 1765
    :cond_a
    iget-boolean v0, p0, Lfba;->aa:Z

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lfba;->ak:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 1766
    iput-boolean v1, p0, Lfba;->ag:Z

    move v0, v1

    move-object v3, p0

    .line 1770
    :goto_6
    iput-boolean v0, v3, Lfba;->af:Z

    .line 1775
    :cond_b
    iget-boolean v0, p0, Lfba;->Y:Z

    if-eqz v0, :cond_20

    .line 1776
    iput v7, p0, Lfba;->ay:I

    .line 1787
    :goto_7
    iget-object v0, p0, Lfba;->T:Lmdp;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lfba;->T:Lmdp;

    iget-object v0, v0, Lmdp;->a:Lnlr;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lfba;->T:Lmdp;

    iget-object v0, v0, Lmdp;->a:Lnlr;

    iget-object v0, v0, Lnlr;->b:Lnfq;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lfba;->T:Lmdp;

    iget-object v0, v0, Lmdp;->a:Lnlr;

    iget-object v0, v0, Lnlr;->b:Lnfq;

    .line 1789
    invoke-direct {p0, v0}, Lfba;->a(Lnfq;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1790
    iput-boolean v1, p0, Lfba;->aq:Z

    .line 1795
    :goto_8
    iget-object v0, p0, Lfba;->U:Lnlp;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lfba;->U:Lnlp;

    iget-object v0, v0, Lnlp;->a:Lpvl;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lfba;->U:Lnlp;

    iget-object v0, v0, Lnlp;->a:Lpvl;

    iget-object v0, v0, Lpvl;->b:[Lpvt;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lfba;->U:Lnlp;

    iget-object v0, v0, Lnlp;->a:Lpvl;

    iget-object v0, v0, Lpvl;->b:[Lpvt;

    array-length v0, v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lfba;->U:Lnlp;

    iget-object v0, v0, Lnlp;->a:Lpvl;

    iget-object v0, v0, Lpvl;->b:[Lpvt;

    aget-object v0, v0, v2

    iget-object v0, v0, Lpvt;->d:[Lpvi;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lfba;->U:Lnlp;

    iget-object v0, v0, Lnlp;->a:Lpvl;

    iget-object v0, v0, Lpvl;->b:[Lpvt;

    aget-object v0, v0, v2

    iget-object v0, v0, Lpvt;->d:[Lpvi;

    array-length v0, v0

    if-eqz v0, :cond_23

    .line 1801
    iput-boolean v1, p0, Lfba;->ar:Z

    .line 1802
    iget-object v0, p0, Lfba;->U:Lnlp;

    iget-object v0, v0, Lnlp;->a:Lpvl;

    iget-object v0, v0, Lpvl;->b:[Lpvt;

    aget-object v0, v0, v2

    iget-object v0, v0, Lpvt;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lfba;->c(Ljava/lang/String;)V

    .line 1807
    :goto_9
    iget-object v0, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a()V

    .line 1808
    iget-object v0, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    iget-object v3, p0, Lfba;->w:Llhl;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(Llhl;)V

    .line 1809
    iget-boolean v0, p0, Lfba;->ab:Z

    if-eqz v0, :cond_24

    .line 1810
    iget-object v0, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v2, 0x7f0a02e2

    .line 1811
    invoke-virtual {p0, v2}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 1810
    invoke-virtual {v0, v6, v2, v1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;I)V

    .line 1852
    :cond_c
    :goto_a
    iget-object v0, p0, Lfba;->P:Ldsx;

    if-eqz v0, :cond_0

    .line 1853
    invoke-virtual {p0}, Lfba;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 1674
    :cond_d
    iget-object v0, p0, Lfba;->L:Ljava/lang/String;

    const-string v3, "p:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1675
    iget-object v0, p0, Lfba;->Q:Lnjt;

    new-instance v3, Lnib;

    invoke-direct {v3}, Lnib;-><init>()V

    iput-object v3, v0, Lnjt;->d:Lnib;

    .line 1676
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    new-instance v3, Lnif;

    invoke-direct {v3}, Lnif;-><init>()V

    iput-object v3, v0, Lnib;->c:Lnif;

    .line 1677
    new-instance v0, Lnka;

    invoke-direct {v0}, Lnka;-><init>()V

    .line 1678
    iget-object v3, p0, Lfba;->L:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lnka;->d:Ljava/lang/String;

    .line 1679
    new-instance v3, Lnie;

    invoke-direct {v3}, Lnie;-><init>()V

    iput-object v3, v0, Lnka;->c:Lnie;

    .line 1680
    iget-object v3, v0, Lnka;->c:Lnie;

    iput v5, v3, Lnie;->a:I

    .line 1681
    iget-object v3, p0, Lfba;->Q:Lnjt;

    iget-object v3, v3, Lnjt;->d:Lnib;

    iget-object v3, v3, Lnib;->c:Lnif;

    new-array v4, v1, [Lnka;

    aput-object v0, v4, v2

    iput-object v4, v3, Lnif;->a:[Lnka;

    goto/16 :goto_1

    .line 1693
    :cond_e
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget v0, v0, Lnjt;->b:I

    if-ne v0, v5, :cond_4

    .line 1694
    iput-boolean v1, p0, Lfba;->Y:Z

    .line 1695
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->f:Lnjg;

    iget v0, v0, Lnjg;->a:I

    if-ne v0, v1, :cond_f

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lfba;->Z:Z

    .line 1696
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-nez v0, :cond_10

    move v0, v2

    :goto_c
    iput-boolean v0, p0, Lfba;->aa:Z

    .line 1697
    iget-boolean v0, p0, Lfba;->aa:Z

    if-eqz v0, :cond_4

    .line 1698
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget v0, v0, Lnix;->c:I

    if-ne v0, v1, :cond_16

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lfba;->ab:Z

    goto/16 :goto_2

    :cond_f
    move v0, v2

    .line 1695
    goto :goto_b

    .line 1696
    :cond_10
    iget v3, v0, Lnjt;->b:I

    if-eq v3, v5, :cond_11

    move v0, v2

    goto :goto_c

    :cond_11
    iget-object v3, v0, Lnjt;->f:Lnjg;

    if-nez v3, :cond_12

    move v0, v2

    goto :goto_c

    :cond_12
    iget-object v3, v0, Lnjt;->f:Lnjg;

    iget v3, v3, Lnjg;->a:I

    if-eq v3, v1, :cond_13

    move v0, v2

    goto :goto_c

    :cond_13
    iget-object v3, v0, Lnjt;->f:Lnjg;

    iget-object v3, v3, Lnjg;->b:Lnix;

    if-eqz v3, :cond_14

    iget-object v0, v0, Lnjt;->f:Lnjg;

    iget-object v0, v0, Lnjg;->b:Lnix;

    iget-object v0, v0, Lnix;->d:Lpmm;

    if-nez v0, :cond_15

    :cond_14
    move v0, v2

    goto :goto_c

    :cond_15
    move v0, v1

    goto :goto_c

    :cond_16
    move v0, v2

    .line 1698
    goto :goto_d

    .line 1705
    :cond_17
    iget-object v0, p1, Ldsx;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 1706
    iget-object v0, p1, Ldsx;->c:Ljava/lang/String;

    iput-object v0, p0, Lfba;->aw:Ljava/lang/String;

    goto/16 :goto_3

    .line 1708
    :cond_18
    const v0, 0x7f0a02e0

    invoke-virtual {p0, v0}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfba;->aw:Ljava/lang/String;

    goto/16 :goto_3

    :cond_19
    move v0, v2

    .line 1712
    goto/16 :goto_4

    .line 1742
    :cond_1a
    iget-boolean v0, p0, Lfba;->aa:Z

    if-eqz v0, :cond_8

    .line 1743
    const/4 v0, 0x0

    iput-object v0, p0, Lfba;->aj:Ljava/lang/String;

    .line 1744
    iget-object v0, p0, Lfba;->Q:Lnjt;

    invoke-static {v0}, Ljnu;->k(Lnjt;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfba;->ak:Ljava/lang/String;

    .line 1745
    iget-object v0, p0, Lfba;->ak:Ljava/lang/String;

    iput-object v0, p0, Lfba;->al:Ljava/lang/String;

    .line 1746
    iget-object v0, p0, Lfba;->ak:Ljava/lang/String;

    if-eqz v0, :cond_1b

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lfba;->ap:Z

    goto/16 :goto_5

    :cond_1b
    move v0, v2

    goto :goto_e

    .line 1768
    :cond_1c
    iget-object v0, p0, Lfba;->ak:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 1769
    iget v0, p0, Lfba;->ah:I

    if-ne v0, v7, :cond_1e

    move v0, v1

    :goto_f
    iput-boolean v0, p0, Lfba;->ag:Z

    .line 1770
    iget-boolean v0, p0, Lfba;->ag:Z

    if-nez v0, :cond_1d

    iget v0, p0, Lfba;->ah:I

    if-ne v0, v5, :cond_1f

    :cond_1d
    move v0, v1

    move-object v3, p0

    goto/16 :goto_6

    :cond_1e
    move v0, v2

    .line 1769
    goto :goto_f

    :cond_1f
    move v0, v2

    move-object v3, p0

    .line 1770
    goto/16 :goto_6

    .line 1778
    :cond_20
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->d:Lnip;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->d:Lnip;

    iget v0, v0, Lnip;->b:I

    const/high16 v3, -0x80000000

    if-eq v0, v3, :cond_21

    .line 1781
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->d:Lnip;

    iget v0, v0, Lnip;->b:I

    iput v0, p0, Lfba;->ay:I

    goto/16 :goto_7

    .line 1783
    :cond_21
    iput v2, p0, Lfba;->ay:I

    goto/16 :goto_7

    .line 1792
    :cond_22
    iput-boolean v2, p0, Lfba;->aq:Z

    goto/16 :goto_8

    .line 1804
    :cond_23
    iput-boolean v2, p0, Lfba;->ar:Z

    goto/16 :goto_9

    .line 1814
    :cond_24
    iget-object v3, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v0, 0x7f0a02e2

    .line 1815
    invoke-virtual {p0, v0}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p0, Lfba;->N:Z

    if-eqz v0, :cond_2b

    sget-object v0, Long;->a:Lhmn;

    .line 1814
    :goto_10
    invoke-virtual {v3, v6, v4, v1, v0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;ILhmn;)V

    .line 1818
    iget-boolean v0, p0, Lfba;->M:Z

    if-nez v0, :cond_25

    invoke-virtual {p0}, Lfba;->m()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1819
    :cond_25
    iget-object v0, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v1, 0x7f0a02e1

    .line 1820
    invoke-virtual {p0, v1}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Long;->d:Lhmn;

    .line 1819
    invoke-virtual {v0, v6, v1, v2, v3}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;ILhmn;)V

    .line 1824
    :cond_26
    iget-object v0, p0, Lfba;->aS:Lgcs;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lfba;->aS:Lgcs;

    iget v1, p0, Lfba;->g:I

    .line 1825
    invoke-interface {v0}, Lgcs;->g()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 1826
    iget-object v0, p0, Lfba;->aS:Lgcs;

    invoke-interface {v0}, Lgcs;->b()Ljava/lang/String;

    move-result-object v0

    .line 1827
    iget-object v1, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    .line 1828
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x5

    sget-object v3, Long;->b:Lhmn;

    .line 1827
    invoke-virtual {v1, v6, v0, v2, v3}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;ILhmn;)V

    .line 1832
    :cond_27
    iget-boolean v0, p0, Lfba;->M:Z

    if-nez v0, :cond_28

    invoke-virtual {p0}, Lfba;->n()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 1833
    :cond_28
    iget-object v0, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v1, 0x7f0a02e3

    .line 1834
    invoke-virtual {p0, v1}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Long;->c:Lhmn;

    .line 1833
    invoke-virtual {v0, v6, v1, v5, v2}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;ILhmn;)V

    .line 1838
    :cond_29
    invoke-virtual {p0}, Lfba;->p()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-boolean v0, p0, Lfba;->aq:Z

    if-eqz v0, :cond_2a

    .line 1839
    iget-object v0, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v1, 0x7f0a02e5

    .line 1840
    invoke-virtual {p0, v1}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Long;->m:Lhmn;

    .line 1839
    invoke-virtual {v0, v6, v1, v7, v2}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;ILhmn;)V

    .line 1844
    :cond_2a
    invoke-virtual {p0}, Lfba;->o()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, p0, Lfba;->ar:Z

    if-eqz v0, :cond_c

    .line 1845
    iget-object v0, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    const v1, 0x7f0a02e4

    .line 1846
    invoke-virtual {p0, v1}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    sget-object v3, Long;->l:Lhmn;

    .line 1845
    invoke-virtual {v0, v6, v1, v2, v3}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(ILjava/lang/String;ILhmn;)V

    goto/16 :goto_a

    .line 1815
    :cond_2b
    const/4 v0, 0x0

    goto/16 :goto_10
.end method

.method public a(Lfys;)V
    .locals 1

    .prologue
    .line 2372
    iput-object p1, p0, Lfba;->K:Lfys;

    .line 2373
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    if-eqz v0, :cond_0

    .line 2374
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Lfys;)V

    .line 2376
    :cond_0
    return-void
.end method

.method public a(Lfyz;Llhl;Ljnt;Ljni;Ljmv;Ljms;Ljmx;Lgas;Ljnj;Ljmi;Ljmy;Ljnk;Ljns;Lfzc;Lfyv;)V
    .locals 0

    .prologue
    .line 450
    iput-object p1, p0, Lfba;->v:Lfyz;

    .line 451
    iput-object p2, p0, Lfba;->w:Llhl;

    .line 452
    iput-object p3, p0, Lfba;->x:Ljnt;

    .line 453
    iput-object p4, p0, Lfba;->y:Ljni;

    .line 454
    iput-object p5, p0, Lfba;->z:Ljmv;

    .line 455
    iput-object p6, p0, Lfba;->A:Ljms;

    .line 456
    iput-object p7, p0, Lfba;->B:Ljmx;

    .line 457
    iput-object p8, p0, Lfba;->C:Lgas;

    .line 458
    iput-object p9, p0, Lfba;->D:Ljnj;

    .line 459
    iput-object p10, p0, Lfba;->F:Ljmi;

    .line 460
    iput-object p11, p0, Lfba;->G:Ljmy;

    .line 461
    iput-object p12, p0, Lfba;->H:Ljnk;

    .line 462
    iput-object p13, p0, Lfba;->E:Ljns;

    .line 463
    iput-object p14, p0, Lfba;->I:Lfzc;

    .line 464
    iput-object p15, p0, Lfba;->J:Lfyv;

    .line 465
    return-void
.end method

.method public a(Ljava/lang/String;ZZLhxh;Lequ;)V
    .locals 13

    .prologue
    .line 411
    iput-object p1, p0, Lfba;->L:Ljava/lang/String;

    .line 412
    iput-boolean p2, p0, Lfba;->M:Z

    .line 413
    move/from16 v0, p3

    iput-boolean v0, p0, Lfba;->N:Z

    .line 414
    move-object/from16 v0, p4

    iput-object v0, p0, Lfba;->aL:Lhxh;

    .line 417
    iget-object v1, p0, Lfba;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0291

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v12

    .line 419
    sget-object v1, Leet;->N:Llpa;

    .line 420
    new-instance v1, Levi;

    iget-object v2, p0, Lfba;->k:Landroid/content/Context;

    const/4 v3, 0x0

    iget v4, p0, Lfba;->g:I

    iget-object v5, p0, Lfba;->L:Ljava/lang/String;

    .line 421
    invoke-static {v5}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v7, p5

    move-object/from16 v8, p5

    invoke-direct/range {v1 .. v11}, Levi;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;ZLeqt;Ldxq;ZZLcom/google/android/apps/plus/views/FastScrollContainer;)V

    iput-object v1, p0, Lfba;->a:Levi;

    .line 428
    iget-object v1, p0, Lfba;->a:Levi;

    invoke-virtual {v1, v12}, Levi;->a(I)V

    .line 430
    iget-object v1, p0, Lfba;->a:Levi;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Levi;->a(Landroid/view/View;)V

    .line 431
    iget-object v1, p0, Lfba;->a:Levi;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Levi;->a(Z)V

    .line 433
    return-void
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 468
    iput-object p1, p0, Lfba;->aC:Loo;

    .line 469
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1018
    iput-boolean p1, p0, Lfba;->aK:Z

    .line 1019
    return-void
.end method

.method public a(ZZ)V
    .locals 1

    .prologue
    .line 1908
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfba;->O:Z

    .line 1909
    if-eqz p1, :cond_0

    .line 1912
    iput-boolean p2, p0, Lfba;->ad:Z

    .line 1914
    :cond_0
    invoke-virtual {p0}, Lfba;->s()V

    .line 1915
    invoke-virtual {p0}, Lfba;->r()V

    .line 1916
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 485
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 486
    iget-object v2, p0, Lfba;->aE:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 490
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 486
    goto :goto_0

    .line 490
    :cond_2
    iget-object v2, p0, Lfba;->aD:Lhym;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public a(Lnjt;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2023
    .line 2024
    iget-object v1, p0, Lfba;->Q:Lnjt;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lfba;->Q:Lnjt;

    iget-object v1, v1, Lnjt;->d:Lnib;

    if-eqz v1, :cond_2

    .line 2025
    iget-object v1, p0, Lfba;->Q:Lnjt;

    iget-object v1, v1, Lnjt;->d:Lnib;

    iget-object v1, v1, Lnib;->f:Ljava/lang/String;

    .line 2028
    :goto_0
    if-eqz p1, :cond_0

    iget-object v2, p1, Lnjt;->d:Lnib;

    if-eqz v2, :cond_0

    .line 2029
    iget-object v0, p1, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->f:Ljava/lang/String;

    .line 2031
    :cond_0
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public aa()[B
    .locals 1

    .prologue
    .line 2238
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->d:Lnip;

    if-nez v0, :cond_1

    .line 2239
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2241
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->d:Lnip;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public ab()[B
    .locals 1

    .prologue
    .line 2245
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->l:Lnjk;

    if-nez v0, :cond_1

    .line 2247
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2249
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->l:Lnjk;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public ac()[B
    .locals 1

    .prologue
    .line 2253
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->k:Lnjl;

    if-nez v0, :cond_1

    .line 2254
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2256
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->k:Lnjl;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public ad()[B
    .locals 1

    .prologue
    .line 2260
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->b:Lnjf;

    if-nez v0, :cond_1

    .line 2261
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 2263
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->b:Lnjf;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public ae()I
    .locals 1

    .prologue
    .line 2270
    iget v0, p0, Lfba;->ay:I

    return v0
.end method

.method public af()Z
    .locals 1

    .prologue
    .line 2338
    iget-object v0, p0, Lfba;->ac:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ag()Lcom/google/android/apps/plus/views/OneProfileAvatarView;
    .locals 1

    .prologue
    .line 2379
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    if-eqz v0, :cond_0

    .line 2380
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->h()Lcom/google/android/apps/plus/views/OneProfileAvatarView;

    move-result-object v0

    .line 2382
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 495
    iget-object v0, p0, Lfba;->aD:Lhym;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lfba;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_0
    move v0, v1

    .line 497
    :goto_0
    if-eqz v0, :cond_1a

    .line 498
    iput-object p1, p0, Lfba;->aE:Ljava/lang/String;

    .line 500
    iget-boolean v5, p0, Lfba;->M:Z

    .line 501
    iget-boolean v0, p0, Lfba;->M:Z

    if-eqz v0, :cond_18

    iget-boolean v0, p0, Lfba;->Y:Z

    if-nez v0, :cond_18

    move v0, v1

    .line 502
    :goto_1
    iget-boolean v6, p0, Lfba;->Z:Z

    .line 503
    iget-boolean v7, p0, Lfba;->aa:Z

    .line 506
    new-instance v3, Lhym;

    sget-object v8, Lfba;->r:[Ljava/lang/String;

    invoke-direct {v3, v8}, Lhym;-><init>([Ljava/lang/String;)V

    iput-object v3, p0, Lfba;->aD:Lhym;

    .line 507
    if-eqz v7, :cond_1

    iget-object v3, p0, Lfba;->Q:Lnjt;

    .line 508
    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutActionsView;->b(Lnjt;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 509
    new-array v3, v11, [Ljava/lang/Object;

    const/16 v8, 0xd

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 510
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 512
    :cond_1
    if-eqz v6, :cond_2

    iget-object v3, p0, Lfba;->Q:Lnjt;

    .line 513
    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLocalContactInfoView;->b(Lnjt;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 514
    new-array v3, v11, [Ljava/lang/Object;

    const/16 v8, 0xe

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 515
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 517
    :cond_2
    if-eqz v7, :cond_3

    iget-object v3, p0, Lfba;->Q:Lnjt;

    .line 518
    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewSummaryView;->b(Lnjt;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 519
    new-array v3, v11, [Ljava/lang/Object;

    const/16 v8, 0x9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 520
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 522
    :cond_3
    if-eqz v7, :cond_4

    iget-object v3, p0, Lfba;->Q:Lnjt;

    .line 523
    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutZagatListView;->b(Lnjt;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 524
    new-array v3, v11, [Ljava/lang/Object;

    const/16 v8, 0xa

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 525
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 527
    :cond_4
    if-eqz v6, :cond_5

    iget-object v3, p0, Lfba;->Q:Lnjt;

    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutHoursView;->b(Lnjt;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 528
    new-array v3, v11, [Ljava/lang/Object;

    const/16 v8, 0xb

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 529
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 531
    :cond_5
    iget-object v3, p0, Lfba;->Q:Lnjt;

    iget-boolean v8, p0, Lfba;->M:Z

    iget-boolean v9, p0, Lfba;->Y:Z

    invoke-static {v3, v8, v9, v6}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutMyStoryView;->a(Lnjt;ZZZ)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 533
    new-array v3, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 534
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 536
    :cond_6
    iget-boolean v3, p0, Lfba;->M:Z

    if-nez v3, :cond_7

    iget-object v3, p0, Lfba;->S:Lmcf;

    if-eqz v3, :cond_19

    iget-object v3, p0, Lfba;->S:Lmcf;

    iget-object v3, v3, Lmcf;->a:Lnlm;

    if-eqz v3, :cond_19

    iget-object v3, p0, Lfba;->S:Lmcf;

    iget-object v3, v3, Lmcf;->a:Lnlm;

    iget-object v3, v3, Lnlm;->a:Lofz;

    :goto_2
    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPeopleView;->b(Lofz;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 539
    :cond_7
    new-array v3, v11, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 540
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 542
    :cond_8
    iget-boolean v3, p0, Lfba;->M:Z

    iget-object v8, p0, Lfba;->V:Lnts;

    iget-object v9, p0, Lfba;->W:Lnto;

    iget-object v10, p0, Lfba;->X:Lnso;

    invoke-static {v3, v8, v9, v10}, Lcom/google/android/libraries/social/squares/profile/about/OneProfileAboutSquaresView;->a(ZLnts;Lnto;Lnso;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 544
    new-array v3, v11, [Ljava/lang/Object;

    const/16 v8, 0x16

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 545
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 547
    :cond_9
    if-nez v0, :cond_a

    iget-object v3, p0, Lfba;->Q:Lnjt;

    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutWorkView;->b(Lnjt;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 548
    :cond_a
    new-array v3, v11, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 549
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 551
    :cond_b
    if-nez v0, :cond_c

    iget-object v3, p0, Lfba;->Q:Lnjt;

    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutEducationView;->b(Lnjt;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 552
    :cond_c
    new-array v3, v11, [Ljava/lang/Object;

    const/4 v8, 0x3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 553
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 555
    :cond_d
    iget-boolean v3, p0, Lfba;->Y:Z

    if-nez v3, :cond_f

    if-nez v0, :cond_e

    iget-object v3, p0, Lfba;->Q:Lnjt;

    .line 557
    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutCurrentLocationView;->b(Lnjt;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 558
    :cond_e
    new-array v3, v11, [Ljava/lang/Object;

    const/4 v8, 0x4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 559
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 561
    :cond_f
    if-nez v0, :cond_10

    iget-object v3, p0, Lfba;->Q:Lnjt;

    invoke-static {v3}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutPlacesView;->b(Lnjt;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 562
    :cond_10
    new-array v3, v11, [Ljava/lang/Object;

    const/4 v8, 0x5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v2

    aput-object v4, v3, v1

    .line 563
    iget-object v8, p0, Lfba;->aD:Lhym;

    invoke-virtual {v8, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 565
    :cond_11
    if-nez v0, :cond_12

    iget-object v0, p0, Lfba;->Q:Lnjt;

    invoke-static {v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutBasicInfoView;->b(Lnjt;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 566
    :cond_12
    new-array v0, v11, [Ljava/lang/Object;

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    aput-object v4, v0, v1

    .line 567
    iget-object v3, p0, Lfba;->aD:Lhym;

    invoke-virtual {v3, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 569
    :cond_13
    if-nez v6, :cond_15

    if-nez v5, :cond_14

    iget-object v0, p0, Lfba;->Q:Lnjt;

    .line 570
    invoke-static {v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutContactInfoView;->b(Lnjt;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 571
    :cond_14
    new-array v0, v11, [Ljava/lang/Object;

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    aput-object v4, v0, v1

    .line 572
    iget-object v3, p0, Lfba;->aD:Lhym;

    invoke-virtual {v3, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 575
    :cond_15
    iget-object v0, p0, Lfba;->Q:Lnjt;

    invoke-static {v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutLinksView;->b(Lnjt;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 576
    new-array v0, v11, [Ljava/lang/Object;

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    aput-object v4, v0, v1

    .line 577
    iget-object v3, p0, Lfba;->aD:Lhym;

    invoke-virtual {v3, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 579
    :cond_16
    if-eqz v7, :cond_1a

    iget-object v0, p0, Lfba;->Q:Lnjt;

    .line 580
    invoke-static {v0}, Lcom/google/android/libraries/social/oneprofile/about/OneProfileAboutReviewView;->b(Lnjt;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 581
    iget-object v0, p0, Lfba;->Q:Lnjt;

    invoke-static {v0}, Ljnu;->g(Lnjt;)Ljava/util/List;

    move-result-object v0

    .line 582
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpmx;

    .line 583
    new-array v4, v11, [Ljava/lang/Object;

    const/16 v5, 0xc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v0, v0, Lpmx;->f:Ljava/lang/String;

    aput-object v0, v4, v1

    .line 584
    iget-object v0, p0, Lfba;->aD:Lhym;

    invoke-virtual {v0, v4}, Lhym;->a([Ljava/lang/Object;)V

    goto :goto_3

    :cond_17
    move v0, v2

    .line 495
    goto/16 :goto_0

    :cond_18
    move v0, v2

    .line 501
    goto/16 :goto_1

    :cond_19
    move-object v3, v4

    .line 536
    goto/16 :goto_2

    .line 589
    :cond_1a
    iget-object v0, p0, Lfba;->aD:Lhym;

    return-object v0
.end method

.method public b()V
    .locals 7

    .prologue
    const/16 v2, 0xff

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 351
    invoke-virtual {p0, v4}, Lfba;->l(I)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    if-nez v0, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v0, p0, Lfba;->aA:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c()I

    move-result v0

    if-nez v0, :cond_a

    .line 357
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    if-eqz v0, :cond_9

    .line 358
    iget-object v0, p0, Lfba;->aA:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_1
    iput v0, p0, Lfba;->aB:I

    .line 360
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget v3, p0, Lfba;->aB:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/OneProfileHeader;->b(I)V

    .line 361
    iget-object v0, p0, Lfba;->aC:Loo;

    if-nez v0, :cond_5

    move v0, v1

    .line 362
    :goto_2
    iget-object v3, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/OneProfileHeader;->getHeight()I

    move-result v3

    sub-int v0, v3, v0

    .line 363
    iget v3, p0, Lfba;->aB:I

    add-int/2addr v3, v0

    .line 364
    sget v5, Lfba;->o:I

    if-gt v3, v5, :cond_6

    .line 365
    iput-boolean v4, p0, Lfba;->aI:Z

    move v0, v1

    move v3, v2

    .line 387
    :goto_3
    iget-object v5, p0, Lfba;->aC:Loo;

    if-eqz v5, :cond_2

    .line 388
    iget-boolean v5, p0, Lfba;->aI:Z

    if-eqz v5, :cond_b

    .line 389
    sget-object v0, Lfba;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 390
    iget-object v0, p0, Lfba;->aC:Loo;

    sget-object v2, Lfba;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Loo;->b(Landroid/graphics/drawable/Drawable;)V

    .line 400
    :cond_2
    :goto_4
    iget-boolean v0, p0, Lfba;->aI:Z

    if-eqz v0, :cond_c

    move v0, v1

    .line 401
    :goto_5
    iget-object v2, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->getVisibility()I

    move-result v2

    if-eq v2, v0, :cond_3

    move v1, v4

    .line 402
    :cond_3
    if-eqz v1, :cond_0

    .line 403
    iget-object v1, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->setVisibility(I)V

    .line 404
    iget-object v0, p0, Lfba;->aJ:Ljava/lang/Runnable;

    invoke-static {v0, v4}, Llsx;->a(Ljava/lang/Runnable;Z)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 359
    goto :goto_1

    .line 361
    :cond_5
    iget-object v0, p0, Lfba;->aC:Loo;

    invoke-virtual {v0}, Loo;->d()I

    move-result v0

    goto :goto_2

    .line 367
    :cond_6
    iput-boolean v1, p0, Lfba;->aI:Z

    .line 370
    iget v3, p0, Lfba;->aB:I

    div-int/lit8 v5, v0, 0x2

    add-int/2addr v3, v5

    sget v5, Lfba;->o:I

    sub-int/2addr v3, v5

    mul-int/lit16 v3, v3, 0xff

    div-int/lit8 v0, v0, 0x2

    sget v5, Lfba;->o:I

    sub-int/2addr v0, v5

    div-int v0, v3, v0

    .line 372
    if-le v0, v2, :cond_8

    move v0, v2

    .line 377
    :cond_7
    :goto_6
    rsub-int v3, v0, 0xff

    move v6, v3

    move v3, v0

    move v0, v6

    .line 379
    goto :goto_3

    .line 374
    :cond_8
    if-gez v0, :cond_7

    move v0, v1

    .line 375
    goto :goto_6

    .line 380
    :cond_9
    iput-boolean v1, p0, Lfba;->aI:Z

    move v0, v1

    move v3, v2

    goto :goto_3

    .line 383
    :cond_a
    iput-boolean v4, p0, Lfba;->aI:Z

    .line 384
    iput v1, p0, Lfba;->aB:I

    move v0, v1

    move v3, v2

    goto :goto_3

    .line 392
    :cond_b
    sget-object v2, Lfba;->q:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 393
    sget-object v2, Lfba;->q:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v2, v4}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 394
    iget-object v0, p0, Lfba;->aC:Loo;

    sget-object v2, Lfba;->q:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0, v2}, Loo;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 400
    :cond_c
    const/4 v0, 0x4

    goto :goto_5
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 1535
    iget-object v0, p0, Lfba;->aA:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(I)V

    .line 1536
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 2365
    iput-boolean p1, p0, Lfba;->az:Z

    .line 2366
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    if-eqz v0, :cond_0

    .line 2367
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->e(Z)V

    .line 2369
    :cond_0
    return-void
.end method

.method public b(Lnjt;)Z
    .locals 2

    .prologue
    .line 2076
    const/4 v0, 0x0

    .line 2077
    if-eqz p1, :cond_0

    iget-object v1, p1, Lnjt;->d:Lnib;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lnjt;->d:Lnib;

    iget-object v1, v1, Lnib;->i:Lnjm;

    if-eqz v1, :cond_0

    .line 2078
    iget-object v1, p1, Lnjt;->d:Lnib;

    iget-object v1, v1, Lnib;->i:Lnjm;

    iget-object v1, v1, Lnjm;->b:Lnjn;

    .line 2079
    if-eqz v1, :cond_1

    .line 2080
    iget-object v0, v1, Lnjn;->c:Ljava/lang/String;

    .line 2085
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lfba;->N()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2081
    :cond_1
    iget-boolean v1, p0, Lfba;->aa:Z

    if-eqz v1, :cond_0

    .line 2082
    invoke-static {p1}, Ljnu;->k(Lnjt;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2085
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c(I)I
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1560
    iget v0, p0, Lfba;->f:I

    packed-switch v0, :pswitch_data_0

    .line 1590
    invoke-super {p0, p1}, Lfdj;->c(I)I

    move-result v0

    :goto_0
    return v0

    .line 1564
    :pswitch_0
    invoke-virtual {p0, v1}, Lfba;->l(I)Landroid/database/Cursor;

    move-result-object v0

    .line 1565
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1566
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-direct {p0, v0}, Lfba;->p(I)I

    move-result v0

    goto :goto_0

    .line 1569
    :pswitch_1
    invoke-virtual {p0, v1}, Lfba;->l(I)Landroid/database/Cursor;

    move-result-object v0

    .line 1570
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1571
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lfba;->p(I)I

    move-result v0

    goto :goto_0

    .line 1573
    :cond_0
    const/16 v0, 0x17

    invoke-direct {p0, v0}, Lfba;->p(I)I

    move-result v0

    goto :goto_0

    .line 1577
    :pswitch_2
    invoke-direct {p0}, Lfba;->ax()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1578
    iget-object v0, p0, Lfba;->a:Levi;

    .line 1579
    invoke-virtual {v0, p1}, Levi;->getItemViewType(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x19

    .line 1578
    invoke-direct {p0, v0}, Lfba;->p(I)I

    move-result v0

    goto :goto_0

    .line 1581
    :cond_1
    invoke-virtual {p0, v1}, Lfba;->l(I)Landroid/database/Cursor;

    .line 1582
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Lfba;->p(I)I

    move-result v0

    goto :goto_0

    .line 1560
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 476
    iget-boolean v1, p0, Lfba;->aI:Z

    if-eqz v1, :cond_1

    .line 477
    iget-object v1, p0, Lfba;->aC:Loo;

    if-nez v1, :cond_0

    .line 478
    :goto_0
    iget-object v1, p0, Lfba;->aA:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    const/4 v2, 0x1

    sget v3, Lfba;->o:I

    add-int/2addr v0, v3

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(II)V

    .line 482
    :goto_1
    return-void

    .line 477
    :cond_0
    iget-object v0, p0, Lfba;->aC:Loo;

    invoke-virtual {v0}, Loo;->d()I

    move-result v0

    goto :goto_0

    .line 480
    :cond_1
    iget-object v1, p0, Lfba;->aA:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget v2, p0, Lfba;->aB:I

    invoke-virtual {v1, v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(II)V

    goto :goto_1
.end method

.method public c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 710
    iput-object p1, p0, Lfba;->aH:Ljava/lang/String;

    .line 711
    return-void
.end method

.method d(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1638
    iget-object v0, p0, Lfba;->k:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 612
    iget-object v0, p0, Lfba;->aG:Lhym;

    if-eqz v0, :cond_0

    .line 613
    iget-object v0, p0, Lfba;->aG:Lhym;

    invoke-virtual {v0}, Lhym;->moveToFirst()Z

    .line 614
    iget-object v0, p0, Lfba;->aG:Lhym;

    invoke-virtual {v0, v2}, Lhym;->getInt(I)I

    move-result v0

    const/16 v3, 0x11

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 615
    :goto_0
    iget-object v3, p0, Lfba;->aG:Lhym;

    invoke-virtual {v3}, Lhym;->getCount()I

    move-result v4

    if-eqz v0, :cond_2

    move v3, v1

    :goto_1
    sub-int v5, v4, v3

    .line 619
    iget-object v3, p0, Lfba;->U:Lnlp;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lfba;->U:Lnlp;

    iget-object v3, v3, Lnlp;->a:Lpvl;

    if-eqz v3, :cond_4

    .line 620
    iget-object v3, p0, Lfba;->U:Lnlp;

    iget-object v3, v3, Lnlp;->a:Lpvl;

    .line 621
    invoke-direct {p0, v3}, Lfba;->a(Lpvl;)Z

    move-result v4

    .line 622
    invoke-direct {p0, v3}, Lfba;->b(Lpvl;)I

    move-result v3

    .line 625
    :goto_2
    if-ne v0, v4, :cond_0

    if-eq v5, v3, :cond_3

    .line 627
    :cond_0
    :goto_3
    return v1

    :cond_1
    move v0, v2

    .line 614
    goto :goto_0

    :cond_2
    move v3, v2

    .line 615
    goto :goto_1

    :cond_3
    move v1, v2

    .line 625
    goto :goto_3

    :cond_4
    move v3, v2

    move v4, v2

    goto :goto_2
.end method

.method public e()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 631
    invoke-virtual {p0}, Lfba;->d()Z

    move-result v0

    .line 632
    if-eqz v0, :cond_2

    .line 633
    new-instance v0, Lhym;

    sget-object v2, Lfba;->s:[Ljava/lang/String;

    invoke-direct {v0, v2}, Lhym;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lfba;->aG:Lhym;

    .line 635
    iget-object v0, p0, Lfba;->U:Lnlp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfba;->U:Lnlp;

    iget-object v0, v0, Lnlp;->a:Lpvl;

    if-eqz v0, :cond_2

    .line 636
    iget-object v0, p0, Lfba;->U:Lnlp;

    iget-object v0, v0, Lnlp;->a:Lpvl;

    .line 637
    invoke-direct {p0, v0}, Lfba;->a(Lpvl;)Z

    move-result v2

    .line 638
    invoke-direct {p0, v0}, Lfba;->b(Lpvl;)I

    move-result v3

    .line 640
    if-eqz v2, :cond_0

    .line 641
    iget-object v0, p0, Lfba;->aG:Lhym;

    new-array v2, v7, [Ljava/lang/Object;

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v6

    invoke-virtual {v0, v2}, Lhym;->a([Ljava/lang/Object;)V

    :cond_0
    move v0, v1

    .line 643
    :goto_0
    if-ge v0, v3, :cond_1

    .line 644
    iget-object v2, p0, Lfba;->aG:Lhym;

    new-array v4, v7, [Ljava/lang/Object;

    const/16 v5, 0x12

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v4}, Lhym;->a([Ljava/lang/Object;)V

    .line 643
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 646
    :cond_1
    iget-object v0, p0, Lfba;->aH:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 647
    iget-object v0, p0, Lfba;->aG:Lhym;

    new-array v2, v7, [Ljava/lang/Object;

    const/16 v3, 0x13

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v6

    invoke-virtual {v0, v2}, Lhym;->a([Ljava/lang/Object;)V

    .line 652
    :cond_2
    iget-object v0, p0, Lfba;->aG:Lhym;

    return-object v0
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 1891
    iget-object v0, p0, Lfba;->d:Lcom/google/android/libraries/social/ui/tabbar/TabBar;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/ui/tabbar/TabBar;->a(I)V

    .line 1892
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    if-eqz v0, :cond_0

    .line 1893
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(I)V

    .line 1895
    :cond_0
    iput p1, p0, Lfba;->f:I

    .line 1896
    return-void
.end method

.method public f()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 656
    iget-object v0, p0, Lfba;->aF:Lhym;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->aF:Lhym;

    invoke-virtual {v0}, Lhym;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lfba;->aF:Lhym;

    invoke-virtual {v0, v2}, Lhym;->getInt(I)I

    move-result v0

    const/16 v3, 0x14

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 658
    :goto_0
    iget-object v3, p0, Lfba;->aF:Lhym;

    invoke-virtual {v3}, Lhym;->getCount()I

    move-result v4

    if-eqz v0, :cond_2

    move v3, v1

    :goto_1
    sub-int v5, v4, v3

    .line 662
    iget-object v3, p0, Lfba;->T:Lmdp;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lfba;->T:Lmdp;

    iget-object v3, v3, Lmdp;->a:Lnlr;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lfba;->T:Lmdp;

    iget-object v3, v3, Lmdp;->a:Lnlr;

    iget-object v3, v3, Lnlr;->b:Lnfq;

    if-eqz v3, :cond_4

    .line 664
    iget-object v3, p0, Lfba;->T:Lmdp;

    iget-object v3, v3, Lmdp;->a:Lnlr;

    iget-object v3, v3, Lnlr;->b:Lnfq;

    .line 665
    invoke-direct {p0, v3}, Lfba;->a(Lnfq;)Z

    move-result v4

    .line 666
    invoke-direct {p0, v3}, Lfba;->b(Lnfq;)I

    move-result v3

    .line 669
    :goto_2
    if-ne v0, v4, :cond_0

    if-eq v5, v3, :cond_3

    .line 671
    :cond_0
    :goto_3
    return v1

    :cond_1
    move v0, v2

    .line 657
    goto :goto_0

    :cond_2
    move v3, v2

    .line 658
    goto :goto_1

    :cond_3
    move v1, v2

    .line 669
    goto :goto_3

    :cond_4
    move v3, v2

    move v4, v2

    goto :goto_2
.end method

.method public f(I)[B
    .locals 2

    .prologue
    .line 2144
    const/4 v0, 0x0

    .line 2145
    iget-object v1, p0, Lfba;->Q:Lnjt;

    if-eqz v1, :cond_0

    .line 2146
    sparse-switch p1, :sswitch_data_0

    .line 2169
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    new-array v0, v0, [B

    :goto_1
    return-object v0

    .line 2148
    :sswitch_0
    iget-object v1, p0, Lfba;->Q:Lnjt;

    iget-object v1, v1, Lnjt;->e:Lnkd;

    if-eqz v1, :cond_0

    .line 2149
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->e:Lnjv;

    goto :goto_0

    .line 2153
    :sswitch_1
    iget-object v1, p0, Lfba;->Q:Lnjt;

    iget-object v1, v1, Lnjt;->e:Lnkd;

    if-eqz v1, :cond_0

    .line 2154
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->n:Lnjv;

    goto :goto_0

    .line 2158
    :sswitch_2
    iget-object v1, p0, Lfba;->Q:Lnjt;

    iget-object v1, v1, Lnjt;->d:Lnib;

    if-eqz v1, :cond_0

    .line 2159
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->d:Lnib;

    iget-object v0, v0, Lnib;->d:Lnjv;

    goto :goto_0

    .line 2163
    :sswitch_3
    iget-object v1, p0, Lfba;->Q:Lnjt;

    iget-object v1, v1, Lnjt;->e:Lnkd;

    if-eqz v1, :cond_0

    .line 2164
    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->e:Lnkd;

    iget-object v0, v0, Lnkd;->j:Lnjv;

    goto :goto_0

    .line 2169
    :cond_1
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    goto :goto_1

    .line 2146
    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_3
        0xd -> :sswitch_0
        0xf -> :sswitch_2
        0x26 -> :sswitch_1
    .end sparse-switch
.end method

.method public g()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 683
    invoke-virtual {p0}, Lfba;->f()Z

    move-result v0

    .line 684
    if-eqz v0, :cond_1

    .line 685
    new-instance v0, Lhym;

    sget-object v2, Lfba;->t:[Ljava/lang/String;

    invoke-direct {v0, v2}, Lhym;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lfba;->aF:Lhym;

    .line 687
    iget-object v0, p0, Lfba;->T:Lmdp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfba;->T:Lmdp;

    iget-object v0, v0, Lmdp;->a:Lnlr;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfba;->T:Lmdp;

    iget-object v0, v0, Lmdp;->a:Lnlr;

    iget-object v0, v0, Lnlr;->b:Lnfq;

    if-eqz v0, :cond_1

    .line 689
    iget-object v0, p0, Lfba;->T:Lmdp;

    iget-object v0, v0, Lmdp;->a:Lnlr;

    iget-object v0, v0, Lnlr;->b:Lnfq;

    .line 690
    invoke-direct {p0, v0}, Lfba;->a(Lnfq;)Z

    move-result v2

    .line 691
    invoke-direct {p0, v0}, Lfba;->b(Lnfq;)I

    move-result v3

    .line 693
    if-eqz v2, :cond_0

    .line 694
    iget-object v0, p0, Lfba;->aF:Lhym;

    new-array v2, v7, [Ljava/lang/Object;

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v6

    invoke-virtual {v0, v2}, Lhym;->a([Ljava/lang/Object;)V

    :cond_0
    move v0, v1

    .line 696
    :goto_0
    if-ge v0, v3, :cond_1

    .line 697
    iget-object v2, p0, Lfba;->aF:Lhym;

    new-array v4, v7, [Ljava/lang/Object;

    const/16 v5, 0x15

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v4}, Lhym;->a([Ljava/lang/Object;)V

    .line 696
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 702
    :cond_1
    iget-object v0, p0, Lfba;->aF:Lhym;

    return-object v0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 1507
    invoke-direct {p0}, Lfba;->ax()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1508
    iget-object v0, p0, Lfba;->a:Levi;

    invoke-virtual {v0}, Levi;->getCount()I

    move-result v0

    .line 1509
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lfba;->j(I)Lhyb;

    move-result-object v1

    .line 1510
    invoke-virtual {v1}, Lhyb;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez v0, :cond_0

    .line 1511
    invoke-virtual {v1}, Lhyb;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1512
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 1515
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lfba;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1517
    iget v1, p0, Lfba;->aR:I

    if-ltz v1, :cond_2

    iget v1, p0, Lfba;->aR:I

    if-eq v0, v1, :cond_2

    .line 1518
    invoke-virtual {p0, v0}, Lfba;->b(I)V

    .line 1520
    :cond_2
    iput v0, p0, Lfba;->aR:I

    .line 1523
    :goto_0
    return v0

    :cond_3
    invoke-super {p0}, Lfdj;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1540
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfba;->a(I)I

    move-result v0

    .line 1541
    invoke-direct {p0}, Lfba;->ax()Z

    move-result v1

    if-eqz v1, :cond_0

    if-lt p1, v0, :cond_0

    .line 1542
    sub-int v0, p1, v0

    .line 1543
    iget-object v1, p0, Lfba;->a:Levi;

    invoke-virtual {v1, v0}, Levi;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 1545
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lfdj;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 1550
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfba;->a(I)I

    move-result v0

    .line 1551
    invoke-direct {p0}, Lfba;->ax()Z

    move-result v1

    if-eqz v1, :cond_0

    if-lt p1, v0, :cond_0

    .line 1552
    sub-int v0, p1, v0

    .line 1553
    iget-object v1, p0, Lfba;->a:Levi;

    invoke-virtual {v1, v0}, Levi;->getItemId(I)J

    move-result-wide v0

    .line 1555
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lfdj;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 1597
    sget-object v0, Leet;->N:Llpa;

    .line 1598
    sget v0, Lfba;->l:I

    add-int/lit8 v0, v0, 0x19

    add-int/lit8 v0, v0, 0x5

    return v0
.end method

.method public h()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1022
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    if-nez v0, :cond_0

    .line 1093
    :goto_0
    return-void

    .line 1027
    :cond_0
    iget-boolean v0, p0, Lfba;->N:Z

    if-eqz v0, :cond_8

    .line 1028
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lfba;->l(I)Landroid/database/Cursor;

    move-result-object v0

    .line 1029
    if-eqz v0, :cond_6

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_6

    .line 1030
    iget-boolean v0, p0, Lfba;->Y:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfba;->aw:Ljava/lang/String;

    .line 1031
    :goto_1
    iget v4, p0, Lfba;->f:I

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    move-object v0, v3

    .line 1056
    :goto_2
    if-eqz v0, :cond_5

    .line 1057
    iget-object v2, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-boolean v3, p0, Lfba;->aN:Z

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(ZZLjava/lang/String;)V

    goto :goto_0

    .line 1030
    :cond_1
    iget-object v0, p0, Lfba;->ax:Ljava/lang/String;

    goto :goto_1

    .line 1033
    :pswitch_1
    iget-boolean v4, p0, Lfba;->aK:Z

    if-eqz v4, :cond_3

    .line 1034
    iget-boolean v4, p0, Lfba;->M:Z

    if-eqz v4, :cond_2

    .line 1035
    const v4, 0x7f0a02e7

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Lfba;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1037
    :cond_2
    const v4, 0x7f0a02e6

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Lfba;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v3

    .line 1042
    goto :goto_2

    .line 1045
    :pswitch_2
    iget-boolean v4, p0, Lfba;->M:Z

    if-eqz v4, :cond_4

    .line 1046
    const v4, 0x7f0a02e8

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Lfba;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1048
    :cond_4
    const v4, 0x7f0a02ea

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Lfba;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1059
    :cond_5
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0, v2, v2, v3}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(ZZLjava/lang/String;)V

    goto :goto_0

    .line 1066
    :cond_6
    if-nez v0, :cond_7

    .line 1067
    iget v0, p0, Lfba;->f:I

    packed-switch v0, :pswitch_data_1

    :cond_7
    move v0, v2

    move-object v1, v3

    .line 1072
    :goto_3
    iget-object v3, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v3, v0, v2, v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(ZZLjava/lang/String;)V

    goto :goto_0

    .line 1071
    :pswitch_3
    const v0, 0x7f0a057b

    invoke-virtual {p0, v0}, Lfba;->d(I)Ljava/lang/String;

    move-result-object v0

    move v6, v1

    move-object v1, v0

    move v0, v6

    goto :goto_3

    .line 1083
    :cond_8
    iget v0, p0, Lfba;->f:I

    packed-switch v0, :pswitch_data_2

    .line 1089
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0, v2, v2, v3}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(ZZLjava/lang/String;)V

    goto/16 :goto_0

    .line 1085
    :pswitch_4
    const v0, 0x7f0a02df

    new-array v3, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lfba;->aw:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-virtual {p0, v0, v3}, Lfba;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1086
    iget-object v3, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v3, v1, v2, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(ZZLjava/lang/String;)V

    goto/16 :goto_0

    .line 1031
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 1067
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
    .end packed-switch

    .line 1083
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 1606
    invoke-direct {p0}, Lfba;->ax()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1607
    iget-object v0, p0, Lfba;->a:Levi;

    invoke-virtual {v0}, Levi;->isEmpty()Z

    move-result v0

    .line 1609
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lfdj;->isEmpty()Z

    move-result v0

    goto :goto_0
.end method

.method public j()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1096
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    if-nez v0, :cond_1

    .line 1139
    :cond_0
    :goto_0
    return-void

    .line 1101
    :cond_1
    iget-boolean v0, p0, Lfba;->ag:Z

    if-eqz v0, :cond_5

    .line 1102
    invoke-virtual {p0}, Lfba;->M()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1103
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {p0}, Lfba;->N()Ljava/lang/String;

    move-result-object v1

    .line 1104
    invoke-virtual {p0}, Lfba;->P()Lnjp;

    move-result-object v2

    invoke-virtual {p0}, Lfba;->H()I

    move-result v3

    .line 1105
    invoke-virtual {p0}, Lfba;->Q()Z

    move-result v4

    invoke-virtual {p0}, Lfba;->F()Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v6

    .line 1103
    :goto_1
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;Lnjp;IZZ)V

    move v0, v6

    .line 1114
    :goto_2
    if-eqz v0, :cond_6

    .line 1115
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget v1, p0, Lfba;->aB:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->b(I)V

    goto :goto_0

    :cond_2
    move v5, v7

    .line 1105
    goto :goto_1

    .line 1107
    :cond_3
    iget-object v0, p0, Lfba;->aj:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 1108
    iget-object v1, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {p0}, Lfba;->I()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lfba;->aj:Ljava/lang/String;

    .line 1109
    invoke-virtual {p0}, Lfba;->Q()Z

    move-result v4

    .line 1110
    invoke-virtual {p0}, Lfba;->F()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v6

    .line 1108
    :goto_3
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    move v0, v6

    .line 1111
    goto :goto_2

    :cond_4
    move v0, v7

    .line 1110
    goto :goto_3

    :cond_5
    move v0, v7

    .line 1120
    :cond_6
    iget-boolean v1, p0, Lfba;->af:Z

    if-eqz v1, :cond_8

    .line 1121
    iget-object v1, p0, Lfba;->al:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 1122
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {p0}, Lfba;->N()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lfba;->au:I

    .line 1123
    invoke-virtual {p0}, Lfba;->O()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lfba;->F()Z

    move-result v4

    if-nez v4, :cond_7

    move v7, v6

    .line 1122
    :cond_7
    invoke-virtual {v0, v1, v2, v3, v7}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 1132
    :goto_4
    if-nez v6, :cond_0

    .line 1137
    :cond_8
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->d()V

    .line 1138
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget v1, p0, Lfba;->aB:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->b(I)V

    goto :goto_0

    .line 1125
    :cond_9
    iget-object v1, p0, Lfba;->aj:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 1126
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {p0}, Lfba;->I()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lfba;->aj:Ljava/lang/String;

    iget v3, p0, Lfba;->au:I

    .line 1128
    invoke-virtual {p0}, Lfba;->F()Z

    move-result v4

    if-nez v4, :cond_a

    move v7, v6

    .line 1126
    :cond_a
    invoke-virtual {v0, v1, v2, v3, v7}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_4

    :cond_b
    move v6, v0

    goto :goto_4

    :cond_c
    move v0, v7

    goto :goto_2
.end method

.method public k()V
    .locals 2

    .prologue
    .line 1142
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    if-eqz v0, :cond_0

    .line 1143
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->b(Ljava/lang/String;)V

    .line 1145
    :cond_0
    return-void
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 1858
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1862
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->c:Lnjw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->c:Lnjw;

    iget-object v0, v0, Lnjw;->c:Ljava/lang/Boolean;

    .line 1865
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1862
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1865
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1869
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->c:Lnjw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->c:Lnjw;

    iget-object v0, v0, Lnjw;->a:Ljava/lang/Boolean;

    .line 1872
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1869
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1872
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 1528
    invoke-super {p0}, Lfdj;->notifyDataSetChanged()V

    .line 1529
    invoke-direct {p0}, Lfba;->ax()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1530
    const/4 v0, 0x1

    iget v1, p0, Lfba;->aR:I

    invoke-virtual {p0, v0, v1}, Lfba;->a(ZI)V

    .line 1532
    :cond_0
    return-void
.end method

.method public o()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1876
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->c:Lnjw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->c:Lnjw;

    iget-object v0, v0, Lnjw;->d:Ljava/lang/Boolean;

    .line 1879
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1876
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1879
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1883
    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->c:Lnjw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    iget-object v0, v0, Lnjt;->c:Lnia;

    iget-object v0, v0, Lnia;->c:Lnjw;

    iget-object v0, v0, Lnjw;->b:Ljava/lang/Boolean;

    .line 1886
    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->T:Lmdp;

    .line 1887
    invoke-static {v0}, Ldks;->b(Lmdp;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1883
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1887
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()V
    .locals 1

    .prologue
    .line 1902
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfba;->O:Z

    .line 1903
    invoke-virtual {p0}, Lfba;->s()V

    .line 1904
    invoke-virtual {p0}, Lfba;->r()V

    .line 1905
    return-void
.end method

.method public r()V
    .locals 2

    .prologue
    .line 1919
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    if-nez v0, :cond_0

    .line 1934
    :goto_0
    return-void

    .line 1923
    :cond_0
    iget-boolean v0, p0, Lfba;->aM:Z

    if-eqz v0, :cond_1

    .line 1924
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-object v1, p0, Lfba;->aP:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 1925
    :cond_1
    iget-boolean v0, p0, Lfba;->aN:Z

    if-eqz v0, :cond_2

    .line 1926
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    iget-boolean v1, p0, Lfba;->Y:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OneProfileHeader;->d(Z)V

    goto :goto_0

    .line 1927
    :cond_2
    iget-boolean v0, p0, Lfba;->aO:Z

    if-eqz v0, :cond_3

    .line 1928
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->f()V

    goto :goto_0

    .line 1929
    :cond_3
    iget-boolean v0, p0, Lfba;->ad:Z

    if-eqz v0, :cond_4

    .line 1930
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->e()V

    goto :goto_0

    .line 1932
    :cond_4
    iget-object v0, p0, Lfba;->c:Lcom/google/android/apps/plus/views/OneProfileHeader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OneProfileHeader;->g()V

    goto :goto_0
.end method

.method public s()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1942
    invoke-static {}, Llsx;->b()V

    .line 1943
    iget-boolean v0, p0, Lfba;->M:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lfba;->ab:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lfba;->Q:Lnjt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfba;->aL:Lhxh;

    .line 1946
    invoke-virtual {v0}, Lhxh;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1947
    :cond_0
    iput-boolean v1, p0, Lfba;->aO:Z

    iput-boolean v1, p0, Lfba;->aN:Z

    iput-boolean v1, p0, Lfba;->aM:Z

    .line 1961
    :goto_0
    invoke-virtual {p0}, Lfba;->notifyDataSetChanged()V

    .line 1962
    return-void

    .line 1948
    :cond_1
    iget-boolean v0, p0, Lfba;->O:Z

    if-eqz v0, :cond_2

    .line 1949
    iput-boolean v1, p0, Lfba;->aN:Z

    iput-boolean v1, p0, Lfba;->aM:Z

    .line 1950
    iput-boolean v2, p0, Lfba;->aO:Z

    goto :goto_0

    .line 1951
    :cond_2
    iget-boolean v0, p0, Lfba;->ad:Z

    if-eqz v0, :cond_3

    .line 1952
    iput-boolean v1, p0, Lfba;->aO:Z

    iput-boolean v1, p0, Lfba;->aN:Z

    iput-boolean v1, p0, Lfba;->aM:Z

    goto :goto_0

    .line 1953
    :cond_3
    iget-object v0, p0, Lfba;->ac:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1954
    iput-boolean v1, p0, Lfba;->aO:Z

    iput-boolean v1, p0, Lfba;->aM:Z

    .line 1955
    iput-boolean v2, p0, Lfba;->aN:Z

    goto :goto_0

    .line 1957
    :cond_4
    iput-boolean v1, p0, Lfba;->aO:Z

    iput-boolean v1, p0, Lfba;->aN:Z

    .line 1958
    iput-boolean v2, p0, Lfba;->aM:Z

    .line 1959
    iget-object v0, p0, Lfba;->aL:Lhxh;

    iget-object v1, p0, Lfba;->ac:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhxh;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lfba;->aP:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public t()Z
    .locals 1

    .prologue
    .line 1965
    iget-boolean v0, p0, Lfba;->Y:Z

    return v0
.end method

.method public u()Z
    .locals 1

    .prologue
    .line 1969
    iget-boolean v0, p0, Lfba;->Z:Z

    return v0
.end method

.method public v()Z
    .locals 1

    .prologue
    .line 1973
    iget-boolean v0, p0, Lfba;->aa:Z

    return v0
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 1981
    iget-boolean v0, p0, Lfba;->ad:Z

    return v0
.end method

.method public x()Z
    .locals 1

    .prologue
    .line 1985
    iget-boolean v0, p0, Lfba;->ae:Z

    return v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1989
    iget-object v0, p0, Lfba;->aw:Ljava/lang/String;

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1993
    iget-object v0, p0, Lfba;->ax:Ljava/lang/String;

    return-object v0
.end method
