.class public final Lpyf;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Integer;

.field private d:I

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput v0, p0, Lpyf;->a:I

    .line 20
    iput v0, p0, Lpyf;->d:I

    .line 25
    iput v0, p0, Lpyf;->f:I

    .line 28
    iput v0, p0, Lpyf;->g:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 70
    const/4 v0, 0x0

    .line 71
    iget-object v1, p0, Lpyf;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 72
    const/4 v0, 0x1

    iget-object v1, p0, Lpyf;->b:Ljava/lang/Long;

    .line 73
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 75
    :cond_0
    iget-object v1, p0, Lpyf;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 76
    const/4 v1, 0x2

    iget-object v2, p0, Lpyf;->c:Ljava/lang/Integer;

    .line 77
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_1
    iget v1, p0, Lpyf;->d:I

    if-eq v1, v3, :cond_2

    .line 80
    const/4 v1, 0x3

    iget v2, p0, Lpyf;->d:I

    .line 81
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 83
    :cond_2
    iget v1, p0, Lpyf;->a:I

    if-eq v1, v3, :cond_3

    .line 84
    const/4 v1, 0x4

    iget v2, p0, Lpyf;->a:I

    .line 85
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_3
    iget-object v1, p0, Lpyf;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 88
    const/4 v1, 0x5

    iget-object v2, p0, Lpyf;->e:Ljava/lang/String;

    .line 89
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_4
    iget v1, p0, Lpyf;->f:I

    if-eq v1, v3, :cond_5

    .line 92
    const/4 v1, 0x6

    iget v2, p0, Lpyf;->f:I

    .line 93
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_5
    iget v1, p0, Lpyf;->g:I

    if-eq v1, v3, :cond_6

    .line 96
    const/4 v1, 0x7

    iget v2, p0, Lpyf;->g:I

    .line 97
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_6
    iget-object v1, p0, Lpyf;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 100
    const/16 v1, 0x8

    iget-object v2, p0, Lpyf;->h:Ljava/lang/String;

    .line 101
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_7
    iget-object v1, p0, Lpyf;->i:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 104
    const/16 v1, 0x9

    iget-object v2, p0, Lpyf;->i:Ljava/lang/Long;

    .line 105
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 107
    :cond_8
    iget-object v1, p0, Lpyf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    iput v0, p0, Lpyf;->ai:I

    .line 109
    return v0
.end method

.method public a(Loxn;)Lpyf;
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 117
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 118
    sparse-switch v0, :sswitch_data_0

    .line 122
    iget-object v1, p0, Lpyf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpyf;->ah:Ljava/util/List;

    .line 126
    :cond_1
    iget-object v1, p0, Lpyf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    :sswitch_0
    return-object p0

    .line 133
    :sswitch_1
    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpyf;->b:Ljava/lang/Long;

    goto :goto_0

    .line 137
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpyf;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 141
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 142
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2328

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x24

    if-eq v0, v1, :cond_2

    const/16 v1, 0x25

    if-eq v0, v1, :cond_2

    if-eqz v0, :cond_2

    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-eq v0, v1, :cond_2

    const/16 v1, 0x17

    if-eq v0, v1, :cond_2

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x20

    if-eq v0, v1, :cond_2

    const/16 v1, 0x21

    if-eq v0, v1, :cond_2

    const/16 v1, 0x22

    if-eq v0, v1, :cond_2

    const/16 v1, 0x23

    if-eq v0, v1, :cond_2

    const/16 v1, 0x72

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xce

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd5

    if-eq v0, v1, :cond_2

    const/16 v1, 0x133

    if-eq v0, v1, :cond_2

    const/16 v1, 0x130

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x191

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1fb

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1fc

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1fd

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1f4

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1f5

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1f6

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1f7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8fc

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8fe

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2bd

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2be

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2bf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x320

    if-eq v0, v1, :cond_2

    const/16 v1, 0x321

    if-eq v0, v1, :cond_2

    const/16 v1, 0x384

    if-eq v0, v1, :cond_2

    const/16 v1, 0x385

    if-eq v0, v1, :cond_2

    const/16 v1, 0x386

    if-eq v0, v1, :cond_2

    const/16 v1, 0x387

    if-eq v0, v1, :cond_2

    const/16 v1, 0x389

    if-eq v0, v1, :cond_2

    const/16 v1, 0x38a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x38b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x38c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x38d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x38e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3e9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ea

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ec

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ed

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ef

    if-eq v0, v1, :cond_2

    const/16 v1, 0x89f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4b8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4c2

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4c3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x137

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x131

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4b2

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4b6

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4b3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x4b5

    if-eq v0, v1, :cond_2

    const/16 v1, 0x518

    if-eq v0, v1, :cond_2

    const/16 v1, 0x517

    if-eq v0, v1, :cond_2

    const/16 v1, 0x51e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x51f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x520

    if-eq v0, v1, :cond_2

    const/16 v1, 0x521

    if-eq v0, v1, :cond_2

    const/16 v1, 0x522

    if-eq v0, v1, :cond_2

    const/16 v1, 0x523

    if-eq v0, v1, :cond_2

    const/16 v1, 0x524

    if-eq v0, v1, :cond_2

    const/16 v1, 0x525

    if-eq v0, v1, :cond_2

    const/16 v1, 0x57c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x57d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x57e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x57f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x580

    if-eq v0, v1, :cond_2

    const/16 v1, 0x581

    if-eq v0, v1, :cond_2

    const/16 v1, 0x582

    if-eq v0, v1, :cond_2

    const/16 v1, 0x583

    if-eq v0, v1, :cond_2

    const/16 v1, 0x584

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5dc

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5dd

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5de

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5e0

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5e2

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5e3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5e7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5e8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5e9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5ea

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5eb

    if-eq v0, v1, :cond_2

    const/16 v1, 0x642

    if-eq v0, v1, :cond_2

    const/16 v1, 0x640

    if-eq v0, v1, :cond_2

    const/16 v1, 0x641

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a5

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6aa

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6ab

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6ac

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6ad

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6ae

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6af

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b0

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b1

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b2

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2711

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2712

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2713

    if-eq v0, v1, :cond_2

    const/16 v1, 0x271b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x283c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x283d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x283e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x283f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2840

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2841

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2842

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2969

    if-eq v0, v1, :cond_2

    const/16 v1, 0x296b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x296c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x296d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x296e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2972

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2973

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2974

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2975

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2976

    if-eq v0, v1, :cond_2

    const/16 v1, 0x835

    if-eq v0, v1, :cond_2

    const/16 v1, 0x836

    if-eq v0, v1, :cond_2

    const/16 v1, 0x837

    if-eq v0, v1, :cond_2

    const/16 v1, 0x838

    if-eq v0, v1, :cond_2

    const/16 v1, 0x839

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x843

    if-eq v0, v1, :cond_2

    const/16 v1, 0x845

    if-eq v0, v1, :cond_2

    const/16 v1, 0x846

    if-eq v0, v1, :cond_2

    const/16 v1, 0x847

    if-eq v0, v1, :cond_2

    const/16 v1, 0x848

    if-eq v0, v1, :cond_2

    const/16 v1, 0x849

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x850

    if-eq v0, v1, :cond_2

    const/16 v1, 0x853

    if-eq v0, v1, :cond_2

    const/16 v1, 0x854

    if-eq v0, v1, :cond_2

    const/16 v1, 0x856

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x840

    if-eq v0, v1, :cond_2

    const/16 v1, 0x841

    if-eq v0, v1, :cond_2

    const/16 v1, 0x842

    if-eq v0, v1, :cond_2

    const/16 v1, 0x851

    if-eq v0, v1, :cond_2

    const/16 v1, 0x899

    if-eq v0, v1, :cond_2

    const/16 v1, 0x190

    if-eq v0, v1, :cond_2

    const/16 v1, 0x516

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2714

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2715

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2716

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2717

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2718

    if-eq v0, v1, :cond_2

    const/16 v1, 0x2719

    if-eq v0, v1, :cond_2

    const/16 v1, 0x271a

    if-eq v0, v1, :cond_2

    const/16 v1, 0xca

    if-eq v0, v1, :cond_2

    const/16 v1, 0xdc

    if-eq v0, v1, :cond_2

    const/16 v1, 0x139

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x296f

    if-ne v0, v1, :cond_3

    .line 352
    :cond_2
    iput v0, p0, Lpyf;->d:I

    goto/16 :goto_0

    .line 354
    :cond_3
    const/4 v0, -0x1

    iput v0, p0, Lpyf;->d:I

    goto/16 :goto_0

    .line 359
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 360
    if-eqz v0, :cond_4

    if-eq v0, v2, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 369
    :cond_4
    iput v0, p0, Lpyf;->a:I

    goto/16 :goto_0

    .line 371
    :cond_5
    const/4 v0, 0x0

    iput v0, p0, Lpyf;->a:I

    goto/16 :goto_0

    .line 376
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyf;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 380
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 381
    if-eq v0, v2, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-eq v0, v6, :cond_6

    const/4 v1, 0x6

    if-eq v0, v1, :cond_6

    const/4 v1, 0x7

    if-eq v0, v1, :cond_6

    const/16 v1, 0x8

    if-eq v0, v1, :cond_6

    const/16 v1, 0x9

    if-eq v0, v1, :cond_6

    const/16 v1, 0xa

    if-eq v0, v1, :cond_6

    const/16 v1, 0xb

    if-eq v0, v1, :cond_6

    const/16 v1, 0xc

    if-eq v0, v1, :cond_6

    const/16 v1, 0xd

    if-eq v0, v1, :cond_6

    const/16 v1, 0xe

    if-eq v0, v1, :cond_6

    const/16 v1, 0xf

    if-eq v0, v1, :cond_6

    const/16 v1, 0x10

    if-eq v0, v1, :cond_6

    const/16 v1, 0x11

    if-eq v0, v1, :cond_6

    const/16 v1, 0x12

    if-eq v0, v1, :cond_6

    const/16 v1, 0x13

    if-eq v0, v1, :cond_6

    const/16 v1, 0x14

    if-eq v0, v1, :cond_6

    const/16 v1, 0x16

    if-eq v0, v1, :cond_6

    const/16 v1, 0x17

    if-eq v0, v1, :cond_6

    const/16 v1, 0x18

    if-eq v0, v1, :cond_6

    const/16 v1, 0x19

    if-eq v0, v1, :cond_6

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_6

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_6

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_6

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_6

    const/16 v1, 0x1f

    if-eq v0, v1, :cond_6

    const/16 v1, 0x20

    if-eq v0, v1, :cond_6

    const/16 v1, 0x21

    if-eq v0, v1, :cond_6

    const/16 v1, 0x22

    if-eq v0, v1, :cond_6

    const/16 v1, 0x23

    if-eq v0, v1, :cond_6

    const/16 v1, 0x24

    if-eq v0, v1, :cond_6

    const/16 v1, 0x25

    if-eq v0, v1, :cond_6

    const/16 v1, 0x26

    if-eq v0, v1, :cond_6

    const/16 v1, 0x27

    if-eq v0, v1, :cond_6

    const/16 v1, 0x28

    if-eq v0, v1, :cond_6

    const/16 v1, 0x1e

    if-eq v0, v1, :cond_6

    const/16 v1, 0x15

    if-ne v0, v1, :cond_7

    .line 421
    :cond_6
    iput v0, p0, Lpyf;->f:I

    goto/16 :goto_0

    .line 423
    :cond_7
    iput v2, p0, Lpyf;->f:I

    goto/16 :goto_0

    .line 428
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 429
    if-eqz v0, :cond_8

    if-eq v0, v2, :cond_8

    if-eq v0, v3, :cond_8

    if-eq v0, v4, :cond_8

    if-eq v0, v5, :cond_8

    if-eq v0, v6, :cond_8

    const/4 v1, 0x6

    if-eq v0, v1, :cond_8

    const/4 v1, 0x7

    if-eq v0, v1, :cond_8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_8

    const/16 v1, 0x9

    if-eq v0, v1, :cond_8

    const/16 v1, 0xa

    if-eq v0, v1, :cond_8

    const/16 v1, 0xb

    if-eq v0, v1, :cond_8

    const/16 v1, 0xc

    if-eq v0, v1, :cond_8

    const/16 v1, 0xd

    if-eq v0, v1, :cond_8

    const/16 v1, 0xe

    if-eq v0, v1, :cond_8

    const/16 v1, 0xf

    if-eq v0, v1, :cond_8

    const/16 v1, 0x10

    if-eq v0, v1, :cond_8

    const/16 v1, 0x11

    if-eq v0, v1, :cond_8

    const/16 v1, 0x12

    if-eq v0, v1, :cond_8

    const/16 v1, 0x13

    if-eq v0, v1, :cond_8

    const/16 v1, 0x14

    if-eq v0, v1, :cond_8

    const/16 v1, 0x15

    if-eq v0, v1, :cond_8

    const/16 v1, 0x16

    if-eq v0, v1, :cond_8

    const/16 v1, 0x17

    if-eq v0, v1, :cond_8

    const/16 v1, 0x18

    if-eq v0, v1, :cond_8

    const/16 v1, 0x19

    if-eq v0, v1, :cond_8

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_9

    .line 456
    :cond_8
    iput v0, p0, Lpyf;->g:I

    goto/16 :goto_0

    .line 458
    :cond_9
    const/4 v0, 0x0

    iput v0, p0, Lpyf;->g:I

    goto/16 :goto_0

    .line 463
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpyf;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 467
    :sswitch_9
    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpyf;->i:Ljava/lang/Long;

    goto/16 :goto_0

    .line 118
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x49 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 37
    iget-object v0, p0, Lpyf;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    iget-object v1, p0, Lpyf;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->c(IJ)V

    .line 40
    :cond_0
    iget-object v0, p0, Lpyf;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 41
    const/4 v0, 0x2

    iget-object v1, p0, Lpyf;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 43
    :cond_1
    iget v0, p0, Lpyf;->d:I

    if-eq v0, v4, :cond_2

    .line 44
    const/4 v0, 0x3

    iget v1, p0, Lpyf;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 46
    :cond_2
    iget v0, p0, Lpyf;->a:I

    if-eq v0, v4, :cond_3

    .line 47
    const/4 v0, 0x4

    iget v1, p0, Lpyf;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 49
    :cond_3
    iget-object v0, p0, Lpyf;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 50
    const/4 v0, 0x5

    iget-object v1, p0, Lpyf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 52
    :cond_4
    iget v0, p0, Lpyf;->f:I

    if-eq v0, v4, :cond_5

    .line 53
    const/4 v0, 0x6

    iget v1, p0, Lpyf;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 55
    :cond_5
    iget v0, p0, Lpyf;->g:I

    if-eq v0, v4, :cond_6

    .line 56
    const/4 v0, 0x7

    iget v1, p0, Lpyf;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 58
    :cond_6
    iget-object v0, p0, Lpyf;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 59
    const/16 v0, 0x8

    iget-object v1, p0, Lpyf;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 61
    :cond_7
    iget-object v0, p0, Lpyf;->i:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 62
    const/16 v0, 0x9

    iget-object v1, p0, Lpyf;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->c(IJ)V

    .line 64
    :cond_8
    iget-object v0, p0, Lpyf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 66
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpyf;->a(Loxn;)Lpyf;

    move-result-object v0

    return-object v0
.end method
