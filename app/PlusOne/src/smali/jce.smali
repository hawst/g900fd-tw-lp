.class public final Ljce;
.super Lmn;
.source "PG"


# instance fields
.field final synthetic e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 539
    iput-object p1, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    .line 540
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Lmn;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 541
    return-void
.end method

.method static synthetic a(Ljce;)Z
    .locals 3

    .prologue
    .line 537
    iget-object v0, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-static {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->d(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Ljbt;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    iget-object v1, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-static {v1}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->e(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Llnl;

    move-result-object v1

    const-class v2, Lhee;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    sget-object v2, Ljcb;->a:Lief;

    invoke-interface {v0, v2, v1}, Lieh;->b(Lief;I)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Ljce;)V
    .locals 4

    .prologue
    .line 537
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "com.google.android.apps.plus"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "*/*"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "android.intent.category.OPENABLE"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x80000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "account_id"

    iget-object v0, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-static {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->c(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Llnl;

    move-result-object v0

    const-class v3, Lhee;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    iget-object v2, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a03b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 568
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 569
    invoke-virtual {p0, p2}, Ljce;->d(Landroid/database/Cursor;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 582
    :pswitch_0
    new-instance v0, Ljbu;

    invoke-direct {v0, p1}, Ljbu;-><init>(Landroid/content/Context;)V

    .line 583
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljbu;->n(Z)V

    .line 584
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljbu;->b(I)V

    .line 585
    iget-object v1, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020584

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljbu;->e(Landroid/graphics/drawable/Drawable;)V

    .line 587
    iget-object v1, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    .line 588
    invoke-virtual {v1}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->o()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Llhq;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 587
    invoke-virtual {v0, v1}, Ljbu;->d(Landroid/graphics/drawable/Drawable;)V

    .line 589
    :goto_0
    return-object v0

    .line 571
    :pswitch_1
    const v1, 0x7f04010a

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 573
    :pswitch_2
    const v1, 0x7f04010b

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 575
    iget-object v0, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-static {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Ljbp;

    move-result-object v0

    if-nez v0, :cond_0

    .line 576
    iget-object v2, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    new-instance v3, Ljbp;

    iget-object v0, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    .line 577
    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v4

    const v0, 0x7f100371

    .line 578
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    invoke-direct {v3, v4, v0}, Ljbp;-><init>(Landroid/app/Activity;Landroid/view/TextureView;)V

    .line 576
    invoke-static {v2, v3}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->a(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;Ljbp;)Ljbp;

    :cond_0
    move-object v0, v1

    .line 580
    goto :goto_0

    .line 569
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(I)V
    .locals 2

    .prologue
    .line 656
    iget-object v0, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/view/View;->performHapticFeedback(II)Z

    .line 658
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 685
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Ljce;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 724
    :pswitch_0
    const-string v0, "_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v2

    :goto_0
    if-ne v1, v2, :cond_1

    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    :goto_1
    invoke-static {v0, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Ljbv;

    packed-switch v1, :pswitch_data_1

    :pswitch_1
    sget-object v0, Ljac;->a:Ljac;

    :goto_2
    invoke-direct {v4, p2, v3, v0}, Ljbv;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljac;)V

    move-object v0, p1

    check-cast v0, Ljbu;

    invoke-interface {v4}, Lizr;->f()Lizu;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljbu;->a(Lizu;)V

    iget-object v1, p0, Ljce;->e:Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;

    invoke-static {v1}, Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;->b(Lcom/google/android/libraries/social/mediapicker/MediaPickerFragment;)Ljbx;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljbx;->a(Lizr;)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljbu;->a(Z)V

    invoke-virtual {v0, v4}, Ljbu;->setTag(Ljava/lang/Object;)V

    new-instance v1, Ljcf;

    invoke-direct {v1, p0}, Ljcf;-><init>(Ljce;)V

    invoke-virtual {v0, v1}, Ljbu;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Ljcg;

    invoke-direct {v1, p0}, Ljcg;-><init>(Ljce;)V

    invoke-virtual {v0, v1}, Ljbu;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 728
    :goto_3
    invoke-virtual {p1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 729
    invoke-virtual {p1, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 730
    return-void

    .line 687
    :pswitch_2
    new-instance v0, Ljch;

    invoke-direct {v0, p0}, Ljch;-><init>(Ljce;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 707
    :pswitch_3
    new-instance v0, Ljci;

    invoke-direct {v0, p0}, Ljci;-><init>(Ljce;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 724
    :cond_0
    const-string v0, "media_type"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v0, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    goto :goto_1

    :pswitch_4
    sget-object v0, Ljac;->a:Ljac;

    goto :goto_2

    :pswitch_5
    sget-object v0, Ljac;->b:Ljac;

    goto :goto_2

    .line 685
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 724
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method

.method public d(Landroid/database/Cursor;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 555
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 556
    const-wide/16 v4, -0x65

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 561
    :goto_0
    return v0

    .line 558
    :cond_0
    const-wide/16 v0, -0x66

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    .line 559
    const/4 v0, 0x2

    goto :goto_0

    .line 561
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Ljce;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 551
    iget-object v0, p0, Ljce;->b:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Ljce;->d(Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 545
    const/4 v0, 0x3

    return v0
.end method
