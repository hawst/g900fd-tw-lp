.class final Lflt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkhq;


# instance fields
.field private synthetic a:Lflp;


# direct methods
.method constructor <init>(Lflp;)V
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lflt;->a:Lflp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lkhl;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 326
    invoke-virtual {p1}, Lkhl;->u()Ljava/lang/String;

    move-result-object v2

    .line 327
    check-cast p1, Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d()Z

    move-result v3

    .line 328
    new-instance v4, Lnyq;

    invoke-direct {v4}, Lnyq;-><init>()V

    .line 331
    iget-object v5, p0, Lflt;->a:Lflp;

    invoke-static {v5}, Lflp;->b(Lflp;)Lhee;

    move-result-object v5

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    .line 333
    const-string v6, "auto_enhance"

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 334
    iget-object v2, p0, Lflt;->a:Lflp;

    invoke-static {v2}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v2

    iget-object v2, v2, Lnyq;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v2, v3, :cond_2

    .line 335
    :goto_0
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnyq;->h:Ljava/lang/Boolean;

    .line 336
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    iget-object v1, v1, Lnyq;->h:Ljava/lang/Boolean;

    iput-object v1, v4, Lnyq;->h:Ljava/lang/Boolean;

    .line 361
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 362
    new-instance v1, Lfma;

    iget-object v2, p0, Lflt;->a:Lflp;

    invoke-static {v2}, Lflp;->k(Lflp;)Llnl;

    move-result-object v2

    invoke-direct {v1, v2, v5, v4}, Lfma;-><init>(Landroid/content/Context;ILnyq;)V

    .line 363
    iget-object v2, p0, Lflt;->a:Lflp;

    invoke-static {v2}, Lflp;->l(Lflp;)Lhoc;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhoc;->c(Lhny;)V

    .line 365
    :cond_1
    :goto_2
    return v0

    :cond_2
    move v0, v1

    .line 334
    goto :goto_0

    .line 337
    :cond_3
    const-string v6, "auto_awesome"

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 338
    iget-object v2, p0, Lflt;->a:Lflp;

    invoke-static {v2}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v2

    iget-object v2, v2, Lnyq;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v2, v3, :cond_4

    .line 339
    :goto_3
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnyq;->i:Ljava/lang/Boolean;

    .line 340
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    iget-object v1, v1, Lnyq;->i:Ljava/lang/Boolean;

    iput-object v1, v4, Lnyq;->i:Ljava/lang/Boolean;

    .line 341
    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    .line 342
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->i(Lflp;)Llnl;

    move-result-object v1

    invoke-static {v1, v5, v3}, Ldhv;->g(Landroid/content/Context;IZ)V

    goto :goto_1

    :cond_4
    move v0, v1

    .line 338
    goto :goto_3

    .line 344
    :cond_5
    const-string v6, "google_drive"

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 345
    iget-object v2, p0, Lflt;->a:Lflp;

    invoke-static {v2}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v2

    iget-object v2, v2, Lnyq;->f:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v2, v3, :cond_6

    .line 346
    :goto_4
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnyq;->f:Ljava/lang/Boolean;

    .line 347
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    iget-object v1, v1, Lnyq;->f:Ljava/lang/Boolean;

    iput-object v1, v4, Lnyq;->f:Ljava/lang/Boolean;

    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 345
    goto :goto_4

    .line 348
    :cond_7
    const-string v6, "photo_location"

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 349
    iget-object v2, p0, Lflt;->a:Lflp;

    invoke-static {v2}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v2

    iget-object v2, v2, Lnyq;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v2, v3, :cond_8

    .line 350
    :goto_5
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnyq;->a:Ljava/lang/Boolean;

    .line 351
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    iget-object v1, v1, Lnyq;->a:Ljava/lang/Boolean;

    iput-object v1, v4, Lnyq;->a:Ljava/lang/Boolean;

    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 349
    goto :goto_5

    .line 352
    :cond_9
    const-string v6, "find_my_face"

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 353
    iget-object v2, p0, Lflt;->a:Lflp;

    invoke-static {v2}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v2

    iget-object v2, v2, Lnyq;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eq v2, v3, :cond_a

    .line 354
    :goto_6
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnyq;->c:Ljava/lang/Boolean;

    .line 355
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->h(Lflp;)Lnyq;

    move-result-object v1

    iget-object v1, v1, Lnyq;->c:Ljava/lang/Boolean;

    iput-object v1, v4, Lnyq;->c:Ljava/lang/Boolean;

    goto/16 :goto_1

    :cond_a
    move v0, v1

    .line 353
    goto :goto_6

    .line 356
    :cond_b
    const-string v6, "auto_awesome_movies"

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 357
    iget-object v1, p0, Lflt;->a:Lflp;

    invoke-static {v1}, Lflp;->j(Lflp;)Llnl;

    move-result-object v1

    invoke-static {v1, v5, v3}, Ldhv;->g(Landroid/content/Context;IZ)V

    goto/16 :goto_2

    :cond_c
    move v0, v1

    goto/16 :goto_1
.end method
