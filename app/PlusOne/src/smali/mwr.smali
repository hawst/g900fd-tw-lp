.class public final Lmwr;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmwr;


# instance fields
.field private b:I

.field private c:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5814
    const/4 v0, 0x0

    new-array v0, v0, [Lmwr;

    sput-object v0, Lmwr;->a:[Lmwr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5815
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5818
    const/high16 v0, -0x80000000

    iput v0, p0, Lmwr;->b:I

    .line 5815
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5837
    const/4 v0, 0x0

    .line 5838
    iget v1, p0, Lmwr;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 5839
    const/4 v0, 0x1

    iget v1, p0, Lmwr;->b:I

    .line 5840
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5842
    :cond_0
    iget-object v1, p0, Lmwr;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 5843
    const/4 v1, 0x2

    iget-object v2, p0, Lmwr;->c:Ljava/lang/Float;

    .line 5844
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 5846
    :cond_1
    iget-object v1, p0, Lmwr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5847
    iput v0, p0, Lmwr;->ai:I

    .line 5848
    return v0
.end method

.method public a(Loxn;)Lmwr;
    .locals 2

    .prologue
    .line 5856
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5857
    sparse-switch v0, :sswitch_data_0

    .line 5861
    iget-object v1, p0, Lmwr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5862
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwr;->ah:Ljava/util/List;

    .line 5865
    :cond_1
    iget-object v1, p0, Lmwr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5867
    :sswitch_0
    return-object p0

    .line 5872
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5873
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x16

    if-eq v0, v1, :cond_2

    const/16 v1, 0x17

    if-eq v0, v1, :cond_2

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x15

    if-eq v0, v1, :cond_2

    const/16 v1, 0x19

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x1d

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-ne v0, v1, :cond_3

    .line 5903
    :cond_2
    iput v0, p0, Lmwr;->b:I

    goto/16 :goto_0

    .line 5905
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmwr;->b:I

    goto/16 :goto_0

    .line 5910
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmwr;->c:Ljava/lang/Float;

    goto/16 :goto_0

    .line 5857
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5825
    iget v0, p0, Lmwr;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 5826
    const/4 v0, 0x1

    iget v1, p0, Lmwr;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5828
    :cond_0
    iget-object v0, p0, Lmwr;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 5829
    const/4 v0, 0x2

    iget-object v1, p0, Lmwr;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 5831
    :cond_1
    iget-object v0, p0, Lmwr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5833
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5811
    invoke-virtual {p0, p1}, Lmwr;->a(Loxn;)Lmwr;

    move-result-object v0

    return-object v0
.end method
