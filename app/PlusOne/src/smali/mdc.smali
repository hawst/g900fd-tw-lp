.class public final Lmdc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnte;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17251
    invoke-direct {p0}, Loxq;-><init>()V

    .line 17254
    iput-object v0, p0, Lmdc;->apiHeader:Llyq;

    .line 17257
    iput-object v0, p0, Lmdc;->a:Lnte;

    .line 17251
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 17274
    const/4 v0, 0x0

    .line 17275
    iget-object v1, p0, Lmdc;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 17276
    const/4 v0, 0x1

    iget-object v1, p0, Lmdc;->apiHeader:Llyq;

    .line 17277
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17279
    :cond_0
    iget-object v1, p0, Lmdc;->a:Lnte;

    if-eqz v1, :cond_1

    .line 17280
    const/4 v1, 0x2

    iget-object v2, p0, Lmdc;->a:Lnte;

    .line 17281
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17283
    :cond_1
    iget-object v1, p0, Lmdc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17284
    iput v0, p0, Lmdc;->ai:I

    .line 17285
    return v0
.end method

.method public a(Loxn;)Lmdc;
    .locals 2

    .prologue
    .line 17293
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 17294
    sparse-switch v0, :sswitch_data_0

    .line 17298
    iget-object v1, p0, Lmdc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 17299
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdc;->ah:Ljava/util/List;

    .line 17302
    :cond_1
    iget-object v1, p0, Lmdc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 17304
    :sswitch_0
    return-object p0

    .line 17309
    :sswitch_1
    iget-object v0, p0, Lmdc;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 17310
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmdc;->apiHeader:Llyq;

    .line 17312
    :cond_2
    iget-object v0, p0, Lmdc;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17316
    :sswitch_2
    iget-object v0, p0, Lmdc;->a:Lnte;

    if-nez v0, :cond_3

    .line 17317
    new-instance v0, Lnte;

    invoke-direct {v0}, Lnte;-><init>()V

    iput-object v0, p0, Lmdc;->a:Lnte;

    .line 17319
    :cond_3
    iget-object v0, p0, Lmdc;->a:Lnte;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 17294
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 17262
    iget-object v0, p0, Lmdc;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 17263
    const/4 v0, 0x1

    iget-object v1, p0, Lmdc;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17265
    :cond_0
    iget-object v0, p0, Lmdc;->a:Lnte;

    if-eqz v0, :cond_1

    .line 17266
    const/4 v0, 0x2

    iget-object v1, p0, Lmdc;->a:Lnte;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 17268
    :cond_1
    iget-object v0, p0, Lmdc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 17270
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 17247
    invoke-virtual {p0, p1}, Lmdc;->a(Loxn;)Lmdc;

    move-result-object v0

    return-object v0
.end method
