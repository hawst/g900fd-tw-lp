.class public Ljgm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static volatile a:Lorg/chromium/net/HttpUrlRequestFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/nio/channels/WritableByteChannel;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")",
            "Lorg/chromium/net/HttpUrlRequest;"
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {p0}, Ljgm;->a(Landroid/content/Context;)Lorg/chromium/net/HttpUrlRequestFactory;

    move-result-object v0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lorg/chromium/net/HttpUrlRequestFactory;->b(Ljava/lang/String;ILjava/util/Map;Ljava/nio/channels/WritableByteChannel;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lorg/chromium/net/HttpUrlRequestListener;",
            ")",
            "Lorg/chromium/net/HttpUrlRequest;"
        }
    .end annotation

    .prologue
    .line 47
    invoke-static {p0}, Ljgm;->a(Landroid/content/Context;)Lorg/chromium/net/HttpUrlRequestFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/chromium/net/HttpUrlRequestFactory;->b(Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    return-object v0
.end method

.method private static declared-synchronized a(Landroid/content/Context;)Lorg/chromium/net/HttpUrlRequestFactory;
    .locals 3

    .prologue
    .line 26
    const-class v1, Ljgm;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljgm;->a:Lorg/chromium/net/HttpUrlRequestFactory;

    if-nez v0, :cond_2

    .line 27
    const-class v2, Ljgm;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 28
    :try_start_1
    sget-object v0, Ljgm;->a:Lorg/chromium/net/HttpUrlRequestFactory;

    if-nez v0, :cond_1

    .line 29
    const-class v0, Lorg/chromium/net/HttpUrlRequestFactory;

    invoke-static {p0, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/net/HttpUrlRequestFactory;

    .line 30
    if-nez v0, :cond_0

    .line 31
    const-class v0, Lorg/chromium/net/UrlRequestContextConfig;

    .line 32
    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/net/UrlRequestContextConfig;

    .line 31
    invoke-static {p0, v0}, Lorg/chromium/net/HttpUrlRequestFactory;->a(Landroid/content/Context;Lorg/chromium/net/UrlRequestContextConfig;)Lorg/chromium/net/HttpUrlRequestFactory;

    move-result-object v0

    .line 34
    :cond_0
    sput-object v0, Ljgm;->a:Lorg/chromium/net/HttpUrlRequestFactory;

    .line 36
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 38
    :cond_2
    :try_start_2
    sget-object v0, Ljgm;->a:Lorg/chromium/net/HttpUrlRequestFactory;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    monitor-exit v1

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 26
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
