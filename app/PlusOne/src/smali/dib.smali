.class public final Ldib;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lhms;

.field private final b:Landroid/content/Context;

.field private c:Landroid/os/Bundle;

.field private d:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lhms;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Ldib;->b:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Ldib;->a:Lhms;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lhms;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ldid;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lhms;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ldid;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 62
    invoke-direct {p0, p1, p2}, Ldib;-><init>(Landroid/content/Context;Lhms;)V

    .line 64
    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    if-eqz p6, :cond_2

    .line 67
    invoke-interface {p6}, Ldid;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 66
    invoke-static {v0}, Ldib;->a(Ljava/lang/Integer;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Ldib;->c:Landroid/os/Bundle;

    .line 68
    iget-object v0, p0, Ldib;->c:Landroid/os/Bundle;

    invoke-static {p6, v0}, Ldib;->a(Ldid;Landroid/os/Bundle;)V

    .line 72
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 73
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v1, p0, Ldib;->c:Landroid/os/Bundle;

    const-string v2, "extra_participant_ids"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 76
    iget-object v0, p0, Ldib;->c:Landroid/os/Bundle;

    const-string v1, "extra_circle_ids"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 79
    :cond_0
    if-eqz p5, :cond_1

    invoke-virtual {p5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    if-eqz p6, :cond_3

    .line 82
    invoke-interface {p6}, Ldid;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 81
    invoke-static {v0}, Ldib;->a(Ljava/lang/Integer;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Ldib;->d:Landroid/os/Bundle;

    .line 83
    iget-object v0, p0, Ldib;->d:Landroid/os/Bundle;

    invoke-static {p6, v0}, Ldib;->a(Ldid;Landroid/os/Bundle;)V

    .line 87
    :goto_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 88
    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v1, p0, Ldib;->d:Landroid/os/Bundle;

    const-string v2, "extra_participant_ids"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 91
    iget-object v0, p0, Ldib;->d:Landroid/os/Bundle;

    const-string v1, "extra_circle_ids"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 94
    :cond_1
    return-void

    .line 70
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Ldib;->c:Landroid/os/Bundle;

    goto :goto_0

    .line 85
    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Ldib;->d:Landroid/os/Bundle;

    goto :goto_1
.end method

.method private static a(Ljava/lang/Integer;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 97
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 98
    if-eqz p0, :cond_0

    .line 99
    const-string v1, "extra_circle_mutate_location_id"

    .line 100
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 99
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 102
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lhms;Landroid/os/Bundle;)Ldib;
    .locals 2

    .prologue
    .line 126
    if-eqz p2, :cond_1

    const-string v0, "cmclh_add_log_bundle"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "cmclh_remove_log_bundle"

    .line 127
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    :cond_0
    new-instance v0, Ldib;

    invoke-direct {v0, p0, p1}, Ldib;-><init>(Landroid/content/Context;Lhms;)V

    .line 130
    const-string v1, "cmclh_add_log_bundle"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v0, Ldib;->c:Landroid/os/Bundle;

    .line 131
    const-string v1, "cmclh_remove_log_bundle"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v0, Ldib;->d:Landroid/os/Bundle;

    .line 134
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(II)Ldid;
    .locals 3

    .prologue
    .line 189
    invoke-static {p0}, Ldib;->b(I)I

    move-result v1

    .line 192
    sparse-switch p0, :sswitch_data_0

    .line 202
    const/4 v0, 0x0

    .line 206
    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v0, v2}, Ldib;->a(ILjava/lang/Integer;Ljava/lang/Integer;)Ldid;

    move-result-object v0

    return-object v0

    .line 194
    :sswitch_0
    const/16 v0, 0x67

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 198
    :sswitch_1
    const/16 v0, 0x69

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 192
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(IILjava/lang/Integer;)Ldid;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 311
    packed-switch p0, :pswitch_data_0

    .line 354
    :goto_0
    :pswitch_0
    invoke-static {p1, v0, p2}, Ldib;->a(ILjava/lang/Integer;Ljava/lang/Integer;)Ldid;

    move-result-object v0

    return-object v0

    .line 313
    :pswitch_1
    const/4 p1, 0x7

    .line 314
    const/16 v0, 0x68

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 318
    :pswitch_2
    const/16 p1, 0x5d

    .line 319
    const/16 v0, 0x67

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 323
    :pswitch_3
    const/16 p1, 0xa9

    .line 324
    const/16 v0, 0x69

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 328
    :pswitch_4
    const/16 p1, 0x5a

    .line 330
    const/16 v0, 0x6a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 334
    :pswitch_5
    const/16 p1, 0xe8

    .line 335
    const/16 v0, 0x7c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 339
    :pswitch_6
    const/16 p1, 0x72

    .line 342
    goto :goto_0

    .line 345
    :pswitch_7
    const/16 p1, 0xd8

    .line 347
    goto :goto_0

    .line 311
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static a(ILjava/lang/Integer;Ljava/lang/Integer;)Ldid;
    .locals 1

    .prologue
    .line 273
    new-instance v0, Ldic;

    invoke-direct {v0, p0, p1, p2}, Ldic;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private static a(Ldid;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 138
    invoke-interface {p0}, Ldid;->b()Ljava/lang/Integer;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_0

    .line 140
    const-string v1, "extra_promo_type"

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 142
    :cond_0
    invoke-interface {p0}, Ldid;->aR_()Ljava/lang/Integer;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_1

    .line 144
    const-string v1, "extra_promo_group_id"

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    :cond_1
    return-void
.end method

.method public static b(I)I
    .locals 1

    .prologue
    const/16 v0, 0x42

    .line 210
    packed-switch p0, :pswitch_data_0

    .line 259
    const/4 v0, 0x0

    :goto_0
    :pswitch_0
    return v0

    .line 212
    :pswitch_1
    const/16 v0, 0x45

    goto :goto_0

    .line 216
    :pswitch_2
    const/16 v0, 0xe8

    goto :goto_0

    .line 220
    :pswitch_3
    const/16 v0, 0x5d

    goto :goto_0

    .line 224
    :pswitch_4
    const/16 v0, 0xc2

    goto :goto_0

    .line 228
    :pswitch_5
    const/16 v0, 0xe7

    goto :goto_0

    .line 232
    :pswitch_6
    const/16 v0, 0xc3

    goto :goto_0

    .line 235
    :pswitch_7
    const/16 v0, 0x40

    goto :goto_0

    .line 238
    :pswitch_8
    const/16 v0, 0x41

    goto :goto_0

    .line 247
    :pswitch_9
    const/16 v0, 0xa9

    goto :goto_0

    .line 250
    :pswitch_a
    const/16 v0, 0x7f

    goto :goto_0

    .line 253
    :pswitch_b
    const/16 v0, 0xe

    goto :goto_0

    .line 256
    :pswitch_c
    const/16 v0, 0xd

    goto :goto_0

    .line 210
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 155
    iget-object v0, p0, Ldib;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Ldib;->a:Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Ldib;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->bP:Lhmv;

    .line 158
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    iget-object v2, p0, Ldib;->c:Landroid/os/Bundle;

    .line 159
    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 156
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 161
    iput-object v3, p0, Ldib;->c:Landroid/os/Bundle;

    .line 163
    :cond_0
    iget-object v0, p0, Ldib;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 164
    iget-object v0, p0, Ldib;->a:Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Ldib;->b:Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->bQ:Lhmv;

    .line 165
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    iget-object v2, p0, Ldib;->d:Landroid/os/Bundle;

    .line 166
    invoke-virtual {v1, v2}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 164
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 168
    iput-object v3, p0, Ldib;->d:Landroid/os/Bundle;

    .line 170
    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 111
    const-string v0, "cmclh_add_log_bundle"

    iget-object v1, p0, Ldib;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 112
    const-string v0, "cmclh_remove_log_bundle"

    iget-object v1, p0, Ldib;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 113
    return-void
.end method
