.class public final Ljux;
.super Lllq;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lnzx;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lnzx;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lllq;-><init>()V

    .line 27
    iput-object p1, p0, Ljux;->a:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Ljux;->b:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Ljux;->c:Lnzx;

    .line 30
    return-void
.end method

.method public static a([B)Ljux;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 51
    if-nez p0, :cond_0

    .line 69
    :goto_0
    return-object v1

    .line 55
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 57
    invoke-static {v0}, Ljux;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    .line 58
    invoke-static {v0}, Ljux;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    .line 61
    :try_start_0
    invoke-static {v0}, Ljux;->c(Ljava/nio/ByteBuffer;)[B
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 62
    if-nez v0, :cond_1

    move-object v0, v1

    .line 69
    :goto_1
    new-instance v1, Ljux;

    invoke-direct {v1, v2, v3, v0}, Ljux;-><init>(Ljava/lang/String;Ljava/lang/String;Lnzx;)V

    goto :goto_0

    .line 62
    :cond_1
    :try_start_1
    new-instance v4, Lnzx;

    invoke-direct {v4}, Lnzx;-><init>()V

    .line 63
    invoke-static {v4, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzx;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 64
    :catch_0
    move-exception v0

    .line 65
    const-string v2, "DbCollectionData"

    const-string v3, "Unable to parse Tile from byte array."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Ljux;)[B
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 33
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x40

    invoke-direct {v2, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 34
    new-instance v3, Ljava/io/DataOutputStream;

    invoke-direct {v3, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 37
    :try_start_0
    iget-object v0, p0, Ljux;->a:Ljava/lang/String;

    invoke-static {v3, v0}, Ljux;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Ljux;->b:Ljava/lang/String;

    invoke-static {v3, v0}, Ljux;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Ljux;->c:Lnzx;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-static {v3, v0}, Ljux;->a(Ljava/io/DataOutputStream;[B)V

    .line 41
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 42
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V

    .line 47
    :goto_1
    return-object v0

    .line 39
    :cond_0
    iget-object v0, p0, Ljux;->c:Lnzx;

    .line 40
    invoke-static {v0}, Loxu;->a(Loxu;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    const-string v2, "DbCollectionData"

    const-string v3, "Unable to serialize collection data."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 45
    goto :goto_1
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ljux;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Ljux;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lnzx;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Ljux;->c:Lnzx;

    return-object v0
.end method
