.class final Lhve;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static s:Lhve;


# instance fields
.field public final a:Lfo;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:Landroid/graphics/Rect;

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:Landroid/graphics/Paint;

.field public final n:I

.field public final o:Landroid/graphics/drawable/NinePatchDrawable;

.field public final p:Landroid/graphics/drawable/NinePatchDrawable;

.field public final q:[Landroid/graphics/Paint;

.field public final r:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 75
    invoke-static {}, Lfo;->a()Lfo;

    move-result-object v0

    iput-object v0, p0, Lhve;->a:Lfo;

    .line 77
    const v0, 0x7f0c0046

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhve;->b:I

    .line 79
    const v0, 0x7f0d0187

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhve;->c:I

    .line 80
    const v0, 0x7f0d0188

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    .line 83
    const v0, 0x7f0d018a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhve;->e:I

    .line 84
    const v0, 0x7f0d018b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhve;->d:I

    .line 85
    const v0, 0x7f0d018c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhve;->f:I

    .line 87
    const v0, 0x7f0d0199

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhve;->g:I

    .line 88
    const v0, 0x7f0d019a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhve;->h:I

    .line 90
    const v0, 0x7f0d019b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhve;->j:I

    .line 92
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhve;->i:Landroid/graphics/Rect;

    .line 94
    const v0, 0x7f0c004d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhve;->k:I

    .line 95
    const v0, 0x7f0c004f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhve;->l:I

    .line 97
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lhve;->m:Landroid/graphics/Paint;

    .line 98
    iget-object v0, p0, Lhve;->m:Landroid/graphics/Paint;

    const v2, 0x7f0b0101

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    iget-object v0, p0, Lhve;->m:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 100
    iget-object v0, p0, Lhve;->m:Landroid/graphics/Paint;

    const v2, 0x7f0d019c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 102
    const v0, 0x7f0c0048

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    .line 104
    const v0, 0x7f0c0047

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lhve;->n:I

    .line 107
    const v0, 0x7f020079

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lhve;->o:Landroid/graphics/drawable/NinePatchDrawable;

    .line 108
    const v0, 0x7f02007a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v0, p0, Lhve;->p:Landroid/graphics/drawable/NinePatchDrawable;

    .line 111
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/Paint;

    iput-object v0, p0, Lhve;->q:[Landroid/graphics/Paint;

    .line 112
    iget-object v0, p0, Lhve;->q:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v3

    .line 113
    iget-object v0, p0, Lhve;->q:[Landroid/graphics/Paint;

    aget-object v0, v0, v3

    const v2, 0x7f0b0104

    .line 114
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 113
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 115
    iget-object v0, p0, Lhve;->q:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v4

    .line 116
    iget-object v0, p0, Lhve;->q:[Landroid/graphics/Paint;

    aget-object v0, v0, v4

    const v2, 0x7f0b0105

    .line 117
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 116
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 118
    iget-object v0, p0, Lhve;->q:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v0, v5

    .line 119
    iget-object v0, p0, Lhve;->q:[Landroid/graphics/Paint;

    aget-object v0, v0, v5

    const v2, 0x7f0b0106

    .line 120
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 119
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 122
    const v0, 0x7f0d019d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lhve;->r:I

    .line 123
    return-void
.end method

.method public static a(Landroid/content/Context;)Lhve;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lhve;->s:Lhve;

    if-nez v0, :cond_0

    .line 127
    invoke-static {}, Llsx;->b()V

    .line 128
    new-instance v0, Lhve;

    invoke-direct {v0, p0}, Lhve;-><init>(Landroid/content/Context;)V

    sput-object v0, Lhve;->s:Lhve;

    .line 130
    :cond_0
    sget-object v0, Lhve;->s:Lhve;

    return-object v0
.end method
