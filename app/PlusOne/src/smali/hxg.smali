.class public final Lhxg;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:[Ljava/lang/String;

.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II[Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 33
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 48
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lhxg;->b:Ldp;

    .line 49
    sget-object v0, Lhxi;->a:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lhxg;->a(Landroid/net/Uri;)V

    .line 50
    iput p2, p0, Lhxg;->c:I

    .line 51
    iput-object p4, p0, Lhxg;->d:[Ljava/lang/String;

    .line 52
    iput p3, p0, Lhxg;->e:I

    .line 53
    iput-object p5, p0, Lhxg;->f:Ljava/lang/String;

    .line 54
    iput p6, p0, Lhxg;->g:I

    .line 55
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 62
    invoke-virtual {p0}, Lhxg;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    .line 63
    const-class v1, Lhxk;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhxk;

    .line 64
    invoke-virtual {p0}, Lhxg;->n()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lhxg;->c:I

    iget v3, p0, Lhxg;->e:I

    iget-object v4, p0, Lhxg;->d:[Ljava/lang/String;

    iget-object v5, p0, Lhxg;->f:Ljava/lang/String;

    iget v6, p0, Lhxg;->g:I

    const/4 v7, 0x1

    invoke-interface/range {v0 .. v7}, Lhxk;->a(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;IZ)Landroid/database/Cursor;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_0

    .line 67
    iget-object v1, p0, Lhxg;->b:Ldp;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 69
    :cond_0
    return-object v0
.end method
