.class public final Ldmi;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmhm;",
        "Lmhn;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Random;


# instance fields
.field private final b:Ljava/lang/String;

.field private final p:Ljava/lang/String;

.field private final q:Ljava/lang/String;

.field private final r:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Ldmi;->a:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[B)V
    .locals 6

    .prologue
    .line 40
    const-string v3, "postcomment"

    new-instance v4, Lmhm;

    invoke-direct {v4}, Lmhm;-><init>()V

    new-instance v5, Lmhn;

    invoke-direct {v5}, Lmhn;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 42
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 43
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    .line 44
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmi;->b:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Ldmi;->p:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Ldmi;->q:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Ldmi;->r:[B

    .line 48
    return-void
.end method


# virtual methods
.method protected a(Lmhm;)V
    .locals 7

    .prologue
    .line 52
    new-instance v0, Lnvq;

    invoke-direct {v0}, Lnvq;-><init>()V

    iput-object v0, p1, Lmhm;->a:Lnvq;

    .line 53
    iget-object v0, p1, Lmhm;->a:Lnvq;

    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 55
    iget-object v1, p0, Ldmi;->b:Ljava/lang/String;

    sget-object v4, Ldmi;->a:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextInt()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1f

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lnvq;->b:Ljava/lang/String;

    .line 56
    iget-object v1, p0, Ldmi;->p:Ljava/lang/String;

    iput-object v1, v0, Lnvq;->a:Ljava/lang/String;

    .line 57
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lnvq;->d:Ljava/lang/Long;

    .line 58
    iget-object v1, p0, Ldmi;->q:Ljava/lang/String;

    iput-object v1, v0, Lnvq;->c:Ljava/lang/String;

    .line 59
    const/4 v1, 0x2

    iput v1, v0, Lnvq;->e:I

    .line 61
    iget-object v1, p0, Ldmi;->r:[B

    invoke-static {v1}, Llap;->a([B)Lnwr;

    move-result-object v1

    .line 62
    if-eqz v1, :cond_0

    .line 63
    new-instance v2, Logw;

    invoke-direct {v2}, Logw;-><init>()V

    iput-object v2, v0, Lnvq;->f:Logw;

    .line 64
    iget-object v0, v0, Lnvq;->f:Logw;

    iput-object v1, v0, Logw;->a:Lnwr;

    .line 66
    :cond_0
    return-void
.end method

.method protected a(Lmhn;)V
    .locals 4

    .prologue
    .line 71
    if-eqz p1, :cond_0

    iget-object v0, p1, Lmhn;->a:Lnvz;

    if-nez v0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v0, p1, Lmhn;->a:Lnvz;

    .line 78
    iget-object v0, v0, Lnvz;->a:Lofa;

    .line 79
    if-eqz v0, :cond_0

    .line 84
    iget-object v1, p0, Ldmi;->p:Ljava/lang/String;

    iget-object v2, v0, Lofa;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Ldmi;->f:Landroid/content/Context;

    iget v2, p0, Ldmi;->c:I

    iget-object v3, v0, Lofa;->h:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Llap;->a(Landroid/content/Context;ILjava/lang/String;Lofa;)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lmhm;

    invoke-virtual {p0, p1}, Ldmi;->a(Lmhm;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Lmhn;

    invoke-virtual {p0, p1}, Ldmi;->a(Lmhn;)V

    return-void
.end method
