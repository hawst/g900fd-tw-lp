.class public final Lmbx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmqt;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18466
    invoke-direct {p0}, Loxq;-><init>()V

    .line 18469
    iput-object v0, p0, Lmbx;->apiHeader:Llyr;

    .line 18472
    iput-object v0, p0, Lmbx;->a:Lmqt;

    .line 18466
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 18489
    const/4 v0, 0x0

    .line 18490
    iget-object v1, p0, Lmbx;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 18491
    const/4 v0, 0x1

    iget-object v1, p0, Lmbx;->apiHeader:Llyr;

    .line 18492
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 18494
    :cond_0
    iget-object v1, p0, Lmbx;->a:Lmqt;

    if-eqz v1, :cond_1

    .line 18495
    const/4 v1, 0x2

    iget-object v2, p0, Lmbx;->a:Lmqt;

    .line 18496
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18498
    :cond_1
    iget-object v1, p0, Lmbx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 18499
    iput v0, p0, Lmbx;->ai:I

    .line 18500
    return v0
.end method

.method public a(Loxn;)Lmbx;
    .locals 2

    .prologue
    .line 18508
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 18509
    sparse-switch v0, :sswitch_data_0

    .line 18513
    iget-object v1, p0, Lmbx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 18514
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbx;->ah:Ljava/util/List;

    .line 18517
    :cond_1
    iget-object v1, p0, Lmbx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 18519
    :sswitch_0
    return-object p0

    .line 18524
    :sswitch_1
    iget-object v0, p0, Lmbx;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 18525
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbx;->apiHeader:Llyr;

    .line 18527
    :cond_2
    iget-object v0, p0, Lmbx;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18531
    :sswitch_2
    iget-object v0, p0, Lmbx;->a:Lmqt;

    if-nez v0, :cond_3

    .line 18532
    new-instance v0, Lmqt;

    invoke-direct {v0}, Lmqt;-><init>()V

    iput-object v0, p0, Lmbx;->a:Lmqt;

    .line 18534
    :cond_3
    iget-object v0, p0, Lmbx;->a:Lmqt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 18509
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 18477
    iget-object v0, p0, Lmbx;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 18478
    const/4 v0, 0x1

    iget-object v1, p0, Lmbx;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18480
    :cond_0
    iget-object v0, p0, Lmbx;->a:Lmqt;

    if-eqz v0, :cond_1

    .line 18481
    const/4 v0, 0x2

    iget-object v1, p0, Lmbx;->a:Lmqt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 18483
    :cond_1
    iget-object v0, p0, Lmbx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 18485
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 18462
    invoke-virtual {p0, p1}, Lmbx;->a(Loxn;)Lmbx;

    move-result-object v0

    return-object v0
.end method
