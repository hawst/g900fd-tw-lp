.class public final Lcra;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:J

.field public final h:J

.field public i:J

.field public final j:I

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcrb;",
            ">;"
        }
    .end annotation
.end field

.field private l:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JJJII)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcra;->a:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcra;->b:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcra;->c:Ljava/lang/String;

    .line 45
    iput p4, p0, Lcra;->d:I

    .line 46
    iput-object p5, p0, Lcra;->e:Ljava/lang/String;

    .line 47
    iput-object p6, p0, Lcra;->f:Ljava/lang/String;

    .line 48
    iput-wide p7, p0, Lcra;->g:J

    .line 49
    iput-wide p9, p0, Lcra;->h:J

    .line 50
    iput-wide p11, p0, Lcra;->i:J

    .line 51
    iput p13, p0, Lcra;->j:I

    .line 52
    iput p14, p0, Lcra;->l:I

    .line 53
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcra;->l:I

    return v0
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcrb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    iput-object p1, p0, Lcra;->k:Ljava/util/List;

    .line 68
    return-void
.end method

.method public b()Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljuc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcra;->k:Ljava/util/List;

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x0

    .line 87
    :goto_0
    return-object v0

    .line 78
    :cond_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 79
    iget-object v0, p0, Lcra;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v10

    .line 81
    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v10, :cond_1

    .line 82
    iget-object v0, p0, Lcra;->k:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcrb;

    .line 83
    new-instance v0, Ljuc;

    iget-object v1, v6, Lcrb;->a:Ljava/lang/String;

    iget-object v2, v6, Lcrb;->d:Lcra;

    iget-object v2, v2, Lcra;->c:Ljava/lang/String;

    iget-object v3, v6, Lcrb;->c:Lizu;

    iget-wide v4, v6, Lcrb;->g:J

    iget-wide v6, v6, Lcrb;->h:J

    invoke-direct/range {v0 .. v7}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    :cond_1
    move-object v0, v8

    .line 87
    goto :goto_0
.end method
