.class public final Ldxo;
.super Lepp;
.source "PG"


# instance fields
.field private final d:Legi;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILept;Legi;)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lepp;-><init>(Landroid/content/Context;ILept;Z)V

    .line 25
    iput-object p4, p0, Ldxo;->d:Legi;

    .line 26
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Ldxo;->c:Landroid/content/Context;

    iget v1, p0, Ldxo;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Leyq;->b(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v0

    .line 37
    iget-object v1, p0, Ldxo;->d:Legi;

    invoke-virtual {v1, v0}, Legi;->b(Landroid/content/Intent;)V

    .line 38
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 59
    iget-object v0, p0, Ldxo;->c:Landroid/content/Context;

    iget v1, p0, Ldxo;->b:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->f(Landroid/content/Context;I)I

    move-result v1

    .line 60
    if-gtz v1, :cond_0

    .line 61
    invoke-static {}, Lfvc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x34

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Auto Awesome promo shown for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " new movies."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    iget-object v0, p0, Ldxo;->c:Landroid/content/Context;

    invoke-static {v0}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v0

    invoke-virtual {v0}, Ljem;->g()Ljdw;

    move-result-object v0

    iget v2, p0, Ldxo;->b:I

    .line 67
    invoke-interface {v0, v2}, Ljdw;->c(I)V

    .line 68
    const v0, 0x7f1001ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 69
    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f110077

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 70
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 69
    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    :cond_1
    return-void
.end method

.method public a(Lnyq;)Z
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1}, Lepp;->a(Lnyq;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldxo;->c:Landroid/content/Context;

    iget v1, p0, Ldxo;->b:I

    .line 31
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->h(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Ldxo;->c:Landroid/content/Context;

    iget v1, p0, Ldxo;->b:I

    const/4 v2, 0x1

    new-instance v3, Ldxp;

    invoke-direct {v3, p0}, Ldxp;-><init>(Ldxo;)V

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;IZLdvj;)V

    .line 50
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 54
    const v0, 0x7f04004f

    return v0
.end method

.method public e()Lepn;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lepn;->e:Lepn;

    return-object v0
.end method
