.class public final Loft;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loft;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Lofs;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1573
    const/4 v0, 0x0

    new-array v0, v0, [Loft;

    sput-object v0, Loft;->a:[Loft;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1574
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1593
    const/high16 v0, -0x80000000

    iput v0, p0, Loft;->b:I

    .line 1610
    sget-object v0, Lofs;->a:[Lofs;

    iput-object v0, p0, Loft;->f:[Lofs;

    .line 1574
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1652
    .line 1653
    iget v0, p0, Loft;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_9

    .line 1654
    const/4 v0, 0x1

    iget v2, p0, Loft;->b:I

    .line 1655
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1657
    :goto_0
    iget-object v2, p0, Loft;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1658
    const/4 v2, 0x2

    iget-object v3, p0, Loft;->g:Ljava/lang/String;

    .line 1659
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1661
    :cond_0
    iget-object v2, p0, Loft;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1662
    const/4 v2, 0x3

    iget-object v3, p0, Loft;->c:Ljava/lang/String;

    .line 1663
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1665
    :cond_1
    iget-object v2, p0, Loft;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 1666
    const/4 v2, 0x4

    iget-object v3, p0, Loft;->d:Ljava/lang/String;

    .line 1667
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1669
    :cond_2
    iget-object v2, p0, Loft;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 1670
    const/4 v2, 0x5

    iget-object v3, p0, Loft;->h:Ljava/lang/String;

    .line 1671
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1673
    :cond_3
    iget-object v2, p0, Loft;->i:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 1674
    const/4 v2, 0x6

    iget-object v3, p0, Loft;->i:Ljava/lang/String;

    .line 1675
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1677
    :cond_4
    iget-object v2, p0, Loft;->f:[Lofs;

    if-eqz v2, :cond_6

    .line 1678
    iget-object v2, p0, Loft;->f:[Lofs;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 1679
    if-eqz v4, :cond_5

    .line 1680
    const/4 v5, 0x7

    .line 1681
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1678
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1685
    :cond_6
    iget-object v1, p0, Loft;->e:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 1686
    const/16 v1, 0x8

    iget-object v2, p0, Loft;->e:Ljava/lang/String;

    .line 1687
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1689
    :cond_7
    iget-object v1, p0, Loft;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 1690
    const/16 v1, 0x9

    iget-object v2, p0, Loft;->j:Ljava/lang/Integer;

    .line 1691
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1693
    :cond_8
    iget-object v1, p0, Loft;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1694
    iput v0, p0, Loft;->ai:I

    .line 1695
    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loft;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1703
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1704
    sparse-switch v0, :sswitch_data_0

    .line 1708
    iget-object v2, p0, Loft;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1709
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loft;->ah:Ljava/util/List;

    .line 1712
    :cond_1
    iget-object v2, p0, Loft;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1714
    :sswitch_0
    return-object p0

    .line 1719
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1720
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_2

    const/4 v2, 0x6

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    if-eq v0, v2, :cond_2

    const/16 v2, 0x8

    if-eq v0, v2, :cond_2

    const/16 v2, 0x9

    if-eq v0, v2, :cond_2

    const/16 v2, 0xa

    if-eq v0, v2, :cond_2

    const/16 v2, 0xb

    if-eq v0, v2, :cond_2

    const/16 v2, 0xc

    if-eq v0, v2, :cond_2

    const/16 v2, 0xd

    if-ne v0, v2, :cond_3

    .line 1733
    :cond_2
    iput v0, p0, Loft;->b:I

    goto :goto_0

    .line 1735
    :cond_3
    iput v4, p0, Loft;->b:I

    goto :goto_0

    .line 1740
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loft;->g:Ljava/lang/String;

    goto :goto_0

    .line 1744
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loft;->c:Ljava/lang/String;

    goto :goto_0

    .line 1748
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loft;->d:Ljava/lang/String;

    goto :goto_0

    .line 1752
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loft;->h:Ljava/lang/String;

    goto :goto_0

    .line 1756
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loft;->i:Ljava/lang/String;

    goto :goto_0

    .line 1760
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1761
    iget-object v0, p0, Loft;->f:[Lofs;

    if-nez v0, :cond_5

    move v0, v1

    .line 1762
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lofs;

    .line 1763
    iget-object v3, p0, Loft;->f:[Lofs;

    if-eqz v3, :cond_4

    .line 1764
    iget-object v3, p0, Loft;->f:[Lofs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1766
    :cond_4
    iput-object v2, p0, Loft;->f:[Lofs;

    .line 1767
    :goto_2
    iget-object v2, p0, Loft;->f:[Lofs;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1768
    iget-object v2, p0, Loft;->f:[Lofs;

    new-instance v3, Lofs;

    invoke-direct {v3}, Lofs;-><init>()V

    aput-object v3, v2, v0

    .line 1769
    iget-object v2, p0, Loft;->f:[Lofs;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1770
    invoke-virtual {p1}, Loxn;->a()I

    .line 1767
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1761
    :cond_5
    iget-object v0, p0, Loft;->f:[Lofs;

    array-length v0, v0

    goto :goto_1

    .line 1773
    :cond_6
    iget-object v2, p0, Loft;->f:[Lofs;

    new-instance v3, Lofs;

    invoke-direct {v3}, Lofs;-><init>()V

    aput-object v3, v2, v0

    .line 1774
    iget-object v2, p0, Loft;->f:[Lofs;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1778
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loft;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 1782
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loft;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 1704
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1615
    iget v0, p0, Loft;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1616
    const/4 v0, 0x1

    iget v1, p0, Loft;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1618
    :cond_0
    iget-object v0, p0, Loft;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1619
    const/4 v0, 0x2

    iget-object v1, p0, Loft;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1621
    :cond_1
    iget-object v0, p0, Loft;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1622
    const/4 v0, 0x3

    iget-object v1, p0, Loft;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1624
    :cond_2
    iget-object v0, p0, Loft;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1625
    const/4 v0, 0x4

    iget-object v1, p0, Loft;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1627
    :cond_3
    iget-object v0, p0, Loft;->h:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1628
    const/4 v0, 0x5

    iget-object v1, p0, Loft;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1630
    :cond_4
    iget-object v0, p0, Loft;->i:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1631
    const/4 v0, 0x6

    iget-object v1, p0, Loft;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1633
    :cond_5
    iget-object v0, p0, Loft;->f:[Lofs;

    if-eqz v0, :cond_7

    .line 1634
    iget-object v1, p0, Loft;->f:[Lofs;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 1635
    if-eqz v3, :cond_6

    .line 1636
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1634
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1640
    :cond_7
    iget-object v0, p0, Loft;->e:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1641
    const/16 v0, 0x8

    iget-object v1, p0, Loft;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1643
    :cond_8
    iget-object v0, p0, Loft;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 1644
    const/16 v0, 0x9

    iget-object v1, p0, Loft;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1646
    :cond_9
    iget-object v0, p0, Loft;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1648
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1570
    invoke-virtual {p0, p1}, Loft;->a(Loxn;)Loft;

    move-result-object v0

    return-object v0
.end method
