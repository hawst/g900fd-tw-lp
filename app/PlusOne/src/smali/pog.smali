.class public final Lpog;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpog;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lppf;

.field private d:[Lppg;

.field private e:Lppg;

.field private f:[Lpoi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    new-array v0, v0, [Lpog;

    sput-object v0, Lpog;->a:[Lpog;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    iput-object v1, p0, Lpog;->c:Lppf;

    .line 22
    sget-object v0, Lppg;->a:[Lppg;

    iput-object v0, p0, Lpog;->d:[Lppg;

    .line 25
    iput-object v1, p0, Lpog;->e:Lppg;

    .line 28
    sget-object v0, Lpoi;->a:[Lpoi;

    iput-object v0, p0, Lpog;->f:[Lpoi;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 62
    .line 63
    iget-object v0, p0, Lpog;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 64
    const/4 v0, 0x1

    iget-object v2, p0, Lpog;->b:Ljava/lang/String;

    .line 65
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 67
    :goto_0
    iget-object v2, p0, Lpog;->c:Lppf;

    if-eqz v2, :cond_0

    .line 68
    const/4 v2, 0x2

    iget-object v3, p0, Lpog;->c:Lppf;

    .line 69
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 71
    :cond_0
    iget-object v2, p0, Lpog;->d:[Lppg;

    if-eqz v2, :cond_2

    .line 72
    iget-object v3, p0, Lpog;->d:[Lppg;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 73
    if-eqz v5, :cond_1

    .line 74
    const/4 v6, 0x3

    .line 75
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 72
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 79
    :cond_2
    iget-object v2, p0, Lpog;->e:Lppg;

    if-eqz v2, :cond_3

    .line 80
    const/4 v2, 0x4

    iget-object v3, p0, Lpog;->e:Lppg;

    .line 81
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 83
    :cond_3
    iget-object v2, p0, Lpog;->f:[Lpoi;

    if-eqz v2, :cond_5

    .line 84
    iget-object v2, p0, Lpog;->f:[Lpoi;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 85
    if-eqz v4, :cond_4

    .line 86
    const/4 v5, 0x5

    .line 87
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 84
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 91
    :cond_5
    iget-object v1, p0, Lpog;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    iput v0, p0, Lpog;->ai:I

    .line 93
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpog;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 101
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 102
    sparse-switch v0, :sswitch_data_0

    .line 106
    iget-object v2, p0, Lpog;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 107
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpog;->ah:Ljava/util/List;

    .line 110
    :cond_1
    iget-object v2, p0, Lpog;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    :sswitch_0
    return-object p0

    .line 117
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpog;->b:Ljava/lang/String;

    goto :goto_0

    .line 121
    :sswitch_2
    iget-object v0, p0, Lpog;->c:Lppf;

    if-nez v0, :cond_2

    .line 122
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpog;->c:Lppf;

    .line 124
    :cond_2
    iget-object v0, p0, Lpog;->c:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 128
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 129
    iget-object v0, p0, Lpog;->d:[Lppg;

    if-nez v0, :cond_4

    move v0, v1

    .line 130
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lppg;

    .line 131
    iget-object v3, p0, Lpog;->d:[Lppg;

    if-eqz v3, :cond_3

    .line 132
    iget-object v3, p0, Lpog;->d:[Lppg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 134
    :cond_3
    iput-object v2, p0, Lpog;->d:[Lppg;

    .line 135
    :goto_2
    iget-object v2, p0, Lpog;->d:[Lppg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 136
    iget-object v2, p0, Lpog;->d:[Lppg;

    new-instance v3, Lppg;

    invoke-direct {v3}, Lppg;-><init>()V

    aput-object v3, v2, v0

    .line 137
    iget-object v2, p0, Lpog;->d:[Lppg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 138
    invoke-virtual {p1}, Loxn;->a()I

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 129
    :cond_4
    iget-object v0, p0, Lpog;->d:[Lppg;

    array-length v0, v0

    goto :goto_1

    .line 141
    :cond_5
    iget-object v2, p0, Lpog;->d:[Lppg;

    new-instance v3, Lppg;

    invoke-direct {v3}, Lppg;-><init>()V

    aput-object v3, v2, v0

    .line 142
    iget-object v2, p0, Lpog;->d:[Lppg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 146
    :sswitch_4
    iget-object v0, p0, Lpog;->e:Lppg;

    if-nez v0, :cond_6

    .line 147
    new-instance v0, Lppg;

    invoke-direct {v0}, Lppg;-><init>()V

    iput-object v0, p0, Lpog;->e:Lppg;

    .line 149
    :cond_6
    iget-object v0, p0, Lpog;->e:Lppg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 153
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 154
    iget-object v0, p0, Lpog;->f:[Lpoi;

    if-nez v0, :cond_8

    move v0, v1

    .line 155
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpoi;

    .line 156
    iget-object v3, p0, Lpog;->f:[Lpoi;

    if-eqz v3, :cond_7

    .line 157
    iget-object v3, p0, Lpog;->f:[Lpoi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 159
    :cond_7
    iput-object v2, p0, Lpog;->f:[Lpoi;

    .line 160
    :goto_4
    iget-object v2, p0, Lpog;->f:[Lpoi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 161
    iget-object v2, p0, Lpog;->f:[Lpoi;

    new-instance v3, Lpoi;

    invoke-direct {v3}, Lpoi;-><init>()V

    aput-object v3, v2, v0

    .line 162
    iget-object v2, p0, Lpog;->f:[Lpoi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 163
    invoke-virtual {p1}, Loxn;->a()I

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 154
    :cond_8
    iget-object v0, p0, Lpog;->f:[Lpoi;

    array-length v0, v0

    goto :goto_3

    .line 166
    :cond_9
    iget-object v2, p0, Lpog;->f:[Lpoi;

    new-instance v3, Lpoi;

    invoke-direct {v3}, Lpoi;-><init>()V

    aput-object v3, v2, v0

    .line 167
    iget-object v2, p0, Lpog;->f:[Lpoi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 102
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-object v1, p0, Lpog;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 34
    const/4 v1, 0x1

    iget-object v2, p0, Lpog;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 36
    :cond_0
    iget-object v1, p0, Lpog;->c:Lppf;

    if-eqz v1, :cond_1

    .line 37
    const/4 v1, 0x2

    iget-object v2, p0, Lpog;->c:Lppf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 39
    :cond_1
    iget-object v1, p0, Lpog;->d:[Lppg;

    if-eqz v1, :cond_3

    .line 40
    iget-object v2, p0, Lpog;->d:[Lppg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 41
    if-eqz v4, :cond_2

    .line 42
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 40
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 46
    :cond_3
    iget-object v1, p0, Lpog;->e:Lppg;

    if-eqz v1, :cond_4

    .line 47
    const/4 v1, 0x4

    iget-object v2, p0, Lpog;->e:Lppg;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 49
    :cond_4
    iget-object v1, p0, Lpog;->f:[Lpoi;

    if-eqz v1, :cond_6

    .line 50
    iget-object v1, p0, Lpog;->f:[Lpoi;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 51
    if-eqz v3, :cond_5

    .line 52
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 50
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 56
    :cond_6
    iget-object v0, p0, Lpog;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 58
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lpog;->a(Loxn;)Lpog;

    move-result-object v0

    return-object v0
.end method
