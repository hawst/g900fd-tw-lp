.class public final Llrv;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a([Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 25
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;[TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 104
    if-nez p0, :cond_1

    .line 121
    :cond_0
    :goto_0
    return-object p1

    .line 107
    :cond_1
    if-nez p1, :cond_2

    move-object p1, p0

    .line 108
    goto :goto_0

    .line 110
    :cond_2
    array-length v0, p0

    .line 111
    if-eqz v0, :cond_0

    .line 114
    array-length v1, p1

    .line 115
    if-nez v1, :cond_3

    move-object p1, p0

    .line 116
    goto :goto_0

    .line 119
    :cond_3
    add-int v2, v0, v1

    invoke-static {p0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p0

    .line 120
    const/4 v2, 0x0

    invoke-static {p1, v2, p0, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p1, p0

    .line 121
    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 93
    invoke-static {p1, p0}, Llrv;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method
