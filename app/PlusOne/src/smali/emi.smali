.class public final Lemi;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ldvw;",
        ">;>;"
    }
.end annotation


# static fields
.field private static b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:I

.field private d:J

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnqx;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ldvx;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lemk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lemi;->b:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IJLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IJ",
            "Ljava/util/List",
            "<",
            "Lnqx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lemi;->e:Ljava/util/List;

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lemi;->f:Ljava/util/HashMap;

    .line 137
    invoke-direct {p0, p2, p3, p4, p5}, Lemi;->a(IJLjava/util/List;)V

    .line 138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IJLnqx;)V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lemi;->e:Ljava/util/List;

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lemi;->f:Ljava/util/HashMap;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 128
    if-eqz p5, :cond_0

    .line 129
    invoke-virtual {v0, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_0
    invoke-direct {p0, p2, p3, p4, v0}, Lemi;->a(IJLjava/util/List;)V

    .line 132
    return-void
.end method

.method public static a(Lnqx;Z)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x2d

    .line 663
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 664
    iget v1, p0, Lnqx;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 665
    iget-object v1, p0, Lnqx;->d:Lohl;

    if-eqz v1, :cond_1

    .line 666
    iget-object v1, p0, Lnqx;->d:Lohl;

    .line 667
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lohl;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lohl;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 668
    iget-object v2, v1, Lohl;->c:Ljava/lang/Integer;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 669
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Lohl;->c:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 671
    :cond_0
    iget-object v2, v1, Lohl;->d:Ljava/lang/Integer;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 672
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v1, v1, Lohl;->d:Ljava/lang/Integer;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 675
    :cond_1
    if-eqz p1, :cond_2

    .line 676
    const-string v1, "-p"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 678
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llsf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(IJLjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/List",
            "<",
            "Lnqx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    iput p1, p0, Lemi;->c:I

    .line 143
    iput-wide p2, p0, Lemi;->d:J

    .line 144
    iput-object p4, p0, Lemi;->e:Ljava/util/List;

    .line 145
    invoke-virtual {p0}, Lemi;->C()V

    .line 146
    return-void
.end method

.method public static a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lemi;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    const-string v0, "people_view_loader_dismissed_people"

    sget-object v1, Lemi;->b:Ljava/util/HashSet;

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 75
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lemi;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 61
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ldvw;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;",
            "Ldvw;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 418
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 419
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 420
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvw;

    .line 421
    iget v1, v0, Ldvw;->a:I

    iget v5, p2, Ldvw;->a:I

    if-ne v1, v5, :cond_7

    const/4 v1, 0x3

    iget v5, v0, Ldvw;->a:I

    if-eq v1, v5, :cond_1

    iget v1, v0, Ldvw;->a:I

    if-ne v6, v1, :cond_2

    :cond_1
    iget-object v1, v0, Ldvw;->b:Lohl;

    if-nez v1, :cond_6

    :cond_2
    move v1, v2

    :goto_0
    if-nez v1, :cond_3

    iget-object v1, v0, Ldvw;->b:Lohl;

    iget-object v5, p2, Ldvw;->b:Lohl;

    invoke-direct {p0, v1, v5}, Lemi;->a(Lohl;Lohl;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_3
    move v1, v2

    :goto_1
    if-eqz v1, :cond_0

    .line 422
    const-string v1, "PeopleViewLoader"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 423
    iget v1, v0, Ldvw;->a:I

    iget-object v2, v0, Ldvw;->b:Lohl;

    if-eqz v2, :cond_9

    const-string v2, "; "

    iget-object v0, v0, Ldvw;->b:Lohl;

    iget-object v0, v0, Lohl;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "removing from responses: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 430
    :cond_5
    return-void

    :cond_6
    move v1, v3

    .line 421
    goto :goto_0

    :cond_7
    move v1, v3

    goto :goto_1

    .line 423
    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    const-string v0, ""

    goto :goto_2
.end method

.method public static a(Ldvw;)Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Ldvw;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Lemi;->a(Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/ArrayList;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnrp;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 90
    sget-object v1, Lemi;->b:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    :goto_0
    return v0

    .line 99
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    .line 100
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnrp;

    .line 102
    iget-object v3, v0, Lnrp;->b:Lohv;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lnrp;->b:Lohv;

    iget-object v3, v3, Lohv;->b:Lohp;

    if-eqz v3, :cond_2

    .line 103
    iget-object v0, v0, Lnrp;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    iget-object v0, v0, Lohp;->d:Ljava/lang/String;

    .line 104
    sget-object v3, Lemi;->b:Ljava/util/HashSet;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 105
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 106
    const/4 v1, 0x1

    move v0, v1

    :goto_2
    move v1, v0

    .line 109
    goto :goto_1

    :cond_1
    move v0, v1

    .line 110
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method private a(Lnqx;)Z
    .locals 2

    .prologue
    .line 402
    iget-object v0, p1, Lnqx;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lnqx;Ldvx;)Z
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 561
    const/4 v0, 0x3

    iget v3, p1, Lnqx;->b:I

    if-eq v0, v3, :cond_0

    iget v0, p1, Lnqx;->b:I

    if-ne v10, v0, :cond_7

    :cond_0
    iget-object v0, p1, Lnqx;->d:Lohl;

    if-nez v0, :cond_7

    .line 563
    iget-object v4, p2, Ldvx;->a:Ljava/util/ArrayList;

    .line 565
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    .line 566
    :goto_0
    if-ge v3, v5, :cond_6

    .line 567
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvw;

    .line 569
    new-instance v6, Lnqx;

    invoke-direct {v6}, Lnqx;-><init>()V

    .line 570
    iget v7, v0, Ldvw;->a:I

    iput v7, v6, Lnqx;->b:I

    .line 571
    iget-object v0, v0, Ldvw;->b:Lohl;

    iput-object v0, v6, Lnqx;->d:Lohl;

    .line 572
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lnqx;->c:Ljava/lang/Integer;

    .line 573
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lnqx;->e:Ljava/lang/Boolean;

    .line 575
    iget-object v0, p0, Lemi;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnqx;

    iget v8, v0, Lnqx;->b:I

    iget v9, v6, Lnqx;->b:I

    if-ne v8, v9, :cond_3

    iget-object v0, v0, Lnqx;->d:Lohl;

    iget-object v8, v6, Lnqx;->d:Lohl;

    invoke-direct {p0, v0, v8}, Lemi;->a(Lohl;Lohl;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    if-nez v0, :cond_2

    .line 576
    iget-object v0, p0, Lemi;->e:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 577
    const-string v0, "PeopleViewLoader"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 578
    const-string v0, "queueing to all request: "

    iget-object v6, v6, Lnqx;->d:Lohl;

    iget-object v6, v6, Lohl;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v0, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 566
    :cond_2
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    move v0, v2

    .line 575
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    .line 578
    :cond_5
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    :cond_6
    move v0, v1

    .line 586
    :goto_4
    return v0

    :cond_7
    move v0, v2

    goto :goto_4
.end method

.method private a(Lohl;Lohl;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 451
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 472
    :goto_0
    return v0

    .line 455
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    move v0, v1

    .line 456
    goto :goto_0

    .line 462
    :cond_2
    iget-object v2, p1, Lohl;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p2, Lohl;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    move v2, v0

    .line 463
    :goto_1
    if-eqz v2, :cond_4

    iget-object v2, p1, Lohl;->c:Ljava/lang/Integer;

    iget-object v3, p2, Lohl;->c:Ljava/lang/Integer;

    invoke-static {v2, v3}, Llsl;->a(Ljava/lang/Integer;Ljava/lang/Integer;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 464
    goto :goto_0

    :cond_3
    move v2, v1

    .line 462
    goto :goto_1

    .line 467
    :cond_4
    iget-object v2, p1, Lohl;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    iget-object v2, p2, Lohl;->d:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    .line 468
    :goto_2
    if-eqz v0, :cond_6

    iget-object v0, p1, Lohl;->d:Ljava/lang/Integer;

    iget-object v2, p2, Lohl;->d:Ljava/lang/Integer;

    invoke-static {v0, v2}, Llsl;->a(Ljava/lang/Integer;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    .line 469
    goto :goto_0

    :cond_5
    move v0, v1

    .line 467
    goto :goto_2

    .line 472
    :cond_6
    iget-object v0, p1, Lohl;->a:Ljava/lang/String;

    iget-object v1, p2, Lohl;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method private b(Lnqx;)Ldvx;
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 515
    const/4 v0, 0x0

    .line 518
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lemi;->a(Lnqx;Z)Ljava/lang/String;

    move-result-object v2

    .line 519
    const-string v1, "Cache: Looking for: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 520
    :goto_0
    invoke-virtual {p0}, Lemi;->n()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Ldwn;->a(Landroid/content/Context;)Ldwn;

    move-result-object v3

    .line 521
    iget-wide v4, p0, Lemi;->d:J

    invoke-virtual {v3, v2, v4, v5}, Ldwn;->a(Ljava/lang/String;J)[B

    move-result-object v1

    .line 522
    invoke-direct {p0, p1}, Lemi;->a(Lnqx;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 523
    if-nez v1, :cond_5

    .line 524
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lemi;->a(Lnqx;Z)Ljava/lang/String;

    move-result-object v2

    .line 525
    const-string v1, "Cache: Looking for: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 526
    :goto_1
    iget-wide v4, p0, Lemi;->d:J

    invoke-virtual {v3, v2, v4, v5}, Ldwn;->a(Ljava/lang/String;J)[B

    move-result-object v1

    .line 527
    if-eqz v1, :cond_0

    .line 528
    const-string v3, "PeopleViewLoader"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 535
    :cond_0
    :goto_2
    if-eqz v1, :cond_2

    .line 543
    :try_start_0
    invoke-static {v1}, Ldvx;->a([B)Ldvx;

    move-result-object v0

    .line 544
    const-string v1, "PeopleViewLoader"

    const/4 v3, 0x2

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 545
    iget-object v1, v0, Ldvx;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x28

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "loaded from cache; "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " responses"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    :cond_1
    iget-object v1, p0, Lemi;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 554
    :cond_2
    :goto_3
    return-object v0

    .line 519
    :cond_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 525
    :cond_4
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 534
    :cond_5
    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p1, Lnqx;->c:Ljava/lang/Integer;

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_3
.end method

.method public static b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lemi;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "people_view_loader_dismissed_people"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    const-string v0, "people_view_loader_dismissed_people"

    .line 81
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    sput-object v0, Lemi;->b:Ljava/util/HashSet;

    .line 83
    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lemi;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c(Ljava/util/ArrayList;)Lkfu;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnqx;",
            ">;)",
            "Lkfu;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 603
    iget v6, p0, Lemi;->c:I

    .line 612
    invoke-virtual {p0}, Lemi;->n()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lkfo;

    invoke-virtual {p0}, Lemi;->n()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v6}, Lkfo;-><init>(Landroid/content/Context;I)V

    invoke-static {v0, v1}, Lkgi;->a(Landroid/content/Context;Lkfo;)Lkfu;

    move-result-object v7

    .line 614
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 615
    const/4 v0, 0x0

    move v5, v0

    move-object v4, v2

    move-object v1, v2

    :goto_0
    if-ge v5, v8, :cond_5

    .line 616
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnqx;

    .line 619
    iget v3, v0, Lnqx;->b:I

    if-eqz v3, :cond_0

    const/16 v3, 0xf

    iget v9, v0, Lnqx;->b:I

    if-eq v3, v9, :cond_0

    const/4 v3, 0x1

    iget v9, v0, Lnqx;->b:I

    if-ne v3, v9, :cond_2

    .line 622
    :cond_0
    if-nez v1, :cond_7

    .line 623
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 624
    new-instance v1, Ldlx;

    invoke-virtual {p0}, Lemi;->n()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v1, v9, v6, v3}, Ldlx;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 627
    :goto_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v10, v1

    move-object v1, v3

    move-object v3, v10

    .line 643
    :goto_2
    if-eqz v3, :cond_1

    .line 644
    invoke-virtual {v7, v3}, Lkfu;->a(Lkff;)V

    .line 615
    :cond_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 628
    :cond_2
    const/4 v3, 0x3

    iget v9, v0, Lnqx;->b:I

    if-eq v3, v9, :cond_3

    const/4 v3, 0x2

    iget v9, v0, Lnqx;->b:I

    if-ne v3, v9, :cond_4

    .line 630
    :cond_3
    if-nez v4, :cond_6

    .line 631
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 632
    new-instance v3, Ldlx;

    invoke-virtual {p0}, Lemi;->n()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v3, v9, v6, v4}, Ldlx;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 635
    :goto_3
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 637
    :cond_4
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 639
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 640
    new-instance v3, Ldlx;

    invoke-virtual {p0}, Lemi;->n()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0, v6, v9}, Ldlx;-><init>(Landroid/content/Context;ILjava/util/List;)V

    goto :goto_2

    .line 648
    :cond_5
    return-object v7

    :cond_6
    move-object v3, v2

    goto :goto_3

    :cond_7
    move-object v3, v1

    move-object v1, v2

    goto :goto_1
.end method

.method public static l()V
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lemi;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 69
    return-void
.end method


# virtual methods
.method public bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lemi;->b(Ljava/util/ArrayList;)V

    return-void
.end method

.method public b(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 653
    invoke-virtual {p0}, Lemi;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 657
    :cond_1
    invoke-virtual {p0}, Lemi;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 658
    invoke-super {p0, p1}, Lhxz;->b(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected i()V
    .locals 1

    .prologue
    .line 683
    invoke-super {p0}, Lhxz;->i()V

    .line 685
    iget-object v0, p0, Lemi;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 686
    iget-object v0, p0, Lemi;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 688
    :cond_0
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lemi;->m()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/util/ArrayList;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    move-object/from16 v0, p0

    iget v11, v0, Lemi;->c:I

    .line 156
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 158
    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->e:Ljava/util/List;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v4, v8

    .line 398
    :goto_0
    return-object v4

    .line 165
    :cond_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 169
    const/4 v4, 0x0

    move v6, v4

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v6, v4, :cond_c

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->e:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnqx;

    .line 171
    const-string v5, "PeopleViewLoader"

    const/4 v7, 0x2

    invoke-static {v5, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 172
    move-object/from16 v0, p0

    iget-object v5, v0, Lemi;->e:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    iget v9, v4, Lnqx;->b:I

    iget-object v5, v4, Lnqx;->d:Lohl;

    if-eqz v5, :cond_4

    const-string v12, "; "

    iget-object v5, v4, Lnqx;->d:Lohl;

    iget-object v5, v5, Lohl;->a:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v13

    if-eqz v13, :cond_3

    invoke-virtual {v12, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_2
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, 0x32

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v12, " requests; processing type: "

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    :cond_2
    const/4 v5, 0x0

    invoke-static {v4, v5}, Lemi;->a(Lnqx;Z)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lemi;->f:Ljava/util/HashMap;

    invoke-virtual {v7, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ldvx;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lemi;->a(Lnqx;)Z

    move-result v7

    if-eqz v7, :cond_6

    if-nez v5, :cond_5

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lemi;->a(Lnqx;Z)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lemi;->f:Ljava/util/HashMap;

    invoke-virtual {v7, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ldvx;

    if-eqz v5, :cond_6

    const-string v7, "PeopleViewLoader"

    const/4 v9, 0x2

    invoke-static {v7, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_6

    move-object v7, v5

    :goto_3
    if-eqz v7, :cond_2f

    new-instance v5, Ldvx;

    invoke-direct {v5, v7}, Ldvx;-><init>(Ldvx;)V

    :goto_4
    if-nez v5, :cond_2e

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lemi;->b(Lnqx;)Ldvx;

    move-result-object v5

    move-object v9, v5

    .line 178
    :goto_5
    if-eqz v9, :cond_b

    .line 179
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v9}, Lemi;->a(Lnqx;Ldvx;)Z

    move-result v5

    if-nez v5, :cond_a

    .line 180
    const/4 v5, 0x0

    .line 184
    iget-object v7, v9, Ldvx;->a:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v7, v5

    :goto_6
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ldvw;

    .line 185
    invoke-static {v5}, Lemi;->a(Ldvw;)Z

    move-result v13

    or-int/2addr v7, v13

    .line 186
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 172
    :cond_3
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v12}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_4
    const-string v5, ""

    goto/16 :goto_2

    .line 177
    :cond_5
    const/16 v7, 0x32

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v4, Lnqx;->c:Ljava/lang/Integer;

    :cond_6
    move-object v7, v5

    goto :goto_3

    .line 189
    :cond_7
    if-eqz v7, :cond_a

    .line 190
    const-string v5, "PeopleViewLoader"

    const/4 v7, 0x2

    invoke-static {v5, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_8

    iget v5, v4, Lnqx;->b:I

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v12, 0x31

    invoke-direct {v7, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "need to update cache for request type="

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lemi;->g:Ljava/util/ArrayList;

    if-nez v5, :cond_9

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lemi;->g:Ljava/util/ArrayList;

    :cond_9
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lemi;->a(Lnqx;)Z

    move-result v5

    invoke-static {v4, v5}, Lemi;->a(Lnqx;Z)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lemi;->g:Ljava/util/ArrayList;

    new-instance v7, Lemk;

    invoke-virtual {v9}, Ldvx;->a()[B

    move-result-object v9

    invoke-direct {v7, v4, v9}, Lemk;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    :cond_a
    :goto_7
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto/16 :goto_1

    .line 194
    :cond_b
    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 202
    :cond_c
    new-instance v5, Landroid/os/ConditionVariable;

    invoke-direct {v5}, Landroid/os/ConditionVariable;-><init>()V

    .line 206
    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_15

    .line 207
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lemi;->c(Ljava/util/ArrayList;)Lkfu;

    move-result-object v4

    .line 208
    new-instance v6, Ljava/lang/Thread;

    new-instance v7, Lemj;

    invoke-direct {v7, v4, v5}, Lemj;-><init>(Lkfu;Landroid/os/ConditionVariable;)V

    const-string v9, "PeopleViewLoader"

    invoke-direct {v6, v7, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 215
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 226
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lemi;->n()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v11}, Ldsm;->e(Landroid/content/Context;I)Ljava/util/HashMap;

    move-result-object v12

    .line 227
    invoke-virtual {v5}, Landroid/os/ConditionVariable;->block()V

    .line 236
    if-eqz v4, :cond_27

    .line 237
    invoke-virtual {v4}, Lkfu;->j()Ljava/util/ArrayList;

    move-result-object v13

    .line 242
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 243
    const/4 v4, 0x0

    move v10, v4

    :goto_9
    if-ge v10, v14, :cond_27

    .line 244
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ldlx;

    .line 245
    const-string v5, "PeopleViewLoader"

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 246
    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x19

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "processing op "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 248
    :cond_d
    invoke-virtual {v4}, Ldlx;->t()Z

    move-result v5

    if-nez v5, :cond_26

    .line 249
    invoke-virtual {v4}, Ldlx;->b()Lnra;

    move-result-object v5

    iget-object v15, v5, Lnra;->a:[Lnqx;

    .line 250
    invoke-virtual {v4}, Ldlx;->c()Lnrk;

    move-result-object v4

    iget-object v0, v4, Lnrk;->a:[Lnri;

    move-object/from16 v16, v0

    .line 253
    const-string v4, "PeopleViewLoader"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 254
    array-length v4, v15

    move-object/from16 v0, v16

    array-length v5, v0

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x2b

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " requests; "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " responses"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    :cond_e
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 259
    array-length v0, v15

    move/from16 v18, v0

    const/4 v4, 0x0

    move v9, v4

    :goto_a
    move/from16 v0, v18

    if-ge v9, v0, :cond_26

    aget-object v19, v15, v9

    .line 260
    const-string v4, "PeopleViewLoader"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 261
    move-object/from16 v0, v19

    iget v5, v0, Lnqx;->b:I

    move-object/from16 v0, v19

    iget-object v4, v0, Lnqx;->d:Lohl;

    if-eqz v4, :cond_17

    const-string v6, "; "

    move-object/from16 v0, v19

    iget-object v4, v0, Lnqx;->d:Lohl;

    iget-object v4, v4, Lohl;->a:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_16

    invoke-virtual {v6, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_b
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x25

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "processing request, type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_f
    const/4 v4, 0x3

    move-object/from16 v0, v19

    iget v5, v0, Lnqx;->b:I

    if-eq v4, v5, :cond_10

    const/4 v4, 0x2

    move-object/from16 v0, v19

    iget v5, v0, Lnqx;->b:I

    if-ne v4, v5, :cond_11

    :cond_10
    move-object/from16 v0, v19

    iget-object v4, v0, Lnqx;->d:Lohl;

    if-nez v4, :cond_18

    :cond_11
    const/4 v4, 0x1

    move v7, v4

    .line 271
    :goto_c
    if-nez v16, :cond_19

    const/4 v4, 0x0

    .line 272
    :goto_d
    const/4 v5, 0x0

    move v6, v5

    :goto_e
    if-ge v6, v4, :cond_1d

    .line 273
    aget-object v20, v16, v6

    .line 274
    const-string v5, "PeopleViewLoader"

    const/16 v21, 0x2

    move/from16 v0, v21

    invoke-static {v5, v0}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 275
    move-object/from16 v0, v20

    iget v0, v0, Lnri;->b:I

    move/from16 v21, v0

    move-object/from16 v0, v20

    iget-object v5, v0, Lnri;->d:Lohl;

    if-eqz v5, :cond_1b

    const-string v22, "; "

    move-object/from16 v0, v20

    iget-object v5, v0, Lnri;->d:Lohl;

    iget-object v5, v5, Lohl;->a:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v23

    if-eqz v23, :cond_1a

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_f
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    add-int/lit8 v23, v23, 0x24

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v23, "checking response, type: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    :cond_12
    move-object/from16 v0, v19

    iget v5, v0, Lnqx;->b:I

    move-object/from16 v0, v20

    iget v0, v0, Lnri;->b:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ne v5, v0, :cond_14

    if-nez v7, :cond_13

    move-object/from16 v0, v19

    iget-object v5, v0, Lnqx;->d:Lohl;

    move-object/from16 v0, v20

    iget-object v0, v0, Lnri;->d:Lohl;

    move-object/from16 v21, v0

    .line 282
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v5, v1}, Lemi;->a(Lohl;Lohl;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 283
    :cond_13
    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    move-object/from16 v0, v20

    iget v5, v0, Lnri;->b:I

    const/16 v21, 0xf

    move/from16 v0, v21

    if-ne v5, v0, :cond_14

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lemi;->a(Lnqx;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 288
    move-object/from16 v0, v20

    iget-object v5, v0, Lnri;->f:Lnrg;

    if-eqz v5, :cond_1c

    move-object/from16 v0, v20

    iget-object v5, v0, Lnri;->f:Lnrg;

    iget-object v5, v5, Lnrg;->a:Ljava/lang/Boolean;

    .line 289
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1c

    const/4 v5, 0x1

    .line 290
    :goto_10
    invoke-virtual/range {p0 .. p0}, Lemi;->n()Landroid/content/Context;

    move-result-object v21

    move-object/from16 v0, v20

    iget-object v0, v0, Lnri;->c:[Lnrp;

    move-object/from16 v20, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-static {v0, v11, v1, v5}, Ldhv;->a(Landroid/content/Context;I[Lnrp;Z)V

    .line 272
    :cond_14
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto/16 :goto_e

    .line 218
    :cond_15
    const/4 v4, 0x0

    .line 221
    invoke-virtual {v5}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_8

    .line 261
    :cond_16
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_b

    :cond_17
    const-string v4, ""

    goto/16 :goto_b

    .line 266
    :cond_18
    const/4 v4, 0x0

    move v7, v4

    goto/16 :goto_c

    .line 271
    :cond_19
    move-object/from16 v0, v16

    array-length v4, v0

    goto/16 :goto_d

    .line 275
    :cond_1a
    new-instance v5, Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_f

    :cond_1b
    const-string v5, ""

    goto/16 :goto_f

    .line 289
    :cond_1c
    const/4 v5, 0x0

    goto :goto_10

    .line 296
    :cond_1d
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v7

    .line 297
    const-string v4, "PeopleViewLoader"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 298
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x1a

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "total matches: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 301
    :cond_1e
    new-instance v20, Ldvx;

    invoke-direct/range {v20 .. v20}, Ldvx;-><init>()V

    .line 303
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lemi;->a(Lnqx;)Z

    move-result v4

    move-object/from16 v0, v19

    invoke-static {v0, v4}, Lemi;->a(Lnqx;Z)Ljava/lang/String;

    move-result-object v21

    .line 304
    const/4 v4, 0x0

    move v6, v4

    :goto_11
    if-ge v6, v7, :cond_24

    .line 305
    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnri;

    .line 306
    iget-object v5, v4, Lnri;->f:Lnrg;

    if-eqz v5, :cond_22

    iget-object v5, v4, Lnri;->f:Lnrg;

    iget-object v5, v5, Lnrg;->a:Ljava/lang/Boolean;

    .line 307
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_22

    const/4 v5, 0x1

    .line 308
    :goto_12
    new-instance v22, Ldvw;

    iget v0, v4, Lnri;->b:I

    move/from16 v23, v0

    iget-object v0, v4, Lnri;->d:Lohl;

    move-object/from16 v24, v0

    new-instance v25, Ljava/util/ArrayList;

    iget-object v0, v4, Lnri;->c:[Lnrp;

    move-object/from16 v26, v0

    .line 310
    invoke-static/range {v26 .. v26}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v26

    invoke-direct/range {v25 .. v26}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, v22

    move/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    invoke-direct {v0, v1, v2, v3, v5}, Ldvw;-><init>(ILohl;Ljava/util/ArrayList;Z)V

    .line 313
    sget-object v5, Lemi;->b:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1f

    .line 314
    invoke-static/range {v22 .. v22}, Lemi;->a(Ldvw;)Z

    .line 320
    :cond_1f
    const/4 v5, 0x3

    move-object/from16 v0, v19

    iget v0, v0, Lnqx;->b:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-eq v5, v0, :cond_20

    const/4 v5, 0x2

    move-object/from16 v0, v19

    iget v0, v0, Lnqx;->b:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-ne v5, v0, :cond_23

    :cond_20
    move-object/from16 v0, v19

    iget-object v5, v0, Lnqx;->d:Lohl;

    if-nez v5, :cond_23

    .line 323
    new-instance v5, Lnqx;

    invoke-direct {v5}, Lnqx;-><init>()V

    .line 324
    move-object/from16 v0, v19

    iget v0, v0, Lnqx;->b:I

    move/from16 v23, v0

    move/from16 v0, v23

    iput v0, v5, Lnqx;->b:I

    .line 325
    iget-object v0, v4, Lnri;->d:Lohl;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iput-object v0, v5, Lnqx;->d:Lohl;

    .line 326
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lemi;->a(Lnqx;)Z

    move-result v23

    move/from16 v0, v23

    invoke-static {v5, v0}, Lemi;->a(Lnqx;Z)Ljava/lang/String;

    move-result-object v5

    .line 328
    new-instance v23, Ldvx;

    invoke-direct/range {v23 .. v23}, Ldvx;-><init>()V

    .line 329
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ldvx;->a(Ldvw;)V

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lemi;->g:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    if-nez v24, :cond_21

    .line 332
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lemi;->g:Ljava/util/ArrayList;

    .line 335
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lemi;->g:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    new-instance v25, Lemk;

    .line 336
    invoke-virtual/range {v23 .. v23}, Ldvx;->a()[B

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v0, v5, v1}, Lemk;-><init>(Ljava/lang/String;[B)V

    .line 335
    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    move-object/from16 v0, p0

    iget-object v0, v0, Lemi;->f:Ljava/util/HashMap;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    new-instance v5, Ldvw;

    iget v0, v4, Lnri;->b:I

    move/from16 v23, v0

    iget-object v4, v4, Lnri;->d:Lohl;

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    const/16 v25, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v5, v0, v4, v1, v2}, Ldvw;-><init>(ILohl;Ljava/util/ArrayList;Z)V

    .line 343
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ldvx;->a(Ldvw;)V

    .line 349
    :goto_13
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v8, v1}, Lemi;->a(Ljava/util/ArrayList;Ldvw;)V

    .line 350
    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto/16 :goto_11

    .line 307
    :cond_22
    const/4 v5, 0x0

    goto/16 :goto_12

    .line 346
    :cond_23
    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ldvx;->a(Ldvw;)V

    goto :goto_13

    .line 353
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->g:Ljava/util/ArrayList;

    if-nez v4, :cond_25

    .line 354
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lemi;->g:Ljava/util/ArrayList;

    .line 357
    :cond_25
    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->g:Ljava/util/ArrayList;

    new-instance v5, Lemk;

    .line 358
    invoke-virtual/range {v20 .. v20}, Ldvx;->a()[B

    move-result-object v6

    move-object/from16 v0, v21

    invoke-direct {v5, v0, v6}, Lemk;-><init>(Ljava/lang/String;[B)V

    .line 357
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->f:Ljava/util/HashMap;

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->clear()V

    .line 259
    add-int/lit8 v4, v9, 0x1

    move v9, v4

    goto/16 :goto_a

    .line 243
    :cond_26
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto/16 :goto_9

    .line 368
    :cond_27
    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->g:Ljava/util/ArrayList;

    if-eqz v4, :cond_2a

    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2a

    .line 369
    invoke-virtual/range {p0 .. p0}, Lemi;->n()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Ldwn;->a(Landroid/content/Context;)Ldwn;

    move-result-object v5

    .line 373
    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_14
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_29

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lemk;

    .line 374
    const-string v7, "PeopleViewLoader"

    const/4 v9, 0x2

    invoke-static {v7, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_28

    .line 375
    iget-object v7, v4, Lemk;->a:Ljava/lang/String;

    iget-object v9, v4, Lemk;->b:[B

    array-length v9, v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x1c

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "   saving "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, "; size="

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 377
    :cond_28
    iget-object v7, v4, Lemk;->a:Ljava/lang/String;

    iget-object v4, v4, Lemk;->b:[B

    invoke-virtual {v5, v7, v4}, Ldwn;->a(Ljava/lang/String;[B)V

    goto :goto_14

    .line 379
    :cond_29
    move-object/from16 v0, p0

    iget-object v4, v0, Lemi;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 386
    :cond_2a
    invoke-static {v8, v12}, Ldsm;->a(Ljava/util/ArrayList;Ljava/util/HashMap;)V

    .line 391
    const-string v4, "PeopleViewLoader"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 392
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_15
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ldvw;

    .line 393
    iget v7, v4, Ldvw;->a:I

    iget-object v5, v4, Ldvw;->b:Lohl;

    if-eqz v5, :cond_2c

    const-string v9, "; "

    iget-object v5, v4, Ldvw;->b:Lohl;

    iget-object v5, v5, Lohl;->a:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_2b

    invoke-virtual {v9, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :goto_16
    iget-object v4, v4, Ldvw;->c:Ljava/util/ArrayList;

    .line 395
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x2e

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "returning type="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "; "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " people"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_15

    .line 393
    :cond_2b
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_16

    :cond_2c
    const-string v5, ""

    goto :goto_16

    :cond_2d
    move-object v4, v8

    .line 398
    goto/16 :goto_0

    :cond_2e
    move-object v9, v5

    goto/16 :goto_5

    :cond_2f
    move-object v5, v7

    goto/16 :goto_4
.end method
