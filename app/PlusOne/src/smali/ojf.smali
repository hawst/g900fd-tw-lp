.class public final Lojf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field private c:Ljava/lang/String;

.field private d:Lohu;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Loxq;-><init>()V

    .line 130
    const/high16 v0, -0x80000000

    iput v0, p0, Lojf;->b:I

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lojf;->d:Lohu;

    .line 117
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 156
    const/4 v0, 0x0

    .line 157
    iget-object v1, p0, Lojf;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 158
    const/4 v0, 0x1

    iget-object v1, p0, Lojf;->a:Ljava/lang/String;

    .line 159
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 161
    :cond_0
    iget-object v1, p0, Lojf;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 162
    const/4 v1, 0x2

    iget-object v2, p0, Lojf;->c:Ljava/lang/String;

    .line 163
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_1
    iget v1, p0, Lojf;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 166
    const/4 v1, 0x3

    iget v2, p0, Lojf;->b:I

    .line 167
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_2
    iget-object v1, p0, Lojf;->d:Lohu;

    if-eqz v1, :cond_3

    .line 170
    const/4 v1, 0x4

    iget-object v2, p0, Lojf;->d:Lohu;

    .line 171
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_3
    iget-object v1, p0, Lojf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    iput v0, p0, Lojf;->ai:I

    .line 175
    return v0
.end method

.method public a(Loxn;)Lojf;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 183
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 184
    sparse-switch v0, :sswitch_data_0

    .line 188
    iget-object v1, p0, Lojf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 189
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lojf;->ah:Ljava/util/List;

    .line 192
    :cond_1
    iget-object v1, p0, Lojf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    :sswitch_0
    return-object p0

    .line 199
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojf;->a:Ljava/lang/String;

    goto :goto_0

    .line 203
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojf;->c:Ljava/lang/String;

    goto :goto_0

    .line 207
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 208
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 211
    :cond_2
    iput v0, p0, Lojf;->b:I

    goto :goto_0

    .line 213
    :cond_3
    iput v2, p0, Lojf;->b:I

    goto :goto_0

    .line 218
    :sswitch_4
    iget-object v0, p0, Lojf;->d:Lohu;

    if-nez v0, :cond_4

    .line 219
    new-instance v0, Lohu;

    invoke-direct {v0}, Lohu;-><init>()V

    iput-object v0, p0, Lojf;->d:Lohu;

    .line 221
    :cond_4
    iget-object v0, p0, Lojf;->d:Lohu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 184
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lojf;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x1

    iget-object v1, p0, Lojf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 141
    :cond_0
    iget-object v0, p0, Lojf;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 142
    const/4 v0, 0x2

    iget-object v1, p0, Lojf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 144
    :cond_1
    iget v0, p0, Lojf;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 145
    const/4 v0, 0x3

    iget v1, p0, Lojf;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 147
    :cond_2
    iget-object v0, p0, Lojf;->d:Lohu;

    if-eqz v0, :cond_3

    .line 148
    const/4 v0, 0x4

    iget-object v1, p0, Lojf;->d:Lohu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 150
    :cond_3
    iget-object v0, p0, Lojf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 152
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Lojf;->a(Loxn;)Lojf;

    move-result-object v0

    return-object v0
.end method
