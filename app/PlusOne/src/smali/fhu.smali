.class public final Lfhu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[J

.field private static final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    new-array v0, v0, [J

    sput-object v0, Lfhu;->a:[J

    .line 28
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lfhu;->b:Landroid/util/SparseArray;

    return-void
.end method

.method public static a()I
    .locals 2

    .prologue
    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public static a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/content/Intent;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 148
    const-string v0, "com.google.android.libraries.social.notifications.FROM_NOTIFICATION"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 149
    const-string v0, "skip_interstitials"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 150
    invoke-static {p0, p1}, Ldhv;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    new-instance v0, Livy;

    invoke-direct {v0, p0}, Livy;-><init>(Landroid/content/Context;)V

    new-instance v1, Liwg;

    invoke-direct {v1}, Liwg;-><init>()V

    .line 154
    invoke-virtual {v1, p1}, Liwg;->a(I)Liwg;

    move-result-object v1

    .line 153
    invoke-virtual {v0, v1}, Livy;->a(Liwg;)Livy;

    move-result-object v0

    .line 156
    invoke-virtual {v0, p2}, Livy;->a(Landroid/content/Intent;)Livy;

    move-result-object v0

    .line 157
    invoke-virtual {v0}, Livy;->a()Landroid/content/Intent;

    move-result-object p2

    .line 160
    :cond_0
    return-object p2
.end method

.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 34
    const-string v0, "prefetch_notifications"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(IJ)V
    .locals 3

    .prologue
    .line 42
    sget-object v0, Lfhu;->b:Landroid/util/SparseArray;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 43
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;I)V
    .locals 6

    .prologue
    .line 66
    const-class v2, Lfhu;

    monitor-enter v2

    :try_start_0
    invoke-static {p0, p1}, Ljlp;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    .line 68
    const-string v0, "notification"

    .line 69
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 70
    sget-object v4, Lfht;->a:[I

    const/4 v1, 0x0

    :goto_0
    const/4 v5, 0x5

    if-ge v1, v5, :cond_0

    aget v5, v4, v1

    .line 71
    invoke-virtual {v0, v3, v5}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 74
    :cond_0
    invoke-static {p0, p1}, Lhsb;->a(Landroid/content/Context;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit v2

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;II)V
    .locals 3

    .prologue
    .line 81
    const-class v1, Lfhu;

    monitor-enter v1

    :try_start_0
    invoke-static {p0, p1}, Ljlp;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 83
    const-string v0, "notification"

    .line 84
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 85
    invoke-virtual {v0, v2, p2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    monitor-exit v1

    return-void

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;ILandroid/app/Notification;IZ)V
    .locals 2

    .prologue
    .line 170
    iget v0, p2, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p2, Landroid/app/Notification;->flags:I

    .line 171
    iget v0, p2, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p2, Landroid/app/Notification;->flags:I

    .line 172
    iget v0, p2, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p2, Landroid/app/Notification;->flags:I

    .line 173
    const/4 v0, -0x1

    iput v0, p2, Landroid/app/Notification;->ledARGB:I

    .line 174
    const/16 v0, 0x1f4

    iput v0, p2, Landroid/app/Notification;->ledOnMS:I

    .line 175
    const/16 v0, 0x7d0

    iput v0, p2, Landroid/app/Notification;->ledOffMS:I

    .line 178
    invoke-static {p0, p1}, Leum;->b(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p4, :cond_0

    .line 179
    iget v0, p2, Landroid/app/Notification;->defaults:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p2, Landroid/app/Notification;->defaults:I

    .line 184
    :goto_0
    invoke-static {p0, p1}, Leum;->d(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p4, :cond_1

    .line 185
    invoke-static {p0, p1}, Leum;->c(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p2, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 190
    :goto_1
    const-string v0, "notification"

    .line 191
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 193
    invoke-static {p0, p1}, Ljlp;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 195
    return-void

    .line 181
    :cond_0
    sget-object v0, Lfhu;->a:[J

    iput-object v0, p2, Landroid/app/Notification;->vibrate:[J

    goto :goto_0

    .line 187
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p2, Landroid/app/Notification;->sound:Landroid/net/Uri;

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ILandroid/content/Intent;Lda;IJZZZZLjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Landroid/content/Intent;",
            "Lda;",
            "IJZZZZ",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 97
    if-eqz p2, :cond_2

    .line 100
    const-string v0, "com.google.android.libraries.social.notifications.updated_version"

    invoke-virtual {p2, v0, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 101
    const-string v0, "com.google.android.libraries.social.notifications.NOTIFICATION_READ"

    invoke-virtual {p2, v0, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 102
    invoke-virtual {p11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    const-string v2, "com.google.android.libraries.social.notifications.notif_id"

    const/4 v0, 0x0

    invoke-virtual {p11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    :cond_0
    const-string v0, "com.google.android.libraries.social.notifications.coalescing_codes"

    invoke-virtual {p2, v0, p11}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 107
    const-string v0, "com.google.android.libraries.social.notifications.ext_ids"

    invoke-virtual {p2, v0, p12}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 111
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    const-string v2, "com.google.android.libraries.social.notifications.notif_types"

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 115
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    if-eqz p3, :cond_2

    .line 119
    invoke-virtual {p3}, Lda;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 122
    if-nez p8, :cond_3

    .line 123
    invoke-static {p0, p1}, Leyq;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 130
    :goto_0
    if-eqz v0, :cond_1

    .line 132
    invoke-static {p0, p1, v0}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 131
    invoke-virtual {p3, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 136
    :cond_1
    const-string v0, "com.google.android.libraries.social.notifications.FROM_ANDROID_NOTIFICATION"

    invoke-virtual {p2, v0, p10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 139
    invoke-static {p0, p1, p2}, Lfhu;->a(Landroid/content/Context;ILandroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p3, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 142
    :cond_2
    return-void

    .line 124
    :cond_3
    if-eqz p9, :cond_4

    .line 125
    invoke-static {p0, p1, v1}, Leyq;->a(Landroid/content/Context;ILandroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 127
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 46
    const-string v0, "prefetch_notifications"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static b(I)Ljava/lang/Long;
    .locals 4

    .prologue
    .line 38
    sget-object v0, Lfhu;->b:Landroid/util/SparseArray;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method
