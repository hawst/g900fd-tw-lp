.class public final Lmok;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lmol;

.field private c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2395
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2402
    const/4 v0, 0x0

    iput-object v0, p0, Lmok;->b:Lmol;

    .line 2395
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 2422
    const/4 v0, 0x0

    .line 2423
    iget-object v1, p0, Lmok;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2424
    const/4 v0, 0x1

    iget-object v1, p0, Lmok;->a:Ljava/lang/String;

    .line 2425
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2427
    :cond_0
    iget-object v1, p0, Lmok;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 2428
    const/4 v1, 0x2

    iget-object v2, p0, Lmok;->c:Ljava/lang/Long;

    .line 2429
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2431
    :cond_1
    iget-object v1, p0, Lmok;->b:Lmol;

    if-eqz v1, :cond_2

    .line 2432
    const/4 v1, 0x3

    iget-object v2, p0, Lmok;->b:Lmol;

    .line 2433
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2435
    :cond_2
    iget-object v1, p0, Lmok;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2436
    iput v0, p0, Lmok;->ai:I

    .line 2437
    return v0
.end method

.method public a(Loxn;)Lmok;
    .locals 2

    .prologue
    .line 2445
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2446
    sparse-switch v0, :sswitch_data_0

    .line 2450
    iget-object v1, p0, Lmok;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2451
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmok;->ah:Ljava/util/List;

    .line 2454
    :cond_1
    iget-object v1, p0, Lmok;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2456
    :sswitch_0
    return-object p0

    .line 2461
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmok;->a:Ljava/lang/String;

    goto :goto_0

    .line 2465
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmok;->c:Ljava/lang/Long;

    goto :goto_0

    .line 2469
    :sswitch_3
    iget-object v0, p0, Lmok;->b:Lmol;

    if-nez v0, :cond_2

    .line 2470
    new-instance v0, Lmol;

    invoke-direct {v0}, Lmol;-><init>()V

    iput-object v0, p0, Lmok;->b:Lmol;

    .line 2472
    :cond_2
    iget-object v0, p0, Lmok;->b:Lmol;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2446
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 2407
    iget-object v0, p0, Lmok;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2408
    const/4 v0, 0x1

    iget-object v1, p0, Lmok;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2410
    :cond_0
    iget-object v0, p0, Lmok;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2411
    const/4 v0, 0x2

    iget-object v1, p0, Lmok;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2413
    :cond_1
    iget-object v0, p0, Lmok;->b:Lmol;

    if-eqz v0, :cond_2

    .line 2414
    const/4 v0, 0x3

    iget-object v1, p0, Lmok;->b:Lmol;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2416
    :cond_2
    iget-object v0, p0, Lmok;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2418
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2391
    invoke-virtual {p0, p1}, Lmok;->a(Loxn;)Lmok;

    move-result-object v0

    return-object v0
.end method
