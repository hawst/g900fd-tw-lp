.class public final Lnor;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lnoy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3071
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3074
    sget-object v0, Lnoy;->a:[Lnoy;

    iput-object v0, p0, Lnor;->a:[Lnoy;

    .line 3071
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3092
    .line 3093
    iget-object v1, p0, Lnor;->a:[Lnoy;

    if-eqz v1, :cond_1

    .line 3094
    iget-object v2, p0, Lnor;->a:[Lnoy;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3095
    if-eqz v4, :cond_0

    .line 3096
    const/4 v5, 0x2

    .line 3097
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3094
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3101
    :cond_1
    iget-object v1, p0, Lnor;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3102
    iput v0, p0, Lnor;->ai:I

    .line 3103
    return v0
.end method

.method public a(Loxn;)Lnor;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3111
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3112
    sparse-switch v0, :sswitch_data_0

    .line 3116
    iget-object v2, p0, Lnor;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 3117
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnor;->ah:Ljava/util/List;

    .line 3120
    :cond_1
    iget-object v2, p0, Lnor;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3122
    :sswitch_0
    return-object p0

    .line 3127
    :sswitch_1
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3128
    iget-object v0, p0, Lnor;->a:[Lnoy;

    if-nez v0, :cond_3

    move v0, v1

    .line 3129
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnoy;

    .line 3130
    iget-object v3, p0, Lnor;->a:[Lnoy;

    if-eqz v3, :cond_2

    .line 3131
    iget-object v3, p0, Lnor;->a:[Lnoy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3133
    :cond_2
    iput-object v2, p0, Lnor;->a:[Lnoy;

    .line 3134
    :goto_2
    iget-object v2, p0, Lnor;->a:[Lnoy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 3135
    iget-object v2, p0, Lnor;->a:[Lnoy;

    new-instance v3, Lnoy;

    invoke-direct {v3}, Lnoy;-><init>()V

    aput-object v3, v2, v0

    .line 3136
    iget-object v2, p0, Lnor;->a:[Lnoy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3137
    invoke-virtual {p1}, Loxn;->a()I

    .line 3134
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3128
    :cond_3
    iget-object v0, p0, Lnor;->a:[Lnoy;

    array-length v0, v0

    goto :goto_1

    .line 3140
    :cond_4
    iget-object v2, p0, Lnor;->a:[Lnoy;

    new-instance v3, Lnoy;

    invoke-direct {v3}, Lnoy;-><init>()V

    aput-object v3, v2, v0

    .line 3141
    iget-object v2, p0, Lnor;->a:[Lnoy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3112
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3079
    iget-object v0, p0, Lnor;->a:[Lnoy;

    if-eqz v0, :cond_1

    .line 3080
    iget-object v1, p0, Lnor;->a:[Lnoy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 3081
    if-eqz v3, :cond_0

    .line 3082
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 3080
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3086
    :cond_1
    iget-object v0, p0, Lnor;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3088
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3067
    invoke-virtual {p0, p1}, Lnor;->a(Loxn;)Lnor;

    move-result-object v0

    return-object v0
.end method
