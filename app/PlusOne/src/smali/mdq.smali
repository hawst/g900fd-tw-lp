.class public final Lmdq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lntf;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15793
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15796
    iput-object v0, p0, Lmdq;->apiHeader:Llyq;

    .line 15799
    iput-object v0, p0, Lmdq;->a:Lntf;

    .line 15793
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 15816
    const/4 v0, 0x0

    .line 15817
    iget-object v1, p0, Lmdq;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 15818
    const/4 v0, 0x1

    iget-object v1, p0, Lmdq;->apiHeader:Llyq;

    .line 15819
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15821
    :cond_0
    iget-object v1, p0, Lmdq;->a:Lntf;

    if-eqz v1, :cond_1

    .line 15822
    const/4 v1, 0x2

    iget-object v2, p0, Lmdq;->a:Lntf;

    .line 15823
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15825
    :cond_1
    iget-object v1, p0, Lmdq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15826
    iput v0, p0, Lmdq;->ai:I

    .line 15827
    return v0
.end method

.method public a(Loxn;)Lmdq;
    .locals 2

    .prologue
    .line 15835
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 15836
    sparse-switch v0, :sswitch_data_0

    .line 15840
    iget-object v1, p0, Lmdq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 15841
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdq;->ah:Ljava/util/List;

    .line 15844
    :cond_1
    iget-object v1, p0, Lmdq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 15846
    :sswitch_0
    return-object p0

    .line 15851
    :sswitch_1
    iget-object v0, p0, Lmdq;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 15852
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmdq;->apiHeader:Llyq;

    .line 15854
    :cond_2
    iget-object v0, p0, Lmdq;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15858
    :sswitch_2
    iget-object v0, p0, Lmdq;->a:Lntf;

    if-nez v0, :cond_3

    .line 15859
    new-instance v0, Lntf;

    invoke-direct {v0}, Lntf;-><init>()V

    iput-object v0, p0, Lmdq;->a:Lntf;

    .line 15861
    :cond_3
    iget-object v0, p0, Lmdq;->a:Lntf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 15836
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 15804
    iget-object v0, p0, Lmdq;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 15805
    const/4 v0, 0x1

    iget-object v1, p0, Lmdq;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15807
    :cond_0
    iget-object v0, p0, Lmdq;->a:Lntf;

    if-eqz v0, :cond_1

    .line 15808
    const/4 v0, 0x2

    iget-object v1, p0, Lmdq;->a:Lntf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 15810
    :cond_1
    iget-object v0, p0, Lmdq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 15812
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 15789
    invoke-virtual {p0, p1}, Lmdq;->a(Loxn;)Lmdq;

    move-result-object v0

    return-object v0
.end method
