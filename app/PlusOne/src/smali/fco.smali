.class final Lfco;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;
.implements Lheg;
.implements Ligy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lheg;",
        "Ligy;"
    }
.end annotation


# instance fields
.field a:Livx;

.field private b:Landroid/app/Activity;

.field private c:Ligv;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 88
    new-instance v0, Lhye;

    iget-object v1, p0, Lfco;->b:Landroid/app/Activity;

    iget-object v2, p0, Lfco;->b:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "account_name"

    aput-object v6, v3, v5

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lhye;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 3

    .prologue
    .line 75
    iput-object p1, p0, Lfco;->b:Landroid/app/Activity;

    .line 76
    iput-object p3, p0, Lfco;->c:Ligv;

    .line 77
    invoke-virtual {p4, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lfco;->a:Livx;

    .line 79
    check-cast p1, Lz;

    invoke-virtual {p1}, Lz;->g()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 80
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 94
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    :cond_0
    iget-object v0, p0, Lfco;->c:Ligv;

    iget-object v1, p0, Lfco;->b:Landroid/app/Activity;

    const v2, 0x7f0a0884

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ligv;->a(ILjava/lang/String;)V

    .line 108
    :goto_0
    return-void

    .line 99
    :cond_1
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 100
    new-instance v1, Lfcp;

    invoke-direct {v1, p0, v0}, Lfcp;-><init>(Lfco;Ljava/lang/String;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 63
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lfco;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(ZIIII)V
    .locals 4

    .prologue
    .line 118
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    .line 119
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfco;->b:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 120
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lfco;->b:Landroid/app/Activity;

    const-class v3, Lcom/google/android/apps/plus/phone/ProfileActionGatewayActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 121
    const/high16 v1, 0x2800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 123
    const-string v1, "account_id"

    iget-object v2, p0, Lfco;->a:Livx;

    invoke-virtual {v2}, Livx;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    iget-object v1, p0, Lfco;->c:Ligv;

    invoke-interface {v1, v0}, Ligv;->a(Landroid/content/Intent;)V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lfco;->c:Ligv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ligv;->a(I)V

    goto :goto_0
.end method
