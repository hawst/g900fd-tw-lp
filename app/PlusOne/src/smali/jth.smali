.class public final Ljth;
.super Llol;
.source "PG"

# interfaces
.implements Lhio;
.implements Lhyz;
.implements Lhzq;
.implements Ljoy;
.implements Ljpa;
.implements Lllh;


# instance fields
.field private N:Ljqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljqc",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field

.field private O:Lhzb;

.field private P:I

.field private Q:Ljpr;

.field private R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljsm;",
            ">;"
        }
    .end annotation
.end field

.field private S:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljod;",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;>;"
        }
    .end annotation
.end field

.field private T:Ljava/lang/String;

.field private U:Lhzl;

.field private V:Lllg;

.field private W:Ljqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljqc",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field

.field private X:Lhhx;

.field private Y:Ljqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljqc",
            "<",
            "Ljod;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Llol;-><init>()V

    .line 50
    sget-object v0, Ljof;->b:Ljqc;

    iput-object v0, p0, Ljth;->N:Ljqc;

    .line 61
    iget-object v0, p0, Ljth;->N:Ljqc;

    iput-object v0, p0, Ljth;->W:Ljqc;

    .line 64
    new-instance v0, Ljti;

    invoke-direct {v0, p0}, Ljti;-><init>(Ljth;)V

    iput-object v0, p0, Ljth;->Y:Ljqc;

    .line 76
    return-void
.end method

.method static synthetic a(Ljth;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ljth;->T:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljod;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 266
    if-eqz p1, :cond_4

    .line 267
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 268
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 269
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljod;

    .line 270
    iget-object v1, p0, Ljth;->U:Lhzl;

    instance-of v1, v1, Lhzk;

    if-eqz v1, :cond_0

    .line 271
    iget-object v1, p0, Ljth;->U:Lhzl;

    check-cast v1, Lhzk;

    const/16 v4, 0xaa

    .line 272
    invoke-interface {v0}, Ljod;->a()Ljava/lang/String;

    move-result-object v5

    .line 271
    invoke-interface {v1, v4, v5}, Lhzk;->a(ILjava/lang/String;)Z

    move-result v1

    .line 273
    if-nez v1, :cond_1

    .line 274
    :cond_0
    invoke-static {}, Ljsm;->a()Ljso;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljso;->a(Ljod;)Ljso;

    move-result-object v0

    invoke-virtual {v0}, Ljso;->a()Ljsm;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 279
    :cond_2
    iput-object v3, p0, Ljth;->R:Ljava/util/List;

    .line 283
    :goto_1
    iget-object v0, p0, Ljth;->O:Lhzb;

    if-eqz v0, :cond_3

    .line 284
    iget-object v0, p0, Ljth;->O:Lhzb;

    invoke-interface {v0}, Lhzb;->e()V

    .line 286
    :cond_3
    return-void

    .line 281
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Ljth;->R:Ljava/util/List;

    goto :goto_1
.end method

.method static synthetic b(Ljth;)Ljqc;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Ljth;->W:Ljqc;

    return-object v0
.end method


# virtual methods
.method public U()V
    .locals 4

    .prologue
    .line 259
    iget-object v0, p0, Ljth;->Q:Ljpr;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Ljth;->Q:Ljpr;

    iget v1, p0, Ljth;->P:I

    const/4 v2, 0x3

    iget-object v3, p0, Ljth;->W:Ljqc;

    invoke-interface {v0, p0, v1, v2, v3}, Ljpr;->a(Ljpa;IILjqc;)V

    .line 263
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    return v0
.end method

.method public a(I)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Ljth;->R:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public a(ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Ljth;->R:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsm;

    invoke-virtual {v0}, Ljsm;->b()Ljod;

    move-result-object v1

    .line 227
    check-cast p2, Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;

    iget-object v0, p0, Ljth;->S:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljth;->S:Ljava/util/Map;

    .line 228
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 227
    :goto_0
    invoke-virtual {p2, v1, v0}, Lcom/google/android/libraries/social/people/providers/search/CircleSearchRowView;->a(Ljod;Ljava/util/List;)V

    .line 229
    return-void

    .line 228
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 105
    if-eqz p1, :cond_0

    .line 106
    const-string v0, "state_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljth;->T:Ljava/lang/String;

    .line 108
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Ljth;->U:Lhzl;

    if-eqz v0, :cond_1

    instance-of v0, p1, Ljsm;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Ljth;->U:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->c(Landroid/os/Parcelable;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    iget-object v0, p0, Ljth;->U:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->b(Landroid/os/Parcelable;)Z

    .line 93
    :goto_0
    iget-object v0, p0, Ljth;->V:Lllg;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Ljth;->V:Lllg;

    invoke-virtual {v0}, Lllg;->a()V

    .line 96
    :cond_0
    iget-object v0, p0, Ljth;->X:Lhhx;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Ljth;->X:Lhhx;

    invoke-virtual {v0}, Lhhx;->b()V

    .line 100
    :cond_1
    return-void

    .line 90
    :cond_2
    iget-object v0, p0, Ljth;->U:Lhzl;

    invoke-interface {v0, p1}, Lhzl;->a(Landroid/os/Parcelable;)Z

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 234
    return-void
.end method

.method public a(Lhzb;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Ljth;->O:Lhzb;

    .line 249
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 290
    if-eqz p1, :cond_0

    .line 291
    iget-object v0, p0, Ljth;->T:Ljava/lang/String;

    .line 292
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ljth;->T:Ljava/lang/String;

    .line 293
    iget-object v1, p0, Ljth;->T:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Ljth;->T:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 295
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ljth;->a(Ljava/util/List;)V

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    invoke-virtual {p0}, Ljth;->e()V

    goto :goto_0
.end method

.method public a(Ljob;)V
    .locals 1

    .prologue
    .line 125
    invoke-interface {p1}, Ljob;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Ljth;->a(Ljava/util/List;)V

    .line 126
    invoke-interface {p1}, Ljob;->b()V

    .line 127
    return-void
.end method

.method public a(Ljqh;)V
    .locals 5

    .prologue
    .line 136
    .line 137
    invoke-interface {p1}, Ljqh;->a()Ljava/util/List;

    move-result-object v2

    .line 138
    invoke-interface {p1}, Ljqh;->b()V

    .line 140
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ljth;->S:Ljava/util/Map;

    .line 141
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 142
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 143
    iget-object v3, p0, Ljth;->S:Ljava/util/Map;

    iget-object v4, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 146
    :cond_0
    iget-object v0, p0, Ljth;->O:Lhzb;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Ljth;->O:Lhzb;

    invoke-interface {v0}, Lhzb;->e()V

    .line 149
    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 80
    if-eqz p1, :cond_0

    sget-object v0, Ljof;->d:Ljqc;

    :goto_0
    iput-object v0, p0, Ljth;->W:Ljqc;

    .line 81
    invoke-virtual {p0}, Ljth;->e()V

    .line 82
    return-void

    .line 80
    :cond_0
    iget-object v0, p0, Ljth;->N:Ljqc;

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Landroid/content/Context;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 204
    invoke-virtual {p0, v2}, Ljth;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040064

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljob;)V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public c(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 160
    iget-object v0, p0, Ljth;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    iput v0, p0, Ljth;->P:I

    .line 161
    iget-object v0, p0, Ljth;->au:Llnh;

    const-class v1, Ljpr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpr;

    iput-object v0, p0, Ljth;->Q:Ljpr;

    .line 163
    iget-object v0, p0, Ljth;->au:Llnh;

    const-class v1, Lllg;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllg;

    iput-object v0, p0, Ljth;->V:Lllg;

    .line 164
    iget-object v0, p0, Ljth;->V:Lllg;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Ljth;->V:Lllg;

    invoke-virtual {v0, p0}, Lllg;->a(Lllh;)V

    .line 168
    :cond_0
    iget-object v0, p0, Ljth;->au:Llnh;

    const-class v1, Lhzl;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Ljth;->U:Lhzl;

    .line 171
    iget-object v0, p0, Ljth;->au:Llnh;

    const-class v1, Lhin;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhin;

    .line 172
    if-eqz v0, :cond_1

    .line 173
    invoke-virtual {v0, p0}, Lhin;->a(Lhio;)V

    .line 177
    :cond_1
    iget-object v0, p0, Ljth;->au:Llnh;

    const-class v1, Ljsb;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljsb;

    .line 178
    if-eqz v0, :cond_2

    .line 179
    iget-object v1, v0, Ljsb;->a:Ljqc;

    if-eqz v1, :cond_2

    .line 180
    iget-object v0, v0, Ljsb;->a:Ljqc;

    iput-object v0, p0, Ljth;->N:Ljqc;

    .line 181
    iget-object v0, p0, Ljth;->N:Ljqc;

    iput-object v0, p0, Ljth;->W:Ljqc;

    .line 184
    :cond_2
    iget-object v0, p0, Ljth;->au:Llnh;

    const-class v1, Lhhx;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhx;

    iput-object v0, p0, Ljth;->X:Lhhx;

    .line 185
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Ljth;->R:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ljth;->R:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 253
    iget-object v0, p0, Ljth;->Q:Ljpr;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Ljth;->Q:Ljpr;

    iget v1, p0, Ljth;->P:I

    iget-object v2, p0, Ljth;->Y:Ljqc;

    invoke-interface {v0, p0, v1, v2}, Ljpr;->a(Ljoy;ILjqc;)V

    .line 256
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 120
    const-string v0, "state_query"

    iget-object v1, p0, Ljth;->T:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0}, Llol;->g()V

    .line 113
    invoke-virtual {p0}, Ljth;->U()V

    .line 114
    invoke-virtual {p0}, Ljth;->e()V

    .line 115
    return-void
.end method
