.class public final Lofn;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lofn;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5543
    const/4 v0, 0x0

    new-array v0, v0, [Lofn;

    sput-object v0, Lofn;->a:[Lofn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5544
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5565
    const/4 v0, 0x0

    .line 5566
    iget-object v1, p0, Lofn;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5567
    const/4 v0, 0x1

    iget-object v1, p0, Lofn;->b:Ljava/lang/String;

    .line 5568
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5570
    :cond_0
    iget-object v1, p0, Lofn;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5571
    const/4 v1, 0x2

    iget-object v2, p0, Lofn;->c:Ljava/lang/String;

    .line 5572
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5574
    :cond_1
    iget-object v1, p0, Lofn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5575
    iput v0, p0, Lofn;->ai:I

    .line 5576
    return v0
.end method

.method public a(Loxn;)Lofn;
    .locals 2

    .prologue
    .line 5584
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5585
    sparse-switch v0, :sswitch_data_0

    .line 5589
    iget-object v1, p0, Lofn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5590
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofn;->ah:Ljava/util/List;

    .line 5593
    :cond_1
    iget-object v1, p0, Lofn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5595
    :sswitch_0
    return-object p0

    .line 5600
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofn;->b:Ljava/lang/String;

    goto :goto_0

    .line 5604
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofn;->c:Ljava/lang/String;

    goto :goto_0

    .line 5585
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5553
    iget-object v0, p0, Lofn;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 5554
    const/4 v0, 0x1

    iget-object v1, p0, Lofn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5556
    :cond_0
    iget-object v0, p0, Lofn;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5557
    const/4 v0, 0x2

    iget-object v1, p0, Lofn;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5559
    :cond_1
    iget-object v0, p0, Lofn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5561
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5540
    invoke-virtual {p0, p1}, Lofn;->a(Loxn;)Lofn;

    move-result-object v0

    return-object v0
.end method
