.class Lhhc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljod;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lhhc;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lhxc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lhhd;

    invoke-direct {v0}, Lhhd;-><init>()V

    sput-object v0, Lhhc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-class v0, Lhhc;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhxc;

    iput-object v0, p0, Lhhc;->a:Lhxc;

    .line 22
    return-void
.end method

.method constructor <init>(Lhxc;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lhhc;->a:Lhxc;

    .line 18
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lhhc;->a:Lhxc;

    invoke-virtual {v0}, Lhxc;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lhhc;->a:Lhxc;

    invoke-virtual {v0}, Lhxc;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lhhc;->a:Lhxc;

    invoke-virtual {v0}, Lhxc;->c()I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lhhc;->a:Lhxc;

    invoke-virtual {v0}, Lhxc;->d()I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 63
    if-eqz p1, :cond_0

    instance-of v0, p1, Ljod;

    if-nez v0, :cond_1

    .line 64
    :cond_0
    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    .line 66
    :cond_1
    check-cast p1, Ljod;

    .line 67
    if-ne p1, p0, :cond_2

    .line 68
    const/4 v0, 0x1

    goto :goto_0

    .line 72
    :cond_2
    iget-object v0, p0, Lhhc;->a:Lhxc;

    invoke-virtual {v0}, Lhxc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljod;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lhhc;->a:Lhxc;

    invoke-virtual {v0}, Lhxc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lhhc;->a:Lhxc;

    invoke-virtual {v0}, Lhxc;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lhhc;->a:Lhxc;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 94
    return-void
.end method
