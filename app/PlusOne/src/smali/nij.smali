.class public final Lnij;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnij;


# instance fields
.field public b:I

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Double;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field private i:Lnja;

.field private j:Lnja;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/Long;

.field private n:[Lnii;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2604
    const/4 v0, 0x0

    new-array v0, v0, [Lnij;

    sput-object v0, Lnij;->a:[Lnij;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2605
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2613
    iput-object v0, p0, Lnij;->i:Lnja;

    .line 2616
    iput-object v0, p0, Lnij;->j:Lnja;

    .line 2619
    const/high16 v0, -0x80000000

    iput v0, p0, Lnij;->b:I

    .line 2640
    sget-object v0, Lnii;->a:[Lnii;

    iput-object v0, p0, Lnij;->n:[Lnii;

    .line 2605
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2694
    .line 2695
    iget v0, p0, Lnij;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_d

    .line 2696
    const/4 v0, 0x2

    iget v2, p0, Lnij;->b:I

    .line 2697
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2699
    :goto_0
    iget-object v2, p0, Lnij;->c:Ljava/lang/Double;

    if-eqz v2, :cond_0

    .line 2700
    const/4 v2, 0x3

    iget-object v3, p0, Lnij;->c:Ljava/lang/Double;

    .line 2701
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 2703
    :cond_0
    iget-object v2, p0, Lnij;->d:Ljava/lang/Double;

    if-eqz v2, :cond_1

    .line 2704
    const/4 v2, 0x4

    iget-object v3, p0, Lnij;->d:Ljava/lang/Double;

    .line 2705
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 2707
    :cond_1
    iget-object v2, p0, Lnij;->e:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 2708
    const/4 v2, 0x5

    iget-object v3, p0, Lnij;->e:Ljava/lang/Long;

    .line 2709
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2711
    :cond_2
    iget-object v2, p0, Lnij;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 2712
    const/4 v2, 0x6

    iget-object v3, p0, Lnij;->f:Ljava/lang/Integer;

    .line 2713
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2715
    :cond_3
    iget-object v2, p0, Lnij;->g:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 2716
    const/4 v2, 0x7

    iget-object v3, p0, Lnij;->g:Ljava/lang/String;

    .line 2717
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2719
    :cond_4
    iget-object v2, p0, Lnij;->k:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 2720
    const/16 v2, 0x8

    iget-object v3, p0, Lnij;->k:Ljava/lang/String;

    .line 2721
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2723
    :cond_5
    iget-object v2, p0, Lnij;->l:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 2724
    const/16 v2, 0x9

    iget-object v3, p0, Lnij;->l:Ljava/lang/String;

    .line 2725
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2727
    :cond_6
    iget-object v2, p0, Lnij;->h:Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 2728
    const/16 v2, 0xa

    iget-object v3, p0, Lnij;->h:Ljava/lang/String;

    .line 2729
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2731
    :cond_7
    iget-object v2, p0, Lnij;->m:Ljava/lang/Long;

    if-eqz v2, :cond_8

    .line 2732
    const/16 v2, 0xb

    iget-object v3, p0, Lnij;->m:Ljava/lang/Long;

    .line 2733
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2735
    :cond_8
    iget-object v2, p0, Lnij;->i:Lnja;

    if-eqz v2, :cond_9

    .line 2736
    const/16 v2, 0xc

    iget-object v3, p0, Lnij;->i:Lnja;

    .line 2737
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2739
    :cond_9
    iget-object v2, p0, Lnij;->j:Lnja;

    if-eqz v2, :cond_a

    .line 2740
    const/16 v2, 0xd

    iget-object v3, p0, Lnij;->j:Lnja;

    .line 2741
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2743
    :cond_a
    iget-object v2, p0, Lnij;->n:[Lnii;

    if-eqz v2, :cond_c

    .line 2744
    iget-object v2, p0, Lnij;->n:[Lnii;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 2745
    if-eqz v4, :cond_b

    .line 2746
    const/16 v5, 0xe

    .line 2747
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2744
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2751
    :cond_c
    iget-object v1, p0, Lnij;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2752
    iput v0, p0, Lnij;->ai:I

    .line 2753
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnij;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2761
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2762
    sparse-switch v0, :sswitch_data_0

    .line 2766
    iget-object v2, p0, Lnij;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2767
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnij;->ah:Ljava/util/List;

    .line 2770
    :cond_1
    iget-object v2, p0, Lnij;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2772
    :sswitch_0
    return-object p0

    .line 2777
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2778
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 2780
    :cond_2
    iput v0, p0, Lnij;->b:I

    goto :goto_0

    .line 2782
    :cond_3
    iput v4, p0, Lnij;->b:I

    goto :goto_0

    .line 2787
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnij;->c:Ljava/lang/Double;

    goto :goto_0

    .line 2791
    :sswitch_3
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lnij;->d:Ljava/lang/Double;

    goto :goto_0

    .line 2795
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnij;->e:Ljava/lang/Long;

    goto :goto_0

    .line 2799
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnij;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 2803
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnij;->g:Ljava/lang/String;

    goto :goto_0

    .line 2807
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnij;->k:Ljava/lang/String;

    goto :goto_0

    .line 2811
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnij;->l:Ljava/lang/String;

    goto :goto_0

    .line 2815
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnij;->h:Ljava/lang/String;

    goto :goto_0

    .line 2819
    :sswitch_a
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnij;->m:Ljava/lang/Long;

    goto :goto_0

    .line 2823
    :sswitch_b
    iget-object v0, p0, Lnij;->i:Lnja;

    if-nez v0, :cond_4

    .line 2824
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnij;->i:Lnja;

    .line 2826
    :cond_4
    iget-object v0, p0, Lnij;->i:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2830
    :sswitch_c
    iget-object v0, p0, Lnij;->j:Lnja;

    if-nez v0, :cond_5

    .line 2831
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnij;->j:Lnja;

    .line 2833
    :cond_5
    iget-object v0, p0, Lnij;->j:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2837
    :sswitch_d
    const/16 v0, 0x72

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2838
    iget-object v0, p0, Lnij;->n:[Lnii;

    if-nez v0, :cond_7

    move v0, v1

    .line 2839
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnii;

    .line 2840
    iget-object v3, p0, Lnij;->n:[Lnii;

    if-eqz v3, :cond_6

    .line 2841
    iget-object v3, p0, Lnij;->n:[Lnii;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2843
    :cond_6
    iput-object v2, p0, Lnij;->n:[Lnii;

    .line 2844
    :goto_2
    iget-object v2, p0, Lnij;->n:[Lnii;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 2845
    iget-object v2, p0, Lnij;->n:[Lnii;

    new-instance v3, Lnii;

    invoke-direct {v3}, Lnii;-><init>()V

    aput-object v3, v2, v0

    .line 2846
    iget-object v2, p0, Lnij;->n:[Lnii;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2847
    invoke-virtual {p1}, Loxn;->a()I

    .line 2844
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2838
    :cond_7
    iget-object v0, p0, Lnij;->n:[Lnii;

    array-length v0, v0

    goto :goto_1

    .line 2850
    :cond_8
    iget-object v2, p0, Lnij;->n:[Lnii;

    new-instance v3, Lnii;

    invoke-direct {v3}, Lnii;-><init>()V

    aput-object v3, v2, v0

    .line 2851
    iget-object v2, p0, Lnij;->n:[Lnii;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 2762
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x19 -> :sswitch_2
        0x21 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2645
    iget v0, p0, Lnij;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2646
    const/4 v0, 0x2

    iget v1, p0, Lnij;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2648
    :cond_0
    iget-object v0, p0, Lnij;->c:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 2649
    const/4 v0, 0x3

    iget-object v1, p0, Lnij;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 2651
    :cond_1
    iget-object v0, p0, Lnij;->d:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 2652
    const/4 v0, 0x4

    iget-object v1, p0, Lnij;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 2654
    :cond_2
    iget-object v0, p0, Lnij;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2655
    const/4 v0, 0x5

    iget-object v1, p0, Lnij;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2657
    :cond_3
    iget-object v0, p0, Lnij;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2658
    const/4 v0, 0x6

    iget-object v1, p0, Lnij;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2660
    :cond_4
    iget-object v0, p0, Lnij;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2661
    const/4 v0, 0x7

    iget-object v1, p0, Lnij;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2663
    :cond_5
    iget-object v0, p0, Lnij;->k:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 2664
    const/16 v0, 0x8

    iget-object v1, p0, Lnij;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2666
    :cond_6
    iget-object v0, p0, Lnij;->l:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 2667
    const/16 v0, 0x9

    iget-object v1, p0, Lnij;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2669
    :cond_7
    iget-object v0, p0, Lnij;->h:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 2670
    const/16 v0, 0xa

    iget-object v1, p0, Lnij;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2672
    :cond_8
    iget-object v0, p0, Lnij;->m:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 2673
    const/16 v0, 0xb

    iget-object v1, p0, Lnij;->m:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2675
    :cond_9
    iget-object v0, p0, Lnij;->i:Lnja;

    if-eqz v0, :cond_a

    .line 2676
    const/16 v0, 0xc

    iget-object v1, p0, Lnij;->i:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2678
    :cond_a
    iget-object v0, p0, Lnij;->j:Lnja;

    if-eqz v0, :cond_b

    .line 2679
    const/16 v0, 0xd

    iget-object v1, p0, Lnij;->j:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2681
    :cond_b
    iget-object v0, p0, Lnij;->n:[Lnii;

    if-eqz v0, :cond_d

    .line 2682
    iget-object v1, p0, Lnij;->n:[Lnii;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_d

    aget-object v3, v1, v0

    .line 2683
    if-eqz v3, :cond_c

    .line 2684
    const/16 v4, 0xe

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2682
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2688
    :cond_d
    iget-object v0, p0, Lnij;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2690
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2601
    invoke-virtual {p0, p1}, Lnij;->a(Loxn;)Lnij;

    move-result-object v0

    return-object v0
.end method
