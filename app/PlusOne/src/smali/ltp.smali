.class public final Lltp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lltp;


# instance fields
.field public b:I

.field public c:I

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Lltp;

    sput-object v0, Lltp;->a:[Lltp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 12
    iput v0, p0, Lltp;->b:I

    .line 15
    iput v0, p0, Lltp;->c:I

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 37
    const/4 v0, 0x0

    .line 38
    iget v1, p0, Lltp;->b:I

    if-eq v1, v2, :cond_0

    .line 39
    const/4 v0, 0x1

    iget v1, p0, Lltp;->b:I

    .line 40
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42
    :cond_0
    iget v1, p0, Lltp;->c:I

    if-eq v1, v2, :cond_1

    .line 43
    const/4 v1, 0x2

    iget v2, p0, Lltp;->c:I

    .line 44
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_1
    iget-object v1, p0, Lltp;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 47
    const/4 v1, 0x3

    iget-object v2, p0, Lltp;->d:Ljava/lang/String;

    .line 48
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    :cond_2
    iget-object v1, p0, Lltp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    iput v0, p0, Lltp;->ai:I

    .line 52
    return v0
.end method

.method public a(Loxn;)Lltp;
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 60
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 61
    sparse-switch v0, :sswitch_data_0

    .line 65
    iget-object v1, p0, Lltp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lltp;->ah:Ljava/util/List;

    .line 69
    :cond_1
    iget-object v1, p0, Lltp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 71
    :sswitch_0
    return-object p0

    .line 76
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 77
    if-eq v0, v2, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    .line 81
    :cond_2
    iput v0, p0, Lltp;->b:I

    goto :goto_0

    .line 83
    :cond_3
    iput v2, p0, Lltp;->b:I

    goto :goto_0

    .line 88
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 89
    if-eq v0, v2, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-ne v0, v1, :cond_5

    .line 95
    :cond_4
    iput v0, p0, Lltp;->c:I

    goto :goto_0

    .line 97
    :cond_5
    iput v2, p0, Lltp;->c:I

    goto :goto_0

    .line 102
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltp;->d:Ljava/lang/String;

    goto :goto_0

    .line 61
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 22
    iget v0, p0, Lltp;->b:I

    if-eq v0, v2, :cond_0

    .line 23
    const/4 v0, 0x1

    iget v1, p0, Lltp;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 25
    :cond_0
    iget v0, p0, Lltp;->c:I

    if-eq v0, v2, :cond_1

    .line 26
    const/4 v0, 0x2

    iget v1, p0, Lltp;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 28
    :cond_1
    iget-object v0, p0, Lltp;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 29
    const/4 v0, 0x3

    iget-object v1, p0, Lltp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 31
    :cond_2
    iget-object v0, p0, Lltp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lltp;->a(Loxn;)Lltp;

    move-result-object v0

    return-object v0
.end method
