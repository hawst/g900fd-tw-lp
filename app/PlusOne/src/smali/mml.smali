.class public final Lmml;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmml;


# instance fields
.field public b:Lmmm;

.field public c:I

.field public d:Lnzx;

.field public e:Lmmn;

.field public f:Ljava/lang/Boolean;

.field private g:Lmon;

.field private h:I

.field private i:Loud;

.field private j:Ljava/lang/Float;

.field private k:Logr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1551
    const/4 v0, 0x0

    new-array v0, v0, [Lmml;

    sput-object v0, Lmml;->a:[Lmml;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    .line 1552
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1568
    iput-object v0, p0, Lmml;->b:Lmmm;

    .line 1571
    iput-object v0, p0, Lmml;->g:Lmon;

    .line 1574
    iput v1, p0, Lmml;->c:I

    .line 1577
    iput v1, p0, Lmml;->h:I

    .line 1580
    iput-object v0, p0, Lmml;->d:Lnzx;

    .line 1583
    iput-object v0, p0, Lmml;->i:Loud;

    .line 1588
    iput-object v0, p0, Lmml;->k:Logr;

    .line 1591
    iput-object v0, p0, Lmml;->e:Lmmn;

    .line 1552
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 1634
    const/4 v0, 0x0

    .line 1635
    iget-object v1, p0, Lmml;->b:Lmmm;

    if-eqz v1, :cond_0

    .line 1636
    const/4 v0, 0x1

    iget-object v1, p0, Lmml;->b:Lmmm;

    .line 1637
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1639
    :cond_0
    iget-object v1, p0, Lmml;->g:Lmon;

    if-eqz v1, :cond_1

    .line 1640
    const/4 v1, 0x2

    iget-object v2, p0, Lmml;->g:Lmon;

    .line 1641
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1643
    :cond_1
    iget v1, p0, Lmml;->c:I

    if-eq v1, v3, :cond_2

    .line 1644
    const/4 v1, 0x3

    iget v2, p0, Lmml;->c:I

    .line 1645
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1647
    :cond_2
    iget v1, p0, Lmml;->h:I

    if-eq v1, v3, :cond_3

    .line 1648
    const/4 v1, 0x4

    iget v2, p0, Lmml;->h:I

    .line 1649
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1651
    :cond_3
    iget-object v1, p0, Lmml;->j:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 1652
    const/4 v1, 0x6

    iget-object v2, p0, Lmml;->j:Ljava/lang/Float;

    .line 1653
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1655
    :cond_4
    iget-object v1, p0, Lmml;->k:Logr;

    if-eqz v1, :cond_5

    .line 1656
    const/4 v1, 0x7

    iget-object v2, p0, Lmml;->k:Logr;

    .line 1657
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1659
    :cond_5
    iget-object v1, p0, Lmml;->e:Lmmn;

    if-eqz v1, :cond_6

    .line 1660
    const/16 v1, 0x8

    iget-object v2, p0, Lmml;->e:Lmmn;

    .line 1661
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1663
    :cond_6
    iget-object v1, p0, Lmml;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 1664
    const/16 v1, 0x9

    iget-object v2, p0, Lmml;->f:Ljava/lang/Boolean;

    .line 1665
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1667
    :cond_7
    iget-object v1, p0, Lmml;->d:Lnzx;

    if-eqz v1, :cond_8

    .line 1668
    const/16 v1, 0xa

    iget-object v2, p0, Lmml;->d:Lnzx;

    .line 1669
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1671
    :cond_8
    iget-object v1, p0, Lmml;->i:Loud;

    if-eqz v1, :cond_9

    .line 1672
    const/16 v1, 0xb

    iget-object v2, p0, Lmml;->i:Loud;

    .line 1673
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1675
    :cond_9
    iget-object v1, p0, Lmml;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1676
    iput v0, p0, Lmml;->ai:I

    .line 1677
    return v0
.end method

.method public a(Loxn;)Lmml;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1685
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1686
    sparse-switch v0, :sswitch_data_0

    .line 1690
    iget-object v1, p0, Lmml;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1691
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmml;->ah:Ljava/util/List;

    .line 1694
    :cond_1
    iget-object v1, p0, Lmml;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1696
    :sswitch_0
    return-object p0

    .line 1701
    :sswitch_1
    iget-object v0, p0, Lmml;->b:Lmmm;

    if-nez v0, :cond_2

    .line 1702
    new-instance v0, Lmmm;

    invoke-direct {v0}, Lmmm;-><init>()V

    iput-object v0, p0, Lmml;->b:Lmmm;

    .line 1704
    :cond_2
    iget-object v0, p0, Lmml;->b:Lmmm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1708
    :sswitch_2
    iget-object v0, p0, Lmml;->g:Lmon;

    if-nez v0, :cond_3

    .line 1709
    new-instance v0, Lmon;

    invoke-direct {v0}, Lmon;-><init>()V

    iput-object v0, p0, Lmml;->g:Lmon;

    .line 1711
    :cond_3
    iget-object v0, p0, Lmml;->g:Lmon;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1715
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1716
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_5

    .line 1720
    :cond_4
    iput v0, p0, Lmml;->c:I

    goto :goto_0

    .line 1722
    :cond_5
    iput v2, p0, Lmml;->c:I

    goto :goto_0

    .line 1727
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1728
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-ne v0, v4, :cond_7

    .line 1731
    :cond_6
    iput v0, p0, Lmml;->h:I

    goto :goto_0

    .line 1733
    :cond_7
    iput v2, p0, Lmml;->h:I

    goto :goto_0

    .line 1738
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmml;->j:Ljava/lang/Float;

    goto :goto_0

    .line 1742
    :sswitch_6
    iget-object v0, p0, Lmml;->k:Logr;

    if-nez v0, :cond_8

    .line 1743
    new-instance v0, Logr;

    invoke-direct {v0}, Logr;-><init>()V

    iput-object v0, p0, Lmml;->k:Logr;

    .line 1745
    :cond_8
    iget-object v0, p0, Lmml;->k:Logr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1749
    :sswitch_7
    iget-object v0, p0, Lmml;->e:Lmmn;

    if-nez v0, :cond_9

    .line 1750
    new-instance v0, Lmmn;

    invoke-direct {v0}, Lmmn;-><init>()V

    iput-object v0, p0, Lmml;->e:Lmmn;

    .line 1752
    :cond_9
    iget-object v0, p0, Lmml;->e:Lmmn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1756
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmml;->f:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1760
    :sswitch_9
    iget-object v0, p0, Lmml;->d:Lnzx;

    if-nez v0, :cond_a

    .line 1761
    new-instance v0, Lnzx;

    invoke-direct {v0}, Lnzx;-><init>()V

    iput-object v0, p0, Lmml;->d:Lnzx;

    .line 1763
    :cond_a
    iget-object v0, p0, Lmml;->d:Lnzx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1767
    :sswitch_a
    iget-object v0, p0, Lmml;->i:Loud;

    if-nez v0, :cond_b

    .line 1768
    new-instance v0, Loud;

    invoke-direct {v0}, Loud;-><init>()V

    iput-object v0, p0, Lmml;->i:Loud;

    .line 1770
    :cond_b
    iget-object v0, p0, Lmml;->i:Loud;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1686
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x35 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 1598
    iget-object v0, p0, Lmml;->b:Lmmm;

    if-eqz v0, :cond_0

    .line 1599
    const/4 v0, 0x1

    iget-object v1, p0, Lmml;->b:Lmmm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1601
    :cond_0
    iget-object v0, p0, Lmml;->g:Lmon;

    if-eqz v0, :cond_1

    .line 1602
    const/4 v0, 0x2

    iget-object v1, p0, Lmml;->g:Lmon;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1604
    :cond_1
    iget v0, p0, Lmml;->c:I

    if-eq v0, v2, :cond_2

    .line 1605
    const/4 v0, 0x3

    iget v1, p0, Lmml;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1607
    :cond_2
    iget v0, p0, Lmml;->h:I

    if-eq v0, v2, :cond_3

    .line 1608
    const/4 v0, 0x4

    iget v1, p0, Lmml;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1610
    :cond_3
    iget-object v0, p0, Lmml;->j:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 1611
    const/4 v0, 0x6

    iget-object v1, p0, Lmml;->j:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1613
    :cond_4
    iget-object v0, p0, Lmml;->k:Logr;

    if-eqz v0, :cond_5

    .line 1614
    const/4 v0, 0x7

    iget-object v1, p0, Lmml;->k:Logr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1616
    :cond_5
    iget-object v0, p0, Lmml;->e:Lmmn;

    if-eqz v0, :cond_6

    .line 1617
    const/16 v0, 0x8

    iget-object v1, p0, Lmml;->e:Lmmn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1619
    :cond_6
    iget-object v0, p0, Lmml;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 1620
    const/16 v0, 0x9

    iget-object v1, p0, Lmml;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1622
    :cond_7
    iget-object v0, p0, Lmml;->d:Lnzx;

    if-eqz v0, :cond_8

    .line 1623
    const/16 v0, 0xa

    iget-object v1, p0, Lmml;->d:Lnzx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1625
    :cond_8
    iget-object v0, p0, Lmml;->i:Loud;

    if-eqz v0, :cond_9

    .line 1626
    const/16 v0, 0xb

    iget-object v1, p0, Lmml;->i:Loud;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1628
    :cond_9
    iget-object v0, p0, Lmml;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1630
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1548
    invoke-virtual {p0, p1}, Lmml;->a(Loxn;)Lmml;

    move-result-object v0

    return-object v0
.end method
