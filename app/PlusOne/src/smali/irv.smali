.class public final Lirv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/ingest/IngestActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V
    .locals 0

    .prologue
    .line 589
    iput-object p1, p0, Lirv;->a:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Lirv;->a:Lcom/google/android/libraries/social/ingest/IngestActivity;

    check-cast p2, Lirz;

    invoke-virtual {p2}, Lirz;->a()Lcom/google/android/libraries/social/ingest/IngestService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;Lcom/google/android/libraries/social/ingest/IngestService;)Lcom/google/android/libraries/social/ingest/IngestService;

    .line 593
    iget-object v0, p0, Lirv;->a:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->m(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/IngestService;

    move-result-object v0

    iget-object v1, p0, Lirv;->a:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ingest/IngestService;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    .line 594
    iget-object v0, p0, Lirv;->a:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->m(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/IngestService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/IngestService;->a()Liso;

    move-result-object v0

    .line 595
    iget-object v1, p0, Lirv;->a:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->b(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lisd;

    move-result-object v1

    invoke-virtual {v1, v0}, Lisd;->a(Liso;)V

    .line 596
    iget-object v1, p0, Lirv;->a:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->g(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lise;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 597
    iget-object v1, p0, Lirv;->a:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->g(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lise;

    move-result-object v1

    invoke-virtual {v1, v0}, Lise;->a(Liso;)V

    .line 599
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lirv;->a:Lcom/google/android/libraries/social/ingest/IngestActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;Lcom/google/android/libraries/social/ingest/IngestService;)Lcom/google/android/libraries/social/ingest/IngestService;

    .line 604
    return-void
.end method
