.class final Leun;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheq;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    const-string v0, "notifications_upgraded"

    return-object v0
.end method

.method public a(Landroid/content/Context;Lhem;)V
    .locals 5

    .prologue
    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 138
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 139
    const-string v2, "push_notifications"

    const-string v3, "notifications_enabled"

    const v4, 0x7f0e0012

    .line 141
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 139
    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-interface {p2, v2, v3}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    .line 142
    const-string v2, "notifications_vibrate"

    const-string v3, "notifications_vibrate"

    const v4, 0x7f0e0013

    .line 144
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 142
    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-interface {p2, v2, v3}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    .line 145
    const-string v2, "notifications_ringtone"

    const-string v3, "notifications_ringtone"

    const v4, 0x7f0a05b7

    .line 147
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v2, v0}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    .line 148
    return-void
.end method
