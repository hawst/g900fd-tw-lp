.class public final Lflo;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Lnyq;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Landroid/content/Context;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Lnyr;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lnyr;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object p1, p0, Lflo;->b:Landroid/content/Context;

    .line 28
    iput p2, p0, Lflo;->c:I

    .line 29
    iput-object p3, p0, Lflo;->d:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Lflo;->e:Lnyr;

    .line 31
    return-void
.end method


# virtual methods
.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lflo;->l()Lnyq;

    move-result-object v0

    return-object v0
.end method

.method public l()Lnyq;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 35
    :try_start_0
    new-instance v0, Lnyr;

    invoke-direct {v0}, Lnyr;-><init>()V

    iget-object v2, p0, Lflo;->e:Lnyr;

    .line 38
    invoke-static {v2}, Loxu;->a(Loxu;)[B

    move-result-object v2

    .line 37
    invoke-static {v0, v2}, Lnyr;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnyr;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    new-instance v2, Lhqc;

    iget-object v3, p0, Lflo;->b:Landroid/content/Context;

    new-instance v4, Lkfo;

    iget-object v5, p0, Lflo;->b:Landroid/content/Context;

    iget v6, p0, Lflo;->c:I

    invoke-direct {v4, v5, v6}, Lkfo;-><init>(Landroid/content/Context;I)V

    iget-object v5, p0, Lflo;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v0}, Lhqc;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Lnyr;)V

    .line 46
    invoke-virtual {v2}, Lhqc;->l()V

    .line 47
    invoke-virtual {v2}, Lhqc;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 51
    :goto_0
    return-object v0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    const-string v2, "PhotosSettingsLoader"

    const-string v3, "Failed to load photos settings"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 41
    goto :goto_0

    .line 51
    :cond_0
    invoke-virtual {v2}, Lhqc;->i()Lnyq;

    move-result-object v0

    goto :goto_0
.end method
