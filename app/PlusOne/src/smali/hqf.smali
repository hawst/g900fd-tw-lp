.class public final Lhqf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:[Lhql;


# direct methods
.method public static a(Landroid/content/Context;Ljava/util/List;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 239
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 240
    invoke-static {v0}, Llsb;->b(Landroid/net/Uri;)Z

    move-result v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Uri provided is not a MediaStore Uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Llsk;->a(ZLjava/lang/Object;)V

    goto :goto_0

    .line 244
    :cond_0
    const/4 v0, 0x0

    .line 246
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 247
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 248
    invoke-static {v0}, Lhqf;->a(Landroid/net/Uri;)Lhql;

    move-result-object v0

    .line 250
    if-eqz v0, :cond_2

    invoke-virtual {v0, p0, v4, v5}, Lhql;->b(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 251
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_2
    move v1, v0

    .line 253
    goto :goto_1

    .line 255
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public static a(Landroid/net/Uri;)Lhql;
    .locals 7

    .prologue
    .line 314
    invoke-static {}, Lhqf;->a()V

    .line 315
    invoke-static {p0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 317
    sget-object v4, Lhqf;->a:[Lhql;

    array-length v5, v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v0, v4, v1

    .line 318
    invoke-virtual {v0}, Lhql;->b()Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 319
    invoke-virtual {p0, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 324
    :goto_1
    return-object v0

    .line 317
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 324
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 35
    sget-object v0, Lhqf;->a:[Lhql;

    if-nez v0, :cond_0

    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [Lhql;

    const/4 v1, 0x0

    new-instance v2, Lhqj;

    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3}, Lhqj;-><init>(Landroid/net/Uri;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lhqj;

    sget-object v3, Lhsf;->a:Landroid/net/Uri;

    invoke-direct {v2, v3}, Lhqj;-><init>(Landroid/net/Uri;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lhqm;

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3}, Lhqm;-><init>(Landroid/net/Uri;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lhqm;

    sget-object v3, Lhsf;->b:Landroid/net/Uri;

    invoke-direct {v2, v3}, Lhqm;-><init>(Landroid/net/Uri;)V

    aput-object v2, v0, v1

    sput-object v0, Lhqf;->a:[Lhql;

    .line 43
    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Lhql;IILhqh;Landroid/database/Cursor;I)V
    .locals 9

    .prologue
    .line 192
    const/4 v0, 0x5

    invoke-interface {p5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 193
    const/4 v2, 0x6

    invoke-interface {p5, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 194
    const/4 v2, 0x3

    invoke-interface {p5, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 196
    iget-wide v6, p4, Lhqh;->d:J

    iget-boolean v8, p4, Lhqh;->e:Z

    if-eqz v8, :cond_1

    move-wide v0, v2

    :goto_0
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p4, Lhqh;->d:J

    .line 198
    iget v0, p4, Lhqh;->c:I

    const/4 v1, 0x4

    invoke-interface {p5, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p4, Lhqh;->c:I

    .line 199
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p4, Lhqh;->a:Ljava/lang/Integer;

    .line 203
    iget-object v6, p4, Lhqh;->f:Ljava/util/TreeSet;

    .line 205
    if-lez p6, :cond_2

    invoke-virtual {v6}, Ljava/util/TreeSet;->size()I

    move-result v0

    if-lt v0, p6, :cond_0

    .line 206
    invoke-virtual {v6}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqk;

    iget-wide v0, v0, Lhqk;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 207
    :cond_0
    iget-boolean v3, p4, Lhqh;->e:Z

    move-object v0, p1

    move-object v1, p0

    move v2, p3

    move v4, p2

    move v5, p6

    .line 208
    invoke-virtual/range {v0 .. v5}, Lhql;->a(Landroid/content/Context;IZII)Ljava/util/ArrayList;

    move-result-object v0

    .line 210
    invoke-virtual {v6, v0}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    .line 212
    invoke-virtual {v6}, Ljava/util/TreeSet;->size()I

    move-result v0

    sub-int/2addr v0, p6

    .line 214
    :goto_1
    if-lez v0, :cond_2

    .line 215
    invoke-virtual {v6}, Ljava/util/TreeSet;->last()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 216
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 197
    :cond_1
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 219
    :cond_2
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/ArrayList;II)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lhqh;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 98
    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    .line 99
    const/4 v2, 0x0

    :goto_0
    sget-object v0, Lhqf;->a:[Lhql;

    array-length v0, v0

    if-ge v2, v0, :cond_7

    .line 100
    sget-object v0, Lhqf;->a:[Lhql;

    aget-object v1, v0, v2

    .line 101
    const/4 v0, 0x4

    if-ne p3, v0, :cond_0

    instance-of v0, v1, Lhqm;

    if-eqz v0, :cond_5

    .line 102
    :cond_0
    const/4 v0, 0x5

    if-eq p3, v0, :cond_1

    const/4 v0, 0x3

    if-ne p3, v0, :cond_2

    :cond_1
    instance-of v0, v1, Lhqj;

    if-eqz v0, :cond_5

    .line 106
    :cond_2
    const/4 v5, 0x0

    .line 112
    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {v1, p0, v0}, Lhql;->a(Landroid/content/Context;I)Landroid/database/Cursor;

    move-result-object v5

    .line 114
    if-eqz v5, :cond_4

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 118
    :cond_3
    const/4 v0, 0x1

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 119
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqh;

    .line 121
    if-nez v0, :cond_9

    .line 122
    new-instance v4, Lhqh;

    invoke-direct {v4}, Lhqh;-><init>()V

    .line 123
    const/4 v0, 0x2

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lhqh;->b:Ljava/lang/String;

    .line 125
    iput-object v3, v4, Lhqh;->a:Ljava/lang/Integer;

    .line 127
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v7, v0, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 130
    :goto_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v0, p0

    move v6, p2

    invoke-static/range {v0 .. v6}, Lhqf;->a(Landroid/content/Context;Lhql;IILhqh;Landroid/database/Cursor;I)V

    .line 132
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    .line 135
    :cond_4
    if-eqz v5, :cond_5

    .line 136
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 99
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    if-eqz v5, :cond_6

    .line 136
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 141
    :cond_7
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_2
    if-ltz v0, :cond_8

    .line 142
    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 144
    :cond_8
    return-void

    :cond_9
    move-object v4, v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;II)[Lhqh;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 57
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 58
    invoke-static {}, Lhqf;->a()V

    .line 60
    if-eq p2, v0, :cond_2

    move v2, v0

    .line 61
    :goto_0
    const/4 v4, 0x2

    if-eq p2, v4, :cond_3

    const/4 v4, 0x3

    if-eq p2, v4, :cond_3

    .line 64
    :goto_1
    if-eqz v2, :cond_0

    .line 65
    invoke-static {p0, v3, p1, p2}, Lhqf;->a(Landroid/content/Context;Ljava/util/ArrayList;II)V

    .line 68
    :cond_0
    if-eqz v0, :cond_1

    .line 69
    invoke-static {p0, v3, p1, p2}, Lhqf;->b(Landroid/content/Context;Ljava/util/ArrayList;II)V

    .line 72
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lhqh;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhqh;

    .line 73
    new-instance v1, Lhqg;

    invoke-direct {v1}, Lhqg;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 81
    return-object v0

    :cond_2
    move v2, v1

    .line 60
    goto :goto_0

    :cond_3
    move v0, v1

    .line 61
    goto :goto_1
.end method

.method private static b(Landroid/content/Context;Ljava/util/ArrayList;II)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lhqh;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 148
    new-instance v4, Lhqh;

    invoke-direct {v4}, Lhqh;-><init>()V

    .line 149
    const v0, 0x7f0a0432

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lhqh;->b:Ljava/lang/String;

    .line 150
    const-wide/32 v0, 0x7fffffff

    iput-wide v0, v4, Lhqh;->d:J

    .line 151
    iput-boolean v2, v4, Lhqh;->e:Z

    .line 153
    const/4 v2, 0x0

    :goto_0
    sget-object v0, Lhqf;->a:[Lhql;

    array-length v0, v0

    if-ge v2, v0, :cond_6

    .line 154
    sget-object v0, Lhqf;->a:[Lhql;

    aget-object v1, v0, v2

    .line 155
    const/4 v0, 0x4

    if-ne p3, v0, :cond_0

    instance-of v0, v1, Lhqm;

    if-eqz v0, :cond_4

    .line 156
    :cond_0
    const/4 v0, 0x5

    if-ne p3, v0, :cond_1

    instance-of v0, v1, Lhqj;

    if-eqz v0, :cond_4

    .line 158
    :cond_1
    const/4 v5, 0x0

    .line 164
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {v1, p0, v0}, Lhql;->a(Landroid/content/Context;I)Landroid/database/Cursor;

    move-result-object v5

    .line 166
    if-eqz v5, :cond_3

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 168
    :cond_2
    const/4 v0, 0x1

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    move-object v0, p0

    move v6, p2

    .line 170
    invoke-static/range {v0 .. v6}, Lhqf;->a(Landroid/content/Context;Lhql;IILhqh;Landroid/database/Cursor;I)V

    .line 172
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 175
    :cond_3
    if-eqz v5, :cond_4

    .line 176
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 153
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 175
    :catchall_0
    move-exception v0

    if-eqz v5, :cond_5

    .line 176
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 181
    :cond_6
    iget v0, v4, Lhqh;->c:I

    if-lez v0, :cond_7

    .line 182
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_7
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lhqi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 276
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqi;

    .line 277
    iget-object v2, v0, Lhqi;->a:Lhql;

    iget-wide v4, v0, Lhqi;->b:J

    invoke-virtual {v2, p0, v4, v5}, Lhql;->b(Landroid/content/Context;J)Z

    goto :goto_0

    .line 279
    :cond_0
    return-void
.end method
