.class public final Lmec;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmpu;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19195
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19198
    iput-object v0, p0, Lmec;->apiHeader:Llyq;

    .line 19201
    iput-object v0, p0, Lmec;->a:Lmpu;

    .line 19195
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 19218
    const/4 v0, 0x0

    .line 19219
    iget-object v1, p0, Lmec;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 19220
    const/4 v0, 0x1

    iget-object v1, p0, Lmec;->apiHeader:Llyq;

    .line 19221
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 19223
    :cond_0
    iget-object v1, p0, Lmec;->a:Lmpu;

    if-eqz v1, :cond_1

    .line 19224
    const/4 v1, 0x2

    iget-object v2, p0, Lmec;->a:Lmpu;

    .line 19225
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19227
    :cond_1
    iget-object v1, p0, Lmec;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19228
    iput v0, p0, Lmec;->ai:I

    .line 19229
    return v0
.end method

.method public a(Loxn;)Lmec;
    .locals 2

    .prologue
    .line 19237
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 19238
    sparse-switch v0, :sswitch_data_0

    .line 19242
    iget-object v1, p0, Lmec;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 19243
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmec;->ah:Ljava/util/List;

    .line 19246
    :cond_1
    iget-object v1, p0, Lmec;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 19248
    :sswitch_0
    return-object p0

    .line 19253
    :sswitch_1
    iget-object v0, p0, Lmec;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 19254
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmec;->apiHeader:Llyq;

    .line 19256
    :cond_2
    iget-object v0, p0, Lmec;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 19260
    :sswitch_2
    iget-object v0, p0, Lmec;->a:Lmpu;

    if-nez v0, :cond_3

    .line 19261
    new-instance v0, Lmpu;

    invoke-direct {v0}, Lmpu;-><init>()V

    iput-object v0, p0, Lmec;->a:Lmpu;

    .line 19263
    :cond_3
    iget-object v0, p0, Lmec;->a:Lmpu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 19238
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 19206
    iget-object v0, p0, Lmec;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 19207
    const/4 v0, 0x1

    iget-object v1, p0, Lmec;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 19209
    :cond_0
    iget-object v0, p0, Lmec;->a:Lmpu;

    if-eqz v0, :cond_1

    .line 19210
    const/4 v0, 0x2

    iget-object v1, p0, Lmec;->a:Lmpu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 19212
    :cond_1
    iget-object v0, p0, Lmec;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 19214
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 19191
    invoke-virtual {p0, p1}, Lmec;->a(Loxn;)Lmec;

    move-result-object v0

    return-object v0
.end method
