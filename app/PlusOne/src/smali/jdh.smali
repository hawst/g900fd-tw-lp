.class final Ljdh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:Landroid/content/Context;

.field private final g:Ljbh;

.field private final h:Ljava/lang/String;

.field private final i:Lopf;

.field private j:Z

.field private k:J

.field private l:Landroid/net/Uri;

.field private m:Landroid/net/Uri;

.field private n:Ljbh;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 147
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "datetaken"

    aput-object v1, v0, v2

    const-string v1, "date_added"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    sput-object v0, Ljdh;->a:[Ljava/lang/String;

    .line 149
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "datetaken"

    aput-object v1, v0, v2

    const-string v1, "date_added"

    aput-object v1, v0, v3

    const-string v1, "_data"

    aput-object v1, v0, v4

    sput-object v0, Ljdh;->b:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lopf;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    iput-object p1, p0, Ljdh;->f:Landroid/content/Context;

    .line 175
    iput-object p2, p0, Ljdh;->l:Landroid/net/Uri;

    .line 176
    iput-object p2, p0, Ljdh;->m:Landroid/net/Uri;

    .line 177
    iput-object p3, p0, Ljdh;->h:Ljava/lang/String;

    .line 178
    iput-object p5, p0, Ljdh;->i:Lopf;

    .line 179
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Ljdh;->d:Ljava/lang/String;

    .line 180
    iget-object v0, p0, Ljdh;->m:Landroid/net/Uri;

    invoke-direct {p0, v0}, Ljdh;->a(Landroid/net/Uri;)Ljbh;

    move-result-object v0

    .line 181
    iput-object v0, p0, Ljdh;->g:Ljbh;

    .line 182
    iget-object v1, p0, Ljdh;->g:Ljbh;

    iput-object v1, p0, Ljdh;->n:Ljbh;

    .line 183
    invoke-virtual {v0}, Ljbh;->b()J

    move-result-wide v0

    iput-wide v0, p0, Ljdh;->k:J

    .line 184
    iget-wide v0, p0, Ljdh;->k:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-gtz v0, :cond_1

    .line 185
    new-instance v0, Ljcw;

    iget-object v1, p0, Ljdh;->m:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Empty content at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljcw;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    invoke-static {p1, p2}, Ljdv;->a(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p4

    goto :goto_0

    .line 187
    :cond_1
    invoke-static {p2}, Llsb;->b(Landroid/net/Uri;)Z

    move-result v0

    .line 189
    if-eqz v0, :cond_7

    .line 190
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 192
    iget-object v1, p0, Ljdh;->d:Ljava/lang/String;

    invoke-static {v1}, Llsb;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 193
    sget-object v2, Ljdh;->a:[Ljava/lang/String;

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 194
    iget-object v0, p0, Ljdh;->m:Landroid/net/Uri;

    invoke-static {v1, v0}, Ljct;->a(Landroid/database/Cursor;Landroid/net/Uri;)V

    .line 195
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 196
    const-string v2, "datetaken"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Ljdh;->e:J

    .line 209
    :goto_1
    if-nez v0, :cond_2

    .line 210
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 212
    :cond_2
    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 213
    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iput-object v0, p0, Ljdh;->c:Ljava/lang/String;

    .line 214
    return-void

    .line 197
    :cond_4
    iget-object v1, p0, Ljdh;->d:Ljava/lang/String;

    invoke-static {v1}, Llsb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 198
    sget-object v2, Ljdh;->b:[Ljava/lang/String;

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 199
    iget-object v0, p0, Ljdh;->m:Landroid/net/Uri;

    invoke-static {v1, v0}, Ljct;->a(Landroid/database/Cursor;Landroid/net/Uri;)V

    .line 200
    const-string v0, "_data"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 201
    const-string v2, "datetaken"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Ljdh;->e:J

    goto :goto_1

    .line 203
    :cond_5
    new-instance v1, Ljcv;

    const-string v2, "Invalid content at "

    .line 204
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Ljcv;-><init>(Ljava/lang/String;Z)V

    throw v1

    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 206
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ljdh;->e:J

    move-object v0, v3

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;)Ljbh;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 339
    iget-object v1, p0, Ljdh;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 341
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 342
    invoke-static {v1}, Ljbh;->b(Ljava/io/InputStream;)Ljbh;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 346
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 344
    :catch_1
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public a(J)Ljava/io/InputStream;
    .locals 5

    .prologue
    .line 236
    iget-object v0, p0, Ljdh;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Ljdh;->m:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    .line 237
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 238
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    .line 239
    invoke-virtual {v1, p1, p2}, Ljava/io/BufferedInputStream;->skip(J)J

    .line 241
    :cond_0
    return-object v1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Ljdh;->f:Landroid/content/Context;

    iget-object v1, p0, Ljdh;->m:Landroid/net/Uri;

    invoke-static {v0, v1}, Ljdv;->c(Landroid/content/Context;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_0

    .line 224
    iput-object v0, p0, Ljdh;->m:Landroid/net/Uri;

    .line 225
    iget-object v0, p0, Ljdh;->m:Landroid/net/Uri;

    invoke-direct {p0, v0}, Ljdh;->a(Landroid/net/Uri;)Ljbh;

    move-result-object v0

    .line 226
    iput-object v0, p0, Ljdh;->n:Ljbh;

    .line 227
    invoke-virtual {v0}, Ljbh;->b()J

    move-result-wide v0

    iput-wide v0, p0, Ljdh;->k:J

    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljdh;->j:Z

    .line 230
    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Ljdh;->j:Z

    return v0
.end method

.method public c()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Ljdh;->m:Landroid/net/Uri;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Ljdh;->c:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Ljdh;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 286
    iget-wide v0, p0, Ljdh;->e:J

    return-wide v0
.end method

.method public g()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Ljdh;->l:Landroid/net/Uri;

    return-object v0
.end method

.method public h()Ljbh;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Ljdh;->g:Ljbh;

    return-object v0
.end method

.method public i()Ljbh;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Ljdh;->n:Ljbh;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Ljdh;->h:Ljava/lang/String;

    return-object v0
.end method

.method public k()J
    .locals 2

    .prologue
    .line 327
    iget-wide v0, p0, Ljdh;->k:J

    return-wide v0
.end method

.method public l()Lopf;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Ljdh;->i:Lopf;

    return-object v0
.end method
