.class public final Lkcx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Landroid/graphics/Bitmap;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method


# virtual methods
.method public a(I)Z
    .locals 2

    .prologue
    const/16 v1, 0x64

    const/4 v0, 0x0

    .line 102
    if-gez p1, :cond_2

    move p1, v0

    .line 103
    :cond_0
    :goto_0
    iget v1, p0, Lkcx;->a:I

    if-eq p1, v1, :cond_1

    const/4 v0, 0x1

    .line 104
    :cond_1
    iput p1, p0, Lkcx;->a:I

    .line 105
    return v0

    .line 102
    :cond_2
    if-le p1, v1, :cond_0

    move p1, v1

    goto :goto_0
.end method

.method public a(ILjava/lang/String;Landroid/graphics/Bitmap;Z)Z
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lkcx;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lkcx;->b:Ljava/lang/String;

    .line 58
    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkcx;->c:Landroid/graphics/Bitmap;

    if-ne p3, v0, :cond_0

    iget-boolean v0, p0, Lkcx;->d:Z

    if-eq p4, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 61
    :goto_0
    iput-object p2, p0, Lkcx;->b:Ljava/lang/String;

    .line 62
    iput-object p3, p0, Lkcx;->c:Landroid/graphics/Bitmap;

    .line 63
    iput-boolean p4, p0, Lkcx;->d:Z

    .line 64
    return v0

    .line 58
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
