.class public final Ljat;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lizx;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lizx;

.field private c:Lizp;

.field private d:Ljgn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Lizw;

    invoke-direct {v0}, Lizw;-><init>()V

    iput-object v0, p0, Ljat;->b:Lizx;

    .line 27
    iput-object p1, p0, Ljat;->a:Landroid/content/Context;

    .line 28
    new-instance v0, Ljaq;

    invoke-direct {v0, p1}, Ljaq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljat;->c:Lizp;

    .line 29
    const-class v0, Ljgn;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    iput-object v0, p0, Ljat;->d:Ljgn;

    .line 30
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 6

    .prologue
    const/16 v4, 0x3c

    const/16 v3, 0x32

    const/16 v2, 0x28

    const/16 v1, 0x1e

    const/16 v0, 0x14

    .line 211
    const-string v5, "L20"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 242
    :cond_0
    :goto_0
    return v0

    .line 213
    :cond_1
    const-string v5, "L30"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v1

    .line 214
    goto :goto_0

    .line 215
    :cond_2
    const-string v5, "L40"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v2

    .line 216
    goto :goto_0

    .line 217
    :cond_3
    const-string v5, "L50"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v0, v3

    .line 218
    goto :goto_0

    .line 219
    :cond_4
    const-string v5, "L60"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    move v0, v4

    .line 220
    goto :goto_0

    .line 221
    :cond_5
    const-string v5, "L70"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 222
    const/16 v0, 0x46

    goto :goto_0

    .line 223
    :cond_6
    const-string v5, "L80"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 224
    const/16 v0, 0x50

    goto :goto_0

    .line 225
    :cond_7
    const-string v5, "L90"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 226
    const/16 v0, 0x5a

    goto :goto_0

    .line 227
    :cond_8
    const-string v5, "WL20"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 229
    const-string v0, "WL30"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    .line 230
    goto :goto_0

    .line 231
    :cond_9
    const-string v0, "WL40"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v2

    .line 232
    goto :goto_0

    .line 233
    :cond_a
    const-string v0, "WL50"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v3

    .line 234
    goto :goto_0

    .line 235
    :cond_b
    const-string v0, "WL60"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v4

    .line 236
    goto :goto_0

    .line 237
    :cond_c
    const-string v0, "WL70"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 238
    const/16 v0, 0x46

    goto/16 :goto_0

    .line 239
    :cond_d
    const-string v0, "WL80"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 240
    const/16 v0, 0x50

    goto/16 :goto_0

    .line 241
    :cond_e
    const-string v0, "WL90"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 242
    const/16 v0, 0x5a

    goto/16 :goto_0

    .line 244
    :cond_f
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Value is not valid"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Ljat;->a:Landroid/content/Context;

    const-class v1, Ljaa;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljaa;

    invoke-interface {v0}, Ljaa;->a()Z

    move-result v0

    return v0
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Ljat;->a:Landroid/content/Context;

    const-class v1, Ljgp;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgp;

    .line 38
    invoke-interface {v0}, Ljgp;->a()Z

    move-result v0

    return v0
.end method

.method private c(I)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 169
    .line 171
    invoke-direct {p0}, Ljat;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Ljat;->c:Lizp;

    invoke-interface {v0}, Lizp;->h()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ljat;->c:Lizp;

    invoke-interface {v0}, Lizp;->i()Ljava/lang/String;

    move-result-object v0

    const-string v3, "low_speed_network"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_9

    .line 172
    invoke-direct {p0}, Ljat;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Ljat;->c:Lizp;

    invoke-interface {v0}, Lizp;->h()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Ljat;->c:Lizp;

    invoke-interface {v0}, Lizp;->i()Ljava/lang/String;

    move-result-object v0

    const-string v3, "very_low_speed_network"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_b

    .line 174
    iget-object v0, p0, Ljat;->d:Ljgn;

    invoke-interface {v0}, Ljgn;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 175
    const/4 v0, 0x2

    .line 181
    :goto_2
    iget-object v3, p0, Ljat;->c:Lizp;

    invoke-interface {v3}, Lizp;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 182
    add-int/lit8 v0, v0, 0x1

    .line 185
    :cond_0
    if-nez v0, :cond_a

    move v1, p1

    .line 204
    :cond_1
    :goto_3
    :pswitch_0
    return v1

    .line 171
    :cond_2
    iget-object v0, p0, Ljat;->d:Ljgn;

    invoke-interface {v0}, Ljgn;->l()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Ljat;->c:Lizp;

    invoke-interface {v0}, Lizp;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    iget-object v0, p0, Ljat;->d:Ljgn;

    invoke-interface {v0}, Ljgn;->f()Z

    move-result v0

    goto :goto_0

    .line 172
    :cond_6
    iget-object v0, p0, Ljat;->d:Ljgn;

    invoke-interface {v0}, Ljgn;->l()I

    move-result v0

    const/4 v3, -0x2

    if-ne v0, v3, :cond_7

    move v0, v2

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    move v0, v1

    goto :goto_1

    :cond_9
    move v0, v2

    .line 177
    goto :goto_2

    .line 189
    :cond_a
    packed-switch p1, :pswitch_data_0

    .line 206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 192
    :pswitch_1
    const/4 v1, 0x3

    goto :goto_3

    .line 194
    :pswitch_2
    if-ne v0, v2, :cond_1

    move v1, v2

    .line 195
    goto :goto_3

    :cond_b
    move v0, v1

    goto :goto_2

    .line 189
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ljat;->c:Lizp;

    invoke-interface {v0}, Lizp;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 2

    .prologue
    .line 129
    invoke-direct {p0}, Ljat;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    const/4 v0, -0x1

    .line 138
    :goto_0
    return v0

    .line 133
    :cond_0
    invoke-direct {p0, p1}, Ljat;->c(I)I

    move-result v0

    .line 135
    invoke-direct {p0}, Ljat;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136
    const/4 v0, 0x1

    goto :goto_0

    .line 138
    :cond_1
    iget-object v1, p0, Ljat;->b:Lizx;

    invoke-interface {v1, v0}, Lizx;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method public a(II)I
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljat;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    :goto_0
    return p2

    .line 85
    :cond_0
    invoke-direct {p0, p1}, Ljat;->c(I)I

    move-result v0

    .line 87
    invoke-direct {p0}, Ljat;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 88
    packed-switch v0, :pswitch_data_0

    .line 118
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 91
    :pswitch_0
    and-int/lit8 v0, p2, -0x21

    :goto_1
    move p2, v0

    .line 120
    goto :goto_0

    .line 94
    :pswitch_1
    iget-object v0, p0, Ljat;->c:Lizp;

    iget-object v1, p0, Ljat;->c:Lizp;

    invoke-interface {v1}, Lizp;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lizp;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    or-int/lit8 v0, p2, 0x20

    goto :goto_1

    .line 98
    :cond_1
    and-int/lit8 v0, p2, -0x21

    .line 100
    goto :goto_1

    .line 102
    :pswitch_2
    iget-object v0, p0, Ljat;->c:Lizp;

    iget-object v1, p0, Ljat;->c:Lizp;

    invoke-interface {v1}, Lizp;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lizp;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    or-int/lit8 v0, p2, 0x20

    goto :goto_1

    .line 106
    :cond_2
    and-int/lit8 v0, p2, -0x21

    .line 108
    goto :goto_1

    .line 110
    :pswitch_3
    iget-object v0, p0, Ljat;->c:Lizp;

    iget-object v1, p0, Ljat;->c:Lizp;

    invoke-interface {v1}, Lizp;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lizp;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 111
    or-int/lit8 v0, p2, 0x20

    goto :goto_1

    .line 114
    :cond_3
    and-int/lit8 v0, p2, -0x21

    .line 116
    goto :goto_1

    .line 123
    :cond_4
    iget-object v1, p0, Ljat;->b:Lizx;

    invoke-interface {v1, v0, p2}, Lizx;->a(II)I

    move-result p2

    goto :goto_0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b(I)I
    .locals 2

    .prologue
    .line 144
    invoke-direct {p0}, Ljat;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    const/4 v0, -0x1

    .line 164
    :goto_0
    return v0

    .line 148
    :cond_0
    invoke-direct {p0, p1}, Ljat;->c(I)I

    move-result v0

    .line 150
    invoke-direct {p0}, Ljat;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    packed-switch v0, :pswitch_data_0

    .line 161
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 153
    :pswitch_0
    const/16 v0, 0x5a

    goto :goto_0

    .line 155
    :pswitch_1
    iget-object v0, p0, Ljat;->c:Lizp;

    invoke-interface {v0}, Lizp;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljat;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 157
    :pswitch_2
    iget-object v0, p0, Ljat;->c:Lizp;

    invoke-interface {v0}, Lizp;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljat;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 159
    :pswitch_3
    iget-object v0, p0, Ljat;->c:Lizp;

    invoke-interface {v0}, Lizp;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljat;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 164
    :cond_1
    iget-object v1, p0, Ljat;->b:Lizx;

    invoke-interface {v1, v0}, Lizx;->b(I)I

    move-result v0

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
