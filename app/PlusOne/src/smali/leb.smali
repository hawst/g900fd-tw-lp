.class public final Lleb;
.super Landroid/database/DataSetObserver;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZII)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 295
    const-string v0, "StreamGridView"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x63

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "SGDSO.onChanged destroyLayoutRecords="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", invalidateFrom="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", restorePosition="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 301
    :cond_0
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v2, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v2, v2, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;I)I

    .line 303
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)I

    move-result v0

    if-lez v0, :cond_1

    .line 304
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v2, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)I

    move-result v2

    iget-object v3, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v3, v3, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v3, v3, Lldv;->a:I

    add-int/2addr v2, v3

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;I)I

    .line 310
    :cond_1
    const/4 v0, -0x1

    if-eq p3, v0, :cond_2

    if-nez p1, :cond_2

    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)I

    move-result v0

    if-ge p3, v0, :cond_2

    .line 311
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V

    .line 312
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0, v4}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Z)Z

    .line 313
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, p3, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(II)V

    .line 314
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)Lljq;

    move-result-object v0

    invoke-virtual {v0}, Lljq;->f()V

    .line 315
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->requestLayout()V

    .line 351
    :goto_0
    return-void

    .line 319
    :cond_2
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v2, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)Z

    move-result v2

    or-int/2addr v2, p1

    invoke-static {v0, v2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Z)Z

    .line 322
    if-eqz p1, :cond_6

    .line 323
    if-gtz p2, :cond_4

    .line 324
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V

    .line 325
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;I)I

    .line 326
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->e(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V

    .line 327
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->f(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V

    .line 328
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->g(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)Lgu;

    move-result-object v0

    invoke-virtual {v0}, Lgu;->c()V

    .line 329
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a(II)V

    .line 347
    :goto_1
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->d(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 348
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->c(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)Lljq;

    move-result-object v0

    invoke-virtual {v0}, Lljq;->f()V

    .line 350
    :cond_3
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->requestLayout()V

    goto :goto_0

    .line 334
    :cond_4
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0, v4}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Z)Z

    .line 335
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    iget-object v0, v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->a:Lldv;

    iget v2, v0, Lldv;->a:I

    move v0, v1

    .line 337
    :goto_2
    if-ge v0, v2, :cond_5

    .line 338
    iget-object v3, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->h(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)[I

    move-result-object v3

    iget-object v4, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v4}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->i(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)[I

    move-result-object v4

    aget v4, v4, v0

    aput v4, v3, v0

    .line 337
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 340
    :cond_5
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->j(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V

    .line 341
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Z)Z

    goto :goto_1

    .line 344
    :cond_6
    iget-object v0, p0, Lleb;->a:Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;

    invoke-static {v0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;->b(Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;)V

    goto :goto_1
.end method

.method public onChanged()V
    .locals 0

    .prologue
    .line 288
    return-void
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 291
    return-void
.end method
