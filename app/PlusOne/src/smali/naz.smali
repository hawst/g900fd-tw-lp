.class public final Lnaz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnys;

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0}, Loxq;-><init>()V

    .line 243
    const/4 v0, 0x0

    iput-object v0, p0, Lnaz;->a:Lnys;

    .line 246
    const/high16 v0, -0x80000000

    iput v0, p0, Lnaz;->b:I

    .line 233
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 264
    iget-object v1, p0, Lnaz;->a:Lnys;

    if-eqz v1, :cond_0

    .line 265
    const/4 v0, 0x1

    iget-object v1, p0, Lnaz;->a:Lnys;

    .line 266
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 268
    :cond_0
    iget v1, p0, Lnaz;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 269
    const/4 v1, 0x2

    iget v2, p0, Lnaz;->b:I

    .line 270
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_1
    iget-object v1, p0, Lnaz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    iput v0, p0, Lnaz;->ai:I

    .line 274
    return v0
.end method

.method public a(Loxn;)Lnaz;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 282
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 283
    sparse-switch v0, :sswitch_data_0

    .line 287
    iget-object v1, p0, Lnaz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 288
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnaz;->ah:Ljava/util/List;

    .line 291
    :cond_1
    iget-object v1, p0, Lnaz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    :sswitch_0
    return-object p0

    .line 298
    :sswitch_1
    iget-object v0, p0, Lnaz;->a:Lnys;

    if-nez v0, :cond_2

    .line 299
    new-instance v0, Lnys;

    invoke-direct {v0}, Lnys;-><init>()V

    iput-object v0, p0, Lnaz;->a:Lnys;

    .line 301
    :cond_2
    iget-object v0, p0, Lnaz;->a:Lnys;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 305
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 306
    if-eq v0, v2, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 310
    :cond_3
    iput v0, p0, Lnaz;->b:I

    goto :goto_0

    .line 312
    :cond_4
    iput v2, p0, Lnaz;->b:I

    goto :goto_0

    .line 283
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lnaz;->a:Lnys;

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x1

    iget-object v1, p0, Lnaz;->a:Lnys;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 254
    :cond_0
    iget v0, p0, Lnaz;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 255
    const/4 v0, 0x2

    iget v1, p0, Lnaz;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 257
    :cond_1
    iget-object v0, p0, Lnaz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 259
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0, p1}, Lnaz;->a(Loxn;)Lnaz;

    move-result-object v0

    return-object v0
.end method
