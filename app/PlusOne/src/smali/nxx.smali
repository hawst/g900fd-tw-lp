.class public final Lnxx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnxx;


# instance fields
.field private b:[I

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x0

    new-array v0, v0, [Lnxx;

    sput-object v0, Lnxx;->a:[Lnxx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Loxq;-><init>()V

    .line 194
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lnxx;->b:[I

    .line 175
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 215
    .line 216
    iget-object v1, p0, Lnxx;->b:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lnxx;->b:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 218
    iget-object v2, p0, Lnxx;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 220
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_0
    iget-object v0, p0, Lnxx;->b:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 225
    :cond_1
    iget-object v1, p0, Lnxx;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 226
    const/4 v1, 0x2

    iget-object v2, p0, Lnxx;->c:Ljava/lang/String;

    .line 227
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    :cond_2
    iget-object v1, p0, Lnxx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    iput v0, p0, Lnxx;->ai:I

    .line 231
    return v0
.end method

.method public a(Loxn;)Lnxx;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 239
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 240
    sparse-switch v0, :sswitch_data_0

    .line 244
    iget-object v1, p0, Lnxx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 245
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnxx;->ah:Ljava/util/List;

    .line 248
    :cond_1
    iget-object v1, p0, Lnxx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    :sswitch_0
    return-object p0

    .line 255
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 256
    iget-object v0, p0, Lnxx;->b:[I

    array-length v0, v0

    .line 257
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 258
    iget-object v2, p0, Lnxx;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 259
    iput-object v1, p0, Lnxx;->b:[I

    .line 260
    :goto_1
    iget-object v1, p0, Lnxx;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 261
    iget-object v1, p0, Lnxx;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 262
    invoke-virtual {p1}, Loxn;->a()I

    .line 260
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 265
    :cond_2
    iget-object v1, p0, Lnxx;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 269
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnxx;->c:Ljava/lang/String;

    goto :goto_0

    .line 240
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 201
    iget-object v0, p0, Lnxx;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnxx;->b:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 202
    iget-object v1, p0, Lnxx;->b:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 203
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    iget-object v0, p0, Lnxx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 207
    const/4 v0, 0x2

    iget-object v1, p0, Lnxx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 209
    :cond_1
    iget-object v0, p0, Lnxx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 211
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0, p1}, Lnxx;->a(Loxn;)Lnxx;

    move-result-object v0

    return-object v0
.end method
