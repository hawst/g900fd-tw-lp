.class public final Lpna;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lpvy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1003
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1010
    const/4 v0, 0x0

    iput-object v0, p0, Lpna;->c:Lpvy;

    .line 1003
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1030
    const/4 v0, 0x0

    .line 1031
    iget-object v1, p0, Lpna;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1032
    const/4 v0, 0x1

    iget-object v1, p0, Lpna;->a:Ljava/lang/String;

    .line 1033
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1035
    :cond_0
    iget-object v1, p0, Lpna;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1036
    const/4 v1, 0x2

    iget-object v2, p0, Lpna;->b:Ljava/lang/String;

    .line 1037
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1039
    :cond_1
    iget-object v1, p0, Lpna;->c:Lpvy;

    if-eqz v1, :cond_2

    .line 1040
    const/4 v1, 0x3

    iget-object v2, p0, Lpna;->c:Lpvy;

    .line 1041
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1043
    :cond_2
    iget-object v1, p0, Lpna;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1044
    iput v0, p0, Lpna;->ai:I

    .line 1045
    return v0
.end method

.method public a(Loxn;)Lpna;
    .locals 2

    .prologue
    .line 1053
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1054
    sparse-switch v0, :sswitch_data_0

    .line 1058
    iget-object v1, p0, Lpna;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1059
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpna;->ah:Ljava/util/List;

    .line 1062
    :cond_1
    iget-object v1, p0, Lpna;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1064
    :sswitch_0
    return-object p0

    .line 1069
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpna;->a:Ljava/lang/String;

    goto :goto_0

    .line 1073
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpna;->b:Ljava/lang/String;

    goto :goto_0

    .line 1077
    :sswitch_3
    iget-object v0, p0, Lpna;->c:Lpvy;

    if-nez v0, :cond_2

    .line 1078
    new-instance v0, Lpvy;

    invoke-direct {v0}, Lpvy;-><init>()V

    iput-object v0, p0, Lpna;->c:Lpvy;

    .line 1080
    :cond_2
    iget-object v0, p0, Lpna;->c:Lpvy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1054
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1015
    iget-object v0, p0, Lpna;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1016
    const/4 v0, 0x1

    iget-object v1, p0, Lpna;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1018
    :cond_0
    iget-object v0, p0, Lpna;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1019
    const/4 v0, 0x2

    iget-object v1, p0, Lpna;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1021
    :cond_1
    iget-object v0, p0, Lpna;->c:Lpvy;

    if-eqz v0, :cond_2

    .line 1022
    const/4 v0, 0x3

    iget-object v1, p0, Lpna;->c:Lpvy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1024
    :cond_2
    iget-object v0, p0, Lpna;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1026
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 999
    invoke-virtual {p0, p1}, Lpna;->a(Loxn;)Lpna;

    move-result-object v0

    return-object v0
.end method
