.class final Lhvi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lhvh;


# direct methods
.method constructor <init>(Lhvh;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lhvi;->a:Lhvh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 66
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v1, v1, Lhvh;->a:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v1, v1, Lhvh;->b:[Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v1, v1, Lhvh;->c:[Landroid/graphics/Rect;

    if-nez v1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v1, v1, Lhvh;->a:[Ljava/lang/String;

    array-length v1, v1

    .line 71
    if-le v1, v6, :cond_0

    .line 75
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 76
    iget-object v4, p0, Lhvi;->a:Lhvh;

    iget v4, v4, Lhvh;->e:I

    if-nez v4, :cond_5

    .line 77
    iget-object v4, p0, Lhvi;->a:Lhvh;

    iget-wide v4, v4, Lhvh;->f:J

    cmp-long v4, v2, v4

    if-ltz v4, :cond_4

    .line 78
    iget-object v4, p0, Lhvi;->a:Lhvh;

    iput v6, v4, Lhvh;->e:I

    .line 79
    iget-object v4, p0, Lhvi;->a:Lhvh;

    const-wide/16 v6, 0x3e8

    add-long/2addr v2, v6

    iput-wide v2, v4, Lhvh;->f:J

    .line 80
    iget-object v2, p0, Lhvi;->a:Lhvh;

    iget-object v3, p0, Lhvi;->a:Lhvh;

    iget v3, v3, Lhvh;->h:I

    add-int/lit8 v3, v3, 0x1

    rem-int v1, v3, v1

    iput v1, v2, Lhvh;->i:I

    .line 82
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v1, v1, Lhvh;->c:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 83
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v1, v1, Lhvh;->c:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    iget v4, v1, Landroid/graphics/Rect;->right:I

    move v1, v0

    .line 86
    :goto_1
    iget-object v2, p0, Lhvi;->a:Lhvh;

    iget-object v2, v2, Lhvh;->c:[Landroid/graphics/Rect;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 87
    iget-object v2, p0, Lhvi;->a:Lhvh;

    iget v2, v2, Lhvh;->i:I

    if-ne v0, v2, :cond_2

    iget-object v2, p0, Lhvi;->a:Lhvh;

    iget v2, v2, Lhvh;->d:I

    :goto_2
    add-int/2addr v2, v1

    .line 89
    iget-object v5, p0, Lhvi;->a:Lhvh;

    iget-object v5, v5, Lhvh;->c:[Landroid/graphics/Rect;

    aget-object v5, v5, v0

    invoke-virtual {v5, v3, v1, v4, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 86
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_1

    .line 87
    :cond_2
    iget-object v2, p0, Lhvi;->a:Lhvh;

    iget v2, v2, Lhvh;->d:I

    div-int/lit8 v2, v2, 0x4

    goto :goto_2

    .line 93
    :cond_3
    iget-object v0, p0, Lhvi;->a:Lhvh;

    invoke-virtual {v0}, Lhvh;->invalidate()V

    .line 113
    :cond_4
    :goto_3
    iget-object v0, p0, Lhvi;->a:Lhvh;

    iget-boolean v0, v0, Lhvh;->j:Z

    if-eqz v0, :cond_0

    .line 114
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v1, v1, Lhvh;->k:Ljava/lang/Runnable;

    const-wide/16 v2, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 95
    :cond_5
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget v1, v1, Lhvh;->e:I

    if-ne v1, v6, :cond_0

    .line 96
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-wide v4, v1, Lhvh;->f:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_6

    .line 97
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iput v0, v1, Lhvh;->e:I

    .line 98
    iget-object v1, p0, Lhvi;->a:Lhvh;

    const-wide/16 v4, 0x1b58

    add-long/2addr v2, v4

    iput-wide v2, v1, Lhvh;->f:J

    .line 99
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iput v7, v1, Lhvh;->g:F

    .line 100
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v2, p0, Lhvi;->a:Lhvh;

    iget v2, v2, Lhvh;->i:I

    iput v2, v1, Lhvh;->h:I

    .line 101
    :goto_4
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v1, v1, Lhvh;->b:[Landroid/graphics/Rect;

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 102
    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-object v1, v1, Lhvh;->b:[Landroid/graphics/Rect;

    aget-object v1, v1, v0

    iget-object v2, p0, Lhvi;->a:Lhvh;

    iget-object v2, v2, Lhvh;->c:[Landroid/graphics/Rect;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 105
    :cond_6
    iget-object v0, p0, Lhvi;->a:Lhvh;

    iget-object v1, p0, Lhvi;->a:Lhvh;

    iget-wide v4, v1, Lhvh;->f:J

    sub-long v2, v4, v2

    long-to-float v1, v2

    mul-float/2addr v1, v7

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    iput v1, v0, Lhvh;->g:F

    .line 108
    :cond_7
    iget-object v0, p0, Lhvi;->a:Lhvh;

    invoke-virtual {v0}, Lhvh;->invalidate()V

    goto :goto_3
.end method
