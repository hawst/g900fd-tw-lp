.class public final Lnhx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnhx;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5747
    const/4 v0, 0x0

    new-array v0, v0, [Lnhx;

    sput-object v0, Lnhx;->a:[Lnhx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5748
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5756
    const/high16 v0, -0x80000000

    iput v0, p0, Lnhx;->b:I

    .line 5748
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5775
    const/4 v0, 0x0

    .line 5776
    iget v1, p0, Lnhx;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 5777
    const/4 v0, 0x1

    iget v1, p0, Lnhx;->b:I

    .line 5778
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5780
    :cond_0
    iget-object v1, p0, Lnhx;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5781
    const/4 v1, 0x2

    iget-object v2, p0, Lnhx;->c:Ljava/lang/String;

    .line 5782
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5784
    :cond_1
    iget-object v1, p0, Lnhx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5785
    iput v0, p0, Lnhx;->ai:I

    .line 5786
    return v0
.end method

.method public a(Loxn;)Lnhx;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5794
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5795
    sparse-switch v0, :sswitch_data_0

    .line 5799
    iget-object v1, p0, Lnhx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5800
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnhx;->ah:Ljava/util/List;

    .line 5803
    :cond_1
    iget-object v1, p0, Lnhx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5805
    :sswitch_0
    return-object p0

    .line 5810
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5811
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 5813
    :cond_2
    iput v0, p0, Lnhx;->b:I

    goto :goto_0

    .line 5815
    :cond_3
    iput v2, p0, Lnhx;->b:I

    goto :goto_0

    .line 5820
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnhx;->c:Ljava/lang/String;

    goto :goto_0

    .line 5795
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5763
    iget v0, p0, Lnhx;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 5764
    const/4 v0, 0x1

    iget v1, p0, Lnhx;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5766
    :cond_0
    iget-object v0, p0, Lnhx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5767
    const/4 v0, 0x2

    iget-object v1, p0, Lnhx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5769
    :cond_1
    iget-object v0, p0, Lnhx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5771
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5744
    invoke-virtual {p0, p1}, Lnhx;->a(Loxn;)Lnhx;

    move-result-object v0

    return-object v0
.end method
