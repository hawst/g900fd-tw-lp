.class public final Lhhw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhfv;


# instance fields
.field private a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lhfw;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Lhfv;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;Lhfv;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lhfw;",
            ">;",
            "Lhfv;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lhhw;->a:Landroid/util/SparseArray;

    .line 28
    iput-object p2, p0, Lhhw;->b:Lhfv;

    .line 29
    iput-object p3, p0, Lhhw;->c:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 32
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfw;

    .line 33
    invoke-virtual {v0}, Lhfw;->b()I

    move-result v3

    .line 34
    iget-object v1, p0, Lhhw;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 35
    if-nez v1, :cond_0

    .line 36
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 37
    iget-object v4, p0, Lhhw;->a:Landroid/util/SparseArray;

    invoke-virtual {v4, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 39
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 41
    :cond_1
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lhhw;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 55
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public a(Lhfw;)I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lhhw;->b:Lhfv;

    invoke-interface {v0, p1}, Lhfv;->a(Lhfw;)I

    move-result v0

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lhhw;->c:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/content/Context;Lhfw;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lhhw;->b:Lhfv;

    invoke-interface {v0, p1, p2}, Lhfv;->a(Landroid/content/Context;Lhfw;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(IILhfw;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lhhw;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 49
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfw;

    invoke-virtual {v0, p3}, Lhfw;->a(Lhfw;)V

    .line 50
    return-void
.end method

.method public a(Landroid/database/Cursor;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 44
    return-void
.end method

.method public a(Lu;ILhxc;Ljava/lang/String;Llgs;)Z
    .locals 6

    .prologue
    .line 91
    iget-object v0, p0, Lhhw;->b:Lhfv;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lhfv;->a(Lu;ILhxc;Ljava/lang/String;Llgs;)Z

    move-result v0

    return v0
.end method

.method public b(Lhfw;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhfw;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lhhw;->b:Lhfv;

    invoke-interface {v0, p1}, Lhfv;->b(Lhfw;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lhhw;->b:Lhfv;

    invoke-interface {v0}, Lhfv;->b()Z

    move-result v0

    return v0
.end method

.method public c(Lhfw;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lhhw;->b:Lhfv;

    invoke-interface {v0, p1}, Lhfv;->c(Lhfw;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
