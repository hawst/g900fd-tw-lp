.class public final Lnyk;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Ljava/lang/Integer;

.field private b:[Ljava/lang/Integer;

.field private c:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6240
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6243
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lnyk;->a:[Ljava/lang/Integer;

    .line 6246
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lnyk;->b:[Ljava/lang/Integer;

    .line 6249
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Lnyk;->c:[Ljava/lang/Integer;

    .line 6240
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 6275
    .line 6276
    iget-object v0, p0, Lnyk;->a:[Ljava/lang/Integer;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lnyk;->a:[Ljava/lang/Integer;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 6278
    iget-object v3, p0, Lnyk;->a:[Ljava/lang/Integer;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 6280
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 6278
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6283
    :cond_0
    iget-object v0, p0, Lnyk;->a:[Ljava/lang/Integer;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 6285
    :goto_1
    iget-object v2, p0, Lnyk;->b:[Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnyk;->b:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 6287
    iget-object v4, p0, Lnyk;->b:[Ljava/lang/Integer;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 6289
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 6287
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 6291
    :cond_1
    add-int/2addr v0, v3

    .line 6292
    iget-object v2, p0, Lnyk;->b:[Ljava/lang/Integer;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 6294
    :cond_2
    iget-object v2, p0, Lnyk;->c:[Ljava/lang/Integer;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lnyk;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 6296
    iget-object v3, p0, Lnyk;->c:[Ljava/lang/Integer;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 6298
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 6296
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 6300
    :cond_3
    add-int/2addr v0, v2

    .line 6301
    iget-object v1, p0, Lnyk;->c:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6303
    :cond_4
    iget-object v1, p0, Lnyk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6304
    iput v0, p0, Lnyk;->ai:I

    .line 6305
    return v0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lnyk;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6313
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6314
    sparse-switch v0, :sswitch_data_0

    .line 6318
    iget-object v1, p0, Lnyk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6319
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyk;->ah:Ljava/util/List;

    .line 6322
    :cond_1
    iget-object v1, p0, Lnyk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6324
    :sswitch_0
    return-object p0

    .line 6329
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 6330
    iget-object v0, p0, Lnyk;->a:[Ljava/lang/Integer;

    array-length v0, v0

    .line 6331
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 6332
    iget-object v2, p0, Lnyk;->a:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6333
    iput-object v1, p0, Lnyk;->a:[Ljava/lang/Integer;

    .line 6334
    :goto_1
    iget-object v1, p0, Lnyk;->a:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 6335
    iget-object v1, p0, Lnyk;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 6336
    invoke-virtual {p1}, Loxn;->a()I

    .line 6334
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6339
    :cond_2
    iget-object v1, p0, Lnyk;->a:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 6343
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 6344
    iget-object v0, p0, Lnyk;->b:[Ljava/lang/Integer;

    array-length v0, v0

    .line 6345
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 6346
    iget-object v2, p0, Lnyk;->b:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6347
    iput-object v1, p0, Lnyk;->b:[Ljava/lang/Integer;

    .line 6348
    :goto_2
    iget-object v1, p0, Lnyk;->b:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 6349
    iget-object v1, p0, Lnyk;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 6350
    invoke-virtual {p1}, Loxn;->a()I

    .line 6348
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6353
    :cond_3
    iget-object v1, p0, Lnyk;->b:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 6357
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 6358
    iget-object v0, p0, Lnyk;->c:[Ljava/lang/Integer;

    array-length v0, v0

    .line 6359
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 6360
    iget-object v2, p0, Lnyk;->c:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6361
    iput-object v1, p0, Lnyk;->c:[Ljava/lang/Integer;

    .line 6362
    :goto_3
    iget-object v1, p0, Lnyk;->c:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 6363
    iget-object v1, p0, Lnyk;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 6364
    invoke-virtual {p1}, Loxn;->a()I

    .line 6362
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6367
    :cond_4
    iget-object v1, p0, Lnyk;->c:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 6314
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6254
    iget-object v1, p0, Lnyk;->a:[Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 6255
    iget-object v2, p0, Lnyk;->a:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 6256
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 6255
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6259
    :cond_0
    iget-object v1, p0, Lnyk;->b:[Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 6260
    iget-object v2, p0, Lnyk;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6261
    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 6260
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6264
    :cond_1
    iget-object v1, p0, Lnyk;->c:[Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 6265
    iget-object v1, p0, Lnyk;->c:[Ljava/lang/Integer;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 6266
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 6265
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6269
    :cond_2
    iget-object v0, p0, Lnyk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6271
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6236
    invoke-virtual {p0, p1}, Lnyk;->a(Loxn;)Lnyk;

    move-result-object v0

    return-object v0
.end method
