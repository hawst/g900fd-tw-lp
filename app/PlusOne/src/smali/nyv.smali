.class public final Lnyv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7064
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 7085
    const/4 v0, 0x0

    .line 7086
    iget-object v1, p0, Lnyv;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 7087
    const/4 v0, 0x1

    iget-object v1, p0, Lnyv;->a:Ljava/lang/Long;

    .line 7088
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7090
    :cond_0
    iget-object v1, p0, Lnyv;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 7091
    const/4 v1, 0x2

    iget-object v2, p0, Lnyv;->b:Ljava/lang/Long;

    .line 7092
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7094
    :cond_1
    iget-object v1, p0, Lnyv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7095
    iput v0, p0, Lnyv;->ai:I

    .line 7096
    return v0
.end method

.method public a(Loxn;)Lnyv;
    .locals 2

    .prologue
    .line 7104
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7105
    sparse-switch v0, :sswitch_data_0

    .line 7109
    iget-object v1, p0, Lnyv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnyv;->ah:Ljava/util/List;

    .line 7113
    :cond_1
    iget-object v1, p0, Lnyv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7115
    :sswitch_0
    return-object p0

    .line 7120
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnyv;->a:Ljava/lang/Long;

    goto :goto_0

    .line 7124
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnyv;->b:Ljava/lang/Long;

    goto :goto_0

    .line 7105
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 7073
    iget-object v0, p0, Lnyv;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 7074
    const/4 v0, 0x1

    iget-object v1, p0, Lnyv;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 7076
    :cond_0
    iget-object v0, p0, Lnyv;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 7077
    const/4 v0, 0x2

    iget-object v1, p0, Lnyv;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 7079
    :cond_1
    iget-object v0, p0, Lnyv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7081
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7060
    invoke-virtual {p0, p1}, Lnyv;->a(Loxn;)Lnyv;

    move-result-object v0

    return-object v0
.end method
