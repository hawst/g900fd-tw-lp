.class public final Lfyd;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Lozp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/16 v7, 0x11

    const/4 v2, 0x1

    const/4 v6, -0x1

    const v5, -0x777778

    .line 87
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 88
    sget-object v0, Lfyd;->a:Llct;

    if-nez v0, :cond_0

    .line 89
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lfyd;->a:Llct;

    .line 91
    :cond_0
    new-instance v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 92
    iget-object v0, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 93
    iget-object v0, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    .line 94
    iget-object v0, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 95
    iget-object v0, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(F)V

    .line 96
    iget-object v0, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setBackgroundColor(I)V

    .line 98
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    .line 99
    iget-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 100
    iget-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    sget-object v1, Lfyd;->a:Llct;

    iget v1, v1, Llct;->P:I

    sget-object v2, Lfyd;->a:Llct;

    iget v2, v2, Llct;->S:I

    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->P:I

    sget-object v4, Lfyd;->a:Llct;

    iget v4, v4, Llct;->S:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 104
    iget-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    .line 105
    invoke-virtual {p0}, Lfyd;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a096b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 104
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfyd;->h:Landroid/widget/TextView;

    .line 108
    iget-object v0, p0, Lfyd;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 109
    iget-object v0, p0, Lfyd;->h:Landroid/widget/TextView;

    sget-object v1, Lfyd;->a:Llct;

    iget v1, v1, Llct;->P:I

    sget-object v2, Lfyd;->a:Llct;

    iget v2, v2, Llct;->S:I

    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->Q:I

    sget-object v4, Lfyd;->a:Llct;

    iget v4, v4, Llct;->S:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 114
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfyd;->l:Landroid/widget/ImageView;

    .line 115
    iget-object v0, p0, Lfyd;->l:Landroid/widget/ImageView;

    sget-object v1, Lfyd;->a:Llct;

    iget-object v1, v1, Llct;->M:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 116
    iget-object v0, p0, Lfyd;->l:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 118
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfyd;->i:Landroid/widget/ImageView;

    .line 119
    iget-object v0, p0, Lfyd;->i:Landroid/widget/ImageView;

    sget-object v1, Lfyd;->a:Llct;

    iget-object v1, v1, Llct;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 120
    iget-object v0, p0, Lfyd;->i:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 122
    const/16 v0, 0x25

    invoke-static {p1, p2, p3, v0}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lfyd;->j:Landroid/widget/TextView;

    .line 124
    iget-object v0, p0, Lfyd;->j:Landroid/widget/TextView;

    sget-object v1, Lfyd;->a:Llct;

    iget v1, v1, Llct;->aH:I

    sget-object v2, Lfyd;->a:Llct;

    iget v2, v2, Llct;->aI:I

    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->aH:I

    sget-object v4, Lfyd;->a:Llct;

    iget v4, v4, Llct;->aI:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 127
    iget-object v0, p0, Lfyd;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 128
    iget-object v0, p0, Lfyd;->j:Landroid/widget/TextView;

    sget-object v1, Lfyd;->a:Llct;

    iget v1, v1, Llct;->aJ:F

    sget-object v2, Lfyd;->a:Llct;

    iget v2, v2, Llct;->aK:F

    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->aL:F

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 132
    const/16 v0, 0x28

    invoke-static {p1, p2, p3, v0}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lfyd;->k:Landroid/widget/TextView;

    .line 134
    iget-object v0, p0, Lfyd;->k:Landroid/widget/TextView;

    sget-object v1, Lfyd;->a:Llct;

    iget v1, v1, Llct;->aH:I

    sget-object v2, Lfyd;->a:Llct;

    iget v2, v2, Llct;->aI:I

    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->aH:I

    sget-object v4, Lfyd;->a:Llct;

    iget v4, v4, Llct;->aI:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 137
    iget-object v0, p0, Lfyd;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 138
    iget-object v0, p0, Lfyd;->k:Landroid/widget/TextView;

    sget-object v1, Lfyd;->a:Llct;

    iget v1, v1, Llct;->aJ:F

    sget-object v2, Lfyd;->a:Llct;

    iget v2, v2, Llct;->aK:F

    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->aL:F

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 141
    return-void
.end method

.method private a(Z)V
    .locals 12

    .prologue
    const v11, 0x7f0a096d

    const v10, 0x7f02013e

    const/4 v7, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 170
    invoke-virtual {p0}, Lfyd;->removeAllViews()V

    .line 172
    invoke-virtual {p0}, Lfyd;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    .line 174
    iget-object v0, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0, v0}, Lfyd;->addView(Landroid/view/View;)V

    .line 176
    invoke-direct {p0}, Lfyd;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfyd;->b:Lozp;

    .line 177
    invoke-static {v0}, Ldrm;->a(Lozp;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_0
    move v1, v3

    .line 178
    :goto_0
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v0, p0, Lfyd;->b:Lozp;

    invoke-static {v0}, Ldrm;->d(Lozp;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-direct {p0}, Lfyd;->e()Loyy;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-direct {p0}, Lfyd;->e()Loyy;

    move-result-object v0

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    :goto_1
    const-wide/16 v8, 0x2710

    sub-long/2addr v4, v8

    const-wide/32 v8, 0x493e0

    div-long/2addr v4, v8

    long-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    const v4, 0x45c11f

    if-le v0, v4, :cond_d

    const-string v4, "https://i1.ytimg.com/vi/"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%s/wide_360p_v%s.jpg"

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lfyd;->b:Lozp;

    iget-object v8, v8, Lozp;->m:Ljava/lang/String;

    aput-object v8, v7, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v3

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_2
    iget-object v0, p0, Lfyd;->b:Lozp;

    if-eqz v0, :cond_2

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->r:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_3
    invoke-static {v6}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v5

    .line 179
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    .line 180
    if-nez v6, :cond_11

    const-string v0, ".gif"

    invoke-virtual {v5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    move v4, v3

    .line 182
    :goto_4
    if-nez v6, :cond_3

    .line 183
    if-eqz p1, :cond_12

    if-eqz v1, :cond_12

    sget-object v0, Ljac;->b:Ljac;

    .line 184
    :goto_5
    if-eqz v4, :cond_13

    .line 185
    sget-object v0, Ljac;->d:Ljac;

    .line 186
    iget-object v7, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v7, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(Z)V

    .line 190
    :goto_6
    invoke-virtual {p0}, Lfyd;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v5, v0}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    .line 191
    iget-object v5, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v5, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 193
    :cond_3
    if-eqz p1, :cond_5

    if-nez v6, :cond_4

    if-eqz v4, :cond_6

    :cond_4
    if-eqz v1, :cond_6

    .line 194
    :cond_5
    if-eqz p1, :cond_14

    sget-object v0, Lfyd;->a:Llct;

    iget-object v0, v0, Llct;->f:Landroid/graphics/Bitmap;

    .line 195
    :goto_7
    iget-object v1, p0, Lfyd;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 196
    iget-object v0, p0, Lfyd;->i:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lfyd;->addView(Landroid/view/View;)V

    .line 199
    :cond_6
    iget-object v0, p0, Lfyd;->b:Lozp;

    invoke-static {v0}, Ldrm;->a(Lozp;)Z

    move-result v0

    if-nez v0, :cond_15

    :goto_8
    if-eqz v3, :cond_7

    .line 200
    iget-object v0, p0, Lfyd;->l:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lfyd;->addView(Landroid/view/View;)V

    .line 202
    :cond_7
    iget-object v0, p0, Lfyd;->b:Lozp;

    invoke-static {v0}, Ldrm;->d(Lozp;)Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-direct {p0}, Lfyd;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lfyd;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lfyd;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lfyd;->h:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfyd;->addView(Landroid/view/View;)V

    :cond_8
    iget-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    const v1, 0x7f0a096b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfyd;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    const v1, 0x7f02013d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 204
    :cond_9
    :goto_9
    if-eqz p1, :cond_a

    iget-object v0, p0, Lfyd;->b:Lozp;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lfyd;->b:Lozp;

    invoke-static {v0}, Ldrm;->a(Lozp;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 205
    iget-object v0, p0, Lfyd;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfyd;->addView(Landroid/view/View;)V

    .line 206
    iget-object v0, p0, Lfyd;->k:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfyd;->addView(Landroid/view/View;)V

    .line 209
    :cond_a
    invoke-virtual {p0}, Lfyd;->b()V

    .line 210
    return-void

    :cond_b
    move v1, v2

    .line 177
    goto/16 :goto_0

    .line 178
    :cond_c
    const-wide/16 v4, 0x0

    goto/16 :goto_1

    :cond_d
    const-string v4, "https://i1.ytimg.com/vi/"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%s/hddefault_v%s.jpg"

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lfyd;->b:Lozp;

    iget-object v8, v8, Lozp;->m:Ljava/lang/String;

    aput-object v8, v7, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v3

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_e
    iget-object v0, p0, Lfyd;->b:Lozp;

    invoke-static {v0}, Ldrm;->a(Lozp;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "https://i1.ytimg.com/vi/"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "%s/hqdefault.jpg"

    new-array v5, v3, [Ljava/lang/Object;

    iget-object v7, p0, Lfyd;->b:Lozp;

    iget-object v7, v7, Lozp;->m:Ljava/lang/String;

    aput-object v7, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_f
    invoke-direct {p0}, Lfyd;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lfyd;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Llnf;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v4, "https://i1.ytimg.com/vi/"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%s/hqdefault.jpg"

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v0, v7, v2

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_10
    iget-object v4, p0, Lfyd;->d:Ljava/lang/String;

    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->j:Loya;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->j:Loya;

    sget-object v5, Lpai;->a:Loxr;

    invoke-virtual {v0, v5}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpai;

    iget-object v5, v0, Lpai;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_17

    iget-object v0, v0, Lpai;->b:Ljava/lang/String;

    :goto_a
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v0}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v4, 0x100

    invoke-static {v0, v4}, Ljbd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_11
    move v4, v2

    .line 180
    goto/16 :goto_4

    .line 183
    :cond_12
    sget-object v0, Ljac;->a:Ljac;

    goto/16 :goto_5

    .line 188
    :cond_13
    iget-object v7, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v7, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(Z)V

    goto/16 :goto_6

    .line 194
    :cond_14
    sget-object v0, Lfyd;->a:Llct;

    iget-object v0, v0, Llct;->g:Landroid/graphics/Bitmap;

    goto/16 :goto_7

    :cond_15
    move v3, v2

    .line 199
    goto/16 :goto_8

    .line 202
    :cond_16
    invoke-direct {p0}, Lfyd;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfyd;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lfyd;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto/16 :goto_9

    :cond_17
    move-object v0, v4

    goto :goto_a
.end method

.method private c()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 393
    iget-object v0, p0, Lfyd;->b:Lozp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->q:[Lozl;

    if-nez v0, :cond_1

    .line 402
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 396
    :goto_1
    iget-object v3, p0, Lfyd;->b:Lozp;

    iget-object v3, v3, Lozp;->q:[Lozl;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 397
    iget-object v3, p0, Lfyd;->b:Lozp;

    iget-object v3, v3, Lozp;->q:[Lozl;

    aget-object v3, v3, v0

    iget v3, v3, Lozl;->b:I

    if-ne v3, v2, :cond_2

    move v1, v2

    .line 399
    goto :goto_0

    .line 396
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lfyd;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lfyd;->e:Ljava/lang/String;

    .line 478
    :goto_0
    return-object v0

    .line 474
    :cond_0
    iget-object v0, p0, Lfyd;->b:Lozp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->l:Lpbj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->l:Lpbj;

    iget-object v0, v0, Lpbj;->f:Lltl;

    if-eqz v0, :cond_1

    .line 476
    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->l:Lpbj;

    iget-object v0, v0, Lpbj;->f:Lltl;

    iget-object v0, v0, Lltl;->a:Ljava/lang/String;

    goto :goto_0

    .line 478
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Loyy;
    .locals 2

    .prologue
    .line 486
    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->n:Loya;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->n:Loya;

    sget-object v1, Loyy;->a:Loxr;

    .line 487
    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 492
    invoke-static {p0}, Llii;->e(Landroid/view/View;)V

    .line 493
    invoke-virtual {p0}, Lfyd;->removeAllViews()V

    .line 494
    iput-object v0, p0, Lfyd;->e:Ljava/lang/String;

    .line 495
    iput-object v0, p0, Lfyd;->b:Lozp;

    .line 496
    iget-object v0, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 497
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 148
    iput-object p1, p0, Lfyd;->e:Ljava/lang/String;

    .line 149
    iput-object v0, p0, Lfyd;->b:Lozp;

    .line 150
    iput-object v0, p0, Lfyd;->c:Ljava/lang/String;

    .line 151
    iput-object v0, p0, Lfyd;->d:Ljava/lang/String;

    .line 153
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lfyd;->a(Z)V

    .line 154
    return-void
.end method

.method public a(Lozp;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lfyd;->e:Ljava/lang/String;

    .line 162
    iput-object p1, p0, Lfyd;->b:Lozp;

    .line 163
    iput-object p2, p0, Lfyd;->c:Ljava/lang/String;

    .line 164
    iput-object p3, p0, Lfyd;->d:Ljava/lang/String;

    .line 166
    invoke-direct {p0, p4}, Lfyd;->a(Z)V

    .line 167
    return-void
.end method

.method public b()V
    .locals 10

    .prologue
    .line 216
    invoke-virtual {p0}, Lfyd;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 217
    iget-object v1, p0, Lfyd;->c:Ljava/lang/String;

    .line 218
    iget-object v0, p0, Lfyd;->b:Lozp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->j:Loya;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->j:Loya;

    sget-object v2, Lpai;->a:Loxr;

    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpai;

    .line 220
    iget-object v2, v0, Lpai;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 221
    iget-object v0, v0, Lpai;->c:Ljava/lang/String;

    move-object v1, v0

    .line 225
    :cond_0
    iget-object v0, p0, Lfyd;->j:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_1

    iget-object v0, p0, Lfyd;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 227
    iget-object v0, p0, Lfyd;->b:Lozp;

    iget-object v0, v0, Lozp;->h:Loya;

    sget-object v2, Loyy;->a:Loxr;

    invoke-virtual {v0, v2}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loyy;

    iget-object v0, v0, Loyy;->b:Ljava/lang/Long;

    .line 228
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 230
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v6, v4

    if-lez v2, :cond_6

    .line 231
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 232
    iget-object v2, p0, Lfyd;->j:Landroid/widget/TextView;

    const v6, 0x7f0a0910

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v1, v7, v8

    .line 233
    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 232
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long/2addr v0, v4

    .line 240
    iget-object v4, p0, Lfyd;->k:Landroid/widget/TextView;

    .line 241
    const-wide/32 v6, 0xea60

    add-long/2addr v0, v6

    const-wide/32 v6, 0x5265c00

    div-long v6, v0, v6

    long-to-int v2, v6

    const-wide/32 v6, 0x5265c00

    rem-long v6, v0, v6

    const-wide/32 v8, 0x36ee80

    div-long/2addr v6, v8

    long-to-int v5, v6

    const-wide/32 v6, 0x36ee80

    rem-long/2addr v0, v6

    const-wide/32 v6, 0xea60

    div-long/2addr v0, v6

    long-to-int v6, v0

    if-nez v2, :cond_3

    const-string v0, ""

    move-object v2, v0

    :goto_1
    if-nez v5, :cond_4

    const-string v0, ""

    move-object v1, v0

    :goto_2
    if-nez v6, :cond_5

    const-string v0, ""

    :goto_3
    const v5, 0x7f0a0446

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    const/4 v2, 0x1

    aput-object v1, v6, v2

    const/4 v1, 0x2

    aput-object v0, v6, v1

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 240
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    :cond_1
    :goto_4
    return-void

    .line 235
    :cond_2
    iget-object v1, p0, Lfyd;->j:Landroid/widget/TextView;

    const v2, 0x7f0a0911

    .line 236
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 235
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 241
    :cond_3
    const v0, 0x7f110019

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v1, v7

    invoke-virtual {v3, v0, v2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_4
    const v0, 0x7f11001a

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v1, v7

    invoke-virtual {v3, v0, v5, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    :cond_5
    const v0, 0x7f11001b

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v5, v7

    invoke-virtual {v3, v0, v6, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 243
    :cond_6
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 244
    iget-object v0, p0, Lfyd;->j:Landroid/widget/TextView;

    const v2, 0x7f0a0912

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    .line 245
    invoke-virtual {v3, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 244
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    :goto_5
    iget-object v0, p0, Lfyd;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 248
    :cond_7
    iget-object v0, p0, Lfyd;->j:Landroid/widget/TextView;

    const v1, 0x7f0a0913

    .line 249
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 248
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 300
    iget-object v0, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v0

    .line 301
    iget-object v1, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v1

    .line 302
    iget-object v2, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2, v3, v3, v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 304
    iget-object v2, p0, Lfyd;->l:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_0

    .line 306
    iget-object v2, p0, Lfyd;->l:Landroid/widget/ImageView;

    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->C:I

    sget-object v4, Lfyd;->a:Llct;

    iget v4, v4, Llct;->D:I

    iget-object v5, p0, Lfyd;->l:Landroid/widget/ImageView;

    .line 307
    invoke-virtual {v5}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lfyd;->l:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v6

    .line 306
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/ImageView;->layout(IIII)V

    .line 310
    :cond_0
    iget-object v2, p0, Lfyd;->g:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_1

    .line 311
    invoke-virtual {p0}, Lfyd;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lfyd;->g:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->C:I

    sub-int/2addr v2, v3

    .line 313
    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->D:I

    .line 314
    iget-object v4, p0, Lfyd;->g:Landroid/widget/TextView;

    iget-object v5, p0, Lfyd;->g:Landroid/widget/TextView;

    .line 315
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v2

    iget-object v6, p0, Lfyd;->g:Landroid/widget/TextView;

    .line 316
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v3

    .line 314
    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 319
    :cond_1
    iget-object v2, p0, Lfyd;->h:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_2

    .line 321
    sget-object v2, Lfyd;->a:Llct;

    iget v2, v2, Llct;->R:I

    .line 322
    invoke-virtual {p0}, Lfyd;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lfyd;->g:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    sget-object v4, Lfyd;->a:Llct;

    iget v4, v4, Llct;->C:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lfyd;->h:Landroid/widget/TextView;

    .line 323
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/2addr v2, v3

    .line 325
    sget-object v3, Lfyd;->a:Llct;

    iget v3, v3, Llct;->D:I

    .line 326
    iget-object v4, p0, Lfyd;->h:Landroid/widget/TextView;

    iget-object v5, p0, Lfyd;->h:Landroid/widget/TextView;

    .line 327
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v2

    iget-object v6, p0, Lfyd;->h:Landroid/widget/TextView;

    .line 328
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v3

    .line 326
    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/widget/TextView;->layout(IIII)V

    .line 330
    :cond_2
    iget-object v2, p0, Lfyd;->i:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_3

    .line 332
    iget-object v2, p0, Lfyd;->i:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    .line 333
    iget-object v3, p0, Lfyd;->i:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    .line 334
    div-int/lit8 v4, v0, 0x2

    div-int/lit8 v5, v2, 0x2

    sub-int/2addr v4, v5

    .line 335
    div-int/lit8 v5, v1, 0x2

    div-int/lit8 v6, v3, 0x2

    sub-int/2addr v5, v6

    .line 336
    add-int/2addr v3, v5

    .line 337
    iget-object v6, p0, Lfyd;->i:Landroid/widget/ImageView;

    add-int/2addr v2, v4

    invoke-virtual {v6, v4, v5, v2, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 339
    :cond_3
    iget-object v2, p0, Lfyd;->j:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_4

    iget-object v2, p0, Lfyd;->k:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_4

    .line 340
    iget-object v2, p0, Lfyd;->j:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    .line 341
    iget-object v3, p0, Lfyd;->k:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    .line 342
    iget-object v4, p0, Lfyd;->j:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    .line 343
    iget-object v5, p0, Lfyd;->k:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    .line 345
    div-int/lit8 v6, v0, 0x2

    div-int/lit8 v7, v5, 0x2

    sub-int/2addr v6, v7

    .line 346
    sub-int/2addr v1, v3

    sget-object v7, Lfyd;->a:Llct;

    iget v7, v7, Llct;->l:I

    sub-int/2addr v1, v7

    .line 347
    iget-object v7, p0, Lfyd;->k:Landroid/widget/TextView;

    add-int/2addr v5, v6

    add-int/2addr v3, v1

    invoke-virtual {v7, v6, v1, v5, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 350
    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v3, v4, 0x2

    sub-int/2addr v0, v3

    .line 351
    sub-int/2addr v1, v2

    .line 352
    iget-object v3, p0, Lfyd;->j:Landroid/widget/TextView;

    add-int/2addr v4, v0

    add-int/2addr v2, v1

    invoke-virtual {v3, v0, v1, v4, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 355
    :cond_4
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const/high16 v8, -0x80000000

    .line 258
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 259
    iget-object v1, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 260
    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    int-to-double v4, v0

    const-wide v6, 0x3ffc51eb851eb852L    # 1.77

    div-double/2addr v4, v6

    .line 261
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 259
    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 263
    iget-object v1, p0, Lfyd;->f:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v1

    .line 265
    iget-object v2, p0, Lfyd;->l:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_0

    .line 266
    iget-object v2, p0, Lfyd;->l:Landroid/widget/ImageView;

    div-int/lit8 v3, v0, 0x4

    .line 267
    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 268
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 266
    invoke-virtual {v2, v3, v4}, Landroid/widget/ImageView;->measure(II)V

    .line 271
    :cond_0
    iget-object v2, p0, Lfyd;->g:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_1

    .line 272
    iget-object v2, p0, Lfyd;->g:Landroid/widget/TextView;

    .line 273
    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 274
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 272
    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 277
    :cond_1
    iget-object v2, p0, Lfyd;->h:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_2

    .line 278
    iget-object v2, p0, Lfyd;->h:Landroid/widget/TextView;

    .line 279
    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 280
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 278
    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 282
    :cond_2
    iget-object v2, p0, Lfyd;->i:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_3

    .line 283
    iget-object v2, p0, Lfyd;->i:Landroid/widget/ImageView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 284
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 283
    invoke-virtual {v2, v3, v4}, Landroid/widget/ImageView;->measure(II)V

    .line 286
    :cond_3
    iget-object v2, p0, Lfyd;->j:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_4

    .line 287
    iget-object v2, p0, Lfyd;->j:Landroid/widget/TextView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 288
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 287
    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 290
    :cond_4
    iget-object v2, p0, Lfyd;->k:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_5

    .line 291
    iget-object v2, p0, Lfyd;->k:Landroid/widget/TextView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 292
    invoke-static {v9, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 291
    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 294
    :cond_5
    invoke-virtual {p0, v0, v1}, Lfyd;->setMeasuredDimension(II)V

    .line 295
    return-void
.end method
