.class public final Lbn;
.super Lcl;
.source "PG"


# instance fields
.field private final a:Landroid/os/Bundle;

.field private final b:[Lcq;

.field private c:I

.field private d:Ljava/lang/CharSequence;

.field private e:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2135
    new-instance v0, Lbo;

    invoke-direct {v0}, Lbo;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 1791
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lbn;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lcq;)V

    .line 1792
    return-void
.end method

.method constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;[Lcq;)V
    .locals 1

    .prologue
    .line 1795
    invoke-direct {p0}, Lcl;-><init>()V

    .line 1796
    iput p1, p0, Lbn;->c:I

    .line 1797
    invoke-static {p2}, Lbs;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lbn;->d:Ljava/lang/CharSequence;

    .line 1798
    iput-object p3, p0, Lbn;->e:Landroid/app/PendingIntent;

    .line 1799
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Lbn;->a:Landroid/os/Bundle;

    .line 1800
    iput-object p5, p0, Lbn;->b:[Lcq;

    .line 1801
    return-void

    .line 1799
    :cond_0
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 1805
    iget v0, p0, Lbn;->c:I

    return v0
.end method

.method protected b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1810
    iget-object v0, p0, Lbn;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected c()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 1815
    iget-object v0, p0, Lbn;->e:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1823
    iget-object v0, p0, Lbn;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method public e()[Lcq;
    .locals 1

    .prologue
    .line 1832
    iget-object v0, p0, Lbn;->b:[Lcq;

    return-object v0
.end method

.method public synthetic f()[Lcx;
    .locals 1

    .prologue
    .line 1772
    invoke-virtual {p0}, Lbn;->e()[Lcq;

    move-result-object v0

    return-object v0
.end method
