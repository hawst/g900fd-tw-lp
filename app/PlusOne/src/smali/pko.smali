.class public final Lpko;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpko;


# instance fields
.field private b:Ljava/lang/String;

.field private c:[Lpkk;

.field private d:Lppf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    new-array v0, v0, [Lpko;

    sput-object v0, Lpko;->a:[Lpko;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 254
    invoke-direct {p0}, Loxq;-><init>()V

    .line 259
    sget-object v0, Lpkk;->a:[Lpkk;

    iput-object v0, p0, Lpko;->c:[Lpkk;

    .line 262
    const/4 v0, 0x0

    iput-object v0, p0, Lpko;->d:Lppf;

    .line 254
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 286
    .line 287
    iget-object v0, p0, Lpko;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 288
    const/4 v0, 0x1

    iget-object v2, p0, Lpko;->b:Ljava/lang/String;

    .line 289
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 291
    :goto_0
    iget-object v2, p0, Lpko;->c:[Lpkk;

    if-eqz v2, :cond_1

    .line 292
    iget-object v2, p0, Lpko;->c:[Lpkk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 293
    if-eqz v4, :cond_0

    .line 294
    const/4 v5, 0x2

    .line 295
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 292
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 299
    :cond_1
    iget-object v1, p0, Lpko;->d:Lppf;

    if-eqz v1, :cond_2

    .line 300
    const/4 v1, 0x3

    iget-object v2, p0, Lpko;->d:Lppf;

    .line 301
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    :cond_2
    iget-object v1, p0, Lpko;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    iput v0, p0, Lpko;->ai:I

    .line 305
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpko;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 313
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 314
    sparse-switch v0, :sswitch_data_0

    .line 318
    iget-object v2, p0, Lpko;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 319
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpko;->ah:Ljava/util/List;

    .line 322
    :cond_1
    iget-object v2, p0, Lpko;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 324
    :sswitch_0
    return-object p0

    .line 329
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpko;->b:Ljava/lang/String;

    goto :goto_0

    .line 333
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 334
    iget-object v0, p0, Lpko;->c:[Lpkk;

    if-nez v0, :cond_3

    move v0, v1

    .line 335
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpkk;

    .line 336
    iget-object v3, p0, Lpko;->c:[Lpkk;

    if-eqz v3, :cond_2

    .line 337
    iget-object v3, p0, Lpko;->c:[Lpkk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 339
    :cond_2
    iput-object v2, p0, Lpko;->c:[Lpkk;

    .line 340
    :goto_2
    iget-object v2, p0, Lpko;->c:[Lpkk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 341
    iget-object v2, p0, Lpko;->c:[Lpkk;

    new-instance v3, Lpkk;

    invoke-direct {v3}, Lpkk;-><init>()V

    aput-object v3, v2, v0

    .line 342
    iget-object v2, p0, Lpko;->c:[Lpkk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 343
    invoke-virtual {p1}, Loxn;->a()I

    .line 340
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 334
    :cond_3
    iget-object v0, p0, Lpko;->c:[Lpkk;

    array-length v0, v0

    goto :goto_1

    .line 346
    :cond_4
    iget-object v2, p0, Lpko;->c:[Lpkk;

    new-instance v3, Lpkk;

    invoke-direct {v3}, Lpkk;-><init>()V

    aput-object v3, v2, v0

    .line 347
    iget-object v2, p0, Lpko;->c:[Lpkk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 351
    :sswitch_3
    iget-object v0, p0, Lpko;->d:Lppf;

    if-nez v0, :cond_5

    .line 352
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpko;->d:Lppf;

    .line 354
    :cond_5
    iget-object v0, p0, Lpko;->d:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 314
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 267
    iget-object v0, p0, Lpko;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 268
    const/4 v0, 0x1

    iget-object v1, p0, Lpko;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 270
    :cond_0
    iget-object v0, p0, Lpko;->c:[Lpkk;

    if-eqz v0, :cond_2

    .line 271
    iget-object v1, p0, Lpko;->c:[Lpkk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 272
    if-eqz v3, :cond_1

    .line 273
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 271
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 277
    :cond_2
    iget-object v0, p0, Lpko;->d:Lppf;

    if-eqz v0, :cond_3

    .line 278
    const/4 v0, 0x3

    iget-object v1, p0, Lpko;->d:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 280
    :cond_3
    iget-object v0, p0, Lpko;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 282
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 250
    invoke-virtual {p0, p1}, Lpko;->a(Loxn;)Lpko;

    move-result-object v0

    return-object v0
.end method
