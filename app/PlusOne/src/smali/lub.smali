.class public final Llub;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llub;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Llvn;

.field public d:I

.field public e:I

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Boolean;

.field private i:Ljava/lang/String;

.field private j:[Llud;

.field private k:Ljava/lang/Long;

.field private l:Ljava/lang/String;

.field private m:Llux;

.field private n:Llua;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Llub;

    sput-object v0, Llub;->a:[Llub;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33
    sget-object v0, Llud;->a:[Llud;

    iput-object v0, p0, Llub;->j:[Llud;

    .line 36
    iput-object v1, p0, Llub;->c:Llvn;

    .line 39
    iput v2, p0, Llub;->d:I

    .line 44
    iput v2, p0, Llub;->e:I

    .line 55
    iput-object v1, p0, Llub;->m:Llux;

    .line 58
    iput-object v1, p0, Llub;->n:Llua;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, -0x80000000

    .line 112
    .line 113
    iget-object v0, p0, Llub;->b:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 114
    const/4 v0, 0x1

    iget-object v2, p0, Llub;->b:Ljava/lang/String;

    .line 115
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 117
    :goto_0
    iget-object v2, p0, Llub;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 118
    const/4 v2, 0x2

    iget-object v3, p0, Llub;->i:Ljava/lang/String;

    .line 119
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 121
    :cond_0
    iget-object v2, p0, Llub;->j:[Llud;

    if-eqz v2, :cond_2

    .line 122
    iget-object v2, p0, Llub;->j:[Llud;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 123
    if-eqz v4, :cond_1

    .line 124
    const/4 v5, 0x3

    .line 125
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 122
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 129
    :cond_2
    iget-object v1, p0, Llub;->c:Llvn;

    if-eqz v1, :cond_3

    .line 130
    const/4 v1, 0x4

    iget-object v2, p0, Llub;->c:Llvn;

    .line 131
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_3
    iget v1, p0, Llub;->d:I

    if-eq v1, v6, :cond_4

    .line 134
    const/4 v1, 0x5

    iget v2, p0, Llub;->d:I

    .line 135
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_4
    iget-object v1, p0, Llub;->k:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 138
    const/4 v1, 0x6

    iget-object v2, p0, Llub;->k:Ljava/lang/Long;

    .line 139
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_5
    iget v1, p0, Llub;->e:I

    if-eq v1, v6, :cond_6

    .line 142
    const/4 v1, 0x7

    iget v2, p0, Llub;->e:I

    .line 143
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_6
    iget-object v1, p0, Llub;->l:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 146
    const/16 v1, 0x8

    iget-object v2, p0, Llub;->l:Ljava/lang/String;

    .line 147
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_7
    iget-object v1, p0, Llub;->f:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 150
    const/16 v1, 0x9

    iget-object v2, p0, Llub;->f:Ljava/lang/Long;

    .line 151
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_8
    iget-object v1, p0, Llub;->g:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 154
    const/16 v1, 0xa

    iget-object v2, p0, Llub;->g:Ljava/lang/Long;

    .line 155
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_9
    iget-object v1, p0, Llub;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    .line 158
    const/16 v1, 0xb

    iget-object v2, p0, Llub;->h:Ljava/lang/Boolean;

    .line 159
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 161
    :cond_a
    iget-object v1, p0, Llub;->m:Llux;

    if-eqz v1, :cond_b

    .line 162
    const/16 v1, 0xc

    iget-object v2, p0, Llub;->m:Llux;

    .line 163
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_b
    iget-object v1, p0, Llub;->n:Llua;

    if-eqz v1, :cond_c

    .line 166
    const/16 v1, 0xd

    iget-object v2, p0, Llub;->n:Llua;

    .line 167
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_c
    iget-object v1, p0, Llub;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    iput v0, p0, Llub;->ai:I

    .line 171
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Llub;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 179
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 180
    sparse-switch v0, :sswitch_data_0

    .line 184
    iget-object v2, p0, Llub;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 185
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llub;->ah:Ljava/util/List;

    .line 188
    :cond_1
    iget-object v2, p0, Llub;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    :sswitch_0
    return-object p0

    .line 195
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llub;->b:Ljava/lang/String;

    goto :goto_0

    .line 199
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llub;->i:Ljava/lang/String;

    goto :goto_0

    .line 203
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 204
    iget-object v0, p0, Llub;->j:[Llud;

    if-nez v0, :cond_3

    move v0, v1

    .line 205
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llud;

    .line 206
    iget-object v3, p0, Llub;->j:[Llud;

    if-eqz v3, :cond_2

    .line 207
    iget-object v3, p0, Llub;->j:[Llud;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 209
    :cond_2
    iput-object v2, p0, Llub;->j:[Llud;

    .line 210
    :goto_2
    iget-object v2, p0, Llub;->j:[Llud;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 211
    iget-object v2, p0, Llub;->j:[Llud;

    new-instance v3, Llud;

    invoke-direct {v3}, Llud;-><init>()V

    aput-object v3, v2, v0

    .line 212
    iget-object v2, p0, Llub;->j:[Llud;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 213
    invoke-virtual {p1}, Loxn;->a()I

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 204
    :cond_3
    iget-object v0, p0, Llub;->j:[Llud;

    array-length v0, v0

    goto :goto_1

    .line 216
    :cond_4
    iget-object v2, p0, Llub;->j:[Llud;

    new-instance v3, Llud;

    invoke-direct {v3}, Llud;-><init>()V

    aput-object v3, v2, v0

    .line 217
    iget-object v2, p0, Llub;->j:[Llud;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 221
    :sswitch_4
    iget-object v0, p0, Llub;->c:Llvn;

    if-nez v0, :cond_5

    .line 222
    new-instance v0, Llvn;

    invoke-direct {v0}, Llvn;-><init>()V

    iput-object v0, p0, Llub;->c:Llvn;

    .line 224
    :cond_5
    iget-object v0, p0, Llub;->c:Llvn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 228
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 229
    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-eq v0, v6, :cond_6

    if-eq v0, v7, :cond_6

    const/4 v2, 0x5

    if-ne v0, v2, :cond_7

    .line 235
    :cond_6
    iput v0, p0, Llub;->d:I

    goto/16 :goto_0

    .line 237
    :cond_7
    iput v1, p0, Llub;->d:I

    goto/16 :goto_0

    .line 242
    :sswitch_6
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llub;->k:Ljava/lang/Long;

    goto/16 :goto_0

    .line 246
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 247
    if-eqz v0, :cond_8

    if-eq v0, v4, :cond_8

    if-eq v0, v5, :cond_8

    if-eq v0, v6, :cond_8

    if-ne v0, v7, :cond_9

    .line 252
    :cond_8
    iput v0, p0, Llub;->e:I

    goto/16 :goto_0

    .line 254
    :cond_9
    iput v1, p0, Llub;->e:I

    goto/16 :goto_0

    .line 259
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llub;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 263
    :sswitch_9
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llub;->f:Ljava/lang/Long;

    goto/16 :goto_0

    .line 267
    :sswitch_a
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Llub;->g:Ljava/lang/Long;

    goto/16 :goto_0

    .line 271
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Llub;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 275
    :sswitch_c
    iget-object v0, p0, Llub;->m:Llux;

    if-nez v0, :cond_a

    .line 276
    new-instance v0, Llux;

    invoke-direct {v0}, Llux;-><init>()V

    iput-object v0, p0, Llub;->m:Llux;

    .line 278
    :cond_a
    iget-object v0, p0, Llub;->m:Llux;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 282
    :sswitch_d
    iget-object v0, p0, Llub;->n:Llua;

    if-nez v0, :cond_b

    .line 283
    new-instance v0, Llua;

    invoke-direct {v0}, Llua;-><init>()V

    iput-object v0, p0, Llub;->n:Llua;

    .line 285
    :cond_b
    iget-object v0, p0, Llub;->n:Llua;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 180
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 63
    iget-object v0, p0, Llub;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 64
    const/4 v0, 0x1

    iget-object v1, p0, Llub;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 66
    :cond_0
    iget-object v0, p0, Llub;->i:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 67
    const/4 v0, 0x2

    iget-object v1, p0, Llub;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 69
    :cond_1
    iget-object v0, p0, Llub;->j:[Llud;

    if-eqz v0, :cond_3

    .line 70
    iget-object v1, p0, Llub;->j:[Llud;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 71
    if-eqz v3, :cond_2

    .line 72
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 70
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_3
    iget-object v0, p0, Llub;->c:Llvn;

    if-eqz v0, :cond_4

    .line 77
    const/4 v0, 0x4

    iget-object v1, p0, Llub;->c:Llvn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 79
    :cond_4
    iget v0, p0, Llub;->d:I

    if-eq v0, v5, :cond_5

    .line 80
    const/4 v0, 0x5

    iget v1, p0, Llub;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 82
    :cond_5
    iget-object v0, p0, Llub;->k:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 83
    const/4 v0, 0x6

    iget-object v1, p0, Llub;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 85
    :cond_6
    iget v0, p0, Llub;->e:I

    if-eq v0, v5, :cond_7

    .line 86
    const/4 v0, 0x7

    iget v1, p0, Llub;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 88
    :cond_7
    iget-object v0, p0, Llub;->l:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 89
    const/16 v0, 0x8

    iget-object v1, p0, Llub;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 91
    :cond_8
    iget-object v0, p0, Llub;->f:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 92
    const/16 v0, 0x9

    iget-object v1, p0, Llub;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 94
    :cond_9
    iget-object v0, p0, Llub;->g:Ljava/lang/Long;

    if-eqz v0, :cond_a

    .line 95
    const/16 v0, 0xa

    iget-object v1, p0, Llub;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 97
    :cond_a
    iget-object v0, p0, Llub;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 98
    const/16 v0, 0xb

    iget-object v1, p0, Llub;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 100
    :cond_b
    iget-object v0, p0, Llub;->m:Llux;

    if-eqz v0, :cond_c

    .line 101
    const/16 v0, 0xc

    iget-object v1, p0, Llub;->m:Llux;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 103
    :cond_c
    iget-object v0, p0, Llub;->n:Llua;

    if-eqz v0, :cond_d

    .line 104
    const/16 v0, 0xd

    iget-object v1, p0, Llub;->n:Llua;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 106
    :cond_d
    iget-object v0, p0, Llub;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 108
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Llub;->a(Loxn;)Llub;

    move-result-object v0

    return-object v0
.end method
