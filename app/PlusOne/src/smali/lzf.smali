.class public final Llzf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lncu;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27862
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27865
    iput-object v0, p0, Llzf;->apiHeader:Llyr;

    .line 27868
    iput-object v0, p0, Llzf;->a:Lncu;

    .line 27862
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27885
    const/4 v0, 0x0

    .line 27886
    iget-object v1, p0, Llzf;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 27887
    const/4 v0, 0x1

    iget-object v1, p0, Llzf;->apiHeader:Llyr;

    .line 27888
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27890
    :cond_0
    iget-object v1, p0, Llzf;->a:Lncu;

    if-eqz v1, :cond_1

    .line 27891
    const/4 v1, 0x2

    iget-object v2, p0, Llzf;->a:Lncu;

    .line 27892
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27894
    :cond_1
    iget-object v1, p0, Llzf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27895
    iput v0, p0, Llzf;->ai:I

    .line 27896
    return v0
.end method

.method public a(Loxn;)Llzf;
    .locals 2

    .prologue
    .line 27904
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27905
    sparse-switch v0, :sswitch_data_0

    .line 27909
    iget-object v1, p0, Llzf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27910
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzf;->ah:Ljava/util/List;

    .line 27913
    :cond_1
    iget-object v1, p0, Llzf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27915
    :sswitch_0
    return-object p0

    .line 27920
    :sswitch_1
    iget-object v0, p0, Llzf;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 27921
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzf;->apiHeader:Llyr;

    .line 27923
    :cond_2
    iget-object v0, p0, Llzf;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27927
    :sswitch_2
    iget-object v0, p0, Llzf;->a:Lncu;

    if-nez v0, :cond_3

    .line 27928
    new-instance v0, Lncu;

    invoke-direct {v0}, Lncu;-><init>()V

    iput-object v0, p0, Llzf;->a:Lncu;

    .line 27930
    :cond_3
    iget-object v0, p0, Llzf;->a:Lncu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27905
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27873
    iget-object v0, p0, Llzf;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 27874
    const/4 v0, 0x1

    iget-object v1, p0, Llzf;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27876
    :cond_0
    iget-object v0, p0, Llzf;->a:Lncu;

    if-eqz v0, :cond_1

    .line 27877
    const/4 v0, 0x2

    iget-object v1, p0, Llzf;->a:Lncu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27879
    :cond_1
    iget-object v0, p0, Llzf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27881
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 27858
    invoke-virtual {p0, p1}, Llzf;->a(Loxn;)Llzf;

    move-result-object v0

    return-object v0
.end method
