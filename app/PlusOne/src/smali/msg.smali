.class public final Lmsg;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lmsc;

.field public b:Lmsc;

.field public c:Lmsc;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    sget-object v0, Lmsc;->a:[Lmsc;

    iput-object v0, p0, Lmsg;->a:[Lmsc;

    .line 16
    iput-object v1, p0, Lmsg;->b:Lmsc;

    .line 19
    iput-object v1, p0, Lmsg;->c:Lmsc;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 48
    .line 49
    iget-object v1, p0, Lmsg;->a:[Lmsc;

    if-eqz v1, :cond_1

    .line 50
    iget-object v2, p0, Lmsg;->a:[Lmsc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 51
    if-eqz v4, :cond_0

    .line 52
    const/4 v5, 0x1

    .line 53
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 50
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    :cond_1
    iget-object v1, p0, Lmsg;->b:Lmsc;

    if-eqz v1, :cond_2

    .line 58
    const/4 v1, 0x2

    iget-object v2, p0, Lmsg;->b:Lmsc;

    .line 59
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    :cond_2
    iget-object v1, p0, Lmsg;->c:Lmsc;

    if-eqz v1, :cond_3

    .line 62
    const/4 v1, 0x3

    iget-object v2, p0, Lmsg;->c:Lmsc;

    .line 63
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_3
    iget-object v1, p0, Lmsg;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 66
    const/4 v1, 0x4

    iget-object v2, p0, Lmsg;->d:Ljava/lang/Boolean;

    .line 67
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 69
    :cond_4
    iget-object v1, p0, Lmsg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 70
    iput v0, p0, Lmsg;->ai:I

    .line 71
    return v0
.end method

.method public a(Loxn;)Lmsg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 80
    sparse-switch v0, :sswitch_data_0

    .line 84
    iget-object v2, p0, Lmsg;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 85
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmsg;->ah:Ljava/util/List;

    .line 88
    :cond_1
    iget-object v2, p0, Lmsg;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    :sswitch_0
    return-object p0

    .line 95
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 96
    iget-object v0, p0, Lmsg;->a:[Lmsc;

    if-nez v0, :cond_3

    move v0, v1

    .line 97
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmsc;

    .line 98
    iget-object v3, p0, Lmsg;->a:[Lmsc;

    if-eqz v3, :cond_2

    .line 99
    iget-object v3, p0, Lmsg;->a:[Lmsc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    :cond_2
    iput-object v2, p0, Lmsg;->a:[Lmsc;

    .line 102
    :goto_2
    iget-object v2, p0, Lmsg;->a:[Lmsc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 103
    iget-object v2, p0, Lmsg;->a:[Lmsc;

    new-instance v3, Lmsc;

    invoke-direct {v3}, Lmsc;-><init>()V

    aput-object v3, v2, v0

    .line 104
    iget-object v2, p0, Lmsg;->a:[Lmsc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 105
    invoke-virtual {p1}, Loxn;->a()I

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 96
    :cond_3
    iget-object v0, p0, Lmsg;->a:[Lmsc;

    array-length v0, v0

    goto :goto_1

    .line 108
    :cond_4
    iget-object v2, p0, Lmsg;->a:[Lmsc;

    new-instance v3, Lmsc;

    invoke-direct {v3}, Lmsc;-><init>()V

    aput-object v3, v2, v0

    .line 109
    iget-object v2, p0, Lmsg;->a:[Lmsc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 113
    :sswitch_2
    iget-object v0, p0, Lmsg;->b:Lmsc;

    if-nez v0, :cond_5

    .line 114
    new-instance v0, Lmsc;

    invoke-direct {v0}, Lmsc;-><init>()V

    iput-object v0, p0, Lmsg;->b:Lmsc;

    .line 116
    :cond_5
    iget-object v0, p0, Lmsg;->b:Lmsc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 120
    :sswitch_3
    iget-object v0, p0, Lmsg;->c:Lmsc;

    if-nez v0, :cond_6

    .line 121
    new-instance v0, Lmsc;

    invoke-direct {v0}, Lmsc;-><init>()V

    iput-object v0, p0, Lmsg;->c:Lmsc;

    .line 123
    :cond_6
    iget-object v0, p0, Lmsg;->c:Lmsc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 127
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmsg;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 80
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 26
    iget-object v0, p0, Lmsg;->a:[Lmsc;

    if-eqz v0, :cond_1

    .line 27
    iget-object v1, p0, Lmsg;->a:[Lmsc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 28
    if-eqz v3, :cond_0

    .line 29
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 27
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33
    :cond_1
    iget-object v0, p0, Lmsg;->b:Lmsc;

    if-eqz v0, :cond_2

    .line 34
    const/4 v0, 0x2

    iget-object v1, p0, Lmsg;->b:Lmsc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 36
    :cond_2
    iget-object v0, p0, Lmsg;->c:Lmsc;

    if-eqz v0, :cond_3

    .line 37
    const/4 v0, 0x3

    iget-object v1, p0, Lmsg;->c:Lmsc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 39
    :cond_3
    iget-object v0, p0, Lmsg;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 40
    const/4 v0, 0x4

    iget-object v1, p0, Lmsg;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 42
    :cond_4
    iget-object v0, p0, Lmsg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 44
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmsg;->a(Loxn;)Lmsg;

    move-result-object v0

    return-object v0
.end method
