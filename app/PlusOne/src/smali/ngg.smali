.class public final Lngg;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lngg;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lngh;

.field public d:Lngi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x0

    new-array v0, v0, [Lngg;

    sput-object v0, Lngg;->a:[Lngg;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 410
    invoke-direct {p0}, Loxq;-><init>()V

    .line 415
    iput-object v0, p0, Lngg;->c:Lngh;

    .line 418
    iput-object v0, p0, Lngg;->d:Lngi;

    .line 410
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 438
    const/4 v0, 0x0

    .line 439
    iget-object v1, p0, Lngg;->c:Lngh;

    if-eqz v1, :cond_0

    .line 440
    const/4 v0, 0x2

    iget-object v1, p0, Lngg;->c:Lngh;

    .line 441
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 443
    :cond_0
    iget-object v1, p0, Lngg;->d:Lngi;

    if-eqz v1, :cond_1

    .line 444
    const/4 v1, 0x3

    iget-object v2, p0, Lngg;->d:Lngi;

    .line 445
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    :cond_1
    iget-object v1, p0, Lngg;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 448
    const/4 v1, 0x4

    iget-object v2, p0, Lngg;->b:Ljava/lang/String;

    .line 449
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 451
    :cond_2
    iget-object v1, p0, Lngg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 452
    iput v0, p0, Lngg;->ai:I

    .line 453
    return v0
.end method

.method public a(Loxn;)Lngg;
    .locals 2

    .prologue
    .line 461
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 462
    sparse-switch v0, :sswitch_data_0

    .line 466
    iget-object v1, p0, Lngg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 467
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lngg;->ah:Ljava/util/List;

    .line 470
    :cond_1
    iget-object v1, p0, Lngg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    :sswitch_0
    return-object p0

    .line 477
    :sswitch_1
    iget-object v0, p0, Lngg;->c:Lngh;

    if-nez v0, :cond_2

    .line 478
    new-instance v0, Lngh;

    invoke-direct {v0}, Lngh;-><init>()V

    iput-object v0, p0, Lngg;->c:Lngh;

    .line 480
    :cond_2
    iget-object v0, p0, Lngg;->c:Lngh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 484
    :sswitch_2
    iget-object v0, p0, Lngg;->d:Lngi;

    if-nez v0, :cond_3

    .line 485
    new-instance v0, Lngi;

    invoke-direct {v0}, Lngi;-><init>()V

    iput-object v0, p0, Lngg;->d:Lngi;

    .line 487
    :cond_3
    iget-object v0, p0, Lngg;->d:Lngi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 491
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngg;->b:Ljava/lang/String;

    goto :goto_0

    .line 462
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Lngg;->c:Lngh;

    if-eqz v0, :cond_0

    .line 424
    const/4 v0, 0x2

    iget-object v1, p0, Lngg;->c:Lngh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 426
    :cond_0
    iget-object v0, p0, Lngg;->d:Lngi;

    if-eqz v0, :cond_1

    .line 427
    const/4 v0, 0x3

    iget-object v1, p0, Lngg;->d:Lngi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 429
    :cond_1
    iget-object v0, p0, Lngg;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 430
    const/4 v0, 0x4

    iget-object v1, p0, Lngg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 432
    :cond_2
    iget-object v0, p0, Lngg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 434
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 406
    invoke-virtual {p0, p1}, Lngg;->a(Loxn;)Lngg;

    move-result-object v0

    return-object v0
.end method
