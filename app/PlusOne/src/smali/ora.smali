.class public final Lora;
.super Loxq;
.source "PG"


# instance fields
.field private a:Losf;

.field private b:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 835
    invoke-direct {p0}, Loxq;-><init>()V

    .line 847
    const/4 v0, 0x0

    iput-object v0, p0, Lora;->a:Losf;

    .line 850
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lora;->b:[I

    .line 835
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 869
    .line 870
    iget-object v0, p0, Lora;->a:Losf;

    if-eqz v0, :cond_2

    .line 871
    const/4 v0, 0x1

    iget-object v2, p0, Lora;->a:Losf;

    .line 872
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 874
    :goto_0
    iget-object v2, p0, Lora;->b:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lora;->b:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 876
    iget-object v3, p0, Lora;->b:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 878
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 876
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 880
    :cond_0
    add-int/2addr v0, v2

    .line 881
    iget-object v1, p0, Lora;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 883
    :cond_1
    iget-object v1, p0, Lora;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 884
    iput v0, p0, Lora;->ai:I

    .line 885
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lora;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 893
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 894
    sparse-switch v0, :sswitch_data_0

    .line 898
    iget-object v1, p0, Lora;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 899
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lora;->ah:Ljava/util/List;

    .line 902
    :cond_1
    iget-object v1, p0, Lora;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 904
    :sswitch_0
    return-object p0

    .line 909
    :sswitch_1
    iget-object v0, p0, Lora;->a:Losf;

    if-nez v0, :cond_2

    .line 910
    new-instance v0, Losf;

    invoke-direct {v0}, Losf;-><init>()V

    iput-object v0, p0, Lora;->a:Losf;

    .line 912
    :cond_2
    iget-object v0, p0, Lora;->a:Losf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 916
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 917
    iget-object v0, p0, Lora;->b:[I

    array-length v0, v0

    .line 918
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 919
    iget-object v2, p0, Lora;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 920
    iput-object v1, p0, Lora;->b:[I

    .line 921
    :goto_1
    iget-object v1, p0, Lora;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 922
    iget-object v1, p0, Lora;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 923
    invoke-virtual {p1}, Loxn;->a()I

    .line 921
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 926
    :cond_3
    iget-object v1, p0, Lora;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 894
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 855
    iget-object v0, p0, Lora;->a:Losf;

    if-eqz v0, :cond_0

    .line 856
    const/4 v0, 0x1

    iget-object v1, p0, Lora;->a:Losf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 858
    :cond_0
    iget-object v0, p0, Lora;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lora;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 859
    iget-object v1, p0, Lora;->b:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 860
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 859
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 863
    :cond_1
    iget-object v0, p0, Lora;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 865
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 831
    invoke-virtual {p0, p1}, Lora;->a(Loxn;)Lora;

    move-result-object v0

    return-object v0
.end method
