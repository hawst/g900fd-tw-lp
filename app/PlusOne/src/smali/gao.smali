.class public final Lgao;
.super Lgbz;
.source "PG"


# instance fields
.field private A:Lljg;

.field private B:Lljg;

.field private y:Lpal;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgao;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgao;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method


# virtual methods
.method protected a(III)I
    .locals 10

    .prologue
    .line 80
    iget-object v0, p0, Lgao;->y:Lpal;

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return p2

    .line 84
    :cond_0
    iget-object v0, p0, Lgao;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sget-object v1, Lgao;->z:Llct;

    iget v1, v1, Llct;->l:I

    add-int/2addr v0, v1

    add-int v5, p2, v0

    .line 87
    iget-object v0, p0, Lgao;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lgao;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    sget-object v1, Lgao;->z:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sub-int v3, p3, v0

    .line 90
    iget-object v0, p0, Lgao;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, p1

    sget-object v1, Lgao;->z:Llct;

    iget v1, v1, Llct;->m:I

    add-int v9, v0, v1

    .line 93
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 94
    iget-object v0, p0, Lgao;->y:Lpal;

    iget-object v0, v0, Lpal;->c:[Loya;

    if-eqz v0, :cond_2

    .line 95
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    iget-object v0, p0, Lgao;->y:Lpal;

    iget-object v0, v0, Lpal;->c:[Loya;

    array-length v0, v0

    if-ge v2, v0, :cond_2

    .line 96
    iget-object v0, p0, Lgao;->y:Lpal;

    iget-object v0, v0, Lpal;->c:[Loya;

    aget-object v0, v0, v2

    sget-object v4, Lpcl;->a:Loxr;

    invoke-virtual {v0, v4}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpcl;

    .line 97
    if-eqz v0, :cond_1

    .line 98
    iget-object v6, v0, Lpcl;->b:Ljava/lang/String;

    .line 101
    iget-object v4, v0, Lpcl;->c:Ljava/lang/String;

    .line 102
    iget-object v7, v0, Lpcl;->d:Lpcj;

    if-eqz v7, :cond_5

    iget-object v7, v0, Lpcl;->d:Lpcj;

    iget-object v7, v7, Lpcj;->a:Ljava/lang/String;

    .line 103
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 104
    iget-object v0, v0, Lpcl;->d:Lpcj;

    iget-object v0, v0, Lpcj;->a:Ljava/lang/String;

    .line 106
    :goto_2
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 107
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 108
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    .line 112
    invoke-virtual {p0}, Lgao;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f090042

    invoke-direct {v4, v7, v8}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 108
    invoke-static {v1, v6, v4}, Llhv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 113
    const-string v4, "\u00a0"

    invoke-virtual {v1, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 114
    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    .line 118
    invoke-virtual {p0}, Lgao;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f090043

    invoke-direct {v4, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    .line 114
    invoke-static {v1, v0, v4}, Llhv;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lgao;->y:Lpal;

    iget-object v0, v0, Lpal;->c:[Loya;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-eq v2, v0, :cond_1

    .line 121
    const-string v0, "  "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 95
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 126
    :cond_2
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 127
    sget-object v0, Lgao;->z:Llct;

    iget v0, v0, Llct;->aG:I

    add-int v8, v5, v0

    .line 129
    new-instance v0, Lljg;

    .line 131
    invoke-virtual {p0}, Lgao;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v4, 0x19

    invoke-static {v2, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lgao;->B:Lljg;

    .line 134
    iget-object v0, p0, Lgao;->B:Lljg;

    invoke-virtual {v0, v9, v8}, Lljg;->a(II)V

    .line 135
    iget-object v0, p0, Lgao;->B:Lljg;

    invoke-virtual {v0}, Lljg;->getHeight()I

    move-result v0

    sget-object v1, Lgao;->z:Llct;

    iget v1, v1, Llct;->aF:I

    add-int/2addr v0, v1

    add-int/2addr v0, v8

    move v8, v0

    .line 140
    :goto_3
    iget-object v0, p0, Lgao;->y:Lpal;

    iget-object v0, v0, Lpal;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 141
    new-instance v0, Lljg;

    iget-object v1, p0, Lgao;->y:Lpal;

    iget-object v1, v1, Lpal;->e:Ljava/lang/String;

    .line 143
    invoke-virtual {p0}, Lgao;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v4, 0x19

    invoke-static {v2, v4}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lljg;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lgao;->A:Lljg;

    .line 147
    iget-object v0, p0, Lgao;->A:Lljg;

    invoke-virtual {v0, v9, v8}, Lljg;->a(II)V

    .line 149
    iget-object v0, p0, Lgao;->A:Lljg;

    invoke-virtual {v0}, Lljg;->getHeight()I

    move-result v0

    add-int/2addr v8, v0

    :cond_3
    move p2, v8

    .line 152
    goto/16 :goto_0

    :cond_4
    move v8, v5

    goto :goto_3

    :cond_5
    move-object v0, v4

    goto/16 :goto_2
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 4

    .prologue
    .line 160
    iget-object v0, p0, Lgao;->B:Lljg;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lgao;->B:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    .line 163
    iget-object v1, p0, Lgao;->B:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    .line 164
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 165
    iget-object v2, p0, Lgao;->B:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    .line 166
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 167
    iget-object v0, p0, Lgao;->B:Lljg;

    invoke-virtual {v0}, Lljg;->d()I

    move-result p2

    .line 170
    :cond_0
    iget-object v0, p0, Lgao;->A:Lljg;

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lgao;->A:Lljg;

    invoke-virtual {v0}, Lljg;->b()I

    move-result v0

    .line 173
    iget-object v1, p0, Lgao;->A:Lljg;

    invoke-virtual {v1}, Lljg;->c()I

    move-result v1

    .line 174
    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 175
    iget-object v2, p0, Lgao;->A:Lljg;

    invoke-virtual {v2, p1}, Lljg;->draw(Landroid/graphics/Canvas;)V

    .line 176
    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 177
    iget-object v0, p0, Lgao;->A:Lljg;

    invoke-virtual {v0}, Lljg;->d()I

    move-result p2

    .line 180
    :cond_1
    return p2
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 196
    invoke-super {p0}, Lgbz;->a()V

    .line 197
    iput-object v0, p0, Lgao;->y:Lpal;

    .line 198
    iput-object v0, p0, Lgao;->A:Lljg;

    .line 199
    iput-object v0, p0, Lgao;->B:Lljg;

    .line 200
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 57
    const/16 v0, 0x17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 59
    :try_start_0
    new-instance v1, Lpal;

    invoke-direct {v1}, Lpal;-><init>()V

    invoke-static {v1, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lpal;

    iput-object v0, p0, Lgao;->y:Lpal;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    const-string v1, "PlaceReviewCardGroup"

    const-string v2, "Failed to parse the PlaceReview"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    return v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lgao;->y:Lpal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgao;->y:Lpal;

    iget-object v0, v0, Lpal;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lgao;->y:Lpal;

    iget-object v0, v0, Lpal;->b:Ljava/lang/String;

    .line 70
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lgao;->i:Lfdp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgao;->y:Lpal;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgao;->y:Lpal;

    iget-object v0, v0, Lpal;->d:Loya;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lgao;->y:Lpal;

    iget-object v0, v0, Lpal;->d:Loya;

    sget-object v1, Lpao;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpao;

    .line 188
    if-eqz v0, :cond_0

    iget-object v1, v0, Lpao;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    iget-object v1, p0, Lgao;->i:Lfdp;

    iget-object v0, v0, Lpao;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lfdp;->a(Ljava/lang/String;)V

    .line 192
    :cond_0
    return-void
.end method
