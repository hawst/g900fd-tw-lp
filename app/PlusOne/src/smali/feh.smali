.class final Lfeh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Ligy;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Ligv;

.field private c:Livx;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 157
    iget-object v0, p0, Lfeh;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 158
    iget-object v1, p0, Lfeh;->c:Livx;

    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    const-class v3, Lixj;

    .line 159
    invoke-virtual {v2, v3}, Liwg;->a(Ljava/lang/Class;)Liwg;

    move-result-object v2

    const-string v3, "account_id"

    const/4 v4, -0x1

    .line 160
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Liwg;->a(I)Liwg;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Liwg;->d()Liwg;

    move-result-object v0

    const-class v2, Liwl;

    new-instance v3, Liwm;

    invoke-direct {v3}, Liwm;-><init>()V

    iget-object v4, p0, Lfeh;->a:Landroid/app/Activity;

    const v5, 0x7f0a0b58

    .line 163
    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Liwm;->a(Ljava/lang/String;)Liwm;

    move-result-object v3

    .line 164
    invoke-virtual {v3}, Liwm;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 162
    invoke-virtual {v0, v2, v3}, Liwg;->a(Ljava/lang/Class;Landroid/os/Bundle;)Liwg;

    move-result-object v0

    .line 158
    invoke-virtual {v1, v0}, Livx;->a(Liwg;)V

    .line 167
    return-void
.end method

.method public a(Landroid/app/Activity;Llqr;Ligv;Livx;)V
    .locals 1

    .prologue
    .line 139
    iput-object p1, p0, Lfeh;->a:Landroid/app/Activity;

    .line 140
    iput-object p3, p0, Lfeh;->b:Ligv;

    .line 141
    invoke-virtual {p4, p0}, Livx;->b(Lheg;)Livx;

    move-result-object v0

    iput-object v0, p0, Lfeh;->c:Livx;

    .line 142
    return-void
.end method

.method public a(ZIIII)V
    .locals 5

    .prologue
    .line 148
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    .line 149
    iget-object v0, p0, Lfeh;->b:Ligv;

    iget-object v1, p0, Lfeh;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v3, p0, Lfeh;->a:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account_id"

    invoke-virtual {v2, v3, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "com.google.android.apps.plus.CONTENT_URL"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "http://plus.google.com/share"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "url"

    invoke-virtual {v3, v4, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v3, "text/plain"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "from_url_gateway"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-interface {v0, v2}, Ligv;->a(Landroid/content/Intent;)V

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lfeh;->b:Ligv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ligv;->a(I)V

    goto :goto_0
.end method
