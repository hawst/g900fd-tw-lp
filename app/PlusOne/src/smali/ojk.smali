.class public final Lojk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lojm;

.field public b:Lojn;

.field public c:Lojl;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 676
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1003
    iput-object v0, p0, Lojk;->a:Lojm;

    .line 1006
    iput-object v0, p0, Lojk;->b:Lojn;

    .line 1009
    iput-object v0, p0, Lojk;->c:Lojl;

    .line 676
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1034
    const/4 v0, 0x0

    .line 1035
    iget-object v1, p0, Lojk;->a:Lojm;

    if-eqz v1, :cond_0

    .line 1036
    const/4 v0, 0x1

    iget-object v1, p0, Lojk;->a:Lojm;

    .line 1037
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1039
    :cond_0
    iget-object v1, p0, Lojk;->b:Lojn;

    if-eqz v1, :cond_1

    .line 1040
    const/4 v1, 0x2

    iget-object v2, p0, Lojk;->b:Lojn;

    .line 1041
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1043
    :cond_1
    iget-object v1, p0, Lojk;->c:Lojl;

    if-eqz v1, :cond_2

    .line 1044
    const/4 v1, 0x3

    iget-object v2, p0, Lojk;->c:Lojl;

    .line 1045
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1047
    :cond_2
    iget-object v1, p0, Lojk;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1048
    const/4 v1, 0x4

    iget-object v2, p0, Lojk;->d:Ljava/lang/String;

    .line 1049
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1051
    :cond_3
    iget-object v1, p0, Lojk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1052
    iput v0, p0, Lojk;->ai:I

    .line 1053
    return v0
.end method

.method public a(Loxn;)Lojk;
    .locals 2

    .prologue
    .line 1061
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1062
    sparse-switch v0, :sswitch_data_0

    .line 1066
    iget-object v1, p0, Lojk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1067
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lojk;->ah:Ljava/util/List;

    .line 1070
    :cond_1
    iget-object v1, p0, Lojk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1072
    :sswitch_0
    return-object p0

    .line 1077
    :sswitch_1
    iget-object v0, p0, Lojk;->a:Lojm;

    if-nez v0, :cond_2

    .line 1078
    new-instance v0, Lojm;

    invoke-direct {v0}, Lojm;-><init>()V

    iput-object v0, p0, Lojk;->a:Lojm;

    .line 1080
    :cond_2
    iget-object v0, p0, Lojk;->a:Lojm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1084
    :sswitch_2
    iget-object v0, p0, Lojk;->b:Lojn;

    if-nez v0, :cond_3

    .line 1085
    new-instance v0, Lojn;

    invoke-direct {v0}, Lojn;-><init>()V

    iput-object v0, p0, Lojk;->b:Lojn;

    .line 1087
    :cond_3
    iget-object v0, p0, Lojk;->b:Lojn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1091
    :sswitch_3
    iget-object v0, p0, Lojk;->c:Lojl;

    if-nez v0, :cond_4

    .line 1092
    new-instance v0, Lojl;

    invoke-direct {v0}, Lojl;-><init>()V

    iput-object v0, p0, Lojk;->c:Lojl;

    .line 1094
    :cond_4
    iget-object v0, p0, Lojk;->c:Lojl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1098
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojk;->d:Ljava/lang/String;

    goto :goto_0

    .line 1062
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1016
    iget-object v0, p0, Lojk;->a:Lojm;

    if-eqz v0, :cond_0

    .line 1017
    const/4 v0, 0x1

    iget-object v1, p0, Lojk;->a:Lojm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1019
    :cond_0
    iget-object v0, p0, Lojk;->b:Lojn;

    if-eqz v0, :cond_1

    .line 1020
    const/4 v0, 0x2

    iget-object v1, p0, Lojk;->b:Lojn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1022
    :cond_1
    iget-object v0, p0, Lojk;->c:Lojl;

    if-eqz v0, :cond_2

    .line 1023
    const/4 v0, 0x3

    iget-object v1, p0, Lojk;->c:Lojl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1025
    :cond_2
    iget-object v0, p0, Lojk;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1026
    const/4 v0, 0x4

    iget-object v1, p0, Lojk;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1028
    :cond_3
    iget-object v0, p0, Lojk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1030
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 672
    invoke-virtual {p0, p1}, Lojk;->a(Loxn;)Lojk;

    move-result-object v0

    return-object v0
.end method
