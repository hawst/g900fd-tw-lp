.class public final Ljdk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static a:Ljdm;

.field private static final b:J

.field private static final c:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljdm;",
            ">;>;"
        }
    .end annotation
.end field

.field private static d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 40
    const/4 v0, 0x0

    sput-object v0, Ljdk;->a:Ljdm;

    .line 44
    const-string v0, "picasasync.metrics.time"

    const-wide/16 v2, 0x64

    invoke-static {v0, v2, v3}, Ljdp;->a(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Ljdk;->b:J

    .line 47
    new-instance v0, Ljdl;

    invoke-direct {v0}, Ljdl;-><init>()V

    sput-object v0, Ljdk;->c:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 151
    sget-object v0, Ljdk;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 152
    invoke-static {p0}, Ljdm;->a(Ljava/lang/String;)Ljdm;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public static a(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 161
    invoke-static {v0, p0, v0}, Ljdk;->a(Landroid/content/Context;ILjava/lang/String;)V

    .line 162
    return-void
.end method

.method public static a(J)V
    .locals 4

    .prologue
    .line 287
    sget-object v0, Ljdk;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 288
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 289
    if-lez v1, :cond_0

    .line 290
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdm;

    .line 291
    iget-wide v2, v0, Ljdm;->f:J

    add-long/2addr v2, p0

    iput-wide v2, v0, Ljdm;->f:J

    .line 293
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)V
    .locals 12

    .prologue
    .line 173
    sget-object v0, Ljdk;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 174
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt p1, v1, :cond_0

    if-gtz p1, :cond_1

    .line 175
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "size: %s, id: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 176
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 179
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_5

    .line 180
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljdm;

    .line 181
    const-string v2, "MetricsUtils"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 182
    const-string v2, "WARNING: unclosed metrics: "

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 184
    :cond_2
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 186
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljdm;

    invoke-virtual {v2, v1}, Ljdm;->b(Ljdm;)V

    .line 188
    :cond_3
    invoke-virtual {v1}, Ljdm;->a()V

    goto :goto_0

    .line 182
    :cond_4
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 191
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljdm;

    .line 192
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v1, Ljdm;->e:J

    .line 194
    sget-wide v2, Ljdk;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_6

    iget-wide v2, v1, Ljdm;->e:J

    iget-wide v4, v1, Ljdm;->d:J

    sub-long/2addr v2, v4

    sget-wide v4, Ljdk;->b:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_6

    .line 196
    const-string v2, "MetricsUtils"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 197
    invoke-virtual {v1, p2}, Ljdm;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 200
    :cond_6
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 202
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdm;

    invoke-virtual {v0, v1}, Ljdm;->b(Ljdm;)V

    .line 205
    :cond_7
    if-eqz p0, :cond_8

    if-eqz p2, :cond_8

    iget v0, v1, Ljdm;->g:I

    if-lez v0, :cond_8

    .line 206
    iget-wide v2, v1, Ljdm;->e:J

    iget-wide v4, v1, Ljdm;->d:J

    sub-long/2addr v2, v4

    iget-wide v4, v1, Ljdm;->f:J

    iget v0, v1, Ljdm;->g:I

    iget-wide v6, v1, Ljdm;->c:J

    iget-wide v8, v1, Ljdm;->b:J

    sget-object v10, Ljdk;->d:Ljava/lang/Class;

    if-eqz v10, :cond_8

    new-instance v10, Landroid/content/Intent;

    sget-object v11, Ljdk;->d:Ljava/lang/Class;

    invoke-direct {v10, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v11, "com.google.android.apps.plus.iu.op_report"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "op_name"

    invoke-virtual {v10, v11, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v11, "total_time"

    invoke-virtual {v10, v11, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "net_duration"

    invoke-virtual {v10, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "transaction_count"

    invoke-virtual {v10, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "sent_bytes"

    invoke-virtual {v10, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v0, "received_bytes"

    invoke-virtual {v10, v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0, v10}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 213
    :cond_8
    invoke-virtual {v1}, Ljdm;->a()V

    .line 214
    return-void
.end method

.method public static a(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 219
    sput-object p0, Ljdk;->d:Ljava/lang/Class;

    .line 220
    return-void
.end method

.method public static b(I)V
    .locals 2

    .prologue
    .line 260
    sget-object v0, Ljdk;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 261
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 262
    if-lez v1, :cond_0

    .line 263
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdm;

    .line 264
    iget v1, v0, Ljdm;->a:I

    add-int/2addr v1, p0

    iput v1, v0, Ljdm;->a:I

    .line 266
    :cond_0
    return-void
.end method

.method public static b(J)V
    .locals 4

    .prologue
    .line 296
    sget-object v0, Ljdk;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 297
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 298
    if-lez v1, :cond_0

    .line 299
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdm;

    .line 300
    iget v1, v0, Ljdm;->g:I

    int-to-long v2, v1

    add-long/2addr v2, p0

    long-to-int v1, v2

    iput v1, v0, Ljdm;->g:I

    .line 302
    :cond_0
    return-void
.end method
