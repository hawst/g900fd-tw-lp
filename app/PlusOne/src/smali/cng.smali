.class public Lcng;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lhkd;

.field private c:Landroid/content/Context;

.field private d:Lctz;

.field private e:Lhee;

.field private f:Lhms;

.field private g:Lhke;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcnh;

    invoke-direct {v0, p0}, Lcnh;-><init>(Lcng;)V

    iput-object v0, p0, Lcng;->b:Lhkd;

    .line 67
    iput-object p1, p0, Lcng;->a:Landroid/app/Activity;

    .line 69
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 70
    return-void
.end method

.method static synthetic a(Lcng;)Lctz;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcng;->d:Lctz;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcng;->f:Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lcng;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->dR:Lhmv;

    .line 85
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 83
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 87
    new-instance v0, Ldup;

    iget-object v1, p0, Lcng;->d:Lctz;

    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ldup;-><init>(Ljcn;Z)V

    .line 90
    iget-object v1, p0, Lcng;->e:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 91
    iget-object v2, p0, Lcng;->e:Lhee;

    invoke-interface {v2}, Lhee;->g()Lhej;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 92
    iget-object v3, p0, Lcng;->a:Landroid/app/Activity;

    invoke-static {v3, v1, v0, v2}, Leyq;->a(Landroid/content/Context;ILduo;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcng;->g:Lhke;

    const v2, 0x7f1000c0

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 95
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 74
    iput-object p1, p0, Lcng;->c:Landroid/content/Context;

    .line 75
    const-class v0, Lctz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lcng;->d:Lctz;

    .line 76
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lcng;->e:Lhee;

    .line 77
    const-class v0, Lhms;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    iput-object v0, p0, Lcng;->f:Lhms;

    .line 78
    const-class v0, Lhke;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhke;

    iput-object v0, p0, Lcng;->g:Lhke;

    .line 79
    iget-object v0, p0, Lcng;->g:Lhke;

    const v1, 0x7f1000c0

    iget-object v2, p0, Lcng;->b:Lhkd;

    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    .line 80
    return-void
.end method
