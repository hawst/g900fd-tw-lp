.class public final Lgbp;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "PG"


# instance fields
.field private a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

.field private synthetic b:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;Landroid/view/inputmethod/InputConnection;Z)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lgbp;->b:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    .line 139
    invoke-direct {p0, p2, p3}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    .line 140
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lgbp;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    .line 144
    return-void
.end method

.method public deleteSurroundingText(II)Z
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lgbp;->b:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->getSelectionStart()I

    move-result v0

    .line 152
    iget-object v1, p0, Lgbp;->b:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->getSelectionEnd()I

    move-result v1

    .line 153
    if-lez p1, :cond_0

    if-gtz p2, :cond_0

    if-gtz v0, :cond_0

    if-gtz v1, :cond_0

    .line 156
    iget-object v0, p0, Lgbp;->b:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->a(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)Lgbq;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbp;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lgbp;->b:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->a(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)Lgbq;

    move-result-object v0

    iget-object v1, p0, Lgbp;->a:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-interface {v0, v1}, Lgbq;->a(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)V

    .line 158
    const/4 v0, 0x1

    .line 161
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/InputConnectionWrapper;->deleteSurroundingText(II)Z

    move-result v0

    goto :goto_0
.end method
