.class public final Lojp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1184
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1225
    const/4 v0, 0x0

    .line 1226
    iget-object v1, p0, Lojp;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1227
    const/4 v0, 0x1

    iget-object v1, p0, Lojp;->a:Ljava/lang/String;

    .line 1228
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1230
    :cond_0
    iget-object v1, p0, Lojp;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1231
    const/4 v1, 0x2

    iget-object v2, p0, Lojp;->b:Ljava/lang/String;

    .line 1232
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1234
    :cond_1
    iget-object v1, p0, Lojp;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1235
    const/4 v1, 0x3

    iget-object v2, p0, Lojp;->d:Ljava/lang/String;

    .line 1236
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1238
    :cond_2
    iget-object v1, p0, Lojp;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 1239
    const/4 v1, 0x4

    iget-object v2, p0, Lojp;->e:Ljava/lang/Boolean;

    .line 1240
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1242
    :cond_3
    iget-object v1, p0, Lojp;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1243
    const/4 v1, 0x5

    iget-object v2, p0, Lojp;->c:Ljava/lang/Boolean;

    .line 1244
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1246
    :cond_4
    iget-object v1, p0, Lojp;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 1247
    const/4 v1, 0x6

    iget-object v2, p0, Lojp;->f:Ljava/lang/String;

    .line 1248
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1250
    :cond_5
    iget-object v1, p0, Lojp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1251
    iput v0, p0, Lojp;->ai:I

    .line 1252
    return v0
.end method

.method public a(Loxn;)Lojp;
    .locals 2

    .prologue
    .line 1260
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1261
    sparse-switch v0, :sswitch_data_0

    .line 1265
    iget-object v1, p0, Lojp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1266
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lojp;->ah:Ljava/util/List;

    .line 1269
    :cond_1
    iget-object v1, p0, Lojp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1271
    :sswitch_0
    return-object p0

    .line 1276
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojp;->a:Ljava/lang/String;

    goto :goto_0

    .line 1280
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojp;->b:Ljava/lang/String;

    goto :goto_0

    .line 1284
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojp;->d:Ljava/lang/String;

    goto :goto_0

    .line 1288
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lojp;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 1292
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lojp;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1296
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lojp;->f:Ljava/lang/String;

    goto :goto_0

    .line 1261
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1201
    iget-object v0, p0, Lojp;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1202
    const/4 v0, 0x1

    iget-object v1, p0, Lojp;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1204
    :cond_0
    iget-object v0, p0, Lojp;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1205
    const/4 v0, 0x2

    iget-object v1, p0, Lojp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1207
    :cond_1
    iget-object v0, p0, Lojp;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1208
    const/4 v0, 0x3

    iget-object v1, p0, Lojp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1210
    :cond_2
    iget-object v0, p0, Lojp;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1211
    const/4 v0, 0x4

    iget-object v1, p0, Lojp;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1213
    :cond_3
    iget-object v0, p0, Lojp;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1214
    const/4 v0, 0x5

    iget-object v1, p0, Lojp;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1216
    :cond_4
    iget-object v0, p0, Lojp;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1217
    const/4 v0, 0x6

    iget-object v1, p0, Lojp;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1219
    :cond_5
    iget-object v0, p0, Lojp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1221
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1180
    invoke-virtual {p0, p1}, Lojp;->a(Loxn;)Lojp;

    move-result-object v0

    return-object v0
.end method
