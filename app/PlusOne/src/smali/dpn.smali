.class public final Ldpn;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:J


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;J)V
    .locals 2

    .prologue
    .line 28
    const-string v0, "NotificationsReportAbuseTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 30
    iput p2, p0, Ldpn;->a:I

    .line 31
    iput-object p3, p0, Ldpn;->b:Ljava/lang/String;

    .line 32
    iput-wide p4, p0, Ldpn;->c:J

    .line 33
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    .line 37
    iget v2, p0, Ldpn;->a:I

    .line 38
    new-instance v0, Ldlv;

    .line 39
    invoke-virtual {p0}, Ldpn;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Ldpn;->b:Ljava/lang/String;

    iget-wide v4, p0, Ldpn;->c:J

    invoke-direct/range {v0 .. v5}, Ldlv;-><init>(Landroid/content/Context;ILjava/lang/String;J)V

    .line 40
    invoke-virtual {v0}, Ldlv;->l()V

    .line 42
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    .line 43
    invoke-virtual {v0}, Ldlv;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldpn;->f()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a055e

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v1

    :cond_0
    invoke-virtual {p0}, Ldpn;->f()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0a055d

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0}, Ldpn;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a055c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
