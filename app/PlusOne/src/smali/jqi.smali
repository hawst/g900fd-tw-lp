.class public final Ljqi;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lipj;

.field private final c:Liok;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 39
    const-string v0, "AddCircleTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 40
    const-class v0, Liof;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liof;

    .line 41
    const-class v1, Liok;

    invoke-static {p1, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liok;

    iput-object v1, p0, Ljqi;->c:Liok;

    .line 42
    invoke-interface {v0}, Liof;->a()Lipj;

    move-result-object v0

    iput-object v0, p0, Ljqi;->b:Lipj;

    .line 43
    iput-object p1, p0, Ljqi;->a:Landroid/content/Context;

    .line 44
    iput p2, p0, Ljqi;->d:I

    .line 45
    iput-object p3, p0, Ljqi;->e:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Ljqi;->f:Ljava/lang/String;

    .line 47
    iput-boolean p5, p0, Ljqi;->h:Z

    .line 48
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 53
    iget-object v0, p0, Ljqi;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Ljqi;->d:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 54
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 58
    iget-object v0, p0, Ljqi;->b:Lipj;

    const-wide/16 v4, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v4, v5, v1}, Lipj;->a(JLjava/util/concurrent/TimeUnit;)Liiw;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Liiw;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 62
    iget-object v0, p0, Ljqi;->c:Liok;

    iget-object v1, p0, Ljqi;->b:Lipj;

    iget-object v4, p0, Ljqi;->e:Ljava/lang/String;

    iget-object v5, p0, Ljqi;->f:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Liok;->a(Lipj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lijq;

    move-result-object v0

    .line 63
    invoke-interface {v0}, Lijq;->a()Lijs;

    move-result-object v0

    check-cast v0, Linu;

    .line 64
    invoke-interface {v0}, Linu;->a()Lika;

    move-result-object v1

    invoke-interface {v1}, Lika;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 65
    invoke-interface {v0}, Linu;->b()Ljava/lang/String;

    move-result-object v4

    .line 66
    iget-object v0, p0, Ljqi;->c:Liok;

    iget-object v1, p0, Ljqi;->b:Lipj;

    iget-object v5, p0, Ljqi;->e:Ljava/lang/String;

    iget-boolean v6, p0, Ljqi;->h:Z

    .line 67
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p0, Ljqi;->f:Ljava/lang/String;

    .line 66
    invoke-interface/range {v0 .. v7}, Liok;->a(Lipj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Lijq;

    move-result-object v0

    .line 67
    invoke-interface {v0}, Lijq;->a()Lijs;

    move-result-object v1

    .line 68
    invoke-interface {v1}, Lijs;->a()Lika;

    move-result-object v0

    invoke-interface {v0}, Lika;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xc8

    .line 70
    :goto_0
    iget-object v3, p0, Ljqi;->a:Landroid/content/Context;

    const/16 v4, 0xb

    invoke-static {v3, v2, v4, v1}, Ljpe;->a(Landroid/content/Context;Ljava/lang/String;ILijs;)V

    .line 80
    :goto_1
    iget-object v1, p0, Ljqi;->b:Lipj;

    invoke-interface {v1}, Lipj;->d()V

    .line 82
    invoke-static {}, Ljpe;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    const-string v1, "accountId: %s. enableForSharing: %s. statusCode: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Ljqi;->d:I

    .line 85
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Ljqi;->h:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 84
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 83
    invoke-static {}, Ljpe;->b()V

    .line 88
    :cond_0
    new-instance v1, Lhoz;

    invoke-direct {v1, v0, v8, v8}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    return-object v1

    .line 69
    :cond_1
    invoke-interface {v1}, Lijs;->a()Lika;

    move-result-object v0

    invoke-interface {v0}, Lika;->c()I

    move-result v0

    goto :goto_0

    .line 73
    :cond_2
    invoke-interface {v0}, Linu;->a()Lika;

    move-result-object v1

    invoke-interface {v1}, Lika;->c()I

    move-result v1

    .line 74
    iget-object v3, p0, Ljqi;->a:Landroid/content/Context;

    const/16 v4, 0x9

    invoke-static {v3, v2, v4, v0}, Ljpe;->a(Landroid/content/Context;Ljava/lang/String;ILijs;)V

    move v0, v1

    .line 77
    goto :goto_1

    .line 78
    :cond_3
    invoke-interface {v0}, Liiw;->b()I

    move-result v0

    goto :goto_1
.end method
