.class public final Lbs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/content/Context;

.field b:Ljava/lang/CharSequence;

.field c:Ljava/lang/CharSequence;

.field d:Landroid/app/PendingIntent;

.field e:Landroid/graphics/Bitmap;

.field f:Ljava/lang/CharSequence;

.field g:I

.field h:I

.field i:Z

.field j:Lce;

.field k:Ljava/lang/CharSequence;

.field l:I

.field m:I

.field n:Z

.field o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbn;",
            ">;"
        }
    .end annotation
.end field

.field p:Ljava/lang/String;

.field q:Landroid/os/Bundle;

.field r:I

.field s:I

.field t:Landroid/app/Notification;

.field u:Landroid/app/Notification;

.field public v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 874
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbs;->i:Z

    .line 884
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbs;->o:Ljava/util/ArrayList;

    .line 885
    iput v4, p0, Lbs;->r:I

    .line 889
    iput v4, p0, Lbs;->s:I

    .line 892
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lbs;->u:Landroid/app/Notification;

    .line 907
    iput-object p1, p0, Lbs;->a:Landroid/content/Context;

    .line 910
    iget-object v0, p0, Lbs;->u:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 911
    iget-object v0, p0, Lbs;->u:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 912
    iput v4, p0, Lbs;->h:I

    .line 913
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbs;->v:Ljava/util/ArrayList;

    .line 914
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 1262
    if-eqz p2, :cond_0

    .line 1263
    iget-object v0, p0, Lbs;->u:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1267
    :goto_0
    return-void

    .line 1265
    :cond_0
    iget-object v0, p0, Lbs;->u:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method

.method protected static f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 1536
    if-nez p0, :cond_1

    .line 1540
    :cond_0
    :goto_0
    return-object p0

    .line 1537
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 1538
    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1409
    iget-object v0, p0, Lbs;->q:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 1410
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lbs;->q:Landroid/os/Bundle;

    .line 1412
    :cond_0
    iget-object v0, p0, Lbs;->q:Landroid/os/Bundle;

    return-object v0
.end method

.method public a(I)Lbs;
    .locals 1

    .prologue
    .line 958
    iget-object v0, p0, Lbs;->u:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 959
    return-object p0
.end method

.method public a(IIZ)Lbs;
    .locals 0

    .prologue
    .line 1031
    iput p1, p0, Lbs;->l:I

    .line 1032
    iput p2, p0, Lbs;->m:I

    .line 1033
    iput-boolean p3, p0, Lbs;->n:Z

    .line 1034
    return-object p0
.end method

.method public a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lbs;
    .locals 2

    .prologue
    .line 1432
    iget-object v0, p0, Lbs;->o:Ljava/util/ArrayList;

    new-instance v1, Lbn;

    invoke-direct {v1, p1, p2, p3}, Lbn;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1433
    return-object p0
.end method

.method public a(J)Lbs;
    .locals 1

    .prologue
    .line 921
    iget-object v0, p0, Lbs;->u:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 922
    return-object p0
.end method

.method public a(Landroid/app/Notification;)Lbs;
    .locals 0

    .prologue
    .line 1506
    iput-object p1, p0, Lbs;->t:Landroid/app/Notification;

    .line 1507
    return-object p0
.end method

.method public a(Landroid/app/PendingIntent;)Lbs;
    .locals 0

    .prologue
    .line 1054
    iput-object p1, p0, Lbs;->d:Landroid/app/PendingIntent;

    .line 1055
    return-object p0
.end method

.method public a(Landroid/graphics/Bitmap;)Lbs;
    .locals 0

    .prologue
    .line 1118
    iput-object p1, p0, Lbs;->e:Landroid/graphics/Bitmap;

    .line 1119
    return-object p0
.end method

.method public a(Landroid/widget/RemoteViews;)Lbs;
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Lbs;->u:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 1042
    return-object p0
.end method

.method public a(Lbt;)Lbs;
    .locals 0

    .prologue
    .line 1515
    invoke-interface {p1, p0}, Lbt;->a(Lbs;)Lbs;

    .line 1516
    return-object p0
.end method

.method public a(Lce;)Lbs;
    .locals 1

    .prologue
    .line 1464
    iget-object v0, p0, Lbs;->j:Lce;

    if-eq v0, p1, :cond_0

    .line 1465
    iput-object p1, p0, Lbs;->j:Lce;

    .line 1466
    iget-object v0, p0, Lbs;->j:Lce;

    if-eqz v0, :cond_0

    .line 1467
    iget-object v0, p0, Lbs;->j:Lce;

    invoke-virtual {v0, p0}, Lce;->a(Lbs;)V

    .line 1470
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lbs;
    .locals 1

    .prologue
    .line 982
    invoke-static {p1}, Lbs;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lbs;->b:Ljava/lang/CharSequence;

    .line 983
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbs;
    .locals 0

    .prologue
    .line 1239
    iput-object p1, p0, Lbs;->p:Ljava/lang/String;

    .line 1240
    return-object p0
.end method

.method public a(Z)Lbs;
    .locals 1

    .prologue
    .line 1196
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lbs;->a(IZ)V

    .line 1197
    return-object p0
.end method

.method public b()Landroid/app/Notification;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1524
    invoke-static {}, Lbm;->a()Lbv;

    move-result-object v0

    invoke-interface {v0, p0}, Lbv;->a(Lbs;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lbs;
    .locals 0

    .prologue
    .line 1014
    iput p1, p0, Lbs;->g:I

    .line 1015
    return-object p0
.end method

.method public b(Landroid/app/PendingIntent;)Lbs;
    .locals 1

    .prologue
    .line 1066
    iget-object v0, p0, Lbs;->u:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 1067
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lbs;
    .locals 1

    .prologue
    .line 990
    invoke-static {p1}, Lbs;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lbs;->c:Ljava/lang/CharSequence;

    .line 991
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lbs;
    .locals 1

    .prologue
    .line 1314
    iget-object v0, p0, Lbs;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1315
    return-object p0
.end method

.method public b(Z)Lbs;
    .locals 1

    .prologue
    .line 1216
    const/16 v0, 0x10

    invoke-direct {p0, v0, p1}, Lbs;->a(IZ)V

    .line 1217
    return-object p0
.end method

.method public c()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 1532
    invoke-static {}, Lbm;->a()Lbv;

    move-result-object v0

    invoke-interface {v0, p0}, Lbv;->a(Lbs;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public c(I)Lbs;
    .locals 0

    .prologue
    .line 1287
    iput p1, p0, Lbs;->h:I

    .line 1288
    return-object p0
.end method

.method public c(Ljava/lang/CharSequence;)Lbs;
    .locals 1

    .prologue
    .line 1004
    invoke-static {p1}, Lbs;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lbs;->k:Ljava/lang/CharSequence;

    .line 1005
    return-object p0
.end method

.method public d(I)Lbs;
    .locals 0

    .prologue
    .line 1481
    iput p1, p0, Lbs;->r:I

    .line 1482
    return-object p0
.end method

.method public d(Ljava/lang/CharSequence;)Lbs;
    .locals 1

    .prologue
    .line 1022
    invoke-static {p1}, Lbs;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lbs;->f:Ljava/lang/CharSequence;

    .line 1023
    return-object p0
.end method

.method public e(I)Lbs;
    .locals 0

    .prologue
    .line 1493
    iput p1, p0, Lbs;->s:I

    .line 1494
    return-object p0
.end method

.method public e(Ljava/lang/CharSequence;)Lbs;
    .locals 2

    .prologue
    .line 1099
    iget-object v0, p0, Lbs;->u:Landroid/app/Notification;

    invoke-static {p1}, Lbs;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 1100
    return-object p0
.end method
