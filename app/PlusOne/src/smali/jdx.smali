.class public final Ljdx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljdx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljdz;

.field public final b:Ljava/lang/Long;

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Ljdy;

    invoke-direct {v0}, Ljdy;-><init>()V

    sput-object v0, Ljdx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljdz;Ljava/lang/Long;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "photoId or assetId should be non-null"

    invoke-static {v0, v1}, Llsk;->a(ZLjava/lang/Object;)V

    .line 58
    iput-object p1, p0, Ljdx;->a:Ljdz;

    .line 59
    iput-object p2, p0, Ljdx;->b:Ljava/lang/Long;

    .line 60
    const/16 v0, 0x11

    invoke-static {p2, v0}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v0

    invoke-static {p1, v0}, Llsh;->a(Ljava/lang/Object;I)I

    move-result v0

    iput v0, p0, Ljdx;->c:I

    .line 61
    return-void

    .line 55
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(J)Ljdx;
    .locals 4

    .prologue
    .line 35
    new-instance v0, Ljdx;

    const/4 v1, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljdx;-><init>(Ljdz;Ljava/lang/Long;)V

    return-object v0
.end method

.method public static a(Ljdz;)Ljdx;
    .locals 3

    .prologue
    .line 31
    new-instance v1, Ljdx;

    const-string v0, "photoId"

    invoke-static {p0, v0}, Llsk;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdz;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ljdx;-><init>(Ljdz;Ljava/lang/Long;)V

    return-object v1
.end method

.method public static a(Lonw;)Ljdx;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    iget-object v0, p0, Lonw;->b:Lony;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "burst ids should be null"

    invoke-static {v0, v4}, Llsk;->a(ZLjava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lonw;->c:Looa;

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "soundtrack id should be null"

    invoke-static {v1, v0}, Llsk;->a(ZLjava/lang/Object;)V

    .line 43
    iget-object v2, p0, Lonw;->d:Lonx;

    .line 44
    new-instance v4, Ljdx;

    iget-object v0, p0, Lonw;->a:Lonz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lonw;->a:Lonz;

    .line 45
    invoke-static {v0}, Ljdz;->a(Lonz;)Ljdz;

    move-result-object v0

    move-object v1, v0

    :goto_2
    if-eqz v2, :cond_3

    iget-object v0, v2, Lonx;->a:Ljava/lang/Long;

    :goto_3
    invoke-direct {v4, v1, v0}, Ljdx;-><init>(Ljdz;Ljava/lang/Long;)V

    return-object v4

    :cond_0
    move v0, v2

    .line 40
    goto :goto_0

    :cond_1
    move v1, v2

    .line 41
    goto :goto_1

    :cond_2
    move-object v1, v3

    .line 45
    goto :goto_2

    :cond_3
    move-object v0, v3

    goto :goto_3
.end method

.method public static a([B)Ljdx;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lonw;

    invoke-direct {v0}, Lonw;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lonw;

    invoke-static {v0}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lonw;
    .locals 3

    .prologue
    .line 64
    new-instance v1, Lonw;

    invoke-direct {v1}, Lonw;-><init>()V

    .line 65
    iget-object v0, p0, Ljdx;->a:Ljdz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljdx;->a:Ljdz;

    invoke-virtual {v0}, Ljdz;->a()Lonz;

    move-result-object v0

    :goto_0
    iput-object v0, v1, Lonw;->a:Lonz;

    .line 66
    iget-object v0, p0, Ljdx;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 67
    new-instance v0, Lonx;

    invoke-direct {v0}, Lonx;-><init>()V

    iput-object v0, v1, Lonw;->d:Lonx;

    .line 68
    iget-object v0, v1, Lonw;->d:Lonx;

    iget-object v2, p0, Ljdx;->b:Ljava/lang/Long;

    iput-object v2, v0, Lonx;->a:Ljava/lang/Long;

    .line 70
    :cond_0
    return-object v1

    .line 65
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()[B
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Ljdx;->a()Lonw;

    move-result-object v0

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 80
    instance-of v2, p1, Ljdx;

    if-nez v2, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    if-ne p1, p0, :cond_2

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_2
    check-cast p1, Ljdx;

    .line 87
    iget-object v2, p0, Ljdx;->a:Ljdz;

    iget-object v3, p1, Ljdx;->a:Ljdz;

    invoke-static {v2, v3}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Ljdx;->b:Ljava/lang/Long;

    iget-object v3, p1, Ljdx;->b:Ljava/lang/Long;

    .line 88
    invoke-static {v2, v3}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Ljdx;->c:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 108
    const-string v0, "CloudMediaId"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Ljdx;->a:Ljdz;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Ljdx;->b:Ljava/lang/Long;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Llsh;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 93
    invoke-virtual {p0}, Ljdx;->b()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 94
    return-void
.end method
