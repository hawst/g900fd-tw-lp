.class public final Lmsi;
.super Loxq;
.source "PG"


# instance fields
.field private a:[I

.field private b:[Lmsn;

.field private c:[Lmso;

.field private d:[Lmtd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3972
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3975
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lmsi;->a:[I

    .line 3978
    sget-object v0, Lmsn;->a:[Lmsn;

    iput-object v0, p0, Lmsi;->b:[Lmsn;

    .line 3981
    sget-object v0, Lmso;->a:[Lmso;

    iput-object v0, p0, Lmsi;->c:[Lmso;

    .line 3984
    sget-object v0, Lmtd;->a:[Lmtd;

    iput-object v0, p0, Lmsi;->d:[Lmtd;

    .line 3972
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 4021
    .line 4022
    iget-object v0, p0, Lmsi;->a:[I

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmsi;->a:[I

    array-length v0, v0

    if-lez v0, :cond_7

    .line 4024
    iget-object v3, p0, Lmsi;->a:[I

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    .line 4026
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 4024
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4029
    :cond_0
    iget-object v0, p0, Lmsi;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 4031
    :goto_1
    iget-object v2, p0, Lmsi;->b:[Lmsn;

    if-eqz v2, :cond_2

    .line 4032
    iget-object v3, p0, Lmsi;->b:[Lmsn;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 4033
    if-eqz v5, :cond_1

    .line 4034
    const/4 v6, 0x2

    .line 4035
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4032
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 4039
    :cond_2
    iget-object v2, p0, Lmsi;->c:[Lmso;

    if-eqz v2, :cond_4

    .line 4040
    iget-object v3, p0, Lmsi;->c:[Lmso;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 4041
    if-eqz v5, :cond_3

    .line 4042
    const/4 v6, 0x3

    .line 4043
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 4040
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 4047
    :cond_4
    iget-object v2, p0, Lmsi;->d:[Lmtd;

    if-eqz v2, :cond_6

    .line 4048
    iget-object v2, p0, Lmsi;->d:[Lmtd;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 4049
    if-eqz v4, :cond_5

    .line 4050
    const/4 v5, 0x4

    .line 4051
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4048
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 4055
    :cond_6
    iget-object v1, p0, Lmsi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4056
    iput v0, p0, Lmsi;->ai:I

    .line 4057
    return v0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lmsi;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4065
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4066
    sparse-switch v0, :sswitch_data_0

    .line 4070
    iget-object v2, p0, Lmsi;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4071
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmsi;->ah:Ljava/util/List;

    .line 4074
    :cond_1
    iget-object v2, p0, Lmsi;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4076
    :sswitch_0
    return-object p0

    .line 4081
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4082
    iget-object v0, p0, Lmsi;->a:[I

    array-length v0, v0

    .line 4083
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 4084
    iget-object v3, p0, Lmsi;->a:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4085
    iput-object v2, p0, Lmsi;->a:[I

    .line 4086
    :goto_1
    iget-object v2, p0, Lmsi;->a:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    .line 4087
    iget-object v2, p0, Lmsi;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 4088
    invoke-virtual {p1}, Loxn;->a()I

    .line 4086
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4091
    :cond_2
    iget-object v2, p0, Lmsi;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto :goto_0

    .line 4095
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4096
    iget-object v0, p0, Lmsi;->b:[Lmsn;

    if-nez v0, :cond_4

    move v0, v1

    .line 4097
    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lmsn;

    .line 4098
    iget-object v3, p0, Lmsi;->b:[Lmsn;

    if-eqz v3, :cond_3

    .line 4099
    iget-object v3, p0, Lmsi;->b:[Lmsn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4101
    :cond_3
    iput-object v2, p0, Lmsi;->b:[Lmsn;

    .line 4102
    :goto_3
    iget-object v2, p0, Lmsi;->b:[Lmsn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 4103
    iget-object v2, p0, Lmsi;->b:[Lmsn;

    new-instance v3, Lmsn;

    invoke-direct {v3}, Lmsn;-><init>()V

    aput-object v3, v2, v0

    .line 4104
    iget-object v2, p0, Lmsi;->b:[Lmsn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4105
    invoke-virtual {p1}, Loxn;->a()I

    .line 4102
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 4096
    :cond_4
    iget-object v0, p0, Lmsi;->b:[Lmsn;

    array-length v0, v0

    goto :goto_2

    .line 4108
    :cond_5
    iget-object v2, p0, Lmsi;->b:[Lmsn;

    new-instance v3, Lmsn;

    invoke-direct {v3}, Lmsn;-><init>()V

    aput-object v3, v2, v0

    .line 4109
    iget-object v2, p0, Lmsi;->b:[Lmsn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4113
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4114
    iget-object v0, p0, Lmsi;->c:[Lmso;

    if-nez v0, :cond_7

    move v0, v1

    .line 4115
    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lmso;

    .line 4116
    iget-object v3, p0, Lmsi;->c:[Lmso;

    if-eqz v3, :cond_6

    .line 4117
    iget-object v3, p0, Lmsi;->c:[Lmso;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4119
    :cond_6
    iput-object v2, p0, Lmsi;->c:[Lmso;

    .line 4120
    :goto_5
    iget-object v2, p0, Lmsi;->c:[Lmso;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 4121
    iget-object v2, p0, Lmsi;->c:[Lmso;

    new-instance v3, Lmso;

    invoke-direct {v3}, Lmso;-><init>()V

    aput-object v3, v2, v0

    .line 4122
    iget-object v2, p0, Lmsi;->c:[Lmso;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4123
    invoke-virtual {p1}, Loxn;->a()I

    .line 4120
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 4114
    :cond_7
    iget-object v0, p0, Lmsi;->c:[Lmso;

    array-length v0, v0

    goto :goto_4

    .line 4126
    :cond_8
    iget-object v2, p0, Lmsi;->c:[Lmso;

    new-instance v3, Lmso;

    invoke-direct {v3}, Lmso;-><init>()V

    aput-object v3, v2, v0

    .line 4127
    iget-object v2, p0, Lmsi;->c:[Lmso;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4131
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4132
    iget-object v0, p0, Lmsi;->d:[Lmtd;

    if-nez v0, :cond_a

    move v0, v1

    .line 4133
    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lmtd;

    .line 4134
    iget-object v3, p0, Lmsi;->d:[Lmtd;

    if-eqz v3, :cond_9

    .line 4135
    iget-object v3, p0, Lmsi;->d:[Lmtd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4137
    :cond_9
    iput-object v2, p0, Lmsi;->d:[Lmtd;

    .line 4138
    :goto_7
    iget-object v2, p0, Lmsi;->d:[Lmtd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    .line 4139
    iget-object v2, p0, Lmsi;->d:[Lmtd;

    new-instance v3, Lmtd;

    invoke-direct {v3}, Lmtd;-><init>()V

    aput-object v3, v2, v0

    .line 4140
    iget-object v2, p0, Lmsi;->d:[Lmtd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4141
    invoke-virtual {p1}, Loxn;->a()I

    .line 4138
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 4132
    :cond_a
    iget-object v0, p0, Lmsi;->d:[Lmtd;

    array-length v0, v0

    goto :goto_6

    .line 4144
    :cond_b
    iget-object v2, p0, Lmsi;->d:[Lmtd;

    new-instance v3, Lmtd;

    invoke-direct {v3}, Lmtd;-><init>()V

    aput-object v3, v2, v0

    .line 4145
    iget-object v2, p0, Lmsi;->d:[Lmtd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4066
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3989
    iget-object v1, p0, Lmsi;->a:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmsi;->a:[I

    array-length v1, v1

    if-lez v1, :cond_0

    .line 3990
    iget-object v2, p0, Lmsi;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 3991
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 3990
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3994
    :cond_0
    iget-object v1, p0, Lmsi;->b:[Lmsn;

    if-eqz v1, :cond_2

    .line 3995
    iget-object v2, p0, Lmsi;->b:[Lmsn;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 3996
    if-eqz v4, :cond_1

    .line 3997
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 3995
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4001
    :cond_2
    iget-object v1, p0, Lmsi;->c:[Lmso;

    if-eqz v1, :cond_4

    .line 4002
    iget-object v2, p0, Lmsi;->c:[Lmso;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 4003
    if-eqz v4, :cond_3

    .line 4004
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 4002
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4008
    :cond_4
    iget-object v1, p0, Lmsi;->d:[Lmtd;

    if-eqz v1, :cond_6

    .line 4009
    iget-object v1, p0, Lmsi;->d:[Lmtd;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 4010
    if-eqz v3, :cond_5

    .line 4011
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 4009
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 4015
    :cond_6
    iget-object v0, p0, Lmsi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4017
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3968
    invoke-virtual {p0, p1}, Lmsi;->a(Loxn;)Lmsi;

    move-result-object v0

    return-object v0
.end method
