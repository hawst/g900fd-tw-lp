.class public final Lodw;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 141
    invoke-direct {p0}, Loxq;-><init>()V

    .line 144
    iput v0, p0, Lodw;->a:I

    .line 147
    iput v0, p0, Lodw;->b:I

    .line 141
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 164
    const/4 v0, 0x0

    .line 165
    iget v1, p0, Lodw;->a:I

    if-eq v1, v2, :cond_0

    .line 166
    const/4 v0, 0x1

    iget v1, p0, Lodw;->a:I

    .line 167
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 169
    :cond_0
    iget v1, p0, Lodw;->b:I

    if-eq v1, v2, :cond_1

    .line 170
    const/4 v1, 0x2

    iget v2, p0, Lodw;->b:I

    .line 171
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_1
    iget-object v1, p0, Lodw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    iput v0, p0, Lodw;->ai:I

    .line 175
    return v0
.end method

.method public a(Loxn;)Lodw;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 183
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 184
    sparse-switch v0, :sswitch_data_0

    .line 188
    iget-object v1, p0, Lodw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 189
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lodw;->ah:Ljava/util/List;

    .line 192
    :cond_1
    iget-object v1, p0, Lodw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    :sswitch_0
    return-object p0

    .line 199
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 200
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 205
    :cond_2
    iput v0, p0, Lodw;->a:I

    goto :goto_0

    .line 207
    :cond_3
    iput v2, p0, Lodw;->a:I

    goto :goto_0

    .line 212
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 213
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-ne v0, v4, :cond_5

    .line 216
    :cond_4
    iput v0, p0, Lodw;->b:I

    goto :goto_0

    .line 218
    :cond_5
    iput v2, p0, Lodw;->b:I

    goto :goto_0

    .line 184
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 152
    iget v0, p0, Lodw;->a:I

    if-eq v0, v2, :cond_0

    .line 153
    const/4 v0, 0x1

    iget v1, p0, Lodw;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 155
    :cond_0
    iget v0, p0, Lodw;->b:I

    if-eq v0, v2, :cond_1

    .line 156
    const/4 v0, 0x2

    iget v1, p0, Lodw;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 158
    :cond_1
    iget-object v0, p0, Lodw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 160
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0, p1}, Lodw;->a(Loxn;)Lodw;

    move-result-object v0

    return-object v0
.end method
