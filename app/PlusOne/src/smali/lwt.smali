.class public final Llwt;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llwt;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2552
    const/4 v0, 0x0

    new-array v0, v0, [Llwt;

    sput-object v0, Llwt;->a:[Llwt;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2553
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2562
    const/4 v0, 0x0

    iput-object v0, p0, Llwt;->b:Llxp;

    .line 2567
    const/high16 v0, -0x80000000

    iput v0, p0, Llwt;->d:I

    .line 2553
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2587
    const/4 v0, 0x0

    .line 2588
    iget-object v1, p0, Llwt;->b:Llxp;

    if-eqz v1, :cond_0

    .line 2589
    const/4 v0, 0x1

    iget-object v1, p0, Llwt;->b:Llxp;

    .line 2590
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2592
    :cond_0
    iget-object v1, p0, Llwt;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2593
    const/4 v1, 0x2

    iget-object v2, p0, Llwt;->c:Ljava/lang/String;

    .line 2594
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2596
    :cond_1
    iget v1, p0, Llwt;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 2597
    const/4 v1, 0x3

    iget v2, p0, Llwt;->d:I

    .line 2598
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2600
    :cond_2
    iget-object v1, p0, Llwt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2601
    iput v0, p0, Llwt;->ai:I

    .line 2602
    return v0
.end method

.method public a(Loxn;)Llwt;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2610
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2611
    sparse-switch v0, :sswitch_data_0

    .line 2615
    iget-object v1, p0, Llwt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2616
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llwt;->ah:Ljava/util/List;

    .line 2619
    :cond_1
    iget-object v1, p0, Llwt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2621
    :sswitch_0
    return-object p0

    .line 2626
    :sswitch_1
    iget-object v0, p0, Llwt;->b:Llxp;

    if-nez v0, :cond_2

    .line 2627
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llwt;->b:Llxp;

    .line 2629
    :cond_2
    iget-object v0, p0, Llwt;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2633
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llwt;->c:Ljava/lang/String;

    goto :goto_0

    .line 2637
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2638
    if-eq v0, v2, :cond_3

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 2641
    :cond_3
    iput v0, p0, Llwt;->d:I

    goto :goto_0

    .line 2643
    :cond_4
    iput v2, p0, Llwt;->d:I

    goto :goto_0

    .line 2611
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2572
    iget-object v0, p0, Llwt;->b:Llxp;

    if-eqz v0, :cond_0

    .line 2573
    const/4 v0, 0x1

    iget-object v1, p0, Llwt;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2575
    :cond_0
    iget-object v0, p0, Llwt;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2576
    const/4 v0, 0x2

    iget-object v1, p0, Llwt;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2578
    :cond_1
    iget v0, p0, Llwt;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 2579
    const/4 v0, 0x3

    iget v1, p0, Llwt;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2581
    :cond_2
    iget-object v0, p0, Llwt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2583
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2549
    invoke-virtual {p0, p1}, Llwt;->a(Loxn;)Llwt;

    move-result-object v0

    return-object v0
.end method
