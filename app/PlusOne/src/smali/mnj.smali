.class public final Lmnj;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Lmms;

.field public c:Lmnh;

.field public d:Ljava/lang/String;

.field public e:Lmoc;

.field public f:Lmns;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5588
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5611
    const/high16 v0, -0x80000000

    iput v0, p0, Lmnj;->a:I

    .line 5614
    iput-object v1, p0, Lmnj;->b:Lmms;

    .line 5617
    iput-object v1, p0, Lmnj;->c:Lmnh;

    .line 5622
    iput-object v1, p0, Lmnj;->e:Lmoc;

    .line 5625
    iput-object v1, p0, Lmnj;->f:Lmns;

    .line 5588
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5654
    const/4 v0, 0x0

    .line 5655
    iget v1, p0, Lmnj;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 5656
    const/4 v0, 0x1

    iget v1, p0, Lmnj;->a:I

    .line 5657
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5659
    :cond_0
    iget-object v1, p0, Lmnj;->b:Lmms;

    if-eqz v1, :cond_1

    .line 5660
    const/4 v1, 0x2

    iget-object v2, p0, Lmnj;->b:Lmms;

    .line 5661
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5663
    :cond_1
    iget-object v1, p0, Lmnj;->c:Lmnh;

    if-eqz v1, :cond_2

    .line 5664
    const/4 v1, 0x3

    iget-object v2, p0, Lmnj;->c:Lmnh;

    .line 5665
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5667
    :cond_2
    iget-object v1, p0, Lmnj;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5668
    const/4 v1, 0x4

    iget-object v2, p0, Lmnj;->d:Ljava/lang/String;

    .line 5669
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5671
    :cond_3
    iget-object v1, p0, Lmnj;->e:Lmoc;

    if-eqz v1, :cond_4

    .line 5672
    const/4 v1, 0x6

    iget-object v2, p0, Lmnj;->e:Lmoc;

    .line 5673
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5675
    :cond_4
    iget-object v1, p0, Lmnj;->f:Lmns;

    if-eqz v1, :cond_5

    .line 5676
    const/4 v1, 0x7

    iget-object v2, p0, Lmnj;->f:Lmns;

    .line 5677
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5679
    :cond_5
    iget-object v1, p0, Lmnj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5680
    iput v0, p0, Lmnj;->ai:I

    .line 5681
    return v0
.end method

.method public a(Loxn;)Lmnj;
    .locals 2

    .prologue
    .line 5689
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5690
    sparse-switch v0, :sswitch_data_0

    .line 5694
    iget-object v1, p0, Lmnj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5695
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnj;->ah:Ljava/util/List;

    .line 5698
    :cond_1
    iget-object v1, p0, Lmnj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5700
    :sswitch_0
    return-object p0

    .line 5705
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5706
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 5723
    :cond_2
    iput v0, p0, Lmnj;->a:I

    goto :goto_0

    .line 5725
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmnj;->a:I

    goto :goto_0

    .line 5730
    :sswitch_2
    iget-object v0, p0, Lmnj;->b:Lmms;

    if-nez v0, :cond_4

    .line 5731
    new-instance v0, Lmms;

    invoke-direct {v0}, Lmms;-><init>()V

    iput-object v0, p0, Lmnj;->b:Lmms;

    .line 5733
    :cond_4
    iget-object v0, p0, Lmnj;->b:Lmms;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5737
    :sswitch_3
    iget-object v0, p0, Lmnj;->c:Lmnh;

    if-nez v0, :cond_5

    .line 5738
    new-instance v0, Lmnh;

    invoke-direct {v0}, Lmnh;-><init>()V

    iput-object v0, p0, Lmnj;->c:Lmnh;

    .line 5740
    :cond_5
    iget-object v0, p0, Lmnj;->c:Lmnh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5744
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmnj;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 5748
    :sswitch_5
    iget-object v0, p0, Lmnj;->e:Lmoc;

    if-nez v0, :cond_6

    .line 5749
    new-instance v0, Lmoc;

    invoke-direct {v0}, Lmoc;-><init>()V

    iput-object v0, p0, Lmnj;->e:Lmoc;

    .line 5751
    :cond_6
    iget-object v0, p0, Lmnj;->e:Lmoc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5755
    :sswitch_6
    iget-object v0, p0, Lmnj;->f:Lmns;

    if-nez v0, :cond_7

    .line 5756
    new-instance v0, Lmns;

    invoke-direct {v0}, Lmns;-><init>()V

    iput-object v0, p0, Lmnj;->f:Lmns;

    .line 5758
    :cond_7
    iget-object v0, p0, Lmnj;->f:Lmns;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 5690
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5630
    iget v0, p0, Lmnj;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 5631
    const/4 v0, 0x1

    iget v1, p0, Lmnj;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5633
    :cond_0
    iget-object v0, p0, Lmnj;->b:Lmms;

    if-eqz v0, :cond_1

    .line 5634
    const/4 v0, 0x2

    iget-object v1, p0, Lmnj;->b:Lmms;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5636
    :cond_1
    iget-object v0, p0, Lmnj;->c:Lmnh;

    if-eqz v0, :cond_2

    .line 5637
    const/4 v0, 0x3

    iget-object v1, p0, Lmnj;->c:Lmnh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5639
    :cond_2
    iget-object v0, p0, Lmnj;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 5640
    const/4 v0, 0x4

    iget-object v1, p0, Lmnj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5642
    :cond_3
    iget-object v0, p0, Lmnj;->e:Lmoc;

    if-eqz v0, :cond_4

    .line 5643
    const/4 v0, 0x6

    iget-object v1, p0, Lmnj;->e:Lmoc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5645
    :cond_4
    iget-object v0, p0, Lmnj;->f:Lmns;

    if-eqz v0, :cond_5

    .line 5646
    const/4 v0, 0x7

    iget-object v1, p0, Lmnj;->f:Lmns;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5648
    :cond_5
    iget-object v0, p0, Lmnj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5650
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5584
    invoke-virtual {p0, p1}, Lmnj;->a(Loxn;)Lmnj;

    move-result-object v0

    return-object v0
.end method
