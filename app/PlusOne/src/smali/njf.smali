.class public final Lnjf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnja;

.field public b:[Lnje;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4109
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4112
    const/4 v0, 0x0

    iput-object v0, p0, Lnjf;->a:Lnja;

    .line 4115
    sget-object v0, Lnje;->a:[Lnje;

    iput-object v0, p0, Lnjf;->b:[Lnje;

    .line 4109
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4136
    .line 4137
    iget-object v0, p0, Lnjf;->a:Lnja;

    if-eqz v0, :cond_2

    .line 4138
    const/4 v0, 0x1

    iget-object v2, p0, Lnjf;->a:Lnja;

    .line 4139
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4141
    :goto_0
    iget-object v2, p0, Lnjf;->b:[Lnje;

    if-eqz v2, :cond_1

    .line 4142
    iget-object v2, p0, Lnjf;->b:[Lnje;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 4143
    if-eqz v4, :cond_0

    .line 4144
    const/4 v5, 0x2

    .line 4145
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4142
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4149
    :cond_1
    iget-object v1, p0, Lnjf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4150
    iput v0, p0, Lnjf;->ai:I

    .line 4151
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnjf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4159
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4160
    sparse-switch v0, :sswitch_data_0

    .line 4164
    iget-object v2, p0, Lnjf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 4165
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnjf;->ah:Ljava/util/List;

    .line 4168
    :cond_1
    iget-object v2, p0, Lnjf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4170
    :sswitch_0
    return-object p0

    .line 4175
    :sswitch_1
    iget-object v0, p0, Lnjf;->a:Lnja;

    if-nez v0, :cond_2

    .line 4176
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnjf;->a:Lnja;

    .line 4178
    :cond_2
    iget-object v0, p0, Lnjf;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4182
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 4183
    iget-object v0, p0, Lnjf;->b:[Lnje;

    if-nez v0, :cond_4

    move v0, v1

    .line 4184
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnje;

    .line 4185
    iget-object v3, p0, Lnjf;->b:[Lnje;

    if-eqz v3, :cond_3

    .line 4186
    iget-object v3, p0, Lnjf;->b:[Lnje;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4188
    :cond_3
    iput-object v2, p0, Lnjf;->b:[Lnje;

    .line 4189
    :goto_2
    iget-object v2, p0, Lnjf;->b:[Lnje;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 4190
    iget-object v2, p0, Lnjf;->b:[Lnje;

    new-instance v3, Lnje;

    invoke-direct {v3}, Lnje;-><init>()V

    aput-object v3, v2, v0

    .line 4191
    iget-object v2, p0, Lnjf;->b:[Lnje;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 4192
    invoke-virtual {p1}, Loxn;->a()I

    .line 4189
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4183
    :cond_4
    iget-object v0, p0, Lnjf;->b:[Lnje;

    array-length v0, v0

    goto :goto_1

    .line 4195
    :cond_5
    iget-object v2, p0, Lnjf;->b:[Lnje;

    new-instance v3, Lnje;

    invoke-direct {v3}, Lnje;-><init>()V

    aput-object v3, v2, v0

    .line 4196
    iget-object v2, p0, Lnjf;->b:[Lnje;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4160
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 4120
    iget-object v0, p0, Lnjf;->a:Lnja;

    if-eqz v0, :cond_0

    .line 4121
    const/4 v0, 0x1

    iget-object v1, p0, Lnjf;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4123
    :cond_0
    iget-object v0, p0, Lnjf;->b:[Lnje;

    if-eqz v0, :cond_2

    .line 4124
    iget-object v1, p0, Lnjf;->b:[Lnje;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 4125
    if-eqz v3, :cond_1

    .line 4126
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 4124
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4130
    :cond_2
    iget-object v0, p0, Lnjf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4132
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4105
    invoke-virtual {p0, p1}, Lnjf;->a(Loxn;)Lnjf;

    move-result-object v0

    return-object v0
.end method
