.class public final Lpvn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1603
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1608
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpvn;->b:[Ljava/lang/String;

    .line 1603
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1627
    .line 1628
    iget-object v0, p0, Lpvn;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1629
    const/4 v0, 0x1

    iget-object v2, p0, Lpvn;->a:Ljava/lang/String;

    .line 1630
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1632
    :goto_0
    iget-object v2, p0, Lpvn;->b:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lpvn;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1634
    iget-object v3, p0, Lpvn;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 1636
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 1634
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1638
    :cond_0
    add-int/2addr v0, v2

    .line 1639
    iget-object v1, p0, Lpvn;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1641
    :cond_1
    iget-object v1, p0, Lpvn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1642
    iput v0, p0, Lpvn;->ai:I

    .line 1643
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpvn;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1651
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1652
    sparse-switch v0, :sswitch_data_0

    .line 1656
    iget-object v1, p0, Lpvn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1657
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpvn;->ah:Ljava/util/List;

    .line 1660
    :cond_1
    iget-object v1, p0, Lpvn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1662
    :sswitch_0
    return-object p0

    .line 1667
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvn;->a:Ljava/lang/String;

    goto :goto_0

    .line 1671
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1672
    iget-object v0, p0, Lpvn;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 1673
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1674
    iget-object v2, p0, Lpvn;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1675
    iput-object v1, p0, Lpvn;->b:[Ljava/lang/String;

    .line 1676
    :goto_1
    iget-object v1, p0, Lpvn;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1677
    iget-object v1, p0, Lpvn;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1678
    invoke-virtual {p1}, Loxn;->a()I

    .line 1676
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1681
    :cond_2
    iget-object v1, p0, Lpvn;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1652
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1613
    iget-object v0, p0, Lpvn;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1614
    const/4 v0, 0x1

    iget-object v1, p0, Lpvn;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1616
    :cond_0
    iget-object v0, p0, Lpvn;->b:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1617
    iget-object v1, p0, Lpvn;->b:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1618
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 1617
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1621
    :cond_1
    iget-object v0, p0, Lpvn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1623
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1599
    invoke-virtual {p0, p1}, Lpvn;->a(Loxn;)Lpvn;

    move-result-object v0

    return-object v0
.end method
