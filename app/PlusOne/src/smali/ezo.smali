.class public final Lezo;
.super Lhye;
.source "PG"


# instance fields
.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 82
    iput-boolean p2, p0, Lezo;->b:Z

    .line 83
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 15

    .prologue
    .line 87
    new-instance v6, Lhym;

    sget-object v0, Lezq;->a:[Ljava/lang/String;

    invoke-direct {v6, v0}, Lhym;-><init>([Ljava/lang/String;)V

    .line 89
    iget-boolean v0, p0, Lezo;->b:Z

    if-eqz v0, :cond_0

    move-object v0, v6

    .line 178
    :goto_0
    return-object v0

    .line 95
    :cond_0
    invoke-virtual {p0}, Lezo;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 96
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Email;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lezr;->a:[Ljava/lang/String;

    const-string v3, "in_visible_group!=0"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 99
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 100
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10, v9}, Ljava/util/HashMap;-><init>(I)V

    .line 102
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 106
    add-int/lit16 v1, v9, 0x3e7

    add-int/lit8 v1, v1, -0x1

    div-int/lit16 v12, v1, 0x3e7

    .line 109
    const/4 v1, 0x0

    move v7, v1

    :goto_1
    if-ge v7, v12, :cond_6

    .line 110
    const/4 v1, 0x0

    .line 113
    :try_start_0
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v3

    .line 114
    const-string v2, "raw_contact_id IN ("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    :goto_2
    const/16 v2, 0x3e7

    if-ge v1, v2, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 117
    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 118
    new-instance v2, Lezp;

    invoke-direct {v2}, Lezp;-><init>()V

    .line 119
    const/4 v13, 0x1

    invoke-interface {v8, v13}, Landroid/database/Cursor;->isNull(I)Z

    move-result v13

    if-nez v13, :cond_1

    .line 120
    const/4 v13, 0x1

    .line 121
    invoke-interface {v8, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    iput-object v13, v2, Lezp;->b:Ljava/lang/String;

    .line 124
    :cond_1
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 125
    invoke-interface {v8}, Landroid/database/Cursor;->getPosition()I

    move-result v13

    add-int/lit8 v14, v9, -0x1

    if-ge v13, v14, :cond_2

    const/16 v13, 0x3e6

    if-ge v1, v13, :cond_2

    .line 127
    const-string v13, ","

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v10, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 132
    add-int/lit8 v1, v1, 0x1

    .line 133
    goto :goto_2

    .line 135
    :cond_3
    const-string v1, ") AND (mimetype=\'vnd.android.cursor.item/name"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 136
    const-string v2, "\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lezs;->a:[Ljava/lang/String;

    .line 139
    invoke-static {v3}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "data1 ASC"

    .line 138
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 142
    :cond_4
    :goto_3
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 143
    const/4 v1, 0x1

    .line 144
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 145
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 146
    const/4 v1, 0x0

    .line 150
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 151
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 153
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 152
    invoke-virtual {v10, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lezp;

    .line 154
    iput-object v3, v1, Lezp;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 158
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 162
    :catchall_1
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 158
    :cond_5
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 109
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto/16 :goto_1

    .line 162
    :cond_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 165
    invoke-static {v11}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 167
    const/4 v0, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_4
    if-ge v1, v2, :cond_8

    .line 168
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezp;

    .line 169
    iget-object v3, v0, Lezp;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, v0, Lezp;->b:Ljava/lang/String;

    .line 170
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 171
    invoke-virtual {v0}, Lezp;->a()[Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 167
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 175
    :cond_8
    invoke-virtual {v10}, Ljava/util/HashMap;->clear()V

    .line 176
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    move-object v0, v6

    .line 178
    goto/16 :goto_0
.end method
