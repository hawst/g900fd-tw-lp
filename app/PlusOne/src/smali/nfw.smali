.class public final Lnfw;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Lnyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1128
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1133
    const/4 v0, 0x0

    iput-object v0, p0, Lnfw;->b:Lnyq;

    .line 1128
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1148
    const/4 v0, 0x1

    iget-object v1, p0, Lnfw;->a:Ljava/lang/Boolean;

    .line 1150
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1151
    iget-object v1, p0, Lnfw;->b:Lnyq;

    if-eqz v1, :cond_0

    .line 1152
    const/4 v1, 0x2

    iget-object v2, p0, Lnfw;->b:Lnyq;

    .line 1153
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1155
    :cond_0
    iget-object v1, p0, Lnfw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1156
    iput v0, p0, Lnfw;->ai:I

    .line 1157
    return v0
.end method

.method public a(Loxn;)Lnfw;
    .locals 2

    .prologue
    .line 1165
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1166
    sparse-switch v0, :sswitch_data_0

    .line 1170
    iget-object v1, p0, Lnfw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1171
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnfw;->ah:Ljava/util/List;

    .line 1174
    :cond_1
    iget-object v1, p0, Lnfw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1176
    :sswitch_0
    return-object p0

    .line 1181
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnfw;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 1185
    :sswitch_2
    iget-object v0, p0, Lnfw;->b:Lnyq;

    if-nez v0, :cond_2

    .line 1186
    new-instance v0, Lnyq;

    invoke-direct {v0}, Lnyq;-><init>()V

    iput-object v0, p0, Lnfw;->b:Lnyq;

    .line 1188
    :cond_2
    iget-object v0, p0, Lnfw;->b:Lnyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1166
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1138
    const/4 v0, 0x1

    iget-object v1, p0, Lnfw;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1139
    iget-object v0, p0, Lnfw;->b:Lnyq;

    if-eqz v0, :cond_0

    .line 1140
    const/4 v0, 0x2

    iget-object v1, p0, Lnfw;->b:Lnyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1142
    :cond_0
    iget-object v0, p0, Lnfw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1144
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1124
    invoke-virtual {p0, p1}, Lnfw;->a(Loxn;)Lnfw;

    move-result-object v0

    return-object v0
.end method
