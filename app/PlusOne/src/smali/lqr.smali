.class public Llqr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final d:Landroid/os/Bundle;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llrg;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llqw;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Llqw;

.field private f:Llqw;

.field private g:Llqw;

.field private h:Llqw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Llqr;->d:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llqr;->a:Ljava/util/List;

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llqr;->b:Ljava/util/List;

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Llqr;->c:Ljava/util/HashSet;

    .line 73
    return-void
.end method

.method static synthetic a(Llqr;Llrg;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Llqr;->b(Llrg;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Llrg;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    instance-of v1, p1, Llrd;

    if-eqz v1, :cond_0

    .line 216
    instance-of v0, p1, Llrh;

    if-eqz v0, :cond_1

    .line 217
    check-cast p1, Llrh;

    invoke-interface {p1}, Llrh;->b()Ljava/lang/String;

    move-result-object v0

    .line 222
    :cond_0
    :goto_0
    return-object v0

    .line 219
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Llrg;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    .line 105
    if-eqz p2, :cond_0

    .line 106
    invoke-direct {p0, p1}, Llqr;->b(Llrg;)Ljava/lang/String;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_1

    .line 108
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 113
    :cond_0
    :goto_0
    return-object v0

    .line 110
    :cond_1
    sget-object v0, Llqr;->d:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public a(Llqw;)Llqw;
    .locals 2

    .prologue
    .line 82
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 83
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llrg;

    .line 84
    invoke-interface {p1, v0}, Llqw;->a(Llrg;)V

    .line 82
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 86
    :cond_0
    iget-object v0, p0, Llqr;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    return-object p1
.end method

.method public a(Llrg;)Llrg;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Llrg;",
            ">(TT;)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1}, Llqr;->b(Llrg;)Ljava/lang/String;

    move-result-object v1

    .line 53
    if-eqz v1, :cond_1

    .line 54
    iget-object v2, p0, Llqr;->c:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Duplicate observer tag: \'%s\'. Implement LifecycleObserverTag to provide unique tags."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 59
    :cond_0
    iget-object v2, p0, Llqr;->c:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_1
    iget-object v1, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v0

    .line 63
    :goto_0
    iget-object v0, p0, Llqr;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 64
    iget-object v0, p0, Llqr;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llqw;

    .line 65
    invoke-interface {v0, p1}, Llqw;->a(Llrg;)V

    .line 63
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 67
    :cond_2
    return-object p1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 197
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 198
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llrg;

    .line 199
    instance-of v2, v0, Llqy;

    if-eqz v2, :cond_0

    .line 200
    check-cast v0, Llqy;

    invoke-interface {v0, p1, p2, p3}, Llqy;->a(IILandroid/content/Intent;)V

    .line 197
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 203
    :cond_1
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Llqr;->g:Llqw;

    invoke-virtual {p0, v0}, Llqr;->b(Llqw;)V

    .line 152
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 153
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llrg;

    .line 154
    instance-of v2, v0, Llrb;

    if-eqz v2, :cond_0

    .line 155
    check-cast v0, Llrb;

    invoke-interface {v0}, Llrb;->c()V

    .line 152
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 158
    :cond_1
    return-void
.end method

.method public b(Llqw;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Llqr;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Llqr;->h:Llqw;

    invoke-virtual {p0, v0}, Llqr;->b(Llqw;)V

    .line 187
    iget-object v0, p0, Llqr;->e:Llqw;

    invoke-virtual {p0, v0}, Llqr;->b(Llqw;)V

    .line 188
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 189
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llrg;

    .line 190
    instance-of v2, v0, Llra;

    if-eqz v2, :cond_0

    .line 191
    check-cast v0, Llra;

    invoke-interface {v0}, Llra;->E_()V

    .line 188
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 194
    :cond_1
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 117
    new-instance v0, Llqs;

    invoke-direct {v0, p0, p1}, Llqs;-><init>(Llqr;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Llqr;->a(Llqw;)Llqw;

    move-result-object v0

    iput-object v0, p0, Llqr;->e:Llqw;

    .line 126
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 171
    new-instance v0, Llqv;

    invoke-direct {v0, p0, p1}, Llqv;-><init>(Llqr;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Llqr;->a(Llqw;)Llqw;

    move-result-object v0

    iput-object v0, p0, Llqr;->h:Llqw;

    .line 183
    return-void
.end method

.method public j()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Llqt;

    invoke-direct {v0}, Llqt;-><init>()V

    invoke-virtual {p0, v0}, Llqr;->a(Llqw;)Llqw;

    move-result-object v0

    iput-object v0, p0, Llqr;->f:Llqw;

    .line 137
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Llqu;

    invoke-direct {v0}, Llqu;-><init>()V

    invoke-virtual {p0, v0}, Llqr;->a(Llqw;)Llqw;

    move-result-object v0

    iput-object v0, p0, Llqr;->g:Llqw;

    .line 148
    return-void
.end method

.method public l()V
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Llqr;->f:Llqw;

    invoke-virtual {p0, v0}, Llqr;->b(Llqw;)V

    .line 162
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 163
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llrg;

    .line 164
    instance-of v2, v0, Llrf;

    if-eqz v2, :cond_0

    .line 165
    check-cast v0, Llrf;

    invoke-interface {v0}, Llrf;->aP_()V

    .line 162
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 168
    :cond_1
    return-void
.end method

.method public m()V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Llqr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 211
    :cond_0
    return-void
.end method
