.class public final Ldih;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Llyo;",
        "Llyp;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 40
    const-string v3, "allphotosview"

    new-instance v4, Llyo;

    invoke-direct {v4}, Llyo;-><init>()V

    new-instance v5, Llyp;

    invoke-direct {v5}, Llyp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 42
    iput-object p3, p0, Ldih;->a:Ljava/lang/String;

    .line 43
    return-void
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 121
    new-array v7, v10, [Landroid/net/Uri;

    sget-object v0, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v0, v7, v1

    sget-object v0, Lhsf;->a:Landroid/net/Uri;

    aput-object v0, v7, v3

    const/4 v0, 0x2

    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v2, v7, v0

    const/4 v0, 0x3

    sget-object v2, Lhsf;->b:Landroid/net/Uri;

    aput-object v2, v7, v0

    .line 127
    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 130
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move v6, v1

    .line 131
    :goto_0
    if-ge v6, v10, :cond_0

    .line 132
    aget-object v1, v7, v6

    const-string v3, "_data LIKE \'%/DCIM/%\'"

    const-string v5, "_id DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 136
    if-eqz v3, :cond_1

    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 137
    const/4 v1, 0x0

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 138
    aget-object v1, v7, v6

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 145
    if-eqz v3, :cond_0

    .line 146
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 150
    :cond_0
    return-object v4

    .line 145
    :cond_1
    if-eqz v3, :cond_2

    .line 146
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 131
    :cond_2
    :goto_1
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    .line 140
    :catch_0
    move-exception v1

    .line 143
    :try_start_1
    const-string v5, "allphotosview"

    const-string v8, "Could not load camera photo"

    invoke-static {v5, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    if-eqz v3, :cond_2

    .line 146
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 145
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_3

    .line 146
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method


# virtual methods
.method protected a(Llyo;)V
    .locals 3

    .prologue
    .line 47
    new-instance v0, Lndy;

    invoke-direct {v0}, Lndy;-><init>()V

    iput-object v0, p1, Llyo;->a:Lndy;

    .line 48
    iget-object v0, p1, Llyo;->a:Lndy;

    .line 49
    iget-object v1, p0, Ldih;->a:Ljava/lang/String;

    iput-object v1, v0, Lndy;->a:Ljava/lang/String;

    .line 50
    new-instance v1, Lneo;

    invoke-direct {v1}, Lneo;-><init>()V

    iput-object v1, v0, Lndy;->b:Lneo;

    .line 51
    iget-object v1, v0, Lndy;->b:Lneo;

    const/4 v2, 0x1

    iput v2, v1, Lneo;->a:I

    .line 52
    iget-object v0, v0, Lndy;->b:Lneo;

    const/4 v1, 0x2

    iput v1, v0, Lneo;->b:I

    .line 53
    return-void
.end method

.method protected a(Llyp;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 57
    iget-object v1, p1, Llyp;->a:Lnfd;

    .line 60
    iget-object v3, v1, Lnfd;->a:[Lnzx;

    .line 61
    iget-object v0, p0, Ldih;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Ldih;->f:Landroid/content/Context;

    new-instance v2, Lnzx;

    invoke-direct {v2}, Lnzx;-><init>()V

    const-string v4, "~camera"

    iput-object v4, v2, Lnzx;->b:Ljava/lang/String;

    const/16 v4, 0x64

    iput v4, v2, Lnzx;->k:I

    const-string v4, "camera roll"

    iput-object v4, v2, Lnzx;->c:Ljava/lang/String;

    new-instance v4, Lnyl;

    invoke-direct {v4}, Lnyl;-><init>()V

    iput-object v4, v2, Lnzx;->f:Lnyl;

    iget-object v4, v2, Lnzx;->f:Lnyl;

    invoke-static {v0}, Ldih;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lnyl;->b:Ljava/lang/String;

    if-nez v3, :cond_1

    new-array v0, v7, [Lnzx;

    aput-object v2, v0, v5

    :goto_0
    move-object v3, v0

    .line 65
    :cond_0
    new-array v0, v5, [Ljava/lang/String;

    invoke-static {v7, v0}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 67
    iget-object v0, p0, Ldih;->f:Landroid/content/Context;

    iget v4, p0, Ldih;->c:I

    invoke-static {v0, v4, v2}, Ljvj;->c(Landroid/content/Context;ILjava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_2

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 69
    :goto_1
    iget-object v4, p0, Ldih;->a:Ljava/lang/String;

    if-eqz v4, :cond_3

    if-eqz v0, :cond_3

    iget-object v4, p0, Ldih;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 74
    iget-object v0, p0, Ldih;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 75
    invoke-static {v2}, Ljvj;->q(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 83
    :goto_2
    return-void

    .line 62
    :cond_1
    array-length v0, v3

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Lnzx;

    array-length v4, v3

    invoke-static {v3, v5, v0, v7, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-object v2, v0, v5

    goto :goto_0

    :cond_2
    move-object v0, v6

    .line 68
    goto :goto_1

    .line 79
    :cond_3
    iget-object v4, p0, Ldih;->f:Landroid/content/Context;

    iget v8, p0, Ldih;->c:I

    iget-object v1, v1, Lnfd;->b:Ljava/lang/String;

    iget-object v0, p0, Ldih;->a:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v7

    :goto_3
    invoke-static {v4, v8, v2, v1, v0}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 81
    iget-object v0, p0, Ldih;->f:Landroid/content/Context;

    iget v1, p0, Ldih;->c:I

    iget-object v4, p0, Ldih;->a:Ljava/lang/String;

    if-nez v4, :cond_5

    move v4, v7

    :goto_4
    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    goto :goto_2

    :cond_4
    move v0, v5

    .line 79
    goto :goto_3

    :cond_5
    move v4, v5

    .line 81
    goto :goto_4
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Llyo;

    invoke-virtual {p0, p1}, Ldih;->a(Llyo;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Llyp;

    invoke-virtual {p0, p1}, Ldih;->a(Llyp;)V

    return-void
.end method
