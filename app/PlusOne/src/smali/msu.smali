.class public final Lmsu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmsu;


# instance fields
.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2580
    const/4 v0, 0x0

    new-array v0, v0, [Lmsu;

    sput-object v0, Lmsu;->a:[Lmsu;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2581
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 2607
    const/4 v0, 0x0

    .line 2608
    iget-object v1, p0, Lmsu;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2609
    const/4 v0, 0x1

    iget-object v1, p0, Lmsu;->b:Ljava/lang/Integer;

    .line 2610
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2612
    :cond_0
    iget-object v1, p0, Lmsu;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2613
    const/4 v1, 0x2

    iget-object v2, p0, Lmsu;->c:Ljava/lang/String;

    .line 2614
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2616
    :cond_1
    iget-object v1, p0, Lmsu;->d:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2617
    const/4 v1, 0x3

    iget-object v2, p0, Lmsu;->d:Ljava/lang/Long;

    .line 2618
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2620
    :cond_2
    iget-object v1, p0, Lmsu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2621
    iput v0, p0, Lmsu;->ai:I

    .line 2622
    return v0
.end method

.method public a(Loxn;)Lmsu;
    .locals 2

    .prologue
    .line 2630
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2631
    sparse-switch v0, :sswitch_data_0

    .line 2635
    iget-object v1, p0, Lmsu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2636
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmsu;->ah:Ljava/util/List;

    .line 2639
    :cond_1
    iget-object v1, p0, Lmsu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2641
    :sswitch_0
    return-object p0

    .line 2646
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmsu;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2650
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmsu;->c:Ljava/lang/String;

    goto :goto_0

    .line 2654
    :sswitch_3
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmsu;->d:Ljava/lang/Long;

    goto :goto_0

    .line 2631
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 2592
    iget-object v0, p0, Lmsu;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2593
    const/4 v0, 0x1

    iget-object v1, p0, Lmsu;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2595
    :cond_0
    iget-object v0, p0, Lmsu;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2596
    const/4 v0, 0x2

    iget-object v1, p0, Lmsu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2598
    :cond_1
    iget-object v0, p0, Lmsu;->d:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2599
    const/4 v0, 0x3

    iget-object v1, p0, Lmsu;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 2601
    :cond_2
    iget-object v0, p0, Lmsu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2603
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2577
    invoke-virtual {p0, p1}, Lmsu;->a(Loxn;)Lmsu;

    move-result-object v0

    return-object v0
.end method
