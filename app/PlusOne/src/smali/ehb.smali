.class public final Lehb;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lbc;
.implements Leij;
.implements Lfuf;
.implements Lfvw;
.implements Lgba;
.implements Lgbb;
.implements Llix;
.implements Lljp;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnLongClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Leij;",
        "Lfuf;",
        "Lfvw;",
        "Lgba;",
        "Lgbb;",
        "Llix;",
        "Lljp;"
    }
.end annotation


# static fields
.field private static N:Z

.field private static ah:I

.field private static ai:I

.field private static aj:I

.field private static ak:I

.field private static al:Landroid/view/animation/Interpolator;


# instance fields
.field private aA:Ljava/lang/String;

.field private aB:Ljava/lang/String;

.field private aC:Ljava/lang/String;

.field private aD:I

.field private aE:Lhgw;

.field private aF:Lhgw;

.field private aG:Z

.field private aH:Z

.field private aI:Ljava/lang/Boolean;

.field private aJ:I

.field private aK:Ljava/lang/String;

.field private aL:Ljava/lang/String;

.field private aM:Ljava/lang/String;

.field private aN:Ljava/lang/String;

.field private aO:Ljava/lang/String;

.field private aP:Ljava/lang/String;

.field private aQ:J

.field private aR:J

.field private aS:Ljava/lang/String;

.field private aT:Z

.field private aU:Z

.field private aV:Landroid/animation/Animator$AnimatorListener;

.field private final aW:Licq;

.field private final aX:Lhob;

.field private final aY:Lfhh;

.field private am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

.field private ao:Landroid/view/View;

.field private ap:Landroid/view/View;

.field private aq:Landroid/view/View;

.field private ar:Lfdb;

.field private as:Z

.field private aw:Z

.field private ax:Z

.field private ay:Ljava/lang/Integer;

.field private az:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 167
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lehb;->al:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 116
    invoke-direct {p0}, Legi;-><init>()V

    .line 196
    const/4 v0, 0x0

    iput v0, p0, Lehb;->aJ:I

    .line 212
    new-instance v0, Licq;

    iget-object v1, p0, Lehb;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064c

    .line 213
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lehb;->aW:Licq;

    .line 215
    new-instance v0, Lehc;

    invoke-direct {v0, p0}, Lehc;-><init>(Lehb;)V

    iput-object v0, p0, Lehb;->aX:Lhob;

    .line 233
    new-instance v0, Lehd;

    invoke-direct {v0, p0}, Lehd;-><init>(Lehb;)V

    iput-object v0, p0, Lehb;->aY:Lfhh;

    .line 1462
    return-void
.end method

.method static synthetic a(Lehb;Lhgw;)Lhgw;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lehb;->aE:Lhgw;

    return-object p1
.end method

.method private a(ILfib;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1160
    iget-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 1182
    :cond_0
    :goto_0
    return-void

    .line 1163
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    .line 1165
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1166
    invoke-virtual {p0}, Lehb;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 1168
    invoke-virtual {p2}, Lfib;->c()I

    move-result v1

    const/16 v2, 0x194

    if-ne v1, v2, :cond_2

    .line 1170
    iput-boolean v3, p0, Lehb;->ax:Z

    .line 1181
    :goto_1
    invoke-virtual {p0}, Lehb;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lehb;->d(Landroid/view/View;)V

    goto :goto_0

    .line 1172
    :cond_2
    iput-boolean v3, p0, Lehb;->Z:Z

    .line 1173
    const v1, 0x7f0a07ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1174
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1178
    :cond_3
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->h()V

    goto :goto_1
.end method

.method static synthetic a(Lehb;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lehb;->aC()V

    return-void
.end method

.method static synthetic a(Lehb;ILfib;)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Lehb;->a(ILfib;)V

    return-void
.end method

.method static synthetic a(Lehb;Z)Z
    .locals 0

    .prologue
    .line 116
    iput-boolean p1, p0, Lehb;->aH:Z

    return p1
.end method

.method private aA()V
    .locals 4

    .prologue
    .line 1122
    invoke-direct {p0}, Lehb;->aB()Lehu;

    move-result-object v0

    .line 1123
    if-nez v0, :cond_0

    .line 1124
    new-instance v0, Lehu;

    invoke-direct {v0}, Lehu;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account_id"

    iget-object v3, p0, Lehb;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "activity_id"

    iget-object v3, p0, Lehb;->aS:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "host_mode"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "force_full_bleed"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "ActionBarFragmentMixin.Enabled"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lehu;->f(Landroid/os/Bundle;)V

    .line 1125
    invoke-virtual {v0, p0}, Lehu;->a(Leij;)V

    .line 1127
    invoke-virtual {p0}, Lehb;->q()Lae;

    move-result-object v1

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    .line 1128
    const v2, 0x7f100323

    const-string v3, "activity_1_up_fragment"

    invoke-virtual {v1, v2, v0, v3}, Lat;->a(ILu;Ljava/lang/String;)Lat;

    .line 1130
    invoke-virtual {v1}, Lat;->b()I

    .line 1137
    :goto_0
    return-void

    .line 1132
    :cond_0
    invoke-virtual {v0, p0}, Lehu;->a(Leij;)V

    goto :goto_0
.end method

.method private aB()Lehu;
    .locals 2

    .prologue
    .line 1140
    invoke-virtual {p0}, Lehb;->q()Lae;

    move-result-object v0

    .line 1141
    const-string v1, "activity_1_up_fragment"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lehu;

    return-object v0
.end method

.method private aC()V
    .locals 7

    .prologue
    .line 1386
    iget-object v0, p0, Lehb;->aF:Lhgw;

    if-nez v0, :cond_1

    .line 1388
    iget-object v0, p0, Lehb;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lhoc;

    .line 1389
    const-string v0, "ReadCollectionAudienceTask"

    invoke-virtual {v6, v0}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1390
    new-instance v0, Ldps;

    .line 1391
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Lehb;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehb;->aN:Ljava/lang/String;

    iget-object v4, p0, Lehb;->aA:Ljava/lang/String;

    invoke-direct {p0}, Lehb;->ax()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Ldps;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 1392
    invoke-virtual {v6, v0}, Lhoc;->c(Lhny;)V

    .line 1403
    :cond_0
    :goto_0
    return-void

    .line 1395
    :cond_1
    invoke-direct {p0}, Lehb;->ax()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lfvc;->l:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1397
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lehb;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lehb;->aN:Ljava/lang/String;

    iget-object v3, p0, Lehb;->aA:Ljava/lang/String;

    iget-object v4, p0, Lehb;->aE:Lhgw;

    .line 1396
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/apps/plus/phone/AlbumShareActivity;

    invoke-direct {v5, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account_id"

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "auth_key"

    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "cluster_id"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "extra_acl"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1398
    const/4 v0, 0x2

    invoke-virtual {p0, v5, v0}, Lehb;->a(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1400
    :cond_2
    invoke-direct {p0}, Lehb;->au()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0a09a2

    invoke-virtual {p0, v0}, Lehb;->e_(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lehb;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "SingleAlbum"

    iget-object v3, p0, Lehb;->aF:Lhgw;

    invoke-static {v1, v2, v0, v3}, Lfuk;->a(ILjava/lang/String;Ljava/lang/String;Lhgw;)Lekr;

    move-result-object v0

    invoke-virtual {v0}, Lekr;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "restrict_to_domain"

    iget-boolean v3, p0, Lehb;->aG:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lehb;->p()Lae;

    move-result-object v1

    const-string v2, "tag_audience"

    invoke-virtual {v0, v1, v2}, Lekr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lehb;->ar:Lfdb;

    invoke-virtual {v0}, Lfdb;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private au()Z
    .locals 2

    .prologue
    .line 856
    const-string v0, "PLUS_EVENT"

    iget-object v1, p0, Lehb;->aK:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private av()Z
    .locals 4

    .prologue
    .line 860
    iget-wide v0, p0, Lehb;->aR:J

    const-wide/16 v2, 0x800

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aw()Z
    .locals 4

    .prologue
    .line 864
    iget-wide v0, p0, Lehb;->aR:J

    const-wide/16 v2, 0x2

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ax()Z
    .locals 3

    .prologue
    .line 868
    iget-object v0, p0, Lehb;->aL:Ljava/lang/String;

    iget-object v1, p0, Lehb;->P:Lhee;

    invoke-interface {v1}, Lhee;->g()Lhej;

    move-result-object v1

    const-string v2, "gaia_id"

    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private ay()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1027
    invoke-virtual {p0}, Lehb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "cluster_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private az()Z
    .locals 2

    .prologue
    .line 1053
    iget-object v0, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1054
    iget-object v0, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->c()V

    .line 1055
    iget-object v0, p0, Lehb;->ap:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Llsj;->a(Landroid/view/View;F)V

    .line 1056
    const/4 v0, 0x1

    .line 1058
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lehb;Lhgw;)Lhgw;
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lehb;->aF:Lhgw;

    return-object p1
.end method

.method static synthetic b(Lehb;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lehb;->aI:Ljava/lang/Boolean;

    return-object v0
.end method

.method private b(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 1407
    iget-object v0, p0, Lehb;->P:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "domain_name"

    .line 1408
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1410
    new-instance v1, Lfvx;

    iget-object v2, p0, Lehb;->at:Llnl;

    invoke-direct {v1, v2}, Lfvx;-><init>(Landroid/content/Context;)V

    .line 1411
    iget v2, p0, Lehb;->aD:I

    invoke-virtual {v1, v2, v0}, Lfvx;->a(ILjava/lang/String;)V

    .line 1415
    iget v0, p2, Landroid/graphics/Rect;->left:I

    .line 1416
    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1417
    invoke-virtual {v1, p1, v0, v2}, Lfvx;->a(Landroid/view/View;II)V

    .line 1418
    return-void
.end method

.method private b(ILfib;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1185
    iget-object v2, p0, Lehb;->az:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lehb;->az:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_2

    :cond_0
    move v0, v1

    .line 1222
    :cond_1
    :goto_0
    return v0

    .line 1188
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lehb;->az:Ljava/lang/Integer;

    .line 1189
    iget-object v2, p0, Lehb;->T:Lcmz;

    invoke-virtual {v2}, Lcmz;->a()V

    .line 1191
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1193
    iget v0, p0, Lehb;->aJ:I

    packed-switch v0, :pswitch_data_0

    .line 1212
    const v0, 0x7f0a058e

    .line 1215
    :goto_1
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 1216
    goto :goto_0

    .line 1195
    :pswitch_0
    invoke-direct {p0}, Lehb;->aw()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lehb;->aU:Z

    if-nez v0, :cond_3

    const v0, 0x7f0a07a9

    goto :goto_1

    :cond_3
    const v0, 0x7f0a07a8

    goto :goto_1

    .line 1200
    :pswitch_1
    const v0, 0x7f0a07ab

    .line 1201
    goto :goto_1

    .line 1204
    :pswitch_2
    const v0, 0x7f0a0654

    .line 1205
    goto :goto_1

    .line 1208
    :pswitch_3
    const v0, 0x7f0a0653

    .line 1209
    goto :goto_1

    .line 1218
    :cond_4
    iget v1, p0, Lehb;->aJ:I

    if-ne v1, v0, :cond_1

    .line 1219
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->finish()V

    goto :goto_0

    .line 1193
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lehb;ILfib;)Z
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Lehb;->b(ILfib;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lehb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lehb;->aq:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lehb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1062
    if-nez p1, :cond_0

    .line 1078
    :goto_0
    return-void

    .line 1066
    :cond_0
    invoke-virtual {p0}, Lehb;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1067
    iget-boolean v0, p0, Lehb;->as:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lehb;->aw:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 1068
    iget-object v0, p0, Lehb;->aW:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 1075
    :goto_1
    invoke-virtual {p0, p1}, Lehb;->e(Landroid/view/View;)V

    .line 1076
    invoke-virtual {p0}, Lehb;->Z_()V

    .line 1077
    invoke-virtual {p0}, Lehb;->ag()V

    goto :goto_0

    .line 1070
    :cond_1
    iget-object v0, p0, Lehb;->aW:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 1073
    :cond_2
    iget-object v0, p0, Lehb;->aW:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1
.end method

.method static synthetic e(Lehb;)Llnl;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lehb;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 707
    sget-object v0, Lhmw;->ag:Lhmw;

    return-object v0
.end method

.method public I_()V
    .locals 1

    .prologue
    .line 1020
    invoke-super {p0}, Legi;->I_()V

    .line 1021
    iget-object v0, p0, Lehb;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1022
    invoke-direct {p0}, Lehb;->az()Z

    .line 1024
    :cond_0
    return-void
.end method

.method public J_()Z
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 712
    invoke-super {p0}, Legi;->L_()V

    .line 715
    iput-boolean v5, p0, Lehb;->ax:Z

    .line 716
    iput-boolean v5, p0, Lehb;->Z:Z

    .line 719
    iput-object v4, p0, Lehb;->aE:Lhgw;

    .line 720
    iput-object v4, p0, Lehb;->aF:Lhgw;

    .line 722
    iget-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lehb;->at:Llnl;

    iget-object v2, p0, Lehb;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehb;->aL:Ljava/lang/String;

    iget-object v6, p0, Lehb;->aA:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0, v6}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    .line 723
    :cond_0
    iget-object v0, p0, Lehb;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lhoc;

    iget-object v0, p0, Lehb;->aS:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "GetActivityTask"

    invoke-virtual {v6, v0}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ldox;

    iget-object v1, p0, Lehb;->at:Llnl;

    iget-object v2, p0, Lehb;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehb;->aS:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Ldox;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v6, v0}, Lhoc;->b(Lhny;)V

    .line 724
    :cond_1
    invoke-virtual {p0}, Lehb;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lehb;->d(Landroid/view/View;)V

    .line 725
    iget-object v0, p0, Lehb;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lehb;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ek:Lhmv;

    .line 726
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 725
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 727
    return-void
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lehb;->ar:Lfdb;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 697
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 696
    :cond_1
    iget-object v0, p0, Lehb;->ar:Lfdb;

    invoke-virtual {v0}, Lfdb;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 697
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 944
    invoke-direct {p0}, Lehb;->az()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 945
    const/4 v0, 0x1

    .line 949
    :goto_0
    return v0

    .line 946
    :cond_0
    iget-boolean v0, p0, Lehb;->ac:Z

    if-eqz v0, :cond_1

    .line 947
    invoke-super {p0}, Legi;->a()Z

    move-result v0

    goto :goto_0

    .line 949
    :cond_1
    invoke-super {p0}, Legi;->V()Z

    move-result v0

    goto :goto_0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 1486
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->l()Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    .line 320
    iget-object v0, p0, Lehb;->at:Llnl;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400e0

    invoke-super {p0, v0, p2, p3, v1}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v9

    .line 322
    iget-object v0, p0, Lehb;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0291

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    .line 324
    new-instance v0, Ljvl;

    iget-object v1, p0, Lehb;->at:Llnl;

    invoke-direct {v0, v1}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v5, v0, Ljvl;->a:I

    .line 325
    invoke-virtual {p0}, Lehb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "hide_header"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    move v8, v0

    .line 327
    :goto_0
    new-instance v0, Lfdb;

    iget-object v1, p0, Lehb;->at:Llnl;

    const/4 v2, 0x0

    iget-object v3, p0, Lehb;->P:Lhee;

    .line 328
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lehb;->aA:Ljava/lang/String;

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lfdb;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;ILjava/lang/String;Lfvw;)V

    iput-object v0, p0, Lehb;->ar:Lfdb;

    .line 330
    iget-object v0, p0, Lehb;->ar:Lfdb;

    invoke-virtual {v0, p0}, Lfdb;->a(Landroid/view/View$OnClickListener;)V

    .line 331
    iget-object v0, p0, Lehb;->ar:Lfdb;

    invoke-virtual {v0, p0}, Lfdb;->a(Landroid/view/View$OnLongClickListener;)V

    .line 333
    const v0, 0x7f100304

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 334
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Z)V

    .line 335
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v10}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 336
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 337
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(Z)V

    .line 338
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    new-instance v1, Lehg;

    invoke-direct {v1}, Lehg;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 339
    iget-object v1, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    if-eqz v8, :cond_7

    iget-object v0, p0, Lehb;->ar:Lfdb;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 341
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const v1, 0x7f020415

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 343
    const v0, 0x7f10015f

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lehb;->ao:Landroid/view/View;

    .line 344
    const v0, 0x7f100322

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lehb;->ap:Landroid/view/View;

    .line 346
    const v0, 0x7f10015e

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    iput-object v0, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    .line 347
    iget-object v0, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    sget v1, Lehb;->ak:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a(I)V

    .line 348
    iget-object v0, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a(Lljp;)V

    .line 349
    iget-object v0, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a(Llix;)V

    .line 351
    const v0, 0x7f10005b

    .line 352
    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;

    .line 354
    const v1, 0x7f100324

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lehb;->aq:Landroid/view/View;

    .line 355
    const v1, 0x7f100325

    .line 356
    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;

    .line 357
    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->a(Lgba;)V

    .line 359
    const v1, 0x7f1002a9

    .line 360
    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SingleAlbumTouchHandler;

    .line 362
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/SingleAlbumTouchHandler;->a(Landroid/view/View;)V

    .line 363
    iget-object v0, p0, Lehb;->ao:Landroid/view/View;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/SingleAlbumTouchHandler;->b(Landroid/view/View;)V

    .line 364
    const v0, 0x7f1001eb

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/SingleAlbumTouchHandler;->c(Landroid/view/View;)V

    .line 365
    const v0, 0x7f10060f

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/SingleAlbumTouchHandler;->d(Landroid/view/View;)V

    .line 366
    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/SingleAlbumTouchHandler;->a(Lgbb;)V

    .line 368
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    .line 369
    iget-object v0, p0, Lehb;->ap:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 372
    :cond_0
    iget-object v0, p0, Lehb;->aS:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 373
    invoke-direct {p0}, Lehb;->aA()V

    .line 376
    :cond_1
    invoke-virtual {p0}, Lehb;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    iget-object v0, p0, Lehb;->aW:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 381
    :cond_2
    invoke-virtual {p0}, Lehb;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 385
    const/4 v0, 0x0

    iput-boolean v0, p0, Lehb;->aH:Z

    .line 387
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    .line 388
    iget-object v1, p0, Lehb;->P:Lhee;

    invoke-interface {v1}, Lhee;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lehb;->P:Lhee;

    .line 389
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljfb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 390
    invoke-virtual {p0}, Lehb;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    new-instance v3, Lehf;

    invoke-direct {v3, p0}, Lehf;-><init>(Lehb;)V

    invoke-virtual {v0, v1, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 394
    :cond_3
    return-object v9

    .line 325
    :cond_4
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "profile"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "posts"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x1

    move v8, v0

    goto/16 :goto_0

    .line 339
    :cond_7
    new-instance v0, Levw;

    iget-object v2, p0, Lehb;->ar:Lfdb;

    invoke-direct {v0, v2}, Levw;-><init>(Landroid/widget/ListAdapter;)V

    goto/16 :goto_1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 731
    packed-switch p1, :pswitch_data_0

    .line 740
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 733
    :pswitch_0
    invoke-virtual {p0}, Lehb;->al()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lehb;->at:Llnl;

    iget-object v1, p0, Lehb;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 735
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lehb;->aa:I

    .line 734
    invoke-static {v0, v1, v2, v3}, Lfdd;->a(Landroid/content/Context;ILjava/lang/String;I)Lfdd;

    move-result-object v0

    goto :goto_0

    .line 737
    :cond_0
    new-instance v0, Lfdd;

    iget-object v1, p0, Lehb;->at:Llnl;

    iget-object v2, p0, Lehb;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lfdd;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 731
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(I)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1f4

    const/4 v2, 0x0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 918
    iget-object v1, p0, Lehb;->aI:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lehb;->aI:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_0
    sget v1, Lehb;->ai:I

    if-le p1, v1, :cond_4

    .line 920
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lehb;->aI:Ljava/lang/Boolean;

    .line 921
    invoke-static {}, Llsj;->c()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lehb;->aq:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 928
    :cond_1
    :goto_0
    iget-object v1, p0, Lehb;->ap:Landroid/view/View;

    sget v2, Lehb;->ah:I

    if-le p1, v2, :cond_6

    :goto_1
    invoke-static {v1, v0}, Llsj;->a(Landroid/view/View;F)V

    .line 930
    return-void

    .line 921
    :cond_2
    iget-object v1, p0, Lehb;->aV:Landroid/animation/Animator$AnimatorListener;

    if-nez v1, :cond_3

    new-instance v1, Lehe;

    invoke-direct {v1, p0}, Lehe;-><init>(Lehb;)V

    iput-object v1, p0, Lehb;->aV:Landroid/animation/Animator$AnimatorListener;

    :cond_3
    iget-object v1, p0, Lehb;->aq:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    sget-object v2, Lehb;->al:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-object v2, p0, Lehb;->aV:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-static {}, Llsj;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 922
    :cond_4
    iget-object v1, p0, Lehb;->aI:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lehb;->aI:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_5
    sget v1, Lehb;->aj:I

    if-ge p1, v1, :cond_1

    .line 924
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lehb;->aI:Ljava/lang/Boolean;

    .line 925
    iget-object v1, p0, Lehb;->aq:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-static {}, Llsj;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lehb;->aq:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    sget-object v2, Lehb;->al:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-static {}, Llsj;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 928
    :cond_6
    int-to-float v0, p1

    sget v2, Lehb;->ah:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    goto :goto_1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 984
    packed-switch p1, :pswitch_data_0

    .line 1012
    invoke-super {p0, p1, p2, p3}, Legi;->a(IILandroid/content/Intent;)V

    .line 1016
    :cond_0
    :goto_0
    return-void

    .line 986
    :pswitch_0
    if-ne p2, v0, :cond_0

    .line 987
    const-string v0, "tile_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 988
    const/4 v1, 0x2

    iput v1, p0, Lehb;->aJ:I

    .line 989
    iget-object v1, p0, Lehb;->at:Llnl;

    iget-object v2, p0, Lehb;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lehb;->aO:Ljava/lang/String;

    .line 990
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v4

    .line 989
    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehb;->az:Ljava/lang/Integer;

    .line 992
    iget-object v0, p0, Lehb;->T:Lcmz;

    const v1, 0x7f0a08ef

    invoke-virtual {p0, v1}, Lehb;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcmz;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 998
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 999
    invoke-virtual {p0}, Lehb;->L_()V

    goto :goto_0

    .line 1005
    :pswitch_2
    if-ne p2, v0, :cond_0

    .line 1006
    invoke-virtual {p0}, Lehb;->L_()V

    goto :goto_0

    .line 984
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 963
    .line 968
    invoke-virtual {p0}, Lehb;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100325

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;

    .line 969
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->b(Landroid/database/Cursor;)V

    .line 971
    const/4 v0, 0x1

    iput-boolean v0, p0, Lehb;->aT:Z

    .line 972
    invoke-virtual {p0}, Lehb;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lehb;->e(Landroid/view/View;)V

    .line 973
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 272
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 273
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    sget-boolean v1, Lehb;->N:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lehb;->ah:I

    const v1, 0x7f0d02f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lehb;->ai:I

    const v1, 0x7f0d02f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lehb;->aj:I

    const v1, 0x7f0d029e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lehb;->ak:I

    const/4 v0, 0x1

    sput-boolean v0, Lehb;->N:Z

    .line 275
    :cond_0
    if-eqz p1, :cond_6

    .line 276
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    .line 279
    :cond_1
    const-string v0, "pending_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 280
    const-string v0, "pending_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehb;->az:Ljava/lang/Integer;

    .line 283
    :cond_2
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 284
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Lehb;->aE:Lhgw;

    .line 286
    :cond_3
    const-string v0, "people_list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 287
    const-string v0, "people_list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Lehb;->aF:Lhgw;

    .line 290
    :cond_4
    const-string v0, "drive_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 291
    const-string v0, "drive_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aB:Ljava/lang/String;

    .line 294
    :cond_5
    const-string v0, "operation_type"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lehb;->aJ:I

    .line 295
    const-string v0, "album_tile_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aO:Ljava/lang/String;

    .line 296
    const-string v0, "album_not_found"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lehb;->ax:Z

    .line 299
    :cond_6
    invoke-virtual {p0}, Lehb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aS:Ljava/lang/String;

    .line 300
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v1

    .line 301
    invoke-static {v1}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aL:Ljava/lang/String;

    .line 302
    invoke-static {v1}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aM:Ljava/lang/String;

    .line 303
    invoke-static {v1}, Ljvj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aK:Ljava/lang/String;

    .line 305
    invoke-virtual {p0}, Lehb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "auth_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 306
    invoke-virtual {p0}, Lehb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "auth_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aA:Ljava/lang/String;

    .line 309
    :cond_7
    iget-object v0, p0, Lehb;->at:Llnl;

    const-class v2, Lfur;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfur;

    .line 310
    invoke-virtual {v0}, Lfur;->b()Z

    move-result v0

    iput-boolean v0, p0, Lehb;->aU:Z

    .line 312
    const-string v0, "SingleAlbum"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 313
    const-string v0, "onCreate clusterId="

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 315
    :cond_8
    :goto_0
    return-void

    .line 313
    :cond_9
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 954
    const-string v0, "delete_album"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 955
    const/4 v0, 0x1

    iput v0, p0, Lehb;->aJ:I

    iget-object v0, p0, Lehb;->at:Llnl;

    iget-object v1, p0, Lehb;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lehb;->aA:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->d(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehb;->az:Ljava/lang/Integer;

    iget-object v1, p0, Lehb;->T:Lcmz;

    invoke-direct {p0}, Lehb;->aw()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lehb;->aU:Z

    if-nez v0, :cond_0

    const v0, 0x7f0a0688

    :goto_0
    invoke-virtual {p0, v0}, Lehb;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcmz;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lehb;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lehb;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ed:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 959
    :goto_1
    return-void

    .line 955
    :cond_0
    const v0, 0x7f0a0681

    goto :goto_0

    .line 957
    :cond_1
    invoke-super {p0, p1, p2}, Legi;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1364
    invoke-direct {p0}, Lehb;->au()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1365
    invoke-direct {p0}, Lehb;->ax()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1366
    iget-object v0, p0, Lehb;->at:Llnl;

    iget-object v1, p0, Lehb;->P:Lhee;

    .line 1367
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lehb;->aM:Ljava/lang/String;

    iget-object v3, p0, Lehb;->aA:Ljava/lang/String;

    iget-object v4, p0, Lehb;->aL:Ljava/lang/String;

    .line 1366
    invoke-static {v0, v1, v2, v3, v4}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lehb;->a(Landroid/content/Intent;)V

    .line 1377
    :goto_0
    return-void

    .line 1370
    :cond_0
    invoke-direct {p0, p1, p2}, Lehb;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0

    .line 1372
    :cond_1
    iget v1, p0, Lehb;->aD:I

    if-ne v1, v0, :cond_3

    :goto_1
    if-nez v0, :cond_2

    invoke-direct {p0}, Lehb;->ax()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lfvc;->l:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1373
    :cond_2
    invoke-direct {p0}, Lehb;->aC()V

    goto :goto_0

    .line 1372
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1375
    :cond_4
    invoke-direct {p0, p1, p2}, Lehb;->b(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 843
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v5, 0xe

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v8, 0x0

    const/4 v1, 0x1

    .line 746
    if-eqz p2, :cond_0

    .line 747
    iget-object v0, p0, Lehb;->ar:Lfdb;

    invoke-virtual {v0}, Lfdb;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 748
    if-eqz v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 749
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 753
    :cond_0
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 833
    :goto_0
    return-void

    .line 755
    :pswitch_0
    iput-boolean v1, p0, Lehb;->as:Z

    .line 756
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_3

    .line 757
    :cond_1
    iget-boolean v0, p0, Lehb;->ax:Z

    if-nez v0, :cond_2

    .line 758
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v0

    .line 759
    new-array v2, v1, [Ljava/lang/String;

    .line 760
    invoke-static {v0}, Ljvj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v8

    .line 759
    invoke-static {v3, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 761
    iput-boolean v1, p0, Lehb;->aw:Z

    .line 762
    new-instance v1, Lfue;

    iget-object v2, p0, Lehb;->at:Llnl;

    iget-object v0, p0, Lehb;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    const-wide/16 v6, -0x1

    move-object v4, p0

    invoke-direct/range {v1 .. v7}, Lfue;-><init>(Landroid/content/Context;ILfuf;Ljava/lang/String;J)V

    new-array v0, v8, [Ljava/lang/Void;

    .line 763
    invoke-virtual {v1, v0}, Lfue;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 830
    :cond_2
    :goto_1
    iget-object v0, p0, Lehb;->ar:Lfdb;

    invoke-virtual {v0, p2}, Lfdb;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 831
    invoke-virtual {p0}, Lehb;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lehb;->d(Landroid/view/View;)V

    .line 832
    iget-object v0, p0, Lehb;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto :goto_0

    .line 765
    :cond_3
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v1, :cond_4

    invoke-direct {p0}, Lehb;->au()Z

    move-result v0

    if-nez v0, :cond_4

    .line 769
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    goto :goto_0

    .line 772
    :cond_4
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aO:Ljava/lang/String;

    .line 773
    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aP:Ljava/lang/String;

    .line 774
    invoke-virtual {p0}, Lehb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "show_title"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 775
    iget-object v0, p0, Lehb;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 778
    :cond_5
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aN:Ljava/lang/String;

    .line 783
    iget-object v0, p0, Lehb;->aL:Ljava/lang/String;

    if-nez v0, :cond_6

    iget-object v0, p0, Lehb;->aN:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 784
    iget-object v0, p0, Lehb;->aN:Ljava/lang/String;

    invoke-static {v0}, Ljvj;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aL:Ljava/lang/String;

    .line 787
    :cond_6
    const/16 v0, 0xc

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lehb;->aQ:J

    .line 788
    const/16 v0, 0xd

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lehb;->aR:J

    .line 789
    const/16 v0, 0xb

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lehb;->aD:I

    .line 791
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 792
    invoke-interface {p2, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 794
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 793
    invoke-static {v0}, Ljux;->a([B)Ljux;

    move-result-object v2

    .line 795
    invoke-virtual {v2}, Ljux;->c()Lnzx;

    move-result-object v0

    sget-object v3, Lnzr;->a:Loxr;

    .line 796
    invoke-virtual {v0, v3}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    iget-object v0, v0, Lnzr;->b:Lnxr;

    .line 798
    if-eqz v0, :cond_7

    .line 799
    iget-object v3, v0, Lnxr;->k:Ljava/lang/String;

    iput-object v3, p0, Lehb;->aC:Ljava/lang/String;

    .line 800
    iget-object v3, v0, Lnxr;->l:Lnxl;

    if-eqz v3, :cond_a

    iget-object v0, v0, Lnxr;->l:Lnxl;

    iget-object v0, v0, Lnxl;->b:Lnxm;

    if-eqz v0, :cond_a

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lehb;->aG:Z

    .line 804
    :cond_7
    iget-wide v0, p0, Lehb;->aR:J

    const-wide/16 v4, 0x2

    and-long/2addr v0, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_8

    if-eqz v2, :cond_8

    .line 806
    invoke-virtual {v2}, Ljux;->c()Lnzx;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 807
    invoke-virtual {v2}, Ljux;->c()Lnzx;

    move-result-object v0

    sget-object v1, Lnzr;->a:Loxr;

    invoke-virtual {v0, v1}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzr;

    .line 809
    if-eqz v0, :cond_8

    iget-object v1, v0, Lnzr;->b:Lnxr;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lnzr;->b:Lnxr;

    iget-object v1, v1, Lnxr;->e:Lnxj;

    if-eqz v1, :cond_8

    .line 811
    iget-object v0, v0, Lnzr;->b:Lnxr;

    iget-object v0, v0, Lnxr;->e:Lnxj;

    iget-object v0, v0, Lnxj;->c:Ljava/lang/String;

    iput-object v0, p0, Lehb;->aB:Ljava/lang/String;

    .line 814
    invoke-virtual {p0}, Lehb;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100321

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;

    .line 816
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/SingleAlbumSocialFooterView;->a(Landroid/database/Cursor;)V

    .line 820
    :cond_8
    iget-object v0, p0, Lehb;->aS:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    .line 821
    invoke-virtual {v2}, Ljux;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lehb;->aS:Ljava/lang/String;

    .line 822
    iget-object v0, p0, Lehb;->aS:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 823
    invoke-direct {p0}, Lehb;->aA()V

    .line 825
    :cond_9
    iget-object v0, p0, Lehb;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto/16 :goto_1

    :cond_a
    move v0, v8

    .line 800
    goto :goto_2

    .line 753
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 116
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lehb;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 504
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 505
    const-string v1, "g:"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 506
    :goto_0
    iget-object v1, p0, Lehb;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 507
    iget-object v2, p0, Lehb;->at:Llnl;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v1, v0, v3, v4}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 509
    invoke-virtual {p0, v0}, Lehb;->a(Landroid/content/Intent;)V

    .line 511
    :cond_0
    return-void

    .line 505
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 0

    .prologue
    .line 978
    invoke-super {p0, p1, p2, p3}, Legi;->a(Ljava/lang/String;Lhoz;Lhos;)V

    .line 979
    invoke-virtual {p0}, Lehb;->Z_()V

    .line 980
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLlah;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 516
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    const-class v1, Lhtl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtl;

    .line 517
    if-eqz p5, :cond_0

    .line 518
    invoke-virtual {p5}, Llah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v7, v6

    .line 517
    invoke-interface/range {v0 .. v7}, Lhtl;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLhtm;[B)V

    .line 520
    return-void

    .line 518
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 572
    invoke-super {p0, p1}, Legi;->a(Loo;)V

    .line 574
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 575
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 934
    if-nez p1, :cond_0

    .line 936
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 937
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 940
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f0a0597

    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 650
    iget-object v0, p0, Lehb;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 651
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 652
    const v3, 0x7f100688

    if-ne v2, v3, :cond_0

    .line 653
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0, v7}, Leyq;->a(Landroid/content/Context;ILjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 654
    const-string v2, "album_id"

    iget-object v3, p0, Lehb;->aM:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 655
    const-string v2, "album_owner_id"

    iget-object v3, p0, Lehb;->aL:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 656
    const-string v2, "cluster_id"

    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 657
    const-string v2, "link_url"

    iget-object v3, p0, Lehb;->aC:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 658
    invoke-virtual {p0, v0, v1}, Lehb;->a(Landroid/content/Intent;I)V

    .line 659
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    const v2, 0x7f05001e

    invoke-virtual {v0, v2, v5}, Lz;->overridePendingTransition(II)V

    move v0, v1

    .line 691
    :goto_0
    return v0

    .line 665
    :cond_0
    const v3, 0x7f1006cc

    if-eq v2, v3, :cond_1

    const v3, 0x7f1006cf

    if-ne v2, v3, :cond_3

    .line 666
    :cond_1
    invoke-virtual {p0}, Lehb;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0}, Lehb;->aw()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lehb;->aU:Z

    if-nez v2, :cond_2

    const v2, 0x7f0a0685

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0686

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0687

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v4, v0}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    :goto_1
    invoke-virtual {v0, p0, v5}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Lehb;->p()Lae;

    move-result-object v2

    const-string v3, "delete_album"

    invoke-virtual {v0, v2, v3}, Llgr;->a(Lae;Ljava/lang/String;)V

    move v0, v1

    .line 667
    goto :goto_0

    .line 666
    :cond_2
    const v2, 0x7f0a067f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0680

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0801

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v4, v0}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    goto :goto_1

    .line 668
    :cond_3
    const v3, 0x7f1006ca

    if-ne v2, v3, :cond_5

    .line 669
    invoke-direct {p0}, Lehb;->av()Z

    move-result v2

    .line 670
    if-eqz v2, :cond_4

    const/4 v0, 0x3

    :goto_2
    iput v0, p0, Lehb;->aJ:I

    iget-object v0, p0, Lehb;->at:Llnl;

    iget-object v3, p0, Lehb;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lehb;->aA:Ljava/lang/String;

    invoke-static {v0, v3, v4, v5, v2}, Lcom/google/android/apps/plus/service/EsService;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lehb;->az:Ljava/lang/Integer;

    iget-object v0, p0, Lehb;->T:Lcmz;

    const v2, 0x7f0a0591

    invoke-virtual {p0, v2}, Lehb;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcmz;->a(Ljava/lang/String;)V

    move v0, v1

    .line 671
    goto/16 :goto_0

    .line 670
    :cond_4
    const/4 v0, 0x4

    goto :goto_2

    .line 672
    :cond_5
    const v3, 0x7f1006be

    if-ne v2, v3, :cond_6

    .line 675
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Lehb;->aM:Ljava/lang/String;

    iget-object v4, p0, Lehb;->aL:Ljava/lang/String;

    .line 674
    invoke-static {v2, v0, v3, v4, v7}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lehb;->a(Landroid/content/Intent;)V

    move v0, v1

    .line 676
    goto/16 :goto_0

    .line 677
    :cond_6
    const v0, 0x7f1006ce

    if-ne v2, v0, :cond_7

    .line 679
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lehb;->aB:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 680
    invoke-virtual {p0, v0}, Lehb;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    move v0, v1

    .line 685
    goto/16 :goto_0

    .line 682
    :catch_0
    move-exception v0

    iget-object v0, p0, Lehb;->at:Llnl;

    const v2, 0x7f0a0655

    invoke-static {v0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 683
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3

    .line 686
    :cond_7
    const v0, 0x7f100681

    if-eq v2, v0, :cond_8

    const v0, 0x7f100682

    if-ne v2, v0, :cond_9

    .line 688
    :cond_8
    invoke-virtual {p0}, Lehb;->ai()V

    move v0, v1

    .line 689
    goto/16 :goto_0

    .line 691
    :cond_9
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 405
    invoke-super {p0}, Legi;->aO_()V

    .line 407
    iget-object v0, p0, Lehb;->aY:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 409
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a()V

    .line 413
    :cond_0
    iget-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 414
    iget-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 415
    invoke-virtual {p0}, Lehb;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    iget-object v0, p0, Lehb;->aW:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 424
    :cond_1
    :goto_0
    iget-object v0, p0, Lehb;->az:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 425
    iget-object v0, p0, Lehb;->az:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 426
    iget-object v0, p0, Lehb;->az:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 427
    iget-object v1, p0, Lehb;->az:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lehb;->b(ILfib;)Z

    .line 431
    :cond_2
    invoke-virtual {p0}, Lehb;->Z_()V

    .line 432
    return-void

    .line 419
    :cond_3
    iget-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 420
    iget-object v1, p0, Lehb;->ay:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lehb;->a(ILfib;)V

    goto :goto_0
.end method

.method public ac()V
    .locals 2

    .prologue
    .line 525
    invoke-direct {p0}, Lehb;->aB()Lehu;

    move-result-object v0

    .line 526
    if-eqz v0, :cond_0

    iget-object v1, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    if-eqz v1, :cond_0

    .line 527
    iget-object v1, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->b()V

    .line 528
    invoke-virtual {v0}, Lehu;->U()V

    .line 530
    :cond_0
    return-void
.end method

.method public ad()V
    .locals 6

    .prologue
    .line 543
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lehb;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    invoke-static {v5}, Ljvj;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lehb;->aM:Ljava/lang/String;

    iget-object v4, p0, Lehb;->aL:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lehb;->a(Landroid/content/Intent;I)V

    .line 544
    return-void
.end method

.method protected ai()V
    .locals 3

    .prologue
    .line 847
    iget-object v0, p0, Lehb;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lehb;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eM:Lhmv;

    .line 848
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 847
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 849
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    .line 850
    iget-boolean v1, p0, Lehb;->aH:Z

    iget-object v2, p0, Lehb;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 849
    invoke-static {v0, v1, v2}, Lduf;->a(Landroid/content/Context;ZI)Landroid/content/Intent;

    move-result-object v0

    .line 851
    const-string v1, "cluster_id"

    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 852
    iget-object v1, p0, Lehb;->S:Lhke;

    const v2, 0x7f1000c5

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 853
    return-void
.end method

.method protected final aj()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 487
    iget-object v0, p0, Lehb;->at:Llnl;

    const-class v1, Lcnt;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnt;

    .line 488
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcnt;->a(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected b(Lhjk;)V
    .locals 11

    .prologue
    const v10, 0x7f100688

    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 579
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 581
    iget-object v0, p0, Lehb;->at:Llnl;

    const/4 v3, 0x2

    invoke-static {v0, v3}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    invoke-interface {p1, v10}, Lhjk;->c(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 584
    const v3, 0x7f020548

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 587
    :cond_0
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljvj;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "PHOTO_COLLECTION"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "PLUS_EVENT"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_1
    move v0, v2

    :goto_0
    iget-wide v4, p0, Lehb;->aQ:J

    const-wide/16 v6, 0x200

    and-long/2addr v4, v6

    cmp-long v3, v4, v8

    if-eqz v3, :cond_6

    if-nez v0, :cond_6

    iget-object v0, p0, Lehb;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->e()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    .line 588
    invoke-interface {p1, v10}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 591
    :cond_2
    const/4 v0, 0x3

    .line 592
    iget-boolean v3, p0, Lehb;->ag:Z

    if-eqz v3, :cond_3

    .line 593
    const/4 v0, 0x7

    .line 595
    :cond_3
    invoke-virtual {p0, p1, v0}, Lehb;->a(Lhjk;I)V

    .line 596
    invoke-virtual {p0}, Lehb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "show_title"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 597
    iget-object v0, p0, Lehb;->aP:Ljava/lang/String;

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 600
    :cond_4
    const v0, 0x7f10067b

    .line 601
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 602
    invoke-virtual {v0, v2}, Lhjv;->a(I)V

    .line 604
    iget-boolean v0, p0, Lehb;->ac:Z

    if-eqz v0, :cond_7

    .line 646
    :cond_5
    :goto_2
    return-void

    :cond_6
    move v0, v1

    .line 587
    goto :goto_1

    .line 608
    :cond_7
    invoke-direct {p0}, Lehb;->au()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 609
    const v0, 0x7f1006be

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 617
    :cond_8
    iget-wide v4, p0, Lehb;->aQ:J

    const-wide/32 v6, 0x1000000

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljvj;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v2

    :goto_3
    if-eqz v0, :cond_9

    .line 618
    const v0, 0x7f1006ca

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 619
    invoke-direct {p0}, Lehb;->av()Z

    move-result v0

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 620
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v0, v4, :cond_c

    move v0, v2

    :goto_4
    if-nez v0, :cond_9

    .line 621
    invoke-direct {p0}, Lehb;->av()Z

    move-result v0

    if-eqz v0, :cond_d

    const v0, 0x7f0a063f

    :goto_5
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 628
    :cond_9
    iget-wide v4, p0, Lehb;->aQ:J

    const-wide/16 v6, 0x4

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-eqz v0, :cond_e

    :goto_6
    if-eqz v2, :cond_a

    invoke-direct {p0}, Lehb;->au()Z

    move-result v0

    if-nez v0, :cond_a

    .line 630
    invoke-direct {p0}, Lehb;->aw()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lehb;->aU:Z

    if-nez v0, :cond_f

    .line 631
    const v0, 0x7f1006cf

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 637
    :cond_a
    :goto_7
    iget-object v0, p0, Lehb;->aB:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 638
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    .line 639
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 640
    iget-object v3, p0, Lehb;->aB:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 642
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 643
    const v0, 0x7f1006ce

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto/16 :goto_2

    :cond_b
    move v0, v1

    .line 617
    goto :goto_3

    :cond_c
    move v0, v1

    .line 620
    goto :goto_4

    .line 621
    :cond_d
    const v0, 0x7f0a0640

    goto :goto_5

    :cond_e
    move v2, v1

    .line 628
    goto :goto_6

    .line 633
    :cond_f
    const v0, 0x7f1006cc

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_7

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 266
    invoke-super {p0, p1}, Legi;->c(Landroid/os/Bundle;)V

    .line 267
    iget-object v0, p0, Lehb;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iget-object v1, p0, Lehb;->aX:Lhob;

    invoke-virtual {v0, v1}, Lhoc;->a(Lhob;)Lhoc;

    .line 268
    return-void
.end method

.method public c(Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 449
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 450
    const v0, 0x7f100092

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 451
    const v3, 0x7f100325

    if-ne v1, v3, :cond_1

    .line 452
    iget-object v0, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehb;->aS:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 453
    iget-object v0, p0, Lehb;->an:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->b()V

    :cond_0
    move v0, v2

    .line 482
    :goto_0
    return v0

    .line 458
    :cond_1
    const v1, 0x7f100079

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 459
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 460
    const/4 v0, 0x0

    goto :goto_0

    .line 463
    :cond_2
    invoke-virtual {p0, v0}, Lehb;->a(Lizu;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 464
    goto :goto_0

    .line 467
    :cond_3
    iget-object v0, p0, Lehb;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 468
    new-instance v3, Ldew;

    .line 469
    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 470
    invoke-virtual {v3, v1}, Ldew;->a(Ljava/lang/String;)Ldew;

    move-result-object v0

    .line 471
    invoke-direct {p0}, Lehb;->ay()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lehb;->X:Lctz;

    .line 472
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lehb;->Y:Lctq;

    .line 473
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    iget-object v1, p0, Lehb;->aA:Ljava/lang/String;

    .line 474
    invoke-virtual {v0, v1}, Ldew;->c(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lehb;->Y:Lctq;

    .line 475
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->b(I)Ldew;

    move-result-object v0

    iget-boolean v1, p0, Lehb;->ag:Z

    .line 476
    invoke-virtual {v0, v1}, Ldew;->i(Z)Ldew;

    move-result-object v0

    .line 477
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v1

    .line 479
    iget-object v0, p0, Lehb;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Lehb;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->dZ:Lhmv;

    .line 480
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 479
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 481
    invoke-virtual {p0, v1}, Lehb;->b(Landroid/content/Intent;)V

    move v0, v2

    .line 482
    goto/16 :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 548
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 549
    iget-object v0, p0, Lehb;->ay:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 550
    const-string v0, "refresh_request"

    iget-object v1, p0, Lehb;->ay:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 552
    :cond_0
    iget-object v0, p0, Lehb;->az:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 553
    const-string v0, "pending_request"

    iget-object v1, p0, Lehb;->az:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 555
    :cond_1
    iget-object v0, p0, Lehb;->aE:Lhgw;

    if-eqz v0, :cond_2

    .line 556
    const-string v0, "audience"

    iget-object v1, p0, Lehb;->aE:Lhgw;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 558
    :cond_2
    iget-object v0, p0, Lehb;->aF:Lhgw;

    if-eqz v0, :cond_3

    .line 559
    const-string v0, "people_list"

    iget-object v1, p0, Lehb;->aF:Lhgw;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 561
    :cond_3
    iget-object v0, p0, Lehb;->aB:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 562
    const-string v0, "drive_url"

    iget-object v1, p0, Lehb;->aB:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :cond_4
    const-string v0, "operation_type"

    iget v1, p0, Lehb;->aJ:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 566
    const-string v0, "album_tile_id"

    iget-object v1, p0, Lehb;->aO:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    const-string v0, "album_not_found"

    iget-boolean v1, p0, Lehb;->ax:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 568
    return-void
.end method

.method protected e(Landroid/view/View;)V
    .locals 8

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1082
    if-nez p1, :cond_0

    .line 1115
    :goto_0
    return-void

    .line 1086
    :cond_0
    invoke-super {p0, p1}, Legi;->e(Landroid/view/View;)V

    .line 1088
    const v0, 0x7f100325

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1089
    const v0, 0x7f100321

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 1096
    invoke-virtual {p0}, Lehb;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "hide_footer"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1098
    iget-object v3, p0, Lehb;->Y:Lctq;

    invoke-virtual {v3}, Lctq;->d()Z

    move-result v3

    if-nez v3, :cond_2

    if-nez v0, :cond_2

    .line 1099
    iget-boolean v0, p0, Lehb;->aT:Z

    if-eqz v0, :cond_1

    .line 1103
    const/4 v0, 0x1

    move v3, v1

    move v7, v1

    move v1, v2

    move v2, v7

    .line 1111
    :goto_1
    iget-object v6, p0, Lehb;->ao:Landroid/view/View;

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1112
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1113
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1114
    iget-object v1, p0, Lehb;->ar:Lfdb;

    invoke-virtual {v1, v0}, Lfdb;->a(Z)V

    goto :goto_0

    .line 1105
    :cond_1
    invoke-direct {p0}, Lehb;->aw()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    move v3, v2

    .line 1106
    goto :goto_1

    :cond_2
    move v0, v1

    move v3, v2

    move v1, v2

    goto :goto_1
.end method

.method public g()V
    .locals 1

    .prologue
    .line 399
    invoke-super {p0}, Legi;->g()V

    .line 400
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 401
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 443
    invoke-super {p0}, Legi;->h()V

    .line 444
    iget-object v0, p0, Lehb;->am:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 445
    return-void
.end method

.method public l_(Z)V
    .locals 1

    .prologue
    .line 1355
    const/4 v0, 0x0

    iput-boolean v0, p0, Lehb;->aw:Z

    .line 1356
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lehb;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1357
    invoke-virtual {p0}, Lehb;->L_()V

    .line 1359
    :cond_0
    invoke-virtual {p0}, Lehb;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lehb;->d(Landroid/view/View;)V

    .line 1360
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 493
    iget-object v1, p0, Lehb;->Y:Lctq;

    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    instance-of v1, p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    if-eqz v1, :cond_0

    .line 495
    check-cast p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    invoke-virtual {p0, p1}, Lehb;->b(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    .line 496
    invoke-virtual {p0, v0}, Lehb;->e(I)V

    .line 497
    const/4 v0, 0x1

    .line 499
    :cond_0
    return v0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 436
    invoke-super {p0}, Legi;->z()V

    .line 438
    iget-object v0, p0, Lehb;->aY:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 439
    return-void
.end method
