.class public final Lday;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lctm;


# instance fields
.field a:Ldax;

.field b:I

.field c:Z

.field d:Z

.field e:Ljava/lang/String;

.field f:I

.field g:Z

.field h:Z

.field private i:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lday;->i:Landroid/content/Context;

    .line 31
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x2

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 35
    check-cast p1, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;

    .line 36
    if-nez p1, :cond_1

    .line 37
    iget-object v0, p0, Lday;->i:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 38
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 39
    const v1, 0x7f04018c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;

    .line 42
    :goto_0
    iget v1, p0, Lday;->b:I

    iget-boolean v2, p0, Lday;->c:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->a(IZ)V

    .line 43
    iget-boolean v1, p0, Lday;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lday;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 44
    new-instance v1, Loae;

    invoke-direct {v1}, Loae;-><init>()V

    .line 45
    iget-object v2, p0, Lday;->e:Ljava/lang/String;

    iput-object v2, v1, Loae;->a:Ljava/lang/String;

    .line 46
    iget v2, p0, Lday;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Loae;->e:Ljava/lang/Integer;

    .line 47
    iget-boolean v2, p0, Lday;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Loae;->c:Ljava/lang/Boolean;

    .line 48
    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->a(Loae;)V

    .line 50
    :cond_0
    iget-boolean v1, p0, Lday;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->a(Z)V

    .line 51
    iget-object v1, p0, Lday;->a:Ldax;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->a(Ldax;)V

    .line 52
    invoke-virtual {v0}, Lcom/google/android/apps/photos/viewer/components/comments/SocialBarView;->requestLayout()V

    .line 54
    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method public a(Lctn;)V
    .locals 0

    .prologue
    .line 65
    return-void
.end method
