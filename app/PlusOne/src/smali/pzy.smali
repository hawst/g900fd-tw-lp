.class public final Lpzy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1996
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2017
    const/4 v0, 0x0

    .line 2018
    iget-object v1, p0, Lpzy;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2019
    const/4 v0, 0x1

    iget-object v1, p0, Lpzy;->a:Ljava/lang/Integer;

    .line 2020
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2022
    :cond_0
    iget-object v1, p0, Lpzy;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2023
    const/4 v1, 0x2

    iget-object v2, p0, Lpzy;->b:Ljava/lang/String;

    .line 2024
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2026
    :cond_1
    iget-object v1, p0, Lpzy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2027
    iput v0, p0, Lpzy;->ai:I

    .line 2028
    return v0
.end method

.method public a(Loxn;)Lpzy;
    .locals 2

    .prologue
    .line 2036
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2037
    sparse-switch v0, :sswitch_data_0

    .line 2041
    iget-object v1, p0, Lpzy;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2042
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpzy;->ah:Ljava/util/List;

    .line 2045
    :cond_1
    iget-object v1, p0, Lpzy;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2047
    :sswitch_0
    return-object p0

    .line 2052
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpzy;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 2056
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpzy;->b:Ljava/lang/String;

    goto :goto_0

    .line 2037
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2005
    iget-object v0, p0, Lpzy;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2006
    const/4 v0, 0x1

    iget-object v1, p0, Lpzy;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2008
    :cond_0
    iget-object v0, p0, Lpzy;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2009
    const/4 v0, 0x2

    iget-object v1, p0, Lpzy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2011
    :cond_1
    iget-object v0, p0, Lpzy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2013
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1992
    invoke-virtual {p0, p1}, Lpzy;->a(Loxn;)Lpzy;

    move-result-object v0

    return-object v0
.end method
