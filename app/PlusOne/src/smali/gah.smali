.class public final Lgah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:F

.field private b:F

.field private c:J

.field private final d:Lcom/google/android/apps/plus/views/PhotoView;

.field private e:F

.field private f:F

.field private g:J

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/PhotoView;)V
    .locals 2

    .prologue
    .line 1977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1978
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgah;->g:J

    .line 1979
    iput-object p1, p0, Lgah;->d:Lcom/google/android/apps/plus/views/PhotoView;

    .line 1980
    return-void
.end method

.method public static synthetic a(Lgah;)Z
    .locals 1

    .prologue
    .line 1962
    iget-boolean v0, p0, Lgah;->h:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 2008
    iget-object v0, p0, Lgah;->d:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2009
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgah;->h:Z

    .line 2010
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgah;->i:Z

    .line 2011
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgah;->g:J

    .line 2012
    return-void
.end method

.method public a(FF)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v8, 0x437a0000    # 250.0f

    .line 1986
    iput p1, p0, Lgah;->e:F

    .line 1987
    iput p2, p0, Lgah;->f:F

    .line 1988
    const-wide/16 v2, 0xfa

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lgah;->g:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    .line 1989
    iget-boolean v4, p0, Lgah;->h:Z

    if-eqz v4, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 1990
    iget v1, p0, Lgah;->e:F

    long-to-float v4, v2

    div-float/2addr v1, v4

    iput v1, p0, Lgah;->a:F

    .line 1991
    iget v1, p0, Lgah;->f:F

    long-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lgah;->b:F

    .line 2001
    :goto_0
    return v0

    .line 1994
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lgah;->g:J

    .line 1995
    div-float v2, p1, v8

    iput v2, p0, Lgah;->a:F

    .line 1996
    div-float v2, p2, v8

    iput v2, p0, Lgah;->b:F

    .line 1998
    iput-boolean v0, p0, Lgah;->i:Z

    .line 1999
    iput-boolean v1, p0, Lgah;->h:Z

    .line 2000
    iget-object v0, p0, Lgah;->d:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    .line 2001
    goto :goto_0
.end method

.method public run()V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x0

    .line 2017
    iget-boolean v0, p0, Lgah;->i:Z

    if-eqz v0, :cond_0

    .line 2064
    :goto_0
    return-void

    .line 2022
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 2023
    iget-wide v2, p0, Lgah;->g:J

    cmp-long v0, v2, v6

    if-eqz v0, :cond_5

    iget-wide v2, p0, Lgah;->g:J

    sub-long v2, v4, v2

    long-to-float v0, v2

    .line 2025
    :goto_1
    iget-wide v2, p0, Lgah;->g:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_1

    .line 2026
    iput-wide v4, p0, Lgah;->c:J

    .line 2027
    iput-wide v4, p0, Lgah;->g:J

    .line 2035
    :cond_1
    const/high16 v2, 0x437a0000    # 250.0f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_6

    .line 2036
    iget v2, p0, Lgah;->e:F

    .line 2037
    iget v0, p0, Lgah;->f:F

    .line 2044
    :goto_2
    iput-wide v4, p0, Lgah;->c:J

    .line 2045
    iget-object v3, p0, Lgah;->d:Lcom/google/android/apps/plus/views/PhotoView;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v3, v4, v2, v0, v5}, Lcom/google/android/apps/plus/views/PhotoView;->a(Lcom/google/android/apps/plus/views/PhotoView;ZFFZ)Z

    .line 2047
    iget v3, p0, Lgah;->e:F

    sub-float v2, v3, v2

    iput v2, p0, Lgah;->e:F

    .line 2048
    iget v2, p0, Lgah;->f:F

    sub-float v0, v2, v0

    iput v0, p0, Lgah;->f:F

    .line 2051
    iget v0, p0, Lgah;->e:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 2052
    iput v1, p0, Lgah;->a:F

    .line 2054
    :cond_2
    iget v0, p0, Lgah;->f:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    .line 2055
    iput v1, p0, Lgah;->b:F

    .line 2058
    :cond_3
    iget v0, p0, Lgah;->a:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget v0, p0, Lgah;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    .line 2059
    invoke-virtual {p0}, Lgah;->a()V

    .line 2063
    :cond_4
    iget-object v0, p0, Lgah;->d:Lcom/google/android/apps/plus/views/PhotoView;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, p0, v2, v3}, Lcom/google/android/apps/plus/views/PhotoView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_5
    move v0, v1

    .line 2023
    goto :goto_1

    .line 2039
    :cond_6
    iget-wide v2, p0, Lgah;->c:J

    sub-long v6, v4, v2

    .line 2040
    iget v0, p0, Lgah;->a:F

    long-to-float v2, v6

    mul-float/2addr v2, v0

    .line 2041
    iget v0, p0, Lgah;->b:F

    long-to-float v3, v6

    mul-float/2addr v0, v3

    goto :goto_2
.end method
