.class public final Ljjd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjm;
.implements Ljjp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-string v0, "com.google.android.libraries.social.notifications.FETCH_NOTIFICATIONS_COUNT"

    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 20
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->a(Landroid/content/Intent;)Lkfo;

    move-result-object v0

    .line 21
    if-nez v0, :cond_0

    .line 30
    :goto_0
    return-void

    .line 25
    :cond_0
    new-instance v1, Ljje;

    .line 27
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->b(Landroid/content/Intent;)Ljix;

    move-result-object v2

    invoke-direct {v1, p2, v0, v2}, Ljje;-><init>(Landroid/content/Context;Lkfo;Ljix;)V

    .line 28
    const-class v0, Ljjo;

    invoke-static {p2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjo;

    const/4 v2, 0x0

    .line 29
    invoke-virtual {v0, v1, p0, p2, v2}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 39
    check-cast p1, Ljje;

    .line 40
    invoke-virtual {p1}, Ljje;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    invoke-virtual {p1}, Ljje;->j()V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    const-string v0, "FetchNotsCountHandler"

    const-string v1, "Failed to fetch notifications count."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Ljjd;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
