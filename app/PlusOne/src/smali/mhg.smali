.class public final Lmhg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lngt;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33613
    invoke-direct {p0}, Loxq;-><init>()V

    .line 33616
    iput-object v0, p0, Lmhg;->apiHeader:Llyq;

    .line 33619
    iput-object v0, p0, Lmhg;->a:Lngt;

    .line 33613
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 33636
    const/4 v0, 0x0

    .line 33637
    iget-object v1, p0, Lmhg;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 33638
    const/4 v0, 0x1

    iget-object v1, p0, Lmhg;->apiHeader:Llyq;

    .line 33639
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 33641
    :cond_0
    iget-object v1, p0, Lmhg;->a:Lngt;

    if-eqz v1, :cond_1

    .line 33642
    const/4 v1, 0x2

    iget-object v2, p0, Lmhg;->a:Lngt;

    .line 33643
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33645
    :cond_1
    iget-object v1, p0, Lmhg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 33646
    iput v0, p0, Lmhg;->ai:I

    .line 33647
    return v0
.end method

.method public a(Loxn;)Lmhg;
    .locals 2

    .prologue
    .line 33655
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 33656
    sparse-switch v0, :sswitch_data_0

    .line 33660
    iget-object v1, p0, Lmhg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 33661
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhg;->ah:Ljava/util/List;

    .line 33664
    :cond_1
    iget-object v1, p0, Lmhg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33666
    :sswitch_0
    return-object p0

    .line 33671
    :sswitch_1
    iget-object v0, p0, Lmhg;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 33672
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmhg;->apiHeader:Llyq;

    .line 33674
    :cond_2
    iget-object v0, p0, Lmhg;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33678
    :sswitch_2
    iget-object v0, p0, Lmhg;->a:Lngt;

    if-nez v0, :cond_3

    .line 33679
    new-instance v0, Lngt;

    invoke-direct {v0}, Lngt;-><init>()V

    iput-object v0, p0, Lmhg;->a:Lngt;

    .line 33681
    :cond_3
    iget-object v0, p0, Lmhg;->a:Lngt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 33656
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33624
    iget-object v0, p0, Lmhg;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 33625
    const/4 v0, 0x1

    iget-object v1, p0, Lmhg;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33627
    :cond_0
    iget-object v0, p0, Lmhg;->a:Lngt;

    if-eqz v0, :cond_1

    .line 33628
    const/4 v0, 0x2

    iget-object v1, p0, Lmhg;->a:Lngt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33630
    :cond_1
    iget-object v0, p0, Lmhg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 33632
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 33609
    invoke-virtual {p0, p1}, Lmhg;->a(Loxn;)Lmhg;

    move-result-object v0

    return-object v0
.end method
