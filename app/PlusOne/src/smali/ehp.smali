.class final Lehp;
.super Lfdp;
.source "PG"


# instance fields
.field private synthetic a:Lehh;


# direct methods
.method public constructor <init>(Lehh;)V
    .locals 2

    .prologue
    .line 3414
    iput-object p1, p0, Lehp;->a:Lehh;

    .line 3415
    invoke-static {p1}, Lehh;->k(Lehh;)Llnl;

    move-result-object v0

    iget-object v1, p1, Lehh;->R:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lfdp;-><init>(Landroid/content/Context;I)V

    .line 3416
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 3457
    return-void
.end method

.method public a(Landroid/text/style/URLSpan;)V
    .locals 0

    .prologue
    .line 3461
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3449
    .line 3450
    invoke-static {p1, p2}, Lkxy;->a(Ljava/lang/String;Ljava/lang/String;)Lkxy;

    move-result-object v0

    .line 3451
    iget-object v1, p0, Lehp;->a:Lehh;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lkxy;->a(Lu;I)V

    .line 3452
    iget-object v1, p0, Lehp;->a:Lehh;

    invoke-virtual {v1}, Lehh;->p()Lae;

    move-result-object v1

    const-string v2, "hsf_moderation"

    invoke-virtual {v0, v1, v2}, Lkxy;->a(Lae;Ljava/lang/String;)V

    .line 3453
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 3435
    iget-object v0, p0, Lehp;->a:Lehh;

    invoke-static {p1}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    move-object v2, p2

    move-object v5, v4

    move-object v6, v4

    move-object v7, p3

    move v8, p4

    invoke-virtual/range {v0 .. v8}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 3438
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 3421
    .line 3423
    iget-object v0, p0, Lehp;->a:Lehh;

    invoke-static {v0}, Lehh;->l(Lehh;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p5, :cond_0

    .line 3424
    iget-object v0, p0, Lehp;->a:Lehh;

    invoke-static {v0}, Lehh;->m(Lehh;)Llnl;

    move-result-object v0

    iget-object v1, p0, Lehp;->a:Lehh;

    iget-object v1, v1, Lehh;->as:Landroid/database/Cursor;

    invoke-static {v0, v1, v2}, Ldsm;->a(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v4

    .line 3426
    iget-object v0, p0, Lehp;->a:Lehh;

    invoke-static {v0}, Lehh;->n(Lehh;)Llnl;

    move-result-object v0

    invoke-static {v0, v2}, Ldsm;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v5

    .line 3428
    :goto_0
    iget-object v0, p0, Lehp;->a:Lehh;

    invoke-static {p1}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, p2

    move v3, p6

    move-object v7, p3

    move v8, p4

    invoke-virtual/range {v0 .. v8}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 3430
    return-void

    :cond_0
    move-object v5, v6

    move-object v4, v6

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3467
    iget-object v0, p0, Lehp;->a:Lehh;

    invoke-virtual {v0}, Lehh;->aI()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lehp;->a:Lehh;

    iget-object v0, v0, Lehh;->ac:Levm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lehp;->a:Lehh;

    iget-object v0, v0, Lehh;->ac:Levm;

    .line 3468
    invoke-virtual {v0}, Levm;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lehp;->a:Lehh;

    iget-object v0, v0, Lehh;->ac:Levm;

    invoke-virtual {v0}, Levm;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3469
    :cond_0
    invoke-super {p0, p1, p2}, Lfdp;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3476
    :goto_0
    return-void

    .line 3471
    :cond_1
    iget-object v0, p0, Lehp;->a:Lehh;

    iget-object v0, v0, Lehh;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 3472
    iget-object v1, p0, Lehp;->a:Lehh;

    invoke-static {v1}, Lehh;->o(Lehh;)Llnl;

    move-result-object v1

    invoke-static {v1, v0, p1, p2}, Leyq;->f(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 3474
    iget-object v1, p0, Lehp;->a:Lehh;

    invoke-virtual {v1, v0}, Lehh;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 3442
    iget-object v0, p0, Lehp;->a:Lehh;

    invoke-static {p1}, Ljpu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    move-object v2, p2

    move-object v5, v4

    move-object v6, v4

    move-object v7, p3

    move v8, p4

    invoke-virtual/range {v0 .. v8}, Lehh;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 3445
    return-void
.end method
