.class public final Leon;
.super Lenl;
.source "PG"


# instance fields
.field private aA:Landroid/widget/RadioGroup;

.field private aB:Landroid/widget/CheckBox;

.field an:Landroid/animation/LayoutTransition$TransitionListener;

.field ao:Landroid/view/ViewGroup;

.field ap:Landroid/view/ViewGroup;

.field private aq:Lnkf;

.field private ar:Lnkf;

.field private as:Z

.field private aw:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private ax:Landroid/widget/CheckBox;

.field private ay:Landroid/widget/RadioGroup;

.field private az:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lenl;-><init>()V

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Leon;->aw:Ljava/util/HashMap;

    return-void
.end method

.method private a(Landroid/widget/RadioGroup;Z)V
    .locals 3

    .prologue
    .line 245
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getChildCount()I

    move-result v1

    .line 246
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 247
    invoke-virtual {p1, v0}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 249
    :cond_0
    return-void
.end method

.method static synthetic a(Leon;)V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Leon;->aw:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v3, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Leon;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Leon;->b(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 227
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 228
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 229
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 230
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 228
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 232
    :cond_0
    return-void
.end method

.method private al()V
    .locals 13

    .prologue
    const v1, 0x7f100530

    const v0, 0x7f10052f

    const/16 v2, 0x8

    const/4 v12, 0x1

    const/4 v3, 0x0

    .line 277
    iget-object v4, p0, Leon;->ar:Lnkf;

    iget-object v4, v4, Lnkf;->b:[Lnjh;

    if-eqz v4, :cond_5

    .line 278
    const/16 v4, 0x3e8

    .line 280
    iget-object v5, p0, Leon;->ar:Lnkf;

    iget-object v8, v5, Lnkf;->b:[Lnjh;

    array-length v9, v8

    move v5, v3

    move v6, v4

    move v4, v3

    :goto_0
    if-ge v4, v9, :cond_0

    aget-object v10, v8, v4

    .line 281
    new-instance v11, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Leon;->n()Lz;

    move-result-object v7

    invoke-direct {v11, v7}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 282
    add-int/lit8 v7, v6, 0x1

    invoke-virtual {v11, v6}, Landroid/widget/CheckBox;->setId(I)V

    .line 283
    iget-object v6, v10, Lnjh;->b:Ljava/lang/String;

    invoke-virtual {v11, v6}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 284
    iget-object v6, v10, Lnjh;->c:Ljava/lang/String;

    invoke-virtual {v11, v6}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 285
    iget-object v6, v10, Lnjh;->d:Ljava/lang/Boolean;

    invoke-static {v6}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v6

    .line 286
    invoke-virtual {v11, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 287
    or-int/2addr v5, v6

    .line 288
    iget-object v6, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v6, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 280
    add-int/lit8 v4, v4, 0x1

    move v6, v7

    goto :goto_0

    .line 291
    :cond_0
    iget-object v4, p0, Leon;->ax:Landroid/widget/CheckBox;

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 292
    if-eqz v5, :cond_3

    .line 293
    invoke-direct {p0, v12}, Leon;->b(Z)V

    .line 294
    iget-object v4, p0, Leon;->ar:Lnkf;

    iget-object v4, v4, Lnkf;->c:Ljava/lang/Boolean;

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    .line 295
    iget-object v5, p0, Leon;->ay:Landroid/widget/RadioGroup;

    if-eqz v4, :cond_1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 296
    iget-object v1, p0, Leon;->ap:Landroid/view/ViewGroup;

    if-eqz v4, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 297
    iget-object v0, p0, Leon;->aA:Landroid/widget/RadioGroup;

    iget-object v1, p0, Leon;->ar:Lnkf;

    iget v1, v1, Lnkf;->a:I

    invoke-static {v1}, Leon;->e(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 319
    :goto_3
    return-void

    :cond_1
    move v0, v1

    .line 295
    goto :goto_1

    :cond_2
    move v0, v3

    .line 296
    goto :goto_2

    .line 299
    :cond_3
    invoke-direct {p0, v3}, Leon;->b(Z)V

    .line 300
    iget-object v1, p0, Leon;->ay:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 301
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 302
    iget-boolean v0, p0, Leon;->as:Z

    if-eqz v0, :cond_4

    .line 303
    invoke-direct {p0, v12}, Leon;->a(Z)V

    .line 304
    iput-boolean v3, p0, Leon;->as:Z

    .line 306
    :cond_4
    iget-object v0, p0, Leon;->aA:Landroid/widget/RadioGroup;

    const v1, 0x7f100534

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_3

    .line 310
    :cond_5
    iget-object v4, p0, Leon;->ax:Landroid/widget/CheckBox;

    invoke-virtual {v4, v12}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 311
    iget-object v4, p0, Leon;->ay:Landroid/widget/RadioGroup;

    invoke-virtual {v4, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 312
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Leon;->aA:Landroid/widget/RadioGroup;

    const v2, 0x7f100534

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 316
    iget-object v0, p0, Leon;->ay:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 317
    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setEnabled(Z)V

    goto :goto_3
.end method

.method static synthetic b(Leon;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 38
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    return-void
.end method

.method static synthetic b(Leon;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Leon;->a(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 267
    iget-object v0, p0, Leon;->ay:Landroid/widget/RadioGroup;

    invoke-direct {p0, v0, p1}, Leon;->a(Landroid/widget/RadioGroup;Z)V

    .line 268
    iget-object v1, p0, Leon;->az:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/high16 v0, -0x1000000

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 269
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 270
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 271
    iget-object v2, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 268
    :cond_0
    sget v0, Leon;->S:I

    goto :goto_0

    .line 273
    :cond_1
    iget-object v0, p0, Leon;->aA:Landroid/widget/RadioGroup;

    invoke-direct {p0, v0, p1}, Leon;->a(Landroid/widget/RadioGroup;Z)V

    .line 274
    return-void
.end method

.method static synthetic c(Leon;)V
    .locals 5

    .prologue
    .line 38
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v3, p0, Leon;->aw:Ljava/util/HashMap;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static e(I)I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    .line 45
    const v0, 0x7f100534

    .line 49
    :goto_0
    return v0

    .line 46
    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 47
    const v0, 0x7f100535

    goto :goto_0

    .line 49
    :cond_1
    const v0, 0x7f10052f

    goto :goto_0
.end method


# virtual methods
.method public U()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 323
    invoke-super {p0}, Lenl;->U()V

    .line 326
    iget-object v0, p0, Leon;->aq:Lnkf;

    iget-object v2, v0, Lnkf;->b:[Lnjh;

    array-length v4, v2

    move v0, v3

    move v5, v3

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v1, v2, v0

    .line 327
    iget-object v1, v1, Lnjh;->d:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    .line 328
    or-int/2addr v1, v5

    .line 326
    add-int/lit8 v0, v0, 0x1

    move v5, v1

    goto :goto_0

    .line 332
    :cond_0
    iget-object v0, p0, Leon;->aq:Lnkf;

    iget-object v6, v0, Lnkf;->b:[Lnjh;

    array-length v7, v6

    move v2, v3

    move v0, v3

    :goto_1
    if-ge v2, v7, :cond_2

    aget-object v1, v6, v2

    .line 333
    iget-object v8, p0, Leon;->ap:Landroid/view/ViewGroup;

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v8, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 335
    if-eqz v5, :cond_1

    .line 336
    iget-object v1, v1, Lnjh;->d:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v8

    .line 337
    new-instance v1, Lenp;

    invoke-direct {v1, p0, v8}, Lenp;-><init>(Lenl;Z)V

    .line 338
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v8

    invoke-virtual {v1, v0, v8}, Lenp;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 342
    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 332
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v4

    goto :goto_1

    .line 340
    :cond_1
    new-instance v1, Lenp;

    invoke-direct {v1, p0, v3}, Lenp;-><init>(Lenl;Z)V

    goto :goto_2

    .line 346
    :cond_2
    if-eqz v5, :cond_3

    .line 347
    iget-object v0, p0, Leon;->aq:Lnkf;

    iget v0, v0, Lnkf;->a:I

    invoke-static {v0}, Leon;->e(I)I

    move-result v0

    .line 351
    :goto_3
    new-instance v1, Lenv;

    invoke-direct {v1, p0, v0}, Lenv;-><init>(Lenl;I)V

    .line 352
    iget-object v0, p0, Leon;->aA:Landroid/widget/RadioGroup;

    iget-object v2, p0, Leon;->aA:Landroid/widget/RadioGroup;

    .line 353
    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v2

    .line 352
    invoke-virtual {v1, v0, v2}, Lenv;->onCheckedChanged(Landroid/widget/RadioGroup;I)V

    .line 354
    iget-object v0, p0, Leon;->aA:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 357
    iget-object v0, p0, Leon;->aq:Lnkf;

    iget-object v0, v0, Lnkf;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 358
    iget-object v0, p0, Leon;->aq:Lnkf;

    iget-object v0, v0, Lnkf;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 362
    :goto_4
    new-instance v1, Lenp;

    invoke-direct {v1, p0, v0}, Lenp;-><init>(Lenl;Z)V

    .line 363
    iget-object v0, p0, Leon;->aB:Landroid/widget/CheckBox;

    iget-object v2, p0, Leon;->aB:Landroid/widget/CheckBox;

    .line 364
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 363
    invoke-virtual {v1, v0, v2}, Lenp;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    .line 365
    iget-object v0, p0, Leon;->aB:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 366
    return-void

    .line 349
    :cond_3
    const v0, 0x7f100534

    goto :goto_3

    .line 360
    :cond_4
    const/4 v0, 0x1

    goto :goto_4
.end method

.method protected V()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 370
    invoke-super {p0}, Lenl;->V()V

    .line 372
    new-instance v5, Lnkf;

    invoke-direct {v5}, Lnkf;-><init>()V

    .line 373
    iget-object v0, p0, Leon;->ax:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 375
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    .line 376
    new-array v0, v6, [Lnjh;

    iput-object v0, v5, Lnkf;->b:[Lnjh;

    move v3, v2

    .line 377
    :goto_0
    if-ge v4, v6, :cond_0

    .line 378
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 379
    new-instance v7, Lnjh;

    invoke-direct {v7}, Lnjh;-><init>()V

    .line 380
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v7, Lnjh;->b:Ljava/lang/String;

    .line 381
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lnjh;->c:Ljava/lang/String;

    .line 382
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v7, Lnjh;->d:Ljava/lang/Boolean;

    .line 383
    iget-object v0, v7, Lnjh;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    and-int/2addr v0, v3

    .line 384
    iget-object v1, v5, Lnkf;->b:[Lnjh;

    aput-object v7, v1, v4

    .line 377
    add-int/lit8 v4, v4, 0x1

    move v3, v0

    goto :goto_0

    .line 386
    :cond_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lnkf;->c:Ljava/lang/Boolean;

    .line 387
    iget-object v0, p0, Leon;->aA:Landroid/widget/RadioGroup;

    .line 388
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f100534

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    :goto_1
    iput v0, v5, Lnkf;->a:I

    .line 403
    :goto_2
    iget-object v0, p0, Leon;->aB:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lnkf;->d:Ljava/lang/Boolean;

    .line 405
    invoke-virtual {p0, v5}, Leon;->a(Lnkf;)V

    .line 406
    return-void

    .line 388
    :cond_1
    const v1, 0x7f100535

    if-ne v0, v1, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    const/high16 v0, -0x80000000

    goto :goto_1

    .line 390
    :cond_3
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    .line 391
    new-array v0, v6, [Lnjh;

    iput-object v0, v5, Lnkf;->b:[Lnjh;

    move v3, v4

    .line 392
    :goto_3
    if-ge v3, v6, :cond_4

    .line 393
    iget-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 394
    new-instance v7, Lnjh;

    invoke-direct {v7}, Lnjh;-><init>()V

    .line 395
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v7, Lnjh;->b:Ljava/lang/String;

    .line 396
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v7, Lnjh;->d:Ljava/lang/Boolean;

    .line 397
    invoke-virtual {v0}, Landroid/widget/CheckBox;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lnjh;->c:Ljava/lang/String;

    .line 398
    iget-object v0, v5, Lnkf;->b:[Lnjh;

    aput-object v7, v0, v3

    .line 392
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 400
    :cond_4
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v5, Lnkf;->c:Ljava/lang/Boolean;

    .line 401
    iput v2, v5, Lnkf;->a:I

    goto :goto_2
.end method

.method protected W()Lfhh;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Leoo;

    invoke-direct {v0, p0}, Leoo;-><init>(Leon;)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 90
    iput-boolean v0, p0, Leon;->U:Z

    .line 91
    iput-boolean v0, p0, Leon;->V:Z

    .line 92
    invoke-super {p0, p1}, Lenl;->a(Landroid/os/Bundle;)V

    .line 94
    if-eqz p1, :cond_0

    .line 95
    const-string v0, "people_in_your_circles"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Leon;->as:Z

    .line 96
    const-string v0, "circles_list"

    .line 97
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Leon;->aw:Ljava/util/HashMap;

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    iput-boolean v0, p0, Leon;->as:Z

    goto :goto_0
.end method

.method protected a(Ldsx;)V
    .locals 1

    .prologue
    .line 416
    if-eqz p1, :cond_0

    iget-object v0, p1, Ldsx;->j:Lmcf;

    if-eqz v0, :cond_0

    iget-object v0, p1, Ldsx;->j:Lmcf;

    iget-object v0, v0, Lmcf;->a:Lnlm;

    if-eqz v0, :cond_0

    iget-object v0, p1, Ldsx;->j:Lmcf;

    iget-object v0, v0, Lmcf;->a:Lnlm;

    iget-object v0, v0, Lnlm;->b:Lnkf;

    if-nez v0, :cond_1

    .line 418
    :cond_0
    invoke-virtual {p0}, Leon;->c()V

    .line 419
    invoke-virtual {p0}, Leon;->d()V

    .line 428
    :goto_0
    return-void

    .line 422
    :cond_1
    iget-object v0, p1, Ldsx;->j:Lmcf;

    iget-object v0, v0, Lmcf;->a:Lnlm;

    iget-object v0, v0, Lnlm;->b:Lnkf;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    iput-object v0, p0, Leon;->aa:[B

    .line 423
    iget-object v0, p0, Leon;->Z:[B

    if-nez v0, :cond_2

    .line 424
    iget-object v0, p0, Leon;->aa:[B

    iput-object v0, p0, Leon;->Z:[B

    .line 426
    :cond_2
    invoke-virtual {p0}, Leon;->c()V

    .line 427
    invoke-virtual {p0}, Leon;->d()V

    goto :goto_0
.end method

.method protected a(Lnkf;)V
    .locals 2

    .prologue
    .line 409
    .line 410
    invoke-virtual {p0}, Leon;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Leon;->am:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 409
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILnkf;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leon;->al:Ljava/lang/Integer;

    .line 411
    const v0, 0x7f0a0378

    invoke-virtual {p0, v0}, Leon;->c(I)V

    .line 412
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0}, Lenl;->c()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Leon;->ar:Lnkf;

    .line 114
    iget-object v0, p0, Leon;->Z:[B

    if-eqz v0, :cond_0

    .line 116
    :try_start_0
    new-instance v0, Lnkf;

    invoke-direct {v0}, Lnkf;-><init>()V

    iget-object v1, p0, Leon;->Z:[B

    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnkf;

    iput-object v0, p0, Leon;->ar:Lnkf;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :cond_0
    :goto_0
    iget-object v0, p0, Leon;->ar:Lnkf;

    if-nez v0, :cond_1

    .line 122
    new-instance v0, Lnkf;

    invoke-direct {v0}, Lnkf;-><init>()V

    iput-object v0, p0, Leon;->ar:Lnkf;

    .line 124
    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Lenl;->d()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Leon;->aq:Lnkf;

    .line 130
    iget-object v0, p0, Leon;->aa:[B

    if-eqz v0, :cond_0

    .line 132
    :try_start_0
    new-instance v0, Lnkf;

    invoke-direct {v0}, Lnkf;-><init>()V

    iget-object v1, p0, Leon;->aa:[B

    .line 133
    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnkf;

    iput-object v0, p0, Leon;->aq:Lnkf;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :cond_0
    :goto_0
    iget-object v0, p0, Leon;->aq:Lnkf;

    if-nez v0, :cond_1

    .line 139
    new-instance v0, Lnkf;

    invoke-direct {v0}, Lnkf;-><init>()V

    iput-object v0, p0, Leon;->aq:Lnkf;

    .line 141
    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected e()V
    .locals 5

    .prologue
    .line 145
    invoke-super {p0}, Lenl;->e()V

    .line 147
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Leon;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401bb

    iget-object v2, p0, Leon;->ai:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leon;->ao:Landroid/view/ViewGroup;

    .line 149
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 151
    sget v1, Leon;->R:I

    sget v2, Leon;->R:I

    sget v3, Leon;->R:I

    sget v4, Leon;->R:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 153
    iget-object v1, p0, Leon;->ai:Landroid/widget/LinearLayout;

    iget-object v2, p0, Leon;->ao:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    iget-object v0, p0, Leon;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f10052d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Leon;->ax:Landroid/widget/CheckBox;

    .line 156
    iget-object v0, p0, Leon;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f10052e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Leon;->ay:Landroid/widget/RadioGroup;

    .line 157
    iget-object v0, p0, Leon;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100531

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Leon;->ap:Landroid/view/ViewGroup;

    .line 158
    iget-object v0, p0, Leon;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100532

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Leon;->az:Landroid/widget/TextView;

    .line 159
    iget-object v0, p0, Leon;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100533

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Leon;->aA:Landroid/widget/RadioGroup;

    .line 160
    invoke-direct {p0}, Leon;->al()V

    .line 162
    iget-object v0, p0, Leon;->ax:Landroid/widget/CheckBox;

    new-instance v1, Leop;

    invoke-direct {v1, p0}, Leop;-><init>(Leon;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 181
    iget-object v0, p0, Leon;->ay:Landroid/widget/RadioGroup;

    new-instance v1, Leoq;

    invoke-direct {v1, p0}, Leoq;-><init>(Leon;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 218
    iget-object v0, p0, Leon;->ao:Landroid/view/ViewGroup;

    const v1, 0x7f100537

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Leon;->aB:Landroid/widget/CheckBox;

    .line 219
    iget-object v0, p0, Leon;->ar:Lnkf;

    iget-object v0, v0, Lnkf;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Leon;->aB:Landroid/widget/CheckBox;

    iget-object v1, p0, Leon;->ar:Lnkf;

    iget-object v1, v1, Lnkf;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 224
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Leon;->aB:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    invoke-super {p0, p1}, Lenl;->e(Landroid/os/Bundle;)V

    .line 106
    const-string v0, "people_in_your_circles"

    iget-boolean v1, p0, Leon;->as:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    const-string v0, "circles_list"

    iget-object v1, p0, Leon;->aw:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 108
    return-void
.end method
