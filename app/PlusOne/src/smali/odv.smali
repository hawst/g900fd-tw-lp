.class public final Lodv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lodv;


# instance fields
.field public b:Lods;

.field public c:Lodw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    new-array v0, v0, [Lodv;

    sput-object v0, Lodv;->a:[Lodv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 232
    invoke-direct {p0}, Loxq;-><init>()V

    .line 235
    iput-object v0, p0, Lodv;->b:Lods;

    .line 238
    iput-object v0, p0, Lodv;->c:Lodw;

    .line 232
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 255
    const/4 v0, 0x0

    .line 256
    iget-object v1, p0, Lodv;->b:Lods;

    if-eqz v1, :cond_0

    .line 257
    const/4 v0, 0x1

    iget-object v1, p0, Lodv;->b:Lods;

    .line 258
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 260
    :cond_0
    iget-object v1, p0, Lodv;->c:Lodw;

    if-eqz v1, :cond_1

    .line 261
    const/4 v1, 0x2

    iget-object v2, p0, Lodv;->c:Lodw;

    .line 262
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_1
    iget-object v1, p0, Lodv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    iput v0, p0, Lodv;->ai:I

    .line 266
    return v0
.end method

.method public a(Loxn;)Lodv;
    .locals 2

    .prologue
    .line 274
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 275
    sparse-switch v0, :sswitch_data_0

    .line 279
    iget-object v1, p0, Lodv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 280
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lodv;->ah:Ljava/util/List;

    .line 283
    :cond_1
    iget-object v1, p0, Lodv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    :sswitch_0
    return-object p0

    .line 290
    :sswitch_1
    iget-object v0, p0, Lodv;->b:Lods;

    if-nez v0, :cond_2

    .line 291
    new-instance v0, Lods;

    invoke-direct {v0}, Lods;-><init>()V

    iput-object v0, p0, Lodv;->b:Lods;

    .line 293
    :cond_2
    iget-object v0, p0, Lodv;->b:Lods;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 297
    :sswitch_2
    iget-object v0, p0, Lodv;->c:Lodw;

    if-nez v0, :cond_3

    .line 298
    new-instance v0, Lodw;

    invoke-direct {v0}, Lodw;-><init>()V

    iput-object v0, p0, Lodv;->c:Lodw;

    .line 300
    :cond_3
    iget-object v0, p0, Lodv;->c:Lodw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 275
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lodv;->b:Lods;

    if-eqz v0, :cond_0

    .line 244
    const/4 v0, 0x1

    iget-object v1, p0, Lodv;->b:Lods;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 246
    :cond_0
    iget-object v0, p0, Lodv;->c:Lodw;

    if-eqz v0, :cond_1

    .line 247
    const/4 v0, 0x2

    iget-object v1, p0, Lodv;->c:Lodw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 249
    :cond_1
    iget-object v0, p0, Lodv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 251
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0, p1}, Lodv;->a(Loxn;)Lodv;

    move-result-object v0

    return-object v0
.end method
