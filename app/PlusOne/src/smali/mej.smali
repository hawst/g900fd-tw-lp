.class public final Lmej;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlt;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32074
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32077
    iput-object v0, p0, Lmej;->apiHeader:Llyr;

    .line 32080
    iput-object v0, p0, Lmej;->a:Lmlt;

    .line 32074
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32097
    const/4 v0, 0x0

    .line 32098
    iget-object v1, p0, Lmej;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 32099
    const/4 v0, 0x1

    iget-object v1, p0, Lmej;->apiHeader:Llyr;

    .line 32100
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32102
    :cond_0
    iget-object v1, p0, Lmej;->a:Lmlt;

    if-eqz v1, :cond_1

    .line 32103
    const/4 v1, 0x2

    iget-object v2, p0, Lmej;->a:Lmlt;

    .line 32104
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32106
    :cond_1
    iget-object v1, p0, Lmej;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32107
    iput v0, p0, Lmej;->ai:I

    .line 32108
    return v0
.end method

.method public a(Loxn;)Lmej;
    .locals 2

    .prologue
    .line 32116
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32117
    sparse-switch v0, :sswitch_data_0

    .line 32121
    iget-object v1, p0, Lmej;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32122
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmej;->ah:Ljava/util/List;

    .line 32125
    :cond_1
    iget-object v1, p0, Lmej;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32127
    :sswitch_0
    return-object p0

    .line 32132
    :sswitch_1
    iget-object v0, p0, Lmej;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 32133
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmej;->apiHeader:Llyr;

    .line 32135
    :cond_2
    iget-object v0, p0, Lmej;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32139
    :sswitch_2
    iget-object v0, p0, Lmej;->a:Lmlt;

    if-nez v0, :cond_3

    .line 32140
    new-instance v0, Lmlt;

    invoke-direct {v0}, Lmlt;-><init>()V

    iput-object v0, p0, Lmej;->a:Lmlt;

    .line 32142
    :cond_3
    iget-object v0, p0, Lmej;->a:Lmlt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32117
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32085
    iget-object v0, p0, Lmej;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 32086
    const/4 v0, 0x1

    iget-object v1, p0, Lmej;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32088
    :cond_0
    iget-object v0, p0, Lmej;->a:Lmlt;

    if-eqz v0, :cond_1

    .line 32089
    const/4 v0, 0x2

    iget-object v1, p0, Lmej;->a:Lmlt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32091
    :cond_1
    iget-object v0, p0, Lmej;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32093
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 32070
    invoke-virtual {p0, p1}, Lmej;->a(Loxn;)Lmej;

    move-result-object v0

    return-object v0
.end method
