.class public final Lmce;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnla;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 403
    invoke-direct {p0}, Loxq;-><init>()V

    .line 406
    iput-object v0, p0, Lmce;->apiHeader:Llyq;

    .line 409
    iput-object v0, p0, Lmce;->a:Lnla;

    .line 403
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 426
    const/4 v0, 0x0

    .line 427
    iget-object v1, p0, Lmce;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 428
    const/4 v0, 0x1

    iget-object v1, p0, Lmce;->apiHeader:Llyq;

    .line 429
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 431
    :cond_0
    iget-object v1, p0, Lmce;->a:Lnla;

    if-eqz v1, :cond_1

    .line 432
    const/4 v1, 0x2

    iget-object v2, p0, Lmce;->a:Lnla;

    .line 433
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    :cond_1
    iget-object v1, p0, Lmce;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 436
    iput v0, p0, Lmce;->ai:I

    .line 437
    return v0
.end method

.method public a(Loxn;)Lmce;
    .locals 2

    .prologue
    .line 445
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 446
    sparse-switch v0, :sswitch_data_0

    .line 450
    iget-object v1, p0, Lmce;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 451
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmce;->ah:Ljava/util/List;

    .line 454
    :cond_1
    iget-object v1, p0, Lmce;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 456
    :sswitch_0
    return-object p0

    .line 461
    :sswitch_1
    iget-object v0, p0, Lmce;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 462
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmce;->apiHeader:Llyq;

    .line 464
    :cond_2
    iget-object v0, p0, Lmce;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 468
    :sswitch_2
    iget-object v0, p0, Lmce;->a:Lnla;

    if-nez v0, :cond_3

    .line 469
    new-instance v0, Lnla;

    invoke-direct {v0}, Lnla;-><init>()V

    iput-object v0, p0, Lmce;->a:Lnla;

    .line 471
    :cond_3
    iget-object v0, p0, Lmce;->a:Lnla;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 446
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Lmce;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 415
    const/4 v0, 0x1

    iget-object v1, p0, Lmce;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 417
    :cond_0
    iget-object v0, p0, Lmce;->a:Lnla;

    if-eqz v0, :cond_1

    .line 418
    const/4 v0, 0x2

    iget-object v1, p0, Lmce;->a:Lnla;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 420
    :cond_1
    iget-object v0, p0, Lmce;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 422
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 399
    invoke-virtual {p0, p1}, Lmce;->a(Loxn;)Lmce;

    move-result-object v0

    return-object v0
.end method
