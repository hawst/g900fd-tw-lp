.class public final Lmme;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:I

.field private f:Lofr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1130
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1148
    const/high16 v0, -0x80000000

    iput v0, p0, Lmme;->e:I

    .line 1151
    const/4 v0, 0x0

    iput-object v0, p0, Lmme;->f:Lofr;

    .line 1130
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1180
    const/4 v0, 0x0

    .line 1181
    iget-object v1, p0, Lmme;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1182
    const/4 v0, 0x1

    iget-object v1, p0, Lmme;->a:Ljava/lang/String;

    .line 1183
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1185
    :cond_0
    iget-object v1, p0, Lmme;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1186
    const/4 v1, 0x2

    iget-object v2, p0, Lmme;->b:Ljava/lang/String;

    .line 1187
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1189
    :cond_1
    iget-object v1, p0, Lmme;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1190
    const/4 v1, 0x3

    iget-object v2, p0, Lmme;->c:Ljava/lang/Integer;

    .line 1191
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1193
    :cond_2
    iget-object v1, p0, Lmme;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1194
    const/4 v1, 0x4

    iget-object v2, p0, Lmme;->d:Ljava/lang/Integer;

    .line 1195
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1197
    :cond_3
    iget v1, p0, Lmme;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 1198
    const/4 v1, 0x5

    iget v2, p0, Lmme;->e:I

    .line 1199
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1201
    :cond_4
    iget-object v1, p0, Lmme;->f:Lofr;

    if-eqz v1, :cond_5

    .line 1202
    const/4 v1, 0x6

    iget-object v2, p0, Lmme;->f:Lofr;

    .line 1203
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1205
    :cond_5
    iget-object v1, p0, Lmme;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1206
    iput v0, p0, Lmme;->ai:I

    .line 1207
    return v0
.end method

.method public a(Loxn;)Lmme;
    .locals 2

    .prologue
    .line 1215
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1216
    sparse-switch v0, :sswitch_data_0

    .line 1220
    iget-object v1, p0, Lmme;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1221
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmme;->ah:Ljava/util/List;

    .line 1224
    :cond_1
    iget-object v1, p0, Lmme;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1226
    :sswitch_0
    return-object p0

    .line 1231
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmme;->a:Ljava/lang/String;

    goto :goto_0

    .line 1235
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmme;->b:Ljava/lang/String;

    goto :goto_0

    .line 1239
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmme;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 1243
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmme;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 1247
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1248
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 1252
    :cond_2
    iput v0, p0, Lmme;->e:I

    goto :goto_0

    .line 1254
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmme;->e:I

    goto :goto_0

    .line 1259
    :sswitch_6
    iget-object v0, p0, Lmme;->f:Lofr;

    if-nez v0, :cond_4

    .line 1260
    new-instance v0, Lofr;

    invoke-direct {v0}, Lofr;-><init>()V

    iput-object v0, p0, Lmme;->f:Lofr;

    .line 1262
    :cond_4
    iget-object v0, p0, Lmme;->f:Lofr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1216
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1156
    iget-object v0, p0, Lmme;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1157
    const/4 v0, 0x1

    iget-object v1, p0, Lmme;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1159
    :cond_0
    iget-object v0, p0, Lmme;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1160
    const/4 v0, 0x2

    iget-object v1, p0, Lmme;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1162
    :cond_1
    iget-object v0, p0, Lmme;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1163
    const/4 v0, 0x3

    iget-object v1, p0, Lmme;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1165
    :cond_2
    iget-object v0, p0, Lmme;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1166
    const/4 v0, 0x4

    iget-object v1, p0, Lmme;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1168
    :cond_3
    iget v0, p0, Lmme;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_4

    .line 1169
    const/4 v0, 0x5

    iget v1, p0, Lmme;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1171
    :cond_4
    iget-object v0, p0, Lmme;->f:Lofr;

    if-eqz v0, :cond_5

    .line 1172
    const/4 v0, 0x6

    iget-object v1, p0, Lmme;->f:Lofr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1174
    :cond_5
    iget-object v0, p0, Lmme;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1176
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1126
    invoke-virtual {p0, p1}, Lmme;->a(Loxn;)Lmme;

    move-result-object v0

    return-object v0
.end method
