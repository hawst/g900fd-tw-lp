.class public final Lmpa;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmpa;


# instance fields
.field private b:Lmpb;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    new-array v0, v0, [Lmpa;

    sput-object v0, Lmpa;->a:[Lmpa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lmpa;->b:Lmpb;

    .line 16
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, Lmpa;->b:Lmpb;

    if-eqz v1, :cond_0

    .line 45
    const/4 v0, 0x1

    iget-object v1, p0, Lmpa;->b:Lmpb;

    .line 46
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 48
    :cond_0
    iget-object v1, p0, Lmpa;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 49
    const/4 v1, 0x2

    iget-object v2, p0, Lmpa;->c:Ljava/lang/String;

    .line 50
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    :cond_1
    iget-object v1, p0, Lmpa;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 53
    const/4 v1, 0x4

    iget-object v2, p0, Lmpa;->d:Ljava/lang/String;

    .line 54
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    :cond_2
    iget-object v1, p0, Lmpa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 57
    iput v0, p0, Lmpa;->ai:I

    .line 58
    return v0
.end method

.method public a(Loxn;)Lmpa;
    .locals 2

    .prologue
    .line 66
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 67
    sparse-switch v0, :sswitch_data_0

    .line 71
    iget-object v1, p0, Lmpa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmpa;->ah:Ljava/util/List;

    .line 75
    :cond_1
    iget-object v1, p0, Lmpa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    :sswitch_0
    return-object p0

    .line 82
    :sswitch_1
    iget-object v0, p0, Lmpa;->b:Lmpb;

    if-nez v0, :cond_2

    .line 83
    new-instance v0, Lmpb;

    invoke-direct {v0}, Lmpb;-><init>()V

    iput-object v0, p0, Lmpa;->b:Lmpb;

    .line 85
    :cond_2
    iget-object v0, p0, Lmpa;->b:Lmpb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 89
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpa;->c:Ljava/lang/String;

    goto :goto_0

    .line 93
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmpa;->d:Ljava/lang/String;

    goto :goto_0

    .line 67
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lmpa;->b:Lmpb;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iget-object v1, p0, Lmpa;->b:Lmpb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 31
    :cond_0
    iget-object v0, p0, Lmpa;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 32
    const/4 v0, 0x2

    iget-object v1, p0, Lmpa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 34
    :cond_1
    iget-object v0, p0, Lmpa;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 35
    const/4 v0, 0x4

    iget-object v1, p0, Lmpa;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 37
    :cond_2
    iget-object v0, p0, Lmpa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 39
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lmpa;->a(Loxn;)Lmpa;

    move-result-object v0

    return-object v0
.end method
