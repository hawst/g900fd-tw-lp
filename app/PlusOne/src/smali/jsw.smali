.class public final Ljsw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lhzm;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljsw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljpv;

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Ljsx;

    invoke-direct {v0}, Ljsx;-><init>()V

    sput-object v0, Ljsw;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-boolean v1, p0, Ljsw;->b:Z

    .line 32
    const-class v0, Ljsw;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljpv;

    iput-object v0, p0, Ljsw;->a:Ljpv;

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ljsw;->b:Z

    .line 34
    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljsy;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljsw;->b:Z

    .line 27
    iget-object v0, p1, Ljsy;->a:Ljpv;

    iput-object v0, p0, Ljsw;->a:Ljpv;

    .line 28
    iget-boolean v0, p1, Ljsy;->b:Z

    iput-boolean v0, p0, Ljsw;->b:Z

    .line 29
    return-void
.end method

.method public static a()Ljsy;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Ljsy;

    invoke-direct {v0}, Ljsy;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    packed-switch p1, :pswitch_data_0

    .line 50
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 48
    :pswitch_0
    invoke-virtual {p0}, Ljsw;->b()Ljpv;

    move-result-object v0

    invoke-interface {v0}, Ljpv;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public b()Ljpv;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ljsw;->a:Ljpv;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Ljsw;->b:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ljsw;->a:Ljpv;

    invoke-interface {v0}, Ljpv;->describeContents()I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 61
    instance-of v0, p1, Ljsw;

    if-eqz v0, :cond_0

    .line 62
    check-cast p1, Ljsw;

    .line 63
    iget-object v0, p0, Ljsw;->a:Ljpv;

    iget-object v1, p1, Ljsw;->a:Ljpv;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 65
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Ljsw;->a:Ljpv;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ljsw;->a:Ljpv;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Ljsw;->a:Ljpv;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 81
    iget-boolean v0, p0, Ljsw;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    return-void

    .line 81
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
