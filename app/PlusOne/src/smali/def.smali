.class public Ldef;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljlw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljlw",
        "<",
        "Ldef;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljlx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlx",
            "<",
            "Ldef;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/graphics/Rect;

.field private c:Z

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ldef;->b:Landroid/graphics/Rect;

    .line 38
    new-instance v0, Ljlv;

    invoke-direct {v0, p0}, Ljlv;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Ldef;->a:Ljlx;

    .line 39
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 103
    iput p1, p0, Ldef;->d:I

    .line 104
    invoke-virtual {p0}, Ldef;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 105
    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ldef;->b:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 79
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 70
    iput-boolean p1, p0, Ldef;->c:Z

    .line 71
    invoke-virtual {p0}, Ldef;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 72
    return-void
.end method

.method public b()Ljlx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljlx",
            "<",
            "Ldef;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Ldef;->a:Ljlx;

    return-object v0
.end method

.method public b(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Ldef;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 86
    invoke-virtual {p0}, Ldef;->b()Ljlx;

    move-result-object v0

    invoke-interface {v0}, Ljlx;->a()V

    .line 87
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 66
    iget-boolean v0, p0, Ldef;->c:Z

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Ldef;->d:I

    return v0
.end method
