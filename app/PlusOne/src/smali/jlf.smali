.class public final Ljlf;
.super Lhyp;
.source "PG"

# interfaces
.implements Ljll;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhyp;",
        "Ljll",
        "<",
        "Ljle;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljlh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlh",
            "<",
            "Ljle;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljlm;

.field private d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljle;",
            "Ljkx;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljle;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljlk;)V
    .locals 4

    .prologue
    .line 27
    invoke-direct {p0}, Lhyp;-><init>()V

    .line 28
    iput-object p1, p0, Ljlf;->a:Landroid/content/Context;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ljlf;->d:Ljava/util/HashMap;

    .line 30
    sget-object v0, Ljle;->a:Ljle;

    iput-object v0, p0, Ljlf;->e:Ljle;

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 33
    new-instance v1, Ljlh;

    invoke-direct {v1, p1, p0, p2}, Ljlh;-><init>(Landroid/content/Context;Ljll;Ljlk;)V

    iput-object v1, p0, Ljlf;->b:Ljlh;

    .line 34
    iget-object v1, p0, Ljlf;->b:Ljlh;

    sget-object v2, Ljle;->a:Ljle;

    const v3, 0x7f0a039f

    .line 36
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 34
    invoke-virtual {v1, v2, v3}, Ljlh;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v1, p0, Ljlf;->b:Ljlh;

    sget-object v2, Ljle;->c:Ljle;

    const v3, 0x7f0a03a0

    .line 39
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 37
    invoke-virtual {v1, v2, v3}, Ljlh;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v1, p0, Ljlf;->b:Ljlh;

    sget-object v2, Ljle;->b:Ljle;

    const v3, 0x7f0a03a1

    .line 42
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 40
    invoke-virtual {v1, v2, v0}, Ljlh;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Ljlf;->b:Ljlh;

    sget-object v1, Ljle;->a:Ljle;

    invoke-virtual {v0, v1}, Ljlh;->a(Ljava/lang/Object;)Z

    .line 44
    iget-object v0, p0, Ljlf;->b:Ljlh;

    invoke-virtual {p0, v0}, Ljlf;->a(Landroid/widget/Adapter;)V

    .line 46
    new-instance v0, Ljlm;

    iget-object v1, p0, Ljlf;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljlm;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Ljlf;->c:Ljlm;

    .line 47
    iget-object v0, p0, Ljlf;->c:Ljlm;

    invoke-virtual {p0, v0}, Ljlf;->a(Landroid/widget/Adapter;)V

    .line 48
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Ljle;

    invoke-virtual {p0, p1}, Ljlf;->a(Ljle;)V

    return-void
.end method

.method public a(Ljle;)V
    .locals 2

    .prologue
    .line 53
    iput-object p1, p0, Ljlf;->e:Ljle;

    .line 54
    iget-object v1, p0, Ljlf;->c:Ljlm;

    iget-object v0, p0, Ljlf;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {v1, v0}, Ljlm;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 55
    return-void
.end method

.method public a(Ljle;Ljkx;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Ljlf;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, Ljlf;->e:Ljle;

    if-ne v0, p1, :cond_0

    .line 69
    iget-object v0, p0, Ljlf;->c:Ljlm;

    invoke-virtual {v0, p2}, Ljlm;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 71
    :cond_0
    return-void
.end method
