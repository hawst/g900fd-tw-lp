.class public final Loiu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loiu;


# instance fields
.field public b:Lohv;

.field public c:Loix;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/Double;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Lohn;

.field private i:Ljava/lang/Integer;

.field private j:Loiv;

.field private k:Ljava/lang/Boolean;

.field private l:[Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4260
    const/4 v0, 0x0

    new-array v0, v0, [Loiu;

    sput-object v0, Loiu;->a:[Loiu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4261
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4264
    iput-object v0, p0, Loiu;->b:Lohv;

    .line 4269
    iput-object v0, p0, Loiu;->c:Loix;

    .line 4276
    iput-object v0, p0, Loiu;->h:Lohn;

    .line 4283
    iput-object v0, p0, Loiu;->j:Loiv;

    .line 4288
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loiu;->l:[Ljava/lang/String;

    .line 4261
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 4339
    .line 4340
    iget-object v0, p0, Loiu;->b:Lohv;

    if-eqz v0, :cond_c

    .line 4341
    const/4 v0, 0x1

    iget-object v2, p0, Loiu;->b:Lohv;

    .line 4342
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4344
    :goto_0
    iget-object v2, p0, Loiu;->e:Ljava/lang/Double;

    if-eqz v2, :cond_0

    .line 4345
    const/4 v2, 0x2

    iget-object v3, p0, Loiu;->e:Ljava/lang/Double;

    .line 4346
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 4348
    :cond_0
    iget-object v2, p0, Loiu;->c:Loix;

    if-eqz v2, :cond_1

    .line 4349
    const/4 v2, 0x3

    iget-object v3, p0, Loiu;->c:Loix;

    .line 4350
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4352
    :cond_1
    iget-object v2, p0, Loiu;->f:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 4353
    const/4 v2, 0x5

    iget-object v3, p0, Loiu;->f:Ljava/lang/Integer;

    .line 4354
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4356
    :cond_2
    iget-object v2, p0, Loiu;->g:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    .line 4357
    const/4 v2, 0x6

    iget-object v3, p0, Loiu;->g:Ljava/lang/Integer;

    .line 4358
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4360
    :cond_3
    iget-object v2, p0, Loiu;->l:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Loiu;->l:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 4362
    iget-object v3, p0, Loiu;->l:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 4364
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 4362
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4366
    :cond_4
    add-int/2addr v0, v2

    .line 4367
    iget-object v1, p0, Loiu;->l:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4369
    :cond_5
    iget-object v1, p0, Loiu;->h:Lohn;

    if-eqz v1, :cond_6

    .line 4370
    const/16 v1, 0x8

    iget-object v2, p0, Loiu;->h:Lohn;

    .line 4371
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4373
    :cond_6
    iget-object v1, p0, Loiu;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 4374
    const/16 v1, 0x9

    iget-object v2, p0, Loiu;->i:Ljava/lang/Integer;

    .line 4375
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4377
    :cond_7
    iget-object v1, p0, Loiu;->m:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 4378
    const/16 v1, 0xa

    iget-object v2, p0, Loiu;->m:Ljava/lang/String;

    .line 4379
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4381
    :cond_8
    iget-object v1, p0, Loiu;->d:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 4382
    const/16 v1, 0xb

    iget-object v2, p0, Loiu;->d:Ljava/lang/String;

    .line 4383
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4385
    :cond_9
    iget-object v1, p0, Loiu;->j:Loiv;

    if-eqz v1, :cond_a

    .line 4386
    const/16 v1, 0xc

    iget-object v2, p0, Loiu;->j:Loiv;

    .line 4387
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4389
    :cond_a
    iget-object v1, p0, Loiu;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 4390
    const/16 v1, 0xd

    iget-object v2, p0, Loiu;->k:Ljava/lang/Boolean;

    .line 4391
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4393
    :cond_b
    iget-object v1, p0, Loiu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4394
    iput v0, p0, Loiu;->ai:I

    .line 4395
    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loiu;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4403
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4404
    sparse-switch v0, :sswitch_data_0

    .line 4408
    iget-object v1, p0, Loiu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4409
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loiu;->ah:Ljava/util/List;

    .line 4412
    :cond_1
    iget-object v1, p0, Loiu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4414
    :sswitch_0
    return-object p0

    .line 4419
    :sswitch_1
    iget-object v0, p0, Loiu;->b:Lohv;

    if-nez v0, :cond_2

    .line 4420
    new-instance v0, Lohv;

    invoke-direct {v0}, Lohv;-><init>()V

    iput-object v0, p0, Loiu;->b:Lohv;

    .line 4422
    :cond_2
    iget-object v0, p0, Loiu;->b:Lohv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4426
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Loiu;->e:Ljava/lang/Double;

    goto :goto_0

    .line 4430
    :sswitch_3
    iget-object v0, p0, Loiu;->c:Loix;

    if-nez v0, :cond_3

    .line 4431
    new-instance v0, Loix;

    invoke-direct {v0}, Loix;-><init>()V

    iput-object v0, p0, Loiu;->c:Loix;

    .line 4433
    :cond_3
    iget-object v0, p0, Loiu;->c:Loix;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4437
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loiu;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 4441
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loiu;->g:Ljava/lang/Integer;

    goto :goto_0

    .line 4445
    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 4446
    iget-object v0, p0, Loiu;->l:[Ljava/lang/String;

    array-length v0, v0

    .line 4447
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 4448
    iget-object v2, p0, Loiu;->l:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4449
    iput-object v1, p0, Loiu;->l:[Ljava/lang/String;

    .line 4450
    :goto_1
    iget-object v1, p0, Loiu;->l:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 4451
    iget-object v1, p0, Loiu;->l:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 4452
    invoke-virtual {p1}, Loxn;->a()I

    .line 4450
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4455
    :cond_4
    iget-object v1, p0, Loiu;->l:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 4459
    :sswitch_7
    iget-object v0, p0, Loiu;->h:Lohn;

    if-nez v0, :cond_5

    .line 4460
    new-instance v0, Lohn;

    invoke-direct {v0}, Lohn;-><init>()V

    iput-object v0, p0, Loiu;->h:Lohn;

    .line 4462
    :cond_5
    iget-object v0, p0, Loiu;->h:Lohn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4466
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loiu;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 4470
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loiu;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 4474
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loiu;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 4478
    :sswitch_b
    iget-object v0, p0, Loiu;->j:Loiv;

    if-nez v0, :cond_6

    .line 4479
    new-instance v0, Loiv;

    invoke-direct {v0}, Loiv;-><init>()V

    iput-object v0, p0, Loiu;->j:Loiv;

    .line 4481
    :cond_6
    iget-object v0, p0, Loiu;->j:Loiv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 4485
    :sswitch_c
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loiu;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 4404
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x68 -> :sswitch_c
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 4295
    iget-object v0, p0, Loiu;->b:Lohv;

    if-eqz v0, :cond_0

    .line 4296
    const/4 v0, 0x1

    iget-object v1, p0, Loiu;->b:Lohv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4298
    :cond_0
    iget-object v0, p0, Loiu;->e:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 4299
    const/4 v0, 0x2

    iget-object v1, p0, Loiu;->e:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 4301
    :cond_1
    iget-object v0, p0, Loiu;->c:Loix;

    if-eqz v0, :cond_2

    .line 4302
    const/4 v0, 0x3

    iget-object v1, p0, Loiu;->c:Loix;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4304
    :cond_2
    iget-object v0, p0, Loiu;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 4305
    const/4 v0, 0x5

    iget-object v1, p0, Loiu;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4307
    :cond_3
    iget-object v0, p0, Loiu;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 4308
    const/4 v0, 0x6

    iget-object v1, p0, Loiu;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4310
    :cond_4
    iget-object v0, p0, Loiu;->l:[Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 4311
    iget-object v1, p0, Loiu;->l:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 4312
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 4311
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4315
    :cond_5
    iget-object v0, p0, Loiu;->h:Lohn;

    if-eqz v0, :cond_6

    .line 4316
    const/16 v0, 0x8

    iget-object v1, p0, Loiu;->h:Lohn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4318
    :cond_6
    iget-object v0, p0, Loiu;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 4319
    const/16 v0, 0x9

    iget-object v1, p0, Loiu;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4321
    :cond_7
    iget-object v0, p0, Loiu;->m:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 4322
    const/16 v0, 0xa

    iget-object v1, p0, Loiu;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4324
    :cond_8
    iget-object v0, p0, Loiu;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 4325
    const/16 v0, 0xb

    iget-object v1, p0, Loiu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4327
    :cond_9
    iget-object v0, p0, Loiu;->j:Loiv;

    if-eqz v0, :cond_a

    .line 4328
    const/16 v0, 0xc

    iget-object v1, p0, Loiu;->j:Loiv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4330
    :cond_a
    iget-object v0, p0, Loiu;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 4331
    const/16 v0, 0xd

    iget-object v1, p0, Loiu;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 4333
    :cond_b
    iget-object v0, p0, Loiu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4335
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4257
    invoke-virtual {p0, p1}, Loiu;->a(Loxn;)Loiu;

    move-result-object v0

    return-object v0
.end method
