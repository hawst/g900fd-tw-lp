.class public final Lpwh;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpwh;


# instance fields
.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    new-array v0, v0, [Lpwh;

    sput-object v0, Lpwh;->a:[Lpwh;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 36
    const/4 v0, 0x1

    iget-object v1, p0, Lpwh;->b:Ljava/lang/Long;

    .line 38
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 39
    const/4 v1, 0x2

    iget-object v2, p0, Lpwh;->c:Ljava/lang/String;

    .line 40
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41
    const/4 v1, 0x3

    iget-object v2, p0, Lpwh;->d:Ljava/lang/String;

    .line 42
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43
    iget-object v1, p0, Lpwh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44
    iput v0, p0, Lpwh;->ai:I

    .line 45
    return v0
.end method

.method public a(Loxn;)Lpwh;
    .locals 2

    .prologue
    .line 53
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 54
    sparse-switch v0, :sswitch_data_0

    .line 58
    iget-object v1, p0, Lpwh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 59
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpwh;->ah:Ljava/util/List;

    .line 62
    :cond_1
    iget-object v1, p0, Lpwh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    :sswitch_0
    return-object p0

    .line 69
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpwh;->b:Ljava/lang/Long;

    goto :goto_0

    .line 73
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpwh;->c:Ljava/lang/String;

    goto :goto_0

    .line 77
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpwh;->d:Ljava/lang/String;

    goto :goto_0

    .line 54
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 27
    const/4 v0, 0x1

    iget-object v1, p0, Lpwh;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 28
    const/4 v0, 0x2

    iget-object v1, p0, Lpwh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 29
    const/4 v0, 0x3

    iget-object v1, p0, Lpwh;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 30
    iget-object v0, p0, Lpwh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lpwh;->a(Loxn;)Lpwh;

    move-result-object v0

    return-object v0
.end method
