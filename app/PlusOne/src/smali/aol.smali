.class public Laol;
.super Lamn;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Laic;


# instance fields
.field private final A:Laru;

.field private final B:Lalr;

.field private final C:Laqy;

.field private final D:Lamb;

.field private final E:Laum;

.field private final F:Lava;

.field private final G:Lase;

.field private final H:Laxd;

.field private final I:Lanj;

.field private final J:Laso;

.field private final K:Lawq;

.field private final L:Lapu;

.field private final M:Larp;

.field private N:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<***>;"
        }
    .end annotation
.end field

.field private O:Z

.field private P:Lbue;

.field private final Q:Lapd;

.field private final R:Lapf;

.field private final S:Lapq;

.field private final T:Laiz;

.field private final U:Laqm;

.field private final V:Lasa;

.field private final W:Lawk;

.field private final X:Lawk;

.field private final Y:Laqb;

.field private final Z:Laoy;

.field private final aa:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Laox;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:Lawc;

.field private final e:Lapg;

.field private final f:Lbqg;

.field private final g:Lapp;

.field private final h:Laiw;

.field private final j:Lbqo;

.field private final k:Lawy;

.field private final l:Lalg;

.field private final m:Lalo;

.field private final n:Lany;

.field private final o:Laoh;

.field private final p:Lasy;

.field private final q:Lcdu;

.field private final r:Lavj;

.field private final s:Laxp;

.field private final t:Lapl;

.field private final u:Lbyf;

.field private final v:Land;

.field private final w:Lajl;

.field private final x:Ljfb;

.field private final y:Lanh;

.field private final z:Laqc;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 112
    const-class v0, Laol;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laol;->a:Ljava/lang/String;

    .line 119
    new-instance v0, Laic;

    sget-object v1, Laib;->c:Laib;

    sget-object v2, Lahx;->b:[Lahz;

    invoke-direct {v0, v1, v2}, Laic;-><init>(Laib;[Lahz;)V

    sput-object v0, Laol;->b:Laic;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lawc;Laqa;Laxu;Lapg;Lbqg;Lapp;Laro;Lahx;Laiw;Lbqo;Lbig;Lbjp;Lbjp;Lbjl;Lbgf;Lbte;Lbjf;Lalg;Lalg;Lalo;Lany;Laoh;Lchl;Lchl;Ljava/util/concurrent/Executor;Lchk;Lasy;Laqx;Lanh;Lald;Lamy;Lcdu;Lapl;Lavj;Laxp;Lced;Lbyf;Lawy;Land;Lajl;ZLali;Ljfb;Laxt;Laxz;Lbrh;Lbzf;Llmg;Llmg;Ljava/io/File;Lcfg;Lcez;Laxw;)V
    .locals 37
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lawc;",
            "Laqa;",
            "Laxu;",
            "Lapg;",
            "Lbqg;",
            "Lapp;",
            "Laro;",
            "Lahx;",
            "Laiw;",
            "Lbqo;",
            "Lbig;",
            "Lbjp",
            "<",
            "Lbhk;",
            ">;",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;",
            "Lbjl",
            "<",
            "Lbhl;",
            ">;",
            "Lbgf;",
            "Lbte;",
            "Lbjf;",
            "Lalg;",
            "Lalg;",
            "Lalo;",
            "Lany;",
            "Laoh;",
            "Lchl;",
            "Lchl;",
            "Ljava/util/concurrent/Executor;",
            "Lchk;",
            "Lasy;",
            "Laqx;",
            "Lanh;",
            "Lald;",
            "Lamy;",
            "Lcdu;",
            "Lapl;",
            "Lavj;",
            "Laxp;",
            "Lced;",
            "Lbyf;",
            "Lawy;",
            "Land;",
            "Lajl;",
            "Z",
            "Lali;",
            "Ljfb;",
            "Laxt;",
            "Laxz;",
            "Lbrh;",
            "Lbzf;",
            "Llmg;",
            "Llmg;",
            "Ljava/io/File;",
            "Lcfg;",
            "Lcez;",
            "Laxw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 491
    invoke-direct/range {p0 .. p0}, Lamn;-><init>()V

    .line 299
    new-instance v3, Lapd;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lapd;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->Q:Lapd;

    .line 301
    new-instance v3, Lapf;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lapf;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->R:Lapf;

    .line 303
    new-instance v3, Laom;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Laom;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->S:Lapq;

    .line 314
    new-instance v3, Lapb;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lapb;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->T:Laiz;

    .line 316
    new-instance v3, Laon;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Laon;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->U:Laqm;

    .line 335
    new-instance v3, Lape;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lape;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->V:Lasa;

    .line 338
    new-instance v3, Laoo;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Laoo;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->W:Lawk;

    .line 348
    new-instance v3, Laop;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Laop;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->X:Lawk;

    .line 356
    new-instance v3, Laoq;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Laoq;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->Y:Laqb;

    .line 373
    new-instance v3, Laor;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Laor;-><init>(Laol;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->Z:Laoy;

    .line 386
    new-instance v3, Laos;

    const-class v4, Laox;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Laos;-><init>(Laol;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->aa:Lasn;

    .line 492
    const-string v3, "context"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->c:Landroid/content/Context;

    .line 493
    const-string v3, "mainState"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lawc;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->d:Lawc;

    .line 494
    const-string v3, "ui"

    const/4 v4, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lapg;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->e:Lapg;

    .line 495
    const-string v3, "uriMimeTypeRetriever"

    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbqg;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->f:Lbqg;

    .line 496
    const-string v3, "display"

    const/4 v4, 0x0

    move-object/from16 v0, p7

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lapp;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->g:Lapp;

    .line 497
    const-string v3, "batchAnalyzer"

    const/4 v4, 0x0

    move-object/from16 v0, p10

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laiw;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->h:Laiw;

    .line 498
    const-string v3, "posterStore"

    const/4 v4, 0x0

    move-object/from16 v0, p11

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbqo;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->j:Lbqo;

    .line 499
    const-string v3, "serialAsyncTaskRunner"

    .line 500
    const/4 v4, 0x0

    move-object/from16 v0, p19

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lalg;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->l:Lalg;

    .line 501
    const-string v3, "checkInputUrisTaskFactory"

    .line 502
    const/4 v4, 0x0

    move-object/from16 v0, p21

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lalo;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->m:Lalo;

    .line 503
    const-string v3, "loadCloudMediaTaskFactory"

    .line 504
    const/4 v4, 0x0

    move-object/from16 v0, p22

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lany;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->n:Lany;

    .line 505
    const-string v3, "loadClusterMediaTaskFactory"

    .line 506
    const/4 v4, 0x0

    move-object/from16 v0, p23

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laoh;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->o:Laoh;

    .line 507
    const-string v3, "stateTracker"

    const/4 v4, 0x0

    move-object/from16 v0, p28

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lasy;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->p:Lasy;

    .line 508
    const-string v3, "analyticsSession"

    const/4 v4, 0x0

    move-object/from16 v0, p33

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcdu;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->q:Lcdu;

    .line 509
    const-string v3, "mediaUriFetcher"

    const/4 v4, 0x0

    move-object/from16 v0, p34

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lapl;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->t:Lapl;

    .line 510
    const-string v3, "soundtrackUsageRecorder"

    const/4 v4, 0x0

    move-object/from16 v0, p35

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lavj;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->r:Lavj;

    .line 512
    const-string v3, "userHintDecider"

    const/4 v4, 0x0

    move-object/from16 v0, p36

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laxp;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->s:Laxp;

    .line 513
    const-string v3, "autoAttributesApplier"

    const/4 v4, 0x0

    move-object/from16 v0, p38

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbyf;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->u:Lbyf;

    .line 515
    const-string v3, "downloadMediaTaskFactory"

    const/4 v4, 0x0

    move-object/from16 v0, p40

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Land;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->v:Land;

    .line 517
    const-string v3, "metricsStore"

    const/4 v4, 0x0

    move-object/from16 v0, p41

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lajl;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->w:Lajl;

    .line 518
    const-string v3, "movieMakerProvider"

    const/4 v4, 0x0

    move-object/from16 v0, p44

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljfb;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->x:Ljfb;

    .line 519
    const-string v3, "clipEditorDecoderFactory"

    const/4 v4, 0x0

    move-object/from16 v0, p15

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 521
    new-instance v3, Laso;

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->d:Lawc;

    .line 522
    invoke-interface {v4}, Lawc;->am()Lawp;

    move-result-object v4

    move-object/from16 v0, p44

    move-object/from16 v1, p19

    move-object/from16 v2, p43

    invoke-direct {v3, v4, v0, v1, v2}, Laso;-><init>(Lawp;Ljfb;Lalg;Lali;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->J:Laso;

    .line 527
    new-instance v3, Lamb;

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->d:Lawc;

    .line 528
    invoke-interface {v4}, Lawc;->ar()Lavw;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Laol;->q:Lcdu;

    move-object/from16 v0, p0

    iget-object v9, v0, Laol;->g:Lapp;

    move-object/from16 v5, p45

    move-object/from16 v6, p46

    move-object/from16 v7, p4

    move-object/from16 v10, p54

    move-object/from16 v11, p20

    invoke-direct/range {v3 .. v11}, Lamb;-><init>(Lavw;Laxt;Laxz;Laxu;Lcdu;Lamd;Laxw;Lalg;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->D:Lamb;

    .line 537
    new-instance v3, Lava;

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->d:Lawc;

    .line 539
    invoke-interface {v4}, Lawc;->ap()Lawi;

    move-result-object v4

    move-object/from16 v0, p20

    move-object/from16 v1, p47

    invoke-direct {v3, v0, v4, v1}, Lava;-><init>(Lalg;Lawi;Lbrh;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->F:Lava;

    .line 542
    new-instance v3, Laum;

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->F:Lava;

    move-object/from16 v0, p0

    iget-object v5, v0, Laol;->d:Lawc;

    .line 544
    invoke-interface {v5}, Lawc;->aj()Lawj;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Laol;->r:Lavj;

    .line 546
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0103

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 547
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0104

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Laol;->s:Laxp;

    move-object/from16 v0, p0

    iget-object v10, v0, Laol;->q:Lcdu;

    new-instance v11, Lauy;

    move-object/from16 v0, p0

    iget-object v12, v0, Laol;->c:Landroid/content/Context;

    invoke-direct {v11, v12}, Lauy;-><init>(Landroid/content/Context;)V

    invoke-direct/range {v3 .. v11}, Laum;-><init>(Laur;Lawj;Lavj;Ljava/lang/String;Ljava/lang/String;Laxp;Lcdu;Lauy;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->E:Laum;

    .line 552
    new-instance v3, Lapu;

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->d:Lawc;

    .line 553
    invoke-interface {v4}, Lawc;->at()Lawd;

    move-result-object v4

    invoke-direct {v3, v4}, Lapu;-><init>(Lawd;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->L:Lapu;

    .line 555
    new-instance v20, Lbzb;

    .line 556
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->d:Lawc;

    invoke-interface {v4}, Lawc;->C()Lama;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4}, Lbzb;-><init>(Landroid/content/res/Resources;Lama;)V

    .line 558
    const-class v3, Laol;

    const-string v4, "decoder"

    .line 559
    move-object/from16 v0, p27

    invoke-interface {v0, v3, v4}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v12

    .line 560
    new-instance v30, Laqc;

    move-object/from16 v0, p0

    iget-object v3, v0, Laol;->d:Lawc;

    .line 562
    invoke-interface {v3}, Lawc;->ah()Lawf;

    move-result-object v15

    new-instance v3, Lbsc;

    move-object/from16 v4, p1

    move-object/from16 v5, p12

    move-object/from16 v6, p13

    move-object/from16 v7, p14

    move-object/from16 v8, p16

    move-object/from16 v9, p17

    move-object/from16 v10, p28

    move-object/from16 v11, p26

    move-object/from16 v13, p25

    move-object/from16 v14, p18

    invoke-direct/range {v3 .. v14}, Lbsc;-><init>(Landroid/content/Context;Lbig;Lbjp;Lbjp;Lbgf;Lbte;Lasy;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lchl;Lbjf;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->q:Lcdu;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->U:Laqm;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->Y:Laqb;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->g:Lapp;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->r:Lavj;

    move-object/from16 v29, v0

    move-object/from16 v13, v30

    move-object/from16 v14, p1

    move-object/from16 v16, p16

    move-object/from16 v17, v3

    move-object/from16 v18, p28

    move-object/from16 v19, p19

    move-object/from16 v21, p29

    move-object/from16 v22, p30

    move-object/from16 v26, p36

    move-object/from16 v27, p37

    invoke-direct/range {v13 .. v29}, Laqc;-><init>(Landroid/content/Context;Lawf;Lbgf;Lbtp;Lasy;Lalg;Lbzb;Laqx;Lanh;Lcdu;Laqm;Laqb;Laxp;Lced;Laqt;Lavj;)V

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Laol;->z:Laqc;

    .line 589
    const-string v3, "gservicesSettings"

    const/4 v4, 0x0

    move-object/from16 v0, p30

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lanh;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->y:Lanh;

    .line 590
    const-string v3, "editingSessionStoreProvider"

    const/4 v4, 0x0

    move-object/from16 v0, p39

    invoke-static {v0, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lawy;

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->k:Lawy;

    .line 593
    new-instance v17, Laxd;

    move-object/from16 v0, p0

    iget-object v3, v0, Laol;->d:Lawc;

    .line 594
    invoke-interface {v3}, Lawc;->ai()Lawm;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->y:Lanh;

    move-object/from16 v21, v0

    new-instance v22, Laxc;

    move-object/from16 v0, p0

    iget-object v3, v0, Laol;->d:Lawc;

    .line 599
    invoke-interface {v3}, Lawc;->al()Lavy;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->k:Lawy;

    move-object/from16 v0, v22

    invoke-direct {v0, v3, v4}, Laxc;-><init>(Lavy;Lawy;)V

    move-object/from16 v19, p19

    move-object/from16 v23, p48

    invoke-direct/range {v17 .. v23}, Laxd;-><init>(Lawm;Lalg;Lbzb;Lbzg;Laxc;Lbzf;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Laol;->H:Laxd;

    .line 602
    const/4 v3, 0x2

    const-class v4, Laol;

    const-string v5, "poster-decoding"

    .line 603
    move-object/from16 v0, p27

    invoke-interface {v0, v3, v4, v5}, Lchk;->a(ILjava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v21

    .line 605
    new-instance v10, Lbze;

    move-object/from16 v0, p0

    iget-object v3, v0, Laol;->d:Lawc;

    invoke-interface {v3}, Lawc;->C()Lama;

    move-result-object v3

    invoke-direct {v10, v3}, Lbze;-><init>(Lama;)V

    .line 607
    new-instance v3, Lawq;

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->d:Lawc;

    .line 608
    invoke-interface {v4}, Lawc;->au()Lavu;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v8, v0, Laol;->y:Lanh;

    move-object/from16 v5, p20

    move-object/from16 v6, p51

    move-object/from16 v7, p52

    invoke-direct/range {v3 .. v8}, Lawq;-><init>(Lavu;Lalg;Ljava/io/File;Lcfg;Lalf;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->K:Lawq;

    .line 613
    new-instance v3, Laru;

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->j:Lbqo;

    move-object/from16 v0, p0

    iget-object v6, v0, Laol;->t:Lapl;

    move-object/from16 v0, p0

    iget-object v7, v0, Laol;->q:Lcdu;

    move-object/from16 v0, p0

    iget-object v5, v0, Laol;->d:Lawc;

    .line 618
    invoke-interface {v5}, Lawc;->an()Lawl;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Laol;->g:Lapp;

    move-object/from16 v0, p0

    iget-object v11, v0, Laol;->V:Lasa;

    move-object/from16 v5, v21

    invoke-direct/range {v3 .. v11}, Laru;-><init>(Lbqo;Ljava/util/concurrent/Executor;Lapl;Lcdu;Lawl;Larz;Lbze;Lasa;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->A:Laru;

    .line 622
    new-instance v11, Lalr;

    new-instance v3, Lbsh;

    move-object/from16 v4, p14

    move-object/from16 v5, p15

    move-object/from16 v6, p12

    move-object/from16 v7, p27

    move-object/from16 v8, p16

    move-object/from16 v9, p28

    invoke-direct/range {v3 .. v9}, Lbsh;-><init>(Lbjp;Lbjl;Lbig;Lchk;Lbgf;Lasy;)V

    new-instance v13, Lbsh;

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p12

    move-object/from16 v17, p27

    move-object/from16 v18, p16

    move-object/from16 v19, p28

    invoke-direct/range {v13 .. v19}, Lbsh;-><init>(Lbjp;Lbjl;Lbig;Lchk;Lbgf;Lasy;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->d:Lawc;

    .line 639
    invoke-interface {v4}, Lawc;->ao()Lavv;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->y:Lanh;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->q:Lcdu;

    move-object/from16 v22, v0

    move-object v14, v11

    move-object/from16 v15, p16

    move-object/from16 v16, p11

    move-object/from16 v17, v3

    move-object/from16 v18, v13

    invoke-direct/range {v14 .. v22}, Lalr;-><init>(Lbgf;Lbqo;Lbtu;Lbtu;Lavv;Lbzg;Ljava/util/concurrent/Executor;Lcdu;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Laol;->B:Lalr;

    .line 643
    new-instance v13, Laqy;

    move-object/from16 v0, p0

    iget-object v3, v0, Laol;->d:Lawc;

    .line 645
    invoke-interface {v3}, Lawc;->aq()Lawh;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->g:Lapp;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->q:Lcdu;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Laol;->Y:Laqb;

    move-object/from16 v33, v0

    new-instance v35, Laot;

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Laot;-><init>(Laol;)V

    move-object/from16 v14, p1

    move-object/from16 v16, p3

    move-object/from16 v17, p44

    move-object/from16 v18, p13

    move-object/from16 v19, p14

    move-object/from16 v20, p16

    move-object/from16 v21, p17

    move-object/from16 v22, p18

    move-object/from16 v23, p12

    move-object/from16 v24, p8

    move-object/from16 v26, p24

    move-object/from16 v27, p31

    move-object/from16 v28, p32

    move-object/from16 v29, p28

    move-object/from16 v31, p26

    move-object/from16 v32, v12

    move-object/from16 v34, p30

    move/from16 v36, p42

    invoke-direct/range {v13 .. v36}, Laqy;-><init>(Landroid/content/Context;Lawh;Laqa;Ljfb;Lbjp;Lbjp;Lbgf;Lbte;Lbjf;Lbig;Laro;Larj;Lchl;Lald;Lamy;Lasy;Lcdu;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Laqb;Lanh;Lark;Z)V

    move-object/from16 v0, p0

    iput-object v13, v0, Laol;->C:Laqy;

    .line 675
    new-instance v3, Lase;

    move-object/from16 v0, p0

    iget-object v4, v0, Laol;->d:Lawc;

    .line 676
    invoke-interface {v4}, Lawc;->ak()Lawn;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Laol;->q:Lcdu;

    move-object/from16 v0, p30

    invoke-direct {v3, v4, v0, v5}, Lase;-><init>(Lawn;Lbtd;Lcdu;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->G:Lase;

    .line 680
    new-instance v4, Lanj;

    move-object/from16 v0, p0

    iget-object v3, v0, Laol;->d:Lawc;

    .line 682
    invoke-interface {v3}, Lawc;->as()Lawb;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Laol;->d:Lawc;

    .line 685
    invoke-interface {v3}, Lawc;->C()Lama;

    move-result-object v9

    move-object/from16 v5, p9

    move-object/from16 v7, p19

    move-object/from16 v8, p21

    move-object/from16 v11, p7

    move-object/from16 v12, p40

    invoke-direct/range {v4 .. v12}, Lanj;-><init>(Lahx;Lawb;Lalg;Lalo;Lama;Lbze;Lann;Land;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Laol;->I:Lanj;

    .line 690
    new-instance v3, Larp;

    const-class v4, Larp;

    const-string v5, "cache-cleanup"

    .line 691
    move-object/from16 v0, p27

    invoke-interface {v0, v4, v5}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    move-object/from16 v0, p49

    move-object/from16 v1, p50

    move-object/from16 v2, p53

    invoke-direct {v3, v4, v0, v1, v2}, Larp;-><init>(Ljava/util/concurrent/Executor;Llmg;Llmg;Lcez;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Laol;->M:Larp;

    .line 695
    return-void
.end method

.method static synthetic a(Laol;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Laol;->N:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic a(Laol;)Lapp;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->g:Lapp;

    return-object v0
.end method

.method static synthetic a(Laol;Lbue;)Lbue;
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Laol;->P:Lbue;

    return-object p1
.end method

.method static synthetic a(Laol;Laox;)V
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Laol;->e()V

    iget-object v0, p0, Laol;->Z:Laoy;

    invoke-interface {p1, v0}, Laox;->a(Laoy;)V

    return-void
.end method

.method static synthetic a(Laol;Ljava/util/Map;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 110
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->bh()Ljava/util/Map;

    move-result-object v3

    const-string v0, "cloudMediaIdToUriMap"

    invoke-static {v3, v0, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->be()Ljfd;

    move-result-object v0

    const-string v1, "getStoryboardResult"

    invoke-static {v0, v1, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v1, v0, Ljfd;->b:Lood;

    const-string v2, "getStoryboardResult.storyboard"

    invoke-static {v1, v2, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v0, v0, Ljfd;->b:Lood;

    iget-object v4, v0, Lood;->b:[Lonr;

    array-length v5, v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v0, v4, v2

    iget-object v1, v0, Lonr;->c:Lonw;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lonr;->c:Lonw;

    invoke-static {v0}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmu;

    if-nez v0, :cond_0

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x10

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "No metadata for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    :cond_0
    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljej;

    if-nez v1, :cond_1

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "No URI for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v1, v1, Ljej;->a:Landroid/net/Uri;

    iget-object v6, p0, Laol;->d:Lawc;

    invoke-interface {v6, v1, v0}, Lawc;->a(Landroid/net/Uri;Lbmu;)V

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    return-void
.end method

.method static synthetic a(Laol;Z)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0, p1}, Laol;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 956
    invoke-virtual {p0}, Laol;->e()V

    .line 959
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->q()J

    move-result-wide v0

    .line 960
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 961
    iget-object v2, p0, Laol;->r:Lavj;

    invoke-virtual {v2, v0, v1}, Lavj;->b(J)V

    .line 964
    :cond_0
    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->h()V

    .line 965
    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->i()V

    .line 966
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->bd()Lawg;

    move-result-object v0

    sget-object v1, Lawg;->c:Lawg;

    if-ne v0, v1, :cond_1

    .line 968
    invoke-virtual {p0}, Laol;->i()Lamb;

    move-result-object v0

    invoke-virtual {v0}, Lamb;->c()V

    .line 972
    :goto_0
    iget-object v0, p0, Laol;->s:Laxp;

    sget-object v1, Laxq;->i:Laxq;

    invoke-virtual {v0, v1}, Laxp;->a(Laxq;)V

    .line 973
    return-void

    .line 970
    :cond_1
    invoke-virtual {p0}, Laol;->h()Laqy;

    move-result-object v0

    invoke-virtual {v0, p1}, Laqy;->a(Z)V

    goto :goto_0
.end method

.method static synthetic a(Laol;Lood;)Z
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->y:Lanh;

    invoke-virtual {v0, p1}, Lanh;->a(Lood;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Laol;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Laol;->v()V

    return-void
.end method

.method static synthetic b(Laol;Landroid/os/AsyncTask;)V
    .locals 3

    .prologue
    .line 110
    const-string v0, "task"

    iget-object v1, p0, Laol;->N:Landroid/os/AsyncTask;

    const-string v2, "mActiveTask replaced without cancelling the first"

    invoke-static {p1, v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-object v0, p0, Laol;->N:Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic b(Laol;Laox;)V
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Laol;->e()V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Laox;->a(Laoy;)V

    return-void
.end method

.method static synthetic b(Laol;Lood;)V
    .locals 6

    .prologue
    .line 110
    iget-object v0, p1, Lood;->h:Looe;

    iget-object v0, v0, Looe;->c:Looa;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lood;->h:Looe;

    iget-object v0, v0, Looe;->c:Looa;

    iget-object v0, v0, Looa;->a:Ljava/lang/Long;

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Laol;->a:Ljava/lang/String;

    iget-object v0, p0, Laol;->d:Lawc;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lawc;->a(Lboh;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lood;->h:Looe;

    iget-object v0, v0, Looe;->c:Looa;

    iget-object v0, v0, Looa;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Laol;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Applying soundtrack "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from cloud storyboard"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Laol;->d:Lawc;

    invoke-interface {v2}, Lawc;->u()Lbrc;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2, v0, v1}, Lbrc;->a(J)Lboh;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v3, Laol;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x3f

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Cloud soundtrack id "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is unknown to the app."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0, v2}, Lawc;->a(Lboh;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Laol;->d:Lawc;

    invoke-interface {v2, v0, v1}, Lawc;->a(J)V

    goto :goto_0
.end method

.method static synthetic b(Laol;Z)Z
    .locals 0

    .prologue
    .line 110
    iput-boolean p1, p0, Laol;->O:Z

    return p1
.end method

.method static synthetic c(Laol;)Laqy;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->C:Laqy;

    return-object v0
.end method

.method static synthetic c(Laol;Lood;)V
    .locals 4

    .prologue
    .line 110
    iget-object v0, p1, Lood;->h:Looe;

    iget v0, v0, Looe;->a:I

    invoke-static {v0}, Lbza;->a(I)Lbza;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Laol;->d:Lawc;

    invoke-interface {v1, v0}, Lawc;->a(Lbza;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Laol;->a:Ljava/lang/String;

    iget-object v1, p1, Lood;->h:Looe;

    iget v1, v1, Looe;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized cloud theme id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Laol;->d:Lawc;

    sget-object v1, Lbza;->a:Lbza;

    invoke-interface {v0, v1}, Lawc;->a(Lbza;)V

    goto :goto_0
.end method

.method static synthetic d(Laol;)Lawc;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->d:Lawc;

    return-object v0
.end method

.method static synthetic d(Laol;Lood;)V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p1, Lood;->h:Looe;

    iget-object v0, v0, Looe;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laol;->d:Lawc;

    iget-object v1, p1, Lood;->h:Looe;

    iget-object v1, v1, Looe;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lawc;->j(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Laol;->d:Lawc;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lawc;->j(Z)V

    goto :goto_0
.end method

.method static synthetic e(Laol;)Lasy;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->p:Lasy;

    return-object v0
.end method

.method static synthetic f(Laol;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Laol;->s()V

    return-void
.end method

.method static synthetic g(Laol;)Laqc;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->z:Laqc;

    return-object v0
.end method

.method static synthetic h(Laol;)Lalo;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->m:Lalo;

    return-object v0
.end method

.method static synthetic i(Laol;)Lalg;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->l:Lalg;

    return-object v0
.end method

.method static synthetic j(Laol;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Laol;->u()V

    return-void
.end method

.method static synthetic k(Laol;)Lapg;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->e:Lapg;

    return-object v0
.end method

.method static synthetic l(Laol;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Laol;->t()V

    return-void
.end method

.method static synthetic m(Laol;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 110
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->bh()Ljava/util/Map;

    move-result-object v2

    const-string v0, "cloudMediaIdToUriMap"

    invoke-static {v2, v0, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->be()Ljfd;

    move-result-object v0

    const-string v1, "getStoryboardResult"

    invoke-static {v0, v1, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v1, v0, Ljfd;->b:Lood;

    const-string v3, "getStoryboardResult.storyboard"

    invoke-static {v1, v3, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v0, v0, Ljfd;->b:Lood;

    iget-object v3, v0, Lood;->b:[Lonr;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    iget-object v0, v5, Lonr;->c:Lonw;

    if-eqz v0, :cond_2

    iget-object v0, v5, Lonr;->c:Lonw;

    invoke-static {v0}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    if-nez v0, :cond_0

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "No URI for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    new-instance v6, Lbkm;

    invoke-direct {v6}, Lbkm;-><init>()V

    iget v7, v5, Lonr;->b:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    iget-object v7, v5, Lonr;->e:Lont;

    if-eqz v7, :cond_1

    invoke-virtual {v6}, Lbkm;->a()Lbko;

    move-result-object v7

    iget-object v5, v5, Lonr;->e:Lont;

    iget-object v5, v5, Lont;->b:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lbko;->a(J)Lbko;

    :cond_1
    invoke-virtual {v6}, Lbkm;->c()Lbkr;

    move-result-object v5

    iget-object v6, p0, Laol;->d:Lawc;

    invoke-interface {v6, v0, v5}, Lawc;->a(Landroid/net/Uri;Lbkr;)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    return-void
.end method

.method static synthetic n(Laol;)Lcdu;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->q:Lcdu;

    return-object v0
.end method

.method static synthetic o(Laol;)V
    .locals 3

    .prologue
    .line 110
    iget-boolean v0, p0, Laol;->O:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Laol;->q:Lcdu;

    iget-object v1, p0, Laol;->d:Lawc;

    invoke-interface {v1}, Lawc;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcdu;->a(I)V

    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->aa()V

    :cond_0
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->j()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Laol;->s()V

    iget-object v0, p0, Laol;->g:Lapp;

    invoke-interface {v0}, Lapp;->r()V

    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->g()Z

    iget-object v0, p0, Laol;->m:Lalo;

    new-instance v1, Laov;

    invoke-direct {v1, p0}, Laov;-><init>(Laol;)V

    invoke-interface {v0, v1}, Lalo;->a(Lalp;)Lalm;

    move-result-object v0

    iput-object v0, p0, Laol;->N:Landroid/os/AsyncTask;

    iget-object v1, p0, Laol;->d:Lawc;

    invoke-interface {v1}, Lawc;->k()[Lavz;

    move-result-object v1

    iget-object v2, p0, Laol;->l:Lalg;

    invoke-interface {v2, v0, v1}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method static synthetic p(Laol;)Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Laol;->O:Z

    return v0
.end method

.method static synthetic q(Laol;)Lbue;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->P:Lbue;

    return-object v0
.end method

.method static synthetic q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Laol;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r()Laic;
    .locals 1

    .prologue
    .line 110
    sget-object v0, Laol;->b:Laic;

    return-object v0
.end method

.method static synthetic r(Laol;)V
    .locals 8

    .prologue
    .line 110
    invoke-virtual {p0}, Laol;->e()V

    invoke-direct {p0}, Laol;->s()V

    new-instance v1, Lbit;

    iget-object v0, p0, Laol;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {v1, v0}, Lbit;-><init>(Landroid/content/ContentResolver;)V

    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->j()I

    move-result v2

    new-array v3, v2, [Lajn;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v4, p0, Laol;->d:Lawc;

    invoke-interface {v4, v0}, Lawc;->a(I)Lavz;

    move-result-object v4

    iget-object v4, v4, Lavz;->a:Landroid/net/Uri;

    iget-object v5, p0, Laol;->d:Lawc;

    invoke-interface {v5, v0}, Lawc;->c(I)Lbmu;

    move-result-object v5

    iget-object v6, p0, Laol;->d:Lawc;

    invoke-interface {v6, v0}, Lawc;->d(I)Lbkr;

    move-result-object v6

    new-instance v7, Lajn;

    invoke-direct {v7, v4, v5, v6}, Lajn;-><init>(Landroid/net/Uri;Lbmu;Lbkr;)V

    aput-object v7, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lajm;

    iget-object v2, p0, Laol;->w:Lajl;

    iget-object v4, p0, Laol;->R:Lapf;

    iget-object v5, p0, Laol;->f:Lbqg;

    invoke-direct {v0, v1, v2, v4, v5}, Lajm;-><init>(Lbit;Lajl;Lajo;Lbqg;)V

    iget-object v1, p0, Laol;->l:Lalg;

    invoke-interface {v1, v0, v3}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    iput-object v0, p0, Laol;->N:Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic s(Laol;)Laxp;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->s:Laxp;

    return-object v0
.end method

.method private s()V
    .locals 2

    .prologue
    .line 1026
    iget-object v0, p0, Laol;->N:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 1027
    iget-object v0, p0, Laol;->N:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 1028
    const/4 v0, 0x0

    iput-object v0, p0, Laol;->N:Landroid/os/AsyncTask;

    .line 1030
    :cond_0
    return-void
.end method

.method static synthetic t(Laol;)Laiz;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->T:Laiz;

    return-object v0
.end method

.method private t()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1040
    invoke-virtual {p0}, Laol;->e()V

    .line 1042
    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->g()Z

    .line 1044
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->bd()Lawg;

    move-result-object v0

    sget-object v1, Lawg;->c:Lawg;

    if-ne v0, v1, :cond_0

    .line 1046
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->aR()Ljdz;

    move-result-object v0

    .line 1047
    const-string v1, "aamPhotoId"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 1048
    iget-object v1, p0, Laol;->n:Lany;

    iget-object v2, p0, Laol;->K:Lawq;

    .line 1049
    invoke-virtual {v2}, Lawq;->c()Lanw;

    move-result-object v2

    iget-object v3, p0, Laol;->M:Larp;

    .line 1050
    invoke-virtual {v3}, Larp;->d()Lart;

    move-result-object v3

    new-instance v4, Lapc;

    invoke-direct {v4, p0}, Lapc;-><init>(Laol;)V

    .line 1048
    invoke-interface {v1, v2, v3, v4}, Lany;->a(Lanw;Lart;Lanz;)Lanu;

    move-result-object v1

    .line 1052
    iget-object v2, p0, Laol;->l:Lalg;

    new-array v3, v6, [Ljdz;

    aput-object v0, v3, v5

    invoke-interface {v2, v1, v3}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 1053
    iput-object v1, p0, Laol;->N:Landroid/os/AsyncTask;

    .line 1054
    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->n()V

    .line 1069
    :goto_0
    return-void

    .line 1055
    :cond_0
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->aQ()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->j()I

    move-result v0

    if-nez v0, :cond_1

    .line 1058
    invoke-direct {p0}, Laol;->s()V

    .line 1060
    iget-object v0, p0, Laol;->o:Laoh;

    new-instance v1, Laoz;

    invoke-direct {v1, p0}, Laoz;-><init>(Laol;)V

    iget-object v2, p0, Laol;->d:Lawc;

    .line 1062
    invoke-interface {v2}, Lawc;->aU()Z

    move-result v2

    .line 1060
    invoke-interface {v0, v1, v2}, Laoh;->a(Laoi;Z)Laoe;

    move-result-object v0

    .line 1063
    iget-object v1, p0, Laol;->l:Lalg;

    new-array v2, v6, [Ljed;

    iget-object v3, p0, Laol;->d:Lawc;

    .line 1064
    invoke-interface {v3}, Lawc;->aO()Ljed;

    move-result-object v3

    aput-object v3, v2, v5

    .line 1063
    invoke-interface {v1, v0, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 1065
    iput-object v0, p0, Laol;->N:Landroid/os/AsyncTask;

    goto :goto_0

    .line 1067
    :cond_1
    invoke-direct {p0}, Laol;->u()V

    goto :goto_0
.end method

.method static synthetic u(Laol;)Laiw;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->h:Laiw;

    return-object v0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 1077
    iget-object v0, p0, Laol;->P:Lbue;

    if-eqz v0, :cond_0

    .line 1078
    iget-object v0, p0, Laol;->d:Lawc;

    iget-object v1, p0, Laol;->P:Lbue;

    invoke-interface {v0, v1}, Lawc;->a(Lbue;)V

    .line 1081
    :cond_0
    iget-object v0, p0, Laol;->v:Land;

    new-instance v1, Laow;

    invoke-direct {v1, p0}, Laow;-><init>(Laol;)V

    .line 1082
    invoke-interface {v0, v1}, Land;->a(Lane;)Lana;

    move-result-object v0

    .line 1083
    iget-object v1, p0, Laol;->l:Lalg;

    iget-object v2, p0, Laol;->d:Lawc;

    .line 1084
    invoke-interface {v2}, Lawc;->k()[Lavz;

    move-result-object v2

    .line 1083
    invoke-interface {v1, v0, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 1085
    iput-object v0, p0, Laol;->N:Landroid/os/AsyncTask;

    .line 1086
    return-void
.end method

.method static synthetic v(Laol;)Laru;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->A:Laru;

    return-object v0
.end method

.method private v()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1181
    invoke-virtual {p0}, Laol;->e()V

    .line 1182
    iget-object v3, p0, Laol;->e:Lapg;

    iget-object v2, p0, Laol;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Laol;->g:Lapp;

    invoke-interface {v2}, Lapp;->k()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    invoke-interface {v3, v0}, Lapg;->b(Z)V

    .line 1183
    return-void

    :cond_0
    move v2, v1

    .line 1182
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic w(Laol;)Lalr;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->B:Lalr;

    return-object v0
.end method

.method static synthetic x(Laol;)Lbyf;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->u:Lbyf;

    return-object v0
.end method

.method static synthetic y(Laol;)Lanj;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Laol;->I:Lanj;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 782
    invoke-super {p0}, Lamn;->a()V

    .line 784
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->f()Z

    move-result v4

    .line 786
    if-eqz v4, :cond_0

    .line 788
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0, v1}, Lawc;->g(Z)V

    .line 791
    :cond_0
    if-eqz v4, :cond_1

    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->aq()Lawh;

    move-result-object v0

    invoke-interface {v0}, Lawh;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 793
    iget-object v0, p0, Laol;->C:Laqy;

    invoke-virtual {v0}, Laqy;->c()V

    .line 796
    :cond_1
    iget-object v0, p0, Laol;->r:Lavj;

    invoke-virtual {v0}, Lavj;->a()V

    .line 797
    iget-object v0, p0, Laol;->s:Laxp;

    invoke-virtual {v0}, Laxp;->a()V

    .line 798
    iget-object v0, p0, Laol;->L:Lapu;

    invoke-virtual {v0}, Lapu;->a()V

    .line 799
    iget-object v0, p0, Laol;->K:Lawq;

    invoke-virtual {v0}, Lawq;->a()V

    .line 800
    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->a()V

    .line 801
    iget-object v0, p0, Laol;->F:Lava;

    invoke-virtual {v0}, Lava;->a()V

    .line 802
    iget-object v0, p0, Laol;->E:Laum;

    invoke-virtual {v0}, Laum;->a()V

    .line 803
    iget-object v0, p0, Laol;->A:Laru;

    invoke-virtual {v0}, Laru;->a()V

    .line 804
    iget-object v0, p0, Laol;->B:Lalr;

    invoke-virtual {v0}, Lalr;->a()V

    .line 805
    iget-object v0, p0, Laol;->C:Laqy;

    invoke-virtual {v0}, Laqy;->a()V

    .line 806
    iget-object v0, p0, Laol;->G:Lase;

    invoke-virtual {v0}, Lase;->a()V

    .line 807
    iget-object v0, p0, Laol;->H:Laxd;

    invoke-virtual {v0}, Laxd;->a()V

    .line 808
    iget-object v0, p0, Laol;->I:Lanj;

    invoke-virtual {v0}, Lanj;->a()V

    .line 809
    iget-object v0, p0, Laol;->J:Laso;

    invoke-virtual {v0}, Laso;->a()V

    .line 810
    iget-object v0, p0, Laol;->D:Lamb;

    invoke-virtual {v0}, Lamb;->a()V

    .line 811
    iget-object v0, p0, Laol;->M:Larp;

    invoke-virtual {v0}, Larp;->a()V

    .line 813
    if-eqz v4, :cond_a

    .line 814
    iget-object v0, p0, Laol;->q:Lcdu;

    iget-object v3, p0, Laol;->d:Lawc;

    invoke-interface {v3}, Lawc;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcdu;->b(Ljava/lang/String;)V

    .line 815
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->aQ()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Laol;->q:Lcdu;

    sget-object v3, Lcea;->b:Lcea;

    invoke-virtual {v0, v3}, Lcdu;->a(Lcea;)V

    .line 820
    :cond_2
    :goto_0
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->aQ()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->j()I

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 821
    :goto_1
    iget-object v3, p0, Laol;->d:Lawc;

    invoke-interface {v3}, Lawc;->aR()Ljdz;

    move-result-object v3

    if-eqz v3, :cond_c

    iget-object v3, p0, Laol;->d:Lawc;

    .line 822
    invoke-interface {v3}, Lawc;->be()Ljfd;

    move-result-object v3

    if-nez v3, :cond_c

    move v3, v1

    .line 824
    :goto_2
    iget-object v5, p0, Laol;->g:Lapp;

    iget-object v6, p0, Laol;->S:Lapq;

    invoke-interface {v5, v6}, Lapp;->a(Lapq;)V

    .line 826
    if-nez v4, :cond_3

    iget-object v5, p0, Laol;->d:Lawc;

    invoke-interface {v5}, Lawc;->d()Z

    move-result v5

    if-eqz v5, :cond_3

    if-nez v0, :cond_3

    if-eqz v3, :cond_e

    .line 828
    :cond_3
    iget-object v0, p0, Laol;->g:Lapp;

    invoke-interface {v0}, Lapp;->i()V

    .line 829
    iget-object v0, p0, Laol;->g:Lapp;

    invoke-interface {v0}, Lapp;->c()V

    .line 830
    iput-boolean v4, p0, Laol;->O:Z

    .line 831
    invoke-virtual {p0}, Laol;->e()V

    invoke-direct {p0}, Laol;->s()V

    const/4 v0, 0x0

    iput-object v0, p0, Laol;->P:Lbue;

    iget-object v0, p0, Laol;->y:Lanh;

    invoke-virtual {v0}, Lanh;->r()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->bd()Lawg;

    move-result-object v0

    sget-object v3, Lawg;->c:Lawg;

    if-eq v0, v3, :cond_d

    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->g()Z

    new-instance v0, Laoj;

    iget-object v3, p0, Laol;->Q:Lapd;

    iget-object v4, p0, Laol;->k:Lawy;

    invoke-interface {v4}, Lawy;->a()Lawx;

    move-result-object v4

    iget-object v5, p0, Laol;->d:Lawc;

    invoke-interface {v5}, Lawc;->aP()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Laol;->d:Lawc;

    invoke-interface {v6}, Lawc;->aO()Ljed;

    move-result-object v6

    iget-object v7, p0, Laol;->d:Lawc;

    invoke-interface {v7}, Lawc;->k()[Lavz;

    invoke-direct {v0, v3, v4, v5, v6}, Laoj;-><init>(Laok;Lawx;Ljava/lang/String;Ljed;)V

    iput-object v0, p0, Laol;->N:Landroid/os/AsyncTask;

    iget-object v3, p0, Laol;->l:Lalg;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-interface {v3, v0, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 841
    :cond_4
    :goto_3
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->j()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->aQ()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Laol;->d:Lawc;

    .line 842
    invoke-interface {v0}, Lawc;->bd()Lawg;

    move-result-object v0

    sget-object v2, Lawg;->c:Lawg;

    if-eq v0, v2, :cond_6

    .line 843
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->n()Z

    move-result v0

    if-nez v0, :cond_5

    .line 844
    invoke-virtual {p0}, Laol;->m()V

    .line 845
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0, v1}, Lawc;->a(Z)V

    .line 847
    :cond_5
    iget-object v0, p0, Laol;->c:Landroid/content/Context;

    invoke-static {v0}, Lbje;->a(Landroid/content/Context;)V

    .line 850
    :cond_6
    iget-object v0, p0, Laol;->e:Lapg;

    invoke-interface {v0}, Lapg;->l()V

    .line 852
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->aA()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Laol;->d:Lawc;

    .line 853
    invoke-interface {v0}, Lawc;->aq()Lawh;

    move-result-object v0

    invoke-interface {v0}, Lawh;->az()Z

    move-result v0

    if-nez v0, :cond_7

    .line 863
    iget-object v0, p0, Laol;->e:Lapg;

    invoke-interface {v0}, Lapg;->n()V

    .line 866
    :cond_7
    iget-object v0, p0, Laol;->aa:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 867
    iget-object v0, p0, Laol;->d:Lawc;

    iget-object v1, p0, Laol;->X:Lawk;

    invoke-interface {v0, v1}, Lawc;->D(Lawk;)V

    .line 868
    iget-object v0, p0, Laol;->d:Lawc;

    iget-object v1, p0, Laol;->W:Lawk;

    invoke-interface {v0, v1}, Lawc;->i(Lawk;)V

    .line 869
    return-void

    .line 815
    :cond_8
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->aR()Ljdz;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Laol;->q:Lcdu;

    sget-object v3, Lcea;->a:Lcea;

    invoke-virtual {v0, v3}, Lcdu;->a(Lcea;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Laol;->d:Lawc;

    invoke-interface {v0}, Lawc;->j()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Laol;->q:Lcdu;

    sget-object v3, Lcea;->c:Lcea;

    invoke-virtual {v0, v3}, Lcdu;->a(Lcea;)V

    goto/16 :goto_0

    .line 817
    :cond_a
    iget-object v0, p0, Laol;->q:Lcdu;

    invoke-virtual {v0}, Lcdu;->a()V

    goto/16 :goto_0

    :cond_b
    move v0, v2

    .line 820
    goto/16 :goto_1

    :cond_c
    move v3, v2

    .line 822
    goto/16 :goto_2

    .line 831
    :cond_d
    invoke-direct {p0}, Laol;->t()V

    goto/16 :goto_3

    .line 833
    :cond_e
    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 834
    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->f()V

    .line 835
    iget-object v0, p0, Laol;->g:Lapp;

    invoke-interface {v0}, Lapp;->r()V

    .line 836
    invoke-direct {p0}, Laol;->v()V

    .line 837
    iget-object v0, p0, Laol;->I:Lanj;

    invoke-virtual {v0}, Lanj;->c()V

    goto/16 :goto_3
.end method

.method public a(Laox;)V
    .locals 1

    .prologue
    .line 933
    iget-object v0, p0, Laol;->aa:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 934
    return-void
.end method

.method public a(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 982
    iget-boolean v0, p0, Lamn;->i:Z

    if-nez v0, :cond_0

    .line 984
    const/4 v0, 0x0

    .line 989
    :goto_0
    return v0

    .line 986
    :cond_0
    iget-object v0, p0, Laol;->e:Lapg;

    const v1, 0x7f12000c

    invoke-interface {v0, v1, p1}, Lapg;->d(ILandroid/view/Menu;)V

    .line 987
    iget-object v0, p0, Laol;->g:Lapp;

    invoke-interface {v0, p1}, Lapp;->a(Landroid/view/Menu;)V

    .line 988
    iget-object v0, p0, Laol;->g:Lapp;

    invoke-interface {v0}, Lapp;->r()V

    .line 989
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 993
    invoke-virtual {p0}, Laol;->e()V

    .line 994
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 995
    const v3, 0x102002c

    if-ne v2, v3, :cond_1

    .line 996
    iget-object v2, p0, Laol;->g:Lapp;

    invoke-interface {v2, v1}, Lapp;->a(Z)V

    .line 997
    iget-object v1, p0, Laol;->g:Lapp;

    invoke-interface {v1}, Lapp;->r()V

    .line 1017
    :cond_0
    :goto_0
    return v0

    .line 999
    :cond_1
    const v3, 0x7f1006ea

    if-ne v2, v3, :cond_2

    .line 1000
    invoke-virtual {p0}, Laol;->m()V

    move v0, v1

    .line 1001
    goto :goto_0

    .line 1002
    :cond_2
    const v3, 0x7f1006eb

    if-ne v2, v3, :cond_4

    .line 1004
    iget-object v2, p0, Laol;->g:Lapp;

    invoke-interface {v2}, Lapp;->k()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1007
    iget-object v2, p0, Laol;->g:Lapp;

    invoke-interface {v2, v0}, Lapp;->a(Z)V

    :goto_1
    move v0, v1

    .line 1012
    goto :goto_0

    .line 1010
    :cond_3
    invoke-direct {p0, v0}, Laol;->a(Z)V

    goto :goto_1

    .line 1013
    :cond_4
    const v3, 0x7f1006ec

    if-ne v2, v3, :cond_0

    .line 1014
    invoke-virtual {p0}, Laol;->p()V

    move v0, v1

    .line 1015
    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 873
    invoke-direct {p0}, Laol;->s()V

    .line 874
    iget-object v0, p0, Laol;->h:Laiw;

    invoke-virtual {v0}, Laiw;->a()V

    .line 875
    iget-object v0, p0, Laol;->L:Lapu;

    invoke-virtual {v0}, Lapu;->b()V

    .line 876
    iget-object v0, p0, Laol;->K:Lawq;

    invoke-virtual {v0}, Lawq;->b()V

    .line 877
    iget-object v0, p0, Laol;->z:Laqc;

    invoke-virtual {v0}, Laqc;->b()V

    .line 878
    iget-object v0, p0, Laol;->A:Laru;

    invoke-virtual {v0}, Laru;->b()V

    .line 879
    iget-object v0, p0, Laol;->B:Lalr;

    invoke-virtual {v0}, Lalr;->b()V

    .line 880
    iget-object v0, p0, Laol;->C:Laqy;

    invoke-virtual {v0}, Laqy;->b()V

    .line 881
    iget-object v0, p0, Laol;->E:Laum;

    invoke-virtual {v0}, Laum;->b()V

    .line 882
    iget-object v0, p0, Laol;->F:Lava;

    invoke-virtual {v0}, Lava;->b()V

    .line 883
    iget-object v0, p0, Laol;->G:Lase;

    invoke-virtual {v0}, Lase;->b()V

    .line 884
    iget-object v0, p0, Laol;->H:Laxd;

    invoke-virtual {v0}, Laxd;->b()V

    .line 885
    iget-object v0, p0, Laol;->I:Lanj;

    invoke-virtual {v0}, Lanj;->b()V

    .line 886
    iget-object v0, p0, Laol;->J:Laso;

    invoke-virtual {v0}, Laso;->b()V

    .line 887
    iget-object v0, p0, Laol;->D:Lamb;

    invoke-virtual {v0}, Lamb;->b()V

    .line 888
    iget-object v0, p0, Laol;->M:Larp;

    invoke-virtual {v0}, Larp;->b()V

    .line 890
    iget-object v0, p0, Laol;->d:Lawc;

    iget-object v1, p0, Laol;->W:Lawk;

    invoke-interface {v0, v1}, Lawc;->j(Lawk;)V

    .line 891
    iget-object v0, p0, Laol;->d:Lawc;

    iget-object v1, p0, Laol;->X:Lawk;

    invoke-interface {v0, v1}, Lawc;->E(Lawk;)V

    .line 892
    iget-object v0, p0, Laol;->g:Lapp;

    iget-object v1, p0, Laol;->S:Lapq;

    invoke-interface {v0, v1}, Lapp;->b(Lapq;)V

    .line 893
    iget-object v0, p0, Laol;->aa:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 895
    invoke-super {p0}, Lamn;->b()V

    .line 897
    iget-object v0, p0, Laol;->e:Lapg;

    invoke-interface {v0}, Lapg;->l()V

    .line 898
    return-void
.end method

.method public b(Laox;)V
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Laol;->aa:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 938
    return-void
.end method

.method public c()Laqc;
    .locals 1

    .prologue
    .line 701
    iget-object v0, p0, Laol;->z:Laqc;

    return-object v0
.end method

.method public d()Laum;
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Laol;->E:Laum;

    return-object v0
.end method

.method public f()Laru;
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Laol;->A:Laru;

    return-object v0
.end method

.method public g()Lalr;
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Laol;->B:Lalr;

    return-object v0
.end method

.method public h()Laqy;
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Laol;->C:Laqy;

    return-object v0
.end method

.method public i()Lamb;
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Laol;->D:Lamb;

    return-object v0
.end method

.method public j()Lapu;
    .locals 1

    .prologue
    .line 741
    iget-object v0, p0, Laol;->L:Lapu;

    return-object v0
.end method

.method public k()Lase;
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Laol;->G:Lase;

    return-object v0
.end method

.method public l()Lanj;
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Laol;->I:Lanj;

    return-object v0
.end method

.method public m()V
    .locals 4

    .prologue
    .line 905
    invoke-virtual {p0}, Laol;->e()V

    .line 907
    invoke-static {}, Lapm;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 908
    iget-object v0, p0, Laol;->x:Ljfb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljfb;->a(Z)Landroid/content/Intent;

    move-result-object v0

    .line 909
    const-string v1, "source_id"

    iget-object v2, p0, Laol;->d:Lawc;

    invoke-interface {v2}, Lawc;->h()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 915
    :goto_0
    iget-object v1, p0, Laol;->e:Lapg;

    invoke-interface {v1, v0}, Lapg;->c(Landroid/content/Intent;)V

    .line 916
    return-void

    .line 911
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 912
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Laol;->c:Landroid/content/Context;

    const-class v3, Lcom/google/android/apps/moviemaker/picker/PickerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Laol;->C:Laqy;

    invoke-virtual {v0}, Laqy;->c()V

    .line 921
    return-void
.end method

.method public o()V
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Laol;->M:Larp;

    invoke-virtual {v0}, Larp;->c()V

    .line 930
    return-void
.end method

.method public p()V
    .locals 1

    .prologue
    .line 977
    invoke-virtual {p0}, Laol;->e()V

    .line 978
    iget-object v0, p0, Laol;->e:Lapg;

    invoke-interface {v0}, Lapg;->m()V

    .line 979
    return-void
.end method
