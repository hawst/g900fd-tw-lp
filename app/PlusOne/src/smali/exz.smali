.class public final Lexz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljbt;
.implements Llnx;
.implements Llrg;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lhke;


# direct methods
.method constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 45
    iput-object p1, p0, Lexz;->a:Landroid/content/Context;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)Lizu;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    .line 103
    const-string v0, "photo_url"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 104
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    :goto_0
    return-object v1

    .line 108
    :cond_0
    const-string v0, "media_type"

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 112
    packed-switch v0, :pswitch_data_0

    .line 118
    sget-object v6, Ljac;->a:Ljac;

    .line 122
    :goto_1
    const-string v0, "photo_id"

    invoke-virtual {p1, v0, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 123
    cmp-long v0, v2, v8

    if-nez v0, :cond_1

    .line 124
    iget-object v0, p0, Lexz;->a:Landroid/content/Context;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1, v6}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v1

    goto :goto_0

    .line 114
    :pswitch_0
    sget-object v6, Ljac;->b:Ljac;

    goto :goto_1

    .line 126
    :cond_1
    iget-object v0, p0, Lexz;->a:Landroid/content/Context;

    move-object v5, v1

    move-object v7, v1

    invoke-static/range {v0 .. v7}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;Ljava/lang/String;)Lizu;

    move-result-object v1

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 55
    iget-object v0, p0, Lexz;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 56
    sget-object v1, Lhmv;->dA:Lhmv;

    invoke-virtual {p0, v1, v0}, Lexz;->a(Lhmv;I)V

    .line 58
    iget-object v1, p0, Lexz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Leyq;->f(Landroid/content/Context;I)Leyt;

    move-result-object v0

    .line 59
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Leyt;->a(Ljava/lang/Integer;)Leyt;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v2}, Leyt;->a(Z)Leyt;

    move-result-object v0

    const/16 v1, 0x10

    .line 61
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Leyt;->b(Ljava/lang/Integer;)Leyt;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v2}, Leyt;->a(I)Leyt;

    move-result-object v0

    .line 63
    invoke-virtual {v0, v2}, Leyt;->f(I)Leyt;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Leyt;->a()Landroid/content/Intent;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lexz;->b:Lhke;

    invoke-virtual {v1, p1, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 66
    return-void
.end method

.method public a(ILhkd;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lexz;->b:Lhke;

    invoke-virtual {v0, p1, p2}, Lhke;->a(ILhkd;)Lhke;

    .line 51
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lhke;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhke;

    iput-object v0, p0, Lexz;->b:Lhke;

    .line 41
    return-void
.end method

.method a(Lhmv;I)V
    .locals 3

    .prologue
    .line 96
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 97
    iget-object v0, p0, Lexz;->a:Landroid/content/Context;

    const-class v1, Lhms;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lexz;->a:Landroid/content/Context;

    invoke-direct {v1, v2, p2}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 98
    invoke-virtual {v1, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 97
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 100
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 70
    iget-object v0, p0, Lexz;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 71
    sget-object v1, Lhmv;->dA:Lhmv;

    invoke-virtual {p0, v1, v0}, Lexz;->a(Lhmv;I)V

    .line 73
    iget-object v1, p0, Lexz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Leyq;->f(Landroid/content/Context;I)Leyt;

    move-result-object v0

    .line 74
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Leyt;->a(Ljava/lang/Integer;)Leyt;

    move-result-object v0

    .line 75
    invoke-virtual {v0, v2}, Leyt;->a(Z)Leyt;

    move-result-object v0

    const/16 v1, 0x1e

    .line 76
    invoke-virtual {v0, v1}, Leyt;->a(I)Leyt;

    move-result-object v0

    .line 77
    invoke-virtual {v0, v2}, Leyt;->f(I)Leyt;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Leyt;->a()Landroid/content/Intent;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lexz;->b:Lhke;

    invoke-virtual {v1, p1, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 80
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lexz;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 85
    sget-object v1, Lhmv;->dA:Lhmv;

    invoke-virtual {p0, v1, v0}, Lexz;->a(Lhmv;I)V

    .line 87
    iget-object v1, p0, Lexz;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Leyq;->f(Landroid/content/Context;I)Leyt;

    move-result-object v0

    const/4 v1, 0x2

    .line 88
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Leyt;->a(Ljava/lang/Integer;)Leyt;

    move-result-object v0

    const/16 v1, 0x1e

    .line 89
    invoke-virtual {v0, v1}, Leyt;->a(I)Leyt;

    move-result-object v0

    const/4 v1, 0x1

    .line 90
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Leyt;->a(Ljava/lang/Boolean;)Leyt;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Leyt;->a()Landroid/content/Intent;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lexz;->b:Lhke;

    invoke-virtual {v1, p1, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 93
    return-void
.end method
