.class public Ljst;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhzd;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Ljst;->a:Ljava/lang/String;

    .line 41
    iput p2, p0, Ljst;->b:I

    .line 42
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Ljst;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/util/ArrayList;)Lu;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lu;"
        }
    .end annotation

    .prologue
    .line 72
    new-instance v0, Ljsp;

    invoke-direct {v0}, Ljsp;-><init>()V

    .line 73
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 74
    const-string v2, "ShareouselSlide.ARG_ENSEMBLE_NAME"

    invoke-virtual {p0}, Ljst;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v2, "ShareouselSlide.ARG_SHAREOUSEL_ORDER"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 76
    invoke-virtual {v0, v1}, Lhix;->f(Landroid/os/Bundle;)V

    .line 77
    return-object v0
.end method

.method public a(Lhzc;)V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Ljst;->b:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 56
    const v0, 0x7f0204db

    return v0
.end method

.method public d()Lhzc;
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lhzc;

    invoke-direct {v0}, Lhzc;-><init>()V

    .line 66
    invoke-virtual {p0, v0}, Ljst;->a(Lhzc;)V

    .line 67
    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method
