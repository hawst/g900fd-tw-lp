.class final Ldve;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:I

.field final b:Ldvz;

.field final c:Landroid/content/SyncResult;

.field final d:J

.field final e:J

.field f:Ldul;

.field g:J

.field h:J

.field i:J

.field j:I

.field k:I

.field l:I

.field m:I

.field n:J

.field private o:J

.field private p:J

.field private q:Z

.field private final r:Lnyc;

.field private final s:J

.field private final t:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

.field private u:J

.field private v:J

.field private synthetic w:Ldur;


# direct methods
.method public constructor <init>(Ldur;Landroid/content/Context;ILdvz;Landroid/content/SyncResult;Lnyc;J)V
    .locals 7

    .prologue
    .line 1973
    iput-object p1, p0, Ldve;->w:Ldur;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1974
    iput p3, p0, Ldve;->a:I

    .line 1975
    iput-object p4, p0, Ldve;->b:Ldvz;

    .line 1976
    iput-object p5, p0, Ldve;->c:Landroid/content/SyncResult;

    .line 1977
    iput-object p6, p0, Ldve;->r:Lnyc;

    .line 1978
    invoke-static {p2}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    move-result-object v0

    iput-object v0, p0, Ldve;->t:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    .line 1979
    iput-wide p7, p0, Ldve;->s:J

    .line 1981
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ldve;->p:J

    .line 1982
    invoke-static {p1}, Ldur;->d(Ldur;)J

    move-result-wide v0

    iput-wide v0, p0, Ldve;->d:J

    .line 1983
    invoke-static {p1, p2}, Ldur;->a(Ldur;Landroid/content/Context;)J

    move-result-wide v0

    iput-wide v0, p0, Ldve;->g:J

    .line 1985
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljfb;->b(I)Z

    move-result v0

    iput-boolean v0, p0, Ldve;->q:Z

    .line 1988
    invoke-static {p1}, Ldur;->e(Ldur;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1989
    iget-wide v2, p0, Ldve;->d:J

    invoke-virtual {p0}, Ldve;->e()J

    move-result-wide v4

    sub-long/2addr v2, v4

    int-to-long v0, v0

    div-long v0, v2, v0

    iput-wide v0, p0, Ldve;->e:J

    .line 1991
    const-wide/32 v0, 0x6400000

    iget-wide v2, p0, Ldve;->e:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Ldve;->o:J

    .line 1993
    iget-wide v0, p0, Ldve;->o:J

    iput-wide v0, p0, Ldve;->i:J

    .line 1996
    invoke-static {p2, p3}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1997
    invoke-static {p1, v0}, Ldur;->a(Ldur;Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v2

    iput-wide v2, p0, Ldve;->v:J

    .line 1998
    invoke-static {p1, v0}, Ldur;->b(Ldur;Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v0

    iput-wide v0, p0, Ldve;->u:J

    .line 1999
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 2086
    sparse-switch p1, :sswitch_data_0

    .line 2090
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2087
    :sswitch_0
    iget v0, p0, Ldve;->j:I

    goto :goto_0

    .line 2088
    :sswitch_1
    iget v0, p0, Ldve;->k:I

    goto :goto_0

    .line 2089
    :sswitch_2
    iget v0, p0, Ldve;->l:I

    goto :goto_0

    .line 2086
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 2002
    iget-object v0, p0, Ldve;->b:Ldvz;

    invoke-virtual {v0}, Ldvz;->c()Z

    move-result v0

    return v0
.end method

.method public a(II)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2035
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    sparse-switch p2, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown representation type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget-object v2, p0, Ldve;->r:Lnyc;

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldve;->r:Lnyc;

    iget-object v2, v2, Lnyc;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-wide v2, p0, Ldve;->v:J

    iget-object v4, p0, Ldve;->r:Lnyc;

    iget-object v4, v4, Lnyc;->c:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Ldve;->r:Lnyc;

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldve;->r:Lnyc;

    iget-object v2, v2, Lnyc;->e:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    iget-wide v2, p0, Ldve;->u:J

    iget-object v4, p0, Ldve;->r:Lnyc;

    iget-object v4, v4, Lnyc;->e:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    move v2, v0

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public b(II)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 2066
    sparse-switch p1, :sswitch_data_0

    .line 2080
    :goto_0
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 2081
    sparse-switch p1, :sswitch_data_1

    .line 2083
    :cond_0
    :goto_1
    return-void

    .line 2068
    :sswitch_0
    iget v0, p0, Ldve;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldve;->j:I

    goto :goto_0

    .line 2072
    :sswitch_1
    iget v0, p0, Ldve;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldve;->k:I

    goto :goto_0

    .line 2076
    :sswitch_2
    iget v0, p0, Ldve;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldve;->l:I

    goto :goto_0

    .line 2081
    :sswitch_3
    iget-wide v0, p0, Ldve;->v:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldve;->v:J

    goto :goto_1

    :sswitch_4
    iget-wide v0, p0, Ldve;->u:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldve;->u:J

    goto :goto_1

    :sswitch_5
    iget-wide v0, p0, Ldve;->u:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldve;->u:J

    goto :goto_1

    .line 2066
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch

    .line 2081
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_3
        0x2 -> :sswitch_4
        0x8 -> :sswitch_5
    .end sparse-switch
.end method

.method public b()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2023
    iget-object v2, p0, Ldve;->b:Ldvz;

    invoke-virtual {v2}, Ldvz;->c()Z

    move-result v2

    if-nez v2, :cond_0

    iget-wide v2, p0, Ldve;->i:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Ldve;->w:Ldur;

    .line 2024
    invoke-static {v2}, Ldur;->f(Ldur;)Ljgn;

    move-result-object v2

    invoke-interface {v2}, Ljgn;->j()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2025
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Ldve;->p:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x1d4c0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 2026
    iget-wide v2, p0, Ldve;->p:J

    iget-wide v4, p0, Ldve;->s:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x1499700

    cmp-long v2, v2, v4

    if-ltz v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    iget-object v2, p0, Ldve;->t:Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/autobackup/AutoBackupEnvironment;->a()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 2039
    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 2043
    iget-boolean v0, p0, Ldve;->q:Z

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 2047
    invoke-virtual {p0}, Ldve;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldve;->w:Ldur;

    .line 2048
    invoke-static {v0}, Ldur;->g(Ldur;)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Ldve;->w:Ldur;

    invoke-static {v0}, Ldur;->h(Ldur;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 23

    .prologue
    .line 2096
    move-object/from16 v0, p0

    iget-object v2, v0, Ldve;->w:Ldur;

    invoke-static {v2}, Ldur;->b(Ldur;)Lhei;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ldve;->w:Ldur;

    invoke-static {v3}, Ldur;->a(Ldur;)Ldve;

    move-result-object v3

    iget v3, v3, Ldve;->a:I

    invoke-interface {v2, v3}, Lhei;->a(I)Lhej;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2097
    invoke-static {v2}, Lifu;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Ldve;->g:J

    move-object/from16 v0, p0

    iget v3, v0, Ldve;->j:I

    move-object/from16 v0, p0

    iget v6, v0, Ldve;->k:I

    move-object/from16 v0, p0

    iget v7, v0, Ldve;->l:I

    move-object/from16 v0, p0

    iget-wide v8, v0, Ldve;->h:J

    move-object/from16 v0, p0

    iget v10, v0, Ldve;->m:I

    move-object/from16 v0, p0

    iget-wide v12, v0, Ldve;->n:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Ldve;->v:J

    move-object/from16 v0, p0

    iget-object v11, v0, Ldve;->r:Lnyc;

    iget-object v11, v11, Lnyc;->e:Ljava/lang/Integer;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-wide v0, v0, Ldve;->u:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ldve;->r:Lnyc;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lnyc;->e:Ljava/lang/Integer;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Ldve;->p:J

    move-wide/from16 v20, v0

    .line 2111
    invoke-static/range {v20 .. v21}, Llse;->a(J)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    move/from16 v0, v21

    add-int/lit16 v0, v0, 0x1b7

    move/from16 v21, v0

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int v21, v21, v22

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int v21, v21, v22

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v22

    add-int v21, v21, v22

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v21, "account: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v20, ", totalSpaceUsed: "

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", thumbnails: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", largeImages: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", videos: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", bytes: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", cacheEvictions: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", cacheEvictionBytes: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", AllPhotos Thumbnails synced total: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", AllPhotos Settings max thumbnails on wifi: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", AllPhotos Large/Videos synced total: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", AllPhotos Settings max large/videos on wifi: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
