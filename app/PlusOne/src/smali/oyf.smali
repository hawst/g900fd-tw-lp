.class public final Loyf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Loyf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field private E:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Loya;

.field public g:Loya;

.field private h:Ljava/lang/String;

.field private i:Lpdi;

.field private j:Ljava/lang/String;

.field private k:[Loya;

.field private l:Loya;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Loya;

.field private p:[Loya;

.field private q:Ljava/lang/Boolean;

.field private r:Ljava/lang/String;

.field private s:Loya;

.field private t:[Loya;

.field private u:I

.field private v:Loya;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x286ee2e

    new-instance v1, Loyg;

    invoke-direct {v1}, Loyg;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Loyf;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Loyf;->i:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyf;->k:[Loya;

    .line 35
    iput-object v1, p0, Loyf;->l:Loya;

    .line 40
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyf;->f:[Loya;

    .line 45
    iput-object v1, p0, Loyf;->o:Loya;

    .line 48
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyf;->p:[Loya;

    .line 55
    iput-object v1, p0, Loyf;->s:Loya;

    .line 58
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Loyf;->t:[Loya;

    .line 61
    const/high16 v0, -0x80000000

    iput v0, p0, Loyf;->u:I

    .line 64
    iput-object v1, p0, Loyf;->v:Loya;

    .line 71
    iput-object v1, p0, Loyf;->g:Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 202
    .line 203
    iget-object v0, p0, Loyf;->b:Ljava/lang/String;

    if-eqz v0, :cond_21

    .line 204
    const/4 v0, 0x1

    iget-object v2, p0, Loyf;->b:Ljava/lang/String;

    .line 205
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 207
    :goto_0
    iget-object v2, p0, Loyf;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 208
    const/4 v2, 0x2

    iget-object v3, p0, Loyf;->c:Ljava/lang/String;

    .line 209
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 211
    :cond_0
    iget-object v2, p0, Loyf;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 212
    const/4 v2, 0x3

    iget-object v3, p0, Loyf;->d:Ljava/lang/String;

    .line 213
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 215
    :cond_1
    iget-object v2, p0, Loyf;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 216
    const/4 v2, 0x4

    iget-object v3, p0, Loyf;->e:Ljava/lang/String;

    .line 217
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 219
    :cond_2
    iget-object v2, p0, Loyf;->h:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 220
    const/4 v2, 0x5

    iget-object v3, p0, Loyf;->h:Ljava/lang/String;

    .line 221
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 223
    :cond_3
    iget-object v2, p0, Loyf;->i:Lpdi;

    if-eqz v2, :cond_4

    .line 224
    const/4 v2, 0x6

    iget-object v3, p0, Loyf;->i:Lpdi;

    .line 225
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 227
    :cond_4
    iget-object v2, p0, Loyf;->j:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 228
    const/4 v2, 0x7

    iget-object v3, p0, Loyf;->j:Ljava/lang/String;

    .line 229
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 231
    :cond_5
    iget-object v2, p0, Loyf;->k:[Loya;

    if-eqz v2, :cond_7

    .line 232
    iget-object v3, p0, Loyf;->k:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 233
    if-eqz v5, :cond_6

    .line 234
    const/16 v6, 0x8

    .line 235
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 232
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 239
    :cond_7
    iget-object v2, p0, Loyf;->l:Loya;

    if-eqz v2, :cond_8

    .line 240
    const/16 v2, 0x9

    iget-object v3, p0, Loyf;->l:Loya;

    .line 241
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 243
    :cond_8
    iget-object v2, p0, Loyf;->m:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 244
    const/16 v2, 0xa

    iget-object v3, p0, Loyf;->m:Ljava/lang/String;

    .line 245
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 247
    :cond_9
    iget-object v2, p0, Loyf;->f:[Loya;

    if-eqz v2, :cond_b

    .line 248
    iget-object v3, p0, Loyf;->f:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_b

    aget-object v5, v3, v2

    .line 249
    if-eqz v5, :cond_a

    .line 250
    const/16 v6, 0xb

    .line 251
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 248
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 255
    :cond_b
    iget-object v2, p0, Loyf;->n:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 256
    const/16 v2, 0xc

    iget-object v3, p0, Loyf;->n:Ljava/lang/String;

    .line 257
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 259
    :cond_c
    iget-object v2, p0, Loyf;->o:Loya;

    if-eqz v2, :cond_d

    .line 260
    const/16 v2, 0x12

    iget-object v3, p0, Loyf;->o:Loya;

    .line 261
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 263
    :cond_d
    iget-object v2, p0, Loyf;->p:[Loya;

    if-eqz v2, :cond_f

    .line 264
    iget-object v3, p0, Loyf;->p:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_f

    aget-object v5, v3, v2

    .line 265
    if-eqz v5, :cond_e

    .line 266
    const/16 v6, 0x2a

    .line 267
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 264
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 271
    :cond_f
    iget-object v2, p0, Loyf;->q:Ljava/lang/Boolean;

    if-eqz v2, :cond_10

    .line 272
    const/16 v2, 0x41

    iget-object v3, p0, Loyf;->q:Ljava/lang/Boolean;

    .line 273
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 275
    :cond_10
    iget-object v2, p0, Loyf;->r:Ljava/lang/String;

    if-eqz v2, :cond_11

    .line 276
    const/16 v2, 0x4b

    iget-object v3, p0, Loyf;->r:Ljava/lang/String;

    .line 277
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 279
    :cond_11
    iget-object v2, p0, Loyf;->s:Loya;

    if-eqz v2, :cond_12

    .line 280
    const/16 v2, 0x52

    iget-object v3, p0, Loyf;->s:Loya;

    .line 281
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 283
    :cond_12
    iget-object v2, p0, Loyf;->t:[Loya;

    if-eqz v2, :cond_14

    .line 284
    iget-object v2, p0, Loyf;->t:[Loya;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_14

    aget-object v4, v2, v1

    .line 285
    if-eqz v4, :cond_13

    .line 286
    const/16 v5, 0x53

    .line 287
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 284
    :cond_13
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 291
    :cond_14
    iget v1, p0, Loyf;->u:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_15

    .line 292
    const/16 v1, 0x5a

    iget v2, p0, Loyf;->u:I

    .line 293
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_15
    iget-object v1, p0, Loyf;->v:Loya;

    if-eqz v1, :cond_16

    .line 296
    const/16 v1, 0x60

    iget-object v2, p0, Loyf;->v:Loya;

    .line 297
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 299
    :cond_16
    iget-object v1, p0, Loyf;->w:Ljava/lang/String;

    if-eqz v1, :cond_17

    .line 300
    const/16 v1, 0x6f

    iget-object v2, p0, Loyf;->w:Ljava/lang/String;

    .line 301
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    :cond_17
    iget-object v1, p0, Loyf;->x:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 304
    const/16 v1, 0x70

    iget-object v2, p0, Loyf;->x:Ljava/lang/String;

    .line 305
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 307
    :cond_18
    iget-object v1, p0, Loyf;->g:Loya;

    if-eqz v1, :cond_19

    .line 308
    const/16 v1, 0xb9

    iget-object v2, p0, Loyf;->g:Loya;

    .line 309
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 311
    :cond_19
    iget-object v1, p0, Loyf;->y:Ljava/lang/String;

    if-eqz v1, :cond_1a

    .line 312
    const/16 v1, 0xbc

    iget-object v2, p0, Loyf;->y:Ljava/lang/String;

    .line 313
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 315
    :cond_1a
    iget-object v1, p0, Loyf;->z:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 316
    const/16 v1, 0xbd

    iget-object v2, p0, Loyf;->z:Ljava/lang/String;

    .line 317
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 319
    :cond_1b
    iget-object v1, p0, Loyf;->A:Ljava/lang/String;

    if-eqz v1, :cond_1c

    .line 320
    const/16 v1, 0xbe

    iget-object v2, p0, Loyf;->A:Ljava/lang/String;

    .line 321
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_1c
    iget-object v1, p0, Loyf;->B:Ljava/lang/String;

    if-eqz v1, :cond_1d

    .line 324
    const/16 v1, 0xbf

    iget-object v2, p0, Loyf;->B:Ljava/lang/String;

    .line 325
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 327
    :cond_1d
    iget-object v1, p0, Loyf;->C:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 328
    const/16 v1, 0xf9

    iget-object v2, p0, Loyf;->C:Ljava/lang/String;

    .line 329
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    :cond_1e
    iget-object v1, p0, Loyf;->D:Ljava/lang/String;

    if-eqz v1, :cond_1f

    .line 332
    const/16 v1, 0xfc

    iget-object v2, p0, Loyf;->D:Ljava/lang/String;

    .line 333
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 335
    :cond_1f
    iget-object v1, p0, Loyf;->E:Ljava/lang/String;

    if-eqz v1, :cond_20

    .line 336
    const/16 v1, 0xfe

    iget-object v2, p0, Loyf;->E:Ljava/lang/String;

    .line 337
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 339
    :cond_20
    iget-object v1, p0, Loyf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 340
    iput v0, p0, Loyf;->ai:I

    .line 341
    return v0

    :cond_21
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loyf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 349
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 350
    sparse-switch v0, :sswitch_data_0

    .line 354
    iget-object v2, p0, Loyf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 355
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loyf;->ah:Ljava/util/List;

    .line 358
    :cond_1
    iget-object v2, p0, Loyf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    :sswitch_0
    return-object p0

    .line 365
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->b:Ljava/lang/String;

    goto :goto_0

    .line 369
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->c:Ljava/lang/String;

    goto :goto_0

    .line 373
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->d:Ljava/lang/String;

    goto :goto_0

    .line 377
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->e:Ljava/lang/String;

    goto :goto_0

    .line 381
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->h:Ljava/lang/String;

    goto :goto_0

    .line 385
    :sswitch_6
    iget-object v0, p0, Loyf;->i:Lpdi;

    if-nez v0, :cond_2

    .line 386
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Loyf;->i:Lpdi;

    .line 388
    :cond_2
    iget-object v0, p0, Loyf;->i:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 392
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->j:Ljava/lang/String;

    goto :goto_0

    .line 396
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 397
    iget-object v0, p0, Loyf;->k:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 398
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 399
    iget-object v3, p0, Loyf;->k:[Loya;

    if-eqz v3, :cond_3

    .line 400
    iget-object v3, p0, Loyf;->k:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 402
    :cond_3
    iput-object v2, p0, Loyf;->k:[Loya;

    .line 403
    :goto_2
    iget-object v2, p0, Loyf;->k:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 404
    iget-object v2, p0, Loyf;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 405
    iget-object v2, p0, Loyf;->k:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 406
    invoke-virtual {p1}, Loxn;->a()I

    .line 403
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 397
    :cond_4
    iget-object v0, p0, Loyf;->k:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 409
    :cond_5
    iget-object v2, p0, Loyf;->k:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 410
    iget-object v2, p0, Loyf;->k:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 414
    :sswitch_9
    iget-object v0, p0, Loyf;->l:Loya;

    if-nez v0, :cond_6

    .line 415
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyf;->l:Loya;

    .line 417
    :cond_6
    iget-object v0, p0, Loyf;->l:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 421
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 425
    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 426
    iget-object v0, p0, Loyf;->f:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 427
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 428
    iget-object v3, p0, Loyf;->f:[Loya;

    if-eqz v3, :cond_7

    .line 429
    iget-object v3, p0, Loyf;->f:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 431
    :cond_7
    iput-object v2, p0, Loyf;->f:[Loya;

    .line 432
    :goto_4
    iget-object v2, p0, Loyf;->f:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 433
    iget-object v2, p0, Loyf;->f:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 434
    iget-object v2, p0, Loyf;->f:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 435
    invoke-virtual {p1}, Loxn;->a()I

    .line 432
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 426
    :cond_8
    iget-object v0, p0, Loyf;->f:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 438
    :cond_9
    iget-object v2, p0, Loyf;->f:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 439
    iget-object v2, p0, Loyf;->f:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 443
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 447
    :sswitch_d
    iget-object v0, p0, Loyf;->o:Loya;

    if-nez v0, :cond_a

    .line 448
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyf;->o:Loya;

    .line 450
    :cond_a
    iget-object v0, p0, Loyf;->o:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 454
    :sswitch_e
    const/16 v0, 0x152

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 455
    iget-object v0, p0, Loyf;->p:[Loya;

    if-nez v0, :cond_c

    move v0, v1

    .line 456
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 457
    iget-object v3, p0, Loyf;->p:[Loya;

    if-eqz v3, :cond_b

    .line 458
    iget-object v3, p0, Loyf;->p:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 460
    :cond_b
    iput-object v2, p0, Loyf;->p:[Loya;

    .line 461
    :goto_6
    iget-object v2, p0, Loyf;->p:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    .line 462
    iget-object v2, p0, Loyf;->p:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 463
    iget-object v2, p0, Loyf;->p:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 464
    invoke-virtual {p1}, Loxn;->a()I

    .line 461
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 455
    :cond_c
    iget-object v0, p0, Loyf;->p:[Loya;

    array-length v0, v0

    goto :goto_5

    .line 467
    :cond_d
    iget-object v2, p0, Loyf;->p:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 468
    iget-object v2, p0, Loyf;->p:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 472
    :sswitch_f
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loyf;->q:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 476
    :sswitch_10
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 480
    :sswitch_11
    iget-object v0, p0, Loyf;->s:Loya;

    if-nez v0, :cond_e

    .line 481
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyf;->s:Loya;

    .line 483
    :cond_e
    iget-object v0, p0, Loyf;->s:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 487
    :sswitch_12
    const/16 v0, 0x29a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 488
    iget-object v0, p0, Loyf;->t:[Loya;

    if-nez v0, :cond_10

    move v0, v1

    .line 489
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 490
    iget-object v3, p0, Loyf;->t:[Loya;

    if-eqz v3, :cond_f

    .line 491
    iget-object v3, p0, Loyf;->t:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 493
    :cond_f
    iput-object v2, p0, Loyf;->t:[Loya;

    .line 494
    :goto_8
    iget-object v2, p0, Loyf;->t:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_11

    .line 495
    iget-object v2, p0, Loyf;->t:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 496
    iget-object v2, p0, Loyf;->t:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 497
    invoke-virtual {p1}, Loxn;->a()I

    .line 494
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 488
    :cond_10
    iget-object v0, p0, Loyf;->t:[Loya;

    array-length v0, v0

    goto :goto_7

    .line 500
    :cond_11
    iget-object v2, p0, Loyf;->t:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 501
    iget-object v2, p0, Loyf;->t:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 505
    :sswitch_13
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 506
    if-eqz v0, :cond_12

    const/4 v2, 0x1

    if-ne v0, v2, :cond_13

    .line 508
    :cond_12
    iput v0, p0, Loyf;->u:I

    goto/16 :goto_0

    .line 510
    :cond_13
    iput v1, p0, Loyf;->u:I

    goto/16 :goto_0

    .line 515
    :sswitch_14
    iget-object v0, p0, Loyf;->v:Loya;

    if-nez v0, :cond_14

    .line 516
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyf;->v:Loya;

    .line 518
    :cond_14
    iget-object v0, p0, Loyf;->v:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 522
    :sswitch_15
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 526
    :sswitch_16
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->x:Ljava/lang/String;

    goto/16 :goto_0

    .line 530
    :sswitch_17
    iget-object v0, p0, Loyf;->g:Loya;

    if-nez v0, :cond_15

    .line 531
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loyf;->g:Loya;

    .line 533
    :cond_15
    iget-object v0, p0, Loyf;->g:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 537
    :sswitch_18
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->y:Ljava/lang/String;

    goto/16 :goto_0

    .line 541
    :sswitch_19
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->z:Ljava/lang/String;

    goto/16 :goto_0

    .line 545
    :sswitch_1a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->A:Ljava/lang/String;

    goto/16 :goto_0

    .line 549
    :sswitch_1b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->B:Ljava/lang/String;

    goto/16 :goto_0

    .line 553
    :sswitch_1c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 557
    :sswitch_1d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->D:Ljava/lang/String;

    goto/16 :goto_0

    .line 561
    :sswitch_1e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyf;->E:Ljava/lang/String;

    goto/16 :goto_0

    .line 350
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x92 -> :sswitch_d
        0x152 -> :sswitch_e
        0x208 -> :sswitch_f
        0x25a -> :sswitch_10
        0x292 -> :sswitch_11
        0x29a -> :sswitch_12
        0x2d0 -> :sswitch_13
        0x302 -> :sswitch_14
        0x37a -> :sswitch_15
        0x382 -> :sswitch_16
        0x5ca -> :sswitch_17
        0x5e2 -> :sswitch_18
        0x5ea -> :sswitch_19
        0x5f2 -> :sswitch_1a
        0x5fa -> :sswitch_1b
        0x7ca -> :sswitch_1c
        0x7e2 -> :sswitch_1d
        0x7f2 -> :sswitch_1e
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 90
    iget-object v1, p0, Loyf;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 91
    const/4 v1, 0x1

    iget-object v2, p0, Loyf;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 93
    :cond_0
    iget-object v1, p0, Loyf;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 94
    const/4 v1, 0x2

    iget-object v2, p0, Loyf;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 96
    :cond_1
    iget-object v1, p0, Loyf;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 97
    const/4 v1, 0x3

    iget-object v2, p0, Loyf;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 99
    :cond_2
    iget-object v1, p0, Loyf;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 100
    const/4 v1, 0x4

    iget-object v2, p0, Loyf;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 102
    :cond_3
    iget-object v1, p0, Loyf;->h:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 103
    const/4 v1, 0x5

    iget-object v2, p0, Loyf;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 105
    :cond_4
    iget-object v1, p0, Loyf;->i:Lpdi;

    if-eqz v1, :cond_5

    .line 106
    const/4 v1, 0x6

    iget-object v2, p0, Loyf;->i:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 108
    :cond_5
    iget-object v1, p0, Loyf;->j:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 109
    const/4 v1, 0x7

    iget-object v2, p0, Loyf;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 111
    :cond_6
    iget-object v1, p0, Loyf;->k:[Loya;

    if-eqz v1, :cond_8

    .line 112
    iget-object v2, p0, Loyf;->k:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 113
    if-eqz v4, :cond_7

    .line 114
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 112
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 118
    :cond_8
    iget-object v1, p0, Loyf;->l:Loya;

    if-eqz v1, :cond_9

    .line 119
    const/16 v1, 0x9

    iget-object v2, p0, Loyf;->l:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 121
    :cond_9
    iget-object v1, p0, Loyf;->m:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 122
    const/16 v1, 0xa

    iget-object v2, p0, Loyf;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 124
    :cond_a
    iget-object v1, p0, Loyf;->f:[Loya;

    if-eqz v1, :cond_c

    .line 125
    iget-object v2, p0, Loyf;->f:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 126
    if-eqz v4, :cond_b

    .line 127
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 125
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 131
    :cond_c
    iget-object v1, p0, Loyf;->n:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 132
    const/16 v1, 0xc

    iget-object v2, p0, Loyf;->n:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 134
    :cond_d
    iget-object v1, p0, Loyf;->o:Loya;

    if-eqz v1, :cond_e

    .line 135
    const/16 v1, 0x12

    iget-object v2, p0, Loyf;->o:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 137
    :cond_e
    iget-object v1, p0, Loyf;->p:[Loya;

    if-eqz v1, :cond_10

    .line 138
    iget-object v2, p0, Loyf;->p:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 139
    if-eqz v4, :cond_f

    .line 140
    const/16 v5, 0x2a

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 138
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 144
    :cond_10
    iget-object v1, p0, Loyf;->q:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 145
    const/16 v1, 0x41

    iget-object v2, p0, Loyf;->q:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 147
    :cond_11
    iget-object v1, p0, Loyf;->r:Ljava/lang/String;

    if-eqz v1, :cond_12

    .line 148
    const/16 v1, 0x4b

    iget-object v2, p0, Loyf;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 150
    :cond_12
    iget-object v1, p0, Loyf;->s:Loya;

    if-eqz v1, :cond_13

    .line 151
    const/16 v1, 0x52

    iget-object v2, p0, Loyf;->s:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 153
    :cond_13
    iget-object v1, p0, Loyf;->t:[Loya;

    if-eqz v1, :cond_15

    .line 154
    iget-object v1, p0, Loyf;->t:[Loya;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_15

    aget-object v3, v1, v0

    .line 155
    if-eqz v3, :cond_14

    .line 156
    const/16 v4, 0x53

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 154
    :cond_14
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 160
    :cond_15
    iget v0, p0, Loyf;->u:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_16

    .line 161
    const/16 v0, 0x5a

    iget v1, p0, Loyf;->u:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 163
    :cond_16
    iget-object v0, p0, Loyf;->v:Loya;

    if-eqz v0, :cond_17

    .line 164
    const/16 v0, 0x60

    iget-object v1, p0, Loyf;->v:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 166
    :cond_17
    iget-object v0, p0, Loyf;->w:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 167
    const/16 v0, 0x6f

    iget-object v1, p0, Loyf;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 169
    :cond_18
    iget-object v0, p0, Loyf;->x:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 170
    const/16 v0, 0x70

    iget-object v1, p0, Loyf;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 172
    :cond_19
    iget-object v0, p0, Loyf;->g:Loya;

    if-eqz v0, :cond_1a

    .line 173
    const/16 v0, 0xb9

    iget-object v1, p0, Loyf;->g:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 175
    :cond_1a
    iget-object v0, p0, Loyf;->y:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 176
    const/16 v0, 0xbc

    iget-object v1, p0, Loyf;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 178
    :cond_1b
    iget-object v0, p0, Loyf;->z:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 179
    const/16 v0, 0xbd

    iget-object v1, p0, Loyf;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 181
    :cond_1c
    iget-object v0, p0, Loyf;->A:Ljava/lang/String;

    if-eqz v0, :cond_1d

    .line 182
    const/16 v0, 0xbe

    iget-object v1, p0, Loyf;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 184
    :cond_1d
    iget-object v0, p0, Loyf;->B:Ljava/lang/String;

    if-eqz v0, :cond_1e

    .line 185
    const/16 v0, 0xbf

    iget-object v1, p0, Loyf;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 187
    :cond_1e
    iget-object v0, p0, Loyf;->C:Ljava/lang/String;

    if-eqz v0, :cond_1f

    .line 188
    const/16 v0, 0xf9

    iget-object v1, p0, Loyf;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 190
    :cond_1f
    iget-object v0, p0, Loyf;->D:Ljava/lang/String;

    if-eqz v0, :cond_20

    .line 191
    const/16 v0, 0xfc

    iget-object v1, p0, Loyf;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 193
    :cond_20
    iget-object v0, p0, Loyf;->E:Ljava/lang/String;

    if-eqz v0, :cond_21

    .line 194
    const/16 v0, 0xfe

    iget-object v1, p0, Loyf;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 196
    :cond_21
    iget-object v0, p0, Loyf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 198
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loyf;->a(Loxn;)Loyf;

    move-result-object v0

    return-object v0
.end method
