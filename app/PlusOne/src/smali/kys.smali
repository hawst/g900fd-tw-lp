.class public final Lkys;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:[Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lkys;->b:Ldp;

    .line 40
    iput p2, p0, Lkys;->c:I

    .line 41
    iput-object p4, p0, Lkys;->d:[Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lkys;->e:Ljava/lang/String;

    .line 43
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 47
    invoke-virtual {p0}, Lkys;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lktq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    .line 49
    iget v1, p0, Lkys;->c:I

    iget-object v2, p0, Lkys;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lktq;->c(ILjava/lang/String;)J

    move-result-wide v2

    .line 50
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/32 v4, 0x124f80

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 52
    new-instance v1, Lkyp;

    invoke-virtual {p0}, Lkys;->n()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lkfo;

    .line 53
    invoke-virtual {p0}, Lkys;->n()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lkys;->c:I

    invoke-direct {v3, v4, v5}, Lkfo;-><init>(Landroid/content/Context;I)V

    iget-object v4, p0, Lkys;->e:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lkyp;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    .line 54
    invoke-virtual {v1}, Lkyp;->l()V

    .line 55
    invoke-virtual {v1}, Lkyp;->t()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    const/4 v0, 0x0

    .line 66
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :cond_1
    invoke-virtual {v1}, Lkyp;->D()Loxu;

    move-result-object v1

    check-cast v1, Lmdt;

    iget-object v1, v1, Lmdt;->a:Lnwd;

    iget-object v1, v1, Lnwd;->a:Lodt;

    iget-object v2, p0, Lkys;->e:Ljava/lang/String;

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    iget-object v2, v1, Lodt;->a:[Lodv;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lodt;->a:[Lodv;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    iget-object v5, v4, Lodv;->b:Lods;

    if-eqz v5, :cond_2

    iget-object v6, v4, Lodv;->c:Lodw;

    if-eqz v6, :cond_2

    iget-object v4, v4, Lodv;->c:Lodw;

    iget v6, v5, Lods;->b:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lkys;->e:Ljava/lang/String;

    iget-object v5, v5, Lods;->d:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget v5, p0, Lkys;->c:I

    iget-object v6, p0, Lkys;->e:Ljava/lang/String;

    invoke-interface {v0, v5, v6, v4}, Lktq;->a(ILjava/lang/String;Lodw;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 62
    :cond_3
    iget v1, p0, Lkys;->c:I

    iget-object v2, p0, Lkys;->e:Ljava/lang/String;

    iget-object v3, p0, Lkys;->d:[Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lktq;->a(ILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    iget-object v1, p0, Lkys;->b:Ldp;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method
