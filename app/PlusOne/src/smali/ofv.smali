.class public final Lofv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lofv;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lodp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 497
    const/4 v0, 0x0

    new-array v0, v0, [Lofv;

    sput-object v0, Lofv;->a:[Lofv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 498
    invoke-direct {p0}, Loxq;-><init>()V

    .line 515
    const/4 v0, 0x0

    iput-object v0, p0, Lofv;->i:Lodp;

    .line 498
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 542
    const/4 v0, 0x1

    iget-object v1, p0, Lofv;->b:Ljava/lang/String;

    .line 544
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 545
    const/4 v1, 0x2

    iget-object v2, p0, Lofv;->c:Ljava/lang/String;

    .line 546
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 547
    const/4 v1, 0x3

    iget-object v2, p0, Lofv;->e:Ljava/lang/Boolean;

    .line 548
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 549
    const/4 v1, 0x4

    iget-object v2, p0, Lofv;->f:Ljava/lang/Boolean;

    .line 550
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 551
    iget-object v1, p0, Lofv;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 552
    const/4 v1, 0x5

    iget-object v2, p0, Lofv;->d:Ljava/lang/String;

    .line 553
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 555
    :cond_0
    iget-object v1, p0, Lofv;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 556
    const/4 v1, 0x6

    iget-object v2, p0, Lofv;->g:Ljava/lang/String;

    .line 557
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 559
    :cond_1
    iget-object v1, p0, Lofv;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 560
    const/4 v1, 0x7

    iget-object v2, p0, Lofv;->h:Ljava/lang/String;

    .line 561
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 563
    :cond_2
    iget-object v1, p0, Lofv;->i:Lodp;

    if-eqz v1, :cond_3

    .line 564
    const/16 v1, 0x8

    iget-object v2, p0, Lofv;->i:Lodp;

    .line 565
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_3
    iget-object v1, p0, Lofv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 568
    iput v0, p0, Lofv;->ai:I

    .line 569
    return v0
.end method

.method public a(Loxn;)Lofv;
    .locals 2

    .prologue
    .line 577
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 578
    sparse-switch v0, :sswitch_data_0

    .line 582
    iget-object v1, p0, Lofv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 583
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofv;->ah:Ljava/util/List;

    .line 586
    :cond_1
    iget-object v1, p0, Lofv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 588
    :sswitch_0
    return-object p0

    .line 593
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofv;->b:Ljava/lang/String;

    goto :goto_0

    .line 597
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofv;->c:Ljava/lang/String;

    goto :goto_0

    .line 601
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofv;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 605
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofv;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 609
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofv;->d:Ljava/lang/String;

    goto :goto_0

    .line 613
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofv;->g:Ljava/lang/String;

    goto :goto_0

    .line 617
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofv;->h:Ljava/lang/String;

    goto :goto_0

    .line 621
    :sswitch_8
    iget-object v0, p0, Lofv;->i:Lodp;

    if-nez v0, :cond_2

    .line 622
    new-instance v0, Lodp;

    invoke-direct {v0}, Lodp;-><init>()V

    iput-object v0, p0, Lofv;->i:Lodp;

    .line 624
    :cond_2
    iget-object v0, p0, Lofv;->i:Lodp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 578
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 520
    const/4 v0, 0x1

    iget-object v1, p0, Lofv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 521
    const/4 v0, 0x2

    iget-object v1, p0, Lofv;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 522
    const/4 v0, 0x3

    iget-object v1, p0, Lofv;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 523
    const/4 v0, 0x4

    iget-object v1, p0, Lofv;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 524
    iget-object v0, p0, Lofv;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 525
    const/4 v0, 0x5

    iget-object v1, p0, Lofv;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 527
    :cond_0
    iget-object v0, p0, Lofv;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 528
    const/4 v0, 0x6

    iget-object v1, p0, Lofv;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 530
    :cond_1
    iget-object v0, p0, Lofv;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 531
    const/4 v0, 0x7

    iget-object v1, p0, Lofv;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 533
    :cond_2
    iget-object v0, p0, Lofv;->i:Lodp;

    if-eqz v0, :cond_3

    .line 534
    const/16 v0, 0x8

    iget-object v1, p0, Lofv;->i:Lodp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 536
    :cond_3
    iget-object v0, p0, Lofv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 538
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 494
    invoke-virtual {p0, p1}, Lofv;->a(Loxn;)Lofv;

    move-result-object v0

    return-object v0
.end method
