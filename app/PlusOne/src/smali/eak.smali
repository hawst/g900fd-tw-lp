.class public abstract Leak;
.super Llol;
.source "PG"

# interfaces
.implements Lhmq;


# instance fields
.field private N:Ljava/lang/Integer;

.field private O:Ljava/lang/Integer;

.field private P:Z

.field private Q:Z

.field private R:Lhoc;

.field private final S:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Llol;-><init>()V

    .line 43
    new-instance v0, Lhoc;

    iget-object v1, p0, Leak;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Leak;->R:Lhoc;

    .line 46
    new-instance v0, Leal;

    invoke-direct {v0, p0}, Leal;-><init>(Leak;)V

    iput-object v0, p0, Leak;->S:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-virtual {p1, p4, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 103
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 78
    if-eqz p1, :cond_1

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Leak;->Q:Z

    .line 80
    const-string v0, "n_pending_req"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "n_pending_req"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leak;->N:Ljava/lang/Integer;

    .line 84
    :cond_0
    const-string v0, "o_pending_req"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    const-string v0, "o_pending_req"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leak;->O:Ljava/lang/Integer;

    .line 88
    :cond_1
    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 263
    const v0, 0x7f10025f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 264
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 265
    return-void
.end method

.method public a(Landroid/widget/ProgressBar;)V
    .locals 1

    .prologue
    .line 273
    if-nez p1, :cond_0

    .line 279
    :goto_0
    return-void

    .line 277
    :cond_0
    iget-object v0, p0, Leak;->N:Ljava/lang/Integer;

    if-nez v0, :cond_1

    iget-object v0, p0, Leak;->O:Ljava/lang/Integer;

    if-nez v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public aO_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 111
    invoke-super {p0}, Llol;->aO_()V

    .line 114
    iget-object v0, p0, Leak;->N:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 115
    iget-object v0, p0, Leak;->N:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 116
    invoke-virtual {p0}, Leak;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 117
    invoke-virtual {p0}, Leak;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Leak;->d(Landroid/view/View;)V

    move v0, v1

    .line 125
    :goto_0
    iget-object v3, p0, Leak;->O:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    .line 126
    iget-object v3, p0, Leak;->O:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 127
    invoke-virtual {p0}, Leak;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    invoke-virtual {p0}, Leak;->x()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Leak;->d(Landroid/view/View;)V

    .line 136
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Leak;->N:Ljava/lang/Integer;

    if-nez v0, :cond_1

    iget-object v0, p0, Leak;->O:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 137
    invoke-virtual {p0}, Leak;->x()Landroid/view/View;

    .line 139
    invoke-virtual {p0}, Leak;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    invoke-virtual {p0}, Leak;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Leak;->g(Landroid/view/View;)V

    .line 144
    :cond_1
    iput-boolean v1, p0, Leak;->P:Z

    .line 145
    return-void

    .line 120
    :cond_2
    iput-object v4, p0, Leak;->N:Ljava/lang/Integer;

    move v0, v2

    .line 121
    goto :goto_0

    .line 131
    :cond_3
    iput-object v4, p0, Leak;->O:Ljava/lang/Integer;

    move v0, v2

    .line 132
    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public ab()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Leak;->P:Z

    return v0
.end method

.method protected ac()V
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Leak;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Leak;->ab()Z

    move-result v0

    if-nez v0, :cond_0

    .line 205
    invoke-virtual {p0}, Leak;->x()Landroid/view/View;

    move-result-object v0

    .line 206
    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {p0, v0}, Leak;->e(Landroid/view/View;)V

    .line 210
    :cond_0
    return-void
.end method

.method protected ad()V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Leak;->S:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 286
    return-void
.end method

.method public ae()Lhoc;
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Leak;->R:Lhoc;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 302
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 63
    iget-object v0, p0, Leak;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 64
    return-void
.end method

.method public d(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 190
    iget-boolean v0, p0, Leak;->Q:Z

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Leak;->S:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Leak;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Leak;->S:Landroid/os/Handler;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    invoke-virtual {p0, p1}, Leak;->e(Landroid/view/View;)V

    goto :goto_0
.end method

.method public abstract d()Z
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 170
    iget-object v0, p0, Leak;->N:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "n_pending_req"

    iget-object v1, p0, Leak;->N:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 174
    :cond_0
    iget-object v0, p0, Leak;->O:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 175
    const-string v0, "o_pending_req"

    iget-object v1, p0, Leak;->O:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 177
    :cond_1
    return-void
.end method

.method protected e(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 216
    invoke-virtual {p0}, Leak;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 218
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 219
    const v1, 0x7f10025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 220
    const v1, 0x7f100260

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 222
    :cond_0
    return-void
.end method

.method protected f(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 228
    invoke-virtual {p0}, Leak;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 230
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 231
    const v1, 0x7f10025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 232
    const v1, 0x7f100260

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 234
    :cond_0
    return-void
.end method

.method public g(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 242
    invoke-virtual {p0}, Leak;->ad()V

    .line 243
    invoke-virtual {p0, p1}, Leak;->f(Landroid/view/View;)V

    .line 244
    return-void
.end method

.method public h(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 252
    invoke-virtual {p0}, Leak;->ad()V

    .line 253
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 254
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 152
    invoke-super {p0}, Llol;->z()V

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Leak;->P:Z

    .line 155
    return-void
.end method
