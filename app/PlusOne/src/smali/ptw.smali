.class public final Lptw;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lptz;

.field private b:Ljava/lang/Float;

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Loxq;-><init>()V

    .line 112
    sget-object v0, Lptz;->a:[Lptz;

    iput-object v0, p0, Lptw;->a:[Lptz;

    .line 109
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 145
    .line 146
    iget-object v1, p0, Lptw;->a:[Lptz;

    if-eqz v1, :cond_1

    .line 147
    iget-object v2, p0, Lptw;->a:[Lptz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 148
    if-eqz v4, :cond_0

    .line 149
    const/4 v5, 0x1

    .line 150
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 147
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 154
    :cond_1
    iget-object v1, p0, Lptw;->b:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 155
    const/4 v1, 0x2

    iget-object v2, p0, Lptw;->b:Ljava/lang/Float;

    .line 156
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 158
    :cond_2
    iget-object v1, p0, Lptw;->c:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 159
    const/4 v1, 0x3

    iget-object v2, p0, Lptw;->c:Ljava/lang/Float;

    .line 160
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 162
    :cond_3
    iget-object v1, p0, Lptw;->d:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 163
    const/4 v1, 0x4

    iget-object v2, p0, Lptw;->d:Ljava/lang/Float;

    .line 164
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 166
    :cond_4
    iget-object v1, p0, Lptw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    iput v0, p0, Lptw;->ai:I

    .line 168
    return v0
.end method

.method public a(Loxn;)Lptw;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 176
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 177
    sparse-switch v0, :sswitch_data_0

    .line 181
    iget-object v2, p0, Lptw;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 182
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lptw;->ah:Ljava/util/List;

    .line 185
    :cond_1
    iget-object v2, p0, Lptw;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 187
    :sswitch_0
    return-object p0

    .line 192
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 193
    iget-object v0, p0, Lptw;->a:[Lptz;

    if-nez v0, :cond_3

    move v0, v1

    .line 194
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lptz;

    .line 195
    iget-object v3, p0, Lptw;->a:[Lptz;

    if-eqz v3, :cond_2

    .line 196
    iget-object v3, p0, Lptw;->a:[Lptz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    :cond_2
    iput-object v2, p0, Lptw;->a:[Lptz;

    .line 199
    :goto_2
    iget-object v2, p0, Lptw;->a:[Lptz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 200
    iget-object v2, p0, Lptw;->a:[Lptz;

    new-instance v3, Lptz;

    invoke-direct {v3}, Lptz;-><init>()V

    aput-object v3, v2, v0

    .line 201
    iget-object v2, p0, Lptw;->a:[Lptz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 202
    invoke-virtual {p1}, Loxn;->a()I

    .line 199
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 193
    :cond_3
    iget-object v0, p0, Lptw;->a:[Lptz;

    array-length v0, v0

    goto :goto_1

    .line 205
    :cond_4
    iget-object v2, p0, Lptw;->a:[Lptz;

    new-instance v3, Lptz;

    invoke-direct {v3}, Lptz;-><init>()V

    aput-object v3, v2, v0

    .line 206
    iget-object v2, p0, Lptw;->a:[Lptz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 210
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lptw;->b:Ljava/lang/Float;

    goto :goto_0

    .line 214
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lptw;->c:Ljava/lang/Float;

    goto :goto_0

    .line 218
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lptw;->d:Ljava/lang/Float;

    goto/16 :goto_0

    .line 177
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 123
    iget-object v0, p0, Lptw;->a:[Lptz;

    if-eqz v0, :cond_1

    .line 124
    iget-object v1, p0, Lptw;->a:[Lptz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 125
    if-eqz v3, :cond_0

    .line 126
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 124
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130
    :cond_1
    iget-object v0, p0, Lptw;->b:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 131
    const/4 v0, 0x2

    iget-object v1, p0, Lptw;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 133
    :cond_2
    iget-object v0, p0, Lptw;->c:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 134
    const/4 v0, 0x3

    iget-object v1, p0, Lptw;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 136
    :cond_3
    iget-object v0, p0, Lptw;->d:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 137
    const/4 v0, 0x4

    iget-object v1, p0, Lptw;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 139
    :cond_4
    iget-object v0, p0, Lptw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 141
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lptw;->a(Loxn;)Lptw;

    move-result-object v0

    return-object v0
.end method
