.class public final Lldj;
.super Llch;
.source "PG"


# instance fields
.field private c:Llby;

.field private d:Landroid/text/StaticLayout;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lldj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lldj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Llch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lldj;->setClickable(Z)V

    .line 44
    return-void
.end method


# virtual methods
.method protected a(IIII)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 70
    invoke-virtual {p0}, Lldj;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 71
    new-instance v0, Landroid/text/StaticLayout;

    const v1, 0x7f0a0484

    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x15

    .line 72
    invoke-static {v2, v3}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move v3, p4

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lldj;->d:Landroid/text/StaticLayout;

    .line 76
    sget-object v0, Lldj;->z:Llct;

    iget v0, v0, Llct;->aZ:I

    iget-object v1, p0, Lldj;->d:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lldj;->z:Llct;

    iget v1, v1, Llct;->aZ:I

    add-int/2addr v0, v1

    add-int/2addr v0, p2

    .line 78
    iput v0, p0, Lldj;->e:I

    .line 80
    iget v1, p0, Lldj;->t:I

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 82
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 83
    invoke-virtual {p0}, Lldj;->getChildCount()I

    move-result v3

    :goto_0
    if-ge v7, v3, :cond_0

    .line 84
    invoke-virtual {p0, v7}, Lldj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 85
    invoke-virtual {v4, v1, v2}, Landroid/view/View;->measure(II)V

    .line 86
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    .line 83
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 89
    :cond_0
    return v0
.end method

.method protected a(Landroid/graphics/Canvas;IIIII)I
    .locals 6

    .prologue
    .line 113
    sget-object v0, Lldj;->z:Llct;

    iget v0, v0, Llct;->aZ:I

    add-int/2addr v0, p6

    .line 116
    iget-object v1, p0, Lldj;->d:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    .line 117
    int-to-float v1, p4

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 118
    iget-object v1, p0, Lldj;->d:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 119
    neg-int v1, p4

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 120
    iget-object v1, p0, Lldj;->d:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget-object v2, Lldj;->z:Llct;

    iget v2, v2, Llct;->aZ:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 121
    sget-object v1, Lldj;->z:Llct;

    iget v1, v1, Llct;->m:I

    sub-int v3, p3, v1

    .line 122
    sget-object v1, Lldj;->z:Llct;

    iget v1, v1, Llct;->m:I

    int-to-float v1, v1

    int-to-float v2, v0

    int-to-float v3, v3

    int-to-float v4, v0

    sget-object v0, Lldj;->z:Llct;

    iget-object v5, v0, Llct;->w:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 126
    :cond_0
    invoke-virtual {p0}, Lldj;->getHeight()I

    move-result v0

    add-int/2addr v0, p6

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-super {p0}, Llch;->a()V

    .line 133
    invoke-virtual {p0}, Lldj;->m()V

    .line 135
    iput-object v0, p0, Lldj;->c:Llby;

    .line 136
    iput-object v0, p0, Lldj;->d:Landroid/text/StaticLayout;

    .line 137
    const/4 v0, 0x0

    iput v0, p0, Lldj;->e:I

    .line 138
    return-void
.end method

.method protected a(Landroid/database/Cursor;Llcr;I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-super {p0, p1, p2, p3}, Llch;->a(Landroid/database/Cursor;Llcr;I)V

    .line 49
    const/16 v0, 0x1b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 50
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    iput-object v0, p0, Lldj;->c:Llby;

    .line 52
    invoke-virtual {p0}, Lldj;->m()V

    .line 53
    invoke-virtual {p0}, Lldj;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 54
    iget-object v0, p0, Lldj;->c:Llby;

    invoke-virtual {v0}, Llby;->a()I

    move-result v0

    const/4 v2, 0x3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v2, v1

    .line 56
    :goto_0
    if-ge v2, v4, :cond_3

    .line 57
    new-instance v5, Lldi;

    invoke-direct {v5, v3}, Lldi;-><init>(Landroid/content/Context;)V

    .line 58
    iget-object v6, p0, Lldj;->a:Llci;

    iget-object v0, p0, Lldj;->c:Llby;

    invoke-virtual {v0, v2}, Llby;->a(I)Ljava/lang/String;

    move-result-object v7

    iget-object v0, p0, Lldj;->c:Llby;

    .line 59
    invoke-virtual {v0, v2}, Llby;->b(I)I

    move-result v8

    add-int/lit8 v0, v4, -0x1

    if-eq v2, v0, :cond_2

    const/4 v0, 0x1

    .line 58
    :goto_1
    invoke-virtual {v5, v6, v7, v8, v0}, Lldi;->a(Llci;Ljava/lang/String;IZ)V

    .line 60
    invoke-virtual {p0, v5}, Lldj;->addView(Landroid/view/View;)V

    .line 56
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 50
    :cond_1
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    new-instance v0, Llby;

    invoke-direct {v0}, Llby;-><init>()V

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, v0, Llby;->a:[Ljava/lang/String;

    new-array v2, v4, [I

    iput-object v2, v0, Llby;->b:[I

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_0

    iget-object v5, v0, Llby;->a:[Ljava/lang/String;

    invoke-static {v3}, Llby;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    iget-object v5, v0, Llby;->b:[I

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    aput v6, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 59
    goto :goto_1

    .line 63
    :cond_3
    invoke-virtual {p0}, Lldj;->requestLayout()V

    .line 64
    invoke-virtual {p0}, Lldj;->invalidate()V

    .line 65
    return-void
.end method

.method protected a(ZIIII)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 94
    invoke-super/range {p0 .. p5}, Llch;->a(ZIIII)V

    .line 96
    iget v1, p0, Lldj;->e:I

    .line 97
    iget v2, p0, Lldj;->t:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 99
    invoke-static {v0, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 100
    invoke-virtual {p0}, Lldj;->getChildCount()I

    move-result v4

    :goto_0
    if-ge v0, v4, :cond_0

    .line 101
    invoke-virtual {p0, v0}, Lldj;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 102
    invoke-virtual {v5, v2, v3}, Landroid/view/View;->measure(II)V

    .line 103
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 104
    iget-object v7, p0, Lldj;->q:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Lldj;->q:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget v9, p0, Lldj;->t:I

    add-int/2addr v8, v9

    add-int v9, v1, v6

    invoke-virtual {v5, v7, v1, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 106
    add-int/2addr v1, v6

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_0
    return-void
.end method
