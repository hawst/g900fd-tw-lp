.class public final Llsx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/lang/Thread;

.field private static b:Landroid/os/Handler;


# direct methods
.method public static a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 69
    return-void
.end method

.method public static a(Ljava/lang/Runnable;J)V
    .locals 1

    .prologue
    .line 82
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 83
    return-void
.end method

.method public static a(Ljava/lang/Runnable;Z)V
    .locals 1

    .prologue
    .line 72
    if-eqz p1, :cond_0

    .line 73
    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 75
    :cond_0
    invoke-static {p0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 76
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 22
    sget-object v0, Llsx;->a:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 23
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    sput-object v0, Llsx;->a:Ljava/lang/Thread;

    .line 25
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Llsx;->a:Ljava/lang/Thread;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 39
    invoke-static {}, Llsx;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must be called on the UI thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    return-void
.end method

.method public static c()V
    .locals 2

    .prologue
    .line 48
    invoke-static {}, Llsx;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Must be called on a background thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    return-void
.end method

.method public static d()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 57
    sget-object v0, Llsx;->b:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Llsx;->b:Landroid/os/Handler;

    .line 61
    :cond_0
    sget-object v0, Llsx;->b:Landroid/os/Handler;

    return-object v0
.end method
