.class public final Lohm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lohm;


# instance fields
.field public b:Lohn;

.field public c:Lohw;

.field private d:Ljava/lang/Boolean;

.field private e:Loia;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3510
    const/4 v0, 0x0

    new-array v0, v0, [Lohm;

    sput-object v0, Lohm;->a:[Lohm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3511
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3514
    iput-object v0, p0, Lohm;->b:Lohn;

    .line 3517
    iput-object v0, p0, Lohm;->c:Lohw;

    .line 3522
    iput-object v0, p0, Lohm;->e:Loia;

    .line 3511
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3545
    const/4 v0, 0x0

    .line 3546
    iget-object v1, p0, Lohm;->b:Lohn;

    if-eqz v1, :cond_0

    .line 3547
    const/4 v0, 0x1

    iget-object v1, p0, Lohm;->b:Lohn;

    .line 3548
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3550
    :cond_0
    iget-object v1, p0, Lohm;->c:Lohw;

    if-eqz v1, :cond_1

    .line 3551
    const/4 v1, 0x2

    iget-object v2, p0, Lohm;->c:Lohw;

    .line 3552
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3554
    :cond_1
    iget-object v1, p0, Lohm;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3555
    const/4 v1, 0x3

    iget-object v2, p0, Lohm;->d:Ljava/lang/Boolean;

    .line 3556
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3558
    :cond_2
    iget-object v1, p0, Lohm;->e:Loia;

    if-eqz v1, :cond_3

    .line 3559
    const/4 v1, 0x5

    iget-object v2, p0, Lohm;->e:Loia;

    .line 3560
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3562
    :cond_3
    iget-object v1, p0, Lohm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3563
    iput v0, p0, Lohm;->ai:I

    .line 3564
    return v0
.end method

.method public a(Loxn;)Lohm;
    .locals 2

    .prologue
    .line 3572
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3573
    sparse-switch v0, :sswitch_data_0

    .line 3577
    iget-object v1, p0, Lohm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3578
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohm;->ah:Ljava/util/List;

    .line 3581
    :cond_1
    iget-object v1, p0, Lohm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3583
    :sswitch_0
    return-object p0

    .line 3588
    :sswitch_1
    iget-object v0, p0, Lohm;->b:Lohn;

    if-nez v0, :cond_2

    .line 3589
    new-instance v0, Lohn;

    invoke-direct {v0}, Lohn;-><init>()V

    iput-object v0, p0, Lohm;->b:Lohn;

    .line 3591
    :cond_2
    iget-object v0, p0, Lohm;->b:Lohn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3595
    :sswitch_2
    iget-object v0, p0, Lohm;->c:Lohw;

    if-nez v0, :cond_3

    .line 3596
    new-instance v0, Lohw;

    invoke-direct {v0}, Lohw;-><init>()V

    iput-object v0, p0, Lohm;->c:Lohw;

    .line 3598
    :cond_3
    iget-object v0, p0, Lohm;->c:Lohw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3602
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lohm;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 3606
    :sswitch_4
    iget-object v0, p0, Lohm;->e:Loia;

    if-nez v0, :cond_4

    .line 3607
    new-instance v0, Loia;

    invoke-direct {v0}, Loia;-><init>()V

    iput-object v0, p0, Lohm;->e:Loia;

    .line 3609
    :cond_4
    iget-object v0, p0, Lohm;->e:Loia;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3573
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3527
    iget-object v0, p0, Lohm;->b:Lohn;

    if-eqz v0, :cond_0

    .line 3528
    const/4 v0, 0x1

    iget-object v1, p0, Lohm;->b:Lohn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3530
    :cond_0
    iget-object v0, p0, Lohm;->c:Lohw;

    if-eqz v0, :cond_1

    .line 3531
    const/4 v0, 0x2

    iget-object v1, p0, Lohm;->c:Lohw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3533
    :cond_1
    iget-object v0, p0, Lohm;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3534
    const/4 v0, 0x3

    iget-object v1, p0, Lohm;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 3536
    :cond_2
    iget-object v0, p0, Lohm;->e:Loia;

    if-eqz v0, :cond_3

    .line 3537
    const/4 v0, 0x5

    iget-object v1, p0, Lohm;->e:Loia;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3539
    :cond_3
    iget-object v0, p0, Lohm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3541
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3507
    invoke-virtual {p0, p1}, Lohm;->a(Loxn;)Lohm;

    move-result-object v0

    return-object v0
.end method
