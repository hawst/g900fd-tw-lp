.class public final Ljks;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljjm;
.implements Ljjp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    const-string v0, "com.google.android.libraries.social.notifications.UPDATE_LAST_VIEWED_VERSION"

    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 23
    invoke-static {p1}, Lcom/google/android/libraries/social/notifications/impl/GunsIntentService;->a(Landroid/content/Intent;)Lkfo;

    move-result-object v8

    .line 24
    if-nez v8, :cond_1

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 28
    :cond_1
    invoke-static {p2}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v0

    invoke-virtual {v8}, Lkfo;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lkfo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "GunsSyncer"

    const-string v1, "Cannot find database helper for account."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    :cond_2
    :goto_1
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 32
    const-string v0, "sync_version"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 33
    const-string v1, "viewed_sync_version"

    .line 34
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 33
    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 36
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 40
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 41
    new-instance v1, Ljkt;

    .line 42
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, p2, v8, v2, v3}, Ljkt;-><init>(Landroid/content/Context;Lkfo;J)V

    .line 45
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 46
    const-string v3, "new_last_viewed_version"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 48
    const-class v0, Ljjo;

    invoke-static {p2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljjo;

    .line 49
    invoke-virtual {v0, v1, p0, p2, v2}, Ljjo;->a(Lkgg;Ljjp;Landroid/content/Context;Landroid/os/Bundle;)V

    goto :goto_0

    .line 28
    :cond_3
    invoke-virtual {v0}, Ljky;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "user_data"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_2

    move-object v2, v0

    goto :goto_1

    .line 36
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public a(Lkgg;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 60
    invoke-virtual {p1}, Lkgg;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p1, Lkff;->g:Lkfo;

    .line 62
    const-string v1, "new_last_viewed_version"

    const-wide/16 v2, 0x0

    invoke-virtual {p3, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 63
    invoke-virtual {v0}, Lkfo;->a()Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-virtual {v0}, Lkfo;->b()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-static {p2}, Ljkz;->a(Landroid/content/Context;)Ljkz;

    move-result-object v4

    invoke-virtual {v4, v1, v0}, Ljkz;->a(Ljava/lang/String;Ljava/lang/String;)Ljky;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "GunsSyncer"

    const-string v1, "Cannot find database helper for account."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    invoke-virtual {v0}, Ljky;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "sync_version"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "user_data"

    const-string v5, "sync_version < ? "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    invoke-virtual {v0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Ljks;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
