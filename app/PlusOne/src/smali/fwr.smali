.class public final Lfwr;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lljh;


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:Lfws;

.field private d:[Lfwq;

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfwr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    invoke-direct {p0, p1}, Lfwr;->a(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    invoke-direct {p0, p1}, Lfwr;->a(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Llcr;

    invoke-direct {v0, p1}, Llcr;-><init>(Landroid/content/Context;)V

    .line 54
    iget v0, v0, Llcr;->d:I

    iput v0, p0, Lfwr;->b:I

    .line 55
    iput-object p1, p0, Lfwr;->a:Landroid/content/Context;

    .line 56
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lfwr;->e:I

    if-ge v0, v1, :cond_1

    .line 61
    iget-object v1, p0, Lfwr;->d:[Lfwq;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 62
    iget-object v1, p0, Lfwr;->d:[Lfwq;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lfwq;->a()V

    .line 60
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_1
    return-void
.end method

.method public a(Landroid/database/Cursor;[ILfws;)V
    .locals 4

    .prologue
    .line 68
    invoke-virtual {p0}, Lfwr;->removeAllViews()V

    .line 69
    iput-object p3, p0, Lfwr;->c:Lfws;

    .line 70
    array-length v0, p2

    iput v0, p0, Lfwr;->e:I

    .line 72
    iget v0, p0, Lfwr;->e:I

    new-array v0, v0, [Lfwq;

    iput-object v0, p0, Lfwr;->d:[Lfwq;

    .line 74
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lfwr;->e:I

    if-ge v0, v1, :cond_1

    .line 75
    aget v1, p2, v0

    .line 76
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 77
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p0, Lfwr;->d:[Lfwq;

    new-instance v2, Lfwq;

    iget-object v3, p0, Lfwr;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lfwq;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v0

    .line 82
    iget-object v1, p0, Lfwr;->d:[Lfwq;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lfwq;->a(Landroid/database/Cursor;)V

    .line 83
    iget-object v1, p0, Lfwr;->d:[Lfwq;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Lfwq;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v1, p0, Lfwr;->d:[Lfwq;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lfwr;->addView(Landroid/view/View;)V

    .line 74
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lfwr;->c:Lfws;

    if-eqz v0, :cond_0

    .line 142
    check-cast p1, Lfwq;

    .line 143
    invoke-virtual {p1}, Lfwq;->b()Ljava/lang/String;

    move-result-object v0

    .line 144
    invoke-virtual {p1}, Lfwq;->c()Ljava/lang/String;

    move-result-object v1

    .line 145
    iget-object v2, p0, Lfwr;->c:Lfws;

    invoke-interface {v2, v0, v1}, Lfws;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 119
    invoke-virtual {p0}, Lfwr;->getPaddingTop()I

    move-result v2

    .line 120
    iget v0, p0, Lfwr;->g:I

    add-int v3, v2, v0

    .line 121
    invoke-virtual {p0}, Lfwr;->getPaddingLeft()I

    move-result v1

    .line 123
    const/4 v0, 0x0

    :goto_0
    iget v4, p0, Lfwr;->e:I

    if-ge v0, v4, :cond_1

    .line 124
    iget-object v4, p0, Lfwr;->d:[Lfwq;

    aget-object v4, v4, v0

    if-eqz v4, :cond_0

    .line 125
    iget-object v4, p0, Lfwr;->d:[Lfwq;

    aget-object v4, v4, v0

    iget v5, p0, Lfwr;->f:I

    add-int/2addr v5, v1

    invoke-virtual {v4, v1, v2, v5, v3}, Lfwq;->layout(IIII)V

    .line 126
    iget v4, p0, Lfwr;->f:I

    iget v5, p0, Lfwr;->b:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    .line 123
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lfwr;->d:[Lfwq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfwr;->d:[Lfwq;

    array-length v0, v0

    .line 92
    :goto_0
    if-nez v0, :cond_1

    .line 93
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 115
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 91
    goto :goto_0

    .line 97
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 98
    invoke-virtual {p0}, Lfwr;->getPaddingLeft()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p0}, Lfwr;->getPaddingRight()I

    move-result v3

    sub-int/2addr v0, v3

    .line 100
    iget v3, p0, Lfwr;->b:I

    iget v4, p0, Lfwr;->e:I

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    .line 101
    sub-int/2addr v0, v3

    iget v3, p0, Lfwr;->e:I

    div-int/2addr v0, v3

    iput v0, p0, Lfwr;->f:I

    move v0, v1

    .line 103
    :goto_2
    iget v3, p0, Lfwr;->e:I

    if-ge v0, v3, :cond_3

    .line 104
    iget-object v3, p0, Lfwr;->d:[Lfwq;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    .line 105
    iget-object v3, p0, Lfwr;->d:[Lfwq;

    aget-object v3, v3, v0

    iget v4, p0, Lfwr;->f:I

    const/high16 v5, 0x40000000    # 2.0f

    .line 106
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 107
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 105
    invoke-virtual {v3, v4, v5}, Lfwq;->measure(II)V

    .line 103
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 111
    :cond_3
    iget-object v0, p0, Lfwr;->d:[Lfwq;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lfwq;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lfwr;->g:I

    .line 112
    iget v0, p0, Lfwr;->g:I

    invoke-virtual {p0}, Lfwr;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lfwr;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    invoke-virtual {p0, v2, v0}, Lfwr;->setMeasuredDimension(II)V

    goto :goto_1
.end method

.method public requestLayout()V
    .locals 0

    .prologue
    .line 136
    invoke-virtual {p0}, Lfwr;->forceLayout()V

    .line 137
    return-void
.end method
