.class public final Lebp;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ldxj;


# instance fields
.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;

.field private P:Lhgw;

.field private Q:Lhgw;

.field private R:Ljava/lang/String;

.field private S:J

.field private T:Z

.field private U:Z

.field private V:Lhee;

.field private W:Landroid/view/View;

.field private X:Landroid/widget/CheckedTextView;

.field private Y:Landroid/widget/CheckedTextView;

.field private Z:Landroid/widget/TextView;

.field private aa:Landroid/widget/TextView;

.field private ab:Landroid/widget/Button;

.field private ac:Landroid/widget/Button;

.field private ad:Lhoc;

.field private ae:Lhob;

.field private af:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Llol;-><init>()V

    .line 80
    new-instance v0, Lhoc;

    .line 81
    invoke-virtual {p0}, Lebp;->z_()Llqr;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Lebp;->ad:Lhoc;

    .line 83
    new-instance v0, Lebq;

    invoke-direct {v0, p0}, Lebq;-><init>(Lebp;)V

    iput-object v0, p0, Lebp;->ae:Lhob;

    .line 413
    new-instance v0, Lebr;

    invoke-direct {v0, p0}, Lebr;-><init>(Lebp;)V

    iput-object v0, p0, Lebp;->af:Lbc;

    return-void
.end method

.method private U()I
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lebp;->V:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    return v0
.end method

.method static synthetic a(Lebp;J)J
    .locals 1

    .prologue
    .line 48
    iput-wide p1, p0, Lebp;->S:J

    return-wide p1
.end method

.method static synthetic a(Lebp;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lebp;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lebp;->R:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lebp;Lhoz;)V
    .locals 3

    .prologue
    .line 48
    invoke-virtual {p1}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0997

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a07ae

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 6

    .prologue
    .line 395
    iget-object v0, p0, Lebp;->ad:Lhoc;

    const-string v1, "UpdateCollectionShareLinkTask"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    new-instance v0, Ldqh;

    .line 397
    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v1

    invoke-direct {p0}, Lebp;->U()I

    move-result v2

    iget-object v3, p0, Lebp;->N:Ljava/lang/String;

    iget-object v4, p0, Lebp;->O:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Ldqh;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 399
    iget-object v1, p0, Lebp;->ad:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->c(Lhny;)V

    .line 401
    :cond_0
    return-void
.end method

.method private a(J)Z
    .locals 5

    .prologue
    .line 267
    iget-wide v0, p0, Lebp;->S:J

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lebp;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lebp;->U:Z

    return p1
.end method

.method private a(Lhgw;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 293
    invoke-virtual {p1}, Lhgw;->b()[Lhxc;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 294
    invoke-virtual {v4}, Lhxc;->c()I

    move-result v4

    const/16 v5, 0x9

    if-ne v4, v5, :cond_1

    .line 295
    const/4 v0, 0x1

    .line 298
    :cond_0
    return v0

    .line 293
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lebp;)I
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lebp;->U()I

    move-result v0

    return v0
.end method

.method static synthetic b(Lebp;Lhoz;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 48
    invoke-virtual {p1}, Lhoz;->f()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "allow_share_via_link"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "album_link_url"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lebp;->R:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    invoke-direct {p0}, Lebp;->e()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0997

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a07ae

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic b(Lebp;J)Z
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lebp;->a(J)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lebp;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Lebp;->T:Z

    return p1
.end method

.method static synthetic c(Lebp;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lebp;->N:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 8

    .prologue
    const-wide/32 v6, 0x8000

    const v4, 0x7f0a099e

    const/4 v1, 0x0

    .line 272
    iget-object v0, p0, Lebp;->W:Landroid/view/View;

    const-wide/16 v2, 0x1000

    invoke-direct {p0, v2, v3}, Lebp;->a(J)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 273
    iget-object v2, p0, Lebp;->Z:Landroid/widget/TextView;

    iget-object v0, p0, Lebp;->Q:Lhgw;

    if-nez v0, :cond_2

    invoke-virtual {p0, v4}, Lebp;->e_(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v0, p0, Lebp;->Q:Lhgw;

    invoke-direct {p0, v0}, Lebp;->a(Lhgw;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 276
    iget-object v0, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 277
    iget-object v0, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 282
    :goto_1
    iget-object v0, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    .line 283
    invoke-direct {p0, v6, v7}, Lebp;->a(J)Z

    move-result v2

    .line 282
    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 284
    iget-object v0, p0, Lebp;->Y:Landroid/widget/CheckedTextView;

    const-wide/32 v2, 0x10000

    .line 285
    invoke-direct {p0, v2, v3}, Lebp;->a(J)Z

    move-result v2

    .line 284
    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 286
    iget-object v2, p0, Lebp;->aa:Landroid/widget/TextView;

    .line 287
    invoke-direct {p0, v6, v7}, Lebp;->a(J)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lebp;->R:Ljava/lang/String;

    .line 288
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    .line 286
    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 289
    return-void

    .line 273
    :cond_2
    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhgw;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v4}, Lebp;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 279
    :cond_3
    iget-object v0, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v0, v1

    .line 288
    goto :goto_2
.end method

.method static synthetic d(Lebp;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lebp;->T:Z

    return v0
.end method

.method static synthetic e(Lebp;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 381
    iget-object v0, p0, Lebp;->Q:Lhgw;

    invoke-direct {p0, v0}, Lebp;->a(Lhgw;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lebp;->P:Lhgw;

    .line 382
    invoke-direct {p0, v0}, Lebp;->a(Lhgw;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 383
    :goto_0
    iget-object v1, p0, Lebp;->R:Ljava/lang/String;

    .line 384
    invoke-static {v1, v0, p0}, Ldxh;->a(Ljava/lang/String;ZLu;)Ldxh;

    move-result-object v0

    .line 385
    invoke-virtual {p0}, Lebp;->p()Lae;

    move-result-object v1

    const-string v2, "share_via_link"

    invoke-virtual {v0, v1, v2}, Ldxh;->a(Lae;Ljava/lang/String;)V

    .line 386
    return-void

    .line 382
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lebp;)Landroid/widget/CheckedTextView;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lebp;->Y:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method static synthetic g(Lebp;)Z
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lebp;->V:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "is_dasher_account"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lebp;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lebp;->d()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 141
    const v0, 0x7f040042

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 143
    const v0, 0x7f10018b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lebp;->W:Landroid/view/View;

    .line 144
    iget-object v0, p0, Lebp;->W:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    const v0, 0x7f10018c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebp;->Z:Landroid/widget/TextView;

    .line 146
    const v0, 0x7f10018d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    .line 147
    iget-object v0, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, p0}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    const v0, 0x7f10018e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lebp;->Y:Landroid/widget/CheckedTextView;

    .line 149
    iget-object v0, p0, Lebp;->Y:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, p0}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    const v0, 0x7f10018f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lebp;->aa:Landroid/widget/TextView;

    .line 151
    iget-object v0, p0, Lebp;->aa:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    const v0, 0x7f100191

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lebp;->ab:Landroid/widget/Button;

    .line 154
    iget-object v0, p0, Lebp;->ab:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    const v0, 0x7f100178

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lebp;->ac:Landroid/widget/Button;

    .line 156
    iget-object v0, p0, Lebp;->ac:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    invoke-virtual {p0}, Lebp;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lebp;->af:Lbc;

    invoke-virtual {v0, v4, v2, v3}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 161
    if-eqz p3, :cond_0

    .line 162
    iget-object v0, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    const-string v2, "disable_reshares"

    invoke-virtual {p3, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 163
    iget-object v0, p0, Lebp;->Y:Landroid/widget/CheckedTextView;

    const-string v2, "show_location_data"

    .line 164
    invoke-virtual {p3, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 163
    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 167
    :cond_0
    return-object v1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lebp;->a(Z)V

    .line 391
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 352
    packed-switch p1, :pswitch_data_0

    .line 363
    invoke-super {p0, p1, p2, p3}, Llol;->a(IILandroid/content/Intent;)V

    .line 368
    :goto_0
    return-void

    .line 354
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 355
    const-string v0, "extra_acl"

    .line 356
    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    .line 355
    invoke-static {v0}, Lhgw;->a(Lhgw;)Lhgw;

    move-result-object v0

    iput-object v0, p0, Lebp;->Q:Lhgw;

    .line 357
    invoke-direct {p0}, Lebp;->d()V

    .line 359
    :cond_0
    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v0

    const v1, 0x7f05000c

    const v2, 0x7f05001d

    invoke-virtual {v0, v1, v2}, Lz;->overridePendingTransition(II)V

    goto :goto_0

    .line 352
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 111
    if-eqz p1, :cond_0

    .line 112
    const-string v0, "new_audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Lebp;->Q:Lhgw;

    .line 113
    const-string v0, "first_load_finished"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lebp;->T:Z

    .line 116
    :cond_0
    invoke-virtual {p0}, Lebp;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 117
    const-string v1, "cluster_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lebp;->N:Ljava/lang/String;

    .line 118
    const-string v1, "auth_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lebp;->O:Ljava/lang/String;

    .line 119
    const-string v1, "extra_acl"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhgw;

    iput-object v0, p0, Lebp;->P:Lhgw;

    .line 120
    iget-object v0, p0, Lebp;->Q:Lhgw;

    if-nez v0, :cond_1

    .line 121
    iget-object v0, p0, Lebp;->P:Lhgw;

    invoke-virtual {v0}, Lhgw;->l()Lhgw;

    move-result-object v0

    iput-object v0, p0, Lebp;->Q:Lhgw;

    .line 124
    :cond_1
    iget-object v0, p0, Lebp;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lebp;->V:Lhee;

    .line 125
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0}, Llol;->aO_()V

    .line 175
    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 176
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 100
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 102
    iget-object v0, p0, Lebp;->ad:Lhoc;

    iget-object v1, p0, Lebp;->ae:Lhob;

    invoke-virtual {v0, v1}, Lhoc;->a(Lhob;)Lhoc;

    .line 103
    iget-object v0, p0, Lebp;->ad:Lhoc;

    new-instance v1, Lhpf;

    invoke-virtual {p0}, Lebp;->n()Lz;

    move-result-object v2

    .line 104
    invoke-virtual {p0}, Lebp;->p()Lae;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lhpf;-><init>(Landroid/content/Context;Lae;)V

    .line 103
    invoke-virtual {v0, v1}, Lhoc;->a(Lhos;)V

    .line 105
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 131
    const-string v0, "new_audience"

    iget-object v1, p0, Lebp;->Q:Lhgw;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 132
    const-string v0, "first_load_finished"

    iget-boolean v1, p0, Lebp;->T:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 133
    const-string v0, "disable_reshares"

    iget-object v1, p0, Lebp;->X:Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 134
    const-string v0, "show_location_data"

    iget-object v1, p0, Lebp;->Y:Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 135
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 16

    .prologue
    .line 245
    move-object/from16 v0, p1

    instance-of v1, v0, Landroid/widget/CheckedTextView;

    if-eqz v1, :cond_0

    move-object/from16 v1, p1

    .line 246
    check-cast v1, Landroid/widget/CheckedTextView;

    .line 247
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->toggle()V

    .line 249
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 250
    const v2, 0x7f10018d

    if-ne v1, v2, :cond_3

    .line 251
    move-object/from16 v0, p0

    iget-object v2, v0, Lebp;->aa:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lebp;->X:Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 263
    :cond_1
    :goto_1
    return-void

    .line 251
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 252
    :cond_3
    const v2, 0x7f10018f

    if-ne v1, v2, :cond_5

    .line 253
    move-object/from16 v0, p0

    iget-object v1, v0, Lebp;->R:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct/range {p0 .. p0}, Lebp;->e()V

    goto :goto_1

    :cond_4
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lebp;->a(Z)V

    goto :goto_1

    .line 254
    :cond_5
    const v2, 0x7f10018b

    if-ne v1, v2, :cond_7

    .line 255
    invoke-virtual/range {p0 .. p0}, Lebp;->n()Lz;

    move-result-object v1

    invoke-direct/range {p0 .. p0}, Lebp;->U()I

    move-result v2

    const v3, 0x7f0a0999

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lebp;->e_(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lebp;->Q:Lhgw;

    const/4 v5, 0x5

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lebp;->U:Z

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lebp;->U:Z

    if-nez v13, :cond_6

    const/4 v13, 0x1

    :goto_2
    const/4 v14, 0x0

    const/4 v15, 0x2

    invoke-static/range {v1 .. v15}, Leyq;->a(Landroid/content/Context;ILjava/lang/String;Lhgw;IZZZZZZZZZI)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lebp;->a(Landroid/content/Intent;I)V

    invoke-virtual/range {p0 .. p0}, Lebp;->n()Lz;

    move-result-object v1

    const v2, 0x7f05001e

    const v3, 0x7f05000c

    invoke-virtual {v1, v2, v3}, Lz;->overridePendingTransition(II)V

    goto :goto_1

    :cond_6
    const/4 v13, 0x0

    goto :goto_2

    .line 256
    :cond_7
    const v2, 0x7f100191

    if-ne v1, v2, :cond_9

    .line 257
    move-object/from16 v0, p0

    iget-object v1, v0, Lebp;->ad:Lhoc;

    const-string v2, "UpdateCollectionTask"

    invoke-virtual {v1, v2}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lebp;->X:Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lebp;->Y:Landroid/widget/CheckedTextView;

    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lebp;->Q:Lhgw;

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lebp;->a(Lhgw;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v6, 0x0

    :cond_8
    new-instance v1, Ldqi;

    invoke-virtual/range {p0 .. p0}, Lebp;->n()Lz;

    move-result-object v2

    invoke-virtual {v2}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lebp;->U()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lebp;->N:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lebp;->O:Ljava/lang/String;

    invoke-direct/range {v1 .. v7}, Ldqi;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;ZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lebp;->P:Lhgw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lebp;->Q:Lhgw;

    invoke-virtual {v1, v2, v3}, Ldqi;->a(Lhgw;Lhgw;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lebp;->ad:Lhoc;

    invoke-virtual {v2, v1}, Lhoc;->c(Lhny;)V

    goto/16 :goto_1

    .line 258
    :cond_9
    const v2, 0x7f100178

    if-ne v1, v2, :cond_1

    .line 259
    invoke-virtual/range {p0 .. p0}, Lebp;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->finish()V

    goto/16 :goto_1
.end method
