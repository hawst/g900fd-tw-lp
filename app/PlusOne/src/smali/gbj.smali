.class public final Lgbj;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Landroid/text/StaticLayout;

.field private c:Landroid/text/StaticLayout;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgbj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgbj;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    sget-object v0, Lgbj;->a:Llct;

    if-nez v0, :cond_0

    .line 50
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lgbj;->a:Llct;

    .line 53
    :cond_0
    return-void
.end method

.method private c()I
    .locals 3

    .prologue
    .line 124
    sget-object v0, Lgbj;->a:Llct;

    iget v0, v0, Llct;->m:I

    sget-object v1, Lgbj;->a:Llct;

    iget v1, v1, Llct;->k:I

    add-int/2addr v0, v1

    sget-object v1, Lgbj;->a:Llct;

    iget-object v1, v1, Llct;->L:Landroid/graphics/Bitmap;

    .line 125
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget-object v2, Lgbj;->a:Llct;

    iget-object v2, v2, Llct;->aw:Landroid/graphics/Bitmap;

    .line 126
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 124
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 189
    iput-object v0, p0, Lgbj;->b:Landroid/text/StaticLayout;

    .line 190
    iput-object v0, p0, Lgbj;->c:Landroid/text/StaticLayout;

    .line 191
    iput-object v0, p0, Lgbj;->d:Ljava/lang/String;

    .line 192
    iput-object v0, p0, Lgbj;->f:Ljava/lang/String;

    .line 193
    iput-object v0, p0, Lgbj;->e:Ljava/lang/String;

    .line 194
    return-void
.end method

.method public a(Lozp;Ljava/lang/String;Z)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x1

    const/4 v10, 0x0

    .line 56
    iput-object p2, p0, Lgbj;->d:Ljava/lang/String;

    .line 57
    invoke-virtual {p0}, Lgbj;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 58
    iget-object v0, p1, Lozp;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 59
    iget-object v0, p1, Lozp;->b:Ljava/lang/String;

    iput-object v0, p0, Lgbj;->e:Ljava/lang/String;

    .line 66
    :goto_0
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 69
    iget-object v0, p1, Lozp;->h:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Loyy;

    .line 71
    iget-object v0, p1, Lozp;->h:Loya;

    if-eqz v0, :cond_2

    iget-object v0, v8, Loyy;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 72
    iget-object v0, v8, Loyy;->c:Ljava/lang/String;

    invoke-static {v0}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    .line 75
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 76
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 77
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    iget-object v3, v8, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 76
    invoke-static/range {v0 .. v6}, Lidi;->a(JLjava/util/TimeZone;JLjava/util/TimeZone;Z)Z

    move-result v7

    .line 80
    invoke-virtual {p0}, Lgbj;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, v8, Loyy;->b:Ljava/lang/Long;

    .line 81
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, v8, Loyy;->c:Ljava/lang/String;

    move v5, v10

    move-object v6, v9

    .line 80
    invoke-static/range {v1 .. v7}, Lidi;->a(Landroid/content/Context;JLjava/lang/String;ZLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbj;->f:Ljava/lang/String;

    .line 83
    return-void

    .line 61
    :cond_0
    if-eqz p3, :cond_1

    const v0, 0x7f0a096e

    :goto_2
    new-array v2, v6, [Ljava/lang/Object;

    sget-object v3, Lgbj;->a:Llct;

    iget-object v3, v3, Llct;->a:Lfo;

    iget-object v4, p0, Lgbj;->d:Ljava/lang/String;

    .line 63
    invoke-virtual {v3, v4}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    .line 61
    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbj;->e:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const v0, 0x7f0a096f

    goto :goto_2

    :cond_2
    move-object v5, v9

    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 87
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 88
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lgbj;->f:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 89
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lgbj;->e:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 90
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lgbj;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 143
    invoke-virtual {p0}, Lgbj;->getLeft()I

    move-result v0

    sget-object v1, Lgbj;->a:Llct;

    iget v1, v1, Llct;->C:I

    add-int/2addr v1, v0

    .line 145
    sget-object v0, Lgbj;->a:Llct;

    iget-object v0, v0, Llct;->L:Landroid/graphics/Bitmap;

    .line 146
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget-object v2, Lgbj;->a:Llct;

    iget-object v2, v2, Llct;->aw:Landroid/graphics/Bitmap;

    .line 147
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    .line 145
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 150
    sget-object v2, Lgbj;->a:Llct;

    iget-object v2, v2, Llct;->L:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Lgbj;->a:Llct;

    iget-object v3, v3, Llct;->aw:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-le v2, v3, :cond_2

    .line 157
    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 159
    invoke-direct {p0}, Lgbj;->c()I

    move-result v2

    .line 160
    sget-object v3, Lgbj;->a:Llct;

    iget v3, v3, Llct;->m:I

    .line 164
    sget-object v4, Lgbj;->a:Llct;

    iget-object v4, v4, Llct;->L:Landroid/graphics/Bitmap;

    int-to-float v1, v1

    int-to-float v5, v3

    invoke-virtual {p1, v4, v1, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 167
    iget-object v1, p0, Lgbj;->b:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    .line 168
    int-to-float v1, v2

    int-to-float v4, v3

    invoke-virtual {p1, v1, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 169
    iget-object v1, p0, Lgbj;->b:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 170
    neg-int v1, v2

    int-to-float v1, v1

    neg-int v4, v3

    int-to-float v4, v4

    invoke-virtual {p1, v1, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 172
    :cond_0
    iget-object v1, p0, Lgbj;->b:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v1, v3

    .line 173
    sget-object v3, Lgbj;->a:Llct;

    iget v3, v3, Llct;->m:I

    add-int/2addr v1, v3

    .line 176
    sget-object v3, Lgbj;->a:Llct;

    iget-object v3, v3, Llct;->aw:Landroid/graphics/Bitmap;

    int-to-float v0, v0

    int-to-float v4, v1

    invoke-virtual {p1, v3, v0, v4, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 180
    iget-object v0, p0, Lgbj;->c:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    .line 181
    int-to-float v0, v2

    int-to-float v3, v1

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 182
    iget-object v0, p0, Lgbj;->c:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 183
    neg-int v0, v2

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 185
    :cond_1
    return-void

    :cond_2
    move v7, v1

    move v1, v0

    move v0, v7

    .line 155
    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 95
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 96
    invoke-virtual {p0}, Lgbj;->getContext()Landroid/content/Context;

    move-result-object v9

    .line 97
    sget-object v0, Lgbj;->a:Llct;

    iget v10, v0, Llct;->m:I

    .line 99
    sget-object v0, Lgbj;->a:Llct;

    iget v11, v0, Llct;->m:I

    .line 100
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lgbj;->e:Ljava/lang/String;

    const/16 v2, 0x1a

    .line 101
    invoke-static {v9, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    .line 103
    invoke-direct {p0}, Lgbj;->c()I

    move-result v3

    sub-int v3, v8, v3

    sub-int/2addr v3, v11

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lgbj;->b:Landroid/text/StaticLayout;

    .line 106
    sget-object v0, Lgbj;->a:Llct;

    iget-object v0, v0, Llct;->aw:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lgbj;->b:Landroid/text/StaticLayout;

    .line 107
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    .line 106
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v10

    .line 108
    sget-object v1, Lgbj;->a:Llct;

    iget v1, v1, Llct;->m:I

    add-int v10, v0, v1

    .line 110
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lgbj;->f:Ljava/lang/String;

    const/16 v2, 0xb

    .line 111
    invoke-static {v9, v2}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    .line 112
    invoke-direct {p0}, Lgbj;->c()I

    move-result v3

    sub-int v3, v8, v3

    sub-int/2addr v3, v11

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lgbj;->c:Landroid/text/StaticLayout;

    .line 115
    sget-object v0, Lgbj;->a:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr v0, v10

    .line 116
    invoke-virtual {p0, v8, v0}, Lgbj;->setMeasuredDimension(II)V

    .line 117
    return-void
.end method
