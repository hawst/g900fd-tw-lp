.class Lhgl;
.super Lhya;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:[I


# instance fields
.field public b:Lhgt;

.field public c:Lhfv;

.field public d:I

.field private e:[Ljava/lang/String;

.field private f:[Ljava/lang/Object;

.field private g:Landroid/view/LayoutInflater;

.field private h:Lhgm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lhgl;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
    .end array-data
.end method

.method constructor <init>(Landroid/content/Context;ILhgt;Lhgm;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1}, Lhya;-><init>(Landroid/content/Context;)V

    .line 38
    new-array v0, v3, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v0, v1

    iput-object v0, p0, Lhgl;->e:[Ljava/lang/String;

    .line 39
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lhgl;->f:[Ljava/lang/Object;

    .line 53
    iput p2, p0, Lhgl;->d:I

    .line 54
    iput-object p3, p0, Lhgl;->b:Lhgt;

    .line 55
    iput-object p4, p0, Lhgl;->h:Lhgm;

    .line 57
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lhgl;->g:Landroid/view/LayoutInflater;

    move v0, v1

    .line 59
    :goto_0
    const/4 v2, 0x5

    if-ge v0, v2, :cond_0

    .line 60
    invoke-virtual {p0, v1, v3}, Lhgl;->b(ZZ)V

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_0
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lhgl;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f04002a

    const/4 v2, 0x0

    .line 109
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/acl/AclPickerListRow;

    .line 110
    iget v1, p0, Lhgl;->d:I

    invoke-virtual {v0, v1, p0}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a(ILandroid/view/View$OnClickListener;)V

    .line 111
    return-object v0
.end method

.method protected a(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lhgl;->g:Landroid/view/LayoutInflater;

    const v1, 0x7f040020

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;)V
    .locals 5

    .prologue
    .line 101
    check-cast p1, Lcom/google/android/libraries/social/acl/AclPickerCategoryHeader;

    .line 102
    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid partition: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x7f0a0535

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/acl/AclPickerCategoryHeader;->a(I)V

    .line 103
    return-void

    .line 102
    :pswitch_1
    const v0, 0x7f0a0536

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a0537

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a0538

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0a0539

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected a(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 8

    .prologue
    .line 116
    check-cast p1, Lcom/google/android/libraries/social/acl/AclPickerListRow;

    .line 118
    iget-object v0, p0, Lhgl;->c:Lhfv;

    invoke-interface {v0, p2}, Lhfv;->a(I)I

    move-result v0

    .line 119
    iget v1, p0, Lhgl;->d:I

    mul-int v3, p4, v1

    .line 120
    iget v1, p0, Lhgl;->d:I

    add-int/2addr v1, v3

    .line 121
    if-le v1, v0, :cond_0

    :goto_0
    move v2, v3

    .line 124
    :goto_1
    if-ge v2, v0, :cond_2

    .line 125
    new-instance v4, Lhfw;

    invoke-direct {v4}, Lhfw;-><init>()V

    .line 126
    iget-object v5, p0, Lhgl;->c:Lhfv;

    invoke-interface {v5, p2, v2, v4}, Lhfv;->a(IILhfw;)V

    .line 127
    sub-int v5, v2, v3

    iget-object v6, p0, Lhgl;->c:Lhfv;

    iget-object v7, p0, Lhgl;->b:Lhgt;

    .line 128
    invoke-virtual {v7, v4}, Lhgt;->b(Lhfw;)Z

    move-result v7

    .line 127
    invoke-virtual {p1, v5, v6, v4, v7}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a(ILhfv;Lhfw;Z)V

    .line 124
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 121
    goto :goto_0

    .line 131
    :goto_2
    if-ge v0, v1, :cond_1

    .line 132
    sub-int v2, v0, v3

    const/4 v4, 0x4

    invoke-virtual {p1, v2, v4}, Lcom/google/android/libraries/social/acl/AclPickerListRow;->a(II)V

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 134
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public a(Lhfv;[I)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 65
    iput-object p1, p0, Lhgl;->c:Lhfv;

    .line 66
    if-nez p1, :cond_1

    .line 67
    invoke-virtual {p0, v0, v1}, Lhgl;->a(ILandroid/database/Cursor;)V

    .line 68
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lhgl;->a(ILandroid/database/Cursor;)V

    .line 69
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lhgl;->a(ILandroid/database/Cursor;)V

    .line 70
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Lhgl;->a(ILandroid/database/Cursor;)V

    .line 71
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lhgl;->a(ILandroid/database/Cursor;)V

    .line 79
    :cond_0
    return-void

    .line 74
    :cond_1
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    .line 75
    aget v1, p2, v0

    .line 76
    sget-object v2, Lhgl;->a:[I

    aget v2, v2, v1

    .line 77
    invoke-interface {p1, v1}, Lhfv;->a(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lhgl;->b(I)Landroid/database/Cursor;

    move-result-object v1

    .line 76
    invoke-virtual {p0, v2, v1}, Lhgl;->a(ILandroid/database/Cursor;)V

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected b(I)Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 86
    iget v0, p0, Lhgl;->d:I

    add-int/lit8 v0, v0, -0x1

    add-int/2addr v0, p1

    iget v1, p0, Lhgl;->d:I

    div-int v1, v0, v1

    .line 87
    new-instance v2, Lhym;

    iget-object v0, p0, Lhgl;->e:[Ljava/lang/String;

    invoke-direct {v2, v0, p1}, Lhym;-><init>([Ljava/lang/String;I)V

    .line 88
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 89
    iget-object v3, p0, Lhgl;->f:[Ljava/lang/Object;

    invoke-virtual {v2, v3}, Lhym;->a([Ljava/lang/Object;)V

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_0
    return-object v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lhgl;->h:Lhgm;

    if-eqz v0, :cond_0

    .line 139
    iget-object v1, p0, Lhgl;->h:Lhgm;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfw;

    invoke-interface {v1, v0, p1}, Lhgm;->a(Lhfw;Landroid/view/View;)V

    .line 141
    :cond_0
    return-void
.end method
