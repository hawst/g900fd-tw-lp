.class public abstract Lkdh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdg;


# static fields
.field private static final a:I

.field private static c:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final b:Landroid/content/Context;

.field private final d:Landroid/os/Handler;

.field private final e:Lkdk;

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lkdc;",
            "Lkda;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lkdm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const/4 v0, 0x1

    .line 33
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lkdh;->a:I

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lkdi;

    invoke-direct {v2, p0}, Lkdi;-><init>(Lkdh;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lkdh;->d:Landroid/os/Handler;

    .line 60
    new-instance v0, Lkdk;

    invoke-direct {v0, p0}, Lkdk;-><init>(Lkdh;)V

    iput-object v0, p0, Lkdh;->e:Lkdk;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lkdh;->f:Ljava/util/HashMap;

    .line 67
    iput-object p1, p0, Lkdh;->b:Landroid/content/Context;

    .line 68
    sget v0, Lkdh;->a:I

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lkdh;->c:Ljava/util/concurrent/ExecutorService;

    .line 69
    return-void
.end method

.method static synthetic a(Lkdh;Lkda;)V
    .locals 4

    .prologue
    .line 24
    invoke-virtual {p1}, Lkda;->getIdentifier()Lkdc;

    move-result-object v0

    invoke-virtual {p1}, Lkda;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Finished preloading resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lkda;->logDebug(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lkdh;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lkdh;->d:Landroid/os/Handler;

    iget-object v1, p0, Lkdh;->d:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lkdh;->b:Landroid/content/Context;

    return-object v0
.end method

.method public a(Lkda;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 156
    iget-object v0, p0, Lkdh;->d:Landroid/os/Handler;

    iget-object v1, p0, Lkdh;->d:Landroid/os/Handler;

    .line 157
    invoke-virtual {v1, v2, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 156
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 158
    return-void
.end method

.method public a(Lkda;II)V
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Lkdh;->d:Landroid/os/Handler;

    iget-object v1, p0, Lkdh;->d:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p2, p3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 167
    return-void
.end method

.method public a(Lkda;ILjava/lang/Object;)V
    .locals 5

    .prologue
    .line 174
    iget-object v0, p0, Lkdh;->d:Landroid/os/Handler;

    iget-object v1, p0, Lkdh;->d:Landroid/os/Handler;

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v4, Lkdl;

    invoke-direct {v4, p1, p3}, Lkdl;-><init>(Lkda;Ljava/lang/Object;)V

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 176
    return-void
.end method

.method public a(Lkda;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 193
    sget-object v0, Lkdh;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lkdj;

    invoke-direct {v1, p1, p2}, Lkdj;-><init>(Lkda;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 199
    return-void
.end method

.method a(Landroid/os/Message;)Z
    .locals 2

    .prologue
    .line 213
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 244
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 215
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lkda;

    .line 216
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Lkda;->mStatus:I

    .line 217
    invoke-virtual {v0}, Lkda;->notifyConsumers()V

    goto :goto_0

    .line 221
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lkda;

    .line 222
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Lkda;->mStatus:I

    .line 223
    iget v1, p1, Landroid/os/Message;->arg2:I

    iput v1, v0, Lkda;->mHttpStatusCode:I

    .line 224
    invoke-virtual {v0}, Lkda;->notifyConsumers()V

    goto :goto_0

    .line 228
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lkdl;

    .line 229
    iget-object v1, v0, Lkdl;->a:Lkda;

    .line 230
    iget-object v0, v0, Lkdl;->b:Ljava/lang/Object;

    iput-object v0, v1, Lkda;->mResource:Ljava/lang/Object;

    .line 231
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, v1, Lkda;->mStatus:I

    .line 232
    invoke-virtual {v1}, Lkda;->notifyConsumers()V

    goto :goto_0

    .line 236
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lkda;

    .line 237
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Lkda;->mResourceType:I

    .line 238
    invoke-virtual {v0}, Lkda;->notifyConsumers()V

    goto :goto_0

    .line 242
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lkda;

    .line 243
    iget-object v1, p0, Lkdh;->e:Lkdk;

    invoke-virtual {v0, v1}, Lkda;->unregister(Lkdd;)V

    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method b()Lkdm;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lkdh;->g:Lkdm;

    if-nez v0, :cond_0

    .line 203
    new-instance v0, Lkdm;

    invoke-direct {v0}, Lkdm;-><init>()V

    iput-object v0, p0, Lkdh;->g:Lkdm;

    .line 204
    iget-object v0, p0, Lkdh;->g:Lkdm;

    invoke-virtual {v0}, Lkdm;->start()V

    .line 206
    :cond_0
    iget-object v0, p0, Lkdh;->g:Lkdm;

    return-object v0
.end method

.method public b(Lkda;I)V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lkdh;->d:Landroid/os/Handler;

    iget-object v1, p0, Lkdh;->d:Landroid/os/Handler;

    const/4 v2, 0x3

    const/4 v3, 0x0

    .line 184
    invoke-virtual {v1, v2, p2, v3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 183
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 185
    return-void
.end method

.method public c(Lkda;)V
    .locals 4

    .prologue
    .line 90
    invoke-virtual {p1}, Lkda;->getIdentifier()Lkdc;

    move-result-object v0

    .line 91
    invoke-virtual {p0, v0}, Lkdh;->a(Lkdc;)Lkda;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-virtual {p1}, Lkda;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Preloading resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lkda;->logDebug(Ljava/lang/String;)V

    .line 99
    :cond_1
    iget-object v1, p0, Lkdh;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v0, p0, Lkdh;->e:Lkdk;

    invoke-virtual {p0, p1, v0}, Lkdh;->a(Lkda;Lkdd;)V

    goto :goto_0
.end method

.method public d(Lkda;)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lkdh;->e:Lkdk;

    invoke-virtual {p1, v0}, Lkda;->unregister(Lkdd;)V

    .line 106
    invoke-virtual {p1}, Lkda;->getIdentifier()Lkdc;

    move-result-object v0

    .line 107
    if-eqz v0, :cond_0

    .line 108
    iget-object v1, p0, Lkdh;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :cond_0
    return-void
.end method

.method public e(Lkda;)V
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lkdh;->b()Lkdm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkdm;->a(Lkda;)V

    .line 86
    return-void
.end method
