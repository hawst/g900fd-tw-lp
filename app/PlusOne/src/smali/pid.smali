.class public final Lpid;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lppf;

.field private c:[Lpie;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Loxq;-><init>()V

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Lpid;->b:Lppf;

    .line 239
    sget-object v0, Lpie;->a:[Lpie;

    iput-object v0, p0, Lpid;->c:[Lpie;

    .line 18
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 263
    .line 264
    iget-object v0, p0, Lpid;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 265
    const/4 v0, 0x1

    iget-object v2, p0, Lpid;->a:Ljava/lang/String;

    .line 266
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 268
    :goto_0
    iget-object v2, p0, Lpid;->b:Lppf;

    if-eqz v2, :cond_0

    .line 269
    const/4 v2, 0x2

    iget-object v3, p0, Lpid;->b:Lppf;

    .line 270
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 272
    :cond_0
    iget-object v2, p0, Lpid;->c:[Lpie;

    if-eqz v2, :cond_2

    .line 273
    iget-object v2, p0, Lpid;->c:[Lpie;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 274
    if-eqz v4, :cond_1

    .line 275
    const/4 v5, 0x3

    .line 276
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 273
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 280
    :cond_2
    iget-object v1, p0, Lpid;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    iput v0, p0, Lpid;->ai:I

    .line 282
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpid;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 290
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 291
    sparse-switch v0, :sswitch_data_0

    .line 295
    iget-object v2, p0, Lpid;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 296
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpid;->ah:Ljava/util/List;

    .line 299
    :cond_1
    iget-object v2, p0, Lpid;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    :sswitch_0
    return-object p0

    .line 306
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpid;->a:Ljava/lang/String;

    goto :goto_0

    .line 310
    :sswitch_2
    iget-object v0, p0, Lpid;->b:Lppf;

    if-nez v0, :cond_2

    .line 311
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpid;->b:Lppf;

    .line 313
    :cond_2
    iget-object v0, p0, Lpid;->b:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 317
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 318
    iget-object v0, p0, Lpid;->c:[Lpie;

    if-nez v0, :cond_4

    move v0, v1

    .line 319
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpie;

    .line 320
    iget-object v3, p0, Lpid;->c:[Lpie;

    if-eqz v3, :cond_3

    .line 321
    iget-object v3, p0, Lpid;->c:[Lpie;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 323
    :cond_3
    iput-object v2, p0, Lpid;->c:[Lpie;

    .line 324
    :goto_2
    iget-object v2, p0, Lpid;->c:[Lpie;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 325
    iget-object v2, p0, Lpid;->c:[Lpie;

    new-instance v3, Lpie;

    invoke-direct {v3}, Lpie;-><init>()V

    aput-object v3, v2, v0

    .line 326
    iget-object v2, p0, Lpid;->c:[Lpie;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 327
    invoke-virtual {p1}, Loxn;->a()I

    .line 324
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 318
    :cond_4
    iget-object v0, p0, Lpid;->c:[Lpie;

    array-length v0, v0

    goto :goto_1

    .line 330
    :cond_5
    iget-object v2, p0, Lpid;->c:[Lpie;

    new-instance v3, Lpie;

    invoke-direct {v3}, Lpie;-><init>()V

    aput-object v3, v2, v0

    .line 331
    iget-object v2, p0, Lpid;->c:[Lpie;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 291
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 244
    iget-object v0, p0, Lpid;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 245
    const/4 v0, 0x1

    iget-object v1, p0, Lpid;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 247
    :cond_0
    iget-object v0, p0, Lpid;->b:Lppf;

    if-eqz v0, :cond_1

    .line 248
    const/4 v0, 0x2

    iget-object v1, p0, Lpid;->b:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 250
    :cond_1
    iget-object v0, p0, Lpid;->c:[Lpie;

    if-eqz v0, :cond_3

    .line 251
    iget-object v1, p0, Lpid;->c:[Lpie;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 252
    if-eqz v3, :cond_2

    .line 253
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 251
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 257
    :cond_3
    iget-object v0, p0, Lpid;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 259
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1}, Lpid;->a(Loxn;)Lpid;

    move-result-object v0

    return-object v0
.end method
