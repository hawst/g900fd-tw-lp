.class public final Lfch;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhsa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/phone/AutoBackupSetupNotificationActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 35
    const-string v1, "profile_picture_springboard"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 39
    invoke-static {p1}, Lda;->a(Landroid/content/Context;)Lda;

    move-result-object v1

    .line 40
    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Leyq;->a(Landroid/content/Context;ILandroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 41
    invoke-virtual {v1, v0}, Lda;->a(Landroid/content/Intent;)Lda;

    .line 43
    invoke-static {}, Lfhu;->a()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lda;->a(II)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsLauncherActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 21
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 22
    return-object v0
.end method

.method public b(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 27
    invoke-static {p1, p2}, Leyq;->e(Landroid/content/Context;I)Leyr;

    move-result-object v0

    .line 28
    invoke-virtual {v0, p1}, Leyr;->a(Landroid/content/Context;)Leyr;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Leyr;->a()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Leyq;->a(Landroid/content/Context;ILandroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    return-object v0
.end method
