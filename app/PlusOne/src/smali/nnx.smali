.class public final Lnnx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnnx;


# instance fields
.field private b:I

.field private c:Lock;

.field private d:Lock;

.field private e:Ljava/lang/Boolean;

.field private f:Locf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    new-array v0, v0, [Lnnx;

    sput-object v0, Lnnx;->a:[Lnnx;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Loxq;-><init>()V

    .line 122
    const/high16 v0, -0x80000000

    iput v0, p0, Lnnx;->b:I

    .line 125
    iput-object v1, p0, Lnnx;->c:Lock;

    .line 128
    iput-object v1, p0, Lnnx;->d:Lock;

    .line 133
    iput-object v1, p0, Lnnx;->f:Locf;

    .line 113
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 159
    const/4 v0, 0x0

    .line 160
    iget v1, p0, Lnnx;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 161
    const/4 v0, 0x1

    iget v1, p0, Lnnx;->b:I

    .line 162
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 164
    :cond_0
    iget-object v1, p0, Lnnx;->c:Lock;

    if-eqz v1, :cond_1

    .line 165
    const/4 v1, 0x2

    iget-object v2, p0, Lnnx;->c:Lock;

    .line 166
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_1
    iget-object v1, p0, Lnnx;->d:Lock;

    if-eqz v1, :cond_2

    .line 169
    const/4 v1, 0x3

    iget-object v2, p0, Lnnx;->d:Lock;

    .line 170
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_2
    iget-object v1, p0, Lnnx;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 173
    const/4 v1, 0x4

    iget-object v2, p0, Lnnx;->e:Ljava/lang/Boolean;

    .line 174
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 176
    :cond_3
    iget-object v1, p0, Lnnx;->f:Locf;

    if-eqz v1, :cond_4

    .line 177
    const/4 v1, 0x5

    iget-object v2, p0, Lnnx;->f:Locf;

    .line 178
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_4
    iget-object v1, p0, Lnnx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    iput v0, p0, Lnnx;->ai:I

    .line 182
    return v0
.end method

.method public a(Loxn;)Lnnx;
    .locals 2

    .prologue
    .line 190
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 191
    sparse-switch v0, :sswitch_data_0

    .line 195
    iget-object v1, p0, Lnnx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 196
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnnx;->ah:Ljava/util/List;

    .line 199
    :cond_1
    iget-object v1, p0, Lnnx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    :sswitch_0
    return-object p0

    .line 206
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 207
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 210
    :cond_2
    iput v0, p0, Lnnx;->b:I

    goto :goto_0

    .line 212
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnnx;->b:I

    goto :goto_0

    .line 217
    :sswitch_2
    iget-object v0, p0, Lnnx;->c:Lock;

    if-nez v0, :cond_4

    .line 218
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Lnnx;->c:Lock;

    .line 220
    :cond_4
    iget-object v0, p0, Lnnx;->c:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 224
    :sswitch_3
    iget-object v0, p0, Lnnx;->d:Lock;

    if-nez v0, :cond_5

    .line 225
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Lnnx;->d:Lock;

    .line 227
    :cond_5
    iget-object v0, p0, Lnnx;->d:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 231
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnnx;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 235
    :sswitch_5
    iget-object v0, p0, Lnnx;->f:Locf;

    if-nez v0, :cond_6

    .line 236
    new-instance v0, Locf;

    invoke-direct {v0}, Locf;-><init>()V

    iput-object v0, p0, Lnnx;->f:Locf;

    .line 238
    :cond_6
    iget-object v0, p0, Lnnx;->f:Locf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 191
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 138
    iget v0, p0, Lnnx;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 139
    const/4 v0, 0x1

    iget v1, p0, Lnnx;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 141
    :cond_0
    iget-object v0, p0, Lnnx;->c:Lock;

    if-eqz v0, :cond_1

    .line 142
    const/4 v0, 0x2

    iget-object v1, p0, Lnnx;->c:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 144
    :cond_1
    iget-object v0, p0, Lnnx;->d:Lock;

    if-eqz v0, :cond_2

    .line 145
    const/4 v0, 0x3

    iget-object v1, p0, Lnnx;->d:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 147
    :cond_2
    iget-object v0, p0, Lnnx;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 148
    const/4 v0, 0x4

    iget-object v1, p0, Lnnx;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 150
    :cond_3
    iget-object v0, p0, Lnnx;->f:Locf;

    if-eqz v0, :cond_4

    .line 151
    const/4 v0, 0x5

    iget-object v1, p0, Lnnx;->f:Locf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 153
    :cond_4
    iget-object v0, p0, Lnnx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 155
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0, p1}, Lnnx;->a(Loxn;)Lnnx;

    move-result-object v0

    return-object v0
.end method
