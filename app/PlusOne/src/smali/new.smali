.class public final Lnew;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:Lneh;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2860
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2867
    const/4 v0, 0x0

    iput-object v0, p0, Lnew;->c:Lneh;

    .line 2860
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2887
    const/4 v0, 0x0

    .line 2888
    iget-object v1, p0, Lnew;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2889
    const/4 v0, 0x1

    iget-object v1, p0, Lnew;->b:Ljava/lang/Integer;

    .line 2890
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2892
    :cond_0
    iget-object v1, p0, Lnew;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2893
    const/4 v1, 0x2

    iget-object v2, p0, Lnew;->a:Ljava/lang/String;

    .line 2894
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2896
    :cond_1
    iget-object v1, p0, Lnew;->c:Lneh;

    if-eqz v1, :cond_2

    .line 2897
    const/4 v1, 0x3

    iget-object v2, p0, Lnew;->c:Lneh;

    .line 2898
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2900
    :cond_2
    iget-object v1, p0, Lnew;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2901
    iput v0, p0, Lnew;->ai:I

    .line 2902
    return v0
.end method

.method public a(Loxn;)Lnew;
    .locals 2

    .prologue
    .line 2910
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2911
    sparse-switch v0, :sswitch_data_0

    .line 2915
    iget-object v1, p0, Lnew;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2916
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnew;->ah:Ljava/util/List;

    .line 2919
    :cond_1
    iget-object v1, p0, Lnew;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2921
    :sswitch_0
    return-object p0

    .line 2926
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnew;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2930
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnew;->a:Ljava/lang/String;

    goto :goto_0

    .line 2934
    :sswitch_3
    iget-object v0, p0, Lnew;->c:Lneh;

    if-nez v0, :cond_2

    .line 2935
    new-instance v0, Lneh;

    invoke-direct {v0}, Lneh;-><init>()V

    iput-object v0, p0, Lnew;->c:Lneh;

    .line 2937
    :cond_2
    iget-object v0, p0, Lnew;->c:Lneh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2911
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2872
    iget-object v0, p0, Lnew;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2873
    const/4 v0, 0x1

    iget-object v1, p0, Lnew;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2875
    :cond_0
    iget-object v0, p0, Lnew;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2876
    const/4 v0, 0x2

    iget-object v1, p0, Lnew;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2878
    :cond_1
    iget-object v0, p0, Lnew;->c:Lneh;

    if-eqz v0, :cond_2

    .line 2879
    const/4 v0, 0x3

    iget-object v1, p0, Lnew;->c:Lneh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2881
    :cond_2
    iget-object v0, p0, Lnew;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2883
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2856
    invoke-virtual {p0, p1}, Lnew;->a(Loxn;)Lnew;

    move-result-object v0

    return-object v0
.end method
