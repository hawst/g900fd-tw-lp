.class public final Lnsc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lpdt;

.field public e:Ljava/lang/String;

.field public f:Lnse;

.field public g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/Boolean;

.field private m:I

.field private n:Lnse;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x80000000

    .line 96
    invoke-direct {p0}, Loxq;-><init>()V

    .line 107
    iput-object v1, p0, Lnsc;->d:Lpdt;

    .line 110
    iput v0, p0, Lnsc;->i:I

    .line 113
    iput v0, p0, Lnsc;->j:I

    .line 122
    iput v0, p0, Lnsc;->m:I

    .line 125
    iput-object v1, p0, Lnsc;->n:Lnse;

    .line 128
    iput-object v1, p0, Lnsc;->f:Lnse;

    .line 96
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 183
    const/4 v0, 0x0

    .line 184
    iget-object v1, p0, Lnsc;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 185
    const/4 v0, 0x1

    iget-object v1, p0, Lnsc;->a:Ljava/lang/String;

    .line 186
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 188
    :cond_0
    iget-object v1, p0, Lnsc;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 189
    const/4 v1, 0x2

    iget-object v2, p0, Lnsc;->b:Ljava/lang/String;

    .line 190
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    :cond_1
    iget-object v1, p0, Lnsc;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 193
    const/4 v1, 0x3

    iget-object v2, p0, Lnsc;->h:Ljava/lang/String;

    .line 194
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_2
    iget-object v1, p0, Lnsc;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 197
    const/4 v1, 0x4

    iget-object v2, p0, Lnsc;->c:Ljava/lang/String;

    .line 198
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_3
    iget-object v1, p0, Lnsc;->d:Lpdt;

    if-eqz v1, :cond_4

    .line 201
    const/4 v1, 0x5

    iget-object v2, p0, Lnsc;->d:Lpdt;

    .line 202
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_4
    iget v1, p0, Lnsc;->i:I

    if-eq v1, v3, :cond_5

    .line 205
    const/4 v1, 0x7

    iget v2, p0, Lnsc;->i:I

    .line 206
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_5
    iget v1, p0, Lnsc;->j:I

    if-eq v1, v3, :cond_6

    .line 209
    const/16 v1, 0x8

    iget v2, p0, Lnsc;->j:I

    .line 210
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_6
    iget-object v1, p0, Lnsc;->e:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 213
    const/16 v1, 0x9

    iget-object v2, p0, Lnsc;->e:Ljava/lang/String;

    .line 214
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_7
    iget-object v1, p0, Lnsc;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 217
    const/16 v1, 0xa

    iget-object v2, p0, Lnsc;->l:Ljava/lang/Boolean;

    .line 218
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 220
    :cond_8
    iget v1, p0, Lnsc;->m:I

    if-eq v1, v3, :cond_9

    .line 221
    const/16 v1, 0xb

    iget v2, p0, Lnsc;->m:I

    .line 222
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    :cond_9
    iget-object v1, p0, Lnsc;->n:Lnse;

    if-eqz v1, :cond_a

    .line 225
    const/16 v1, 0xc

    iget-object v2, p0, Lnsc;->n:Lnse;

    .line 226
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_a
    iget-object v1, p0, Lnsc;->f:Lnse;

    if-eqz v1, :cond_b

    .line 229
    const/16 v1, 0xd

    iget-object v2, p0, Lnsc;->f:Lnse;

    .line 230
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_b
    iget-object v1, p0, Lnsc;->g:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 233
    const/16 v1, 0xe

    iget-object v2, p0, Lnsc;->g:Ljava/lang/String;

    .line 234
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_c
    iget-object v1, p0, Lnsc;->k:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 237
    const/16 v1, 0xf

    iget-object v2, p0, Lnsc;->k:Ljava/lang/String;

    .line 238
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    :cond_d
    iget-object v1, p0, Lnsc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    iput v0, p0, Lnsc;->ai:I

    .line 242
    return v0
.end method

.method public a(Loxn;)Lnsc;
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 250
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 251
    sparse-switch v0, :sswitch_data_0

    .line 255
    iget-object v1, p0, Lnsc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 256
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnsc;->ah:Ljava/util/List;

    .line 259
    :cond_1
    iget-object v1, p0, Lnsc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    :sswitch_0
    return-object p0

    .line 266
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsc;->a:Ljava/lang/String;

    goto :goto_0

    .line 270
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsc;->b:Ljava/lang/String;

    goto :goto_0

    .line 274
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsc;->h:Ljava/lang/String;

    goto :goto_0

    .line 278
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsc;->c:Ljava/lang/String;

    goto :goto_0

    .line 282
    :sswitch_5
    iget-object v0, p0, Lnsc;->d:Lpdt;

    if-nez v0, :cond_2

    .line 283
    new-instance v0, Lpdt;

    invoke-direct {v0}, Lpdt;-><init>()V

    iput-object v0, p0, Lnsc;->d:Lpdt;

    .line 285
    :cond_2
    iget-object v0, p0, Lnsc;->d:Lpdt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 289
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 290
    if-eqz v0, :cond_3

    if-eq v0, v3, :cond_3

    if-ne v0, v5, :cond_4

    .line 293
    :cond_3
    iput v0, p0, Lnsc;->i:I

    goto :goto_0

    .line 295
    :cond_4
    iput v2, p0, Lnsc;->i:I

    goto :goto_0

    .line 300
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 301
    if-eqz v0, :cond_5

    if-eq v0, v3, :cond_5

    if-eq v0, v4, :cond_5

    if-eq v0, v5, :cond_5

    const/4 v1, 0x4

    if-eq v0, v1, :cond_5

    const/4 v1, 0x5

    if-eq v0, v1, :cond_5

    const/4 v1, 0x6

    if-ne v0, v1, :cond_6

    .line 308
    :cond_5
    iput v0, p0, Lnsc;->j:I

    goto :goto_0

    .line 310
    :cond_6
    iput v2, p0, Lnsc;->j:I

    goto :goto_0

    .line 315
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsc;->e:Ljava/lang/String;

    goto :goto_0

    .line 319
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnsc;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 323
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 324
    if-eqz v0, :cond_7

    if-eq v0, v3, :cond_7

    if-ne v0, v4, :cond_8

    .line 327
    :cond_7
    iput v0, p0, Lnsc;->m:I

    goto/16 :goto_0

    .line 329
    :cond_8
    iput v2, p0, Lnsc;->m:I

    goto/16 :goto_0

    .line 334
    :sswitch_b
    iget-object v0, p0, Lnsc;->n:Lnse;

    if-nez v0, :cond_9

    .line 335
    new-instance v0, Lnse;

    invoke-direct {v0}, Lnse;-><init>()V

    iput-object v0, p0, Lnsc;->n:Lnse;

    .line 337
    :cond_9
    iget-object v0, p0, Lnsc;->n:Lnse;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 341
    :sswitch_c
    iget-object v0, p0, Lnsc;->f:Lnse;

    if-nez v0, :cond_a

    .line 342
    new-instance v0, Lnse;

    invoke-direct {v0}, Lnse;-><init>()V

    iput-object v0, p0, Lnsc;->f:Lnse;

    .line 344
    :cond_a
    iget-object v0, p0, Lnsc;->f:Lnse;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 348
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsc;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 352
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsc;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 251
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 135
    iget-object v0, p0, Lnsc;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 136
    const/4 v0, 0x1

    iget-object v1, p0, Lnsc;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 138
    :cond_0
    iget-object v0, p0, Lnsc;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 139
    const/4 v0, 0x2

    iget-object v1, p0, Lnsc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 141
    :cond_1
    iget-object v0, p0, Lnsc;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 142
    const/4 v0, 0x3

    iget-object v1, p0, Lnsc;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 144
    :cond_2
    iget-object v0, p0, Lnsc;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 145
    const/4 v0, 0x4

    iget-object v1, p0, Lnsc;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 147
    :cond_3
    iget-object v0, p0, Lnsc;->d:Lpdt;

    if-eqz v0, :cond_4

    .line 148
    const/4 v0, 0x5

    iget-object v1, p0, Lnsc;->d:Lpdt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 150
    :cond_4
    iget v0, p0, Lnsc;->i:I

    if-eq v0, v2, :cond_5

    .line 151
    const/4 v0, 0x7

    iget v1, p0, Lnsc;->i:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 153
    :cond_5
    iget v0, p0, Lnsc;->j:I

    if-eq v0, v2, :cond_6

    .line 154
    const/16 v0, 0x8

    iget v1, p0, Lnsc;->j:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 156
    :cond_6
    iget-object v0, p0, Lnsc;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 157
    const/16 v0, 0x9

    iget-object v1, p0, Lnsc;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 159
    :cond_7
    iget-object v0, p0, Lnsc;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 160
    const/16 v0, 0xa

    iget-object v1, p0, Lnsc;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 162
    :cond_8
    iget v0, p0, Lnsc;->m:I

    if-eq v0, v2, :cond_9

    .line 163
    const/16 v0, 0xb

    iget v1, p0, Lnsc;->m:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 165
    :cond_9
    iget-object v0, p0, Lnsc;->n:Lnse;

    if-eqz v0, :cond_a

    .line 166
    const/16 v0, 0xc

    iget-object v1, p0, Lnsc;->n:Lnse;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 168
    :cond_a
    iget-object v0, p0, Lnsc;->f:Lnse;

    if-eqz v0, :cond_b

    .line 169
    const/16 v0, 0xd

    iget-object v1, p0, Lnsc;->f:Lnse;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 171
    :cond_b
    iget-object v0, p0, Lnsc;->g:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 172
    const/16 v0, 0xe

    iget-object v1, p0, Lnsc;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 174
    :cond_c
    iget-object v0, p0, Lnsc;->k:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 175
    const/16 v0, 0xf

    iget-object v1, p0, Lnsc;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 177
    :cond_d
    iget-object v0, p0, Lnsc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 179
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lnsc;->a(Loxn;)Lnsc;

    move-result-object v0

    return-object v0
.end method
