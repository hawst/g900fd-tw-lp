.class final Lddb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/graphics/Point;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Ldda;


# direct methods
.method constructor <init>(Ldda;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lddb;->a:Ldda;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/graphics/Point;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lddb;->a:Ldda;

    invoke-virtual {v0}, Ldda;->V()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->a()Lizu;

    move-result-object v0

    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    .line 50
    new-instance v1, Ldcv;

    iget-object v2, p0, Lddb;->a:Ldda;

    invoke-virtual {v2}, Ldda;->n()Lz;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ldcv;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    return-object v1
.end method

.method public a(Landroid/graphics/Point;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Point;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    if-nez p1, :cond_0

    .line 56
    iget-object v0, p0, Lddb;->a:Ldda;

    invoke-virtual {v0}, Ldda;->U()V

    .line 67
    :goto_0
    return-void

    .line 60
    :cond_0
    new-instance v0, Lddw;

    invoke-direct {v0}, Lddw;-><init>()V

    .line 61
    iget-object v1, p0, Lddb;->a:Ldda;

    invoke-virtual {v1}, Ldda;->V()Lddl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lddw;->a(Lddl;)Ldds;

    .line 62
    iget v1, p1, Landroid/graphics/Point;->x:I

    invoke-virtual {v0, v1}, Lddw;->a(I)Lddw;

    .line 63
    iget v1, p1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1}, Lddw;->b(I)Lddw;

    .line 64
    iget-object v1, p0, Lddb;->a:Ldda;

    invoke-virtual {v0}, Lddw;->a()Lddl;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldda;->a(Lddl;)V

    .line 66
    iget-object v0, p0, Lddb;->a:Ldda;

    iget-object v1, p0, Lddb;->a:Ldda;

    invoke-virtual {v1}, Ldda;->V()Lddl;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldda;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/graphics/Point;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 46
    check-cast p2, Landroid/graphics/Point;

    invoke-virtual {p0, p2}, Lddb;->a(Landroid/graphics/Point;)V

    return-void
.end method
