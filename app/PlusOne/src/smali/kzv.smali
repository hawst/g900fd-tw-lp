.class public final Lkzv;
.super Lllq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:J

.field private i:S

.field private j:S

.field private k:S

.field private l:S

.field private m:Ljac;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lllq;-><init>()V

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Lllq;-><init>()V

    .line 56
    iput-object p1, p0, Lkzv;->f:Ljava/lang/String;

    .line 57
    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lkzv;->h:J

    .line 58
    iput-object p3, p0, Lkzv;->g:Ljava/lang/String;

    .line 59
    iput-object p4, p0, Lkzv;->e:Ljava/lang/String;

    .line 60
    sget-object v0, Ljac;->a:Ljac;

    iput-object v0, p0, Lkzv;->m:Ljac;

    .line 61
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Loya;)V
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0}, Lllq;-><init>()V

    .line 87
    iput-object p1, p0, Lkzv;->a:Ljava/lang/String;

    .line 88
    iput-object p2, p0, Lkzv;->b:Ljava/lang/String;

    .line 89
    invoke-virtual {p0, p3}, Lkzv;->a(Ljava/lang/String;)V

    .line 90
    sget-object v0, Ljac;->a:Ljac;

    iput-object v0, p0, Lkzv;->m:Ljac;

    .line 92
    if-eqz p4, :cond_0

    .line 93
    sget-object v0, Lozv;->a:Loxr;

    invoke-virtual {p4, v0}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lozv;

    .line 94
    if-eqz v0, :cond_0

    iget-object v1, v0, Lozv;->d:Ljava/lang/Integer;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lkzv;->k:S

    iget-object v1, v0, Lozv;->e:Ljava/lang/Integer;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lkzv;->l:S

    iget-object v1, v0, Lozv;->c:Lpdi;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lozv;->c:Lpdi;

    iget-object v0, v0, Lpdi;->a:Ljava/lang/String;

    iput-object v0, p0, Lkzv;->e:Ljava/lang/String;

    .line 96
    :cond_0
    return-void
.end method

.method public constructor <init>(Loyf;)V
    .locals 4

    .prologue
    .line 82
    iget-object v0, p1, Loyf;->d:Ljava/lang/String;

    iget-object v1, p1, Loyf;->e:Ljava/lang/String;

    iget-object v2, p1, Loyf;->b:Ljava/lang/String;

    iget-object v3, p1, Loyf;->g:Loya;

    invoke-direct {p0, v0, v1, v2, v3}, Lkzv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Loya;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Lpbu;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Lllq;-><init>()V

    .line 100
    iget-object v0, p1, Lpbu;->c:Lpdi;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lpbu;->c:Lpdi;

    iget-object v0, v0, Lpdi;->a:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 101
    iget-object v0, p1, Lpbu;->c:Lpdi;

    iget-object v0, v0, Lpdi;->a:Ljava/lang/String;

    invoke-static {v0}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    :goto_0
    invoke-static {v0}, Ljbd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzv;->e:Ljava/lang/String;

    .line 107
    iput-object v1, p0, Lkzv;->f:Ljava/lang/String;

    .line 108
    iget-object v0, p1, Lpbu;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p1, Lpbu;->g:Ljava/lang/String;

    iput-object v0, p0, Lkzv;->f:Ljava/lang/String;

    .line 111
    :cond_0
    iput-object v1, p0, Lkzv;->g:Ljava/lang/String;

    .line 113
    :try_start_0
    iget-object v0, p1, Lpbu;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    iget-object v2, p1, Lpbu;->h:Ljava/lang/String;

    .line 114
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p1, Lpbu;->h:Ljava/lang/String;

    iput-object v0, p0, Lkzv;->g:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :cond_1
    :goto_1
    iget-object v0, p1, Lpbu;->i:Ljava/lang/String;

    invoke-static {v0}, Llsl;->a(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lkzv;->h:J

    .line 121
    iget-object v0, p1, Lpbu;->d:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lkzv;->i:S

    .line 122
    iget-object v0, p1, Lpbu;->e:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lkzv;->j:S

    .line 123
    iget v0, p1, Lpbu;->j:I

    packed-switch v0, :pswitch_data_0

    .line 134
    sget-object v0, Ljac;->a:Ljac;

    iput-object v0, p0, Lkzv;->m:Ljac;

    .line 137
    :goto_2
    invoke-virtual {p0}, Lkzv;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p1, Lpbu;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lkzv;->a(Ljava/lang/String;)V

    .line 140
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    .line 103
    goto :goto_0

    .line 125
    :pswitch_0
    sget-object v0, Ljac;->b:Ljac;

    iput-object v0, p0, Lkzv;->m:Ljac;

    goto :goto_2

    .line 128
    :pswitch_1
    sget-object v0, Ljac;->c:Ljac;

    iput-object v0, p0, Lkzv;->m:Ljac;

    goto :goto_2

    .line 131
    :pswitch_2
    sget-object v0, Ljac;->d:Ljac;

    iput-object v0, p0, Lkzv;->m:Ljac;

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    .line 123
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(Lpdd;)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p1, Lpdd;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lpdd;->d:Ljava/lang/String;

    :goto_0
    const/4 v1, 0x0

    iget-object v2, p1, Lpdd;->b:Ljava/lang/String;

    iget-object v3, p1, Lpdd;->f:Loya;

    invoke-direct {p0, v0, v1, v2, v3}, Lkzv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Loya;)V

    .line 73
    return-void

    .line 71
    :cond_0
    iget-object v0, p1, Lpdd;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lpdg;)V
    .locals 4

    .prologue
    .line 64
    iget-object v0, p1, Lpdg;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lpdg;->d:Ljava/lang/String;

    :goto_0
    const/4 v1, 0x0

    iget-object v2, p1, Lpdg;->b:Ljava/lang/String;

    iget-object v3, p1, Lpdg;->g:Loya;

    invoke-direct {p0, v0, v1, v2, v3}, Lkzv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Loya;)V

    .line 66
    return-void

    .line 64
    :cond_0
    iget-object v0, p1, Lpdg;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public constructor <init>(Lpdl;)V
    .locals 3

    .prologue
    const/16 v2, 0x1e0

    .line 142
    invoke-direct {p0}, Lllq;-><init>()V

    .line 143
    iget-object v0, p1, Lpdl;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lkzv;->a(Ljava/lang/String;)V

    .line 144
    iget-object v0, p1, Lpdl;->e:Lpdi;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p1, Lpdl;->e:Lpdi;

    iget-object v0, v0, Lpdi;->a:Ljava/lang/String;

    iput-object v0, p0, Lkzv;->e:Ljava/lang/String;

    .line 147
    :cond_0
    iget-object v0, p1, Lpdl;->f:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    .line 148
    iget-object v1, p1, Lpdl;->g:Ljava/lang/Integer;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v1

    .line 149
    if-lez v0, :cond_1

    if-lez v1, :cond_1

    .line 150
    iput-short v2, p0, Lkzv;->i:S

    .line 151
    iget-short v2, p0, Lkzv;->i:S

    mul-int/2addr v1, v2

    div-int v0, v1, v0

    int-to-short v0, v0

    iput-short v0, p0, Lkzv;->j:S

    .line 158
    :goto_0
    iget-object v0, p1, Lpdl;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 159
    iget-object v0, p1, Lpdl;->c:Ljava/lang/String;

    iput-object v0, p0, Lkzv;->a:Ljava/lang/String;

    .line 160
    iget-object v0, p1, Lpdl;->d:Ljava/lang/String;

    iput-object v0, p0, Lkzv;->b:Ljava/lang/String;

    .line 164
    :goto_1
    sget-object v0, Ljac;->b:Ljac;

    iput-object v0, p0, Lkzv;->m:Ljac;

    .line 165
    return-void

    .line 154
    :cond_1
    iput-short v2, p0, Lkzv;->i:S

    .line 155
    const/16 v0, 0x168

    iput-short v0, p0, Lkzv;->j:S

    goto :goto_0

    .line 162
    :cond_2
    iget-object v0, p1, Lpdl;->d:Ljava/lang/String;

    iput-object v0, p0, Lkzv;->a:Ljava/lang/String;

    goto :goto_1
.end method

.method public constructor <init>(Lpdp;)V
    .locals 4

    .prologue
    .line 76
    iget-object v0, p1, Lpdp;->d:Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p1, Lpdp;->b:Ljava/lang/String;

    iget-object v3, p1, Lpdp;->f:Loya;

    invoke-direct {p0, v0, v1, v2, v3}, Lkzv;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Loya;)V

    .line 78
    return-void
.end method

.method public static a([B)Lkzv;
    .locals 2

    .prologue
    .line 195
    if-nez p0, :cond_0

    .line 196
    const/4 v0, 0x0

    .line 203
    :goto_0
    return-object v0

    .line 199
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 200
    new-instance v0, Lkzv;

    invoke-direct {v0}, Lkzv;-><init>()V

    .line 202
    invoke-virtual {v0, v1}, Lkzv;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lkzv;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/io/DataOutputStream;)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lkzv;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzv;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lkzv;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzv;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lkzv;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzv;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lkzv;->d:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzv;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lkzv;->e:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzv;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lkzv;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzv;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lkzv;->g:Ljava/lang/String;

    invoke-static {p1, v0}, Lkzv;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 186
    iget-wide v0, p0, Lkzv;->h:J

    invoke-virtual {p1, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 187
    iget-short v0, p0, Lkzv;->i:S

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 188
    iget-short v0, p0, Lkzv;->j:S

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 189
    iget-object v0, p0, Lkzv;->m:Ljac;

    iget v0, v0, Ljac;->e:I

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 190
    iget-short v0, p0, Lkzv;->k:S

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 191
    iget-short v0, p0, Lkzv;->l:S

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 192
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 231
    iput-object p1, p0, Lkzv;->c:Ljava/lang/String;

    .line 232
    iget-object v0, p0, Lkzv;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "www."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lkzv;->d:Ljava/lang/String;

    .line 233
    return-void

    .line 232
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 207
    invoke-static {p1}, Lkzv;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzv;->a:Ljava/lang/String;

    .line 208
    invoke-static {p1}, Lkzv;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzv;->b:Ljava/lang/String;

    .line 209
    invoke-static {p1}, Lkzv;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzv;->c:Ljava/lang/String;

    .line 210
    invoke-static {p1}, Lkzv;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzv;->d:Ljava/lang/String;

    .line 211
    invoke-static {p1}, Lkzv;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzv;->e:Ljava/lang/String;

    .line 212
    invoke-static {p1}, Lkzv;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzv;->f:Ljava/lang/String;

    .line 213
    invoke-static {p1}, Lkzv;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkzv;->g:Ljava/lang/String;

    .line 214
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v0

    iput-wide v0, p0, Lkzv;->h:J

    .line 215
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    iput-short v0, p0, Lkzv;->i:S

    .line 216
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    iput-short v0, p0, Lkzv;->j:S

    .line 217
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    invoke-static {v0}, Ljac;->a(I)Ljac;

    move-result-object v0

    iput-object v0, p0, Lkzv;->m:Ljac;

    .line 218
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    iput-short v0, p0, Lkzv;->k:S

    .line 219
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    iput-short v0, p0, Lkzv;->l:S

    .line 220
    return-void
.end method

.method public a(S)V
    .locals 0

    .prologue
    .line 240
    iput-short p1, p0, Lkzv;->k:S

    .line 241
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lkzv;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lkzv;->e:Ljava/lang/String;

    .line 237
    return-void
.end method

.method public b(S)V
    .locals 0

    .prologue
    .line 244
    iput-short p1, p0, Lkzv;->l:S

    .line 245
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lkzv;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lkzv;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lkzv;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lkzv;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lkzv;->g:Ljava/lang/String;

    return-object v0
.end method

.method public h()J
    .locals 2

    .prologue
    .line 268
    iget-wide v0, p0, Lkzv;->h:J

    return-wide v0
.end method

.method public i()S
    .locals 1

    .prologue
    .line 272
    iget-short v0, p0, Lkzv;->i:S

    return v0
.end method

.method public j()S
    .locals 1

    .prologue
    .line 276
    iget-short v0, p0, Lkzv;->j:S

    return v0
.end method

.method public k()S
    .locals 1

    .prologue
    .line 280
    iget-short v0, p0, Lkzv;->k:S

    return v0
.end method

.method public l()S
    .locals 1

    .prologue
    .line 284
    iget-short v0, p0, Lkzv;->l:S

    return v0
.end method

.method public m()Ljac;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lkzv;->m:Ljac;

    return-object v0
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lkzv;->m:Ljac;

    sget-object v1, Ljac;->b:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    invoke-virtual {p0}, Lkzv;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkzv;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
