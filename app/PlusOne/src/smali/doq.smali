.class public final Ldoq;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 30
    const-string v0, "DismissAllNotificationsTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 32
    iput p2, p0, Ldoq;->a:I

    .line 33
    return-void
.end method


# virtual methods
.method public a()Lhoz;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 37
    invoke-virtual {p0}, Ldoq;->f()Landroid/content/Context;

    move-result-object v1

    .line 39
    const-class v0, Lieh;

    invoke-static {v1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 40
    sget-object v2, Ldxd;->g:Lief;

    iget v3, p0, Ldoq;->a:I

    invoke-interface {v0, v2, v3}, Lieh;->b(Lief;I)Z

    move-result v0

    .line 41
    if-nez v0, :cond_3

    .line 44
    iget v0, p0, Ldoq;->a:I

    .line 45
    invoke-static {v1, v0}, Ldsf;->i(Landroid/content/Context;I)Ljava/util/ArrayList;

    move-result-object v5

    .line 47
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    :cond_0
    new-instance v0, Lhoz;

    invoke-direct {v0, v10}, Lhoz;-><init>(Z)V

    .line 81
    :goto_0
    new-instance v2, Ldjp;

    iget v3, p0, Ldoq;->a:I

    invoke-direct {v2, v1, v3, v11}, Ldjp;-><init>(Landroid/content/Context;ILkfp;)V

    .line 83
    invoke-virtual {v2}, Ldjp;->l()V

    .line 85
    return-object v0

    .line 50
    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 51
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 52
    new-array v4, v6, [J

    .line 53
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_2

    .line 54
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 55
    iget-object v7, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    aput-wide v8, v4, v2

    .line 53
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 59
    :cond_2
    iget v0, p0, Ldoq;->a:I

    invoke-static {v1, v0, v3, v10, v10}, Ldsf;->a(Landroid/content/Context;ILjava/util/List;ZZ)V

    .line 61
    iget v0, p0, Ldoq;->a:I

    invoke-static {v1, v0, v10}, Ldsf;->a(Landroid/content/Context;II)V

    .line 64
    new-instance v0, Ldnf;

    iget v2, p0, Ldoq;->a:I

    const/4 v5, 0x2

    const-string v6, "GPLUS_APP_V3"

    invoke-direct/range {v0 .. v6}, Ldnf;-><init>(Landroid/content/Context;ILjava/util/List;[JILjava/lang/String;)V

    .line 66
    invoke-virtual {v0}, Lkff;->l()V

    .line 67
    new-instance v2, Lhoz;

    iget v3, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v2, v3, v0, v11}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v2

    .line 69
    goto :goto_0

    .line 72
    :cond_3
    iget v0, p0, Ldoq;->a:I

    invoke-static {v1, v0}, Ldsf;->a(Landroid/content/Context;I)V

    .line 74
    iget v0, p0, Ldoq;->a:I

    .line 75
    invoke-static {v1, v0}, Ldsf;->h(Landroid/content/Context;I)J

    move-result-wide v2

    .line 76
    new-instance v4, Ldle;

    iget v0, p0, Ldoq;->a:I

    invoke-direct {v4, v1, v0, v2, v3}, Ldle;-><init>(Landroid/content/Context;IJ)V

    .line 77
    invoke-virtual {v4}, Lkff;->l()V

    .line 78
    new-instance v0, Lhoz;

    iget v2, v4, Lkff;->i:I

    iget-object v3, v4, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v0, v2, v3, v11}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method
