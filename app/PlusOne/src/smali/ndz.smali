.class public final Lndz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnwy;

.field public b:I

.field public c:Lnei;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 471
    invoke-direct {p0}, Loxq;-><init>()V

    .line 474
    iput-object v1, p0, Lndz;->a:Lnwy;

    .line 477
    const/high16 v0, -0x80000000

    iput v0, p0, Lndz;->b:I

    .line 480
    iput-object v1, p0, Lndz;->c:Lnei;

    .line 471
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 510
    const/4 v0, 0x0

    .line 511
    iget-object v1, p0, Lndz;->a:Lnwy;

    if-eqz v1, :cond_0

    .line 512
    const/4 v0, 0x1

    iget-object v1, p0, Lndz;->a:Lnwy;

    .line 513
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 515
    :cond_0
    iget v1, p0, Lndz;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 516
    const/4 v1, 0x2

    iget v2, p0, Lndz;->b:I

    .line 517
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 519
    :cond_1
    iget-object v1, p0, Lndz;->c:Lnei;

    if-eqz v1, :cond_2

    .line 520
    const/4 v1, 0x3

    iget-object v2, p0, Lndz;->c:Lnei;

    .line 521
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 523
    :cond_2
    iget-object v1, p0, Lndz;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 524
    const/4 v1, 0x4

    iget-object v2, p0, Lndz;->d:Ljava/lang/Boolean;

    .line 525
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 527
    :cond_3
    iget-object v1, p0, Lndz;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 528
    const/4 v1, 0x5

    iget-object v2, p0, Lndz;->e:Ljava/lang/Boolean;

    .line 529
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 531
    :cond_4
    iget-object v1, p0, Lndz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 532
    iput v0, p0, Lndz;->ai:I

    .line 533
    return v0
.end method

.method public a(Loxn;)Lndz;
    .locals 2

    .prologue
    .line 541
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 542
    sparse-switch v0, :sswitch_data_0

    .line 546
    iget-object v1, p0, Lndz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 547
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lndz;->ah:Ljava/util/List;

    .line 550
    :cond_1
    iget-object v1, p0, Lndz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    :sswitch_0
    return-object p0

    .line 557
    :sswitch_1
    iget-object v0, p0, Lndz;->a:Lnwy;

    if-nez v0, :cond_2

    .line 558
    new-instance v0, Lnwy;

    invoke-direct {v0}, Lnwy;-><init>()V

    iput-object v0, p0, Lndz;->a:Lnwy;

    .line 560
    :cond_2
    iget-object v0, p0, Lndz;->a:Lnwy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 564
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 565
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 569
    :cond_3
    iput v0, p0, Lndz;->b:I

    goto :goto_0

    .line 571
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lndz;->b:I

    goto :goto_0

    .line 576
    :sswitch_3
    iget-object v0, p0, Lndz;->c:Lnei;

    if-nez v0, :cond_5

    .line 577
    new-instance v0, Lnei;

    invoke-direct {v0}, Lnei;-><init>()V

    iput-object v0, p0, Lndz;->c:Lnei;

    .line 579
    :cond_5
    iget-object v0, p0, Lndz;->c:Lnei;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 583
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lndz;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 587
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lndz;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 542
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 489
    iget-object v0, p0, Lndz;->a:Lnwy;

    if-eqz v0, :cond_0

    .line 490
    const/4 v0, 0x1

    iget-object v1, p0, Lndz;->a:Lnwy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 492
    :cond_0
    iget v0, p0, Lndz;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 493
    const/4 v0, 0x2

    iget v1, p0, Lndz;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 495
    :cond_1
    iget-object v0, p0, Lndz;->c:Lnei;

    if-eqz v0, :cond_2

    .line 496
    const/4 v0, 0x3

    iget-object v1, p0, Lndz;->c:Lnei;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 498
    :cond_2
    iget-object v0, p0, Lndz;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 499
    const/4 v0, 0x4

    iget-object v1, p0, Lndz;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 501
    :cond_3
    iget-object v0, p0, Lndz;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 502
    const/4 v0, 0x5

    iget-object v1, p0, Lndz;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 504
    :cond_4
    iget-object v0, p0, Lndz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 506
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 467
    invoke-virtual {p0, p1}, Lndz;->a(Loxn;)Lndz;

    move-result-object v0

    return-object v0
.end method
