.class public final Lobk;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lodo;

.field private b:Lobl;

.field private c:[Lodo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 720
    invoke-direct {p0}, Loxq;-><init>()V

    .line 723
    iput-object v0, p0, Lobk;->a:Lodo;

    .line 726
    iput-object v0, p0, Lobk;->b:Lobl;

    .line 729
    sget-object v0, Lodo;->a:[Lodo;

    iput-object v0, p0, Lobk;->c:[Lodo;

    .line 720
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 753
    .line 754
    iget-object v0, p0, Lobk;->a:Lodo;

    if-eqz v0, :cond_3

    .line 755
    const/4 v0, 0x1

    iget-object v2, p0, Lobk;->a:Lodo;

    .line 756
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 758
    :goto_0
    iget-object v2, p0, Lobk;->b:Lobl;

    if-eqz v2, :cond_0

    .line 759
    const/4 v2, 0x2

    iget-object v3, p0, Lobk;->b:Lobl;

    .line 760
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 762
    :cond_0
    iget-object v2, p0, Lobk;->c:[Lodo;

    if-eqz v2, :cond_2

    .line 763
    iget-object v2, p0, Lobk;->c:[Lodo;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 764
    if-eqz v4, :cond_1

    .line 765
    const/4 v5, 0x3

    .line 766
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 763
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 770
    :cond_2
    iget-object v1, p0, Lobk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 771
    iput v0, p0, Lobk;->ai:I

    .line 772
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lobk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 780
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 781
    sparse-switch v0, :sswitch_data_0

    .line 785
    iget-object v2, p0, Lobk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 786
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lobk;->ah:Ljava/util/List;

    .line 789
    :cond_1
    iget-object v2, p0, Lobk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 791
    :sswitch_0
    return-object p0

    .line 796
    :sswitch_1
    iget-object v0, p0, Lobk;->a:Lodo;

    if-nez v0, :cond_2

    .line 797
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lobk;->a:Lodo;

    .line 799
    :cond_2
    iget-object v0, p0, Lobk;->a:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 803
    :sswitch_2
    iget-object v0, p0, Lobk;->b:Lobl;

    if-nez v0, :cond_3

    .line 804
    new-instance v0, Lobl;

    invoke-direct {v0}, Lobl;-><init>()V

    iput-object v0, p0, Lobk;->b:Lobl;

    .line 806
    :cond_3
    iget-object v0, p0, Lobk;->b:Lobl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 810
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 811
    iget-object v0, p0, Lobk;->c:[Lodo;

    if-nez v0, :cond_5

    move v0, v1

    .line 812
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lodo;

    .line 813
    iget-object v3, p0, Lobk;->c:[Lodo;

    if-eqz v3, :cond_4

    .line 814
    iget-object v3, p0, Lobk;->c:[Lodo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 816
    :cond_4
    iput-object v2, p0, Lobk;->c:[Lodo;

    .line 817
    :goto_2
    iget-object v2, p0, Lobk;->c:[Lodo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 818
    iget-object v2, p0, Lobk;->c:[Lodo;

    new-instance v3, Lodo;

    invoke-direct {v3}, Lodo;-><init>()V

    aput-object v3, v2, v0

    .line 819
    iget-object v2, p0, Lobk;->c:[Lodo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 820
    invoke-virtual {p1}, Loxn;->a()I

    .line 817
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 811
    :cond_5
    iget-object v0, p0, Lobk;->c:[Lodo;

    array-length v0, v0

    goto :goto_1

    .line 823
    :cond_6
    iget-object v2, p0, Lobk;->c:[Lodo;

    new-instance v3, Lodo;

    invoke-direct {v3}, Lodo;-><init>()V

    aput-object v3, v2, v0

    .line 824
    iget-object v2, p0, Lobk;->c:[Lodo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 781
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 734
    iget-object v0, p0, Lobk;->a:Lodo;

    if-eqz v0, :cond_0

    .line 735
    const/4 v0, 0x1

    iget-object v1, p0, Lobk;->a:Lodo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 737
    :cond_0
    iget-object v0, p0, Lobk;->b:Lobl;

    if-eqz v0, :cond_1

    .line 738
    const/4 v0, 0x2

    iget-object v1, p0, Lobk;->b:Lobl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 740
    :cond_1
    iget-object v0, p0, Lobk;->c:[Lodo;

    if-eqz v0, :cond_3

    .line 741
    iget-object v1, p0, Lobk;->c:[Lodo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 742
    if-eqz v3, :cond_2

    .line 743
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 741
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 747
    :cond_3
    iget-object v0, p0, Lobk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 749
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 716
    invoke-virtual {p0, p1}, Lobk;->a(Loxn;)Lobk;

    move-result-object v0

    return-object v0
.end method
