.class public final Llib;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Landroid/database/ContentObserver;

.field private static final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/text/TextPaint;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private static c:F

.field private static d:Llie;

.field private static e:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/text/SpannableStringBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Llib;->b:Ljava/util/ArrayList;

    .line 49
    const/4 v0, 0x0

    sput v0, Llib;->c:F

    .line 72
    new-instance v0, Llie;

    invoke-direct {v0}, Llie;-><init>()V

    sput-object v0, Llib;->d:Llie;

    .line 74
    new-instance v0, Llic;

    invoke-direct {v0}, Llic;-><init>()V

    sput-object v0, Llib;->e:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 1

    .prologue
    .line 536
    invoke-static {p0, p1}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {v0}, Llib;->a(Landroid/text/TextPaint;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/text/TextPaint;)I
    .locals 2

    .prologue
    .line 527
    invoke-virtual {p0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    invoke-virtual {p0}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public static a(Landroid/text/TextPaint;Ljava/lang/CharSequence;)I
    .locals 1

    .prologue
    .line 511
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/text/TextPaint;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 515
    if-nez p1, :cond_0

    .line 516
    const/4 v0, 0x0

    .line 519
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    sget v1, Llib;->c:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;
    .locals 14

    .prologue
    .line 319
    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    move v0, p0

    move v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    invoke-static/range {v0 .. v13}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;ZIZLljv;)Landroid/text/StaticLayout;

    move-result-object v0

    .line 334
    return-object v0
.end method

.method private static a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;ZIZLljv;)Landroid/text/StaticLayout;
    .locals 14

    .prologue
    .line 441
    move-object/from16 v0, p8

    invoke-virtual {v0, p0, p1}, Landroid/graphics/Point;->set(II)V

    .line 443
    if-eqz p4, :cond_0

    .line 445
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int v2, v2, p6

    .line 446
    sub-int p2, p2, v2

    .line 448
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/2addr v3, p0

    .line 449
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v4, p1

    .line 448
    move-object/from16 v0, p5

    invoke-virtual {v0, p0, p1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 450
    move-object/from16 v0, p8

    iget v3, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v2, v3

    move-object/from16 v0, p8

    iget v3, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p8

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 453
    :cond_0
    const/4 v2, 0x0

    move/from16 v0, p2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 455
    if-gtz v4, :cond_6

    .line 456
    const-string v3, ""

    .line 459
    :goto_0
    if-eqz p12, :cond_3

    .line 460
    if-eqz p10, :cond_2

    .line 461
    const/high16 v6, 0x3f800000    # 1.0f

    move-object/from16 v2, p9

    move/from16 v5, p11

    move-object/from16 v7, p13

    invoke-static/range {v2 .. v7}, Llir;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIFLljv;)Llir;

    move-result-object v5

    .line 496
    :goto_1
    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    move/from16 v0, p3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-eqz p4, :cond_5

    .line 497
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 496
    :goto_2
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 499
    if-eqz p4, :cond_1

    .line 500
    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    .line 501
    const/4 v4, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v4, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 504
    :cond_1
    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 505
    move-object/from16 v0, p8

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p8

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v2, v4

    move-object/from16 v0, p8

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Point;->set(II)V

    .line 507
    return-object v5

    .line 469
    :cond_2
    new-instance v5, Llir;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v6, v3

    move-object/from16 v7, p9

    move v8, v4

    move-object/from16 v13, p13

    invoke-direct/range {v5 .. v13}, Llir;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLljv;)V

    goto :goto_1

    .line 480
    :cond_3
    if-eqz p10, :cond_4

    .line 481
    move-object/from16 v0, p9

    move/from16 v1, p11

    invoke-static {v0, v3, v4, v1}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v5

    goto :goto_1

    .line 486
    :cond_4
    new-instance v5, Landroid/text/StaticLayout;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v6, v3

    move-object/from16 v7, p9

    move v8, v4

    invoke-direct/range {v5 .. v12}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    goto :goto_1

    .line 497
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    :cond_6
    move-object/from16 v3, p7

    goto/16 :goto_0
.end method

.method public static a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {p0, p1, p2, p3, v0}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 154
    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p2, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-nez p3, :cond_2

    const-string v1, ""

    :cond_0
    :goto_0
    new-instance v0, Landroid/text/StaticLayout;

    move-object v2, p0

    move-object v4, p4

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    :cond_1
    return-object v0

    :cond_2
    const/4 v0, 0x1

    if-ne p3, v0, :cond_3

    invoke-static {p1, p0, v3, v9, v8}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/text/StaticLayout;

    move-object v1, p1

    move-object v2, p0

    move-object v4, p4

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    if-le v1, p3, :cond_1

    add-int/lit8 v1, p3, -0x2

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v2

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-interface {p1, v7, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    instance-of v4, p1, Landroid/text/Spanned;

    if-eqz v4, :cond_4

    invoke-static {}, Llsx;->b()V

    sget-object v0, Llib;->d:Llie;

    :goto_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    invoke-interface {p1, v2, v8}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-static {v8, p0, v3, v9, v0}, Llib;->a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    if-eqz v4, :cond_0

    check-cast p1, Landroid/text/Spanned;

    invoke-static {p1, v2, v1, v0}, Llib;->a(Landroid/text/Spanned;ILandroid/text/SpannableStringBuilder;Llie;)V

    goto :goto_0

    :cond_4
    move-object v0, v8

    goto :goto_1
.end method

.method public static a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Llib;->b(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 240
    invoke-static {p0, p1, p2, p3, p4}, Llib;->b(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 241
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 242
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 243
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 244
    if-ne v1, v4, :cond_0

    if-ne v2, v4, :cond_0

    move-object v0, v3

    .line 262
    :goto_0
    return-object v0

    .line 247
    :cond_0
    sget-object v0, Llib;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpannableStringBuilder;

    .line 248
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 250
    if-eq v1, v4, :cond_3

    if-eq v2, v4, :cond_3

    .line 251
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 257
    :cond_1
    :goto_1
    const/4 v2, 0x0

    invoke-interface {v3, v2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 258
    const/16 v2, 0x2026

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 259
    if-eqz p4, :cond_2

    .line 260
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {p4, v1, v2}, Landroid/text/TextUtils$EllipsizeCallback;->ellipsized(II)V

    .line 262
    :cond_2
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 252
    :cond_3
    if-ne v1, v4, :cond_1

    move v1, v2

    .line 255
    goto :goto_1
.end method

.method public static a()Llie;
    .locals 1

    .prologue
    .line 83
    invoke-static {}, Llsx;->b()V

    .line 84
    sget-object v0, Llib;->d:Llie;

    return-object v0
.end method

.method public static a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/text/TextPaint;ZI)Lljg;
    .locals 14

    .prologue
    .line 399
    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8}, Landroid/graphics/Point;-><init>()V

    .line 400
    const/4 v12, 0x1

    const/4 v13, 0x0

    move v0, p0

    move v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-static/range {v0 .. v13}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;ZIZLljv;)Landroid/text/StaticLayout;

    move-result-object v0

    check-cast v0, Lljg;

    .line 414
    iget v1, v8, Landroid/graphics/Point;->x:I

    iget v2, v8, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Lljg;->a(II)V

    .line 416
    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 98
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 99
    sget-object v1, Llib;->a:Landroid/database/ContentObserver;

    if-nez v1, :cond_0

    .line 100
    new-instance v1, Llid;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Llid;-><init>(Landroid/os/Handler;Landroid/content/res/Resources;)V

    sput-object v1, Llib;->a:Landroid/database/ContentObserver;

    .line 109
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 110
    const-string v2, "font_scale"

    .line 111
    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Llib;->a:Landroid/database/ContentObserver;

    .line 110
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 114
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_1

    .line 115
    const v1, 0x7f0d0250

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Llib;->c:F

    .line 117
    :cond_1
    return-void
.end method

.method public static a(Landroid/text/Spanned;ILandroid/text/SpannableStringBuilder;Llie;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 166
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->clearSpans()V

    .line 167
    iget v0, p3, Llie;->a:I

    if-lez v0, :cond_0

    iget v0, p3, Llie;->a:I

    add-int v2, p1, v0

    .line 168
    :goto_0
    const-class v3, Ljava/lang/Object;

    move-object v0, p0

    move-object v4, p2

    move v5, v1

    .line 167
    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    .line 170
    invoke-virtual {p3}, Llie;->a()V

    .line 171
    return-void

    .line 168
    :cond_0
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    goto :goto_0
.end method

.method public static a(Landroid/text/TextPaint;I)V
    .locals 3

    .prologue
    .line 126
    sget-object v0, Llib;->b:Ljava/util/ArrayList;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    return-void
.end method

.method public static b(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;Landroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 290
    const/4 v0, 0x0

    int-to-float v1, p2

    sget v2, Llib;->c:F

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;ZLandroid/text/TextUtils$EllipsizeCallback;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Llib;->b:Ljava/util/ArrayList;

    return-object v0
.end method
