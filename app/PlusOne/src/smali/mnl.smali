.class public final Lmnl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmnm;

.field public b:Lmnn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5771
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5948
    iput-object v0, p0, Lmnl;->a:Lmnm;

    .line 5951
    iput-object v0, p0, Lmnl;->b:Lmnn;

    .line 5771
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5968
    const/4 v0, 0x0

    .line 5969
    iget-object v1, p0, Lmnl;->a:Lmnm;

    if-eqz v1, :cond_0

    .line 5970
    const/4 v0, 0x1

    iget-object v1, p0, Lmnl;->a:Lmnm;

    .line 5971
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5973
    :cond_0
    iget-object v1, p0, Lmnl;->b:Lmnn;

    if-eqz v1, :cond_1

    .line 5974
    const/4 v1, 0x2

    iget-object v2, p0, Lmnl;->b:Lmnn;

    .line 5975
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5977
    :cond_1
    iget-object v1, p0, Lmnl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5978
    iput v0, p0, Lmnl;->ai:I

    .line 5979
    return v0
.end method

.method public a(Loxn;)Lmnl;
    .locals 2

    .prologue
    .line 5987
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5988
    sparse-switch v0, :sswitch_data_0

    .line 5992
    iget-object v1, p0, Lmnl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5993
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnl;->ah:Ljava/util/List;

    .line 5996
    :cond_1
    iget-object v1, p0, Lmnl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5998
    :sswitch_0
    return-object p0

    .line 6003
    :sswitch_1
    iget-object v0, p0, Lmnl;->a:Lmnm;

    if-nez v0, :cond_2

    .line 6004
    new-instance v0, Lmnm;

    invoke-direct {v0}, Lmnm;-><init>()V

    iput-object v0, p0, Lmnl;->a:Lmnm;

    .line 6006
    :cond_2
    iget-object v0, p0, Lmnl;->a:Lmnm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6010
    :sswitch_2
    iget-object v0, p0, Lmnl;->b:Lmnn;

    if-nez v0, :cond_3

    .line 6011
    new-instance v0, Lmnn;

    invoke-direct {v0}, Lmnn;-><init>()V

    iput-object v0, p0, Lmnl;->b:Lmnn;

    .line 6013
    :cond_3
    iget-object v0, p0, Lmnl;->b:Lmnn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5988
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5956
    iget-object v0, p0, Lmnl;->a:Lmnm;

    if-eqz v0, :cond_0

    .line 5957
    const/4 v0, 0x1

    iget-object v1, p0, Lmnl;->a:Lmnm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5959
    :cond_0
    iget-object v0, p0, Lmnl;->b:Lmnn;

    if-eqz v0, :cond_1

    .line 5960
    const/4 v0, 0x2

    iget-object v1, p0, Lmnl;->b:Lmnn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 5962
    :cond_1
    iget-object v0, p0, Lmnl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5964
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5767
    invoke-virtual {p0, p1}, Lmnl;->a(Loxn;)Lmnl;

    move-result-object v0

    return-object v0
.end method
