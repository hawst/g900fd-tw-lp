.class public abstract enum Llso;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Llso;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Llso;

.field public static final enum b:Llso;

.field public static final enum c:Llso;

.field private static enum d:Llso;

.field private static enum e:Llso;

.field private static final synthetic g:[Llso;


# instance fields
.field private f:J


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    new-instance v0, Llsp;

    const-string v1, "TERABYTES"

    const-wide v2, 0x10000000000L

    invoke-direct {v0, v1, v4, v2, v3}, Llsp;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Llso;->d:Llso;

    .line 16
    new-instance v0, Llsq;

    const-string v1, "GIGABYTES"

    const-wide/32 v2, 0x40000000

    invoke-direct {v0, v1, v5, v2, v3}, Llsq;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Llso;->a:Llso;

    .line 22
    new-instance v0, Llsr;

    const-string v1, "MEGABYTES"

    const-wide/32 v2, 0x100000

    invoke-direct {v0, v1, v6, v2, v3}, Llsr;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Llso;->b:Llso;

    .line 28
    new-instance v0, Llss;

    const-string v1, "KILOBYTES"

    const-wide/16 v2, 0x400

    invoke-direct {v0, v1, v7, v2, v3}, Llss;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Llso;->c:Llso;

    .line 34
    new-instance v0, Llst;

    const-string v1, "BYTES"

    const-wide/16 v2, 0x1

    invoke-direct {v0, v1, v8, v2, v3}, Llst;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, Llso;->e:Llso;

    .line 9
    const/4 v0, 0x5

    new-array v0, v0, [Llso;

    sget-object v1, Llso;->d:Llso;

    aput-object v1, v0, v4

    sget-object v1, Llso;->a:Llso;

    aput-object v1, v0, v5

    sget-object v1, Llso;->b:Llso;

    aput-object v1, v0, v6

    sget-object v1, Llso;->c:Llso;

    aput-object v1, v0, v7

    sget-object v1, Llso;->e:Llso;

    aput-object v1, v0, v8

    sput-object v0, Llso;->g:[Llso;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput-wide p3, p0, Llso;->f:J

    .line 45
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Llso;
    .locals 1

    .prologue
    .line 9
    const-class v0, Llso;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Llso;

    return-object v0
.end method

.method public static values()[Llso;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Llso;->g:[Llso;

    invoke-virtual {v0}, [Llso;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Llso;

    return-object v0
.end method


# virtual methods
.method public a(J)J
    .locals 3

    .prologue
    .line 53
    iget-wide v0, p0, Llso;->f:J

    mul-long/2addr v0, p1

    return-wide v0
.end method
