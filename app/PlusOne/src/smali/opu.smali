.class public final Lopu;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lorz;

.field private b:Lopt;

.field private c:Lopt;

.field private d:Lopt;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 232
    invoke-direct {p0}, Loxq;-><init>()V

    .line 235
    iput-object v0, p0, Lopu;->a:Lorz;

    .line 238
    iput-object v0, p0, Lopu;->b:Lopt;

    .line 241
    iput-object v0, p0, Lopu;->c:Lopt;

    .line 244
    iput-object v0, p0, Lopu;->d:Lopt;

    .line 232
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 267
    const/4 v0, 0x0

    .line 268
    iget-object v1, p0, Lopu;->a:Lorz;

    if-eqz v1, :cond_0

    .line 269
    const/4 v0, 0x1

    iget-object v1, p0, Lopu;->a:Lorz;

    .line 270
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 272
    :cond_0
    iget-object v1, p0, Lopu;->b:Lopt;

    if-eqz v1, :cond_1

    .line 273
    const/4 v1, 0x2

    iget-object v2, p0, Lopu;->b:Lopt;

    .line 274
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 276
    :cond_1
    iget-object v1, p0, Lopu;->c:Lopt;

    if-eqz v1, :cond_2

    .line 277
    const/4 v1, 0x3

    iget-object v2, p0, Lopu;->c:Lopt;

    .line 278
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    :cond_2
    iget-object v1, p0, Lopu;->d:Lopt;

    if-eqz v1, :cond_3

    .line 281
    const/4 v1, 0x4

    iget-object v2, p0, Lopu;->d:Lopt;

    .line 282
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    :cond_3
    iget-object v1, p0, Lopu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 285
    iput v0, p0, Lopu;->ai:I

    .line 286
    return v0
.end method

.method public a(Loxn;)Lopu;
    .locals 2

    .prologue
    .line 294
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 295
    sparse-switch v0, :sswitch_data_0

    .line 299
    iget-object v1, p0, Lopu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 300
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lopu;->ah:Ljava/util/List;

    .line 303
    :cond_1
    iget-object v1, p0, Lopu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    :sswitch_0
    return-object p0

    .line 310
    :sswitch_1
    iget-object v0, p0, Lopu;->a:Lorz;

    if-nez v0, :cond_2

    .line 311
    new-instance v0, Lorz;

    invoke-direct {v0}, Lorz;-><init>()V

    iput-object v0, p0, Lopu;->a:Lorz;

    .line 313
    :cond_2
    iget-object v0, p0, Lopu;->a:Lorz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 317
    :sswitch_2
    iget-object v0, p0, Lopu;->b:Lopt;

    if-nez v0, :cond_3

    .line 318
    new-instance v0, Lopt;

    invoke-direct {v0}, Lopt;-><init>()V

    iput-object v0, p0, Lopu;->b:Lopt;

    .line 320
    :cond_3
    iget-object v0, p0, Lopu;->b:Lopt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 324
    :sswitch_3
    iget-object v0, p0, Lopu;->c:Lopt;

    if-nez v0, :cond_4

    .line 325
    new-instance v0, Lopt;

    invoke-direct {v0}, Lopt;-><init>()V

    iput-object v0, p0, Lopu;->c:Lopt;

    .line 327
    :cond_4
    iget-object v0, p0, Lopu;->c:Lopt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 331
    :sswitch_4
    iget-object v0, p0, Lopu;->d:Lopt;

    if-nez v0, :cond_5

    .line 332
    new-instance v0, Lopt;

    invoke-direct {v0}, Lopt;-><init>()V

    iput-object v0, p0, Lopu;->d:Lopt;

    .line 334
    :cond_5
    iget-object v0, p0, Lopu;->d:Lopt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 295
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lopu;->a:Lorz;

    if-eqz v0, :cond_0

    .line 250
    const/4 v0, 0x1

    iget-object v1, p0, Lopu;->a:Lorz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 252
    :cond_0
    iget-object v0, p0, Lopu;->b:Lopt;

    if-eqz v0, :cond_1

    .line 253
    const/4 v0, 0x2

    iget-object v1, p0, Lopu;->b:Lopt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 255
    :cond_1
    iget-object v0, p0, Lopu;->c:Lopt;

    if-eqz v0, :cond_2

    .line 256
    const/4 v0, 0x3

    iget-object v1, p0, Lopu;->c:Lopt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 258
    :cond_2
    iget-object v0, p0, Lopu;->d:Lopt;

    if-eqz v0, :cond_3

    .line 259
    const/4 v0, 0x4

    iget-object v1, p0, Lopu;->d:Lopt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 261
    :cond_3
    iget-object v0, p0, Lopu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 263
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0, p1}, Lopu;->a(Loxn;)Lopu;

    move-result-object v0

    return-object v0
.end method
