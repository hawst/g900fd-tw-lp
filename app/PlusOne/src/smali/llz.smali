.class final Lllz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lorg/chromium/net/HttpUrlRequestListener;


# instance fields
.field private synthetic a:Llly;


# direct methods
.method constructor <init>(Llly;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lllz;->a:Llly;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 4

    .prologue
    .line 65
    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->b()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->c()Ljava/io/IOException;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 66
    :cond_0
    iget-object v0, p0, Lllz;->a:Llly;

    new-instance v1, Llmb;

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->b()I

    move-result v2

    invoke-interface {p1}, Lorg/chromium/net/HttpUrlRequest;->c()Ljava/io/IOException;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Llmb;-><init>(ILjava/lang/Exception;)V

    invoke-static {v0, v1}, Llly;->a(Llly;Llmb;)Llmb;

    .line 68
    :cond_1
    return-void
.end method


# virtual methods
.method public onRequestComplete(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lllz;->a:Llly;

    invoke-static {v0}, Llly;->a(Llly;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 56
    invoke-direct {p0, p1}, Lllz;->a(Lorg/chromium/net/HttpUrlRequest;)V

    .line 57
    iget-object v0, p0, Lllz;->a:Llly;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Llly;->a(Llly;Z)Z

    .line 58
    iget-object v0, p0, Lllz;->a:Llly;

    invoke-static {v0}, Llly;->b(Llly;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 60
    :cond_0
    return-void
.end method

.method public onResponseStarted(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lllz;->a:Llly;

    invoke-static {v0}, Llly;->a(Llly;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 47
    invoke-direct {p0, p1}, Lllz;->a(Lorg/chromium/net/HttpUrlRequest;)V

    .line 48
    iget-object v0, p0, Lllz;->a:Llly;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Llly;->a(Llly;Z)Z

    .line 49
    iget-object v0, p0, Lllz;->a:Llly;

    invoke-static {v0}, Llly;->b(Llly;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 51
    :cond_0
    return-void
.end method
