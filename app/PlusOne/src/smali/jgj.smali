.class public final Ljgj;
.super Ljfy;
.source "PG"

# interfaces
.implements Ljge;


# instance fields
.field private c:Ljgl;

.field private d:Ljga;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 236
    invoke-direct {p0}, Ljfy;-><init>()V

    .line 237
    const-string v0, "recents_virtual_circles"

    invoke-virtual {p0, v0}, Ljgj;->b(Ljava/lang/String;)V

    .line 238
    return-void
.end method

.method private f(I)I
    .locals 1

    .prologue
    .line 230
    invoke-virtual {p0}, Ljgj;->av_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    add-int/lit8 p1, p1, -0x1

    .line 233
    :cond_0
    return p1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Ljgj;->c:Ljgl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljgj;->c:Ljgl;

    iget v0, v0, Ljgl;->b:I

    if-nez v0, :cond_1

    .line 280
    :cond_0
    const/4 v0, 0x0

    .line 285
    :goto_0
    return v0

    .line 282
    :cond_1
    invoke-virtual {p0}, Ljgj;->av_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 283
    iget-object v0, p0, Ljgj;->c:Ljgl;

    iget v0, v0, Ljgl;->b:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 285
    :cond_2
    iget-object v0, p0, Ljgj;->c:Ljgl;

    iget v0, v0, Ljgl;->b:I

    goto :goto_0
.end method

.method public a(I)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 319
    invoke-direct {p0, p1}, Ljgj;->f(I)I

    move-result v0

    .line 320
    iget-object v1, p0, Ljgj;->c:Ljgl;

    iget-object v1, v1, Ljgl;->a:[Ljgk;

    aget-object v1, v1, v0

    .line 321
    iget-object v2, p0, Ljgj;->c:Ljgl;

    iget-object v2, v2, Ljgl;->a:[Ljgk;

    aget-object v0, v2, v0

    iget-wide v2, v0, Ljgk;->b:J

    .line 322
    iget-object v0, p0, Ljgj;->d:Ljga;

    iget-object v1, v1, Ljgk;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljga;->a(Ljava/lang/String;)Ljfy;

    move-result-object v0

    .line 323
    invoke-virtual {v0, v2, v3}, Ljfy;->c(J)Landroid/content/Intent;

    move-result-object v0

    .line 324
    return-object v0
.end method

.method public a(ILjgb;)V
    .locals 4

    .prologue
    .line 303
    invoke-direct {p0, p1}, Ljgj;->f(I)I

    move-result v0

    .line 304
    iget-object v1, p0, Ljgj;->c:Ljgl;

    iget-object v1, v1, Ljgl;->a:[Ljgk;

    aget-object v1, v1, v0

    iget-object v1, v1, Ljgk;->a:Ljava/lang/String;

    .line 305
    iget-object v2, p0, Ljgj;->c:Ljgl;

    iget-object v2, v2, Ljgl;->a:[Ljgk;

    aget-object v0, v2, v0

    iget-wide v2, v0, Ljgk;->b:J

    .line 306
    iget-object v0, p0, Ljgj;->d:Ljga;

    invoke-interface {v0, v1}, Ljga;->a(Ljava/lang/String;)Ljfy;

    move-result-object v0

    .line 307
    invoke-virtual {v0, v2, v3, p2}, Ljfy;->a(JLjgb;)V

    .line 308
    invoke-interface {p2}, Ljgb;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 309
    invoke-virtual {v0}, Ljfy;->d()I

    move-result v0

    invoke-interface {p2, v0}, Ljgb;->a(I)V

    .line 311
    :cond_0
    return-void
.end method

.method public a(JLjgb;)V
    .locals 0

    .prologue
    .line 358
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 242
    invoke-virtual {p0}, Ljgj;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Ljgj;->c:Ljgl;

    invoke-direct {p0, p2}, Ljgj;->f(I)I

    move-result v1

    iget-object v2, v0, Ljgl;->a:[Ljgk;

    aget-object v2, v2, v1

    iget-object v2, v2, Ljgk;->a:Ljava/lang/String;

    iget-object v3, v0, Ljgl;->a:[Ljgk;

    aget-object v3, v3, v1

    iget-wide v4, v3, Ljgk;->b:J

    invoke-virtual {v0, v1}, Ljgl;->a(I)V

    invoke-virtual {v0, v2, v4, v5}, Ljgl;->a(Ljava/lang/String;J)V

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 248
    :cond_1
    iget-object v0, p0, Ljgj;->d:Ljga;

    invoke-interface {v0, p1}, Ljga;->a(Ljava/lang/String;)Ljfy;

    move-result-object v0

    .line 250
    invoke-virtual {v0, p2}, Ljfy;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    invoke-virtual {v0, p2}, Ljfy;->d(I)I

    move-result v1

    .line 255
    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    .line 256
    invoke-virtual {v0, p2}, Ljfy;->e(I)J

    move-result-wide v2

    .line 257
    iget-object v1, p0, Ljgj;->c:Ljgl;

    invoke-virtual {v0}, Ljfy;->w()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v2, v3}, Ljgl;->b(Ljava/lang/String;J)Z

    move-result v0

    .line 258
    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {p0}, Ljgj;->v()V

    goto :goto_0
.end method

.method public a(Ljfz;J)V
    .locals 0

    .prologue
    .line 339
    return-void
.end method

.method public a(Ljga;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Ljgj;->d:Ljga;

    .line 378
    return-void
.end method

.method public a(J)Z
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 342
    invoke-virtual {p0}, Ljgj;->av_()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 343
    cmp-long v2, p1, v4

    if-lez v2, :cond_1

    iget-object v2, p0, Ljgj;->c:Ljgl;

    iget v2, v2, Ljgl;->b:I

    int-to-long v2, v2

    cmp-long v2, p1, v2

    if-gez v2, :cond_1

    .line 345
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 343
    goto :goto_0

    .line 345
    :cond_2
    cmp-long v2, p1, v4

    if-ltz v2, :cond_3

    iget-object v2, p0, Ljgj;->c:Ljgl;

    iget v2, v2, Ljgl;->b:I

    int-to-long v2, v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public aL_()V
    .locals 3

    .prologue
    .line 403
    iget-object v0, p0, Ljgj;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 404
    new-instance v1, Ljgl;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, Ljgl;-><init>(I)V

    iput-object v1, p0, Ljgj;->c:Ljgl;

    .line 405
    iget v1, p0, Ljgj;->b:I

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 406
    iget v1, p0, Ljgj;->b:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 407
    const-string v1, "navigation_recents"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lhej;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 408
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 409
    iget-object v1, p0, Ljgj;->c:Ljgl;

    invoke-virtual {v1, v0}, Ljgl;->a(Ljava/lang/String;)V

    .line 416
    :cond_0
    return-void
.end method

.method public as_()V
    .locals 0

    .prologue
    .line 315
    return-void
.end method

.method protected at_()V
    .locals 0

    .prologue
    .line 329
    return-void
.end method

.method public av_()Z
    .locals 1

    .prologue
    .line 367
    const/4 v0, 0x1

    return v0
.end method

.method public b(J)I
    .locals 1

    .prologue
    .line 350
    const/4 v0, 0x0

    return v0
.end method

.method public c(J)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(I)Z
    .locals 4

    .prologue
    .line 266
    invoke-virtual {p0}, Ljgj;->av_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    if-nez p1, :cond_0

    .line 268
    const/4 v0, 0x0

    .line 274
    :goto_0
    return v0

    .line 271
    :cond_0
    invoke-direct {p0, p1}, Ljgj;->f(I)I

    move-result v0

    .line 272
    iget-object v1, p0, Ljgj;->c:Ljgl;

    iget-object v1, v1, Ljgl;->a:[Ljgk;

    aget-object v1, v1, v0

    iget-object v1, v1, Ljgk;->a:Ljava/lang/String;

    .line 273
    iget-object v2, p0, Ljgj;->d:Ljga;

    invoke-interface {v2, v1}, Ljga;->a(Ljava/lang/String;)Ljfy;

    move-result-object v1

    .line 274
    iget-object v2, p0, Ljgj;->c:Ljgl;

    iget-object v2, v2, Ljgl;->a:[Ljgk;

    aget-object v0, v2, v0

    iget-wide v2, v0, Ljgk;->b:J

    invoke-virtual {v1, v2, v3}, Ljfy;->a(J)Z

    move-result v0

    goto :goto_0
.end method

.method public d(I)I
    .locals 4

    .prologue
    .line 291
    invoke-virtual {p0}, Ljgj;->av_()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 292
    const/4 v0, 0x6

    .line 298
    :goto_0
    return v0

    .line 294
    :cond_0
    invoke-direct {p0, p1}, Ljgj;->f(I)I

    move-result v0

    .line 295
    iget-object v1, p0, Ljgj;->c:Ljgl;

    iget-object v1, v1, Ljgl;->a:[Ljgk;

    aget-object v1, v1, v0

    iget-object v1, v1, Ljgk;->a:Ljava/lang/String;

    .line 296
    iget-object v2, p0, Ljgj;->c:Ljgl;

    iget-object v2, v2, Ljgl;->a:[Ljgk;

    aget-object v0, v2, v0

    iget-wide v2, v0, Ljgk;->b:J

    .line 297
    iget-object v0, p0, Ljgj;->d:Ljga;

    invoke-interface {v0, v1}, Ljga;->a(Ljava/lang/String;)Ljfy;

    move-result-object v0

    .line 298
    invoke-virtual {v0, v2, v3}, Ljfy;->b(J)I

    move-result v0

    goto :goto_0
.end method

.method public e(I)J
    .locals 2

    .prologue
    .line 333
    int-to-long v0, p1

    return-wide v0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Ljgj;->a:Landroid/content/Context;

    const v1, 0x7f0a03ab

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 382
    iget-object v0, p0, Ljgj;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 383
    iget v1, p0, Ljgj;->b:I

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Ljgj;->c:Ljgl;

    invoke-virtual {v0}, Ljgl;->b()Ljava/lang/String;

    move-result-object v1

    .line 385
    iget-object v0, p0, Ljgj;->a:Landroid/content/Context;

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v2, p0, Ljgj;->b:I

    .line 386
    invoke-interface {v0, v2}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v2, "navigation_recents"

    .line 387
    invoke-interface {v0, v2, v1}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    move-result-object v0

    .line 388
    invoke-interface {v0}, Lhek;->c()I

    .line 390
    :cond_0
    return-void
.end method

.method public h()V
    .locals 3

    .prologue
    .line 394
    iget-object v0, p0, Ljgj;->c:Ljgl;

    iget v0, v0, Ljgl;->b:I

    .line 395
    iget-object v1, p0, Ljgj;->c:Ljgl;

    iget-object v2, p0, Ljgj;->d:Ljga;

    invoke-virtual {v1, v2}, Ljgl;->a(Ljga;)I

    move-result v1

    .line 396
    if-eq v0, v1, :cond_0

    .line 397
    invoke-virtual {p0}, Ljgj;->v()V

    .line 399
    :cond_0
    return-void
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x1

    return v0
.end method
