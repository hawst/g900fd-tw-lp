.class public final Lmzm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Locf;

.field public b:Ljava/lang/Integer;

.field public c:[Lnyz;

.field private d:Lmzp;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 126
    invoke-direct {p0}, Loxq;-><init>()V

    .line 129
    iput-object v0, p0, Lmzm;->d:Lmzp;

    .line 132
    iput-object v0, p0, Lmzm;->a:Locf;

    .line 141
    sget-object v0, Lnyz;->a:[Lnyz;

    iput-object v0, p0, Lmzm;->c:[Lnyz;

    .line 126
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 179
    .line 180
    iget-object v0, p0, Lmzm;->d:Lmzp;

    if-eqz v0, :cond_7

    .line 181
    const/4 v0, 0x1

    iget-object v2, p0, Lmzm;->d:Lmzp;

    .line 182
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 184
    :goto_0
    iget-object v2, p0, Lmzm;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 185
    const/4 v2, 0x2

    iget-object v3, p0, Lmzm;->b:Ljava/lang/Integer;

    .line 186
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 188
    :cond_0
    iget-object v2, p0, Lmzm;->c:[Lnyz;

    if-eqz v2, :cond_2

    .line 189
    iget-object v2, p0, Lmzm;->c:[Lnyz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 190
    if-eqz v4, :cond_1

    .line 191
    const/4 v5, 0x3

    .line 192
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 189
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 196
    :cond_2
    iget-object v1, p0, Lmzm;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 197
    const/4 v1, 0x4

    iget-object v2, p0, Lmzm;->g:Ljava/lang/String;

    .line 198
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_3
    iget-object v1, p0, Lmzm;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 201
    const/4 v1, 0x5

    iget-object v2, p0, Lmzm;->f:Ljava/lang/Integer;

    .line 202
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_4
    iget-object v1, p0, Lmzm;->a:Locf;

    if-eqz v1, :cond_5

    .line 205
    const/4 v1, 0x6

    iget-object v2, p0, Lmzm;->a:Locf;

    .line 206
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_5
    iget-object v1, p0, Lmzm;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 209
    const/4 v1, 0x7

    iget-object v2, p0, Lmzm;->e:Ljava/lang/Boolean;

    .line 210
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 212
    :cond_6
    iget-object v1, p0, Lmzm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    iput v0, p0, Lmzm;->ai:I

    .line 214
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmzm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 222
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 223
    sparse-switch v0, :sswitch_data_0

    .line 227
    iget-object v2, p0, Lmzm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 228
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmzm;->ah:Ljava/util/List;

    .line 231
    :cond_1
    iget-object v2, p0, Lmzm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    :sswitch_0
    return-object p0

    .line 238
    :sswitch_1
    iget-object v0, p0, Lmzm;->d:Lmzp;

    if-nez v0, :cond_2

    .line 239
    new-instance v0, Lmzp;

    invoke-direct {v0}, Lmzp;-><init>()V

    iput-object v0, p0, Lmzm;->d:Lmzp;

    .line 241
    :cond_2
    iget-object v0, p0, Lmzm;->d:Lmzp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 245
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmzm;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 249
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 250
    iget-object v0, p0, Lmzm;->c:[Lnyz;

    if-nez v0, :cond_4

    move v0, v1

    .line 251
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyz;

    .line 252
    iget-object v3, p0, Lmzm;->c:[Lnyz;

    if-eqz v3, :cond_3

    .line 253
    iget-object v3, p0, Lmzm;->c:[Lnyz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 255
    :cond_3
    iput-object v2, p0, Lmzm;->c:[Lnyz;

    .line 256
    :goto_2
    iget-object v2, p0, Lmzm;->c:[Lnyz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 257
    iget-object v2, p0, Lmzm;->c:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 258
    iget-object v2, p0, Lmzm;->c:[Lnyz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 259
    invoke-virtual {p1}, Loxn;->a()I

    .line 256
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 250
    :cond_4
    iget-object v0, p0, Lmzm;->c:[Lnyz;

    array-length v0, v0

    goto :goto_1

    .line 262
    :cond_5
    iget-object v2, p0, Lmzm;->c:[Lnyz;

    new-instance v3, Lnyz;

    invoke-direct {v3}, Lnyz;-><init>()V

    aput-object v3, v2, v0

    .line 263
    iget-object v2, p0, Lmzm;->c:[Lnyz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 267
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmzm;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 271
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmzm;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 275
    :sswitch_6
    iget-object v0, p0, Lmzm;->a:Locf;

    if-nez v0, :cond_6

    .line 276
    new-instance v0, Locf;

    invoke-direct {v0}, Locf;-><init>()V

    iput-object v0, p0, Lmzm;->a:Locf;

    .line 278
    :cond_6
    iget-object v0, p0, Lmzm;->a:Locf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 282
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzm;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 223
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 148
    iget-object v0, p0, Lmzm;->d:Lmzp;

    if-eqz v0, :cond_0

    .line 149
    const/4 v0, 0x1

    iget-object v1, p0, Lmzm;->d:Lmzp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 151
    :cond_0
    iget-object v0, p0, Lmzm;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 152
    const/4 v0, 0x2

    iget-object v1, p0, Lmzm;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 154
    :cond_1
    iget-object v0, p0, Lmzm;->c:[Lnyz;

    if-eqz v0, :cond_3

    .line 155
    iget-object v1, p0, Lmzm;->c:[Lnyz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 156
    if-eqz v3, :cond_2

    .line 157
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 155
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_3
    iget-object v0, p0, Lmzm;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 162
    const/4 v0, 0x4

    iget-object v1, p0, Lmzm;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 164
    :cond_4
    iget-object v0, p0, Lmzm;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 165
    const/4 v0, 0x5

    iget-object v1, p0, Lmzm;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 167
    :cond_5
    iget-object v0, p0, Lmzm;->a:Locf;

    if-eqz v0, :cond_6

    .line 168
    const/4 v0, 0x6

    iget-object v1, p0, Lmzm;->a:Locf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 170
    :cond_6
    iget-object v0, p0, Lmzm;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 171
    const/4 v0, 0x7

    iget-object v1, p0, Lmzm;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 173
    :cond_7
    iget-object v0, p0, Lmzm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 175
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lmzm;->a(Loxn;)Lmzm;

    move-result-object v0

    return-object v0
.end method
