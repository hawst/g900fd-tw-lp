.class public Llkl;
.super Lcom/google/android/libraries/social/media/ui/MediaView;
.source "PG"


# static fields
.field private static final a:Landroid/graphics/Paint;

.field private static b:Lizs;


# instance fields
.field private c:Z

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/graphics/Matrix;

.field private k:Landroid/graphics/Bitmap;

.field private l:Landroid/graphics/Bitmap;

.field private m:Lizu;

.field private n:Lcom/google/android/libraries/social/media/MediaResource;

.field private o:Landroid/graphics/Matrix;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 42
    sput-object v0, Llkl;->a:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    .line 48
    iput-boolean v1, p0, Llkl;->d:Z

    .line 49
    iput v1, p0, Llkl;->e:I

    .line 54
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Llkl;->j:Landroid/graphics/Matrix;

    .line 60
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Llkl;->o:Landroid/graphics/Matrix;

    .line 75
    sget-object v0, Llkl;->b:Lizs;

    if-nez v0, :cond_0

    .line 76
    invoke-virtual {p0}, Llkl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lizs;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Llkl;->b:Lizs;

    .line 79
    :cond_0
    invoke-virtual {p0, v2}, Llkl;->b(I)V

    .line 80
    invoke-virtual {p0}, Llkl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Llkl;->c:Z

    .line 64
    return-void

    :cond_1
    move v0, v2

    .line 80
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    iput-boolean v1, p0, Llkl;->d:Z

    .line 49
    iput v1, p0, Llkl;->e:I

    .line 54
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Llkl;->j:Landroid/graphics/Matrix;

    .line 60
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Llkl;->o:Landroid/graphics/Matrix;

    .line 75
    sget-object v0, Llkl;->b:Lizs;

    if-nez v0, :cond_0

    .line 76
    invoke-virtual {p0}, Llkl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lizs;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Llkl;->b:Lizs;

    .line 79
    :cond_0
    invoke-virtual {p0, v2}, Llkl;->b(I)V

    .line 80
    invoke-virtual {p0}, Llkl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Llkl;->c:Z

    .line 68
    return-void

    :cond_1
    move v0, v2

    .line 80
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    iput-boolean v1, p0, Llkl;->d:Z

    .line 49
    iput v1, p0, Llkl;->e:I

    .line 54
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Llkl;->j:Landroid/graphics/Matrix;

    .line 60
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Llkl;->o:Landroid/graphics/Matrix;

    .line 75
    sget-object v0, Llkl;->b:Lizs;

    if-nez v0, :cond_0

    .line 76
    invoke-virtual {p0}, Llkl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lizs;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Llkl;->b:Lizs;

    .line 79
    :cond_0
    invoke-virtual {p0, v2}, Llkl;->b(I)V

    .line 80
    invoke-virtual {p0}, Llkl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Llkl;->c:Z

    .line 72
    return-void

    :cond_1
    move v0, v2

    .line 80
    goto :goto_0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 268
    iget-object v0, p0, Llkl;->n:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llkl;->n:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 269
    :goto_0
    if-nez v0, :cond_1

    .line 284
    :goto_1
    return-void

    .line 268
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 273
    :cond_1
    iget-object v1, p0, Llkl;->o:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 276
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 277
    iget v2, p0, Llkl;->g:I

    int-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 278
    iget-object v2, p0, Llkl;->o:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 281
    iget-object v1, p0, Llkl;->o:Landroid/graphics/Matrix;

    const/4 v2, 0x0

    iget v3, p0, Llkl;->h:I

    neg-int v3, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 283
    iget-object v1, p0, Llkl;->o:Landroid/graphics/Matrix;

    sget-object v2, Llkl;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_1
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Llkl;->g:I

    neg-int v0, v0

    .line 217
    if-ge p1, v0, :cond_0

    move p1, v0

    .line 221
    :cond_0
    iget v0, p0, Llkl;->h:I

    if-eq v0, p1, :cond_1

    .line 222
    iput p1, p0, Llkl;->h:I

    .line 223
    invoke-virtual {p0}, Llkl;->invalidate()V

    .line 225
    :cond_1
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/16 v7, 0x3ac

    const/high16 v6, 0x446b0000    # 940.0f

    const/4 v5, 0x0

    .line 397
    iget v0, p0, Llkl;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 398
    iget-object v0, p0, Llkl;->j:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    invoke-virtual {p0}, Llkl;->t()I

    move-result v1

    invoke-virtual {p0}, Llkl;->u()I

    move-result v0

    iget v2, p0, Llkl;->f:I

    if-le v2, v1, :cond_0

    iget v2, p0, Llkl;->f:I

    int-to-float v2, v2

    int-to-float v1, v1

    div-float/2addr v2, v1

    iget-object v1, p0, Llkl;->j:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget v1, p0, Llkl;->f:I

    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    :cond_0
    iget v2, p0, Llkl;->g:I

    if-le v2, v0, :cond_1

    iget v2, p0, Llkl;->g:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    iget-object v2, p0, Llkl;->j:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v1, v0

    iget v0, p0, Llkl;->g:I

    :cond_1
    iget v2, p0, Llkl;->f:I

    if-le v1, v2, :cond_2

    iget-object v2, p0, Llkl;->j:Landroid/graphics/Matrix;

    iget v3, p0, Llkl;->g:I

    sub-int v3, v0, v3

    neg-int v3, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v2, v5, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_2
    iget v2, p0, Llkl;->g:I

    if-le v0, v2, :cond_3

    iget-object v2, p0, Llkl;->j:Landroid/graphics/Matrix;

    iget v3, p0, Llkl;->f:I

    sub-int/2addr v1, v3

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v2, v1, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_3
    iget-object v1, p0, Llkl;->j:Landroid/graphics/Matrix;

    iget v2, p0, Llkl;->h:I

    neg-int v2, v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v1, p0, Llkl;->j:Landroid/graphics/Matrix;

    invoke-virtual {p0, v1}, Llkl;->a(Landroid/graphics/Matrix;)V

    iget v1, p0, Llkl;->g:I

    if-ge v0, v1, :cond_4

    invoke-direct {p0, p1}, Llkl;->b(Landroid/graphics/Canvas;)V

    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/graphics/Canvas;)V

    .line 402
    :cond_5
    :goto_0
    return-void

    .line 399
    :cond_6
    iget v0, p0, Llkl;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 400
    invoke-virtual {p0}, Llkl;->s()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Llkl;->t()I

    move-result v2

    invoke-virtual {p0}, Llkl;->u()I

    move-result v0

    iget-object v1, p0, Llkl;->j:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    iget v1, p0, Llkl;->f:I

    if-le v1, v2, :cond_a

    iget v1, p0, Llkl;->f:I

    int-to-float v1, v1

    int-to-float v3, v2

    div-float v3, v1, v3

    iget v1, p0, Llkl;->f:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iget-object v4, p0, Llkl;->j:Landroid/graphics/Matrix;

    invoke-virtual {v4, v3, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    :goto_1
    iget v3, p0, Llkl;->i:I

    if-eqz v3, :cond_7

    if-ge v1, v7, :cond_b

    int-to-float v1, v2

    div-float/2addr v1, v6

    iget v2, p0, Llkl;->i:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    :goto_2
    iget-object v2, p0, Llkl;->j:Landroid/graphics/Matrix;

    int-to-float v1, v1

    invoke-virtual {v2, v5, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_7
    iget v1, p0, Llkl;->f:I

    int-to-float v1, v1

    const/high16 v2, 0x40a00000    # 5.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    if-le v0, v1, :cond_9

    iget-object v0, p0, Llkl;->l:Landroid/graphics/Bitmap;

    if-nez v0, :cond_8

    iget v0, p0, Llkl;->f:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llkl;->l:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Llkl;->l:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Llkl;->k:Landroid/graphics/Bitmap;

    iget-object v3, p0, Llkl;->j:Landroid/graphics/Matrix;

    sget-object v4, Llkl;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    :cond_8
    iget-object v0, p0, Llkl;->j:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    move v0, v1

    :cond_9
    iget-object v1, p0, Llkl;->j:Landroid/graphics/Matrix;

    iget v2, p0, Llkl;->g:I

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {v1, v5, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Llkl;->j:Landroid/graphics/Matrix;

    iget v1, p0, Llkl;->h:I

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v5, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-direct {p0, p1}, Llkl;->b(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Llkl;->l:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_c

    iget-object v0, p0, Llkl;->l:Landroid/graphics/Bitmap;

    :goto_3
    iget-object v1, p0, Llkl;->j:Landroid/graphics/Matrix;

    sget-object v2, Llkl;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_a
    move v1, v2

    goto :goto_1

    :cond_b
    if-le v1, v7, :cond_d

    int-to-float v1, v1

    div-float/2addr v1, v6

    iget v2, p0, Llkl;->i:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_2

    :cond_c
    iget-object v0, p0, Llkl;->k:Landroid/graphics/Bitmap;

    goto :goto_3

    :cond_d
    move v1, v3

    goto :goto_2
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 212
    invoke-virtual {p0}, Llkl;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Ljac;->a:Ljac;

    invoke-static {v0, p1, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Llkl;->m:Lizu;

    .line 213
    return-void
.end method

.method public a(Ljava/lang/String;Lnjp;IZ)V
    .locals 5

    .prologue
    .line 162
    if-eqz p2, :cond_0

    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p2, Lnjp;->b:Ljava/lang/Float;

    .line 163
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v2, p2, Lnjp;->a:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v3, p2, Lnjp;->d:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    iget-object v4, p2, Lnjp;->c:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 165
    :goto_0
    new-instance v1, Llkn;

    invoke-direct {v1, p1, v0, p3}, Llkn;-><init>(Ljava/lang/String;Landroid/graphics/RectF;I)V

    .line 166
    invoke-virtual {p0, v1, p4}, Llkl;->a(Llkn;Z)V

    .line 167
    return-void

    .line 163
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Llkn;Z)V
    .locals 8

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, -0x1

    .line 170
    invoke-virtual {p1}, Llkn;->a()Ljava/lang/String;

    move-result-object v1

    .line 171
    iget v0, p0, Llkl;->e:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    iget-boolean v0, p0, Llkl;->c:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Llkl;->d:Z

    if-eqz v0, :cond_4

    .line 172
    invoke-virtual {p1}, Llkn;->b()Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 174
    const-string v0, "-fcrop64"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 175
    if-eq v3, v5, :cond_4

    .line 180
    const/16 v0, 0x2d

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 181
    const/16 v0, 0x2f

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v1, v0, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 182
    if-ne v2, v5, :cond_1

    .line 189
    :goto_0
    if-ne v0, v5, :cond_0

    .line 190
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 195
    :cond_0
    invoke-virtual {p1}, Llkn;->b()Landroid/graphics/RectF;

    move-result-object v2

    .line 196
    iget v4, v2, Landroid/graphics/RectF;->top:F

    iget v5, v2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v4, v5

    div-float/2addr v4, v6

    .line 197
    iget v5, v2, Landroid/graphics/RectF;->top:F

    sub-float v5, v4, v5

    div-float/2addr v5, v6

    .line 198
    sub-float v6, v4, v5

    .line 199
    add-float/2addr v4, v5

    .line 201
    new-instance v5, Landroid/graphics/RectF;

    iget v7, v2, Landroid/graphics/RectF;->left:F

    iget v2, v2, Landroid/graphics/RectF;->right:F

    invoke-direct {v5, v7, v6, v2, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 202
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 203
    invoke-virtual {p1}, Llkn;->c()I

    move-result v3

    invoke-static {v5, v3}, Ljbd;->a(Landroid/graphics/RectF;I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 204
    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 208
    :goto_1
    invoke-virtual {p0}, Llkl;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p2, :cond_3

    sget-object v1, Ljac;->d:Ljac;

    :goto_2
    invoke-static {v2, v0, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    .line 207
    invoke-super {p0, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 209
    return-void

    .line 184
    :cond_1
    if-eq v0, v5, :cond_2

    .line 185
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 187
    goto/16 :goto_0

    .line 208
    :cond_3
    sget-object v1, Ljac;->a:Ljac;

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 235
    iput-boolean p1, p0, Llkl;->d:Z

    .line 236
    return-void
.end method

.method protected b(Lkda;)V
    .locals 2

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Lkda;)V

    .line 128
    iget v0, p0, Llkl;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    instance-of v0, p1, Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    .line 129
    check-cast p1, Lcom/google/android/libraries/social/media/MediaResource;

    .line 130
    invoke-virtual {p1}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llkl;->k:Landroid/graphics/Bitmap;

    .line 132
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x1

    iput v0, p0, Llkl;->e:I

    .line 136
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x2

    iput v0, p0, Llkl;->e:I

    .line 140
    return-void
.end method

.method public i()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Llkl;->g:I

    return v0
.end method

.method public j(I)V
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Llkl;->i:I

    if-eq v0, p1, :cond_0

    .line 229
    iput p1, p0, Llkl;->i:I

    .line 230
    invoke-virtual {p0}, Llkl;->invalidate()V

    .line 232
    :cond_0
    return-void
.end method

.method protected n()V
    .locals 4

    .prologue
    .line 86
    invoke-super {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->n()V

    .line 87
    iget-object v0, p0, Llkl;->m:Lizu;

    if-eqz v0, :cond_0

    .line 88
    sget-object v0, Llkl;->b:Lizs;

    iget-object v1, p0, Llkl;->m:Lizu;

    const/4 v2, 0x4

    new-instance v3, Llkm;

    invoke-direct {v3, p0}, Llkm;-><init>(Llkl;)V

    .line 89
    invoke-virtual {v0, v1, v2, v3}, Lizs;->a(Lizu;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Llkl;->n:Lcom/google/android/libraries/social/media/MediaResource;

    .line 108
    :cond_0
    return-void
.end method

.method public o()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-super {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->o()V

    .line 114
    iget-object v0, p0, Llkl;->n:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Llkl;->n:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 116
    iput-object v1, p0, Llkl;->n:Lcom/google/android/libraries/social/media/MediaResource;

    .line 119
    :cond_0
    iget-object v0, p0, Llkl;->l:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Llkl;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 121
    iput-object v1, p0, Llkl;->l:Landroid/graphics/Bitmap;

    .line 123
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    const/16 v0, 0x3ac

    .line 240
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->onMeasure(II)V

    .line 242
    invoke-virtual {p0}, Llkl;->getMeasuredWidth()I

    move-result v1

    iput v1, p0, Llkl;->f:I

    .line 243
    iget-boolean v1, p0, Llkl;->c:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Llkl;->d:Z

    if-eqz v1, :cond_1

    .line 244
    iget v1, p0, Llkl;->f:I

    int-to-float v1, v1

    const v2, 0x40633333    # 3.55f

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Llkl;->g:I

    .line 249
    :goto_0
    iget v1, p0, Llkl;->e:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 250
    iget v0, p0, Llkl;->f:I

    iget v1, p0, Llkl;->g:I

    invoke-virtual {p0, v0, v1}, Llkl;->a(II)V

    .line 264
    :cond_0
    :goto_1
    iget v0, p0, Llkl;->f:I

    iget v1, p0, Llkl;->g:I

    invoke-virtual {p0, v0, v1}, Llkl;->setMeasuredDimension(II)V

    .line 265
    return-void

    .line 246
    :cond_1
    iget v1, p0, Llkl;->f:I

    int-to-float v1, v1

    const v2, 0x3fe38e39

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Llkl;->g:I

    goto :goto_0

    .line 252
    :cond_2
    iget v1, p0, Llkl;->e:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 255
    iget v1, p0, Llkl;->f:I

    if-le v1, v0, :cond_3

    .line 261
    :goto_2
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Llkl;->a(II)V

    goto :goto_1

    .line 258
    :cond_3
    iget v0, p0, Llkl;->f:I

    goto :goto_2
.end method
