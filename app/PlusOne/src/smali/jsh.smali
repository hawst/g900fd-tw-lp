.class public final Ljsh;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;


# static fields
.field private static a:I

.field private static b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private static e:Landroid/graphics/Bitmap;

.field private static f:Lhso;

.field private static final k:[Landroid/graphics/Paint;

.field private static final r:Landroid/graphics/Rect;

.field private static final s:Landroid/graphics/Rect;

.field private static final t:Landroid/graphics/Rect;


# instance fields
.field private c:Z

.field private d:Z

.field private g:I

.field private final h:[Ljava/lang/String;

.field private final i:[Lkda;

.field private final j:[Ljsi;

.field private l:I

.field private m:Z

.field private n:I

.field private o:I

.field private final p:I

.field private final q:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v1, 0x2

    .line 109
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/Paint;

    .line 110
    sput-object v0, Ljsh;->k:[Landroid/graphics/Paint;

    move v0, v1

    :goto_0
    if-ltz v0, :cond_0

    .line 111
    add-int/lit8 v2, v0, 0x1

    int-to-double v2, v2

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 112
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 113
    const-wide v6, 0x406fe00000000000L    # 255.0

    const-wide v8, 0x3fe6666666666666L    # 0.7

    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-int v5, v6

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 114
    sget-object v5, Ljsh;->k:[Landroid/graphics/Paint;

    double-to-int v2, v2

    aput-object v4, v5, v2

    .line 110
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 129
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Ljsh;->r:Landroid/graphics/Rect;

    .line 130
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Ljsh;->s:Landroid/graphics/Rect;

    .line 131
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Ljsh;->t:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    const v8, 0x7f0b0127

    const v7, 0x7f0204e2

    const/4 v1, 0x0

    const/4 v6, 0x3

    .line 134
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    new-array v0, v6, [Ljava/lang/String;

    iput-object v0, p0, Ljsh;->h:[Ljava/lang/String;

    .line 95
    new-array v0, v6, [Lkda;

    iput-object v0, p0, Ljsh;->i:[Lkda;

    .line 101
    new-array v0, v6, [Ljsi;

    iput-object v0, p0, Ljsh;->j:[Ljsi;

    .line 146
    invoke-virtual {p0, v1}, Ljsh;->setWillNotDraw(Z)V

    .line 147
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {p0}, Ljsh;->d()I

    move-result v2

    invoke-direct {p0}, Ljsh;->d()I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Ljsh;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 149
    invoke-virtual {p0}, Ljsh;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhss;->e(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Ljsh;->l:I

    .line 152
    sget-object v0, Ljsh;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const v0, 0x7f0b0129

    invoke-direct {p0, v0}, Ljsh;->b(I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Ljsh;->e:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Ljsh;->b:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Ljsh;->b:Landroid/util/SparseArray;

    const/16 v2, 0x9

    new-instance v3, Landroid/util/Pair;

    invoke-direct {p0, v8}, Ljsh;->b(I)Landroid/graphics/Bitmap;

    move-result-object v4

    const v5, 0x7f020540

    invoke-direct {p0, v5}, Ljsh;->a(I)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Ljsh;->b:Landroid/util/SparseArray;

    const/16 v2, 0x8

    new-instance v3, Landroid/util/Pair;

    invoke-direct {p0, v8}, Ljsh;->b(I)Landroid/graphics/Bitmap;

    move-result-object v4

    const v5, 0x7f020500

    invoke-direct {p0, v5}, Ljsh;->a(I)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Ljsh;->b:Landroid/util/SparseArray;

    const/4 v2, 0x5

    new-instance v3, Landroid/util/Pair;

    const v4, 0x7f0b0126

    invoke-direct {p0, v4}, Ljsh;->b(I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {p0, v7}, Ljsh;->a(I)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Ljsh;->b:Landroid/util/SparseArray;

    const/4 v2, 0x7

    new-instance v3, Landroid/util/Pair;

    const v4, 0x7f0b0128

    invoke-direct {p0, v4}, Ljsh;->b(I)Landroid/graphics/Bitmap;

    move-result-object v4

    const v5, 0x7f0204de

    invoke-direct {p0, v5}, Ljsh;->a(I)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Ljsh;->b:Landroid/util/SparseArray;

    const/4 v2, 0x1

    new-instance v3, Landroid/util/Pair;

    const v4, 0x7f0b012b

    invoke-direct {p0, v4}, Ljsh;->b(I)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {p0, v7}, Ljsh;->a(I)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 154
    :cond_1
    invoke-virtual {p0}, Ljsh;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lhso;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhso;

    sput-object v0, Ljsh;->f:Lhso;

    .line 155
    invoke-virtual {p0}, Ljsh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0201

    .line 156
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Ljsh;->p:I

    .line 157
    invoke-virtual {p0}, Ljsh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d01ff

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Ljsh;->q:I

    .line 159
    const-string v0, "round"

    invoke-static {v0}, Lhss;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ljsh;->n:I

    .line 160
    const-string v0, "medium"

    invoke-static {v0}, Lhss;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ljsh;->o:I

    move v0, v1

    .line 163
    :goto_0
    if-ge v0, v6, :cond_2

    .line 164
    iget-object v1, p0, Ljsh;->j:[Ljsi;

    new-instance v2, Ljsi;

    invoke-direct {v2}, Ljsi;-><init>()V

    aput-object v2, v1, v0

    .line 163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_2
    return-void
.end method

.method private a(I)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 401
    invoke-virtual {p0}, Ljsh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private b(I)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 409
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Landroid/graphics/drawable/shapes/OvalShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/OvalShape;-><init>()V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p0}, Ljsh;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget v1, p0, Ljsh;->l:I

    iget v2, p0, Ljsh;->l:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    return-object v3
.end method

.method private d()I
    .locals 2

    .prologue
    .line 344
    sget v0, Ljsh;->a:I

    if-nez v0, :cond_0

    .line 345
    invoke-virtual {p0}, Ljsh;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01fe

    .line 346
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Ljsh;->a:I

    .line 348
    :cond_0
    sget v0, Ljsh;->a:I

    return v0
.end method


# virtual methods
.method protected a()Landroid/util/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    sget-object v0, Ljsh;->b:Landroid/util/SparseArray;

    iget v1, p0, Ljsh;->g:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    return-object v0
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 195
    iput p1, p0, Ljsh;->g:I

    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljsh;->c:Z

    .line 197
    iput-boolean p2, p0, Ljsh;->d:Z

    .line 198
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 174
    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 175
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    .line 176
    if-eqz v0, :cond_0

    .line 177
    iget-object v3, p0, Ljsh;->h:[Ljava/lang/String;

    invoke-interface {v0}, Ljpv;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    .line 174
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 181
    :cond_1
    iput-boolean v2, p0, Ljsh;->c:Z

    .line 182
    const/4 v0, -0x1

    iput v0, p0, Ljsh;->g:I

    .line 183
    iput-boolean v2, p0, Ljsh;->d:Z

    .line 184
    invoke-virtual {p0}, Ljsh;->b()V

    .line 185
    return-void
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 230
    invoke-virtual {p0}, Ljsh;->invalidate()V

    .line 231
    return-void
.end method

.method public b()V
    .locals 10

    .prologue
    const/4 v8, 0x3

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 206
    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v5

    :goto_0
    if-eqz v0, :cond_5

    .line 207
    iget-object v0, p0, Ljsh;->h:[Ljava/lang/String;

    move v0, v1

    .line 208
    :goto_1
    iget-object v2, p0, Ljsh;->h:[Ljava/lang/String;

    if-ge v0, v8, :cond_1

    .line 212
    iget-object v2, p0, Ljsh;->i:[Lkda;

    sget-object v3, Ljsh;->f:Lhso;

    iget-object v4, p0, Ljsh;->h:[Ljava/lang/String;

    aget-object v4, v4, v0

    iget v6, p0, Ljsh;->o:I

    iget v7, p0, Ljsh;->n:I

    invoke-interface {v3, v4, v6, v7, p0}, Lhso;->a(Ljava/lang/String;IILkdd;)Lkda;

    move-result-object v3

    aput-object v3, v2, v0

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 206
    goto :goto_0

    :cond_1
    move v0, v1

    move v2, v1

    .line 215
    :goto_2
    iget-object v3, p0, Ljsh;->i:[Lkda;

    if-ge v0, v8, :cond_3

    iget-object v3, p0, Ljsh;->i:[Lkda;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    add-int/lit8 v2, v2, 0x1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget v0, p0, Ljsh;->p:I

    add-int/lit8 v3, v2, -0x1

    mul-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    iget v3, p0, Ljsh;->p:I

    sub-int v4, v3, v0

    iget v3, p0, Ljsh;->p:I

    sub-int/2addr v3, v0

    add-int/lit8 v0, v2, -0x1

    move v9, v0

    move v0, v3

    move v3, v4

    move v4, v9

    :goto_3
    if-ltz v4, :cond_5

    iget-object v6, p0, Ljsh;->i:[Lkda;

    aget-object v6, v6, v4

    if-nez v6, :cond_4

    iget-object v6, p0, Ljsh;->j:[Ljsi;

    aget-object v6, v6, v4

    iput-boolean v1, v6, Ljsi;->e:Z

    :goto_4
    add-int/lit8 v4, v4, -0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v6, v4, 0x1

    sub-int/2addr v6, v2

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    iget-object v7, p0, Ljsh;->j:[Ljsi;

    aget-object v7, v7, v4

    iput v3, v7, Ljsi;->a:I

    iget-object v7, p0, Ljsh;->j:[Ljsi;

    aget-object v7, v7, v4

    iput v0, v7, Ljsi;->b:I

    iget-object v7, p0, Ljsh;->j:[Ljsi;

    aget-object v7, v7, v4

    iget-object v8, p0, Ljsh;->i:[Lkda;

    aget-object v8, v8, v6

    iput-object v8, v7, Ljsi;->c:Lkda;

    iget-object v7, p0, Ljsh;->j:[Ljsi;

    aget-object v7, v7, v4

    iput v6, v7, Ljsi;->d:I

    iget-object v6, p0, Ljsh;->j:[Ljsi;

    aget-object v6, v6, v4

    iput-boolean v5, v6, Ljsi;->e:Z

    iget v6, p0, Ljsh;->p:I

    add-int/2addr v3, v6

    iget v6, p0, Ljsh;->p:I

    add-int/2addr v0, v6

    goto :goto_4

    .line 218
    :cond_5
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 222
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Ljsh;->i:[Lkda;

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 223
    iget-object v1, p0, Ljsh;->i:[Lkda;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Lkda;->unregister(Lkdd;)V

    .line 224
    iget-object v1, p0, Ljsh;->i:[Lkda;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 222
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 235
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 236
    invoke-virtual {p0}, Ljsh;->b()V

    .line 237
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 276
    iget-boolean v0, p0, Ljsh;->c:Z

    if-eqz v0, :cond_3

    .line 277
    invoke-virtual {p0}, Ljsh;->a()Landroid/util/Pair;

    move-result-object v2

    iget-boolean v0, p0, Ljsh;->d:Z

    if-eqz v0, :cond_1

    sget-object v0, Ljsh;->e:Landroid/graphics/Bitmap;

    move-object v1, v0

    :goto_0
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    iget-boolean v2, p0, Ljsh;->m:Z

    if-eqz v2, :cond_2

    sget-object v2, Ljsh;->s:Landroid/graphics/Rect;

    sget-object v3, Ljsh;->t:Landroid/graphics/Rect;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :goto_1
    invoke-virtual {p0}, Ljsh;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Ljsh;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1, v0, v7, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 306
    :cond_0
    return-void

    .line 277
    :cond_1
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v1, v7, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 281
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Ljsh;->j:[Ljsi;

    const/4 v0, 0x3

    if-ge v1, v0, :cond_0

    .line 283
    iget-object v0, p0, Ljsh;->j:[Ljsi;

    aget-object v2, v0, v1

    .line 284
    iget-boolean v0, v2, Ljsi;->e:Z

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, v2, Ljsi;->c:Lkda;

    .line 289
    iget v3, v2, Ljsi;->a:I

    int-to-float v3, v3

    iget v4, v2, Ljsi;->b:I

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 291
    if-eqz v0, :cond_5

    .line 292
    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 293
    if-nez v0, :cond_4

    .line 294
    invoke-virtual {p0}, Ljsh;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v3, p0, Ljsh;->n:I

    invoke-static {v0, v3}, Lhss;->d(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 297
    :cond_4
    iget-boolean v3, p0, Ljsh;->m:Z

    if-eqz v3, :cond_6

    .line 298
    sget-object v3, Ljsh;->s:Landroid/graphics/Rect;

    sget-object v4, Ljsh;->r:Landroid/graphics/Rect;

    sget-object v5, Ljsh;->k:[Landroid/graphics/Paint;

    iget v6, v2, Ljsi;->d:I

    aget-object v5, v5, v6

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 303
    :goto_3
    iget v0, v2, Ljsi;->a:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v2, v2, Ljsi;->b:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 281
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 301
    :cond_6
    sget-object v3, Ljsh;->k:[Landroid/graphics/Paint;

    iget v4, v2, Ljsi;->d:I

    aget-object v3, v3, v4

    invoke-virtual {p1, v0, v7, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 241
    iget v0, p0, Ljsh;->l:I

    .line 242
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 244
    if-ne v1, v4, :cond_2

    .line 245
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 250
    :cond_0
    :goto_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 253
    if-ne v1, v4, :cond_3

    .line 254
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 260
    :goto_1
    iget v1, p0, Ljsh;->l:I

    if-eq v0, v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    iput-boolean v1, p0, Ljsh;->m:Z

    .line 261
    iget-boolean v1, p0, Ljsh;->m:Z

    if-eqz v1, :cond_1

    .line 262
    sget-object v1, Ljsh;->r:Landroid/graphics/Rect;

    iget v3, p0, Ljsh;->q:I

    iget v4, p0, Ljsh;->q:I

    invoke-virtual {v1, v2, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 263
    sget-object v1, Ljsh;->t:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v2, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 264
    iget v1, p0, Ljsh;->l:I

    if-le v1, v0, :cond_1

    .line 265
    sget-object v1, Ljsh;->s:Landroid/graphics/Rect;

    iget v3, p0, Ljsh;->l:I

    iget v4, p0, Ljsh;->l:I

    invoke-virtual {v1, v2, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 269
    :cond_1
    invoke-virtual {p0, v0, v0}, Ljsh;->setMeasuredDimension(II)V

    .line 270
    return-void

    .line 246
    :cond_2
    const/high16 v3, -0x80000000

    if-ne v1, v3, :cond_0

    .line 247
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 256
    :cond_3
    iget v1, p0, Ljsh;->l:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    :cond_4
    move v1, v2

    .line 260
    goto :goto_2
.end method
