.class final Lejw;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private a:Lois;

.field private synthetic b:Leju;


# direct methods
.method constructor <init>(Leju;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lejw;->b:Leju;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Loiu;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lejw;->a:Lois;

    iget-object v0, v0, Lois;->d:[Loiu;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a([Lois;I)V
    .locals 8

    .prologue
    .line 86
    const/4 v0, 0x1

    :goto_0
    array-length v1, p1

    if-gt v0, v1, :cond_1

    .line 87
    add-int/lit8 v1, v0, -0x1

    aget-object v1, p1, v1

    .line 88
    iget v2, v1, Lois;->b:I

    if-ne p2, v2, :cond_0

    .line 89
    iput-object v1, p0, Lejw;->a:Lois;

    .line 86
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_1
    iget-object v1, p0, Lejw;->a:Lois;

    iget-object v0, p0, Lejw;->b:Leju;

    invoke-static {v0}, Leju;->b(Leju;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v2, v1, Lois;->d:[Loiu;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    array-length v4, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, v2, v0

    iget-object v6, v5, Loiu;->b:Lohv;

    iget-object v6, v6, Lohv;->b:Lohp;

    iget-object v6, v6, Lohp;->d:Ljava/lang/String;

    iget-object v7, p0, Lejw;->b:Leju;

    invoke-static {v7}, Leju;->b(Leju;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Loiu;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Loiu;

    iput-object v0, v1, Lois;->d:[Loiu;

    .line 93
    :cond_4
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lejw;->a:Lois;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lejw;->a:Lois;

    iget-object v0, v0, Lois;->d:[Loiu;

    array-length v0, v0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lejw;->a(I)Loiu;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 116
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 141
    invoke-virtual {p0, p1}, Lejw;->a(I)Loiu;

    move-result-object v1

    .line 144
    if-nez p2, :cond_0

    .line 145
    iget-object v0, p0, Lejw;->b:Leju;

    invoke-virtual {v0, v5}, Leju;->a_(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f040164

    .line 146
    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    move-object p2, v0

    .line 151
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowView;

    .line 152
    iget-object v2, p0, Lejw;->b:Leju;

    iget-object v3, p0, Lejw;->b:Leju;

    iget-object v3, v3, Leju;->U:Lhxh;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Lfze;Lhxh;Z)V

    .line 154
    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(Ljava/lang/Object;Lfuw;)V

    .line 155
    iget-object v1, p0, Lejw;->b:Leju;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->a(ILjava/lang/Object;)V

    .line 158
    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->c()V

    .line 160
    return-object p2

    .line 148
    :cond_0
    check-cast p2, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lejw;->a:Lois;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method
