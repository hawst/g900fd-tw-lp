.class public final Lmjk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnet;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27295
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27298
    iput-object v0, p0, Lmjk;->apiHeader:Llyq;

    .line 27301
    iput-object v0, p0, Lmjk;->a:Lnet;

    .line 27295
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27318
    const/4 v0, 0x0

    .line 27319
    iget-object v1, p0, Lmjk;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 27320
    const/4 v0, 0x1

    iget-object v1, p0, Lmjk;->apiHeader:Llyq;

    .line 27321
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27323
    :cond_0
    iget-object v1, p0, Lmjk;->a:Lnet;

    if-eqz v1, :cond_1

    .line 27324
    const/4 v1, 0x2

    iget-object v2, p0, Lmjk;->a:Lnet;

    .line 27325
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27327
    :cond_1
    iget-object v1, p0, Lmjk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27328
    iput v0, p0, Lmjk;->ai:I

    .line 27329
    return v0
.end method

.method public a(Loxn;)Lmjk;
    .locals 2

    .prologue
    .line 27337
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27338
    sparse-switch v0, :sswitch_data_0

    .line 27342
    iget-object v1, p0, Lmjk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27343
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjk;->ah:Ljava/util/List;

    .line 27346
    :cond_1
    iget-object v1, p0, Lmjk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27348
    :sswitch_0
    return-object p0

    .line 27353
    :sswitch_1
    iget-object v0, p0, Lmjk;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 27354
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmjk;->apiHeader:Llyq;

    .line 27356
    :cond_2
    iget-object v0, p0, Lmjk;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27360
    :sswitch_2
    iget-object v0, p0, Lmjk;->a:Lnet;

    if-nez v0, :cond_3

    .line 27361
    new-instance v0, Lnet;

    invoke-direct {v0}, Lnet;-><init>()V

    iput-object v0, p0, Lmjk;->a:Lnet;

    .line 27363
    :cond_3
    iget-object v0, p0, Lmjk;->a:Lnet;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27338
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27306
    iget-object v0, p0, Lmjk;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 27307
    const/4 v0, 0x1

    iget-object v1, p0, Lmjk;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27309
    :cond_0
    iget-object v0, p0, Lmjk;->a:Lnet;

    if-eqz v0, :cond_1

    .line 27310
    const/4 v0, 0x2

    iget-object v1, p0, Lmjk;->a:Lnet;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27312
    :cond_1
    iget-object v0, p0, Lmjk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27314
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 27291
    invoke-virtual {p0, p1}, Lmjk;->a(Loxn;)Lmjk;

    move-result-object v0

    return-object v0
.end method
