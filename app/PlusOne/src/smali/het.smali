.class public final Lhet;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhee;
.implements Llnx;
.implements Llqz;
.implements Llrc;
.implements Llrd;
.implements Llre;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:I

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lheg;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private e:Lhei;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lhet;->b:I

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhet;->c:Ljava/util/List;

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhet;->d:Z

    .line 47
    iput-object p1, p0, Lhet;->a:Landroid/app/Activity;

    .line 48
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 49
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    .line 136
    iget v0, p0, Lhet;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    .line 137
    iget-boolean v0, p0, Lhet;->d:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 142
    :goto_0
    if-nez v0, :cond_1

    .line 143
    const-string v0, "IntentAccountHandler"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    iget v0, p0, Lhet;->b:I

    iget-object v1, p0, Lhet;->a:Landroid/app/Activity;

    .line 145
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid account state with accountId "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for activity "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :cond_0
    invoke-virtual {p0}, Lhet;->c()V

    .line 149
    :cond_1
    return-void

    .line 137
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 139
    :cond_3
    iget-object v0, p0, Lhet;->e:Lhei;

    iget v1, p0, Lhet;->b:I

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lheg;)Lhee;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lhet;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    return-object p0
.end method

.method public a(Llnh;)Lhet;
    .locals 1

    .prologue
    .line 52
    const-class v0, Lhee;

    invoke-virtual {p1, v0, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 53
    return-object p0
.end method

.method public a(Z)Lhet;
    .locals 0

    .prologue
    .line 62
    iput-boolean p1, p0, Lhet;->d:Z

    .line 63
    return-object p0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lhet;->h()V

    .line 85
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lhei;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhet;->e:Lhei;

    .line 69
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    .line 73
    if-nez p1, :cond_1

    .line 74
    iget-object v0, p0, Lhet;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "account_id"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lhet;->b:I

    .line 75
    invoke-direct {p0}, Lhet;->h()V

    .line 76
    iget-object v0, p0, Lhet;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lheg;

    iget v2, p0, Lhet;->b:I

    if-eq v2, v4, :cond_0

    const/4 v3, 0x3

    :goto_1
    iget v5, p0, Lhet;->b:I

    move v2, v1

    invoke-interface/range {v0 .. v5}, Lheg;->a(ZIIII)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x2

    goto :goto_1

    .line 78
    :cond_1
    const-string v0, "state_account_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lhet;->b:I

    .line 80
    :cond_2
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lhet;->h()V

    .line 90
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 94
    const-string v0, "state_account_id"

    iget v1, p0, Lhet;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 95
    return-void
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lhet;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 157
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lhet;->b:I

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 104
    iget v0, p0, Lhet;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 109
    iget v0, p0, Lhet;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhet;->e:Lhei;

    iget v1, p0, Lhet;->b:I

    .line 110
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lhej;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lhet;->e:Lhei;

    iget v1, p0, Lhet;->b:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    return-object v0
.end method
