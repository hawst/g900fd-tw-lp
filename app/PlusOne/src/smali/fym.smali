.class public final Lfym;
.super Lgbz;
.source "PG"


# instance fields
.field private A:I

.field private B:Lkzt;

.field private y:Lgbf;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfym;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfym;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lfym;->A:I

    .line 48
    new-instance v0, Lgbf;

    invoke-direct {v0, p1, p2, p3}, Lgbf;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfym;->y:Lgbf;

    .line 49
    return-void
.end method


# virtual methods
.method protected a(III)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 79
    iput p2, p0, Lfym;->A:I

    .line 80
    iget-object v0, p0, Lfym;->y:Lgbf;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p3, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 81
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 80
    invoke-virtual {v0, v1, v2}, Lgbf;->measure(II)V

    .line 82
    iget-object v0, p0, Lfym;->y:Lgbf;

    invoke-virtual {v0}, Lgbf;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lfym;->y:Lgbf;

    invoke-virtual {v0}, Lgbf;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Lgbz;->a()V

    .line 105
    const/4 v0, -0x1

    iput v0, p0, Lfym;->A:I

    .line 106
    iget-object v0, p0, Lfym;->y:Lgbf;

    invoke-virtual {v0}, Lgbf;->a()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lfym;->B:Lkzt;

    .line 108
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 1

    .prologue
    .line 53
    const/16 v0, 0x17

    .line 54
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    .line 57
    invoke-static {v0}, Lkzt;->a([B)Lkzt;

    move-result-object v0

    iput-object v0, p0, Lfym;->B:Lkzt;

    .line 59
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/StringBuilder;)V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lfym;->y:Lgbf;

    if-eqz v0, :cond_0

    .line 113
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v1, 0x0

    iget-object v2, p0, Lfym;->y:Lgbf;

    .line 114
    invoke-virtual {v2}, Lgbf;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 113
    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 116
    :cond_0
    return-void
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method protected a_(Landroid/database/Cursor;Llcr;I)V
    .locals 6

    .prologue
    .line 64
    iget-object v0, p0, Lfym;->B:Lkzt;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lfym;->y:Lgbf;

    invoke-virtual {p0, v0}, Lfym;->removeView(Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lfym;->y:Lgbf;

    invoke-virtual {p0, v0}, Lfym;->addView(Landroid/view/View;)V

    .line 67
    iget v0, p0, Lfym;->o:I

    invoke-virtual {p0, p2, v0}, Lfym;->a(Lhuk;I)I

    move-result v3

    .line 68
    iget-object v0, p0, Lfym;->y:Lgbf;

    iget-object v1, p0, Lfym;->B:Lkzt;

    iget-object v2, p0, Lfym;->d:Ljava/lang/String;

    iget-wide v4, p0, Lfym;->f:J

    invoke-virtual/range {v0 .. v5}, Lgbf;->a(Lkzt;Ljava/lang/String;IJ)V

    .line 70
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 92
    invoke-super/range {p0 .. p5}, Lgbz;->onLayout(ZIIII)V

    .line 93
    iget-object v0, p0, Lfym;->y:Lgbf;

    invoke-virtual {v0}, Lgbf;->getMeasuredHeight()I

    move-result v0

    .line 94
    iget v1, p0, Lfym;->A:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-eqz v0, :cond_0

    .line 95
    iget v1, p0, Lfym;->m:I

    iget-object v2, p0, Lfym;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lfym;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    .line 97
    iget-object v2, p0, Lfym;->y:Lgbf;

    iget-object v3, p0, Lfym;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lfym;->A:I

    iget-object v5, p0, Lfym;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v5

    iget v5, p0, Lfym;->A:I

    add-int/2addr v0, v5

    invoke-virtual {v2, v3, v4, v1, v0}, Lgbf;->layout(IIII)V

    .line 100
    :cond_0
    return-void
.end method
