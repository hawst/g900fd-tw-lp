.class public final Lgbg;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private final a:Llct;

.field private b:Lkzt;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/graphics/Bitmap;

.field private h:Ljava/lang/String;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 51
    invoke-virtual {p0}, Lgbg;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 52
    invoke-static {v0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v1

    iput-object v1, p0, Lgbg;->a:Llct;

    .line 54
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lgbg;->setWillNotDraw(Z)V

    .line 56
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lgbg;->f:Landroid/widget/TextView;

    .line 57
    iget-object v1, p0, Lgbg;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lgbg;->addView(Landroid/view/View;)V

    .line 59
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lgbg;->i:Landroid/widget/TextView;

    .line 60
    iget-object v0, p0, Lgbg;->i:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lgbg;->addView(Landroid/view/View;)V

    .line 39
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lgbg;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lgbg;->f:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 161
    iput-object v2, p0, Lgbg;->e:Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lgbg;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lgbg;->i:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 165
    iput-object v2, p0, Lgbg;->h:Ljava/lang/String;

    .line 167
    iput-object v2, p0, Lgbg;->b:Lkzt;

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgbg;->c:Z

    .line 169
    iput-object v2, p0, Lgbg;->d:Ljava/lang/String;

    .line 171
    iput-object v2, p0, Lgbg;->g:Landroid/graphics/Bitmap;

    .line 172
    return-void
.end method

.method public a(Lkzt;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 64
    iput-object p1, p0, Lgbg;->b:Lkzt;

    .line 65
    iget-object v0, p0, Lgbg;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lgbg;->c:Z

    .line 66
    iput-object p2, p0, Lgbg;->d:Ljava/lang/String;

    .line 67
    invoke-virtual {p0}, Lgbg;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 68
    invoke-virtual {p0}, Lgbg;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 70
    iget-object v0, p0, Lgbg;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 71
    iget-object v0, p0, Lgbg;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbg;->e:Ljava/lang/String;

    .line 78
    :goto_1
    iget-boolean v0, p0, Lgbg;->c:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x1a

    .line 82
    :goto_2
    iget-object v1, p0, Lgbg;->f:Landroid/widget/TextView;

    invoke-static {v3, v1, v0}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 83
    iget-object v0, p0, Lgbg;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lgbg;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lgbg;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lgbg;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lgbg;->i:Landroid/widget/TextView;

    const/16 v1, 0xb

    invoke-static {v3, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 88
    iget-boolean v0, p0, Lgbg;->c:Z

    if-eqz v0, :cond_4

    const v0, 0x7f0a0969

    :goto_3
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbg;->h:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lgbg;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lgbg;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lgbg;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lgbg;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 93
    iget-boolean v0, p0, Lgbg;->c:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lgbg;->a:Llct;

    iget-object v0, v0, Llct;->L:Landroid/graphics/Bitmap;

    :goto_4
    iput-object v0, p0, Lgbg;->g:Landroid/graphics/Bitmap;

    .line 97
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 98
    return-void

    :cond_0
    move v0, v2

    .line 65
    goto :goto_0

    .line 73
    :cond_1
    iget-object v0, p0, Lgbg;->b:Lkzt;

    invoke-virtual {v0}, Lkzt;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a096e

    :goto_5
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, Lgbg;->a:Llct;

    iget-object v5, v5, Llct;->a:Lfo;

    iget-object v6, p0, Lgbg;->d:Ljava/lang/String;

    .line 75
    invoke-virtual {v5, v6}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    .line 73
    invoke-virtual {v4, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgbg;->e:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const v0, 0x7f0a096f

    goto :goto_5

    .line 78
    :cond_3
    const/16 v0, 0x22

    goto :goto_2

    .line 88
    :cond_4
    const v0, 0x7f0a0967

    goto :goto_3

    .line 93
    :cond_5
    iget-object v0, p0, Lgbg;->a:Llct;

    iget-object v0, v0, Llct;->K:Landroid/graphics/Bitmap;

    goto :goto_4
.end method

.method public b()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 102
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    .line 103
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lgbg;->h:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 104
    new-array v1, v4, [Ljava/lang/CharSequence;

    iget-object v2, p0, Lgbg;->e:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 105
    invoke-static {v0}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lgbg;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 150
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 151
    iget-object v0, p0, Lgbg;->a:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lgbg;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    iget-object v1, p0, Lgbg;->g:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lgbg;->a:Llct;

    iget v2, v2, Llct;->m:I

    int-to-float v2, v2

    int-to-float v0, v0

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 155
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 134
    iget-object v0, p0, Lgbg;->a:Llct;

    iget v0, v0, Llct;->m:I

    .line 138
    iget-object v1, p0, Lgbg;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lgbg;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    iget-object v3, p0, Lgbg;->f:Landroid/widget/TextView;

    .line 139
    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 138
    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 141
    iget-object v1, p0, Lgbg;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lgbg;->a:Llct;

    iget v2, v2, Llct;->n:I

    add-int/2addr v1, v2

    add-int/2addr v1, v0

    .line 142
    iget-object v2, p0, Lgbg;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    add-int/2addr v0, v2

    .line 144
    iget-object v2, p0, Lgbg;->i:Landroid/widget/TextView;

    iget-object v3, p0, Lgbg;->i:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lgbg;->i:Landroid/widget/TextView;

    .line 145
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 144
    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 146
    return-void
.end method

.method public onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 110
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 112
    iget-object v1, p0, Lgbg;->a:Llct;

    iget v1, v1, Llct;->m:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 113
    iget-object v2, p0, Lgbg;->a:Llct;

    iget v2, v2, Llct;->m:I

    .line 114
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 116
    iget-object v4, p0, Lgbg;->f:Landroid/widget/TextView;

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v5, v3}, Landroid/widget/TextView;->measure(II)V

    .line 119
    iget-object v4, p0, Lgbg;->f:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    iget-object v5, p0, Lgbg;->a:Llct;

    iget v5, v5, Llct;->m:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    .line 121
    iget-object v4, p0, Lgbg;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v1, v4

    iget-object v4, p0, Lgbg;->a:Llct;

    iget v4, v4, Llct;->n:I

    sub-int/2addr v1, v4

    .line 123
    iget-object v4, p0, Lgbg;->i:Landroid/widget/TextView;

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v4, v1, v3}, Landroid/widget/TextView;->measure(II)V

    .line 126
    iget-object v1, p0, Lgbg;->a:Llct;

    iget v1, v1, Llct;->m:I

    iget-object v3, p0, Lgbg;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget-object v4, p0, Lgbg;->i:Landroid/widget/TextView;

    .line 127
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    .line 126
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    .line 129
    invoke-virtual {p0, v0, v1}, Lgbg;->setMeasuredDimension(II)V

    .line 130
    return-void
.end method
