.class public final Ldjq;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmay;",
        "Lmaz;",
        ">;"
    }
.end annotation


# static fields
.field private static a:[I

.field private static b:[I

.field private static p:[I

.field private static q:[I

.field private static r:[I

.field private static s:[I

.field private static t:I


# instance fields
.field private A:[B

.field private B:[B

.field private C:J

.field private D:J

.field private final u:J

.field private final v:[B

.field private final w:Ldsl;

.field private final x:I

.field private final y:I

.field private z:[Llui;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 70
    new-array v0, v1, [I

    aput v1, v0, v2

    sput-object v0, Ldjq;->a:[I

    .line 72
    new-array v0, v1, [I

    aput v4, v0, v2

    sput-object v0, Ldjq;->b:[I

    .line 75
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Ldjq;->p:[I

    .line 84
    new-array v0, v1, [I

    aput v3, v0, v2

    sput-object v0, Ldjq;->q:[I

    .line 90
    new-array v0, v1, [I

    aput v4, v0, v2

    sput-object v0, Ldjq;->r:[I

    .line 93
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Ldjq;->s:[I

    .line 97
    const/4 v0, -0x1

    sput v0, Ldjq;->t:I

    return-void

    .line 75
    :array_0
    .array-data 4
        0x1
        0x2
        0x4
    .end array-data

    .line 93
    :array_1
    .array-data 4
        0x2
        0x3
        0x4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;IJLdsl;II)V
    .locals 10

    .prologue
    .line 122
    const-string v5, "fetchnotifications"

    new-instance v6, Lmay;

    invoke-direct {v6}, Lmay;-><init>()V

    new-instance v7, Lmaz;

    invoke-direct {v7}, Lmaz;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v1 .. v7}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 125
    iput-wide p4, p0, Ldjq;->u:J

    .line 126
    const/4 v1, 0x0

    iput-object v1, p0, Ldjq;->v:[B

    .line 127
    move-object/from16 v0, p6

    iput-object v0, p0, Ldjq;->w:Ldsl;

    .line 128
    move/from16 v0, p7

    iput v0, p0, Ldjq;->x:I

    .line 129
    move/from16 v0, p8

    iput v0, p0, Ldjq;->y:I

    .line 130
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;I[BLdsl;II)V
    .locals 7

    .prologue
    .line 147
    const-string v4, "fetchnotifications"

    new-instance v5, Lmay;

    invoke-direct {v5}, Lmay;-><init>()V

    new-instance v6, Lmaz;

    invoke-direct {v6}, Lmaz;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 151
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldjq;->u:J

    .line 152
    iput-object p4, p0, Ldjq;->v:[B

    .line 153
    iput-object p5, p0, Ldjq;->w:Ldsl;

    .line 154
    iput p6, p0, Ldjq;->x:I

    .line 155
    iput p7, p0, Ldjq;->y:I

    .line 156
    return-void
.end method


# virtual methods
.method protected a(Lmay;)V
    .locals 6

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x1

    .line 238
    new-instance v0, Lmyf;

    invoke-direct {v0}, Lmyf;-><init>()V

    iput-object v0, p1, Lmay;->a:Lmyf;

    .line 239
    iget-object v3, p1, Lmay;->a:Lmyf;

    .line 242
    new-instance v4, Lmyb;

    invoke-direct {v4}, Lmyb;-><init>()V

    .line 243
    iget-object v0, p0, Ldjq;->w:Ldsl;

    sget-object v5, Ldjr;->a:[I

    invoke-virtual {v0}, Ldsl;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    iput v0, v4, Lmyb;->a:I

    .line 244
    iput-object v4, v3, Lmyf;->c:Lmyb;

    .line 247
    new-instance v0, Lmyk;

    invoke-direct {v0}, Lmyk;-><init>()V

    .line 249
    new-instance v4, Lmxz;

    invoke-direct {v4}, Lmxz;-><init>()V

    .line 250
    sget v5, Ldjq;->t:I

    if-gez v5, :cond_0

    iget-object v5, p0, Ldjq;->f:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    sput v5, Ldjq;->t:I

    :cond_0
    sget v5, Ldjq;->t:I

    sparse-switch v5, :sswitch_data_0

    const/4 v1, 0x0

    :goto_1
    :sswitch_0
    iput v1, v4, Lmxz;->a:I

    .line 251
    iput-object v4, v0, Lmyk;->b:Lmxz;

    .line 252
    const-string v1, "android_gplus"

    iput-object v1, v0, Lmyk;->a:Ljava/lang/String;

    .line 253
    iput-object v0, v3, Lmyf;->a:Lmyk;

    .line 256
    iget-object v0, p0, Ldjq;->v:[B

    if-nez v0, :cond_2

    .line 257
    new-instance v0, Lmye;

    invoke-direct {v0}, Lmye;-><init>()V

    iput-object v0, v3, Lmyf;->b:Lmye;

    .line 258
    iget-object v0, v3, Lmyf;->b:Lmye;

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lmye;->b:Ljava/lang/Integer;

    .line 259
    iget-wide v0, p0, Ldjq;->u:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 260
    iget-object v0, v3, Lmyf;->b:Lmye;

    iget-wide v4, p0, Ldjq;->u:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lmye;->a:Ljava/lang/Long;

    .line 262
    :cond_1
    iget-object v0, v3, Lmyf;->b:Lmye;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lmye;->f:Ljava/lang/Boolean;

    .line 263
    iget-object v0, v3, Lmyf;->b:Lmye;

    const-string v1, "GPLUS_APP_V3"

    iput-object v1, v0, Lmye;->d:Ljava/lang/String;

    .line 264
    iget-object v1, v3, Lmyf;->b:Lmye;

    iget v0, p0, Ldjq;->y:I

    sparse-switch v0, :sswitch_data_1

    sget-object v0, Ldjq;->q:[I

    :goto_2
    iput-object v0, v1, Lmye;->e:[I

    .line 265
    iget-object v1, v3, Lmyf;->b:Lmye;

    iget v0, p0, Ldjq;->x:I

    sparse-switch v0, :sswitch_data_2

    sget-object v0, Ldjq;->a:[I

    :goto_3
    iput-object v0, v1, Lmye;->c:[I

    .line 274
    :goto_4
    return-void

    .line 243
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_1
    move v0, v1

    goto :goto_0

    .line 250
    :sswitch_1
    const/4 v1, 0x4

    goto :goto_1

    :sswitch_2
    const/4 v1, 0x5

    goto :goto_1

    :sswitch_3
    const/16 v1, 0x9

    goto :goto_1

    .line 264
    :sswitch_4
    sget-object v0, Ldjq;->s:[I

    goto :goto_2

    :sswitch_5
    sget-object v0, Ldjq;->r:[I

    goto :goto_2

    .line 265
    :sswitch_6
    sget-object v0, Ldjq;->p:[I

    goto :goto_3

    :sswitch_7
    sget-object v0, Ldjq;->b:[I

    goto :goto_3

    .line 268
    :cond_2
    :try_start_0
    new-instance v0, Lmye;

    invoke-direct {v0}, Lmye;-><init>()V

    iget-object v1, p0, Ldjq;->v:[B

    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmye;

    iput-object v0, v3, Lmyf;->b:Lmye;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 271
    :catch_0
    move-exception v0

    const-string v0, "HttpOperation"

    invoke-virtual {p0, v0}, Ldjq;->d(Ljava/lang/String;)V

    goto :goto_4

    .line 243
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 250
    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xa0 -> :sswitch_0
        0xf0 -> :sswitch_1
        0x140 -> :sswitch_2
        0x1e0 -> :sswitch_3
        0x280 -> :sswitch_3
    .end sparse-switch

    .line 264
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_5
        0xe -> :sswitch_4
    .end sparse-switch

    .line 265
    :sswitch_data_2
    .sparse-switch
        0x2 -> :sswitch_7
        0xf -> :sswitch_6
    .end sparse-switch
.end method

.method protected a(Lmaz;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 278
    iget-object v1, p1, Lmaz;->a:Lmyt;

    .line 279
    if-eqz v1, :cond_7

    .line 280
    iget-object v0, v1, Lmyt;->f:Lluo;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lluo;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Ldjq;->f:Landroid/content/Context;

    iget-object v0, v0, Lluo;->a:Ljava/lang/String;

    invoke-static {v2, v0}, Ldsf;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 282
    :cond_0
    iget-object v0, v1, Lmyt;->e:Lmyx;

    if-eqz v0, :cond_1

    .line 283
    iget-object v0, v1, Lmyt;->e:Lmyx;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    iput-object v0, p0, Ldjq;->B:[B

    .line 286
    :cond_1
    iget-object v2, p0, Ldjq;->f:Landroid/content/Context;

    iget v3, p0, Ldjq;->c:I

    iget-object v4, v1, Lmyt;->a:[Llui;

    iget-object v5, p0, Ldjq;->w:Ldsl;

    const-class v0, Lieh;

    invoke-static {v2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v6, Ljlq;->a:Lief;

    invoke-interface {v0, v6, v3}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-eqz v4, :cond_4

    array-length v0, v4

    if-lez v0, :cond_4

    const/4 v0, 0x0

    :goto_0
    array-length v7, v4

    if-ge v0, v7, :cond_4

    aget-object v7, v4, v0

    iget-object v8, v7, Llui;->k:Llux;

    if-nez v8, :cond_2

    const-string v8, "df"

    iget-object v9, v7, Llui;->h:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_2
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "NotificationsAckUtil"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v7, 0x2a

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Reporting notifications, size: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llui;

    invoke-virtual {v0}, Llui;->toString()Ljava/lang/String;

    goto :goto_1

    :cond_5
    const-class v0, Lhoc;

    invoke-static {v2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    new-instance v0, Ldpm;

    invoke-direct {v0, v2, v3, v6, v5}, Ldpm;-><init>(Landroid/content/Context;ILjava/util/ArrayList;Ldsl;)V

    invoke-static {v2, v0}, Lhoc;->a(Landroid/content/Context;Lhny;)V

    .line 289
    :cond_6
    iget-object v0, p0, Ldjq;->f:Landroid/content/Context;

    iget v2, p0, Ldjq;->c:I

    iget-object v3, v1, Lmyt;->a:[Llui;

    invoke-static {v0, v2, v3}, Lfuv;->a(Landroid/content/Context;I[Llui;)[Llui;

    move-result-object v0

    iput-object v0, p0, Ldjq;->z:[Llui;

    .line 291
    iget-object v0, v1, Lmyt;->c:Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    iput-wide v2, p0, Ldjq;->C:J

    .line 292
    iget-object v0, v1, Lmyt;->d:Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v2

    iput-wide v2, p0, Ldjq;->D:J

    .line 294
    iget-object v0, v1, Lmyt;->b:Lmye;

    if-eqz v0, :cond_8

    .line 295
    iget-object v0, v1, Lmyt;->b:Lmye;

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    iput-object v0, p0, Ldjq;->A:[B

    .line 297
    iget-wide v0, p0, Ldjq;->u:J

    cmp-long v0, v0, v10

    if-lez v0, :cond_7

    .line 300
    iget-object v0, p0, Ldjq;->f:Landroid/content/Context;

    iget v1, p0, Ldjq;->c:I

    invoke-static {v0, v1}, Ldsf;->d(Landroid/content/Context;I)V

    .line 301
    const-string v0, "EsNotificationData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 315
    :cond_7
    :goto_2
    return-void

    .line 307
    :cond_8
    iget-wide v0, p0, Ldjq;->u:J

    cmp-long v0, v0, v10

    if-lez v0, :cond_9

    .line 310
    sget-object v0, Ldsf;->a:[B

    iput-object v0, p0, Ldjq;->A:[B

    goto :goto_2

    .line 314
    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Ldjq;->A:[B

    goto :goto_2
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lmay;

    invoke-virtual {p0, p1}, Ldjq;->a(Lmay;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 28
    check-cast p1, Lmaz;

    invoke-virtual {p0, p1}, Ldjq;->a(Lmaz;)V

    return-void
.end method

.method public b()[Llui;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Ldjq;->z:[Llui;

    return-object v0
.end method

.method public c()[B
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Ldjq;->B:[B

    return-object v0
.end method

.method public d()[B
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Ldjq;->A:[B

    return-object v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 338
    iget-wide v0, p0, Ldjq;->C:J

    return-wide v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 342
    iget-wide v0, p0, Ldjq;->D:J

    return-wide v0
.end method
