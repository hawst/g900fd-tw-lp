.class public final Lodj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lodj;


# instance fields
.field public b:I

.field public c:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    new-array v0, v0, [Lodj;

    sput-object v0, Lodj;->a:[Lodj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Loxq;-><init>()V

    .line 111
    const/high16 v0, -0x80000000

    iput v0, p0, Lodj;->b:I

    .line 114
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lodj;->c:[Ljava/lang/String;

    .line 101
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 133
    .line 134
    iget v0, p0, Lodj;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_2

    .line 135
    const/4 v0, 0x1

    iget v2, p0, Lodj;->b:I

    .line 136
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 138
    :goto_0
    iget-object v2, p0, Lodj;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lodj;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 140
    iget-object v3, p0, Lodj;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 142
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 140
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 144
    :cond_0
    add-int/2addr v0, v2

    .line 145
    iget-object v1, p0, Lodj;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 147
    :cond_1
    iget-object v1, p0, Lodj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    iput v0, p0, Lodj;->ai:I

    .line 149
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lodj;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 157
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 158
    sparse-switch v0, :sswitch_data_0

    .line 162
    iget-object v1, p0, Lodj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 163
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lodj;->ah:Ljava/util/List;

    .line 166
    :cond_1
    iget-object v1, p0, Lodj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    :sswitch_0
    return-object p0

    .line 173
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 174
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 178
    :cond_2
    iput v0, p0, Lodj;->b:I

    goto :goto_0

    .line 180
    :cond_3
    iput v3, p0, Lodj;->b:I

    goto :goto_0

    .line 185
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 186
    iget-object v0, p0, Lodj;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 187
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 188
    iget-object v2, p0, Lodj;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 189
    iput-object v1, p0, Lodj;->c:[Ljava/lang/String;

    .line 190
    :goto_1
    iget-object v1, p0, Lodj;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 191
    iget-object v1, p0, Lodj;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 192
    invoke-virtual {p1}, Loxn;->a()I

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 195
    :cond_4
    iget-object v1, p0, Lodj;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 158
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 119
    iget v0, p0, Lodj;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 120
    const/4 v0, 0x1

    iget v1, p0, Lodj;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 122
    :cond_0
    iget-object v0, p0, Lodj;->c:[Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 123
    iget-object v1, p0, Lodj;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 124
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_1
    iget-object v0, p0, Lodj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 129
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lodj;->a(Loxn;)Lodj;

    move-result-object v0

    return-object v0
.end method
