.class public final Lojl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Integer;

.field public d:Loiz;

.field private e:[I

.field private f:[Ljava/lang/Long;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 828
    invoke-direct {p0}, Loxq;-><init>()V

    .line 835
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lojl;->e:[I

    .line 840
    const/4 v0, 0x0

    iput-object v0, p0, Lojl;->d:Loiz;

    .line 843
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lojl;->f:[Ljava/lang/Long;

    .line 828
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 881
    .line 882
    iget-object v0, p0, Lojl;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 883
    const/4 v0, 0x1

    iget-object v2, p0, Lojl;->a:Ljava/lang/Boolean;

    .line 884
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 886
    :goto_0
    iget-object v2, p0, Lojl;->e:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lojl;->e:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 888
    iget-object v4, p0, Lojl;->e:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget v6, v4, v2

    .line 890
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 888
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 892
    :cond_0
    add-int/2addr v0, v3

    .line 893
    iget-object v2, p0, Lojl;->e:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 895
    :cond_1
    iget-object v2, p0, Lojl;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 896
    const/4 v2, 0x3

    iget-object v3, p0, Lojl;->c:Ljava/lang/Integer;

    .line 897
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 899
    :cond_2
    iget-object v2, p0, Lojl;->d:Loiz;

    if-eqz v2, :cond_3

    .line 900
    const/4 v2, 0x4

    iget-object v3, p0, Lojl;->d:Loiz;

    .line 901
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 903
    :cond_3
    iget-object v2, p0, Lojl;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 904
    const/4 v2, 0x5

    iget-object v3, p0, Lojl;->b:Ljava/lang/Boolean;

    .line 905
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 907
    :cond_4
    iget-object v2, p0, Lojl;->f:[Ljava/lang/Long;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lojl;->f:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 909
    iget-object v3, p0, Lojl;->f:[Ljava/lang/Long;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 911
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 909
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 913
    :cond_5
    add-int/2addr v0, v2

    .line 914
    iget-object v1, p0, Lojl;->f:[Ljava/lang/Long;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 916
    :cond_6
    iget-object v1, p0, Lojl;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 917
    const/4 v1, 0x7

    iget-object v2, p0, Lojl;->g:Ljava/lang/Boolean;

    .line 918
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 920
    :cond_7
    iget-object v1, p0, Lojl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 921
    iput v0, p0, Lojl;->ai:I

    .line 922
    return v0

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lojl;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 930
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 931
    sparse-switch v0, :sswitch_data_0

    .line 935
    iget-object v1, p0, Lojl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 936
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lojl;->ah:Ljava/util/List;

    .line 939
    :cond_1
    iget-object v1, p0, Lojl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 941
    :sswitch_0
    return-object p0

    .line 946
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lojl;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 950
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 951
    iget-object v0, p0, Lojl;->e:[I

    array-length v0, v0

    .line 952
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 953
    iget-object v2, p0, Lojl;->e:[I

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 954
    iput-object v1, p0, Lojl;->e:[I

    .line 955
    :goto_1
    iget-object v1, p0, Lojl;->e:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 956
    iget-object v1, p0, Lojl;->e:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 957
    invoke-virtual {p1}, Loxn;->a()I

    .line 955
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 960
    :cond_2
    iget-object v1, p0, Lojl;->e:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 964
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lojl;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 968
    :sswitch_4
    iget-object v0, p0, Lojl;->d:Loiz;

    if-nez v0, :cond_3

    .line 969
    new-instance v0, Loiz;

    invoke-direct {v0}, Loiz;-><init>()V

    iput-object v0, p0, Lojl;->d:Loiz;

    .line 971
    :cond_3
    iget-object v0, p0, Lojl;->d:Loiz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 975
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lojl;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 979
    :sswitch_6
    const/16 v0, 0x30

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 980
    iget-object v0, p0, Lojl;->f:[Ljava/lang/Long;

    array-length v0, v0

    .line 981
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    .line 982
    iget-object v2, p0, Lojl;->f:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 983
    iput-object v1, p0, Lojl;->f:[Ljava/lang/Long;

    .line 984
    :goto_2
    iget-object v1, p0, Lojl;->f:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 985
    iget-object v1, p0, Lojl;->f:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 986
    invoke-virtual {p1}, Loxn;->a()I

    .line 984
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 989
    :cond_4
    iget-object v1, p0, Lojl;->f:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 993
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lojl;->g:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 931
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 850
    iget-object v1, p0, Lojl;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 851
    const/4 v1, 0x1

    iget-object v2, p0, Lojl;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 853
    :cond_0
    iget-object v1, p0, Lojl;->e:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lojl;->e:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 854
    iget-object v2, p0, Lojl;->e:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v4, v2, v1

    .line 855
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 854
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 858
    :cond_1
    iget-object v1, p0, Lojl;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 859
    const/4 v1, 0x3

    iget-object v2, p0, Lojl;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 861
    :cond_2
    iget-object v1, p0, Lojl;->d:Loiz;

    if-eqz v1, :cond_3

    .line 862
    const/4 v1, 0x4

    iget-object v2, p0, Lojl;->d:Loiz;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 864
    :cond_3
    iget-object v1, p0, Lojl;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 865
    const/4 v1, 0x5

    iget-object v2, p0, Lojl;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 867
    :cond_4
    iget-object v1, p0, Lojl;->f:[Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 868
    iget-object v1, p0, Lojl;->f:[Ljava/lang/Long;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 869
    const/4 v4, 0x6

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v4, v6, v7}, Loxo;->b(IJ)V

    .line 868
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 872
    :cond_5
    iget-object v0, p0, Lojl;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 873
    const/4 v0, 0x7

    iget-object v1, p0, Lojl;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 875
    :cond_6
    iget-object v0, p0, Lojl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 877
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 824
    invoke-virtual {p0, p1}, Lojl;->a(Loxn;)Lojl;

    move-result-object v0

    return-object v0
.end method
