.class public final Ljts;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljpv;",
        ">;>;"
    }
.end annotation


# instance fields
.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljpv;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljpr;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 31
    const-class v0, Ljpr;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpr;

    iput-object v0, p0, Ljts;->c:Ljpr;

    .line 32
    iput p2, p0, Ljts;->d:I

    .line 33
    return-void
.end method


# virtual methods
.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Ljts;->l()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljpv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v6, Landroid/os/ConditionVariable;

    invoke-direct {v6}, Landroid/os/ConditionVariable;-><init>()V

    .line 38
    iget-object v0, p0, Ljts;->c:Ljpr;

    new-instance v1, Ljtt;

    invoke-direct {v1, p0, v6}, Ljtt;-><init>(Ljts;Landroid/os/ConditionVariable;)V

    iget v2, p0, Ljts;->d:I

    const/4 v3, 0x2

    const v4, 0x7fffffff

    sget-object v5, Ljpx;->a:Ljqc;

    invoke-interface/range {v0 .. v5}, Ljpr;->a(Ljoz;IIILjqc;)V

    .line 50
    invoke-virtual {v6}, Landroid/os/ConditionVariable;->block()V

    .line 52
    invoke-virtual {p0}, Ljts;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljts;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    :cond_0
    const/4 v0, 0x0

    .line 62
    :goto_0
    return-object v0

    .line 56
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 57
    iget-object v0, p0, Ljts;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 58
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 59
    iget-object v0, p0, Ljts;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljpv;

    .line 60
    invoke-interface {v0}, Ljpv;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 62
    goto :goto_0
.end method
