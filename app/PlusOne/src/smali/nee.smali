.class public final Lnee;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3395
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3400
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnee;->b:[Ljava/lang/String;

    .line 3403
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnee;->c:[Ljava/lang/String;

    .line 3395
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 3437
    .line 3438
    iget-object v0, p0, Lnee;->a:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 3439
    const/4 v0, 0x1

    iget-object v2, p0, Lnee;->a:Ljava/lang/String;

    .line 3440
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3442
    :goto_0
    iget-object v2, p0, Lnee;->b:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lnee;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 3444
    iget-object v4, p0, Lnee;->b:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 3446
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 3444
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3448
    :cond_0
    add-int/2addr v0, v3

    .line 3449
    iget-object v2, p0, Lnee;->b:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3451
    :cond_1
    iget-object v2, p0, Lnee;->c:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lnee;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 3453
    iget-object v3, p0, Lnee;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 3455
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 3453
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3457
    :cond_2
    add-int/2addr v0, v2

    .line 3458
    iget-object v1, p0, Lnee;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3460
    :cond_3
    iget-object v1, p0, Lnee;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3461
    const/4 v1, 0x4

    iget-object v2, p0, Lnee;->d:Ljava/lang/String;

    .line 3462
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3464
    :cond_4
    iget-object v1, p0, Lnee;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 3465
    const/4 v1, 0x5

    iget-object v2, p0, Lnee;->e:Ljava/lang/String;

    .line 3466
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3468
    :cond_5
    iget-object v1, p0, Lnee;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3469
    iput v0, p0, Lnee;->ai:I

    .line 3470
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnee;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3478
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3479
    sparse-switch v0, :sswitch_data_0

    .line 3483
    iget-object v1, p0, Lnee;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3484
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnee;->ah:Ljava/util/List;

    .line 3487
    :cond_1
    iget-object v1, p0, Lnee;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3489
    :sswitch_0
    return-object p0

    .line 3494
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnee;->a:Ljava/lang/String;

    goto :goto_0

    .line 3498
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 3499
    iget-object v0, p0, Lnee;->b:[Ljava/lang/String;

    array-length v0, v0

    .line 3500
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 3501
    iget-object v2, p0, Lnee;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3502
    iput-object v1, p0, Lnee;->b:[Ljava/lang/String;

    .line 3503
    :goto_1
    iget-object v1, p0, Lnee;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 3504
    iget-object v1, p0, Lnee;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3505
    invoke-virtual {p1}, Loxn;->a()I

    .line 3503
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3508
    :cond_2
    iget-object v1, p0, Lnee;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 3512
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 3513
    iget-object v0, p0, Lnee;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 3514
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 3515
    iget-object v2, p0, Lnee;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3516
    iput-object v1, p0, Lnee;->c:[Ljava/lang/String;

    .line 3517
    :goto_2
    iget-object v1, p0, Lnee;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 3518
    iget-object v1, p0, Lnee;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3519
    invoke-virtual {p1}, Loxn;->a()I

    .line 3517
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3522
    :cond_3
    iget-object v1, p0, Lnee;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 3526
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnee;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 3530
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnee;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 3479
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3412
    iget-object v1, p0, Lnee;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3413
    const/4 v1, 0x1

    iget-object v2, p0, Lnee;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 3415
    :cond_0
    iget-object v1, p0, Lnee;->b:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 3416
    iget-object v2, p0, Lnee;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 3417
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 3416
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3420
    :cond_1
    iget-object v1, p0, Lnee;->c:[Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 3421
    iget-object v1, p0, Lnee;->c:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 3422
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 3421
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3425
    :cond_2
    iget-object v0, p0, Lnee;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 3426
    const/4 v0, 0x4

    iget-object v1, p0, Lnee;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3428
    :cond_3
    iget-object v0, p0, Lnee;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3429
    const/4 v0, 0x5

    iget-object v1, p0, Lnee;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3431
    :cond_4
    iget-object v0, p0, Lnee;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3433
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3391
    invoke-virtual {p0, p1}, Lnee;->a(Loxn;)Lnee;

    move-result-object v0

    return-object v0
.end method
