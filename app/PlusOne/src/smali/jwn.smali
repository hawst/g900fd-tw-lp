.class public final Ljwn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static a:Ljwp;

.field private static final b:J

.field private static final c:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljwp;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 41
    const/4 v0, 0x0

    sput-object v0, Ljwn;->a:Ljwp;

    .line 45
    const-string v0, "picasasync.metrics.time"

    const-wide/16 v2, 0x64

    invoke-static {v0, v2, v3}, Ljxf;->a(Ljava/lang/String;J)J

    move-result-wide v0

    sput-wide v0, Ljwn;->b:J

    .line 48
    new-instance v0, Ljwo;

    invoke-direct {v0}, Ljwo;-><init>()V

    sput-object v0, Ljwn;->c:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 152
    sget-object v0, Ljwn;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 153
    invoke-static {p0}, Ljwp;->a(Ljava/lang/String;)Ljwp;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public static a(I)V
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-static {p0, v0}, Ljwn;->a(ILjava/lang/String;)V

    .line 163
    return-void
.end method

.method public static a(ILjava/lang/String;)V
    .locals 13

    .prologue
    .line 174
    sget-object v0, Ljwn;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 175
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt p0, v1, :cond_0

    if-gtz p0, :cond_1

    .line 176
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "size: %s, id: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 177
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 180
    :cond_1
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p0, v1, :cond_4

    .line 181
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljwp;

    .line 182
    const-string v2, "WARNING: unclosed metrics: "

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 183
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 185
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljwp;

    invoke-virtual {v2, v1}, Ljwp;->b(Ljwp;)V

    .line 187
    :cond_2
    invoke-virtual {v1}, Ljwp;->a()V

    goto :goto_0

    .line 182
    :cond_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 190
    :cond_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljwp;

    .line 191
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v1, Ljwp;->e:J

    .line 193
    const-string v2, "MetricsUtils"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-wide v2, Ljwn;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_5

    iget-wide v2, v1, Ljwp;->e:J

    iget-wide v4, v1, Ljwp;->d:J

    sub-long/2addr v2, v4

    sget-wide v4, Ljwn;->b:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_5

    .line 195
    invoke-virtual {v1, p1}, Ljwp;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 197
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    .line 199
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwp;

    invoke-virtual {v0, v1}, Ljwp;->b(Ljwp;)V

    .line 202
    :cond_6
    if-eqz p1, :cond_7

    iget v0, v1, Ljwp;->g:I

    if-lez v0, :cond_7

    .line 203
    iget-wide v2, v1, Ljwp;->e:J

    iget-wide v4, v1, Ljwp;->d:J

    sub-long/2addr v2, v4

    iget-wide v4, v1, Ljwp;->f:J

    iget v0, v1, Ljwp;->g:I

    iget-wide v6, v1, Ljwp;->c:J

    iget-wide v8, v1, Ljwp;->b:J

    sget-object v10, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->a:Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;

    if-eqz v10, :cond_7

    sget-object v10, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->b:Ljava/lang/Class;

    if-nez v10, :cond_8

    .line 210
    :cond_7
    :goto_2
    invoke-virtual {v1}, Ljwp;->a()V

    .line 211
    return-void

    .line 203
    :cond_8
    sget-object v10, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->a:Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;

    iget-object v10, v10, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->c:Landroid/content/Context;

    new-instance v11, Landroid/content/Intent;

    sget-object v12, Lcom/google/android/libraries/social/picasalegacy/PicasaStoreFacade;->b:Ljava/lang/Class;

    invoke-direct {v11, v10, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v12, "com.google.android.picasastore.op_report"

    invoke-virtual {v11, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v12, "op_name"

    invoke-virtual {v11, v12, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v12, "total_time"

    invoke-virtual {v11, v12, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "net_duration"

    invoke-virtual {v11, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "transaction_count"

    invoke-virtual {v11, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "sent_bytes"

    invoke-virtual {v11, v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v0, "received_bytes"

    invoke-virtual {v11, v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {v10, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_2
.end method

.method public static a(J)V
    .locals 4

    .prologue
    .line 232
    sget-object v0, Ljwn;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 233
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 234
    if-lez v1, :cond_0

    .line 235
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwp;

    .line 236
    iget-wide v2, v0, Ljwp;->c:J

    add-long/2addr v2, p0

    iput-wide v2, v0, Ljwp;->c:J

    .line 238
    :cond_0
    return-void
.end method

.method public static b(I)V
    .locals 2

    .prologue
    .line 223
    sget-object v0, Ljwn;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 224
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 225
    if-lez v1, :cond_0

    .line 226
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwp;

    .line 227
    iget v1, v0, Ljwp;->a:I

    add-int/2addr v1, p0

    iput v1, v0, Ljwp;->a:I

    .line 229
    :cond_0
    return-void
.end method

.method public static b(J)V
    .locals 4

    .prologue
    .line 241
    sget-object v0, Ljwn;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 242
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 243
    if-lez v1, :cond_0

    .line 244
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwp;

    .line 245
    iget-wide v2, v0, Ljwp;->b:J

    add-long/2addr v2, p0

    iput-wide v2, v0, Ljwp;->b:J

    .line 247
    :cond_0
    return-void
.end method

.method public static c(J)V
    .locals 4

    .prologue
    .line 272
    sget-object v0, Ljwn;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 273
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 274
    if-lez v1, :cond_0

    .line 275
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljwp;

    .line 276
    iget-wide v2, v0, Ljwp;->f:J

    add-long/2addr v2, p0

    iput-wide v2, v0, Ljwp;->f:J

    .line 277
    iget v1, v0, Ljwp;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Ljwp;->g:I

    .line 279
    :cond_0
    return-void
.end method
