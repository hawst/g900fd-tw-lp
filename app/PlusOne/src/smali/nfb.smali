.class public final Lnfb;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lnym;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Loxq;-><init>()V

    .line 104
    sget-object v0, Lnym;->a:[Lnym;

    iput-object v0, p0, Lnfb;->a:[Lnym;

    .line 101
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 122
    .line 123
    iget-object v1, p0, Lnfb;->a:[Lnym;

    if-eqz v1, :cond_1

    .line 124
    iget-object v2, p0, Lnfb;->a:[Lnym;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 125
    if-eqz v4, :cond_0

    .line 126
    const/4 v5, 0x1

    .line 127
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 124
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 131
    :cond_1
    iget-object v1, p0, Lnfb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    iput v0, p0, Lnfb;->ai:I

    .line 133
    return v0
.end method

.method public a(Loxn;)Lnfb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 141
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 142
    sparse-switch v0, :sswitch_data_0

    .line 146
    iget-object v2, p0, Lnfb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 147
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnfb;->ah:Ljava/util/List;

    .line 150
    :cond_1
    iget-object v2, p0, Lnfb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    :sswitch_0
    return-object p0

    .line 157
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 158
    iget-object v0, p0, Lnfb;->a:[Lnym;

    if-nez v0, :cond_3

    move v0, v1

    .line 159
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnym;

    .line 160
    iget-object v3, p0, Lnfb;->a:[Lnym;

    if-eqz v3, :cond_2

    .line 161
    iget-object v3, p0, Lnfb;->a:[Lnym;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 163
    :cond_2
    iput-object v2, p0, Lnfb;->a:[Lnym;

    .line 164
    :goto_2
    iget-object v2, p0, Lnfb;->a:[Lnym;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 165
    iget-object v2, p0, Lnfb;->a:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 166
    iget-object v2, p0, Lnfb;->a:[Lnym;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 167
    invoke-virtual {p1}, Loxn;->a()I

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 158
    :cond_3
    iget-object v0, p0, Lnfb;->a:[Lnym;

    array-length v0, v0

    goto :goto_1

    .line 170
    :cond_4
    iget-object v2, p0, Lnfb;->a:[Lnym;

    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    aput-object v3, v2, v0

    .line 171
    iget-object v2, p0, Lnfb;->a:[Lnym;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 142
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 109
    iget-object v0, p0, Lnfb;->a:[Lnym;

    if-eqz v0, :cond_1

    .line 110
    iget-object v1, p0, Lnfb;->a:[Lnym;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 111
    if-eqz v3, :cond_0

    .line 112
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 110
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    :cond_1
    iget-object v0, p0, Lnfb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 118
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lnfb;->a(Loxn;)Lnfb;

    move-result-object v0

    return-object v0
.end method
