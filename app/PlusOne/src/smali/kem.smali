.class final Lkem;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lked;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final a:Lkef;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lken;

    invoke-direct {v0}, Lken;-><init>()V

    sput-object v0, Lkem;->a:Lkef;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method


# virtual methods
.method public a(II)Lkee;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lkee;

    sget-object v1, Lkem;->a:Lkef;

    invoke-direct {v0, p1, p2, v1}, Lkee;-><init>(IILkef;)V

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)Lkee;
    .locals 7

    .prologue
    .line 33
    new-instance v1, Lkee;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 34
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v0

    int-to-long v4, v0

    sget-object v6, Lkem;->a:Lkef;

    invoke-direct/range {v1 .. v6}, Lkee;-><init>(IIJLkef;)V

    return-object v1
.end method

.method public a(Lkee;Ljava/util/SortedSet;Lkeg;)Lkee;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkee;",
            "Ljava/util/SortedSet",
            "<",
            "Lkee;",
            ">;",
            "Lkeg;",
            ")",
            "Lkee;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-interface {p2}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkee;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lkee;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 46
    iget v0, p1, Lkee;->b:I

    iget v1, p1, Lkee;->a:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p2, v0, v1, v2}, Landroid/graphics/Bitmap;->reconfigure(IILandroid/graphics/Bitmap$Config;)V

    .line 47
    return-void
.end method
