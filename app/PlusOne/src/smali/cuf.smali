.class public final Lcuf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcne;


# instance fields
.field private final a:Lctz;

.field private final b:Landroid/app/Activity;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lctz;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcuf;->b:Landroid/app/Activity;

    .line 70
    iput-object p2, p0, Lcuf;->a:Lctz;

    .line 71
    iput-object p3, p0, Lcuf;->c:Ljava/lang/String;

    .line 72
    iput p4, p0, Lcuf;->d:I

    .line 73
    iput p5, p0, Lcuf;->e:I

    .line 74
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcuf;->a:Lctz;

    invoke-virtual {v0}, Lctz;->c()V

    .line 116
    return-void
.end method

.method public a(Lxn;)V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method public a(Lxn;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lcuf;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcuf;->c:Ljava/lang/String;

    .line 80
    :goto_0
    const v1, 0x7f1000ba

    invoke-interface {p2, v2, v1, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 84
    const/4 v0, 0x1

    return v0

    .line 78
    :cond_0
    iget-object v0, p0, Lcuf;->b:Landroid/app/Activity;

    const v1, 0x7f0a0ab7

    .line 79
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lxn;Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 99
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 100
    const v1, 0x7f1000ba

    if-ne v0, v1, :cond_0

    .line 101
    iget-object v0, p0, Lcuf;->b:Landroid/app/Activity;

    const-class v1, Lcnq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnq;

    invoke-interface {v0}, Lcnq;->a()V

    .line 103
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public b(Lxn;Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 89
    iget-object v0, p0, Lcuf;->a:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v2

    .line 90
    iget v0, p0, Lcuf;->d:I

    if-gt v0, v2, :cond_0

    iget v0, p0, Lcuf;->e:I

    if-lt v0, v2, :cond_0

    move v0, v1

    .line 92
    :goto_0
    const v3, 0x7f1000ba

    invoke-interface {p2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 93
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lxn;->b(Ljava/lang/CharSequence;)V

    .line 94
    return v1

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
