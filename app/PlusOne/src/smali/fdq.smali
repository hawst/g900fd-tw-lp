.class public final Lfdq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhtg;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lfdq;->a:Landroid/content/Context;

    .line 25
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lhua;I)V
    .locals 6

    .prologue
    .line 29
    iget-object v0, p0, Lfdq;->a:Landroid/content/Context;

    invoke-static {v0}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v1

    .line 30
    const-class v0, Lhee;

    invoke-virtual {v1, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 32
    iget-object v0, p0, Lfdq;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/RelatedsStreamActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "account_id"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "relateds"

    invoke-virtual {v3, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "tab"

    invoke-virtual {v3, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "activity_id"

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 36
    const-string v0, "extra_activity_id"

    invoke-virtual {v4, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const-string v0, "extra_related_topic_index"

    invoke-virtual {v4, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 38
    const-class v0, Lhms;

    invoke-virtual {v1, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v5, p0, Lfdq;->a:Landroid/content/Context;

    invoke-direct {v1, v5, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->ah:Lhmv;

    .line 40
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 41
    invoke-virtual {v1, v4}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    .line 38
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 44
    iget-object v0, p0, Lfdq;->a:Landroid/content/Context;

    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 45
    return-void
.end method
