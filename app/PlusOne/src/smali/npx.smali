.class public final Lnpx;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnpx;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1452
    const/4 v0, 0x0

    new-array v0, v0, [Lnpx;

    sput-object v0, Lnpx;->a:[Lnpx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1453
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1463
    const/high16 v0, -0x80000000

    iput v0, p0, Lnpx;->b:I

    .line 1453
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1492
    const/4 v0, 0x0

    .line 1493
    iget v1, p0, Lnpx;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1494
    const/4 v0, 0x1

    iget v1, p0, Lnpx;->b:I

    .line 1495
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1497
    :cond_0
    iget-object v1, p0, Lnpx;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1498
    const/4 v1, 0x2

    iget-object v2, p0, Lnpx;->c:Ljava/lang/String;

    .line 1499
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1501
    :cond_1
    iget-object v1, p0, Lnpx;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1502
    const/4 v1, 0x3

    iget-object v2, p0, Lnpx;->d:Ljava/lang/String;

    .line 1503
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1505
    :cond_2
    iget-object v1, p0, Lnpx;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1506
    const/4 v1, 0x4

    iget-object v2, p0, Lnpx;->e:Ljava/lang/Integer;

    .line 1507
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1509
    :cond_3
    iget-object v1, p0, Lnpx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1510
    iput v0, p0, Lnpx;->ai:I

    .line 1511
    return v0
.end method

.method public a(Loxn;)Lnpx;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1519
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1520
    sparse-switch v0, :sswitch_data_0

    .line 1524
    iget-object v1, p0, Lnpx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1525
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnpx;->ah:Ljava/util/List;

    .line 1528
    :cond_1
    iget-object v1, p0, Lnpx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1530
    :sswitch_0
    return-object p0

    .line 1535
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1536
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 1540
    :cond_2
    iput v0, p0, Lnpx;->b:I

    goto :goto_0

    .line 1542
    :cond_3
    iput v2, p0, Lnpx;->b:I

    goto :goto_0

    .line 1547
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnpx;->c:Ljava/lang/String;

    goto :goto_0

    .line 1551
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnpx;->d:Ljava/lang/String;

    goto :goto_0

    .line 1555
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnpx;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 1520
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1474
    iget v0, p0, Lnpx;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1475
    const/4 v0, 0x1

    iget v1, p0, Lnpx;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1477
    :cond_0
    iget-object v0, p0, Lnpx;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1478
    const/4 v0, 0x2

    iget-object v1, p0, Lnpx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1480
    :cond_1
    iget-object v0, p0, Lnpx;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1481
    const/4 v0, 0x3

    iget-object v1, p0, Lnpx;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1483
    :cond_2
    iget-object v0, p0, Lnpx;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1484
    const/4 v0, 0x4

    iget-object v1, p0, Lnpx;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1486
    :cond_3
    iget-object v0, p0, Lnpx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1488
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1449
    invoke-virtual {p0, p1}, Lnpx;->a(Loxn;)Lnpx;

    move-result-object v0

    return-object v0
.end method
