.class public Lhxw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private volatile a:Lhxx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 30
    iget-object v1, p0, Lhxw;->a:Lhxx;

    .line 31
    if-nez v1, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-object v0

    .line 35
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, v1, Lhxx;->b:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 42
    iget-object v0, v1, Lhxx;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lltc;)V
    .locals 6

    .prologue
    .line 50
    iget-object v0, p1, Lltc;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_0

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lhxw;->a:Lhxx;

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p1, Lltc;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 57
    new-instance v2, Lhxx;

    iget-object v3, p1, Lltc;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v0, v1}, Lhxx;-><init>(Ljava/lang/String;J)V

    iput-object v2, p0, Lhxw;->a:Lhxx;

    goto :goto_0
.end method
