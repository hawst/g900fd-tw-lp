.class public final Lcrf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcvg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcvg",
        "<",
        "Landroid/database/Cursor;",
        "Ljava/util/List",
        "<",
        "Lctm;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcrf;->a:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcrf;->b:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lcrf;->c:Landroid/graphics/Rect;

    .line 31
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 22
    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcrf;->a(Landroid/database/Cursor;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lctm;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v9, 0x9

    .line 35
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 37
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v7

    .line 59
    :goto_0
    return-object v0

    .line 41
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v7

    .line 42
    goto :goto_0

    .line 46
    :cond_2
    new-instance v8, Lctj;

    new-instance v0, Lcre;

    iget-object v1, p0, Lcrf;->b:Ljava/lang/String;

    const/4 v1, 0x3

    .line 48
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    .line 49
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 50
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x5

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5}, Ljvj;->a(J)Ljac;

    move-result-object v4

    iget-object v5, p0, Lcrf;->a:Landroid/content/Context;

    invoke-static {v5, v3, v6, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v3

    .line 51
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 52
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcre;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JI)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcrf;->c:Landroid/graphics/Rect;

    invoke-direct {v8, v0, v1, v2}, Lctj;-><init>(Lcre;ILandroid/graphics/Rect;)V

    .line 46
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 58
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    move-object v0, v7

    .line 59
    goto :goto_0
.end method
