.class public final Llxa;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxa;


# instance fields
.field public b:Ljava/lang/String;

.field private c:Llxp;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2926
    const/4 v0, 0x0

    new-array v0, v0, [Llxa;

    sput-object v0, Llxa;->a:[Llxa;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2927
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2930
    const/4 v0, 0x0

    iput-object v0, p0, Llxa;->c:Llxp;

    .line 2927
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2959
    const/4 v0, 0x0

    .line 2960
    iget-object v1, p0, Llxa;->c:Llxp;

    if-eqz v1, :cond_0

    .line 2961
    const/4 v0, 0x1

    iget-object v1, p0, Llxa;->c:Llxp;

    .line 2962
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2964
    :cond_0
    iget-object v1, p0, Llxa;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2965
    const/4 v1, 0x2

    iget-object v2, p0, Llxa;->b:Ljava/lang/String;

    .line 2966
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2968
    :cond_1
    iget-object v1, p0, Llxa;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2969
    const/4 v1, 0x3

    iget-object v2, p0, Llxa;->d:Ljava/lang/String;

    .line 2970
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2972
    :cond_2
    iget-object v1, p0, Llxa;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2973
    const/4 v1, 0x4

    iget-object v2, p0, Llxa;->e:Ljava/lang/String;

    .line 2974
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2976
    :cond_3
    iget-object v1, p0, Llxa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2977
    iput v0, p0, Llxa;->ai:I

    .line 2978
    return v0
.end method

.method public a(Loxn;)Llxa;
    .locals 2

    .prologue
    .line 2986
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2987
    sparse-switch v0, :sswitch_data_0

    .line 2991
    iget-object v1, p0, Llxa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2992
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxa;->ah:Ljava/util/List;

    .line 2995
    :cond_1
    iget-object v1, p0, Llxa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2997
    :sswitch_0
    return-object p0

    .line 3002
    :sswitch_1
    iget-object v0, p0, Llxa;->c:Llxp;

    if-nez v0, :cond_2

    .line 3003
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxa;->c:Llxp;

    .line 3005
    :cond_2
    iget-object v0, p0, Llxa;->c:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3009
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxa;->b:Ljava/lang/String;

    goto :goto_0

    .line 3013
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxa;->d:Ljava/lang/String;

    goto :goto_0

    .line 3017
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxa;->e:Ljava/lang/String;

    goto :goto_0

    .line 2987
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2941
    iget-object v0, p0, Llxa;->c:Llxp;

    if-eqz v0, :cond_0

    .line 2942
    const/4 v0, 0x1

    iget-object v1, p0, Llxa;->c:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2944
    :cond_0
    iget-object v0, p0, Llxa;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2945
    const/4 v0, 0x2

    iget-object v1, p0, Llxa;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2947
    :cond_1
    iget-object v0, p0, Llxa;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2948
    const/4 v0, 0x3

    iget-object v1, p0, Llxa;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2950
    :cond_2
    iget-object v0, p0, Llxa;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2951
    const/4 v0, 0x4

    iget-object v1, p0, Llxa;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2953
    :cond_3
    iget-object v0, p0, Llxa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2955
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2923
    invoke-virtual {p0, p1}, Llxa;->a(Loxn;)Llxa;

    move-result-object v0

    return-object v0
.end method
