.class public final Lmgx;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lowp;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23164
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23167
    iput-object v0, p0, Lmgx;->apiHeader:Llyr;

    .line 23170
    iput-object v0, p0, Lmgx;->a:Lowp;

    .line 23164
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 23187
    const/4 v0, 0x0

    .line 23188
    iget-object v1, p0, Lmgx;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 23189
    const/4 v0, 0x1

    iget-object v1, p0, Lmgx;->apiHeader:Llyr;

    .line 23190
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23192
    :cond_0
    iget-object v1, p0, Lmgx;->a:Lowp;

    if-eqz v1, :cond_1

    .line 23193
    const/4 v1, 0x2

    iget-object v2, p0, Lmgx;->a:Lowp;

    .line 23194
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23196
    :cond_1
    iget-object v1, p0, Lmgx;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23197
    iput v0, p0, Lmgx;->ai:I

    .line 23198
    return v0
.end method

.method public a(Loxn;)Lmgx;
    .locals 2

    .prologue
    .line 23206
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 23207
    sparse-switch v0, :sswitch_data_0

    .line 23211
    iget-object v1, p0, Lmgx;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 23212
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgx;->ah:Ljava/util/List;

    .line 23215
    :cond_1
    iget-object v1, p0, Lmgx;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23217
    :sswitch_0
    return-object p0

    .line 23222
    :sswitch_1
    iget-object v0, p0, Lmgx;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 23223
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgx;->apiHeader:Llyr;

    .line 23225
    :cond_2
    iget-object v0, p0, Lmgx;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23229
    :sswitch_2
    iget-object v0, p0, Lmgx;->a:Lowp;

    if-nez v0, :cond_3

    .line 23230
    new-instance v0, Lowp;

    invoke-direct {v0}, Lowp;-><init>()V

    iput-object v0, p0, Lmgx;->a:Lowp;

    .line 23232
    :cond_3
    iget-object v0, p0, Lmgx;->a:Lowp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23207
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23175
    iget-object v0, p0, Lmgx;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 23176
    const/4 v0, 0x1

    iget-object v1, p0, Lmgx;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23178
    :cond_0
    iget-object v0, p0, Lmgx;->a:Lowp;

    if-eqz v0, :cond_1

    .line 23179
    const/4 v0, 0x2

    iget-object v1, p0, Lmgx;->a:Lowp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23181
    :cond_1
    iget-object v0, p0, Lmgx;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 23183
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 23160
    invoke-virtual {p0, p1}, Lmgx;->a(Loxn;)Lmgx;

    move-result-object v0

    return-object v0
.end method
