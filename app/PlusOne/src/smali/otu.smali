.class public final Lotu;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lowr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 840
    invoke-direct {p0}, Loxq;-><init>()V

    .line 843
    sget-object v0, Lowr;->a:[Lowr;

    iput-object v0, p0, Lotu;->a:[Lowr;

    .line 840
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 861
    .line 862
    iget-object v1, p0, Lotu;->a:[Lowr;

    if-eqz v1, :cond_1

    .line 863
    iget-object v2, p0, Lotu;->a:[Lowr;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 864
    if-eqz v4, :cond_0

    .line 865
    const/4 v5, 0x1

    .line 866
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 863
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 870
    :cond_1
    iget-object v1, p0, Lotu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 871
    iput v0, p0, Lotu;->ai:I

    .line 872
    return v0
.end method

.method public a(Loxn;)Lotu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 880
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 881
    sparse-switch v0, :sswitch_data_0

    .line 885
    iget-object v2, p0, Lotu;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 886
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lotu;->ah:Ljava/util/List;

    .line 889
    :cond_1
    iget-object v2, p0, Lotu;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 891
    :sswitch_0
    return-object p0

    .line 896
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 897
    iget-object v0, p0, Lotu;->a:[Lowr;

    if-nez v0, :cond_3

    move v0, v1

    .line 898
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lowr;

    .line 899
    iget-object v3, p0, Lotu;->a:[Lowr;

    if-eqz v3, :cond_2

    .line 900
    iget-object v3, p0, Lotu;->a:[Lowr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 902
    :cond_2
    iput-object v2, p0, Lotu;->a:[Lowr;

    .line 903
    :goto_2
    iget-object v2, p0, Lotu;->a:[Lowr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 904
    iget-object v2, p0, Lotu;->a:[Lowr;

    new-instance v3, Lowr;

    invoke-direct {v3}, Lowr;-><init>()V

    aput-object v3, v2, v0

    .line 905
    iget-object v2, p0, Lotu;->a:[Lowr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 906
    invoke-virtual {p1}, Loxn;->a()I

    .line 903
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 897
    :cond_3
    iget-object v0, p0, Lotu;->a:[Lowr;

    array-length v0, v0

    goto :goto_1

    .line 909
    :cond_4
    iget-object v2, p0, Lotu;->a:[Lowr;

    new-instance v3, Lowr;

    invoke-direct {v3}, Lowr;-><init>()V

    aput-object v3, v2, v0

    .line 910
    iget-object v2, p0, Lotu;->a:[Lowr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 881
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 848
    iget-object v0, p0, Lotu;->a:[Lowr;

    if-eqz v0, :cond_1

    .line 849
    iget-object v1, p0, Lotu;->a:[Lowr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 850
    if-eqz v3, :cond_0

    .line 851
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 849
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 855
    :cond_1
    iget-object v0, p0, Lotu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 857
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 836
    invoke-virtual {p0, p1}, Lotu;->a(Loxn;)Lotu;

    move-result-object v0

    return-object v0
.end method
