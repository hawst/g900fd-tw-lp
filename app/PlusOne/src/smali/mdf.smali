.class public final Lmdf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlp;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32722
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32725
    iput-object v0, p0, Lmdf;->apiHeader:Llyr;

    .line 32728
    iput-object v0, p0, Lmdf;->a:Lmlp;

    .line 32722
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32745
    const/4 v0, 0x0

    .line 32746
    iget-object v1, p0, Lmdf;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 32747
    const/4 v0, 0x1

    iget-object v1, p0, Lmdf;->apiHeader:Llyr;

    .line 32748
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32750
    :cond_0
    iget-object v1, p0, Lmdf;->a:Lmlp;

    if-eqz v1, :cond_1

    .line 32751
    const/4 v1, 0x2

    iget-object v2, p0, Lmdf;->a:Lmlp;

    .line 32752
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32754
    :cond_1
    iget-object v1, p0, Lmdf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32755
    iput v0, p0, Lmdf;->ai:I

    .line 32756
    return v0
.end method

.method public a(Loxn;)Lmdf;
    .locals 2

    .prologue
    .line 32764
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32765
    sparse-switch v0, :sswitch_data_0

    .line 32769
    iget-object v1, p0, Lmdf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32770
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdf;->ah:Ljava/util/List;

    .line 32773
    :cond_1
    iget-object v1, p0, Lmdf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32775
    :sswitch_0
    return-object p0

    .line 32780
    :sswitch_1
    iget-object v0, p0, Lmdf;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 32781
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmdf;->apiHeader:Llyr;

    .line 32783
    :cond_2
    iget-object v0, p0, Lmdf;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32787
    :sswitch_2
    iget-object v0, p0, Lmdf;->a:Lmlp;

    if-nez v0, :cond_3

    .line 32788
    new-instance v0, Lmlp;

    invoke-direct {v0}, Lmlp;-><init>()V

    iput-object v0, p0, Lmdf;->a:Lmlp;

    .line 32790
    :cond_3
    iget-object v0, p0, Lmdf;->a:Lmlp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32765
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32733
    iget-object v0, p0, Lmdf;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 32734
    const/4 v0, 0x1

    iget-object v1, p0, Lmdf;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32736
    :cond_0
    iget-object v0, p0, Lmdf;->a:Lmlp;

    if-eqz v0, :cond_1

    .line 32737
    const/4 v0, 0x2

    iget-object v1, p0, Lmdf;->a:Lmlp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32739
    :cond_1
    iget-object v0, p0, Lmdf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32741
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 32718
    invoke-virtual {p0, p1}, Lmdf;->a(Loxn;)Lmdf;

    move-result-object v0

    return-object v0
.end method
