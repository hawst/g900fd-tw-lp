.class public final Llag;
.super Lllq;
.source "PG"

# interfaces
.implements Lhty;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lllq;-><init>()V

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lllq;-><init>()V

    .line 27
    iput-object p1, p0, Llag;->a:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Llag;->b:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Llag;->c:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Llag;->d:Ljava/lang/String;

    .line 31
    iput-boolean p5, p0, Llag;->e:Z

    .line 32
    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;)Llag;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 63
    invoke-static {p0}, Llag;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-static {p0}, Llag;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    .line 65
    invoke-static {p0}, Llag;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    .line 66
    invoke-static {p0}, Llag;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    .line 67
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 68
    :goto_0
    new-instance v0, Llag;

    invoke-direct/range {v0 .. v5}, Llag;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    .line 67
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public static a(Llag;Ljava/io/DataOutputStream;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Llag;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Llag;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Llag;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Llag;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Llag;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Llag;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Llag;->d:Ljava/lang/String;

    invoke-static {p1, v0}, Llag;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 51
    iget-boolean v0, p0, Llag;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 52
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Llag;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Llag;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Llag;->d:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Llag;->e:Z

    return v0
.end method
