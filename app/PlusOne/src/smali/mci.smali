.class public final Lmci;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lneg;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26161
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26164
    iput-object v0, p0, Lmci;->apiHeader:Llyq;

    .line 26167
    iput-object v0, p0, Lmci;->a:Lneg;

    .line 26161
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26184
    const/4 v0, 0x0

    .line 26185
    iget-object v1, p0, Lmci;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 26186
    const/4 v0, 0x1

    iget-object v1, p0, Lmci;->apiHeader:Llyq;

    .line 26187
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26189
    :cond_0
    iget-object v1, p0, Lmci;->a:Lneg;

    if-eqz v1, :cond_1

    .line 26190
    const/4 v1, 0x2

    iget-object v2, p0, Lmci;->a:Lneg;

    .line 26191
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26193
    :cond_1
    iget-object v1, p0, Lmci;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26194
    iput v0, p0, Lmci;->ai:I

    .line 26195
    return v0
.end method

.method public a(Loxn;)Lmci;
    .locals 2

    .prologue
    .line 26203
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26204
    sparse-switch v0, :sswitch_data_0

    .line 26208
    iget-object v1, p0, Lmci;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26209
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmci;->ah:Ljava/util/List;

    .line 26212
    :cond_1
    iget-object v1, p0, Lmci;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26214
    :sswitch_0
    return-object p0

    .line 26219
    :sswitch_1
    iget-object v0, p0, Lmci;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 26220
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmci;->apiHeader:Llyq;

    .line 26222
    :cond_2
    iget-object v0, p0, Lmci;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26226
    :sswitch_2
    iget-object v0, p0, Lmci;->a:Lneg;

    if-nez v0, :cond_3

    .line 26227
    new-instance v0, Lneg;

    invoke-direct {v0}, Lneg;-><init>()V

    iput-object v0, p0, Lmci;->a:Lneg;

    .line 26229
    :cond_3
    iget-object v0, p0, Lmci;->a:Lneg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26204
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26172
    iget-object v0, p0, Lmci;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 26173
    const/4 v0, 0x1

    iget-object v1, p0, Lmci;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26175
    :cond_0
    iget-object v0, p0, Lmci;->a:Lneg;

    if-eqz v0, :cond_1

    .line 26176
    const/4 v0, 0x2

    iget-object v1, p0, Lmci;->a:Lneg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26178
    :cond_1
    iget-object v0, p0, Lmci;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26180
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26157
    invoke-virtual {p0, p1}, Lmci;->a(Loxn;)Lmci;

    move-result-object v0

    return-object v0
.end method
