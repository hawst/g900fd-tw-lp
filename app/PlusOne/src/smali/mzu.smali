.class public final Lmzu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmzo;

.field public b:Lmzt;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/Boolean;

.field public h:Loxz;

.field public i:I

.field private j:Lmzs;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    iput-object v0, p0, Lmzu;->a:Lmzo;

    .line 16
    iput-object v0, p0, Lmzu;->b:Lmzt;

    .line 25
    iput-object v0, p0, Lmzu;->j:Lmzs;

    .line 34
    iput-object v0, p0, Lmzu;->h:Loxz;

    .line 37
    const/high16 v0, -0x80000000

    iput v0, p0, Lmzu;->i:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 87
    iget-object v1, p0, Lmzu;->a:Lmzo;

    if-eqz v1, :cond_0

    .line 88
    const/4 v0, 0x1

    iget-object v1, p0, Lmzu;->a:Lmzo;

    .line 89
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 91
    :cond_0
    iget-object v1, p0, Lmzu;->b:Lmzt;

    if-eqz v1, :cond_1

    .line 92
    const/4 v1, 0x2

    iget-object v2, p0, Lmzu;->b:Lmzt;

    .line 93
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_1
    iget-object v1, p0, Lmzu;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 96
    const/4 v1, 0x3

    iget-object v2, p0, Lmzu;->c:Ljava/lang/String;

    .line 97
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_2
    iget-object v1, p0, Lmzu;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 100
    const/4 v1, 0x4

    iget-object v2, p0, Lmzu;->d:Ljava/lang/Boolean;

    .line 101
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 103
    :cond_3
    iget-object v1, p0, Lmzu;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 104
    const/4 v1, 0x5

    iget-object v2, p0, Lmzu;->e:Ljava/lang/Boolean;

    .line 105
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 107
    :cond_4
    iget-object v1, p0, Lmzu;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 108
    const/4 v1, 0x6

    iget-object v2, p0, Lmzu;->k:Ljava/lang/Boolean;

    .line 109
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 111
    :cond_5
    iget-object v1, p0, Lmzu;->f:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 112
    const/4 v1, 0x7

    iget-object v2, p0, Lmzu;->f:Ljava/lang/Boolean;

    .line 113
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 115
    :cond_6
    iget-object v1, p0, Lmzu;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 116
    const/16 v1, 0x8

    iget-object v2, p0, Lmzu;->g:Ljava/lang/Boolean;

    .line 117
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 119
    :cond_7
    iget-object v1, p0, Lmzu;->h:Loxz;

    if-eqz v1, :cond_8

    .line 120
    const/16 v1, 0x9

    iget-object v2, p0, Lmzu;->h:Loxz;

    .line 121
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_8
    iget-object v1, p0, Lmzu;->j:Lmzs;

    if-eqz v1, :cond_9

    .line 124
    const/16 v1, 0xa

    iget-object v2, p0, Lmzu;->j:Lmzs;

    .line 125
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_9
    iget v1, p0, Lmzu;->i:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_a

    .line 128
    const/16 v1, 0xb

    iget v2, p0, Lmzu;->i:I

    .line 129
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_a
    iget-object v1, p0, Lmzu;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 132
    const/16 v1, 0xc

    iget-object v2, p0, Lmzu;->l:Ljava/lang/Boolean;

    .line 133
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 135
    :cond_b
    iget-object v1, p0, Lmzu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    iput v0, p0, Lmzu;->ai:I

    .line 137
    return v0
.end method

.method public a(Loxn;)Lmzu;
    .locals 2

    .prologue
    .line 145
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 146
    sparse-switch v0, :sswitch_data_0

    .line 150
    iget-object v1, p0, Lmzu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 151
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmzu;->ah:Ljava/util/List;

    .line 154
    :cond_1
    iget-object v1, p0, Lmzu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    :sswitch_0
    return-object p0

    .line 161
    :sswitch_1
    iget-object v0, p0, Lmzu;->a:Lmzo;

    if-nez v0, :cond_2

    .line 162
    new-instance v0, Lmzo;

    invoke-direct {v0}, Lmzo;-><init>()V

    iput-object v0, p0, Lmzu;->a:Lmzo;

    .line 164
    :cond_2
    iget-object v0, p0, Lmzu;->a:Lmzo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 168
    :sswitch_2
    iget-object v0, p0, Lmzu;->b:Lmzt;

    if-nez v0, :cond_3

    .line 169
    new-instance v0, Lmzt;

    invoke-direct {v0}, Lmzt;-><init>()V

    iput-object v0, p0, Lmzu;->b:Lmzt;

    .line 171
    :cond_3
    iget-object v0, p0, Lmzu;->b:Lmzt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 175
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmzu;->c:Ljava/lang/String;

    goto :goto_0

    .line 179
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzu;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 183
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzu;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 187
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzu;->k:Ljava/lang/Boolean;

    goto :goto_0

    .line 191
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzu;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 195
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzu;->g:Ljava/lang/Boolean;

    goto :goto_0

    .line 199
    :sswitch_9
    iget-object v0, p0, Lmzu;->h:Loxz;

    if-nez v0, :cond_4

    .line 200
    new-instance v0, Loxz;

    invoke-direct {v0}, Loxz;-><init>()V

    iput-object v0, p0, Lmzu;->h:Loxz;

    .line 202
    :cond_4
    iget-object v0, p0, Lmzu;->h:Loxz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 206
    :sswitch_a
    iget-object v0, p0, Lmzu;->j:Lmzs;

    if-nez v0, :cond_5

    .line 207
    new-instance v0, Lmzs;

    invoke-direct {v0}, Lmzs;-><init>()V

    iput-object v0, p0, Lmzu;->j:Lmzs;

    .line 209
    :cond_5
    iget-object v0, p0, Lmzu;->j:Lmzs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 213
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 214
    if-eqz v0, :cond_6

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 217
    :cond_6
    iput v0, p0, Lmzu;->i:I

    goto/16 :goto_0

    .line 219
    :cond_7
    const/4 v0, 0x0

    iput v0, p0, Lmzu;->i:I

    goto/16 :goto_0

    .line 224
    :sswitch_c
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmzu;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 146
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lmzu;->a:Lmzo;

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x1

    iget-object v1, p0, Lmzu;->a:Lmzo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 47
    :cond_0
    iget-object v0, p0, Lmzu;->b:Lmzt;

    if-eqz v0, :cond_1

    .line 48
    const/4 v0, 0x2

    iget-object v1, p0, Lmzu;->b:Lmzt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 50
    :cond_1
    iget-object v0, p0, Lmzu;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 51
    const/4 v0, 0x3

    iget-object v1, p0, Lmzu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 53
    :cond_2
    iget-object v0, p0, Lmzu;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 54
    const/4 v0, 0x4

    iget-object v1, p0, Lmzu;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 56
    :cond_3
    iget-object v0, p0, Lmzu;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 57
    const/4 v0, 0x5

    iget-object v1, p0, Lmzu;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 59
    :cond_4
    iget-object v0, p0, Lmzu;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 60
    const/4 v0, 0x6

    iget-object v1, p0, Lmzu;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 62
    :cond_5
    iget-object v0, p0, Lmzu;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 63
    const/4 v0, 0x7

    iget-object v1, p0, Lmzu;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 65
    :cond_6
    iget-object v0, p0, Lmzu;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 66
    const/16 v0, 0x8

    iget-object v1, p0, Lmzu;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 68
    :cond_7
    iget-object v0, p0, Lmzu;->h:Loxz;

    if-eqz v0, :cond_8

    .line 69
    const/16 v0, 0x9

    iget-object v1, p0, Lmzu;->h:Loxz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 71
    :cond_8
    iget-object v0, p0, Lmzu;->j:Lmzs;

    if-eqz v0, :cond_9

    .line 72
    const/16 v0, 0xa

    iget-object v1, p0, Lmzu;->j:Lmzs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 74
    :cond_9
    iget v0, p0, Lmzu;->i:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_a

    .line 75
    const/16 v0, 0xb

    iget v1, p0, Lmzu;->i:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 77
    :cond_a
    iget-object v0, p0, Lmzu;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    .line 78
    const/16 v0, 0xc

    iget-object v1, p0, Lmzu;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 80
    :cond_b
    iget-object v0, p0, Lmzu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 82
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lmzu;->a(Loxn;)Lmzu;

    move-result-object v0

    return-object v0
.end method
