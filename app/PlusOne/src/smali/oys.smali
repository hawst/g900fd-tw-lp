.class public final Loys;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Loys;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Lpdi;

.field private f:Ljava/lang/String;

.field private g:Loya;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8
    const v0, 0x25cf00c

    new-instance v1, Loyt;

    invoke-direct {v1}, Loyt;-><init>()V

    .line 13
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Loys;->a:Loxr;

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 45
    const/high16 v0, -0x80000000

    iput v0, p0, Loys;->d:I

    .line 48
    iput-object v1, p0, Loys;->e:Lpdi;

    .line 51
    iput-object v1, p0, Loys;->g:Loya;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 80
    const/4 v0, 0x0

    .line 81
    iget-object v1, p0, Loys;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 82
    const/4 v0, 0x1

    iget-object v1, p0, Loys;->b:Ljava/lang/String;

    .line 83
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 85
    :cond_0
    iget-object v1, p0, Loys;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 86
    const/4 v1, 0x2

    iget-object v2, p0, Loys;->c:Ljava/lang/String;

    .line 87
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_1
    iget-object v1, p0, Loys;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 90
    const/4 v1, 0x3

    iget-object v2, p0, Loys;->f:Ljava/lang/String;

    .line 91
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_2
    iget v1, p0, Loys;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 94
    const/4 v1, 0x4

    iget v2, p0, Loys;->d:I

    .line 95
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_3
    iget-object v1, p0, Loys;->e:Lpdi;

    if-eqz v1, :cond_4

    .line 98
    const/4 v1, 0x5

    iget-object v2, p0, Loys;->e:Lpdi;

    .line 99
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_4
    iget-object v1, p0, Loys;->g:Loya;

    if-eqz v1, :cond_5

    .line 102
    const/4 v1, 0x6

    iget-object v2, p0, Loys;->g:Loya;

    .line 103
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    :cond_5
    iget-object v1, p0, Loys;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    iput v0, p0, Loys;->ai:I

    .line 107
    return v0
.end method

.method public a(Loxn;)Loys;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 115
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 116
    sparse-switch v0, :sswitch_data_0

    .line 120
    iget-object v1, p0, Loys;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 121
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loys;->ah:Ljava/util/List;

    .line 124
    :cond_1
    iget-object v1, p0, Loys;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    :sswitch_0
    return-object p0

    .line 131
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loys;->b:Ljava/lang/String;

    goto :goto_0

    .line 135
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loys;->c:Ljava/lang/String;

    goto :goto_0

    .line 139
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loys;->f:Ljava/lang/String;

    goto :goto_0

    .line 143
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 144
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-eq v0, v1, :cond_2

    const/16 v1, 0x14

    if-ne v0, v1, :cond_3

    .line 164
    :cond_2
    iput v0, p0, Loys;->d:I

    goto :goto_0

    .line 166
    :cond_3
    iput v2, p0, Loys;->d:I

    goto/16 :goto_0

    .line 171
    :sswitch_5
    iget-object v0, p0, Loys;->e:Lpdi;

    if-nez v0, :cond_4

    .line 172
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Loys;->e:Lpdi;

    .line 174
    :cond_4
    iget-object v0, p0, Loys;->e:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 178
    :sswitch_6
    iget-object v0, p0, Loys;->g:Loya;

    if-nez v0, :cond_5

    .line 179
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Loys;->g:Loya;

    .line 181
    :cond_5
    iget-object v0, p0, Loys;->g:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 116
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Loys;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    const/4 v0, 0x1

    iget-object v1, p0, Loys;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 59
    :cond_0
    iget-object v0, p0, Loys;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 60
    const/4 v0, 0x2

    iget-object v1, p0, Loys;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 62
    :cond_1
    iget-object v0, p0, Loys;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 63
    const/4 v0, 0x3

    iget-object v1, p0, Loys;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 65
    :cond_2
    iget v0, p0, Loys;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 66
    const/4 v0, 0x4

    iget v1, p0, Loys;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 68
    :cond_3
    iget-object v0, p0, Loys;->e:Lpdi;

    if-eqz v0, :cond_4

    .line 69
    const/4 v0, 0x5

    iget-object v1, p0, Loys;->e:Lpdi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 71
    :cond_4
    iget-object v0, p0, Loys;->g:Loya;

    if-eqz v0, :cond_5

    .line 72
    const/4 v0, 0x6

    iget-object v1, p0, Loys;->g:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 74
    :cond_5
    iget-object v0, p0, Loys;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 76
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Loys;->a(Loxn;)Loys;

    move-result-object v0

    return-object v0
.end method
