.class public final Lfuw;
.super Lfvi;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Lfvq;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lfvi;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lfvq;)V

    .line 31
    return-void
.end method

.method public static a(Landroid/content/Context;IILandroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 50
    if-nez p0, :cond_0

    .line 51
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 52
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 53
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 55
    new-instance v0, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;-><init>(Landroid/content/Context;)V

    .line 56
    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a(I)V

    .line 58
    invoke-static {p0}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v1

    iget v1, v1, Llct;->aB:I

    .line 57
    invoke-virtual {v0, v3, v1, v3, v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a(IIII)V

    .line 59
    invoke-virtual {p3, v0, p4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 61
    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/social/stream/legacy/views/StreamTooltipView;->a(J)V

    .line 63
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 39
    .line 40
    if-eqz p0, :cond_1

    .line 41
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 42
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 43
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 45
    :goto_0
    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/widget/ListView;ILjava/util/HashMap;)Ljava/lang/Float;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "I",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Float;",
            ">;)",
            "Ljava/lang/Float;"
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p1, p2}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfuw;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 93
    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    return-object v0
.end method

.method protected a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 97
    if-nez p1, :cond_0

    move-object p1, v0

    .line 111
    :goto_0
    return-object p1

    .line 100
    :cond_0
    instance-of v1, p1, Lohv;

    if-eqz v1, :cond_1

    .line 101
    check-cast p1, Lohv;

    iget-object v0, p1, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->b(Lohp;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 102
    :cond_1
    instance-of v1, p1, Lnrp;

    if-eqz v1, :cond_3

    .line 103
    check-cast p1, Lnrp;

    .line 104
    iget-object v1, p1, Lnrp;->b:Lohv;

    if-nez v1, :cond_2

    move-object p1, v0

    .line 105
    goto :goto_0

    .line 107
    :cond_2
    iget-object v0, p1, Lnrp;->b:Lohv;

    iget-object v0, v0, Lohv;->b:Lohp;

    invoke-static {v0}, Ldsm;->b(Lohp;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 108
    :cond_3
    instance-of v1, p1, Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 109
    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_4
    move-object p1, v0

    .line 111
    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;[Lcom/google/android/apps/plus/views/PeopleListRowView;II)V
    .locals 7

    .prologue
    .line 75
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    .line 76
    new-instance v0, Lfux;

    move-object v1, p0

    move v3, p4

    move v4, p5

    move-object v5, p3

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lfux;-><init>(Lfuw;Landroid/view/ViewTreeObserver;II[Lcom/google/android/apps/plus/views/PeopleListRowView;Landroid/view/View;)V

    invoke-virtual {v2, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 88
    return-void
.end method

.method public a(Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public a(F)Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public d(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 67
    const v0, 0x7f10048c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 68
    const/4 v0, 0x0

    .line 70
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
