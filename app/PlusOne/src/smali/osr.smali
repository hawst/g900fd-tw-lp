.class public final Losr;
.super Loxq;
.source "PG"


# instance fields
.field private a:Loui;

.field private b:Louj;

.field private c:Loug;

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2102
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2105
    iput-object v0, p0, Losr;->a:Loui;

    .line 2108
    iput-object v0, p0, Losr;->b:Louj;

    .line 2111
    iput-object v0, p0, Losr;->c:Loug;

    .line 2102
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2136
    const/4 v0, 0x0

    .line 2137
    iget-object v1, p0, Losr;->a:Loui;

    if-eqz v1, :cond_0

    .line 2138
    const/4 v0, 0x1

    iget-object v1, p0, Losr;->a:Loui;

    .line 2139
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2141
    :cond_0
    iget-object v1, p0, Losr;->b:Louj;

    if-eqz v1, :cond_1

    .line 2142
    const/4 v1, 0x2

    iget-object v2, p0, Losr;->b:Louj;

    .line 2143
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2145
    :cond_1
    iget-object v1, p0, Losr;->c:Loug;

    if-eqz v1, :cond_2

    .line 2146
    const/4 v1, 0x3

    iget-object v2, p0, Losr;->c:Loug;

    .line 2147
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2149
    :cond_2
    iget-object v1, p0, Losr;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2150
    const/4 v1, 0x4

    iget-object v2, p0, Losr;->d:Ljava/lang/Boolean;

    .line 2151
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2153
    :cond_3
    iget-object v1, p0, Losr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2154
    iput v0, p0, Losr;->ai:I

    .line 2155
    return v0
.end method

.method public a(Loxn;)Losr;
    .locals 2

    .prologue
    .line 2163
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2164
    sparse-switch v0, :sswitch_data_0

    .line 2168
    iget-object v1, p0, Losr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2169
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Losr;->ah:Ljava/util/List;

    .line 2172
    :cond_1
    iget-object v1, p0, Losr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2174
    :sswitch_0
    return-object p0

    .line 2179
    :sswitch_1
    iget-object v0, p0, Losr;->a:Loui;

    if-nez v0, :cond_2

    .line 2180
    new-instance v0, Loui;

    invoke-direct {v0}, Loui;-><init>()V

    iput-object v0, p0, Losr;->a:Loui;

    .line 2182
    :cond_2
    iget-object v0, p0, Losr;->a:Loui;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2186
    :sswitch_2
    iget-object v0, p0, Losr;->b:Louj;

    if-nez v0, :cond_3

    .line 2187
    new-instance v0, Louj;

    invoke-direct {v0}, Louj;-><init>()V

    iput-object v0, p0, Losr;->b:Louj;

    .line 2189
    :cond_3
    iget-object v0, p0, Losr;->b:Louj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2193
    :sswitch_3
    iget-object v0, p0, Losr;->c:Loug;

    if-nez v0, :cond_4

    .line 2194
    new-instance v0, Loug;

    invoke-direct {v0}, Loug;-><init>()V

    iput-object v0, p0, Losr;->c:Loug;

    .line 2196
    :cond_4
    iget-object v0, p0, Losr;->c:Loug;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2200
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Losr;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 2164
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2118
    iget-object v0, p0, Losr;->a:Loui;

    if-eqz v0, :cond_0

    .line 2119
    const/4 v0, 0x1

    iget-object v1, p0, Losr;->a:Loui;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2121
    :cond_0
    iget-object v0, p0, Losr;->b:Louj;

    if-eqz v0, :cond_1

    .line 2122
    const/4 v0, 0x2

    iget-object v1, p0, Losr;->b:Louj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2124
    :cond_1
    iget-object v0, p0, Losr;->c:Loug;

    if-eqz v0, :cond_2

    .line 2125
    const/4 v0, 0x3

    iget-object v1, p0, Losr;->c:Loug;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2127
    :cond_2
    iget-object v0, p0, Losr;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2128
    const/4 v0, 0x4

    iget-object v1, p0, Losr;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2130
    :cond_3
    iget-object v0, p0, Losr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2132
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2098
    invoke-virtual {p0, p1}, Losr;->a(Loxn;)Losr;

    move-result-object v0

    return-object v0
.end method
