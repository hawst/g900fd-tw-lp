.class public final Loid;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 876
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 900
    const/4 v0, 0x1

    iget-object v1, p0, Loid;->a:Ljava/lang/String;

    .line 902
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 903
    iget-object v1, p0, Loid;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 904
    const/4 v1, 0x2

    iget-object v2, p0, Loid;->b:Ljava/lang/Integer;

    .line 905
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 907
    :cond_0
    iget-object v1, p0, Loid;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 908
    const/4 v1, 0x3

    iget-object v2, p0, Loid;->c:Ljava/lang/Integer;

    .line 909
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 911
    :cond_1
    iget-object v1, p0, Loid;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 912
    iput v0, p0, Loid;->ai:I

    .line 913
    return v0
.end method

.method public a(Loxn;)Loid;
    .locals 2

    .prologue
    .line 921
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 922
    sparse-switch v0, :sswitch_data_0

    .line 926
    iget-object v1, p0, Loid;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 927
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loid;->ah:Ljava/util/List;

    .line 930
    :cond_1
    iget-object v1, p0, Loid;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 932
    :sswitch_0
    return-object p0

    .line 937
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loid;->a:Ljava/lang/String;

    goto :goto_0

    .line 941
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loid;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 945
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loid;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 922
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 887
    const/4 v0, 0x1

    iget-object v1, p0, Loid;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 888
    iget-object v0, p0, Loid;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 889
    const/4 v0, 0x2

    iget-object v1, p0, Loid;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 891
    :cond_0
    iget-object v0, p0, Loid;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 892
    const/4 v0, 0x3

    iget-object v1, p0, Loid;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 894
    :cond_1
    iget-object v0, p0, Loid;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 896
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 872
    invoke-virtual {p0, p1}, Loid;->a(Loxn;)Loid;

    move-result-object v0

    return-object v0
.end method
