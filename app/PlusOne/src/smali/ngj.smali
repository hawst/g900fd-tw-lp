.class public final Lngj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lngk;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/Long;

.field private h:I

.field private i:Lngj;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Loxq;-><init>()V

    .line 156
    iput-object v1, p0, Lngj;->b:Lngk;

    .line 169
    const/high16 v0, -0x80000000

    iput v0, p0, Lngj;->h:I

    .line 172
    iput-object v1, p0, Lngj;->i:Lngj;

    .line 70
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 210
    const/4 v0, 0x0

    .line 211
    iget-object v1, p0, Lngj;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 212
    const/4 v0, 0x1

    iget-object v1, p0, Lngj;->a:Ljava/lang/String;

    .line 213
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 215
    :cond_0
    iget-object v1, p0, Lngj;->b:Lngk;

    if-eqz v1, :cond_1

    .line 216
    const/4 v1, 0x2

    iget-object v2, p0, Lngj;->b:Lngk;

    .line 217
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_1
    iget-object v1, p0, Lngj;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 220
    const/4 v1, 0x3

    iget-object v2, p0, Lngj;->c:Ljava/lang/String;

    .line 221
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_2
    iget-object v1, p0, Lngj;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 224
    const/4 v1, 0x4

    iget-object v2, p0, Lngj;->e:Ljava/lang/String;

    .line 225
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_3
    iget-object v1, p0, Lngj;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 228
    const/4 v1, 0x5

    iget-object v2, p0, Lngj;->d:Ljava/lang/String;

    .line 229
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_4
    iget-object v1, p0, Lngj;->f:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 232
    const/4 v1, 0x6

    iget-object v2, p0, Lngj;->f:Ljava/lang/Long;

    .line 233
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    :cond_5
    iget-object v1, p0, Lngj;->g:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 236
    const/4 v1, 0x7

    iget-object v2, p0, Lngj;->g:Ljava/lang/Long;

    .line 237
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_6
    iget v1, p0, Lngj;->h:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_7

    .line 240
    const/16 v1, 0x8

    iget v2, p0, Lngj;->h:I

    .line 241
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    :cond_7
    iget-object v1, p0, Lngj;->i:Lngj;

    if-eqz v1, :cond_8

    .line 244
    const/16 v1, 0x9

    iget-object v2, p0, Lngj;->i:Lngj;

    .line 245
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 247
    :cond_8
    iget-object v1, p0, Lngj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    iput v0, p0, Lngj;->ai:I

    .line 249
    return v0
.end method

.method public a(Loxn;)Lngj;
    .locals 2

    .prologue
    .line 257
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 258
    sparse-switch v0, :sswitch_data_0

    .line 262
    iget-object v1, p0, Lngj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 263
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lngj;->ah:Ljava/util/List;

    .line 266
    :cond_1
    iget-object v1, p0, Lngj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 268
    :sswitch_0
    return-object p0

    .line 273
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngj;->a:Ljava/lang/String;

    goto :goto_0

    .line 277
    :sswitch_2
    iget-object v0, p0, Lngj;->b:Lngk;

    if-nez v0, :cond_2

    .line 278
    new-instance v0, Lngk;

    invoke-direct {v0}, Lngk;-><init>()V

    iput-object v0, p0, Lngj;->b:Lngk;

    .line 280
    :cond_2
    iget-object v0, p0, Lngj;->b:Lngk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 284
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngj;->c:Ljava/lang/String;

    goto :goto_0

    .line 288
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngj;->e:Ljava/lang/String;

    goto :goto_0

    .line 292
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lngj;->d:Ljava/lang/String;

    goto :goto_0

    .line 296
    :sswitch_6
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lngj;->f:Ljava/lang/Long;

    goto :goto_0

    .line 300
    :sswitch_7
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lngj;->g:Ljava/lang/Long;

    goto :goto_0

    .line 304
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 305
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_4

    .line 310
    :cond_3
    iput v0, p0, Lngj;->h:I

    goto :goto_0

    .line 312
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lngj;->h:I

    goto :goto_0

    .line 317
    :sswitch_9
    iget-object v0, p0, Lngj;->i:Lngj;

    if-nez v0, :cond_5

    .line 318
    new-instance v0, Lngj;

    invoke-direct {v0}, Lngj;-><init>()V

    iput-object v0, p0, Lngj;->i:Lngj;

    .line 320
    :cond_5
    iget-object v0, p0, Lngj;->i:Lngj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 258
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 177
    iget-object v0, p0, Lngj;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 178
    const/4 v0, 0x1

    iget-object v1, p0, Lngj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 180
    :cond_0
    iget-object v0, p0, Lngj;->b:Lngk;

    if-eqz v0, :cond_1

    .line 181
    const/4 v0, 0x2

    iget-object v1, p0, Lngj;->b:Lngk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 183
    :cond_1
    iget-object v0, p0, Lngj;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 184
    const/4 v0, 0x3

    iget-object v1, p0, Lngj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 186
    :cond_2
    iget-object v0, p0, Lngj;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 187
    const/4 v0, 0x4

    iget-object v1, p0, Lngj;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 189
    :cond_3
    iget-object v0, p0, Lngj;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 190
    const/4 v0, 0x5

    iget-object v1, p0, Lngj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 192
    :cond_4
    iget-object v0, p0, Lngj;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 193
    const/4 v0, 0x6

    iget-object v1, p0, Lngj;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 195
    :cond_5
    iget-object v0, p0, Lngj;->g:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 196
    const/4 v0, 0x7

    iget-object v1, p0, Lngj;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 198
    :cond_6
    iget v0, p0, Lngj;->h:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_7

    .line 199
    const/16 v0, 0x8

    iget v1, p0, Lngj;->h:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 201
    :cond_7
    iget-object v0, p0, Lngj;->i:Lngj;

    if-eqz v0, :cond_8

    .line 202
    const/16 v0, 0x9

    iget-object v1, p0, Lngj;->i:Lngj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 204
    :cond_8
    iget-object v0, p0, Lngj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 206
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lngj;->a(Loxn;)Lngj;

    move-result-object v0

    return-object v0
.end method
