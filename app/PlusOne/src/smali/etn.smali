.class public final Letn;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;
.implements Leth;
.implements Lhjj;
.implements Lhmq;
.implements Lhob;
.implements Lhsw;
.implements Litn;
.implements Llix;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Llol;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Leth;",
        "Lhjj;",
        "Lhmq;",
        "Lhob;",
        "Lhsw;",
        "Litn;",
        "Llix;"
    }
.end annotation


# static fields
.field private static final N:Lium;

.field private static O:Z

.field private static P:I

.field private static Q:I

.field private static R:I

.field private static S:I

.field private static T:I

.field private static U:I

.field private static V:I

.field private static W:I


# instance fields
.field private final X:Lhov;

.field private final Y:Lhje;

.field private Z:Lhee;

.field private aA:Z

.field private aB:Z

.field private aC:Z

.field private aD:Z

.field private aE:Line;

.field private aF:I

.field private aG:F

.field private aH:I

.field private aI:Z

.field private aJ:Z

.field private aK:Z

.field private aL:I

.field private aM:I

.field private aN:Z

.field private aO:Z

.field private aP:Z

.field private aQ:I

.field private aR:Z

.field private aS:Lfvs;

.field private aT:Lfvs;

.field private aU:Lfvs;

.field private aV:Ljava/lang/Runnable;

.field private aW:Ljava/lang/Runnable;

.field private aX:Lhoc;

.field private final aY:Liue;

.field private aa:Limw;

.field private ab:Limp;

.field private ac:Landroid/os/StrictMode$ThreadPolicy;

.field private ad:Liug;

.field private ae:Ling;

.field private af:Ljava/lang/String;

.field private ag:Ljava/lang/String;

.field private ah:I

.field private ai:Litz;

.field private aj:Landroid/view/View;

.field private ak:Landroid/view/View;

.field private al:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

.field private am:Lnhl;

.field private an:[Lnhm;

.field private ao:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnhm;",
            ">;"
        }
    .end annotation
.end field

.field private ap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private aq:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ar:[Lnhm;

.field private as:Liuq;

.field private aw:Lesy;

.field private ax:I

.field private ay:Ljava/lang/String;

.field private az:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187
    new-instance v0, Leto;

    invoke-direct {v0}, Leto;-><init>()V

    sput-object v0, Letn;->N:Lium;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 127
    invoke-direct {p0}, Llol;-><init>()V

    .line 212
    new-instance v0, Lhov;

    iget-object v1, p0, Letn;->av:Llqm;

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Letn;->X:Lhov;

    .line 214
    new-instance v0, Lhje;

    iget-object v1, p0, Letn;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Letn;->Y:Lhje;

    .line 220
    new-instance v0, Lhsx;

    iget-object v1, p0, Letn;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lhsx;-><init>(Llqr;Lhsw;)V

    .line 243
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Letn;->ao:Ljava/util/List;

    .line 244
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Letn;->ap:Ljava/util/Map;

    .line 245
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Letn;->aq:Ljava/util/HashSet;

    .line 281
    new-instance v0, Lett;

    invoke-direct {v0, p0}, Lett;-><init>(Letn;)V

    iput-object v0, p0, Letn;->aY:Liue;

    .line 2134
    return-void
.end method

.method static synthetic Y()I
    .locals 1

    .prologue
    .line 127
    sget v0, Letn;->S:I

    return v0
.end method

.method private Z()V
    .locals 1

    .prologue
    .line 941
    iget v0, p0, Letn;->aL:I

    if-nez v0, :cond_0

    .line 942
    iget v0, p0, Letn;->aL:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Letn;->aL:I

    invoke-direct {p0, v0}, Letn;->c(I)V

    .line 944
    :cond_0
    return-void
.end method

.method static synthetic a(Letn;F)F
    .locals 0

    .prologue
    .line 127
    iput p1, p0, Letn;->aG:F

    return p1
.end method

.method static synthetic a(Letn;Ling;)Ling;
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Letn;->ae:Ling;

    return-object p1
.end method

.method private a(Lnij;)Ling;
    .locals 6

    .prologue
    .line 1486
    if-eqz p1, :cond_0

    iget-object v0, p1, Lnij;->c:Ljava/lang/Double;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lnij;->d:Ljava/lang/Double;

    if-nez v0, :cond_1

    .line 1487
    :cond_0
    const/4 v0, 0x0

    .line 1489
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ling;

    iget-object v1, p1, Lnij;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iget-object v1, p1, Lnij;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Ling;-><init>(DD)V

    goto :goto_0
.end method

.method static synthetic a(Letn;Linl;)Liup;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Letn;->as:Liuq;

    invoke-virtual {v0, p1}, Liuq;->a(Linl;)Liup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Letn;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Letn;->ad()V

    return-void
.end method

.method static synthetic a(Letn;I)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Letn;->c(I)V

    return-void
.end method

.method static synthetic a(Letn;Lhmv;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Letn;->a(Lhmv;)V

    return-void
.end method

.method static synthetic a(Letn;Liup;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0, p1}, Letn;->a(Liup;)V

    return-void
.end method

.method private a(Lhmv;)V
    .locals 4

    .prologue
    .line 1974
    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 1975
    iget-object v0, p0, Letn;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Letn;->at:Llnl;

    invoke-direct {v2, v3, v1}, Lhmr;-><init>(Landroid/content/Context;I)V

    .line 1977
    invoke-virtual {v2, p1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 1978
    invoke-virtual {p0}, Letn;->F_()Lhmw;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    .line 1975
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1979
    return-void
.end method

.method private a(Liup;)V
    .locals 2

    .prologue
    .line 1418
    iget-object v0, p0, Letn;->ab:Limp;

    if-nez v0, :cond_1

    .line 1438
    :cond_0
    :goto_0
    return-void

    .line 1421
    :cond_1
    if-nez p1, :cond_2

    .line 1422
    invoke-direct {p0}, Letn;->ai()V

    goto :goto_0

    .line 1425
    :cond_2
    iget-object v0, p0, Letn;->az:Ljava/lang/String;

    invoke-direct {p0, v0}, Letn;->f(Ljava/lang/String;)Liup;

    move-result-object v0

    if-eq p1, v0, :cond_0

    .line 1430
    const/4 v0, 0x0

    iput-object v0, p0, Letn;->ay:Ljava/lang/String;

    .line 1431
    invoke-virtual {p1}, Liup;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Letn;->az:Ljava/lang/String;

    .line 1432
    iget-object v0, p0, Letn;->as:Liuq;

    invoke-virtual {v0, p1}, Liuq;->a(Liup;)V

    .line 1433
    invoke-virtual {p1}, Liup;->f()Ljava/util/List;

    move-result-object v0

    .line 1434
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lnhm;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnhm;

    invoke-direct {p0, v0}, Letn;->a([Lnhm;)V

    .line 1435
    invoke-virtual {p0}, Letn;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Letn;->c(Landroid/view/View;)V

    .line 1437
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Letn;->a(Liup;I)Z

    goto :goto_0
.end method

.method private a(Lt;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1114
    invoke-direct {p0}, Letn;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Letn;->aN:Z

    if-eqz v0, :cond_1

    .line 1119
    :cond_0
    :goto_0
    return-void

    .line 1117
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, p0, v0}, Lt;->a(Lu;I)V

    .line 1118
    invoke-virtual {p0}, Letn;->p()Lae;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lt;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a([Lnhm;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1577
    invoke-direct {p0}, Letn;->ak()Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1579
    iget-boolean v0, p0, Letn;->aP:Z

    .line 1580
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "show_actions"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "user_device_locations"

    new-instance v3, Lhyv;

    invoke-direct {v3, p1}, Lhyv;-><init>([Loxu;)V

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v0, Lesu;

    invoke-direct {v0}, Lesu;-><init>()V

    invoke-virtual {v0, v2}, Lesu;->f(Landroid/os/Bundle;)V

    .line 1581
    invoke-virtual {v0, p0}, Lesu;->a(Litn;)V

    .line 1583
    invoke-virtual {p0}, Letn;->q()Lae;

    move-result-object v2

    .line 1584
    invoke-virtual {v2}, Lae;->a()Lat;

    move-result-object v3

    const v4, 0x7f1002a8

    invoke-virtual {v3, v4, v0}, Lat;->b(ILu;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    .line 1585
    invoke-virtual {v2}, Lae;->b()Z

    .line 1587
    array-length v2, p1

    sget v0, Letn;->U:I

    mul-int/2addr v0, v2

    add-int/lit8 v3, v2, -0x1

    sget v4, Letn;->V:I

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    if-le v2, v7, :cond_0

    sget v2, Letn;->T:I

    add-int/2addr v0, v2

    :cond_0
    sget v3, Letn;->S:I

    iget v2, p0, Letn;->aF:I

    if-nez v2, :cond_3

    invoke-virtual {p0}, Letn;->x()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/2addr v0, v3

    iput v0, p0, Letn;->aH:I

    .line 1588
    iget-object v0, p0, Letn;->al:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    iget v1, p0, Letn;->aH:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->b(I)V

    .line 1591
    invoke-direct {p0}, Letn;->an()Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 1592
    return-void

    :cond_1
    move v2, v1

    .line 1587
    goto :goto_0

    :cond_2
    sget v1, Letn;->T:I

    sget v4, Letn;->U:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    sget v4, Letn;->U:I

    sget v5, Letn;->V:I

    add-int/2addr v4, v5

    div-int/lit8 v5, v2, 0x3

    sget v6, Letn;->W:I

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    sub-int/2addr v2, v5

    sub-int/2addr v2, v1

    div-int/2addr v2, v4

    const/4 v5, 0x5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    mul-int/2addr v2, v4

    add-int/2addr v1, v2

    iput v1, p0, Letn;->aF:I

    :cond_3
    iget v1, p0, Letn;->aF:I

    goto :goto_1
.end method

.method private a(II)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1445
    iget-object v0, p0, Letn;->ab:Limp;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    move v0, v2

    .line 1482
    :goto_0
    return v0

    .line 1449
    :cond_1
    iget-object v0, p0, Letn;->az:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1450
    invoke-direct {p0}, Letn;->aj()V

    .line 1452
    :cond_2
    iput p1, p0, Letn;->ax:I

    .line 1453
    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 1454
    const-string v3, "FriendLocations"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1455
    const-string v3, "selectUser: "

    iget-object v4, v0, Lnhm;->d:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1458
    :cond_3
    :goto_1
    iget-object v3, v0, Lnhm;->b:Ljava/lang/String;

    iget-object v4, p0, Letn;->ay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1459
    new-array v3, v1, [Lnhm;

    aput-object v0, v3, v2

    invoke-direct {p0, v3}, Letn;->a([Lnhm;)V

    .line 1460
    iget-object v3, v0, Lnhm;->b:Ljava/lang/String;

    iput-object v3, p0, Letn;->ay:Ljava/lang/String;

    .line 1461
    invoke-virtual {p0}, Letn;->x()Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Letn;->c(Landroid/view/View;)V

    .line 1464
    :cond_4
    invoke-direct {p0}, Letn;->af()F

    move-result v3

    int-to-float v4, p2

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_8

    .line 1466
    iget-object v0, p0, Letn;->ab:Limp;

    if-eqz v0, :cond_5

    if-ltz p1, :cond_5

    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_7

    .line 1468
    :cond_5
    :goto_2
    iget-object v0, p0, Letn;->ay:Ljava/lang/String;

    invoke-direct {p0, v0}, Letn;->e(Ljava/lang/String;)Liup;

    move-result-object v0

    .line 1469
    iget-object v2, p0, Letn;->as:Liuq;

    invoke-virtual {v2, v0}, Liuq;->a(Liup;)V

    :goto_3
    move v0, v1

    .line 1482
    goto :goto_0

    .line 1455
    :cond_6
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1466
    :cond_7
    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    iget-object v0, v0, Lnhm;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Letn;->e(Ljava/lang/String;)Liup;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Letn;->a(Liup;I)Z

    goto :goto_2

    .line 1473
    :cond_8
    iget-object v3, v0, Lnhm;->b:Ljava/lang/String;

    iput-object v3, p0, Letn;->ag:Ljava/lang/String;

    .line 1474
    iput p2, p0, Letn;->ah:I

    .line 1475
    iget-object v0, v0, Lnhm;->c:[Lnij;

    invoke-static {v0}, Liuo;->a([Lnij;)Lnij;

    move-result-object v0

    .line 1476
    invoke-direct {p0, v0}, Letn;->a(Lnij;)Ling;

    move-result-object v0

    .line 1480
    iget v3, p0, Letn;->ah:I

    invoke-direct {p0, v0, v3, v2}, Letn;->a(Ling;IZ)Z

    goto :goto_3
.end method

.method static synthetic a(Letn;Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Letn;->a(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Letn;Z)Z
    .locals 0

    .prologue
    .line 127
    iput-boolean p1, p0, Letn;->aK:Z

    return p1
.end method

.method private a(Ling;IZ)Z
    .locals 6

    .prologue
    .line 1368
    iget-object v0, p0, Letn;->ab:Limp;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 1369
    :cond_0
    const/4 v0, 0x0

    .line 1381
    :goto_0
    return v0

    .line 1372
    :cond_1
    if-eqz p3, :cond_3

    .line 1373
    iget-object v0, p0, Letn;->ab:Limp;

    invoke-interface {v0}, Limp;->a()Limu;

    move-result-object v1

    invoke-interface {v1, p1}, Limu;->a(Ling;)Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Liut;->a(Landroid/content/Context;)I

    move-result v3

    iget v0, p0, Letn;->aH:I

    invoke-direct {p0}, Letn;->ao()Z

    move-result v4

    if-nez v4, :cond_2

    sget v4, Letn;->S:I

    sub-int/2addr v0, v4

    :cond_2
    iget-boolean v4, p0, Letn;->aN:Z

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Letn;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Letn;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d016e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iget v5, v2, Landroid/graphics/Point;->y:I

    div-int/lit8 v0, v0, 0x2

    sub-int/2addr v0, v4

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v5

    iput v0, v2, Landroid/graphics/Point;->y:I

    :goto_1
    invoke-interface {v1, v2}, Limu;->a(Landroid/graphics/Point;)Ling;

    move-result-object p1

    .line 1376
    :cond_3
    const/4 v0, -0x1

    if-eq p2, v0, :cond_5

    iget-object v0, p0, Letn;->ab:Limp;

    invoke-interface {v0}, Limp;->c()Linc;

    move-result-object v0

    invoke-interface {v0}, Linc;->a()F

    move-result v0

    int-to-float v1, p2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    .line 1377
    iget-object v0, p0, Letn;->ab:Limp;

    invoke-interface {v0, p1, p2}, Limp;->b(Ling;I)V

    .line 1381
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1373
    :cond_4
    iget v4, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    iput v0, v2, Landroid/graphics/Point;->y:I

    goto :goto_1

    .line 1379
    :cond_5
    iget-object v0, p0, Letn;->ab:Limp;

    invoke-interface {v0, p1}, Limp;->a(Ling;)V

    goto :goto_2
.end method

.method private a(Liup;I)Z
    .locals 2

    .prologue
    .line 1360
    iget-object v0, p0, Letn;->ab:Limp;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 1361
    :cond_0
    const/4 v0, 0x0

    .line 1364
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Liup;->d()Ling;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p2, v1}, Letn;->a(Ling;IZ)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 1

    .prologue
    .line 1441
    invoke-direct {p0, p1}, Letn;->i(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0, p2}, Letn;->a(II)Z

    move-result v0

    return v0
.end method

.method private aa()Lfvs;
    .locals 4

    .prologue
    .line 976
    iget-object v0, p0, Letn;->aS:Lfvs;

    if-nez v0, :cond_0

    .line 977
    new-instance v0, Lety;

    const-wide/32 v2, 0x1d4c0

    invoke-direct {v0, p0, v2, v3}, Lety;-><init>(Letn;J)V

    iput-object v0, p0, Letn;->aS:Lfvs;

    .line 989
    :cond_0
    iget-object v0, p0, Letn;->aS:Lfvs;

    return-object v0
.end method

.method private ab()V
    .locals 3

    .prologue
    .line 993
    const/4 v0, 0x0

    .line 994
    invoke-direct {p0}, Letn;->ah()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 995
    iget-object v1, p0, Letn;->ay:Ljava/lang/String;

    invoke-direct {p0, v1}, Letn;->i(Ljava/lang/String;)I

    move-result v1

    .line 996
    if-ltz v1, :cond_0

    iget-object v2, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 997
    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 998
    const/4 v1, 0x1

    new-array v1, v1, [Lnhm;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    move-object v0, v1

    :cond_0
    move-object v1, v0

    .line 1009
    :goto_0
    if-eqz v1, :cond_1

    .line 1010
    invoke-virtual {p0}, Letn;->q()Lae;

    move-result-object v0

    const v2, 0x7f1002a8

    invoke-virtual {v0, v2}, Lae;->a(I)Lu;

    move-result-object v0

    check-cast v0, Lesu;

    .line 1011
    if-eqz v0, :cond_1

    .line 1012
    invoke-virtual {v0, v1}, Lesu;->a([Lnhm;)V

    .line 1016
    :cond_1
    const-string v0, "hflf_friend_list"

    invoke-direct {p0, v0}, Letn;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1017
    invoke-virtual {p0}, Letn;->p()Lae;

    move-result-object v0

    const-string v1, "hflf_friend_list"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lits;

    iget-object v1, p0, Letn;->ao:Ljava/util/List;

    .line 1018
    invoke-virtual {v0, v1}, Lits;->a(Ljava/util/List;)V

    .line 1020
    :cond_2
    return-void

    .line 1000
    :cond_3
    invoke-direct {p0}, Letn;->ag()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1001
    iget-object v1, p0, Letn;->az:Ljava/lang/String;

    invoke-direct {p0, v1}, Letn;->f(Ljava/lang/String;)Liup;

    move-result-object v1

    .line 1002
    if-eqz v1, :cond_4

    .line 1003
    invoke-virtual {v1}, Liup;->f()Ljava/util/List;

    move-result-object v0

    .line 1005
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lnhm;

    .line 1004
    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnhm;

    move-object v1, v0

    goto :goto_0

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method private ac()V
    .locals 1

    .prologue
    .line 1024
    iget-boolean v0, p0, Letn;->aN:Z

    if-nez v0, :cond_0

    .line 1025
    iget-object v0, p0, Letn;->Y:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 1027
    :cond_0
    return-void
.end method

.method private ad()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1031
    iget-boolean v0, p0, Letn;->aN:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Letn;->am:Lnhl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Letn;->ai:Litz;

    .line 1032
    invoke-virtual {v0}, Litz;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1061
    :cond_0
    :goto_0
    return-void

    .line 1036
    :cond_1
    invoke-virtual {p0}, Letn;->u()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1037
    iput-boolean v1, p0, Letn;->aI:Z

    goto :goto_0

    .line 1041
    :cond_2
    const/4 v0, 0x0

    .line 1043
    iget-boolean v2, p0, Letn;->aB:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Letn;->am:Lnhl;

    iget-object v2, v2, Lnhl;->b:Ljava/lang/Boolean;

    .line 1044
    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1045
    invoke-virtual {p0}, Letn;->W()V

    move v0, v1

    .line 1055
    :cond_3
    :goto_1
    if-nez v0, :cond_0

    .line 1056
    iget-boolean v0, p0, Letn;->aA:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Letn;->an:[Lnhm;

    if-eqz v0, :cond_4

    iget-object v0, p0, Letn;->an:[Lnhm;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 1058
    :cond_4
    invoke-virtual {p0}, Letn;->e()V

    goto :goto_0

    .line 1047
    :cond_5
    iget-boolean v2, p0, Letn;->aC:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Letn;->am:Lnhl;

    iget-object v2, v2, Lnhl;->c:Ljava/lang/Boolean;

    .line 1048
    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1049
    invoke-direct {p0}, Letn;->ae()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1050
    iget-object v0, p0, Letn;->ai:Litz;

    sget-object v2, Letn;->N:Lium;

    invoke-virtual {v0, v2}, Litz;->a(Lium;)Z

    .line 1051
    iput-boolean v1, p0, Letn;->aC:Z

    const-string v0, "shown_reporting_dialog:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v3, "account_name"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {p0}, Letn;->as()Lhoc;

    move-result-object v2

    new-instance v3, Liuv;

    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v4

    iget-boolean v5, p0, Letn;->aC:Z

    invoke-direct {v3, v4, v0, v5}, Liuv;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {v2, v3}, Lhoc;->b(Lhny;)V

    move v0, v1

    .line 1052
    goto :goto_1

    .line 1051
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private ae()Z
    .locals 1

    .prologue
    .line 1130
    const-string v0, "hflf_no_shares"

    invoke-direct {p0, v0}, Letn;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hflf_all_filtered"

    .line 1131
    invoke-direct {p0, v0}, Letn;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hflf_enable_sharing"

    .line 1132
    invoke-direct {p0, v0}, Letn;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hflf_friend_list"

    .line 1133
    invoke-direct {p0, v0}, Letn;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Letn;->ai:Litz;

    .line 1134
    invoke-virtual {v0}, Litz;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private af()F
    .locals 1

    .prologue
    .line 1343
    iget-object v0, p0, Letn;->ab:Limp;

    if-nez v0, :cond_0

    .line 1344
    const/4 v0, 0x0

    .line 1346
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Letn;->ab:Limp;

    invoke-interface {v0}, Limp;->c()Linc;

    move-result-object v0

    invoke-interface {v0}, Linc;->a()F

    move-result v0

    goto :goto_0
.end method

.method private ag()Z
    .locals 1

    .prologue
    .line 1503
    iget-object v0, p0, Letn;->az:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ah()Z
    .locals 1

    .prologue
    .line 1507
    iget-object v0, p0, Letn;->ay:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ai()V
    .locals 4

    .prologue
    .line 1517
    invoke-direct {p0}, Letn;->an()Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1518
    iget-object v0, p0, Letn;->al:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1519
    iget-object v0, p0, Letn;->al:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->c()V

    .line 1520
    invoke-direct {p0}, Letn;->ak()Ljava/lang/Runnable;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 1522
    :cond_0
    sget v0, Letn;->S:I

    iput v0, p0, Letn;->aH:I

    .line 1524
    const/4 v0, 0x0

    iput-object v0, p0, Letn;->ay:Ljava/lang/String;

    .line 1525
    invoke-direct {p0}, Letn;->aj()V

    .line 1526
    return-void
.end method

.method private aj()V
    .locals 1

    .prologue
    .line 1536
    iget-object v0, p0, Letn;->as:Liuq;

    invoke-virtual {v0}, Liuq;->a()V

    .line 1537
    const/4 v0, 0x0

    iput-object v0, p0, Letn;->az:Ljava/lang/String;

    .line 1538
    invoke-virtual {p0}, Letn;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Letn;->c(Landroid/view/View;)V

    .line 1539
    return-void
.end method

.method private ak()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 1542
    iget-object v0, p0, Letn;->aW:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1543
    new-instance v0, Leua;

    invoke-direct {v0, p0}, Leua;-><init>(Letn;)V

    iput-object v0, p0, Letn;->aW:Ljava/lang/Runnable;

    .line 1557
    :cond_0
    iget-object v0, p0, Letn;->aW:Ljava/lang/Runnable;

    return-object v0
.end method

.method private al()Z
    .locals 1

    .prologue
    .line 1561
    invoke-virtual {p0}, Letn;->t()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Letn;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private am()V
    .locals 7

    .prologue
    .line 1565
    iget-object v0, p0, Letn;->am:Lnhl;

    if-nez v0, :cond_0

    .line 1574
    :goto_0
    return-void

    .line 1570
    :cond_0
    sget-object v0, Lhmv;->cT:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    .line 1571
    iget-object v2, p0, Letn;->at:Llnl;

    iget-object v0, p0, Letn;->Z:Lhee;

    .line 1572
    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    iget-object v4, p0, Letn;->aq:Ljava/util/HashSet;

    new-instance v5, Ljava/util/HashMap;

    iget-object v0, p0, Letn;->ap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/HashMap;-><init>(I)V

    iget-object v0, p0, Letn;->ap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const-string v0, "FriendLocations"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x16

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "circleToUserCountMap: "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1571
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/locations/FriendLocationsCircleFilterActivity;

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "circle_ids"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "sharing_user_count"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1573
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Letn;->a(Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method

.method private an()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 1601
    iget-object v0, p0, Letn;->aV:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1602
    new-instance v0, Letp;

    invoke-direct {v0, p0}, Letp;-><init>(Letn;)V

    iput-object v0, p0, Letn;->aV:Ljava/lang/Runnable;

    .line 1611
    :cond_0
    iget-object v0, p0, Letn;->aV:Ljava/lang/Runnable;

    return-object v0
.end method

.method private ao()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1651
    iget-object v1, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1661
    :cond_0
    :goto_0
    return v0

    .line 1655
    :cond_1
    invoke-virtual {p0}, Letn;->x()Landroid/view/View;

    move-result-object v1

    .line 1656
    if-eqz v1, :cond_0

    .line 1660
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget v2, p0, Letn;->aH:I

    sub-int/2addr v1, v2

    .line 1661
    sget v2, Letn;->W:I

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private ap()V
    .locals 1

    .prologue
    .line 1691
    iget-object v0, p0, Letn;->aE:Line;

    if-eqz v0, :cond_0

    .line 1692
    iget-object v0, p0, Letn;->aE:Line;

    invoke-interface {v0}, Line;->a()V

    .line 1694
    :cond_0
    return-void
.end method

.method private aq()Z
    .locals 2

    .prologue
    .line 1705
    iget-object v0, p0, Letn;->ab:Limp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Letn;->ab:Limp;

    invoke-interface {v0}, Limp;->d()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ar()Lfvs;
    .locals 4

    .prologue
    .line 1831
    iget-object v0, p0, Letn;->aT:Lfvs;

    if-nez v0, :cond_0

    .line 1833
    new-instance v0, Letq;

    const-wide/16 v2, 0xc8

    invoke-direct {v0, p0, v2, v3}, Letq;-><init>(Letn;J)V

    iput-object v0, p0, Letn;->aT:Lfvs;

    .line 1848
    :cond_0
    iget-object v0, p0, Letn;->aT:Lfvs;

    return-object v0
.end method

.method private as()Lhoc;
    .locals 2

    .prologue
    .line 2149
    iget-object v0, p0, Letn;->aX:Lhoc;

    if-nez v0, :cond_0

    .line 2150
    iget-object v0, p0, Letn;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Letn;->aX:Lhoc;

    .line 2152
    :cond_0
    iget-object v0, p0, Letn;->aX:Lhoc;

    return-object v0
.end method

.method static synthetic b(Letn;)I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Letn;->aL:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Letn;->aL:I

    return v0
.end method

.method private c(I)V
    .locals 8

    .prologue
    .line 947
    invoke-direct {p0}, Letn;->as()Lhoc;

    move-result-object v7

    .line 948
    const-string v0, "GetFriendLocationsTask"

    invoke-virtual {v7, v0}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 973
    :goto_0
    return-void

    .line 951
    :cond_0
    iget-object v0, p0, Letn;->ai:Litz;

    invoke-virtual {v0}, Litz;->k()V

    .line 956
    invoke-virtual {p0}, Letn;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 957
    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 958
    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 961
    const/4 v3, 0x0

    .line 963
    :cond_1
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v1

    .line 964
    if-eqz v1, :cond_2

    .line 965
    new-instance v0, Ldoy;

    iget-object v2, p0, Letn;->Z:Lhee;

    .line 966
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v5, 0x1

    iget-object v6, p0, Letn;->an:[Lnhm;

    move v4, p1

    invoke-direct/range {v0 .. v6}, Ldoy;-><init>(Landroid/content/Context;ILjava/lang/String;IZ[Lnhm;)V

    .line 965
    invoke-virtual {v7, v0}, Lhoc;->b(Lhny;)V

    .line 969
    invoke-direct {p0}, Letn;->ac()V

    .line 972
    :cond_2
    invoke-direct {p0}, Letn;->aa()Lfvs;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lfvs;->a(J)V

    goto :goto_0
.end method

.method private c(Landroid/view/View;)V
    .locals 9

    .prologue
    const/16 v4, 0xe

    const/16 v3, 0x8

    const/4 v8, 0x4

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 747
    if-nez p1, :cond_0

    .line 795
    :goto_0
    return-void

    .line 751
    :cond_0
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 752
    iget-object v2, p0, Letn;->ab:Limp;

    if-eqz v2, :cond_1

    iget-object v2, p0, Letn;->ae:Ling;

    if-nez v2, :cond_2

    iget-object v2, p0, Letn;->an:[Lnhm;

    if-nez v2, :cond_2

    .line 754
    :cond_1
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 755
    const v1, 0x7f10025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 756
    const v1, 0x7f100260

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 758
    iget-object v0, p0, Letn;->ak:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 759
    iget-object v0, p0, Letn;->aj:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 764
    :cond_2
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 765
    iget-object v0, p0, Letn;->ak:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    .line 768
    iget-object v0, p0, Letn;->ae:Ling;

    if-eqz v0, :cond_8

    .line 769
    iget-object v0, p0, Letn;->ab:Limp;

    iget-object v2, p0, Letn;->ae:Ling;

    invoke-interface {v0, v2, v4}, Limp;->a(Ling;I)V

    .line 773
    :cond_3
    iget-object v0, p0, Letn;->ak:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 774
    iget-object v2, p0, Letn;->aj:Landroid/view/View;

    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    .line 775
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v8

    .line 774
    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 778
    :cond_4
    invoke-direct {p0}, Letn;->ah()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-direct {p0}, Letn;->ag()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_5
    move v0, v1

    .line 781
    :goto_2
    iget-object v2, p0, Letn;->ab:Limp;

    invoke-interface {v2}, Limp;->b()Lina;

    move-result-object v3

    .line 782
    if-nez v0, :cond_c

    move v2, v1

    :goto_3
    invoke-interface {v3, v2}, Lina;->a(Z)V

    .line 784
    if-eqz v0, :cond_e

    .line 785
    invoke-direct {p0}, Letn;->ap()V

    const/4 v0, 0x0

    iget-object v2, p0, Letn;->az:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v0, p0, Letn;->az:Ljava/lang/String;

    invoke-direct {p0, v0}, Letn;->f(Ljava/lang/String;)Liup;

    move-result-object v0

    :cond_6
    :goto_4
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Liup;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v1, :cond_7

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    iget-object v0, v0, Lnhm;->c:[Lnij;

    invoke-static {v0}, Liuo;->a([Lnij;)Lnij;

    move-result-object v2

    iget-object v0, v2, Lnij;->f:Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Letn;->ab:Limp;

    invoke-direct {p0, v2}, Letn;->a(Lnij;)Ling;

    move-result-object v1

    iget-object v2, v2, Lnij;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-double v2, v2

    sget v4, Letn;->P:I

    sget v5, Letn;->Q:I

    sget v6, Letn;->R:I

    invoke-interface/range {v0 .. v6}, Limp;->a(Ling;DIII)Line;

    move-result-object v0

    iput-object v0, p0, Letn;->aE:Line;

    .line 787
    :cond_7
    iget-object v0, p0, Letn;->ab:Limp;

    invoke-interface {v0, v7}, Limp;->a(Z)V

    .line 794
    :goto_5
    iget-object v0, p0, Letn;->aj:Landroid/view/View;

    invoke-direct {p0}, Letn;->ao()Z

    move-result v1

    if-eqz v1, :cond_f

    :goto_6
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 770
    :cond_8
    iget v0, p0, Letn;->aQ:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_9

    move v0, v1

    :goto_7
    if-nez v0, :cond_3

    .line 771
    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Letn;->ag:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v7

    :goto_8
    iget-object v2, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    invoke-direct {p0, v0, v4}, Letn;->a(II)Z

    move-result v2

    if-nez v2, :cond_3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    move v0, v7

    .line 770
    goto :goto_7

    :cond_a
    move v0, v7

    .line 775
    goto/16 :goto_1

    :cond_b
    move v0, v7

    .line 778
    goto/16 :goto_2

    :cond_c
    move v2, v7

    .line 782
    goto/16 :goto_3

    .line 785
    :cond_d
    iget-object v2, p0, Letn;->ay:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v0, p0, Letn;->ay:Ljava/lang/String;

    invoke-direct {p0, v0}, Letn;->e(Ljava/lang/String;)Liup;

    move-result-object v0

    goto/16 :goto_4

    .line 789
    :cond_e
    invoke-direct {p0}, Letn;->ap()V

    .line 790
    iget-object v0, p0, Letn;->ab:Limp;

    invoke-interface {v0, v1}, Limp;->a(Z)V

    goto :goto_5

    :cond_f
    move v7, v8

    .line 794
    goto :goto_6
.end method

.method static synthetic c(Letn;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Letn;->ai()V

    return-void
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 1957
    .line 1958
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Letn;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 1957
    invoke-static {v0, v1}, Leyq;->c(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 1959
    const-string v1, "start_wizard"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1960
    if-eqz p1, :cond_0

    .line 1961
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Letn;->a(Landroid/content/Intent;I)V

    .line 1966
    :goto_0
    return-void

    .line 1964
    :cond_0
    invoke-virtual {p0, v0}, Letn;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic d(Letn;)F
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Letn;->aG:F

    return v0
.end method

.method private e(Ljava/lang/String;)Liup;
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Letn;->as:Liuq;

    invoke-virtual {v0, p1}, Liuq;->a(Ljava/lang/String;)Liup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Letn;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Letn;->ag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Letn;)Lfvs;
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Letn;->ar()Lfvs;

    move-result-object v0

    return-object v0
.end method

.method private f(Ljava/lang/String;)Liup;
    .locals 1

    .prologue
    .line 618
    iget-object v0, p0, Letn;->as:Liuq;

    invoke-virtual {v0, p1}, Liuq;->b(Ljava/lang/String;)Liup;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Letn;)Liuq;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Letn;->as:Liuq;

    return-object v0
.end method

.method private g(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1122
    .line 1123
    invoke-virtual {p0}, Letn;->p()Lae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 1124
    if-eqz v0, :cond_0

    .line 1125
    invoke-virtual {v0}, Lt;->a()V

    .line 1127
    :cond_0
    return-void
.end method

.method static synthetic h(Letn;)Lfvs;
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Letn;->aU:Lfvs;

    if-nez v0, :cond_0

    new-instance v0, Letr;

    const-wide/16 v2, 0x1f4

    invoke-direct {v0, p0, v2, v3}, Letr;-><init>(Letn;J)V

    iput-object v0, p0, Letn;->aU:Lfvs;

    :cond_0
    iget-object v0, p0, Letn;->aU:Lfvs;

    return-object v0
.end method

.method private h(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1138
    invoke-virtual {p0}, Letn;->p()Lae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 1493
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1494
    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 1495
    iget-object v0, v0, Lnhm;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1499
    :goto_1
    return v1

    .line 1493
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1499
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method static synthetic i(Letn;)Z
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Letn;->al()Z

    move-result v0

    return v0
.end method

.method static synthetic j(Letn;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Letn;->Z()V

    return-void
.end method

.method private j(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2101
    sget-object v0, Lhmv;->du:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    .line 2102
    invoke-virtual {p0}, Letn;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1002aa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2103
    const v0, 0x7f100139

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2104
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2105
    new-instance v0, Lets;

    invoke-direct {v0, v1}, Lets;-><init>(Landroid/view/View;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2111
    invoke-static {v1}, Leud;->a(Landroid/view/View;)V

    .line 2112
    return-void
.end method

.method static synthetic k(Letn;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 127
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Letn;->aq:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Letn;->ap:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_0

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Letn;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "gaia_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Letn;->ar:[Lnhm;

    if-eqz v0, :cond_5

    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    iget-object v2, p0, Letn;->ar:[Lnhm;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_3
    invoke-virtual {p0}, Letn;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Letn;->c(Landroid/view/View;)V

    invoke-direct {p0}, Letn;->ar()Lfvs;

    move-result-object v0

    iget-object v0, v0, Lfvs;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iget-boolean v0, p0, Letn;->aJ:Z

    if-eqz v0, :cond_4

    iput-boolean v1, p0, Letn;->aJ:Z

    invoke-direct {p0}, Letn;->ab()V

    :cond_4
    return-void

    :cond_5
    iget-object v0, p0, Letn;->am:Lnhl;

    if-eqz v0, :cond_7

    iget-object v0, p0, Letn;->am:Lnhl;

    iget-object v0, v0, Lnhl;->a:[Lnij;

    if-eqz v0, :cond_7

    iget-object v0, p0, Letn;->am:Lnhl;

    iget-object v0, v0, Lnhl;->a:[Lnij;

    array-length v0, v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Letn;->am:Lnhl;

    iget-object v0, v0, Lnhl;->b:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lnhm;

    invoke-direct {v0}, Lnhm;-><init>()V

    iget-object v3, p0, Letn;->Z:Lhee;

    invoke-interface {v3}, Lhee;->g()Lhej;

    move-result-object v3

    const-string v4, "gaia_id"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lnhm;->b:Ljava/lang/String;

    const-string v4, "profile_photo_url"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lnhm;->e:Ljava/lang/String;

    const-string v4, "display_name"

    invoke-interface {v3, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lnhm;->d:Ljava/lang/String;

    iget-object v3, p0, Letn;->am:Lnhl;

    iget-object v3, v3, Lnhl;->a:[Lnij;

    iput-object v3, v0, Lnhm;->c:[Lnij;

    iget-object v3, v0, Lnhm;->c:[Lnij;

    invoke-static {v3}, Liuo;->a([Lnij;)Lnij;

    move-result-object v3

    if-eqz v3, :cond_6

    iget-object v4, p0, Letn;->ae:Ling;

    if-eqz v4, :cond_6

    iget-object v4, p0, Letn;->ae:Ling;

    iget-wide v4, v4, Ling;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    iput-object v4, v3, Lnij;->c:Ljava/lang/Double;

    iget-object v4, p0, Letn;->ae:Ling;

    iget-wide v4, v4, Ling;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    iput-object v4, v3, Lnij;->d:Ljava/lang/Double;

    :cond_6
    iget-object v3, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    iget-object v0, p0, Letn;->an:[Lnhm;

    if-eqz v0, :cond_3

    iget-object v3, p0, Letn;->an:[Lnhm;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    iget-object v6, v5, Lnhm;->b:Ljava/lang/String;

    invoke-interface {v2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private k(Landroid/os/Bundle;)Z
    .locals 4

    .prologue
    .line 1325
    const-string v0, "circle_ids"

    .line 1326
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 1328
    iget-object v1, p0, Letn;->aq:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1329
    const/4 v0, 0x0

    .line 1339
    :goto_0
    return v0

    .line 1332
    :cond_0
    const-string v1, "FriendLocations"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1333
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Update selected circles: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1335
    :cond_1
    iget-object v1, p0, Letn;->aq:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 1336
    if-eqz v0, :cond_2

    .line 1337
    iget-object v1, p0, Letn;->aq:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 1339
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic l(Letn;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    iget-boolean v2, p0, Letn;->aA:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Letn;->an:[Lnhm;

    if-eqz v2, :cond_0

    iget-object v2, p0, Letn;->an:[Lnhm;

    array-length v2, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Letn;->U()V

    :cond_0
    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v0, :cond_2

    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v3, "gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    iget-object v0, v0, Lnhm;->b:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static synthetic m(Letn;)Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Letn;->al:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    return-object v0
.end method

.method static synthetic n(Letn;)Z
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Letn;->ah()Z

    move-result v0

    return v0
.end method

.method static synthetic o(Letn;)Z
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Letn;->ag()Z

    move-result v0

    return v0
.end method

.method static synthetic p(Letn;)Ljava/util/List;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    return-object v0
.end method

.method static synthetic q(Letn;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 127
    iget-object v0, p0, Letn;->as:Liuq;

    invoke-virtual {v0}, Liuq;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Letn;->ag:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Letn;->af()F

    move-result v0

    iget v1, p0, Letn;->ah:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    iget-object v0, p0, Letn;->ag:Ljava/lang/String;

    iput-object v3, p0, Letn;->ag:Ljava/lang/String;

    iget v1, p0, Letn;->ah:I

    invoke-direct {p0, v0, v1}, Letn;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Letn;->aD:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Letn;->aD:Z

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Letn;->aD:Z

    iget-object v0, p0, Letn;->ag:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Letn;->ag:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-direct {p0, v0, v1}, Letn;->a(Ljava/lang/String;I)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Letn;->az:Ljava/lang/String;

    invoke-direct {p0, v0}, Letn;->f(Ljava/lang/String;)Liup;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v3, p0, Letn;->az:Ljava/lang/String;

    invoke-direct {p0, v0}, Letn;->a(Liup;)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Letn;->ag()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Letn;->az:Ljava/lang/String;

    invoke-direct {p0, v0}, Letn;->f(Ljava/lang/String;)Liup;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Letn;->ai()V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Letn;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Letn;->ay:Ljava/lang/String;

    invoke-direct {p0, v0}, Letn;->e(Ljava/lang/String;)Liup;

    move-result-object v0

    iget-object v1, p0, Letn;->as:Liuq;

    invoke-virtual {v1, v0}, Liuq;->a(Liup;)V

    goto :goto_0
.end method

.method static synthetic r(Letn;)Ling;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Letn;->ae:Ling;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Letn;->ai:Litz;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Letn;->ai:Litz;

    invoke-virtual {v0}, Litz;->c()V

    .line 462
    :cond_0
    iget-object v0, p0, Letn;->as:Liuq;

    if-eqz v0, :cond_1

    .line 463
    iget-object v0, p0, Letn;->as:Liuq;

    invoke-virtual {v0}, Liuq;->b()V

    .line 466
    :cond_1
    iget-object v0, p0, Letn;->ac:Landroid/os/StrictMode$ThreadPolicy;

    if-eqz v0, :cond_2

    .line 467
    iget-object v0, p0, Letn;->ac:Landroid/os/StrictMode$ThreadPolicy;

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 469
    :cond_2
    invoke-super {p0}, Llol;->A()V

    .line 470
    return-void
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 1970
    sget-object v0, Lhmw;->R:Lhmw;

    return-object v0
.end method

.method public U()V
    .locals 4

    .prologue
    .line 1087
    const v0, 0x7f0a0a8a

    const v1, 0x7f0a0a8b

    const/4 v2, 0x0

    const v3, 0x7f0a0a8c

    invoke-static {v0, v1, v2, v3}, Litv;->a(IILjava/lang/String;I)Litv;

    move-result-object v0

    .line 1091
    const/4 v1, 0x1

    iput-boolean v1, p0, Letn;->aA:Z

    .line 1092
    const-string v1, "hflf_all_filtered"

    invoke-direct {p0, v0, v1}, Letn;->a(Lt;Ljava/lang/String;)V

    .line 1093
    return-void
.end method

.method public V()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 882
    iget-boolean v1, p0, Letn;->aN:Z

    if-nez v1, :cond_1

    invoke-direct {p0}, Letn;->ah()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Letn;->ag()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 883
    :cond_0
    invoke-direct {p0}, Letn;->ai()V

    .line 894
    :goto_0
    return v0

    .line 888
    :cond_1
    iget-boolean v1, p0, Letn;->aN:Z

    if-eqz v1, :cond_2

    .line 889
    iget-object v1, p0, Letn;->aw:Lesy;

    invoke-virtual {v1}, Lesy;->c()V

    goto :goto_0

    .line 894
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public W()V
    .locals 5

    .prologue
    .line 1096
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    const-string v1, "plus_location"

    const-string v2, "https://support.google.com/plus/?hl=%locale%"

    invoke-static {v0, v1, v2}, Litk;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1098
    const v1, 0x7f0a0a8d

    const v2, 0x7f0a03f5

    .line 1100
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0a0a8e

    .line 1098
    invoke-static {v1, v2, v0, v3}, Litv;->a(IILjava/lang/String;I)Litv;

    move-result-object v0

    .line 1102
    const-string v1, "hflf_enable_sharing"

    invoke-direct {p0, v0, v1}, Letn;->a(Lt;Ljava/lang/String;)V

    .line 1103
    const/4 v0, 0x1

    iput-boolean v0, p0, Letn;->aB:Z

    const-string v0, "shown_sharing_dialog:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0}, Letn;->as()Lhoc;

    move-result-object v1

    new-instance v2, Liuv;

    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v3

    iget-boolean v4, p0, Letn;->aB:Z

    invoke-direct {v2, v3, v0, v4}, Liuv;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Lhoc;->b(Lhny;)V

    .line 1104
    return-void

    .line 1103
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public X()V
    .locals 2

    .prologue
    .line 1108
    iget-object v0, p0, Letn;->ao:Ljava/util/List;

    iget-boolean v1, p0, Letn;->aP:Z

    .line 1109
    invoke-static {v0, v1}, Lits;->a(Ljava/util/List;Z)Lits;

    move-result-object v0

    .line 1110
    const-string v1, "hflf_friend_list"

    invoke-direct {p0, v0, v1}, Letn;->a(Lt;Ljava/lang/String;)V

    .line 1111
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const v5, 0x7f1002a7

    .line 373
    const v0, 0x7f0400af

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 376
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v1

    .line 379
    instance-of v0, v1, Lcom/google/android/apps/plus/phone/HomeActivity;

    if-eqz v0, :cond_0

    .line 380
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 381
    instance-of v2, v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v2, :cond_0

    .line 382
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    int-to-float v2, v2

    .line 383
    invoke-static {v1}, Ljgh;->a(Landroid/content/Context;)F

    move-result v4

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 386
    :cond_0
    invoke-virtual {p0}, Letn;->q()Lae;

    move-result-object v2

    iget-object v0, p0, Letn;->au:Llnh;

    const-class v4, Limx;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Limx;

    invoke-virtual {v2, v5}, Lae;->a(I)Lu;

    move-result-object v4

    invoke-interface {v0, v4}, Limx;->a(Lu;)Limw;

    move-result-object v4

    iput-object v4, p0, Letn;->aa:Limw;

    iget-object v4, p0, Letn;->aa:Limw;

    invoke-interface {v4}, Limw;->a()Lu;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-interface {v0, v6}, Limx;->a(Z)Limw;

    move-result-object v0

    iput-object v0, p0, Letn;->aa:Limw;

    invoke-virtual {v2}, Lae;->a()Lat;

    move-result-object v0

    iget-object v2, p0, Letn;->aa:Limw;

    invoke-interface {v2}, Limw;->a()Lu;

    move-result-object v2

    invoke-virtual {v0, v5, v2}, Lat;->b(ILu;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->c()I

    :cond_1
    const v0, 0x7f100190

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Letn;->aj:Landroid/view/View;

    iget-object v0, p0, Letn;->aj:Landroid/view/View;

    const v2, 0x7f10029d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Letn;->aj:Landroid/view/View;

    const v2, 0x7f10029e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Letn;->aj:Landroid/view/View;

    const v2, 0x7f10029f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Letn;->aj:Landroid/view/View;

    const v2, 0x3f4ccccd    # 0.8f

    invoke-static {v0, v2}, Llsj;->a(Landroid/view/View;F)V

    const v0, 0x7f10015e

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    iput-object v0, p0, Letn;->al:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    iget-object v0, p0, Letn;->al:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;->a(Llix;)V

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Letn;->ak:Landroid/view/View;

    const v0, 0x7f10015f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v0, 0x7f1002a9

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/location/FriendLocationsTouchHandler;

    iget-object v4, p0, Letn;->ak:Landroid/view/View;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/location/FriendLocationsTouchHandler;->a(Landroid/view/View;)V

    iget-object v4, p0, Letn;->aj:Landroid/view/View;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/location/FriendLocationsTouchHandler;->b(Landroid/view/View;)V

    iget-object v4, p0, Letn;->al:Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/location/FriendLocationsTouchHandler;->a(Lcom/google/android/libraries/social/ui/views/DeprecatedExpandingScrollView;)V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/location/FriendLocationsTouchHandler;->c(Landroid/view/View;)V

    new-instance v0, Lesy;

    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v2

    const v4, 0x7f1002ab

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Letn;->Z:Lhee;

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    invoke-direct {v0, v2, v4, v5, p0}, Lesy;-><init>(Landroid/content/Context;Landroid/view/View;ILeth;)V

    iput-object v0, p0, Letn;->aw:Lesy;

    iget v0, p0, Letn;->aM:I

    if-ne v0, v7, :cond_4

    iget-boolean v0, p0, Letn;->aO:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Letn;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "notification_payload"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x0

    :try_start_0
    new-instance v4, Llwa;

    invoke-direct {v4}, Llwa;-><init>()V

    invoke-static {v4, v0}, Llwa;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Llwa;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Letn;->aw:Lesy;

    invoke-virtual {v2, v0}, Lesy;->a(Llwa;)V

    :cond_2
    :goto_1
    invoke-direct {p0, v3}, Letn;->c(Landroid/view/View;)V

    .line 388
    instance-of v0, v1, Ljgf;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 389
    check-cast v0, Ljgf;

    .line 390
    const-string v1, "locations"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v1, v4, v5}, Ljgf;->a(Ljava/lang/String;J)Z

    .line 393
    :cond_3
    return-object v3

    .line 386
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Loxt;->printStackTrace()V

    move-object v0, v2

    goto :goto_0

    :cond_4
    iget v0, p0, Letn;->aM:I

    if-lez v0, :cond_2

    sget-object v0, Lhmv;->dh:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    iput-boolean v7, p0, Letn;->aO:Z

    goto :goto_1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1252
    packed-switch p1, :pswitch_data_0

    .line 1259
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1254
    :pswitch_0
    new-instance v0, Lesv;

    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Letn;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Letn;->an:[Lnhm;

    invoke-direct {v0, v1, v2, v3}, Lesv;-><init>(Landroid/content/Context;I[Lnhm;)V

    goto :goto_0

    .line 1252
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2022
    iput-boolean v1, p0, Letn;->aN:Z

    .line 2023
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    invoke-virtual {v0, v1}, Lz;->setRequestedOrientation(I)V

    .line 2024
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    .line 2025
    invoke-virtual {v0}, Loo;->f()V

    .line 2026
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 803
    invoke-super {p0, p1, p2, p3}, Llol;->a(IILandroid/content/Intent;)V

    .line 805
    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 807
    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    .line 833
    :cond_0
    :goto_0
    return-void

    .line 810
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 812
    :pswitch_0
    invoke-direct {p0}, Letn;->Z()V

    goto :goto_0

    .line 817
    :pswitch_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v1}, Letn;->k(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 818
    sget-object v1, Lhmv;->cU:Lhmv;

    invoke-direct {p0, v1}, Letn;->a(Lhmv;)V

    .line 820
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v1

    iget-object v2, p0, Letn;->aq:Ljava/util/HashSet;

    invoke-static {v1, v0, v2}, Ldhv;->a(Landroid/content/Context;ILjava/util/HashSet;)V

    .line 822
    invoke-virtual {p0}, Letn;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 810
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(ILjava/lang/String;ZZ)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 2069
    if-eqz p4, :cond_0

    .line 2070
    iput-boolean v5, p0, Letn;->aB:Z

    .line 2071
    iput-boolean v5, p0, Letn;->aC:Z

    .line 2073
    iget-object v0, p0, Letn;->ai:Litz;

    invoke-virtual {v0}, Litz;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2074
    iget-object v0, p0, Letn;->ai:Litz;

    invoke-virtual {v0}, Litz;->j()V

    .line 2078
    :cond_0
    if-eqz p3, :cond_3

    .line 2079
    if-ne p1, v5, :cond_2

    .line 2080
    sget-object v0, Lhmv;->dr:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    .line 2084
    :goto_0
    invoke-direct {p0}, Letn;->as()Lhoc;

    move-result-object v6

    new-instance v0, Ldpf;

    iget-object v1, p0, Letn;->at:Llnl;

    iget-object v2, p0, Letn;->Z:Lhee;

    .line 2085
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    move-object v3, p2

    move v4, p1

    move v5, p4

    invoke-direct/range {v0 .. v5}, Ldpf;-><init>(Landroid/content/Context;ILjava/lang/String;IZ)V

    .line 2084
    invoke-virtual {v6, v0}, Lhoc;->b(Lhny;)V

    .line 2090
    :cond_1
    :goto_1
    return-void

    .line 2082
    :cond_2
    sget-object v0, Lhmv;->ds:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    goto :goto_0

    .line 2086
    :cond_3
    if-eqz p4, :cond_1

    iget-object v0, p0, Letn;->am:Lnhl;

    iget-object v0, v0, Lnhl;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2087
    invoke-direct {p0}, Letn;->as()Lhoc;

    move-result-object v6

    iget-object v1, p0, Letn;->at:Llnl;

    iget-object v0, p0, Letn;->Z:Lhee;

    .line 2088
    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 2087
    new-instance v0, Ldpf;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct/range {v0 .. v5}, Ldpf;-><init>(Landroid/content/Context;ILjava/lang/String;IZ)V

    invoke-virtual {v6, v0}, Lhoc;->b(Lhny;)V

    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 291
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 293
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v1

    .line 294
    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    .line 296
    invoke-static {v1, v2}, Leuc;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 297
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 368
    :goto_0
    return-void

    .line 301
    :cond_0
    iget-object v0, p0, Letn;->au:Llnh;

    const-class v3, Lieh;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 302
    sget-object v3, Ldxd;->c:Lief;

    .line 303
    invoke-interface {v0, v3, v2}, Lieh;->b(Lief;I)Z

    move-result v0

    iput-boolean v0, p0, Letn;->aP:Z

    .line 305
    sget-boolean v0, Letn;->O:Z

    if-nez v0, :cond_1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d0354

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Letn;->P:I

    const v3, 0x7f0b0317

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Letn;->Q:I

    const v3, 0x7f0b0318

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Letn;->R:I

    const v3, 0x7f0d034c

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Letn;->S:I

    const v3, 0x7f0d0180

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    shl-int/lit8 v3, v3, 0x1

    const v4, 0x7f0d017f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    add-int/2addr v3, v4

    sput v3, Letn;->T:I

    const v3, 0x7f0d017e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Letn;->V:I

    const v3, 0x7f0d016b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Letn;->U:I

    const v3, 0x7f0d034f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1}, Liut;->a(Landroid/content/Context;)I

    move-result v3

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v3

    sput v0, Letn;->W:I

    sput-boolean v6, Letn;->O:Z

    .line 309
    :cond_1
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskReads()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    iput-object v0, p0, Letn;->ac:Landroid/os/StrictMode$ThreadPolicy;

    .line 310
    invoke-static {}, Landroid/os/StrictMode;->allowThreadDiskWrites()Landroid/os/StrictMode$ThreadPolicy;

    .line 312
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 313
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v3

    invoke-virtual {v3}, Lz;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 314
    new-instance v3, Liuq;

    invoke-direct {v3, v1, v2, v0}, Liuq;-><init>(Landroid/content/Context;ILandroid/util/DisplayMetrics;)V

    iput-object v3, p0, Letn;->as:Liuq;

    .line 316
    new-instance v0, Litz;

    iget-object v2, p0, Letn;->Z:Lhee;

    .line 317
    invoke-interface {v2}, Lhee;->g()Lhej;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 318
    invoke-virtual {p0}, Letn;->p()Lae;

    move-result-object v3

    iget-object v4, p0, Letn;->aY:Liue;

    invoke-direct {v0, v1, v2, v3, v4}, Litz;-><init>(Landroid/content/Context;Ljava/lang/String;Lae;Liue;)V

    iput-object v0, p0, Letn;->ai:Litz;

    .line 320
    if-eqz p1, :cond_6

    .line 321
    const-string v0, "user_device_locations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 322
    const-string v0, "user_device_locations"

    .line 323
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyv;

    .line 324
    new-array v2, v5, [Lnhm;

    invoke-virtual {v0, v2}, Lhyv;->a([Loxu;)[Loxu;

    move-result-object v0

    check-cast v0, [Lnhm;

    iput-object v0, p0, Letn;->an:[Lnhm;

    .line 327
    :cond_2
    const-string v0, "test_locations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 328
    const-string v0, "test_locations"

    .line 329
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyv;

    .line 330
    new-array v2, v5, [Lnhm;

    invoke-virtual {v0, v2}, Lhyv;->a([Loxu;)[Loxu;

    move-result-object v0

    check-cast v0, [Lnhm;

    iput-object v0, p0, Letn;->ar:[Lnhm;

    .line 333
    :cond_3
    const-string v0, "owner_device_location_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 334
    const-string v0, "owner_device_location_info"

    .line 335
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    .line 336
    new-instance v2, Lnhl;

    invoke-direct {v2}, Lnhl;-><init>()V

    invoke-virtual {v0, v2}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lnhl;

    iput-object v0, p0, Letn;->am:Lnhl;

    .line 339
    :cond_4
    const-string v0, "centered_marker_index"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Letn;->ax:I

    .line 340
    const-string v0, "selected_marker_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Letn;->az:Ljava/lang/String;

    .line 342
    const-string v0, "selected_user"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Letn;->ay:Ljava/lang/String;

    .line 343
    const-string v0, "view_config"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Letn;->aQ:I

    .line 344
    const-string v0, "shown_no_friends_dialog"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Letn;->aA:Z

    .line 345
    const-string v0, "was_satellite_view"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Letn;->aR:Z

    .line 347
    const-string v0, "last_refresh_timestamp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 348
    invoke-direct {p0}, Letn;->aa()Lfvs;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lfvs;->a(J)V

    .line 350
    const-string v0, "finished_pre_work"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Letn;->aO:Z

    .line 358
    :cond_5
    :goto_1
    iput-boolean v6, p0, Letn;->aD:Z

    .line 360
    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 362
    invoke-static {v1}, Lkib;->b(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 363
    const-string v0, "shown_sharing_dialog:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-interface {v1, v0, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Letn;->aB:Z

    .line 364
    const-string v0, "shown_reporting_dialog:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 365
    :goto_3
    invoke-interface {v1, v0, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Letn;->aC:Z

    .line 367
    invoke-virtual {p0}, Letn;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "num_coalesced_notifs"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Letn;->aM:I

    goto/16 :goto_0

    .line 352
    :cond_6
    invoke-virtual {p0}, Letn;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "gaia_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Letn;->af:Ljava/lang/String;

    .line 353
    iget-object v0, p0, Letn;->af:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 354
    iget-object v0, p0, Letn;->af:Ljava/lang/String;

    iput-object v0, p0, Letn;->ag:Ljava/lang/String;

    .line 355
    const/16 v0, 0xe

    iput v0, p0, Letn;->ah:I

    goto :goto_1

    .line 363
    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 364
    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1312
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1265
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1307
    :goto_0
    return-void

    .line 1267
    :pswitch_0
    if-nez p2, :cond_0

    .line 1268
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1269
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1273
    :cond_0
    iget-boolean v0, p0, Letn;->aN:Z

    if-nez v0, :cond_3

    .line 1274
    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Letn;->k(Landroid/os/Bundle;)Z

    .line 1276
    iget-object v0, p0, Letn;->ap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1277
    const/4 v0, -0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1278
    :cond_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1279
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1280
    const/4 v0, 0x1

    .line 1281
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1282
    invoke-static {v0}, Ldsm;->d(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 1284
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_1

    .line 1285
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1286
    iget-object v1, p0, Letn;->ap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 1287
    if-nez v1, :cond_2

    .line 1288
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1290
    :cond_2
    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1291
    iget-object v7, p0, Letn;->ap:Ljava/util/Map;

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1284
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1295
    :cond_3
    new-instance v0, Letz;

    invoke-direct {v0, p0}, Letz;-><init>(Letn;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1265
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 127
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Letn;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public final a(Lhjk;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 837
    const v0, 0x7f0a062b

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 839
    const v0, 0x7f10067b

    .line 840
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 841
    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 843
    invoke-direct {p0}, Letn;->as()Lhoc;

    move-result-object v2

    const-string v3, "GetFriendLocationsTask"

    .line 844
    invoke-virtual {v2, v3}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p0, Letn;->aL:I

    if-lez v2, :cond_2

    .line 846
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 847
    invoke-virtual {v0}, Lhjv;->b()V

    .line 850
    :cond_1
    const v0, 0x7f1006d3

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 851
    const v0, 0x7f1006d0

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 853
    invoke-direct {p0}, Letn;->aq()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 854
    const v0, 0x7f1006d2

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 858
    :goto_1
    return-void

    .line 844
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 856
    :cond_3
    const v0, 0x7f1006d1

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1143
    invoke-direct {p0}, Letn;->al()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1145
    const-string v0, "hflf_no_shares"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "hflf_friend_list"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1146
    :cond_0
    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 1147
    const v1, 0x7f0a0a8f

    .line 1148
    invoke-virtual {p0, v1}, Letn;->e_(I)Ljava/lang/String;

    move-result-object v1

    .line 1150
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0, v1}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1151
    invoke-virtual {p0, v0}, Letn;->a(Landroid/content/Intent;)V

    .line 1158
    :cond_1
    :goto_0
    const-string v0, "hflf_friend_list"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1159
    invoke-direct {p0, p1}, Letn;->g(Ljava/lang/String;)V

    .line 1162
    :cond_2
    return-void

    .line 1152
    :cond_3
    const-string v0, "hflf_all_filtered"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1153
    invoke-direct {p0}, Letn;->am()V

    goto :goto_0

    .line 1154
    :cond_4
    const-string v0, "hflf_enable_sharing"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1155
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Letn;->c(Z)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;IZ)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2030
    iput-boolean v3, p0, Letn;->aN:Z

    .line 2031
    iput-boolean v2, p0, Letn;->aO:Z

    .line 2032
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lz;->setRequestedOrientation(I)V

    .line 2033
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    .line 2034
    invoke-virtual {v0}, Loo;->e()V

    .line 2037
    if-eqz p1, :cond_0

    .line 2038
    invoke-virtual {p0}, Letn;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 2040
    packed-switch p2, :pswitch_data_0

    .line 2048
    const v1, 0x7f0a0aa7

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2051
    :goto_0
    invoke-direct {p0, v0}, Letn;->j(Ljava/lang/String;)V

    .line 2054
    :cond_0
    invoke-virtual {p0}, Letn;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 2056
    if-eqz p3, :cond_1

    .line 2057
    invoke-direct {p0, v3}, Letn;->c(Z)V

    .line 2059
    :cond_1
    return-void

    .line 2042
    :pswitch_0
    const v1, 0x7f0a0aa8

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2045
    :pswitch_1
    const v1, 0x7f0a0aa9

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2040
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 9

    .prologue
    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 507
    const-string v0, "GetFriendLocationsTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 509
    invoke-virtual {p0}, Letn;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1002aa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 510
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 511
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 595
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    if-nez p2, :cond_2

    .line 518
    invoke-direct {p0}, Letn;->Z()V

    goto :goto_0

    .line 523
    :cond_2
    invoke-static {p2}, Ldoy;->a(Lhoz;)[Lnhm;

    move-result-object v5

    .line 525
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_b

    :cond_3
    move-object v4, v1

    .line 526
    :goto_1
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_c

    :cond_4
    move v3, v2

    .line 530
    :goto_2
    if-nez v5, :cond_d

    .line 531
    iget-object v0, p0, Letn;->an:[Lnhm;

    if-nez v0, :cond_5

    .line 532
    new-array v0, v2, [Lnhm;

    iput-object v0, p0, Letn;->an:[Lnhm;

    .line 538
    :cond_5
    :goto_3
    if-eqz v4, :cond_6

    .line 539
    iput-object v4, p0, Letn;->am:Lnhl;

    .line 542
    :cond_6
    if-eqz p2, :cond_7

    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_e

    :cond_7
    move-object v0, v1

    .line 544
    :goto_4
    iget-object v4, p0, Letn;->aw:Lesy;

    iget-object v5, p0, Letn;->am:Lnhl;

    iget-object v6, p0, Letn;->ai:Litz;

    .line 545
    invoke-virtual {v6}, Litz;->f()Z

    move-result v6

    invoke-virtual {v4, v5, v0, v6}, Lesy;->a(Lnhl;Lnjt;Z)V

    .line 547
    iget v4, p0, Letn;->aM:I

    if-le v4, v8, :cond_9

    .line 548
    if-eqz v0, :cond_8

    iget-object v4, v0, Lnjt;->g:Ljava/lang/String;

    if-eqz v4, :cond_8

    .line 550
    iget v4, p0, Letn;->aM:I

    if-ne v4, v7, :cond_10

    .line 551
    const v4, 0x7f0a03e4

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v0, v0, Lnjt;->g:Ljava/lang/String;

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Letn;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Letn;->j(Ljava/lang/String;)V

    .line 562
    :cond_8
    :goto_5
    iput v2, p0, Letn;->aM:I

    .line 565
    :cond_9
    const-string v0, "extra_num_users_on_map"

    iget-object v4, p0, Letn;->an:[Lnhm;

    array-length v4, v4

    invoke-static {v0, v4}, Lhmt;->a(Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v4

    .line 567
    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v5

    .line 568
    iget-object v0, p0, Letn;->au:Llnh;

    const-class v6, Lhms;

    invoke-virtual {v0, v6}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v6, Lhmr;

    iget-object v7, p0, Letn;->at:Llnl;

    invoke-direct {v6, v7, v5}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v5, Lhmv;->cH:Lhmv;

    .line 570
    invoke-virtual {v6, v5}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v5

    .line 571
    invoke-virtual {v5, v4}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v4

    .line 568
    invoke-interface {v0, v4}, Lhms;->a(Lhmr;)V

    .line 577
    if-lez v3, :cond_11

    .line 578
    iget-object v0, p0, Letn;->X:Lhov;

    new-instance v4, Letu;

    invoke-direct {v4, p0}, Letu;-><init>(Letn;)V

    int-to-long v6, v3

    invoke-virtual {v0, v4, v6, v7}, Lhov;->a(Ljava/lang/Runnable;J)Lhox;

    .line 590
    :cond_a
    invoke-virtual {p0}, Letn;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2, v1, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 592
    invoke-direct {p0}, Letn;->ac()V

    .line 593
    invoke-direct {p0}, Letn;->ad()V

    .line 594
    iput-boolean v8, p0, Letn;->aJ:Z

    goto/16 :goto_0

    .line 525
    :cond_b
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "owner_device_location"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    new-instance v3, Lnhl;

    invoke-direct {v3}, Lnhl;-><init>()V

    invoke-virtual {v0, v3}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lnhl;

    move-object v4, v0

    goto/16 :goto_1

    .line 526
    :cond_c
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "delay_interval"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    move v3, v0

    goto/16 :goto_2

    .line 535
    :cond_d
    iput-object v5, p0, Letn;->an:[Lnhm;

    goto/16 :goto_3

    .line 542
    :cond_e
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "target_profile"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    if-nez v0, :cond_f

    move-object v0, v1

    goto/16 :goto_4

    :cond_f
    new-instance v4, Lnjt;

    invoke-direct {v4}, Lnjt;-><init>()V

    invoke-virtual {v0, v4}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lnjt;

    goto/16 :goto_4

    .line 554
    :cond_10
    const v4, 0x7f0a03e3

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v0, v0, Lnjt;->g:Ljava/lang/String;

    aput-object v0, v5, v2

    iget v0, p0, Letn;->aM:I

    add-int/lit8 v0, v0, -0x1

    .line 556
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    .line 554
    invoke-virtual {p0, v4, v5}, Letn;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Letn;->j(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 586
    :cond_11
    iput v2, p0, Letn;->aL:I

    .line 587
    iget-object v3, p0, Letn;->an:[Lnhm;

    array-length v4, v3

    move v0, v2

    :goto_6
    if-ge v0, v4, :cond_a

    aget-object v5, v3, v0

    iput-object v1, v5, Lnhm;->f:Lnhi;

    add-int/lit8 v0, v0, 0x1

    goto :goto_6
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1166
    invoke-direct {p0}, Letn;->al()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1167
    const-string v0, "hflf_friend_list"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1168
    invoke-direct {p0, p2}, Letn;->g(Ljava/lang/String;)V

    .line 1169
    const/16 v0, 0xe

    invoke-direct {p0, p1, v0}, Letn;->a(Ljava/lang/String;I)Z

    .line 1170
    sget-object v1, Lhmv;->cM:Lhmv;

    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v2

    iget-object v0, p0, Letn;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Letn;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3, v1}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    sget-object v2, Lhmw;->T:Lhmw;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 1177
    :cond_0
    :goto_0
    return-void

    .line 1173
    :cond_1
    const/16 v0, 0x12

    invoke-direct {p0, p1, v0}, Letn;->a(Ljava/lang/String;I)Z

    .line 1174
    sget-object v0, Lhmv;->cM:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 2127
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lley;->a(Loo;Z)V

    .line 2128
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 1698
    if-nez p1, :cond_0

    .line 1700
    invoke-direct {p0}, Letn;->ai()V

    .line 1702
    :cond_0
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 862
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 863
    const v3, 0x7f10067b

    if-ne v2, v3, :cond_1

    .line 864
    invoke-virtual {p0}, Letn;->d()V

    .line 877
    :cond_0
    :goto_0
    return v1

    .line 866
    :cond_1
    const v3, 0x7f1006d3

    if-ne v2, v3, :cond_2

    .line 867
    sget-object v2, Lhmv;->cS:Lhmv;

    invoke-direct {p0, v2}, Letn;->a(Lhmv;)V

    .line 868
    invoke-direct {p0, v0}, Letn;->c(Z)V

    goto :goto_0

    .line 870
    :cond_2
    const v3, 0x7f1006d0

    if-ne v2, v3, :cond_3

    .line 871
    invoke-direct {p0}, Letn;->am()V

    goto :goto_0

    .line 873
    :cond_3
    const v3, 0x7f1006d2

    if-eq v2, v3, :cond_4

    const v3, 0x7f1006d1

    if-ne v2, v3, :cond_7

    .line 874
    :cond_4
    iget-object v0, p0, Letn;->ab:Limp;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Letn;->aq()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lhmv;->cR:Lhmv;

    :goto_1
    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    iget-object v2, p0, Letn;->ab:Limp;

    invoke-direct {p0}, Letn;->aq()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_2
    invoke-interface {v2, v0}, Limp;->a(I)V

    iget-object v0, p0, Letn;->Y:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto :goto_0

    :cond_5
    sget-object v0, Lhmv;->cQ:Lhmv;

    goto :goto_1

    :cond_6
    const/4 v0, 0x4

    goto :goto_2

    :cond_7
    move v1, v0

    .line 877
    goto :goto_0
.end method

.method public aO_()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 398
    invoke-super {p0}, Llol;->aO_()V

    .line 399
    iput v2, p0, Letn;->aL:I

    .line 401
    iget-object v0, p0, Letn;->ab:Limp;

    if-nez v0, :cond_1

    .line 402
    iget-object v0, p0, Letn;->aa:Limw;

    invoke-interface {v0}, Limw;->b()Limp;

    move-result-object v0

    iput-object v0, p0, Letn;->ab:Limp;

    .line 403
    iget-object v0, p0, Letn;->ab:Limp;

    if-nez v0, :cond_0

    .line 404
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 445
    :goto_0
    return-void

    .line 408
    :cond_0
    iget-object v0, p0, Letn;->as:Liuq;

    iget-object v1, p0, Letn;->ab:Limp;

    invoke-virtual {v0, v1}, Liuq;->a(Limp;)V

    .line 410
    iget-object v0, p0, Letn;->ab:Limp;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Limp;->a(Z)V

    .line 412
    iget-object v0, p0, Letn;->ab:Limp;

    new-instance v1, Letv;

    invoke-direct {v1, p0}, Letv;-><init>(Letn;)V

    invoke-interface {v0, v1}, Limp;->a(Lims;)V

    iget-object v0, p0, Letn;->ab:Limp;

    new-instance v1, Letw;

    invoke-direct {v1, p0}, Letw;-><init>(Letn;)V

    invoke-interface {v0, v1}, Limp;->a(Limr;)V

    iget-object v0, p0, Letn;->ab:Limp;

    new-instance v1, Letx;

    invoke-direct {v1, p0}, Letx;-><init>(Letn;)V

    invoke-interface {v0, v1}, Limp;->a(Limq;)V

    .line 415
    :cond_1
    iget-boolean v0, p0, Letn;->aR:Z

    if-eqz v0, :cond_2

    .line 416
    iget-object v0, p0, Letn;->ab:Limp;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Limp;->a(I)V

    .line 419
    :cond_2
    invoke-direct {p0}, Letn;->as()Lhoc;

    move-result-object v0

    .line 420
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 421
    iget-object v1, p0, Letn;->an:[Lnhm;

    if-nez v1, :cond_3

    const-string v1, "GetFriendLocationsTask"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 423
    invoke-direct {p0}, Letn;->Z()V

    .line 425
    :cond_3
    invoke-direct {p0}, Letn;->ac()V

    .line 427
    iget-boolean v0, p0, Letn;->aD:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Letn;->an:[Lnhm;

    if-eqz v0, :cond_4

    .line 428
    invoke-virtual {p0}, Letn;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 432
    :cond_4
    iget-boolean v0, p0, Letn;->aI:Z

    if-eqz v0, :cond_5

    .line 433
    invoke-direct {p0}, Letn;->ad()V

    .line 434
    iput-boolean v2, p0, Letn;->aI:Z

    .line 437
    :cond_5
    iget-boolean v0, p0, Letn;->aK:Z

    if-eqz v0, :cond_6

    .line 438
    invoke-direct {p0}, Letn;->ar()Lfvs;

    move-result-object v0

    invoke-virtual {v0}, Lfvs;->run()V

    .line 439
    iput-boolean v2, p0, Letn;->aK:Z

    .line 442
    :cond_6
    iget-object v0, p0, Letn;->ad:Liug;

    if-nez v0, :cond_7

    new-instance v0, Liug;

    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v1

    const-wide/16 v2, 0x2710

    new-instance v5, Leub;

    invoke-direct {v5, p0}, Leub;-><init>(Letn;)V

    invoke-direct/range {v0 .. v5}, Liug;-><init>(Landroid/content/Context;JLandroid/location/Location;Lilo;)V

    iput-object v0, p0, Letn;->ad:Liug;

    iget-object v0, p0, Letn;->ad:Liug;

    invoke-virtual {v0}, Liug;->b()V

    .line 444
    :cond_7
    invoke-direct {p0}, Letn;->aa()Lfvs;

    move-result-object v0

    invoke-virtual {v0}, Lfvs;->run()V

    goto/16 :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 2063
    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xe

    invoke-direct {p0, v0, v1}, Letn;->a(Ljava/lang/String;I)Z

    .line 2064
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2116
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1181
    sget-object v0, Lhmv;->cL:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    .line 1183
    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Letn;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x0

    .line 1182
    invoke-static {v0, v1, p1, v2}, Leyq;->c(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Letn;->a(Landroid/content/Intent;)V

    .line 1184
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 2132
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2094
    if-eqz p1, :cond_0

    .line 2095
    iput-boolean v0, p0, Letn;->aB:Z

    .line 2096
    iput-boolean v0, p0, Letn;->aC:Z

    .line 2098
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2120
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 2121
    iget-object v0, p0, Letn;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 2122
    iget-object v0, p0, Letn;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Letn;->Z:Lhee;

    .line 2123
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1193
    invoke-direct {p0, p1}, Letn;->i(Ljava/lang/String;)I

    move-result v0

    .line 1194
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1221
    :cond_0
    :goto_0
    return-void

    .line 1197
    :cond_1
    iget-object v1, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnhm;

    .line 1198
    iget-object v1, v0, Lnhm;->c:[Lnij;

    .line 1199
    invoke-static {v1}, Liuo;->a([Lnij;)Lnij;

    move-result-object v1

    .line 1200
    iget-object v2, v1, Lnij;->c:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1201
    iget-object v3, v1, Lnij;->d:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1205
    iget-object v0, v0, Lnhm;->c:[Lnij;

    invoke-static {v0}, Liuo;->b([Lnij;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1206
    const-string v2, "http://maps.google.com/maps?saddr=&daddr="

    iget-object v0, v1, Lnij;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1207
    :goto_1
    sget-object v1, Lhmv;->df:Lhmv;

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 1212
    :goto_2
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1214
    iget-object v1, p0, Letn;->at:Llnl;

    .line 1215
    invoke-virtual {v1}, Llnl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v3, 0x10000

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 1217
    if-eqz v1, :cond_0

    .line 1218
    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    .line 1219
    invoke-virtual {p0, v2}, Letn;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 1206
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1209
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2a

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v1, v4

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "http://maps.google.com/maps?saddr=&daddr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1210
    sget-object v0, Lhmv;->de:Lhmv;

    goto :goto_2
.end method

.method public d()V
    .locals 1

    .prologue
    .line 933
    sget-object v0, Lhmv;->cJ:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    .line 937
    invoke-direct {p0}, Letn;->Z()V

    .line 938
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1231
    iget-object v0, p0, Letn;->Z:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1232
    iget-object v0, p0, Letn;->at:Llnl;

    iget-object v1, p0, Letn;->Z:Lhee;

    .line 1234
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x0

    .line 1233
    invoke-static {v0, v1, v2}, Leyq;->h(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1241
    :goto_0
    iget-object v1, p0, Letn;->at:Llnl;

    .line 1242
    invoke-virtual {v1}, Llnl;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 1244
    if-eqz v1, :cond_0

    .line 1245
    sget-object v1, Lhmv;->dd:Lhmv;

    invoke-direct {p0, v1}, Letn;->a(Lhmv;)V

    .line 1246
    invoke-virtual {p0, v0}, Letn;->a(Landroid/content/Intent;)V

    .line 1248
    :cond_0
    return-void

    .line 1236
    :cond_1
    iget-object v0, p0, Letn;->at:Llnl;

    iget-object v1, p0, Letn;->Z:Lhee;

    .line 1238
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 1237
    invoke-static {v0, v1, p1}, Leyq;->h(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 1078
    const v0, 0x7f0a0a88

    const v1, 0x7f0a0a89

    const/4 v2, 0x0

    const v3, 0x7f0a03eb

    invoke-static {v0, v1, v2, v3}, Litv;->a(IILjava/lang/String;I)Litv;

    move-result-object v0

    .line 1082
    const/4 v1, 0x1

    iput-boolean v1, p0, Letn;->aA:Z

    .line 1083
    const-string v1, "hflf_no_shares"

    invoke-direct {p0, v0, v1}, Letn;->a(Lt;Ljava/lang/String;)V

    .line 1084
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 474
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 476
    iget-object v0, p0, Letn;->an:[Lnhm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Letn;->an:[Lnhm;

    array-length v0, v0

    if-eqz v0, :cond_0

    .line 477
    const-string v0, "user_device_locations"

    new-instance v1, Lhyv;

    iget-object v2, p0, Letn;->an:[Lnhm;

    invoke-direct {v1, v2}, Lhyv;-><init>([Loxu;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 481
    :cond_0
    iget-object v0, p0, Letn;->ar:[Lnhm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Letn;->ar:[Lnhm;

    array-length v0, v0

    if-eqz v0, :cond_1

    .line 482
    const-string v0, "test_locations"

    new-instance v1, Lhyv;

    iget-object v2, p0, Letn;->ar:[Lnhm;

    invoke-direct {v1, v2}, Lhyv;-><init>([Loxu;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 486
    :cond_1
    iget-object v0, p0, Letn;->am:Lnhl;

    if-eqz v0, :cond_2

    .line 487
    const-string v0, "owner_device_location_info"

    new-instance v1, Lhyt;

    iget-object v2, p0, Letn;->am:Lnhl;

    invoke-direct {v1, v2}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 491
    :cond_2
    const-string v0, "centered_marker_index"

    iget v1, p0, Letn;->ax:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 492
    const-string v0, "selected_marker_key"

    iget-object v1, p0, Letn;->az:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    const-string v0, "selected_user"

    iget-object v1, p0, Letn;->ay:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v0, "shown_no_friends_dialog"

    iget-boolean v1, p0, Letn;->aA:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 496
    const-string v0, "view_config"

    invoke-virtual {p0}, Letn;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->getChangingConfigurations()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 497
    const-string v0, "was_satellite_view"

    invoke-direct {p0}, Letn;->aq()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 499
    const-string v0, "last_refresh_timestamp"

    invoke-direct {p0}, Letn;->aa()Lfvs;

    move-result-object v1

    invoke-virtual {v1}, Lfvs;->c()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 501
    const-string v0, "finished_pre_work"

    iget-boolean v1, p0, Letn;->aO:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 502
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const v3, 0x7f10029d

    .line 899
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 901
    iget-object v1, p0, Letn;->ao:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 902
    const v2, 0x7f10029e

    if-ne v0, v2, :cond_1

    .line 903
    if-lez v1, :cond_0

    iget-object v0, p0, Letn;->ab:Limp;

    if-eqz v0, :cond_0

    .line 904
    sget-object v0, Lhmv;->cI:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    .line 905
    invoke-virtual {p0}, Letn;->X()V

    .line 923
    :cond_0
    :goto_0
    return-void

    .line 907
    :cond_1
    if-eq v0, v3, :cond_2

    const v2, 0x7f10029f

    if-ne v0, v2, :cond_0

    .line 908
    :cond_2
    if-lez v1, :cond_0

    iget-object v2, p0, Letn;->ab:Limp;

    if-eqz v2, :cond_0

    .line 909
    if-ne v0, v3, :cond_4

    .line 910
    sget-object v0, Lhmv;->cO:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    .line 911
    iget v0, p0, Letn;->ax:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Letn;->ax:I

    if-gez v0, :cond_3

    .line 912
    add-int/lit8 v0, v1, -0x1

    iput v0, p0, Letn;->ax:I

    .line 920
    :cond_3
    :goto_1
    iget v0, p0, Letn;->ax:I

    const/16 v1, 0xe

    invoke-direct {p0, v0, v1}, Letn;->a(II)Z

    goto :goto_0

    .line 915
    :cond_4
    sget-object v0, Lhmv;->cN:Lhmv;

    invoke-direct {p0, v0}, Letn;->a(Lhmv;)V

    .line 916
    iget v0, p0, Letn;->ax:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Letn;->ax:I

    if-lt v0, v1, :cond_3

    .line 917
    const/4 v0, 0x0

    iput v0, p0, Letn;->ax:I

    goto :goto_1
.end method

.method public z()V
    .locals 1

    .prologue
    .line 449
    invoke-super {p0}, Llol;->z()V

    .line 451
    iget-object v0, p0, Letn;->ad:Liug;

    if-eqz v0, :cond_0

    iget-object v0, p0, Letn;->ad:Liug;

    invoke-virtual {v0}, Liug;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Letn;->ad:Liug;

    .line 453
    :cond_0
    invoke-direct {p0}, Letn;->aa()Lfvs;

    move-result-object v0

    invoke-virtual {v0}, Lfvs;->b()V

    .line 454
    return-void
.end method
