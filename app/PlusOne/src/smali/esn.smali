.class public abstract Lesn;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static a:Ljava/lang/Object;

.field private static b:Ljava/lang/Object;

.field private static final c:Ljava/nio/charset/Charset;

.field private static g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lesq",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final e:[Ljava/lang/Object;

.field private f:[Lesp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lesn;->a:Ljava/lang/Object;

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lesn;->b:Ljava/lang/Object;

    .line 66
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lesn;->c:Ljava/nio/charset/Charset;

    .line 96
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lesn;->g:Ljava/util/HashMap;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iput-object v0, p0, Lesn;->d:Ljava/lang/Class;

    .line 105
    iput-object v0, p0, Lesn;->e:[Ljava/lang/Object;

    .line 106
    return-void
.end method

.method protected varargs constructor <init>(Ljava/lang/Class;[Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object p1, p0, Lesn;->d:Ljava/lang/Class;

    .line 127
    iput-object p2, p0, Lesn;->e:[Ljava/lang/Object;

    .line 128
    return-void
.end method

.method public static a(Ljava/lang/Class;)Lesn;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lesn",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 142
    sget-object v0, Lesn;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lesq;

    .line 143
    if-nez v0, :cond_0

    .line 144
    new-instance v0, Lesq;

    invoke-direct {v0, p0}, Lesq;-><init>(Ljava/lang/Class;)V

    .line 145
    sget-object v1, Lesn;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    :cond_0
    return-object v0
.end method

.method public static varargs a(Ljava/lang/Class;[Ljava/lang/Object;)Lesn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;[",
            "Ljava/lang/Object;",
            ")",
            "Lesn",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 156
    new-instance v0, Leso;

    invoke-direct {v0, p0, p1}, Leso;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-object v0
.end method

.method private a(Lesp;Ljava/lang/reflect/Field;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 246
    iput-object p2, p1, Lesp;->c:Ljava/lang/reflect/Field;

    .line 248
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    .line 249
    const-class v2, Ljava/lang/String;

    if-ne v0, v2, :cond_2

    .line 250
    iput v1, p1, Lesp;->d:I

    .line 293
    :cond_0
    :goto_0
    if-eqz p3, :cond_1

    .line 294
    iget v0, p1, Lesp;->d:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p1, Lesp;->d:I

    .line 296
    :cond_1
    return-void

    .line 251
    :cond_2
    const-class v2, Ljava/lang/Integer;

    if-eq v0, v2, :cond_3

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_4

    .line 252
    :cond_3
    iput v3, p1, Lesp;->d:I

    goto :goto_0

    .line 253
    :cond_4
    const-class v2, Ljava/lang/Long;

    if-eq v0, v2, :cond_5

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_6

    .line 254
    :cond_5
    iput v4, p1, Lesp;->d:I

    goto :goto_0

    .line 255
    :cond_6
    const-class v2, Ljava/lang/Float;

    if-eq v0, v2, :cond_7

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_8

    .line 256
    :cond_7
    iput v5, p1, Lesp;->d:I

    goto :goto_0

    .line 257
    :cond_8
    const-class v2, Ljava/lang/Double;

    if-eq v0, v2, :cond_9

    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_a

    .line 258
    :cond_9
    iput v6, p1, Lesp;->d:I

    goto :goto_0

    .line 259
    :cond_a
    const-class v2, Ljava/lang/Boolean;

    if-eq v0, v2, :cond_b

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_c

    .line 260
    :cond_b
    const/4 v0, 0x5

    iput v0, p1, Lesp;->d:I

    goto :goto_0

    .line 261
    :cond_c
    const-class v2, Ljava/math/BigInteger;

    if-ne v0, v2, :cond_d

    .line 262
    const/4 v0, 0x6

    iput v0, p1, Lesp;->d:I

    goto :goto_0

    .line 263
    :cond_d
    const-class v2, Ljava/util/List;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 264
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 265
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/Class;

    .line 266
    const/4 v2, 0x7

    iput v2, p1, Lesp;->d:I

    .line 267
    const-class v2, Ljava/lang/String;

    if-ne v0, v2, :cond_e

    .line 268
    iput v1, p1, Lesp;->e:I

    .line 285
    :goto_1
    if-eqz p3, :cond_0

    .line 286
    iget v0, p1, Lesp;->e:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p1, Lesp;->e:I

    move p3, v1

    .line 287
    goto :goto_0

    .line 269
    :cond_e
    const-class v2, Ljava/lang/Integer;

    if-ne v0, v2, :cond_f

    .line 270
    iput v3, p1, Lesp;->e:I

    goto :goto_1

    .line 271
    :cond_f
    const-class v2, Ljava/lang/Long;

    if-ne v0, v2, :cond_10

    .line 272
    iput v4, p1, Lesp;->e:I

    goto :goto_1

    .line 273
    :cond_10
    const-class v2, Ljava/lang/Float;

    if-ne v0, v2, :cond_11

    .line 274
    iput v5, p1, Lesp;->e:I

    goto :goto_1

    .line 275
    :cond_11
    const-class v2, Ljava/lang/Double;

    if-ne v0, v2, :cond_12

    .line 276
    iput v6, p1, Lesp;->e:I

    goto :goto_1

    .line 277
    :cond_12
    const-class v2, Ljava/lang/Boolean;

    if-ne v0, v2, :cond_13

    .line 278
    const/4 v0, 0x5

    iput v0, p1, Lesp;->e:I

    goto :goto_1

    .line 279
    :cond_13
    const-class v2, Ljava/math/BigInteger;

    if-ne v0, v2, :cond_14

    .line 280
    const/4 v0, 0x6

    iput v0, p1, Lesp;->e:I

    goto :goto_1

    .line 282
    :cond_14
    const/16 v0, 0x8

    iput v0, p1, Lesp;->e:I

    goto :goto_1

    .line 290
    :cond_15
    const/16 v0, 0x8

    iput v0, p1, Lesp;->d:I

    goto/16 :goto_0
.end method

.method private b(Landroid/util/JsonWriter;Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 649
    iget-object v0, p0, Lesn;->f:[Lesp;

    if-nez v0, :cond_1

    .line 650
    iget-object v0, p0, Lesn;->d:Ljava/lang/Class;

    if-nez v0, :cond_0

    .line 651
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "A JSON class must either configure the automatic parser or override read(Jsonwriter)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 654
    :cond_0
    invoke-direct {p0}, Lesn;->c()V

    .line 657
    :cond_1
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginObject()Landroid/util/JsonWriter;

    .line 658
    invoke-virtual {p0, p2}, Lesn;->b(Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    move v2, v3

    .line 659
    :goto_0
    array-length v0, v5

    if-ge v2, v0, :cond_4

    .line 660
    aget-object v0, v5, v2

    if-eqz v0, :cond_2

    .line 661
    aget-object v0, v5, v2

    .line 665
    iget-object v1, p0, Lesn;->f:[Lesp;

    aget-object v6, v1, v2

    .line 666
    iget-object v1, v6, Lesp;->b:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->name(Ljava/lang/String;)Landroid/util/JsonWriter;

    .line 667
    iget v1, v6, Lesp;->d:I

    packed-switch v1, :pswitch_data_0

    .line 749
    :cond_2
    :goto_1
    :pswitch_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 670
    :pswitch_1
    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    goto :goto_1

    .line 676
    :pswitch_2
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->value(Ljava/lang/Number;)Landroid/util/JsonWriter;

    goto :goto_1

    .line 680
    :pswitch_3
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->value(Ljava/lang/Number;)Landroid/util/JsonWriter;

    goto :goto_1

    .line 684
    :pswitch_4
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->value(Ljava/lang/Number;)Landroid/util/JsonWriter;

    goto :goto_1

    .line 688
    :pswitch_5
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->value(Z)Landroid/util/JsonWriter;

    goto :goto_1

    .line 697
    :pswitch_6
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    goto :goto_1

    .line 701
    :pswitch_7
    iget-object v1, v6, Lesp;->f:Lesn;

    invoke-direct {v1, p1, v0}, Lesn;->b(Landroid/util/JsonWriter;Ljava/lang/Object;)V

    goto :goto_1

    .line 705
    :pswitch_8
    check-cast v0, Ljava/util/List;

    .line 706
    invoke-virtual {p1}, Landroid/util/JsonWriter;->beginArray()Landroid/util/JsonWriter;

    .line 707
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    move v4, v3

    .line 708
    :goto_2
    if-ge v4, v7, :cond_3

    .line 709
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 710
    iget v8, v6, Lesp;->e:I

    sparse-switch v8, :sswitch_data_0

    .line 708
    :goto_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 713
    :sswitch_0
    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    goto :goto_3

    .line 719
    :sswitch_1
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/Number;)Landroid/util/JsonWriter;

    goto :goto_3

    .line 723
    :sswitch_2
    check-cast v1, Ljava/lang/Float;

    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/Number;)Landroid/util/JsonWriter;

    goto :goto_3

    .line 727
    :sswitch_3
    check-cast v1, Ljava/lang/Double;

    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/Number;)Landroid/util/JsonWriter;

    goto :goto_3

    .line 731
    :sswitch_4
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->value(Z)Landroid/util/JsonWriter;

    goto :goto_3

    .line 740
    :sswitch_5
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/util/JsonWriter;->value(Ljava/lang/String;)Landroid/util/JsonWriter;

    goto :goto_3

    .line 744
    :sswitch_6
    iget-object v8, v6, Lesp;->f:Lesn;

    invoke-direct {v8, p1, v1}, Lesn;->b(Landroid/util/JsonWriter;Ljava/lang/Object;)V

    goto :goto_3

    .line 748
    :cond_3
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endArray()Landroid/util/JsonWriter;

    goto :goto_1

    .line 754
    :cond_4
    invoke-virtual {p1}, Landroid/util/JsonWriter;->endObject()Landroid/util/JsonWriter;

    .line 755
    return-void

    .line 667
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch

    .line 710
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_1
        0x8 -> :sswitch_6
        0x20 -> :sswitch_0
        0x21 -> :sswitch_5
        0x22 -> :sswitch_5
        0x23 -> :sswitch_5
        0x24 -> :sswitch_5
        0x25 -> :sswitch_5
        0x26 -> :sswitch_5
    .end sparse-switch
.end method

.method private declared-synchronized c()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 164
    monitor-enter p0

    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v4

    .line 166
    :goto_0
    iget-object v1, p0, Lesn;->e:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 167
    new-instance v6, Lesp;

    invoke-direct {v6}, Lesp;-><init>()V

    .line 168
    iget-object v2, p0, Lesn;->e:[Ljava/lang/Object;

    add-int/lit8 v1, v0, 0x1

    aget-object v0, v2, v0

    .line 169
    sget-object v2, Lesn;->b:Ljava/lang/Object;

    if-ne v0, v2, :cond_0

    .line 170
    iget-object v0, p0, Lesn;->e:[Ljava/lang/Object;

    add-int/lit8 v2, v1, 0x1

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lesp;->b:Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lesn;->e:[Ljava/lang/Object;

    add-int/lit8 v1, v2, 0x1

    aget-object v0, v0, v2

    .line 175
    :cond_0
    sget-object v2, Lesn;->a:Ljava/lang/Object;

    if-ne v0, v2, :cond_7

    .line 176
    const/4 v0, 0x1

    .line 177
    iget-object v3, p0, Lesn;->e:[Ljava/lang/Object;

    add-int/lit8 v2, v1, 0x1

    aget-object v1, v3, v1

    move v3, v0

    move-object v0, v1

    .line 180
    :goto_1
    instance-of v1, v0, Lesn;

    if-eqz v1, :cond_2

    .line 181
    check-cast v0, Lesn;

    iput-object v0, v6, Lesp;->f:Lesn;

    .line 182
    iget-object v0, p0, Lesn;->e:[Ljava/lang/Object;

    add-int/lit8 v1, v2, 0x1

    aget-object v0, v0, v2

    .line 197
    :goto_2
    check-cast v0, Ljava/lang/String;

    .line 198
    iget-object v2, v6, Lesp;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 199
    iput-object v0, v6, Lesp;->b:Ljava/lang/String;

    .line 202
    :cond_1
    iget-object v2, v6, Lesp;->b:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v2

    iput-char v2, v6, Lesp;->a:C
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    :try_start_1
    iget-object v2, p0, Lesn;->d:Ljava/lang/Class;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 211
    :try_start_2
    invoke-direct {p0, v6, v0, v3}, Lesn;->a(Lesp;Ljava/lang/reflect/Field;Z)V

    .line 213
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 214
    goto :goto_0

    .line 183
    :cond_2
    const-class v1, Ljava/lang/Integer;

    if-eq v1, v0, :cond_3

    const-class v1, Ljava/lang/Long;

    if-eq v1, v0, :cond_3

    const-class v1, Ljava/lang/Float;

    if-eq v1, v0, :cond_3

    const-class v1, Ljava/lang/Double;

    if-eq v1, v0, :cond_3

    const-class v1, Ljava/lang/Boolean;

    if-eq v1, v0, :cond_3

    const-class v1, Ljava/math/BigInteger;

    if-ne v1, v0, :cond_4

    .line 186
    :cond_3
    iget-object v0, p0, Lesn;->e:[Ljava/lang/Object;

    add-int/lit8 v1, v2, 0x1

    aget-object v0, v0, v2

    goto :goto_2

    .line 187
    :cond_4
    instance-of v1, v0, Ljava/lang/Class;

    if-eqz v1, :cond_6

    .line 188
    check-cast v0, Ljava/lang/Class;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 190
    :try_start_3
    const-string v1, "getInstance"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v1, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lesn;

    iput-object v1, v6, Lesp;->f:Lesn;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 194
    :try_start_4
    iget-object v0, p0, Lesn;->e:[Ljava/lang/Object;

    add-int/lit8 v1, v2, 0x1

    aget-object v0, v0, v2

    goto :goto_2

    .line 191
    :catch_0
    move-exception v1

    .line 192
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x16

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid EsJson class: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 208
    :catch_1
    move-exception v1

    :try_start_5
    new-instance v1, Ljava/lang/IllegalStateException;

    iget-object v2, p0, Lesn;->d:Ljava/lang/Class;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "No such field: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 216
    :cond_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lesp;

    iput-object v0, p0, Lesn;->f:[Lesp;

    .line 217
    iget-object v0, p0, Lesn;->f:[Lesp;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 218
    monitor-exit p0

    return-void

    :cond_6
    move v1, v2

    goto/16 :goto_2

    :cond_7
    move v3, v4

    move v2, v1

    goto/16 :goto_1
.end method


# virtual methods
.method public a(Landroid/util/JsonReader;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonReader;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 420
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lesn;->a(Landroid/util/JsonReader;Lesr;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/util/JsonReader;Lesr;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonReader;",
            "Lesr;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 429
    iget-object v0, p0, Lesn;->f:[Lesp;

    if-nez v0, :cond_1

    .line 430
    iget-object v0, p0, Lesn;->d:Ljava/lang/Class;

    if-nez v0, :cond_0

    .line 431
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "A JSON class must either configure the automatic parser or override read(JsonReader)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 434
    :cond_0
    invoke-direct {p0}, Lesn;->c()V

    .line 437
    :cond_1
    invoke-virtual {p0}, Lesn;->b()Ljava/lang/Object;

    move-result-object v5

    .line 438
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 439
    :cond_2
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 440
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v4

    .line 442
    invoke-virtual {v4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    move v0, v1

    .line 443
    :goto_1
    iget-object v2, p0, Lesn;->f:[Lesp;

    array-length v2, v2

    if-ge v0, v2, :cond_a

    .line 444
    iget-object v2, p0, Lesn;->f:[Lesp;

    aget-object v2, v2, v0

    .line 446
    iget-char v7, v2, Lesp;->a:C

    if-ne v7, v6, :cond_3

    iget-object v7, v2, Lesp;->b:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v4, v2

    .line 452
    :goto_2
    if-eqz v4, :cond_7

    .line 454
    iget v0, v4, Lesp;->d:I

    packed-switch v0, :pswitch_data_0

    .line 577
    :pswitch_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v2, v3

    .line 582
    :goto_3
    :try_start_0
    iget-object v0, v4, Lesp;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v0, v5, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 583
    :catch_0
    move-exception v0

    .line 584
    new-instance v1, Ljava/io/IOException;

    iget-object v3, v4, Lesp;->c:Ljava/lang/reflect/Field;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 586
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1d

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Cannot assign field value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 443
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 457
    :pswitch_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 458
    goto :goto_3

    .line 461
    :pswitch_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v2, v0

    .line 462
    goto :goto_3

    .line 465
    :pswitch_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    move-object v2, v0

    .line 466
    goto :goto_3

    .line 469
    :pswitch_4
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v2, v0

    .line 470
    goto/16 :goto_3

    .line 473
    :pswitch_5
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    move-object v2, v0

    .line 474
    goto/16 :goto_3

    .line 477
    :pswitch_6
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextDouble()D

    move-result-wide v6

    double-to-float v0, v6

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    move-object v2, v0

    .line 478
    goto/16 :goto_3

    .line 481
    :pswitch_7
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    move-object v2, v0

    .line 482
    goto/16 :goto_3

    .line 485
    :pswitch_8
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextDouble()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    move-object v2, v0

    .line 486
    goto/16 :goto_3

    .line 489
    :pswitch_9
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    move-object v2, v0

    .line 490
    goto/16 :goto_3

    .line 493
    :pswitch_a
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_4
    move-object v2, v0

    .line 494
    goto/16 :goto_3

    .line 493
    :cond_4
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_4

    .line 497
    :pswitch_b
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    move-object v2, v0

    .line 498
    goto/16 :goto_3

    .line 502
    :pswitch_c
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    move-object v2, v0

    .line 503
    goto/16 :goto_3

    .line 506
    :pswitch_d
    iget-object v0, v4, Lesp;->f:Lesn;

    invoke-virtual {v0, p1, p2}, Lesn;->a(Landroid/util/JsonReader;Lesr;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    .line 507
    goto/16 :goto_3

    .line 510
    :pswitch_e
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 511
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginArray()V

    .line 512
    :goto_5
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 513
    iget v0, v4, Lesp;->e:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_5

    .line 516
    :sswitch_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 520
    :sswitch_1
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 524
    :sswitch_2
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 528
    :sswitch_3
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextLong()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 532
    :sswitch_4
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 536
    :sswitch_5
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextDouble()D

    move-result-wide v6

    double-to-float v0, v6

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 540
    :sswitch_6
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 544
    :sswitch_7
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextDouble()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 548
    :sswitch_8
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 552
    :sswitch_9
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextBoolean()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_6
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_5
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_6

    .line 556
    :sswitch_a
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 561
    :sswitch_b
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 565
    :sswitch_c
    iget-object v0, v4, Lesp;->f:Lesn;

    invoke-virtual {v0, p1}, Lesn;->a(Landroid/util/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    .line 571
    :cond_6
    invoke-virtual {p1}, Landroid/util/JsonReader;->endArray()V

    goto/16 :goto_3

    .line 591
    :cond_7
    if-eqz p2, :cond_9

    .line 592
    invoke-interface {p2}, Lesr;->a()Z

    move-result v0

    .line 595
    :goto_7
    if-nez v0, :cond_2

    .line 597
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 601
    :cond_8
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 602
    return-object v5

    :cond_9
    move v0, v1

    goto :goto_7

    :cond_a
    move-object v4, v3

    goto/16 :goto_2

    .line 454
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_c
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_9
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 513
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_3
        0x3 -> :sswitch_5
        0x4 -> :sswitch_7
        0x5 -> :sswitch_9
        0x6 -> :sswitch_b
        0x8 -> :sswitch_c
        0x20 -> :sswitch_0
        0x21 -> :sswitch_2
        0x22 -> :sswitch_4
        0x23 -> :sswitch_6
        0x24 -> :sswitch_8
        0x25 -> :sswitch_a
        0x26 -> :sswitch_b
    .end sparse-switch
.end method

.method public a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 302
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lesn;->a(Ljava/io/InputStream;Lesr;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/io/InputStream;Lesr;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Lesr;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 310
    new-instance v0, Landroid/util/JsonReader;

    new-instance v1, Ljava/io/InputStreamReader;

    sget-object v2, Lesn;->c:Ljava/nio/charset/Charset;

    invoke-direct {v1, p1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Landroid/util/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 311
    invoke-virtual {p0, v0, p2}, Lesn;->a(Landroid/util/JsonReader;Lesr;)Ljava/lang/Object;

    move-result-object v1

    .line 312
    invoke-virtual {v0}, Landroid/util/JsonReader;->close()V

    .line 313
    return-object v1
.end method

.method public a([B)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TT;"
        }
    .end annotation

    .prologue
    .line 321
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0, v0}, Lesn;->a(Ljava/io/InputStream;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 322
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 323
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Cannot parse JSON using "

    .line 324
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lesn;->d:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 225
    array-length v0, v2

    new-array v0, v0, [Lesp;

    iput-object v0, p0, Lesn;->f:[Lesp;

    move v0, v1

    .line 226
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 227
    aget-object v3, v2, v0

    .line 228
    new-instance v4, Lesp;

    invoke-direct {v4}, Lesp;-><init>()V

    .line 229
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lesp;->b:Ljava/lang/String;

    .line 230
    iget-object v5, v4, Lesp;->b:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    iput-char v5, v4, Lesp;->a:C

    .line 231
    invoke-direct {p0, v4, v3, v1}, Lesn;->a(Lesp;Ljava/lang/reflect/Field;Z)V

    .line 232
    iget v3, v4, Lesp;->d:I

    if-eq v3, v6, :cond_0

    iget v3, v4, Lesp;->e:I

    if-ne v3, v6, :cond_1

    .line 234
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot use default JSON for object containing fields of non-basic types: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lesn;->d:Ljava/lang/Class;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v4, Lesp;->c:Ljava/lang/reflect/Field;

    .line 236
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 238
    :cond_1
    iget-object v3, p0, Lesn;->f:[Lesp;

    aput-object v4, v3, v0

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    :cond_2
    return-void
.end method

.method public a(Landroid/util/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 611
    invoke-direct {p0, p1, p2}, Lesn;->b(Landroid/util/JsonWriter;Ljava/lang/Object;)V

    .line 612
    return-void
.end method

.method public a(Ljava/io/OutputStream;Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/OutputStream;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 347
    new-instance v0, Landroid/util/JsonWriter;

    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    sget-object v3, Lesn;->c:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    const/16 v3, 0x2000

    invoke-direct {v1, v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    invoke-direct {v0, v1}, Landroid/util/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 349
    invoke-virtual {p0, v0, p2}, Lesn;->a(Landroid/util/JsonWriter;Ljava/lang/Object;)V

    .line 350
    invoke-virtual {v0}, Landroid/util/JsonWriter;->flush()V

    .line 351
    return-void
.end method

.method public a(Ljava/lang/Object;)[B
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[B"
        }
    .end annotation

    .prologue
    .line 357
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 359
    :try_start_0
    invoke-virtual {p0, v0, p1}, Lesn;->a(Ljava/io/OutputStream;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 360
    :catch_0
    move-exception v1

    .line 361
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Cannot generate JSON using "

    .line 362
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 408
    :try_start_0
    iget-object v0, p0, Lesn;->d:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 409
    :catch_0
    move-exception v0

    .line 410
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Cannot create new instance"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected b(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 762
    iget-object v0, p0, Lesn;->f:[Lesp;

    array-length v0, v0

    new-array v1, v0, [Ljava/lang/Object;

    .line 763
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lesn;->f:[Lesp;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 765
    :try_start_0
    iget-object v2, p0, Lesn;->f:[Lesp;

    aget-object v2, v2, v0

    iget-object v2, v2, Lesp;->c:Ljava/lang/reflect/Field;

    invoke-virtual {v2, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 763
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 766
    :catch_0
    move-exception v1

    .line 767
    new-instance v2, Ljava/lang/RuntimeException;

    iget-object v3, p0, Lesn;->f:[Lesp;

    aget-object v0, v3, v0

    iget-object v0, v0, Lesp;->c:Ljava/lang/reflect/Field;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot obtain field value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 771
    :cond_0
    return-object v1
.end method
