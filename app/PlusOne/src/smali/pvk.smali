.class public final Lpvk;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lpvj;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 200
    invoke-direct {p0}, Loxq;-><init>()V

    .line 203
    sget-object v0, Lpvj;->a:[Lpvj;

    iput-object v0, p0, Lpvk;->a:[Lpvj;

    .line 200
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 236
    .line 237
    iget-object v1, p0, Lpvk;->a:[Lpvj;

    if-eqz v1, :cond_1

    .line 238
    iget-object v2, p0, Lpvk;->a:[Lpvj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 239
    if-eqz v4, :cond_0

    .line 240
    const/4 v5, 0x1

    .line 241
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 238
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    :cond_1
    iget-object v1, p0, Lpvk;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 246
    const/4 v1, 0x2

    iget-object v2, p0, Lpvk;->b:Ljava/lang/Integer;

    .line 247
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    :cond_2
    iget-object v1, p0, Lpvk;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 250
    const/4 v1, 0x3

    iget-object v2, p0, Lpvk;->c:Ljava/lang/String;

    .line 251
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_3
    iget-object v1, p0, Lpvk;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 254
    const/4 v1, 0x4

    iget-object v2, p0, Lpvk;->d:Ljava/lang/String;

    .line 255
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_4
    iget-object v1, p0, Lpvk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    iput v0, p0, Lpvk;->ai:I

    .line 259
    return v0
.end method

.method public a(Loxn;)Lpvk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 267
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 268
    sparse-switch v0, :sswitch_data_0

    .line 272
    iget-object v2, p0, Lpvk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 273
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpvk;->ah:Ljava/util/List;

    .line 276
    :cond_1
    iget-object v2, p0, Lpvk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 278
    :sswitch_0
    return-object p0

    .line 283
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 284
    iget-object v0, p0, Lpvk;->a:[Lpvj;

    if-nez v0, :cond_3

    move v0, v1

    .line 285
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpvj;

    .line 286
    iget-object v3, p0, Lpvk;->a:[Lpvj;

    if-eqz v3, :cond_2

    .line 287
    iget-object v3, p0, Lpvk;->a:[Lpvj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 289
    :cond_2
    iput-object v2, p0, Lpvk;->a:[Lpvj;

    .line 290
    :goto_2
    iget-object v2, p0, Lpvk;->a:[Lpvj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 291
    iget-object v2, p0, Lpvk;->a:[Lpvj;

    new-instance v3, Lpvj;

    invoke-direct {v3}, Lpvj;-><init>()V

    aput-object v3, v2, v0

    .line 292
    iget-object v2, p0, Lpvk;->a:[Lpvj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 293
    invoke-virtual {p1}, Loxn;->a()I

    .line 290
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 284
    :cond_3
    iget-object v0, p0, Lpvk;->a:[Lpvj;

    array-length v0, v0

    goto :goto_1

    .line 296
    :cond_4
    iget-object v2, p0, Lpvk;->a:[Lpvj;

    new-instance v3, Lpvj;

    invoke-direct {v3}, Lpvj;-><init>()V

    aput-object v3, v2, v0

    .line 297
    iget-object v2, p0, Lpvk;->a:[Lpvj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 301
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpvk;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 305
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvk;->c:Ljava/lang/String;

    goto :goto_0

    .line 309
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpvk;->d:Ljava/lang/String;

    goto :goto_0

    .line 268
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 214
    iget-object v0, p0, Lpvk;->a:[Lpvj;

    if-eqz v0, :cond_1

    .line 215
    iget-object v1, p0, Lpvk;->a:[Lpvj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 216
    if-eqz v3, :cond_0

    .line 217
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 215
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    :cond_1
    iget-object v0, p0, Lpvk;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 222
    const/4 v0, 0x2

    iget-object v1, p0, Lpvk;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 224
    :cond_2
    iget-object v0, p0, Lpvk;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 225
    const/4 v0, 0x3

    iget-object v1, p0, Lpvk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 227
    :cond_3
    iget-object v0, p0, Lpvk;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 228
    const/4 v0, 0x4

    iget-object v1, p0, Lpvk;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 230
    :cond_4
    iget-object v0, p0, Lpvk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 232
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lpvk;->a(Loxn;)Lpvk;

    move-result-object v0

    return-object v0
.end method
