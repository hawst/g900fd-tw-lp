.class public final Lpof;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpof;


# instance fields
.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x0

    new-array v0, v0, [Lpof;

    sput-object v0, Lpof;->a:[Lpof;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 444
    invoke-direct {p0}, Loxq;-><init>()V

    .line 454
    const/high16 v0, -0x80000000

    iput v0, p0, Lpof;->c:I

    .line 444
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 476
    const/4 v0, 0x0

    .line 477
    iget-object v1, p0, Lpof;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 478
    const/4 v0, 0x1

    iget-object v1, p0, Lpof;->b:Ljava/lang/String;

    .line 479
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 481
    :cond_0
    iget v1, p0, Lpof;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 482
    const/4 v1, 0x2

    iget v2, p0, Lpof;->c:I

    .line 483
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 485
    :cond_1
    iget-object v1, p0, Lpof;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 486
    const/4 v1, 0x3

    iget-object v2, p0, Lpof;->d:Ljava/lang/Integer;

    .line 487
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 489
    :cond_2
    iget-object v1, p0, Lpof;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    iput v0, p0, Lpof;->ai:I

    .line 491
    return v0
.end method

.method public a(Loxn;)Lpof;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 499
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 500
    sparse-switch v0, :sswitch_data_0

    .line 504
    iget-object v1, p0, Lpof;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 505
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpof;->ah:Ljava/util/List;

    .line 508
    :cond_1
    iget-object v1, p0, Lpof;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 510
    :sswitch_0
    return-object p0

    .line 515
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpof;->b:Ljava/lang/String;

    goto :goto_0

    .line 519
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 520
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 522
    :cond_2
    iput v0, p0, Lpof;->c:I

    goto :goto_0

    .line 524
    :cond_3
    iput v2, p0, Lpof;->c:I

    goto :goto_0

    .line 529
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lpof;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 500
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Lpof;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 462
    const/4 v0, 0x1

    iget-object v1, p0, Lpof;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 464
    :cond_0
    iget v0, p0, Lpof;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 465
    const/4 v0, 0x2

    iget v1, p0, Lpof;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 467
    :cond_1
    iget-object v0, p0, Lpof;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 468
    const/4 v0, 0x3

    iget-object v1, p0, Lpof;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 470
    :cond_2
    iget-object v0, p0, Lpof;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 472
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 440
    invoke-virtual {p0, p1}, Lpof;->a(Loxn;)Lpof;

    move-result-object v0

    return-object v0
.end method
