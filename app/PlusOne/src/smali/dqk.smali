.class public final Ldqk;
.super Lhny;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Landroid/content/Context;

.field private e:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 37
    const-string v0, "UploadCoverPhotoTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Ldqk;->d:Landroid/content/Context;

    .line 40
    iput p2, p0, Ldqk;->b:I

    .line 41
    iput-object p3, p0, Ldqk;->c:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Ldqk;->a:Ljava/lang/String;

    .line 43
    iput-object p5, p0, Ldqk;->e:Landroid/graphics/RectF;

    .line 44
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 12

    .prologue
    const-wide/16 v6, 0x0

    const v11, 0x7f0a0592

    const/4 v10, 0x1

    const/4 v5, 0x0

    const/4 v9, 0x3

    .line 48
    iget-object v0, p0, Ldqk;->d:Landroid/content/Context;

    invoke-static {v0}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v0

    .line 49
    iget-object v1, p0, Ldqk;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lhrx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    iget v8, p0, Ldqk;->b:I

    .line 52
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 53
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    new-instance v2, Ljvu;

    iget-object v3, p0, Ldqk;->d:Landroid/content/Context;

    iget-object v4, p0, Ldqk;->c:Ljava/lang/String;

    invoke-direct {v2, v3, v8, v4, v1}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    .line 56
    invoke-virtual {v2}, Ljvu;->l()V

    .line 59
    invoke-virtual {v2}, Ljvu;->t()Z

    move-result v1

    if-nez v1, :cond_0

    .line 60
    invoke-virtual {v2, v0}, Ljvu;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 61
    :cond_0
    const-string v0, "UploadCoverPhoto"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    iget v0, v2, Lkff;->i:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "CheckPhotosExistenceOperation error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    move-wide v2, v6

    .line 73
    :goto_0
    cmp-long v0, v2, v6

    if-nez v0, :cond_5

    .line 74
    new-instance v0, Ldog;

    iget-object v1, p0, Ldqk;->d:Landroid/content/Context;

    new-instance v2, Lkfo;

    iget-object v3, p0, Ldqk;->d:Landroid/content/Context;

    invoke-direct {v2, v3, v8}, Lkfo;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Ldqk;->c:Ljava/lang/String;

    const-string v4, "scrapbook"

    iget-object v5, p0, Ldqk;->a:Ljava/lang/String;

    iget-object v6, p0, Ldqk;->e:Landroid/graphics/RectF;

    invoke-direct/range {v0 .. v6}, Ldog;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/RectF;)V

    .line 77
    invoke-virtual {v0}, Ldog;->l()V

    .line 79
    invoke-virtual {v0}, Ldog;->t()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 80
    const-string v1, "UploadCoverPhoto"

    invoke-static {v1, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 81
    iget v1, v0, Lkff;->i:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "UploadMediaOperation error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    :cond_2
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    .line 84
    invoke-virtual {p0}, Ldqk;->f()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    .line 106
    :goto_1
    return-object v0

    .line 67
    :cond_3
    invoke-virtual {v2, v0}, Ljvu;->c(Ljava/lang/String;)J

    move-result-wide v0

    move-wide v2, v0

    goto :goto_0

    .line 87
    :cond_4
    new-instance v0, Lhoz;

    invoke-direct {v0, v10}, Lhoz;-><init>(Z)V

    goto :goto_1

    .line 93
    :cond_5
    new-instance v0, Ldnk;

    iget-object v1, p0, Ldqk;->d:Landroid/content/Context;

    .line 94
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Ldqk;->e:Landroid/graphics/RectF;

    move v2, v8

    move v6, v5

    invoke-direct/range {v0 .. v6}, Ldnk;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/graphics/RectF;IZ)V

    .line 96
    invoke-virtual {v0}, Ldnk;->l()V

    .line 98
    invoke-virtual {v0}, Ldnk;->t()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 99
    const-string v1, "UploadCoverPhoto"

    invoke-static {v1, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 100
    iget v1, v0, Lkff;->i:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "SetScrapbookPhotoOperation error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 102
    :cond_6
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v0, v0, Lkff;->k:Ljava/lang/Exception;

    .line 103
    invoke-virtual {p0}, Ldqk;->f()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_1

    .line 106
    :cond_7
    new-instance v0, Lhoz;

    invoke-direct {v0, v10}, Lhoz;-><init>(Z)V

    goto :goto_1
.end method

.method public b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Ldqk;->f()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a08ef

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
