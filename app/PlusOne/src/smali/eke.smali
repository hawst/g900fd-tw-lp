.class final Leke;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lekd;


# direct methods
.method constructor <init>(Lekd;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Leke;->a:Lekd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 122
    iget-object v0, p0, Leke;->a:Lekd;

    iput-boolean v6, v0, Lekd;->R:Z

    .line 123
    iget-object v0, p0, Leke;->a:Lekd;

    invoke-static {v0}, Lekd;->a(Lekd;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    new-instance v0, Ldyt;

    iget-object v1, p0, Leke;->a:Lekd;

    invoke-static {v1}, Lekd;->b(Lekd;)Llnl;

    move-result-object v1

    iget-object v2, p0, Leke;->a:Lekd;

    iget-object v2, v2, Lekd;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    sget-object v3, Lekd;->N:[Ljava/lang/String;

    iget-object v4, p0, Leke;->a:Lekd;

    .line 125
    invoke-static {v4}, Lekd;->c(Lekd;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Ldyt;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 129
    :goto_0
    return-object v0

    .line 127
    :cond_0
    new-instance v0, Lekz;

    iget-object v1, p0, Leke;->a:Lekd;

    invoke-static {v1}, Lekd;->d(Lekd;)Llnl;

    move-result-object v1

    iget-object v2, p0, Leke;->a:Lekd;

    iget-object v2, v2, Lekd;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    sget-object v3, Lekd;->N:[Ljava/lang/String;

    iget-object v4, p0, Leke;->a:Lekd;

    .line 129
    invoke-static {v4}, Lekd;->a(Lekd;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "interaction_sort_key DESC"

    invoke-direct/range {v0 .. v6}, Lekz;-><init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Leke;->a:Lekd;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lekd;->R:Z

    .line 140
    iget-object v0, p0, Leke;->a:Lekd;

    invoke-static {v0}, Lekd;->c(Lekd;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 141
    iget-object v0, p0, Leke;->a:Lekd;

    invoke-static {v0}, Lekd;->a(Lekd;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    :cond_0
    iget-object v0, p0, Leke;->a:Lekd;

    invoke-static {v0}, Lekd;->c(Lekd;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    :cond_1
    iget-object v0, p0, Leke;->a:Lekd;

    invoke-virtual {v0, p1}, Lekd;->a(Landroid/database/Cursor;)V

    .line 150
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 154
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 118
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Leke;->a(Landroid/database/Cursor;)V

    return-void
.end method
