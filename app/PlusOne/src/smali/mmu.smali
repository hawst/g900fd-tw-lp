.class public final Lmmu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmmu;


# instance fields
.field public b:Ljava/lang/Long;

.field public c:I

.field public d:Ljava/lang/Integer;

.field public e:Lmmw;

.field public f:Lmmy;

.field public g:Lmmv;

.field public h:Lmmx;

.field public i:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6565
    const/4 v0, 0x0

    new-array v0, v0, [Lmmu;

    sput-object v0, Lmmu;->a:[Lmmu;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6566
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6988
    const/high16 v0, -0x80000000

    iput v0, p0, Lmmu;->c:I

    .line 6993
    iput-object v1, p0, Lmmu;->e:Lmmw;

    .line 6996
    iput-object v1, p0, Lmmu;->f:Lmmy;

    .line 6999
    iput-object v1, p0, Lmmu;->g:Lmmv;

    .line 7002
    iput-object v1, p0, Lmmu;->h:Lmmx;

    .line 6566
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 7039
    const/4 v0, 0x0

    .line 7040
    iget-object v1, p0, Lmmu;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 7041
    const/4 v0, 0x1

    iget-object v1, p0, Lmmu;->b:Ljava/lang/Long;

    .line 7042
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7044
    :cond_0
    iget v1, p0, Lmmu;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 7045
    const/4 v1, 0x2

    iget v2, p0, Lmmu;->c:I

    .line 7046
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7048
    :cond_1
    iget-object v1, p0, Lmmu;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 7049
    const/4 v1, 0x4

    iget-object v2, p0, Lmmu;->d:Ljava/lang/Integer;

    .line 7050
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7052
    :cond_2
    iget-object v1, p0, Lmmu;->e:Lmmw;

    if-eqz v1, :cond_3

    .line 7053
    const/4 v1, 0x5

    iget-object v2, p0, Lmmu;->e:Lmmw;

    .line 7054
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7056
    :cond_3
    iget-object v1, p0, Lmmu;->f:Lmmy;

    if-eqz v1, :cond_4

    .line 7057
    const/4 v1, 0x6

    iget-object v2, p0, Lmmu;->f:Lmmy;

    .line 7058
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7060
    :cond_4
    iget-object v1, p0, Lmmu;->g:Lmmv;

    if-eqz v1, :cond_5

    .line 7061
    const/4 v1, 0x7

    iget-object v2, p0, Lmmu;->g:Lmmv;

    .line 7062
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7064
    :cond_5
    iget-object v1, p0, Lmmu;->h:Lmmx;

    if-eqz v1, :cond_6

    .line 7065
    const/16 v1, 0x8

    iget-object v2, p0, Lmmu;->h:Lmmx;

    .line 7066
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7068
    :cond_6
    iget-object v1, p0, Lmmu;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 7069
    const/16 v1, 0x9

    iget-object v2, p0, Lmmu;->i:Ljava/lang/Boolean;

    .line 7070
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7072
    :cond_7
    iget-object v1, p0, Lmmu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7073
    iput v0, p0, Lmmu;->ai:I

    .line 7074
    return v0
.end method

.method public a(Loxn;)Lmmu;
    .locals 2

    .prologue
    .line 7082
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7083
    sparse-switch v0, :sswitch_data_0

    .line 7087
    iget-object v1, p0, Lmmu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7088
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmmu;->ah:Ljava/util/List;

    .line 7091
    :cond_1
    iget-object v1, p0, Lmmu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7093
    :sswitch_0
    return-object p0

    .line 7098
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmmu;->b:Ljava/lang/Long;

    goto :goto_0

    .line 7102
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 7103
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 7109
    :cond_2
    iput v0, p0, Lmmu;->c:I

    goto :goto_0

    .line 7111
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lmmu;->c:I

    goto :goto_0

    .line 7116
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmmu;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 7120
    :sswitch_4
    iget-object v0, p0, Lmmu;->e:Lmmw;

    if-nez v0, :cond_4

    .line 7121
    new-instance v0, Lmmw;

    invoke-direct {v0}, Lmmw;-><init>()V

    iput-object v0, p0, Lmmu;->e:Lmmw;

    .line 7123
    :cond_4
    iget-object v0, p0, Lmmu;->e:Lmmw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7127
    :sswitch_5
    iget-object v0, p0, Lmmu;->f:Lmmy;

    if-nez v0, :cond_5

    .line 7128
    new-instance v0, Lmmy;

    invoke-direct {v0}, Lmmy;-><init>()V

    iput-object v0, p0, Lmmu;->f:Lmmy;

    .line 7130
    :cond_5
    iget-object v0, p0, Lmmu;->f:Lmmy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7134
    :sswitch_6
    iget-object v0, p0, Lmmu;->g:Lmmv;

    if-nez v0, :cond_6

    .line 7135
    new-instance v0, Lmmv;

    invoke-direct {v0}, Lmmv;-><init>()V

    iput-object v0, p0, Lmmu;->g:Lmmv;

    .line 7137
    :cond_6
    iget-object v0, p0, Lmmu;->g:Lmmv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7141
    :sswitch_7
    iget-object v0, p0, Lmmu;->h:Lmmx;

    if-nez v0, :cond_7

    .line 7142
    new-instance v0, Lmmx;

    invoke-direct {v0}, Lmmx;-><init>()V

    iput-object v0, p0, Lmmu;->h:Lmmx;

    .line 7144
    :cond_7
    iget-object v0, p0, Lmmu;->h:Lmmx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7148
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmmu;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 7083
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 7009
    iget-object v0, p0, Lmmu;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 7010
    const/4 v0, 0x1

    iget-object v1, p0, Lmmu;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 7012
    :cond_0
    iget v0, p0, Lmmu;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 7013
    const/4 v0, 0x2

    iget v1, p0, Lmmu;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7015
    :cond_1
    iget-object v0, p0, Lmmu;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 7016
    const/4 v0, 0x4

    iget-object v1, p0, Lmmu;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7018
    :cond_2
    iget-object v0, p0, Lmmu;->e:Lmmw;

    if-eqz v0, :cond_3

    .line 7019
    const/4 v0, 0x5

    iget-object v1, p0, Lmmu;->e:Lmmw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7021
    :cond_3
    iget-object v0, p0, Lmmu;->f:Lmmy;

    if-eqz v0, :cond_4

    .line 7022
    const/4 v0, 0x6

    iget-object v1, p0, Lmmu;->f:Lmmy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7024
    :cond_4
    iget-object v0, p0, Lmmu;->g:Lmmv;

    if-eqz v0, :cond_5

    .line 7025
    const/4 v0, 0x7

    iget-object v1, p0, Lmmu;->g:Lmmv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7027
    :cond_5
    iget-object v0, p0, Lmmu;->h:Lmmx;

    if-eqz v0, :cond_6

    .line 7028
    const/16 v0, 0x8

    iget-object v1, p0, Lmmu;->h:Lmmx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7030
    :cond_6
    iget-object v0, p0, Lmmu;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 7031
    const/16 v0, 0x9

    iget-object v1, p0, Lmmu;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7033
    :cond_7
    iget-object v0, p0, Lmmu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7035
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6562
    invoke-virtual {p0, p1}, Lmmu;->a(Loxn;)Lmmu;

    move-result-object v0

    return-object v0
.end method
