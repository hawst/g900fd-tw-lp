.class public final Ldmq;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmii;",
        "Lmij;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;I)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 28
    const-string v4, "registerdevice"

    new-instance v5, Lmii;

    invoke-direct {v5}, Lmii;-><init>()V

    new-instance v6, Lmij;

    invoke-direct {v6}, Lmij;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 31
    const-string v0, "com.google.android.plus.GCM"

    invoke-virtual {p1, v0, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "regIds"

    invoke-interface {v0, v1, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldmq;->a:Ljava/lang/String;

    .line 32
    if-eqz p1, :cond_1

    const/4 v0, -0x1

    if-eq p3, v0, :cond_1

    const-string v0, "com.google.android.plus.GCM"

    invoke-virtual {p1, v0, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "com.google.android.plus.GCM"

    invoke-virtual {p1, v1, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "regIds"

    invoke-interface {v1, v2, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "destinations"

    invoke-interface {v0, v2, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfho;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p3}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ldmq;->b:Z

    .line 33
    return-void

    :cond_0
    move v0, v7

    .line 32
    goto :goto_0

    :cond_1
    move v0, v7

    goto :goto_0
.end method


# virtual methods
.method protected a(Lmii;)V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lmze;

    invoke-direct {v0}, Lmze;-><init>()V

    iput-object v0, p1, Lmii;->a:Lmze;

    .line 43
    iget-object v0, p1, Lmii;->a:Lmze;

    .line 44
    new-instance v1, Lmza;

    invoke-direct {v1}, Lmza;-><init>()V

    iput-object v1, v0, Lmze;->a:Lmza;

    .line 47
    iget-object v1, v0, Lmze;->a:Lmza;

    new-instance v2, Lmzb;

    invoke-direct {v2}, Lmzb;-><init>()V

    iput-object v2, v1, Lmza;->a:Lmzb;

    .line 51
    iget-boolean v1, p0, Ldmq;->b:Z

    if-eqz v1, :cond_0

    .line 52
    iget-object v0, v0, Lmze;->a:Lmza;

    iget-object v0, v0, Lmza;->a:Lmzb;

    iget-object v1, p0, Ldmq;->a:Ljava/lang/String;

    iput-object v1, v0, Lmzb;->a:Ljava/lang/String;

    .line 54
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmii;

    invoke-virtual {p0, p1}, Ldmq;->a(Lmii;)V

    return-void
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Ldmq;->f:Landroid/content/Context;

    iget v1, p0, Ldmq;->c:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Ldhv;->c(Landroid/content/Context;IZ)V

    .line 59
    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0}, Ldmq;->b()V

    return-void
.end method
