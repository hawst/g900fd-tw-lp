.class public final Llzz;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnmm;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4210
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4213
    iput-object v0, p0, Llzz;->apiHeader:Llyr;

    .line 4216
    iput-object v0, p0, Llzz;->a:Lnmm;

    .line 4210
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4233
    const/4 v0, 0x0

    .line 4234
    iget-object v1, p0, Llzz;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 4235
    const/4 v0, 0x1

    iget-object v1, p0, Llzz;->apiHeader:Llyr;

    .line 4236
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4238
    :cond_0
    iget-object v1, p0, Llzz;->a:Lnmm;

    if-eqz v1, :cond_1

    .line 4239
    const/4 v1, 0x2

    iget-object v2, p0, Llzz;->a:Lnmm;

    .line 4240
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4242
    :cond_1
    iget-object v1, p0, Llzz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4243
    iput v0, p0, Llzz;->ai:I

    .line 4244
    return v0
.end method

.method public a(Loxn;)Llzz;
    .locals 2

    .prologue
    .line 4252
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4253
    sparse-switch v0, :sswitch_data_0

    .line 4257
    iget-object v1, p0, Llzz;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4258
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzz;->ah:Ljava/util/List;

    .line 4261
    :cond_1
    iget-object v1, p0, Llzz;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4263
    :sswitch_0
    return-object p0

    .line 4268
    :sswitch_1
    iget-object v0, p0, Llzz;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 4269
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llzz;->apiHeader:Llyr;

    .line 4271
    :cond_2
    iget-object v0, p0, Llzz;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4275
    :sswitch_2
    iget-object v0, p0, Llzz;->a:Lnmm;

    if-nez v0, :cond_3

    .line 4276
    new-instance v0, Lnmm;

    invoke-direct {v0}, Lnmm;-><init>()V

    iput-object v0, p0, Llzz;->a:Lnmm;

    .line 4278
    :cond_3
    iget-object v0, p0, Llzz;->a:Lnmm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4253
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4221
    iget-object v0, p0, Llzz;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 4222
    const/4 v0, 0x1

    iget-object v1, p0, Llzz;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4224
    :cond_0
    iget-object v0, p0, Llzz;->a:Lnmm;

    if-eqz v0, :cond_1

    .line 4225
    const/4 v0, 0x2

    iget-object v1, p0, Llzz;->a:Lnmm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4227
    :cond_1
    iget-object v0, p0, Llzz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4229
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4206
    invoke-virtual {p0, p1}, Llzz;->a(Loxn;)Llzz;

    move-result-object v0

    return-object v0
.end method
