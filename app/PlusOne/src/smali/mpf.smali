.class public final Lmpf;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:[Lmpg;

.field private d:Ljava/lang/Boolean;

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 114
    invoke-direct {p0}, Loxq;-><init>()V

    .line 117
    iput v1, p0, Lmpf;->a:I

    .line 120
    iput v1, p0, Lmpf;->b:I

    .line 123
    sget-object v0, Lmpg;->a:[Lmpg;

    iput-object v0, p0, Lmpf;->c:[Lmpg;

    .line 128
    iput v1, p0, Lmpf;->e:I

    .line 131
    iput v1, p0, Lmpf;->f:I

    .line 134
    iput v1, p0, Lmpf;->g:I

    .line 114
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v6, -0x80000000

    .line 170
    .line 171
    iget v0, p0, Lmpf;->a:I

    if-eq v0, v6, :cond_7

    .line 172
    const/4 v0, 0x1

    iget v2, p0, Lmpf;->a:I

    .line 173
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 175
    :goto_0
    iget v2, p0, Lmpf;->b:I

    if-eq v2, v6, :cond_0

    .line 176
    const/4 v2, 0x2

    iget v3, p0, Lmpf;->b:I

    .line 177
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 179
    :cond_0
    iget-object v2, p0, Lmpf;->c:[Lmpg;

    if-eqz v2, :cond_2

    .line 180
    iget-object v2, p0, Lmpf;->c:[Lmpg;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 181
    if-eqz v4, :cond_1

    .line 182
    const/4 v5, 0x3

    .line 183
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 180
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 187
    :cond_2
    iget-object v1, p0, Lmpf;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 188
    const/4 v1, 0x4

    iget-object v2, p0, Lmpf;->d:Ljava/lang/Boolean;

    .line 189
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 191
    :cond_3
    iget v1, p0, Lmpf;->e:I

    if-eq v1, v6, :cond_4

    .line 192
    const/4 v1, 0x5

    iget v2, p0, Lmpf;->e:I

    .line 193
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_4
    iget v1, p0, Lmpf;->f:I

    if-eq v1, v6, :cond_5

    .line 196
    const/4 v1, 0x6

    iget v2, p0, Lmpf;->f:I

    .line 197
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_5
    iget v1, p0, Lmpf;->g:I

    if-eq v1, v6, :cond_6

    .line 200
    const/4 v1, 0x7

    iget v2, p0, Lmpf;->g:I

    .line 201
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_6
    iget-object v1, p0, Lmpf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    iput v0, p0, Lmpf;->ai:I

    .line 205
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmpf;
    .locals 8

    .prologue
    const/16 v7, 0x28

    const/16 v6, 0x1e

    const/16 v5, 0x14

    const/16 v4, 0xa

    const/4 v1, 0x0

    .line 213
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 214
    sparse-switch v0, :sswitch_data_0

    .line 218
    iget-object v2, p0, Lmpf;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 219
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmpf;->ah:Ljava/util/List;

    .line 222
    :cond_1
    iget-object v2, p0, Lmpf;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    :sswitch_0
    return-object p0

    .line 229
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 230
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-eq v0, v7, :cond_2

    const/16 v2, 0x32

    if-ne v0, v2, :cond_3

    .line 236
    :cond_2
    iput v0, p0, Lmpf;->a:I

    goto :goto_0

    .line 238
    :cond_3
    iput v1, p0, Lmpf;->a:I

    goto :goto_0

    .line 243
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 244
    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    if-eq v0, v7, :cond_4

    const/16 v2, 0x32

    if-ne v0, v2, :cond_5

    .line 250
    :cond_4
    iput v0, p0, Lmpf;->b:I

    goto :goto_0

    .line 252
    :cond_5
    iput v1, p0, Lmpf;->b:I

    goto :goto_0

    .line 257
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 258
    iget-object v0, p0, Lmpf;->c:[Lmpg;

    if-nez v0, :cond_7

    move v0, v1

    .line 259
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmpg;

    .line 260
    iget-object v3, p0, Lmpf;->c:[Lmpg;

    if-eqz v3, :cond_6

    .line 261
    iget-object v3, p0, Lmpf;->c:[Lmpg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 263
    :cond_6
    iput-object v2, p0, Lmpf;->c:[Lmpg;

    .line 264
    :goto_2
    iget-object v2, p0, Lmpf;->c:[Lmpg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    .line 265
    iget-object v2, p0, Lmpf;->c:[Lmpg;

    new-instance v3, Lmpg;

    invoke-direct {v3}, Lmpg;-><init>()V

    aput-object v3, v2, v0

    .line 266
    iget-object v2, p0, Lmpf;->c:[Lmpg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 267
    invoke-virtual {p1}, Loxn;->a()I

    .line 264
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 258
    :cond_7
    iget-object v0, p0, Lmpf;->c:[Lmpg;

    array-length v0, v0

    goto :goto_1

    .line 270
    :cond_8
    iget-object v2, p0, Lmpf;->c:[Lmpg;

    new-instance v3, Lmpg;

    invoke-direct {v3}, Lmpg;-><init>()V

    aput-object v3, v2, v0

    .line 271
    iget-object v2, p0, Lmpf;->c:[Lmpg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 275
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmpf;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 279
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 280
    if-eqz v0, :cond_9

    if-eq v0, v4, :cond_9

    if-eq v0, v5, :cond_9

    if-eq v0, v6, :cond_9

    if-eq v0, v7, :cond_9

    const/16 v2, 0x32

    if-ne v0, v2, :cond_a

    .line 286
    :cond_9
    iput v0, p0, Lmpf;->e:I

    goto/16 :goto_0

    .line 288
    :cond_a
    iput v1, p0, Lmpf;->e:I

    goto/16 :goto_0

    .line 293
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 294
    if-eqz v0, :cond_b

    if-eq v0, v4, :cond_b

    if-eq v0, v5, :cond_b

    if-eq v0, v6, :cond_b

    if-eq v0, v7, :cond_b

    const/16 v2, 0x32

    if-ne v0, v2, :cond_c

    .line 300
    :cond_b
    iput v0, p0, Lmpf;->f:I

    goto/16 :goto_0

    .line 302
    :cond_c
    iput v1, p0, Lmpf;->f:I

    goto/16 :goto_0

    .line 307
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 308
    if-eqz v0, :cond_d

    const/4 v2, 0x1

    if-eq v0, v2, :cond_d

    const/4 v2, 0x2

    if-eq v0, v2, :cond_d

    const/4 v2, 0x3

    if-ne v0, v2, :cond_e

    .line 312
    :cond_d
    iput v0, p0, Lmpf;->g:I

    goto/16 :goto_0

    .line 314
    :cond_e
    iput v1, p0, Lmpf;->g:I

    goto/16 :goto_0

    .line 214
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/high16 v5, -0x80000000

    .line 139
    iget v0, p0, Lmpf;->a:I

    if-eq v0, v5, :cond_0

    .line 140
    const/4 v0, 0x1

    iget v1, p0, Lmpf;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 142
    :cond_0
    iget v0, p0, Lmpf;->b:I

    if-eq v0, v5, :cond_1

    .line 143
    const/4 v0, 0x2

    iget v1, p0, Lmpf;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 145
    :cond_1
    iget-object v0, p0, Lmpf;->c:[Lmpg;

    if-eqz v0, :cond_3

    .line 146
    iget-object v1, p0, Lmpf;->c:[Lmpg;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 147
    if-eqz v3, :cond_2

    .line 148
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 146
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    :cond_3
    iget-object v0, p0, Lmpf;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 153
    const/4 v0, 0x4

    iget-object v1, p0, Lmpf;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 155
    :cond_4
    iget v0, p0, Lmpf;->e:I

    if-eq v0, v5, :cond_5

    .line 156
    const/4 v0, 0x5

    iget v1, p0, Lmpf;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 158
    :cond_5
    iget v0, p0, Lmpf;->f:I

    if-eq v0, v5, :cond_6

    .line 159
    const/4 v0, 0x6

    iget v1, p0, Lmpf;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 161
    :cond_6
    iget v0, p0, Lmpf;->g:I

    if-eq v0, v5, :cond_7

    .line 162
    const/4 v0, 0x7

    iget v1, p0, Lmpf;->g:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 164
    :cond_7
    iget-object v0, p0, Lmpf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 166
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lmpf;->a(Loxn;)Lmpf;

    move-result-object v0

    return-object v0
.end method
