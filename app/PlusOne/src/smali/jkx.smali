.class public final Ljkx;
.super Landroid/database/CursorWrapper;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 34
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    const-string v0, "key"

    invoke-virtual {p0, v0}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 42
    invoke-virtual {p0, v0}, Ljkx;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 50
    :try_start_0
    const-string v0, "last_modified_version"

    invoke-virtual {p0, v0}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 51
    invoke-virtual {p0, v0}, Ljkx;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 53
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public c()Llvo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64
    :try_start_0
    new-instance v0, Llvo;

    invoke-direct {v0}, Llvo;-><init>()V

    .line 65
    const-string v2, "simple_collapsed_layout"

    .line 66
    invoke-virtual {p0, v2}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Ljkx;->getBlob(I)[B

    move-result-object v2

    .line 65
    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 76
    :goto_0
    return-object v0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    const-string v2, "GunsCursor"

    const-string v3, "Malformed SimpleCollapsedLayout data for notification."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 70
    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    .line 72
    const-string v2, "GunsCursor"

    const-string v3, "SimpleCollapsedLayout not in db"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 73
    goto :goto_0

    .line 74
    :catch_2
    move-exception v0

    .line 75
    const-string v2, "GunsCursor"

    const-string v3, "NullPointerException when getting SimpleCollapsedLayout"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 76
    goto :goto_0
.end method

.method public d()Llvg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 88
    :try_start_0
    new-instance v0, Llvg;

    invoke-direct {v0}, Llvg;-><init>()V

    .line 89
    const-string v2, "expanded_info"

    .line 90
    invoke-virtual {p0, v2}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2}, Ljkx;->getBlob(I)[B

    move-result-object v2

    .line 89
    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 100
    :goto_0
    return-object v0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    const-string v2, "GunsCursor"

    const-string v3, "Malformed ExpandedInfo data for notification."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 94
    goto :goto_0

    .line 95
    :catch_1
    move-exception v0

    .line 96
    const-string v2, "GunsCursor"

    const-string v3, "ExpandedInfo not in db"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 97
    goto :goto_0

    .line 98
    :catch_2
    move-exception v0

    .line 99
    const-string v2, "GunsCursor"

    const-string v3, "NullPointerException when getting ExpandedInfo"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 100
    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 110
    :try_start_0
    const-string v0, "read_state"

    invoke-virtual {p0, v0}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 111
    invoke-virtual {p0, v0}, Ljkx;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 113
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 123
    :try_start_0
    const-string v0, "priority"

    invoke-virtual {p0, v0}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 124
    invoke-virtual {p0, v0}, Ljkx;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 126
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/lang/Long;
    .locals 4

    .prologue
    .line 135
    :try_start_0
    const-string v0, "creation_time"

    invoke-virtual {p0, v0}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 137
    invoke-virtual {p0, v0}, Ljkx;->getLong(I)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 139
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public h()Llva;
    .locals 4

    .prologue
    .line 148
    const/4 v1, 0x0

    .line 150
    :try_start_0
    const-string v0, "payload"

    invoke-virtual {p0, v0}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 151
    invoke-virtual {p0, v0}, Ljkx;->getBlob(I)[B

    move-result-object v0

    .line 152
    if-eqz v0, :cond_0

    .line 153
    new-instance v2, Llva;

    invoke-direct {v2}, Llva;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Llva;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :goto_0
    return-object v0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    const-string v2, "GunsCursor"

    const-string v3, "Exception trying to parse payload protocol buffer."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    const-string v0, "default_destination"

    invoke-virtual {p0, v0}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 166
    invoke-virtual {p0, v0}, Ljkx;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    const-string v0, "category"

    invoke-virtual {p0, v0}, Ljkx;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 175
    invoke-virtual {p0, v0}, Ljkx;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
