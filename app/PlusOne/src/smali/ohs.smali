.class public final Lohs;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1396
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1427
    const/4 v0, 0x0

    .line 1428
    iget-object v1, p0, Lohs;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1429
    const/4 v0, 0x1

    iget-object v1, p0, Lohs;->a:Ljava/lang/Integer;

    .line 1430
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1432
    :cond_0
    iget-object v1, p0, Lohs;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1433
    const/4 v1, 0x2

    iget-object v2, p0, Lohs;->b:Ljava/lang/String;

    .line 1434
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1436
    :cond_1
    iget-object v1, p0, Lohs;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1437
    const/4 v1, 0x3

    iget-object v2, p0, Lohs;->c:Ljava/lang/String;

    .line 1438
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1440
    :cond_2
    iget-object v1, p0, Lohs;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1441
    const/4 v1, 0x5

    iget-object v2, p0, Lohs;->d:Ljava/lang/Integer;

    .line 1442
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1444
    :cond_3
    iget-object v1, p0, Lohs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1445
    iput v0, p0, Lohs;->ai:I

    .line 1446
    return v0
.end method

.method public a(Loxn;)Lohs;
    .locals 2

    .prologue
    .line 1454
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1455
    sparse-switch v0, :sswitch_data_0

    .line 1459
    iget-object v1, p0, Lohs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1460
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohs;->ah:Ljava/util/List;

    .line 1463
    :cond_1
    iget-object v1, p0, Lohs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1465
    :sswitch_0
    return-object p0

    .line 1470
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohs;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 1474
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohs;->b:Ljava/lang/String;

    goto :goto_0

    .line 1478
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohs;->c:Ljava/lang/String;

    goto :goto_0

    .line 1482
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lohs;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 1455
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1409
    iget-object v0, p0, Lohs;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1410
    const/4 v0, 0x1

    iget-object v1, p0, Lohs;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1412
    :cond_0
    iget-object v0, p0, Lohs;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1413
    const/4 v0, 0x2

    iget-object v1, p0, Lohs;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1415
    :cond_1
    iget-object v0, p0, Lohs;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1416
    const/4 v0, 0x3

    iget-object v1, p0, Lohs;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1418
    :cond_2
    iget-object v0, p0, Lohs;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1419
    const/4 v0, 0x5

    iget-object v1, p0, Lohs;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1421
    :cond_3
    iget-object v0, p0, Lohs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1423
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1392
    invoke-virtual {p0, p1}, Lohs;->a(Loxn;)Lohs;

    move-result-object v0

    return-object v0
.end method
