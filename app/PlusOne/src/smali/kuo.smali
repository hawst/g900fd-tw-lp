.class public final Lkuo;
.super Lfdj;
.source "PG"


# static fields
.field private static a:I


# instance fields
.field private A:Ljava/lang/Boolean;

.field private B:Ljava/lang/Boolean;

.field private C:Lkyk;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private l:I

.field private m:I

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:I

.field private v:Ljava/lang/String;

.field private w:Lnse;

.field private x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkyf;",
            ">;"
        }
    .end annotation
.end field

.field private y:I

.field private z:Lpdt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, -0x1

    sput v0, Lkuo;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct/range {p0 .. p8}, Lfdj;-><init>(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)V

    .line 74
    iput v1, p0, Lkuo;->y:I

    .line 90
    sget v0, Lkuo;->a:I

    if-gez v0, :cond_0

    .line 91
    invoke-super {p0}, Lfdj;->getViewTypeCount()I

    move-result v0

    sput v0, Lkuo;->a:I

    .line 95
    :cond_0
    iput v1, p0, Lkuo;->i:I

    .line 96
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 151
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 152
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    if-nez v1, :cond_2

    .line 153
    const v1, 0x7f0401f4

    .line 154
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    .line 155
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 156
    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 158
    :cond_0
    const-string v1, "SquareStreamAdapter"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "newView() -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const v1, 0x7f0401ef

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 261
    invoke-virtual {p0, v1}, Lkuo;->l(I)Landroid/database/Cursor;

    move-result-object v0

    .line 265
    if-eq p1, v0, :cond_2

    move v0, v1

    .line 266
    :goto_0
    const-string v2, "SquareStreamAdapter"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 267
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "changeStreamHeaderCursor cursorChanged="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 270
    :cond_0
    invoke-super {p0, v1, p1}, Lfdj;->a(ILandroid/database/Cursor;)V

    .line 271
    if-eqz v0, :cond_1

    .line 272
    const/4 v0, -0x1

    invoke-virtual {p0, v1, v0}, Lkuo;->a(ZI)V

    .line 274
    :cond_1
    return-void

    .line 265
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 169
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    if-nez v0, :cond_4

    .line 170
    const-string v0, "SquareStreamAdapter"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "bindView(); "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    :cond_0
    check-cast p1, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;

    .line 174
    invoke-virtual {p0}, Lkuo;->o()Lldx;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    iget-object v0, p0, Lkuo;->A:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    iget v2, p0, Lkuo;->g:I

    iget-object v3, p0, Lkuo;->B:Ljava/lang/Boolean;

    .line 176
    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    .line 175
    invoke-virtual {p1, v0, v2, v3}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(ZIZ)V

    .line 177
    iget-object v0, p0, Lkuo;->x:Ljava/util/List;

    iget v2, p0, Lkuo;->y:I

    invoke-virtual {p1, v0, v2}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(Ljava/util/List;I)V

    .line 178
    iget-object v0, p0, Lkuo;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 179
    iget-boolean v0, p0, Lkuo;->r:Z

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b(Z)V

    iget-object v0, p0, Lkuo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lkuo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b(Ljava/lang/String;)V

    iget v0, p0, Lkuo;->f:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b(I)V

    iget-object v0, p0, Lkuo;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lkuo;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lkuo;->w:Lnse;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(Lnse;)V

    iget-object v0, p0, Lkuo;->z:Lpdt;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(Lpdt;)V

    iget v0, p0, Lkuo;->m:I

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(I)V

    iget-boolean v0, p0, Lkuo;->o:Z

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(Z)V

    invoke-virtual {p0}, Lkuo;->g()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c(I)V

    invoke-virtual {p1}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->b()V

    iget v0, p0, Lkuo;->l:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c(Z)V

    iget-boolean v0, p0, Lkuo;->r:Z

    if-nez v0, :cond_3

    iget v0, p0, Lkuo;->m:I

    if-ne v0, v1, :cond_3

    iget v0, p0, Lkuo;->n:I

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->c()V

    .line 180
    :goto_1
    iget-object v0, p0, Lkuo;->C:Lkyk;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->a(Lkyk;)V

    .line 185
    :cond_1
    :goto_2
    return-void

    .line 179
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/libraries/social/squares/stream/SquareStreamHeaderView;->d()V

    goto :goto_1

    .line 183
    :cond_4
    invoke-virtual {p0}, Lkuo;->o()Lldx;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 321
    instance-of v0, p1, Lgbz;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 322
    check-cast v0, Lgbz;

    invoke-virtual {p0}, Lkuo;->h()Z

    move-result v1

    invoke-virtual {v0, v1}, Lgbz;->d(Z)V

    .line 324
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lfdj;->a(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V

    .line 325
    return-void
.end method

.method public a(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 382
    iput-object p1, p0, Lkuo;->A:Ljava/lang/Boolean;

    .line 383
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkyf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    iput-object p1, p0, Lkuo;->x:Ljava/util/List;

    .line 143
    return-void
.end method

.method public a(Lkyk;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lkuo;->C:Lkyk;

    .line 100
    return-void
.end method

.method public ak()V
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x0

    iput v0, p0, Lkuo;->i:I

    .line 366
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 146
    iput p1, p0, Lkuo;->y:I

    .line 147
    return-void
.end method

.method public b(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 110
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 111
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkuo;->b:Ljava/lang/String;

    .line 112
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkuo;->c:Ljava/lang/String;

    .line 113
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkuo;->d:Ljava/lang/String;

    .line 114
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkuo;->e:Ljava/lang/String;

    .line 115
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lkuo;->f:I

    .line 116
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lkuo;->l:I

    .line 117
    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lkuo;->r:Z

    .line 118
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lkuo;->m:I

    .line 119
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lkuo;->n:I

    .line 120
    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lkuo;->o:Z

    .line 121
    iget-boolean v0, p0, Lkuo;->r:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lkuo;->p:Z

    .line 122
    iget-boolean v0, p0, Lkuo;->r:Z

    if-eqz v0, :cond_4

    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lkuo;->q:Z

    .line 123
    const/16 v0, 0x11

    .line 124
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lkuo;->s:Z

    .line 125
    const/16 v0, 0x1a

    .line 126
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lkuo;->t:Z

    .line 127
    const/16 v0, 0x1c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lkuo;->u:I

    .line 128
    const/16 v0, 0x1e

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkuo;->v:Ljava/lang/String;

    .line 129
    const/16 v0, 0x1f

    .line 130
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 129
    invoke-static {v0}, Lktf;->a([B)Lnse;

    move-result-object v0

    iput-object v0, p0, Lkuo;->w:Lnse;

    .line 131
    new-instance v0, Lpdt;

    invoke-direct {v0}, Lpdt;-><init>()V

    const/16 v3, 0x20

    .line 132
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 131
    invoke-static {v0, v3}, Lhys;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lpdt;

    iput-object v0, p0, Lkuo;->z:Lpdt;

    .line 134
    iget-object v0, p0, Lkuo;->A:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 135
    iget-boolean v0, p0, Lkuo;->r:Z

    if-nez v0, :cond_7

    :goto_6
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lkuo;->A:Ljava/lang/Boolean;

    .line 138
    :cond_0
    invoke-virtual {p0}, Lkuo;->notifyDataSetChanged()V

    .line 139
    return-void

    :cond_1
    move v0, v2

    .line 117
    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 120
    goto :goto_1

    :cond_3
    move v0, v2

    .line 121
    goto :goto_2

    :cond_4
    move v0, v2

    .line 122
    goto :goto_3

    :cond_5
    move v0, v2

    .line 124
    goto :goto_4

    :cond_6
    move v0, v2

    .line 126
    goto :goto_5

    :cond_7
    move v1, v2

    .line 135
    goto :goto_6
.end method

.method public b(Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lkuo;->B:Ljava/lang/Boolean;

    .line 390
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 219
    iget-boolean v0, p0, Lkuo;->p:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 226
    iget-boolean v0, p0, Lkuo;->q:Z

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lkuo;->s:Z

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 240
    iget v0, p0, Lkuo;->l:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 244
    iget-boolean v0, p0, Lkuo;->t:Z

    return v0
.end method

.method public g()I
    .locals 2

    .prologue
    .line 252
    iget v0, p0, Lkuo;->l:I

    iget v1, p0, Lkuo;->n:I

    invoke-static {v0, v1}, Lkvu;->a(II)I

    move-result v0

    return v0
.end method

.method public g(I)I
    .locals 1

    .prologue
    .line 329
    packed-switch p1, :pswitch_data_0

    .line 336
    sget v0, Lkuo;->a:I

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    .line 331
    :pswitch_0
    sget v0, Lkuo;->a:I

    add-int/lit8 v0, v0, 0x0

    goto :goto_0

    .line 329
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 343
    sget v0, Lkuo;->a:I

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public h()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 280
    iget v1, p0, Lkuo;->l:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lkuo;->l:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 359
    invoke-super {p0}, Lfdj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkuo;->p()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 288
    iget v0, p0, Lkuo;->l:I

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lkuo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lkuo;->d:Ljava/lang/String;

    return-object v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lkuo;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Ldyq;
    .locals 4

    .prologue
    .line 316
    new-instance v1, Ldyq;

    iget v2, p0, Lkuo;->u:I

    iget-boolean v0, p0, Lkuo;->t:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-boolean v3, p0, Lkuo;->s:Z

    invoke-direct {v1, v2, v0, v3}, Ldyq;-><init>(IZZ)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Lldx;
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 351
    new-instance v1, Lldx;

    const/4 v2, -0x2

    invoke-direct {v1, v2}, Lldx;-><init>(I)V

    .line 353
    iget-object v2, p0, Lkuo;->j:Llcr;

    iget v2, v2, Llcr;->a:I

    if-lt v2, v0, :cond_0

    :goto_0
    iput v0, v1, Lldx;->a:I

    .line 354
    return-object v1

    .line 353
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lkuo;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 396
    iget v0, p0, Lkuo;->m:I

    return v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Lkuo;->n:I

    return v0
.end method
