.class public final Lmhi;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lodg;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6073
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6076
    iput-object v0, p0, Lmhi;->apiHeader:Llyq;

    .line 6079
    iput-object v0, p0, Lmhi;->a:Lodg;

    .line 6073
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6096
    const/4 v0, 0x0

    .line 6097
    iget-object v1, p0, Lmhi;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 6098
    const/4 v0, 0x1

    iget-object v1, p0, Lmhi;->apiHeader:Llyq;

    .line 6099
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6101
    :cond_0
    iget-object v1, p0, Lmhi;->a:Lodg;

    if-eqz v1, :cond_1

    .line 6102
    const/4 v1, 0x2

    iget-object v2, p0, Lmhi;->a:Lodg;

    .line 6103
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6105
    :cond_1
    iget-object v1, p0, Lmhi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6106
    iput v0, p0, Lmhi;->ai:I

    .line 6107
    return v0
.end method

.method public a(Loxn;)Lmhi;
    .locals 2

    .prologue
    .line 6115
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6116
    sparse-switch v0, :sswitch_data_0

    .line 6120
    iget-object v1, p0, Lmhi;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6121
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhi;->ah:Ljava/util/List;

    .line 6124
    :cond_1
    iget-object v1, p0, Lmhi;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6126
    :sswitch_0
    return-object p0

    .line 6131
    :sswitch_1
    iget-object v0, p0, Lmhi;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 6132
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmhi;->apiHeader:Llyq;

    .line 6134
    :cond_2
    iget-object v0, p0, Lmhi;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6138
    :sswitch_2
    iget-object v0, p0, Lmhi;->a:Lodg;

    if-nez v0, :cond_3

    .line 6139
    new-instance v0, Lodg;

    invoke-direct {v0}, Lodg;-><init>()V

    iput-object v0, p0, Lmhi;->a:Lodg;

    .line 6141
    :cond_3
    iget-object v0, p0, Lmhi;->a:Lodg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6116
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6084
    iget-object v0, p0, Lmhi;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 6085
    const/4 v0, 0x1

    iget-object v1, p0, Lmhi;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6087
    :cond_0
    iget-object v0, p0, Lmhi;->a:Lodg;

    if-eqz v0, :cond_1

    .line 6088
    const/4 v0, 0x2

    iget-object v1, p0, Lmhi;->a:Lodg;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6090
    :cond_1
    iget-object v0, p0, Lmhi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6092
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6069
    invoke-virtual {p0, p1}, Lmhi;->a(Loxn;)Lmhi;

    move-result-object v0

    return-object v0
.end method
