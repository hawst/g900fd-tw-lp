.class public final Lmkf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lndu;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20410
    invoke-direct {p0}, Loxq;-><init>()V

    .line 20413
    iput-object v0, p0, Lmkf;->apiHeader:Llyr;

    .line 20416
    iput-object v0, p0, Lmkf;->a:Lndu;

    .line 20410
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 20433
    const/4 v0, 0x0

    .line 20434
    iget-object v1, p0, Lmkf;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 20435
    const/4 v0, 0x1

    iget-object v1, p0, Lmkf;->apiHeader:Llyr;

    .line 20436
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 20438
    :cond_0
    iget-object v1, p0, Lmkf;->a:Lndu;

    if-eqz v1, :cond_1

    .line 20439
    const/4 v1, 0x2

    iget-object v2, p0, Lmkf;->a:Lndu;

    .line 20440
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20442
    :cond_1
    iget-object v1, p0, Lmkf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20443
    iput v0, p0, Lmkf;->ai:I

    .line 20444
    return v0
.end method

.method public a(Loxn;)Lmkf;
    .locals 2

    .prologue
    .line 20452
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 20453
    sparse-switch v0, :sswitch_data_0

    .line 20457
    iget-object v1, p0, Lmkf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 20458
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmkf;->ah:Ljava/util/List;

    .line 20461
    :cond_1
    iget-object v1, p0, Lmkf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 20463
    :sswitch_0
    return-object p0

    .line 20468
    :sswitch_1
    iget-object v0, p0, Lmkf;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 20469
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmkf;->apiHeader:Llyr;

    .line 20471
    :cond_2
    iget-object v0, p0, Lmkf;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20475
    :sswitch_2
    iget-object v0, p0, Lmkf;->a:Lndu;

    if-nez v0, :cond_3

    .line 20476
    new-instance v0, Lndu;

    invoke-direct {v0}, Lndu;-><init>()V

    iput-object v0, p0, Lmkf;->a:Lndu;

    .line 20478
    :cond_3
    iget-object v0, p0, Lmkf;->a:Lndu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 20453
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 20421
    iget-object v0, p0, Lmkf;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 20422
    const/4 v0, 0x1

    iget-object v1, p0, Lmkf;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20424
    :cond_0
    iget-object v0, p0, Lmkf;->a:Lndu;

    if-eqz v0, :cond_1

    .line 20425
    const/4 v0, 0x2

    iget-object v1, p0, Lmkf;->a:Lndu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 20427
    :cond_1
    iget-object v0, p0, Lmkf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 20429
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 20406
    invoke-virtual {p0, p1}, Lmkf;->a(Loxn;)Lmkf;

    move-result-object v0

    return-object v0
.end method
