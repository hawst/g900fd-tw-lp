.class public final Lovk;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Long;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24
    const/high16 v0, -0x80000000

    iput v0, p0, Lovk;->a:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 78
    const/4 v0, 0x0

    .line 79
    iget v1, p0, Lovk;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 80
    const/4 v0, 0x1

    iget v1, p0, Lovk;->a:I

    .line 81
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 83
    :cond_0
    iget-object v1, p0, Lovk;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 84
    const/4 v1, 0x2

    iget-object v2, p0, Lovk;->b:Ljava/lang/String;

    .line 85
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_1
    iget-object v1, p0, Lovk;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 88
    const/4 v1, 0x3

    iget-object v2, p0, Lovk;->c:Ljava/lang/Long;

    .line 89
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    :cond_2
    iget-object v1, p0, Lovk;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 92
    const/4 v1, 0x4

    iget-object v2, p0, Lovk;->e:Ljava/lang/String;

    .line 93
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    :cond_3
    iget-object v1, p0, Lovk;->f:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 96
    const/4 v1, 0x5

    iget-object v2, p0, Lovk;->f:Ljava/lang/Long;

    .line 97
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_4
    iget-object v1, p0, Lovk;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 100
    const/4 v1, 0x6

    iget-object v2, p0, Lovk;->d:Ljava/lang/String;

    .line 101
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_5
    iget-object v1, p0, Lovk;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 104
    const/4 v1, 0x7

    iget-object v2, p0, Lovk;->g:Ljava/lang/String;

    .line 105
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_6
    iget-object v1, p0, Lovk;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 108
    const/16 v1, 0x8

    iget-object v2, p0, Lovk;->h:Ljava/lang/String;

    .line 109
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_7
    iget-object v1, p0, Lovk;->i:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 112
    const/16 v1, 0x9

    iget-object v2, p0, Lovk;->i:Ljava/lang/String;

    .line 113
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_8
    iget-object v1, p0, Lovk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    iput v0, p0, Lovk;->ai:I

    .line 117
    return v0
.end method

.method public a(Loxn;)Lovk;
    .locals 2

    .prologue
    .line 125
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 126
    sparse-switch v0, :sswitch_data_0

    .line 130
    iget-object v1, p0, Lovk;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 131
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lovk;->ah:Ljava/util/List;

    .line 134
    :cond_1
    iget-object v1, p0, Lovk;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    :sswitch_0
    return-object p0

    .line 141
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 142
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-ne v0, v1, :cond_3

    .line 150
    :cond_2
    iput v0, p0, Lovk;->a:I

    goto :goto_0

    .line 152
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lovk;->a:I

    goto :goto_0

    .line 157
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lovk;->b:Ljava/lang/String;

    goto :goto_0

    .line 161
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lovk;->c:Ljava/lang/Long;

    goto :goto_0

    .line 165
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lovk;->e:Ljava/lang/String;

    goto :goto_0

    .line 169
    :sswitch_5
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lovk;->f:Ljava/lang/Long;

    goto :goto_0

    .line 173
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lovk;->d:Ljava/lang/String;

    goto :goto_0

    .line 177
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lovk;->g:Ljava/lang/String;

    goto :goto_0

    .line 181
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lovk;->h:Ljava/lang/String;

    goto :goto_0

    .line 185
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lovk;->i:Ljava/lang/String;

    goto :goto_0

    .line 126
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 45
    iget v0, p0, Lovk;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 46
    const/4 v0, 0x1

    iget v1, p0, Lovk;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 48
    :cond_0
    iget-object v0, p0, Lovk;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 49
    const/4 v0, 0x2

    iget-object v1, p0, Lovk;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 51
    :cond_1
    iget-object v0, p0, Lovk;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 52
    const/4 v0, 0x3

    iget-object v1, p0, Lovk;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 54
    :cond_2
    iget-object v0, p0, Lovk;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 55
    const/4 v0, 0x4

    iget-object v1, p0, Lovk;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 57
    :cond_3
    iget-object v0, p0, Lovk;->f:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 58
    const/4 v0, 0x5

    iget-object v1, p0, Lovk;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 60
    :cond_4
    iget-object v0, p0, Lovk;->d:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 61
    const/4 v0, 0x6

    iget-object v1, p0, Lovk;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 63
    :cond_5
    iget-object v0, p0, Lovk;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 64
    const/4 v0, 0x7

    iget-object v1, p0, Lovk;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 66
    :cond_6
    iget-object v0, p0, Lovk;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 67
    const/16 v0, 0x8

    iget-object v1, p0, Lovk;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 69
    :cond_7
    iget-object v0, p0, Lovk;->i:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 70
    const/16 v0, 0x9

    iget-object v1, p0, Lovk;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 72
    :cond_8
    iget-object v0, p0, Lovk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 74
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lovk;->a(Loxn;)Lovk;

    move-result-object v0

    return-object v0
.end method
