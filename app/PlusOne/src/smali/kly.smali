.class public Lkly;
.super Lkcu;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkcu",
        "<",
        "Lkmd;",
        ">;"
    }
.end annotation


# instance fields
.field private final e:Lhei;

.field private final f:Lieh;

.field private final g:Lloz;

.field private h:Z

.field private i:Z

.field private j:Lkmf;

.field private k:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 611
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "ShareQueue"

    invoke-direct {p0, v0, v1}, Lkcu;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 612
    sput-object p0, Lkcu;->a:Lkcu;

    .line 613
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lkly;->e:Lhei;

    .line 614
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    iput-object v0, p0, Lkly;->f:Lieh;

    .line 615
    new-instance v0, Lloz;

    const-string v1, "debug.plus.enable_request_queue"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lkly;->g:Lloz;

    .line 616
    new-instance v0, Lklz;

    invoke-direct {v0, p0}, Lklz;-><init>(Lkly;)V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.google.android.libraries.social.autobackup.upload_progress"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ldr;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 617
    invoke-virtual {p0}, Lkly;->c()V

    .line 618
    return-void
.end method

.method static synthetic a(FF)I
    .locals 1

    .prologue
    .line 79
    invoke-static {p0, p1}, Lkly;->b(FF)I

    move-result v0

    return v0
.end method

.method private a(ILmhi;Ljava/util/List;Lkey;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lmhi;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;",
            "Lkey;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 951
    new-instance v0, Lklm;

    iget-object v3, p0, Lkly;->b:Landroid/content/Context;

    invoke-direct {v0, v3, p1, p2, p4}, Lklm;-><init>(Landroid/content/Context;ILmhi;Lkey;)V

    .line 953
    invoke-virtual {v0}, Lklm;->l()V

    .line 954
    iget-object v3, p0, Lkly;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lklm;->e(Ljava/lang/String;)V

    .line 956
    invoke-virtual {v0}, Lklm;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmhj;

    iget-object v3, v0, Lmhj;->a:Loda;

    .line 957
    iget-object v0, v3, Loda;->a:Logi;

    iget-object v4, v0, Logi;->a:[Logr;

    move v0, v1

    .line 958
    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    .line 959
    aget-object v5, v4, v0

    .line 962
    iget-object v6, p0, Lkly;->b:Landroid/content/Context;

    invoke-direct {p0, p3}, Lkly;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    iget-object v5, v5, Logr;->r:Ljava/lang/String;

    invoke-static {v6, p1, v7, v5, v1}, Lhqd;->a(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Z)V

    .line 958
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 969
    :cond_0
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    const-class v4, Lkmv;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkmv;

    .line 970
    iget-object v4, v3, Loda;->b:Loge;

    if-eqz v4, :cond_1

    .line 971
    iget-object v4, v3, Loda;->b:Loge;

    invoke-virtual {v0, p1, v4}, Lkmv;->a(ILoge;)V

    .line 974
    :cond_1
    iget-object v0, v3, Loda;->d:Lodb;

    if-eqz v0, :cond_3

    iget-object v0, v3, Loda;->d:Lodb;

    iget-object v0, v0, Lodb;->a:Ljava/lang/Boolean;

    .line 975
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 977
    :goto_1
    if-eqz v0, :cond_2

    move v1, v2

    :cond_2
    return v1

    :cond_3
    move v0, v1

    .line 975
    goto :goto_1
.end method

.method static synthetic a(Lkly;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    return-object v0
.end method

.method private a(IIIZ)Ljava/lang/String;
    .locals 6

    .prologue
    const v5, 0x7f11000d

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1106
    iget-boolean v0, p0, Lkly;->h:Z

    if-nez v0, :cond_1

    .line 1107
    if-nez p4, :cond_0

    if-ne p1, v3, :cond_0

    .line 1108
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    const v1, 0x7f0a02d3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1126
    :goto_0
    return-object v0

    .line 1110
    :cond_0
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11000e

    new-array v2, v3, [Ljava/lang/Object;

    .line 1111
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 1110
    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1113
    :cond_1
    if-ne p1, v3, :cond_5

    .line 1114
    if-lez p2, :cond_2

    if-lez p3, :cond_2

    .line 1115
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    const v1, 0x7f0a02d2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1116
    :cond_2
    if-lez p2, :cond_3

    .line 1117
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11000c

    new-array v2, v3, [Ljava/lang/Object;

    .line 1118
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 1117
    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1119
    :cond_3
    if-lez p3, :cond_4

    .line 1120
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f11000b

    new-array v2, v3, [Ljava/lang/Object;

    .line 1121
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    .line 1120
    invoke-virtual {v0, v1, p3, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1123
    :cond_4
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v5, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1126
    :cond_5
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    .line 1127
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 1126
    invoke-virtual {v0, v5, p1, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 985
    if-nez p1, :cond_0

    .line 986
    const/4 v0, 0x0

    .line 997
    :goto_0
    return-object v0

    .line 989
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 990
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 991
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 992
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    .line 993
    invoke-virtual {v0}, Lizu;->i()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 994
    invoke-virtual {v0}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 991
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 997
    goto :goto_0
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)Lkmd;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 663
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 666
    :try_start_0
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    :try_start_1
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v1

    .line 668
    const/4 v4, 0x4

    if-eq v1, v4, :cond_0

    .line 669
    new-instance v1, Ljava/io/IOException;

    const-string v4, "Incompatible share queue file format."

    invoke-direct {v1, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 686
    :catch_0
    move-exception v1

    .line 687
    :goto_0
    :try_start_2
    iget-object v4, p0, Lkly;->c:Ljava/lang/String;

    const-string v5, "Error reading item file. Removing item file and skipping item."

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 688
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 689
    invoke-direct {p0, v2}, Lkly;->a(Ljava/io/Closeable;)V

    .line 691
    :goto_1
    return-object v0

    .line 672
    :cond_0
    :try_start_3
    iget-object v1, p0, Lkly;->b:Landroid/content/Context;

    invoke-static {v1, v2}, Lkmd;->a(Landroid/content/Context;Ljava/io/DataInputStream;)Lkmd;

    move-result-object v1

    .line 673
    if-nez v1, :cond_1

    .line 674
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 675
    invoke-direct {p0, v2}, Lkly;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 677
    :cond_1
    :try_start_4
    invoke-virtual {v1}, Lkmd;->a()I

    move-result v4

    invoke-direct {p0, v4}, Lkly;->d(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 678
    iget-object v1, p0, Lkly;->c:Ljava/lang/String;

    const-string v4, "Account associated with item not logged in. Removing item file and skipping item."

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 681
    invoke-direct {p0, v2}, Lkly;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 683
    :cond_2
    :try_start_5
    invoke-static {v1, p2}, Lkmd;->a(Lkmd;Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 691
    invoke-direct {p0, v2}, Lkly;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    goto :goto_1

    .line 689
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_2
    invoke-direct {p0, v2}, Lkly;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 686
    :catch_1
    move-exception v1

    move-object v2, v0

    goto :goto_0
.end method

.method private a(Landroid/app/Notification;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 1259
    invoke-direct {p0}, Lkly;->j()V

    .line 1260
    iget-object v1, p0, Lkly;->k:Landroid/app/NotificationManager;

    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0, p3, p1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1261
    return-void

    .line 1260
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/io/Closeable;)V
    .locals 3

    .prologue
    .line 798
    if-nez p1, :cond_0

    .line 806
    :goto_0
    return-void

    .line 802
    :cond_0
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 803
    :catch_0
    move-exception v0

    .line 804
    iget-object v1, p0, Lkly;->c:Ljava/lang/String;

    const-string v2, "Error while attempting to close stream."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic a(Lkly;IZ)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lkly;->c(IZ)V

    return-void
.end method

.method private static b(FF)I
    .locals 2

    .prologue
    .line 1247
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    div-float v0, p0, p1

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(IZ)V
    .locals 25

    .prologue
    .line 1034
    invoke-static {}, Llsx;->c()V

    .line 1035
    move-object/from16 v0, p0

    iget-object v7, v0, Lkly;->d:Ljava/lang/Object;

    monitor-enter v7

    .line 1036
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lkly;->f()I

    move-result v8

    move v6, v2

    :goto_0
    if-ge v6, v8, :cond_4

    .line 1037
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lkly;->b(I)Lkcw;

    move-result-object v2

    check-cast v2, Lkmd;

    .line 1038
    invoke-static {v2}, Lkmd;->a(Lkmd;)V

    .line 1039
    invoke-virtual {v2}, Lkmd;->a()I

    move-result v3

    move/from16 v0, p1

    if-ne v3, v0, :cond_0

    invoke-static {v2}, Lkmd;->p(Lkmd;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1040
    invoke-static {v2}, Lkmd;->b(Lkmd;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 1046
    :cond_0
    :goto_1
    const/4 v3, 0x1

    invoke-static {v2}, Lkmd;->h(Lkmd;)I

    move-result v4

    invoke-static {v2}, Lkmd;->i(Lkmd;)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-static {v2}, Lkmd;->j(Lkmd;)I

    move-result v5

    invoke-static {v2}, Lkmd;->k(Lkmd;)I

    move-result v9

    sub-int/2addr v5, v9

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lkly;->a(IIIZ)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, Lkmd;->p(Lkmd;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lkly;->i:Z

    if-nez v3, :cond_3

    :cond_1
    const/4 v3, 0x1

    :goto_2
    invoke-static {v2, v4, v3}, Lkmd;->a(Lkmd;Ljava/lang/String;Z)Z

    .line 1036
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_0

    .line 1045
    :pswitch_0
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lkmd;->a(Lkmd;Z)Z

    invoke-static {v2}, Lkmd;->b(Lkmd;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    invoke-static {v2}, Lkmd;->q(Lkmd;)J

    move-result-wide v4

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lkly;->j:Lkmf;

    move-object/from16 v0, p0

    iget-object v9, v0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v2}, Lkmd;->a()I

    move-result v10

    invoke-interface {v3, v9, v10, v4, v5}, Lkmf;->a(Landroid/content/Context;IJ)V

    goto :goto_1

    .line 1052
    :catchall_0
    move-exception v2

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1045
    :cond_2
    const-wide/16 v4, 0x0

    goto :goto_3

    .line 1046
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 1052
    :cond_4
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1053
    invoke-virtual/range {p0 .. p0}, Lkly;->e()V

    .line 1054
    const/4 v13, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lkly;->d:Ljava/lang/Object;

    move-object/from16 v17, v0

    monitor-enter v17

    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lkly;->f()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lkly;->g()I

    move-result v5

    const/4 v2, 0x0

    move/from16 v16, v12

    move v12, v6

    move v6, v9

    move v9, v11

    move v11, v4

    move-object v4, v13

    move v13, v7

    move v7, v2

    move/from16 v24, v3

    move v3, v10

    move/from16 v10, v24

    :goto_4
    move/from16 v0, v18

    if-ge v7, v0, :cond_7

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lkly;->b(I)Lkcw;

    move-result-object v2

    check-cast v2, Lkmd;

    int-to-long v14, v6

    invoke-static {v2}, Lkmd;->s(Lkmd;)J

    move-result-wide v20

    add-long v14, v14, v20

    long-to-int v15, v14

    int-to-long v0, v8

    move-wide/from16 v20, v0

    invoke-static {v2}, Lkmd;->t(Lkmd;)J

    move-result-wide v22

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v14, v0

    invoke-static {v2}, Lkmd;->h(Lkmd;)I

    move-result v6

    add-int/2addr v13, v6

    invoke-static {v2}, Lkmd;->i(Lkmd;)I

    move-result v6

    add-int/2addr v12, v6

    invoke-static {v2}, Lkmd;->j(Lkmd;)I

    move-result v6

    add-int/2addr v11, v6

    invoke-static {v2}, Lkmd;->k(Lkmd;)I

    move-result v6

    add-int v8, v10, v6

    invoke-static {v2}, Lkmd;->b(Lkmd;)I

    move-result v6

    const/4 v10, 0x1

    if-ne v6, v10, :cond_12

    if-eqz v9, :cond_5

    move-object/from16 v0, p0

    iget-object v6, v0, Lkly;->c:Ljava/lang/String;

    const-string v9, "More than one item in queue is processing. This should never happen."

    invoke-static {v6, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const/4 v6, 0x1

    :goto_5
    invoke-static {v2}, Lkmd;->u(Lkmd;)Z

    move-result v9

    and-int v9, v9, v16

    invoke-static {v2}, Lkmd;->b(Lkmd;)I

    move-result v10

    const/16 v16, 0x3

    move/from16 v0, v16

    if-ne v10, v0, :cond_6

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lkly;->d(Lkmd;)V

    move v2, v3

    move-object v3, v4

    :goto_6
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v10, v8

    move/from16 v16, v9

    move v9, v6

    move v8, v14

    move-object v4, v3

    move v3, v2

    move v6, v15

    goto :goto_4

    :cond_6
    const/4 v3, 0x1

    if-nez v4, :cond_11

    invoke-static {v2}, Lkmd;->v(Lkmd;)Z

    move-result v10

    if-eqz v10, :cond_11

    move/from16 v24, v3

    move-object v3, v2

    move/from16 v2, v24

    goto :goto_6

    :cond_7
    monitor-exit v17
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lkly;->h:Z

    if-nez v7, :cond_a

    const/4 v5, 0x0

    move v7, v5

    :goto_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lkly;->c:Ljava/lang/String;

    const/4 v15, 0x4

    invoke-static {v14, v15}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lkly;->c:Ljava/lang/String;

    int-to-float v14, v7

    int-to-float v15, v5

    invoke-static {v14, v15}, Lkly;->b(FF)I

    move-result v14

    new-instance v15, Ljava/lang/StringBuilder;

    const/16 v17, 0xb1

    move/from16 v0, v17

    invoke-direct {v15, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v17, "ShareQueue: "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, " posts; "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v15, "/"

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " bytes; "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " photos; "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " videos; all media uploaded: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "; progress: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " ("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "%)"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    if-eqz v3, :cond_d

    if-eqz v16, :cond_9

    if-nez v9, :cond_d

    :cond_9
    const/4 v3, 0x1

    :goto_8
    if-eqz v3, :cond_f

    new-instance v3, Lbs;

    move-object/from16 v0, p0

    iget-object v6, v0, Lkly;->b:Landroid/content/Context;

    invoke-direct {v3, v6}, Lbs;-><init>(Landroid/content/Context;)V

    sub-int v6, v13, v12

    sub-int v8, v11, v10

    const/4 v9, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1, v6, v8, v9}, Lkly;->a(IIIZ)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    move-result-object v3

    const v6, 0x7f02053d

    invoke-virtual {v3, v6}, Lbs;->a(I)Lbs;

    move-result-object v6

    if-eqz v4, :cond_e

    invoke-static {v4}, Lkmd;->r(Lkmd;)Landroid/graphics/Bitmap;

    move-result-object v3

    :goto_9
    invoke-virtual {v6, v3}, Lbs;->a(Landroid/graphics/Bitmap;)Lbs;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lbs;->a(Z)Lbs;

    move-result-object v3

    invoke-virtual {v3, v5, v7, v2}, Lbs;->a(IIZ)Lbs;

    move-result-object v2

    invoke-virtual {v2}, Lbs;->c()Landroid/app/Notification;

    move-result-object v2

    const-string v3, ":notifications:share_queue"

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lkly;->a(Landroid/app/Notification;Ljava/lang/String;I)V

    .line 1055
    :goto_a
    return-void

    .line 1054
    :catchall_1
    move-exception v2

    :try_start_3
    monitor-exit v17
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    :cond_a
    const/4 v7, 0x1

    if-ne v5, v7, :cond_c

    if-eqz v16, :cond_b

    const/4 v5, 0x0

    const/4 v2, 0x1

    move v7, v5

    goto/16 :goto_7

    :cond_b
    move v5, v6

    move v7, v8

    goto/16 :goto_7

    :cond_c
    sub-int v7, v5, v18

    goto/16 :goto_7

    :cond_d
    const/4 v3, 0x0

    goto :goto_8

    :cond_e
    const/4 v3, 0x0

    goto :goto_9

    :cond_f
    const-string v2, ":notifications:share_queue"

    invoke-direct/range {p0 .. p0}, Lkly;->j()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lkly;->k:Landroid/app/NotificationManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_10

    invoke-virtual {v4, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_b
    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    goto :goto_a

    :cond_10
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_b

    :cond_11
    move v2, v3

    move-object v3, v4

    goto/16 :goto_6

    :cond_12
    move v6, v9

    goto/16 :goto_5

    .line 1040
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private d(Lkmd;)V
    .locals 3

    .prologue
    .line 1133
    new-instance v0, Lbs;

    iget-object v1, p0, Lkly;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lbs;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lkly;->b:Landroid/content/Context;

    const v2, 0x7f0a02d4

    .line 1134
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    move-result-object v0

    const v1, 0x7f0203e4

    .line 1135
    invoke-virtual {v0, v1}, Lbs;->a(I)Lbs;

    move-result-object v0

    .line 1136
    invoke-static {p1}, Lkmd;->r(Lkmd;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbs;->a(Landroid/graphics/Bitmap;)Lbs;

    move-result-object v0

    const/4 v1, 0x1

    .line 1137
    invoke-virtual {v0, v1}, Lbs;->b(Z)Lbs;

    move-result-object v0

    .line 1138
    invoke-virtual {v0}, Lbs;->c()Landroid/app/Notification;

    move-result-object v0

    .line 1139
    const-string v1, ":notifications:share_queue_error"

    invoke-static {p1}, Lkmd;->l(Lkmd;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lkly;->a(Landroid/app/Notification;Ljava/lang/String;I)V

    .line 1140
    return-void
.end method

.method private d(I)Z
    .locals 1

    .prologue
    .line 742
    iget-object v0, p0, Lkly;->e:Lhei;

    invoke-interface {v0, p1}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkly;->e:Lhei;

    .line 743
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1275
    iget-object v0, p0, Lkly;->k:Landroid/app/NotificationManager;

    if-nez v0, :cond_0

    .line 1276
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    const-string v1, "notification"

    .line 1277
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lkly;->k:Landroid/app/NotificationManager;

    .line 1279
    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILodg;Ljava/lang/String;Ljava/util/List;Lkey;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lodg;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lizu;",
            ">;",
            "Lkey;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x0

    .line 850
    invoke-static {}, Llsx;->c()V

    .line 851
    new-instance v3, Lmhi;

    invoke-direct {v3}, Lmhi;-><init>()V

    .line 852
    iput-object p2, v3, Lmhi;->a:Lodg;

    .line 853
    if-eqz p4, :cond_1

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    move v1, v7

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizu;

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v4

    sget-object v6, Ljac;->b:Ljac;

    if-ne v4, v6, :cond_0

    invoke-virtual {v0}, Lizu;->j()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0, p1, v0}, Lkly;->b(IZ)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 854
    invoke-virtual {p0}, Lkly;->a()V

    .line 855
    new-instance v0, Lkmd;

    iget-object v1, p0, Lkly;->b:Landroid/content/Context;

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    if-nez p3, :cond_3

    :goto_3
    move v2, p1

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lkmd;-><init>(Landroid/content/Context;ILmhi;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v0, p5}, Lkmd;->a(Lkmd;Lkey;)Lkey;

    invoke-virtual {p0, v0}, Lkly;->b(Lkcw;)V

    .line 856
    const/4 v0, 0x2

    .line 864
    :goto_4
    return v0

    .line 853
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v5

    goto :goto_1

    :cond_2
    move v0, v7

    goto :goto_2

    .line 855
    :cond_3
    const/16 v2, 0xa

    invoke-virtual {p3, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-ltz v2, :cond_4

    invoke-virtual {p3, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    goto :goto_3

    :cond_4
    move-object v5, p3

    goto :goto_3

    .line 858
    :cond_5
    invoke-direct {p0, p1, v3, p4, p5}, Lkly;->a(ILmhi;Ljava/util/List;Lkey;)I

    move-result v8

    .line 861
    new-instance v0, Lkmd;

    iget-object v1, p0, Lkly;->b:Landroid/content/Context;

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    move v2, p1

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lkmd;-><init>(Landroid/content/Context;ILmhi;Ljava/util/Date;Ljava/lang/String;Ljava/util/List;)V

    .line 863
    invoke-virtual {p0, v0, v7}, Lkly;->a(Lkmd;I)V

    move v0, v8

    .line 864
    goto :goto_4
.end method

.method protected bridge synthetic a(Lkcw;)I
    .locals 1

    .prologue
    .line 79
    check-cast p1, Lkmd;

    invoke-virtual {p0, p1}, Lkly;->a(Lkmd;)I

    move-result v0

    return v0
.end method

.method protected a(Lkmd;)I
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 697
    invoke-static {p1}, Lkmd;->a(Lkmd;)V

    .line 698
    invoke-static {p1}, Lkmd;->b(Lkmd;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 730
    new-instance v0, Ljava/lang/RuntimeException;

    .line 731
    invoke-static {p1}, Lkmd;->b(Lkmd;)I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid upload status associated with item: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 704
    :pswitch_0
    :try_start_0
    invoke-virtual {p1}, Lkmd;->a()I

    move-result v0

    invoke-static {p1}, Lkmd;->n(Lkmd;)Lmhi;

    move-result-object v4

    invoke-static {p1}, Lkmd;->e(Lkmd;)Ljava/util/List;

    move-result-object v5

    invoke-static {p1}, Lkmd;->o(Lkmd;)Lkey;

    move-result-object v6

    invoke-direct {p0, v0, v4, v5, v6}, Lkly;->a(ILmhi;Ljava/util/List;Lkey;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 728
    :goto_0
    :pswitch_1
    return v0

    .line 705
    :catch_0
    move-exception v0

    .line 706
    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Lkgf;

    if-eqz v1, :cond_0

    .line 708
    iget-object v1, p0, Lkly;->c:Ljava/lang/String;

    const-string v3, "Server error encountered when sending post."

    invoke-virtual {v0}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 709
    invoke-direct {p0, p1}, Lkly;->d(Lkmd;)V

    move v0, v2

    .line 710
    goto :goto_0

    .line 713
    :cond_0
    invoke-static {p1}, Lkmd;->c(Lkmd;)I

    move v0, v3

    .line 714
    goto :goto_0

    :pswitch_2
    move v0, v2

    .line 720
    goto :goto_0

    .line 723
    :pswitch_3
    :try_start_1
    invoke-static {p1}, Lkmd;->d(Lkmd;)V

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lkmd;->a(Lkmd;Z)Z

    invoke-virtual {p0, p1}, Lkly;->b(Lkmd;)V

    iget-object v1, p0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lkmd;->a()I

    move-result v2

    invoke-static {p1}, Lkmd;->e(Lkmd;)Ljava/util/List;

    move-result-object v4

    invoke-direct {p0, v4}, Lkly;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    invoke-static {p1}, Lkmd;->f(Lkmd;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v1, v2, v4, v5, v6}, Lhqd;->a(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 725
    :catch_1
    move-exception v0

    invoke-static {p1}, Lkmd;->c(Lkmd;)I

    move v0, v3

    .line 726
    goto :goto_0

    .line 698
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(IZ)V
    .locals 0

    .prologue
    .line 1002
    invoke-direct {p0, p1, p2}, Lkly;->c(IZ)V

    .line 1003
    return-void
.end method

.method protected bridge synthetic a(Lkcw;I)V
    .locals 0

    .prologue
    .line 79
    check-cast p1, Lkmd;

    invoke-virtual {p0, p1, p2}, Lkly;->a(Lkmd;I)V

    return-void
.end method

.method protected bridge synthetic a(Lkcw;Z)V
    .locals 0

    .prologue
    .line 79
    check-cast p1, Lkmd;

    invoke-virtual {p0, p1, p2}, Lkly;->a(Lkmd;Z)V

    return-void
.end method

.method protected a(Lkmd;I)V
    .locals 8

    .prologue
    .line 762
    packed-switch p2, :pswitch_data_0

    .line 769
    :goto_0
    :pswitch_0
    return-void

    .line 766
    :pswitch_1
    iget-object v0, p0, Lkly;->e:Lhei;

    invoke-virtual {p1}, Lkmd;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 767
    new-instance v0, Lkol;

    invoke-static {p1}, Lkmd;->g(Lkmd;)I

    move-result v3

    invoke-static {p1}, Lkmd;->h(Lkmd;)I

    move-result v4

    .line 768
    invoke-static {p1}, Lkmd;->i(Lkmd;)I

    move-result v5

    invoke-static {p1}, Lkmd;->j(Lkmd;)I

    move-result v6

    invoke-static {p1}, Lkmd;->k(Lkmd;)I

    move-result v7

    move v2, p2

    invoke-direct/range {v0 .. v7}, Lkol;-><init>(Ljava/lang/String;IIIIII)V

    iget-object v1, p0, Lkly;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lkol;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 762
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Lkmd;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 817
    invoke-virtual {p0}, Lkly;->h()Z

    move-result v1

    if-nez v1, :cond_1

    .line 834
    :cond_0
    :goto_0
    return-void

    .line 820
    :cond_1
    iput-boolean v0, p0, Lkly;->h:Z

    .line 821
    if-eqz p2, :cond_0

    .line 823
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {p1}, Lkmd;->m(Lkmd;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xea60

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    const/4 v0, 0x1

    .line 824
    :cond_2
    if-eqz v0, :cond_3

    const v0, 0x7f11000f

    .line 826
    :goto_1
    iget-object v1, p0, Lkly;->b:Landroid/content/Context;

    .line 827
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lkly;->g()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    .line 828
    new-instance v1, Lkmb;

    invoke-direct {v1, p0, v0}, Lkmb;-><init>(Lkly;Ljava/lang/String;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 824
    :cond_3
    const v0, 0x7f110010

    goto :goto_1
.end method

.method public a(Lkmf;)V
    .locals 0

    .prologue
    .line 1283
    iput-object p1, p0, Lkly;->j:Lkmf;

    .line 1284
    return-void
.end method

.method protected a(I)Z
    .locals 1

    .prologue
    .line 737
    invoke-direct {p0, p1}, Lkly;->d(I)Z

    move-result v0

    return v0
.end method

.method protected b()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 647
    iget-object v1, p0, Lkly;->b:Landroid/content/Context;

    const-string v2, "queue"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 648
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 649
    if-nez v2, :cond_1

    .line 659
    :cond_0
    return-void

    .line 652
    :cond_1
    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 653
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 654
    aget-object v3, v2, v0

    invoke-direct {p0, v1, v3}, Lkly;->a(Ljava/io/File;Ljava/lang/String;)Lkmd;

    move-result-object v3

    .line 655
    if-eqz v3, :cond_2

    .line 656
    invoke-virtual {p0, v3}, Lkly;->b(Lkcw;)V

    .line 653
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public b(ILkfp;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1014
    invoke-static {}, Llsx;->c()V

    .line 1015
    iget-object v0, p0, Lkly;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1016
    iget-object v0, p0, Lkly;->c:Ljava/lang/String;

    .line 1018
    :cond_0
    invoke-virtual {p0}, Lkly;->a()V

    .line 1020
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkly;->i:Z

    iput-boolean v0, p0, Lkly;->h:Z

    .line 1021
    invoke-direct {p0, p1, v1}, Lkly;->c(IZ)V

    .line 1022
    invoke-virtual {p0, p1, p2}, Lkly;->a(ILkfp;)V

    .line 1023
    iput-boolean v1, p0, Lkly;->i:Z

    .line 1024
    iget-object v0, p0, Lkly;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1025
    iget-object v0, p0, Lkly;->c:Ljava/lang/String;

    .line 1027
    :cond_1
    return-void
.end method

.method protected b(Lkmd;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 776
    invoke-static {p1}, Lkmd;->l(Lkmd;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 779
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%016X"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkmd;->a(Lkmd;Ljava/lang/String;)Ljava/lang/String;

    .line 782
    :cond_0
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    const-string v1, "queue"

    invoke-virtual {v0, v1, v6}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 783
    new-instance v3, Ljava/io/File;

    invoke-static {p1}, Lkmd;->l(Lkmd;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 784
    const/4 v2, 0x0

    .line 786
    :try_start_0
    new-instance v1, Ljava/io/DataOutputStream;

    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 787
    const/4 v0, 0x4

    :try_start_1
    invoke-virtual {v1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 788
    invoke-static {p1, v1}, Lkmd;->a(Lkmd;Ljava/io/DataOutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 792
    invoke-direct {p0, v1}, Lkly;->a(Ljava/io/Closeable;)V

    .line 793
    :goto_0
    return-void

    .line 789
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 790
    :goto_1
    :try_start_2
    iget-object v2, p0, Lkly;->c:Ljava/lang/String;

    const-string v3, "Error writing item to file."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 792
    invoke-direct {p0, v1}, Lkly;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-direct {p0, v1}, Lkly;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 789
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public b(IZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 873
    invoke-virtual {p0, p1}, Lkly;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 884
    :cond_0
    :goto_0
    return v0

    .line 878
    :cond_1
    iget-object v1, p0, Lkly;->e:Lhei;

    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 879
    const-string v2, "is_managed_account"

    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 883
    iget-object v1, p0, Lkly;->f:Lieh;

    sget-object v2, Lkmx;->b:Lief;

    invoke-interface {v1, v2, p1}, Lieh;->b(Lief;I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lkly;->g:Lloz;

    .line 884
    if-eqz p2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected synthetic c(Lkcw;)V
    .locals 0

    .prologue
    .line 79
    check-cast p1, Lkmd;

    invoke-virtual {p0, p1}, Lkly;->b(Lkmd;)V

    return-void
.end method

.method protected c(Lkmd;)V
    .locals 3

    .prologue
    .line 810
    iget-object v0, p0, Lkly;->b:Landroid/content/Context;

    const-string v1, "queue"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 811
    new-instance v1, Ljava/io/File;

    invoke-static {p1}, Lkmd;->l(Lkmd;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 812
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 813
    return-void
.end method

.method protected synthetic d(Lkcw;)V
    .locals 0

    .prologue
    .line 79
    check-cast p1, Lkmd;

    invoke-virtual {p0, p1}, Lkly;->c(Lkmd;)V

    return-void
.end method

.method public i()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1086
    iget-object v3, p0, Lkly;->d:Ljava/lang/Object;

    monitor-enter v3

    .line 1087
    :try_start_0
    invoke-virtual {p0}, Lkly;->f()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    .line 1088
    invoke-virtual {p0, v2}, Lkly;->b(I)Lkcw;

    move-result-object v0

    check-cast v0, Lkmd;

    invoke-static {v0}, Lkmd;->j(Lkmd;)I

    move-result v0

    if-lez v0, :cond_0

    .line 1089
    const/4 v0, 0x1

    monitor-exit v3

    .line 1093
    :goto_1
    return v0

    .line 1087
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1092
    :cond_1
    monitor-exit v3

    move v0, v1

    .line 1093
    goto :goto_1

    .line 1092
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
