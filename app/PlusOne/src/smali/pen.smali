.class public final Lpen;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpen;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:Lpek;

.field public e:Lpem;

.field public f:Lpep;

.field public g:Lpel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    new-array v0, v0, [Lpen;

    sput-object v0, Lpen;->a:[Lpen;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 140
    invoke-direct {p0}, Loxq;-><init>()V

    .line 143
    const/high16 v0, -0x80000000

    iput v0, p0, Lpen;->b:I

    .line 148
    iput-object v1, p0, Lpen;->d:Lpek;

    .line 151
    iput-object v1, p0, Lpen;->e:Lpem;

    .line 154
    iput-object v1, p0, Lpen;->f:Lpep;

    .line 157
    iput-object v1, p0, Lpen;->g:Lpel;

    .line 140
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 186
    const/4 v0, 0x0

    .line 187
    iget v1, p0, Lpen;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 188
    const/4 v0, 0x1

    iget v1, p0, Lpen;->b:I

    .line 189
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 191
    :cond_0
    iget-object v1, p0, Lpen;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 192
    const/4 v1, 0x2

    iget-object v2, p0, Lpen;->c:Ljava/lang/String;

    .line 193
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 195
    :cond_1
    iget-object v1, p0, Lpen;->d:Lpek;

    if-eqz v1, :cond_2

    .line 196
    const/4 v1, 0x3

    iget-object v2, p0, Lpen;->d:Lpek;

    .line 197
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    :cond_2
    iget-object v1, p0, Lpen;->e:Lpem;

    if-eqz v1, :cond_3

    .line 200
    const/4 v1, 0x4

    iget-object v2, p0, Lpen;->e:Lpem;

    .line 201
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 203
    :cond_3
    iget-object v1, p0, Lpen;->f:Lpep;

    if-eqz v1, :cond_4

    .line 204
    const/4 v1, 0x5

    iget-object v2, p0, Lpen;->f:Lpep;

    .line 205
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_4
    iget-object v1, p0, Lpen;->g:Lpel;

    if-eqz v1, :cond_5

    .line 208
    const/4 v1, 0x6

    iget-object v2, p0, Lpen;->g:Lpel;

    .line 209
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_5
    iget-object v1, p0, Lpen;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    iput v0, p0, Lpen;->ai:I

    .line 213
    return v0
.end method

.method public a(Loxn;)Lpen;
    .locals 2

    .prologue
    .line 221
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 222
    sparse-switch v0, :sswitch_data_0

    .line 226
    iget-object v1, p0, Lpen;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 227
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpen;->ah:Ljava/util/List;

    .line 230
    :cond_1
    iget-object v1, p0, Lpen;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    :sswitch_0
    return-object p0

    .line 237
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 238
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 243
    :cond_2
    iput v0, p0, Lpen;->b:I

    goto :goto_0

    .line 245
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpen;->b:I

    goto :goto_0

    .line 250
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpen;->c:Ljava/lang/String;

    goto :goto_0

    .line 254
    :sswitch_3
    iget-object v0, p0, Lpen;->d:Lpek;

    if-nez v0, :cond_4

    .line 255
    new-instance v0, Lpek;

    invoke-direct {v0}, Lpek;-><init>()V

    iput-object v0, p0, Lpen;->d:Lpek;

    .line 257
    :cond_4
    iget-object v0, p0, Lpen;->d:Lpek;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 261
    :sswitch_4
    iget-object v0, p0, Lpen;->e:Lpem;

    if-nez v0, :cond_5

    .line 262
    new-instance v0, Lpem;

    invoke-direct {v0}, Lpem;-><init>()V

    iput-object v0, p0, Lpen;->e:Lpem;

    .line 264
    :cond_5
    iget-object v0, p0, Lpen;->e:Lpem;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 268
    :sswitch_5
    iget-object v0, p0, Lpen;->f:Lpep;

    if-nez v0, :cond_6

    .line 269
    new-instance v0, Lpep;

    invoke-direct {v0}, Lpep;-><init>()V

    iput-object v0, p0, Lpen;->f:Lpep;

    .line 271
    :cond_6
    iget-object v0, p0, Lpen;->f:Lpep;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 275
    :sswitch_6
    iget-object v0, p0, Lpen;->g:Lpel;

    if-nez v0, :cond_7

    .line 276
    new-instance v0, Lpel;

    invoke-direct {v0}, Lpel;-><init>()V

    iput-object v0, p0, Lpen;->g:Lpel;

    .line 278
    :cond_7
    iget-object v0, p0, Lpen;->g:Lpel;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 222
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 162
    iget v0, p0, Lpen;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 163
    const/4 v0, 0x1

    iget v1, p0, Lpen;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 165
    :cond_0
    iget-object v0, p0, Lpen;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 166
    const/4 v0, 0x2

    iget-object v1, p0, Lpen;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 168
    :cond_1
    iget-object v0, p0, Lpen;->d:Lpek;

    if-eqz v0, :cond_2

    .line 169
    const/4 v0, 0x3

    iget-object v1, p0, Lpen;->d:Lpek;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 171
    :cond_2
    iget-object v0, p0, Lpen;->e:Lpem;

    if-eqz v0, :cond_3

    .line 172
    const/4 v0, 0x4

    iget-object v1, p0, Lpen;->e:Lpem;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 174
    :cond_3
    iget-object v0, p0, Lpen;->f:Lpep;

    if-eqz v0, :cond_4

    .line 175
    const/4 v0, 0x5

    iget-object v1, p0, Lpen;->f:Lpep;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 177
    :cond_4
    iget-object v0, p0, Lpen;->g:Lpel;

    if-eqz v0, :cond_5

    .line 178
    const/4 v0, 0x6

    iget-object v1, p0, Lpen;->g:Lpel;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 180
    :cond_5
    iget-object v0, p0, Lpen;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 182
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lpen;->a(Loxn;)Lpen;

    move-result-object v0

    return-object v0
.end method
