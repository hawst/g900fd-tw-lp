.class public final Lmmq;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmok;

.field public b:Lodo;

.field public c:Lmoo;

.field public d:Lmmi;

.field public e:[Lmma;

.field public f:Lmoi;

.field public g:Lmlz;

.field public h:[Lmmt;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Lnzx;

.field private m:I

.field private n:Lmoj;

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 7620
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7628
    iput-object v1, p0, Lmmq;->a:Lmok;

    .line 7631
    iput v2, p0, Lmmq;->m:I

    .line 7634
    iput-object v1, p0, Lmmq;->b:Lodo;

    .line 7637
    iput-object v1, p0, Lmmq;->c:Lmoo;

    .line 7640
    iput-object v1, p0, Lmmq;->d:Lmmi;

    .line 7643
    sget-object v0, Lmma;->a:[Lmma;

    iput-object v0, p0, Lmmq;->e:[Lmma;

    .line 7646
    iput-object v1, p0, Lmmq;->f:Lmoi;

    .line 7649
    iput-object v1, p0, Lmmq;->g:Lmlz;

    .line 7652
    sget-object v0, Lmmt;->a:[Lmmt;

    iput-object v0, p0, Lmmq;->h:[Lmmt;

    .line 7655
    iput-object v1, p0, Lmmq;->n:Lmoj;

    .line 7662
    iput v2, p0, Lmmq;->o:I

    .line 7667
    iput-object v1, p0, Lmmq;->l:Lnzx;

    .line 7620
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v1, 0x0

    .line 7731
    .line 7732
    iget-object v0, p0, Lmmq;->a:Lmok;

    if-eqz v0, :cond_10

    .line 7733
    const/4 v0, 0x1

    iget-object v2, p0, Lmmq;->a:Lmok;

    .line 7734
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7736
    :goto_0
    iget v2, p0, Lmmq;->m:I

    if-eq v2, v7, :cond_0

    .line 7737
    const/4 v2, 0x3

    iget v3, p0, Lmmq;->m:I

    .line 7738
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7740
    :cond_0
    iget-object v2, p0, Lmmq;->b:Lodo;

    if-eqz v2, :cond_1

    .line 7741
    const/4 v2, 0x4

    iget-object v3, p0, Lmmq;->b:Lodo;

    .line 7742
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7744
    :cond_1
    iget-object v2, p0, Lmmq;->c:Lmoo;

    if-eqz v2, :cond_2

    .line 7745
    const/4 v2, 0x5

    iget-object v3, p0, Lmmq;->c:Lmoo;

    .line 7746
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7748
    :cond_2
    iget-object v2, p0, Lmmq;->d:Lmmi;

    if-eqz v2, :cond_3

    .line 7749
    const/4 v2, 0x6

    iget-object v3, p0, Lmmq;->d:Lmmi;

    .line 7750
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7752
    :cond_3
    iget-object v2, p0, Lmmq;->e:[Lmma;

    if-eqz v2, :cond_5

    .line 7753
    iget-object v3, p0, Lmmq;->e:[Lmma;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    .line 7754
    if-eqz v5, :cond_4

    .line 7755
    const/4 v6, 0x7

    .line 7756
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 7753
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 7760
    :cond_5
    iget-object v2, p0, Lmmq;->f:Lmoi;

    if-eqz v2, :cond_6

    .line 7761
    const/16 v2, 0x9

    iget-object v3, p0, Lmmq;->f:Lmoi;

    .line 7762
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7764
    :cond_6
    iget-object v2, p0, Lmmq;->g:Lmlz;

    if-eqz v2, :cond_7

    .line 7765
    const/16 v2, 0xa

    iget-object v3, p0, Lmmq;->g:Lmlz;

    .line 7766
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7768
    :cond_7
    iget-object v2, p0, Lmmq;->h:[Lmmt;

    if-eqz v2, :cond_9

    .line 7769
    iget-object v2, p0, Lmmq;->h:[Lmmt;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    .line 7770
    if-eqz v4, :cond_8

    .line 7771
    const/16 v5, 0xb

    .line 7772
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 7769
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 7776
    :cond_9
    iget-object v1, p0, Lmmq;->n:Lmoj;

    if-eqz v1, :cond_a

    .line 7777
    const/16 v1, 0xc

    iget-object v2, p0, Lmmq;->n:Lmoj;

    .line 7778
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7780
    :cond_a
    iget-object v1, p0, Lmmq;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    .line 7781
    const/16 v1, 0xd

    iget-object v2, p0, Lmmq;->i:Ljava/lang/Boolean;

    .line 7782
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 7784
    :cond_b
    iget-object v1, p0, Lmmq;->j:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 7785
    const/16 v1, 0xe

    iget-object v2, p0, Lmmq;->j:Ljava/lang/String;

    .line 7786
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7788
    :cond_c
    iget v1, p0, Lmmq;->o:I

    if-eq v1, v7, :cond_d

    .line 7789
    const/16 v1, 0xf

    iget v2, p0, Lmmq;->o:I

    .line 7790
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7792
    :cond_d
    iget-object v1, p0, Lmmq;->k:Ljava/lang/String;

    if-eqz v1, :cond_e

    .line 7793
    const/16 v1, 0x10

    iget-object v2, p0, Lmmq;->k:Ljava/lang/String;

    .line 7794
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7796
    :cond_e
    iget-object v1, p0, Lmmq;->l:Lnzx;

    if-eqz v1, :cond_f

    .line 7797
    const/16 v1, 0x11

    iget-object v2, p0, Lmmq;->l:Lnzx;

    .line 7798
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7800
    :cond_f
    iget-object v1, p0, Lmmq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7801
    iput v0, p0, Lmmq;->ai:I

    .line 7802
    return v0

    :cond_10
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmmq;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 7810
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7811
    sparse-switch v0, :sswitch_data_0

    .line 7815
    iget-object v2, p0, Lmmq;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 7816
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmmq;->ah:Ljava/util/List;

    .line 7819
    :cond_1
    iget-object v2, p0, Lmmq;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7821
    :sswitch_0
    return-object p0

    .line 7826
    :sswitch_1
    iget-object v0, p0, Lmmq;->a:Lmok;

    if-nez v0, :cond_2

    .line 7827
    new-instance v0, Lmok;

    invoke-direct {v0}, Lmok;-><init>()V

    iput-object v0, p0, Lmmq;->a:Lmok;

    .line 7829
    :cond_2
    iget-object v0, p0, Lmmq;->a:Lmok;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7833
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 7834
    if-eqz v0, :cond_3

    if-ne v0, v4, :cond_4

    .line 7836
    :cond_3
    iput v0, p0, Lmmq;->m:I

    goto :goto_0

    .line 7838
    :cond_4
    iput v1, p0, Lmmq;->m:I

    goto :goto_0

    .line 7843
    :sswitch_3
    iget-object v0, p0, Lmmq;->b:Lodo;

    if-nez v0, :cond_5

    .line 7844
    new-instance v0, Lodo;

    invoke-direct {v0}, Lodo;-><init>()V

    iput-object v0, p0, Lmmq;->b:Lodo;

    .line 7846
    :cond_5
    iget-object v0, p0, Lmmq;->b:Lodo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7850
    :sswitch_4
    iget-object v0, p0, Lmmq;->c:Lmoo;

    if-nez v0, :cond_6

    .line 7851
    new-instance v0, Lmoo;

    invoke-direct {v0}, Lmoo;-><init>()V

    iput-object v0, p0, Lmmq;->c:Lmoo;

    .line 7853
    :cond_6
    iget-object v0, p0, Lmmq;->c:Lmoo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7857
    :sswitch_5
    iget-object v0, p0, Lmmq;->d:Lmmi;

    if-nez v0, :cond_7

    .line 7858
    new-instance v0, Lmmi;

    invoke-direct {v0}, Lmmi;-><init>()V

    iput-object v0, p0, Lmmq;->d:Lmmi;

    .line 7860
    :cond_7
    iget-object v0, p0, Lmmq;->d:Lmmi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7864
    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7865
    iget-object v0, p0, Lmmq;->e:[Lmma;

    if-nez v0, :cond_9

    move v0, v1

    .line 7866
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmma;

    .line 7867
    iget-object v3, p0, Lmmq;->e:[Lmma;

    if-eqz v3, :cond_8

    .line 7868
    iget-object v3, p0, Lmmq;->e:[Lmma;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7870
    :cond_8
    iput-object v2, p0, Lmmq;->e:[Lmma;

    .line 7871
    :goto_2
    iget-object v2, p0, Lmmq;->e:[Lmma;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    .line 7872
    iget-object v2, p0, Lmmq;->e:[Lmma;

    new-instance v3, Lmma;

    invoke-direct {v3}, Lmma;-><init>()V

    aput-object v3, v2, v0

    .line 7873
    iget-object v2, p0, Lmmq;->e:[Lmma;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7874
    invoke-virtual {p1}, Loxn;->a()I

    .line 7871
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 7865
    :cond_9
    iget-object v0, p0, Lmmq;->e:[Lmma;

    array-length v0, v0

    goto :goto_1

    .line 7877
    :cond_a
    iget-object v2, p0, Lmmq;->e:[Lmma;

    new-instance v3, Lmma;

    invoke-direct {v3}, Lmma;-><init>()V

    aput-object v3, v2, v0

    .line 7878
    iget-object v2, p0, Lmmq;->e:[Lmma;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7882
    :sswitch_7
    iget-object v0, p0, Lmmq;->f:Lmoi;

    if-nez v0, :cond_b

    .line 7883
    new-instance v0, Lmoi;

    invoke-direct {v0}, Lmoi;-><init>()V

    iput-object v0, p0, Lmmq;->f:Lmoi;

    .line 7885
    :cond_b
    iget-object v0, p0, Lmmq;->f:Lmoi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7889
    :sswitch_8
    iget-object v0, p0, Lmmq;->g:Lmlz;

    if-nez v0, :cond_c

    .line 7890
    new-instance v0, Lmlz;

    invoke-direct {v0}, Lmlz;-><init>()V

    iput-object v0, p0, Lmmq;->g:Lmlz;

    .line 7892
    :cond_c
    iget-object v0, p0, Lmmq;->g:Lmlz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7896
    :sswitch_9
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 7897
    iget-object v0, p0, Lmmq;->h:[Lmmt;

    if-nez v0, :cond_e

    move v0, v1

    .line 7898
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lmmt;

    .line 7899
    iget-object v3, p0, Lmmq;->h:[Lmmt;

    if-eqz v3, :cond_d

    .line 7900
    iget-object v3, p0, Lmmq;->h:[Lmmt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 7902
    :cond_d
    iput-object v2, p0, Lmmq;->h:[Lmmt;

    .line 7903
    :goto_4
    iget-object v2, p0, Lmmq;->h:[Lmmt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    .line 7904
    iget-object v2, p0, Lmmq;->h:[Lmmt;

    new-instance v3, Lmmt;

    invoke-direct {v3}, Lmmt;-><init>()V

    aput-object v3, v2, v0

    .line 7905
    iget-object v2, p0, Lmmq;->h:[Lmmt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 7906
    invoke-virtual {p1}, Loxn;->a()I

    .line 7903
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 7897
    :cond_e
    iget-object v0, p0, Lmmq;->h:[Lmmt;

    array-length v0, v0

    goto :goto_3

    .line 7909
    :cond_f
    iget-object v2, p0, Lmmq;->h:[Lmmt;

    new-instance v3, Lmmt;

    invoke-direct {v3}, Lmmt;-><init>()V

    aput-object v3, v2, v0

    .line 7910
    iget-object v2, p0, Lmmq;->h:[Lmmt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7914
    :sswitch_a
    iget-object v0, p0, Lmmq;->n:Lmoj;

    if-nez v0, :cond_10

    .line 7915
    new-instance v0, Lmoj;

    invoke-direct {v0}, Lmoj;-><init>()V

    iput-object v0, p0, Lmmq;->n:Lmoj;

    .line 7917
    :cond_10
    iget-object v0, p0, Lmmq;->n:Lmoj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7921
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmmq;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 7925
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmmq;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 7929
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 7930
    if-eq v0, v4, :cond_11

    const/4 v2, 0x2

    if-eq v0, v2, :cond_11

    const/4 v2, 0x3

    if-eq v0, v2, :cond_11

    const/4 v2, 0x4

    if-ne v0, v2, :cond_12

    .line 7934
    :cond_11
    iput v0, p0, Lmmq;->o:I

    goto/16 :goto_0

    .line 7936
    :cond_12
    iput v4, p0, Lmmq;->o:I

    goto/16 :goto_0

    .line 7941
    :sswitch_e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmmq;->k:Ljava/lang/String;

    goto/16 :goto_0

    .line 7945
    :sswitch_f
    iget-object v0, p0, Lmmq;->l:Lnzx;

    if-nez v0, :cond_13

    .line 7946
    new-instance v0, Lnzx;

    invoke-direct {v0}, Lnzx;-><init>()V

    iput-object v0, p0, Lmmq;->l:Lnzx;

    .line 7948
    :cond_13
    iget-object v0, p0, Lmmq;->l:Lnzx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 7811
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x62 -> :sswitch_a
        0x68 -> :sswitch_b
        0x72 -> :sswitch_c
        0x78 -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v6, -0x80000000

    .line 7672
    iget-object v1, p0, Lmmq;->a:Lmok;

    if-eqz v1, :cond_0

    .line 7673
    const/4 v1, 0x1

    iget-object v2, p0, Lmmq;->a:Lmok;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7675
    :cond_0
    iget v1, p0, Lmmq;->m:I

    if-eq v1, v6, :cond_1

    .line 7676
    const/4 v1, 0x3

    iget v2, p0, Lmmq;->m:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 7678
    :cond_1
    iget-object v1, p0, Lmmq;->b:Lodo;

    if-eqz v1, :cond_2

    .line 7679
    const/4 v1, 0x4

    iget-object v2, p0, Lmmq;->b:Lodo;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7681
    :cond_2
    iget-object v1, p0, Lmmq;->c:Lmoo;

    if-eqz v1, :cond_3

    .line 7682
    const/4 v1, 0x5

    iget-object v2, p0, Lmmq;->c:Lmoo;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7684
    :cond_3
    iget-object v1, p0, Lmmq;->d:Lmmi;

    if-eqz v1, :cond_4

    .line 7685
    const/4 v1, 0x6

    iget-object v2, p0, Lmmq;->d:Lmmi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7687
    :cond_4
    iget-object v1, p0, Lmmq;->e:[Lmma;

    if-eqz v1, :cond_6

    .line 7688
    iget-object v2, p0, Lmmq;->e:[Lmma;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 7689
    if-eqz v4, :cond_5

    .line 7690
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 7688
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7694
    :cond_6
    iget-object v1, p0, Lmmq;->f:Lmoi;

    if-eqz v1, :cond_7

    .line 7695
    const/16 v1, 0x9

    iget-object v2, p0, Lmmq;->f:Lmoi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7697
    :cond_7
    iget-object v1, p0, Lmmq;->g:Lmlz;

    if-eqz v1, :cond_8

    .line 7698
    const/16 v1, 0xa

    iget-object v2, p0, Lmmq;->g:Lmlz;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 7700
    :cond_8
    iget-object v1, p0, Lmmq;->h:[Lmmt;

    if-eqz v1, :cond_a

    .line 7701
    iget-object v1, p0, Lmmq;->h:[Lmmt;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    .line 7702
    if-eqz v3, :cond_9

    .line 7703
    const/16 v4, 0xb

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 7701
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 7707
    :cond_a
    iget-object v0, p0, Lmmq;->n:Lmoj;

    if-eqz v0, :cond_b

    .line 7708
    const/16 v0, 0xc

    iget-object v1, p0, Lmmq;->n:Lmoj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7710
    :cond_b
    iget-object v0, p0, Lmmq;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 7711
    const/16 v0, 0xd

    iget-object v1, p0, Lmmq;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 7713
    :cond_c
    iget-object v0, p0, Lmmq;->j:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 7714
    const/16 v0, 0xe

    iget-object v1, p0, Lmmq;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7716
    :cond_d
    iget v0, p0, Lmmq;->o:I

    if-eq v0, v6, :cond_e

    .line 7717
    const/16 v0, 0xf

    iget v1, p0, Lmmq;->o:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 7719
    :cond_e
    iget-object v0, p0, Lmmq;->k:Ljava/lang/String;

    if-eqz v0, :cond_f

    .line 7720
    const/16 v0, 0x10

    iget-object v1, p0, Lmmq;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 7722
    :cond_f
    iget-object v0, p0, Lmmq;->l:Lnzx;

    if-eqz v0, :cond_10

    .line 7723
    const/16 v0, 0x11

    iget-object v1, p0, Lmmq;->l:Lnzx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7725
    :cond_10
    iget-object v0, p0, Lmmq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7727
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7616
    invoke-virtual {p0, p1}, Lmmq;->a(Loxn;)Lmmq;

    move-result-object v0

    return-object v0
.end method
