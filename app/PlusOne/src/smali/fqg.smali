.class public final Lfqg;
.super Lepp;
.source "PG"


# instance fields
.field private final d:Legi;

.field private e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILept;Legi;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lepp;-><init>(Landroid/content/Context;ILept;)V

    .line 32
    iput-object p4, p0, Lfqg;->d:Legi;

    .line 33
    return-void
.end method

.method static synthetic a(Lfqg;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lfqg;->e:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic a(Lfqg;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lfqg;->e:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Lfqg;Lfhh;)V
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lfqg;->a(Lfhh;)V

    return-void
.end method

.method public static a(Landroid/content/Context;ILnyq;)Z
    .locals 4

    .prologue
    .line 41
    sget-object v0, Lepn;->f:Lepn;

    invoke-static {p0, p1, v0}, Ldhv;->a(Landroid/content/Context;ILepn;)Ldwi;

    move-result-object v0

    .line 42
    if-eqz p2, :cond_0

    iget-object v1, p2, Lnyq;->m:Lnyu;

    if-eqz v1, :cond_0

    iget-object v1, p2, Lnyq;->m:Lnyu;

    iget-object v1, v1, Lnyu;->a:Ljava/lang/Boolean;

    .line 44
    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    invoke-static {p0, p1}, Ldhv;->v(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    invoke-virtual {v0}, Ldwi;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lfqg;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 85
    :cond_0
    new-instance v0, Lnyq;

    invoke-direct {v0}, Lnyq;-><init>()V

    .line 86
    new-instance v1, Lnyu;

    invoke-direct {v1}, Lnyu;-><init>()V

    iput-object v1, v0, Lnyq;->m:Lnyu;

    .line 87
    iget-object v1, v0, Lnyq;->m:Lnyu;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnyu;->a:Ljava/lang/Boolean;

    .line 88
    new-instance v1, Lfqh;

    invoke-direct {v1, p0}, Lfqh;-><init>(Lfqg;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 100
    iget-object v1, p0, Lfqg;->c:Landroid/content/Context;

    iget v2, p0, Lfqg;->b:I

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILnyq;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lfqg;->e:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-object v0, p0, Lfqg;->c:Landroid/content/Context;

    iget v1, p0, Lfqg;->b:I

    invoke-static {v0, v1}, Lfqt;->a(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lfqg;->d:Legi;

    invoke-virtual {v1, v0}, Legi;->b(Landroid/content/Intent;)V

    .line 53
    iget-object v0, p0, Lfqg;->d:Legi;

    invoke-virtual {v0}, Legi;->n()Lz;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lz;->overridePendingTransition(II)V

    .line 54
    invoke-direct {p0}, Lfqg;->g()V

    .line 55
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lfqg;->c:Landroid/content/Context;

    const v1, 0x7f0a01e2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lfqg;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lfqg;->c:Landroid/content/Context;

    const v1, 0x7f0a01e4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lfqg;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public a(Lnyq;)Z
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lfqg;->c:Landroid/content/Context;

    iget v1, p0, Lfqg;->b:I

    invoke-static {v0, v1, p1}, Lfqg;->a(Landroid/content/Context;ILnyq;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0}, Lfqg;->f()V

    .line 60
    invoke-direct {p0}, Lfqg;->g()V

    .line 61
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 65
    const v0, 0x7f04020e

    return v0
.end method

.method public e()Lepn;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lepn;->f:Lepn;

    return-object v0
.end method
