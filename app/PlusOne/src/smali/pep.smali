.class public final Lpep;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 493
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 519
    const/4 v0, 0x0

    .line 520
    iget-object v1, p0, Lpep;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 521
    const/4 v0, 0x1

    iget-object v1, p0, Lpep;->b:Ljava/lang/Long;

    .line 522
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 524
    :cond_0
    iget-object v1, p0, Lpep;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 525
    const/4 v1, 0x2

    iget-object v2, p0, Lpep;->a:Ljava/lang/String;

    .line 526
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 528
    :cond_1
    iget-object v1, p0, Lpep;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 529
    const/4 v1, 0x3

    iget-object v2, p0, Lpep;->c:Ljava/lang/String;

    .line 530
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 532
    :cond_2
    iget-object v1, p0, Lpep;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 533
    iput v0, p0, Lpep;->ai:I

    .line 534
    return v0
.end method

.method public a(Loxn;)Lpep;
    .locals 2

    .prologue
    .line 542
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 543
    sparse-switch v0, :sswitch_data_0

    .line 547
    iget-object v1, p0, Lpep;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 548
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpep;->ah:Ljava/util/List;

    .line 551
    :cond_1
    iget-object v1, p0, Lpep;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 553
    :sswitch_0
    return-object p0

    .line 558
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpep;->b:Ljava/lang/Long;

    goto :goto_0

    .line 562
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpep;->a:Ljava/lang/String;

    goto :goto_0

    .line 566
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpep;->c:Ljava/lang/String;

    goto :goto_0

    .line 543
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 504
    iget-object v0, p0, Lpep;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 505
    const/4 v0, 0x1

    iget-object v1, p0, Lpep;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 507
    :cond_0
    iget-object v0, p0, Lpep;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 508
    const/4 v0, 0x2

    iget-object v1, p0, Lpep;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 510
    :cond_1
    iget-object v0, p0, Lpep;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 511
    const/4 v0, 0x3

    iget-object v1, p0, Lpep;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 513
    :cond_2
    iget-object v0, p0, Lpep;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 515
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 489
    invoke-virtual {p0, p1}, Lpep;->a(Loxn;)Lpep;

    move-result-object v0

    return-object v0
.end method
