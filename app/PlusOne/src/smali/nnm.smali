.class public final Lnnm;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnnm;


# instance fields
.field public b:Lnsr;

.field private c:[Lnsh;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 810
    const/4 v0, 0x0

    new-array v0, v0, [Lnnm;

    sput-object v0, Lnnm;->a:[Lnnm;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 811
    invoke-direct {p0}, Loxq;-><init>()V

    .line 814
    const/4 v0, 0x0

    iput-object v0, p0, Lnnm;->b:Lnsr;

    .line 817
    sget-object v0, Lnsh;->a:[Lnsh;

    iput-object v0, p0, Lnnm;->c:[Lnsh;

    .line 811
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 843
    .line 844
    iget-object v0, p0, Lnnm;->b:Lnsr;

    if-eqz v0, :cond_3

    .line 845
    const/4 v0, 0x1

    iget-object v2, p0, Lnnm;->b:Lnsr;

    .line 846
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 848
    :goto_0
    iget-object v2, p0, Lnnm;->c:[Lnsh;

    if-eqz v2, :cond_1

    .line 849
    iget-object v2, p0, Lnnm;->c:[Lnsh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 850
    if-eqz v4, :cond_0

    .line 851
    const/4 v5, 0x2

    .line 852
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 849
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 856
    :cond_1
    iget-object v1, p0, Lnnm;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 857
    const/16 v1, 0xd

    iget-object v2, p0, Lnnm;->d:Ljava/lang/String;

    .line 858
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 860
    :cond_2
    iget-object v1, p0, Lnnm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 861
    iput v0, p0, Lnnm;->ai:I

    .line 862
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnnm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 870
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 871
    sparse-switch v0, :sswitch_data_0

    .line 875
    iget-object v2, p0, Lnnm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 876
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnnm;->ah:Ljava/util/List;

    .line 879
    :cond_1
    iget-object v2, p0, Lnnm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 881
    :sswitch_0
    return-object p0

    .line 886
    :sswitch_1
    iget-object v0, p0, Lnnm;->b:Lnsr;

    if-nez v0, :cond_2

    .line 887
    new-instance v0, Lnsr;

    invoke-direct {v0}, Lnsr;-><init>()V

    iput-object v0, p0, Lnnm;->b:Lnsr;

    .line 889
    :cond_2
    iget-object v0, p0, Lnnm;->b:Lnsr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 893
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 894
    iget-object v0, p0, Lnnm;->c:[Lnsh;

    if-nez v0, :cond_4

    move v0, v1

    .line 895
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnsh;

    .line 896
    iget-object v3, p0, Lnnm;->c:[Lnsh;

    if-eqz v3, :cond_3

    .line 897
    iget-object v3, p0, Lnnm;->c:[Lnsh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 899
    :cond_3
    iput-object v2, p0, Lnnm;->c:[Lnsh;

    .line 900
    :goto_2
    iget-object v2, p0, Lnnm;->c:[Lnsh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 901
    iget-object v2, p0, Lnnm;->c:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 902
    iget-object v2, p0, Lnnm;->c:[Lnsh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 903
    invoke-virtual {p1}, Loxn;->a()I

    .line 900
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 894
    :cond_4
    iget-object v0, p0, Lnnm;->c:[Lnsh;

    array-length v0, v0

    goto :goto_1

    .line 906
    :cond_5
    iget-object v2, p0, Lnnm;->c:[Lnsh;

    new-instance v3, Lnsh;

    invoke-direct {v3}, Lnsh;-><init>()V

    aput-object v3, v2, v0

    .line 907
    iget-object v2, p0, Lnnm;->c:[Lnsh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 911
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnnm;->d:Ljava/lang/String;

    goto :goto_0

    .line 871
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x6a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 824
    iget-object v0, p0, Lnnm;->b:Lnsr;

    if-eqz v0, :cond_0

    .line 825
    const/4 v0, 0x1

    iget-object v1, p0, Lnnm;->b:Lnsr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 827
    :cond_0
    iget-object v0, p0, Lnnm;->c:[Lnsh;

    if-eqz v0, :cond_2

    .line 828
    iget-object v1, p0, Lnnm;->c:[Lnsh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 829
    if-eqz v3, :cond_1

    .line 830
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 828
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 834
    :cond_2
    iget-object v0, p0, Lnnm;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 835
    const/16 v0, 0xd

    iget-object v1, p0, Lnnm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 837
    :cond_3
    iget-object v0, p0, Lnnm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 839
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 807
    invoke-virtual {p0, p1}, Lnnm;->a(Loxn;)Lnnm;

    move-result-object v0

    return-object v0
.end method
