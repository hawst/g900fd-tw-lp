.class public final Lokv;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lokw;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Integer;

.field private l:Ljava/lang/Integer;

.field private m:Ljava/lang/Integer;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Loxq;-><init>()V

    .line 112
    sget-object v0, Lokw;->a:[Lokw;

    iput-object v0, p0, Lokv;->a:[Lokw;

    .line 119
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lokv;->d:[Ljava/lang/String;

    .line 109
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 198
    .line 199
    iget-object v0, p0, Lokv;->a:[Lokw;

    if-eqz v0, :cond_1

    .line 200
    iget-object v3, p0, Lokv;->a:[Lokw;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 201
    if-eqz v5, :cond_0

    .line 202
    const/4 v6, 0x1

    .line 203
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 200
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 207
    :cond_2
    iget-object v2, p0, Lokv;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 208
    const/4 v2, 0x2

    iget-object v3, p0, Lokv;->c:Ljava/lang/String;

    .line 209
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 211
    :cond_3
    iget-object v2, p0, Lokv;->d:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lokv;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 213
    iget-object v3, p0, Lokv;->d:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 215
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 213
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 217
    :cond_4
    add-int/2addr v0, v2

    .line 218
    iget-object v1, p0, Lokv;->d:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 220
    :cond_5
    iget-object v1, p0, Lokv;->e:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 221
    const/4 v1, 0x4

    iget-object v2, p0, Lokv;->e:Ljava/lang/String;

    .line 222
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    :cond_6
    iget-object v1, p0, Lokv;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 225
    const/4 v1, 0x5

    iget-object v2, p0, Lokv;->f:Ljava/lang/Integer;

    .line 226
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_7
    iget-object v1, p0, Lokv;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 229
    const/4 v1, 0x6

    iget-object v2, p0, Lokv;->g:Ljava/lang/Integer;

    .line 230
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_8
    iget-object v1, p0, Lokv;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 233
    const/4 v1, 0x7

    iget-object v2, p0, Lokv;->h:Ljava/lang/Integer;

    .line 234
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_9
    iget-object v1, p0, Lokv;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 237
    const/16 v1, 0x8

    iget-object v2, p0, Lokv;->i:Ljava/lang/Integer;

    .line 238
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 240
    :cond_a
    iget-object v1, p0, Lokv;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 241
    const/16 v1, 0x9

    iget-object v2, p0, Lokv;->j:Ljava/lang/Integer;

    .line 242
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 244
    :cond_b
    iget-object v1, p0, Lokv;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 245
    const/16 v1, 0xa

    iget-object v2, p0, Lokv;->k:Ljava/lang/Integer;

    .line 246
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    :cond_c
    iget-object v1, p0, Lokv;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 249
    const/16 v1, 0xb

    iget-object v2, p0, Lokv;->l:Ljava/lang/Integer;

    .line 250
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    :cond_d
    iget-object v1, p0, Lokv;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 253
    const/16 v1, 0xc

    iget-object v2, p0, Lokv;->m:Ljava/lang/Integer;

    .line 254
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 256
    :cond_e
    iget-object v1, p0, Lokv;->n:Ljava/lang/String;

    if-eqz v1, :cond_f

    .line 257
    const/16 v1, 0xd

    iget-object v2, p0, Lokv;->n:Ljava/lang/String;

    .line 258
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_f
    iget-object v1, p0, Lokv;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 261
    const/16 v1, 0xe

    iget-object v2, p0, Lokv;->b:Ljava/lang/Integer;

    .line 262
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_10
    iget-object v1, p0, Lokv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    iput v0, p0, Lokv;->ai:I

    .line 266
    return v0
.end method

.method public a(Loxn;)Lokv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 274
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 275
    sparse-switch v0, :sswitch_data_0

    .line 279
    iget-object v2, p0, Lokv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 280
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lokv;->ah:Ljava/util/List;

    .line 283
    :cond_1
    iget-object v2, p0, Lokv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    :sswitch_0
    return-object p0

    .line 290
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 291
    iget-object v0, p0, Lokv;->a:[Lokw;

    if-nez v0, :cond_3

    move v0, v1

    .line 292
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lokw;

    .line 293
    iget-object v3, p0, Lokv;->a:[Lokw;

    if-eqz v3, :cond_2

    .line 294
    iget-object v3, p0, Lokv;->a:[Lokw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 296
    :cond_2
    iput-object v2, p0, Lokv;->a:[Lokw;

    .line 297
    :goto_2
    iget-object v2, p0, Lokv;->a:[Lokw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 298
    iget-object v2, p0, Lokv;->a:[Lokw;

    new-instance v3, Lokw;

    invoke-direct {v3}, Lokw;-><init>()V

    aput-object v3, v2, v0

    .line 299
    iget-object v2, p0, Lokv;->a:[Lokw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 300
    invoke-virtual {p1}, Loxn;->a()I

    .line 297
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 291
    :cond_3
    iget-object v0, p0, Lokv;->a:[Lokw;

    array-length v0, v0

    goto :goto_1

    .line 303
    :cond_4
    iget-object v2, p0, Lokv;->a:[Lokw;

    new-instance v3, Lokw;

    invoke-direct {v3}, Lokw;-><init>()V

    aput-object v3, v2, v0

    .line 304
    iget-object v2, p0, Lokv;->a:[Lokw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 308
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokv;->c:Ljava/lang/String;

    goto :goto_0

    .line 312
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 313
    iget-object v0, p0, Lokv;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 314
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 315
    iget-object v3, p0, Lokv;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 316
    iput-object v2, p0, Lokv;->d:[Ljava/lang/String;

    .line 317
    :goto_3
    iget-object v2, p0, Lokv;->d:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 318
    iget-object v2, p0, Lokv;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 319
    invoke-virtual {p1}, Loxn;->a()I

    .line 317
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 322
    :cond_5
    iget-object v2, p0, Lokv;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 326
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokv;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 330
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokv;->f:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 334
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokv;->g:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 338
    :sswitch_7
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokv;->h:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 342
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokv;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 346
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokv;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 350
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokv;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 354
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokv;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 358
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokv;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 362
    :sswitch_d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokv;->n:Ljava/lang/String;

    goto/16 :goto_0

    .line 366
    :sswitch_e
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lokv;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 275
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 144
    iget-object v1, p0, Lokv;->a:[Lokw;

    if-eqz v1, :cond_1

    .line 145
    iget-object v2, p0, Lokv;->a:[Lokw;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 146
    if-eqz v4, :cond_0

    .line 147
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 145
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    :cond_1
    iget-object v1, p0, Lokv;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 152
    const/4 v1, 0x2

    iget-object v2, p0, Lokv;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 154
    :cond_2
    iget-object v1, p0, Lokv;->d:[Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 155
    iget-object v1, p0, Lokv;->d:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 156
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 155
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 159
    :cond_3
    iget-object v0, p0, Lokv;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 160
    const/4 v0, 0x4

    iget-object v1, p0, Lokv;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 162
    :cond_4
    iget-object v0, p0, Lokv;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 163
    const/4 v0, 0x5

    iget-object v1, p0, Lokv;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 165
    :cond_5
    iget-object v0, p0, Lokv;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 166
    const/4 v0, 0x6

    iget-object v1, p0, Lokv;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 168
    :cond_6
    iget-object v0, p0, Lokv;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 169
    const/4 v0, 0x7

    iget-object v1, p0, Lokv;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 171
    :cond_7
    iget-object v0, p0, Lokv;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 172
    const/16 v0, 0x8

    iget-object v1, p0, Lokv;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 174
    :cond_8
    iget-object v0, p0, Lokv;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 175
    const/16 v0, 0x9

    iget-object v1, p0, Lokv;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 177
    :cond_9
    iget-object v0, p0, Lokv;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 178
    const/16 v0, 0xa

    iget-object v1, p0, Lokv;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 180
    :cond_a
    iget-object v0, p0, Lokv;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 181
    const/16 v0, 0xb

    iget-object v1, p0, Lokv;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 183
    :cond_b
    iget-object v0, p0, Lokv;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 184
    const/16 v0, 0xc

    iget-object v1, p0, Lokv;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 186
    :cond_c
    iget-object v0, p0, Lokv;->n:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 187
    const/16 v0, 0xd

    iget-object v1, p0, Lokv;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 189
    :cond_d
    iget-object v0, p0, Lokv;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 190
    const/16 v0, 0xe

    iget-object v1, p0, Lokv;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 192
    :cond_e
    iget-object v0, p0, Lokv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 194
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lokv;->a(Loxn;)Lokv;

    move-result-object v0

    return-object v0
.end method
