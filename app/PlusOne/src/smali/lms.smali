.class final Llms;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/nio/channels/WritableByteChannel;


# instance fields
.field private a:Z

.field private synthetic b:Llmp;


# direct methods
.method constructor <init>(Llmp;)V
    .locals 1

    .prologue
    .line 50
    iput-object p1, p0, Llms;->b:Llmp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Llms;->a:Z

    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Llms;->a:Z

    .line 83
    iget-object v0, p0, Llms;->b:Llmp;

    iget-object v1, v0, Llmp;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 84
    :try_start_0
    iget-object v0, p0, Llms;->b:Llmp;

    const/4 v2, 0x1

    iput-boolean v2, v0, Llmp;->b:Z

    .line 85
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Llms;->a:Z

    return v0
.end method

.method public write(Ljava/nio/ByteBuffer;)I
    .locals 4

    .prologue
    .line 56
    iget-boolean v0, p0, Llms;->a:Z

    if-nez v0, :cond_0

    .line 57
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 60
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 62
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 63
    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 64
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 65
    iget-object v2, p0, Llms;->b:Llmp;

    iget-object v2, v2, Llmp;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 66
    :try_start_0
    iget-object v3, p0, Llms;->b:Llmp;

    iget-object v3, v3, Llmp;->d:Ljava/util/Deque;

    invoke-interface {v3, v1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 67
    iget-object v1, p0, Llms;->b:Llmp;

    iget-object v3, p0, Llms;->b:Llmp;

    iget v3, v3, Llmp;->c:I

    add-int/2addr v3, v0

    iput v3, v1, Llmp;->c:I

    .line 68
    monitor-exit v2

    .line 71
    :goto_0
    return v0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
