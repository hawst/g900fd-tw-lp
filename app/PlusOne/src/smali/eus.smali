.class public Leus;
.super Lhyd;
.source "PG"


# static fields
.field private static final f:[Ljava/lang/String;


# instance fields
.field public e:Landroid/view/LayoutInflater;

.field private final g:Lhee;

.field private final h:Lhei;

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "index"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "given_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "avatar_url"

    aput-object v2, v0, v1

    sput-object v0, Leus;->f:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lhyd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 50
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Leus;->e:Landroid/view/LayoutInflater;

    .line 51
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Leus;->h:Lhei;

    .line 52
    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Leus;->g:Lhee;

    .line 53
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0, p3}, Leus;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Leus;->e:Landroid/view/LayoutInflater;

    const v1, 0x7f040018

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 131
    invoke-virtual {p0, p1, p3}, Leus;->a(Landroid/view/View;Landroid/database/Cursor;)V

    .line 132
    return-void
.end method

.method protected a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 12

    .prologue
    const v5, 0x7f100130

    const/4 v2, 0x1

    const/16 v11, 0x8

    const/4 v3, 0x4

    const/4 v4, 0x0

    .line 137
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, -0x2

    if-ne v0, v1, :cond_1

    .line 138
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 139
    const v0, 0x7f100135

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 141
    const v0, 0x7f100138

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 142
    iget-boolean v1, p0, Leus;->i:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0a0b5a

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 178
    :goto_1
    return-void

    .line 142
    :cond_0
    const v1, 0x7f0a0b5b

    goto :goto_0

    .line 145
    :cond_1
    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 146
    const v0, 0x7f100135

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 148
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 149
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 150
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 151
    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 153
    const v0, 0x7f100131

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 154
    const v0, 0x7f100133

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 155
    const v1, 0x7f100134

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 156
    const v2, 0x7f100132

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 158
    const/4 v10, -0x1

    if-ne v5, v10, :cond_2

    .line 159
    invoke-virtual {v9, v3}, Landroid/view/View;->setVisibility(I)V

    .line 160
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    goto :goto_1

    .line 165
    :cond_2
    iget-boolean v10, p0, Leus;->j:Z

    if-eqz v10, :cond_3

    iget-object v10, p0, Leus;->g:Lhee;

    .line 166
    invoke-interface {v10}, Lhee;->d()I

    move-result v10

    if-ne v5, v10, :cond_3

    move v3, v4

    .line 165
    :cond_3
    invoke-virtual {v9, v3}, Landroid/view/View;->setVisibility(I)V

    .line 169
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    invoke-virtual {v2, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setVisibility(I)V

    .line 174
    const/4 v0, 0x0

    invoke-virtual {v2, v0, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-virtual {v2, v4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->d(Z)V

    goto/16 :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 56
    iput-boolean p1, p0, Leus;->j:Z

    .line 57
    return-void
.end method

.method public a(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 101
    invoke-virtual {p0}, Leus;->a()Landroid/database/Cursor;

    move-result-object v2

    .line 102
    if-eqz v2, :cond_1

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 103
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 104
    const/4 v3, -0x2

    if-ne v2, v3, :cond_0

    .line 106
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 104
    goto :goto_0

    :cond_1
    move v0, v1

    .line 106
    goto :goto_0
.end method

.method public b(I)I
    .locals 2

    .prologue
    .line 110
    invoke-virtual {p0}, Leus;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 111
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 115
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 60
    iput-boolean p1, p0, Leus;->k:Z

    .line 61
    return-void
.end method

.method public c()V
    .locals 15

    .prologue
    const/4 v14, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 64
    iget-object v0, p0, Leus;->h:Lhei;

    new-array v3, v2, [Ljava/lang/String;

    const-string v4, "logged_in"

    aput-object v4, v3, v1

    invoke-interface {v0, v3}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 67
    iput-boolean v1, p0, Leus;->i:Z

    .line 68
    new-instance v8, Landroid/database/MatrixCursor;

    sget-object v0, Leus;->f:[Ljava/lang/String;

    invoke-direct {v8, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 70
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    move v3, v1

    move v4, v1

    move v6, v1

    .line 71
    :goto_0
    if-ge v3, v9, :cond_4

    .line 72
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 74
    iget-object v5, p0, Leus;->h:Lhei;

    invoke-interface {v5, v0}, Lhei;->a(I)Lhej;

    move-result-object v10

    .line 75
    const-string v5, "is_plus_page"

    invoke-interface {v10, v5}, Lhej;->c(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 76
    iput-boolean v2, p0, Leus;->i:Z

    .line 78
    :cond_0
    const/4 v5, 0x6

    new-array v11, v5, [Ljava/lang/Object;

    add-int/lit8 v5, v4, 0x1

    .line 79
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v11, v1

    .line 80
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v11, v2

    const-string v0, "account_name"

    .line 81
    invoke-interface {v10, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v11, v13

    const-string v0, "display_name"

    .line 82
    invoke-interface {v10, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v11, v14

    const/4 v0, 0x4

    const-string v4, "given_name"

    .line 83
    invoke-interface {v10, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v11, v0

    const/4 v0, 0x5

    const-string v4, "profile_photo_url"

    .line 84
    invoke-interface {v10, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v11, v0

    .line 78
    invoke-virtual {v8, v11}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 87
    const-string v0, "is_plus_page"

    .line 88
    invoke-interface {v10, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "is_managed_account"

    .line 89
    invoke-interface {v10, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const-string v0, "page_count"

    .line 90
    invoke-interface {v10, v0, v1}, Lhej;->a(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    :goto_1
    or-int v4, v6, v0

    .line 71
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v6, v4

    move v4, v5

    goto :goto_0

    :cond_3
    move v0, v1

    .line 90
    goto :goto_1

    .line 93
    :cond_4
    iget-boolean v0, p0, Leus;->k:Z

    if-eqz v0, :cond_5

    if-eqz v6, :cond_5

    .line 94
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, -0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    aput-object v12, v0, v13

    aput-object v12, v0, v14

    const/4 v1, 0x4

    aput-object v12, v0, v1

    const/4 v1, 0x5

    aput-object v12, v0, v1

    invoke-virtual {v8, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 97
    :cond_5
    invoke-virtual {p0, v8}, Leus;->a(Landroid/database/Cursor;)V

    .line 98
    return-void
.end method

.method public d()I
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Leus;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Leus;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 182
    const/4 v0, 0x0

    .line 184
    :cond_0
    iget-object v1, p0, Leus;->b:Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 185
    iget-object v2, p0, Leus;->g:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 192
    :goto_0
    return v0

    .line 188
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 189
    iget-object v1, p0, Leus;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 192
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method
