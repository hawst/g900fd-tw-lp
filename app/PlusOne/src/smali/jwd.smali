.class abstract Ljwd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:I

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482
    const/4 v0, 0x2

    iput v0, p0, Ljwd;->a:I

    .line 484
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 514
    const/4 v1, 0x4

    iput v1, p0, Ljwd;->a:I

    .line 515
    invoke-virtual {p0}, Ljwd;->a()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Ljwd;->b:Ljava/lang/Object;

    .line 516
    iget v1, p0, Ljwd;->a:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 517
    iput v0, p0, Ljwd;->a:I

    .line 520
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 499
    iget v0, p0, Ljwd;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 500
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 503
    :cond_0
    sget-object v0, Ljvx;->a:[I

    iget v1, p0, Ljwd;->a:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 510
    invoke-virtual {p0}, Ljwd;->b()Z

    move-result v0

    :goto_0
    return v0

    .line 505
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 507
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 503
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 525
    invoke-virtual {p0}, Ljwd;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 526
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 528
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Ljwd;->a:I

    .line 529
    iget-object v0, p0, Ljwd;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 534
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
