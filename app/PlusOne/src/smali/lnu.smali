.class public Llnu;
.super Llrj;
.source "PG"

# interfaces
.implements Llnk;


# instance fields
.field public final a:Llnh;

.field private c:Llqw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Llrj;-><init>()V

    .line 18
    new-instance v0, Llnh;

    invoke-direct {v0}, Llnh;-><init>()V

    iput-object v0, p0, Llnu;->a:Llnh;

    return-void
.end method

.method static synthetic a(Llnu;)Llqc;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Llnu;->b:Llqc;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Llnu;->a:Llnh;

    new-instance v1, Lloe;

    iget-object v2, p0, Llnu;->b:Llqc;

    invoke-direct {v1, p0, v2}, Lloe;-><init>(Landroid/app/Activity;Llqr;)V

    invoke-virtual {v0, v1}, Llnh;->a(Llnq;)Llnh;

    .line 57
    return-void
.end method

.method public h_()Llnh;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Llnu;->a:Llnh;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 28
    invoke-virtual {p0}, Llnu;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llnh;->b(Landroid/content/Context;)Llnh;

    move-result-object v0

    .line 29
    iget-object v1, p0, Llnu;->a:Llnh;

    invoke-virtual {v1, p0}, Llnh;->a(Landroid/content/Context;)V

    .line 30
    iget-object v1, p0, Llnu;->a:Llnh;

    invoke-virtual {v1, v0}, Llnh;->a(Llnh;)V

    .line 32
    invoke-virtual {p0, p1}, Llnu;->a(Landroid/os/Bundle;)V

    .line 33
    iget-object v0, p0, Llnu;->a:Llnh;

    const-class v1, Lloi;

    invoke-virtual {v0, v1}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lloi;

    iget-object v2, p0, Llnu;->b:Llqc;

    iget-object v3, p0, Llnu;->a:Llnh;

    invoke-interface {v0, p0, v2, v3}, Lloi;->a(Landroid/app/Activity;Llqr;Llnh;)V

    goto :goto_0

    .line 35
    :cond_0
    iget-object v0, p0, Llnu;->a:Llnh;

    invoke-virtual {v0}, Llnh;->a()V

    .line 36
    iget-object v0, p0, Llnu;->b:Llqc;

    new-instance v1, Llnv;

    invoke-direct {v1, p0, p1}, Llnv;-><init>(Llnu;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Llqc;->a(Llqw;)Llqw;

    move-result-object v0

    iput-object v0, p0, Llnu;->c:Llqw;

    .line 46
    invoke-super {p0, p1}, Llrj;->onCreate(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Llnu;->b:Llqc;

    iget-object v1, p0, Llnu;->c:Llqw;

    invoke-virtual {v0, v1}, Llqc;->b(Llqw;)V

    .line 52
    invoke-super {p0}, Llrj;->onDestroy()V

    .line 53
    return-void
.end method
