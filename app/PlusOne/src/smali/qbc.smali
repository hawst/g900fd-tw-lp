.class public final Lqbc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Lqbo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2401
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2414
    const/4 v0, 0x0

    iput-object v0, p0, Lqbc;->f:Lqbo;

    .line 2401
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2443
    const/4 v0, 0x0

    .line 2444
    iget-object v1, p0, Lqbc;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 2445
    const/4 v0, 0x1

    iget-object v1, p0, Lqbc;->a:Ljava/lang/Boolean;

    .line 2446
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 2448
    :cond_0
    iget-object v1, p0, Lqbc;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 2449
    const/4 v1, 0x2

    iget-object v2, p0, Lqbc;->b:Ljava/lang/Boolean;

    .line 2450
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2452
    :cond_1
    iget-object v1, p0, Lqbc;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2453
    const/4 v1, 0x3

    iget-object v2, p0, Lqbc;->c:Ljava/lang/Integer;

    .line 2454
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2456
    :cond_2
    iget-object v1, p0, Lqbc;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2457
    const/4 v1, 0x4

    iget-object v2, p0, Lqbc;->d:Ljava/lang/Integer;

    .line 2458
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2460
    :cond_3
    iget-object v1, p0, Lqbc;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 2461
    const/4 v1, 0x5

    iget-object v2, p0, Lqbc;->e:Ljava/lang/Integer;

    .line 2462
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2464
    :cond_4
    iget-object v1, p0, Lqbc;->f:Lqbo;

    if-eqz v1, :cond_5

    .line 2465
    const/4 v1, 0x6

    iget-object v2, p0, Lqbc;->f:Lqbo;

    .line 2466
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2468
    :cond_5
    iget-object v1, p0, Lqbc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2469
    iput v0, p0, Lqbc;->ai:I

    .line 2470
    return v0
.end method

.method public a(Loxn;)Lqbc;
    .locals 2

    .prologue
    .line 2478
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2479
    sparse-switch v0, :sswitch_data_0

    .line 2483
    iget-object v1, p0, Lqbc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2484
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbc;->ah:Ljava/util/List;

    .line 2487
    :cond_1
    iget-object v1, p0, Lqbc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2489
    :sswitch_0
    return-object p0

    .line 2494
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lqbc;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 2498
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lqbc;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 2502
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbc;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 2506
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbc;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2510
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lqbc;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 2514
    :sswitch_6
    iget-object v0, p0, Lqbc;->f:Lqbo;

    if-nez v0, :cond_2

    .line 2515
    new-instance v0, Lqbo;

    invoke-direct {v0}, Lqbo;-><init>()V

    iput-object v0, p0, Lqbc;->f:Lqbo;

    .line 2517
    :cond_2
    iget-object v0, p0, Lqbc;->f:Lqbo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2479
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2419
    iget-object v0, p0, Lqbc;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 2420
    const/4 v0, 0x1

    iget-object v1, p0, Lqbc;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2422
    :cond_0
    iget-object v0, p0, Lqbc;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 2423
    const/4 v0, 0x2

    iget-object v1, p0, Lqbc;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2425
    :cond_1
    iget-object v0, p0, Lqbc;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2426
    const/4 v0, 0x3

    iget-object v1, p0, Lqbc;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2428
    :cond_2
    iget-object v0, p0, Lqbc;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2429
    const/4 v0, 0x4

    iget-object v1, p0, Lqbc;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2431
    :cond_3
    iget-object v0, p0, Lqbc;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2432
    const/4 v0, 0x5

    iget-object v1, p0, Lqbc;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2434
    :cond_4
    iget-object v0, p0, Lqbc;->f:Lqbo;

    if-eqz v0, :cond_5

    .line 2435
    const/4 v0, 0x6

    iget-object v1, p0, Lqbc;->f:Lqbo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2437
    :cond_5
    iget-object v0, p0, Lqbc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2439
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2397
    invoke-virtual {p0, p1}, Lqbc;->a(Loxn;)Lqbc;

    move-result-object v0

    return-object v0
.end method
