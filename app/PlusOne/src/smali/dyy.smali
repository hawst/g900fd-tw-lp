.class public final Ldyy;
.super Llgr;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private Q:Landroid/widget/TextView;

.field private R:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Llgr;-><init>()V

    .line 38
    return-void
.end method

.method public static U()Ldyy;
    .locals 1

    .prologue
    .line 55
    new-instance v0, Ldyy;

    invoke-direct {v0}, Ldyy;-><init>()V

    return-object v0
.end method

.method private W()Z
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p0}, Ldyy;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_0

    const-string v1, "circle_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Ldyy;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ldyy;->Q:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Ldyy;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ldyy;->R:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public V()V
    .locals 2

    .prologue
    .line 191
    invoke-virtual {p0}, Ldyy;->c()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 192
    if-nez v0, :cond_0

    .line 199
    :goto_0
    return-void

    .line 196
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 197
    iget-object v0, p0, Ldyy;->Q:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 198
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 177
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 82
    invoke-virtual {p0}, Ldyy;->W_()Landroid/content/Context;

    move-result-object v0

    .line 83
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 85
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 86
    const v2, 0x7f040063

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 88
    const v0, 0x7f10013b

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyy;->Q:Landroid/widget/TextView;

    .line 89
    iget-object v0, p0, Ldyy;->Q:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 90
    iget-object v0, p0, Ldyy;->Q:Landroid/widget/TextView;

    const v3, 0x7f0a0836

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setHint(I)V

    .line 91
    iget-object v0, p0, Ldyy;->Q:Landroid/widget/TextView;

    new-instance v3, Ldyz;

    invoke-direct {v3, p0}, Ldyz;-><init>(Ldyy;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 98
    const v0, 0x7f1001f4

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Ldyy;->R:Landroid/widget/CheckBox;

    .line 100
    const v0, 0x7f1001f3

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v3, Ldza;

    invoke-direct {v3, p0}, Ldza;-><init>(Ldyy;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    if-eqz p1, :cond_1

    .line 109
    iget-object v0, p0, Ldyy;->Q:Landroid/widget/TextView;

    const-string v3, "name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Ldyy;->R:Landroid/widget/CheckBox;

    const-string v3, "just_following"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 117
    :cond_0
    :goto_0
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 119
    invoke-direct {p0}, Ldyy;->W()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a0835

    :goto_1
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 122
    const v0, 0x7f0a0596

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 123
    const v0, 0x7f0a0597

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 125
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 111
    :cond_1
    invoke-direct {p0}, Ldyy;->W()Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p0}, Ldyy;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 113
    iget-object v3, p0, Ldyy;->Q:Landroid/widget/TextView;

    const-string v4, "name"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v3, p0, Ldyy;->R:Landroid/widget/CheckBox;

    const-string v4, "just_following"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 119
    :cond_2
    const v0, 0x7f0a0833

    goto :goto_1
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 133
    const-string v0, "name"

    iget-object v1, p0, Ldyy;->Q:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 134
    const-string v0, "just_following"

    iget-object v1, p0, Ldyy;->R:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 135
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 161
    invoke-super {p0}, Llgr;->g()V

    .line 162
    invoke-virtual {p0}, Ldyy;->V()V

    .line 163
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 140
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 141
    invoke-virtual {p0}, Ldyy;->u_()Lu;

    move-result-object v0

    check-cast v0, Ldzb;

    .line 142
    if-nez v0, :cond_0

    .line 143
    invoke-virtual {p0}, Ldyy;->n()Lz;

    move-result-object v0

    check-cast v0, Ldzb;

    .line 146
    :cond_0
    invoke-direct {p0}, Ldyy;->W()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    .line 147
    :goto_0
    iget-object v2, p0, Ldyy;->Q:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Ldyy;->R:Landroid/widget/CheckBox;

    .line 148
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    .line 147
    :goto_1
    invoke-interface {v0, v1, v3, v2}, Ldzb;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 151
    :cond_1
    iget-object v0, p0, Ldyy;->Q:Landroid/widget/TextView;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 153
    invoke-super {p0, p1, p2}, Llgr;->onClick(Landroid/content/DialogInterface;I)V

    .line 154
    return-void

    .line 146
    :cond_2
    invoke-virtual {p0}, Ldyy;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "circle_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 148
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 184
    invoke-virtual {p0}, Ldyy;->V()V

    .line 185
    return-void
.end method
