.class public final Lnok;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnok;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:[Lnnx;

.field private m:I

.field private n:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lnok;

    sput-object v0, Lnok;->a:[Lnok;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 25
    sget-object v0, Lnnx;->a:[Lnnx;

    iput-object v0, p0, Lnok;->l:[Lnnx;

    .line 32
    const/high16 v0, -0x80000000

    iput v0, p0, Lnok;->m:I

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 92
    .line 93
    iget-object v0, p0, Lnok;->b:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 94
    const/4 v0, 0x1

    iget-object v2, p0, Lnok;->b:Ljava/lang/String;

    .line 95
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 97
    :goto_0
    iget-object v2, p0, Lnok;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 98
    const/4 v2, 0x2

    iget-object v3, p0, Lnok;->d:Ljava/lang/String;

    .line 99
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 101
    :cond_0
    iget-object v2, p0, Lnok;->k:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 102
    const/4 v2, 0x3

    iget-object v3, p0, Lnok;->k:Ljava/lang/String;

    .line 103
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 105
    :cond_1
    iget-object v2, p0, Lnok;->e:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 106
    const/4 v2, 0x4

    iget-object v3, p0, Lnok;->e:Ljava/lang/String;

    .line 107
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 109
    :cond_2
    iget-object v2, p0, Lnok;->f:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    .line 110
    const/4 v2, 0x5

    iget-object v3, p0, Lnok;->f:Ljava/lang/Boolean;

    .line 111
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 113
    :cond_3
    iget-object v2, p0, Lnok;->l:[Lnnx;

    if-eqz v2, :cond_5

    .line 114
    iget-object v2, p0, Lnok;->l:[Lnnx;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 115
    if-eqz v4, :cond_4

    .line 116
    const/4 v5, 0x6

    .line 117
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 114
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 121
    :cond_5
    iget-object v1, p0, Lnok;->c:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 122
    const/4 v1, 0x7

    iget-object v2, p0, Lnok;->c:Ljava/lang/String;

    .line 123
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    :cond_6
    iget-object v1, p0, Lnok;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 126
    const/16 v1, 0x8

    iget-object v2, p0, Lnok;->g:Ljava/lang/String;

    .line 127
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_7
    iget-object v1, p0, Lnok;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 130
    const/16 v1, 0x9

    iget-object v2, p0, Lnok;->h:Ljava/lang/Boolean;

    .line 131
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 133
    :cond_8
    iget v1, p0, Lnok;->m:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_9

    .line 134
    const/16 v1, 0xa

    iget v2, p0, Lnok;->m:I

    .line 135
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_9
    iget-object v1, p0, Lnok;->i:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 138
    const/16 v1, 0xb

    iget-object v2, p0, Lnok;->i:Ljava/lang/String;

    .line 139
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_a
    iget-object v1, p0, Lnok;->j:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 142
    const/16 v1, 0xc

    iget-object v2, p0, Lnok;->j:Ljava/lang/String;

    .line 143
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_b
    iget-object v1, p0, Lnok;->n:Ljava/lang/Long;

    if-eqz v1, :cond_c

    .line 146
    const/16 v1, 0xd

    iget-object v2, p0, Lnok;->n:Ljava/lang/Long;

    .line 147
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_c
    iget-object v1, p0, Lnok;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 150
    iput v0, p0, Lnok;->ai:I

    .line 151
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lnok;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 159
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 160
    sparse-switch v0, :sswitch_data_0

    .line 164
    iget-object v2, p0, Lnok;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 165
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnok;->ah:Ljava/util/List;

    .line 168
    :cond_1
    iget-object v2, p0, Lnok;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    :sswitch_0
    return-object p0

    .line 175
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnok;->b:Ljava/lang/String;

    goto :goto_0

    .line 179
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnok;->d:Ljava/lang/String;

    goto :goto_0

    .line 183
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnok;->k:Ljava/lang/String;

    goto :goto_0

    .line 187
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnok;->e:Ljava/lang/String;

    goto :goto_0

    .line 191
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnok;->f:Ljava/lang/Boolean;

    goto :goto_0

    .line 195
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 196
    iget-object v0, p0, Lnok;->l:[Lnnx;

    if-nez v0, :cond_3

    move v0, v1

    .line 197
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnnx;

    .line 198
    iget-object v3, p0, Lnok;->l:[Lnnx;

    if-eqz v3, :cond_2

    .line 199
    iget-object v3, p0, Lnok;->l:[Lnnx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 201
    :cond_2
    iput-object v2, p0, Lnok;->l:[Lnnx;

    .line 202
    :goto_2
    iget-object v2, p0, Lnok;->l:[Lnnx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 203
    iget-object v2, p0, Lnok;->l:[Lnnx;

    new-instance v3, Lnnx;

    invoke-direct {v3}, Lnnx;-><init>()V

    aput-object v3, v2, v0

    .line 204
    iget-object v2, p0, Lnok;->l:[Lnnx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 205
    invoke-virtual {p1}, Loxn;->a()I

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 196
    :cond_3
    iget-object v0, p0, Lnok;->l:[Lnnx;

    array-length v0, v0

    goto :goto_1

    .line 208
    :cond_4
    iget-object v2, p0, Lnok;->l:[Lnnx;

    new-instance v3, Lnnx;

    invoke-direct {v3}, Lnnx;-><init>()V

    aput-object v3, v2, v0

    .line 209
    iget-object v2, p0, Lnok;->l:[Lnnx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 213
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnok;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 217
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnok;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 221
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnok;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 225
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 226
    if-eqz v0, :cond_5

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_5

    const/4 v2, 0x3

    if-eq v0, v2, :cond_5

    const/4 v2, 0x4

    if-eq v0, v2, :cond_5

    const/4 v2, 0x5

    if-eq v0, v2, :cond_5

    const/4 v2, 0x6

    if-eq v0, v2, :cond_5

    const/4 v2, 0x7

    if-eq v0, v2, :cond_5

    const/16 v2, 0x8

    if-ne v0, v2, :cond_6

    .line 235
    :cond_5
    iput v0, p0, Lnok;->m:I

    goto/16 :goto_0

    .line 237
    :cond_6
    iput v1, p0, Lnok;->m:I

    goto/16 :goto_0

    .line 242
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnok;->i:Ljava/lang/String;

    goto/16 :goto_0

    .line 246
    :sswitch_c
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnok;->j:Ljava/lang/String;

    goto/16 :goto_0

    .line 250
    :sswitch_d
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnok;->n:Ljava/lang/Long;

    goto/16 :goto_0

    .line 160
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 43
    iget-object v0, p0, Lnok;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x1

    iget-object v1, p0, Lnok;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    :cond_0
    iget-object v0, p0, Lnok;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 47
    const/4 v0, 0x2

    iget-object v1, p0, Lnok;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 49
    :cond_1
    iget-object v0, p0, Lnok;->k:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 50
    const/4 v0, 0x3

    iget-object v1, p0, Lnok;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 52
    :cond_2
    iget-object v0, p0, Lnok;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 53
    const/4 v0, 0x4

    iget-object v1, p0, Lnok;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 55
    :cond_3
    iget-object v0, p0, Lnok;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 56
    const/4 v0, 0x5

    iget-object v1, p0, Lnok;->f:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 58
    :cond_4
    iget-object v0, p0, Lnok;->l:[Lnnx;

    if-eqz v0, :cond_6

    .line 59
    iget-object v1, p0, Lnok;->l:[Lnnx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 60
    if-eqz v3, :cond_5

    .line 61
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 59
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_6
    iget-object v0, p0, Lnok;->c:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 66
    const/4 v0, 0x7

    iget-object v1, p0, Lnok;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 68
    :cond_7
    iget-object v0, p0, Lnok;->g:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 69
    const/16 v0, 0x8

    iget-object v1, p0, Lnok;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 71
    :cond_8
    iget-object v0, p0, Lnok;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 72
    const/16 v0, 0x9

    iget-object v1, p0, Lnok;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 74
    :cond_9
    iget v0, p0, Lnok;->m:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_a

    .line 75
    const/16 v0, 0xa

    iget v1, p0, Lnok;->m:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 77
    :cond_a
    iget-object v0, p0, Lnok;->i:Ljava/lang/String;

    if-eqz v0, :cond_b

    .line 78
    const/16 v0, 0xb

    iget-object v1, p0, Lnok;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 80
    :cond_b
    iget-object v0, p0, Lnok;->j:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 81
    const/16 v0, 0xc

    iget-object v1, p0, Lnok;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 83
    :cond_c
    iget-object v0, p0, Lnok;->n:Ljava/lang/Long;

    if-eqz v0, :cond_d

    .line 84
    const/16 v0, 0xd

    iget-object v1, p0, Lnok;->n:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 86
    :cond_d
    iget-object v0, p0, Lnok;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 88
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lnok;->a(Loxn;)Lnok;

    move-result-object v0

    return-object v0
.end method
