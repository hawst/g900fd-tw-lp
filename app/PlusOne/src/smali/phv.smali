.class public final Lphv;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lphw;

.field private b:Ljava/lang/String;

.field private c:[Lphw;

.field private d:Ljava/lang/Boolean;

.field private e:Lpnf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Loxq;-><init>()V

    .line 92
    sget-object v0, Lphw;->a:[Lphw;

    iput-object v0, p0, Lphv;->a:[Lphw;

    .line 95
    sget-object v0, Lphw;->a:[Lphw;

    iput-object v0, p0, Lphv;->c:[Lphw;

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lphv;->e:Lpnf;

    .line 14
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 134
    .line 135
    iget-object v0, p0, Lphv;->b:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 136
    const/4 v0, 0x1

    iget-object v2, p0, Lphv;->b:Ljava/lang/String;

    .line 137
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 139
    :goto_0
    iget-object v2, p0, Lphv;->a:[Lphw;

    if-eqz v2, :cond_1

    .line 140
    iget-object v3, p0, Lphv;->a:[Lphw;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 141
    if-eqz v5, :cond_0

    .line 142
    const/4 v6, 0x2

    .line 143
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 140
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 147
    :cond_1
    iget-object v2, p0, Lphv;->c:[Lphw;

    if-eqz v2, :cond_3

    .line 148
    iget-object v2, p0, Lphv;->c:[Lphw;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 149
    if-eqz v4, :cond_2

    .line 150
    const/4 v5, 0x3

    .line 151
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 148
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 155
    :cond_3
    iget-object v1, p0, Lphv;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 156
    const/4 v1, 0x4

    iget-object v2, p0, Lphv;->d:Ljava/lang/Boolean;

    .line 157
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 159
    :cond_4
    iget-object v1, p0, Lphv;->e:Lpnf;

    if-eqz v1, :cond_5

    .line 160
    const/4 v1, 0x5

    iget-object v2, p0, Lphv;->e:Lpnf;

    .line 161
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_5
    iget-object v1, p0, Lphv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    iput v0, p0, Lphv;->ai:I

    .line 165
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lphv;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 173
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 174
    sparse-switch v0, :sswitch_data_0

    .line 178
    iget-object v2, p0, Lphv;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 179
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lphv;->ah:Ljava/util/List;

    .line 182
    :cond_1
    iget-object v2, p0, Lphv;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    :sswitch_0
    return-object p0

    .line 189
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lphv;->b:Ljava/lang/String;

    goto :goto_0

    .line 193
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 194
    iget-object v0, p0, Lphv;->a:[Lphw;

    if-nez v0, :cond_3

    move v0, v1

    .line 195
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lphw;

    .line 196
    iget-object v3, p0, Lphv;->a:[Lphw;

    if-eqz v3, :cond_2

    .line 197
    iget-object v3, p0, Lphv;->a:[Lphw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 199
    :cond_2
    iput-object v2, p0, Lphv;->a:[Lphw;

    .line 200
    :goto_2
    iget-object v2, p0, Lphv;->a:[Lphw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 201
    iget-object v2, p0, Lphv;->a:[Lphw;

    new-instance v3, Lphw;

    invoke-direct {v3}, Lphw;-><init>()V

    aput-object v3, v2, v0

    .line 202
    iget-object v2, p0, Lphv;->a:[Lphw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 203
    invoke-virtual {p1}, Loxn;->a()I

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 194
    :cond_3
    iget-object v0, p0, Lphv;->a:[Lphw;

    array-length v0, v0

    goto :goto_1

    .line 206
    :cond_4
    iget-object v2, p0, Lphv;->a:[Lphw;

    new-instance v3, Lphw;

    invoke-direct {v3}, Lphw;-><init>()V

    aput-object v3, v2, v0

    .line 207
    iget-object v2, p0, Lphv;->a:[Lphw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 211
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 212
    iget-object v0, p0, Lphv;->c:[Lphw;

    if-nez v0, :cond_6

    move v0, v1

    .line 213
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lphw;

    .line 214
    iget-object v3, p0, Lphv;->c:[Lphw;

    if-eqz v3, :cond_5

    .line 215
    iget-object v3, p0, Lphv;->c:[Lphw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 217
    :cond_5
    iput-object v2, p0, Lphv;->c:[Lphw;

    .line 218
    :goto_4
    iget-object v2, p0, Lphv;->c:[Lphw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 219
    iget-object v2, p0, Lphv;->c:[Lphw;

    new-instance v3, Lphw;

    invoke-direct {v3}, Lphw;-><init>()V

    aput-object v3, v2, v0

    .line 220
    iget-object v2, p0, Lphv;->c:[Lphw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 221
    invoke-virtual {p1}, Loxn;->a()I

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 212
    :cond_6
    iget-object v0, p0, Lphv;->c:[Lphw;

    array-length v0, v0

    goto :goto_3

    .line 224
    :cond_7
    iget-object v2, p0, Lphv;->c:[Lphw;

    new-instance v3, Lphw;

    invoke-direct {v3}, Lphw;-><init>()V

    aput-object v3, v2, v0

    .line 225
    iget-object v2, p0, Lphv;->c:[Lphw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 229
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lphv;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 233
    :sswitch_5
    iget-object v0, p0, Lphv;->e:Lpnf;

    if-nez v0, :cond_8

    .line 234
    new-instance v0, Lpnf;

    invoke-direct {v0}, Lpnf;-><init>()V

    iput-object v0, p0, Lphv;->e:Lpnf;

    .line 236
    :cond_8
    iget-object v0, p0, Lphv;->e:Lpnf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 174
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 105
    iget-object v1, p0, Lphv;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 106
    const/4 v1, 0x1

    iget-object v2, p0, Lphv;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 108
    :cond_0
    iget-object v1, p0, Lphv;->a:[Lphw;

    if-eqz v1, :cond_2

    .line 109
    iget-object v2, p0, Lphv;->a:[Lphw;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 110
    if-eqz v4, :cond_1

    .line 111
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 109
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    :cond_2
    iget-object v1, p0, Lphv;->c:[Lphw;

    if-eqz v1, :cond_4

    .line 116
    iget-object v1, p0, Lphv;->c:[Lphw;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 117
    if-eqz v3, :cond_3

    .line 118
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 116
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 122
    :cond_4
    iget-object v0, p0, Lphv;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 123
    const/4 v0, 0x4

    iget-object v1, p0, Lphv;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 125
    :cond_5
    iget-object v0, p0, Lphv;->e:Lpnf;

    if-eqz v0, :cond_6

    .line 126
    const/4 v0, 0x5

    iget-object v1, p0, Lphv;->e:Lpnf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 128
    :cond_6
    iget-object v0, p0, Lphv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 130
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lphv;->a(Loxn;)Lphv;

    move-result-object v0

    return-object v0
.end method
