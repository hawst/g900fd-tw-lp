.class public Llgl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/WindowManager;

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Llgq;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/os/Handler;

.field private final e:Landroid/view/WindowManager$LayoutParams;

.field private final f:Ljava/lang/Runnable;

.field private final g:Landroid/view/View$OnTouchListener;

.field private final h:Llgj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Llgl;->c:Ljava/util/Set;

    .line 50
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    .line 55
    new-instance v0, Llgm;

    invoke-direct {v0, p0}, Llgm;-><init>(Llgl;)V

    iput-object v0, p0, Llgl;->f:Ljava/lang/Runnable;

    .line 62
    new-instance v0, Llgn;

    invoke-direct {v0, p0}, Llgn;-><init>(Llgl;)V

    iput-object v0, p0, Llgl;->g:Landroid/view/View$OnTouchListener;

    .line 71
    new-instance v0, Llgo;

    invoke-direct {v0, p0}, Llgo;-><init>(Llgl;)V

    iput-object v0, p0, Llgl;->h:Llgj;

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Llgl;->d:Landroid/os/Handler;

    .line 123
    iget-object v0, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 125
    iget-object v0, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 126
    iget-object v0, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 127
    iget-object v1, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-ge v0, v2, :cond_0

    const/16 v0, 0x3eb

    :goto_0
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 129
    iget-object v0, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    const-string v1, "ActionableToast"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x50

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 135
    iget-object v1, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    const v2, 0x7f0d010b

    .line 136
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 138
    iget-object v0, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x40028

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 140
    const-string v0, "window"

    .line 141
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Llgl;->b:Landroid/view/WindowManager;

    .line 142
    return-void

    .line 127
    :cond_0
    const/16 v0, 0x7d5

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Llgl;->a(Z)V

    .line 184
    return-void
.end method

.method public a(Llgf;I)V
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Llgl;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {p0}, Llgl;->a()V

    .line 160
    :cond_0
    invoke-virtual {p1}, Llgf;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Llgl;->a:Landroid/view/View;

    .line 161
    iget-object v0, p0, Llgl;->a:Landroid/view/View;

    iget-object v1, p0, Llgl;->g:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Llgl;->a:Landroid/view/View;

    const v1, 0x7f1001d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Llgl;->g:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 166
    iget-object v0, p0, Llgl;->h:Llgj;

    invoke-virtual {p1, v0}, Llgf;->a(Llgj;)V

    .line 169
    iget-object v0, p0, Llgl;->d:Landroid/os/Handler;

    iget-object v1, p0, Llgl;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 170
    iget-object v0, p0, Llgl;->d:Landroid/os/Handler;

    iget-object v1, p0, Llgl;->f:Ljava/lang/Runnable;

    int-to-long v2, p2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 174
    iget-object v0, p0, Llgl;->a:Landroid/view/View;

    iget-object v1, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    iget-object v0, p0, Llgl;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Llgl;->a:Landroid/view/View;

    iget-object v2, p0, Llgl;->e:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 177
    return-void
.end method

.method public a(Llgq;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Llgl;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Llgl;->d:Landroid/os/Handler;

    iget-object v1, p0, Llgl;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 188
    iget-object v0, p0, Llgl;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Llgl;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Llgl;->b:Landroid/view/WindowManager;

    iget-object v1, p0, Llgl;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 197
    const/4 v0, 0x0

    iput-object v0, p0, Llgl;->a:Landroid/view/View;

    .line 198
    if-eqz p1, :cond_1

    .line 201
    iget-object v0, p0, Llgl;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llgq;

    .line 202
    invoke-interface {v0}, Llgq;->d()V

    goto :goto_0

    .line 195
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The rootView must have a parent!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_1
    return-void
.end method

.method public b(Llgq;)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Llgl;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 101
    return-void
.end method
