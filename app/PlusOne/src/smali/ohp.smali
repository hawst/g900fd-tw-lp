.class public final Lohp;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lohp;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    new-array v0, v0, [Lohp;

    sput-object v0, Lohp;->a:[Lohp;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 274
    const/4 v0, 0x0

    .line 275
    iget-object v1, p0, Lohp;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 276
    const/4 v0, 0x1

    iget-object v1, p0, Lohp;->b:Ljava/lang/String;

    .line 277
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 279
    :cond_0
    iget-object v1, p0, Lohp;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 280
    const/4 v1, 0x2

    iget-object v2, p0, Lohp;->c:Ljava/lang/String;

    .line 281
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 283
    :cond_1
    iget-object v1, p0, Lohp;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 284
    const/4 v1, 0x3

    iget-object v2, p0, Lohp;->d:Ljava/lang/String;

    .line 285
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 287
    :cond_2
    iget-object v1, p0, Lohp;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 288
    const/4 v1, 0x4

    iget-object v2, p0, Lohp;->e:Ljava/lang/String;

    .line 289
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_3
    iget-object v1, p0, Lohp;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 292
    const/4 v1, 0x5

    iget-object v2, p0, Lohp;->f:Ljava/lang/String;

    .line 293
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_4
    iget-object v1, p0, Lohp;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 296
    const/4 v1, 0x6

    iget-object v2, p0, Lohp;->g:Ljava/lang/String;

    .line 297
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 299
    :cond_5
    iget-object v1, p0, Lohp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 300
    iput v0, p0, Lohp;->ai:I

    .line 301
    return v0
.end method

.method public a(Loxn;)Lohp;
    .locals 2

    .prologue
    .line 309
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 310
    sparse-switch v0, :sswitch_data_0

    .line 314
    iget-object v1, p0, Lohp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 315
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohp;->ah:Ljava/util/List;

    .line 318
    :cond_1
    iget-object v1, p0, Lohp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    :sswitch_0
    return-object p0

    .line 325
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohp;->b:Ljava/lang/String;

    goto :goto_0

    .line 329
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohp;->c:Ljava/lang/String;

    goto :goto_0

    .line 333
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohp;->d:Ljava/lang/String;

    goto :goto_0

    .line 337
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohp;->e:Ljava/lang/String;

    goto :goto_0

    .line 341
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohp;->f:Ljava/lang/String;

    goto :goto_0

    .line 345
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lohp;->g:Ljava/lang/String;

    goto :goto_0

    .line 310
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lohp;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 251
    const/4 v0, 0x1

    iget-object v1, p0, Lohp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 253
    :cond_0
    iget-object v0, p0, Lohp;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 254
    const/4 v0, 0x2

    iget-object v1, p0, Lohp;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 256
    :cond_1
    iget-object v0, p0, Lohp;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 257
    const/4 v0, 0x3

    iget-object v1, p0, Lohp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 259
    :cond_2
    iget-object v0, p0, Lohp;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 260
    const/4 v0, 0x4

    iget-object v1, p0, Lohp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 262
    :cond_3
    iget-object v0, p0, Lohp;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 263
    const/4 v0, 0x5

    iget-object v1, p0, Lohp;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 265
    :cond_4
    iget-object v0, p0, Lohp;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 266
    const/4 v0, 0x6

    iget-object v1, p0, Lohp;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 268
    :cond_5
    iget-object v0, p0, Lohp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 270
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0, p1}, Lohp;->a(Loxn;)Lohp;

    move-result-object v0

    return-object v0
.end method
