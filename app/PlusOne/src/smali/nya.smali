.class public final Lnya;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnya;


# instance fields
.field private b:I

.field private c:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5561
    const/4 v0, 0x0

    new-array v0, v0, [Lnya;

    sput-object v0, Lnya;->a:[Lnya;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5562
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5569
    const/high16 v0, -0x80000000

    iput v0, p0, Lnya;->b:I

    .line 5562
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 5586
    const/4 v0, 0x1

    iget v1, p0, Lnya;->b:I

    .line 5588
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5589
    iget-object v1, p0, Lnya;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 5590
    const/4 v1, 0x2

    iget-object v2, p0, Lnya;->c:Ljava/lang/Integer;

    .line 5591
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5593
    :cond_0
    iget-object v1, p0, Lnya;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5594
    iput v0, p0, Lnya;->ai:I

    .line 5595
    return v0
.end method

.method public a(Loxn;)Lnya;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5603
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5604
    sparse-switch v0, :sswitch_data_0

    .line 5608
    iget-object v1, p0, Lnya;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5609
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnya;->ah:Ljava/util/List;

    .line 5612
    :cond_1
    iget-object v1, p0, Lnya;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5614
    :sswitch_0
    return-object p0

    .line 5619
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5620
    if-ne v0, v2, :cond_2

    .line 5621
    iput v0, p0, Lnya;->b:I

    goto :goto_0

    .line 5623
    :cond_2
    iput v2, p0, Lnya;->b:I

    goto :goto_0

    .line 5628
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnya;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 5604
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 5576
    const/4 v0, 0x1

    iget v1, p0, Lnya;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5577
    iget-object v0, p0, Lnya;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5578
    const/4 v0, 0x2

    iget-object v1, p0, Lnya;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5580
    :cond_0
    iget-object v0, p0, Lnya;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5582
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5558
    invoke-virtual {p0, p1}, Lnya;->a(Loxn;)Lnya;

    move-result-object v0

    return-object v0
.end method
