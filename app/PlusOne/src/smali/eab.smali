.class public final Leab;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/fragments/EditEventFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V
    .locals 0

    .prologue
    .line 1343
    iput-object p1, p0, Leab;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1346
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1350
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1354
    iget-object v0, p0, Leab;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->i(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1355
    iget-object v1, p0, Leab;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->j(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lidh;

    move-result-object v1

    invoke-virtual {v1}, Lidh;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1356
    iget-object v1, p0, Leab;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->j(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lidh;

    move-result-object v1

    invoke-virtual {v1, v0}, Lidh;->a(Ljava/lang/String;)V

    .line 1357
    iget-object v0, p0, Leab;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(Lcom/google/android/apps/plus/fragments/EditEventFragment;Z)Z

    .line 1359
    iget-object v0, p0, Leab;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->k(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Leae;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1360
    iget-object v0, p0, Leab;->a:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->k(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Leae;

    .line 1363
    :cond_0
    return-void
.end method
