.class public final Lmqk;
.super Loxq;
.source "PG"


# instance fields
.field public a:Loya;

.field public b:Logr;

.field public c:Ljava/lang/String;

.field public d:[Lmqn;

.field public e:[Loax;

.field public f:[Lofa;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field private j:Lpaz;

.field private k:Logi;

.field private l:I

.field private m:I

.field private n:[Lmql;

.field private o:Lmqm;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    const/4 v1, 0x0

    .line 939
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1237
    iput-object v1, p0, Lmqk;->j:Lpaz;

    .line 1240
    iput-object v1, p0, Lmqk;->a:Loya;

    .line 1243
    iput-object v1, p0, Lmqk;->b:Logr;

    .line 1248
    sget-object v0, Lmqn;->a:[Lmqn;

    iput-object v0, p0, Lmqk;->d:[Lmqn;

    .line 1251
    sget-object v0, Loax;->a:[Loax;

    iput-object v0, p0, Lmqk;->e:[Loax;

    .line 1254
    sget-object v0, Lofa;->a:[Lofa;

    iput-object v0, p0, Lmqk;->f:[Lofa;

    .line 1257
    iput-object v1, p0, Lmqk;->k:Logi;

    .line 1264
    iput v2, p0, Lmqk;->l:I

    .line 1267
    iput v2, p0, Lmqk;->m:I

    .line 1270
    iput v2, p0, Lmqk;->i:I

    .line 1273
    sget-object v0, Lmql;->a:[Lmql;

    iput-object v0, p0, Lmqk;->n:[Lmql;

    .line 1276
    iput-object v1, p0, Lmqk;->o:Lmqm;

    .line 939
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v1, 0x0

    .line 1348
    .line 1349
    iget-object v0, p0, Lmqk;->j:Lpaz;

    if-eqz v0, :cond_12

    .line 1350
    const/4 v0, 0x1

    iget-object v2, p0, Lmqk;->j:Lpaz;

    .line 1351
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1353
    :goto_0
    iget-object v2, p0, Lmqk;->b:Logr;

    if-eqz v2, :cond_0

    .line 1354
    const/4 v2, 0x2

    iget-object v3, p0, Lmqk;->b:Logr;

    .line 1355
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1357
    :cond_0
    iget-object v2, p0, Lmqk;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 1358
    const/4 v2, 0x3

    iget-object v3, p0, Lmqk;->c:Ljava/lang/String;

    .line 1359
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1361
    :cond_1
    iget-object v2, p0, Lmqk;->d:[Lmqn;

    if-eqz v2, :cond_3

    .line 1362
    iget-object v3, p0, Lmqk;->d:[Lmqn;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 1363
    if-eqz v5, :cond_2

    .line 1364
    const/4 v6, 0x4

    .line 1365
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1362
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1369
    :cond_3
    iget-object v2, p0, Lmqk;->e:[Loax;

    if-eqz v2, :cond_5

    .line 1370
    iget-object v3, p0, Lmqk;->e:[Loax;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    .line 1371
    if-eqz v5, :cond_4

    .line 1372
    const/4 v6, 0x5

    .line 1373
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1370
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1377
    :cond_5
    iget-object v2, p0, Lmqk;->f:[Lofa;

    if-eqz v2, :cond_7

    .line 1378
    iget-object v3, p0, Lmqk;->f:[Lofa;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 1379
    if-eqz v5, :cond_6

    .line 1380
    const/4 v6, 0x6

    .line 1381
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1378
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1385
    :cond_7
    iget-object v2, p0, Lmqk;->k:Logi;

    if-eqz v2, :cond_8

    .line 1386
    const/4 v2, 0x7

    iget-object v3, p0, Lmqk;->k:Logi;

    .line 1387
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1389
    :cond_8
    iget-object v2, p0, Lmqk;->g:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 1390
    const/16 v2, 0x8

    iget-object v3, p0, Lmqk;->g:Ljava/lang/String;

    .line 1391
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1393
    :cond_9
    iget-object v2, p0, Lmqk;->h:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 1394
    const/16 v2, 0x9

    iget-object v3, p0, Lmqk;->h:Ljava/lang/String;

    .line 1395
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1397
    :cond_a
    iget v2, p0, Lmqk;->l:I

    if-eq v2, v7, :cond_b

    .line 1398
    const/16 v2, 0xa

    iget v3, p0, Lmqk;->l:I

    .line 1399
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1401
    :cond_b
    iget v2, p0, Lmqk;->m:I

    if-eq v2, v7, :cond_c

    .line 1402
    const/16 v2, 0xb

    iget v3, p0, Lmqk;->m:I

    .line 1403
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1405
    :cond_c
    iget v2, p0, Lmqk;->i:I

    if-eq v2, v7, :cond_d

    .line 1406
    const/16 v2, 0xc

    iget v3, p0, Lmqk;->i:I

    .line 1407
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1409
    :cond_d
    iget-object v2, p0, Lmqk;->n:[Lmql;

    if-eqz v2, :cond_f

    .line 1410
    iget-object v2, p0, Lmqk;->n:[Lmql;

    array-length v3, v2

    :goto_4
    if-ge v1, v3, :cond_f

    aget-object v4, v2, v1

    .line 1411
    if-eqz v4, :cond_e

    .line 1412
    const/16 v5, 0xd

    .line 1413
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1410
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1417
    :cond_f
    iget-object v1, p0, Lmqk;->a:Loya;

    if-eqz v1, :cond_10

    .line 1418
    const/16 v1, 0xe

    iget-object v2, p0, Lmqk;->a:Loya;

    .line 1419
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1421
    :cond_10
    iget-object v1, p0, Lmqk;->o:Lmqm;

    if-eqz v1, :cond_11

    .line 1422
    const/16 v1, 0xf

    iget-object v2, p0, Lmqk;->o:Lmqm;

    .line 1423
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1425
    :cond_11
    iget-object v1, p0, Lmqk;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1426
    iput v0, p0, Lmqk;->ai:I

    .line 1427
    return v0

    :cond_12
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lmqk;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1435
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1436
    sparse-switch v0, :sswitch_data_0

    .line 1440
    iget-object v2, p0, Lmqk;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1441
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmqk;->ah:Ljava/util/List;

    .line 1444
    :cond_1
    iget-object v2, p0, Lmqk;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1446
    :sswitch_0
    return-object p0

    .line 1451
    :sswitch_1
    iget-object v0, p0, Lmqk;->j:Lpaz;

    if-nez v0, :cond_2

    .line 1452
    new-instance v0, Lpaz;

    invoke-direct {v0}, Lpaz;-><init>()V

    iput-object v0, p0, Lmqk;->j:Lpaz;

    .line 1454
    :cond_2
    iget-object v0, p0, Lmqk;->j:Lpaz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1458
    :sswitch_2
    iget-object v0, p0, Lmqk;->b:Logr;

    if-nez v0, :cond_3

    .line 1459
    new-instance v0, Logr;

    invoke-direct {v0}, Logr;-><init>()V

    iput-object v0, p0, Lmqk;->b:Logr;

    .line 1461
    :cond_3
    iget-object v0, p0, Lmqk;->b:Logr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1465
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqk;->c:Ljava/lang/String;

    goto :goto_0

    .line 1469
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1470
    iget-object v0, p0, Lmqk;->d:[Lmqn;

    if-nez v0, :cond_5

    move v0, v1

    .line 1471
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmqn;

    .line 1472
    iget-object v3, p0, Lmqk;->d:[Lmqn;

    if-eqz v3, :cond_4

    .line 1473
    iget-object v3, p0, Lmqk;->d:[Lmqn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1475
    :cond_4
    iput-object v2, p0, Lmqk;->d:[Lmqn;

    .line 1476
    :goto_2
    iget-object v2, p0, Lmqk;->d:[Lmqn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 1477
    iget-object v2, p0, Lmqk;->d:[Lmqn;

    new-instance v3, Lmqn;

    invoke-direct {v3}, Lmqn;-><init>()V

    aput-object v3, v2, v0

    .line 1478
    iget-object v2, p0, Lmqk;->d:[Lmqn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1479
    invoke-virtual {p1}, Loxn;->a()I

    .line 1476
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1470
    :cond_5
    iget-object v0, p0, Lmqk;->d:[Lmqn;

    array-length v0, v0

    goto :goto_1

    .line 1482
    :cond_6
    iget-object v2, p0, Lmqk;->d:[Lmqn;

    new-instance v3, Lmqn;

    invoke-direct {v3}, Lmqn;-><init>()V

    aput-object v3, v2, v0

    .line 1483
    iget-object v2, p0, Lmqk;->d:[Lmqn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1487
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1488
    iget-object v0, p0, Lmqk;->e:[Loax;

    if-nez v0, :cond_8

    move v0, v1

    .line 1489
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loax;

    .line 1490
    iget-object v3, p0, Lmqk;->e:[Loax;

    if-eqz v3, :cond_7

    .line 1491
    iget-object v3, p0, Lmqk;->e:[Loax;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1493
    :cond_7
    iput-object v2, p0, Lmqk;->e:[Loax;

    .line 1494
    :goto_4
    iget-object v2, p0, Lmqk;->e:[Loax;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 1495
    iget-object v2, p0, Lmqk;->e:[Loax;

    new-instance v3, Loax;

    invoke-direct {v3}, Loax;-><init>()V

    aput-object v3, v2, v0

    .line 1496
    iget-object v2, p0, Lmqk;->e:[Loax;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1497
    invoke-virtual {p1}, Loxn;->a()I

    .line 1494
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1488
    :cond_8
    iget-object v0, p0, Lmqk;->e:[Loax;

    array-length v0, v0

    goto :goto_3

    .line 1500
    :cond_9
    iget-object v2, p0, Lmqk;->e:[Loax;

    new-instance v3, Loax;

    invoke-direct {v3}, Loax;-><init>()V

    aput-object v3, v2, v0

    .line 1501
    iget-object v2, p0, Lmqk;->e:[Loax;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1505
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1506
    iget-object v0, p0, Lmqk;->f:[Lofa;

    if-nez v0, :cond_b

    move v0, v1

    .line 1507
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lofa;

    .line 1508
    iget-object v3, p0, Lmqk;->f:[Lofa;

    if-eqz v3, :cond_a

    .line 1509
    iget-object v3, p0, Lmqk;->f:[Lofa;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1511
    :cond_a
    iput-object v2, p0, Lmqk;->f:[Lofa;

    .line 1512
    :goto_6
    iget-object v2, p0, Lmqk;->f:[Lofa;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    .line 1513
    iget-object v2, p0, Lmqk;->f:[Lofa;

    new-instance v3, Lofa;

    invoke-direct {v3}, Lofa;-><init>()V

    aput-object v3, v2, v0

    .line 1514
    iget-object v2, p0, Lmqk;->f:[Lofa;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1515
    invoke-virtual {p1}, Loxn;->a()I

    .line 1512
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1506
    :cond_b
    iget-object v0, p0, Lmqk;->f:[Lofa;

    array-length v0, v0

    goto :goto_5

    .line 1518
    :cond_c
    iget-object v2, p0, Lmqk;->f:[Lofa;

    new-instance v3, Lofa;

    invoke-direct {v3}, Lofa;-><init>()V

    aput-object v3, v2, v0

    .line 1519
    iget-object v2, p0, Lmqk;->f:[Lofa;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1523
    :sswitch_7
    iget-object v0, p0, Lmqk;->k:Logi;

    if-nez v0, :cond_d

    .line 1524
    new-instance v0, Logi;

    invoke-direct {v0}, Logi;-><init>()V

    iput-object v0, p0, Lmqk;->k:Logi;

    .line 1526
    :cond_d
    iget-object v0, p0, Lmqk;->k:Logi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1530
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqk;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 1534
    :sswitch_9
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmqk;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 1538
    :sswitch_a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1539
    if-eq v0, v4, :cond_e

    if-eq v0, v5, :cond_e

    if-ne v0, v6, :cond_f

    .line 1542
    :cond_e
    iput v0, p0, Lmqk;->l:I

    goto/16 :goto_0

    .line 1544
    :cond_f
    iput v4, p0, Lmqk;->l:I

    goto/16 :goto_0

    .line 1549
    :sswitch_b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1550
    if-eq v0, v4, :cond_10

    if-ne v0, v5, :cond_11

    .line 1552
    :cond_10
    iput v0, p0, Lmqk;->m:I

    goto/16 :goto_0

    .line 1554
    :cond_11
    iput v4, p0, Lmqk;->m:I

    goto/16 :goto_0

    .line 1559
    :sswitch_c
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1560
    if-eq v0, v4, :cond_12

    if-eq v0, v5, :cond_12

    if-eq v0, v6, :cond_12

    const/4 v2, 0x4

    if-eq v0, v2, :cond_12

    const/4 v2, 0x5

    if-ne v0, v2, :cond_13

    .line 1565
    :cond_12
    iput v0, p0, Lmqk;->i:I

    goto/16 :goto_0

    .line 1567
    :cond_13
    iput v4, p0, Lmqk;->i:I

    goto/16 :goto_0

    .line 1572
    :sswitch_d
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1573
    iget-object v0, p0, Lmqk;->n:[Lmql;

    if-nez v0, :cond_15

    move v0, v1

    .line 1574
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lmql;

    .line 1575
    iget-object v3, p0, Lmqk;->n:[Lmql;

    if-eqz v3, :cond_14

    .line 1576
    iget-object v3, p0, Lmqk;->n:[Lmql;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1578
    :cond_14
    iput-object v2, p0, Lmqk;->n:[Lmql;

    .line 1579
    :goto_8
    iget-object v2, p0, Lmqk;->n:[Lmql;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_16

    .line 1580
    iget-object v2, p0, Lmqk;->n:[Lmql;

    new-instance v3, Lmql;

    invoke-direct {v3}, Lmql;-><init>()V

    aput-object v3, v2, v0

    .line 1581
    iget-object v2, p0, Lmqk;->n:[Lmql;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1582
    invoke-virtual {p1}, Loxn;->a()I

    .line 1579
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 1573
    :cond_15
    iget-object v0, p0, Lmqk;->n:[Lmql;

    array-length v0, v0

    goto :goto_7

    .line 1585
    :cond_16
    iget-object v2, p0, Lmqk;->n:[Lmql;

    new-instance v3, Lmql;

    invoke-direct {v3}, Lmql;-><init>()V

    aput-object v3, v2, v0

    .line 1586
    iget-object v2, p0, Lmqk;->n:[Lmql;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1590
    :sswitch_e
    iget-object v0, p0, Lmqk;->a:Loya;

    if-nez v0, :cond_17

    .line 1591
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lmqk;->a:Loya;

    .line 1593
    :cond_17
    iget-object v0, p0, Lmqk;->a:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1597
    :sswitch_f
    iget-object v0, p0, Lmqk;->o:Lmqm;

    if-nez v0, :cond_18

    .line 1598
    new-instance v0, Lmqm;

    invoke-direct {v0}, Lmqm;-><init>()V

    iput-object v0, p0, Lmqk;->o:Lmqm;

    .line 1600
    :cond_18
    iget-object v0, p0, Lmqk;->o:Lmqm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1436
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    const/4 v0, 0x0

    .line 1281
    iget-object v1, p0, Lmqk;->j:Lpaz;

    if-eqz v1, :cond_0

    .line 1282
    const/4 v1, 0x1

    iget-object v2, p0, Lmqk;->j:Lpaz;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1284
    :cond_0
    iget-object v1, p0, Lmqk;->b:Logr;

    if-eqz v1, :cond_1

    .line 1285
    const/4 v1, 0x2

    iget-object v2, p0, Lmqk;->b:Logr;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1287
    :cond_1
    iget-object v1, p0, Lmqk;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1288
    const/4 v1, 0x3

    iget-object v2, p0, Lmqk;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1290
    :cond_2
    iget-object v1, p0, Lmqk;->d:[Lmqn;

    if-eqz v1, :cond_4

    .line 1291
    iget-object v2, p0, Lmqk;->d:[Lmqn;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1292
    if-eqz v4, :cond_3

    .line 1293
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1291
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1297
    :cond_4
    iget-object v1, p0, Lmqk;->e:[Loax;

    if-eqz v1, :cond_6

    .line 1298
    iget-object v2, p0, Lmqk;->e:[Loax;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 1299
    if-eqz v4, :cond_5

    .line 1300
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1298
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1304
    :cond_6
    iget-object v1, p0, Lmqk;->f:[Lofa;

    if-eqz v1, :cond_8

    .line 1305
    iget-object v2, p0, Lmqk;->f:[Lofa;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 1306
    if-eqz v4, :cond_7

    .line 1307
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 1305
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1311
    :cond_8
    iget-object v1, p0, Lmqk;->k:Logi;

    if-eqz v1, :cond_9

    .line 1312
    const/4 v1, 0x7

    iget-object v2, p0, Lmqk;->k:Logi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 1314
    :cond_9
    iget-object v1, p0, Lmqk;->g:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 1315
    const/16 v1, 0x8

    iget-object v2, p0, Lmqk;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1317
    :cond_a
    iget-object v1, p0, Lmqk;->h:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 1318
    const/16 v1, 0x9

    iget-object v2, p0, Lmqk;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 1320
    :cond_b
    iget v1, p0, Lmqk;->l:I

    if-eq v1, v6, :cond_c

    .line 1321
    const/16 v1, 0xa

    iget v2, p0, Lmqk;->l:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1323
    :cond_c
    iget v1, p0, Lmqk;->m:I

    if-eq v1, v6, :cond_d

    .line 1324
    const/16 v1, 0xb

    iget v2, p0, Lmqk;->m:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1326
    :cond_d
    iget v1, p0, Lmqk;->i:I

    if-eq v1, v6, :cond_e

    .line 1327
    const/16 v1, 0xc

    iget v2, p0, Lmqk;->i:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 1329
    :cond_e
    iget-object v1, p0, Lmqk;->n:[Lmql;

    if-eqz v1, :cond_10

    .line 1330
    iget-object v1, p0, Lmqk;->n:[Lmql;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_10

    aget-object v3, v1, v0

    .line 1331
    if-eqz v3, :cond_f

    .line 1332
    const/16 v4, 0xd

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1330
    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1336
    :cond_10
    iget-object v0, p0, Lmqk;->a:Loya;

    if-eqz v0, :cond_11

    .line 1337
    const/16 v0, 0xe

    iget-object v1, p0, Lmqk;->a:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1339
    :cond_11
    iget-object v0, p0, Lmqk;->o:Lmqm;

    if-eqz v0, :cond_12

    .line 1340
    const/16 v0, 0xf

    iget-object v1, p0, Lmqk;->o:Lmqm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1342
    :cond_12
    iget-object v0, p0, Lmqk;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1344
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 935
    invoke-virtual {p0, p1}, Lmqk;->a(Loxn;)Lmqk;

    move-result-object v0

    return-object v0
.end method
