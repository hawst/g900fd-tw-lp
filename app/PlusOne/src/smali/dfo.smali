.class public final Ldfo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ldft;
.implements Llnx;
.implements Llqi;
.implements Llrb;
.implements Llrc;
.implements Llrg;


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/animation/Animation;

.field private c:Landroid/view/animation/Animation;

.field private final d:Ldft;

.field private final e:Lu;

.field private final f:Ldfr;

.field private g:Ldfs;

.field private h:Loo;

.field private i:Z


# direct methods
.method public constructor <init>(Lu;Llqr;Ldfr;Ldft;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldfo;->a:Ljava/util/List;

    .line 66
    iput-object p1, p0, Ldfo;->e:Lu;

    .line 67
    iput-object p3, p0, Ldfo;->f:Ldfr;

    .line 68
    iput-object p4, p0, Ldfo;->d:Ldft;

    .line 70
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 71
    return-void
.end method

.method private a(ZZ)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 144
    iput-boolean p1, p0, Ldfo;->i:Z

    .line 146
    iget-object v0, p0, Ldfo;->e:Lu;

    invoke-virtual {v0}, Lu;->x()Landroid/view/View;

    move-result-object v1

    .line 147
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v0, v2, :cond_0

    if-eqz v1, :cond_0

    .line 148
    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    .line 149
    if-eqz p1, :cond_1

    .line 150
    or-int/lit8 v0, v0, 0x1

    .line 154
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 157
    :cond_0
    iget-object v0, p0, Ldfo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz p2, :cond_3

    iget-boolean v1, p0, Ldfo;->i:Z

    if-eqz v1, :cond_2

    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 152
    :cond_1
    and-int/lit8 v0, v0, -0x2

    goto :goto_0

    .line 157
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    iget-boolean v1, p0, Ldfo;->i:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Ldfo;->c:Landroid/view/animation/Animation;

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Ldfo;->b:Landroid/view/animation/Animation;

    goto :goto_3

    .line 158
    :cond_5
    if-eqz p1, :cond_7

    .line 159
    iget-object v0, p0, Ldfo;->h:Loo;

    if-eqz v0, :cond_6

    .line 160
    iget-object v0, p0, Ldfo;->h:Loo;

    invoke-virtual {v0}, Loo;->f()V

    .line 168
    :cond_6
    :goto_4
    iget-object v0, p0, Ldfo;->d:Ldft;

    invoke-interface {v0, p1}, Ldft;->a(Z)V

    .line 169
    iget-object v0, p0, Ldfo;->g:Ldfs;

    invoke-interface {v0, p1}, Ldfs;->a(Z)V

    .line 170
    return-void

    .line 163
    :cond_7
    iget-object v0, p0, Ldfo;->h:Loo;

    if-eqz v0, :cond_6

    .line 164
    iget-object v0, p0, Ldfo;->h:Loo;

    invoke-virtual {v0}, Loo;->e()V

    goto :goto_4
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Ldfo;->b:Landroid/view/animation/Animation;

    if-nez v0, :cond_0

    .line 81
    const v0, 0x7f050013

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldfo;->b:Landroid/view/animation/Animation;

    .line 82
    const v0, 0x7f050015

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldfo;->c:Landroid/view/animation/Animation;

    .line 84
    iget-object v0, p0, Ldfo;->b:Landroid/view/animation/Animation;

    new-instance v1, Ldfp;

    invoke-direct {v1, p0}, Ldfp;-><init>(Ldfo;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 93
    iget-object v0, p0, Ldfo;->c:Landroid/view/animation/Animation;

    new-instance v1, Ldfq;

    invoke-direct {v1, p0}, Ldfq;-><init>(Ldfo;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 102
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 75
    const-class v0, Ldfs;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfs;

    iput-object v0, p0, Ldfo;->g:Ldfs;

    .line 76
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Ldfo;->i:Z

    if-ne v0, p1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 131
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ldfo;->a(ZZ)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Ldfo;->i:Z

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Ldfo;->e:Lu;

    invoke-virtual {v0}, Lu;->n()Lz;

    move-result-object v0

    instance-of v1, v0, Los;

    if-eqz v1, :cond_1

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Ldfo;->h:Loo;

    .line 109
    iget-object v0, p0, Ldfo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 110
    iget-object v0, p0, Ldfo;->f:Ldfr;

    iget-object v1, p0, Ldfo;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ldfr;->a(Ljava/util/List;)V

    .line 112
    iget-object v0, p0, Ldfo;->g:Ldfs;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Ldfo;->g:Ldfs;

    invoke-interface {v0, p0}, Ldfs;->a(Ldft;)V

    .line 114
    iget-object v0, p0, Ldfo;->g:Ldfs;

    invoke-interface {v0}, Ldfs;->a()Z

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Ldfo;->a(ZZ)V

    .line 116
    :cond_0
    return-void

    .line 106
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Ldfo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 121
    iget-object v0, p0, Ldfo;->g:Ldfs;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Ldfo;->g:Ldfs;

    invoke-interface {v0, p0}, Ldfs;->b(Ldft;)V

    .line 124
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 139
    iget-boolean v0, p0, Ldfo;->i:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0, v1}, Ldfo;->a(ZZ)V

    .line 140
    return-void

    :cond_0
    move v0, v1

    .line 139
    goto :goto_0
.end method
