.class public final Lhrk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final c:Lloy;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 376
    new-instance v0, Lloy;

    const-string v1, "botched_gallery3_refactoring"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Lhrk;->c:Lloy;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lhrk;->a:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 37
    return-void
.end method

.method private final c(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 363
    iget-object v0, p0, Lhrk;->a:Landroid/content/Context;

    const/4 v1, 0x2

    .line 364
    invoke-static {v0, v3, v1}, Lhqf;->a(Landroid/content/Context;II)[Lhqh;

    move-result-object v1

    .line 366
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 367
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 368
    iget-object v5, v4, Lhqh;->a:Ljava/lang/Integer;

    if-eqz v5, :cond_0

    .line 369
    const-string v5, "bucket_id"

    iget-object v4, v4, Lhqh;->a:Ljava/lang/Integer;

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 370
    const-string v4, "local_folders"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 367
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 373
    :cond_1
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 12

    .prologue
    .line 40
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 41
    iget-object v0, p0, Lhrk;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->a(Landroid/content/Context;)V

    .line 44
    :cond_0
    const/4 v0, 0x6

    if-ge p1, v0, :cond_1

    .line 47
    :try_start_0
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE media_map"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE upload_records"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :cond_1
    :goto_0
    const/4 v0, 0x7

    if-ge p1, v0, :cond_2

    .line 55
    iget-object v0, p0, Lhrk;->a:Landroid/content/Context;

    .line 56
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 57
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.external.photo.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.phoneStorage.photo.last_media_id"

    .line 58
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.external.video.last_media_id"

    .line 59
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.phoneStorage.video.last_media_id"

    .line 60
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 64
    :cond_2
    const/16 v0, 0xd

    if-ge p1, v0, :cond_17

    .line 65
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE media_tracker (_id INTEGER PRIMARY KEY, volume_name TEXT NOT NULL, media_type TEXT NOT NULL,bucket_id TEXT, bucket_name TEXT, last_media_id INTEGER)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    iget-object v1, p0, Lhrk;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "media_scanner.external.photo.last_media_id"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v4, "media_scanner.phoneStorage.photo.last_media_id"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v6, "media_scanner.external.video.last_media_id"

    const-wide/16 v8, 0x0

    invoke-interface {v1, v6, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v8, "media_scanner.phoneStorage.video.last_media_id"

    const-wide/16 v10, 0x0

    invoke-interface {v1, v8, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    new-instance v10, Landroid/content/ContentValues;

    const/4 v11, 0x5

    invoke-direct {v10, v11}, Landroid/content/ContentValues;-><init>(I)V

    const-string v11, "bucket_id"

    invoke-virtual {v10, v11}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v11, "bucket_name"

    invoke-virtual {v10, v11}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v11, "last_media_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v10, v11, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "volume_name"

    const-string v3, "external"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_type"

    const-string v3, "photo"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const-string v2, "last_media_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "volume_name"

    const-string v3, "phoneStorage"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_type"

    const-string v3, "photo"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const-string v2, "last_media_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "volume_name"

    const-string v3, "external"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_type"

    const-string v3, "video"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const-string v2, "last_media_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "volume_name"

    const-string v3, "phoneStorage"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_type"

    const-string v3, "video"

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "media_tracker"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.external.photo.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.phoneStorage.photo.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.external.video.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "media_scanner.phoneStorage.video.last_media_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 66
    const/16 v0, 0xd

    .line 69
    :goto_1
    const/16 v1, 0xe

    if-ge v0, v1, :cond_3

    .line 70
    iget-object v1, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_1
    const-string v0, "ALTER TABLE media_tracker RENAME TO tmp_table"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE media_tracker (_id INTEGER PRIMARY KEY, volume_name TEXT NOT NULL, media_type TEXT NOT NULL,bucket_id TEXT, last_media_id INTEGER NOT NULL DEFAULT(0))"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "INSERT INTO media_tracker(_id, volume_name, media_type, bucket_id) SELECT _id, volume_name, media_type, bucket_id FROM tmp_table;"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE tmp_table;"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 71
    const/16 v0, 0xe

    .line 74
    :cond_3
    const/16 v1, 0xf

    if-ge v0, v1, :cond_4

    .line 75
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 76
    const/16 v0, 0xf

    .line 79
    :cond_4
    const/16 v1, 0x10

    if-ge v0, v1, :cond_5

    .line 80
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lhrk;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupProvider;->b(Landroid/content/Context;)V

    .line 81
    const/16 v0, 0x10

    .line 84
    :cond_5
    const/16 v1, 0x11

    if-ge v0, v1, :cond_6

    .line 85
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE fingerprints (_id INTEGER PRIMARY KEY AUTOINCREMENT,content_uri TEXT,fingerprint TEXT,image_url TEXT,owner_id TEXT,photo_id INTEGER NOT NULL DEFAULT 0)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX fingerprints_index_content_uri ON fingerprints (content_uri)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 86
    const/16 v0, 0x11

    .line 89
    :cond_6
    const/16 v1, 0x12

    if-ge v0, v1, :cond_7

    .line 90
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v0, p0, Lhrk;->a:Landroid/content/Context;

    const-string v1, "iu.picasa.db"

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    .line 91
    const/16 v0, 0x12

    .line 94
    :cond_7
    const/16 v1, 0x13

    if-ge v0, v1, :cond_8

    .line 95
    iget-object v1, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_2
    const-string v0, "DROP TABLE IF EXISTS upload_tasks"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP INDEX IF EXISTS media_record_index_media_id"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "ALTER TABLE media_record RENAME TO tmp_table"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE TABLE media_record (_id INTEGER PRIMARY KEY AUTOINCREMENT,album_id TEXT,bucket_id TEXT,upload_url TEXT,upload_error TEXT,event_id TEXT,fingerprint TEXT,upload_account TEXT,component_name TEXT,plus_page_id TEXT,mime_type TEXT,media_url TEXT NOT NULL,media_time INTEGER NOT NULL,media_id INTEGER NOT NULL,media_hash INTEGER NOT NULL,bytes_total INTEGER NOT NULL DEFAULT -1,retry_end_time INTEGER NOT NULL DEFAULT 0,upload_time INTEGER,bytes_uploaded INTEGER,upload_finish_time INTEGER NOT NULL DEFAULT 0,upload_id INTEGER,upload_reason INTEGER NOT NULL DEFAULT 0,upload_state INTEGER NOT NULL DEFAULT 500,upload_task_state INTEGER,from_camera INTEGER NOT NULL DEFAULT 0,is_image INTEGER NOT NULL DEFAULT 1)"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "CREATE INDEX media_record_index_media_id ON media_record (media_id)"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "INSERT INTO media_record(_id,album_id,upload_error,event_id,upload_account,plus_page_id,media_url,media_time,media_id,media_hash,bytes_total,retry_end_time,upload_time,bytes_uploaded,upload_finish_time,upload_id,upload_reason,upload_state,from_camera,is_image) SELECT _id,album_id,upload_error,event_id,upload_account,plus_page_id,media_url,media_time,media_id,media_hash,bytes_total,retry_end_time,upload_time,bytes_uploaded,upload_finish_time,upload_id,upload_reason,upload_state,from_camera,is_image FROM tmp_table"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "DROP TABLE tmp_table"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE media_record SET album_id = \'instant\' WHERE album_id = \'camera-sync\'"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v0, "UPDATE media_record SET album_id = NULL WHERE album_id = \'events\'"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 96
    const/16 v0, 0x13

    .line 99
    :cond_8
    const/16 v1, 0x14

    if-ge v0, v1, :cond_9

    .line 100
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ALTER TABLE media_tracker RENAME TO media_tracker_legacy"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE exclude_bucket (_id INTEGER PRIMARY KEY, bucket_id TEXT UNIQUE NOT NULL)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE media_tracker (_id INTEGER PRIMARY KEY, volume_name TEXT NOT NULL, media_type TEXT NOT NULL,last_media_id INTEGER NOT NULL DEFAULT(0))"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 101
    const/16 v0, 0x14

    .line 104
    :cond_9
    const/16 v1, 0x15

    if-ge v0, v1, :cond_a

    .line 105
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE IF EXISTS fingerprints"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "DROP INDEX IF EXISTS fingerprints_index_content_uri"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE local_fingerprints (content_uri TEXT PRIMARY KEY NOT NULL, fingerprint TEXT NOT NULL)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE TABLE server_fingerprints (_id INTEGER PRIMARY KEY, fingerprint TEXT NOT NULL, image_url TEXT, photo_id INT NOT NULL DEFAULT(0), owner_id TEXT NOT NULL, UNIQUE (owner_id, image_url), UNIQUE (owner_id, photo_id))"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX local_fingerprints_content_uri ON local_fingerprints(content_uri)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX local_fingerprints_fingerprint ON local_fingerprints(fingerprint)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "CREATE INDEX server_fingerprints_image_url ON server_fingerprints(image_url)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 106
    const/16 v0, 0x15

    .line 109
    :cond_a
    const/16 v1, 0x16

    if-ge v0, v1, :cond_b

    .line 110
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 111
    const/16 v0, 0x16

    .line 114
    :cond_b
    const/16 v1, 0x17

    if-ge v0, v1, :cond_c

    .line 115
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ALTER TABLE media_record ADD COLUMN upload_status INT NOT NULL DEFAULT(0)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "UPDATE media_record SET upload_status = upload_state % 100"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "UPDATE media_record SET upload_state = (upload_state / 100) * 100"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 116
    const/16 v0, 0x17

    .line 119
    :cond_c
    const/16 v1, 0x18

    if-ge v0, v1, :cond_d

    .line 120
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ALTER TABLE media_record ADD COLUMN allow_full_res INT NOT NULL DEFAULT(1)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 121
    const/16 v0, 0x18

    .line 124
    :cond_d
    const/16 v1, 0x19

    if-ge v0, v1, :cond_e

    .line 125
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "CREATE TABLE local_folders (bucket_id TEXT UNIQUE NOT NULL)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 126
    const/16 v0, 0x19

    .line 129
    :cond_e
    const/16 v1, 0x1a

    if-ge v0, v1, :cond_f

    .line 130
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, v0}, Lhrk;->c(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 131
    const/16 v0, 0x1a

    .line 134
    :cond_f
    const/16 v1, 0x1b

    if-ge v0, v1, :cond_10

    .line 135
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v0, Lhrk;->c:Lloy;

    .line 136
    const/16 v0, 0x1b

    .line 139
    :cond_10
    const/16 v1, 0x1c

    if-ge v0, v1, :cond_11

    .line 140
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DELETE FROM local_fingerprints"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 141
    const/16 v0, 0x1c

    .line 144
    :cond_11
    const/16 v1, 0x1d

    if-ge v0, v1, :cond_12

    .line 145
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "ALTER TABLE media_record ADD COLUMN resume_token TEXT"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 146
    const/16 v0, 0x1d

    .line 149
    :cond_12
    const/16 v1, 0x1e

    if-ge v0, v1, :cond_13

    .line 150
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 151
    const/16 v0, 0x1e

    .line 154
    :cond_13
    const/16 v1, 0x1f

    if-ge v0, v1, :cond_14

    .line 155
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "DROP TABLE media_tracker_legacy"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 156
    const/16 v0, 0x1f

    .line 159
    :cond_14
    const/16 v1, 0x20

    if-ge v0, v1, :cond_15

    .line 160
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v0}, Lhrk;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 161
    const/16 v0, 0x20

    .line 164
    :cond_15
    const/16 v1, 0x21

    if-ge v0, v1, :cond_16

    .line 165
    iget-object v0, p0, Lhrk;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v0}, Lhrk;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 166
    :cond_16
    return-void

    .line 70
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 95
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_17
    move v0, p1

    goto/16 :goto_1
.end method

.method a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 446
    iget-object v0, p0, Lhrk;->a:Landroid/content/Context;

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 448
    const-string v2, "ALTER TABLE media_record ADD COLUMN upload_account_id INTEGER NOT NULL DEFAULT -1"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 451
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 454
    :try_start_0
    const-string v3, "SELECT DISTINCT upload_account, plus_page_id FROM media_record"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 456
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 457
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 458
    new-instance v3, Lhrl;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lhrl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 463
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 464
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 463
    :cond_2
    if-eqz v1, :cond_3

    .line 464
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 468
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhrl;

    .line 469
    iget-object v3, v1, Lhrl;->b:Ljava/lang/String;

    .line 470
    iget-object v1, v1, Lhrl;->a:Ljava/lang/String;

    .line 471
    invoke-interface {v0, v1, v3}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 473
    const/4 v5, -0x1

    if-ne v4, v5, :cond_6

    .line 474
    const-string v4, "UploadsDatabaseUpgrader"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 476
    invoke-static {v1}, Lifu;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 477
    invoke-static {v3}, Lifu;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x44

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Could not locate account id when upgrading accountName="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", plusPageId="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    :cond_4
    if-nez v3, :cond_5

    .line 480
    const-string v3, "DELETE FROM media_record WHERE upload_account = ? AND plus_page_id IS NULL"

    new-array v4, v10, [Ljava/lang/Object;

    aput-object v1, v4, v9

    invoke-virtual {p1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 484
    :cond_5
    const-string v4, "DELETE FROM media_record WHERE upload_account = ? AND plus_page_id = ?"

    new-array v5, v11, [Ljava/lang/Object;

    aput-object v1, v5, v9

    aput-object v3, v5, v10

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 489
    :cond_6
    if-nez v3, :cond_7

    .line 490
    const-string v3, "UPDATE media_record SET upload_account_id = ? WHERE upload_account = ?"

    new-array v5, v11, [Ljava/lang/Object;

    .line 491
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v9

    aput-object v1, v5, v10

    .line 490
    invoke-virtual {p1, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 493
    :cond_7
    const-string v5, "UPDATE media_record SET upload_account_id = ? WHERE upload_account = ? AND plus_page_id = ?"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    .line 495
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v9

    aput-object v1, v6, v10

    aput-object v3, v6, v11

    .line 493
    invoke-virtual {p1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 499
    :cond_8
    return-void
.end method

.method b(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 506
    const-string v2, "local_folder_auto_backup"

    .line 508
    iget-object v3, p0, Lhrk;->a:Landroid/content/Context;

    invoke-static {v3}, Lhqv;->b(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v3

    .line 514
    iget-object v4, p0, Lhrk;->a:Landroid/content/Context;

    .line 515
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 514
    invoke-static {v4, v3, v2, v6, v7}, Lhsd;->a(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    move v0, v1

    .line 517
    :cond_0
    if-eqz v0, :cond_5

    .line 521
    const-string v0, "SELECT local_folders.bucket_id, exclude_bucket.bucket_id FROM local_folders LEFT JOIN exclude_bucket ON local_folders.bucket_id = exclude_bucket.bucket_id"

    .line 525
    invoke-virtual {p1, v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 528
    if-eqz v2, :cond_5

    .line 531
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 532
    :cond_1
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 533
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 534
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 538
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 540
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 541
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 542
    const-string v4, "bucket_id"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const-string v4, "exclude_bucket"

    invoke-virtual {p1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 544
    const-string v3, "UploadsDatabaseUpgrader"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 545
    const-string v3, "inserted new excluded bucket "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 550
    :cond_5
    return-void
.end method
