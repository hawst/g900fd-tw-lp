.class public final Lnbp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnwy;

.field public b:Lnbo;

.field private c:Lnei;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 110
    invoke-direct {p0}, Loxq;-><init>()V

    .line 113
    iput-object v0, p0, Lnbp;->a:Lnwy;

    .line 116
    iput-object v0, p0, Lnbp;->b:Lnbo;

    .line 119
    iput-object v0, p0, Lnbp;->c:Lnei;

    .line 110
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 139
    const/4 v0, 0x0

    .line 140
    iget-object v1, p0, Lnbp;->a:Lnwy;

    if-eqz v1, :cond_0

    .line 141
    const/4 v0, 0x1

    iget-object v1, p0, Lnbp;->a:Lnwy;

    .line 142
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 144
    :cond_0
    iget-object v1, p0, Lnbp;->b:Lnbo;

    if-eqz v1, :cond_1

    .line 145
    const/4 v1, 0x2

    iget-object v2, p0, Lnbp;->b:Lnbo;

    .line 146
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_1
    iget-object v1, p0, Lnbp;->c:Lnei;

    if-eqz v1, :cond_2

    .line 149
    const/4 v1, 0x3

    iget-object v2, p0, Lnbp;->c:Lnei;

    .line 150
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 152
    :cond_2
    iget-object v1, p0, Lnbp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    iput v0, p0, Lnbp;->ai:I

    .line 154
    return v0
.end method

.method public a(Loxn;)Lnbp;
    .locals 2

    .prologue
    .line 162
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 163
    sparse-switch v0, :sswitch_data_0

    .line 167
    iget-object v1, p0, Lnbp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 168
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnbp;->ah:Ljava/util/List;

    .line 171
    :cond_1
    iget-object v1, p0, Lnbp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    :sswitch_0
    return-object p0

    .line 178
    :sswitch_1
    iget-object v0, p0, Lnbp;->a:Lnwy;

    if-nez v0, :cond_2

    .line 179
    new-instance v0, Lnwy;

    invoke-direct {v0}, Lnwy;-><init>()V

    iput-object v0, p0, Lnbp;->a:Lnwy;

    .line 181
    :cond_2
    iget-object v0, p0, Lnbp;->a:Lnwy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 185
    :sswitch_2
    iget-object v0, p0, Lnbp;->b:Lnbo;

    if-nez v0, :cond_3

    .line 186
    new-instance v0, Lnbo;

    invoke-direct {v0}, Lnbo;-><init>()V

    iput-object v0, p0, Lnbp;->b:Lnbo;

    .line 188
    :cond_3
    iget-object v0, p0, Lnbp;->b:Lnbo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 192
    :sswitch_3
    iget-object v0, p0, Lnbp;->c:Lnei;

    if-nez v0, :cond_4

    .line 193
    new-instance v0, Lnei;

    invoke-direct {v0}, Lnei;-><init>()V

    iput-object v0, p0, Lnbp;->c:Lnei;

    .line 195
    :cond_4
    iget-object v0, p0, Lnbp;->c:Lnei;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 163
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lnbp;->a:Lnwy;

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x1

    iget-object v1, p0, Lnbp;->a:Lnwy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lnbp;->b:Lnbo;

    if-eqz v0, :cond_1

    .line 128
    const/4 v0, 0x2

    iget-object v1, p0, Lnbp;->b:Lnbo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 130
    :cond_1
    iget-object v0, p0, Lnbp;->c:Lnei;

    if-eqz v0, :cond_2

    .line 131
    const/4 v0, 0x3

    iget-object v1, p0, Lnbp;->c:Lnei;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 133
    :cond_2
    iget-object v0, p0, Lnbp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 135
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0, p1}, Lnbp;->a(Loxn;)Lnbp;

    move-result-object v0

    return-object v0
.end method
