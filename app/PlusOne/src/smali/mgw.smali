.class public final Lmgw;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lowj;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23083
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23086
    iput-object v0, p0, Lmgw;->apiHeader:Llyq;

    .line 23089
    iput-object v0, p0, Lmgw;->a:Lowj;

    .line 23083
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 23106
    const/4 v0, 0x0

    .line 23107
    iget-object v1, p0, Lmgw;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 23108
    const/4 v0, 0x1

    iget-object v1, p0, Lmgw;->apiHeader:Llyq;

    .line 23109
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23111
    :cond_0
    iget-object v1, p0, Lmgw;->a:Lowj;

    if-eqz v1, :cond_1

    .line 23112
    const/4 v1, 0x2

    iget-object v2, p0, Lmgw;->a:Lowj;

    .line 23113
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23115
    :cond_1
    iget-object v1, p0, Lmgw;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23116
    iput v0, p0, Lmgw;->ai:I

    .line 23117
    return v0
.end method

.method public a(Loxn;)Lmgw;
    .locals 2

    .prologue
    .line 23125
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 23126
    sparse-switch v0, :sswitch_data_0

    .line 23130
    iget-object v1, p0, Lmgw;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 23131
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgw;->ah:Ljava/util/List;

    .line 23134
    :cond_1
    iget-object v1, p0, Lmgw;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23136
    :sswitch_0
    return-object p0

    .line 23141
    :sswitch_1
    iget-object v0, p0, Lmgw;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 23142
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmgw;->apiHeader:Llyq;

    .line 23144
    :cond_2
    iget-object v0, p0, Lmgw;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23148
    :sswitch_2
    iget-object v0, p0, Lmgw;->a:Lowj;

    if-nez v0, :cond_3

    .line 23149
    new-instance v0, Lowj;

    invoke-direct {v0}, Lowj;-><init>()V

    iput-object v0, p0, Lmgw;->a:Lowj;

    .line 23151
    :cond_3
    iget-object v0, p0, Lmgw;->a:Lowj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23126
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23094
    iget-object v0, p0, Lmgw;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 23095
    const/4 v0, 0x1

    iget-object v1, p0, Lmgw;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23097
    :cond_0
    iget-object v0, p0, Lmgw;->a:Lowj;

    if-eqz v0, :cond_1

    .line 23098
    const/4 v0, 0x2

    iget-object v1, p0, Lmgw;->a:Lowj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23100
    :cond_1
    iget-object v0, p0, Lmgw;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 23102
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 23079
    invoke-virtual {p0, p1}, Lmgw;->a(Loxn;)Lmgw;

    move-result-object v0

    return-object v0
.end method
