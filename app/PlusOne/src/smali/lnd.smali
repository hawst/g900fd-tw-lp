.class public final Llnd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkdq;


# instance fields
.field private a:Z

.field private b:Llnb;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x2

    return v0
.end method

.method public a(Lcom/google/android/libraries/social/resources/images/ImageResource;Ljava/nio/ByteBuffer;Z)Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 31
    invoke-static {p2}, Lcom/google/android/webp/WebpDecoder;->getConfig(Ljava/nio/ByteBuffer;)Lcom/google/android/webp/WebpDecoder$Config;

    move-result-object v7

    .line 32
    if-eqz v7, :cond_9

    .line 33
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getIdentifier()Lkdc;

    move-result-object v0

    check-cast v0, Lkds;

    invoke-virtual {v0}, Lkds;->a()I

    move-result v0

    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Llnd;->a:Z

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getManager()Lkdv;

    move-result-object v0

    invoke-interface {v0}, Lkdv;->c()Lkeh;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Llnb;

    invoke-interface {v0}, Lkdv;->a()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Llnb;-><init>(Landroid/content/Context;Lkeh;)V

    iput-object v2, p0, Llnd;->b:Llnb;

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Llnd;->a:Z

    :cond_1
    iget-object v0, p0, Llnd;->b:Llnb;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getCachedFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Llnd;->b:Llnb;

    invoke-virtual {v1, v0}, Llnb;->a(Ljava/io/File;)V

    .line 34
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, v7, Lcom/google/android/webp/WebpDecoder$Config;->a:I

    iget v1, v7, Lcom/google/android/webp/WebpDecoder$Config;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Decoding WEBP: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_3
    if-eqz p3, :cond_6

    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getManager()Lkdv;

    move-result-object v0

    iget v1, v7, Lcom/google/android/webp/WebpDecoder$Config;->a:I

    iget v2, v7, Lcom/google/android/webp/WebpDecoder$Config;->b:I

    invoke-interface {v0, v1, v2}, Lkdv;->a(II)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v8, v0

    :goto_0
    if-nez v8, :cond_7

    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Cannot create a bitmap"

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    instance-of v1, v0, Lkdo;

    if-eqz v1, :cond_5

    move-object v1, v0

    check-cast v1, Lkdo;

    iget-object v8, v1, Lkdo;->a:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_5
    move-object v8, v4

    move-object v0, v4

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/libraries/social/resources/images/ImageResource;->getManager()Lkdv;

    move-result-object v0

    iget v1, v7, Lcom/google/android/webp/WebpDecoder$Config;->a:I

    iget v2, v7, Lcom/google/android/webp/WebpDecoder$Config;->b:I

    invoke-interface {v0, v1, v2}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v8

    if-nez v8, :cond_a

    iget v0, v7, Lcom/google/android/webp/WebpDecoder$Config;->a:I

    iget v1, v7, Lcom/google/android/webp/WebpDecoder$Config;->b:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    move-object v0, v8

    goto :goto_0

    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    invoke-static {p2, v8}, Lcom/google/android/webp/WebpDecoder;->a(Ljava/nio/ByteBuffer;Landroid/graphics/Bitmap;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    const-string v4, "image/webp"

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    iget v6, v7, Lcom/google/android/webp/WebpDecoder$Config;->a:I

    iget v7, v7, Lcom/google/android/webp/WebpDecoder$Config;->b:I

    move-object v1, p1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logDecodeTime(JLjava/lang/String;IIILandroid/graphics/Bitmap;)V

    .line 37
    :goto_1
    return-object v0

    .line 34
    :cond_8
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    const-string v0, "Cannot decode WEBP"

    invoke-virtual {p1, v0, v4}, Lcom/google/android/libraries/social/resources/images/ImageResource;->logError(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v4

    goto :goto_1

    :cond_9
    move-object v0, v4

    .line 37
    goto :goto_1

    :cond_a
    move-object v0, v8

    goto :goto_0
.end method
