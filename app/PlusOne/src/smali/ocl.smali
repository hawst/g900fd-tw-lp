.class public final Locl;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lock;

.field public b:Lock;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 504
    invoke-direct {p0}, Loxq;-><init>()V

    .line 507
    iput-object v0, p0, Locl;->a:Lock;

    .line 510
    iput-object v0, p0, Locl;->b:Lock;

    .line 504
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 527
    const/4 v0, 0x0

    .line 528
    iget-object v1, p0, Locl;->a:Lock;

    if-eqz v1, :cond_0

    .line 529
    const/4 v0, 0x1

    iget-object v1, p0, Locl;->a:Lock;

    .line 530
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 532
    :cond_0
    iget-object v1, p0, Locl;->b:Lock;

    if-eqz v1, :cond_1

    .line 533
    const/4 v1, 0x2

    iget-object v2, p0, Locl;->b:Lock;

    .line 534
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 536
    :cond_1
    iget-object v1, p0, Locl;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 537
    iput v0, p0, Locl;->ai:I

    .line 538
    return v0
.end method

.method public a(Loxn;)Locl;
    .locals 2

    .prologue
    .line 546
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 547
    sparse-switch v0, :sswitch_data_0

    .line 551
    iget-object v1, p0, Locl;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 552
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Locl;->ah:Ljava/util/List;

    .line 555
    :cond_1
    iget-object v1, p0, Locl;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 557
    :sswitch_0
    return-object p0

    .line 562
    :sswitch_1
    iget-object v0, p0, Locl;->a:Lock;

    if-nez v0, :cond_2

    .line 563
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Locl;->a:Lock;

    .line 565
    :cond_2
    iget-object v0, p0, Locl;->a:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 569
    :sswitch_2
    iget-object v0, p0, Locl;->b:Lock;

    if-nez v0, :cond_3

    .line 570
    new-instance v0, Lock;

    invoke-direct {v0}, Lock;-><init>()V

    iput-object v0, p0, Locl;->b:Lock;

    .line 572
    :cond_3
    iget-object v0, p0, Locl;->b:Lock;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 547
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 515
    iget-object v0, p0, Locl;->a:Lock;

    if-eqz v0, :cond_0

    .line 516
    const/4 v0, 0x1

    iget-object v1, p0, Locl;->a:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 518
    :cond_0
    iget-object v0, p0, Locl;->b:Lock;

    if-eqz v0, :cond_1

    .line 519
    const/4 v0, 0x2

    iget-object v1, p0, Locl;->b:Lock;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 521
    :cond_1
    iget-object v0, p0, Locl;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 523
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0, p1}, Locl;->a(Loxn;)Locl;

    move-result-object v0

    return-object v0
.end method
