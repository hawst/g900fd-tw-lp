.class final Liud;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lijt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lijt",
        "<",
        "Limf;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Z

.field private synthetic b:Litz;


# direct methods
.method constructor <init>(Litz;Z)V
    .locals 0

    .prologue
    .line 477
    iput-object p1, p0, Liud;->b:Litz;

    iput-boolean p2, p0, Liud;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lijs;)V
    .locals 0

    .prologue
    .line 477
    check-cast p1, Limf;

    invoke-virtual {p0, p1}, Liud;->a(Limf;)V

    return-void
.end method

.method public a(Limf;)V
    .locals 6

    .prologue
    const/high16 v5, 0x10000000

    .line 480
    iget-object v0, p0, Liud;->b:Litz;

    iput-object p1, v0, Litz;->c:Limf;

    .line 482
    const-string v0, "GmsLocationReporting"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    iget-object v0, p0, Liud;->b:Litz;

    iget-object v0, v0, Litz;->c:Limf;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onConnected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    :cond_0
    iget-object v0, p0, Liud;->b:Litz;

    iget-object v0, v0, Litz;->c:Limf;

    invoke-interface {v0}, Limf;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Liud;->b:Litz;

    iget-object v0, v0, Litz;->c:Limf;

    invoke-interface {v0}, Limf;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 487
    iget-object v1, p0, Liud;->b:Litz;

    iget-object v0, p0, Liud;->b:Litz;

    iget-object v2, v0, Litz;->a:Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.apps.maps.LOCATION_SETTINGS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.apps.maps"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-static {v2}, Litz;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {v2}, Litz;->b(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    :goto_0
    iput-object v0, v1, Litz;->d:Landroid/content/Intent;

    .line 490
    :cond_2
    iget-object v0, p0, Liud;->b:Litz;

    invoke-virtual {v0}, Litz;->n()V

    .line 492
    iget-boolean v0, p0, Liud;->a:Z

    if-eqz v0, :cond_3

    .line 495
    iget-object v0, p0, Liud;->b:Litz;

    invoke-virtual {v0}, Litz;->k()V

    .line 497
    :cond_3
    return-void

    .line 487
    :cond_4
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.intent.action.MANAGE_NETWORK_USAGE"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.apps.maps"

    const-string v4, "com.google.android.maps.MapsActivity"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-static {v2}, Litz;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    goto :goto_0
.end method
