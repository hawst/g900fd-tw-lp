.class public final Lnso;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 3259
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3274
    iput v0, p0, Lnso;->a:I

    .line 3277
    iput v0, p0, Lnso;->b:I

    .line 3280
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lnso;->c:[Ljava/lang/String;

    .line 3259
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/high16 v3, -0x80000000

    const/4 v1, 0x0

    .line 3302
    .line 3303
    iget v0, p0, Lnso;->a:I

    if-eq v0, v3, :cond_3

    .line 3304
    const/4 v0, 0x1

    iget v2, p0, Lnso;->a:I

    .line 3305
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3307
    :goto_0
    iget v2, p0, Lnso;->b:I

    if-eq v2, v3, :cond_0

    .line 3308
    const/4 v2, 0x2

    iget v3, p0, Lnso;->b:I

    .line 3309
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3311
    :cond_0
    iget-object v2, p0, Lnso;->c:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnso;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 3313
    iget-object v3, p0, Lnso;->c:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 3315
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 3313
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3317
    :cond_1
    add-int/2addr v0, v2

    .line 3318
    iget-object v1, p0, Lnso;->c:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3320
    :cond_2
    iget-object v1, p0, Lnso;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3321
    iput v0, p0, Lnso;->ai:I

    .line 3322
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnso;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3330
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3331
    sparse-switch v0, :sswitch_data_0

    .line 3335
    iget-object v1, p0, Lnso;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3336
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnso;->ah:Ljava/util/List;

    .line 3339
    :cond_1
    iget-object v1, p0, Lnso;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3341
    :sswitch_0
    return-object p0

    .line 3346
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3347
    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-ne v0, v5, :cond_3

    .line 3350
    :cond_2
    iput v0, p0, Lnso;->a:I

    goto :goto_0

    .line 3352
    :cond_3
    iput v3, p0, Lnso;->a:I

    goto :goto_0

    .line 3357
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 3358
    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-ne v0, v5, :cond_5

    .line 3361
    :cond_4
    iput v0, p0, Lnso;->b:I

    goto :goto_0

    .line 3363
    :cond_5
    iput v3, p0, Lnso;->b:I

    goto :goto_0

    .line 3368
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 3369
    iget-object v0, p0, Lnso;->c:[Ljava/lang/String;

    array-length v0, v0

    .line 3370
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 3371
    iget-object v2, p0, Lnso;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3372
    iput-object v1, p0, Lnso;->c:[Ljava/lang/String;

    .line 3373
    :goto_1
    iget-object v1, p0, Lnso;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    .line 3374
    iget-object v1, p0, Lnso;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3375
    invoke-virtual {p1}, Loxn;->a()I

    .line 3373
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3378
    :cond_6
    iget-object v1, p0, Lnso;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 3331
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/high16 v2, -0x80000000

    .line 3285
    iget v0, p0, Lnso;->a:I

    if-eq v0, v2, :cond_0

    .line 3286
    const/4 v0, 0x1

    iget v1, p0, Lnso;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3288
    :cond_0
    iget v0, p0, Lnso;->b:I

    if-eq v0, v2, :cond_1

    .line 3289
    const/4 v0, 0x2

    iget v1, p0, Lnso;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 3291
    :cond_1
    iget-object v0, p0, Lnso;->c:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3292
    iget-object v1, p0, Lnso;->c:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 3293
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 3292
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3296
    :cond_2
    iget-object v0, p0, Lnso;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3298
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3255
    invoke-virtual {p0, p1}, Lnso;->a(Loxn;)Lnso;

    move-result-object v0

    return-object v0
.end method
