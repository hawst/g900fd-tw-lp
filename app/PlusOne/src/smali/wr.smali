.class final Lwr;
.super Lvj;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Z

.field private c:I

.field private d:I

.field private e:Lwo;

.field private f:I

.field private synthetic g:Lwn;


# direct methods
.method public constructor <init>(Lwn;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 304
    iput-object p1, p0, Lwr;->g:Lwn;

    invoke-direct {p0}, Lvj;-><init>()V

    .line 298
    const/4 v0, -0x1

    iput v0, p0, Lwr;->c:I

    .line 305
    iput-object p2, p0, Lwr;->a:Ljava/lang/String;

    .line 306
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lwr;->g:Lwn;

    invoke-static {v0, p0}, Lwn;->a(Lwn;Lwr;)V

    .line 335
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lwr;->e:Lwo;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lwr;->e:Lwo;

    iget v1, p0, Lwr;->f:I

    invoke-virtual {v0, v1, p1}, Lwo;->a(II)V

    .line 361
    :goto_0
    return-void

    .line 358
    :cond_0
    iput p1, p0, Lwr;->c:I

    .line 359
    const/4 v0, 0x0

    iput v0, p0, Lwr;->d:I

    goto :goto_0
.end method

.method public a(Lwo;)V
    .locals 2

    .prologue
    .line 309
    iput-object p1, p0, Lwr;->e:Lwo;

    .line 310
    iget-object v0, p0, Lwr;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lwo;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lwr;->f:I

    .line 311
    iget-boolean v0, p0, Lwr;->b:Z

    if-eqz v0, :cond_1

    .line 312
    iget v0, p0, Lwr;->f:I

    invoke-virtual {p1, v0}, Lwo;->e(I)V

    .line 313
    iget v0, p0, Lwr;->c:I

    if-ltz v0, :cond_0

    .line 314
    iget v0, p0, Lwr;->f:I

    iget v1, p0, Lwr;->c:I

    invoke-virtual {p1, v0, v1}, Lwo;->a(II)V

    .line 315
    const/4 v0, -0x1

    iput v0, p0, Lwr;->c:I

    .line 317
    :cond_0
    iget v0, p0, Lwr;->d:I

    if-eqz v0, :cond_1

    .line 318
    iget v0, p0, Lwr;->f:I

    iget v1, p0, Lwr;->d:I

    invoke-virtual {p1, v0, v1}, Lwo;->b(II)V

    .line 319
    const/4 v0, 0x0

    iput v0, p0, Lwr;->d:I

    .line 322
    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 339
    const/4 v0, 0x1

    iput-boolean v0, p0, Lwr;->b:Z

    .line 340
    iget-object v0, p0, Lwr;->e:Lwo;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lwr;->e:Lwo;

    iget v1, p0, Lwr;->f:I

    invoke-virtual {v0, v1}, Lwo;->e(I)V

    .line 343
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lwr;->e:Lwo;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lwr;->e:Lwo;

    iget v1, p0, Lwr;->f:I

    invoke-virtual {v0, v1, p1}, Lwo;->b(II)V

    .line 370
    :goto_0
    return-void

    .line 368
    :cond_0
    iget v0, p0, Lwr;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Lwr;->d:I

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Lwr;->b:Z

    .line 348
    iget-object v0, p0, Lwr;->e:Lwo;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lwr;->e:Lwo;

    iget v1, p0, Lwr;->f:I

    invoke-virtual {v0, v1}, Lwo;->f(I)V

    .line 351
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lwr;->e:Lwo;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lwr;->e:Lwo;

    iget v1, p0, Lwr;->f:I

    invoke-virtual {v0, v1}, Lwo;->d(I)V

    .line 327
    const/4 v0, 0x0

    iput-object v0, p0, Lwr;->e:Lwo;

    .line 328
    const/4 v0, 0x0

    iput v0, p0, Lwr;->f:I

    .line 330
    :cond_0
    return-void
.end method
