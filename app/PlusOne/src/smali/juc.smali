.class public final Ljuc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljuf;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljuc;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lnzi;

.field private final c:Lizu;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:J

.field private final h:J

.field private i:I

.field private final j:Ljcj;

.field private final k:Ljcm;

.field private final l:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const/4 v0, 0x1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    .line 26
    invoke-static {v0, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ljuc;->a:Ljava/lang/String;

    .line 301
    new-instance v0, Ljud;

    invoke-direct {v0}, Ljud;-><init>()V

    sput-object v0, Ljuc;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuc;->e:Ljava/lang/String;

    .line 261
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuc;->f:Ljava/lang/String;

    .line 262
    const-class v0, Lizu;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    iput-object v0, p0, Ljuc;->c:Lizu;

    .line 263
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Ljuc;->g:J

    .line 264
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Ljuc;->h:J

    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Ljuc;->i:I

    .line 266
    new-instance v0, Ljug;

    iget-object v1, p0, Ljuc;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljug;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ljuc;->j:Ljcj;

    .line 267
    new-instance v0, Ljue;

    iget-object v1, p0, Ljuc;->c:Lizu;

    invoke-direct {v0, v1}, Ljue;-><init>(Lizu;)V

    iput-object v0, p0, Ljuc;->k:Ljcm;

    .line 268
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljuc;->d:Ljava/lang/String;

    .line 269
    invoke-direct {p0}, Ljuc;->r()J

    move-result-wide v0

    iput-wide v0, p0, Ljuc;->l:J

    .line 271
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 272
    const/4 v1, 0x0

    .line 273
    if-eqz v0, :cond_0

    .line 275
    :try_start_0
    new-instance v2, Lnzi;

    invoke-direct {v2}, Lnzi;-><init>()V

    invoke-static {v2, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnzi;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :goto_0
    iput-object v0, p0, Ljuc;->b:Lnzi;

    .line 281
    return-void

    .line 276
    :catch_0
    move-exception v0

    .line 277
    const-string v2, "MediaItem"

    const-string v3, "Failed to deserialize EditInfo."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Lizu;)V
    .locals 11

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 145
    sget-object v2, Ljuc;->a:Ljava/lang/String;

    sget-object v3, Ljuc;->a:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-wide v8, v6

    move-object v10, v5

    invoke-direct/range {v1 .. v10}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;Ljava/lang/String;JJLnzi;)V

    .line 147
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V
    .locals 12

    .prologue
    .line 141
    const/4 v5, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    invoke-direct/range {v1 .. v10}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;Ljava/lang/String;JJLnzi;)V

    .line 142
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lizu;Ljava/lang/String;JJLnzi;)V
    .locals 3

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Ljuc;->f:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Ljuc;->e:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Ljuc;->c:Lizu;

    .line 58
    iput-object p4, p0, Ljuc;->d:Ljava/lang/String;

    .line 59
    iput-wide p5, p0, Ljuc;->g:J

    .line 60
    iput-object p9, p0, Ljuc;->b:Lnzi;

    .line 61
    iput-wide p7, p0, Ljuc;->h:J

    .line 62
    new-instance v0, Ljug;

    iget-object v1, p0, Ljuc;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljug;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ljuc;->j:Ljcj;

    .line 63
    invoke-direct {p0}, Ljuc;->r()J

    move-result-wide v0

    iput-wide v0, p0, Ljuc;->l:J

    .line 64
    new-instance v0, Ljue;

    iget-object v1, p0, Ljuc;->c:Lizu;

    invoke-direct {v0, v1}, Ljue;-><init>(Lizu;)V

    iput-object v0, p0, Ljuc;->k:Ljcm;

    .line 65
    return-void
.end method

.method private r()J
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 72
    invoke-virtual {p0}, Ljuc;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x1

    .line 73
    :goto_0
    invoke-virtual {p0}, Ljuc;->m()Z

    move-result v4

    if-eqz v4, :cond_2

    const-wide/16 v4, 0x2

    :goto_1
    or-long/2addr v4, v0

    .line 74
    invoke-virtual {p0}, Ljuc;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    const-wide/16 v0, 0x4

    :goto_2
    or-long/2addr v4, v0

    .line 75
    invoke-virtual {p0}, Ljuc;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    const-wide/16 v0, 0x8

    :goto_3
    or-long/2addr v4, v0

    .line 76
    invoke-virtual {p0}, Ljuc;->p()Z

    move-result v0

    if-eqz v0, :cond_5

    const-wide/16 v0, 0x10

    :goto_4
    or-long/2addr v4, v0

    .line 77
    invoke-virtual {p0}, Ljuc;->n()Z

    move-result v0

    if-eqz v0, :cond_6

    const-wide/16 v0, 0x20

    :goto_5
    or-long/2addr v4, v0

    .line 78
    invoke-virtual {p0}, Ljuc;->n()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Ljuc;->q()Z

    move-result v0

    if-nez v0, :cond_7

    const-wide/16 v0, 0x80

    :goto_6
    or-long/2addr v4, v0

    .line 79
    iget-wide v0, p0, Ljuc;->g:J

    const-wide/16 v6, 0x100

    and-long/2addr v0, v6

    cmp-long v0, v0, v2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_0

    const-wide/16 v2, 0x200

    :cond_0
    or-long v0, v4, v2

    const-wide/16 v2, 0x40

    or-long/2addr v0, v2

    return-wide v0

    :cond_1
    move-wide v0, v2

    .line 72
    goto :goto_0

    :cond_2
    move-wide v4, v2

    .line 73
    goto :goto_1

    :cond_3
    move-wide v0, v2

    .line 74
    goto :goto_2

    :cond_4
    move-wide v0, v2

    .line 75
    goto :goto_3

    :cond_5
    move-wide v0, v2

    .line 76
    goto :goto_4

    :cond_6
    move-wide v0, v2

    .line 77
    goto :goto_5

    :cond_7
    move-wide v0, v2

    .line 78
    goto :goto_6

    .line 79
    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 171
    iget-wide v0, p0, Ljuc;->h:J

    return-wide v0
.end method

.method public a(Lnzi;)Ljuf;
    .locals 11

    .prologue
    .line 210
    new-instance v1, Ljuc;

    iget-object v2, p0, Ljuc;->f:Ljava/lang/String;

    iget-object v3, p0, Ljuc;->e:Ljava/lang/String;

    iget-object v4, p0, Ljuc;->c:Lizu;

    iget-object v5, p0, Ljuc;->d:Ljava/lang/String;

    iget-wide v6, p0, Ljuc;->g:J

    iget-wide v8, p0, Ljuc;->h:J

    move-object v10, p1

    invoke-direct/range {v1 .. v10}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;Ljava/lang/String;JJLnzi;)V

    return-object v1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 89
    iput p1, p0, Ljuc;->i:I

    .line 90
    return-void
.end method

.method public b()Ljcj;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Ljuc;->j:Ljcj;

    return-object v0
.end method

.method public c()Ljcm;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Ljuc;->k:Ljcm;

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Ljuc;->l:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 251
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Ljuc;->i:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 112
    instance-of v1, p1, Ljuc;

    if-nez v1, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v0

    .line 115
    :cond_1
    check-cast p1, Ljuc;

    .line 116
    iget-object v1, p0, Ljuc;->f:Ljava/lang/String;

    iget-object v2, p1, Ljuc;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljuc;->e:Ljava/lang/String;

    iget-object v2, p1, Ljuc;->e:Ljava/lang/String;

    .line 117
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljuc;->c:Lizu;

    if-nez v1, :cond_2

    iget-object v1, p1, Ljuc;->c:Lizu;

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Ljuc;->c:Lizu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljuc;->c:Lizu;

    iget-object v2, p1, Ljuc;->c:Lizu;

    .line 119
    invoke-virtual {v1, v2}, Lizu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    iget-wide v2, p0, Ljuc;->g:J

    iget-wide v4, p1, Ljuc;->g:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-object v1, p0, Ljuc;->b:Lnzi;

    if-nez v1, :cond_4

    iget-object v1, p1, Ljuc;->b:Lnzi;

    if-eqz v1, :cond_5

    :cond_4
    iget-object v1, p0, Ljuc;->b:Lnzi;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ljuc;->b:Lnzi;

    iget-object v2, p1, Ljuc;->b:Lnzi;

    .line 122
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_5
    iget-wide v2, p0, Ljuc;->h:J

    iget-wide v4, p1, Ljuc;->h:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Lizu;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Ljuc;->c:Lizu;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Ljuc;->d:Ljava/lang/String;

    return-object v0
.end method

.method public h()Lnzi;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Ljuc;->b:Lnzi;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 129
    iget-object v0, p0, Ljuc;->b:Lnzi;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 131
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljuc;->c:Lizu;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 132
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ljuc;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 133
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ljuc;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 134
    mul-int/lit8 v0, v0, 0x11

    iget-wide v2, p0, Ljuc;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    mul-int/lit8 v0, v0, 0x11

    iget-wide v2, p0, Ljuc;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    return v0

    .line 129
    :cond_0
    iget-object v0, p0, Ljuc;->b:Lnzi;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Ljuc;->c:Lizu;

    invoke-virtual {v0}, Lizu;->hashCode()I

    move-result v0

    goto :goto_1

    .line 132
    :cond_2
    iget-object v0, p0, Ljuc;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 133
    :cond_3
    iget-object v1, p0, Ljuc;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public i()J
    .locals 2

    .prologue
    .line 167
    iget-wide v0, p0, Ljuc;->g:J

    return-wide v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Ljuc;->e:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Ljuc;->f:Ljava/lang/String;

    return-object v0
.end method

.method public l()Z
    .locals 4

    .prologue
    .line 215
    iget-wide v0, p0, Ljuc;->g:J

    const-wide/16 v2, 0x2

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 4

    .prologue
    .line 219
    iget-wide v0, p0, Ljuc;->h:J

    const-wide v2, 0x200000000L

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Ljuc;->c:Lizu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljuc;->c:Lizu;

    invoke-virtual {v0}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Ljuc;->c:Lizu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljuc;->c:Lizu;

    invoke-virtual {v0}, Lizu;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljuc;->c:Lizu;

    invoke-virtual {v0}, Lizu;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Ljuc;->c:Lizu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljuc;->c:Lizu;

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v1, Ljac;->b:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Ljuc;->e:Ljava/lang/String;

    invoke-static {v0}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljuc;->e:Ljava/lang/String;

    .line 241
    invoke-static {}, Ljvj;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 246
    iget-object v0, p0, Ljuc;->e:Ljava/lang/String;

    iget-object v1, p0, Ljuc;->d:Ljava/lang/String;

    iget-object v2, p0, Ljuc;->c:Lizu;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "[MediaItem clusterId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Ljuc;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 286
    iget-object v0, p0, Ljuc;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Ljuc;->c:Lizu;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 288
    iget-wide v0, p0, Ljuc;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 289
    iget-wide v0, p0, Ljuc;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 290
    iget v0, p0, Ljuc;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 291
    iget-object v0, p0, Ljuc;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 293
    iget-object v0, p0, Ljuc;->b:Lnzi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljuc;->b:Lnzi;

    .line 294
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 295
    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 296
    return-void

    .line 294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
