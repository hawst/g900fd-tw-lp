.class public final Lfxk;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lkdd;


# static fields
.field private static a:I

.field private static b:I

.field private static c:Z

.field private static d:Lizs;

.field private static e:Landroid/graphics/Paint;

.field private static f:Landroid/graphics/Paint;

.field private static g:Landroid/text/TextPaint;

.field private static h:Landroid/text/TextPaint;

.field private static i:Landroid/text/TextPaint;

.field private static j:Landroid/graphics/Bitmap;

.field private static k:Landroid/graphics/Bitmap;

.field private static l:Landroid/graphics/Bitmap;

.field private static m:Landroid/graphics/drawable/Drawable;

.field private static n:Landroid/graphics/drawable/NinePatchDrawable;

.field private static o:Ljava/lang/String;

.field private static p:Ljava/lang/String;

.field private static q:I

.field private static r:I


# instance fields
.field private A:Landroid/graphics/Rect;

.field private B:Landroid/graphics/Rect;

.field private C:Landroid/graphics/Rect;

.field private D:Landroid/graphics/Rect;

.field private E:Landroid/text/StaticLayout;

.field private F:Landroid/graphics/Point;

.field private G:Landroid/text/StaticLayout;

.field private H:Landroid/graphics/Point;

.field private I:Landroid/text/StaticLayout;

.field private J:Landroid/graphics/Point;

.field private K:Landroid/text/StaticLayout;

.field private L:Landroid/graphics/Point;

.field private M:Lfwu;

.field private N:Landroid/graphics/Bitmap;

.field private s:Z

.field private t:Lpbl;

.field private u:Lltp;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Lizu;

.field private z:Lkda;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfxk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfxk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 119
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const v4, 0x7f0d024c

    const/4 v3, 0x1

    .line 125
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 127
    invoke-static {p1}, Lidk;->a(Landroid/content/Context;)V

    .line 128
    invoke-virtual {p0, v3}, Lfxk;->setFocusable(Z)V

    .line 130
    sget-boolean v0, Lfxk;->c:Z

    if-nez v0, :cond_0

    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 132
    const-class v0, Lizs;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lfxk;->d:Lizs;

    .line 133
    new-instance v0, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lfxk;->e:Landroid/graphics/Paint;

    .line 134
    const v0, 0x7f0d02b7

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxk;->a:I

    .line 135
    const v0, 0x7f0d02b8

    .line 136
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lfxk;->b:I

    .line 138
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 139
    sput-object v0, Lfxk;->f:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 140
    sget-object v0, Lfxk;->f:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 142
    const/16 v0, 0x22

    invoke-static {p1, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    sput-object v0, Lfxk;->g:Landroid/text/TextPaint;

    .line 144
    const/16 v0, 0xb

    invoke-static {p1, v0}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    sput-object v0, Lfxk;->h:Landroid/text/TextPaint;

    .line 148
    new-instance v0, Landroid/text/TextPaint;

    sget-object v2, Lfxk;->h:Landroid/text/TextPaint;

    invoke-direct {v0, v2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 149
    sput-object v0, Lfxk;->i:Landroid/text/TextPaint;

    const v2, 0x7f0b013a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setColor(I)V

    .line 150
    sget-object v0, Lfxk;->i:Landroid/text/TextPaint;

    .line 151
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 150
    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 152
    sget-object v0, Lfxk;->i:Landroid/text/TextPaint;

    invoke-static {v0, v4}, Llib;->a(Landroid/text/TextPaint;I)V

    .line 154
    const v0, 0x7f020279

    invoke-static {v1, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxk;->j:Landroid/graphics/Bitmap;

    .line 155
    const v0, 0x7f020269

    invoke-static {v1, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxk;->k:Landroid/graphics/Bitmap;

    .line 156
    const v0, 0x7f0203a6

    invoke-static {v1, v0}, Lfus;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lfxk;->l:Landroid/graphics/Bitmap;

    .line 157
    const v0, 0x7f020416

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lfxk;->m:Landroid/graphics/drawable/Drawable;

    .line 160
    const v0, 0x7f0a090a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfxk;->p:Ljava/lang/String;

    .line 161
    const v0, 0x7f0a073a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfxk;->o:Ljava/lang/String;

    .line 163
    const v0, 0x7f0200ad

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Lfxk;->n:Landroid/graphics/drawable/NinePatchDrawable;

    .line 166
    invoke-static {p1}, Ldrm;->a(Landroid/content/Context;)I

    move-result v0

    .line 167
    sput v0, Lfxk;->q:I

    invoke-static {v0}, Ldrm;->a(I)I

    move-result v0

    sput v0, Lfxk;->r:I

    .line 168
    sput-boolean v3, Lfxk;->c:Z

    .line 171
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfxk;->A:Landroid/graphics/Rect;

    .line 172
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfxk;->C:Landroid/graphics/Rect;

    .line 173
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfxk;->D:Landroid/graphics/Rect;

    .line 174
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfxk;->B:Landroid/graphics/Rect;

    .line 176
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxk;->F:Landroid/graphics/Point;

    .line 177
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxk;->H:Landroid/graphics/Point;

    .line 178
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxk;->J:Landroid/graphics/Point;

    .line 179
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lfxk;->L:Landroid/graphics/Point;

    .line 180
    return-void
.end method

.method private a(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;
    .locals 11

    .prologue
    .line 284
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v0, p1

    move v1, p2

    move v2, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move/from16 v10, p7

    invoke-static/range {v0 .. v10}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 296
    iget v0, p2, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p2, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 297
    invoke-virtual {p1, p3}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 298
    iget v0, p2, Landroid/graphics/Point;->x:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p2, Landroid/graphics/Point;->y:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 299
    return-void
.end method


# virtual methods
.method protected a(IIIII)I
    .locals 11

    .prologue
    .line 337
    add-int v2, p2, p4

    .line 341
    iget-object v0, p0, Lfxk;->t:Lpbl;

    iget-object v4, v0, Lpbl;->b:Ljava/lang/String;

    iget-object v5, p0, Lfxk;->F:Landroid/graphics/Point;

    sget-object v6, Lfxk;->g:Landroid/text/TextPaint;

    const/4 v7, 0x1

    move-object v0, p0

    move v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v7}, Lfxk;->a(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxk;->E:Landroid/text/StaticLayout;

    .line 343
    iget-object v0, p0, Lfxk;->E:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 346
    add-int v1, v0, p5

    .line 349
    const/4 v3, 0x0

    sget-object v4, Lfxk;->l:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lfxk;->D:Landroid/graphics/Rect;

    iget-object v7, p0, Lfxk;->v:Ljava/lang/String;

    iget-object v8, p0, Lfxk;->H:Landroid/graphics/Point;

    sget-object v9, Lfxk;->h:Landroid/text/TextPaint;

    const/4 v10, 0x1

    move v0, p1

    move v2, p3

    move v6, p4

    invoke-static/range {v0 .. v10}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxk;->G:Landroid/text/StaticLayout;

    .line 352
    iget-object v0, p0, Lfxk;->G:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v1

    .line 357
    iget-object v1, p0, Lfxk;->w:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 358
    add-int v2, v0, p5

    .line 359
    iget-object v0, p0, Lfxk;->H:Landroid/graphics/Point;

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget-object v0, p0, Lfxk;->H:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, p1

    sub-int v3, p3, v0

    iget-object v4, p0, Lfxk;->w:Ljava/lang/String;

    iget-object v5, p0, Lfxk;->J:Landroid/graphics/Point;

    sget-object v6, Lfxk;->h:Landroid/text/TextPaint;

    const/4 v7, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lfxk;->a(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxk;->I:Landroid/text/StaticLayout;

    .line 362
    iget-object v0, p0, Lfxk;->I:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 365
    :cond_0
    new-instance v1, Lidh;

    iget-object v2, p0, Lfxk;->t:Lpbl;

    invoke-direct {v1, v2}, Lidh;-><init>(Lpbl;)V

    invoke-static {v1}, Ldrm;->c(Lidh;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 366
    add-int v9, v0, p5

    .line 367
    new-instance v0, Lfwu;

    invoke-virtual {p0}, Lfxk;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lfxk;->o:Ljava/lang/String;

    sget-object v4, Lfxk;->i:Landroid/text/TextPaint;

    sget-object v5, Lfxk;->n:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v6, Lfxk;->n:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v7, 0x0

    move v8, p1

    invoke-direct/range {v0 .. v9}, Lfwu;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;II)V

    iput-object v0, p0, Lfxk;->M:Lfwu;

    .line 370
    iget-object v0, p0, Lfxk;->M:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v9

    .line 375
    :cond_1
    iget-object v1, p0, Lfxk;->x:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 376
    add-int v1, v0, p5

    .line 378
    const/4 v3, 0x0

    iget-object v4, p0, Lfxk;->N:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lfxk;->C:Landroid/graphics/Rect;

    iget-object v7, p0, Lfxk;->x:Ljava/lang/String;

    iget-object v8, p0, Lfxk;->L:Landroid/graphics/Point;

    sget-object v9, Lfxk;->h:Landroid/text/TextPaint;

    const/4 v10, 0x1

    move v0, p1

    move v2, p3

    move v6, p4

    invoke-static/range {v0 .. v10}, Llib;->a(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lfxk;->K:Landroid/text/StaticLayout;

    .line 382
    iget-object v0, p0, Lfxk;->K:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v1

    .line 385
    :cond_2
    add-int/2addr v0, p4

    .line 387
    sub-int/2addr v0, p2

    return v0
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 251
    iget-boolean v0, p0, Lfxk;->s:Z

    if-nez v0, :cond_0

    .line 280
    :goto_0
    return-void

    .line 255
    :cond_0
    iput-object v1, p0, Lfxk;->u:Lltp;

    .line 256
    iput-object v1, p0, Lfxk;->t:Lpbl;

    .line 258
    iput-object v1, p0, Lfxk;->v:Ljava/lang/String;

    .line 259
    iput-object v1, p0, Lfxk;->w:Ljava/lang/String;

    .line 260
    iput-object v1, p0, Lfxk;->x:Ljava/lang/String;

    .line 262
    iget-object v0, p0, Lfxk;->A:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 264
    iget-object v0, p0, Lfxk;->C:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 265
    iget-object v0, p0, Lfxk;->B:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 266
    iget-object v0, p0, Lfxk;->D:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 268
    iput-object v1, p0, Lfxk;->E:Landroid/text/StaticLayout;

    .line 269
    iput-object v1, p0, Lfxk;->G:Landroid/text/StaticLayout;

    .line 270
    iput-object v1, p0, Lfxk;->I:Landroid/text/StaticLayout;

    .line 271
    iput-object v1, p0, Lfxk;->K:Landroid/text/StaticLayout;

    .line 272
    iput-object v1, p0, Lfxk;->M:Lfwu;

    .line 273
    iput-object v1, p0, Lfxk;->N:Landroid/graphics/Bitmap;

    .line 275
    iget-object v0, p0, Lfxk;->F:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 276
    iget-object v0, p0, Lfxk;->H:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 277
    iget-object v0, p0, Lfxk;->J:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 278
    iget-object v0, p0, Lfxk;->L:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    .line 279
    iput-boolean v2, p0, Lfxk;->s:Z

    goto :goto_0
.end method

.method protected a(IIII)V
    .locals 3

    .prologue
    .line 310
    iget-object v0, p0, Lfxk;->u:Lltp;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lfxk;->A:Landroid/graphics/Rect;

    add-int v1, p1, p3

    add-int v2, p2, p4

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 313
    :cond_0
    return-void
.end method

.method public a(Lfxl;Lpbl;)V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 190
    invoke-virtual {p0}, Lfxk;->c()V

    .line 191
    invoke-virtual {p0}, Lfxk;->a()V

    .line 193
    iput-object p2, p0, Lfxk;->t:Lpbl;

    .line 194
    iget-object v0, p0, Lfxk;->t:Lpbl;

    if-eqz v0, :cond_3

    move v0, v6

    :goto_0
    iput-boolean v0, p0, Lfxk;->s:Z

    .line 196
    iget-boolean v0, p0, Lfxk;->s:Z

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lfxk;->t:Lpbl;

    iget-object v0, v0, Lpbl;->l:Lpbj;

    iget-object v0, v0, Lpbj;->d:Llto;

    invoke-static {v0}, Ldrm;->a(Llto;)Lltp;

    move-result-object v0

    iput-object v0, p0, Lfxk;->u:Lltp;

    .line 199
    iget-object v0, p0, Lfxk;->u:Lltp;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lfxk;->u:Lltp;

    iget v0, v0, Lltp;->b:I

    if-ne v0, v6, :cond_4

    sget-object v0, Ljac;->d:Ljac;

    .line 203
    :goto_1
    invoke-virtual {p0}, Lfxk;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lfxk;->u:Lltp;

    iget-object v2, v2, Lltp;->d:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    iput-object v0, p0, Lfxk;->y:Lizu;

    .line 207
    :cond_0
    iget-object v0, p0, Lfxk;->t:Lpbl;

    iget-object v0, v0, Lpbl;->h:Loya;

    sget-object v1, Loyy;->a:Loxr;

    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Loyy;

    .line 208
    iget-object v0, v7, Loyy;->c:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 209
    iget-object v0, v7, Loyy;->c:Ljava/lang/String;

    invoke-static {v0}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    .line 212
    :goto_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    .line 213
    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 214
    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    iget-object v3, v7, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 213
    invoke-static/range {v0 .. v6}, Lidi;->a(JLjava/util/TimeZone;JLjava/util/TimeZone;Z)Z

    move-result v0

    .line 217
    invoke-virtual {p1}, Lfxl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v7, v8, v9, v0}, Lidi;->a(Landroid/content/Context;Loyy;ZLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfxk;->v:Ljava/lang/String;

    .line 220
    iget-object v0, v7, Loyy;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v10, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 221
    new-instance v0, Lidh;

    iget-object v1, p0, Lfxk;->t:Lpbl;

    invoke-direct {v0, v1}, Lidh;-><init>(Lpbl;)V

    .line 222
    iget-object v1, v7, Loyy;->c:Ljava/lang/String;

    .line 223
    invoke-static {v0}, Ldrm;->c(Lidh;)Z

    move-result v2

    .line 222
    invoke-static {v1, v10, v2}, Lidk;->a(Ljava/lang/String;Ljava/util/Calendar;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lfxk;->w:Ljava/lang/String;

    .line 225
    invoke-virtual {v0}, Lidh;->o()Lpao;

    move-result-object v0

    .line 226
    iget-object v1, p0, Lfxk;->t:Lpbl;

    iget-object v1, v1, Lpbl;->l:Lpbj;

    iget-object v1, v1, Lpbj;->g:Lltn;

    .line 228
    if-eqz v0, :cond_5

    .line 229
    iget-object v1, v0, Lpao;->b:Ljava/lang/String;

    iput-object v1, p0, Lfxk;->x:Ljava/lang/String;

    .line 230
    iget-object v1, p0, Lfxk;->x:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lpao;->d:Loya;

    if-eqz v1, :cond_1

    .line 231
    iget-object v0, v0, Lpao;->d:Loya;

    sget-object v1, Lpcg;->a:Loxr;

    .line 232
    invoke-virtual {v0, v1}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpcg;

    .line 233
    if-eqz v0, :cond_1

    .line 234
    iget-object v0, v0, Lpcg;->c:Ljava/lang/String;

    iput-object v0, p0, Lfxk;->x:Ljava/lang/String;

    .line 237
    :cond_1
    sget-object v0, Lfxk;->j:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxk;->N:Landroid/graphics/Bitmap;

    .line 244
    :cond_2
    :goto_3
    invoke-virtual {p0}, Lfxk;->b()V

    .line 245
    return-void

    :cond_3
    move v0, v8

    .line 194
    goto/16 :goto_0

    .line 201
    :cond_4
    sget-object v0, Ljac;->a:Ljac;

    goto/16 :goto_1

    .line 238
    :cond_5
    if-eqz v1, :cond_2

    .line 239
    sget-object v0, Lfxk;->p:Ljava/lang/String;

    iput-object v0, p0, Lfxk;->x:Ljava/lang/String;

    .line 240
    sget-object v0, Lfxk;->k:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lfxk;->N:Landroid/graphics/Bitmap;

    goto :goto_3

    :cond_6
    move-object v5, v9

    goto/16 :goto_2
.end method

.method protected a(Ljava/lang/StringBuilder;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 316
    new-array v0, v3, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lfxk;->t:Lpbl;

    iget-object v1, v1, Lpbl;->b:Ljava/lang/String;

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 317
    new-array v0, v3, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lfxk;->v:Ljava/lang/String;

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 318
    new-array v0, v3, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lfxk;->w:Ljava/lang/String;

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 319
    new-instance v0, Lidh;

    iget-object v1, p0, Lfxk;->t:Lpbl;

    invoke-direct {v0, v1}, Lidh;-><init>(Lpbl;)V

    invoke-static {v0}, Ldrm;->c(Lidh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    new-array v0, v3, [Ljava/lang/CharSequence;

    sget-object v1, Lfxk;->o:Ljava/lang/String;

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 322
    :cond_0
    new-array v0, v3, [Ljava/lang/CharSequence;

    iget-object v1, p0, Lfxk;->x:Ljava/lang/String;

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    .line 323
    return-void
.end method

.method public a(Lkda;)V
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lfxk;->z:Lkda;

    if-ne p1, v0, :cond_0

    .line 501
    invoke-virtual {p0}, Lfxk;->invalidate()V

    .line 503
    :cond_0
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 475
    iget-object v0, p0, Lfxk;->y:Lizu;

    if-eqz v0, :cond_0

    .line 476
    sget-object v0, Lfxk;->d:Lizs;

    iget-object v1, p0, Lfxk;->y:Lizu;

    sget v2, Lfxk;->q:I

    sget v3, Lfxk;->r:I

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lfxk;->z:Lkda;

    .line 479
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lfxk;->z:Lkda;

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Lfxk;->z:Lkda;

    invoke-virtual {v0, p0}, Lkda;->unregister(Lkdd;)V

    .line 488
    const/4 v0, 0x0

    iput-object v0, p0, Lfxk;->z:Lkda;

    .line 490
    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 465
    sget-object v0, Lfxk;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lfxk;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 466
    invoke-virtual {p0}, Lfxk;->invalidate()V

    .line 467
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 468
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 421
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 422
    iget-boolean v0, p0, Lfxk;->s:Z

    if-nez v0, :cond_0

    .line 461
    :goto_0
    return-void

    .line 426
    :cond_0
    iget-object v0, p0, Lfxk;->B:Landroid/graphics/Rect;

    sget-object v1, Lfxk;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 429
    iget-object v0, p0, Lfxk;->z:Lkda;

    if-eqz v0, :cond_1

    .line 430
    iget-object v0, p0, Lfxk;->z:Lkda;

    invoke-virtual {v0}, Lkda;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 432
    if-eqz v0, :cond_1

    .line 433
    iget-object v1, p0, Lfxk;->A:Landroid/graphics/Rect;

    sget-object v2, Lfxk;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 437
    :cond_1
    iget-object v0, p0, Lfxk;->E:Landroid/text/StaticLayout;

    iget-object v1, p0, Lfxk;->F:Landroid/graphics/Point;

    invoke-direct {p0, v0, v1, p1}, Lfxk;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 440
    iget-object v0, p0, Lfxk;->G:Landroid/text/StaticLayout;

    iget-object v1, p0, Lfxk;->H:Landroid/graphics/Point;

    invoke-direct {p0, v0, v1, p1}, Lfxk;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 441
    sget-object v0, Lfxk;->l:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lfxk;->D:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v3, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 444
    iget-object v0, p0, Lfxk;->I:Landroid/text/StaticLayout;

    if-eqz v0, :cond_2

    .line 445
    iget-object v0, p0, Lfxk;->I:Landroid/text/StaticLayout;

    iget-object v1, p0, Lfxk;->J:Landroid/graphics/Point;

    invoke-direct {p0, v0, v1, p1}, Lfxk;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 449
    :cond_2
    iget-object v0, p0, Lfxk;->M:Lfwu;

    if-eqz v0, :cond_3

    .line 450
    iget-object v0, p0, Lfxk;->M:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 454
    :cond_3
    iget-object v0, p0, Lfxk;->K:Landroid/text/StaticLayout;

    if-eqz v0, :cond_4

    .line 455
    iget-object v0, p0, Lfxk;->K:Landroid/text/StaticLayout;

    iget-object v1, p0, Lfxk;->L:Landroid/graphics/Point;

    invoke-direct {p0, v0, v1, p1}, Lfxk;->a(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 456
    iget-object v0, p0, Lfxk;->N:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lfxk;->C:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v3, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 459
    :cond_4
    sget-object v0, Lfxk;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lfxk;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lfxk;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 460
    sget-object v0, Lfxk;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 397
    sget v1, Lfxk;->a:I

    .line 398
    mul-int/lit8 v0, v1, 0x2

    .line 399
    sget v5, Lfxk;->b:I

    .line 401
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 402
    invoke-static {v6}, Ldrm;->a(I)I

    move-result v2

    .line 404
    invoke-virtual {p0, v4, v4, v6, v2}, Lfxk;->a(IIII)V

    .line 407
    sub-int v3, v6, v1

    sub-int/2addr v3, v0

    .line 410
    iget-object v0, p0, Lfxk;->B:Landroid/graphics/Rect;

    invoke-virtual {v0, v4, v2, v6, v2}, Landroid/graphics/Rect;->set(IIII)V

    move-object v0, p0

    move v4, v1

    .line 413
    invoke-virtual/range {v0 .. v5}, Lfxk;->a(IIIII)I

    move-result v0

    .line 415
    iget-object v1, p0, Lfxk;->B:Landroid/graphics/Rect;

    iget-object v3, p0, Lfxk;->B:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v0

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    .line 416
    add-int/2addr v0, v2

    invoke-virtual {p0, v6, v0}, Lfxk;->setMeasuredDimension(II)V

    .line 417
    return-void
.end method
