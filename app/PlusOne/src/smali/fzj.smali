.class public final enum Lfzj;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lfzj;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lfzj;

.field public static final enum b:Lfzj;

.field public static final enum c:Lfzj;

.field public static final enum d:Lfzj;

.field public static final enum e:Lfzj;

.field public static final enum f:Lfzj;

.field public static final enum g:Lfzj;

.field private static final synthetic k:[Lfzj;


# instance fields
.field private final h:I

.field private final i:I

.field private final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 54
    new-instance v0, Lfzj;

    const-string v1, "COMMENT"

    const v3, 0x7f100499

    const v4, 0x7f10049b

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lfzj;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lfzj;->a:Lfzj;

    .line 55
    new-instance v3, Lfzj;

    const-string v4, "TAG"

    const v6, 0x7f100497

    const v7, 0x7f1004a3

    move v5, v9

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lfzj;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lfzj;->b:Lfzj;

    .line 56
    new-instance v3, Lfzj;

    const-string v4, "PLUS_ONE"

    const v6, 0x7f100495

    const v7, 0x7f1004a0

    move v5, v10

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lfzj;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lfzj;->c:Lfzj;

    .line 57
    new-instance v3, Lfzj;

    const-string v4, "EDIT"

    const v6, 0x7f100494

    const v7, 0x7f100061

    const v8, 0x7f10049d

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lfzj;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lfzj;->d:Lfzj;

    .line 58
    new-instance v3, Lfzj;

    const-string v4, "SHARE"

    const v6, 0x7f100496

    const v7, 0x7f1004a2

    move v5, v12

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lfzj;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lfzj;->e:Lfzj;

    .line 59
    new-instance v3, Lfzj;

    const-string v4, "DELETE"

    const/4 v5, 0x5

    const v6, 0x7f100498

    const v7, 0x7f10049c

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lfzj;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lfzj;->f:Lfzj;

    .line 60
    new-instance v3, Lfzj;

    const-string v4, "HASHTAG"

    const/4 v5, 0x6

    const v6, 0x7f10049a

    const v7, 0x7f10049e

    move v8, v2

    invoke-direct/range {v3 .. v8}, Lfzj;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lfzj;->g:Lfzj;

    .line 53
    const/4 v0, 0x7

    new-array v0, v0, [Lfzj;

    sget-object v1, Lfzj;->a:Lfzj;

    aput-object v1, v0, v2

    sget-object v1, Lfzj;->b:Lfzj;

    aput-object v1, v0, v9

    sget-object v1, Lfzj;->c:Lfzj;

    aput-object v1, v0, v10

    sget-object v1, Lfzj;->d:Lfzj;

    aput-object v1, v0, v11

    sget-object v1, Lfzj;->e:Lfzj;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lfzj;->f:Lfzj;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lfzj;->g:Lfzj;

    aput-object v2, v0, v1

    sput-object v0, Lfzj;->k:[Lfzj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput p3, p0, Lfzj;->h:I

    .line 68
    iput p4, p0, Lfzj;->i:I

    .line 69
    iput p5, p0, Lfzj;->j:I

    .line 70
    return-void
.end method

.method public static synthetic a(Lfzj;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lfzj;->i:I

    return v0
.end method

.method public static synthetic b(Lfzj;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lfzj;->j:I

    return v0
.end method

.method public static synthetic c(Lfzj;)I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lfzj;->h:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lfzj;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lfzj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfzj;

    return-object v0
.end method

.method public static values()[Lfzj;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lfzj;->k:[Lfzj;

    invoke-virtual {v0}, [Lfzj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfzj;

    return-object v0
.end method
