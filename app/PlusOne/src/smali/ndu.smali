.class public final Lndu;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:[Loud;

.field public d:[Lorf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 163
    invoke-direct {p0}, Loxq;-><init>()V

    .line 172
    const/high16 v0, -0x80000000

    iput v0, p0, Lndu;->a:I

    .line 177
    sget-object v0, Loud;->a:[Loud;

    iput-object v0, p0, Lndu;->c:[Loud;

    .line 180
    sget-object v0, Lorf;->a:[Lorf;

    iput-object v0, p0, Lndu;->d:[Lorf;

    .line 163
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 211
    .line 212
    iget-object v0, p0, Lndu;->b:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 213
    const/4 v0, 0x1

    iget-object v2, p0, Lndu;->b:Ljava/lang/String;

    .line 214
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 216
    :goto_0
    iget-object v2, p0, Lndu;->c:[Loud;

    if-eqz v2, :cond_1

    .line 217
    iget-object v3, p0, Lndu;->c:[Loud;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 218
    if-eqz v5, :cond_0

    .line 219
    const/4 v6, 0x2

    .line 220
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 217
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 224
    :cond_1
    iget-object v2, p0, Lndu;->d:[Lorf;

    if-eqz v2, :cond_3

    .line 225
    iget-object v2, p0, Lndu;->d:[Lorf;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 226
    if-eqz v4, :cond_2

    .line 227
    const/4 v5, 0x3

    .line 228
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 225
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 232
    :cond_3
    iget v1, p0, Lndu;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_4

    .line 233
    const/4 v1, 0x4

    iget v2, p0, Lndu;->a:I

    .line 234
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 236
    :cond_4
    iget-object v1, p0, Lndu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    iput v0, p0, Lndu;->ai:I

    .line 238
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lndu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 246
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 247
    sparse-switch v0, :sswitch_data_0

    .line 251
    iget-object v2, p0, Lndu;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 252
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lndu;->ah:Ljava/util/List;

    .line 255
    :cond_1
    iget-object v2, p0, Lndu;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    :sswitch_0
    return-object p0

    .line 262
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lndu;->b:Ljava/lang/String;

    goto :goto_0

    .line 266
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 267
    iget-object v0, p0, Lndu;->c:[Loud;

    if-nez v0, :cond_3

    move v0, v1

    .line 268
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loud;

    .line 269
    iget-object v3, p0, Lndu;->c:[Loud;

    if-eqz v3, :cond_2

    .line 270
    iget-object v3, p0, Lndu;->c:[Loud;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 272
    :cond_2
    iput-object v2, p0, Lndu;->c:[Loud;

    .line 273
    :goto_2
    iget-object v2, p0, Lndu;->c:[Loud;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 274
    iget-object v2, p0, Lndu;->c:[Loud;

    new-instance v3, Loud;

    invoke-direct {v3}, Loud;-><init>()V

    aput-object v3, v2, v0

    .line 275
    iget-object v2, p0, Lndu;->c:[Loud;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 276
    invoke-virtual {p1}, Loxn;->a()I

    .line 273
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 267
    :cond_3
    iget-object v0, p0, Lndu;->c:[Loud;

    array-length v0, v0

    goto :goto_1

    .line 279
    :cond_4
    iget-object v2, p0, Lndu;->c:[Loud;

    new-instance v3, Loud;

    invoke-direct {v3}, Loud;-><init>()V

    aput-object v3, v2, v0

    .line 280
    iget-object v2, p0, Lndu;->c:[Loud;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 284
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 285
    iget-object v0, p0, Lndu;->d:[Lorf;

    if-nez v0, :cond_6

    move v0, v1

    .line 286
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lorf;

    .line 287
    iget-object v3, p0, Lndu;->d:[Lorf;

    if-eqz v3, :cond_5

    .line 288
    iget-object v3, p0, Lndu;->d:[Lorf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 290
    :cond_5
    iput-object v2, p0, Lndu;->d:[Lorf;

    .line 291
    :goto_4
    iget-object v2, p0, Lndu;->d:[Lorf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 292
    iget-object v2, p0, Lndu;->d:[Lorf;

    new-instance v3, Lorf;

    invoke-direct {v3}, Lorf;-><init>()V

    aput-object v3, v2, v0

    .line 293
    iget-object v2, p0, Lndu;->d:[Lorf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 294
    invoke-virtual {p1}, Loxn;->a()I

    .line 291
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 285
    :cond_6
    iget-object v0, p0, Lndu;->d:[Lorf;

    array-length v0, v0

    goto :goto_3

    .line 297
    :cond_7
    iget-object v2, p0, Lndu;->d:[Lorf;

    new-instance v3, Lorf;

    invoke-direct {v3}, Lorf;-><init>()V

    aput-object v3, v2, v0

    .line 298
    iget-object v2, p0, Lndu;->d:[Lorf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 302
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 303
    if-eqz v0, :cond_8

    const/4 v2, 0x1

    if-eq v0, v2, :cond_8

    const/4 v2, 0x2

    if-ne v0, v2, :cond_9

    .line 306
    :cond_8
    iput v0, p0, Lndu;->a:I

    goto/16 :goto_0

    .line 308
    :cond_9
    iput v1, p0, Lndu;->a:I

    goto/16 :goto_0

    .line 247
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 185
    iget-object v1, p0, Lndu;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 186
    const/4 v1, 0x1

    iget-object v2, p0, Lndu;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 188
    :cond_0
    iget-object v1, p0, Lndu;->c:[Loud;

    if-eqz v1, :cond_2

    .line 189
    iget-object v2, p0, Lndu;->c:[Loud;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 190
    if-eqz v4, :cond_1

    .line 191
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 189
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 195
    :cond_2
    iget-object v1, p0, Lndu;->d:[Lorf;

    if-eqz v1, :cond_4

    .line 196
    iget-object v1, p0, Lndu;->d:[Lorf;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 197
    if-eqz v3, :cond_3

    .line 198
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 196
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 202
    :cond_4
    iget v0, p0, Lndu;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_5

    .line 203
    const/4 v0, 0x4

    iget v1, p0, Lndu;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 205
    :cond_5
    iget-object v0, p0, Lndu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 207
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 159
    invoke-virtual {p0, p1}, Lndu;->a(Loxn;)Lndu;

    move-result-object v0

    return-object v0
.end method
