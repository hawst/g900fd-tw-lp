.class public final Lirw;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/libraries/social/ingest/IngestActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V
    .locals 1

    .prologue
    .line 557
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 558
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lirw;->a:Ljava/lang/ref/WeakReference;

    .line 559
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 563
    iget-object v0, p0, Lirw;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ingest/IngestActivity;

    .line 564
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->h(Lcom/google/android/libraries/social/ingest/IngestActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 567
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 572
    :pswitch_0
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->j(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    goto :goto_0

    .line 569
    :pswitch_1
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->i(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    goto :goto_0

    .line 575
    :pswitch_2
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->k(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    goto :goto_0

    .line 578
    :pswitch_3
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->c(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lirx;

    move-result-object v0

    invoke-virtual {v0}, Lirx;->b()V

    goto :goto_0

    .line 581
    :pswitch_4
    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->l(Lcom/google/android/libraries/social/ingest/IngestActivity;)V

    goto :goto_0

    .line 567
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
