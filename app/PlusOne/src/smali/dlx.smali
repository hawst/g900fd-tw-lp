.class public final Ldlx;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmga;",
        "Lmgb;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnqx;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lnra;

.field private p:Lnrk;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lnqx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    const-string v3, "loadpeopleviewdata"

    new-instance v4, Lmga;

    invoke-direct {v4}, Lmga;-><init>()V

    new-instance v5, Lmgb;

    invoke-direct {v5}, Lmgb;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 29
    iput-object p3, p0, Ldlx;->a:Ljava/util/List;

    .line 30
    return-void
.end method


# virtual methods
.method protected a(Lmga;)V
    .locals 6

    .prologue
    .line 34
    new-instance v0, Lnra;

    invoke-direct {v0}, Lnra;-><init>()V

    iput-object v0, p1, Lmga;->a:Lnra;

    .line 35
    iget-object v0, p1, Lmga;->apiHeader:Llyq;

    sget-object v1, Ldjo;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Ldjo;->b:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xdbba0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v1, 0x0

    sput-object v1, Ldjo;->a:Ljava/lang/String;

    :cond_0
    sget-object v1, Ldjo;->a:Ljava/lang/String;

    iput-object v1, v0, Llyq;->c:Ljava/lang/String;

    .line 36
    iget-object v1, p1, Lmga;->a:Lnra;

    .line 37
    if-eqz v1, :cond_1

    iget-object v0, v1, Lnra;->a:[Lnqx;

    if-eqz v0, :cond_1

    .line 38
    iget-object v0, p0, Ldlx;->a:Ljava/util/List;

    iget-object v2, p0, Ldlx;->a:Ljava/util/List;

    .line 39
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lnqx;

    .line 38
    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnqx;

    iput-object v0, v1, Lnra;->a:[Lnqx;

    .line 41
    :cond_1
    iput-object v1, p0, Ldlx;->b:Lnra;

    .line 42
    return-void
.end method

.method protected a(Lmgb;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p1, Lmgb;->a:Lnrk;

    iput-object v0, p0, Ldlx;->p:Lnrk;

    .line 47
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmga;

    invoke-virtual {p0, p1}, Ldlx;->a(Lmga;)V

    return-void
.end method

.method public b()Lnra;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Ldlx;->b:Lnra;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmgb;

    invoke-virtual {p0, p1}, Ldlx;->a(Lmgb;)V

    return-void
.end method

.method public c()Lnrk;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ldlx;->p:Lnrk;

    return-object v0
.end method
