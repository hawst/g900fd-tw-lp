.class public final Lnii;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnii;


# instance fields
.field private b:I

.field private c:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2863
    const/4 v0, 0x0

    new-array v0, v0, [Lnii;

    sput-object v0, Lnii;->a:[Lnii;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2864
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2877
    const/high16 v0, -0x80000000

    iput v0, p0, Lnii;->b:I

    .line 2864
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2896
    const/4 v0, 0x0

    .line 2897
    iget v1, p0, Lnii;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 2898
    const/4 v0, 0x1

    iget v1, p0, Lnii;->b:I

    .line 2899
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2901
    :cond_0
    iget-object v1, p0, Lnii;->c:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 2902
    const/4 v1, 0x2

    iget-object v2, p0, Lnii;->c:Ljava/lang/Float;

    .line 2903
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2905
    :cond_1
    iget-object v1, p0, Lnii;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2906
    iput v0, p0, Lnii;->ai:I

    .line 2907
    return v0
.end method

.method public a(Loxn;)Lnii;
    .locals 2

    .prologue
    .line 2915
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2916
    sparse-switch v0, :sswitch_data_0

    .line 2920
    iget-object v1, p0, Lnii;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2921
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnii;->ah:Ljava/util/List;

    .line 2924
    :cond_1
    iget-object v1, p0, Lnii;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2926
    :sswitch_0
    return-object p0

    .line 2931
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2932
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 2939
    :cond_2
    iput v0, p0, Lnii;->b:I

    goto :goto_0

    .line 2941
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnii;->b:I

    goto :goto_0

    .line 2946
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lnii;->c:Ljava/lang/Float;

    goto :goto_0

    .line 2916
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2884
    iget v0, p0, Lnii;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2885
    const/4 v0, 0x1

    iget v1, p0, Lnii;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2887
    :cond_0
    iget-object v0, p0, Lnii;->c:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 2888
    const/4 v0, 0x2

    iget-object v1, p0, Lnii;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 2890
    :cond_1
    iget-object v0, p0, Lnii;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2892
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2860
    invoke-virtual {p0, p1}, Lnii;->a(Loxn;)Lnii;

    move-result-object v0

    return-object v0
.end method
