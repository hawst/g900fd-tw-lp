.class public final Lhsh;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lhsg;

.field private b:Landroid/content/Context;

.field private c:Ljava/io/File;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhsl;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/net/Uri;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhsg;Ljava/io/File;Ljava/util/List;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lhsg;",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Lhsl;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 442
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 443
    iput-object p1, p0, Lhsh;->b:Landroid/content/Context;

    .line 444
    iput-object p3, p0, Lhsh;->c:Ljava/io/File;

    .line 445
    iput-object p2, p0, Lhsh;->a:Lhsg;

    .line 446
    iput-object p4, p0, Lhsh;->d:Ljava/util/List;

    .line 447
    iput p5, p0, Lhsh;->f:I

    .line 448
    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Void;
    .locals 5

    .prologue
    .line 453
    :try_start_0
    iget-object v0, p0, Lhsh;->b:Landroid/content/Context;

    .line 454
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lhsh;->d:Ljava/util/List;

    .line 453
    invoke-static {v0, v1}, Lhsf;->a(Landroid/content/ContentResolver;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 456
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 457
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lhsh;->c:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 458
    invoke-static {v0}, Ljbh;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 460
    if-eqz v3, :cond_0

    .line 461
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 463
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 464
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 466
    iget-object v4, p0, Lhsh;->b:Landroid/content/Context;

    .line 467
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v4

    .line 466
    invoke-static {v4}, Ljbh;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v4

    .line 469
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 470
    iput-object v0, p0, Lhsh;->e:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    :cond_0
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 463
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 485
    iget-object v0, p0, Lhsh;->a:Lhsg;

    iget-object v1, p0, Lhsh;->c:Ljava/io/File;

    iget-object v1, p0, Lhsh;->e:Landroid/net/Uri;

    iget v2, p0, Lhsh;->f:I

    invoke-interface {v0, v1, v2}, Lhsg;->a(Landroid/net/Uri;I)V

    .line 486
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0}, Lhsh;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 433
    invoke-virtual {p0}, Lhsh;->b()V

    return-void
.end method
