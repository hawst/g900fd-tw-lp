.class public final Lmbh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnwm;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4858
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4861
    iput-object v0, p0, Lmbh;->apiHeader:Llyr;

    .line 4864
    iput-object v0, p0, Lmbh;->a:Lnwm;

    .line 4858
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4881
    const/4 v0, 0x0

    .line 4882
    iget-object v1, p0, Lmbh;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 4883
    const/4 v0, 0x1

    iget-object v1, p0, Lmbh;->apiHeader:Llyr;

    .line 4884
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4886
    :cond_0
    iget-object v1, p0, Lmbh;->a:Lnwm;

    if-eqz v1, :cond_1

    .line 4887
    const/4 v1, 0x2

    iget-object v2, p0, Lmbh;->a:Lnwm;

    .line 4888
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4890
    :cond_1
    iget-object v1, p0, Lmbh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4891
    iput v0, p0, Lmbh;->ai:I

    .line 4892
    return v0
.end method

.method public a(Loxn;)Lmbh;
    .locals 2

    .prologue
    .line 4900
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4901
    sparse-switch v0, :sswitch_data_0

    .line 4905
    iget-object v1, p0, Lmbh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4906
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmbh;->ah:Ljava/util/List;

    .line 4909
    :cond_1
    iget-object v1, p0, Lmbh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4911
    :sswitch_0
    return-object p0

    .line 4916
    :sswitch_1
    iget-object v0, p0, Lmbh;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 4917
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmbh;->apiHeader:Llyr;

    .line 4919
    :cond_2
    iget-object v0, p0, Lmbh;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4923
    :sswitch_2
    iget-object v0, p0, Lmbh;->a:Lnwm;

    if-nez v0, :cond_3

    .line 4924
    new-instance v0, Lnwm;

    invoke-direct {v0}, Lnwm;-><init>()V

    iput-object v0, p0, Lmbh;->a:Lnwm;

    .line 4926
    :cond_3
    iget-object v0, p0, Lmbh;->a:Lnwm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4901
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4869
    iget-object v0, p0, Lmbh;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 4870
    const/4 v0, 0x1

    iget-object v1, p0, Lmbh;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4872
    :cond_0
    iget-object v0, p0, Lmbh;->a:Lnwm;

    if-eqz v0, :cond_1

    .line 4873
    const/4 v0, 0x2

    iget-object v1, p0, Lmbh;->a:Lnwm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4875
    :cond_1
    iget-object v0, p0, Lmbh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4877
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4854
    invoke-virtual {p0, p1}, Lmbh;->a(Loxn;)Lmbh;

    move-result-object v0

    return-object v0
.end method
