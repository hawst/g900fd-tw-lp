.class public final Llwd;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Llwb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0}, Loxq;-><init>()V

    .line 151
    sget-object v0, Llwb;->a:[Llwb;

    iput-object v0, p0, Llwd;->a:[Llwb;

    .line 148
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 169
    .line 170
    iget-object v1, p0, Llwd;->a:[Llwb;

    if-eqz v1, :cond_1

    .line 171
    iget-object v2, p0, Llwd;->a:[Llwb;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 172
    if-eqz v4, :cond_0

    .line 173
    const/4 v5, 0x1

    .line 174
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 171
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 178
    :cond_1
    iget-object v1, p0, Llwd;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    iput v0, p0, Llwd;->ai:I

    .line 180
    return v0
.end method

.method public a(Loxn;)Llwd;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 188
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 189
    sparse-switch v0, :sswitch_data_0

    .line 193
    iget-object v2, p0, Llwd;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 194
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Llwd;->ah:Ljava/util/List;

    .line 197
    :cond_1
    iget-object v2, p0, Llwd;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    :sswitch_0
    return-object p0

    .line 204
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 205
    iget-object v0, p0, Llwd;->a:[Llwb;

    if-nez v0, :cond_3

    move v0, v1

    .line 206
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Llwb;

    .line 207
    iget-object v3, p0, Llwd;->a:[Llwb;

    if-eqz v3, :cond_2

    .line 208
    iget-object v3, p0, Llwd;->a:[Llwb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 210
    :cond_2
    iput-object v2, p0, Llwd;->a:[Llwb;

    .line 211
    :goto_2
    iget-object v2, p0, Llwd;->a:[Llwb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 212
    iget-object v2, p0, Llwd;->a:[Llwb;

    new-instance v3, Llwb;

    invoke-direct {v3}, Llwb;-><init>()V

    aput-object v3, v2, v0

    .line 213
    iget-object v2, p0, Llwd;->a:[Llwb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 214
    invoke-virtual {p1}, Loxn;->a()I

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 205
    :cond_3
    iget-object v0, p0, Llwd;->a:[Llwb;

    array-length v0, v0

    goto :goto_1

    .line 217
    :cond_4
    iget-object v2, p0, Llwd;->a:[Llwb;

    new-instance v3, Llwb;

    invoke-direct {v3}, Llwb;-><init>()V

    aput-object v3, v2, v0

    .line 218
    iget-object v2, p0, Llwd;->a:[Llwb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 189
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 156
    iget-object v0, p0, Llwd;->a:[Llwb;

    if-eqz v0, :cond_1

    .line 157
    iget-object v1, p0, Llwd;->a:[Llwb;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 158
    if-eqz v3, :cond_0

    .line 159
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 157
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 163
    :cond_1
    iget-object v0, p0, Llwd;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 165
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0, p1}, Llwd;->a(Loxn;)Llwd;

    move-result-object v0

    return-object v0
.end method
