.class public final Loyp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/String;

.field public c:[Lpad;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 16
    sget-object v0, Lpad;->a:[Lpad;

    iput-object v0, p0, Loyp;->c:[Lpad;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 45
    .line 46
    iget-object v0, p0, Loyp;->a:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 47
    const/4 v0, 0x1

    iget-object v2, p0, Loyp;->a:Ljava/lang/Long;

    .line 48
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 50
    :goto_0
    iget-object v2, p0, Loyp;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 51
    const/4 v2, 0x2

    iget-object v3, p0, Loyp;->b:Ljava/lang/String;

    .line 52
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 54
    :cond_0
    iget-object v2, p0, Loyp;->c:[Lpad;

    if-eqz v2, :cond_2

    .line 55
    iget-object v2, p0, Loyp;->c:[Lpad;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 56
    if-eqz v4, :cond_1

    .line 57
    const/4 v5, 0x3

    .line 58
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 55
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 62
    :cond_2
    iget-object v1, p0, Loyp;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 63
    const/4 v1, 0x4

    iget-object v2, p0, Loyp;->d:Ljava/lang/String;

    .line 64
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 66
    :cond_3
    iget-object v1, p0, Loyp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 67
    iput v0, p0, Loyp;->ai:I

    .line 68
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loyp;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 76
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 77
    sparse-switch v0, :sswitch_data_0

    .line 81
    iget-object v2, p0, Loyp;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 82
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loyp;->ah:Ljava/util/List;

    .line 85
    :cond_1
    iget-object v2, p0, Loyp;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    :sswitch_0
    return-object p0

    .line 92
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loyp;->a:Ljava/lang/Long;

    goto :goto_0

    .line 96
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyp;->b:Ljava/lang/String;

    goto :goto_0

    .line 100
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 101
    iget-object v0, p0, Loyp;->c:[Lpad;

    if-nez v0, :cond_3

    move v0, v1

    .line 102
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpad;

    .line 103
    iget-object v3, p0, Loyp;->c:[Lpad;

    if-eqz v3, :cond_2

    .line 104
    iget-object v3, p0, Loyp;->c:[Lpad;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 106
    :cond_2
    iput-object v2, p0, Loyp;->c:[Lpad;

    .line 107
    :goto_2
    iget-object v2, p0, Loyp;->c:[Lpad;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 108
    iget-object v2, p0, Loyp;->c:[Lpad;

    new-instance v3, Lpad;

    invoke-direct {v3}, Lpad;-><init>()V

    aput-object v3, v2, v0

    .line 109
    iget-object v2, p0, Loyp;->c:[Lpad;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 110
    invoke-virtual {p1}, Loxn;->a()I

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 101
    :cond_3
    iget-object v0, p0, Loyp;->c:[Lpad;

    array-length v0, v0

    goto :goto_1

    .line 113
    :cond_4
    iget-object v2, p0, Loyp;->c:[Lpad;

    new-instance v3, Lpad;

    invoke-direct {v3}, Lpad;-><init>()V

    aput-object v3, v2, v0

    .line 114
    iget-object v2, p0, Loyp;->c:[Lpad;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 118
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loyp;->d:Ljava/lang/String;

    goto :goto_0

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 23
    iget-object v0, p0, Loyp;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x1

    iget-object v1, p0, Loyp;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 26
    :cond_0
    iget-object v0, p0, Loyp;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 27
    const/4 v0, 0x2

    iget-object v1, p0, Loyp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 29
    :cond_1
    iget-object v0, p0, Loyp;->c:[Lpad;

    if-eqz v0, :cond_3

    .line 30
    iget-object v1, p0, Loyp;->c:[Lpad;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 31
    if-eqz v3, :cond_2

    .line 32
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 30
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_3
    iget-object v0, p0, Loyp;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 37
    const/4 v0, 0x4

    iget-object v1, p0, Loyp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 39
    :cond_4
    iget-object v0, p0, Loyp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 41
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Loyp;->a(Loxn;)Loyp;

    move-result-object v0

    return-object v0
.end method
