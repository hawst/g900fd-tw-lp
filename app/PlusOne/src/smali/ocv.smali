.class public final Locv;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Locv;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Locu;

.field private d:Locs;

.field private e:Locw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 438
    const/4 v0, 0x0

    new-array v0, v0, [Locv;

    sput-object v0, Locv;->a:[Locv;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 439
    invoke-direct {p0}, Loxq;-><init>()V

    .line 444
    iput-object v0, p0, Locv;->c:Locu;

    .line 447
    iput-object v0, p0, Locv;->d:Locs;

    .line 450
    iput-object v0, p0, Locv;->e:Locw;

    .line 439
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 473
    const/4 v0, 0x0

    .line 474
    iget-object v1, p0, Locv;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 475
    const/4 v0, 0x1

    iget-object v1, p0, Locv;->b:Ljava/lang/String;

    .line 476
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 478
    :cond_0
    iget-object v1, p0, Locv;->d:Locs;

    if-eqz v1, :cond_1

    .line 479
    const/4 v1, 0x2

    iget-object v2, p0, Locv;->d:Locs;

    .line 480
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 482
    :cond_1
    iget-object v1, p0, Locv;->e:Locw;

    if-eqz v1, :cond_2

    .line 483
    const/4 v1, 0x3

    iget-object v2, p0, Locv;->e:Locw;

    .line 484
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 486
    :cond_2
    iget-object v1, p0, Locv;->c:Locu;

    if-eqz v1, :cond_3

    .line 487
    const/4 v1, 0x4

    iget-object v2, p0, Locv;->c:Locu;

    .line 488
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    :cond_3
    iget-object v1, p0, Locv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 491
    iput v0, p0, Locv;->ai:I

    .line 492
    return v0
.end method

.method public a(Loxn;)Locv;
    .locals 2

    .prologue
    .line 500
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 501
    sparse-switch v0, :sswitch_data_0

    .line 505
    iget-object v1, p0, Locv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 506
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Locv;->ah:Ljava/util/List;

    .line 509
    :cond_1
    iget-object v1, p0, Locv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 511
    :sswitch_0
    return-object p0

    .line 516
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Locv;->b:Ljava/lang/String;

    goto :goto_0

    .line 520
    :sswitch_2
    iget-object v0, p0, Locv;->d:Locs;

    if-nez v0, :cond_2

    .line 521
    new-instance v0, Locs;

    invoke-direct {v0}, Locs;-><init>()V

    iput-object v0, p0, Locv;->d:Locs;

    .line 523
    :cond_2
    iget-object v0, p0, Locv;->d:Locs;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 527
    :sswitch_3
    iget-object v0, p0, Locv;->e:Locw;

    if-nez v0, :cond_3

    .line 528
    new-instance v0, Locw;

    invoke-direct {v0}, Locw;-><init>()V

    iput-object v0, p0, Locv;->e:Locw;

    .line 530
    :cond_3
    iget-object v0, p0, Locv;->e:Locw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 534
    :sswitch_4
    iget-object v0, p0, Locv;->c:Locu;

    if-nez v0, :cond_4

    .line 535
    new-instance v0, Locu;

    invoke-direct {v0}, Locu;-><init>()V

    iput-object v0, p0, Locv;->c:Locu;

    .line 537
    :cond_4
    iget-object v0, p0, Locv;->c:Locu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 501
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Locv;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 456
    const/4 v0, 0x1

    iget-object v1, p0, Locv;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 458
    :cond_0
    iget-object v0, p0, Locv;->d:Locs;

    if-eqz v0, :cond_1

    .line 459
    const/4 v0, 0x2

    iget-object v1, p0, Locv;->d:Locs;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 461
    :cond_1
    iget-object v0, p0, Locv;->e:Locw;

    if-eqz v0, :cond_2

    .line 462
    const/4 v0, 0x3

    iget-object v1, p0, Locv;->e:Locw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 464
    :cond_2
    iget-object v0, p0, Locv;->c:Locu;

    if-eqz v0, :cond_3

    .line 465
    const/4 v0, 0x4

    iget-object v1, p0, Locv;->c:Locu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 467
    :cond_3
    iget-object v0, p0, Locv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 469
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 435
    invoke-virtual {p0, p1}, Locv;->a(Loxn;)Locv;

    move-result-object v0

    return-object v0
.end method
