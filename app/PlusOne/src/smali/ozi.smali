.class public final Lozi;
.super Loxq;
.source "PG"


# static fields
.field public static final a:Loxr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Loxr",
            "<",
            "Lozi;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/Integer;

.field private C:Ljava/lang/String;

.field private D:Loya;

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Loya;

.field private I:Loya;

.field private J:Loya;

.field private K:Ljava/lang/Integer;

.field private L:[I

.field private M:[Loya;

.field private N:Ljava/lang/Long;

.field private O:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Loya;

.field public d:Lpnk;

.field public e:I

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:[Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lpdi;

.field private n:Ljava/lang/String;

.field private o:[Loya;

.field private p:Loya;

.field private q:[Loya;

.field private r:Ljava/lang/String;

.field private s:[Loya;

.field private t:Ljava/lang/Boolean;

.field private u:Ljava/lang/Integer;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/Boolean;

.field private y:Ljava/lang/Boolean;

.field private z:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x274c591

    new-instance v1, Lozj;

    invoke-direct {v1}, Lozj;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    move-result-object v0

    sput-object v0, Lozi;->a:Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27
    iput-object v1, p0, Lozi;->m:Lpdi;

    .line 32
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lozi;->o:[Loya;

    .line 35
    iput-object v1, p0, Lozi;->p:Loya;

    .line 38
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lozi;->q:[Loya;

    .line 43
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lozi;->s:[Loya;

    .line 46
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lozi;->c:[Loya;

    .line 49
    iput-object v1, p0, Lozi;->d:Lpnk;

    .line 52
    const/high16 v0, -0x80000000

    iput v0, p0, Lozi;->e:I

    .line 79
    iput-object v1, p0, Lozi;->D:Loya;

    .line 88
    iput-object v1, p0, Lozi;->H:Loya;

    .line 91
    iput-object v1, p0, Lozi;->I:Loya;

    .line 94
    iput-object v1, p0, Lozi;->J:Loya;

    .line 99
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lozi;->h:[Ljava/lang/String;

    .line 102
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lozi;->L:[I

    .line 105
    sget-object v0, Loya;->a:[Loya;

    iput-object v0, p0, Lozi;->M:[Loya;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 264
    .line 265
    iget-object v0, p0, Lozi;->i:Ljava/lang/String;

    if-eqz v0, :cond_2e

    .line 266
    const/4 v0, 0x1

    iget-object v2, p0, Lozi;->i:Ljava/lang/String;

    .line 267
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 269
    :goto_0
    iget-object v2, p0, Lozi;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 270
    const/4 v2, 0x2

    iget-object v3, p0, Lozi;->j:Ljava/lang/String;

    .line 271
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 273
    :cond_0
    iget-object v2, p0, Lozi;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 274
    const/4 v2, 0x3

    iget-object v3, p0, Lozi;->b:Ljava/lang/String;

    .line 275
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 277
    :cond_1
    iget-object v2, p0, Lozi;->k:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 278
    const/4 v2, 0x4

    iget-object v3, p0, Lozi;->k:Ljava/lang/String;

    .line 279
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 281
    :cond_2
    iget-object v2, p0, Lozi;->l:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 282
    const/4 v2, 0x5

    iget-object v3, p0, Lozi;->l:Ljava/lang/String;

    .line 283
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 285
    :cond_3
    iget-object v2, p0, Lozi;->m:Lpdi;

    if-eqz v2, :cond_4

    .line 286
    const/4 v2, 0x6

    iget-object v3, p0, Lozi;->m:Lpdi;

    .line 287
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 289
    :cond_4
    iget-object v2, p0, Lozi;->n:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 290
    const/4 v2, 0x7

    iget-object v3, p0, Lozi;->n:Ljava/lang/String;

    .line 291
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 293
    :cond_5
    iget-object v2, p0, Lozi;->o:[Loya;

    if-eqz v2, :cond_7

    .line 294
    iget-object v3, p0, Lozi;->o:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 295
    if-eqz v5, :cond_6

    .line 296
    const/16 v6, 0x8

    .line 297
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 294
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 301
    :cond_7
    iget-object v2, p0, Lozi;->p:Loya;

    if-eqz v2, :cond_8

    .line 302
    const/16 v2, 0x9

    iget-object v3, p0, Lozi;->p:Loya;

    .line 303
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 305
    :cond_8
    iget-object v2, p0, Lozi;->q:[Loya;

    if-eqz v2, :cond_a

    .line 306
    iget-object v3, p0, Lozi;->q:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_a

    aget-object v5, v3, v2

    .line 307
    if-eqz v5, :cond_9

    .line 308
    const/16 v6, 0xb

    .line 309
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 306
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 313
    :cond_a
    iget-object v2, p0, Lozi;->r:Ljava/lang/String;

    if-eqz v2, :cond_b

    .line 314
    const/16 v2, 0xc

    iget-object v3, p0, Lozi;->r:Ljava/lang/String;

    .line 315
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 317
    :cond_b
    iget-object v2, p0, Lozi;->s:[Loya;

    if-eqz v2, :cond_d

    .line 318
    iget-object v3, p0, Lozi;->s:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_d

    aget-object v5, v3, v2

    .line 319
    if-eqz v5, :cond_c

    .line 320
    const/16 v6, 0x2a

    .line 321
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 318
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 325
    :cond_d
    iget-object v2, p0, Lozi;->c:[Loya;

    if-eqz v2, :cond_f

    .line 326
    iget-object v3, p0, Lozi;->c:[Loya;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_f

    aget-object v5, v3, v2

    .line 327
    if-eqz v5, :cond_e

    .line 328
    const/16 v6, 0x32

    .line 329
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 326
    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 333
    :cond_f
    iget-object v2, p0, Lozi;->d:Lpnk;

    if-eqz v2, :cond_10

    .line 334
    const/16 v2, 0x33

    iget-object v3, p0, Lozi;->d:Lpnk;

    .line 335
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 337
    :cond_10
    iget v2, p0, Lozi;->e:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_11

    .line 338
    const/16 v2, 0x34

    iget v3, p0, Lozi;->e:I

    .line 339
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 341
    :cond_11
    iget-object v2, p0, Lozi;->t:Ljava/lang/Boolean;

    if-eqz v2, :cond_12

    .line 342
    const/16 v2, 0x35

    iget-object v3, p0, Lozi;->t:Ljava/lang/Boolean;

    .line 343
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 345
    :cond_12
    iget-object v2, p0, Lozi;->u:Ljava/lang/Integer;

    if-eqz v2, :cond_13

    .line 346
    const/16 v2, 0x36

    iget-object v3, p0, Lozi;->u:Ljava/lang/Integer;

    .line 347
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 349
    :cond_13
    iget-object v2, p0, Lozi;->v:Ljava/lang/String;

    if-eqz v2, :cond_14

    .line 350
    const/16 v2, 0x37

    iget-object v3, p0, Lozi;->v:Ljava/lang/String;

    .line 351
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 353
    :cond_14
    iget-object v2, p0, Lozi;->w:Ljava/lang/String;

    if-eqz v2, :cond_15

    .line 354
    const/16 v2, 0x38

    iget-object v3, p0, Lozi;->w:Ljava/lang/String;

    .line 355
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 357
    :cond_15
    iget-object v2, p0, Lozi;->x:Ljava/lang/Boolean;

    if-eqz v2, :cond_16

    .line 358
    const/16 v2, 0x39

    iget-object v3, p0, Lozi;->x:Ljava/lang/Boolean;

    .line 359
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 361
    :cond_16
    iget-object v2, p0, Lozi;->y:Ljava/lang/Boolean;

    if-eqz v2, :cond_17

    .line 362
    const/16 v2, 0x3a

    iget-object v3, p0, Lozi;->y:Ljava/lang/Boolean;

    .line 363
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 365
    :cond_17
    iget-object v2, p0, Lozi;->z:Ljava/lang/Boolean;

    if-eqz v2, :cond_18

    .line 366
    const/16 v2, 0x3b

    iget-object v3, p0, Lozi;->z:Ljava/lang/Boolean;

    .line 367
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 369
    :cond_18
    iget-object v2, p0, Lozi;->f:Ljava/lang/String;

    if-eqz v2, :cond_19

    .line 370
    const/16 v2, 0x3c

    iget-object v3, p0, Lozi;->f:Ljava/lang/String;

    .line 371
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 373
    :cond_19
    iget-object v2, p0, Lozi;->g:Ljava/lang/String;

    if-eqz v2, :cond_1a

    .line 374
    const/16 v2, 0x3d

    iget-object v3, p0, Lozi;->g:Ljava/lang/String;

    .line 375
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 377
    :cond_1a
    iget-object v2, p0, Lozi;->A:Ljava/lang/String;

    if-eqz v2, :cond_1b

    .line 378
    const/16 v2, 0x3f

    iget-object v3, p0, Lozi;->A:Ljava/lang/String;

    .line 379
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 381
    :cond_1b
    iget-object v2, p0, Lozi;->B:Ljava/lang/Integer;

    if-eqz v2, :cond_1c

    .line 382
    const/16 v2, 0x40

    iget-object v3, p0, Lozi;->B:Ljava/lang/Integer;

    .line 383
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 385
    :cond_1c
    iget-object v2, p0, Lozi;->C:Ljava/lang/String;

    if-eqz v2, :cond_1d

    .line 386
    const/16 v2, 0x44

    iget-object v3, p0, Lozi;->C:Ljava/lang/String;

    .line 387
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 389
    :cond_1d
    iget-object v2, p0, Lozi;->D:Loya;

    if-eqz v2, :cond_1e

    .line 390
    const/16 v2, 0x49

    iget-object v3, p0, Lozi;->D:Loya;

    .line 391
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 393
    :cond_1e
    iget-object v2, p0, Lozi;->E:Ljava/lang/String;

    if-eqz v2, :cond_1f

    .line 394
    const/16 v2, 0x4a

    iget-object v3, p0, Lozi;->E:Ljava/lang/String;

    .line 395
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 397
    :cond_1f
    iget-object v2, p0, Lozi;->F:Ljava/lang/String;

    if-eqz v2, :cond_20

    .line 398
    const/16 v2, 0x4b

    iget-object v3, p0, Lozi;->F:Ljava/lang/String;

    .line 399
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 401
    :cond_20
    iget-object v2, p0, Lozi;->G:Ljava/lang/String;

    if-eqz v2, :cond_21

    .line 402
    const/16 v2, 0xa3

    iget-object v3, p0, Lozi;->G:Ljava/lang/String;

    .line 403
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 405
    :cond_21
    iget-object v2, p0, Lozi;->H:Loya;

    if-eqz v2, :cond_22

    .line 406
    const/16 v2, 0xa7

    iget-object v3, p0, Lozi;->H:Loya;

    .line 407
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 409
    :cond_22
    iget-object v2, p0, Lozi;->I:Loya;

    if-eqz v2, :cond_23

    .line 410
    const/16 v2, 0xa8

    iget-object v3, p0, Lozi;->I:Loya;

    .line 411
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 413
    :cond_23
    iget-object v2, p0, Lozi;->J:Loya;

    if-eqz v2, :cond_24

    .line 414
    const/16 v2, 0xb9

    iget-object v3, p0, Lozi;->J:Loya;

    .line 415
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 417
    :cond_24
    iget-object v2, p0, Lozi;->K:Ljava/lang/Integer;

    if-eqz v2, :cond_25

    .line 418
    const/16 v2, 0xba

    iget-object v3, p0, Lozi;->K:Ljava/lang/Integer;

    .line 419
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 421
    :cond_25
    iget-object v2, p0, Lozi;->h:[Ljava/lang/String;

    if-eqz v2, :cond_27

    iget-object v2, p0, Lozi;->h:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_27

    .line 423
    iget-object v4, p0, Lozi;->h:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_5
    if-ge v2, v5, :cond_26

    aget-object v6, v4, v2

    .line 425
    invoke-static {v6}, Loxo;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    .line 423
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 427
    :cond_26
    add-int/2addr v0, v3

    .line 428
    iget-object v2, p0, Lozi;->h:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 430
    :cond_27
    iget-object v2, p0, Lozi;->L:[I

    if-eqz v2, :cond_29

    iget-object v2, p0, Lozi;->L:[I

    array-length v2, v2

    if-lez v2, :cond_29

    .line 432
    iget-object v4, p0, Lozi;->L:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_6
    if-ge v2, v5, :cond_28

    aget v6, v4, v2

    .line 434
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 432
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 436
    :cond_28
    add-int/2addr v0, v3

    .line 437
    iget-object v2, p0, Lozi;->L:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 439
    :cond_29
    iget-object v2, p0, Lozi;->M:[Loya;

    if-eqz v2, :cond_2b

    .line 440
    iget-object v2, p0, Lozi;->M:[Loya;

    array-length v3, v2

    :goto_7
    if-ge v1, v3, :cond_2b

    aget-object v4, v2, v1

    .line 441
    if-eqz v4, :cond_2a

    .line 442
    const/16 v5, 0xcf

    .line 443
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 440
    :cond_2a
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 447
    :cond_2b
    iget-object v1, p0, Lozi;->N:Ljava/lang/Long;

    if-eqz v1, :cond_2c

    .line 448
    const/16 v1, 0xe0

    iget-object v2, p0, Lozi;->N:Ljava/lang/Long;

    .line 449
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 451
    :cond_2c
    iget-object v1, p0, Lozi;->O:Ljava/lang/String;

    if-eqz v1, :cond_2d

    .line 452
    const/16 v1, 0xfe

    iget-object v2, p0, Lozi;->O:Ljava/lang/String;

    .line 453
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 455
    :cond_2d
    iget-object v1, p0, Lozi;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 456
    iput v0, p0, Lozi;->ai:I

    .line 457
    return v0

    :cond_2e
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lozi;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 465
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 466
    sparse-switch v0, :sswitch_data_0

    .line 470
    iget-object v2, p0, Lozi;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 471
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lozi;->ah:Ljava/util/List;

    .line 474
    :cond_1
    iget-object v2, p0, Lozi;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 476
    :sswitch_0
    return-object p0

    .line 481
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->i:Ljava/lang/String;

    goto :goto_0

    .line 485
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->j:Ljava/lang/String;

    goto :goto_0

    .line 489
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->b:Ljava/lang/String;

    goto :goto_0

    .line 493
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->k:Ljava/lang/String;

    goto :goto_0

    .line 497
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->l:Ljava/lang/String;

    goto :goto_0

    .line 501
    :sswitch_6
    iget-object v0, p0, Lozi;->m:Lpdi;

    if-nez v0, :cond_2

    .line 502
    new-instance v0, Lpdi;

    invoke-direct {v0}, Lpdi;-><init>()V

    iput-object v0, p0, Lozi;->m:Lpdi;

    .line 504
    :cond_2
    iget-object v0, p0, Lozi;->m:Lpdi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 508
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->n:Ljava/lang/String;

    goto :goto_0

    .line 512
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 513
    iget-object v0, p0, Lozi;->o:[Loya;

    if-nez v0, :cond_4

    move v0, v1

    .line 514
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 515
    iget-object v3, p0, Lozi;->o:[Loya;

    if-eqz v3, :cond_3

    .line 516
    iget-object v3, p0, Lozi;->o:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 518
    :cond_3
    iput-object v2, p0, Lozi;->o:[Loya;

    .line 519
    :goto_2
    iget-object v2, p0, Lozi;->o:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 520
    iget-object v2, p0, Lozi;->o:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 521
    iget-object v2, p0, Lozi;->o:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 522
    invoke-virtual {p1}, Loxn;->a()I

    .line 519
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 513
    :cond_4
    iget-object v0, p0, Lozi;->o:[Loya;

    array-length v0, v0

    goto :goto_1

    .line 525
    :cond_5
    iget-object v2, p0, Lozi;->o:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 526
    iget-object v2, p0, Lozi;->o:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 530
    :sswitch_9
    iget-object v0, p0, Lozi;->p:Loya;

    if-nez v0, :cond_6

    .line 531
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozi;->p:Loya;

    .line 533
    :cond_6
    iget-object v0, p0, Lozi;->p:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 537
    :sswitch_a
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 538
    iget-object v0, p0, Lozi;->q:[Loya;

    if-nez v0, :cond_8

    move v0, v1

    .line 539
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 540
    iget-object v3, p0, Lozi;->q:[Loya;

    if-eqz v3, :cond_7

    .line 541
    iget-object v3, p0, Lozi;->q:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 543
    :cond_7
    iput-object v2, p0, Lozi;->q:[Loya;

    .line 544
    :goto_4
    iget-object v2, p0, Lozi;->q:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 545
    iget-object v2, p0, Lozi;->q:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 546
    iget-object v2, p0, Lozi;->q:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 547
    invoke-virtual {p1}, Loxn;->a()I

    .line 544
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 538
    :cond_8
    iget-object v0, p0, Lozi;->q:[Loya;

    array-length v0, v0

    goto :goto_3

    .line 550
    :cond_9
    iget-object v2, p0, Lozi;->q:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 551
    iget-object v2, p0, Lozi;->q:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 555
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->r:Ljava/lang/String;

    goto/16 :goto_0

    .line 559
    :sswitch_c
    const/16 v0, 0x152

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 560
    iget-object v0, p0, Lozi;->s:[Loya;

    if-nez v0, :cond_b

    move v0, v1

    .line 561
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 562
    iget-object v3, p0, Lozi;->s:[Loya;

    if-eqz v3, :cond_a

    .line 563
    iget-object v3, p0, Lozi;->s:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 565
    :cond_a
    iput-object v2, p0, Lozi;->s:[Loya;

    .line 566
    :goto_6
    iget-object v2, p0, Lozi;->s:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    .line 567
    iget-object v2, p0, Lozi;->s:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 568
    iget-object v2, p0, Lozi;->s:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 569
    invoke-virtual {p1}, Loxn;->a()I

    .line 566
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 560
    :cond_b
    iget-object v0, p0, Lozi;->s:[Loya;

    array-length v0, v0

    goto :goto_5

    .line 572
    :cond_c
    iget-object v2, p0, Lozi;->s:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 573
    iget-object v2, p0, Lozi;->s:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 577
    :sswitch_d
    const/16 v0, 0x192

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 578
    iget-object v0, p0, Lozi;->c:[Loya;

    if-nez v0, :cond_e

    move v0, v1

    .line 579
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 580
    iget-object v3, p0, Lozi;->c:[Loya;

    if-eqz v3, :cond_d

    .line 581
    iget-object v3, p0, Lozi;->c:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 583
    :cond_d
    iput-object v2, p0, Lozi;->c:[Loya;

    .line 584
    :goto_8
    iget-object v2, p0, Lozi;->c:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    .line 585
    iget-object v2, p0, Lozi;->c:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 586
    iget-object v2, p0, Lozi;->c:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 587
    invoke-virtual {p1}, Loxn;->a()I

    .line 584
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 578
    :cond_e
    iget-object v0, p0, Lozi;->c:[Loya;

    array-length v0, v0

    goto :goto_7

    .line 590
    :cond_f
    iget-object v2, p0, Lozi;->c:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 591
    iget-object v2, p0, Lozi;->c:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 595
    :sswitch_e
    iget-object v0, p0, Lozi;->d:Lpnk;

    if-nez v0, :cond_10

    .line 596
    new-instance v0, Lpnk;

    invoke-direct {v0}, Lpnk;-><init>()V

    iput-object v0, p0, Lozi;->d:Lpnk;

    .line 598
    :cond_10
    iget-object v0, p0, Lozi;->d:Lpnk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 602
    :sswitch_f
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 603
    if-eqz v0, :cond_11

    const/4 v2, 0x1

    if-ne v0, v2, :cond_12

    .line 605
    :cond_11
    iput v0, p0, Lozi;->e:I

    goto/16 :goto_0

    .line 607
    :cond_12
    iput v1, p0, Lozi;->e:I

    goto/16 :goto_0

    .line 612
    :sswitch_10
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lozi;->t:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 616
    :sswitch_11
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lozi;->u:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 620
    :sswitch_12
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->v:Ljava/lang/String;

    goto/16 :goto_0

    .line 624
    :sswitch_13
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 628
    :sswitch_14
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lozi;->x:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 632
    :sswitch_15
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lozi;->y:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 636
    :sswitch_16
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lozi;->z:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 640
    :sswitch_17
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 644
    :sswitch_18
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->g:Ljava/lang/String;

    goto/16 :goto_0

    .line 648
    :sswitch_19
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->A:Ljava/lang/String;

    goto/16 :goto_0

    .line 652
    :sswitch_1a
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lozi;->B:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 656
    :sswitch_1b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 660
    :sswitch_1c
    iget-object v0, p0, Lozi;->D:Loya;

    if-nez v0, :cond_13

    .line 661
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozi;->D:Loya;

    .line 663
    :cond_13
    iget-object v0, p0, Lozi;->D:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 667
    :sswitch_1d
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->E:Ljava/lang/String;

    goto/16 :goto_0

    .line 671
    :sswitch_1e
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->F:Ljava/lang/String;

    goto/16 :goto_0

    .line 675
    :sswitch_1f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->G:Ljava/lang/String;

    goto/16 :goto_0

    .line 679
    :sswitch_20
    iget-object v0, p0, Lozi;->H:Loya;

    if-nez v0, :cond_14

    .line 680
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozi;->H:Loya;

    .line 682
    :cond_14
    iget-object v0, p0, Lozi;->H:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 686
    :sswitch_21
    iget-object v0, p0, Lozi;->I:Loya;

    if-nez v0, :cond_15

    .line 687
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozi;->I:Loya;

    .line 689
    :cond_15
    iget-object v0, p0, Lozi;->I:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 693
    :sswitch_22
    iget-object v0, p0, Lozi;->J:Loya;

    if-nez v0, :cond_16

    .line 694
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lozi;->J:Loya;

    .line 696
    :cond_16
    iget-object v0, p0, Lozi;->J:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 700
    :sswitch_23
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lozi;->K:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 704
    :sswitch_24
    const/16 v0, 0x612

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 705
    iget-object v0, p0, Lozi;->h:[Ljava/lang/String;

    array-length v0, v0

    .line 706
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    .line 707
    iget-object v3, p0, Lozi;->h:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 708
    iput-object v2, p0, Lozi;->h:[Ljava/lang/String;

    .line 709
    :goto_9
    iget-object v2, p0, Lozi;->h:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_17

    .line 710
    iget-object v2, p0, Lozi;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 711
    invoke-virtual {p1}, Loxn;->a()I

    .line 709
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 714
    :cond_17
    iget-object v2, p0, Lozi;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    .line 718
    :sswitch_25
    const/16 v0, 0x638

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 719
    iget-object v0, p0, Lozi;->L:[I

    array-length v0, v0

    .line 720
    add-int/2addr v2, v0

    new-array v2, v2, [I

    .line 721
    iget-object v3, p0, Lozi;->L:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 722
    iput-object v2, p0, Lozi;->L:[I

    .line 723
    :goto_a
    iget-object v2, p0, Lozi;->L:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_18

    .line 724
    iget-object v2, p0, Lozi;->L:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    .line 725
    invoke-virtual {p1}, Loxn;->a()I

    .line 723
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 728
    :cond_18
    iget-object v2, p0, Lozi;->L:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v3

    aput v3, v2, v0

    goto/16 :goto_0

    .line 732
    :sswitch_26
    const/16 v0, 0x67a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 733
    iget-object v0, p0, Lozi;->M:[Loya;

    if-nez v0, :cond_1a

    move v0, v1

    .line 734
    :goto_b
    add-int/2addr v2, v0

    new-array v2, v2, [Loya;

    .line 735
    iget-object v3, p0, Lozi;->M:[Loya;

    if-eqz v3, :cond_19

    .line 736
    iget-object v3, p0, Lozi;->M:[Loya;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 738
    :cond_19
    iput-object v2, p0, Lozi;->M:[Loya;

    .line 739
    :goto_c
    iget-object v2, p0, Lozi;->M:[Loya;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1b

    .line 740
    iget-object v2, p0, Lozi;->M:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 741
    iget-object v2, p0, Lozi;->M:[Loya;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 742
    invoke-virtual {p1}, Loxn;->a()I

    .line 739
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 733
    :cond_1a
    iget-object v0, p0, Lozi;->M:[Loya;

    array-length v0, v0

    goto :goto_b

    .line 745
    :cond_1b
    iget-object v2, p0, Lozi;->M:[Loya;

    new-instance v3, Loya;

    invoke-direct {v3}, Loya;-><init>()V

    aput-object v3, v2, v0

    .line 746
    iget-object v2, p0, Lozi;->M:[Loya;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 750
    :sswitch_27
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lozi;->N:Ljava/lang/Long;

    goto/16 :goto_0

    .line 754
    :sswitch_28
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lozi;->O:Ljava/lang/String;

    goto/16 :goto_0

    .line 466
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x152 -> :sswitch_c
        0x192 -> :sswitch_d
        0x19a -> :sswitch_e
        0x1a0 -> :sswitch_f
        0x1a8 -> :sswitch_10
        0x1b0 -> :sswitch_11
        0x1ba -> :sswitch_12
        0x1c2 -> :sswitch_13
        0x1c8 -> :sswitch_14
        0x1d0 -> :sswitch_15
        0x1d8 -> :sswitch_16
        0x1e2 -> :sswitch_17
        0x1ea -> :sswitch_18
        0x1fa -> :sswitch_19
        0x200 -> :sswitch_1a
        0x222 -> :sswitch_1b
        0x24a -> :sswitch_1c
        0x252 -> :sswitch_1d
        0x25a -> :sswitch_1e
        0x51a -> :sswitch_1f
        0x53a -> :sswitch_20
        0x542 -> :sswitch_21
        0x5ca -> :sswitch_22
        0x5d0 -> :sswitch_23
        0x612 -> :sswitch_24
        0x638 -> :sswitch_25
        0x67a -> :sswitch_26
        0x700 -> :sswitch_27
        0x7f2 -> :sswitch_28
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 114
    iget-object v1, p0, Lozi;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 115
    const/4 v1, 0x1

    iget-object v2, p0, Lozi;->i:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 117
    :cond_0
    iget-object v1, p0, Lozi;->j:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 118
    const/4 v1, 0x2

    iget-object v2, p0, Lozi;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 120
    :cond_1
    iget-object v1, p0, Lozi;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 121
    const/4 v1, 0x3

    iget-object v2, p0, Lozi;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 123
    :cond_2
    iget-object v1, p0, Lozi;->k:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 124
    const/4 v1, 0x4

    iget-object v2, p0, Lozi;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 126
    :cond_3
    iget-object v1, p0, Lozi;->l:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 127
    const/4 v1, 0x5

    iget-object v2, p0, Lozi;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 129
    :cond_4
    iget-object v1, p0, Lozi;->m:Lpdi;

    if-eqz v1, :cond_5

    .line 130
    const/4 v1, 0x6

    iget-object v2, p0, Lozi;->m:Lpdi;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 132
    :cond_5
    iget-object v1, p0, Lozi;->n:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 133
    const/4 v1, 0x7

    iget-object v2, p0, Lozi;->n:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 135
    :cond_6
    iget-object v1, p0, Lozi;->o:[Loya;

    if-eqz v1, :cond_8

    .line 136
    iget-object v2, p0, Lozi;->o:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 137
    if-eqz v4, :cond_7

    .line 138
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 136
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    :cond_8
    iget-object v1, p0, Lozi;->p:Loya;

    if-eqz v1, :cond_9

    .line 143
    const/16 v1, 0x9

    iget-object v2, p0, Lozi;->p:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 145
    :cond_9
    iget-object v1, p0, Lozi;->q:[Loya;

    if-eqz v1, :cond_b

    .line 146
    iget-object v2, p0, Lozi;->q:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 147
    if-eqz v4, :cond_a

    .line 148
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 146
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 152
    :cond_b
    iget-object v1, p0, Lozi;->r:Ljava/lang/String;

    if-eqz v1, :cond_c

    .line 153
    const/16 v1, 0xc

    iget-object v2, p0, Lozi;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 155
    :cond_c
    iget-object v1, p0, Lozi;->s:[Loya;

    if-eqz v1, :cond_e

    .line 156
    iget-object v2, p0, Lozi;->s:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_e

    aget-object v4, v2, v1

    .line 157
    if-eqz v4, :cond_d

    .line 158
    const/16 v5, 0x2a

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 156
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 162
    :cond_e
    iget-object v1, p0, Lozi;->c:[Loya;

    if-eqz v1, :cond_10

    .line 163
    iget-object v2, p0, Lozi;->c:[Loya;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 164
    if-eqz v4, :cond_f

    .line 165
    const/16 v5, 0x32

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 163
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 169
    :cond_10
    iget-object v1, p0, Lozi;->d:Lpnk;

    if-eqz v1, :cond_11

    .line 170
    const/16 v1, 0x33

    iget-object v2, p0, Lozi;->d:Lpnk;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 172
    :cond_11
    iget v1, p0, Lozi;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_12

    .line 173
    const/16 v1, 0x34

    iget v2, p0, Lozi;->e:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 175
    :cond_12
    iget-object v1, p0, Lozi;->t:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    .line 176
    const/16 v1, 0x35

    iget-object v2, p0, Lozi;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 178
    :cond_13
    iget-object v1, p0, Lozi;->u:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    .line 179
    const/16 v1, 0x36

    iget-object v2, p0, Lozi;->u:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 181
    :cond_14
    iget-object v1, p0, Lozi;->v:Ljava/lang/String;

    if-eqz v1, :cond_15

    .line 182
    const/16 v1, 0x37

    iget-object v2, p0, Lozi;->v:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 184
    :cond_15
    iget-object v1, p0, Lozi;->w:Ljava/lang/String;

    if-eqz v1, :cond_16

    .line 185
    const/16 v1, 0x38

    iget-object v2, p0, Lozi;->w:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 187
    :cond_16
    iget-object v1, p0, Lozi;->x:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    .line 188
    const/16 v1, 0x39

    iget-object v2, p0, Lozi;->x:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 190
    :cond_17
    iget-object v1, p0, Lozi;->y:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    .line 191
    const/16 v1, 0x3a

    iget-object v2, p0, Lozi;->y:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 193
    :cond_18
    iget-object v1, p0, Lozi;->z:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    .line 194
    const/16 v1, 0x3b

    iget-object v2, p0, Lozi;->z:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 196
    :cond_19
    iget-object v1, p0, Lozi;->f:Ljava/lang/String;

    if-eqz v1, :cond_1a

    .line 197
    const/16 v1, 0x3c

    iget-object v2, p0, Lozi;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 199
    :cond_1a
    iget-object v1, p0, Lozi;->g:Ljava/lang/String;

    if-eqz v1, :cond_1b

    .line 200
    const/16 v1, 0x3d

    iget-object v2, p0, Lozi;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 202
    :cond_1b
    iget-object v1, p0, Lozi;->A:Ljava/lang/String;

    if-eqz v1, :cond_1c

    .line 203
    const/16 v1, 0x3f

    iget-object v2, p0, Lozi;->A:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 205
    :cond_1c
    iget-object v1, p0, Lozi;->B:Ljava/lang/Integer;

    if-eqz v1, :cond_1d

    .line 206
    const/16 v1, 0x40

    iget-object v2, p0, Lozi;->B:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 208
    :cond_1d
    iget-object v1, p0, Lozi;->C:Ljava/lang/String;

    if-eqz v1, :cond_1e

    .line 209
    const/16 v1, 0x44

    iget-object v2, p0, Lozi;->C:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 211
    :cond_1e
    iget-object v1, p0, Lozi;->D:Loya;

    if-eqz v1, :cond_1f

    .line 212
    const/16 v1, 0x49

    iget-object v2, p0, Lozi;->D:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 214
    :cond_1f
    iget-object v1, p0, Lozi;->E:Ljava/lang/String;

    if-eqz v1, :cond_20

    .line 215
    const/16 v1, 0x4a

    iget-object v2, p0, Lozi;->E:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 217
    :cond_20
    iget-object v1, p0, Lozi;->F:Ljava/lang/String;

    if-eqz v1, :cond_21

    .line 218
    const/16 v1, 0x4b

    iget-object v2, p0, Lozi;->F:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 220
    :cond_21
    iget-object v1, p0, Lozi;->G:Ljava/lang/String;

    if-eqz v1, :cond_22

    .line 221
    const/16 v1, 0xa3

    iget-object v2, p0, Lozi;->G:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 223
    :cond_22
    iget-object v1, p0, Lozi;->H:Loya;

    if-eqz v1, :cond_23

    .line 224
    const/16 v1, 0xa7

    iget-object v2, p0, Lozi;->H:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 226
    :cond_23
    iget-object v1, p0, Lozi;->I:Loya;

    if-eqz v1, :cond_24

    .line 227
    const/16 v1, 0xa8

    iget-object v2, p0, Lozi;->I:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 229
    :cond_24
    iget-object v1, p0, Lozi;->J:Loya;

    if-eqz v1, :cond_25

    .line 230
    const/16 v1, 0xb9

    iget-object v2, p0, Lozi;->J:Loya;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 232
    :cond_25
    iget-object v1, p0, Lozi;->K:Ljava/lang/Integer;

    if-eqz v1, :cond_26

    .line 233
    const/16 v1, 0xba

    iget-object v2, p0, Lozi;->K:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 235
    :cond_26
    iget-object v1, p0, Lozi;->h:[Ljava/lang/String;

    if-eqz v1, :cond_27

    .line 236
    iget-object v2, p0, Lozi;->h:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_27

    aget-object v4, v2, v1

    .line 237
    const/16 v5, 0xc2

    invoke-virtual {p1, v5, v4}, Loxo;->a(ILjava/lang/String;)V

    .line 236
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 240
    :cond_27
    iget-object v1, p0, Lozi;->L:[I

    if-eqz v1, :cond_28

    iget-object v1, p0, Lozi;->L:[I

    array-length v1, v1

    if-lez v1, :cond_28

    .line 241
    iget-object v2, p0, Lozi;->L:[I

    array-length v3, v2

    move v1, v0

    :goto_5
    if-ge v1, v3, :cond_28

    aget v4, v2, v1

    .line 242
    const/16 v5, 0xc7

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 241
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 245
    :cond_28
    iget-object v1, p0, Lozi;->M:[Loya;

    if-eqz v1, :cond_2a

    .line 246
    iget-object v1, p0, Lozi;->M:[Loya;

    array-length v2, v1

    :goto_6
    if-ge v0, v2, :cond_2a

    aget-object v3, v1, v0

    .line 247
    if-eqz v3, :cond_29

    .line 248
    const/16 v4, 0xcf

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 246
    :cond_29
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 252
    :cond_2a
    iget-object v0, p0, Lozi;->N:Ljava/lang/Long;

    if-eqz v0, :cond_2b

    .line 253
    const/16 v0, 0xe0

    iget-object v1, p0, Lozi;->N:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 255
    :cond_2b
    iget-object v0, p0, Lozi;->O:Ljava/lang/String;

    if-eqz v0, :cond_2c

    .line 256
    const/16 v0, 0xfe

    iget-object v1, p0, Lozi;->O:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 258
    :cond_2c
    iget-object v0, p0, Lozi;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 260
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lozi;->a(Loxn;)Lozi;

    move-result-object v0

    return-object v0
.end method
