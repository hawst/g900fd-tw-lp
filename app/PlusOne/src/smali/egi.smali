.class public abstract Legi;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lctp;
.implements Lcty;
.implements Lepy;
.implements Lhjj;
.implements Lhjn;
.implements Lhjr;
.implements Lhmq;
.implements Lhob;
.implements Lhsg;
.implements Lhsw;
.implements Ljul;
.implements Lkch;
.implements Llgs;
.implements Llkc;
.implements Lllm;


# instance fields
.field private N:Ljava/lang/Integer;

.field public final O:Lhje;

.field public P:Lhee;

.field public final Q:Lctu;

.field public final R:Lcto;

.field public final S:Lhke;

.field public final T:Lcmz;

.field public U:Lcsg;

.field public V:Lcsm;

.field public W:Lhei;

.field public X:Lctz;

.field public Y:Lctq;

.field public Z:Z

.field private aA:I

.field public aa:I

.field public ab:Z

.field public ac:Z

.field public ad:Z

.field public ae:Z

.field public af:Ljava/lang/String;

.field public ag:Z

.field private ah:I

.field private ai:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhsl;",
            ">;"
        }
    .end annotation
.end field

.field private aj:Landroid/view/View;

.field private ak:Landroid/view/View;

.field private al:Lepx;

.field private am:Lhkd;

.field private an:Lhkd;

.field private ao:Lhkd;

.field private ap:Lhkd;

.field private aq:Lhkd;

.field private ar:Z

.field private as:Z

.field private final aw:Lfhh;

.field private ax:Lhoc;

.field private ay:Lkci;

.field private az:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Legq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 114
    invoke-direct {p0}, Llol;-><init>()V

    .line 168
    new-instance v0, Lhje;

    iget-object v1, p0, Legi;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lhje;-><init>(Lu;Llqr;Lhjj;)V

    iput-object v0, p0, Legi;->O:Lhje;

    .line 175
    const/4 v0, 0x0

    iput v0, p0, Legi;->ah:I

    .line 185
    new-instance v0, Lctu;

    iget-object v1, p0, Legi;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lctu;-><init>(Lu;Llqr;Lcty;)V

    iput-object v0, p0, Legi;->Q:Lctu;

    .line 186
    new-instance v0, Lcto;

    iget-object v1, p0, Legi;->av:Llqm;

    invoke-direct {v0, p0, v1, p0}, Lcto;-><init>(Lu;Llqr;Lctp;)V

    iput-object v0, p0, Legi;->R:Lcto;

    .line 188
    new-instance v0, Legj;

    invoke-direct {v0, p0}, Legj;-><init>(Legi;)V

    iput-object v0, p0, Legi;->am:Lhkd;

    .line 223
    new-instance v0, Legk;

    invoke-direct {v0, p0}, Legk;-><init>(Legi;)V

    iput-object v0, p0, Legi;->an:Lhkd;

    .line 243
    new-instance v0, Legl;

    invoke-direct {v0, p0}, Legl;-><init>(Legi;)V

    iput-object v0, p0, Legi;->ao:Lhkd;

    .line 256
    new-instance v0, Legm;

    invoke-direct {v0, p0}, Legm;-><init>(Legi;)V

    iput-object v0, p0, Legi;->ap:Lhkd;

    .line 266
    new-instance v0, Legn;

    invoke-direct {v0, p0}, Legn;-><init>(Legi;)V

    iput-object v0, p0, Legi;->aq:Lhkd;

    .line 289
    new-instance v0, Lhke;

    iget-object v1, p0, Legi;->av:Llqm;

    invoke-direct {v0, v1}, Lhke;-><init>(Llqr;)V

    iget-object v1, p0, Legi;->au:Llnh;

    .line 291
    invoke-virtual {v0, v1}, Lhke;->a(Llnh;)Lhke;

    move-result-object v0

    const v1, 0x7f1000bd

    iget-object v2, p0, Legi;->am:Lhkd;

    .line 292
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    const v1, 0x7f1000c1

    iget-object v2, p0, Legi;->an:Lhkd;

    .line 293
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    const v1, 0x7f1000c2

    iget-object v2, p0, Legi;->ao:Lhkd;

    .line 294
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    const v1, 0x7f1000c3

    iget-object v2, p0, Legi;->ap:Lhkd;

    .line 295
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    const v1, 0x7f1000c5

    iget-object v2, p0, Legi;->aq:Lhkd;

    .line 296
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    iput-object v0, p0, Legi;->S:Lhke;

    .line 299
    new-instance v0, Lcmz;

    iget-object v1, p0, Legi;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lcmz;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Legi;->T:Lcmz;

    .line 305
    new-instance v0, Lhsx;

    iget-object v1, p0, Legi;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lhsx;-><init>(Llqr;Lhsw;)V

    .line 306
    new-instance v0, Lhjp;

    iget-object v1, p0, Legi;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lhjp;-><init>(Llqr;Lhjr;)V

    .line 307
    new-instance v0, Lllo;

    iget-object v1, p0, Legi;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lllo;-><init>(Llqr;Lllm;)V

    .line 351
    new-instance v0, Lego;

    invoke-direct {v0, p0}, Lego;-><init>(Legi;)V

    iput-object v0, p0, Legi;->aw:Lfhh;

    .line 368
    new-instance v0, Lhoc;

    iget-object v1, p0, Legi;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iget-object v1, p0, Legi;->au:Llnh;

    .line 369
    invoke-virtual {v0, v1}, Lhoc;->a(Llnh;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Legi;->ax:Lhoc;

    .line 371
    new-instance v0, Lkci;

    iget-object v1, p0, Legi;->av:Llqm;

    const v2, 0x7f10005b

    invoke-direct {v0, p0, v1, v2, p0}, Lkci;-><init>(Lu;Llqr;ILkch;)V

    iput-object v0, p0, Legi;->ay:Lkci;

    .line 1405
    return-void
.end method

.method public static a(ILizu;)Landroid/content/Intent;
    .locals 6

    .prologue
    .line 606
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 609
    const-string v0, "account_id"

    invoke-virtual {v1, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 613
    invoke-virtual {p1}, Lizu;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 614
    invoke-virtual {p1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 618
    :goto_0
    if-eqz v0, :cond_0

    .line 619
    const-string v2, "photo_url"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 621
    :cond_0
    invoke-virtual {p1}, Lizu;->c()J

    move-result-wide v2

    .line 622
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 623
    const-string v0, "photo_id"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 625
    :cond_1
    invoke-virtual {p1}, Lizu;->a()Ljava/lang/String;

    move-result-object v0

    .line 626
    if-eqz v0, :cond_2

    .line 627
    const-string v2, "tile_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 630
    :cond_2
    const-string v2, "media_type"

    invoke-virtual {p1}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v3, Legp;->a:[I

    invoke-virtual {v0}, Ljac;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 632
    return-object v1

    .line 616
    :cond_3
    invoke-virtual {p1}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 630
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 730
    iget-object v0, p0, Legi;->N:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Legi;->N:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 748
    :cond_0
    :goto_0
    return-void

    .line 733
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Legi;->N:Ljava/lang/Integer;

    .line 735
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 737
    :goto_1
    iget-object v2, p0, Legi;->T:Lcmz;

    invoke-virtual {v2}, Lcmz;->a()V

    .line 739
    if-eqz v0, :cond_3

    .line 740
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    const v2, 0x7f0a058e

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 735
    goto :goto_1

    .line 742
    :cond_3
    iget v0, p0, Legi;->ah:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 744
    :pswitch_0
    instance-of v0, p2, Lfhs;

    if-eqz v0, :cond_0

    .line 745
    check-cast p2, Lfhs;

    .line 746
    invoke-virtual {p2}, Lfhs;->a()Ljava/lang/String;

    move-result-object v0

    .line 747
    invoke-virtual {p0, v0}, Legi;->b_(Ljava/lang/String;)V

    goto :goto_0

    .line 742
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Legi;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Legi;->ac()V

    return-void
.end method

.method static synthetic a(Legi;ILfib;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Legi;->a(ILfib;)V

    return-void
.end method

.method private ac()V
    .locals 4

    .prologue
    .line 925
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 926
    iget-object v0, p0, Legi;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 927
    const-string v0, "photo_picker_mode"

    iget-object v2, p0, Legi;->Y:Lctq;

    invoke-virtual {v2}, Lctq;->c()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 928
    const-string v0, "photo_picker_selected"

    iget-object v2, p0, Legi;->X:Lctz;

    .line 929
    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v2

    .line 928
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 940
    :goto_0
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    .line 942
    if-eqz v0, :cond_0

    .line 943
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 945
    :cond_0
    return-void

    .line 933
    :cond_1
    iget-object v0, p0, Legi;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->c()I

    move-result v0

    .line 934
    const-string v2, "photo_picker_mode"

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    :goto_1
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 938
    const-string v0, "photo_picker_selected"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0

    .line 934
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private ad()Z
    .locals 1

    .prologue
    .line 1399
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    invoke-virtual {v0}, Loo;->g()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Legi;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Legi;->ai:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Legi;)V
    .locals 3

    .prologue
    .line 114
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Legi;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eG:Lhmv;

    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    invoke-virtual {p0}, Legi;->aj()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_id"

    iget-object v2, p0, Legi;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Legi;->a(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public H_()V
    .locals 1

    .prologue
    .line 818
    invoke-virtual {p0}, Legi;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Legi;->e(Landroid/view/View;)V

    .line 819
    return-void
.end method

.method public I_()V
    .locals 1

    .prologue
    .line 1071
    iget-object v0, p0, Legi;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1072
    const/4 v0, 0x0

    iput-boolean v0, p0, Legi;->as:Z

    .line 1074
    :cond_0
    return-void
.end method

.method public J_()Z
    .locals 2

    .prologue
    .line 1351
    iget-object v0, p0, Legi;->ax:Lhoc;

    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Legi;->ax:Lhoc;

    const-string v1, "fetch_older"

    .line 1352
    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public K_()V
    .locals 1

    .prologue
    .line 1367
    iget-object v0, p0, Legi;->ay:Lkci;

    invoke-virtual {v0}, Lkci;->c()V

    .line 1368
    invoke-virtual {p0}, Legi;->L_()V

    .line 1369
    return-void
.end method

.method protected L_()V
    .locals 1

    .prologue
    .line 1372
    invoke-direct {p0}, Legi;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Legi;->ay:Lkci;

    invoke-virtual {v0}, Lkci;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1373
    iget-object v0, p0, Legi;->ay:Lkci;

    invoke-virtual {v0}, Lkci;->d()V

    .line 1375
    :cond_0
    return-void
.end method

.method public abstract U()Z
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Legi;->V:Lcsm;

    invoke-interface {v0}, Lcsm;->a()V

    .line 851
    const/4 v0, 0x0

    return v0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 1379
    const/4 v0, 0x0

    return v0
.end method

.method protected Y_()Z
    .locals 1

    .prologue
    .line 1086
    const/4 v0, 0x0

    return v0
.end method

.method public Z()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1295
    iget-object v0, p0, Legi;->X:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v0

    .line 1296
    iget-object v3, p0, Legi;->Y:Lctq;

    invoke-virtual {v3}, Lctq;->c()I

    move-result v3

    .line 1297
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljcn;->k()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    if-eqz v3, :cond_1

    const/4 v0, 0x5

    if-ne v3, v0, :cond_2

    :cond_1
    move v0, v2

    .line 1301
    :goto_0
    iget-object v3, p0, Legi;->P:Lhee;

    invoke-interface {v3}, Lhee;->f()Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    :goto_1
    return v2

    :cond_2
    move v0, v1

    .line 1297
    goto :goto_0

    :cond_3
    move v2, v1

    .line 1301
    goto :goto_1
.end method

.method protected Z_()V
    .locals 1

    .prologue
    .line 1359
    invoke-direct {p0}, Legi;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1360
    iget-object v0, p0, Legi;->ay:Lkci;

    invoke-virtual {v0}, Lkci;->e()V

    .line 1362
    :cond_0
    iget-object v0, p0, Legi;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 1363
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 515
    const/4 v0, 0x0

    invoke-virtual {p1, p4, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 517
    const v0, 0x7f10031b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Legi;->aj:Landroid/view/View;

    .line 518
    const v0, 0x7f10060f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Legi;->ak:Landroid/view/View;

    .line 520
    if-eqz p3, :cond_1

    const-string v0, "share_only"

    .line 521
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 522
    :goto_0
    iput-boolean v0, p0, Legi;->as:Z

    .line 524
    iget-object v0, p0, Legi;->ak:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Legi;->ak:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 528
    :cond_0
    return-object v1

    .line 522
    :cond_1
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "share_only"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 438
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 442
    return-void
.end method

.method protected a(Landroid/net/Uri;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1191
    iget-object v1, p0, Legi;->X:Lctz;

    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v8

    .line 1192
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v9

    .line 1193
    if-eqz v9, :cond_0

    if-eqz p1, :cond_0

    .line 1195
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 1196
    if-nez v1, :cond_1

    .line 1198
    :goto_0
    if-nez v0, :cond_0

    .line 1199
    invoke-static {}, Ljvj;->a()Ljava/lang/String;

    move-result-object v1

    .line 1200
    sget-object v0, Ljac;->b:Ljac;

    invoke-static {v9, p1, v0}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v3

    .line 1201
    new-instance v0, Ljuc;

    const-wide/16 v4, 0x1000

    const-wide/16 v6, 0x0

    move-object v2, v1

    invoke-direct/range {v0 .. v7}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    .line 1203
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1204
    if-nez v8, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1206
    :goto_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1207
    const-string v0, "shareables"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1208
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 1209
    invoke-virtual {v9}, Landroid/app/Activity;->finish()V

    .line 1212
    :cond_0
    return-void

    .line 1196
    :cond_1
    const-string v2, "photo_picker_crop_mode"

    .line 1197
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 1204
    :cond_2
    const-class v1, Ljuf;

    .line 1205
    invoke-virtual {v8, v1}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1
.end method

.method public a(Landroid/net/Uri;I)V
    .locals 3

    .prologue
    .line 1253
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1267
    :cond_0
    :goto_0
    return-void

    .line 1257
    :cond_1
    const v0, 0x7f1000c1

    if-ne p2, v0, :cond_0

    .line 1258
    if-nez p1, :cond_2

    .line 1259
    const/4 v0, 0x1

    iput v0, p0, Legi;->ah:I

    .line 1260
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Legi;->P:Lhee;

    .line 1261
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const-string v2, "camera-p.jpg"

    .line 1260
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->i(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Legi;->N:Ljava/lang/Integer;

    .line 1262
    iget-object v0, p0, Legi;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto :goto_0

    .line 1264
    :cond_2
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Legi;->b_(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 470
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 472
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v2, Lhoc;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 473
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 474
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lhoc;->a(Lhos;)V

    .line 476
    iget-object v0, p0, Legi;->at:Llnl;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 477
    new-instance v0, Landroid/widget/ArrayAdapter;

    .line 478
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v2

    const v3, 0x7f0401eb

    invoke-direct {v0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Legi;->az:Landroid/widget/ArrayAdapter;

    .line 484
    :goto_0
    iget-object v0, p0, Legi;->az:Landroid/widget/ArrayAdapter;

    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 487
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v2

    .line 488
    const-string v0, "external"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Legi;->ab:Z

    .line 489
    const-string v0, "is_for_get_content"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Legi;->ac:Z

    .line 490
    const-string v0, "is_for_movie_maker_launch"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Legi;->ae:Z

    .line 491
    const-string v0, "force_return_edit_list"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Legi;->ad:Z

    .line 492
    const-string v0, "movie_maker_session_id"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Legi;->af:Ljava/lang/String;

    .line 493
    const-string v0, "filter"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Legi;->aa:I

    .line 495
    if-eqz p1, :cond_0

    .line 496
    const-string v0, "operation_type"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Legi;->ah:I

    .line 497
    const-string v0, "media_snapshot"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Legi;->ai:Ljava/util/ArrayList;

    .line 499
    const-string v0, "pending_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    const-string v0, "pending_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Legi;->N:Ljava/lang/Integer;

    .line 505
    :cond_0
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v3, Lieh;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 506
    const-string v3, "disable_chromecast"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Legi;->Y:Lctq;

    .line 507
    invoke-virtual {v2}, Lctq;->d()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Legi;->P:Lhee;

    .line 508
    invoke-interface {v2}, Lhee;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Ldxd;->m:Lief;

    iget-object v3, p0, Legi;->P:Lhee;

    .line 510
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 509
    invoke-interface {v0, v2, v3}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Legi;->ag:Z

    .line 511
    return-void

    .line 480
    :cond_2
    new-instance v0, Landroid/widget/ArrayAdapter;

    .line 481
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v2

    const v3, 0x7f040037

    invoke-direct {v0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Legi;->az:Landroid/widget/ArrayAdapter;

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 509
    goto :goto_1
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 420
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;I)V
    .locals 0

    .prologue
    .line 1346
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;IIIII)V
    .locals 0

    .prologue
    .line 1342
    return-void
.end method

.method public final a(Lhjk;)V
    .locals 3

    .prologue
    const v0, 0x7f0a0ab5

    .line 996
    iget-object v1, p0, Legi;->Y:Lctq;

    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1005
    :goto_0
    iget-object v1, p0, Legi;->Y:Lctq;

    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    .line 1043
    :pswitch_0
    invoke-virtual {p0, p1}, Legi;->b(Lhjk;)V

    .line 1047
    :cond_0
    :goto_1
    return-void

    .line 1000
    :pswitch_1
    const v1, 0x7f100693

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Lhjk;->a(IZ)V

    goto :goto_0

    .line 1007
    :pswitch_2
    iget v0, p0, Legi;->aa:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    .line 1008
    invoke-virtual {p0, p1}, Legi;->b(Lhjk;)V

    .line 1009
    const v0, 0x7f0a0ab3

    move-object v1, p1

    .line 1011
    :goto_2
    invoke-interface {v1, v0}, Lhjk;->d(I)V

    .line 1014
    iget-boolean v0, p0, Legi;->ac:Z

    if-nez v0, :cond_0

    .line 1015
    const v0, 0x7f10067b

    .line 1016
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 1017
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    goto :goto_1

    .line 1011
    :cond_1
    invoke-virtual {p0}, Legi;->am()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0a0b0e

    move-object v1, p1

    goto :goto_2

    :cond_2
    const v0, 0x7f0a0ab2

    move-object v1, p1

    goto :goto_2

    .line 1023
    :pswitch_3
    iget v1, p0, Legi;->aa:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_3

    .line 1024
    invoke-virtual {p0, p1}, Legi;->b(Lhjk;)V

    .line 1027
    :cond_3
    invoke-virtual {p0}, Legi;->am()Z

    move-result v1

    if-eqz v1, :cond_4

    const v0, 0x7f0a0b0d

    .line 1026
    :cond_4
    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 1030
    iget-boolean v0, p0, Legi;->ac:Z

    if-nez v0, :cond_0

    iget v0, p0, Legi;->aa:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_0

    .line 1031
    invoke-virtual {p0, p1}, Legi;->c(Lhjk;)V

    goto :goto_1

    .line 1038
    :pswitch_4
    invoke-interface {p1, v0}, Lhjk;->d(I)V

    goto :goto_1

    .line 996
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 1005
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected a(Lhjk;I)V
    .locals 2

    .prologue
    .line 1140
    iget-object v0, p0, Legi;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Legi;->ac:Z

    if-eqz v0, :cond_1

    .line 1156
    :cond_0
    :goto_0
    return-void

    .line 1143
    :cond_1
    and-int/lit8 v0, p2, 0x4

    if-nez v0, :cond_2

    .line 1144
    invoke-virtual {p0, p1}, Legi;->e(Lhjk;)V

    .line 1146
    :cond_2
    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_3

    .line 1147
    iget-object v0, p0, Legi;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1148
    const v0, 0x7f100681

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1153
    :cond_3
    :goto_1
    and-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_0

    .line 1154
    const v0, 0x7f100683

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 1150
    :cond_4
    const v0, 0x7f100682

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 386
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    const/4 v0, 0x1

    iput-boolean v0, p0, Legi;->Z:Z

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lhsl;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const v1, 0x7f1000c1

    .line 1273
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1291
    :cond_0
    :goto_0
    return-void

    .line 1277
    :cond_1
    iput-object p1, p0, Legi;->ai:Ljava/util/ArrayList;

    .line 1282
    if-ne p2, v1, :cond_0

    .line 1284
    :try_start_0
    invoke-virtual {p0}, Legi;->n()Lz;

    const-string v0, "camera-p.jpg"

    invoke-static {v0}, Leyq;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1285
    iget-object v1, p0, Legi;->S:Lhke;

    const v2, 0x7f1000c1

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1287
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a08ed

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1288
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public a(Ljcl;)V
    .locals 6

    .prologue
    .line 791
    instance-of v0, p1, Ljuc;

    if-nez v0, :cond_0

    .line 814
    :goto_0
    return-void

    .line 795
    :cond_0
    check-cast p1, Ljuc;

    .line 796
    invoke-virtual {p1}, Ljuc;->f()Lizu;

    move-result-object v0

    .line 797
    iget-object v1, p0, Legi;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 798
    new-instance v2, Ldew;

    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 799
    invoke-virtual {v0}, Lizu;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ldew;->a(Ljava/lang/String;)Ldew;

    move-result-object v1

    .line 800
    invoke-virtual {v1, v0}, Ldew;->a(Lizu;)Ldew;

    move-result-object v1

    .line 801
    invoke-virtual {p1}, Ljuc;->i()J

    move-result-wide v2

    const-wide/32 v4, 0x40000

    and-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 802
    invoke-virtual {p1}, Ljuc;->j()Ljava/lang/String;

    move-result-object v0

    .line 801
    :goto_1
    invoke-virtual {v1, v0}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legi;->Y:Lctq;

    .line 803
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->b(I)Ldew;

    move-result-object v0

    iget-object v1, p0, Legi;->X:Lctz;

    .line 804
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Legi;->Y:Lctq;

    .line 805
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    const/4 v1, 0x0

    .line 806
    invoke-virtual {v0, v1}, Ldew;->i(Z)Ldew;

    move-result-object v0

    iget-boolean v1, p0, Legi;->ac:Z

    .line 807
    invoke-virtual {v0, v1}, Ldew;->b(Z)Ldew;

    move-result-object v0

    iget-boolean v1, p0, Legi;->ae:Z

    .line 808
    invoke-virtual {v0, v1}, Ldew;->c(Z)Ldew;

    move-result-object v0

    iget-object v1, p0, Legi;->af:Ljava/lang/String;

    .line 809
    invoke-virtual {v0, v1}, Ldew;->e(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-boolean v1, p0, Legi;->ad:Z

    .line 810
    invoke-virtual {v0, v1}, Ldew;->g(Z)Ldew;

    move-result-object v0

    iget v1, p0, Legi;->aa:I

    .line 811
    invoke-virtual {v0, v1}, Ldew;->c(I)Ldew;

    move-result-object v0

    .line 812
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    .line 813
    invoke-virtual {p0, v0}, Legi;->b(Landroid/content/Intent;)V

    goto :goto_0

    .line 802
    :cond_1
    invoke-virtual {p1}, Ljuc;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Loo;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 949
    invoke-static {p1, v3}, Lley;->a(Loo;Z)V

    .line 950
    iget-boolean v0, p0, Legi;->ab:Z

    if-eqz v0, :cond_0

    .line 951
    invoke-virtual {p1, v2}, Loo;->c(Z)V

    .line 953
    :cond_0
    invoke-virtual {p0}, Legi;->Y_()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 954
    iget-object v0, p0, Legi;->W:Lhei;

    new-array v1, v3, [Ljava/lang/String;

    const-string v4, "logged_in"

    aput-object v4, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 955
    new-instance v0, Lhed;

    iget-object v1, p0, Legi;->W:Lhei;

    invoke-direct {v0, v1}, Lhed;-><init>(Lhei;)V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 956
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 957
    iget-object v0, p0, Legi;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    if-lt v5, v0, :cond_2

    .line 959
    invoke-virtual {p0}, Legi;->ae()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    .line 961
    :goto_0
    if-eqz v0, :cond_4

    .line 962
    iget-object v0, p0, Legi;->az:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    move v1, v2

    .line 963
    :goto_1
    if-ge v1, v5, :cond_3

    .line 964
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 965
    iget-object v6, p0, Legi;->P:Lhee;

    invoke-interface {v6}, Lhee;->d()I

    move-result v6

    if-ne v6, v0, :cond_1

    .line 966
    iput v1, p0, Legi;->aA:I

    .line 968
    :cond_1
    iget-object v6, p0, Legi;->az:Landroid/widget/ArrayAdapter;

    new-instance v7, Legq;

    invoke-direct {v7, p0, v0}, Legq;-><init>(Legi;I)V

    invoke-virtual {v6, v7}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 963
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 959
    goto :goto_0

    .line 971
    :cond_3
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    const v1, 0x7f040031

    const/4 v4, 0x0

    invoke-static {v0, v1, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 972
    const v0, 0x7f100176

    .line 973
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;

    .line 974
    iget-object v4, p0, Legi;->az:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 975
    iget v4, p0, Legi;->aA:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->setSelection(I)V

    .line 976
    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/actionbar/ActionBarSpinner;->a(Lhjn;)V

    .line 978
    invoke-virtual {p1, v1}, Loo;->a(Landroid/view/View;)V

    .line 979
    invoke-virtual {p1, v3}, Loo;->e(Z)V

    .line 980
    invoke-virtual {p1, v2}, Loo;->d(Z)V

    .line 983
    :cond_4
    return-void
.end method

.method public a()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 828
    invoke-virtual {p0}, Legi;->x()Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Llsn;->b(Landroid/view/View;)V

    .line 831
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "finish_on_back"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 832
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v2

    .line 837
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 838
    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 845
    :goto_0
    return v0

    .line 840
    :cond_0
    iget-object v2, p0, Legi;->Y:Lctq;

    invoke-virtual {v2}, Lctq;->e()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Legi;->ac:Z

    if-eqz v2, :cond_2

    .line 841
    :cond_1
    invoke-direct {p0}, Legi;->ac()V

    .line 842
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->finish()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 845
    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 866
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 867
    const v2, 0x7f10067b

    if-ne v0, v2, :cond_0

    .line 868
    invoke-virtual {p0}, Legi;->L_()V

    move v0, v1

    .line 893
    :goto_0
    return v0

    .line 870
    :cond_0
    const v2, 0x7f100684

    if-eq v0, v2, :cond_1

    const v2, 0x7f100686

    if-ne v0, v2, :cond_2

    .line 872
    :cond_1
    invoke-virtual {p0}, Legi;->ap()V

    move v0, v1

    .line 873
    goto :goto_0

    .line 874
    :cond_2
    const v2, 0x7f100685

    if-eq v0, v2, :cond_3

    const v2, 0x7f100687

    if-ne v0, v2, :cond_4

    .line 876
    :cond_3
    invoke-virtual {p0}, Legi;->aq()V

    move v0, v1

    .line 877
    goto :goto_0

    .line 878
    :cond_4
    const v2, 0x7f100683

    if-ne v0, v2, :cond_5

    .line 879
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v3, p0, Legi;->at:Llnl;

    invoke-direct {v2, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->dS:Lhmv;

    .line 880
    invoke-virtual {v2, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 879
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 881
    iget-object v0, p0, Legi;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 882
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0, v4}, Leyq;->j(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Legi;->a(Landroid/content/Intent;)V

    move v0, v1

    .line 884
    goto :goto_0

    .line 885
    :cond_5
    const v2, 0x7f100681

    if-eq v0, v2, :cond_6

    const v2, 0x7f100682

    if-ne v0, v2, :cond_7

    .line 887
    :cond_6
    invoke-virtual {p0}, Legi;->ai()V

    move v0, v1

    .line 888
    goto :goto_0

    .line 889
    :cond_7
    const v2, 0x7f100696

    if-ne v0, v2, :cond_8

    .line 890
    invoke-virtual {p0, v4}, Legi;->b(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    move v0, v1

    .line 891
    goto :goto_0

    .line 893
    :cond_8
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lizu;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 565
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo_picker_crop_mode"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 568
    iget-object v2, p0, Legi;->Y:Lctq;

    .line 569
    invoke-virtual {v2}, Lctq;->c()I

    move-result v2

    if-ne v2, v1, :cond_1

    iget-boolean v2, p0, Legi;->ab:Z

    if-nez v2, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    .line 572
    :goto_0
    if-nez v2, :cond_2

    .line 602
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 569
    goto :goto_0

    .line 576
    :cond_2
    if-eqz v3, :cond_3

    .line 577
    iget-object v2, p0, Legi;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 578
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v4

    invoke-static {v4, v2}, Leyq;->h(Landroid/content/Context;I)Leyx;

    move-result-object v2

    .line 579
    invoke-virtual {v2, p1}, Leyx;->a(Lizu;)Leyx;

    move-result-object v2

    .line 580
    invoke-virtual {v2, v3}, Leyx;->a(I)Leyx;

    move-result-object v2

    .line 581
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "photo_min_width"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Leyx;->b(I)Leyx;

    move-result-object v2

    .line 582
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "photo_min_height"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Leyx;->c(I)Leyx;

    move-result-object v0

    .line 583
    invoke-virtual {v0}, Leyx;->a()Landroid/content/Intent;

    move-result-object v0

    .line 585
    iget-object v2, p0, Legi;->S:Lhke;

    const v3, 0x7f1000c3

    invoke-virtual {v2, v3, v0}, Lhke;->a(ILandroid/content/Intent;)V

    :goto_2
    move v0, v1

    .line 602
    goto :goto_1

    .line 588
    :cond_3
    iget-boolean v2, p0, Legi;->ac:Z

    if-eqz v2, :cond_4

    .line 590
    invoke-virtual {p0}, Legi;->af()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Legi;->aa:I

    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v4

    .line 589
    invoke-static {v2, v3, p1, v4}, Leys;->a(Ljava/lang/String;ILizu;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 594
    :goto_3
    if-nez v2, :cond_5

    .line 595
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v2

    invoke-virtual {v2, v0}, Lz;->setResult(I)V

    .line 599
    :goto_4
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    goto :goto_2

    .line 592
    :cond_4
    iget-object v2, p0, Legi;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-static {v2, p1}, Legi;->a(ILizu;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_3

    .line 597
    :cond_5
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    const/4 v3, -0x1

    invoke-virtual {v0, v3, v2}, Lz;->setResult(ILandroid/content/Intent;)V

    goto :goto_4
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 686
    invoke-super {p0}, Llol;->aO_()V

    .line 687
    const/4 v0, 0x0

    iput-boolean v0, p0, Legi;->ar:Z

    .line 689
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Ljuk;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    .line 690
    invoke-virtual {v0, p0}, Ljuk;->a(Ljul;)V

    .line 692
    iget-object v0, p0, Legi;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 694
    iget-object v0, p0, Legi;->aw:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 696
    iget-object v0, p0, Legi;->N:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Legi;->N:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 698
    iget-object v0, p0, Legi;->N:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 699
    iget-object v1, p0, Legi;->N:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Legi;->a(ILfib;)V

    .line 702
    :cond_0
    return-void
.end method

.method public aQ_()Z
    .locals 1

    .prologue
    .line 1388
    invoke-virtual {p0}, Legi;->J_()Z

    move-result v0

    return v0
.end method

.method public a_(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 376
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 377
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 378
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v1

    const v2, 0x7f0a068a

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 379
    const/16 v2, 0x33

    aget v3, v0, v3

    const/4 v4, 0x1

    aget v0, v0, v4

    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/Toast;->setGravity(III)V

    .line 380
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 381
    return-void
.end method

.method public ab_()V
    .locals 1

    .prologue
    .line 1078
    invoke-virtual {p0}, Legi;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Legi;->e(Landroid/view/View;)V

    .line 1079
    invoke-direct {p0}, Legi;->ac()V

    .line 1080
    iget-object v0, p0, Legi;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1081
    iget-object v0, p0, Legi;->Q:Lctu;

    invoke-virtual {v0}, Lctu;->d()V

    .line 1083
    :cond_0
    return-void
.end method

.method protected ae()Z
    .locals 2

    .prologue
    .line 758
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "local_folders_only"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected af()Ljava/lang/String;
    .locals 2

    .prologue
    .line 656
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "get_content_action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 657
    if-nez v0, :cond_0

    .line 658
    const-string v0, "android.intent.action.GET_CONTENT"

    .line 660
    :cond_0
    return-object v0
.end method

.method protected ag()V
    .locals 2

    .prologue
    .line 723
    iget-object v0, p0, Legi;->aj:Landroid/view/View;

    if-nez v0, :cond_0

    .line 727
    :goto_0
    return-void

    .line 726
    :cond_0
    iget-object v1, p0, Legi;->aj:Landroid/view/View;

    iget-boolean v0, p0, Legi;->Z:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public ah()Z
    .locals 1

    .prologue
    .line 856
    iget-object v0, p0, Legi;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 857
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    .line 858
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 859
    const/4 v0, 0x1

    .line 861
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected ai()V
    .locals 3

    .prologue
    .line 897
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Legi;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eM:Lhmv;

    .line 898
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 897
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 899
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    .line 900
    invoke-virtual {p0}, Legi;->ar()Z

    move-result v1

    iget-object v2, p0, Legi;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 899
    invoke-static {v0, v1, v2}, Lduf;->a(Landroid/content/Context;ZI)Landroid/content/Intent;

    move-result-object v0

    .line 901
    iget-object v1, p0, Legi;->S:Lhke;

    const v2, 0x7f1000c5

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V

    .line 902
    return-void
.end method

.method protected aj()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 911
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Legi;->at:Llnl;

    const-class v2, Lcom/google/android/apps/photos/phone/GetLocalAutoAwesomeMovieActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 912
    const-string v1, "movie_maker_session_id"

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 913
    return-object v0
.end method

.method public ak()V
    .locals 1

    .prologue
    .line 1066
    iget-object v0, p0, Legi;->O:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    .line 1067
    return-void
.end method

.method protected al()Z
    .locals 1

    .prologue
    .line 1127
    iget v0, p0, Legi;->aa:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected am()Z
    .locals 1

    .prologue
    .line 1131
    iget-boolean v0, p0, Legi;->ae:Z

    return v0
.end method

.method protected an()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1135
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "mode"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected ao()Z
    .locals 3

    .prologue
    .line 1174
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "delete_duplicate_photos"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ap()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1179
    new-instance v0, Lhsi;

    .line 1180
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v1

    const v2, 0x7f1000c1

    invoke-direct {v0, v1, p0, v2}, Lhsi;-><init>(Landroid/content/Context;Lhsg;I)V

    .line 1183
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    .line 1184
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lhsi;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1188
    :goto_0
    return-void

    .line 1186
    :cond_0
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lhsi;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public aq()V
    .locals 3

    .prologue
    .line 1242
    :try_start_0
    invoke-virtual {p0}, Legi;->n()Lz;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1243
    iget-object v1, p0, Legi;->S:Lhke;

    const v2, 0x7f1000c2

    invoke-virtual {v1, v2, v0}, Lhke;->a(ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1248
    :goto_0
    return-void

    .line 1245
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a08ed

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 1246
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected ar()Z
    .locals 2

    .prologue
    .line 1310
    invoke-static {}, Ljfb;->f()Ljfb;

    move-result-object v0

    .line 1311
    iget-object v1, p0, Legi;->P:Lhee;

    invoke-interface {v1}, Lhee;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Legi;->P:Lhee;

    .line 1312
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljfb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected as()Lepx;
    .locals 1

    .prologue
    .line 1319
    iget-object v0, p0, Legi;->al:Lepx;

    if-eqz v0, :cond_0

    .line 1320
    iget-object v0, p0, Legi;->al:Lepx;

    .line 1325
    :goto_0
    return-object v0

    .line 1323
    :cond_0
    new-instance v0, Lepx;

    invoke-direct {v0, p0}, Lepx;-><init>(Landroid/widget/AbsListView$OnScrollListener;)V

    iput-object v0, p0, Legi;->al:Lepx;

    .line 1325
    iget-object v0, p0, Legi;->al:Lepx;

    goto :goto_0
.end method

.method public at()Z
    .locals 1

    .prologue
    .line 1383
    iget-object v0, p0, Legi;->ay:Lkci;

    invoke-virtual {v0}, Lkci;->f()Z

    move-result v0

    return v0
.end method

.method public b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 776
    iget-boolean v0, p0, Legi;->ar:Z

    if-eqz v0, :cond_0

    .line 788
    :goto_0
    return-void

    .line 780
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Legi;->ar:Z

    .line 782
    const-string v0, "delete_duplicate_photos"

    invoke-virtual {p0}, Legi;->ao()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 783
    iget-object v0, p0, Legi;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 784
    iget-object v0, p0, Legi;->S:Lhke;

    const v1, 0x7f1000bd

    invoke-virtual {v0, v1, p1}, Lhke;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 786
    :cond_1
    iget-object v0, p0, Legi;->S:Lhke;

    invoke-virtual {v0, p1}, Lhke;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1396
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 424
    const-string v0, "deselect_on_dismiss"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Legi;->R:Lcto;

    invoke-virtual {v0, v1}, Lcto;->a(I)V

    .line 427
    :cond_0
    return-void
.end method

.method protected b(Lcom/google/android/apps/plus/views/PhotoTileView;)V
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Legi;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    iget-object v0, p0, Legi;->R:Lcto;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcto;->a(I)V

    .line 411
    if-eqz p1, :cond_0

    .line 412
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoTileView;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Legi;->Q:Lctu;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoTileView;->i()Ljcl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lctu;->b(Ljcl;)V

    goto :goto_0
.end method

.method public b(Lhjk;)V
    .locals 2

    .prologue
    .line 1053
    iget-object v0, p0, Legi;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1054
    const v0, 0x7f100683

    invoke-interface {p1, v0}, Lhjk;->c(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1056
    const v1, 0x7f020544

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1059
    :cond_0
    invoke-virtual {p0}, Legi;->U()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Legi;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1060
    const v0, 0x7f100696

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1062
    :cond_1
    return-void
.end method

.method public b(Loo;)V
    .locals 1

    .prologue
    .line 987
    invoke-virtual {p0}, Legi;->Y_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 988
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->a(Landroid/view/View;)V

    .line 989
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->e(Z)V

    .line 990
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->d(Z)V

    .line 992
    :cond_0
    return-void
.end method

.method protected b_(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1215
    iget-object v1, p0, Legi;->T:Lcmz;

    invoke-virtual {v1}, Lcmz;->a()V

    .line 1216
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v8

    .line 1217
    if-eqz v8, :cond_0

    .line 1219
    invoke-virtual {p0}, Legi;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 1220
    if-nez v1, :cond_1

    .line 1222
    :goto_0
    if-nez v0, :cond_0

    .line 1223
    iget-object v0, p0, Legi;->X:Lctz;

    invoke-virtual {v0}, Lctz;->a()Ljcn;

    move-result-object v9

    .line 1224
    invoke-static {}, Ljvj;->a()Ljava/lang/String;

    move-result-object v1

    .line 1225
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v8, v0, v2}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v3

    .line 1227
    new-instance v0, Ljuc;

    const-wide/16 v4, 0x1000

    const-wide/16 v6, 0x0

    move-object v2, v1

    invoke-direct/range {v0 .. v7}, Ljuc;-><init>(Ljava/lang/String;Ljava/lang/String;Lizu;JJ)V

    .line 1229
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1230
    if-nez v9, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1232
    :goto_1
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1233
    const-string v0, "shareables"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1234
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 1235
    invoke-virtual {v8}, Landroid/app/Activity;->finish()V

    .line 1238
    :cond_0
    return-void

    .line 1220
    :cond_1
    const-string v2, "photo_picker_crop_mode"

    .line 1221
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    .line 1230
    :cond_2
    const-class v1, Ljuf;

    .line 1231
    invoke-virtual {v9, v1}, Ljcn;->a(Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_1
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 449
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 450
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lhmq;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 451
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Legi;->P:Lhee;

    .line 453
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lfyp;

    iget-object v2, p0, Legi;->Q:Lctu;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 454
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lepy;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 455
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Legi;->W:Lhei;

    .line 456
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lctz;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Legi;->X:Lctz;

    .line 457
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lctq;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    iput-object v0, p0, Legi;->Y:Lctq;

    .line 458
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lcsg;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsg;

    iput-object v0, p0, Legi;->U:Lcsg;

    .line 459
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lcsm;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcsm;

    iput-object v0, p0, Legi;->V:Lcsm;

    .line 461
    iget-object v0, p0, Legi;->au:Llnh;

    invoke-virtual {v0}, Llnh;->b()Llnh;

    move-result-object v0

    const-class v1, Lepx;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lepx;

    iput-object v0, p0, Legi;->al:Lepx;

    .line 462
    iget-object v0, p0, Legi;->al:Lepx;

    if-nez v0, :cond_0

    .line 463
    new-instance v0, Lepx;

    invoke-direct {v0, p0}, Lepx;-><init>(Landroid/widget/AbsListView$OnScrollListener;)V

    iput-object v0, p0, Legi;->al:Lepx;

    .line 464
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Lepx;

    iget-object v2, p0, Legi;->al:Lepx;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 466
    :cond_0
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 431
    const-string v0, "deselect_on_dismiss"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Legi;->R:Lcto;

    invoke-virtual {v0, v1}, Lcto;->a(I)V

    .line 434
    :cond_0
    return-void
.end method

.method protected c(Lhjk;)V
    .locals 2

    .prologue
    .line 1105
    iget-object v0, p0, Legi;->N:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget v0, p0, Legi;->ah:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 1106
    :cond_0
    iget-object v0, p0, Legi;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1107
    const v0, 0x7f100684

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1113
    :cond_1
    :goto_0
    invoke-virtual {p0}, Legi;->al()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1114
    invoke-virtual {p0, p1}, Legi;->d(Lhjk;)V

    .line 1116
    :cond_2
    return-void

    .line 1109
    :cond_3
    const v0, 0x7f100686

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public c(I)Z
    .locals 3

    .prologue
    .line 1091
    iget v0, p0, Legi;->aA:I

    if-ne v0, p1, :cond_0

    .line 1092
    const/4 v0, 0x0

    .line 1101
    :goto_0
    return v0

    .line 1094
    :cond_0
    iput p1, p0, Legi;->aA:I

    .line 1095
    iget-object v0, p0, Legi;->az:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legq;

    iget v1, v0, Legq;->a:I

    .line 1096
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v2, Livx;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livx;

    .line 1097
    new-instance v2, Liwg;

    invoke-direct {v2}, Liwg;-><init>()V

    .line 1098
    invoke-virtual {v2}, Liwg;->c()Liwg;

    move-result-object v2

    .line 1099
    invoke-virtual {v2, v1}, Liwg;->a(I)Liwg;

    move-result-object v1

    .line 1097
    invoke-virtual {v0, v1}, Livx;->a(Liwg;)V

    .line 1101
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract c(Landroid/view/View;)Z
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 762
    if-eqz p1, :cond_0

    invoke-static {p1}, Ljvj;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d(Lhjk;)V
    .locals 2

    .prologue
    .line 1119
    iget-object v0, p0, Legi;->at:Llnl;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lhns;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1120
    const v0, 0x7f100685

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 1124
    :goto_0
    return-void

    .line 1122
    :cond_0
    const v0, 0x7f100687

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public e()V
    .locals 0

    .prologue
    .line 823
    invoke-direct {p0}, Legi;->ac()V

    .line 824
    return-void
.end method

.method protected e(I)V
    .locals 2

    .prologue
    .line 766
    invoke-virtual {p0}, Legi;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/view/View;->performHapticFeedback(II)Z

    .line 768
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 672
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 674
    const-string v0, "share_only"

    iget-boolean v1, p0, Legi;->as:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 676
    const-string v0, "operation_type"

    iget v1, p0, Legi;->ah:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 677
    iget-object v0, p0, Legi;->N:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 678
    const-string v0, "pending_request"

    iget-object v1, p0, Legi;->N:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 681
    :cond_0
    const-string v0, "media_snapshot"

    iget-object v1, p0, Legi;->ai:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 682
    return-void
.end method

.method protected e(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 720
    return-void
.end method

.method public e(Lhjk;)V
    .locals 2

    .prologue
    .line 1159
    iget-boolean v0, p0, Legi;->ag:Z

    if-nez v0, :cond_0

    .line 1160
    const v0, 0x7f100394

    .line 1161
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Ldhy;

    .line 1162
    iget-object v1, p0, Legi;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 1163
    invoke-virtual {v0, v1}, Ldhy;->a(I)V

    .line 1165
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 665
    iget-object v0, p0, Legi;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTileView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTileView;->m()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v2, p0, Legi;->at:Llnl;

    const v3, 0x7f0a068a

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    const/16 v3, 0x33

    aget v4, v0, v4

    aget v0, v0, v1

    invoke-virtual {v2, v3, v4, v0}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Legi;->ak:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 666
    invoke-virtual {p0}, Legi;->L_()V

    .line 668
    :cond_0
    return-void

    .line 665
    :cond_1
    invoke-virtual {p0, p1}, Legi;->c(Landroid/view/View;)Z

    move-result v0

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 1332
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 1336
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 706
    invoke-super {p0}, Llol;->z()V

    .line 707
    iget-object v0, p0, Legi;->aw:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 709
    iget-object v0, p0, Legi;->au:Llnh;

    const-class v1, Ljuk;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    .line 710
    invoke-virtual {v0, p0}, Ljuk;->b(Ljul;)V

    .line 711
    return-void
.end method
