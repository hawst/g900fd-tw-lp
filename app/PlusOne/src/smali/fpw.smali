.class final Lfpw;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:[Lfof;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>([Lfof;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 205
    iput-object p1, p0, Lfpw;->a:[Lfof;

    .line 206
    iput-object p2, p0, Lfpw;->b:Landroid/content/Context;

    .line 207
    return-void
.end method


# virtual methods
.method public a(I)Lfof;
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0}, Lfpw;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfpw;->a:[Lfof;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lfpw;->a:[Lfof;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lfpw;->a:[Lfof;

    array-length v0, v0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0, p1}, Lfpw;->a(I)Lfof;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 223
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0}, Lfpw;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const v2, 0x7f100037

    const/4 v3, 0x1

    .line 238
    invoke-virtual {p0, p1}, Lfpw;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 275
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot resolve item type for list view when editing location."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 240
    :pswitch_0
    if-nez p2, :cond_0

    .line 241
    iget-object v0, p0, Lfpw;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040207

    .line 242
    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 244
    :cond_0
    invoke-virtual {p0, p1}, Lfpw;->a(I)Lfof;

    move-result-object v1

    .line 245
    const v0, 0x7f100036

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 246
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 248
    const v0, 0x7f1005dd

    .line 249
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 251
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 253
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 254
    iget-object v2, p0, Lfpw;->b:Landroid/content/Context;

    .line 255
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00c4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 256
    iget-object v3, p0, Lfpw;->b:Landroid/content/Context;

    .line 257
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    .line 256
    invoke-static {v1, v2, v2, v3}, Lfsp;->a(Lfof;IIF)Ljava/lang/String;

    move-result-object v2

    .line 258
    iget-object v3, p0, Lfpw;->b:Landroid/content/Context;

    sget-object v4, Ljac;->a:Ljac;

    invoke-static {v3, v2, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 259
    const v0, 0x7f1005de

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 260
    invoke-virtual {v1}, Lfof;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    const v0, 0x7f1005df

    .line 262
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 263
    invoke-virtual {v1}, Lfof;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    :goto_0
    return-object p2

    .line 267
    :pswitch_1
    if-nez p2, :cond_1

    .line 268
    iget-object v0, p0, Lfpw;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040206

    .line 269
    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 271
    :cond_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x2

    return v0
.end method
