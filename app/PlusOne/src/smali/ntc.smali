.class public final Lntc;
.super Loxq;
.source "PG"


# instance fields
.field public a:[I

.field public b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 830
    invoke-direct {p0}, Loxq;-><init>()V

    .line 839
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lntc;->a:[I

    .line 830
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 870
    .line 871
    iget-object v1, p0, Lntc;->a:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Lntc;->a:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 873
    iget-object v2, p0, Lntc;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 875
    invoke-static {v4}, Loxo;->i(I)I

    move-result v4

    add-int/2addr v1, v4

    .line 873
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 878
    :cond_0
    iget-object v0, p0, Lntc;->a:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 880
    :cond_1
    iget-object v1, p0, Lntc;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 881
    const/4 v1, 0x2

    iget-object v2, p0, Lntc;->b:Ljava/lang/Boolean;

    .line 882
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 884
    :cond_2
    iget-object v1, p0, Lntc;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 885
    const/4 v1, 0x3

    iget-object v2, p0, Lntc;->c:Ljava/lang/Boolean;

    .line 886
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 888
    :cond_3
    iget-object v1, p0, Lntc;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 889
    const/4 v1, 0x4

    iget-object v2, p0, Lntc;->d:Ljava/lang/Boolean;

    .line 890
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 892
    :cond_4
    iget-object v1, p0, Lntc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 893
    iput v0, p0, Lntc;->ai:I

    .line 894
    return v0
.end method

.method public a(Loxn;)Lntc;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 902
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 903
    sparse-switch v0, :sswitch_data_0

    .line 907
    iget-object v1, p0, Lntc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 908
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lntc;->ah:Ljava/util/List;

    .line 911
    :cond_1
    iget-object v1, p0, Lntc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 913
    :sswitch_0
    return-object p0

    .line 918
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 919
    iget-object v0, p0, Lntc;->a:[I

    array-length v0, v0

    .line 920
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 921
    iget-object v2, p0, Lntc;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 922
    iput-object v1, p0, Lntc;->a:[I

    .line 923
    :goto_1
    iget-object v1, p0, Lntc;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 924
    iget-object v1, p0, Lntc;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 925
    invoke-virtual {p1}, Loxn;->a()I

    .line 923
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 928
    :cond_2
    iget-object v1, p0, Lntc;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 932
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lntc;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 936
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lntc;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 940
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lntc;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 903
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 850
    iget-object v0, p0, Lntc;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lntc;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 851
    iget-object v1, p0, Lntc;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 852
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 851
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 855
    :cond_0
    iget-object v0, p0, Lntc;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 856
    const/4 v0, 0x2

    iget-object v1, p0, Lntc;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 858
    :cond_1
    iget-object v0, p0, Lntc;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 859
    const/4 v0, 0x3

    iget-object v1, p0, Lntc;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 861
    :cond_2
    iget-object v0, p0, Lntc;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 862
    const/4 v0, 0x4

    iget-object v1, p0, Lntc;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 864
    :cond_3
    iget-object v0, p0, Lntc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 866
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 826
    invoke-virtual {p0, p1}, Lntc;->a(Loxn;)Lntc;

    move-result-object v0

    return-object v0
.end method
