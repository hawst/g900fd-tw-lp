.class public final Ljar;
.super Llol;
.source "PG"

# interfaces
.implements Lkhf;


# static fields
.field private static final N:Lloy;


# instance fields
.field private O:Lkhe;

.field private P:Lkhr;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lloy;

    const-string v1, "debug.plus.dogfood"

    invoke-direct {v0, v1}, Lloy;-><init>(Ljava/lang/String;)V

    sput-object v0, Ljar;->N:Lloy;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Llol;-><init>()V

    .line 29
    new-instance v0, Lkhe;

    iget-object v1, p0, Ljar;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    iput-object v0, p0, Ljar;->O:Lkhe;

    .line 33
    new-instance v0, Lhmg;

    sget-object v1, Loni;->c:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Ljar;->au:Llnh;

    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 36
    return-void
.end method

.method static synthetic a(Ljar;)Llnl;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ljar;->at:Llnl;

    return-object v0
.end method

.method static synthetic b(Ljar;)Llnl;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ljar;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 40
    new-instance v0, Lkhr;

    iget-object v1, p0, Ljar;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljar;->P:Lkhr;

    .line 41
    iget-object v0, p0, Ljar;->P:Lkhr;

    const v1, 0x7f0a03b3

    .line 42
    invoke-virtual {p0, v1}, Ljar;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a03b4

    .line 43
    invoke-virtual {p0, v2}, Ljar;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 41
    invoke-virtual {v0, v1, v2}, Lkhr;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    move-result-object v0

    .line 44
    const-string v1, "com.google.android.libraries.social.media.settings.MediaSettings.low_bandwidth_key"

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/String;)V

    .line 45
    iget-object v1, p0, Ljar;->at:Llnl;

    .line 46
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "com.google.android.libraries.social.media.settings.MediaSettings.low_bandwidth_key"

    const/4 v3, 0x0

    .line 47
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 45
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Z)V

    .line 48
    new-instance v1, Ljas;

    invoke-direct {v1, p0}, Ljas;-><init>(Ljar;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->a(Lkhp;)V

    .line 61
    iget-object v1, p0, Ljar;->O:Lkhe;

    invoke-virtual {v1, v0}, Lkhe;->a(Lkhl;)Lkhl;

    .line 63
    sget-object v0, Ljar;->N:Lloy;

    .line 64
    return-void
.end method
