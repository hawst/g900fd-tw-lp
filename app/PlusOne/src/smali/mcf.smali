.class public final Lmcf;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlm;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 484
    invoke-direct {p0}, Loxq;-><init>()V

    .line 487
    iput-object v0, p0, Lmcf;->apiHeader:Llyr;

    .line 490
    iput-object v0, p0, Lmcf;->a:Lnlm;

    .line 484
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 507
    const/4 v0, 0x0

    .line 508
    iget-object v1, p0, Lmcf;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 509
    const/4 v0, 0x1

    iget-object v1, p0, Lmcf;->apiHeader:Llyr;

    .line 510
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 512
    :cond_0
    iget-object v1, p0, Lmcf;->a:Lnlm;

    if-eqz v1, :cond_1

    .line 513
    const/4 v1, 0x2

    iget-object v2, p0, Lmcf;->a:Lnlm;

    .line 514
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 516
    :cond_1
    iget-object v1, p0, Lmcf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 517
    iput v0, p0, Lmcf;->ai:I

    .line 518
    return v0
.end method

.method public a(Loxn;)Lmcf;
    .locals 2

    .prologue
    .line 526
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 527
    sparse-switch v0, :sswitch_data_0

    .line 531
    iget-object v1, p0, Lmcf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 532
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcf;->ah:Ljava/util/List;

    .line 535
    :cond_1
    iget-object v1, p0, Lmcf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 537
    :sswitch_0
    return-object p0

    .line 542
    :sswitch_1
    iget-object v0, p0, Lmcf;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 543
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmcf;->apiHeader:Llyr;

    .line 545
    :cond_2
    iget-object v0, p0, Lmcf;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 549
    :sswitch_2
    iget-object v0, p0, Lmcf;->a:Lnlm;

    if-nez v0, :cond_3

    .line 550
    new-instance v0, Lnlm;

    invoke-direct {v0}, Lnlm;-><init>()V

    iput-object v0, p0, Lmcf;->a:Lnlm;

    .line 552
    :cond_3
    iget-object v0, p0, Lmcf;->a:Lnlm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 527
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, Lmcf;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 496
    const/4 v0, 0x1

    iget-object v1, p0, Lmcf;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 498
    :cond_0
    iget-object v0, p0, Lmcf;->a:Lnlm;

    if-eqz v0, :cond_1

    .line 499
    const/4 v0, 0x2

    iget-object v1, p0, Lmcf;->a:Lnlm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 501
    :cond_1
    iget-object v0, p0, Lmcf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 503
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 480
    invoke-virtual {p0, p1}, Lmcf;->a(Loxn;)Lmcf;

    move-result-object v0

    return-object v0
.end method
