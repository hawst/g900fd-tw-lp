.class public final Lojb;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Loja;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6072
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6075
    sget-object v0, Loja;->a:[Loja;

    iput-object v0, p0, Lojb;->a:[Loja;

    .line 6072
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 6093
    .line 6094
    iget-object v1, p0, Lojb;->a:[Loja;

    if-eqz v1, :cond_1

    .line 6095
    iget-object v2, p0, Lojb;->a:[Loja;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 6096
    if-eqz v4, :cond_0

    .line 6097
    const/4 v5, 0x1

    .line 6098
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 6095
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6102
    :cond_1
    iget-object v1, p0, Lojb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6103
    iput v0, p0, Lojb;->ai:I

    .line 6104
    return v0
.end method

.method public a(Loxn;)Lojb;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6112
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6113
    sparse-switch v0, :sswitch_data_0

    .line 6117
    iget-object v2, p0, Lojb;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 6118
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lojb;->ah:Ljava/util/List;

    .line 6121
    :cond_1
    iget-object v2, p0, Lojb;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6123
    :sswitch_0
    return-object p0

    .line 6128
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 6129
    iget-object v0, p0, Lojb;->a:[Loja;

    if-nez v0, :cond_3

    move v0, v1

    .line 6130
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loja;

    .line 6131
    iget-object v3, p0, Lojb;->a:[Loja;

    if-eqz v3, :cond_2

    .line 6132
    iget-object v3, p0, Lojb;->a:[Loja;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 6134
    :cond_2
    iput-object v2, p0, Lojb;->a:[Loja;

    .line 6135
    :goto_2
    iget-object v2, p0, Lojb;->a:[Loja;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 6136
    iget-object v2, p0, Lojb;->a:[Loja;

    new-instance v3, Loja;

    invoke-direct {v3}, Loja;-><init>()V

    aput-object v3, v2, v0

    .line 6137
    iget-object v2, p0, Lojb;->a:[Loja;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 6138
    invoke-virtual {p1}, Loxn;->a()I

    .line 6135
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6129
    :cond_3
    iget-object v0, p0, Lojb;->a:[Loja;

    array-length v0, v0

    goto :goto_1

    .line 6141
    :cond_4
    iget-object v2, p0, Lojb;->a:[Loja;

    new-instance v3, Loja;

    invoke-direct {v3}, Loja;-><init>()V

    aput-object v3, v2, v0

    .line 6142
    iget-object v2, p0, Lojb;->a:[Loja;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6113
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 6080
    iget-object v0, p0, Lojb;->a:[Loja;

    if-eqz v0, :cond_1

    .line 6081
    iget-object v1, p0, Lojb;->a:[Loja;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 6082
    if-eqz v3, :cond_0

    .line 6083
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 6081
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6087
    :cond_1
    iget-object v0, p0, Lojb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6089
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6068
    invoke-virtual {p0, p1}, Lojb;->a(Loxn;)Lojb;

    move-result-object v0

    return-object v0
.end method
