.class public final Loju;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lohv;

.field private b:Loin;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 13
    sget-object v0, Lohv;->a:[Lohv;

    iput-object v0, p0, Loju;->a:[Lohv;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Loju;->b:Loin;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 47
    .line 48
    iget-object v1, p0, Loju;->a:[Lohv;

    if-eqz v1, :cond_1

    .line 49
    iget-object v2, p0, Loju;->a:[Lohv;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 50
    if-eqz v4, :cond_0

    .line 51
    const/4 v5, 0x2

    .line 52
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 49
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56
    :cond_1
    iget-object v1, p0, Loju;->b:Loin;

    if-eqz v1, :cond_2

    .line 57
    const/4 v1, 0x3

    iget-object v2, p0, Loju;->b:Loin;

    .line 58
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 60
    :cond_2
    iget-object v1, p0, Loju;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 61
    const/4 v1, 0x4

    iget-object v2, p0, Loju;->c:Ljava/lang/String;

    .line 62
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64
    :cond_3
    iget-object v1, p0, Loju;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 65
    const/4 v1, 0x5

    iget-object v2, p0, Loju;->d:Ljava/lang/Boolean;

    .line 66
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 68
    :cond_4
    iget-object v1, p0, Loju;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    iput v0, p0, Loju;->ai:I

    .line 70
    return v0
.end method

.method public a(Loxn;)Loju;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 78
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 79
    sparse-switch v0, :sswitch_data_0

    .line 83
    iget-object v2, p0, Loju;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 84
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loju;->ah:Ljava/util/List;

    .line 87
    :cond_1
    iget-object v2, p0, Loju;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    :sswitch_0
    return-object p0

    .line 94
    :sswitch_1
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 95
    iget-object v0, p0, Loju;->a:[Lohv;

    if-nez v0, :cond_3

    move v0, v1

    .line 96
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lohv;

    .line 97
    iget-object v3, p0, Loju;->a:[Lohv;

    if-eqz v3, :cond_2

    .line 98
    iget-object v3, p0, Loju;->a:[Lohv;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    :cond_2
    iput-object v2, p0, Loju;->a:[Lohv;

    .line 101
    :goto_2
    iget-object v2, p0, Loju;->a:[Lohv;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 102
    iget-object v2, p0, Loju;->a:[Lohv;

    new-instance v3, Lohv;

    invoke-direct {v3}, Lohv;-><init>()V

    aput-object v3, v2, v0

    .line 103
    iget-object v2, p0, Loju;->a:[Lohv;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 104
    invoke-virtual {p1}, Loxn;->a()I

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 95
    :cond_3
    iget-object v0, p0, Loju;->a:[Lohv;

    array-length v0, v0

    goto :goto_1

    .line 107
    :cond_4
    iget-object v2, p0, Loju;->a:[Lohv;

    new-instance v3, Lohv;

    invoke-direct {v3}, Lohv;-><init>()V

    aput-object v3, v2, v0

    .line 108
    iget-object v2, p0, Loju;->a:[Lohv;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 112
    :sswitch_2
    iget-object v0, p0, Loju;->b:Loin;

    if-nez v0, :cond_5

    .line 113
    new-instance v0, Loin;

    invoke-direct {v0}, Loin;-><init>()V

    iput-object v0, p0, Loju;->b:Loin;

    .line 115
    :cond_5
    iget-object v0, p0, Loju;->b:Loin;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 119
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loju;->c:Ljava/lang/String;

    goto :goto_0

    .line 123
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loju;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 79
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 25
    iget-object v0, p0, Loju;->a:[Lohv;

    if-eqz v0, :cond_1

    .line 26
    iget-object v1, p0, Loju;->a:[Lohv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 27
    if-eqz v3, :cond_0

    .line 28
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 26
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_1
    iget-object v0, p0, Loju;->b:Loin;

    if-eqz v0, :cond_2

    .line 33
    const/4 v0, 0x3

    iget-object v1, p0, Loju;->b:Loin;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 35
    :cond_2
    iget-object v0, p0, Loju;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 36
    const/4 v0, 0x4

    iget-object v1, p0, Loju;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 38
    :cond_3
    iget-object v0, p0, Loju;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 39
    const/4 v0, 0x5

    iget-object v1, p0, Loju;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 41
    :cond_4
    iget-object v0, p0, Loju;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 43
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loju;->a(Loxn;)Loju;

    move-result-object v0

    return-object v0
.end method
