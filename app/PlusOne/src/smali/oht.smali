.class public final Loht;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loht;


# instance fields
.field public b:Lohp;

.field public c:Ljava/lang/String;

.field private d:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3126
    const/4 v0, 0x0

    new-array v0, v0, [Loht;

    sput-object v0, Loht;->a:[Loht;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3127
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3130
    const/4 v0, 0x0

    iput-object v0, p0, Loht;->b:Lohp;

    .line 3135
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Loht;->d:[Ljava/lang/String;

    .line 3127
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3157
    .line 3158
    iget-object v0, p0, Loht;->b:Lohp;

    if-eqz v0, :cond_3

    .line 3159
    const/4 v0, 0x1

    iget-object v2, p0, Loht;->b:Lohp;

    .line 3160
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3162
    :goto_0
    iget-object v2, p0, Loht;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 3163
    const/4 v2, 0x2

    iget-object v3, p0, Loht;->c:Ljava/lang/String;

    .line 3164
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3166
    :cond_0
    iget-object v2, p0, Loht;->d:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Loht;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 3168
    iget-object v3, p0, Loht;->d:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 3170
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 3168
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3172
    :cond_1
    add-int/2addr v0, v2

    .line 3173
    iget-object v1, p0, Loht;->d:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3175
    :cond_2
    iget-object v1, p0, Loht;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3176
    iput v0, p0, Loht;->ai:I

    .line 3177
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loht;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3185
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3186
    sparse-switch v0, :sswitch_data_0

    .line 3190
    iget-object v1, p0, Loht;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3191
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loht;->ah:Ljava/util/List;

    .line 3194
    :cond_1
    iget-object v1, p0, Loht;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3196
    :sswitch_0
    return-object p0

    .line 3201
    :sswitch_1
    iget-object v0, p0, Loht;->b:Lohp;

    if-nez v0, :cond_2

    .line 3202
    new-instance v0, Lohp;

    invoke-direct {v0}, Lohp;-><init>()V

    iput-object v0, p0, Loht;->b:Lohp;

    .line 3204
    :cond_2
    iget-object v0, p0, Loht;->b:Lohp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3208
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loht;->c:Ljava/lang/String;

    goto :goto_0

    .line 3212
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 3213
    iget-object v0, p0, Loht;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 3214
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 3215
    iget-object v2, p0, Loht;->d:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3216
    iput-object v1, p0, Loht;->d:[Ljava/lang/String;

    .line 3217
    :goto_1
    iget-object v1, p0, Loht;->d:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 3218
    iget-object v1, p0, Loht;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3219
    invoke-virtual {p1}, Loxn;->a()I

    .line 3217
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3222
    :cond_3
    iget-object v1, p0, Loht;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 3186
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 3140
    iget-object v0, p0, Loht;->b:Lohp;

    if-eqz v0, :cond_0

    .line 3141
    const/4 v0, 0x1

    iget-object v1, p0, Loht;->b:Lohp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3143
    :cond_0
    iget-object v0, p0, Loht;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 3144
    const/4 v0, 0x2

    iget-object v1, p0, Loht;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 3146
    :cond_1
    iget-object v0, p0, Loht;->d:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 3147
    iget-object v1, p0, Loht;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 3148
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 3147
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3151
    :cond_2
    iget-object v0, p0, Loht;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3153
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3123
    invoke-virtual {p0, p1}, Loht;->a(Loxn;)Loht;

    move-result-object v0

    return-object v0
.end method
