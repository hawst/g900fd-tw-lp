.class public Lyc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/lang/reflect/Method;


# instance fields
.field b:I

.field private c:Landroid/content/Context;

.field private d:Landroid/widget/PopupWindow;

.field private e:Landroid/widget/ListAdapter;

.field private f:Lyf;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Z

.field private l:I

.field private m:Landroid/database/DataSetObserver;

.field private n:Landroid/view/View;

.field private o:Landroid/widget/AdapterView$OnItemClickListener;

.field private final p:Lyn;

.field private final q:Lym;

.field private final r:Lyl;

.field private final s:Lyj;

.field private t:Landroid/os/Handler;

.field private u:Landroid/graphics/Rect;

.field private v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 80
    :try_start_0
    const-class v0, Landroid/widget/PopupWindow;

    const-string v1, "setClipToScreenEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lyc;->a:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 83
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 192
    const/4 v0, 0x0

    const v1, 0x7f0100e5

    invoke-direct {p0, p1, v0, v1}, Lyc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lyc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 216
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 4

    .prologue
    const/4 v0, -0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput v0, p0, Lyc;->g:I

    .line 93
    iput v0, p0, Lyc;->h:I

    .line 98
    iput v2, p0, Lyc;->l:I

    .line 100
    const v0, 0x7fffffff

    iput v0, p0, Lyc;->b:I

    .line 105
    new-instance v0, Lyn;

    invoke-direct {v0, p0}, Lyn;-><init>(Lyc;)V

    iput-object v0, p0, Lyc;->p:Lyn;

    .line 117
    new-instance v0, Lym;

    invoke-direct {v0, p0}, Lym;-><init>(Lyc;)V

    iput-object v0, p0, Lyc;->q:Lym;

    .line 118
    new-instance v0, Lyl;

    invoke-direct {v0, p0}, Lyl;-><init>(Lyc;)V

    iput-object v0, p0, Lyc;->r:Lyl;

    .line 119
    new-instance v0, Lyj;

    invoke-direct {v0, p0}, Lyj;-><init>(Lyc;)V

    iput-object v0, p0, Lyc;->s:Lyj;

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lyc;->t:Landroid/os/Handler;

    .line 124
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lyc;->u:Landroid/graphics/Rect;

    .line 228
    iput-object p1, p0, Lyc;->c:Landroid/content/Context;

    .line 230
    sget-object v0, Lqk;->j:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 232
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lyc;->i:I

    .line 234
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lyc;->j:I

    .line 236
    iget v1, p0, Lyc;->j:I

    if-eqz v1, :cond_0

    .line 237
    iput-boolean v3, p0, Lyc;->k:Z

    .line 239
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 241
    new-instance v0, Ltm;

    invoke-direct {v0, p1, p2, p3}, Ltm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    .line 242
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 245
    iget-object v0, p0, Lyc;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 246
    invoke-static {v0}, Lge;->a(Ljava/util/Locale;)I

    .line 247
    return-void
.end method

.method static synthetic a(Lyc;)Lyf;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lyc;->f:Lyf;

    return-object v0
.end method

.method static synthetic b(Lyc;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic c(Lyc;)Lyn;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lyc;->p:Lyn;

    return-object v0
.end method

.method static synthetic d(Lyc;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lyc;->t:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 671
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 672
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 674
    iput-object v1, p0, Lyc;->f:Lyf;

    .line 675
    iget-object v0, p0, Lyc;->t:Landroid/os/Handler;

    iget-object v1, p0, Lyc;->p:Lyn;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 676
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 481
    iput p1, p0, Lyc;->l:I

    .line 482
    return-void
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 399
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lyc;->n:Landroid/view/View;

    .line 436
    return-void
.end method

.method public a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 541
    iput-object p1, p0, Lyc;->o:Landroid/widget/AdapterView$OnItemClickListener;

    .line 542
    return-void
.end method

.method public a(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lyc;->m:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    .line 257
    new-instance v0, Lyk;

    invoke-direct {v0, p0}, Lyk;-><init>(Lyc;)V

    iput-object v0, p0, Lyc;->m:Landroid/database/DataSetObserver;

    .line 261
    :cond_0
    :goto_0
    iput-object p1, p0, Lyc;->e:Landroid/widget/ListAdapter;

    .line 262
    iget-object v0, p0, Lyc;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 263
    iget-object v0, p0, Lyc;->m:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 266
    :cond_1
    iget-object v0, p0, Lyc;->f:Lyf;

    if-eqz v0, :cond_2

    .line 267
    iget-object v0, p0, Lyc;->f:Lyf;

    iget-object v1, p0, Lyc;->e:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Lyf;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 269
    :cond_2
    return-void

    .line 258
    :cond_3
    iget-object v0, p0, Lyc;->e:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lyc;->e:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lyc;->m:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method

.method public a(Landroid/widget/PopupWindow$OnDismissListener;)V
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 685
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 304
    iput-boolean p1, p0, Lyc;->v:Z

    .line 305
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 306
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 498
    iput p1, p0, Lyc;->h:I

    .line 499
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, -0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 584
    iget-object v0, p0, Lyc;->f:Lyf;

    if-nez v0, :cond_3

    iget-object v3, p0, Lyc;->c:Landroid/content/Context;

    new-instance v0, Lyd;

    invoke-direct {v0, p0}, Lyd;-><init>(Lyc;)V

    new-instance v4, Lyf;

    iget-boolean v0, p0, Lyc;->v:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-direct {v4, v3, v0}, Lyf;-><init>(Landroid/content/Context;Z)V

    iput-object v4, p0, Lyc;->f:Lyf;

    iget-object v0, p0, Lyc;->f:Lyf;

    iget-object v3, p0, Lyc;->e:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v3}, Lyf;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lyc;->f:Lyf;

    iget-object v3, p0, Lyc;->o:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Lyf;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lyc;->f:Lyf;

    invoke-virtual {v0, v1}, Lyf;->setFocusable(Z)V

    iget-object v0, p0, Lyc;->f:Lyf;

    invoke-virtual {v0, v1}, Lyf;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lyc;->f:Lyf;

    new-instance v3, Lye;

    invoke-direct {v3, p0}, Lye;-><init>(Lyc;)V

    invoke-virtual {v0, v3}, Lyf;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lyc;->f:Lyf;

    iget-object v3, p0, Lyc;->r:Lyl;

    invoke-virtual {v0, v3}, Lyf;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lyc;->f:Lyf;

    iget-object v3, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v3, v0}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    :goto_1
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v3, p0, Lyc;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lyc;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lyc;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    iget-boolean v3, p0, Lyc;->k:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lyc;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    neg-int v3, v3

    iput v3, p0, Lyc;->j:I

    :cond_0
    :goto_2
    iget-object v3, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    iget-object v3, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lyc;->d()Landroid/view/View;

    move-result-object v4

    iget v6, p0, Lyc;->j:I

    invoke-virtual {v3, v4, v6}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;I)I

    move-result v4

    iget v3, p0, Lyc;->g:I

    if-ne v3, v5, :cond_5

    add-int/2addr v0, v4

    .line 589
    :goto_3
    invoke-virtual {p0}, Lyc;->f()Z

    move-result v3

    .line 591
    iget-object v4, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 592
    iget v4, p0, Lyc;->h:I

    if-ne v4, v5, :cond_6

    move v4, v5

    .line 602
    :goto_4
    iget v6, p0, Lyc;->g:I

    if-ne v6, v5, :cond_c

    .line 605
    if-eqz v3, :cond_8

    .line 606
    :goto_5
    if-eqz v3, :cond_a

    .line 607
    iget-object v3, p0, Lyc;->d:Landroid/widget/PopupWindow;

    iget v6, p0, Lyc;->h:I

    if-ne v6, v5, :cond_9

    :goto_6
    invoke-virtual {v3, v5, v2}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    move v5, v0

    .line 622
    :goto_7
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 624
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lyc;->d()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lyc;->i:I

    iget v3, p0, Lyc;->j:I

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    .line 665
    :cond_1
    :goto_8
    return-void

    :cond_2
    move v0, v2

    .line 584
    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lyc;->u:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    move v0, v2

    goto :goto_2

    :cond_5
    iget v3, p0, Lyc;->h:I

    packed-switch v3, :pswitch_data_0

    iget v3, p0, Lyc;->h:I

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    :goto_9
    iget-object v6, p0, Lyc;->f:Lyf;

    invoke-virtual {v6, v3, v4, v5}, Lyf;->a(III)I

    move-result v3

    if-lez v3, :cond_16

    :goto_a
    add-int/2addr v0, v3

    goto :goto_3

    :pswitch_0
    iget-object v3, p0, Lyc;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v6, p0, Lyc;->u:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lyc;->u:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    sub-int/2addr v3, v6

    const/high16 v6, -0x80000000

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto :goto_9

    :pswitch_1
    iget-object v3, p0, Lyc;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v6, p0, Lyc;->u:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lyc;->u:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    sub-int/2addr v3, v6

    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    goto :goto_9

    .line 596
    :cond_6
    iget v4, p0, Lyc;->h:I

    if-ne v4, v8, :cond_7

    .line 597
    invoke-virtual {p0}, Lyc;->d()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    goto/16 :goto_4

    .line 599
    :cond_7
    iget v4, p0, Lyc;->h:I

    goto/16 :goto_4

    :cond_8
    move v0, v5

    .line 605
    goto/16 :goto_5

    :cond_9
    move v5, v2

    .line 607
    goto/16 :goto_6

    .line 611
    :cond_a
    iget-object v3, p0, Lyc;->d:Landroid/widget/PopupWindow;

    iget v6, p0, Lyc;->h:I

    if-ne v6, v5, :cond_b

    move v2, v5

    :cond_b
    invoke-virtual {v3, v2, v5}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    move v5, v0

    goto/16 :goto_7

    .line 616
    :cond_c
    iget v2, p0, Lyc;->g:I

    if-ne v2, v8, :cond_d

    move v5, v0

    .line 617
    goto/16 :goto_7

    .line 619
    :cond_d
    iget v5, p0, Lyc;->g:I

    goto/16 :goto_7

    .line 627
    :cond_e
    iget v3, p0, Lyc;->h:I

    if-ne v3, v5, :cond_12

    move v3, v5

    .line 637
    :goto_b
    iget v4, p0, Lyc;->g:I

    if-ne v4, v5, :cond_14

    move v2, v5

    .line 647
    :goto_c
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v3, v2}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    .line 648
    sget-object v0, Lyc;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_f

    :try_start_0
    sget-object v0, Lyc;->a:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lyc;->d:Landroid/widget/PopupWindow;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 652
    :cond_f
    :goto_d
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 653
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lyc;->q:Lym;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    .line 654
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lyc;->d()Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lyc;->i:I

    iget v3, p0, Lyc;->j:I

    iget v4, p0, Lyc;->l:I

    invoke-static {v0, v1, v2, v3, v4}, Lnt;->a(Landroid/widget/PopupWindow;Landroid/view/View;III)V

    .line 656
    iget-object v0, p0, Lyc;->f:Lyf;

    invoke-virtual {v0, v5}, Lyf;->setSelection(I)V

    .line 658
    iget-boolean v0, p0, Lyc;->v:Z

    if-eqz v0, :cond_10

    iget-object v0, p0, Lyc;->f:Lyf;

    invoke-virtual {v0}, Lyf;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 659
    :cond_10
    invoke-virtual {p0}, Lyc;->e()V

    .line 661
    :cond_11
    iget-boolean v0, p0, Lyc;->v:Z

    if-nez v0, :cond_1

    .line 662
    iget-object v0, p0, Lyc;->t:Landroid/os/Handler;

    iget-object v1, p0, Lyc;->s:Lyj;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_8

    .line 630
    :cond_12
    iget v3, p0, Lyc;->h:I

    if-ne v3, v8, :cond_13

    .line 631
    iget-object v3, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {p0}, Lyc;->d()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_b

    .line 633
    :cond_13
    iget-object v3, p0, Lyc;->d:Landroid/widget/PopupWindow;

    iget v4, p0, Lyc;->h:I

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    move v3, v2

    goto :goto_b

    .line 640
    :cond_14
    iget v4, p0, Lyc;->g:I

    if-ne v4, v8, :cond_15

    .line 641
    iget-object v4, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v4, v0}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto :goto_c

    .line 643
    :cond_15
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    iget v4, p0, Lyc;->g:I

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    goto/16 :goto_c

    :catch_0
    move-exception v0

    goto :goto_d

    :cond_16
    move v0, v2

    goto/16 :goto_a

    .line 584
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 509
    if-eqz v0, :cond_0

    .line 510
    iget-object v1, p0, Lyc;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 511
    iget-object v0, p0, Lyc;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lyc;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lyc;->h:I

    .line 515
    :goto_0
    return-void

    .line 513
    :cond_0
    invoke-virtual {p0, p1}, Lyc;->b(I)V

    goto :goto_0
.end method

.method public d()Landroid/view/View;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lyc;->n:Landroid/view/View;

    return-object v0
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 711
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 747
    iget-object v0, p0, Lyc;->f:Lyf;

    .line 748
    if-eqz v0, :cond_0

    .line 750
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lyf;->a(Lyf;Z)Z

    .line 752
    invoke-virtual {v0}, Lyf;->requestLayout()V

    .line 754
    :cond_0
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 768
    iget-object v0, p0, Lyc;->d:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->getInputMethodMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 845
    iget-object v0, p0, Lyc;->f:Lyf;

    return-object v0
.end method
