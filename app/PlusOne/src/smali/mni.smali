.class public final Lmni;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmni;


# instance fields
.field public b:Lmnw;

.field public c:Lmnw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4304
    const/4 v0, 0x0

    new-array v0, v0, [Lmni;

    sput-object v0, Lmni;->a:[Lmni;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4305
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4308
    iput-object v0, p0, Lmni;->b:Lmnw;

    .line 4311
    iput-object v0, p0, Lmni;->c:Lmnw;

    .line 4305
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4328
    const/4 v0, 0x0

    .line 4329
    iget-object v1, p0, Lmni;->b:Lmnw;

    if-eqz v1, :cond_0

    .line 4330
    const/4 v0, 0x1

    iget-object v1, p0, Lmni;->b:Lmnw;

    .line 4331
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4333
    :cond_0
    iget-object v1, p0, Lmni;->c:Lmnw;

    if-eqz v1, :cond_1

    .line 4334
    const/4 v1, 0x2

    iget-object v2, p0, Lmni;->c:Lmnw;

    .line 4335
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4337
    :cond_1
    iget-object v1, p0, Lmni;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4338
    iput v0, p0, Lmni;->ai:I

    .line 4339
    return v0
.end method

.method public a(Loxn;)Lmni;
    .locals 2

    .prologue
    .line 4347
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4348
    sparse-switch v0, :sswitch_data_0

    .line 4352
    iget-object v1, p0, Lmni;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4353
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmni;->ah:Ljava/util/List;

    .line 4356
    :cond_1
    iget-object v1, p0, Lmni;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4358
    :sswitch_0
    return-object p0

    .line 4363
    :sswitch_1
    iget-object v0, p0, Lmni;->b:Lmnw;

    if-nez v0, :cond_2

    .line 4364
    new-instance v0, Lmnw;

    invoke-direct {v0}, Lmnw;-><init>()V

    iput-object v0, p0, Lmni;->b:Lmnw;

    .line 4366
    :cond_2
    iget-object v0, p0, Lmni;->b:Lmnw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4370
    :sswitch_2
    iget-object v0, p0, Lmni;->c:Lmnw;

    if-nez v0, :cond_3

    .line 4371
    new-instance v0, Lmnw;

    invoke-direct {v0}, Lmnw;-><init>()V

    iput-object v0, p0, Lmni;->c:Lmnw;

    .line 4373
    :cond_3
    iget-object v0, p0, Lmni;->c:Lmnw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4348
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4316
    iget-object v0, p0, Lmni;->b:Lmnw;

    if-eqz v0, :cond_0

    .line 4317
    const/4 v0, 0x1

    iget-object v1, p0, Lmni;->b:Lmnw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4319
    :cond_0
    iget-object v0, p0, Lmni;->c:Lmnw;

    if-eqz v0, :cond_1

    .line 4320
    const/4 v0, 0x2

    iget-object v1, p0, Lmni;->c:Lmnw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4322
    :cond_1
    iget-object v0, p0, Lmni;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4324
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4301
    invoke-virtual {p0, p1}, Lmni;->a(Loxn;)Lmni;

    move-result-object v0

    return-object v0
.end method
