.class public Lkcj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkcp;
.implements Llnx;
.implements Llqa;
.implements Llqz;
.implements Llra;
.implements Llrg;


# static fields
.field private static final a:Landroid/view/animation/Interpolator;

.field private static final b:Landroid/view/animation/Interpolator;


# instance fields
.field private c:Lkcn;

.field private d:Landroid/view/WindowManager;

.field private e:I

.field private f:I

.field private g:Z

.field private h:Landroid/view/View;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/view/View;

.field private k:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

.field private l:Landroid/animation/Animator$AnimatorListener;

.field private m:Ljava/lang/Runnable;

.field private n:Landroid/app/Activity;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3fc00000    # 1.5f

    .line 81
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v1}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Lkcj;->a:Landroid/view/animation/Interpolator;

    .line 82
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lkcj;->b:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Lkcj;->e:I

    .line 103
    iput-object p1, p0, Lkcj;->n:Landroid/app/Activity;

    .line 104
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 105
    return-void
.end method

.method static synthetic a(Lkcj;)I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lkcj;->f:I

    return v0
.end method

.method static synthetic a(Lkcj;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lkcj;->d(I)V

    return-void
.end method

.method static synthetic a(Lkcj;Z)Z
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lkcj;->g:Z

    return p1
.end method

.method private b(F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 197
    iget-object v0, p0, Lkcj;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 198
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lkcj;->j:Landroid/view/View;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    iget-object v0, p0, Lkcj;->j:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleX(F)V

    .line 207
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v0, p0, Lkcj;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    div-int/lit8 v1, v0, 0x2

    .line 202
    iget-object v0, p0, Lkcj;->j:Landroid/view/View;

    .line 203
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 204
    invoke-virtual {v0, v1, v2, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 205
    iget-object v1, p0, Lkcj;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 9

    .prologue
    .line 391
    iget-boolean v0, p0, Lkcj;->o:Z

    if-eq p1, v0, :cond_0

    .line 392
    iput-boolean p1, p0, Lkcj;->o:Z

    .line 394
    if-eqz p1, :cond_1

    .line 395
    iget-object v6, p0, Lkcj;->d:Landroid/view/WindowManager;

    iget-object v7, p0, Lkcj;->h:Landroid/view/View;

    .line 396
    invoke-direct {p0}, Lkcj;->f()I

    move-result v8

    invoke-direct {p0}, Lkcj;->g()I

    move-result v2

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x1

    const/16 v3, 0x3e8

    const/16 v4, 0x8

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v8, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 395
    invoke-interface {v6, v7, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    :try_start_0
    iget-object v0, p0, Lkcj;->d:Landroid/view/WindowManager;

    iget-object v1, p0, Lkcj;->h:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private c(Z)V
    .locals 10

    .prologue
    .line 408
    iget-boolean v0, p0, Lkcj;->p:Z

    if-eq p1, v0, :cond_0

    .line 409
    iput-boolean p1, p0, Lkcj;->p:Z

    .line 411
    if-eqz p1, :cond_1

    .line 412
    iget-object v6, p0, Lkcj;->d:Landroid/view/WindowManager;

    iget-object v7, p0, Lkcj;->k:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    .line 413
    invoke-direct {p0}, Lkcj;->f()I

    move-result v8

    invoke-direct {p0}, Lkcj;->g()I

    move-result v9

    iget-object v0, p0, Lkcj;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x1

    const/16 v3, 0x3e8

    const/16 v4, 0x8

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    add-int v1, v8, v9

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 412
    invoke-interface {v6, v7, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    :try_start_0
    iget-object v0, p0, Lkcj;->d:Landroid/view/WindowManager;

    iget-object v1, p0, Lkcj;->k:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private d(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 295
    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 296
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iput p1, p0, Lkcj;->f:I

    iget-boolean v0, p0, Lkcj;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lkcj;->l:Landroid/animation/Animator$AnimatorListener;

    if-nez v0, :cond_0

    new-instance v0, Lkcm;

    invoke-direct {v0, p0}, Lkcm;-><init>(Lkcj;)V

    iput-object v0, p0, Lkcj;->l:Landroid/animation/Animator$AnimatorListener;

    :cond_0
    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTranslationY(F)V

    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lkcj;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lkcj;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lkcj;->l:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 302
    :cond_1
    :goto_0
    return-void

    .line 300
    :cond_2
    invoke-virtual {p0, p1}, Lkcj;->c(I)V

    goto :goto_0
.end method

.method private f()I
    .locals 5

    .prologue
    .line 425
    const/4 v0, 0x0

    .line 426
    iget-object v1, p0, Lkcj;->n:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "status_bar_height"

    const-string v3, "dimen"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 429
    if-lez v1, :cond_0

    .line 430
    iget-object v0, p0, Lkcj;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 432
    :cond_0
    return v0
.end method

.method private g()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 436
    iget-object v0, p0, Lkcj;->n:Landroid/app/Activity;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x7f0100b3

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 438
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 439
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 440
    return v1
.end method


# virtual methods
.method public E_()V
    .locals 1

    .prologue
    .line 155
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 156
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkcj;->c(I)V

    .line 157
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lkcj;->a:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    invoke-direct {p0, v0}, Lkcj;->b(F)V

    .line 162
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 147
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 109
    const-class v0, Lkcn;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkcn;

    iput-object v0, p0, Lkcj;->c:Lkcn;

    .line 111
    const-class v0, Llhe;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llhe;

    .line 112
    if-eqz v0, :cond_0

    .line 113
    new-instance v1, Lkck;

    invoke-direct {v1, p0}, Lkck;-><init>(Lkcj;)V

    invoke-virtual {v0, v1}, Llhe;->a(Llhh;)V

    .line 122
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 126
    iget-object v1, p0, Lkcj;->n:Landroid/app/Activity;

    .line 128
    const-string v0, "window"

    invoke-virtual {v1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lkcj;->d:Landroid/view/WindowManager;

    .line 130
    const v0, 0x7f0401c7

    invoke-static {v1, v0, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lkcj;->h:Landroid/view/View;

    .line 131
    iget-object v0, p0, Lkcj;->h:Landroid/view/View;

    const v2, 0x7f10054c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    .line 132
    iget-object v0, p0, Lkcj;->h:Landroid/view/View;

    const v2, 0x7f10054d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lkcj;->j:Landroid/view/View;

    .line 133
    const v0, 0x7f0401c6

    invoke-static {v1, v0, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    iput-object v0, p0, Lkcj;->k:Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSyncProgressView;

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkcj;->q:Z

    .line 138
    const/4 v0, -0x1

    iput v0, p0, Lkcj;->r:I

    .line 140
    iget-object v0, p0, Lkcj;->c:Lkcn;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lkcj;->c:Lkcn;

    invoke-interface {v0, v1, p0}, Lkcn;->a(Landroid/app/Activity;Lkcj;)V

    .line 143
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 479
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lkcj;->q:Z

    if-eqz v0, :cond_0

    .line 480
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkcj;->q:Z

    .line 481
    iget v0, p0, Lkcj;->r:I

    invoke-virtual {p0, v0}, Lkcj;->c(I)V

    .line 482
    const/4 v0, -0x1

    iput v0, p0, Lkcj;->r:I

    .line 484
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lkcj;->c(I)V

    .line 167
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 151
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 170
    iget v0, p0, Lkcj;->e:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 171
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lkcj;->c(I)V

    .line 175
    :cond_0
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lkcj;->j:Landroid/view/View;

    invoke-static {v0}, Llii;->h(Landroid/view/View;)V

    iget-object v0, p0, Lkcj;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lkcj;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 184
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lkcj;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v1, v0, 0x2

    .line 179
    iget-object v0, p0, Lkcj;->j:Landroid/view/View;

    .line 180
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 181
    invoke-virtual {v0, v1, v2, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 182
    iget-object v1, p0, Lkcj;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method c(I)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 221
    iget-boolean v0, p0, Lkcj;->q:Z

    if-eqz v0, :cond_2

    .line 223
    iget-object v0, p0, Lkcj;->n:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->hasWindowFocus()Z

    move-result v0

    if-nez v0, :cond_1

    .line 224
    iput p1, p0, Lkcj;->r:I

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iput-boolean v4, p0, Lkcj;->q:Z

    .line 230
    :cond_2
    iget v0, p0, Lkcj;->e:I

    if-eq p1, v0, :cond_0

    .line 234
    iput p1, p0, Lkcj;->e:I

    .line 235
    iget-object v0, p0, Lkcj;->m:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 237
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 279
    :pswitch_0
    invoke-direct {p0, v4}, Lkcj;->b(Z)V

    .line 280
    invoke-direct {p0, v4}, Lkcj;->c(Z)V

    goto :goto_0

    .line 239
    :pswitch_1
    invoke-direct {p0, v3}, Lkcj;->b(Z)V

    .line 240
    invoke-direct {p0, v4}, Lkcj;->c(Z)V

    .line 242
    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    const v1, 0x7f0a039d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 243
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lkcj;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTranslationY(F)V

    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lkcj;->b:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_3
    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 248
    :pswitch_2
    invoke-direct {p0, v2}, Lkcj;->b(F)V

    .line 249
    invoke-direct {p0, v3}, Lkcj;->b(Z)V

    .line 250
    invoke-direct {p0, v3}, Lkcj;->c(Z)V

    .line 252
    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    const v1, 0x7f0a039e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 253
    invoke-static {}, Llsj;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setAlpha(F)V

    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTranslationY(F)V

    :cond_4
    iget-object v0, p0, Lkcj;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    new-instance v0, Lkcl;

    invoke-direct {v0, p0}, Lkcl;-><init>(Lkcj;)V

    iput-object v0, p0, Lkcj;->m:Ljava/lang/Runnable;

    .line 261
    iget-object v0, p0, Lkcj;->m:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_0

    .line 267
    :pswitch_3
    invoke-direct {p0, v4}, Lkcj;->b(Z)V

    .line 268
    invoke-direct {p0, v3}, Lkcj;->c(Z)V

    goto/16 :goto_0

    .line 273
    :pswitch_4
    invoke-direct {p0, v4}, Lkcj;->c(Z)V

    .line 274
    invoke-direct {p0, v4}, Lkcj;->d(I)V

    goto/16 :goto_0

    .line 237
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method d()I
    .locals 2

    .prologue
    .line 216
    iget v0, p0, Lkcj;->r:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lkcj;->r:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lkcj;->e:I

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 291
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkcj;->c(I)V

    .line 292
    return-void
.end method
