.class public final Lmod;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmne;

.field public b:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3683
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3686
    const/4 v0, 0x0

    iput-object v0, p0, Lmod;->a:Lmne;

    .line 3683
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3705
    const/4 v0, 0x0

    .line 3706
    iget-object v1, p0, Lmod;->a:Lmne;

    if-eqz v1, :cond_0

    .line 3707
    const/4 v0, 0x1

    iget-object v1, p0, Lmod;->a:Lmne;

    .line 3708
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3710
    :cond_0
    iget-object v1, p0, Lmod;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 3711
    const/4 v1, 0x2

    iget-object v2, p0, Lmod;->b:Ljava/lang/Float;

    .line 3712
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 3714
    :cond_1
    iget-object v1, p0, Lmod;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3715
    iput v0, p0, Lmod;->ai:I

    .line 3716
    return v0
.end method

.method public a(Loxn;)Lmod;
    .locals 2

    .prologue
    .line 3724
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3725
    sparse-switch v0, :sswitch_data_0

    .line 3729
    iget-object v1, p0, Lmod;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3730
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmod;->ah:Ljava/util/List;

    .line 3733
    :cond_1
    iget-object v1, p0, Lmod;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3735
    :sswitch_0
    return-object p0

    .line 3740
    :sswitch_1
    iget-object v0, p0, Lmod;->a:Lmne;

    if-nez v0, :cond_2

    .line 3741
    new-instance v0, Lmne;

    invoke-direct {v0}, Lmne;-><init>()V

    iput-object v0, p0, Lmod;->a:Lmne;

    .line 3743
    :cond_2
    iget-object v0, p0, Lmod;->a:Lmne;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3747
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmod;->b:Ljava/lang/Float;

    goto :goto_0

    .line 3725
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3693
    iget-object v0, p0, Lmod;->a:Lmne;

    if-eqz v0, :cond_0

    .line 3694
    const/4 v0, 0x1

    iget-object v1, p0, Lmod;->a:Lmne;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3696
    :cond_0
    iget-object v0, p0, Lmod;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 3697
    const/4 v0, 0x2

    iget-object v1, p0, Lmod;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 3699
    :cond_1
    iget-object v0, p0, Lmod;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3701
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3679
    invoke-virtual {p0, p1}, Lmod;->a(Loxn;)Lmod;

    move-result-object v0

    return-object v0
.end method
