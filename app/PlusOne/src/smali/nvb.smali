.class public final Lnvb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnvb;


# instance fields
.field public b:Ljava/lang/String;

.field public c:I

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2611
    const/4 v0, 0x0

    new-array v0, v0, [Lnvb;

    sput-object v0, Lnvb;->a:[Lnvb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2612
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2619
    const/high16 v0, -0x80000000

    iput v0, p0, Lnvb;->c:I

    .line 2612
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2639
    const/4 v0, 0x0

    .line 2640
    iget-object v1, p0, Lnvb;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2641
    const/4 v0, 0x1

    iget-object v1, p0, Lnvb;->b:Ljava/lang/String;

    .line 2642
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2644
    :cond_0
    iget-object v1, p0, Lnvb;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2645
    const/4 v1, 0x2

    iget-object v2, p0, Lnvb;->d:Ljava/lang/String;

    .line 2646
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2648
    :cond_1
    iget v1, p0, Lnvb;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_2

    .line 2649
    const/4 v1, 0x3

    iget v2, p0, Lnvb;->c:I

    .line 2650
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2652
    :cond_2
    iget-object v1, p0, Lnvb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2653
    iput v0, p0, Lnvb;->ai:I

    .line 2654
    return v0
.end method

.method public a(Loxn;)Lnvb;
    .locals 2

    .prologue
    .line 2662
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2663
    sparse-switch v0, :sswitch_data_0

    .line 2667
    iget-object v1, p0, Lnvb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2668
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnvb;->ah:Ljava/util/List;

    .line 2671
    :cond_1
    iget-object v1, p0, Lnvb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2673
    :sswitch_0
    return-object p0

    .line 2678
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvb;->b:Ljava/lang/String;

    goto :goto_0

    .line 2682
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnvb;->d:Ljava/lang/String;

    goto :goto_0

    .line 2686
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2687
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 2691
    :cond_2
    iput v0, p0, Lnvb;->c:I

    goto :goto_0

    .line 2693
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnvb;->c:I

    goto :goto_0

    .line 2663
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2624
    iget-object v0, p0, Lnvb;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2625
    const/4 v0, 0x1

    iget-object v1, p0, Lnvb;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2627
    :cond_0
    iget-object v0, p0, Lnvb;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2628
    const/4 v0, 0x2

    iget-object v1, p0, Lnvb;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2630
    :cond_1
    iget v0, p0, Lnvb;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    .line 2631
    const/4 v0, 0x3

    iget v1, p0, Lnvb;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2633
    :cond_2
    iget-object v0, p0, Lnvb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2635
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2608
    invoke-virtual {p0, p1}, Lnvb;->a(Loxn;)Lnvb;

    move-result-object v0

    return-object v0
.end method
