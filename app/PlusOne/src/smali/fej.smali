.class public final Lfej;
.super Lhxz;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lhxz",
        "<[",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:Ljava/lang/String;


# instance fields
.field private e:[Landroid/database/Cursor;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "avatar"

    aput-object v1, v0, v5

    const-string v1, "in_same_visibility_group"

    aput-object v1, v0, v6

    sput-object v0, Lfej;->b:[Ljava/lang/String;

    .line 26
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v5

    sput-object v0, Lfej;->c:[Ljava/lang/String;

    .line 32
    const-string v0, "%s=\'%s\' and %s=\'%s\' and %s!=\'\'"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "account_type"

    aput-object v2, v1, v3

    const-string v2, "com.google"

    aput-object v2, v1, v4

    const-string v2, "mimetype"

    aput-object v2, v1, v5

    const-string v2, "vnd.android.cursor.item/email_v2"

    aput-object v2, v1, v6

    const-string v2, "data1"

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfej;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lhxz;-><init>(Landroid/content/Context;)V

    .line 39
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/database/Cursor;

    iput-object v0, p0, Lfej;->e:[Landroid/database/Cursor;

    .line 44
    iput p2, p0, Lfej;->f:I

    .line 45
    return-void
.end method


# virtual methods
.method protected i()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 72
    invoke-super {p0}, Lhxz;->i()V

    .line 73
    iget-object v0, p0, Lfej;->e:[Landroid/database/Cursor;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lfej;->e:[Landroid/database/Cursor;

    aget-object v0, v0, v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 76
    :cond_0
    iget-object v0, p0, Lfej;->e:[Landroid/database/Cursor;

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lfej;->e:[Landroid/database/Cursor;

    aget-object v0, v0, v2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 79
    :cond_1
    return-void
.end method

.method public synthetic j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lfej;->l()[Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public l()[Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 49
    const-string v4, "in_my_circles=1"

    .line 52
    const-string v6, "name COLLATE LOCALIZED ASC"

    .line 53
    iget-object v7, p0, Lfej;->e:[Landroid/database/Cursor;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lfej;->n()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lfej;->f:I

    sget-object v3, Lfej;->b:[Ljava/lang/String;

    move-object v5, v2

    invoke-static/range {v0 .. v6}, Ldsm;->a(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v7, v8

    .line 58
    invoke-virtual {p0}, Lfej;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 59
    iget v1, p0, Lfej;->f:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_plus_page"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    const-string v8, "display_name COLLATE LOCALIZED ASC"

    .line 61
    iget-object v0, p0, Lfej;->e:[Landroid/database/Cursor;

    const/4 v1, 0x1

    .line 62
    invoke-virtual {p0}, Lfej;->n()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v5, Lfej;->c:[Ljava/lang/String;

    sget-object v6, Lfej;->d:Ljava/lang/String;

    move-object v7, v2

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v0, v1

    .line 67
    :cond_0
    iget-object v0, p0, Lfej;->e:[Landroid/database/Cursor;

    return-object v0
.end method
