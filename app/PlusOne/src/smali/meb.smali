.class public final Lmeb;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lpfm;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3562
    invoke-direct {p0}, Loxq;-><init>()V

    .line 3565
    iput-object v0, p0, Lmeb;->apiHeader:Llyr;

    .line 3568
    iput-object v0, p0, Lmeb;->a:Lpfm;

    .line 3562
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 3585
    const/4 v0, 0x0

    .line 3586
    iget-object v1, p0, Lmeb;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 3587
    const/4 v0, 0x1

    iget-object v1, p0, Lmeb;->apiHeader:Llyr;

    .line 3588
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3590
    :cond_0
    iget-object v1, p0, Lmeb;->a:Lpfm;

    if-eqz v1, :cond_1

    .line 3591
    const/4 v1, 0x2

    iget-object v2, p0, Lmeb;->a:Lpfm;

    .line 3592
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3594
    :cond_1
    iget-object v1, p0, Lmeb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3595
    iput v0, p0, Lmeb;->ai:I

    .line 3596
    return v0
.end method

.method public a(Loxn;)Lmeb;
    .locals 2

    .prologue
    .line 3604
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3605
    sparse-switch v0, :sswitch_data_0

    .line 3609
    iget-object v1, p0, Lmeb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 3610
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmeb;->ah:Ljava/util/List;

    .line 3613
    :cond_1
    iget-object v1, p0, Lmeb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3615
    :sswitch_0
    return-object p0

    .line 3620
    :sswitch_1
    iget-object v0, p0, Lmeb;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 3621
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmeb;->apiHeader:Llyr;

    .line 3623
    :cond_2
    iget-object v0, p0, Lmeb;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3627
    :sswitch_2
    iget-object v0, p0, Lmeb;->a:Lpfm;

    if-nez v0, :cond_3

    .line 3628
    new-instance v0, Lpfm;

    invoke-direct {v0}, Lpfm;-><init>()V

    iput-object v0, p0, Lmeb;->a:Lpfm;

    .line 3630
    :cond_3
    iget-object v0, p0, Lmeb;->a:Lpfm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3605
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 3573
    iget-object v0, p0, Lmeb;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 3574
    const/4 v0, 0x1

    iget-object v1, p0, Lmeb;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3576
    :cond_0
    iget-object v0, p0, Lmeb;->a:Lpfm;

    if-eqz v0, :cond_1

    .line 3577
    const/4 v0, 0x2

    iget-object v1, p0, Lmeb;->a:Lpfm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 3579
    :cond_1
    iget-object v0, p0, Lmeb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 3581
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 3558
    invoke-virtual {p0, p1}, Lmeb;->a(Loxn;)Lmeb;

    move-result-object v0

    return-object v0
.end method
