.class public final Lpez;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Llvu;

.field public c:Llvw;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    const v0, 0x482bd10

    new-instance v1, Lpfa;

    invoke-direct {v1}, Lpfa;-><init>()V

    .line 14
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 13
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    iput-object v0, p0, Lpez;->b:Llvu;

    .line 22
    iput-object v0, p0, Lpez;->c:Llvw;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 43
    iget-object v1, p0, Lpez;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 44
    const/4 v0, 0x1

    iget-object v1, p0, Lpez;->a:Ljava/lang/String;

    .line 45
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 47
    :cond_0
    iget-object v1, p0, Lpez;->b:Llvu;

    if-eqz v1, :cond_1

    .line 48
    const/4 v1, 0x2

    iget-object v2, p0, Lpez;->b:Llvu;

    .line 49
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_1
    iget-object v1, p0, Lpez;->c:Llvw;

    if-eqz v1, :cond_2

    .line 52
    const/4 v1, 0x3

    iget-object v2, p0, Lpez;->c:Llvw;

    .line 53
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 55
    :cond_2
    iget-object v1, p0, Lpez;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 56
    iput v0, p0, Lpez;->ai:I

    .line 57
    return v0
.end method

.method public a(Loxn;)Lpez;
    .locals 2

    .prologue
    .line 65
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 66
    sparse-switch v0, :sswitch_data_0

    .line 70
    iget-object v1, p0, Lpez;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpez;->ah:Ljava/util/List;

    .line 74
    :cond_1
    iget-object v1, p0, Lpez;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    :sswitch_0
    return-object p0

    .line 81
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpez;->a:Ljava/lang/String;

    goto :goto_0

    .line 85
    :sswitch_2
    iget-object v0, p0, Lpez;->b:Llvu;

    if-nez v0, :cond_2

    .line 86
    new-instance v0, Llvu;

    invoke-direct {v0}, Llvu;-><init>()V

    iput-object v0, p0, Lpez;->b:Llvu;

    .line 88
    :cond_2
    iget-object v0, p0, Lpez;->b:Llvu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 92
    :sswitch_3
    iget-object v0, p0, Lpez;->c:Llvw;

    if-nez v0, :cond_3

    .line 93
    new-instance v0, Llvw;

    invoke-direct {v0}, Llvw;-><init>()V

    iput-object v0, p0, Lpez;->c:Llvw;

    .line 95
    :cond_3
    iget-object v0, p0, Lpez;->c:Llvw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 66
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lpez;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-object v1, p0, Lpez;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 30
    :cond_0
    iget-object v0, p0, Lpez;->b:Llvu;

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x2

    iget-object v1, p0, Lpez;->b:Llvu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 33
    :cond_1
    iget-object v0, p0, Lpez;->c:Llvw;

    if-eqz v0, :cond_2

    .line 34
    const/4 v0, 0x3

    iget-object v1, p0, Lpez;->c:Llvw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 36
    :cond_2
    iget-object v0, p0, Lpez;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 38
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpez;->a(Loxn;)Lpez;

    move-result-object v0

    return-object v0
.end method
