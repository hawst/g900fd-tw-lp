.class public final Lnwr;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Lnwt;

.field private c:[Ljava/lang/Long;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/String;

.field private g:Lnws;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 14
    iput-object v1, p0, Lnwr;->b:Lnwt;

    .line 17
    sget-object v0, Loxx;->h:[Ljava/lang/Long;

    iput-object v0, p0, Lnwr;->c:[Ljava/lang/Long;

    .line 26
    iput-object v1, p0, Lnwr;->g:Lnws;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 60
    .line 61
    iget-object v0, p0, Lnwr;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 62
    const/4 v0, 0x1

    iget-object v2, p0, Lnwr;->a:Ljava/lang/Boolean;

    .line 63
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 65
    :goto_0
    iget-object v2, p0, Lnwr;->b:Lnwt;

    if-eqz v2, :cond_0

    .line 66
    const/4 v2, 0x2

    iget-object v3, p0, Lnwr;->b:Lnwt;

    .line 67
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 69
    :cond_0
    iget-object v2, p0, Lnwr;->c:[Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lnwr;->c:[Ljava/lang/Long;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 71
    iget-object v3, p0, Lnwr;->c:[Ljava/lang/Long;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 73
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 71
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 75
    :cond_1
    add-int/2addr v0, v2

    .line 76
    iget-object v1, p0, Lnwr;->c:[Ljava/lang/Long;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 78
    :cond_2
    iget-object v1, p0, Lnwr;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 79
    const/4 v1, 0x4

    iget-object v2, p0, Lnwr;->d:Ljava/lang/String;

    .line 80
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 82
    :cond_3
    iget-object v1, p0, Lnwr;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 83
    const/4 v1, 0x5

    iget-object v2, p0, Lnwr;->e:Ljava/lang/Integer;

    .line 84
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 86
    :cond_4
    iget-object v1, p0, Lnwr;->g:Lnws;

    if-eqz v1, :cond_5

    .line 87
    const/4 v1, 0x6

    iget-object v2, p0, Lnwr;->g:Lnws;

    .line 88
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_5
    iget-object v1, p0, Lnwr;->f:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 91
    const/4 v1, 0x7

    iget-object v2, p0, Lnwr;->f:Ljava/lang/String;

    .line 92
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_6
    iget-object v1, p0, Lnwr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 95
    iput v0, p0, Lnwr;->ai:I

    .line 96
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnwr;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 104
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 105
    sparse-switch v0, :sswitch_data_0

    .line 109
    iget-object v1, p0, Lnwr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnwr;->ah:Ljava/util/List;

    .line 113
    :cond_1
    iget-object v1, p0, Lnwr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    :sswitch_0
    return-object p0

    .line 120
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnwr;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 124
    :sswitch_2
    iget-object v0, p0, Lnwr;->b:Lnwt;

    if-nez v0, :cond_2

    .line 125
    new-instance v0, Lnwt;

    invoke-direct {v0}, Lnwt;-><init>()V

    iput-object v0, p0, Lnwr;->b:Lnwt;

    .line 127
    :cond_2
    iget-object v0, p0, Lnwr;->b:Lnwt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 131
    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 132
    iget-object v0, p0, Lnwr;->c:[Ljava/lang/Long;

    array-length v0, v0

    .line 133
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Long;

    .line 134
    iget-object v2, p0, Lnwr;->c:[Ljava/lang/Long;

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 135
    iput-object v1, p0, Lnwr;->c:[Ljava/lang/Long;

    .line 136
    :goto_1
    iget-object v1, p0, Lnwr;->c:[Ljava/lang/Long;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 137
    iget-object v1, p0, Lnwr;->c:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    .line 138
    invoke-virtual {p1}, Loxn;->a()I

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 141
    :cond_3
    iget-object v1, p0, Lnwr;->c:[Ljava/lang/Long;

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 145
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnwr;->d:Ljava/lang/String;

    goto :goto_0

    .line 149
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnwr;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 153
    :sswitch_6
    iget-object v0, p0, Lnwr;->g:Lnws;

    if-nez v0, :cond_4

    .line 154
    new-instance v0, Lnws;

    invoke-direct {v0}, Lnws;-><init>()V

    iput-object v0, p0, Lnwr;->g:Lnws;

    .line 156
    :cond_4
    iget-object v0, p0, Lnwr;->g:Lnws;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 160
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnwr;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 105
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    .line 31
    iget-object v0, p0, Lnwr;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x1

    iget-object v1, p0, Lnwr;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 34
    :cond_0
    iget-object v0, p0, Lnwr;->b:Lnwt;

    if-eqz v0, :cond_1

    .line 35
    const/4 v0, 0x2

    iget-object v1, p0, Lnwr;->b:Lnwt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 37
    :cond_1
    iget-object v0, p0, Lnwr;->c:[Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 38
    iget-object v1, p0, Lnwr;->c:[Ljava/lang/Long;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 39
    const/4 v4, 0x3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p1, v4, v6, v7}, Loxo;->b(IJ)V

    .line 38
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_2
    iget-object v0, p0, Lnwr;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 43
    const/4 v0, 0x4

    iget-object v1, p0, Lnwr;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 45
    :cond_3
    iget-object v0, p0, Lnwr;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 46
    const/4 v0, 0x5

    iget-object v1, p0, Lnwr;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 48
    :cond_4
    iget-object v0, p0, Lnwr;->g:Lnws;

    if-eqz v0, :cond_5

    .line 49
    const/4 v0, 0x6

    iget-object v1, p0, Lnwr;->g:Lnws;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 51
    :cond_5
    iget-object v0, p0, Lnwr;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 52
    const/4 v0, 0x7

    iget-object v1, p0, Lnwr;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 54
    :cond_6
    iget-object v0, p0, Lnwr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 56
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lnwr;->a(Loxn;)Lnwr;

    move-result-object v0

    return-object v0
.end method
