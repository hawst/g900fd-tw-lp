.class public final Lnka;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnka;


# instance fields
.field public b:Lnja;

.field public c:Lnie;

.field public d:Ljava/lang/String;

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6776
    const/4 v0, 0x0

    new-array v0, v0, [Lnka;

    sput-object v0, Lnka;->a:[Lnka;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6777
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6804
    iput-object v0, p0, Lnka;->b:Lnja;

    .line 6807
    iput-object v0, p0, Lnka;->c:Lnie;

    .line 6812
    const/high16 v0, -0x80000000

    iput v0, p0, Lnka;->e:I

    .line 6777
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6835
    const/4 v0, 0x0

    .line 6836
    iget-object v1, p0, Lnka;->b:Lnja;

    if-eqz v1, :cond_0

    .line 6837
    const/4 v0, 0x1

    iget-object v1, p0, Lnka;->b:Lnja;

    .line 6838
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6840
    :cond_0
    iget-object v1, p0, Lnka;->c:Lnie;

    if-eqz v1, :cond_1

    .line 6841
    const/4 v1, 0x2

    iget-object v2, p0, Lnka;->c:Lnie;

    .line 6842
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6844
    :cond_1
    iget-object v1, p0, Lnka;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 6845
    const/4 v1, 0x3

    iget-object v2, p0, Lnka;->d:Ljava/lang/String;

    .line 6846
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6848
    :cond_2
    iget v1, p0, Lnka;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 6849
    const/4 v1, 0x4

    iget v2, p0, Lnka;->e:I

    .line 6850
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6852
    :cond_3
    iget-object v1, p0, Lnka;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6853
    iput v0, p0, Lnka;->ai:I

    .line 6854
    return v0
.end method

.method public a(Loxn;)Lnka;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6862
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6863
    sparse-switch v0, :sswitch_data_0

    .line 6867
    iget-object v1, p0, Lnka;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6868
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnka;->ah:Ljava/util/List;

    .line 6871
    :cond_1
    iget-object v1, p0, Lnka;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6873
    :sswitch_0
    return-object p0

    .line 6878
    :sswitch_1
    iget-object v0, p0, Lnka;->b:Lnja;

    if-nez v0, :cond_2

    .line 6879
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnka;->b:Lnja;

    .line 6881
    :cond_2
    iget-object v0, p0, Lnka;->b:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6885
    :sswitch_2
    iget-object v0, p0, Lnka;->c:Lnie;

    if-nez v0, :cond_3

    .line 6886
    new-instance v0, Lnie;

    invoke-direct {v0}, Lnie;-><init>()V

    iput-object v0, p0, Lnka;->c:Lnie;

    .line 6888
    :cond_3
    iget-object v0, p0, Lnka;->c:Lnie;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6892
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnka;->d:Ljava/lang/String;

    goto :goto_0

    .line 6896
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 6897
    if-eq v0, v2, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_4

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4

    const/4 v1, 0x7

    if-eq v0, v1, :cond_4

    const/16 v1, 0x8

    if-eq v0, v1, :cond_4

    const/16 v1, 0x9

    if-eq v0, v1, :cond_4

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    const/16 v1, 0xc

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-eq v0, v1, :cond_4

    const/16 v1, 0xe

    if-eq v0, v1, :cond_4

    const/16 v1, 0xf

    if-eq v0, v1, :cond_4

    const/16 v1, 0x10

    if-eq v0, v1, :cond_4

    const/16 v1, 0x11

    if-eq v0, v1, :cond_4

    const/16 v1, 0x12

    if-eq v0, v1, :cond_4

    const/16 v1, 0x13

    if-eq v0, v1, :cond_4

    const/16 v1, 0x14

    if-eq v0, v1, :cond_4

    const/16 v1, 0x15

    if-ne v0, v1, :cond_5

    .line 6918
    :cond_4
    iput v0, p0, Lnka;->e:I

    goto/16 :goto_0

    .line 6920
    :cond_5
    iput v2, p0, Lnka;->e:I

    goto/16 :goto_0

    .line 6863
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6817
    iget-object v0, p0, Lnka;->b:Lnja;

    if-eqz v0, :cond_0

    .line 6818
    const/4 v0, 0x1

    iget-object v1, p0, Lnka;->b:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6820
    :cond_0
    iget-object v0, p0, Lnka;->c:Lnie;

    if-eqz v0, :cond_1

    .line 6821
    const/4 v0, 0x2

    iget-object v1, p0, Lnka;->c:Lnie;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6823
    :cond_1
    iget-object v0, p0, Lnka;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 6824
    const/4 v0, 0x3

    iget-object v1, p0, Lnka;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 6826
    :cond_2
    iget v0, p0, Lnka;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 6827
    const/4 v0, 0x4

    iget v1, p0, Lnka;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 6829
    :cond_3
    iget-object v0, p0, Lnka;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6831
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6773
    invoke-virtual {p0, p1}, Lnka;->a(Loxn;)Lnka;

    move-result-object v0

    return-object v0
.end method
