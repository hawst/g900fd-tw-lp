.class public final Lopm;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Long;

.field private b:[Lpme;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    sget-object v0, Lpme;->a:[Lpme;

    iput-object v0, p0, Lopm;->b:[Lpme;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 36
    .line 37
    iget-object v0, p0, Lopm;->a:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 38
    const/4 v0, 0x1

    iget-object v2, p0, Lopm;->a:Ljava/lang/Long;

    .line 39
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 41
    :goto_0
    iget-object v2, p0, Lopm;->b:[Lpme;

    if-eqz v2, :cond_1

    .line 42
    iget-object v2, p0, Lopm;->b:[Lpme;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 43
    if-eqz v4, :cond_0

    .line 44
    const/4 v5, 0x2

    .line 45
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 42
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 49
    :cond_1
    iget-object v1, p0, Lopm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    iput v0, p0, Lopm;->ai:I

    .line 51
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lopm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 60
    sparse-switch v0, :sswitch_data_0

    .line 64
    iget-object v2, p0, Lopm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lopm;->ah:Ljava/util/List;

    .line 68
    :cond_1
    iget-object v2, p0, Lopm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    :sswitch_0
    return-object p0

    .line 75
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lopm;->a:Ljava/lang/Long;

    goto :goto_0

    .line 79
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 80
    iget-object v0, p0, Lopm;->b:[Lpme;

    if-nez v0, :cond_3

    move v0, v1

    .line 81
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpme;

    .line 82
    iget-object v3, p0, Lopm;->b:[Lpme;

    if-eqz v3, :cond_2

    .line 83
    iget-object v3, p0, Lopm;->b:[Lpme;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 85
    :cond_2
    iput-object v2, p0, Lopm;->b:[Lpme;

    .line 86
    :goto_2
    iget-object v2, p0, Lopm;->b:[Lpme;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 87
    iget-object v2, p0, Lopm;->b:[Lpme;

    new-instance v3, Lpme;

    invoke-direct {v3}, Lpme;-><init>()V

    aput-object v3, v2, v0

    .line 88
    iget-object v2, p0, Lopm;->b:[Lpme;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 89
    invoke-virtual {p1}, Loxn;->a()I

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 80
    :cond_3
    iget-object v0, p0, Lopm;->b:[Lpme;

    array-length v0, v0

    goto :goto_1

    .line 92
    :cond_4
    iget-object v2, p0, Lopm;->b:[Lpme;

    new-instance v3, Lpme;

    invoke-direct {v3}, Lpme;-><init>()V

    aput-object v3, v2, v0

    .line 93
    iget-object v2, p0, Lopm;->b:[Lpme;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 60
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 20
    iget-object v0, p0, Lopm;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x1

    iget-object v1, p0, Lopm;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 23
    :cond_0
    iget-object v0, p0, Lopm;->b:[Lpme;

    if-eqz v0, :cond_2

    .line 24
    iget-object v1, p0, Lopm;->b:[Lpme;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 25
    if-eqz v3, :cond_1

    .line 26
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 24
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_2
    iget-object v0, p0, Lopm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lopm;->a(Loxn;)Lopm;

    move-result-object v0

    return-object v0
.end method
