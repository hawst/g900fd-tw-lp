.class public final Lmjm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lneu;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26485
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26488
    iput-object v0, p0, Lmjm;->apiHeader:Llyq;

    .line 26491
    iput-object v0, p0, Lmjm;->a:Lneu;

    .line 26485
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26508
    const/4 v0, 0x0

    .line 26509
    iget-object v1, p0, Lmjm;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 26510
    const/4 v0, 0x1

    iget-object v1, p0, Lmjm;->apiHeader:Llyq;

    .line 26511
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26513
    :cond_0
    iget-object v1, p0, Lmjm;->a:Lneu;

    if-eqz v1, :cond_1

    .line 26514
    const/4 v1, 0x2

    iget-object v2, p0, Lmjm;->a:Lneu;

    .line 26515
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26517
    :cond_1
    iget-object v1, p0, Lmjm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26518
    iput v0, p0, Lmjm;->ai:I

    .line 26519
    return v0
.end method

.method public a(Loxn;)Lmjm;
    .locals 2

    .prologue
    .line 26527
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26528
    sparse-switch v0, :sswitch_data_0

    .line 26532
    iget-object v1, p0, Lmjm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26533
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjm;->ah:Ljava/util/List;

    .line 26536
    :cond_1
    iget-object v1, p0, Lmjm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26538
    :sswitch_0
    return-object p0

    .line 26543
    :sswitch_1
    iget-object v0, p0, Lmjm;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 26544
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmjm;->apiHeader:Llyq;

    .line 26546
    :cond_2
    iget-object v0, p0, Lmjm;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26550
    :sswitch_2
    iget-object v0, p0, Lmjm;->a:Lneu;

    if-nez v0, :cond_3

    .line 26551
    new-instance v0, Lneu;

    invoke-direct {v0}, Lneu;-><init>()V

    iput-object v0, p0, Lmjm;->a:Lneu;

    .line 26553
    :cond_3
    iget-object v0, p0, Lmjm;->a:Lneu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26528
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26496
    iget-object v0, p0, Lmjm;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 26497
    const/4 v0, 0x1

    iget-object v1, p0, Lmjm;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26499
    :cond_0
    iget-object v0, p0, Lmjm;->a:Lneu;

    if-eqz v0, :cond_1

    .line 26500
    const/4 v0, 0x2

    iget-object v1, p0, Lmjm;->a:Lneu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26502
    :cond_1
    iget-object v0, p0, Lmjm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26504
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26481
    invoke-virtual {p0, p1}, Lmjm;->a(Loxn;)Lmjm;

    move-result-object v0

    return-object v0
.end method
