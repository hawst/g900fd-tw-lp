.class public final Lhza;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhzn;


# instance fields
.field private a:Lhyz;

.field private b:I

.field private c:Landroid/content/Context;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhzq;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lhzl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lhyz;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 39
    iput-object p2, p0, Lhza;->a:Lhyz;

    .line 40
    iput-object p1, p0, Lhza;->c:Landroid/content/Context;

    .line 41
    invoke-interface {p2, p1}, Lhyz;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lhza;->b:I

    .line 42
    const-class v0, Lhzq;

    invoke-static {p1, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lhza;->d:Ljava/util/List;

    .line 43
    const-class v0, Lhzl;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzl;

    iput-object v0, p0, Lhza;->e:Lhzl;

    .line 44
    iget-object v0, p0, Lhza;->e:Lhzl;

    instance-of v0, v0, Lhzp;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lhza;->e:Lhzl;

    check-cast v0, Lhzp;

    invoke-interface {v0, p0}, Lhzp;->a(Lhzn;)V

    .line 47
    :cond_0
    return-void
.end method

.method private a(ILhzi;)Landroid/view/View;
    .locals 9

    .prologue
    .line 146
    iget-object v0, p0, Lhza;->a:Lhyz;

    invoke-interface {v0}, Lhyz;->d()I

    move-result v0

    .line 147
    iget v1, p0, Lhza;->b:I

    mul-int/2addr v1, p1

    .line 148
    iget v2, p0, Lhza;->b:I

    add-int/2addr v2, v1

    .line 149
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v0, v1

    .line 153
    :goto_0
    if-ge v0, v3, :cond_1

    .line 154
    sub-int v4, v0, v1

    .line 155
    invoke-virtual {p2, v4}, Lhzi;->a(I)Landroid/view/View;

    move-result-object v5

    .line 156
    iget-object v6, p0, Lhza;->a:Lhyz;

    invoke-interface {v6, v0}, Lhyz;->a(I)Landroid/os/Parcelable;

    move-result-object v6

    .line 157
    iget-object v7, p0, Lhza;->a:Lhyz;

    iget-object v8, p0, Lhza;->c:Landroid/content/Context;

    iget-object v8, p0, Lhza;->e:Lhzl;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lhza;->e:Lhzl;

    invoke-interface {v8, v6}, Lhzl;->c(Landroid/os/Parcelable;)Z

    :cond_0
    invoke-interface {v7, v0, v5}, Lhyz;->a(ILandroid/view/View;)V

    .line 158
    invoke-virtual {v5, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 159
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    invoke-virtual {p2, v4, v5}, Lhzi;->a(ILandroid/view/View;)V

    .line 161
    invoke-virtual {p2, v4}, Lhzi;->c(I)V

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_1
    :goto_1
    if-ge v0, v2, :cond_2

    .line 166
    sub-int v3, v0, v1

    invoke-virtual {p2, v3}, Lhzi;->b(I)V

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 168
    :cond_2
    return-object p2
.end method

.method private a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lhza;->a:Lhyz;

    iget-object v0, p0, Lhza;->c:Landroid/content/Context;

    .line 130
    return-object p1
.end method

.method private b(Landroid/view/View;)Landroid/view/View;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lhza;->a:Lhyz;

    iget-object v1, p0, Lhza;->c:Landroid/content/Context;

    invoke-interface {v0, p1}, Lhyz;->a(Landroid/view/View;)V

    .line 135
    return-object p1
.end method


# virtual methods
.method public a(ILandroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 51
    invoke-virtual {p0}, Lhza;->notifyDataSetChanged()V

    .line 52
    return-void
.end method

.method public a(ILjava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lhza;->a(ILandroid/os/Parcelable;)V

    .line 57
    return-void
.end method

.method public getCount()I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    iget-object v0, p0, Lhza;->a:Lhyz;

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return v2

    .line 83
    :cond_0
    iget-object v0, p0, Lhza;->a:Lhyz;

    invoke-interface {v0}, Lhyz;->d()I

    move-result v0

    iget v3, p0, Lhza;->b:I

    add-int/lit8 v3, v3, -0x1

    add-int/2addr v0, v3

    iget v3, p0, Lhza;->b:I

    div-int v3, v0, v3

    .line 84
    iget-object v0, p0, Lhza;->a:Lhyz;

    invoke-interface {v0}, Lhyz;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 85
    :goto_1
    iget-object v4, p0, Lhza;->a:Lhyz;

    invoke-interface {v4}, Lhyz;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 87
    :goto_2
    add-int/2addr v0, v3

    add-int v2, v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 84
    goto :goto_1

    :cond_2
    move v1, v2

    .line 85
    goto :goto_2
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lhza;->a:Lhyz;

    invoke-interface {v0, p1}, Lhyz;->a(I)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 114
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lhza;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lhza;->a:Lhyz;

    invoke-interface {v0}, Lhyz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    .line 66
    :goto_0
    return v0

    .line 63
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lhza;->a:Lhyz;

    invoke-interface {v0}, Lhyz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    const/4 v0, 0x0

    goto :goto_0

    .line 66
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lhza;->a:Lhyz;

    invoke-interface {v0}, Lhyz;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 93
    if-nez p2, :cond_0

    iget-object v0, p0, Lhza;->a:Lhyz;

    iget-object v1, p0, Lhza;->c:Landroid/content/Context;

    .line 94
    invoke-interface {v0}, Lhyz;->b()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lhza;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    .line 95
    :cond_0
    invoke-direct {p0, p2}, Lhza;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lhza;->a:Lhyz;

    invoke-interface {v0}, Lhyz;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lhza;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    .line 97
    if-nez p2, :cond_2

    iget-object v0, p0, Lhza;->a:Lhyz;

    iget-object v1, p0, Lhza;->c:Landroid/content/Context;

    .line 98
    invoke-interface {v0, v1}, Lhyz;->c(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lhza;->b(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 99
    :cond_2
    invoke-direct {p0, p2}, Lhza;->b(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 101
    :cond_3
    if-nez p2, :cond_4

    .line 102
    new-instance v0, Lhzi;

    iget-object v1, p0, Lhza;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lhzi;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lhza;->a:Lhyz;

    iget v2, p0, Lhza;->b:I

    invoke-virtual {v0, v1, v2}, Lhzi;->a(Lhyz;I)V

    invoke-direct {p0, p1, v0}, Lhza;->a(ILhzi;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_4
    check-cast p2, Lhzi;

    .line 103
    invoke-direct {p0, p1, p2}, Lhza;->a(ILhzi;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x3

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 119
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lhza;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 120
    iget-object v0, p0, Lhza;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzq;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-interface {v0, v1}, Lhzq;->a(Landroid/os/Parcelable;)V

    .line 119
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 122
    :cond_0
    iget-object v0, p0, Lhza;->a:Lhyz;

    instance-of v0, v0, Lhzq;

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Lhza;->a:Lhyz;

    check-cast v0, Lhzq;

    .line 124
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-interface {v0, v1}, Lhzq;->a(Landroid/os/Parcelable;)V

    .line 126
    :cond_1
    return-void
.end method
