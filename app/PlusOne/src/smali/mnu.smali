.class public final Lmnu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2936
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2957
    const/4 v0, 0x0

    .line 2958
    iget-object v1, p0, Lmnu;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 2959
    const/4 v0, 0x1

    iget-object v1, p0, Lmnu;->a:Ljava/lang/Float;

    .line 2960
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 2962
    :cond_0
    iget-object v1, p0, Lmnu;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 2963
    const/4 v1, 0x2

    iget-object v2, p0, Lmnu;->b:Ljava/lang/Float;

    .line 2964
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2966
    :cond_1
    iget-object v1, p0, Lmnu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2967
    iput v0, p0, Lmnu;->ai:I

    .line 2968
    return v0
.end method

.method public a(Loxn;)Lmnu;
    .locals 2

    .prologue
    .line 2976
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2977
    sparse-switch v0, :sswitch_data_0

    .line 2981
    iget-object v1, p0, Lmnu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2982
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnu;->ah:Ljava/util/List;

    .line 2985
    :cond_1
    iget-object v1, p0, Lmnu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2987
    :sswitch_0
    return-object p0

    .line 2992
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnu;->a:Ljava/lang/Float;

    goto :goto_0

    .line 2996
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lmnu;->b:Ljava/lang/Float;

    goto :goto_0

    .line 2977
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2945
    iget-object v0, p0, Lmnu;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 2946
    const/4 v0, 0x1

    iget-object v1, p0, Lmnu;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 2948
    :cond_0
    iget-object v0, p0, Lmnu;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 2949
    const/4 v0, 0x2

    iget-object v1, p0, Lmnu;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 2951
    :cond_1
    iget-object v0, p0, Lmnu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2953
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2932
    invoke-virtual {p0, p1}, Lmnu;->a(Loxn;)Lmnu;

    move-result-object v0

    return-object v0
.end method
