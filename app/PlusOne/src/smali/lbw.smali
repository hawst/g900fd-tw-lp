.class public final Llbw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkzk;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static volatile b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "context_specific_data"

    aput-object v2, v0, v1

    sput-object v0, Llbw;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lpyk;Logr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    if-nez p1, :cond_0

    .line 167
    iget-object v0, p0, Lpyk;->c:Lpym;

    iget-object v0, v0, Lpym;->b:Ljava/lang/String;

    .line 169
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Logr;->af:Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(Lpyk;)Logr;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 174
    if-eqz p0, :cond_0

    iget v0, p0, Lpyk;->b:I

    const/16 v2, 0x3ea

    if-eq v0, v2, :cond_2

    :cond_0
    move-object v0, v1

    .line 181
    :cond_1
    :goto_0
    return-object v0

    .line 177
    :cond_2
    sget-object v0, Logr;->b:Loxr;

    invoke-virtual {p0, v0}, Lpyk;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Logr;

    .line 178
    if-eqz v0, :cond_3

    iget-object v2, v0, Logr;->af:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_3
    move-object v0, v1

    .line 179
    goto :goto_0
.end method


# virtual methods
.method public a(ILpyk;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    invoke-static {p2}, Llbw;->a(Lpyk;)Logr;

    move-result-object v0

    .line 88
    invoke-static {p2, v0}, Llbw;->a(Lpyk;Logr;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/content/Context;I)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 115
    sget-object v0, Llbw;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 116
    new-instance v2, Ljava/util/ArrayList;

    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/Integer;

    const/16 v3, 0x19a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v3, 0x163

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v5

    const/4 v3, 0x2

    const/16 v4, 0x164

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x3

    const/16 v4, 0x16d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x4

    const/16 v4, 0x17b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x5

    const/16 v4, 0x151

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x6

    const/16 v4, 0x158

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/4 v3, 0x7

    const/16 v4, 0x15d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0x8

    const/16 v4, 0x162

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0x9

    const/16 v4, 0x165

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0xa

    const/16 v4, 0x191

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0xb

    const/16 v4, 0x195

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0xc

    const/16 v4, 0x194

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0xd

    const/16 v4, 0x18f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0xe

    const/16 v4, 0x189

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0xf

    const/16 v4, 0x198

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0x10

    const/16 v4, 0x161

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0x11

    const/16 v4, 0x17e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    const/16 v3, 0x12

    const/16 v4, 0x14f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-class v0, Llds;

    invoke-static {p1, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llds;

    invoke-interface {v0}, Llds;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    new-array v3, v5, [Ljava/lang/String;

    const-string v4, "logged_in"

    aput-object v4, v3, v1

    invoke-interface {v0, v3}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    const/16 v0, 0x187

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    sput-object v2, Llbw;->b:Ljava/util/ArrayList;

    .line 119
    :cond_2
    sget-object v0, Llbw;->b:Ljava/util/ArrayList;

    return-object v0

    .line 116
    :cond_3
    const-class v0, Lkzl;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lkzl;->d(I)Z

    move-result v0

    goto :goto_1
.end method

.method public a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lpyk;IZ)V
    .locals 6

    .prologue
    .line 94
    invoke-static {p4}, Llbw;->a(Lpyk;)Logr;

    move-result-object v0

    .line 95
    if-nez v0, :cond_2

    .line 96
    const-string v0, "UpdateItemStoreExt"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    const-string v0, "Activity id with empty update data: "

    iget-object v1, p4, Lpyk;->c:Lpym;

    iget-object v1, v1, Lpym;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_2
    const/4 v1, 0x1

    new-array v3, v1, [Logr;

    .line 103
    const/4 v1, 0x0

    aput-object v0, v3, v1

    move-object v0, p1

    move v1, p2

    move-object v2, p3

    move v4, p5

    move v5, p6

    .line 104
    invoke-static/range {v0 .. v5}, Llap;->a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;[Logr;IZ)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;ILandroid/database/sqlite/SQLiteDatabase;Lpyk;Ljava/lang/String;JIZZLjava/lang/String;)Z
    .locals 12

    .prologue
    .line 50
    invoke-static/range {p4 .. p4}, Llbw;->a(Lpyk;)Logr;

    move-result-object v2

    .line 51
    move-object/from16 v0, p4

    invoke-static {v0, v2}, Llbw;->a(Lpyk;Logr;)Ljava/lang/String;

    move-result-object v7

    .line 52
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 53
    const-string v2, "UpdateItemStoreExt"

    const-string v3, "Missing activity ID"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const/4 v2, 0x0

    .line 82
    :goto_0
    return v2

    .line 57
    :cond_0
    new-instance v10, Landroid/content/ContentValues;

    const/4 v3, 0x4

    invoke-direct {v10, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 58
    const-string v3, "stream_key"

    move-object/from16 v0, p5

    invoke-virtual {v10, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v3, "unique_activity_id"

    invoke-virtual {v10, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v3, "sort_index"

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v10, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 61
    const-string v3, "stream_fetch_timestamp"

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v10, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 64
    if-eqz v2, :cond_2

    .line 65
    const-string v3, "context_specific_data"

    .line 66
    move/from16 v0, p9

    move/from16 v1, p10

    invoke-static {v2, v0, v1}, Lkzp;->a(Logr;ZZ)[B

    move-result-object v2

    .line 65
    invoke-virtual {v10, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 78
    :cond_1
    :goto_1
    const-string v2, "activity_streams"

    const-string v3, "unique_activity_id"

    const/4 v4, 0x5

    invoke-virtual {p3, v2, v3, v10, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 82
    const/4 v2, 0x1

    goto :goto_0

    .line 69
    :cond_2
    const-string v3, "activity_streams"

    sget-object v4, Llbw;->a:[Ljava/lang/String;

    const-string v5, "stream_key =? AND unique_activity_id =? "

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p11, v6, v2

    const/4 v2, 0x1

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, p3

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 73
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 74
    const-string v3, "context_specific_data"

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-virtual {v10, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    goto :goto_1
.end method

.method public synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Llbw;->c()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 161
    const/16 v0, 0x3ea

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
