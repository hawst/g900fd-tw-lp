.class public final Lltr;
.super Loxq;
.source "PG"


# instance fields
.field private a:Lltw;

.field private b:Loya;

.field private c:Lltt;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;

.field private f:Llts;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Boolean;

.field private i:I

.field private j:Lltv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 157
    iput-object v1, p0, Lltr;->a:Lltw;

    .line 160
    iput-object v1, p0, Lltr;->b:Loya;

    .line 163
    iput-object v1, p0, Lltr;->c:Lltt;

    .line 170
    iput-object v1, p0, Lltr;->f:Llts;

    .line 177
    const/high16 v0, -0x80000000

    iput v0, p0, Lltr;->i:I

    .line 180
    iput-object v1, p0, Lltr;->j:Lltv;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 221
    const/4 v0, 0x0

    .line 222
    iget-object v1, p0, Lltr;->a:Lltw;

    if-eqz v1, :cond_0

    .line 223
    const/4 v0, 0x1

    iget-object v1, p0, Lltr;->a:Lltw;

    .line 224
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 226
    :cond_0
    iget-object v1, p0, Lltr;->b:Loya;

    if-eqz v1, :cond_1

    .line 227
    const/4 v1, 0x2

    iget-object v2, p0, Lltr;->b:Loya;

    .line 228
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    :cond_1
    iget-object v1, p0, Lltr;->c:Lltt;

    if-eqz v1, :cond_2

    .line 231
    const/4 v1, 0x3

    iget-object v2, p0, Lltr;->c:Lltt;

    .line 232
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_2
    iget-object v1, p0, Lltr;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 235
    const/4 v1, 0x4

    iget-object v2, p0, Lltr;->d:Ljava/lang/Long;

    .line 236
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    :cond_3
    iget-object v1, p0, Lltr;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 239
    const/4 v1, 0x5

    iget-object v2, p0, Lltr;->e:Ljava/lang/String;

    .line 240
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    :cond_4
    iget-object v1, p0, Lltr;->f:Llts;

    if-eqz v1, :cond_5

    .line 243
    const/4 v1, 0x6

    iget-object v2, p0, Lltr;->f:Llts;

    .line 244
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 246
    :cond_5
    iget-object v1, p0, Lltr;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 247
    const/4 v1, 0x7

    iget-object v2, p0, Lltr;->g:Ljava/lang/String;

    .line 248
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    :cond_6
    iget-object v1, p0, Lltr;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 251
    const/16 v1, 0x8

    iget-object v2, p0, Lltr;->h:Ljava/lang/Boolean;

    .line 252
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 254
    :cond_7
    iget v1, p0, Lltr;->i:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_8

    .line 255
    const/16 v1, 0x9

    iget v2, p0, Lltr;->i:I

    .line 256
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_8
    iget-object v1, p0, Lltr;->j:Lltv;

    if-eqz v1, :cond_9

    .line 259
    const/16 v1, 0xa

    iget-object v2, p0, Lltr;->j:Lltv;

    .line 260
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    :cond_9
    iget-object v1, p0, Lltr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 263
    iput v0, p0, Lltr;->ai:I

    .line 264
    return v0
.end method

.method public a(Loxn;)Lltr;
    .locals 2

    .prologue
    .line 272
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 273
    sparse-switch v0, :sswitch_data_0

    .line 277
    iget-object v1, p0, Lltr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 278
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lltr;->ah:Ljava/util/List;

    .line 281
    :cond_1
    iget-object v1, p0, Lltr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    :sswitch_0
    return-object p0

    .line 288
    :sswitch_1
    iget-object v0, p0, Lltr;->a:Lltw;

    if-nez v0, :cond_2

    .line 289
    new-instance v0, Lltw;

    invoke-direct {v0}, Lltw;-><init>()V

    iput-object v0, p0, Lltr;->a:Lltw;

    .line 291
    :cond_2
    iget-object v0, p0, Lltr;->a:Lltw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 295
    :sswitch_2
    iget-object v0, p0, Lltr;->b:Loya;

    if-nez v0, :cond_3

    .line 296
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lltr;->b:Loya;

    .line 298
    :cond_3
    iget-object v0, p0, Lltr;->b:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 302
    :sswitch_3
    iget-object v0, p0, Lltr;->c:Lltt;

    if-nez v0, :cond_4

    .line 303
    new-instance v0, Lltt;

    invoke-direct {v0}, Lltt;-><init>()V

    iput-object v0, p0, Lltr;->c:Lltt;

    .line 305
    :cond_4
    iget-object v0, p0, Lltr;->c:Lltt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 309
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lltr;->d:Ljava/lang/Long;

    goto :goto_0

    .line 313
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltr;->e:Ljava/lang/String;

    goto :goto_0

    .line 317
    :sswitch_6
    iget-object v0, p0, Lltr;->f:Llts;

    if-nez v0, :cond_5

    .line 318
    new-instance v0, Llts;

    invoke-direct {v0}, Llts;-><init>()V

    iput-object v0, p0, Lltr;->f:Llts;

    .line 320
    :cond_5
    iget-object v0, p0, Lltr;->f:Llts;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 324
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lltr;->g:Ljava/lang/String;

    goto :goto_0

    .line 328
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lltr;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 332
    :sswitch_9
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 333
    if-eqz v0, :cond_6

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 337
    :cond_6
    iput v0, p0, Lltr;->i:I

    goto/16 :goto_0

    .line 339
    :cond_7
    const/4 v0, 0x0

    iput v0, p0, Lltr;->i:I

    goto/16 :goto_0

    .line 344
    :sswitch_a
    iget-object v0, p0, Lltr;->j:Lltv;

    if-nez v0, :cond_8

    .line 345
    new-instance v0, Lltv;

    invoke-direct {v0}, Lltv;-><init>()V

    iput-object v0, p0, Lltr;->j:Lltv;

    .line 347
    :cond_8
    iget-object v0, p0, Lltr;->j:Lltv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 273
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lltr;->a:Lltw;

    if-eqz v0, :cond_0

    .line 186
    const/4 v0, 0x1

    iget-object v1, p0, Lltr;->a:Lltw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 188
    :cond_0
    iget-object v0, p0, Lltr;->b:Loya;

    if-eqz v0, :cond_1

    .line 189
    const/4 v0, 0x2

    iget-object v1, p0, Lltr;->b:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 191
    :cond_1
    iget-object v0, p0, Lltr;->c:Lltt;

    if-eqz v0, :cond_2

    .line 192
    const/4 v0, 0x3

    iget-object v1, p0, Lltr;->c:Lltt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 194
    :cond_2
    iget-object v0, p0, Lltr;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 195
    const/4 v0, 0x4

    iget-object v1, p0, Lltr;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 197
    :cond_3
    iget-object v0, p0, Lltr;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 198
    const/4 v0, 0x5

    iget-object v1, p0, Lltr;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 200
    :cond_4
    iget-object v0, p0, Lltr;->f:Llts;

    if-eqz v0, :cond_5

    .line 201
    const/4 v0, 0x6

    iget-object v1, p0, Lltr;->f:Llts;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 203
    :cond_5
    iget-object v0, p0, Lltr;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 204
    const/4 v0, 0x7

    iget-object v1, p0, Lltr;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 206
    :cond_6
    iget-object v0, p0, Lltr;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 207
    const/16 v0, 0x8

    iget-object v1, p0, Lltr;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 209
    :cond_7
    iget v0, p0, Lltr;->i:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_8

    .line 210
    const/16 v0, 0x9

    iget v1, p0, Lltr;->i:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 212
    :cond_8
    iget-object v0, p0, Lltr;->j:Lltv;

    if-eqz v0, :cond_9

    .line 213
    const/16 v0, 0xa

    iget-object v1, p0, Lltr;->j:Lltv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 215
    :cond_9
    iget-object v0, p0, Lltr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 217
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lltr;->a(Loxn;)Lltr;

    move-result-object v0

    return-object v0
.end method
