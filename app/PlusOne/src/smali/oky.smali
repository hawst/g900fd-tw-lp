.class public final Loky;
.super Loxq;
.source "PG"


# instance fields
.field private a:[B

.field private b:[I

.field private c:[[B

.field private d:[[B

.field private e:[Ljava/lang/Integer;

.field private f:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 21
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Loky;->b:[I

    .line 24
    sget-object v0, Loxx;->e:[[B

    iput-object v0, p0, Loky;->c:[[B

    .line 27
    sget-object v0, Loxx;->e:[[B

    iput-object v0, p0, Loky;->d:[[B

    .line 30
    sget-object v0, Loxx;->g:[Ljava/lang/Integer;

    iput-object v0, p0, Loky;->e:[Ljava/lang/Integer;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 69
    .line 70
    iget-object v0, p0, Loky;->a:[B

    if-eqz v0, :cond_8

    .line 71
    const/4 v0, 0x1

    iget-object v2, p0, Loky;->a:[B

    .line 72
    invoke-static {v0, v2}, Loxo;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 74
    :goto_0
    iget-object v2, p0, Loky;->b:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Loky;->b:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 76
    iget-object v4, p0, Loky;->b:[I

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget v6, v4, v2

    .line 78
    invoke-static {v6}, Loxo;->i(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 76
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 80
    :cond_0
    add-int/2addr v0, v3

    .line 81
    iget-object v2, p0, Loky;->b:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 83
    :cond_1
    iget-object v2, p0, Loky;->c:[[B

    if-eqz v2, :cond_3

    iget-object v2, p0, Loky;->c:[[B

    array-length v2, v2

    if-lez v2, :cond_3

    .line 85
    iget-object v4, p0, Loky;->c:[[B

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 87
    invoke-static {v6}, Loxo;->c([B)I

    move-result v6

    add-int/2addr v3, v6

    .line 85
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 89
    :cond_2
    add-int/2addr v0, v3

    .line 90
    iget-object v2, p0, Loky;->c:[[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 92
    :cond_3
    iget-object v2, p0, Loky;->d:[[B

    if-eqz v2, :cond_5

    iget-object v2, p0, Loky;->d:[[B

    array-length v2, v2

    if-lez v2, :cond_5

    .line 94
    iget-object v3, p0, Loky;->d:[[B

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 96
    invoke-static {v5}, Loxo;->c([B)I

    move-result v5

    add-int/2addr v2, v5

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 98
    :cond_4
    add-int/2addr v0, v2

    .line 99
    iget-object v1, p0, Loky;->d:[[B

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 101
    :cond_5
    iget-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    if-eqz v1, :cond_6

    iget-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 102
    iget-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 103
    add-int/2addr v0, v1

    .line 104
    iget-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 106
    :cond_6
    iget-object v1, p0, Loky;->f:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 107
    const/4 v1, 0x6

    iget-object v2, p0, Loky;->f:Ljava/lang/Long;

    .line 108
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 110
    :cond_7
    iget-object v1, p0, Loky;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    iput v0, p0, Loky;->ai:I

    .line 112
    return v0

    :cond_8
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Loky;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 120
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 121
    sparse-switch v0, :sswitch_data_0

    .line 125
    iget-object v1, p0, Loky;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 126
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loky;->ah:Ljava/util/List;

    .line 129
    :cond_1
    iget-object v1, p0, Loky;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    :sswitch_0
    return-object p0

    .line 136
    :sswitch_1
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Loky;->a:[B

    goto :goto_0

    .line 140
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 141
    iget-object v0, p0, Loky;->b:[I

    array-length v0, v0

    .line 142
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 143
    iget-object v2, p0, Loky;->b:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 144
    iput-object v1, p0, Loky;->b:[I

    .line 145
    :goto_1
    iget-object v1, p0, Loky;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 146
    iget-object v1, p0, Loky;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 147
    invoke-virtual {p1}, Loxn;->a()I

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 150
    :cond_2
    iget-object v1, p0, Loky;->b:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 154
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 155
    iget-object v0, p0, Loky;->c:[[B

    array-length v0, v0

    .line 156
    add-int/2addr v1, v0

    new-array v1, v1, [[B

    .line 157
    iget-object v2, p0, Loky;->c:[[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 158
    iput-object v1, p0, Loky;->c:[[B

    .line 159
    :goto_2
    iget-object v1, p0, Loky;->c:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 160
    iget-object v1, p0, Loky;->c:[[B

    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v2

    aput-object v2, v1, v0

    .line 161
    invoke-virtual {p1}, Loxn;->a()I

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 164
    :cond_3
    iget-object v1, p0, Loky;->c:[[B

    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 168
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 169
    iget-object v0, p0, Loky;->d:[[B

    array-length v0, v0

    .line 170
    add-int/2addr v1, v0

    new-array v1, v1, [[B

    .line 171
    iget-object v2, p0, Loky;->d:[[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 172
    iput-object v1, p0, Loky;->d:[[B

    .line 173
    :goto_3
    iget-object v1, p0, Loky;->d:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 174
    iget-object v1, p0, Loky;->d:[[B

    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v2

    aput-object v2, v1, v0

    .line 175
    invoke-virtual {p1}, Loxn;->a()I

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 178
    :cond_4
    iget-object v1, p0, Loky;->d:[[B

    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 182
    :sswitch_5
    const/16 v0, 0x2d

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 183
    iget-object v0, p0, Loky;->e:[Ljava/lang/Integer;

    array-length v0, v0

    .line 184
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/Integer;

    .line 185
    iget-object v2, p0, Loky;->e:[Ljava/lang/Integer;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    iput-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    .line 187
    :goto_4
    iget-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    .line 188
    iget-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->i()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 189
    invoke-virtual {p1}, Loxn;->a()I

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 192
    :cond_5
    iget-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    invoke-virtual {p1}, Loxn;->i()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 196
    :sswitch_6
    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Loky;->f:Ljava/lang/Long;

    goto/16 :goto_0

    .line 121
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2d -> :sswitch_5
        0x31 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 37
    iget-object v1, p0, Loky;->a:[B

    if-eqz v1, :cond_0

    .line 38
    const/4 v1, 0x1

    iget-object v2, p0, Loky;->a:[B

    invoke-virtual {p1, v1, v2}, Loxo;->a(I[B)V

    .line 40
    :cond_0
    iget-object v1, p0, Loky;->b:[I

    if-eqz v1, :cond_1

    iget-object v1, p0, Loky;->b:[I

    array-length v1, v1

    if-lez v1, :cond_1

    .line 41
    iget-object v2, p0, Loky;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v4, v2, v1

    .line 42
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->a(II)V

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 45
    :cond_1
    iget-object v1, p0, Loky;->c:[[B

    if-eqz v1, :cond_2

    .line 46
    iget-object v2, p0, Loky;->c:[[B

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 47
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->a(I[B)V

    .line 46
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 50
    :cond_2
    iget-object v1, p0, Loky;->d:[[B

    if-eqz v1, :cond_3

    .line 51
    iget-object v2, p0, Loky;->d:[[B

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 52
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->a(I[B)V

    .line 51
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 55
    :cond_3
    iget-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 56
    iget-object v1, p0, Loky;->e:[Ljava/lang/Integer;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 57
    const/4 v4, 0x5

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {p1, v4, v3}, Loxo;->b(II)V

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 60
    :cond_4
    iget-object v0, p0, Loky;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 61
    const/4 v0, 0x6

    iget-object v1, p0, Loky;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->c(IJ)V

    .line 63
    :cond_5
    iget-object v0, p0, Loky;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 65
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Loky;->a(Loxn;)Loky;

    move-result-object v0

    return-object v0
.end method
