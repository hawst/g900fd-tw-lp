.class public Lfxu;
.super Lfye;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static j:I

.field private static k:Landroid/graphics/Paint;

.field private static l:F

.field private static m:I

.field private static n:I

.field private static o:Z


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Z

.field private f:I

.field private g:I

.field private h:I

.field private i:Lfxv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lfye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    invoke-virtual {p0, p1, p2, p3}, Lfxu;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 0

    .prologue
    .line 263
    return-void
.end method

.method protected a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const v6, 0x7f020416

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 64
    sget-boolean v0, Lfxu;->o:Z

    if-nez v0, :cond_0

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 67
    const v1, 0x7f0d02c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxu;->m:I

    .line 69
    const v1, 0x7f0d02c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxu;->n:I

    .line 72
    const v1, 0x7f0d02b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxu;->j:I

    .line 75
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 76
    sput-object v1, Lfxu;->k:Landroid/graphics/Paint;

    const v2, 0x7f0b030b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    sget-object v1, Lfxu;->k:Landroid/graphics/Paint;

    const v2, 0x7f0d02b9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 81
    sget-object v0, Lfxu;->k:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    sput v0, Lfxu;->l:F

    .line 82
    sput-boolean v4, Lfxu;->o:Z

    .line 85
    :cond_0
    new-instance v0, Lfxv;

    invoke-direct {v0, p1, p2, p3}, Lfxv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxu;->i:Lfxv;

    .line 86
    iget-object v0, p0, Lfxu;->i:Lfxv;

    new-instance v1, Lfyf;

    invoke-direct {v1, v3, v3}, Lfyf;-><init>(II)V

    invoke-virtual {v0, v1}, Lfxv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 87
    iget-object v0, p0, Lfxu;->i:Lfxv;

    invoke-virtual {p0, v0}, Lfxu;->addView(Landroid/view/View;)V

    .line 89
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxu;->a:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lfxu;->a:Landroid/view/View;

    new-instance v1, Lfyf;

    invoke-direct {v1, v3, v3}, Lfyf;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    iget-object v0, p0, Lfxu;->a:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 92
    iget-object v0, p0, Lfxu;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lfxu;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfxu;->b:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lfxu;->b:Landroid/view/View;

    new-instance v1, Lfyf;

    invoke-direct {v1, v3, v3}, Lfyf;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 97
    iget-object v0, p0, Lfxu;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 98
    iget-object v0, p0, Lfxu;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lfxu;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v0, p0, Lfxu;->a:Landroid/view/View;

    invoke-virtual {p0}, Lfxu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 101
    iget-object v0, p0, Lfxu;->b:Landroid/view/View;

    invoke-virtual {p0}, Lfxu;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 103
    iget-object v0, p0, Lfxu;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfxu;->addView(Landroid/view/View;)V

    .line 104
    iget-object v0, p0, Lfxu;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfxu;->addView(Landroid/view/View;)V

    .line 106
    invoke-virtual {p0, v3}, Lfxu;->setWillNotDraw(Z)V

    .line 107
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0}, Lfxu;->b()V

    .line 189
    iput-object p3, p0, Lfxu;->c:Landroid/view/View;

    .line 190
    iput-object p4, p0, Lfxu;->d:Landroid/view/View;

    .line 192
    iget-object v0, p0, Lfxu;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lfxu;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfxu;->addView(Landroid/view/View;)V

    .line 196
    :cond_0
    iget-object v0, p0, Lfxu;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lfxu;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfxu;->addView(Landroid/view/View;)V

    .line 200
    :cond_1
    iget-object v0, p0, Lfxu;->i:Lfxv;

    invoke-virtual {v0, p1, p2}, Lfxv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/util/List;Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/view/View;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-virtual {p0}, Lfxu;->b()V

    .line 205
    iput-object p3, p0, Lfxu;->c:Landroid/view/View;

    .line 206
    iput-object p4, p0, Lfxu;->d:Landroid/view/View;

    .line 207
    const/4 v0, 0x0

    iput v0, p0, Lfxu;->h:I

    .line 209
    iget-object v0, p0, Lfxu;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lfxu;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfxu;->addView(Landroid/view/View;)V

    .line 213
    :cond_0
    iget-object v0, p0, Lfxu;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lfxu;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfxu;->addView(Landroid/view/View;)V

    .line 217
    :cond_1
    iget-object v0, p0, Lfxu;->i:Lfxv;

    invoke-virtual {v0, p1, p2}, Lfxv;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 218
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 184
    iput-boolean p1, p0, Lfxu;->e:Z

    .line 185
    return-void
.end method

.method public a(ZZ)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 179
    iget-object v3, p0, Lfxu;->a:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lfxu;->b:Landroid/view/View;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 181
    return-void

    :cond_0
    move v0, v2

    .line 179
    goto :goto_0

    :cond_1
    move v1, v2

    .line 180
    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 221
    iget-object v0, p0, Lfxu;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lfxu;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfxu;->removeView(Landroid/view/View;)V

    .line 223
    iput-object v1, p0, Lfxu;->c:Landroid/view/View;

    .line 226
    :cond_0
    iget-object v0, p0, Lfxu;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Lfxu;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lfxu;->removeView(Landroid/view/View;)V

    .line 228
    iput-object v1, p0, Lfxu;->d:Landroid/view/View;

    .line 231
    :cond_1
    iget-object v0, p0, Lfxu;->i:Lfxv;

    invoke-virtual {v0}, Lfxv;->b()V

    .line 232
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lfxu;->a:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 253
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfxu;->a(I)V

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    iget-object v0, p0, Lfxu;->b:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 255
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfxu;->a(I)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 236
    iget-boolean v0, p0, Lfxu;->e:Z

    if-eqz v0, :cond_0

    .line 237
    iget v0, p0, Lfxu;->g:I

    int-to-float v3, v0

    sget-object v5, Lfxu;->k:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 240
    :cond_0
    iget v0, p0, Lfxu;->f:I

    int-to-float v0, v0

    sget v2, Lfxu;->l:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 241
    int-to-float v2, v6

    iget v0, p0, Lfxu;->g:I

    int-to-float v3, v0

    int-to-float v4, v6

    sget-object v5, Lfxu;->k:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 243
    iget-object v0, p0, Lfxu;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfxu;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_1

    .line 244
    iget v0, p0, Lfxu;->g:I

    iget v2, p0, Lfxu;->h:I

    sub-int/2addr v0, v2

    .line 245
    int-to-float v3, v0

    int-to-float v5, v0

    int-to-float v6, v6

    sget-object v7, Lfxu;->k:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 247
    :cond_1
    invoke-super {p0, p1}, Lfye;->onDraw(Landroid/graphics/Canvas;)V

    .line 248
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/16 v10, 0x8

    const/high16 v9, -0x80000000

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 115
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 116
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 121
    iget-boolean v0, p0, Lfxu;->e:Z

    if-eqz v0, :cond_4

    .line 122
    const/4 v0, 0x0

    sget v2, Lfxu;->l:F

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 129
    :goto_0
    iget-object v2, p0, Lfxu;->c:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 130
    iget-object v2, p0, Lfxu;->c:Landroid/view/View;

    invoke-static {v2, v4, v9, v1, v1}, Lfxu;->a(Landroid/view/View;IIII)V

    .line 131
    sget v2, Lfxu;->j:I

    mul-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lfxu;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v2, v3

    sget v3, Lfxu;->m:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 132
    iget-object v3, p0, Lfxu;->c:Landroid/view/View;

    invoke-static {v3, v1, v0}, Lfxu;->a(Landroid/view/View;II)V

    .line 133
    iget-object v3, p0, Lfxu;->c:Landroid/view/View;

    invoke-static {v3, v2, v1}, Lfxu;->b(Landroid/view/View;II)V

    .line 136
    sub-int v3, v4, v2

    .line 140
    :goto_1
    iget-object v5, p0, Lfxu;->d:Landroid/view/View;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lfxu;->d:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-eq v5, v10, :cond_2

    .line 141
    iget-object v5, p0, Lfxu;->d:Landroid/view/View;

    invoke-static {v5, v3, v9, v1, v1}, Lfxu;->a(Landroid/view/View;IIII)V

    .line 143
    sget v5, Lfxu;->j:I

    mul-int/lit8 v5, v5, 0x2

    iget-object v7, p0, Lfxu;->d:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v5, v7

    sget v7, Lfxu;->m:I

    invoke-static {v5, v7}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lfxu;->h:I

    .line 144
    iget-object v5, p0, Lfxu;->d:Landroid/view/View;

    iget v7, p0, Lfxu;->h:I

    sub-int v7, v4, v7

    invoke-static {v5, v7, v0}, Lfxu;->a(Landroid/view/View;II)V

    .line 145
    iget-object v5, p0, Lfxu;->d:Landroid/view/View;

    iget v7, p0, Lfxu;->h:I

    invoke-static {v5, v7, v1}, Lfxu;->b(Landroid/view/View;II)V

    .line 146
    iget v5, p0, Lfxu;->h:I

    sget-object v7, Lfxu;->k:Landroid/graphics/Paint;

    invoke-virtual {v7}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v7

    float-to-int v7, v7

    add-int/2addr v5, v7

    .line 147
    sub-int/2addr v3, v5

    move v11, v5

    move v5, v3

    move v3, v11

    .line 150
    :goto_2
    iget-object v7, p0, Lfxu;->i:Lfxv;

    invoke-static {v7, v5, v9, v6, v1}, Lfxu;->a(Landroid/view/View;IIII)V

    .line 151
    iget-object v5, p0, Lfxu;->i:Lfxv;

    invoke-static {v5, v2, v0}, Lfxu;->a(Landroid/view/View;II)V

    .line 153
    const/4 v2, 0x3

    new-array v2, v2, [Landroid/view/View;

    iget-object v5, p0, Lfxu;->d:Landroid/view/View;

    aput-object v5, v2, v1

    const/4 v5, 0x1

    iget-object v6, p0, Lfxu;->c:Landroid/view/View;

    aput-object v6, v2, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lfxu;->i:Lfxv;

    aput-object v6, v2, v5

    .line 154
    sget v5, Lfxu;->n:I

    invoke-static {v2}, Lfxu;->a([Landroid/view/View;)I

    move-result v6

    sget v7, Lfxu;->j:I

    shl-int/lit8 v7, v7, 0x1

    add-int/2addr v6, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 155
    invoke-static {v5, v2}, Lfxu;->a(I[Landroid/view/View;)V

    .line 157
    add-int v2, v0, v5

    .line 159
    iget-object v5, p0, Lfxu;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-eq v5, v10, :cond_0

    .line 160
    iget-object v5, p0, Lfxu;->a:Landroid/view/View;

    invoke-static {v5, v1, v0}, Lfxu;->a(Landroid/view/View;II)V

    .line 161
    iget-object v1, p0, Lfxu;->a:Landroid/view/View;

    sub-int v5, v4, v3

    invoke-static {v1, v5, v8, v2, v8}, Lfxu;->a(Landroid/view/View;IIII)V

    .line 165
    :cond_0
    iget-object v1, p0, Lfxu;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v10, :cond_1

    .line 166
    iget-object v1, p0, Lfxu;->b:Landroid/view/View;

    sub-int v5, v4, v3

    invoke-static {v1, v5, v0}, Lfxu;->a(Landroid/view/View;II)V

    .line 167
    iget-object v0, p0, Lfxu;->b:Landroid/view/View;

    invoke-static {v0, v3, v8, v2, v8}, Lfxu;->a(Landroid/view/View;IIII)V

    .line 171
    :cond_1
    int-to-float v0, v2

    sget v1, Lfxu;->l:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 173
    iput v0, p0, Lfxu;->f:I

    .line 174
    iput v4, p0, Lfxu;->g:I

    .line 175
    iget v0, p0, Lfxu;->f:I

    invoke-virtual {p0, v4, v0}, Lfxu;->setMeasuredDimension(II)V

    .line 176
    return-void

    :cond_2
    move v5, v3

    move v3, v1

    goto :goto_2

    :cond_3
    move v2, v1

    move v3, v4

    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto/16 :goto_0
.end method
