.class public final Llxf;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Llxf;


# instance fields
.field private b:Llxp;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4062
    const/4 v0, 0x0

    new-array v0, v0, [Llxf;

    sput-object v0, Llxf;->a:[Llxf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4063
    invoke-direct {p0}, Loxq;-><init>()V

    .line 4066
    const/4 v0, 0x0

    iput-object v0, p0, Llxf;->b:Llxp;

    .line 4063
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4105
    const/4 v0, 0x0

    .line 4106
    iget-object v1, p0, Llxf;->b:Llxp;

    if-eqz v1, :cond_0

    .line 4107
    const/4 v0, 0x1

    iget-object v1, p0, Llxf;->b:Llxp;

    .line 4108
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4110
    :cond_0
    iget-object v1, p0, Llxf;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 4111
    const/4 v1, 0x2

    iget-object v2, p0, Llxf;->c:Ljava/lang/String;

    .line 4112
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4114
    :cond_1
    iget-object v1, p0, Llxf;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 4115
    const/4 v1, 0x3

    iget-object v2, p0, Llxf;->d:Ljava/lang/String;

    .line 4116
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4118
    :cond_2
    iget-object v1, p0, Llxf;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4119
    const/4 v1, 0x4

    iget-object v2, p0, Llxf;->f:Ljava/lang/String;

    .line 4120
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4122
    :cond_3
    iget-object v1, p0, Llxf;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 4123
    const/4 v1, 0x5

    iget-object v2, p0, Llxf;->e:Ljava/lang/String;

    .line 4124
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4126
    :cond_4
    iget-object v1, p0, Llxf;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 4127
    const/4 v1, 0x6

    iget-object v2, p0, Llxf;->g:Ljava/lang/String;

    .line 4128
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4130
    :cond_5
    iget-object v1, p0, Llxf;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4131
    iput v0, p0, Llxf;->ai:I

    .line 4132
    return v0
.end method

.method public a(Loxn;)Llxf;
    .locals 2

    .prologue
    .line 4140
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4141
    sparse-switch v0, :sswitch_data_0

    .line 4145
    iget-object v1, p0, Llxf;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4146
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llxf;->ah:Ljava/util/List;

    .line 4149
    :cond_1
    iget-object v1, p0, Llxf;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4151
    :sswitch_0
    return-object p0

    .line 4156
    :sswitch_1
    iget-object v0, p0, Llxf;->b:Llxp;

    if-nez v0, :cond_2

    .line 4157
    new-instance v0, Llxp;

    invoke-direct {v0}, Llxp;-><init>()V

    iput-object v0, p0, Llxf;->b:Llxp;

    .line 4159
    :cond_2
    iget-object v0, p0, Llxf;->b:Llxp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 4163
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxf;->c:Ljava/lang/String;

    goto :goto_0

    .line 4167
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxf;->d:Ljava/lang/String;

    goto :goto_0

    .line 4171
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxf;->f:Ljava/lang/String;

    goto :goto_0

    .line 4175
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxf;->e:Ljava/lang/String;

    goto :goto_0

    .line 4179
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Llxf;->g:Ljava/lang/String;

    goto :goto_0

    .line 4141
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4081
    iget-object v0, p0, Llxf;->b:Llxp;

    if-eqz v0, :cond_0

    .line 4082
    const/4 v0, 0x1

    iget-object v1, p0, Llxf;->b:Llxp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 4084
    :cond_0
    iget-object v0, p0, Llxf;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 4085
    const/4 v0, 0x2

    iget-object v1, p0, Llxf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4087
    :cond_1
    iget-object v0, p0, Llxf;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 4088
    const/4 v0, 0x3

    iget-object v1, p0, Llxf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4090
    :cond_2
    iget-object v0, p0, Llxf;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4091
    const/4 v0, 0x4

    iget-object v1, p0, Llxf;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4093
    :cond_3
    iget-object v0, p0, Llxf;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 4094
    const/4 v0, 0x5

    iget-object v1, p0, Llxf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4096
    :cond_4
    iget-object v0, p0, Llxf;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 4097
    const/4 v0, 0x6

    iget-object v1, p0, Llxf;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4099
    :cond_5
    iget-object v0, p0, Llxf;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4101
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4059
    invoke-virtual {p0, p1}, Llxf;->a(Loxn;)Llxf;

    move-result-object v0

    return-object v0
.end method
