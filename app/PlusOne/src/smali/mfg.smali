.class public final Lmfg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnbp;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27619
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27622
    iput-object v0, p0, Lmfg;->apiHeader:Llyq;

    .line 27625
    iput-object v0, p0, Lmfg;->a:Lnbp;

    .line 27619
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27642
    const/4 v0, 0x0

    .line 27643
    iget-object v1, p0, Lmfg;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 27644
    const/4 v0, 0x1

    iget-object v1, p0, Lmfg;->apiHeader:Llyq;

    .line 27645
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27647
    :cond_0
    iget-object v1, p0, Lmfg;->a:Lnbp;

    if-eqz v1, :cond_1

    .line 27648
    const/4 v1, 0x2

    iget-object v2, p0, Lmfg;->a:Lnbp;

    .line 27649
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27651
    :cond_1
    iget-object v1, p0, Lmfg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27652
    iput v0, p0, Lmfg;->ai:I

    .line 27653
    return v0
.end method

.method public a(Loxn;)Lmfg;
    .locals 2

    .prologue
    .line 27661
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27662
    sparse-switch v0, :sswitch_data_0

    .line 27666
    iget-object v1, p0, Lmfg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27667
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfg;->ah:Ljava/util/List;

    .line 27670
    :cond_1
    iget-object v1, p0, Lmfg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27672
    :sswitch_0
    return-object p0

    .line 27677
    :sswitch_1
    iget-object v0, p0, Lmfg;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 27678
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmfg;->apiHeader:Llyq;

    .line 27680
    :cond_2
    iget-object v0, p0, Lmfg;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27684
    :sswitch_2
    iget-object v0, p0, Lmfg;->a:Lnbp;

    if-nez v0, :cond_3

    .line 27685
    new-instance v0, Lnbp;

    invoke-direct {v0}, Lnbp;-><init>()V

    iput-object v0, p0, Lmfg;->a:Lnbp;

    .line 27687
    :cond_3
    iget-object v0, p0, Lmfg;->a:Lnbp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27662
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27630
    iget-object v0, p0, Lmfg;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 27631
    const/4 v0, 0x1

    iget-object v1, p0, Lmfg;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27633
    :cond_0
    iget-object v0, p0, Lmfg;->a:Lnbp;

    if-eqz v0, :cond_1

    .line 27634
    const/4 v0, 0x2

    iget-object v1, p0, Lmfg;->a:Lnbp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27636
    :cond_1
    iget-object v0, p0, Lmfg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27638
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 27615
    invoke-virtual {p0, p1}, Lmfg;->a(Loxn;)Lmfg;

    move-result-object v0

    return-object v0
.end method
