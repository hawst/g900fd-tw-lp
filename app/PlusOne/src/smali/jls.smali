.class public final Ljls;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljlx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljlx",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Ljly;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljly",
            "<TT;>;"
        }
    .end annotation
.end field

.field final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field c:Z

.field private final d:Lhov;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lhov;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lhov;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljly;

    invoke-direct {v0}, Ljly;-><init>()V

    iput-object v0, p0, Ljls;->a:Ljly;

    .line 21
    iput-object p1, p0, Ljls;->b:Ljava/lang/Object;

    .line 22
    iput-object p2, p0, Ljls;->d:Lhov;

    .line 23
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 57
    invoke-static {}, Llsx;->b()V

    .line 59
    iget-boolean v0, p0, Ljls;->c:Z

    if-nez v0, :cond_0

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljls;->c:Z

    .line 64
    iget-object v0, p0, Ljls;->d:Lhov;

    new-instance v1, Ljlu;

    invoke-direct {v1, p0}, Ljlu;-><init>(Ljls;)V

    invoke-virtual {v0, v1}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    .line 75
    :cond_0
    return-void
.end method

.method public a(Ljma;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljma",
            "<-TT;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-static {}, Llsx;->b()V

    .line 45
    iget-object v0, p0, Ljls;->a:Ljly;

    invoke-virtual {v0, p1}, Ljly;->b(Ljma;)V

    .line 46
    return-void
.end method

.method public a(Ljma;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljma",
            "<-TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-static {}, Llsx;->b()V

    .line 29
    iget-object v0, p0, Ljls;->a:Ljly;

    invoke-virtual {v0, p1}, Ljly;->a(Ljma;)V

    .line 31
    if-eqz p2, :cond_0

    .line 32
    iget-object v0, p0, Ljls;->d:Lhov;

    new-instance v1, Ljlt;

    invoke-direct {v1, p0, p1}, Ljlt;-><init>(Ljls;Ljma;)V

    invoke-virtual {v0, v1}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    .line 39
    :cond_0
    return-void
.end method
