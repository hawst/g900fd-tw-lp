.class public final Lofa;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lofa;


# instance fields
.field private A:Lofv;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Logq;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Long;

.field public l:Loae;

.field public m:Ljava/lang/String;

.field public n:Lpeo;

.field private o:Loew;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Loep;

.field private s:Ljava/lang/Boolean;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/Boolean;

.field private v:Ljava/lang/Boolean;

.field private w:Ljava/lang/String;

.field private x:Logc;

.field private y:I

.field private z:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 636
    const/4 v0, 0x0

    new-array v0, v0, [Lofa;

    sput-object v0, Lofa;->a:[Lofa;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 637
    invoke-direct {p0}, Loxq;-><init>()V

    .line 644
    iput-object v1, p0, Lofa;->o:Loew;

    .line 649
    iput-object v1, p0, Lofa;->d:Logq;

    .line 658
    iput-object v1, p0, Lofa;->r:Loep;

    .line 677
    iput-object v1, p0, Lofa;->l:Loae;

    .line 686
    iput-object v1, p0, Lofa;->x:Logc;

    .line 689
    const/high16 v0, -0x80000000

    iput v0, p0, Lofa;->y:I

    .line 694
    iput-object v1, p0, Lofa;->A:Lofv;

    .line 697
    iput-object v1, p0, Lofa;->n:Lpeo;

    .line 637
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 768
    const/4 v0, 0x2

    iget-object v1, p0, Lofa;->b:Ljava/lang/String;

    .line 770
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 771
    const/4 v1, 0x3

    iget-object v2, p0, Lofa;->c:Ljava/lang/String;

    .line 772
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 773
    const/4 v1, 0x4

    iget-object v2, p0, Lofa;->e:Ljava/lang/Long;

    .line 774
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 775
    const/4 v1, 0x5

    iget-object v2, p0, Lofa;->f:Ljava/lang/String;

    .line 776
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 777
    iget-object v1, p0, Lofa;->q:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 778
    const/4 v1, 0x6

    iget-object v2, p0, Lofa;->q:Ljava/lang/String;

    .line 779
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 781
    :cond_0
    const/4 v1, 0x7

    iget-object v2, p0, Lofa;->g:Ljava/lang/String;

    .line 782
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 783
    const/16 v1, 0x8

    iget-object v2, p0, Lofa;->h:Ljava/lang/String;

    .line 784
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 785
    const/16 v1, 0x9

    iget-object v2, p0, Lofa;->i:Ljava/lang/Boolean;

    .line 786
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 787
    const/16 v1, 0xa

    iget-object v2, p0, Lofa;->s:Ljava/lang/Boolean;

    .line 788
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 789
    iget-object v1, p0, Lofa;->t:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 790
    const/16 v1, 0xb

    iget-object v2, p0, Lofa;->t:Ljava/lang/String;

    .line 791
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 793
    :cond_1
    const/16 v1, 0xc

    iget-object v2, p0, Lofa;->u:Ljava/lang/Boolean;

    .line 794
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 795
    iget-object v1, p0, Lofa;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 796
    const/16 v1, 0xd

    iget-object v2, p0, Lofa;->j:Ljava/lang/Boolean;

    .line 797
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 799
    :cond_2
    iget-object v1, p0, Lofa;->k:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 800
    const/16 v1, 0xf

    iget-object v2, p0, Lofa;->k:Ljava/lang/Long;

    .line 801
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 803
    :cond_3
    iget-object v1, p0, Lofa;->l:Loae;

    if-eqz v1, :cond_4

    .line 804
    const/16 v1, 0x10

    iget-object v2, p0, Lofa;->l:Loae;

    .line 805
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 807
    :cond_4
    iget-object v1, p0, Lofa;->m:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 808
    const/16 v1, 0x11

    iget-object v2, p0, Lofa;->m:Ljava/lang/String;

    .line 809
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 811
    :cond_5
    iget-object v1, p0, Lofa;->v:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 812
    const/16 v1, 0x12

    iget-object v2, p0, Lofa;->v:Ljava/lang/Boolean;

    .line 813
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 815
    :cond_6
    iget-object v1, p0, Lofa;->o:Loew;

    if-eqz v1, :cond_7

    .line 816
    const/16 v1, 0x13

    iget-object v2, p0, Lofa;->o:Loew;

    .line 817
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 819
    :cond_7
    iget-object v1, p0, Lofa;->r:Loep;

    if-eqz v1, :cond_8

    .line 820
    const/16 v1, 0x14

    iget-object v2, p0, Lofa;->r:Loep;

    .line 821
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 823
    :cond_8
    iget-object v1, p0, Lofa;->w:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 824
    const/16 v1, 0x15

    iget-object v2, p0, Lofa;->w:Ljava/lang/String;

    .line 825
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 827
    :cond_9
    iget-object v1, p0, Lofa;->x:Logc;

    if-eqz v1, :cond_a

    .line 828
    const/16 v1, 0x16

    iget-object v2, p0, Lofa;->x:Logc;

    .line 829
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 831
    :cond_a
    iget v1, p0, Lofa;->y:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_b

    .line 832
    const/16 v1, 0x17

    iget v2, p0, Lofa;->y:I

    .line 833
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 835
    :cond_b
    iget-object v1, p0, Lofa;->z:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    .line 836
    const/16 v1, 0x18

    iget-object v2, p0, Lofa;->z:Ljava/lang/Boolean;

    .line 837
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 839
    :cond_c
    iget-object v1, p0, Lofa;->p:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 840
    const/16 v1, 0x19

    iget-object v2, p0, Lofa;->p:Ljava/lang/String;

    .line 841
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 843
    :cond_d
    iget-object v1, p0, Lofa;->A:Lofv;

    if-eqz v1, :cond_e

    .line 844
    const/16 v1, 0x1a

    iget-object v2, p0, Lofa;->A:Lofv;

    .line 845
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 847
    :cond_e
    iget-object v1, p0, Lofa;->d:Logq;

    if-eqz v1, :cond_f

    .line 848
    const/16 v1, 0x1b

    iget-object v2, p0, Lofa;->d:Logq;

    .line 849
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 851
    :cond_f
    iget-object v1, p0, Lofa;->n:Lpeo;

    if-eqz v1, :cond_10

    .line 852
    const/16 v1, 0x1c

    iget-object v2, p0, Lofa;->n:Lpeo;

    .line 853
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 855
    :cond_10
    iget-object v1, p0, Lofa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 856
    iput v0, p0, Lofa;->ai:I

    .line 857
    return v0
.end method

.method public a(Loxn;)Lofa;
    .locals 2

    .prologue
    .line 865
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 866
    sparse-switch v0, :sswitch_data_0

    .line 870
    iget-object v1, p0, Lofa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 871
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lofa;->ah:Ljava/util/List;

    .line 874
    :cond_1
    iget-object v1, p0, Lofa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 876
    :sswitch_0
    return-object p0

    .line 881
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->b:Ljava/lang/String;

    goto :goto_0

    .line 885
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->c:Ljava/lang/String;

    goto :goto_0

    .line 889
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lofa;->e:Ljava/lang/Long;

    goto :goto_0

    .line 893
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->f:Ljava/lang/String;

    goto :goto_0

    .line 897
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->q:Ljava/lang/String;

    goto :goto_0

    .line 901
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->g:Ljava/lang/String;

    goto :goto_0

    .line 905
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->h:Ljava/lang/String;

    goto :goto_0

    .line 909
    :sswitch_8
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofa;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 913
    :sswitch_9
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofa;->s:Ljava/lang/Boolean;

    goto :goto_0

    .line 917
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->t:Ljava/lang/String;

    goto :goto_0

    .line 921
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofa;->u:Ljava/lang/Boolean;

    goto :goto_0

    .line 925
    :sswitch_c
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofa;->j:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 929
    :sswitch_d
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lofa;->k:Ljava/lang/Long;

    goto/16 :goto_0

    .line 933
    :sswitch_e
    iget-object v0, p0, Lofa;->l:Loae;

    if-nez v0, :cond_2

    .line 934
    new-instance v0, Loae;

    invoke-direct {v0}, Loae;-><init>()V

    iput-object v0, p0, Lofa;->l:Loae;

    .line 936
    :cond_2
    iget-object v0, p0, Lofa;->l:Loae;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 940
    :sswitch_f
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->m:Ljava/lang/String;

    goto/16 :goto_0

    .line 944
    :sswitch_10
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofa;->v:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 948
    :sswitch_11
    iget-object v0, p0, Lofa;->o:Loew;

    if-nez v0, :cond_3

    .line 949
    new-instance v0, Loew;

    invoke-direct {v0}, Loew;-><init>()V

    iput-object v0, p0, Lofa;->o:Loew;

    .line 951
    :cond_3
    iget-object v0, p0, Lofa;->o:Loew;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 955
    :sswitch_12
    iget-object v0, p0, Lofa;->r:Loep;

    if-nez v0, :cond_4

    .line 956
    new-instance v0, Loep;

    invoke-direct {v0}, Loep;-><init>()V

    iput-object v0, p0, Lofa;->r:Loep;

    .line 958
    :cond_4
    iget-object v0, p0, Lofa;->r:Loep;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 962
    :sswitch_13
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->w:Ljava/lang/String;

    goto/16 :goto_0

    .line 966
    :sswitch_14
    iget-object v0, p0, Lofa;->x:Logc;

    if-nez v0, :cond_5

    .line 967
    new-instance v0, Logc;

    invoke-direct {v0}, Logc;-><init>()V

    iput-object v0, p0, Lofa;->x:Logc;

    .line 969
    :cond_5
    iget-object v0, p0, Lofa;->x:Logc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 973
    :sswitch_15
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 974
    if-eqz v0, :cond_6

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    if-eq v0, v1, :cond_6

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 978
    :cond_6
    iput v0, p0, Lofa;->y:I

    goto/16 :goto_0

    .line 980
    :cond_7
    const/4 v0, 0x0

    iput v0, p0, Lofa;->y:I

    goto/16 :goto_0

    .line 985
    :sswitch_16
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lofa;->z:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 989
    :sswitch_17
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lofa;->p:Ljava/lang/String;

    goto/16 :goto_0

    .line 993
    :sswitch_18
    iget-object v0, p0, Lofa;->A:Lofv;

    if-nez v0, :cond_8

    .line 994
    new-instance v0, Lofv;

    invoke-direct {v0}, Lofv;-><init>()V

    iput-object v0, p0, Lofa;->A:Lofv;

    .line 996
    :cond_8
    iget-object v0, p0, Lofa;->A:Lofv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1000
    :sswitch_19
    iget-object v0, p0, Lofa;->d:Logq;

    if-nez v0, :cond_9

    .line 1001
    new-instance v0, Logq;

    invoke-direct {v0}, Logq;-><init>()V

    iput-object v0, p0, Lofa;->d:Logq;

    .line 1003
    :cond_9
    iget-object v0, p0, Lofa;->d:Logq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1007
    :sswitch_1a
    iget-object v0, p0, Lofa;->n:Lpeo;

    if-nez v0, :cond_a

    .line 1008
    new-instance v0, Lpeo;

    invoke-direct {v0}, Lpeo;-><init>()V

    iput-object v0, p0, Lofa;->n:Lpeo;

    .line 1010
    :cond_a
    iget-object v0, p0, Lofa;->n:Lpeo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 866
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x5a -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x78 -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
        0x90 -> :sswitch_10
        0x9a -> :sswitch_11
        0xa2 -> :sswitch_12
        0xaa -> :sswitch_13
        0xb2 -> :sswitch_14
        0xb8 -> :sswitch_15
        0xc0 -> :sswitch_16
        0xca -> :sswitch_17
        0xd2 -> :sswitch_18
        0xda -> :sswitch_19
        0xe2 -> :sswitch_1a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 702
    const/4 v0, 0x2

    iget-object v1, p0, Lofa;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 703
    const/4 v0, 0x3

    iget-object v1, p0, Lofa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 704
    const/4 v0, 0x4

    iget-object v1, p0, Lofa;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 705
    const/4 v0, 0x5

    iget-object v1, p0, Lofa;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 706
    iget-object v0, p0, Lofa;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 707
    const/4 v0, 0x6

    iget-object v1, p0, Lofa;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 709
    :cond_0
    const/4 v0, 0x7

    iget-object v1, p0, Lofa;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 710
    const/16 v0, 0x8

    iget-object v1, p0, Lofa;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 711
    const/16 v0, 0x9

    iget-object v1, p0, Lofa;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 712
    const/16 v0, 0xa

    iget-object v1, p0, Lofa;->s:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 713
    iget-object v0, p0, Lofa;->t:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 714
    const/16 v0, 0xb

    iget-object v1, p0, Lofa;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 716
    :cond_1
    const/16 v0, 0xc

    iget-object v1, p0, Lofa;->u:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 717
    iget-object v0, p0, Lofa;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 718
    const/16 v0, 0xd

    iget-object v1, p0, Lofa;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 720
    :cond_2
    iget-object v0, p0, Lofa;->k:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 721
    const/16 v0, 0xf

    iget-object v1, p0, Lofa;->k:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 723
    :cond_3
    iget-object v0, p0, Lofa;->l:Loae;

    if-eqz v0, :cond_4

    .line 724
    const/16 v0, 0x10

    iget-object v1, p0, Lofa;->l:Loae;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 726
    :cond_4
    iget-object v0, p0, Lofa;->m:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 727
    const/16 v0, 0x11

    iget-object v1, p0, Lofa;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 729
    :cond_5
    iget-object v0, p0, Lofa;->v:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 730
    const/16 v0, 0x12

    iget-object v1, p0, Lofa;->v:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 732
    :cond_6
    iget-object v0, p0, Lofa;->o:Loew;

    if-eqz v0, :cond_7

    .line 733
    const/16 v0, 0x13

    iget-object v1, p0, Lofa;->o:Loew;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 735
    :cond_7
    iget-object v0, p0, Lofa;->r:Loep;

    if-eqz v0, :cond_8

    .line 736
    const/16 v0, 0x14

    iget-object v1, p0, Lofa;->r:Loep;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 738
    :cond_8
    iget-object v0, p0, Lofa;->w:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 739
    const/16 v0, 0x15

    iget-object v1, p0, Lofa;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 741
    :cond_9
    iget-object v0, p0, Lofa;->x:Logc;

    if-eqz v0, :cond_a

    .line 742
    const/16 v0, 0x16

    iget-object v1, p0, Lofa;->x:Logc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 744
    :cond_a
    iget v0, p0, Lofa;->y:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_b

    .line 745
    const/16 v0, 0x17

    iget v1, p0, Lofa;->y:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 747
    :cond_b
    iget-object v0, p0, Lofa;->z:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    .line 748
    const/16 v0, 0x18

    iget-object v1, p0, Lofa;->z:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 750
    :cond_c
    iget-object v0, p0, Lofa;->p:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 751
    const/16 v0, 0x19

    iget-object v1, p0, Lofa;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 753
    :cond_d
    iget-object v0, p0, Lofa;->A:Lofv;

    if-eqz v0, :cond_e

    .line 754
    const/16 v0, 0x1a

    iget-object v1, p0, Lofa;->A:Lofv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 756
    :cond_e
    iget-object v0, p0, Lofa;->d:Logq;

    if-eqz v0, :cond_f

    .line 757
    const/16 v0, 0x1b

    iget-object v1, p0, Lofa;->d:Logq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 759
    :cond_f
    iget-object v0, p0, Lofa;->n:Lpeo;

    if-eqz v0, :cond_10

    .line 760
    const/16 v0, 0x1c

    iget-object v1, p0, Lofa;->n:Lpeo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 762
    :cond_10
    iget-object v0, p0, Lofa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 764
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 633
    invoke-virtual {p0, p1}, Lofa;->a(Loxn;)Lofa;

    move-result-object v0

    return-object v0
.end method
