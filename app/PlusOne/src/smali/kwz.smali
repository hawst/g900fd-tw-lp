.class public final Lkwz;
.super Lkxh;
.source "PG"

# interfaces
.implements Lkut;


# static fields
.field private static c:I


# instance fields
.field private d:Lkvq;

.field private e:Lkxa;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lae;Lbb;II)V
    .locals 2

    .prologue
    .line 61
    invoke-direct/range {p0 .. p5}, Lkxh;-><init>(Landroid/content/Context;Lae;Lbb;II)V

    .line 63
    sget v0, Lkwz;->c:I

    if-nez v0, :cond_0

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lkwz;->c:I

    .line 67
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lae;Lbb;ILkvq;)V
    .locals 6

    .prologue
    .line 48
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lkwz;-><init>(Landroid/content/Context;Lae;Lbb;II)V

    .line 50
    iput-object p5, p0, Lkwz;->d:Lkvq;

    .line 51
    return-void
.end method

.method private static a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 135
    new-instance v0, Llka;

    const/4 v1, 0x2

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2, v3, v3}, Llka;-><init>(IIII)V

    .line 139
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 140
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 89
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 90
    const v1, 0x7f0401ff

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/String;)Lkxk;
    .locals 7

    .prologue
    .line 83
    new-instance v0, Lkxf;

    iget-object v1, p0, Lkwz;->k:Landroid/content/Context;

    iget v2, p0, Lkwz;->a:I

    iget-object v3, p0, Lkwz;->b:Ljava/lang/String;

    .line 84
    invoke-virtual {p0}, Lkwz;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lkwz;->c()I

    move-result v6

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lkxf;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V

    return-object v0
.end method

.method protected a(Landroid/view/View;I)V
    .locals 5

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 113
    .line 116
    packed-switch p2, :pswitch_data_0

    move v1, v0

    move v2, v0

    .line 127
    :goto_0
    const v3, 0x7f100286

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 128
    const v2, 0x7f100485

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 129
    const v1, 0x7f100486

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 131
    invoke-static {p1}, Lkwz;->a(Landroid/view/View;)V

    .line 132
    return-void

    :pswitch_0
    move v2, v1

    move v1, v0

    .line 119
    goto :goto_0

    :pswitch_1
    move v2, v0

    .line 122
    goto :goto_0

    :pswitch_2
    move v2, v0

    move v4, v0

    move v0, v1

    move v1, v4

    .line 124
    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 3

    .prologue
    .line 102
    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/social/squares/list/SquareListItemView;

    .line 103
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p2, p0, v1, v2}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(Landroid/database/Cursor;Lkut;ZZ)V

    .line 106
    iget-object v1, p0, Lkwz;->d:Lkvq;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/squares/list/SquareListItemView;->a(Lkvq;)V

    .line 108
    invoke-static {p1}, Lkwz;->a(Landroid/view/View;)V

    .line 109
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lkwz;->e:Lkxa;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lkwz;->e:Lkxa;

    invoke-interface {v0, p1}, Lkxa;->c(Ljava/lang/String;)V

    .line 155
    :cond_0
    return-void
.end method

.method public a(Lkxa;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lkwz;->e:Lkxa;

    .line 74
    return-void
.end method

.method protected b(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 96
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 97
    const v1, 0x7f0401f7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lkup;->b:[Ljava/lang/String;

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 144
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 148
    return-void
.end method
