.class public final Lerf;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Llgs;


# instance fields
.field private N:I

.field private O:Ljava/lang/String;

.field private P:I

.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Ljava/lang/String;

.field private T:Ljava/lang/Integer;

.field private U:Lcom/google/android/apps/plus/views/ImageTextButton;

.field private V:Landroid/widget/MultiAutoCompleteTextView;

.field private W:Landroid/view/View;

.field private X:Landroid/widget/ImageView;

.field private Y:Landroid/widget/ImageView;

.field private Z:Landroid/widget/ImageView;

.field private aa:Landroid/widget/ImageView;

.field private ab:Landroid/widget/ImageView;

.field private ac:Lhee;

.field private final ad:Lfhh;

.field private final ae:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Llol;-><init>()V

    .line 64
    new-instance v0, Lerg;

    invoke-direct {v0, p0}, Lerg;-><init>(Lerf;)V

    iput-object v0, p0, Lerf;->ad:Lfhh;

    .line 72
    new-instance v0, Lerh;

    invoke-direct {v0, p0}, Lerh;-><init>(Lerf;)V

    iput-object v0, p0, Lerf;->ae:Landroid/text/TextWatcher;

    return-void
.end method

.method private Z()V
    .locals 3

    .prologue
    const v2, 0x7f020360

    const v1, 0x7f020361

    .line 366
    iget v0, p0, Lerf;->P:I

    packed-switch v0, :pswitch_data_0

    .line 401
    :goto_0
    return-void

    .line 368
    :pswitch_0
    iget-object v0, p0, Lerf;->X:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 369
    iget-object v0, p0, Lerf;->Y:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 370
    iget-object v0, p0, Lerf;->Z:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 371
    iget-object v0, p0, Lerf;->aa:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 372
    iget-object v0, p0, Lerf;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 375
    :pswitch_1
    iget-object v0, p0, Lerf;->X:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 376
    iget-object v0, p0, Lerf;->Y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 377
    iget-object v0, p0, Lerf;->Z:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 378
    iget-object v0, p0, Lerf;->aa:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 379
    iget-object v0, p0, Lerf;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 382
    :pswitch_2
    iget-object v0, p0, Lerf;->X:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 383
    iget-object v0, p0, Lerf;->Y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 384
    iget-object v0, p0, Lerf;->Z:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 385
    iget-object v0, p0, Lerf;->aa:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 386
    iget-object v0, p0, Lerf;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 389
    :pswitch_3
    iget-object v0, p0, Lerf;->X:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 390
    iget-object v0, p0, Lerf;->Y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 391
    iget-object v0, p0, Lerf;->Z:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 392
    iget-object v0, p0, Lerf;->aa:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 393
    iget-object v0, p0, Lerf;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 396
    :pswitch_4
    iget-object v0, p0, Lerf;->X:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 397
    iget-object v0, p0, Lerf;->Y:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 398
    iget-object v0, p0, Lerf;->Z:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 399
    iget-object v0, p0, Lerf;->aa:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 400
    iget-object v0, p0, Lerf;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 366
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private aa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lerf;->V:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected U()V
    .locals 4

    .prologue
    .line 276
    iget-object v0, p0, Lerf;->W:Landroid/view/View;

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 278
    const v0, 0x7f0a024d

    .line 279
    invoke-virtual {p0, v0}, Lerf;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a0963

    .line 280
    invoke-virtual {p0, v1}, Lerf;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a07fa

    .line 281
    invoke-virtual {p0, v2}, Lerf;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a07fd

    invoke-virtual {p0, v3}, Lerf;->e_(I)Ljava/lang/String;

    move-result-object v3

    .line 278
    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 282
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    .line 283
    invoke-virtual {p0}, Lerf;->p()Lae;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 284
    return-void
.end method

.method protected V()V
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lerf;->W:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lerf;->W:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 356
    :cond_0
    invoke-virtual {p0}, Lerf;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llsn;->b(Landroid/view/View;)V

    .line 357
    return-void
.end method

.method protected W()V
    .locals 2

    .prologue
    .line 360
    invoke-virtual {p0}, Lerf;->X()V

    .line 361
    invoke-direct {p0}, Lerf;->Z()V

    .line 362
    iget-object v0, p0, Lerf;->V:Landroid/widget/MultiAutoCompleteTextView;

    iget-object v1, p0, Lerf;->Q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 363
    return-void
.end method

.method protected X()V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lerf;->U:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {p0}, Lerf;->Y()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    .line 414
    return-void
.end method

.method public Y()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 420
    iget v0, p0, Lerf;->P:I

    iget v2, p0, Lerf;->N:I

    if-eq v0, v2, :cond_0

    move v0, v1

    .line 429
    :goto_0
    return v0

    .line 424
    :cond_0
    invoke-direct {p0}, Lerf;->aa()Ljava/lang/String;

    move-result-object v2

    .line 425
    iget-object v0, p0, Lerf;->O:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lerf;->O:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 426
    goto :goto_0

    .line 425
    :cond_1
    const-string v0, ""

    goto :goto_1

    .line 429
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 142
    const v0, 0x7f04023a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 144
    const v0, 0x7f1001da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageTextButton;

    .line 145
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    const v0, 0x7f100652

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageTextButton;

    iput-object v0, p0, Lerf;->U:Lcom/google/android/apps/plus/views/ImageTextButton;

    .line 147
    iget-object v0, p0, Lerf;->U:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    const v0, 0x7f100658

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/MultiAutoCompleteTextView;

    iput-object v0, p0, Lerf;->V:Landroid/widget/MultiAutoCompleteTextView;

    .line 150
    iget-object v0, p0, Lerf;->V:Landroid/widget/MultiAutoCompleteTextView;

    iget-object v2, p0, Lerf;->ae:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/MultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 151
    const v0, 0x7f100319

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lerf;->W:Landroid/view/View;

    .line 153
    const v0, 0x7f100653

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lerf;->X:Landroid/widget/ImageView;

    .line 154
    iget-object v0, p0, Lerf;->X:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 155
    const v0, 0x7f100654

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lerf;->Y:Landroid/widget/ImageView;

    .line 156
    iget-object v0, p0, Lerf;->Y:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    const v0, 0x7f100655

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lerf;->Z:Landroid/widget/ImageView;

    .line 158
    iget-object v0, p0, Lerf;->Z:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    const v0, 0x7f100656

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lerf;->aa:Landroid/widget/ImageView;

    .line 160
    iget-object v0, p0, Lerf;->aa:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    const v0, 0x7f100657

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lerf;->ab:Landroid/widget/ImageView;

    .line 162
    iget-object v0, p0, Lerf;->ab:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    if-nez p3, :cond_0

    .line 165
    iget-object v0, p0, Lerf;->W:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 168
    :cond_0
    invoke-virtual {p0}, Lerf;->W()V

    .line 170
    return-object v1
.end method

.method protected a()V
    .locals 6

    .prologue
    .line 198
    invoke-virtual {p0}, Lerf;->V()V

    .line 199
    iget-object v0, p0, Lerf;->V:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 200
    iget-object v0, p0, Lerf;->at:Llnl;

    iget-object v1, p0, Lerf;->ac:Lhee;

    .line 201
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lerf;->R:Ljava/lang/String;

    iget v3, p0, Lerf;->P:I

    iget-object v5, p0, Lerf;->S:Ljava/lang/String;

    .line 200
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->b(Landroid/content/Context;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lerf;->T:Ljava/lang/Integer;

    .line 203
    const v0, 0x7f0a0965

    invoke-virtual {p0, v0}, Lerf;->c(I)V

    .line 204
    return-void
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method protected a(ILfib;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lerf;->T:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lerf;->T:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    invoke-virtual {p0}, Lerf;->e()V

    .line 223
    invoke-virtual {p0, p2}, Lerf;->a(Lfib;)Z

    move-result v0

    .line 225
    const/4 v1, 0x0

    iput-object v1, p0, Lerf;->T:Ljava/lang/Integer;

    .line 227
    if-nez v0, :cond_0

    .line 228
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lerf;->d(I)V

    goto :goto_0
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 307
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 95
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 97
    if-eqz p1, :cond_0

    .line 98
    const-string v0, "modified_star_rating"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lerf;->P:I

    .line 99
    const-string v0, "modified_review_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerf;->Q:Ljava/lang/String;

    .line 100
    const-string v0, "review_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "review_request_id"

    .line 102
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lerf;->T:Ljava/lang/Integer;

    .line 106
    :cond_0
    invoke-virtual {p0}, Lerf;->k()Landroid/os/Bundle;

    move-result-object v3

    .line 107
    const-string v0, "place_cluster_id"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerf;->R:Ljava/lang/String;

    .line 108
    const-string v0, "activity_id"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lerf;->S:Ljava/lang/String;

    .line 110
    iput v2, p0, Lerf;->N:I

    .line 111
    const/4 v0, 0x0

    iput-object v0, p0, Lerf;->O:Ljava/lang/String;

    .line 113
    const-string v0, "place_star_rating"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    .line 114
    if-eqz p1, :cond_7

    const-string v0, "original_star_rating"

    .line 115
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 116
    :goto_0
    if-nez v4, :cond_1

    if-eqz v0, :cond_2

    .line 117
    :cond_1
    if-eqz v4, :cond_8

    const-string v0, "place_star_rating"

    .line 118
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 119
    :goto_1
    iput v0, p0, Lerf;->N:I

    .line 122
    :cond_2
    const-string v0, "place_review_text"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    .line 123
    if-eqz p1, :cond_9

    const-string v0, "original_review_text"

    .line 124
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    .line 125
    :goto_2
    if-nez v4, :cond_3

    if-eqz v0, :cond_4

    .line 126
    :cond_3
    if-eqz v4, :cond_a

    const-string v0, "place_review_text"

    .line 127
    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    :goto_3
    iput-object v0, p0, Lerf;->O:Ljava/lang/String;

    .line 131
    :cond_4
    iget v0, p0, Lerf;->P:I

    if-nez v0, :cond_5

    .line 132
    iget v0, p0, Lerf;->N:I

    iput v0, p0, Lerf;->P:I

    .line 134
    :cond_5
    iget-object v0, p0, Lerf;->Q:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 135
    iget-object v0, p0, Lerf;->O:Ljava/lang/String;

    iput-object v0, p0, Lerf;->Q:Ljava/lang/String;

    .line 137
    :cond_6
    return-void

    :cond_7
    move v0, v2

    .line 115
    goto :goto_0

    .line 118
    :cond_8
    const-string v0, "original_star_rating"

    .line 119
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    :cond_9
    move v0, v2

    .line 124
    goto :goto_2

    .line 127
    :cond_a
    const-string v0, "original_review_text"

    .line 128
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 288
    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lerf;->d(I)V

    .line 291
    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 272
    invoke-virtual {p0}, Lerf;->n()Lz;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 273
    return-void
.end method

.method protected a(Lfib;)Z
    .locals 5

    .prologue
    const v3, 0x7f0a0592

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 252
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lfib;->e()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 254
    :cond_0
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 255
    invoke-virtual {p1}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 256
    if-eqz v2, :cond_2

    .line 257
    const v3, 0x7f0a095a

    new-array v4, v1, [Ljava/lang/Object;

    aput-object v2, v4, v0

    invoke-virtual {p0, v3, v4}, Lerf;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 264
    :goto_0
    invoke-virtual {p0, v0}, Lerf;->a(Ljava/lang/String;)V

    move v0, v1

    .line 268
    :cond_1
    return v0

    .line 259
    :cond_2
    invoke-virtual {p0, v3}, Lerf;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 262
    :cond_3
    invoke-virtual {p0, v3}, Lerf;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 311
    invoke-super {p0}, Llol;->aO_()V

    .line 313
    iget-object v0, p0, Lerf;->ad:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 315
    iget-object v0, p0, Lerf;->T:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lerf;->T:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 317
    iget-object v0, p0, Lerf;->T:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 318
    const/4 v1, 0x0

    iput-object v1, p0, Lerf;->T:Ljava/lang/Integer;

    .line 319
    invoke-virtual {p0}, Lerf;->e()V

    .line 320
    invoke-virtual {p0, v0}, Lerf;->a(Lfib;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 321
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lerf;->d(I)V

    .line 325
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 295
    return-void
.end method

.method protected c(I)V
    .locals 3

    .prologue
    .line 233
    invoke-virtual {p0}, Lerf;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 236
    if-nez v0, :cond_0

    .line 237
    const/4 v0, 0x0

    .line 238
    invoke-virtual {p0, p1}, Lerf;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 237
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 239
    invoke-virtual {p0}, Lerf;->p()Lae;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 241
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 90
    iget-object v0, p0, Lerf;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lerf;->ac:Lhee;

    .line 91
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 299
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lerf;->U:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageTextButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {p0}, Lerf;->U()V

    .line 215
    :goto_0
    return-void

    .line 213
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lerf;->d(I)V

    goto :goto_0
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 437
    invoke-virtual {p0}, Lerf;->n()Lz;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 438
    invoke-virtual {p0}, Lerf;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 439
    return-void
.end method

.method protected e()V
    .locals 2

    .prologue
    .line 244
    invoke-virtual {p0}, Lerf;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 246
    if-eqz v0, :cond_0

    .line 247
    invoke-virtual {v0}, Lt;->a()V

    .line 249
    :cond_0
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lerf;->T:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 336
    const-string v0, "review_request_id"

    iget-object v1, p0, Lerf;->T:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 339
    :cond_0
    invoke-virtual {p0}, Lerf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "place_star_rating"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 340
    const-string v0, "original_star_rating"

    iget v1, p0, Lerf;->N:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 342
    :cond_1
    const-string v0, "modified_star_rating"

    iget v1, p0, Lerf;->P:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 344
    invoke-virtual {p0}, Lerf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "place_review_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 345
    const-string v0, "original_review_text"

    iget-object v1, p0, Lerf;->O:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_2
    const-string v0, "modified_review_text"

    invoke-direct {p0}, Lerf;->aa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 350
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const v6, 0x7f100656

    const v5, 0x7f100655

    const v4, 0x7f100654

    const v3, 0x7f100653

    const/4 v1, 0x1

    .line 175
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 176
    const v0, 0x7f1001da

    if-ne v2, v0, :cond_1

    .line 177
    invoke-virtual {p0}, Lerf;->d()V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    const v0, 0x7f100652

    if-ne v2, v0, :cond_2

    .line 179
    invoke-virtual {p0}, Lerf;->a()V

    goto :goto_0

    .line 180
    :cond_2
    if-eq v2, v3, :cond_3

    if-eq v2, v4, :cond_3

    if-eq v2, v5, :cond_3

    if-eq v2, v6, :cond_3

    const v0, 0x7f100657

    if-ne v2, v0, :cond_5

    :cond_3
    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    .line 181
    if-ne v2, v3, :cond_6

    .line 182
    iput v1, p0, Lerf;->P:I

    .line 192
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lerf;->X()V

    .line 193
    invoke-direct {p0}, Lerf;->Z()V

    goto :goto_0

    .line 180
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 183
    :cond_6
    if-ne v2, v4, :cond_7

    .line 184
    const/4 v0, 0x2

    iput v0, p0, Lerf;->P:I

    goto :goto_2

    .line 185
    :cond_7
    if-ne v2, v5, :cond_8

    .line 186
    const/4 v0, 0x3

    iput v0, p0, Lerf;->P:I

    goto :goto_2

    .line 187
    :cond_8
    if-ne v2, v6, :cond_9

    .line 188
    const/4 v0, 0x4

    iput v0, p0, Lerf;->P:I

    goto :goto_2

    .line 189
    :cond_9
    const v0, 0x7f100657

    if-ne v2, v0, :cond_4

    .line 190
    const/4 v0, 0x5

    iput v0, p0, Lerf;->P:I

    goto :goto_2
.end method

.method public z()V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lerf;->ad:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 330
    invoke-super {p0}, Llol;->z()V

    .line 331
    return-void
.end method
