.class public final Ldbp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfwv;
.implements Llip;


# static fields
.field private static b:Landroid/graphics/drawable/Drawable;

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:Landroid/graphics/drawable/Drawable;

.field private static e:Landroid/graphics/drawable/Drawable;

.field private static f:Landroid/graphics/drawable/Drawable;

.field private static g:Landroid/graphics/drawable/Drawable;

.field private static h:Landroid/graphics/drawable/Drawable;

.field private static i:Landroid/graphics/drawable/Drawable;

.field private static j:I

.field private static k:Landroid/graphics/Paint;

.field private static l:Landroid/graphics/Paint;

.field private static m:I

.field private static n:I

.field private static o:I


# instance fields
.field private A:Lfwu;

.field private B:Ldbr;

.field private C:I

.field private D:I

.field private E:Ljava/lang/Long;

.field private F:Ljava/lang/String;

.field private G:Ljava/lang/String;

.field private H:Lnyp;

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:I

.field private M:I

.field private N:Landroid/graphics/Point;

.field private O:Landroid/graphics/drawable/Drawable;

.field private P:Ljava/lang/String;

.field private Q:Z

.field private p:Lfwu;

.field private q:Lfwu;

.field private r:I

.field private s:I

.field private t:Lfwu;

.field private u:Landroid/graphics/Rect;

.field private v:Landroid/graphics/Rect;

.field private w:Landroid/graphics/Rect;

.field private x:Landroid/graphics/Rect;

.field private y:Landroid/graphics/Rect;

.field private z:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method private a(Landroid/graphics/Rect;IIILandroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 629
    add-int v0, p3, p2

    .line 630
    sub-int v1, p4, p2

    .line 633
    iget v3, p5, Landroid/graphics/Rect;->left:I

    add-int v4, v0, p2

    if-ge v3, v4, :cond_0

    .line 634
    iget v0, p5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, p2

    .line 636
    :cond_0
    iget v3, p5, Landroid/graphics/Rect;->right:I

    if-le v3, v1, :cond_1

    .line 637
    iget v1, p5, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, p2

    .line 641
    :cond_1
    iget v3, p1, Landroid/graphics/Rect;->left:I

    if-ge v3, v0, :cond_2

    .line 642
    iget v1, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    .line 647
    :goto_0
    invoke-virtual {p1, v0, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 648
    return-object p1

    .line 643
    :cond_2
    iget v0, p1, Landroid/graphics/Rect;->right:I

    if-le v0, v1, :cond_3

    .line 644
    iget v0, p1, Landroid/graphics/Rect;->right:I

    sub-int v0, v1, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private a(IIII)V
    .locals 1

    .prologue
    .line 692
    iget-object v0, p0, Ldbp;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 693
    return-void
.end method

.method private a(IILandroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    .line 618
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    .line 619
    iget-object v1, p0, Ldbp;->x:Landroid/graphics/Rect;

    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    add-int/2addr v2, v0

    .line 620
    invoke-virtual {p3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    add-int/2addr v3, p2

    .line 619
    invoke-virtual {v1, v0, p2, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 621
    iput-object p3, p0, Ldbp;->O:Landroid/graphics/drawable/Drawable;

    .line 622
    return-void
.end method

.method private b(IIII)V
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    .line 700
    return-void
.end method


# virtual methods
.method public a(Llip;Llip;)I
    .locals 1

    .prologue
    .line 263
    sget-object v0, Llip;->a_:Lliq;

    invoke-virtual {v0, p1, p2}, Lliq;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method

.method public a()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Ldbp;->u:Landroid/graphics/Rect;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 452
    iput p1, p0, Ldbp;->L:I

    .line 453
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Ldbp;->N:Landroid/graphics/Point;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Point;->set(II)V

    .line 449
    return-void
.end method

.method public a(IIIII)V
    .locals 7

    .prologue
    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbp;->K:Z

    .line 470
    iput p1, p0, Ldbp;->C:I

    .line 471
    iput p2, p0, Ldbp;->D:I

    .line 474
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 475
    iget v0, p0, Ldbp;->C:I

    iget v1, p0, Ldbp;->D:I

    sget-object v2, Ldbp;->i:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, v1, v2}, Ldbp;->a(IILandroid/graphics/drawable/Drawable;)V

    .line 476
    sget-object v0, Ldbp;->d:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    .line 482
    :goto_0
    iget-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 485
    iget v0, p0, Ldbp;->C:I

    iget-object v1, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 486
    iget v1, p0, Ldbp;->D:I

    iget-object v2, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    add-int/2addr v2, v0

    iget v3, p0, Ldbp;->D:I

    iget-object v4, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    .line 487
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 486
    invoke-direct {p0, v0, v1, v2, v3}, Ldbp;->a(IIII)V

    .line 490
    :cond_0
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->c(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 492
    iget v0, p0, Ldbp;->D:I

    sget v1, Ldbp;->j:I

    add-int/2addr v0, v1

    iget-object v1, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v0, v1

    .line 496
    :goto_1
    iget v1, p0, Ldbp;->r:I

    iget v2, p0, Ldbp;->s:I

    add-int/2addr v2, v1

    .line 497
    iget-boolean v1, p0, Ldbp;->Q:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Ldbp;->t:Lfwu;

    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    move v6, v1

    .line 500
    :goto_2
    iget-object v1, p0, Ldbp;->A:Lfwu;

    if-eqz v1, :cond_2

    .line 501
    iget-object v1, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-le v2, v1, :cond_1

    .line 502
    iget-object v1, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v1, v2}, Lfwu;->b(I)V

    .line 504
    :cond_1
    iget v1, p0, Ldbp;->C:I

    iget-object v2, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 505
    iget-object v2, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget-object v3, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v4, p0, Ldbp;->A:Lfwu;

    .line 506
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v1

    add-int/2addr v4, v6

    .line 505
    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 507
    iget-object v1, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget-object v5, p0, Ldbp;->x:Landroid/graphics/Rect;

    move-object v0, p0

    move v2, p5

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Ldbp;->a(Landroid/graphics/Rect;IIILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Ldbp;->y:Landroid/graphics/Rect;

    .line 509
    iget-object v0, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v6

    iget-object v3, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v0, v1, v2, v3}, Ldbp;->b(IIII)V

    .line 514
    :cond_2
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->c(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 515
    iget v0, p0, Ldbp;->D:I

    sget v1, Ldbp;->j:I

    add-int/2addr v0, v1

    .line 516
    iget-object v1, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 517
    iget-object v2, p0, Ldbp;->p:Lfwu;

    iget v3, p0, Ldbp;->r:I

    invoke-virtual {v2, v3}, Lfwu;->b(I)V

    .line 518
    iget-object v2, p0, Ldbp;->q:Lfwu;

    iget v3, p0, Ldbp;->s:I

    invoke-virtual {v2, v3}, Lfwu;->b(I)V

    .line 520
    iget-object v2, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, p0, Ldbp;->r:I

    iget v4, p0, Ldbp;->s:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    .line 522
    if-lez v2, :cond_3

    .line 523
    iget-object v3, p0, Ldbp;->p:Lfwu;

    iget v4, p0, Ldbp;->r:I

    div-int/lit8 v5, v2, 0x2

    add-int/2addr v4, v5

    invoke-virtual {v3, v4}, Lfwu;->b(I)V

    .line 524
    iget-object v3, p0, Ldbp;->q:Lfwu;

    iget v4, p0, Ldbp;->s:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    invoke-virtual {v3, v2}, Lfwu;->b(I)V

    .line 527
    :cond_3
    iget-object v2, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v4, p0, Ldbp;->p:Lfwu;

    .line 528
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v1

    .line 527
    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 529
    iget-object v1, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->right:I

    .line 530
    iget-object v2, p0, Ldbp;->q:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Ldbp;->q:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int v3, v0, v3

    iget-object v4, p0, Ldbp;->q:Lfwu;

    .line 531
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v1

    .line 530
    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 534
    :cond_4
    iget-object v0, p0, Ldbp;->A:Lfwu;

    if-eqz v0, :cond_5

    .line 535
    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sget v1, Ldbp;->j:I

    sub-int/2addr v0, v1

    .line 536
    iget-object v1, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 537
    iget-object v2, p0, Ldbp;->t:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Ldbp;->t:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Ldbp;->t:Lfwu;

    .line 538
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v1

    .line 537
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 540
    :cond_5
    return-void

    .line 478
    :cond_6
    iget v0, p0, Ldbp;->C:I

    iget v1, p0, Ldbp;->D:I

    sget-object v2, Ldbp;->h:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, v1, v2}, Ldbp;->a(IILandroid/graphics/drawable/Drawable;)V

    .line 479
    sget-object v0, Ldbp;->e:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 494
    :cond_7
    iget v0, p0, Ldbp;->D:I

    sget v1, Ldbp;->j:I

    add-int/2addr v0, v1

    goto/16 :goto_1

    .line 497
    :cond_8
    const/4 v1, 0x0

    move v6, v1

    goto/16 :goto_2
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 655
    iget-boolean v0, p0, Ldbp;->J:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldbp;->A:Lfwu;

    if-eqz v0, :cond_3

    .line 656
    iget-object v0, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Ldbp;->w:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 657
    iget-object v0, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 658
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 659
    iget-object v0, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 660
    iget-object v0, p0, Ldbp;->q:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 661
    iget-object v0, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v0

    iget-object v0, p0, Ldbp;->p:Lfwu;

    .line 662
    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    int-to-float v2, v0

    iget-object v0, p0, Ldbp;->p:Lfwu;

    .line 663
    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Ldbp;->p:Lfwu;

    .line 664
    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x4

    sub-int/2addr v0, v4

    int-to-float v4, v0

    sget-object v5, Ldbp;->l:Landroid/graphics/Paint;

    move-object v0, p1

    .line 661
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 667
    :cond_0
    iget-boolean v0, p0, Ldbp;->Q:Z

    if-eqz v0, :cond_1

    .line 668
    iget-object v0, p0, Ldbp;->t:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 669
    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v0

    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sget v2, Ldbp;->j:I

    add-int/2addr v0, v2

    int-to-float v2, v0

    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sget v4, Ldbp;->j:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    sget-object v5, Ldbp;->k:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 673
    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v0

    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Ldbp;->w:Landroid/graphics/Rect;

    .line 674
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    int-to-float v2, v0

    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Ldbp;->w:Landroid/graphics/Rect;

    .line 676
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x4

    sub-int/2addr v0, v4

    int-to-float v4, v0

    sget-object v5, Ldbp;->l:Landroid/graphics/Paint;

    move-object v0, p1

    .line 673
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 680
    :cond_1
    iget-object v0, p0, Ldbp;->O:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Ldbp;->x:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 681
    iget-object v0, p0, Ldbp;->O:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 686
    :cond_2
    :goto_0
    return-void

    .line 682
    :cond_3
    iget-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 683
    iget-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Ldbp;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 684
    iget-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;Lnys;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 159
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 160
    sget-object v0, Ldbp;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 161
    const v0, 0x7f020339

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ldbp;->b:Landroid/graphics/drawable/Drawable;

    .line 162
    const v0, 0x7f020342

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ldbp;->c:Landroid/graphics/drawable/Drawable;

    .line 163
    const v0, 0x7f02033b

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ldbp;->f:Landroid/graphics/drawable/Drawable;

    .line 164
    const v0, 0x7f02033d

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ldbp;->g:Landroid/graphics/drawable/Drawable;

    .line 166
    const v0, 0x7f02033a

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ldbp;->d:Landroid/graphics/drawable/Drawable;

    .line 167
    const v0, 0x7f020343

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ldbp;->e:Landroid/graphics/drawable/Drawable;

    .line 168
    const v0, 0x7f0203ee

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ldbp;->h:Landroid/graphics/drawable/Drawable;

    .line 169
    const v0, 0x7f02033c

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Ldbp;->i:Landroid/graphics/drawable/Drawable;

    .line 171
    const v0, 0x7f0d0315

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Ldbp;->j:I

    .line 173
    const v0, 0x7f0d0316

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Ldbp;->m:I

    .line 175
    const v0, 0x7f0d0317

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Ldbp;->n:I

    .line 178
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 179
    sput-object v0, Ldbp;->k:Landroid/graphics/Paint;

    const v1, 0x106000c

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 180
    sget-object v0, Ldbp;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 181
    sget-object v0, Ldbp;->k:Landroid/graphics/Paint;

    sget v1, Ldbp;->m:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 183
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 184
    sput-object v0, Ldbp;->l:Landroid/graphics/Paint;

    const v1, 0x106000b

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 185
    sget-object v0, Ldbp;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 186
    sget-object v0, Ldbp;->l:Landroid/graphics/Paint;

    sget v1, Ldbp;->n:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 188
    const v0, 0x7f0d0318

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Ldbp;->o:I

    .line 192
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a09f5

    .line 193
    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v5, p0

    .line 192
    invoke-static/range {v0 .. v5}, Lfum;->a(Landroid/content/Context;Ljava/lang/CharSequence;IIILfwv;)Lfwu;

    move-result-object v0

    iput-object v0, p0, Ldbp;->p:Lfwu;

    .line 196
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a09f6

    .line 197
    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x9

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v5, p0

    .line 196
    invoke-static/range {v0 .. v5}, Lfum;->a(Landroid/content/Context;Ljava/lang/CharSequence;IIILfwv;)Lfwu;

    move-result-object v0

    iput-object v0, p0, Ldbp;->q:Lfwu;

    .line 200
    iget-object v0, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Ldbp;->r:I

    .line 201
    iget-object v0, p0, Ldbp;->q:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Ldbp;->s:I

    .line 202
    const v0, 0x7f020344

    invoke-static {v8, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 204
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const-string v6, "content-descript"

    const/4 v7, 0x0

    move-object v5, p0

    invoke-static/range {v0 .. v7}, Lfum;->a(Landroid/content/Context;Landroid/graphics/Bitmap;IIILfwv;Ljava/lang/CharSequence;Z)Lfwu;

    move-result-object v0

    iput-object v0, p0, Ldbp;->t:Lfwu;

    .line 207
    iget-object v0, p2, Lnys;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Ldbp;->E:Ljava/lang/Long;

    .line 208
    iget-object v0, p2, Lnys;->f:Lnyp;

    iput-object v0, p0, Ldbp;->H:Lnyp;

    .line 209
    iget-object v0, p2, Lnys;->e:Lnyo;

    .line 210
    iget v0, p2, Lnys;->d:I

    iput v0, p0, Ldbp;->M:I

    .line 211
    const/4 v0, 0x0

    .line 212
    iget-object v1, p2, Lnys;->h:[Lnyz;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lnys;->h:[Lnyz;

    array-length v1, v1

    if-eqz v1, :cond_1

    .line 214
    iget-object v0, p2, Lnys;->h:[Lnyz;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 216
    :cond_1
    if-nez v0, :cond_5

    const/4 v2, 0x0

    .line 217
    :goto_1
    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_2
    iput-object v0, p0, Ldbp;->P:Ljava/lang/String;

    .line 218
    iget-object v0, p2, Lnys;->c:Lnyz;

    if-eqz v0, :cond_7

    iget-object v0, p2, Lnys;->c:Lnyz;

    iget-object v1, v0, Lnyz;->d:Ljava/lang/String;

    .line 219
    :goto_3
    iget-object v0, p2, Lnys;->c:Lnyz;

    if-eqz v0, :cond_8

    iget-object v0, p2, Lnys;->c:Lnyz;

    iget-object v0, v0, Lnyz;->c:Ljava/lang/String;

    :goto_4
    iput-object v0, p0, Ldbp;->F:Ljava/lang/String;

    .line 220
    if-eqz v1, :cond_9

    move-object v0, v1

    :goto_5
    iput-object v0, p0, Ldbp;->G:Ljava/lang/String;

    .line 221
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->b(I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 222
    iget-object v0, p0, Ldbp;->P:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 223
    const v0, 0x7f0a0aaa

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 226
    :goto_6
    sget-object v0, Ldbp;->c:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    .line 227
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lfum;->a(Landroid/content/Context;Ljava/lang/CharSequence;IIILfwv;)Lfwu;

    move-result-object v0

    iput-object v0, p0, Ldbp;->A:Lfwu;

    .line 250
    :goto_7
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget v0, p0, Ldbp;->M:I

    .line 251
    invoke-static {v0}, Lfve;->d(I)Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_2
    iget-object v0, p2, Lnys;->i:Ljava/lang/Boolean;

    .line 252
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p2, Lnys;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_3
    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, Ldbp;->Q:Z

    .line 254
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ldbp;->u:Landroid/graphics/Rect;

    .line 255
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    .line 256
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ldbp;->x:Landroid/graphics/Rect;

    .line 257
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Ldbp;->N:Landroid/graphics/Point;

    .line 258
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Ldbp;->y:Landroid/graphics/Rect;

    .line 259
    return-void

    .line 207
    :cond_4
    iget-object v0, p2, Lnys;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_0

    .line 216
    :cond_5
    iget-object v2, v0, Lnyz;->d:Ljava/lang/String;

    goto/16 :goto_1

    .line 217
    :cond_6
    iget-object v0, v0, Lnyz;->c:Ljava/lang/String;

    goto/16 :goto_2

    .line 218
    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 219
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_9
    move-object v0, v2

    .line 220
    goto/16 :goto_5

    .line 229
    :cond_a
    iget v0, p0, Ldbp;->M:I

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    if-eqz v0, :cond_d

    .line 230
    iget-object v0, p0, Ldbp;->F:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 231
    const v0, 0x7f0a0aaa

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 234
    :cond_b
    sget-object v0, Ldbp;->c:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    .line 235
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x7

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lfum;->a(Landroid/content/Context;Ljava/lang/CharSequence;IIILfwv;)Lfwu;

    move-result-object v0

    iput-object v0, p0, Ldbp;->A:Lfwu;

    goto/16 :goto_7

    .line 229
    :cond_c
    const/4 v0, 0x0

    goto :goto_9

    .line 237
    :cond_d
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->d(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 238
    sget-object v0, Ldbp;->c:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Ldbp;->A:Lfwu;

    goto/16 :goto_7

    .line 240
    :cond_e
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->a(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 241
    sget-object v0, Ldbp;->b:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    .line 243
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lfum;->a(Landroid/content/Context;Ljava/lang/CharSequence;IIILfwv;)Lfwu;

    move-result-object v0

    iput-object v0, p0, Ldbp;->A:Lfwu;

    goto/16 :goto_7

    .line 246
    :cond_f
    const/4 v0, 0x0

    iput-object v0, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Ldbp;->A:Lfwu;

    goto/16 :goto_7

    .line 252
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_8

    :cond_11
    move-object v1, v2

    goto/16 :goto_6
.end method

.method public a(Ldbr;)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Ldbp;->B:Ldbr;

    .line 441
    return-void
.end method

.method public a(Lfwu;)V
    .locals 7

    .prologue
    .line 325
    if-nez p1, :cond_1

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 328
    :cond_1
    iget-object v0, p0, Ldbp;->t:Lfwu;

    if-ne p1, v0, :cond_2

    .line 329
    iget-object v0, p0, Ldbp;->B:Ldbr;

    sget-object v1, Ldbq;->a:Ldbq;

    iget-object v2, p0, Ldbp;->E:Ljava/lang/Long;

    iget-object v3, p0, Ldbp;->F:Ljava/lang/String;

    iget-object v4, p0, Ldbp;->G:Ljava/lang/String;

    iget v5, p0, Ldbp;->M:I

    .line 330
    invoke-static {v5}, Lfve;->b(I)Z

    move-result v5

    iget-object v6, p0, Ldbp;->P:Ljava/lang/String;

    .line 329
    invoke-interface/range {v0 .. v6}, Ldbr;->a(Ldbq;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 331
    :cond_2
    iget-object v0, p0, Ldbp;->p:Lfwu;

    if-ne p1, v0, :cond_3

    .line 332
    iget-object v0, p0, Ldbp;->B:Ldbr;

    sget-object v1, Ldbq;->b:Ldbq;

    iget-object v2, p0, Ldbp;->E:Ljava/lang/Long;

    iget-object v3, p0, Ldbp;->F:Ljava/lang/String;

    iget-object v4, p0, Ldbp;->G:Ljava/lang/String;

    iget v5, p0, Ldbp;->M:I

    .line 333
    invoke-static {v5}, Lfve;->b(I)Z

    move-result v5

    iget-object v6, p0, Ldbp;->P:Ljava/lang/String;

    .line 332
    invoke-interface/range {v0 .. v6}, Ldbr;->a(Ldbq;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    .line 334
    :cond_3
    iget-object v0, p0, Ldbp;->q:Lfwu;

    if-ne p1, v0, :cond_0

    .line 335
    iget-object v0, p0, Ldbp;->B:Ldbr;

    sget-object v1, Ldbq;->c:Ldbq;

    iget-object v2, p0, Ldbp;->E:Ljava/lang/Long;

    iget-object v3, p0, Ldbp;->F:Ljava/lang/String;

    iget-object v4, p0, Ldbp;->G:Ljava/lang/String;

    iget v5, p0, Ldbp;->M:I

    .line 336
    invoke-static {v5}, Lfve;->b(I)Z

    move-result v5

    iget-object v6, p0, Ldbp;->P:Ljava/lang/String;

    .line 335
    invoke-interface/range {v0 .. v6}, Ldbr;->a(Ldbq;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 415
    iput-boolean p1, p0, Ldbp;->I:Z

    .line 416
    return-void
.end method

.method public a(III)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 268
    iget-boolean v0, p0, Ldbp;->J:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v0, p1, p2, p3}, Lfwu;->a(III)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 320
    :goto_0
    return v0

    .line 272
    :cond_0
    iget-boolean v0, p0, Ldbp;->J:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbp;->q:Lfwu;

    invoke-virtual {v0, p1, p2, p3}, Lfwu;->a(III)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 273
    goto :goto_0

    .line 277
    :cond_1
    iget-boolean v0, p0, Ldbp;->J:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Ldbp;->Q:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldbp;->t:Lfwu;

    invoke-virtual {v0, p1, p2, p3}, Lfwu;->a(III)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 278
    goto :goto_0

    .line 281
    :cond_2
    const/4 v0, 0x3

    if-ne p3, v0, :cond_3

    .line 282
    iput-boolean v2, p0, Ldbp;->I:Z

    move v0, v1

    .line 283
    goto :goto_0

    .line 286
    :cond_3
    new-instance v0, Landroid/graphics/Rect;

    iget-object v3, p0, Ldbp;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sget v4, Ldbp;->o:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Ldbp;->u:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sget v5, Ldbp;->o:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Ldbp;->u:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sget v6, Ldbp;->o:I

    add-int/2addr v5, v6

    iget-object v6, p0, Ldbp;->u:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    sget v7, Ldbp;->o:I

    add-int/2addr v6, v7

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Ldbp;->v:Landroid/graphics/Rect;

    .line 290
    iget-boolean v0, p0, Ldbp;->J:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    .line 291
    :goto_1
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_6

    .line 292
    if-ne p3, v1, :cond_4

    .line 293
    iput-boolean v2, p0, Ldbp;->I:Z

    :cond_4
    move v0, v2

    .line 295
    goto :goto_0

    .line 290
    :cond_5
    iget-object v0, p0, Ldbp;->v:Landroid/graphics/Rect;

    goto :goto_1

    .line 298
    :cond_6
    packed-switch p3, :pswitch_data_0

    move v0, v2

    .line 316
    goto :goto_0

    .line 300
    :pswitch_0
    iput-boolean v1, p0, Ldbp;->I:Z

    :goto_2
    move v0, v1

    .line 320
    goto :goto_0

    .line 305
    :pswitch_1
    iget-boolean v0, p0, Ldbp;->I:Z

    if-eqz v0, :cond_7

    .line 306
    iget-object v0, p0, Ldbp;->B:Ldbr;

    if-eqz v0, :cond_7

    .line 307
    iget-object v0, p0, Ldbp;->B:Ldbr;

    invoke-interface {v0, p0}, Ldbr;->a(Ldbp;)V

    .line 311
    :cond_7
    iput-boolean v2, p0, Ldbp;->I:Z

    goto :goto_2

    .line 298
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Ldbp;->E:Ljava/lang/Long;

    return-object v0
.end method

.method public b(IIIII)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 544
    iput-boolean v0, p0, Ldbp;->K:Z

    .line 545
    iput p1, p0, Ldbp;->C:I

    .line 546
    iput p2, p0, Ldbp;->D:I

    .line 548
    sget-object v1, Ldbp;->f:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Ldbp;->O:Landroid/graphics/drawable/Drawable;

    .line 551
    iget v1, p0, Ldbp;->M:I

    invoke-static {v1}, Lfve;->a(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 552
    iget v1, p0, Ldbp;->C:I

    iget v2, p0, Ldbp;->D:I

    sget-object v3, Ldbp;->f:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v1, v2, v3}, Ldbp;->a(IILandroid/graphics/drawable/Drawable;)V

    .line 553
    sget-object v1, Ldbp;->b:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    .line 560
    :goto_0
    iget-object v1, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 561
    iget v1, p0, Ldbp;->C:I

    iget-object v2, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 562
    iget v2, p0, Ldbp;->D:I

    iget-object v3, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget v4, p0, Ldbp;->D:I

    iget-object v5, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    .line 563
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 562
    invoke-direct {p0, v1, v2, v3, v4}, Ldbp;->a(IIII)V

    .line 567
    :cond_0
    iget v1, p0, Ldbp;->D:I

    iget-object v2, p0, Ldbp;->O:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    add-int/2addr v1, v2

    sget v2, Ldbp;->j:I

    sub-int/2addr v1, v2

    .line 568
    iget v2, p0, Ldbp;->r:I

    iget v3, p0, Ldbp;->s:I

    add-int/2addr v2, v3

    .line 569
    iget-boolean v3, p0, Ldbp;->Q:Z

    if-eqz v3, :cond_7

    iget-object v0, p0, Ldbp;->t:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    move v6, v0

    .line 571
    :goto_1
    iget-object v0, p0, Ldbp;->A:Lfwu;

    if-eqz v0, :cond_2

    .line 572
    iget-object v0, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-le v2, v0, :cond_1

    .line 573
    iget-object v0, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v0, v2}, Lfwu;->b(I)V

    .line 575
    :cond_1
    iget v0, p0, Ldbp;->C:I

    iget-object v2, p0, Ldbp;->A:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    .line 576
    iget-object v2, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget-object v3, p0, Ldbp;->A:Lfwu;

    .line 577
    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/2addr v3, v0

    add-int/2addr v3, v6

    iget-object v4, p0, Ldbp;->A:Lfwu;

    .line 578
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v1

    .line 576
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 580
    iget-object v1, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget-object v5, p0, Ldbp;->x:Landroid/graphics/Rect;

    move-object v0, p0

    move v2, p5

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Ldbp;->a(Landroid/graphics/Rect;IIILandroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Ldbp;->y:Landroid/graphics/Rect;

    .line 582
    iget-object v0, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v6

    iget-object v3, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v0, v1, v2, v3}, Ldbp;->b(IIII)V

    .line 587
    :cond_2
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->c(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Ldbp;->J:Z

    if-eqz v0, :cond_4

    .line 589
    iget-object v0, p0, Ldbp;->p:Lfwu;

    iget v1, p0, Ldbp;->r:I

    invoke-virtual {v0, v1}, Lfwu;->b(I)V

    .line 590
    iget-object v0, p0, Ldbp;->q:Lfwu;

    iget v1, p0, Ldbp;->s:I

    invoke-virtual {v0, v1}, Lfwu;->b(I)V

    .line 592
    iget-object v0, p0, Ldbp;->y:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Ldbp;->r:I

    iget v2, p0, Ldbp;->s:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 594
    if-lez v0, :cond_3

    .line 595
    iget-object v1, p0, Ldbp;->p:Lfwu;

    iget v2, p0, Ldbp;->r:I

    div-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lfwu;->b(I)V

    .line 596
    iget-object v1, p0, Ldbp;->q:Lfwu;

    iget v2, p0, Ldbp;->s:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lfwu;->b(I)V

    .line 599
    :cond_3
    iget-object v0, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 600
    iget-object v1, p0, Ldbp;->y:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 601
    iget-object v2, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Ldbp;->p:Lfwu;

    .line 602
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v0

    .line 601
    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 603
    iget-object v2, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    add-int/2addr v1, v2

    .line 604
    iget-object v2, p0, Ldbp;->q:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Ldbp;->q:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Ldbp;->q:Lfwu;

    .line 605
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v0

    .line 604
    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 608
    :cond_4
    iget-object v0, p0, Ldbp;->A:Lfwu;

    if-eqz v0, :cond_5

    .line 609
    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sget v1, Ldbp;->j:I

    sub-int/2addr v0, v1

    .line 610
    iget-object v1, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 611
    iget-object v2, p0, Ldbp;->t:Lfwu;

    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Ldbp;->t:Lfwu;

    invoke-virtual {v3}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Ldbp;->t:Lfwu;

    .line 612
    invoke-virtual {v4}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v1

    .line 611
    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 614
    :cond_5
    return-void

    .line 555
    :cond_6
    iget v1, p0, Ldbp;->C:I

    iget v2, p0, Ldbp;->D:I

    sget-object v3, Ldbp;->g:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v1, v2, v3}, Ldbp;->a(IILandroid/graphics/drawable/Drawable;)V

    .line 556
    sget-object v1, Ldbp;->c:Landroid/graphics/drawable/Drawable;

    iput-object v1, p0, Ldbp;->z:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_7
    move v6, v0

    .line 569
    goto/16 :goto_1
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 444
    iput-boolean p1, p0, Ldbp;->J:Z

    .line 445
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Ldbp;->F:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 34
    check-cast p1, Llip;

    check-cast p2, Llip;

    invoke-virtual {p0, p1, p2}, Ldbp;->a(Llip;Llip;)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 2

    .prologue
    .line 372
    iget-boolean v0, p0, Ldbp;->J:Z

    if-eqz v0, :cond_1

    .line 373
    iget v0, p0, Ldbp;->M:I

    invoke-static {v0}, Lfve;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    .line 374
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Ldbp;->p:Lfwu;

    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int/2addr v0, v1

    .line 377
    :goto_0
    return v0

    .line 374
    :cond_0
    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    .line 375
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    goto :goto_0

    .line 377
    :cond_1
    iget-object v0, p0, Ldbp;->u:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 382
    iget v0, p0, Ldbp;->M:I

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Ldbp;->J:Z

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 390
    iget-boolean v0, p0, Ldbp;->K:Z

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 394
    iget-boolean v0, p0, Ldbp;->Q:Z

    return v0
.end method

.method public i()Lnyp;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Ldbp;->H:Lnyp;

    return-object v0
.end method

.method public j()Landroid/graphics/RectF;
    .locals 5

    .prologue
    .line 428
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Ldbp;->H:Lnyp;

    iget-object v1, v1, Lnyp;->a:Ljava/lang/Double;

    .line 429
    invoke-virtual {v1}, Ljava/lang/Double;->floatValue()F

    move-result v1

    iget-object v2, p0, Ldbp;->H:Lnyp;

    iget-object v2, v2, Lnyp;->c:Ljava/lang/Double;

    .line 430
    invoke-virtual {v2}, Ljava/lang/Double;->floatValue()F

    move-result v2

    iget-object v3, p0, Ldbp;->H:Lnyp;

    iget-object v3, v3, Lnyp;->b:Ljava/lang/Double;

    .line 431
    invoke-virtual {v3}, Ljava/lang/Double;->floatValue()F

    move-result v3

    iget-object v4, p0, Ldbp;->H:Lnyp;

    iget-object v4, v4, Lnyp;->d:Ljava/lang/Double;

    .line 432
    invoke-virtual {v4}, Ljava/lang/Double;->floatValue()F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public k()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 457
    invoke-virtual {p0}, Ldbp;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    .line 459
    :goto_0
    new-instance v1, Landroid/graphics/Rect;

    iget-object v2, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-direct {v1, v2, v3, v0, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1

    .line 457
    :cond_0
    iget-object v0, p0, Ldbp;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Ldbp;->t:Lfwu;

    .line 458
    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 464
    iget v0, p0, Ldbp;->L:I

    return v0
.end method
