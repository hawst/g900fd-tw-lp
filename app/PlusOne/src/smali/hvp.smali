.class public final Lhvp;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lkdd;
.implements Lljh;
.implements Lljv;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static c:Lhwd;


# instance fields
.field a:Landroid/widget/ImageButton;

.field b:Landroid/widget/ImageButton;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private e:Llip;

.field private f:Lhvr;

.field private g:Lhub;

.field private h:Lhuc;

.field private i:Lhtv;

.field private j:Lhtu;

.field private k:Lhtw;

.field private l:Lhue;

.field private m:Lizu;

.field private n:Lcom/google/android/libraries/social/media/MediaResource;

.field private o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private p:Ljava/lang/CharSequence;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/graphics/Bitmap;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Lljv;

.field private w:I

.field private x:Z

.field private final y:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 142
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhvp;->d:Ljava/util/List;

    .line 121
    new-instance v0, Lhvq;

    invoke-direct {v0, p0}, Lhvq;-><init>(Lhvp;)V

    iput-object v0, p0, Lhvp;->y:Landroid/view/View$OnClickListener;

    .line 143
    invoke-virtual {p0, v1}, Lhvp;->setClickable(Z)V

    .line 144
    invoke-virtual {p0, v1}, Lhvp;->setWillNotDraw(Z)V

    .line 145
    return-void
.end method

.method static synthetic a(Lhvp;)I
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lhvp;->h()I

    move-result v0

    return v0
.end method

.method private a(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 753
    if-nez p1, :cond_1

    .line 756
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lhvp;->g:Lhub;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()I
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lhvp;->l:Lhue;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 420
    :goto_0
    return v0

    .line 419
    :cond_0
    iget-object v0, p0, Lhvp;->l:Lhue;

    .line 420
    invoke-interface {v0}, Lhue;->c()I

    move-result v0

    goto :goto_0
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 444
    iget-object v2, p0, Lhvp;->a:Landroid/widget/ImageButton;

    if-nez v2, :cond_0

    .line 445
    new-instance v2, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lhvp;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lhvp;->a:Landroid/widget/ImageButton;

    .line 447
    :cond_0
    iget-object v2, p0, Lhvp;->l:Lhue;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhvp;->l:Lhue;

    .line 448
    invoke-interface {v2}, Lhue;->c()I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 450
    :goto_0
    iget-object v3, p0, Lhvp;->a:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    sget-object v2, Lhvp;->c:Lhwd;

    const v2, 0x7f0204d1

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 452
    iget-object v2, p0, Lhvp;->a:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 453
    iget-object v1, p0, Lhvp;->a:Landroid/widget/ImageButton;

    invoke-direct {p0, v1}, Lhvp;->a(Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 454
    iget-object v1, p0, Lhvp;->a:Landroid/widget/ImageButton;

    invoke-virtual {p0, v1}, Lhvp;->addView(Landroid/view/View;)V

    .line 457
    :cond_1
    iget-object v1, p0, Lhvp;->a:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    new-instance v0, Lhmk;

    sget-object v2, Lonm;->p:Lhmn;

    invoke-direct {v0, v2}, Lhmk;-><init>(Lhmn;)V

    :goto_2
    invoke-static {v1, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 460
    iget-object v0, p0, Lhvp;->a:Landroid/widget/ImageButton;

    new-instance v1, Lhmi;

    iget-object v2, p0, Lhvp;->y:Landroid/view/View$OnClickListener;

    invoke-direct {v1, v2}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 462
    return-void

    :cond_2
    move v0, v1

    .line 448
    goto :goto_0

    .line 450
    :cond_3
    sget-object v2, Lhvp;->c:Lhwd;

    const v2, 0x7f0204da

    goto :goto_1

    .line 457
    :cond_4
    new-instance v0, Lhmk;

    sget-object v2, Lonm;->f:Lhmn;

    invoke-direct {v0, v2}, Lhmk;-><init>(Lhmn;)V

    goto :goto_2
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 466
    invoke-direct {p0}, Lhvp;->h()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 467
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 468
    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lhvp;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    .line 470
    :cond_0
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    sget-object v1, Lhvp;->c:Lhwd;

    const v1, 0x7f0204ea

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 472
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    .line 473
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lhvp;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 474
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lhvp;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 475
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lhvp;->addView(Landroid/view/View;)V

    .line 477
    :cond_1
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    new-instance v1, Lhmk;

    sget-object v2, Lonm;->m:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 479
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    new-instance v1, Lhmi;

    iget-object v2, p0, Lhvp;->y:Landroid/view/View$OnClickListener;

    invoke-direct {v1, v2}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 492
    :cond_2
    :goto_0
    return-void

    .line 482
    :cond_3
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 483
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lhvp;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 484
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lhvp;->removeView(Landroid/view/View;)V

    .line 486
    :cond_4
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    const v1, 0x3a5748f

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 487
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 488
    iput-object v2, p0, Lhvp;->b:Landroid/widget/ImageButton;

    goto :goto_0
.end method

.method private k()Z
    .locals 1

    .prologue
    .line 850
    iget-object v0, p0, Lhvp;->j:Lhtu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvp;->j:Lhtu;

    invoke-interface {v0}, Lhtu;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lhvp;->c:Lhwd;

    iget-object v0, v0, Lhwd;->F:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lhvr;Lhtt;Lhue;ZLhuc;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 14

    .prologue
    .line 153
    invoke-virtual {p0}, Lhvp;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 154
    sget-object v1, Lhvp;->c:Lhwd;

    if-nez v1, :cond_0

    .line 155
    invoke-static {v5}, Lhwd;->a(Landroid/content/Context;)Lhwd;

    move-result-object v1

    sput-object v1, Lhvp;->c:Lhwd;

    .line 157
    :cond_0
    move-object/from16 v0, p8

    iput-object v0, p0, Lhvp;->t:Ljava/lang/String;

    .line 158
    move-object/from16 v0, p7

    iput-object v0, p0, Lhvp;->s:Ljava/lang/String;

    .line 159
    move-object/from16 v0, p9

    iput-object v0, p0, Lhvp;->u:Ljava/lang/String;

    .line 161
    iput-object p1, p0, Lhvp;->f:Lhvr;

    .line 162
    const/4 v1, 0x0

    .line 163
    if-eqz p2, :cond_1e

    .line 164
    invoke-interface/range {p2 .. p2}, Lhtt;->b()Lhub;

    move-result-object v1

    iput-object v1, p0, Lhvp;->g:Lhub;

    .line 165
    invoke-interface/range {p2 .. p2}, Lhtt;->a()Lhtv;

    move-result-object v1

    iput-object v1, p0, Lhvp;->i:Lhtv;

    .line 166
    invoke-interface/range {p2 .. p2}, Lhtt;->c()Lhud;

    move-result-object v1

    .line 167
    invoke-interface/range {p2 .. p2}, Lhtt;->d()Lhtu;

    move-result-object v2

    iput-object v2, p0, Lhvp;->j:Lhtu;

    .line 168
    invoke-interface/range {p2 .. p2}, Lhtt;->e()Lhtw;

    move-result-object v2

    iput-object v2, p0, Lhvp;->k:Lhtw;

    .line 169
    move-object/from16 v0, p3

    iput-object v0, p0, Lhvp;->l:Lhue;

    move-object v2, v1

    .line 171
    :goto_0
    iget-object v1, p0, Lhvp;->k:Lhtw;

    if-eqz v1, :cond_c

    if-eqz p10, :cond_c

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lhvp;->x:Z

    .line 173
    move-object/from16 v0, p5

    iput-object v0, p0, Lhvp;->h:Lhuc;

    .line 175
    iget-object v1, p0, Lhvp;->i:Lhtv;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lhvp;->i:Lhtv;

    .line 176
    invoke-interface {v1}, Lhtv;->a()I

    move-result v1

    const/4 v3, 0x1

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lhvp;->i:Lhtv;

    .line 177
    invoke-interface {v1}, Lhtv;->a()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_d

    .line 178
    :cond_1
    sget-object v1, Lhvp;->c:Lhwd;

    iget-object v1, v1, Lhwd;->C:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    .line 189
    :goto_2
    const/4 v1, 0x0

    iput-object v1, p0, Lhvp;->m:Lizu;

    .line 190
    iget-boolean v1, p0, Lhvp;->x:Z

    if-eqz v1, :cond_2

    .line 192
    const/4 v1, 0x0

    iput-object v1, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    .line 194
    iget-object v1, p0, Lhvp;->k:Lhtw;

    invoke-interface {v1}, Lhtw;->a()Ljava/lang/String;

    move-result-object v1

    .line 195
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 196
    sget-object v3, Ljac;->a:Ljac;

    invoke-static {v5, v1, v3}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    iput-object v1, p0, Lhvp;->m:Lizu;

    .line 200
    :cond_2
    invoke-virtual {p0}, Lhvp;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 201
    const/4 v1, 0x0

    iput-object v1, p0, Lhvp;->p:Ljava/lang/CharSequence;

    .line 203
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 204
    if-eqz p5, :cond_15

    invoke-interface/range {p5 .. p5}, Lhuc;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 206
    invoke-interface/range {p5 .. p5}, Lhuc;->b()Ljava/lang/String;

    move-result-object v3

    .line 207
    invoke-interface/range {p5 .. p5}, Lhuc;->d()Ljava/lang/String;

    move-result-object v4

    .line 208
    invoke-interface/range {p5 .. p5}, Lhuc;->e()I

    move-result v1

    const/4 v8, 0x2

    if-ne v1, v8, :cond_11

    const/4 v1, 0x1

    .line 210
    :goto_3
    iget-object v8, p0, Lhvp;->i:Lhtv;

    if-eqz v8, :cond_13

    iget-object v8, p0, Lhvp;->i:Lhtv;

    invoke-interface {v8}, Lhtv;->a()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_13

    .line 213
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 214
    const v1, 0x7f0a046b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    sget-object v9, Lhvp;->c:Lhwd;

    iget-object v9, v9, Lhwd;->a:Lfo;

    .line 215
    invoke-virtual {v9, v4}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    .line 214
    invoke-virtual {v5, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 244
    :cond_3
    :goto_4
    iget-object v1, p0, Lhvp;->f:Lhvr;

    if-nez v1, :cond_16

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 246
    :goto_5
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 248
    iget-object v1, p0, Lhvp;->g:Lhub;

    if-eqz v1, :cond_6

    .line 249
    iget-object v1, p0, Lhvp;->g:Lhub;

    invoke-interface {v1}, Lhub;->a()Ljava/util/List;

    move-result-object v4

    .line 250
    if-eqz v4, :cond_6

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 251
    const/4 v1, 0x0

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhty;

    invoke-interface {v1}, Lhty;->b()Ljava/lang/String;

    move-result-object v8

    .line 252
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    .line 256
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_18

    .line 257
    const/4 v4, 0x1

    if-ne v1, v4, :cond_17

    .line 258
    const v1, 0x7f0a046e

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, ">>NAME_MARKER<<"

    aput-object v10, v4, v9

    const/4 v9, 0x1

    const-string v10, ">>SQUARE_MARKER<<"

    aput-object v10, v4, v9

    invoke-virtual {v6, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 265
    :goto_6
    const-string v4, ">>SQUARE_MARKER<<"

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    move v13, v4

    move-object v4, v1

    move v1, v13

    .line 277
    :goto_7
    const-string v9, ">>NAME_MARKER<<"

    invoke-virtual {v4, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 278
    invoke-virtual {v7, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 280
    if-le v9, v1, :cond_1a

    .line 281
    add-int/lit8 v4, v9, 0xf

    sget-object v10, Lhvp;->c:Lhwd;

    iget-object v10, v10, Lhwd;->a:Lfo;

    .line 282
    invoke-virtual {v10, v8}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 281
    invoke-virtual {v7, v9, v4, v10}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 283
    sget-object v4, Lhvp;->c:Lhwd;

    iget-object v4, v4, Lhwd;->b:Landroid/text/style/StyleSpan;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v10, v9

    const/16 v11, 0x21

    invoke-virtual {v7, v4, v9, v10, v11}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 285
    iget-object v4, p0, Lhvp;->f:Lhvr;

    if-eqz v4, :cond_4

    .line 286
    new-instance v4, Llju;

    const-string v10, ">>NAME_MARKER<<"

    const/4 v11, 0x0

    invoke-direct {v4, v10, v11}, Llju;-><init>(Ljava/lang/String;Z)V

    .line 287
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v9

    const/16 v10, 0x21

    .line 286
    invoke-virtual {v7, v4, v9, v8, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 291
    :cond_4
    const/4 v4, -0x1

    if-eq v1, v4, :cond_5

    .line 292
    add-int/lit8 v4, v1, 0x11

    invoke-virtual {v7, v1, v4, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 310
    :cond_5
    :goto_8
    const/4 v1, 0x0

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v7, v1, v4}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lhvp;->p:Ljava/lang/CharSequence;

    .line 311
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 315
    :cond_6
    iget-object v1, p0, Lhvp;->g:Lhub;

    if-nez v1, :cond_8

    invoke-direct {p0}, Lhvp;->k()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhvp;->j:Lhtu;

    .line 316
    invoke-interface {v1}, Lhtu;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 317
    iget-object v1, p0, Lhvp;->j:Lhtu;

    invoke-interface {v1}, Lhtu;->b()Ljava/lang/String;

    move-result-object v1

    .line 318
    invoke-virtual {v7, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 319
    iget-object v4, p0, Lhvp;->f:Lhvr;

    if-eqz v4, :cond_7

    .line 320
    new-instance v4, Llju;

    const-string v8, ">>CL70259892<<"

    const/4 v9, 0x0

    invoke-direct {v4, v8, v9}, Llju;-><init>(Ljava/lang/String;Z)V

    const/4 v8, 0x0

    .line 321
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v9, 0x21

    .line 320
    invoke-virtual {v7, v4, v8, v1, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 322
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lhvp;->setClickable(Z)V

    .line 323
    invoke-virtual {p0, p0}, Lhvp;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    :cond_7
    const/4 v1, 0x0

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v7, v1, v4}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lhvp;->p:Ljava/lang/CharSequence;

    .line 326
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 329
    :cond_8
    iget-object v1, p0, Lhvp;->p:Ljava/lang/CharSequence;

    if-nez v1, :cond_9

    .line 330
    if-eqz v3, :cond_1b

    .line 331
    iput-object v3, p0, Lhvp;->p:Ljava/lang/CharSequence;

    .line 337
    :cond_9
    :goto_9
    iput-object p0, p0, Lhvp;->v:Lljv;

    .line 341
    const/4 v1, 0x0

    iput-object v1, p0, Lhvp;->m:Lizu;

    .line 342
    iget-boolean v1, p0, Lhvp;->x:Z

    if-eqz v1, :cond_b

    .line 344
    const/4 v1, 0x0

    iput-object v1, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    .line 346
    iget-object v1, p0, Lhvp;->k:Lhtw;

    invoke-interface {v1}, Lhtw;->a()Ljava/lang/String;

    move-result-object v1

    .line 347
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 348
    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v5, v1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    iput-object v1, p0, Lhvp;->m:Lizu;

    .line 352
    :cond_a
    iget-object v1, p0, Lhvp;->k:Lhtw;

    invoke-interface {v1}, Lhtw;->b()Landroid/text/SpannableString;

    move-result-object v1

    iput-object v1, p0, Lhvp;->p:Ljava/lang/CharSequence;

    .line 356
    new-instance v1, Lkqp;

    new-instance v2, Lhmk;

    sget-object v3, Lonm;->e:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-direct {v1, p0, v2, p0}, Lkqp;-><init>(Landroid/view/View;Lhmk;Lljv;)V

    iput-object v1, p0, Lhvp;->v:Lljv;

    .line 359
    iget-object v1, p0, Lhvp;->l:Lhue;

    if-eqz v1, :cond_b

    .line 360
    invoke-virtual {p0}, Lhvp;->e()V

    .line 364
    :cond_b
    iget-object v1, p0, Lhvp;->g:Lhub;

    if-eqz v1, :cond_1c

    .line 365
    iget-object v1, p0, Lhvp;->g:Lhub;

    invoke-interface {v1}, Lhub;->a()Ljava/util/List;

    move-result-object v3

    .line 366
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->r:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 367
    if-lez v4, :cond_1c

    .line 368
    new-array v1, v4, [Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iput-object v1, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 369
    const/4 v1, 0x0

    move v2, v1

    :goto_a
    if-ge v2, v4, :cond_1c

    .line 370
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhty;

    .line 371
    invoke-interface {v1}, Lhty;->c()Ljava/lang/String;

    move-result-object v6

    .line 372
    invoke-interface {v1}, Lhty;->a()Ljava/lang/String;

    move-result-object v1

    .line 373
    iget-object v7, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    new-instance v8, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v8, v5}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    aput-object v8, v7, v2

    .line 374
    iget-object v7, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v7, v7, v2

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 375
    iget-object v7, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v7, v7, v2

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 376
    iget-object v7, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v7, v7, v2

    invoke-virtual {v7, v1, v6}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v1, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v2

    invoke-virtual {v1, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 378
    iget-object v1, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v2

    invoke-virtual {p0, v1}, Lhvp;->addView(Landroid/view/View;)V

    .line 369
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_a

    .line 171
    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 179
    :cond_d
    if-eqz v2, :cond_e

    .line 180
    sget-object v1, Lhvp;->c:Lhwd;

    iget-object v1, v1, Lhwd;->E:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 181
    :cond_e
    if-eqz p4, :cond_f

    .line 182
    sget-object v1, Lhvp;->c:Lhwd;

    iget-object v1, v1, Lhwd;->D:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 183
    :cond_f
    invoke-direct {p0}, Lhvp;->k()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 184
    sget-object v1, Lhvp;->c:Lhwd;

    iget-object v1, v1, Lhwd;->F:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 186
    :cond_10
    const/4 v1, 0x0

    iput-object v1, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 208
    :cond_11
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 217
    :cond_12
    const v1, 0x7f0a046c

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_4

    .line 219
    :cond_13
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 220
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 221
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_14

    .line 222
    const v9, 0x7f0a0470

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    sget-object v12, Lhvp;->c:Lhwd;

    iget-object v12, v12, Lhwd;->a:Lfo;

    .line 223
    invoke-virtual {v12, v3}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v11

    const/4 v3, 0x1

    sget-object v11, Lhvp;->c:Lhwd;

    iget-object v11, v11, Lhwd;->a:Lfo;

    .line 224
    invoke-virtual {v11, v4}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v3

    .line 222
    invoke-virtual {v5, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 228
    :goto_b
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 229
    new-instance v4, Llju;

    const-string v9, ">>SQUARE_MARKER<<"

    const/4 v10, 0x0

    invoke-direct {v4, v9, v10}, Llju;-><init>(Ljava/lang/String;Z)V

    .line 231
    const/16 v9, 0x21

    invoke-virtual {v7, v4, v8, v3, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 233
    if-eqz v1, :cond_3

    .line 235
    new-instance v1, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v1}, Landroid/text/style/StrikethroughSpan;-><init>()V

    .line 236
    const/16 v4, 0x21

    invoke-virtual {v7, v1, v8, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_4

    .line 226
    :cond_14
    sget-object v4, Lhvp;->c:Lhwd;

    iget-object v4, v4, Lhwd;->a:Lfo;

    invoke-virtual {v4, v3}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_b

    .line 240
    :cond_15
    iget-object v1, p0, Lhvp;->i:Lhtv;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhvp;->i:Lhtv;

    invoke-interface {v1}, Lhtv;->a()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_3

    .line 242
    const v1, 0x7f0a046d

    invoke-virtual {v5, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_4

    .line 244
    :cond_16
    const/4 v1, 0x0

    .line 245
    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v7, v1, v3}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    move-object v3, v1

    goto/16 :goto_5

    .line 261
    :cond_17
    const v4, 0x7f11001e

    add-int/lit8 v9, v1, -0x1

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, ">>NAME_MARKER<<"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    add-int/lit8 v1, v1, -0x1

    .line 263
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v10, v11

    const/4 v1, 0x2

    const-string v11, ">>SQUARE_MARKER<<"

    aput-object v11, v10, v1

    .line 261
    invoke-virtual {v6, v4, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_6

    .line 267
    :cond_18
    const/4 v4, 0x1

    if-ne v1, v4, :cond_19

    .line 268
    const v1, 0x7f0a046f

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, ">>NAME_MARKER<<"

    aput-object v10, v4, v9

    invoke-virtual {v6, v1, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 274
    :goto_c
    const/4 v4, -0x1

    move v13, v4

    move-object v4, v1

    move v1, v13

    goto/16 :goto_7

    .line 271
    :cond_19
    const v4, 0x7f11001f

    add-int/lit8 v9, v1, -0x1

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, ">>NAME_MARKER<<"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    add-int/lit8 v1, v1, -0x1

    .line 272
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v10, v11

    .line 271
    invoke-virtual {v6, v4, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_c

    .line 296
    :cond_1a
    add-int/lit8 v4, v1, 0x11

    invoke-virtual {v7, v1, v4, v3}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 299
    add-int/lit8 v1, v9, 0xf

    sget-object v4, Lhvp;->c:Lhwd;

    iget-object v4, v4, Lhwd;->a:Lfo;

    .line 300
    invoke-virtual {v4, v8}, Lfo;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 299
    invoke-virtual {v7, v9, v1, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 301
    sget-object v1, Lhvp;->c:Lhwd;

    iget-object v1, v1, Lhwd;->b:Landroid/text/style/StyleSpan;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v9

    const/16 v10, 0x21

    invoke-virtual {v7, v1, v9, v4, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 303
    iget-object v1, p0, Lhvp;->f:Lhvr;

    if-eqz v1, :cond_5

    .line 304
    new-instance v1, Llju;

    const-string v4, ">>NAME_MARKER<<"

    const/4 v10, 0x0

    invoke-direct {v1, v4, v10}, Llju;-><init>(Ljava/lang/String;Z)V

    .line 305
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v9

    const/16 v8, 0x21

    .line 304
    invoke-virtual {v7, v1, v9, v4, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_8

    .line 332
    :cond_1b
    if-eqz v2, :cond_9

    .line 333
    const v1, 0x7f0a0471

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lhvp;->p:Ljava/lang/CharSequence;

    goto/16 :goto_9

    .line 383
    :cond_1c
    iget-object v1, p0, Lhvp;->p:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 384
    invoke-virtual {p0}, Lhvp;->f()V

    .line 386
    :cond_1d
    invoke-virtual {p0}, Lhvp;->b()V

    .line 388
    invoke-static {p0}, Llhn;->a(Landroid/view/View;)V

    .line 389
    sget-object v1, Lhvp;->c:Lhwd;

    iget v1, v1, Lhwd;->n:I

    add-int v1, v1, p6

    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->k:I

    add-int/2addr v1, v2

    return v1

    :cond_1e
    move-object v2, v1

    goto/16 :goto_0
.end method

.method public a()V
    .locals 5

    .prologue
    const v4, 0x3a5748f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 531
    invoke-virtual {p0}, Lhvp;->removeAllViews()V

    .line 532
    invoke-virtual {p0}, Lhvp;->c()V

    .line 533
    invoke-virtual {p0, v2}, Lhvp;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 534
    invoke-virtual {p0, v3}, Lhvp;->setClickable(Z)V

    .line 536
    iget-object v0, p0, Lhvp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 537
    iput-object v2, p0, Lhvp;->e:Llip;

    .line 538
    iput-object v2, p0, Lhvp;->v:Lljv;

    .line 540
    iput-object v2, p0, Lhvp;->f:Lhvr;

    .line 541
    iput-object v2, p0, Lhvp;->g:Lhub;

    .line 542
    iput-object v2, p0, Lhvp;->h:Lhuc;

    .line 543
    iput-object v2, p0, Lhvp;->i:Lhtv;

    .line 544
    iput-object v2, p0, Lhvp;->j:Lhtu;

    .line 546
    iput-object v2, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 547
    iput-object v2, p0, Lhvp;->p:Ljava/lang/CharSequence;

    .line 549
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 551
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 552
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 555
    :cond_0
    iput-object v2, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    .line 557
    iget-object v0, p0, Lhvp;->a:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lhvp;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 559
    iget-object v0, p0, Lhvp;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 561
    :cond_1
    iput-object v2, p0, Lhvp;->a:Landroid/widget/ImageButton;

    .line 562
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 563
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 564
    iget-object v0, p0, Lhvp;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 566
    :cond_2
    iput-object v2, p0, Lhvp;->b:Landroid/widget/ImageButton;

    .line 568
    iput v3, p0, Lhvp;->w:I

    .line 569
    iput-object v2, p0, Lhvp;->s:Ljava/lang/String;

    .line 571
    iput-object v2, p0, Lhvp;->n:Lcom/google/android/libraries/social/media/MediaResource;

    .line 572
    iput-object v2, p0, Lhvp;->m:Lizu;

    .line 573
    iput-object v2, p0, Lhvp;->k:Lhtw;

    .line 575
    iput-boolean v3, p0, Lhvp;->x:Z

    .line 576
    return-void
.end method

.method a(I)V
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lhvp;->l:Lhue;

    if-eqz v0, :cond_1

    .line 432
    iget-object v0, p0, Lhvp;->f:Lhvr;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lhvp;->f:Lhvr;

    iget-object v1, p0, Lhvp;->l:Lhue;

    .line 434
    invoke-interface {v1}, Lhue;->b()Ljava/lang/String;

    .line 433
    invoke-interface {v0}, Lhvr;->c()V

    .line 436
    :cond_0
    iget-object v0, p0, Lhvp;->l:Lhue;

    invoke-interface {v0, p1}, Lhue;->a(I)V

    .line 438
    :cond_1
    invoke-direct {p0}, Lhvp;->i()V

    .line 439
    invoke-direct {p0}, Lhvp;->j()V

    .line 440
    return-void
.end method

.method public a(Landroid/text/style/URLSpan;)V
    .locals 4

    .prologue
    .line 810
    iget-object v0, p0, Lhvp;->f:Lhvr;

    if-eqz v0, :cond_0

    .line 811
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    .line 812
    const-string v0, ">>CL70259892<<"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhvp;->j:Lhtu;

    if-eqz v0, :cond_1

    .line 813
    iget-object v0, p0, Lhvp;->f:Lhvr;

    iget-object v1, p0, Lhvp;->j:Lhtu;

    .line 814
    invoke-interface {v1}, Lhtu;->a()Ljava/lang/String;

    iget-object v1, p0, Lhvp;->j:Lhtu;

    invoke-interface {v1}, Lhtu;->b()Ljava/lang/String;

    iget-object v1, p0, Lhvp;->s:Ljava/lang/String;

    .line 813
    invoke-interface {v0, v1}, Lhvr;->c(Ljava/lang/String;)V

    .line 833
    :cond_0
    :goto_0
    return-void

    .line 815
    :cond_1
    const-string v0, ">>SQUARE_MARKER<<"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lhvp;->h:Lhuc;

    if-eqz v0, :cond_2

    .line 816
    iget-object v0, p0, Lhvp;->f:Lhvr;

    iget-object v1, p0, Lhvp;->h:Lhuc;

    invoke-interface {v1}, Lhuc;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhvp;->h:Lhuc;

    .line 817
    invoke-interface {v2}, Lhuc;->c()Ljava/lang/String;

    move-result-object v2

    .line 816
    invoke-interface {v0, v1, v2}, Lhvr;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 818
    :cond_2
    const-string v0, ">>NAME_MARKER<<"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 819
    iget-object v0, p0, Lhvp;->g:Lhub;

    invoke-interface {v0}, Lhub;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhvp;->g:Lhub;

    .line 820
    invoke-interface {v0}, Lhub;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 821
    iget-object v1, p0, Lhvp;->f:Lhvr;

    iget-object v0, p0, Lhvp;->g:Lhub;

    .line 822
    invoke-interface {v0}, Lhub;->a()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhty;

    invoke-interface {v0}, Lhty;->a()Ljava/lang/String;

    move-result-object v0

    .line 821
    invoke-interface {v1, v0}, Lhvr;->g(Ljava/lang/String;)V

    goto :goto_0

    .line 824
    :cond_3
    iget-object v0, p0, Lhvp;->f:Lhvr;

    invoke-interface {v0}, Lhvr;->b()V

    goto :goto_0

    .line 827
    :cond_4
    iget-boolean v0, p0, Lhvp;->x:Z

    if-eqz v0, :cond_0

    .line 828
    invoke-virtual {p0}, Lhvp;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lhth;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhth;

    iget-object v2, p0, Lhvp;->u:Ljava/lang/String;

    iget-object v3, p0, Lhvp;->t:Ljava/lang/String;

    .line 829
    invoke-interface {v0, v1, v2, v3}, Lhth;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lkda;)V
    .locals 0

    .prologue
    .line 732
    invoke-virtual {p0}, Lhvp;->invalidate()V

    .line 733
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 702
    iget-object v0, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 703
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 704
    iget-object v1, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b()V

    .line 703
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 708
    :cond_0
    iget-object v0, p0, Lhvp;->m:Lizu;

    if-eqz v0, :cond_1

    .line 709
    invoke-virtual {p0}, Lhvp;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lizs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iget-object v1, p0, Lhvp;->m:Lizu;

    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->m:I

    sget-object v3, Lhvp;->c:Lhwd;

    iget v3, v3, Lhwd;->m:I

    const/16 v4, 0x40

    move-object v5, p0

    .line 710
    invoke-virtual/range {v0 .. v5}, Lizs;->a(Lizu;IIILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lhvp;->n:Lcom/google/android/libraries/social/media/MediaResource;

    .line 714
    :cond_1
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 718
    iget-object v0, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_0

    .line 719
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 720
    iget-object v1, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->c()V

    .line 719
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 724
    :cond_0
    iget-object v0, p0, Lhvp;->n:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_1

    .line 725
    iget-object v0, p0, Lhvp;->n:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 726
    const/4 v0, 0x0

    iput-object v0, p0, Lhvp;->n:Lcom/google/android/libraries/social/media/MediaResource;

    .line 728
    :cond_1
    return-void
.end method

.method d()Z
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lhvp;->l:Lhue;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()V
    .locals 1

    .prologue
    .line 410
    const/4 v0, 0x0

    iput-object v0, p0, Lhvp;->m:Lizu;

    .line 412
    invoke-direct {p0}, Lhvp;->i()V

    .line 413
    invoke-direct {p0}, Lhvp;->j()V

    .line 414
    iget-object v0, p0, Lhvp;->l:Lhue;

    invoke-interface {v0}, Lhue;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhvp;->p:Ljava/lang/CharSequence;

    .line 415
    return-void
.end method

.method f()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 496
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 497
    invoke-virtual {p0}, Lhvp;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xa

    invoke-static {v0, v1, v2, v3}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    .line 499
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    sget-object v1, Lhvp;->c:Lhwd;

    iget v1, v1, Lhwd;->s:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 500
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 501
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 502
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 503
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    .line 505
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-static {}, Lljw;->a()Lljw;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 508
    :cond_0
    iget-object v0, p0, Lhvp;->p:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spannable;

    if-eqz v0, :cond_1

    .line 509
    iget-object v0, p0, Lhvp;->p:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spannable;

    iget-object v1, p0, Lhvp;->v:Lljv;

    invoke-static {v0, v1}, Llju;->a(Landroid/text/Spannable;Lljv;)V

    .line 512
    :cond_1
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lhvp;->p:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    iget-object v1, p0, Lhvp;->p:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 515
    iget-object v0, p0, Lhvp;->l:Lhue;

    if-eqz v0, :cond_2

    .line 516
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    new-instance v1, Lhmk;

    sget-object v2, Lonm;->i:Lhmn;

    invoke-direct {v1, v2}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v1}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 517
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    new-instance v1, Lhmi;

    invoke-direct {v1, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 520
    :cond_2
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhvp;->addView(Landroid/view/View;)V

    .line 521
    return-void
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "accessibility"
    .end annotation

    .prologue
    .line 526
    iget-object v0, p0, Lhvp;->p:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 659
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 660
    invoke-virtual {p0}, Lhvp;->b()V

    .line 661
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 837
    instance-of v0, p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_1

    .line 838
    invoke-virtual {p0}, Lhvp;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhsn;

    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsn;

    .line 840
    if-eqz v0, :cond_0

    .line 841
    check-cast p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lhsn;->a(Ljava/lang/String;)V

    .line 847
    :cond_0
    :goto_0
    return-void

    .line 843
    :cond_1
    invoke-direct {p0}, Lhvp;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvp;->f:Lhvr;

    if-eqz v0, :cond_0

    .line 844
    iget-object v0, p0, Lhvp;->f:Lhvr;

    iget-object v1, p0, Lhvp;->j:Lhtu;

    .line 845
    invoke-interface {v1}, Lhtu;->a()Ljava/lang/String;

    iget-object v1, p0, Lhvp;->j:Lhtu;

    invoke-interface {v1}, Lhtu;->b()Ljava/lang/String;

    iget-object v1, p0, Lhvp;->s:Ljava/lang/String;

    .line 844
    invoke-interface {v0, v1}, Lhvr;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 665
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 666
    invoke-virtual {p0}, Lhvp;->c()V

    .line 667
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 615
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 617
    invoke-virtual {p0}, Lhvp;->getWidth()I

    move-result v0

    .line 619
    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->n:I

    .line 622
    invoke-direct {p0}, Lhvp;->g()Z

    move-result v3

    if-nez v3, :cond_1

    .line 624
    iget-object v3, p0, Lhvp;->n:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lhvp;->x:Z

    if-eqz v3, :cond_0

    .line 625
    iget-object v3, p0, Lhvp;->n:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    .line 628
    :cond_0
    iget-object v3, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_1

    .line 629
    iget-object v3, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int v4, v2, v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-virtual {p1, v3, v1, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 634
    :cond_1
    iget-boolean v3, p0, Lhvp;->x:Z

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lhvp;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 635
    iget-object v3, p0, Lhvp;->a:Landroid/widget/ImageButton;

    invoke-direct {p0, v3}, Lhvp;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 636
    sget-object v3, Lhvp;->c:Lhwd;

    iget v3, v3, Lhwd;->J:I

    sub-int v3, v2, v3

    div-int/lit8 v3, v3, 0x2

    .line 638
    iget-object v4, p0, Lhvp;->a:Landroid/widget/ImageButton;

    sget-object v5, Lhvp;->c:Lhwd;

    iget v5, v5, Lhwd;->J:I

    add-int/lit8 v5, v5, 0x0

    sget-object v6, Lhvp;->c:Lhwd;

    iget v6, v6, Lhwd;->J:I

    add-int/2addr v6, v3

    invoke-virtual {v4, v7, v3, v5, v6}, Landroid/widget/ImageButton;->layout(IIII)V

    .line 641
    :cond_2
    iget-object v3, p0, Lhvp;->b:Landroid/widget/ImageButton;

    invoke-direct {p0, v3}, Lhvp;->a(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 642
    sget-object v3, Lhvp;->c:Lhwd;

    iget v3, v3, Lhwd;->J:I

    sub-int v3, v0, v3

    .line 643
    sget-object v4, Lhvp;->c:Lhwd;

    iget v4, v4, Lhwd;->J:I

    sub-int/2addr v2, v4

    div-int/lit8 v2, v2, 0x2

    .line 644
    iget-object v4, p0, Lhvp;->b:Landroid/widget/ImageButton;

    sget-object v5, Lhvp;->c:Lhwd;

    iget v5, v5, Lhwd;->J:I

    add-int/2addr v5, v3

    sget-object v6, Lhvp;->c:Lhwd;

    iget v6, v6, Lhwd;->J:I

    add-int/2addr v6, v2

    invoke-virtual {v4, v3, v2, v5, v6}, Landroid/widget/ImageButton;->layout(IIII)V

    .line 649
    :cond_3
    iget-object v2, p0, Lhvp;->g:Lhub;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 650
    iget-object v2, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v2, v2, v7

    invoke-virtual {v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    .line 653
    :cond_4
    iget v2, p0, Lhvp;->w:I

    int-to-float v2, v2

    int-to-float v3, v0

    iget v0, p0, Lhvp;->w:I

    int-to-float v4, v0

    sget-object v0, Lhvp;->c:Lhwd;

    iget-object v5, v0, Lhwd;->B:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 655
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 671
    sub-int v1, p5, p3

    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->k:I

    sub-int v2, v1, v2

    .line 674
    iget-object v1, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v1, :cond_0

    move v1, v0

    .line 675
    :goto_0
    iget-object v3, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 676
    iget-object v3, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v3

    .line 677
    sub-int v4, v2, v3

    div-int/lit8 v4, v4, 0x2

    .line 678
    iget-object v5, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    aget-object v5, v5, v0

    add-int v6, v1, v3

    add-int v7, v4, v3

    invoke-virtual {v5, v1, v4, v6, v7}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->layout(IIII)V

    .line 679
    sget-object v4, Lhvp;->c:Lhwd;

    iget v4, v4, Lhwd;->k:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 675
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 683
    :cond_1
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 684
    iget-object v0, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    .line 686
    iget-boolean v2, p0, Lhvp;->x:Z

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lhvp;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lhvp;->a:Landroid/widget/ImageButton;

    invoke-direct {p0, v2}, Lhvp;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 687
    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->J:I

    sget-object v3, Lhvp;->c:Lhwd;

    iget v3, v3, Lhwd;->k:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 695
    :cond_2
    :goto_1
    iget-object v2, p0, Lhvp;->q:Landroid/widget/TextView;

    iget-object v3, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lhvp;->q:Landroid/widget/TextView;

    .line 696
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 695
    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 698
    :cond_3
    return-void

    .line 688
    :cond_4
    invoke-direct {p0}, Lhvp;->g()Z

    move-result v2

    if-nez v2, :cond_2

    .line 689
    iget-object v2, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_5

    .line 690
    iget-object v2, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Lhvp;->c:Lhwd;

    iget v3, v3, Lhwd;->o:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    goto :goto_1

    .line 691
    :cond_5
    iget-boolean v2, p0, Lhvp;->x:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhvp;->m:Lizu;

    if-eqz v2, :cond_2

    .line 692
    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->m:I

    sget-object v3, Lhvp;->c:Lhwd;

    iget v3, v3, Lhwd;->o:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    .line 580
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 584
    iget-object v0, p0, Lhvp;->a:Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    .line 585
    sget-object v0, Lhvp;->c:Lhwd;

    iget v0, v0, Lhwd;->J:I

    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->k:I

    add-int/2addr v0, v2

    sub-int v0, v1, v0

    .line 587
    :goto_0
    iget-object v2, p0, Lhvp;->b:Landroid/widget/ImageButton;

    if-eqz v2, :cond_0

    .line 588
    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->J:I

    sget-object v3, Lhvp;->c:Lhwd;

    iget v3, v3, Lhwd;->k:I

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 592
    :cond_0
    invoke-direct {p0}, Lhvp;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 593
    iget-object v2, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a()I

    move-result v2

    .line 594
    iget-object v3, p0, Lhvp;->o:[Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    array-length v3, v3

    sget-object v4, Lhvp;->c:Lhwd;

    iget v4, v4, Lhwd;->k:I

    add-int/2addr v2, v4

    mul-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 601
    :cond_1
    :goto_1
    iget-object v2, p0, Lhvp;->p:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 602
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 603
    iget-object v2, p0, Lhvp;->q:Landroid/widget/TextView;

    invoke-virtual {v2, v0, p2}, Landroid/widget/TextView;->measure(II)V

    .line 607
    :cond_2
    sget-object v0, Lhvp;->c:Lhwd;

    iget v0, v0, Lhwd;->n:I

    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->k:I

    add-int/2addr v0, v2

    .line 608
    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->n:I

    iput v2, p0, Lhvp;->w:I

    .line 610
    invoke-virtual {p0, v1, v0}, Lhvp;->setMeasuredDimension(II)V

    .line 611
    return-void

    .line 595
    :cond_3
    iget-object v2, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    .line 596
    iget-object v2, p0, Lhvp;->r:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Lhvp;->c:Lhwd;

    iget v3, v3, Lhwd;->o:I

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    goto :goto_1

    .line 597
    :cond_4
    iget-object v2, p0, Lhvp;->m:Lizu;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lhvp;->x:Z

    if-eqz v2, :cond_1

    .line 598
    sget-object v2, Lhvp;->c:Lhwd;

    iget v2, v2, Lhwd;->m:I

    sget-object v3, Lhvp;->c:Lhwd;

    iget v3, v3, Lhwd;->o:I

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 762
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 763
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 766
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 797
    :pswitch_0
    iget-object v0, p0, Lhvp;->e:Llip;

    if-eqz v0, :cond_0

    .line 798
    iget-object v0, p0, Lhvp;->e:Llip;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    invoke-interface {v0, v3, v4, v1}, Llip;->a(III)Z

    move-result v1

    .line 804
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/2addr v0, v1

    .line 805
    :goto_1
    return v0

    .line 768
    :pswitch_1
    iget-object v0, p0, Lhvp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 769
    invoke-interface {v0, v3, v4, v1}, Llip;->a(III)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 770
    iput-object v0, p0, Lhvp;->e:Llip;

    .line 771
    invoke-virtual {p0}, Lhvp;->invalidate()V

    move v0, v2

    .line 772
    goto :goto_1

    .line 779
    :pswitch_2
    iput-object v5, p0, Lhvp;->e:Llip;

    .line 780
    iget-object v0, p0, Lhvp;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 781
    invoke-interface {v0, v3, v4, v2}, Llip;->a(III)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    .line 782
    goto :goto_2

    .line 783
    :cond_2
    invoke-virtual {p0}, Lhvp;->invalidate()V

    goto :goto_0

    .line 788
    :pswitch_3
    iget-object v0, p0, Lhvp;->e:Llip;

    if-eqz v0, :cond_0

    .line 789
    iget-object v0, p0, Lhvp;->e:Llip;

    const/4 v1, 0x3

    invoke-interface {v0, v3, v4, v1}, Llip;->a(III)Z

    move-result v1

    .line 790
    iput-object v5, p0, Lhvp;->e:Llip;

    .line 791
    invoke-virtual {p0}, Lhvp;->invalidate()V

    goto :goto_0

    .line 766
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
