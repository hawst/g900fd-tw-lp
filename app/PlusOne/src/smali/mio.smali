.class public final Lmio;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lneq;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 24865
    invoke-direct {p0}, Loxq;-><init>()V

    .line 24868
    iput-object v0, p0, Lmio;->apiHeader:Llyq;

    .line 24871
    iput-object v0, p0, Lmio;->a:Lneq;

    .line 24865
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 24888
    const/4 v0, 0x0

    .line 24889
    iget-object v1, p0, Lmio;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 24890
    const/4 v0, 0x1

    iget-object v1, p0, Lmio;->apiHeader:Llyq;

    .line 24891
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 24893
    :cond_0
    iget-object v1, p0, Lmio;->a:Lneq;

    if-eqz v1, :cond_1

    .line 24894
    const/4 v1, 0x2

    iget-object v2, p0, Lmio;->a:Lneq;

    .line 24895
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24897
    :cond_1
    iget-object v1, p0, Lmio;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 24898
    iput v0, p0, Lmio;->ai:I

    .line 24899
    return v0
.end method

.method public a(Loxn;)Lmio;
    .locals 2

    .prologue
    .line 24907
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 24908
    sparse-switch v0, :sswitch_data_0

    .line 24912
    iget-object v1, p0, Lmio;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 24913
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmio;->ah:Ljava/util/List;

    .line 24916
    :cond_1
    iget-object v1, p0, Lmio;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24918
    :sswitch_0
    return-object p0

    .line 24923
    :sswitch_1
    iget-object v0, p0, Lmio;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 24924
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmio;->apiHeader:Llyq;

    .line 24926
    :cond_2
    iget-object v0, p0, Lmio;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24930
    :sswitch_2
    iget-object v0, p0, Lmio;->a:Lneq;

    if-nez v0, :cond_3

    .line 24931
    new-instance v0, Lneq;

    invoke-direct {v0}, Lneq;-><init>()V

    iput-object v0, p0, Lmio;->a:Lneq;

    .line 24933
    :cond_3
    iget-object v0, p0, Lmio;->a:Lneq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 24908
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 24876
    iget-object v0, p0, Lmio;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 24877
    const/4 v0, 0x1

    iget-object v1, p0, Lmio;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24879
    :cond_0
    iget-object v0, p0, Lmio;->a:Lneq;

    if-eqz v0, :cond_1

    .line 24880
    const/4 v0, 0x2

    iget-object v1, p0, Lmio;->a:Lneq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 24882
    :cond_1
    iget-object v0, p0, Lmio;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 24884
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 24861
    invoke-virtual {p0, p1}, Lmio;->a(Loxn;)Lmio;

    move-result-object v0

    return-object v0
.end method
