.class public final Leit;
.super Lehh;
.source "PG"


# instance fields
.field private N:Ljava/lang/String;

.field private O:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lehh;-><init>()V

    return-void
.end method


# virtual methods
.method protected V()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method protected a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;
    .locals 9

    .prologue
    .line 84
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v8, p8

    invoke-super/range {v0 .. v8}, Lehh;->a(Landroid/content/Context;Lcom/google/android/libraries/social/stream/legacy/views/StreamGridView;Llcr;ILfdn;Lfdp;Levp;Llci;)Lfdj;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/String;[B)Lhny;
    .locals 5

    .prologue
    .line 181
    iget-object v0, p0, Leit;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 182
    new-instance v1, Llgb;

    iget-object v2, p0, Leit;->at:Llnl;

    iget-object v3, p0, Leit;->N:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v0, v3, v4}, Llgb;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0, p1}, Lehh;->a(Landroid/os/Bundle;)V

    .line 42
    if-eqz p1, :cond_0

    .line 43
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leit;->N:Ljava/lang/String;

    .line 44
    const-string v0, "delayed_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leit;->O:Ljava/lang/String;

    .line 45
    invoke-virtual {p0}, Leit;->af_()V

    .line 46
    invoke-virtual {p0}, Leit;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 50
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-virtual {p0}, Leit;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leit;->O:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 140
    :goto_0
    return-void

    .line 98
    :pswitch_0
    invoke-super {p0, p1, p2}, Lehh;->a(Ldo;Landroid/database/Cursor;)V

    goto :goto_0

    .line 103
    :pswitch_1
    iget-object v0, p0, Leit;->W:Lfdj;

    invoke-virtual {v0, v2}, Lfdj;->c(Z)V

    .line 104
    iget-object v0, p0, Leit;->W:Lfdj;

    const/4 v1, -0x1

    invoke-virtual {v0, p2, v1}, Lfdj;->b(Landroid/database/Cursor;I)V

    .line 105
    invoke-virtual {p0}, Leit;->aA()V

    .line 107
    iput-boolean v2, p0, Leit;->aq:Z

    .line 108
    iput-boolean v2, p0, Leit;->ar:Z

    .line 110
    invoke-virtual {p0}, Leit;->x()Landroid/view/View;

    .line 111
    iget-boolean v0, p0, Leit;->ak:Z

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Leit;->U:Licq;

    const v1, 0x7f0a058f

    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    .line 113
    iget-object v0, p0, Leit;->U:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 137
    :goto_1
    invoke-virtual {p0}, Leit;->ay()V

    goto :goto_0

    .line 115
    :cond_0
    iget-object v0, p0, Leit;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    .line 116
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 117
    iget-object v0, p0, Leit;->U:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 118
    iget-object v0, p0, Leit;->Y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Leit;->aq:Z

    .line 134
    :cond_1
    :goto_2
    iput-boolean v2, p0, Leit;->ai:Z

    goto :goto_1

    .line 119
    :cond_2
    const-string v1, "fetch_newer"

    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "fetch_older"

    .line 120
    invoke-virtual {v0, v1}, Lhoc;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 121
    :cond_3
    invoke-virtual {p0}, Leit;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Leit;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 124
    :cond_4
    iget-object v0, p0, Leit;->N:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 125
    iget-boolean v0, p0, Leit;->ai:Z

    if-eqz v0, :cond_5

    .line 126
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Leit;->a(Z)V

    goto :goto_2

    .line 127
    :cond_5
    invoke-virtual {p0}, Leit;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Leit;->U:Licq;

    const v1, 0x7f0a06f6

    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    .line 129
    iget-object v0, p0, Leit;->U:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 132
    :cond_6
    iget-object v0, p0, Leit;->U:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_2

    .line 96
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 31
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Leit;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 3

    .prologue
    .line 145
    const-string v0, "fetch_newer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fetch_older"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lhos;->a(Z)V

    .line 147
    invoke-static {p2}, Lhoz;->a(Lhoz;)Z

    move-result v0

    iput-boolean v0, p0, Leit;->ak:Z

    .line 148
    invoke-virtual {p0}, Leit;->aD()V

    .line 149
    invoke-virtual {p0}, Leit;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lehh;->a(Ljava/lang/String;Lhoz;Lhos;)V

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 5

    .prologue
    .line 164
    iget-object v0, p0, Leit;->N:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {p0}, Leit;->ay()V

    .line 176
    :goto_0
    return-void

    .line 168
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Leit;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Leit;->U:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 171
    :cond_1
    iget-object v0, p0, Leit;->R:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 172
    new-instance v1, Llgb;

    invoke-virtual {p0}, Leit;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Leit;->N:Ljava/lang/String;

    iget-object v4, p0, Leit;->Y:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3, v4}, Llgb;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 173
    if-eqz p1, :cond_2

    const-string v0, "fetch_newer"

    :goto_1
    invoke-virtual {v1, v0}, Lhny;->b(Ljava/lang/String;)Lhny;

    .line 174
    iget-object v0, p0, Leit;->au:Llnh;

    const-class v2, Lhoc;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    .line 175
    invoke-virtual {p0}, Leit;->ay()V

    goto :goto_0

    .line 173
    :cond_2
    const-string v0, "fetch_older"

    goto :goto_1
.end method

.method protected ad_()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method protected af_()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Leit;->N:Ljava/lang/String;

    invoke-static {v0}, Llgc;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leit;->ag:Ljava/lang/String;

    .line 189
    return-void
.end method

.method protected ag_()V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1}, Lehh;->e(Landroid/os/Bundle;)V

    .line 55
    const-string v0, "query"

    iget-object v1, p0, Leit;->N:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v0, "delayed_query"

    iget-object v1, p0, Leit;->O:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 61
    iget-object v0, p0, Leit;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Leit;->O:Ljava/lang/String;

    iput-object v0, p0, Leit;->N:Ljava/lang/String;

    .line 63
    iput-object v3, p0, Leit;->O:Ljava/lang/String;

    .line 65
    iput-boolean v2, p0, Leit;->ai:Z

    invoke-virtual {p0}, Leit;->af_()V

    invoke-virtual {p0}, Leit;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    invoke-virtual {p0, v2}, Leit;->a(Z)V

    .line 67
    :cond_0
    invoke-super {p0}, Lehh;->g()V

    .line 68
    return-void
.end method
