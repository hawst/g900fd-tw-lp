.class final Ljdq;
.super Ljdi;
.source "PG"


# instance fields
.field private final h:Landroid/content/Context;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:J

.field private final l:J

.field private final m:Ljava/io/InputStream;

.field private final n:Z

.field private final o:Ljdu;

.field private p:Lorg/chromium/net/HttpUrlRequest;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfj;Ljava/lang/String;Ljava/lang/String;JJLjava/io/InputStream;ZLjdu;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p2}, Ljdi;-><init>(Lkfj;)V

    .line 61
    iput-object p1, p0, Ljdq;->h:Landroid/content/Context;

    .line 62
    iput-object p3, p0, Ljdq;->i:Ljava/lang/String;

    .line 63
    iput-object p4, p0, Ljdq;->j:Ljava/lang/String;

    .line 64
    iput-wide p5, p0, Ljdq;->k:J

    .line 65
    iput-wide p7, p0, Ljdq;->l:J

    .line 66
    iput-object p9, p0, Ljdq;->m:Ljava/io/InputStream;

    .line 67
    iput-boolean p10, p0, Ljdq;->n:Z

    .line 68
    iput-object p11, p0, Ljdq;->o:Ljdu;

    .line 69
    iput-object p12, p0, Ljdq;->q:Ljava/lang/String;

    .line 70
    return-void
.end method


# virtual methods
.method public a()V
    .locals 10

    .prologue
    .line 74
    new-instance v0, Ljava/util/HashMap;

    .line 75
    invoke-virtual {p0}, Ljdq;->j()Lkfj;

    move-result-object v1

    iget-object v2, p0, Ljdq;->i:Ljava/lang/String;

    invoke-interface {v1, v2}, Lkfj;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 77
    const-string v1, "Content-Range"

    iget-wide v2, p0, Ljdq;->k:J

    iget-wide v4, p0, Ljdq;->l:J

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    iget-wide v6, p0, Ljdq;->l:J

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x44

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "bytes "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v1, p0, Ljdq;->q:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 80
    const-string v1, "Set-Cookie"

    iget-object v2, p0, Ljdq;->q:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    :cond_0
    iget-object v1, p0, Ljdq;->h:Landroid/content/Context;

    iget-object v2, p0, Ljdq;->i:Ljava/lang/String;

    iget-boolean v3, p0, Ljdq;->n:Z

    const/4 v3, 0x3

    iget-object v4, p0, Ljdq;->g:Lorg/chromium/net/HttpUrlRequestListener;

    invoke-static {v1, v2, v3, v0, v4}, Ljgm;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v0

    iput-object v0, p0, Ljdq;->p:Lorg/chromium/net/HttpUrlRequest;

    .line 87
    iget-object v0, p0, Ljdq;->p:Lorg/chromium/net/HttpUrlRequest;

    iget-object v1, p0, Ljdq;->j:Ljava/lang/String;

    new-instance v2, Ljdr;

    iget-object v3, p0, Ljdq;->m:Ljava/io/InputStream;

    .line 88
    invoke-static {v3}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/InputStream;)Ljava/nio/channels/ReadableByteChannel;

    move-result-object v3

    iget-object v4, p0, Ljdq;->o:Ljdu;

    iget-wide v6, p0, Ljdq;->l:J

    iget-wide v8, p0, Ljdq;->k:J

    sub-long/2addr v6, v8

    invoke-direct {v2, v3, v4, v6, v7}, Ljdr;-><init>(Ljava/nio/channels/ReadableByteChannel;Ljdu;J)V

    iget-wide v4, p0, Ljdq;->l:J

    iget-wide v6, p0, Ljdq;->k:J

    sub-long/2addr v4, v6

    .line 87
    invoke-interface {v0, v1, v2, v4, v5}, Lorg/chromium/net/HttpUrlRequest;->a(Ljava/lang/String;Ljava/nio/channels/ReadableByteChannel;J)V

    .line 89
    iget-object v0, p0, Ljdq;->p:Lorg/chromium/net/HttpUrlRequest;

    const-string v1, "PUT"

    invoke-interface {v0, v1}, Lorg/chromium/net/HttpUrlRequest;->a(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method protected a(Lorg/chromium/net/HttpUrlRequest;)V
    .locals 0

    .prologue
    .line 104
    return-void
.end method

.method protected b()Lorg/chromium/net/HttpUrlRequest;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Ljdq;->p:Lorg/chromium/net/HttpUrlRequest;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Ljdq;->p:Lorg/chromium/net/HttpUrlRequest;

    invoke-interface {v0}, Lorg/chromium/net/HttpUrlRequest;->h()V

    .line 108
    return-void
.end method
