.class public abstract Leuh;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RQ:",
        "Loxu;",
        "RS:",
        "Loxu;",
        ">",
        "Lkgg",
        "<TRQ;TRS;>;"
    }
.end annotation


# instance fields
.field public final c:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "TRQ;TRS;)V"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Leuh;-><init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;ILjava/lang/String;Loxu;Loxu;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkfo;",
            "I",
            "Ljava/lang/String;",
            "TRQ;TRS;)V"
        }
    .end annotation

    .prologue
    .line 36
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 37
    iput p3, p0, Leuh;->c:I

    .line 38
    invoke-virtual {p0}, Leuh;->j()V

    .line 39
    return-void
.end method


# virtual methods
.method protected a_(Loxu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRS;)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Leuh;->b(Loxu;)V

    .line 57
    return-void
.end method

.method public abstract b(Loxu;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRS;)V"
        }
    .end annotation
.end method

.method protected j()V
    .locals 4

    .prologue
    .line 42
    iget v0, p0, Leuh;->c:I

    .line 43
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 44
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x3c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Cannot execute operation on an inactive account: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :cond_0
    return-void
.end method

.method public k()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 64
    iget v0, p0, Leuh;->c:I

    return v0
.end method
