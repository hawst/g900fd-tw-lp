.class public final Lfxa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/views/DreamViewFlipper;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/DreamViewFlipper;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa0

    .line 170
    iget-object v0, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 184
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->e(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    iget-object v2, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v2}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Lcom/google/android/libraries/social/media/ui/MediaView;)Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 176
    iget-object v1, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->b(Lcom/google/android/apps/plus/views/DreamViewFlipper;Lcom/google/android/libraries/social/media/ui/MediaView;)Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 178
    iget-object v0, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    iget-object v1, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->g(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/database/Cursor;

    move-result-object v1

    iget-object v2, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v2}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->f(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->a(Lcom/google/android/apps/plus/views/DreamViewFlipper;Landroid/database/Cursor;Lcom/google/android/libraries/social/media/ui/MediaView;)V

    .line 180
    iget-object v0, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->i(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->h(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 182
    :cond_1
    iget-object v0, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->i(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lfxa;->a:Lcom/google/android/apps/plus/views/DreamViewFlipper;

    invoke-static {v1}, Lcom/google/android/apps/plus/views/DreamViewFlipper;->j(Lcom/google/android/apps/plus/views/DreamViewFlipper;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
