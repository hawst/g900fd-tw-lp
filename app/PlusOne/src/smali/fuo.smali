.class public final Lfuo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Llip;

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Llip;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfuo;->b:Ljava/util/Set;

    .line 29
    iput-object p1, p0, Lfuo;->c:Landroid/view/View;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Llip;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lfuo;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 42
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 44
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v0, v2

    .line 75
    :goto_1
    return v0

    .line 46
    :pswitch_1
    iget-object v0, p0, Lfuo;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 47
    invoke-interface {v0, v3, v4, v2}, Llip;->a(III)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 48
    iput-object v0, p0, Lfuo;->a:Llip;

    .line 49
    iget-object v0, p0, Lfuo;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    move v0, v1

    .line 50
    goto :goto_1

    .line 57
    :pswitch_2
    iput-object v5, p0, Lfuo;->a:Llip;

    .line 58
    iget-object v0, p0, Lfuo;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llip;

    .line 59
    invoke-interface {v0, v3, v4, v1}, Llip;->a(III)Z

    goto :goto_2

    .line 61
    :cond_2
    iget-object v0, p0, Lfuo;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 66
    :pswitch_3
    iget-object v0, p0, Lfuo;->a:Llip;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lfuo;->a:Llip;

    const/4 v1, 0x3

    invoke-interface {v0, v3, v4, v1}, Llip;->a(III)Z

    .line 68
    iput-object v5, p0, Lfuo;->a:Llip;

    .line 69
    iget-object v0, p0, Lfuo;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 44
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public b(Llip;)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lfuo;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 38
    return-void
.end method
