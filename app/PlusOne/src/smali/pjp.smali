.class public final Lpjp;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lpjk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1279
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1284
    const/4 v0, 0x0

    iput-object v0, p0, Lpjp;->b:Lpjk;

    .line 1279
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1301
    const/4 v0, 0x0

    .line 1302
    iget-object v1, p0, Lpjp;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1303
    const/4 v0, 0x1

    iget-object v1, p0, Lpjp;->a:Ljava/lang/String;

    .line 1304
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1306
    :cond_0
    iget-object v1, p0, Lpjp;->b:Lpjk;

    if-eqz v1, :cond_1

    .line 1307
    const/4 v1, 0x2

    iget-object v2, p0, Lpjp;->b:Lpjk;

    .line 1308
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1310
    :cond_1
    iget-object v1, p0, Lpjp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1311
    iput v0, p0, Lpjp;->ai:I

    .line 1312
    return v0
.end method

.method public a(Loxn;)Lpjp;
    .locals 2

    .prologue
    .line 1320
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1321
    sparse-switch v0, :sswitch_data_0

    .line 1325
    iget-object v1, p0, Lpjp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1326
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpjp;->ah:Ljava/util/List;

    .line 1329
    :cond_1
    iget-object v1, p0, Lpjp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1331
    :sswitch_0
    return-object p0

    .line 1336
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpjp;->a:Ljava/lang/String;

    goto :goto_0

    .line 1340
    :sswitch_2
    iget-object v0, p0, Lpjp;->b:Lpjk;

    if-nez v0, :cond_2

    .line 1341
    new-instance v0, Lpjk;

    invoke-direct {v0}, Lpjk;-><init>()V

    iput-object v0, p0, Lpjp;->b:Lpjk;

    .line 1343
    :cond_2
    iget-object v0, p0, Lpjp;->b:Lpjk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1321
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1289
    iget-object v0, p0, Lpjp;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1290
    const/4 v0, 0x1

    iget-object v1, p0, Lpjp;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1292
    :cond_0
    iget-object v0, p0, Lpjp;->b:Lpjk;

    if-eqz v0, :cond_1

    .line 1293
    const/4 v0, 0x2

    iget-object v1, p0, Lpjp;->b:Lpjk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1295
    :cond_1
    iget-object v0, p0, Lpjp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1297
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1275
    invoke-virtual {p0, p1}, Lpjp;->a(Loxn;)Lpjp;

    move-result-object v0

    return-object v0
.end method
