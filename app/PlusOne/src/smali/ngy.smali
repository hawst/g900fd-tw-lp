.class public final Lngy;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnge;

.field public b:[Lnha;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 405
    const v0, 0x41644cb

    new-instance v1, Lngz;

    invoke-direct {v1}, Lngz;-><init>()V

    .line 410
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 409
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 406
    invoke-direct {p0}, Loxq;-><init>()V

    .line 509
    const/4 v0, 0x0

    iput-object v0, p0, Lngy;->a:Lnge;

    .line 512
    sget-object v0, Lnha;->a:[Lnha;

    iput-object v0, p0, Lngy;->b:[Lnha;

    .line 406
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 533
    .line 534
    iget-object v0, p0, Lngy;->a:Lnge;

    if-eqz v0, :cond_2

    .line 535
    const/4 v0, 0x1

    iget-object v2, p0, Lngy;->a:Lnge;

    .line 536
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 538
    :goto_0
    iget-object v2, p0, Lngy;->b:[Lnha;

    if-eqz v2, :cond_1

    .line 539
    iget-object v2, p0, Lngy;->b:[Lnha;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 540
    if-eqz v4, :cond_0

    .line 541
    const/4 v5, 0x2

    .line 542
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 539
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 546
    :cond_1
    iget-object v1, p0, Lngy;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 547
    iput v0, p0, Lngy;->ai:I

    .line 548
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lngy;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 556
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 557
    sparse-switch v0, :sswitch_data_0

    .line 561
    iget-object v2, p0, Lngy;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 562
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lngy;->ah:Ljava/util/List;

    .line 565
    :cond_1
    iget-object v2, p0, Lngy;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 567
    :sswitch_0
    return-object p0

    .line 572
    :sswitch_1
    iget-object v0, p0, Lngy;->a:Lnge;

    if-nez v0, :cond_2

    .line 573
    new-instance v0, Lnge;

    invoke-direct {v0}, Lnge;-><init>()V

    iput-object v0, p0, Lngy;->a:Lnge;

    .line 575
    :cond_2
    iget-object v0, p0, Lngy;->a:Lnge;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 579
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 580
    iget-object v0, p0, Lngy;->b:[Lnha;

    if-nez v0, :cond_4

    move v0, v1

    .line 581
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnha;

    .line 582
    iget-object v3, p0, Lngy;->b:[Lnha;

    if-eqz v3, :cond_3

    .line 583
    iget-object v3, p0, Lngy;->b:[Lnha;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 585
    :cond_3
    iput-object v2, p0, Lngy;->b:[Lnha;

    .line 586
    :goto_2
    iget-object v2, p0, Lngy;->b:[Lnha;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 587
    iget-object v2, p0, Lngy;->b:[Lnha;

    new-instance v3, Lnha;

    invoke-direct {v3}, Lnha;-><init>()V

    aput-object v3, v2, v0

    .line 588
    iget-object v2, p0, Lngy;->b:[Lnha;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 589
    invoke-virtual {p1}, Loxn;->a()I

    .line 586
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 580
    :cond_4
    iget-object v0, p0, Lngy;->b:[Lnha;

    array-length v0, v0

    goto :goto_1

    .line 592
    :cond_5
    iget-object v2, p0, Lngy;->b:[Lnha;

    new-instance v3, Lnha;

    invoke-direct {v3}, Lnha;-><init>()V

    aput-object v3, v2, v0

    .line 593
    iget-object v2, p0, Lngy;->b:[Lnha;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 557
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 517
    iget-object v0, p0, Lngy;->a:Lnge;

    if-eqz v0, :cond_0

    .line 518
    const/4 v0, 0x1

    iget-object v1, p0, Lngy;->a:Lnge;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 520
    :cond_0
    iget-object v0, p0, Lngy;->b:[Lnha;

    if-eqz v0, :cond_2

    .line 521
    iget-object v1, p0, Lngy;->b:[Lnha;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 522
    if-eqz v3, :cond_1

    .line 523
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 521
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 527
    :cond_2
    iget-object v0, p0, Lngy;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 529
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 402
    invoke-virtual {p0, p1}, Lngy;->a(Loxn;)Lngy;

    move-result-object v0

    return-object v0
.end method
