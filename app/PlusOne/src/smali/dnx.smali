.class public final Ldnx;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmlc;",
        "Lmld;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 6

    .prologue
    .line 27
    const-string v3, "viewphotostrash"

    new-instance v4, Lmlc;

    invoke-direct {v4}, Lmlc;-><init>()V

    new-instance v5, Lmld;

    invoke-direct {v5}, Lmld;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 29
    iput-object p3, p0, Ldnx;->a:Ljava/lang/String;

    .line 30
    iput-boolean p4, p0, Ldnx;->b:Z

    .line 31
    return-void
.end method


# virtual methods
.method protected a(Lmlc;)V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lnew;

    invoke-direct {v0}, Lnew;-><init>()V

    iput-object v0, p1, Lmlc;->a:Lnew;

    .line 36
    iget-object v0, p1, Lmlc;->a:Lnew;

    .line 37
    iget-object v1, p0, Ldnx;->a:Ljava/lang/String;

    iput-object v1, v0, Lnew;->a:Ljava/lang/String;

    .line 38
    return-void
.end method

.method protected a(Lmld;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 42
    iget-object v3, p1, Lmld;->a:Lnfx;

    .line 43
    const/4 v0, 0x7

    new-array v1, v5, [Ljava/lang/String;

    invoke-static {v0, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 45
    iget-object v0, p0, Ldnx;->f:Landroid/content/Context;

    iget v1, p0, Ldnx;->c:I

    iget-object v4, v3, Lnfx;->b:Ljava/lang/String;

    iget-boolean v6, p0, Ldnx;->b:Z

    invoke-static {v0, v1, v2, v4, v6}, Ljvj;->b(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 49
    iget-object v4, v3, Lnfx;->a:[Lnzx;

    if-eqz v4, :cond_1

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    aget-object v6, v4, v1

    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v6, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v6, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v0, v0, Lnzu;->b:Lnym;

    if-eqz v0, :cond_0

    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v6, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v7, v0, Lnzu;->b:Lnym;

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iput-object v8, v7, Lnym;->F:Ljava/lang/Boolean;

    sget-object v7, Lnzu;->a:Loxr;

    invoke-virtual {v6, v7, v0}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p0, Ldnx;->f:Landroid/content/Context;

    iget v1, p0, Ldnx;->c:I

    iget-object v3, v3, Lnfx;->a:[Lnzx;

    iget-boolean v4, p0, Ldnx;->b:Z

    const/4 v6, 0x0

    iget-boolean v7, p0, Ldnx;->b:Z

    invoke-static/range {v0 .. v7}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;[Lnzx;ZZLjava/lang/String;Z)V

    .line 53
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmlc;

    invoke-virtual {p0, p1}, Ldnx;->a(Lmlc;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmld;

    invoke-virtual {p0, p1}, Ldnx;->a(Lmld;)V

    return-void
.end method
