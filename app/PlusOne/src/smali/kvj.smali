.class public final Lkvj;
.super Lkxh;
.source "PG"


# static fields
.field private static c:[Ljava/lang/String;


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Z

.field private f:Lkve;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "qualified_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    sput-object v0, Lkvj;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lae;Lbb;ILjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 61
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lkvj;-><init>(Landroid/content/Context;Lae;Lbb;ILjava/lang/String;ZI)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lae;Lbb;ILjava/lang/String;ZI)V
    .locals 6

    .prologue
    .line 75
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p7

    invoke-direct/range {v0 .. v5}, Lkxh;-><init>(Landroid/content/Context;Lae;Lbb;II)V

    .line 77
    iput-object p5, p0, Lkvj;->d:Ljava/lang/String;

    .line 78
    iput-boolean p6, p0, Lkvj;->e:Z

    .line 79
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Exception;)I
    .locals 1

    .prologue
    .line 165
    const-string v0, "MEMBER_SEARCH_TOO_MANY_MATCHES"

    invoke-static {p1, v0}, Lkgf;->a(Ljava/lang/Exception;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    const/4 v0, 0x4

    .line 169
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 106
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 107
    const v1, 0x7f0401fa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/String;)Lkxk;
    .locals 8

    .prologue
    .line 100
    new-instance v0, Lkvk;

    iget-object v1, p0, Lkvj;->k:Landroid/content/Context;

    iget v2, p0, Lkvj;->a:I

    iget-object v3, p0, Lkvj;->b:Ljava/lang/String;

    iget-object v5, p0, Lkvj;->d:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lkvj;->b()[Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lkvj;->c()I

    move-result v7

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lkvk;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V

    return-object v0
.end method

.method protected a(Landroid/view/View;I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 130
    .line 133
    packed-switch p2, :pswitch_data_0

    .line 153
    :pswitch_0
    const v0, 0x7f0a023d

    move v1, v0

    move v0, v2

    .line 157
    :goto_0
    const v3, 0x7f100175

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v0, :cond_0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 159
    const v0, 0x7f100139

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 160
    invoke-virtual {p0}, Lkvj;->as()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    return-void

    .line 135
    :pswitch_1
    const/4 v1, 0x1

    .line 136
    const v0, 0x7f0a057b

    move v4, v0

    move v0, v1

    move v1, v4

    .line 137
    goto :goto_0

    .line 140
    :pswitch_2
    const v0, 0x7f0a0469

    move v1, v0

    move v0, v2

    .line 141
    goto :goto_0

    .line 144
    :pswitch_3
    const v0, 0x7f0a0236

    move v1, v0

    move v0, v2

    .line 145
    goto :goto_0

    .line 148
    :pswitch_4
    const v0, 0x7f0a0237

    move v1, v0

    move v0, v2

    .line 149
    goto :goto_0

    .line 157
    :cond_0
    const/16 v2, 0x8

    goto :goto_1

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 6

    .prologue
    .line 119
    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;

    .line 120
    const/4 v1, 0x1

    .line 121
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    .line 122
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    .line 123
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lhst;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    .line 124
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-boolean v5, p0, Lkvj;->e:Z

    .line 120
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;

    move-result-object v0

    iget-object v1, p0, Lkvj;->f:Lkve;

    .line 125
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;->a(Lkve;)Lcom/google/android/libraries/social/squares/members/SquareMemberListItemView;

    .line 126
    return-void
.end method

.method public a(Lkve;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lkvj;->f:Lkve;

    .line 86
    return-void
.end method

.method protected b(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 113
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 114
    const v1, 0x7f0401f9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lkvj;->c:[Ljava/lang/String;

    return-object v0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x2

    return v0
.end method
