.class public final Ldyg;
.super Lhye;
.source "PG"


# static fields
.field public static final b:Landroid/database/MatrixCursor;


# instance fields
.field private final c:I

.field private final d:[Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:I

.field private volatile g:Ldij;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    sput-object v0, Ldyg;->b:Landroid/database/MatrixCursor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 42
    iput p2, p0, Ldyg;->c:I

    .line 43
    iput-object p3, p0, Ldyg;->d:[Ljava/lang/String;

    .line 44
    iput-object p4, p0, Ldyg;->e:Ljava/lang/String;

    .line 45
    iput p5, p0, Ldyg;->f:I

    .line 46
    if-eqz p6, :cond_0

    const-string v0, "gaia_id IS NOT NULL"

    :goto_0
    invoke-virtual {p0, v0}, Ldyg;->a(Ljava/lang/String;)V

    .line 47
    iput-object p7, p0, Ldyg;->h:Ljava/lang/String;

    .line 48
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private D()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Ldyg;->g:Ldij;

    .line 256
    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {v0}, Ldij;->m()V

    .line 259
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldyg;->g:Ldij;

    .line 260
    return-void
.end method

.method private static a(Ljava/lang/Boolean;)I
    .locals 1

    .prologue
    .line 205
    invoke-static {p0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Llxo;)Llxk;
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Llxo;->c:[Llxk;

    .line 220
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 17

    .prologue
    .line 59
    new-instance v8, Lhym;

    move-object/from16 v0, p0

    iget-object v1, v0, Ldyg;->d:[Ljava/lang/String;

    invoke-direct {v8, v1}, Lhym;-><init>([Ljava/lang/String;)V

    .line 61
    move-object/from16 v0, p0

    iget-object v1, v0, Ldyg;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Ldyg;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Ldyg;->f:I

    if-ge v1, v2, :cond_1

    :cond_0
    move-object v1, v8

    .line 96
    :goto_0
    return-object v1

    .line 65
    :cond_1
    new-instance v1, Ldij;

    .line 66
    invoke-virtual/range {p0 .. p0}, Ldyg;->n()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Ldyg;->c:I

    move-object/from16 v0, p0

    iget-object v4, v0, Ldyg;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Ldyg;->h:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Ldij;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 67
    move-object/from16 v0, p0

    iput-object v1, v0, Ldyg;->g:Ldij;

    .line 69
    :try_start_0
    invoke-virtual {v1}, Ldij;->l()V

    .line 70
    invoke-virtual {v1}, Ldij;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 71
    sget-object v1, Ldyg;->b:Landroid/database/MatrixCursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Ldyg;->g:Ldij;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Ldyg;->g:Ldij;

    .line 77
    invoke-virtual {v1}, Ldij;->t()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 78
    const-string v2, "ACMergedPeople"

    invoke-virtual {v1, v2}, Ldij;->d(Ljava/lang/String;)V

    .line 79
    const/4 v1, 0x0

    goto :goto_0

    .line 74
    :catchall_0
    move-exception v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Ldyg;->g:Ldij;

    throw v1

    .line 82
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Ldyg;->d:[Ljava/lang/String;

    array-length v2, v2

    new-array v2, v2, [Ljava/lang/Object;

    .line 83
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Ldyg;->h:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 85
    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    .line 86
    invoke-virtual {v8, v2}, Lhym;->a([Ljava/lang/Object;)V

    .line 88
    invoke-virtual {v1}, Ldij;->a()[Llww;

    move-result-object v11

    .line 89
    const/4 v1, 0x0

    :goto_1
    array-length v2, v11

    if-ge v1, v2, :cond_27

    .line 90
    aget-object v12, v11, v1

    iget-object v4, v12, Llww;->b:Llxo;

    iget-object v2, v4, Llxo;->b:Llxq;

    if-nez v2, :cond_6

    const/4 v2, 0x0

    :goto_2
    iget-object v3, v4, Llxo;->f:[Llxa;

    if-eqz v3, :cond_7

    array-length v5, v3

    if-lez v5, :cond_7

    const/4 v5, 0x0

    aget-object v3, v3, v5

    iget-object v3, v3, Llxa;->b:Ljava/lang/String;

    :goto_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string v2, "ACMergedPeople"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x27

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "invalid response, no gaiaId nor email: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v2, 0x0

    .line 91
    :goto_4
    if-eqz v2, :cond_5

    .line 92
    invoke-virtual {v8, v2}, Lhym;->a([Ljava/lang/Object;)V

    .line 89
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 90
    :cond_6
    iget-object v2, v2, Llxq;->a:Ljava/lang/String;

    goto :goto_2

    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Ldyg;->d:[Ljava/lang/String;

    array-length v4, v4

    new-array v9, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    move v7, v4

    :goto_5
    array-length v4, v9

    if-ge v7, v4, :cond_26

    move-object/from16 v0, p0

    iget-object v4, v0, Ldyg;->d:[Ljava/lang/String;

    aget-object v4, v4, v7

    const-string v5, "_id"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_9

    const-string v5, "auto_complete_index"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    :cond_9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    :cond_a
    :goto_6
    aput-object v4, v9, v7

    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_5

    :cond_b
    iget-object v6, v12, Llww;->b:Llxo;

    const-string v5, "gaia_id"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    move-object v4, v2

    goto :goto_6

    :cond_c
    const-string v5, "person_id"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_e

    const-string v5, "g:"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_d

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_6

    :cond_d
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    :cond_e
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    const-string v5, "e:"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_f

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_6

    :cond_f
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    :cond_10
    const/4 v4, 0x0

    goto :goto_6

    :cond_11
    const-string v5, "name"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    invoke-static {v6}, Ldyg;->a(Llxo;)Llxk;

    move-result-object v4

    if-eqz v4, :cond_25

    iget-object v4, v4, Llxk;->c:Ljava/lang/String;

    goto :goto_6

    :cond_12
    const-string v5, "verified"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-static {v6}, Ldyg;->a(Llxo;)Llxk;

    move-result-object v4

    if-nez v4, :cond_13

    const/4 v4, 0x0

    :goto_7
    invoke-static {v4}, Ldyg;->a(Ljava/lang/Boolean;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_6

    :cond_13
    iget-object v5, v4, Llxk;->b:Llxp;

    if-nez v5, :cond_14

    const/4 v4, 0x0

    goto :goto_7

    :cond_14
    iget-object v4, v4, Llxk;->b:Llxp;

    iget-object v4, v4, Llxp;->a:Ljava/lang/Boolean;

    goto :goto_7

    :cond_15
    const-string v5, "profile_type"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    iget-object v4, v6, Llxo;->b:Llxq;

    if-nez v4, :cond_16

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/16 :goto_6

    :cond_16
    iget v5, v4, Llxq;->b:I

    packed-switch v5, :pswitch_data_0

    const-string v5, "ACMergedPeople"

    const/4 v10, 0x5

    invoke-static {v5, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_17

    iget v4, v4, Llxq;->b:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x20

    invoke-direct {v6, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "invalid objectType "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ": "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_17
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_0
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/16 :goto_6

    :pswitch_1
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/16 :goto_6

    :cond_18
    const-string v5, "avatar"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    iget-object v4, v6, Llxo;->d:[Llxs;

    if-eqz v4, :cond_19

    array-length v5, v4

    if-lez v5, :cond_19

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Llxs;->b:Ljava/lang/String;

    :goto_8
    invoke-static {v4}, Lhst;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_6

    :cond_19
    const/4 v4, 0x0

    goto :goto_8

    :cond_1a
    const-string v5, "snippet"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_22

    iget-object v4, v6, Llxo;->e:[Llya;

    if-eqz v4, :cond_1c

    array-length v5, v4

    if-lez v5, :cond_1c

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Llya;->b:Ljava/lang/String;

    :goto_9
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    iget-object v4, v6, Llxo;->g:[Llxn;

    if-eqz v4, :cond_1d

    array-length v5, v4

    if-lez v5, :cond_1d

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-object v4, v4, Llxn;->b:Ljava/lang/String;

    :goto_a
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1e

    const/4 v5, 0x1

    :goto_b
    iget-object v6, v6, Llxo;->h:[Llxm;

    if-eqz v6, :cond_1f

    array-length v10, v6

    if-lez v10, :cond_1f

    const/4 v10, 0x0

    aget-object v6, v6, v10

    iget-object v6, v6, Llxm;->b:Ljava/lang/String;

    :goto_c
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_20

    const/4 v10, 0x1

    :goto_d
    if-eqz v5, :cond_1b

    if-eqz v10, :cond_1b

    invoke-virtual/range {p0 .. p0}, Ldyg;->n()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0a0587

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v6, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    :cond_1b
    if-nez v5, :cond_a

    if-eqz v10, :cond_21

    move-object v4, v6

    goto/16 :goto_6

    :cond_1c
    const/4 v4, 0x0

    goto :goto_9

    :cond_1d
    const/4 v4, 0x0

    goto :goto_a

    :cond_1e
    const/4 v5, 0x0

    goto :goto_b

    :cond_1f
    const/4 v6, 0x0

    goto :goto_c

    :cond_20
    const/4 v10, 0x0

    goto :goto_d

    :cond_21
    const/4 v4, 0x0

    goto/16 :goto_6

    :cond_22
    const-string v5, "in_same_visibility_group"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_24

    iget-object v4, v6, Llxo;->b:Llxq;

    if-nez v4, :cond_23

    const/4 v4, 0x0

    :goto_e
    invoke-static {v4}, Ldyg;->a(Ljava/lang/Boolean;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto/16 :goto_6

    :cond_23
    iget-object v4, v4, Llxq;->c:Ljava/lang/Boolean;

    goto :goto_e

    :cond_24
    const-string v5, "auto_complete_suggestion"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_25

    iget-object v4, v12, Llww;->c:Ljava/lang/String;

    goto/16 :goto_6

    :cond_25
    const/4 v4, 0x0

    goto/16 :goto_6

    :cond_26
    move-object v2, v9

    goto/16 :goto_4

    :cond_27
    move-object v1, v8

    .line 96
    goto/16 :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 245
    invoke-direct {p0}, Ldyg;->D()V

    .line 246
    invoke-super {p0}, Lhye;->b()Z

    move-result v0

    return v0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Ldyg;->D()V

    .line 252
    return-void
.end method
