.class public final Lkfo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lkey;

.field private final d:Z

.field private final e:Lkfg;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 36
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkfo;-><init>(Landroid/content/Context;ILkey;ZLkfg;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILkey;)V
    .locals 6

    .prologue
    .line 40
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lkfo;-><init>(Landroid/content/Context;ILkey;ZLkfg;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILkey;ZLkfg;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 55
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 56
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lkfo;->a:Ljava/lang/String;

    .line 57
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkfo;->b:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lkfo;->c:Lkey;

    .line 59
    iput-boolean p4, p0, Lkfo;->d:Z

    .line 60
    iput-object p5, p0, Lkfo;->e:Lkfg;

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILkfp;)V
    .locals 2

    .prologue
    .line 44
    if-eqz p3, :cond_0

    iget-boolean v0, p3, Lkfp;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lkfp;->d()Lkfg;

    move-result-object v0

    :goto_1
    invoke-direct {p0, p1, p2, v1, v0}, Lkfo;-><init>(Landroid/content/Context;IZLkfg;)V

    .line 45
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;IZLkfg;)V
    .locals 6

    .prologue
    .line 49
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lkfo;-><init>(Landroid/content/Context;ILkey;ZLkfg;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 23
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lkey;ZLkfg;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lkey;ZLkfg;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lkfo;->a:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lkfo;->b:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lkfo;->c:Lkey;

    .line 31
    iput-boolean p4, p0, Lkfo;->d:Z

    .line 32
    iput-object p5, p0, Lkfo;->e:Lkfg;

    .line 33
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lkfo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lkfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Lkey;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lkfo;->c:Lkey;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lkfo;->d:Z

    return v0
.end method

.method public e()Lkfg;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lkfo;->e:Lkfg;

    return-object v0
.end method
