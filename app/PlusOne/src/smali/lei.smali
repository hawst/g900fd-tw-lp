.class public final Llei;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private c:Landroid/graphics/drawable/ColorDrawable;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    iput-object v0, p0, Llei;->c:Landroid/graphics/drawable/ColorDrawable;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 50
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Llei;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0, v0}, Llei;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->c(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iput-boolean v4, p0, Llei;->d:Z

    .line 52
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 53
    invoke-virtual {p0}, Llei;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 55
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-nez v0, :cond_2

    .line 56
    new-instance v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 57
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 59
    :cond_2
    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_5

    const v0, 0x7f0b01f3

    .line 61
    :goto_0
    iget-object v2, p0, Llei;->c:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 62
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v2, p0, Llei;->c:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 63
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v2, p0, Llei;->c:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(Landroid/graphics/drawable/Drawable;)V

    .line 64
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v2, p0, Llei;->c:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->c(Landroid/graphics/drawable/Drawable;)V

    .line 65
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 66
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v1, p2, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 68
    :cond_3
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0, v0}, Llei;->addView(Landroid/view/View;)V

    .line 70
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    if-nez v0, :cond_4

    .line 72
    const/16 v0, 0x24

    invoke-static {v1, v3, v4, v0}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Llei;->a:Landroid/widget/TextView;

    .line 74
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 75
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 76
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 77
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 78
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01e9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 80
    iget-object v1, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 81
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 83
    :cond_4
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Llei;->addView(Landroid/view/View;)V

    .line 89
    iput-boolean v5, p0, Llei;->d:Z

    .line 90
    return-void

    .line 59
    :cond_5
    const v0, 0x7f0b01f2

    goto/16 :goto_0

    .line 87
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "TopicId cannot be null or empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 121
    iget-boolean v0, p0, Llei;->d:Z

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Resources must be bound before layout."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    iget-object v1, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 128
    :cond_1
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v0, :cond_2

    .line 129
    iget-object v0, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v1, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 131
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 99
    iget-boolean v0, p0, Llei;->d:Z

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Resources must be bound before measure."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_3

    .line 105
    invoke-virtual {p0}, Llei;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 109
    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 110
    iget-object v2, p0, Llei;->a:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 111
    iget-object v2, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v1, v1}, Landroid/widget/TextView;->measure(II)V

    .line 113
    :cond_1
    iget-object v2, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v2, :cond_2

    .line 114
    iget-object v2, p0, Llei;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2, v1, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 116
    :cond_2
    invoke-virtual {p0, v0, v0}, Llei;->setMeasuredDimension(II)V

    .line 117
    return-void

    .line 107
    :cond_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Llei;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    return-void
.end method
