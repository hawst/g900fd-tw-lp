.class public final Ldlh;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmfe;",
        "Lmff;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ldli;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private r:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkfo;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 50
    const-string v3, "modifymemberships"

    new-instance v4, Lmfe;

    invoke-direct {v4}, Lmfe;-><init>()V

    new-instance v5, Lmff;

    invoke-direct {v5}, Lmff;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 52
    iput-object p3, p0, Ldlh;->a:Ljava/lang/String;

    .line 53
    iput-object p4, p0, Ldlh;->b:Ljava/lang/String;

    .line 55
    iput-object p5, p0, Ldlh;->p:Ljava/util/ArrayList;

    .line 56
    iput-object p6, p0, Ldlh;->q:Ljava/util/ArrayList;

    .line 57
    iput p7, p0, Ldlh;->r:I

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Ldlh;->c:Ljava/util/ArrayList;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkfo;",
            "Ljava/util/ArrayList",
            "<",
            "Ldli;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 75
    const-string v3, "modifymemberships"

    new-instance v4, Lmfe;

    invoke-direct {v4}, Lmfe;-><init>()V

    new-instance v5, Lmff;

    invoke-direct {v5}, Lmff;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 77
    iput-object p3, p0, Ldlh;->c:Ljava/util/ArrayList;

    .line 79
    iput-object p4, p0, Ldlh;->p:Ljava/util/ArrayList;

    .line 80
    iput-object p5, p0, Ldlh;->q:Ljava/util/ArrayList;

    .line 81
    iput p6, p0, Ldlh;->r:I

    .line 83
    iput-object v6, p0, Ldlh;->a:Ljava/lang/String;

    .line 84
    iput-object v6, p0, Ldlh;->b:Ljava/lang/String;

    .line 85
    return-void
.end method


# virtual methods
.method protected a(Lmfe;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 89
    new-instance v0, Lojq;

    invoke-direct {v0}, Lojq;-><init>()V

    iput-object v0, p1, Lmfe;->a:Lojq;

    .line 90
    iget-object v4, p1, Lmfe;->a:Lojq;

    .line 91
    new-instance v0, Lohu;

    invoke-direct {v0}, Lohu;-><init>()V

    iput-object v0, v4, Lojq;->a:Lohu;

    .line 92
    iget-object v0, v4, Lojq;->a:Lohu;

    iget v1, p0, Ldlh;->r:I

    iput v1, v0, Lohu;->b:I

    .line 93
    iget-object v5, v4, Lojq;->a:Lohu;

    iget-object v0, p0, Ldlh;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v1, Loht;

    invoke-direct {v1}, Loht;-><init>()V

    iget-object v0, p0, Ldlh;->a:Ljava/lang/String;

    invoke-static {v0}, Ldsm;->c(Ljava/lang/String;)Lohp;

    move-result-object v0

    iput-object v0, v1, Loht;->b:Lohp;

    iget-object v0, p0, Ldlh;->b:Ljava/lang/String;

    iput-object v0, v1, Loht;->c:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Loht;

    aput-object v1, v0, v2

    :goto_0
    iput-object v0, v5, Lohu;->a:[Loht;

    .line 95
    iget-object v0, p0, Ldlh;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldlh;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 96
    iget-object v0, p0, Ldlh;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lohn;

    iput-object v0, v4, Lojq;->b:[Lohn;

    move v1, v2

    .line 97
    :goto_1
    iget-object v0, p0, Ldlh;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 98
    iget-object v3, v4, Lojq;->b:[Lohn;

    iget-object v0, p0, Ldlh;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lhxe;->b(Ljava/lang/String;)Lohn;

    move-result-object v0

    aput-object v0, v3, v1

    .line 97
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 93
    :cond_0
    iget-object v0, p0, Ldlh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v1, v6, [Loht;

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_1

    iget-object v0, p0, Ldlh;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldli;

    new-instance v7, Loht;

    invoke-direct {v7}, Loht;-><init>()V

    iget-object v8, v0, Ldli;->a:Ljava/lang/String;

    invoke-static {v8}, Ldsm;->c(Ljava/lang/String;)Lohp;

    move-result-object v8

    iput-object v8, v7, Loht;->b:Lohp;

    iget-object v0, v0, Ldli;->b:Ljava/lang/String;

    iput-object v0, v7, Loht;->c:Ljava/lang/String;

    aput-object v7, v1, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 102
    :cond_2
    iget-object v0, p0, Ldlh;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldlh;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 103
    iget-object v0, p0, Ldlh;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lohn;

    iput-object v0, v4, Lojq;->c:[Lohn;

    .line 104
    :goto_3
    iget-object v0, p0, Ldlh;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 105
    iget-object v1, v4, Lojq;->c:[Lohn;

    iget-object v0, p0, Ldlh;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lhxe;->b(Ljava/lang/String;)Lohn;

    move-result-object v0

    aput-object v0, v1, v2

    .line 104
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 108
    :cond_3
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmfe;

    invoke-virtual {p0, p1}, Ldlh;->a(Lmfe;)V

    return-void
.end method
