.class public final Lmwa;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmwa;


# instance fields
.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12173
    const/4 v0, 0x0

    new-array v0, v0, [Lmwa;

    sput-object v0, Lmwa;->a:[Lmwa;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12174
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 12200
    const/4 v0, 0x0

    .line 12201
    iget-object v1, p0, Lmwa;->b:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 12202
    const/4 v0, 0x1

    iget-object v1, p0, Lmwa;->b:Ljava/lang/Long;

    .line 12203
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 12205
    :cond_0
    iget-object v1, p0, Lmwa;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 12206
    const/4 v1, 0x2

    iget-object v2, p0, Lmwa;->c:Ljava/lang/Boolean;

    .line 12207
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12209
    :cond_1
    iget-object v1, p0, Lmwa;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 12210
    const/4 v1, 0x3

    iget-object v2, p0, Lmwa;->d:Ljava/lang/Boolean;

    .line 12211
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12213
    :cond_2
    iget-object v1, p0, Lmwa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12214
    iput v0, p0, Lmwa;->ai:I

    .line 12215
    return v0
.end method

.method public a(Loxn;)Lmwa;
    .locals 2

    .prologue
    .line 12223
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 12224
    sparse-switch v0, :sswitch_data_0

    .line 12228
    iget-object v1, p0, Lmwa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 12229
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmwa;->ah:Ljava/util/List;

    .line 12232
    :cond_1
    iget-object v1, p0, Lmwa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 12234
    :sswitch_0
    return-object p0

    .line 12239
    :sswitch_1
    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lmwa;->b:Ljava/lang/Long;

    goto :goto_0

    .line 12243
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwa;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 12247
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lmwa;->d:Ljava/lang/Boolean;

    goto :goto_0

    .line 12224
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 12185
    iget-object v0, p0, Lmwa;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 12186
    const/4 v0, 0x1

    iget-object v1, p0, Lmwa;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->c(IJ)V

    .line 12188
    :cond_0
    iget-object v0, p0, Lmwa;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 12189
    const/4 v0, 0x2

    iget-object v1, p0, Lmwa;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12191
    :cond_1
    iget-object v0, p0, Lmwa;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 12192
    const/4 v0, 0x3

    iget-object v1, p0, Lmwa;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 12194
    :cond_2
    iget-object v0, p0, Lmwa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 12196
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 12170
    invoke-virtual {p0, p1}, Lmwa;->a(Loxn;)Lmwa;

    move-result-object v0

    return-object v0
.end method
