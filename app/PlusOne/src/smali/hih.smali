.class public final Lhih;
.super Landroid/widget/LinearLayout;
.source "PG"


# static fields
.field private static a:I

.field private static b:I

.field private static c:I


# instance fields
.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/TextView;

.field private final f:I

.field private final g:I

.field private final h:I

.field private i:Z

.field private j:Z

.field private k:Landroid/widget/ImageView;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, -0x1

    sput v0, Lhih;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lhii;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    .line 48
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 43
    iput-boolean v1, p0, Lhih;->j:Z

    .line 49
    invoke-virtual {p0}, Lhih;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 50
    sget v3, Lhih;->a:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 51
    const v3, 0x7f0d022a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lhih;->a:I

    .line 52
    const v3, 0x7f0d0228

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lhih;->b:I

    .line 53
    const v3, 0x7f0d0229

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lhih;->c:I

    .line 55
    :cond_0
    invoke-virtual {p0, v2}, Lhih;->setOrientation(I)V

    .line 57
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhih;->e:Landroid/widget/TextView;

    .line 58
    iget-object v0, p0, Lhih;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 59
    iget-object v0, p0, Lhih;->e:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 61
    invoke-interface {p2}, Lhii;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhih;->d:Landroid/view/View;

    .line 62
    iget-object v0, p0, Lhih;->d:Landroid/view/View;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lhih;->l:Z

    .line 63
    invoke-interface {p2}, Lhii;->c()I

    move-result v0

    iput v0, p0, Lhih;->f:I

    .line 64
    invoke-interface {p2}, Lhii;->e()I

    move-result v0

    iput v0, p0, Lhih;->g:I

    .line 65
    invoke-interface {p2}, Lhii;->f()Z

    move-result v0

    iput-boolean v0, p0, Lhih;->j:Z

    .line 66
    invoke-interface {p2}, Lhii;->d()I

    move-result v0

    iput v0, p0, Lhih;->h:I

    .line 68
    iget-boolean v0, p0, Lhih;->l:Z

    if-eqz v0, :cond_1

    .line 69
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    sget v3, Lhih;->a:I

    sget v4, Lhih;->a:I

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 70
    iget-object v3, p0, Lhih;->d:Landroid/view/View;

    invoke-virtual {p0, v3, v0}, Lhih;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    :cond_1
    iget-object v0, p0, Lhih;->e:Landroid/widget/TextView;

    invoke-interface {p2}, Lhii;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {p0}, Lhih;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lhih;->e:Landroid/widget/TextView;

    iget v4, p0, Lhih;->h:I

    invoke-static {v0, v3, v4}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 75
    iget-object v0, p0, Lhih;->e:Landroid/widget/TextView;

    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    .line 76
    invoke-direct {p0}, Lhih;->d()V

    .line 77
    iget-object v0, p0, Lhih;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lhih;->addView(Landroid/view/View;)V

    .line 79
    iget v0, p0, Lhih;->g:I

    if-ne v0, v1, :cond_3

    .line 80
    const v0, 0x7f020563

    invoke-virtual {p0, v0}, Lhih;->setBackgroundResource(I)V

    .line 86
    :goto_1
    invoke-virtual {p0}, Lhih;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    iget v3, p0, Lhih;->f:I

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 87
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    const-wide/16 v4, 0x96

    invoke-virtual {v0, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(J)V

    invoke-virtual {v0, v2, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    invoke-virtual {v0, v1, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    invoke-virtual {v0, v8, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6, v7}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    invoke-virtual {p0, v0}, Lhih;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 88
    return-void

    :cond_2
    move v0, v2

    .line 62
    goto/16 :goto_0

    .line 81
    :cond_3
    iget v0, p0, Lhih;->g:I

    if-ne v0, v8, :cond_4

    .line 82
    const v0, 0x7f020599

    invoke-virtual {p0, v0}, Lhih;->setBackgroundResource(I)V

    goto :goto_1

    .line 84
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "drawableItem.getShape() returned an invalid value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v0, -0x2

    .line 140
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 142
    const/16 v0, 0x10

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 143
    iget-boolean v0, p0, Lhih;->l:Z

    if-eqz v0, :cond_1

    sget v0, Lhih;->c:I

    div-int/lit8 v0, v0, 0x2

    .line 144
    :goto_0
    sget v2, Lhih;->b:I

    sget v3, Lhih;->c:I

    sget v4, Lhih;->b:I

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 145
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 146
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 147
    sget v0, Lhih;->c:I

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    .line 149
    :cond_0
    iget-object v0, p0, Lhih;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    return-void

    .line 143
    :cond_1
    sget v0, Lhih;->c:I

    goto :goto_0
.end method

.method private e()Landroid/view/View;
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lhih;->k:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lhih;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhih;->k:Landroid/widget/ImageView;

    .line 155
    iget-object v0, p0, Lhih;->k:Landroid/widget/ImageView;

    const v1, 0x7f0204e5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 156
    iget-object v0, p0, Lhih;->k:Landroid/widget/ImageView;

    iget-object v1, p0, Lhih;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 157
    iget-object v0, p0, Lhih;->k:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 158
    invoke-virtual {p0}, Lhih;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0232

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 159
    iget-object v1, p0, Lhih;->k:Landroid/widget/ImageView;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 160
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    sget v1, Lhih;->a:I

    sget v2, Lhih;->a:I

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 161
    iget-object v1, p0, Lhih;->k:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    :cond_0
    iget-object v0, p0, Lhih;->k:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lhih;->i:Z

    return v0
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v1, -0x2

    const/4 v4, 0x0

    .line 102
    iget-boolean v0, p0, Lhih;->j:Z

    if-nez v0, :cond_0

    .line 111
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-direct {p0}, Lhih;->e()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lhih;->addView(Landroid/view/View;I)V

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhih;->i:Z

    .line 107
    iget-boolean v0, p0, Lhih;->l:Z

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lhih;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lhih;->removeView(Landroid/view/View;)V

    .line 110
    :cond_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x10

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    sget v1, Lhih;->b:I

    sget v2, Lhih;->c:I

    sget v3, Lhih;->b:I

    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_2

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    sget v1, Lhih;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    :cond_2
    iget-object v1, p0, Lhih;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 118
    iput-boolean v1, p0, Lhih;->i:Z

    .line 119
    invoke-direct {p0}, Lhih;->e()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhih;->removeView(Landroid/view/View;)V

    .line 120
    iget-boolean v0, p0, Lhih;->l:Z

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lhih;->d:Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lhih;->addView(Landroid/view/View;I)V

    .line 123
    :cond_0
    invoke-direct {p0}, Lhih;->d()V

    .line 124
    return-void
.end method
