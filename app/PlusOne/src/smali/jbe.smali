.class final Ljbe;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[C

.field private static final b:[C

.field private static final c:[C

.field private static final d:[C

.field private static final e:[C

.field private static final f:[C

.field private static final g:[C

.field private static final h:[C

.field private static final i:[C

.field private static final j:[C

.field private static final k:[C

.field private static final l:[C

.field private static final m:[C

.field private static final n:[C

.field private static final o:[C

.field private static final p:[[C


# instance fields
.field private A:Z

.field private B:I

.field private C:I

.field private D:I

.field private E:[I

.field private F:[I

.field private q:[C

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 267
    const-string v0, "http://"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->a:[C

    .line 268
    const-string v0, "https://"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->b:[C

    .line 269
    const-string v0, "lh"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->c:[C

    .line 270
    const-string v0, "sp"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->d:[C

    .line 271
    const-string v0, "bp"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->e:[C

    .line 272
    const-string v0, ".googleusercontent.com/"

    .line 273
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->f:[C

    .line 274
    const-string v0, "www.google.com/visualsearch/lh/"

    .line 275
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->g:[C

    .line 276
    const-string v0, ".google.com/"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->h:[C

    .line 277
    const-string v0, ".blogger.com/"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->i:[C

    .line 278
    const-string v0, ".ggpht.com/"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->j:[C

    .line 279
    const-string v0, "public"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->k:[C

    .line 280
    const-string v0, "proxy"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->l:[C

    .line 281
    const-string v0, "image"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->m:[C

    .line 282
    const-string v0, "%3D"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->n:[C

    .line 283
    const-string v0, "%3d"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Ljbe;->o:[C

    .line 284
    const/4 v0, 0x5

    new-array v0, v0, [[C

    new-array v1, v5, [C

    const/16 v2, 0x4f

    aput-char v2, v1, v4

    aput-object v1, v0, v4

    new-array v1, v5, [C

    const/16 v2, 0x4a

    aput-char v2, v1, v4

    aput-object v1, v0, v5

    new-array v1, v3, [C

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    const/4 v1, 0x3

    new-array v2, v5, [C

    const/16 v3, 0x55

    aput-char v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v5, [C

    const/16 v3, 0x49

    aput-char v3, v2, v4

    aput-object v2, v0, v1

    sput-object v0, Ljbe;->p:[[C

    return-void

    :array_0
    .array-data 2
        0x55s
        0x74s
    .end array-data
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    const/16 v0, 0x7d0

    new-array v0, v0, [C

    iput-object v0, p0, Ljbe;->q:[C

    .line 300
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljbe;->A:Z

    .line 304
    new-array v0, v1, [I

    iput-object v0, p0, Ljbe;->E:[I

    .line 305
    new-array v0, v1, [I

    iput-object v0, p0, Ljbe;->F:[I

    return-void
.end method

.method private static a(F)Ljava/lang/String;
    .locals 3

    .prologue
    .line 453
    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_1

    .line 454
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cropping requires values between 0 and 1"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_1
    const v0, 0x477fff00    # 65535.0f

    mul-float/2addr v0, p0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    .line 457
    const-string v1, "0000"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 458
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 457
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/StringBuilder;)V
    .locals 12

    .prologue
    const/16 v11, 0x2d

    const/4 v1, 0x0

    .line 462
    iget v5, p0, Ljbe;->u:I

    .line 463
    iget v0, p0, Ljbe;->u:I

    iget v2, p0, Ljbe;->B:I

    add-int v7, v0, v2

    .line 464
    :goto_0
    if-ge v5, v7, :cond_5

    move v0, v1

    .line 465
    :goto_1
    sget-object v2, Ljbe;->p:[[C

    const/4 v2, 0x5

    if-ge v0, v2, :cond_2

    .line 466
    const/4 v6, 0x1

    .line 468
    sget-object v2, Ljbe;->p:[[C

    aget-object v8, v2, v0

    move v2, v1

    move v4, v5

    .line 470
    :goto_2
    array-length v3, v8

    if-ge v2, v3, :cond_7

    if-ge v4, v7, :cond_7

    .line 471
    add-int/lit8 v3, v2, 0x1

    aget-char v9, v8, v2

    iget-object v10, p0, Ljbe;->q:[C

    add-int/lit8 v2, v4, 0x1

    aget-char v4, v10, v4

    if-eq v9, v4, :cond_6

    move v4, v2

    move v2, v1

    .line 473
    :goto_3
    if-eqz v2, :cond_1

    array-length v6, v8

    if-ne v3, v6, :cond_0

    if-eq v4, v7, :cond_1

    iget-object v3, p0, Ljbe;->q:[C

    aget-char v3, v3, v4

    if-eq v3, v11, :cond_1

    :cond_0
    move v2, v1

    .line 480
    :cond_1
    if-eqz v2, :cond_3

    .line 481
    sget-object v2, Ljbe;->p:[[C

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 485
    :cond_2
    :goto_4
    if-ge v5, v7, :cond_4

    iget-object v0, p0, Ljbe;->q:[C

    aget-char v0, v0, v5

    if-eq v0, v11, :cond_4

    .line 486
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 465
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 488
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 490
    :cond_5
    return-void

    :cond_6
    move v4, v2

    move v2, v3

    goto :goto_2

    :cond_7
    move v3, v2

    move v2, v6

    goto :goto_3
.end method

.method private a(I[C)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 759
    array-length v2, p2

    .line 760
    add-int v1, p1, v2

    iget v3, p0, Ljbe;->r:I

    if-le v1, v3, :cond_1

    .line 768
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 763
    :goto_1
    if-ge v1, v2, :cond_2

    .line 764
    iget-object v3, p0, Ljbe;->q:[C

    add-int v4, p1, v1

    aget-char v3, v3, v4

    aget-char v4, p2, v1

    if-ne v3, v4, :cond_0

    .line 763
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 768
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a([C)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 739
    iget v1, p0, Ljbe;->s:I

    .line 740
    array-length v4, p1

    .line 741
    add-int v2, v1, v4

    iget v3, p0, Ljbe;->r:I

    if-le v2, v3, :cond_1

    .line 752
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v2, v1

    move v1, v0

    .line 745
    :goto_1
    if-ge v1, v4, :cond_2

    .line 746
    iget-object v5, p0, Ljbe;->q:[C

    add-int/lit8 v3, v2, 0x1

    aget-char v5, v5, v2

    add-int/lit8 v2, v1, 0x1

    aget-char v1, p1, v1

    if-ne v5, v1, :cond_0

    move v1, v2

    move v2, v3

    goto :goto_1

    .line 751
    :cond_2
    iput v2, p0, Ljbe;->s:I

    .line 752
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(I[C)I
    .locals 6

    .prologue
    .line 781
    array-length v2, p2

    .line 782
    iget v0, p0, Ljbe;->r:I

    sub-int v3, v0, v2

    move v0, p1

    .line 784
    :goto_0
    if-gt v0, v3, :cond_1

    .line 785
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 786
    iget-object v4, p0, Ljbe;->q:[C

    add-int v5, v0, v1

    aget-char v4, v4, v5

    aget-char v5, p2, v1

    if-ne v4, v5, :cond_0

    .line 787
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 784
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 792
    :cond_1
    const/4 v0, -0x1

    :cond_2
    return v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x5

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 511
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, p0, Ljbe;->r:I

    .line 512
    iget v0, p0, Ljbe;->r:I

    const/16 v3, 0x7d0

    if-lt v0, v3, :cond_0

    .line 513
    iput-boolean v1, p0, Ljbe;->A:Z

    .line 567
    :goto_0
    return-void

    .line 517
    :cond_0
    iget v0, p0, Ljbe;->r:I

    iget-object v3, p0, Ljbe;->q:[C

    invoke-virtual {p1, v1, v0, v3, v1}, Ljava/lang/String;->getChars(II[CI)V

    .line 518
    iput v1, p0, Ljbe;->s:I

    .line 519
    iput-boolean v1, p0, Ljbe;->x:Z

    .line 520
    iput-boolean v1, p0, Ljbe;->y:Z

    .line 521
    iput-boolean v1, p0, Ljbe;->z:Z

    .line 523
    sget-object v0, Ljbe;->a:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Ljbe;->b:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Ljbe;->w:Z

    .line 524
    iget-boolean v0, p0, Ljbe;->w:Z

    if-nez v0, :cond_f

    .line 525
    iput-boolean v1, p0, Ljbe;->A:Z

    goto :goto_0

    .line 523
    :cond_1
    iget v0, p0, Ljbe;->s:I

    iput v0, p0, Ljbe;->t:I

    sget-object v0, Ljbe;->c:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-eqz v0, :cond_a

    iput-boolean v2, p0, Ljbe;->x:Z

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x33

    if-lt v0, v3, :cond_9

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x36

    if-gt v0, v3, :cond_9

    iget v0, p0, Ljbe;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbe;->s:I

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x2d

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    add-int/lit8 v3, v3, 0x1

    aget-char v0, v0, v3

    const/16 v3, 0x64

    if-ne v0, v3, :cond_5

    iget v0, p0, Ljbe;->s:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Ljbe;->s:I

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x61

    if-lt v0, v3, :cond_2

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x67

    if-le v0, v3, :cond_3

    :cond_2
    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x7a

    if-ne v0, v3, :cond_4

    :cond_3
    iget v0, p0, Ljbe;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbe;->s:I

    sget-object v0, Ljbe;->f:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x2d

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    add-int/lit8 v3, v3, 0x1

    aget-char v0, v0, v3

    const/16 v3, 0x74

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    add-int/lit8 v3, v3, 0x2

    aget-char v0, v0, v3

    const/16 v3, 0x74

    if-ne v0, v3, :cond_6

    iget v0, p0, Ljbe;->s:I

    add-int/lit8 v0, v0, 0x3

    iput v0, p0, Ljbe;->s:I

    sget-object v0, Ljbe;->f:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto/16 :goto_1

    :cond_6
    sget-object v0, Ljbe;->h:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-eqz v0, :cond_8

    iput-boolean v2, p0, Ljbe;->y:Z

    :cond_7
    iget v0, p0, Ljbe;->s:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ljbe;->C:I

    move v0, v2

    goto/16 :goto_1

    :cond_8
    sget-object v0, Ljbe;->f:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Ljbe;->j:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto/16 :goto_1

    :cond_9
    move v0, v1

    goto/16 :goto_1

    :cond_a
    sget-object v0, Ljbe;->d:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-eqz v0, :cond_c

    iput-boolean v2, p0, Ljbe;->x:Z

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x31

    if-lt v0, v3, :cond_b

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x33

    if-gt v0, v3, :cond_b

    iget v0, p0, Ljbe;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbe;->s:I

    sget-object v0, Ljbe;->f:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Ljbe;->j:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto/16 :goto_1

    :cond_b
    move v0, v1

    goto/16 :goto_1

    :cond_c
    iput-boolean v1, p0, Ljbe;->x:Z

    sget-object v0, Ljbe;->e:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x30

    if-lt v0, v3, :cond_d

    iget-object v0, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->s:I

    aget-char v0, v0, v3

    const/16 v3, 0x33

    if-gt v0, v3, :cond_d

    iget v0, p0, Ljbe;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljbe;->s:I

    sget-object v0, Ljbe;->i:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto/16 :goto_1

    :cond_d
    move v0, v1

    goto/16 :goto_1

    :cond_e
    sget-object v0, Ljbe;->g:[C

    invoke-direct {p0, v0}, Ljbe;->a([C)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto/16 :goto_1

    .line 530
    :cond_f
    iget v3, p0, Ljbe;->s:I

    .line 531
    iget v0, p0, Ljbe;->s:I

    .line 532
    iput v1, p0, Ljbe;->D:I

    .line 534
    :goto_2
    iget v4, p0, Ljbe;->r:I

    if-eq v3, v4, :cond_10

    iget-object v4, p0, Ljbe;->q:[C

    aget-char v4, v4, v3

    const/16 v6, 0x2f

    if-ne v4, v6, :cond_11

    .line 535
    :cond_10
    if-ne v3, v0, :cond_12

    iget v4, p0, Ljbe;->r:I

    if-eq v3, v4, :cond_12

    .line 537
    add-int/lit8 v0, v3, 0x1

    .line 551
    :cond_11
    :goto_3
    iget v4, p0, Ljbe;->r:I

    if-eq v3, v4, :cond_14

    .line 552
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 539
    :cond_12
    iget v4, p0, Ljbe;->D:I

    const/16 v6, 0x8

    if-lt v4, v6, :cond_13

    .line 540
    iput-boolean v1, p0, Ljbe;->A:Z

    goto/16 :goto_0

    .line 544
    :cond_13
    iget-object v4, p0, Ljbe;->E:[I

    iget v6, p0, Ljbe;->D:I

    aput v0, v4, v6

    .line 545
    iget-object v4, p0, Ljbe;->F:[I

    iget v6, p0, Ljbe;->D:I

    sub-int v0, v3, v0

    aput v0, v4, v6

    .line 546
    add-int/lit8 v0, v3, 0x1

    .line 547
    iget v4, p0, Ljbe;->D:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Ljbe;->D:I

    goto :goto_3

    .line 558
    :cond_14
    iget v0, p0, Ljbe;->D:I

    if-le v0, v2, :cond_26

    iget-object v0, p0, Ljbe;->F:[I

    aget v0, v0, v1

    sget-object v3, Ljbe;->m:[C

    array-length v3, v3

    if-ne v0, v3, :cond_26

    iget-object v0, p0, Ljbe;->E:[I

    aget v0, v0, v1

    sget-object v3, Ljbe;->m:[C

    invoke-direct {p0, v0, v3}, Ljbe;->a(I[C)Z

    move-result v0

    if-eqz v0, :cond_26

    move v0, v2

    :goto_4
    if-nez v0, :cond_15

    iget v3, p0, Ljbe;->D:I

    if-ne v3, v7, :cond_15

    iget-object v0, p0, Ljbe;->E:[I

    aget v0, v0, v8

    iput v0, p0, Ljbe;->u:I

    iput v1, p0, Ljbe;->B:I

    :goto_5
    move v0, v2

    :goto_6
    if-eqz v0, :cond_19

    .line 559
    iput-boolean v2, p0, Ljbe;->z:Z

    .line 560
    iput-boolean v2, p0, Ljbe;->A:Z

    goto/16 :goto_0

    .line 558
    :cond_15
    if-eqz v0, :cond_16

    iget v3, p0, Ljbe;->D:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_16

    iget-object v0, p0, Ljbe;->E:[I

    aget v0, v0, v7

    iput v0, p0, Ljbe;->u:I

    iput v1, p0, Ljbe;->B:I

    goto :goto_5

    :cond_16
    if-nez v0, :cond_17

    iget v3, p0, Ljbe;->D:I

    const/4 v4, 0x6

    if-ne v3, v4, :cond_17

    iget-object v0, p0, Ljbe;->E:[I

    aget v0, v0, v8

    iput v0, p0, Ljbe;->u:I

    iget-object v0, p0, Ljbe;->F:[I

    aget v0, v0, v8

    iput v0, p0, Ljbe;->B:I

    goto :goto_5

    :cond_17
    if-eqz v0, :cond_18

    iget v0, p0, Ljbe;->D:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_18

    iget-object v0, p0, Ljbe;->E:[I

    aget v0, v0, v7

    iput v0, p0, Ljbe;->u:I

    iget-object v0, p0, Ljbe;->F:[I

    aget v0, v0, v7

    iput v0, p0, Ljbe;->B:I

    goto :goto_5

    :cond_18
    move v0, v1

    goto :goto_6

    .line 561
    :cond_19
    iget v0, p0, Ljbe;->D:I

    if-le v0, v2, :cond_25

    iget-object v0, p0, Ljbe;->F:[I

    aget v0, v0, v1

    sget-object v3, Ljbe;->k:[C

    array-length v3, v3

    if-ne v0, v3, :cond_1a

    iget-object v0, p0, Ljbe;->E:[I

    aget v0, v0, v1

    sget-object v3, Ljbe;->k:[C

    invoke-direct {p0, v0, v3}, Ljbe;->a(I[C)Z

    move-result v0

    if-nez v0, :cond_1b

    :cond_1a
    iget-object v0, p0, Ljbe;->F:[I

    aget v0, v0, v1

    sget-object v3, Ljbe;->l:[C

    array-length v3, v3

    if-ne v0, v3, :cond_25

    iget-object v0, p0, Ljbe;->E:[I

    aget v0, v0, v1

    sget-object v3, Ljbe;->l:[C

    invoke-direct {p0, v0, v3}, Ljbe;->a(I[C)Z

    move-result v0

    if-eqz v0, :cond_25

    :cond_1b
    move v0, v2

    :goto_7
    if-nez v0, :cond_1d

    iget v3, p0, Ljbe;->D:I

    if-ne v3, v2, :cond_1d

    iget-object v0, p0, Ljbe;->E:[I

    aget v3, v0, v1

    iget-object v0, p0, Ljbe;->F:[I

    aget v0, v0, v1

    :goto_8
    move v4, v3

    :goto_9
    iget v6, p0, Ljbe;->r:I

    if-ge v4, v6, :cond_20

    iget-object v6, p0, Ljbe;->q:[C

    aget-char v6, v6, v4

    const/16 v7, 0x3d

    if-ne v6, v7, :cond_1f

    :goto_a
    iput v4, p0, Ljbe;->v:I

    iget v4, p0, Ljbe;->v:I

    if-eq v4, v5, :cond_21

    iget v4, p0, Ljbe;->v:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Ljbe;->u:I

    :cond_1c
    :goto_b
    iget v4, p0, Ljbe;->v:I

    if-eq v4, v5, :cond_23

    iget v4, p0, Ljbe;->u:I

    sub-int v3, v4, v3

    sub-int/2addr v0, v3

    iput v0, p0, Ljbe;->B:I

    :goto_c
    move v0, v2

    :goto_d
    if-eqz v0, :cond_24

    .line 562
    iput-boolean v1, p0, Ljbe;->z:Z

    .line 563
    iput-boolean v2, p0, Ljbe;->A:Z

    goto/16 :goto_0

    .line 561
    :cond_1d
    if-eqz v0, :cond_1e

    iget v0, p0, Ljbe;->D:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1e

    iget-object v0, p0, Ljbe;->E:[I

    aget v3, v0, v2

    iget-object v0, p0, Ljbe;->F:[I

    aget v0, v0, v2

    goto :goto_8

    :cond_1e
    move v0, v1

    goto :goto_d

    :cond_1f
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    :cond_20
    move v4, v5

    goto :goto_a

    :cond_21
    sget-object v4, Ljbe;->n:[C

    invoke-direct {p0, v3, v4}, Ljbe;->b(I[C)I

    move-result v4

    iput v4, p0, Ljbe;->v:I

    iget v4, p0, Ljbe;->v:I

    if-ne v4, v5, :cond_22

    sget-object v4, Ljbe;->o:[C

    invoke-direct {p0, v3, v4}, Ljbe;->b(I[C)I

    move-result v4

    iput v4, p0, Ljbe;->v:I

    :cond_22
    iget v4, p0, Ljbe;->v:I

    if-eq v4, v5, :cond_1c

    iget v4, p0, Ljbe;->v:I

    add-int/lit8 v4, v4, 0x3

    iput v4, p0, Ljbe;->u:I

    goto :goto_b

    :cond_23
    add-int/2addr v0, v3

    iput v0, p0, Ljbe;->v:I

    iget v0, p0, Ljbe;->v:I

    iput v0, p0, Ljbe;->u:I

    iput v1, p0, Ljbe;->B:I

    goto :goto_c

    .line 565
    :cond_24
    iput-boolean v1, p0, Ljbe;->A:Z

    goto/16 :goto_0

    :cond_25
    move v0, v1

    goto :goto_7

    :cond_26
    move v0, v1

    goto/16 :goto_4
.end method

.method private b(Ljava/lang/StringBuilder;)V
    .locals 5

    .prologue
    const/16 v4, 0x2d

    .line 493
    iget v1, p0, Ljbe;->u:I

    .line 494
    iget v0, p0, Ljbe;->u:I

    iget v2, p0, Ljbe;->B:I

    add-int/2addr v2, v0

    .line 495
    :goto_0
    if-ge v1, v2, :cond_4

    .line 496
    iget-object v0, p0, Ljbe;->q:[C

    aget-char v0, v0, v1

    const/16 v3, 0x66

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    .line 497
    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v3, p0, Ljbe;->q:[C

    aget-char v3, v3, v1

    if-eq v3, v4, :cond_2

    .line 498
    if-eqz v0, :cond_0

    .line 499
    iget-object v3, p0, Ljbe;->q:[C

    aget-char v3, v3, v1

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 501
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 496
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 503
    :cond_2
    if-eqz v0, :cond_3

    .line 504
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 506
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 507
    goto :goto_0

    .line 508
    :cond_4
    return-void
.end method


# virtual methods
.method declared-synchronized a(Ljava/lang/String;IIIIIILandroid/graphics/RectF;I)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 319
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Ljbe;->b(Ljava/lang/String;)V

    .line 321
    iget-boolean v1, p0, Ljbe;->A:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 322
    const/4 v0, 0x0

    .line 325
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Ljbe;->r:I

    add-int/lit8 v2, v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    if-eqz p9, :cond_1

    iget-boolean v2, p0, Ljbe;->x:Z

    if-eqz v2, :cond_1

    iget-object v0, p0, Ljbe;->q:[C

    const/4 v2, 0x0

    iget v3, p0, Ljbe;->t:I

    invoke-virtual {v1, v0, v2, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Ljbe;->y:Z

    if-nez v0, :cond_13

    and-int/lit16 v0, p2, 0x80

    if-eqz v0, :cond_13

    sget-object v0, Ljbe;->d:[C

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    rem-int/lit8 v0, p9, 0x3

    add-int/lit8 v0, v0, 0x31

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    iget v0, p0, Ljbe;->t:I

    add-int/lit8 v0, v0, 0x3

    iget-object v2, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->C:I

    sub-int/2addr v3, v0

    invoke-virtual {v1, v2, v0, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget v0, p0, Ljbe;->C:I

    :cond_1
    iget-boolean v2, p0, Ljbe;->z:Z

    if-eqz v2, :cond_14

    iget-object v2, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->u:I

    sub-int/2addr v3, v0

    invoke-virtual {v1, v2, v0, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    :goto_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez p3, :cond_2

    if-nez p4, :cond_3

    if-nez p5, :cond_3

    :cond_2
    const/16 v2, 0x73

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_3
    if-eqz p4, :cond_4

    const/16 v2, 0x77

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4
    if-eqz p5, :cond_5

    const/16 v2, 0x68

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_5
    and-int/lit8 v2, p2, 0x20

    if-eqz v2, :cond_6

    const-string v2, "rw-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    and-int/lit8 v2, p2, 0x1

    if-eqz v2, :cond_7

    const-string v2, "c-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    and-int/lit8 v2, p2, 0x2

    if-eqz v2, :cond_8

    const-string v2, "d-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    and-int/lit8 v2, p2, 0x10

    if-eqz v2, :cond_9

    const-string v2, "k-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    and-int/lit8 v2, p2, 0x4

    if-eqz v2, :cond_a

    const-string v2, "no-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    and-int/lit8 v2, p2, 0x40

    if-eqz v2, :cond_b

    const-string v2, "nu-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    and-int/lit8 v2, p2, 0x8

    if-eqz v2, :cond_c

    const-string v2, "p-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    and-int/lit16 v2, p2, 0x100

    if-eqz v2, :cond_d

    const-string v2, "fSoften=0,25,0-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    and-int/lit16 v2, p2, 0x200

    if-eqz v2, :cond_e

    const-string v2, "ip-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    if-eq p6, v4, :cond_f

    if-eq p7, v4, :cond_f

    packed-switch p6, :pswitch_data_0

    :goto_3
    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_f
    and-int/lit16 v2, p2, 0x400

    if-eqz v2, :cond_10

    if-eqz p8, :cond_10

    const-string v2, "fcrop64=1,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p8, Landroid/graphics/RectF;->left:F

    invoke-static {v2}, Ljbe;->a(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p8, Landroid/graphics/RectF;->top:F

    invoke-static {v2}, Ljbe;->a(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p8, Landroid/graphics/RectF;->right:F

    invoke-static {v2}, Ljbe;->a(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p8, Landroid/graphics/RectF;->bottom:F

    invoke-static {v2}, Ljbe;->a(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    invoke-direct {p0, v1}, Ljbe;->a(Ljava/lang/StringBuilder;)V

    invoke-direct {p0, v1}, Ljbe;->b(Ljava/lang/StringBuilder;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-le v2, v0, :cond_11

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_11
    iget-boolean v0, p0, Ljbe;->z:Z

    if-eqz v0, :cond_12

    iget v0, p0, Ljbe;->B:I

    if-nez v0, :cond_12

    const/16 v0, 0x2f

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_12
    iget-object v0, p0, Ljbe;->q:[C

    iget v2, p0, Ljbe;->u:I

    iget v3, p0, Ljbe;->B:I

    add-int/2addr v2, v3

    iget v3, p0, Ljbe;->r:I

    iget v4, p0, Ljbe;->u:I

    iget v5, p0, Ljbe;->B:I

    add-int/2addr v4, v5

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_13
    sget-object v0, Ljbe;->c:[C

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    rem-int/lit8 v0, p9, 0x4

    add-int/lit8 v0, v0, 0x33

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 325
    :cond_14
    :try_start_2
    iget-object v2, p0, Ljbe;->q:[C

    iget v3, p0, Ljbe;->v:I

    sub-int/2addr v3, v0

    invoke-virtual {v1, v2, v0, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    const/16 v0, 0x3d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :pswitch_0
    const/16 v2, 0x76

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :pswitch_1
    const/16 v2, 0x6c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method declared-synchronized a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 312
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Ljbe;->b(Ljava/lang/String;)V

    .line 313
    iget-boolean v0, p0, Ljbe;->w:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 312
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
