.class public final Laev;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lokw;

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 555
    invoke-direct {p0}, Loxq;-><init>()V

    .line 558
    sget-object v0, Lokw;->a:[Lokw;

    iput-object v0, p0, Laev;->a:[Lokw;

    .line 555
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 581
    .line 582
    iget-object v1, p0, Laev;->a:[Lokw;

    if-eqz v1, :cond_1

    .line 583
    iget-object v2, p0, Laev;->a:[Lokw;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 584
    if-eqz v4, :cond_0

    .line 585
    const/4 v5, 0x1

    .line 586
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 583
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 590
    :cond_1
    iget-object v1, p0, Laev;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 591
    const/4 v1, 0x2

    iget-object v2, p0, Laev;->b:Ljava/lang/Integer;

    .line 592
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 594
    :cond_2
    iget-object v1, p0, Laev;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 595
    iput v0, p0, Laev;->ai:I

    .line 596
    return v0
.end method

.method public a(Loxn;)Laev;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 604
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 605
    sparse-switch v0, :sswitch_data_0

    .line 609
    iget-object v2, p0, Laev;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 610
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Laev;->ah:Ljava/util/List;

    .line 613
    :cond_1
    iget-object v2, p0, Laev;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 615
    :sswitch_0
    return-object p0

    .line 620
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 621
    iget-object v0, p0, Laev;->a:[Lokw;

    if-nez v0, :cond_3

    move v0, v1

    .line 622
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lokw;

    .line 623
    iget-object v3, p0, Laev;->a:[Lokw;

    if-eqz v3, :cond_2

    .line 624
    iget-object v3, p0, Laev;->a:[Lokw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 626
    :cond_2
    iput-object v2, p0, Laev;->a:[Lokw;

    .line 627
    :goto_2
    iget-object v2, p0, Laev;->a:[Lokw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 628
    iget-object v2, p0, Laev;->a:[Lokw;

    new-instance v3, Lokw;

    invoke-direct {v3}, Lokw;-><init>()V

    aput-object v3, v2, v0

    .line 629
    iget-object v2, p0, Laev;->a:[Lokw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 630
    invoke-virtual {p1}, Loxn;->a()I

    .line 627
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 621
    :cond_3
    iget-object v0, p0, Laev;->a:[Lokw;

    array-length v0, v0

    goto :goto_1

    .line 633
    :cond_4
    iget-object v2, p0, Laev;->a:[Lokw;

    new-instance v3, Lokw;

    invoke-direct {v3}, Lokw;-><init>()V

    aput-object v3, v2, v0

    .line 634
    iget-object v2, p0, Laev;->a:[Lokw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 638
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Laev;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 605
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 565
    iget-object v0, p0, Laev;->a:[Lokw;

    if-eqz v0, :cond_1

    .line 566
    iget-object v1, p0, Laev;->a:[Lokw;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 567
    if-eqz v3, :cond_0

    .line 568
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 566
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 572
    :cond_1
    iget-object v0, p0, Laev;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 573
    const/4 v0, 0x2

    iget-object v1, p0, Laev;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 575
    :cond_2
    iget-object v0, p0, Laev;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 577
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 551
    invoke-virtual {p0, p1}, Laev;->a(Loxn;)Laev;

    move-result-object v0

    return-object v0
.end method
