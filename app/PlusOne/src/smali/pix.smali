.class public final Lpix;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpix;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    new-array v0, v0, [Lpix;

    sput-object v0, Lpix;->a:[Lpix;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Loxq;-><init>()V

    .line 155
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lpix;->d:[Ljava/lang/String;

    .line 158
    const/high16 v0, -0x80000000

    iput v0, p0, Lpix;->e:I

    .line 143
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 198
    .line 199
    iget-object v0, p0, Lpix;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 200
    const/4 v0, 0x1

    iget-object v2, p0, Lpix;->b:Ljava/lang/String;

    .line 201
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 203
    :goto_0
    iget-object v2, p0, Lpix;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 204
    const/4 v2, 0x2

    iget-object v3, p0, Lpix;->c:Ljava/lang/String;

    .line 205
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 207
    :cond_0
    iget-object v2, p0, Lpix;->d:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lpix;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 209
    iget-object v3, p0, Lpix;->d:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 211
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 213
    :cond_1
    add-int/2addr v0, v2

    .line 214
    iget-object v1, p0, Lpix;->d:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 216
    :cond_2
    iget v1, p0, Lpix;->e:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_3

    .line 217
    const/4 v1, 0x4

    iget v2, p0, Lpix;->e:I

    .line 218
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_3
    iget-object v1, p0, Lpix;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 221
    const/4 v1, 0x5

    iget-object v2, p0, Lpix;->f:Ljava/lang/String;

    .line 222
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 224
    :cond_4
    iget-object v1, p0, Lpix;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 225
    const/4 v1, 0x6

    iget-object v2, p0, Lpix;->g:Ljava/lang/String;

    .line 226
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 228
    :cond_5
    iget-object v1, p0, Lpix;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 229
    const/4 v1, 0x7

    iget-object v2, p0, Lpix;->h:Ljava/lang/String;

    .line 230
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_6
    iget-object v1, p0, Lpix;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    iput v0, p0, Lpix;->ai:I

    .line 234
    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lpix;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 242
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 243
    sparse-switch v0, :sswitch_data_0

    .line 247
    iget-object v1, p0, Lpix;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 248
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpix;->ah:Ljava/util/List;

    .line 251
    :cond_1
    iget-object v1, p0, Lpix;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 253
    :sswitch_0
    return-object p0

    .line 258
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpix;->b:Ljava/lang/String;

    goto :goto_0

    .line 262
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpix;->c:Ljava/lang/String;

    goto :goto_0

    .line 266
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 267
    iget-object v0, p0, Lpix;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 268
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 269
    iget-object v2, p0, Lpix;->d:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 270
    iput-object v1, p0, Lpix;->d:[Ljava/lang/String;

    .line 271
    :goto_1
    iget-object v1, p0, Lpix;->d:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 272
    iget-object v1, p0, Lpix;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 273
    invoke-virtual {p1}, Loxn;->a()I

    .line 271
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 276
    :cond_2
    iget-object v1, p0, Lpix;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 280
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 281
    if-eq v0, v4, :cond_3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 283
    :cond_3
    iput v0, p0, Lpix;->e:I

    goto :goto_0

    .line 285
    :cond_4
    iput v4, p0, Lpix;->e:I

    goto :goto_0

    .line 290
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpix;->f:Ljava/lang/String;

    goto :goto_0

    .line 294
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpix;->g:Ljava/lang/String;

    goto :goto_0

    .line 298
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpix;->h:Ljava/lang/String;

    goto :goto_0

    .line 243
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 169
    iget-object v0, p0, Lpix;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 170
    const/4 v0, 0x1

    iget-object v1, p0, Lpix;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 172
    :cond_0
    iget-object v0, p0, Lpix;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 173
    const/4 v0, 0x2

    iget-object v1, p0, Lpix;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 175
    :cond_1
    iget-object v0, p0, Lpix;->d:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 176
    iget-object v1, p0, Lpix;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 177
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_2
    iget v0, p0, Lpix;->e:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_3

    .line 181
    const/4 v0, 0x4

    iget v1, p0, Lpix;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 183
    :cond_3
    iget-object v0, p0, Lpix;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 184
    const/4 v0, 0x5

    iget-object v1, p0, Lpix;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 186
    :cond_4
    iget-object v0, p0, Lpix;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 187
    const/4 v0, 0x6

    iget-object v1, p0, Lpix;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 189
    :cond_5
    iget-object v0, p0, Lpix;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 190
    const/4 v0, 0x7

    iget-object v1, p0, Lpix;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 192
    :cond_6
    iget-object v0, p0, Lpix;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 194
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0, p1}, Lpix;->a(Loxn;)Lpix;

    move-result-object v0

    return-object v0
.end method
