.class public Lkkp;
.super Lllq;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lkkp;",
            ">;"
        }
    .end annotation
.end field

.field private static a:[I


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lkkp;->a:[I

    .line 134
    new-instance v0, Lkkq;

    invoke-direct {v0}, Lkkq;-><init>()V

    sput-object v0, Lkkp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void

    .line 24
    :array_0
    .array-data 4
        0x14e
        0x0
    .end array-data
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    const/high16 v0, -0x80000000

    iput v0, p0, Lkkp;->f:I

    .line 34
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    const/high16 v0, -0x80000000

    iput v0, p0, Lkkp;->f:I

    .line 37
    iput-object p3, p0, Lkkp;->b:Ljava/lang/String;

    .line 38
    iput p1, p0, Lkkp;->f:I

    .line 39
    iput-object p2, p0, Lkkp;->d:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lkkp;->e:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lkkp;->c:Ljava/lang/String;

    .line 42
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lllq;-><init>()V

    .line 31
    const/high16 v0, -0x80000000

    iput v0, p0, Lkkp;->f:I

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkkp;->b:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkkp;->c:Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkkp;->d:Ljava/lang/String;

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lkkp;->e:Ljava/lang/String;

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lkkp;->f:I

    .line 153
    return-void
.end method


# virtual methods
.method public a()Loya;
    .locals 5

    .prologue
    .line 58
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    .line 59
    sget-object v1, Lkkp;->a:[I

    iput-object v1, v0, Loya;->b:[I

    .line 60
    sget-object v1, Loys;->a:Loxr;

    new-instance v2, Loys;

    invoke-direct {v2}, Loys;-><init>()V

    iget-object v3, p0, Lkkp;->b:Ljava/lang/String;

    iput-object v3, v2, Loys;->b:Ljava/lang/String;

    iget v3, p0, Lkkp;->f:I

    iput v3, v2, Loys;->d:I

    iget-object v3, p0, Lkkp;->d:Ljava/lang/String;

    iput-object v3, v2, Loys;->c:Ljava/lang/String;

    iget-object v3, p0, Lkkp;->c:Ljava/lang/String;

    if-eqz v3, :cond_0

    new-instance v3, Lpdi;

    invoke-direct {v3}, Lpdi;-><init>()V

    iput-object v3, v2, Loys;->e:Lpdi;

    iget-object v3, v2, Loys;->e:Lpdi;

    iget-object v4, p0, Lkkp;->c:Ljava/lang/String;

    iput-object v4, v3, Lpdi;->a:Ljava/lang/String;

    :cond_0
    invoke-virtual {v0, v1, v2}, Loya;->a(Loxr;Ljava/lang/Object;)V

    .line 61
    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lkkp;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkkp;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkkp;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lkkp;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lkkp;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lkkp;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lkkp;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lkkp;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 166
    iget v0, p0, Lkkp;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 167
    return-void
.end method
