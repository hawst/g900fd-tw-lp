.class public Ldgx;
.super Llol;
.source "PG"

# interfaces
.implements Lhjj;


# static fields
.field public static final N:Ldgo;

.field public static final O:Ldgo;

.field public static final P:Ldgo;

.field public static final Q:Ldgo;

.field public static final R:Ldgo;

.field public static final S:Ldgo;

.field public static final T:Ldgo;


# instance fields
.field private final U:Ldhc;

.field private V:Ldgr;

.field private W:Ldgi;

.field private final X:Ldhd;

.field private Y:Lhjf;

.field private Z:Ldeo;

.field private aa:Ldes;

.field private ab:Ldec;

.field private ac:Ldei;

.field private ad:Lcom/google/android/apps/plus/views/PhotoActionBar;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Ldgo;

    invoke-direct {v0}, Ldgo;-><init>()V

    sput-object v0, Ldgx;->N:Ldgo;

    .line 43
    new-instance v0, Ldgo;

    invoke-direct {v0}, Ldgo;-><init>()V

    sput-object v0, Ldgx;->O:Ldgo;

    .line 44
    new-instance v0, Ldgo;

    invoke-direct {v0}, Ldgo;-><init>()V

    sput-object v0, Ldgx;->P:Ldgo;

    .line 45
    new-instance v0, Ldgo;

    new-instance v1, Ldgq;

    const v2, 0x7f1006c3

    invoke-direct {v1, v2}, Ldgq;-><init>(I)V

    invoke-direct {v0, v1}, Ldgo;-><init>(Ldgq;)V

    sput-object v0, Ldgx;->Q:Ldgo;

    .line 47
    new-instance v0, Ldgo;

    invoke-direct {v0}, Ldgo;-><init>()V

    sput-object v0, Ldgx;->R:Ldgo;

    .line 48
    new-instance v0, Ldgo;

    invoke-direct {v0}, Ldgo;-><init>()V

    sput-object v0, Ldgx;->S:Ldgo;

    .line 49
    new-instance v0, Ldgo;

    new-instance v1, Ldgq;

    const v2, 0x7f10069e

    invoke-direct {v1, v2}, Ldgq;-><init>(I)V

    invoke-direct {v0, v1}, Ldgo;-><init>(Ldgq;)V

    sput-object v0, Ldgx;->T:Ldgo;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0}, Llol;-><init>()V

    .line 51
    new-instance v0, Ldhc;

    invoke-direct {v0, p0}, Ldhc;-><init>(Ldgx;)V

    iput-object v0, p0, Ldgx;->U:Ldhc;

    .line 56
    new-instance v0, Ldhd;

    invoke-direct {v0, p0}, Ldhd;-><init>(Ldgx;)V

    iput-object v0, p0, Ldgx;->X:Ldhd;

    .line 68
    new-instance v0, Ldep;

    iget-object v1, p0, Ldgx;->av:Llqm;

    new-instance v2, Ldgy;

    invoke-direct {v2, p0}, Ldgy;-><init>(Ldgx;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 74
    new-instance v0, Ldet;

    iget-object v1, p0, Ldgx;->av:Llqm;

    new-instance v2, Ldgz;

    invoke-direct {v2, p0}, Ldgz;-><init>(Ldgx;)V

    invoke-direct {v0, v1, v2}, Ldet;-><init>(Llqr;Ldeu;)V

    .line 80
    new-instance v0, Ldej;

    iget-object v1, p0, Ldgx;->av:Llqm;

    new-instance v2, Ldha;

    invoke-direct {v2, p0}, Ldha;-><init>(Ldgx;)V

    invoke-direct {v0, v1, v2}, Ldej;-><init>(Llqr;Ldek;)V

    .line 287
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 139
    iget-object v0, p0, Ldgx;->Z:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    .line 140
    iget-object v3, p0, Ldgx;->Z:Ldeo;

    invoke-virtual {v3}, Ldeo;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lddl;->Q()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    if-nez v3, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v3, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PhotoActionBar;->c()V

    .line 150
    iget-object v3, p0, Ldgx;->V:Ldgr;

    sget-object v4, Ldgx;->N:Ldgo;

    invoke-virtual {v3, v4}, Ldgr;->b(Ldgu;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 151
    iget-object v3, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->d(Z)V

    .line 154
    :cond_2
    iget-object v3, p0, Ldgx;->V:Ldgr;

    sget-object v4, Ldgx;->O:Ldgo;

    invoke-virtual {v3, v4}, Ldgr;->b(Ldgu;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 155
    iget-object v3, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-interface {v0}, Lddl;->O()Z

    move-result v4

    invoke-interface {v0}, Lddl;->R()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v3, v4, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(ZI)V

    .line 156
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->c(Z)V

    .line 164
    :goto_1
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v3, Ldgx;->P:Ldgo;

    invoke-virtual {v0, v3}, Ldgr;->b(Ldgu;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 165
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    iget-object v3, p0, Ldgx;->aa:Ldes;

    invoke-virtual {v3}, Ldes;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(I)V

    .line 166
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->e(Z)V

    .line 167
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    iget-object v3, p0, Ldgx;->V:Ldgr;

    sget-object v4, Ldgx;->P:Ldgo;

    .line 168
    invoke-virtual {v3, v4}, Ldgr;->c(Ldgu;)Z

    move-result v3

    .line 167
    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotoActionBar;->b(Z)V

    .line 171
    :cond_3
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v3, Ldgx;->R:Ldgo;

    invoke-virtual {v0, v3}, Ldgr;->b(Ldgu;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Ldgx;->ab:Ldec;

    if-eqz v0, :cond_9

    .line 172
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    iget-object v3, p0, Ldgx;->ab:Ldec;

    invoke-virtual {v3}, Ldec;->g()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotoActionBar;->b(I)V

    .line 173
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->g(Z)V

    .line 178
    :cond_4
    :goto_2
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v3, Ldgx;->S:Ldgo;

    invoke-virtual {v0, v3}, Ldgr;->b(Ldgu;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 179
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    iget-object v3, p0, Ldgx;->ac:Ldei;

    invoke-virtual {v3}, Ldei;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotoActionBar;->c(I)V

    .line 180
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->h(Z)V

    .line 181
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    iget-object v1, p0, Ldgx;->V:Ldgr;

    sget-object v3, Ldgx;->S:Ldgo;

    invoke-virtual {v1, v3}, Ldgr;->c(Ldgu;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->i(Z)V

    .line 184
    :cond_5
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/PhotoActionBar;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Ldgx;->Y:Lhjf;

    invoke-interface {v0}, Lhjf;->b()V

    goto/16 :goto_0

    .line 158
    :cond_6
    iget-object v3, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v0, v4, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0, v2}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(ZZ)V

    .line 160
    iget-object v3, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v4, Ldgx;->T:Ldgo;

    .line 161
    invoke-virtual {v0, v4}, Ldgr;->b(Ldgu;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    .line 160
    :goto_4
    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(Z)V

    goto/16 :goto_1

    :cond_7
    move v0, v2

    .line 158
    goto :goto_3

    :cond_8
    move v0, v2

    .line 161
    goto :goto_4

    .line 174
    :cond_9
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v3, Ldgx;->Q:Ldgo;

    invoke-virtual {v0, v3}, Ldgr;->b(Ldgu;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 175
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->f(Z)V

    goto :goto_2
.end method

.method static synthetic a(Ldgx;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ldgx;->a()V

    return-void
.end method

.method static synthetic b(Ldgx;)Ldgr;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldgx;->V:Ldgr;

    return-object v0
.end method

.method static synthetic c(Ldgx;)Ldeo;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldgx;->Z:Ldeo;

    return-object v0
.end method

.method static synthetic d(Ldgx;)Lhjf;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ldgx;->Y:Lhjf;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 118
    const v0, 0x7f04017c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 119
    const v0, 0x7f1004a6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoActionBar;

    iput-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    .line 120
    return-object v1
.end method

.method public a(Lhjk;)V
    .locals 2

    .prologue
    .line 201
    iget-object v0, p0, Ldgx;->W:Ldgi;

    invoke-interface {v0}, Ldgi;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iget-object v0, p0, Ldgx;->Z:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    .line 206
    iget-object v1, p0, Ldgx;->Z:Ldeo;

    invoke-virtual {v1}, Ldeo;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lddl;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v1, Ldgx;->T:Ldgo;

    .line 213
    invoke-virtual {v0, v1}, Ldgr;->b(Ldgu;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    sget-object v0, Ldgx;->T:Ldgo;

    invoke-virtual {v0}, Ldgo;->a()Ldgq;

    move-result-object v0

    invoke-virtual {v0}, Ldgq;->a()I

    move-result v0

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 218
    :cond_2
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v1, Ldgx;->Q:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->b(Ldgu;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    sget-object v0, Ldgx;->Q:Ldgo;

    invoke-virtual {v0}, Ldgo;->a()Ldgq;

    move-result-object v0

    invoke-virtual {v0}, Ldgq;->a()I

    move-result v0

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 225
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget-object v2, Ldgx;->T:Ldgo;

    invoke-virtual {v2}, Ldgo;->a()Ldgq;

    move-result-object v2

    invoke-virtual {v2}, Ldgq;->a()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 226
    iget-object v1, p0, Ldgx;->V:Ldgr;

    sget-object v2, Ldgx;->T:Ldgo;

    invoke-virtual {v1, v2}, Ldgr;->d(Ldgu;)V

    .line 233
    :goto_0
    return v0

    .line 228
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget-object v2, Ldgx;->Q:Ldgo;

    invoke-virtual {v2}, Ldgo;->a()Ldgq;

    move-result-object v2

    invoke-virtual {v2}, Ldgq;->a()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 229
    iget-object v1, p0, Ldgx;->V:Ldgr;

    sget-object v2, Ldgx;->Q:Ldgo;

    invoke-virtual {v1, v2}, Ldgr;->d(Ldgu;)V

    goto :goto_0

    .line 233
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0}, Llol;->aO_()V

    .line 126
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    iget-object v1, p0, Ldgx;->U:Ldhc;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(Lfzl;)V

    .line 127
    invoke-direct {p0}, Ldgx;->a()V

    .line 128
    iget-object v0, p0, Ldgx;->W:Ldgi;

    iget-object v1, p0, Ldgx;->X:Ldhd;

    invoke-interface {v0, v1}, Ldgi;->a(Ldgl;)V

    .line 129
    return-void
.end method

.method public b(Loo;)V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 91
    iget-object v0, p0, Ldgx;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Ldgx;->V:Ldgr;

    .line 92
    iget-object v0, p0, Ldgx;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Ldgx;->Z:Ldeo;

    .line 93
    iget-object v0, p0, Ldgx;->au:Llnh;

    const-class v1, Ldes;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldes;

    iput-object v0, p0, Ldgx;->aa:Ldes;

    .line 94
    iget-object v0, p0, Ldgx;->au:Llnh;

    const-class v1, Ldec;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldec;

    iput-object v0, p0, Ldgx;->ab:Ldec;

    .line 95
    iget-object v0, p0, Ldgx;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Ldgx;->W:Ldgi;

    .line 96
    iget-object v0, p0, Ldgx;->au:Llnh;

    const-class v1, Lhjf;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjf;

    iput-object v0, p0, Ldgx;->Y:Lhjf;

    .line 97
    iget-object v0, p0, Ldgx;->au:Llnh;

    const-class v1, Ldei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldei;

    iput-object v0, p0, Ldgx;->ac:Ldei;

    .line 99
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v1, Ldgx;->N:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgu;)V

    .line 100
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v1, Ldgx;->O:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgu;)V

    .line 101
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v1, Ldgx;->P:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgu;)V

    .line 102
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v1, Ldgx;->Q:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgu;)V

    .line 103
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v1, Ldgx;->R:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgu;)V

    .line 104
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v1, Ldgx;->S:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgu;)V

    .line 105
    iget-object v0, p0, Ldgx;->V:Ldgr;

    sget-object v1, Ldgx;->T:Ldgo;

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgu;)V

    .line 107
    iget-object v0, p0, Ldgx;->V:Ldgr;

    new-instance v1, Ldhb;

    invoke-direct {v1, p0}, Ldhb;-><init>(Ldgx;)V

    invoke-virtual {v0, v1}, Ldgr;->a(Ldgw;)V

    .line 113
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0}, Llol;->z()V

    .line 134
    iget-object v0, p0, Ldgx;->ad:Lcom/google/android/apps/plus/views/PhotoActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoActionBar;->a(Lfzl;)V

    .line 135
    iget-object v0, p0, Ldgx;->W:Ldgi;

    iget-object v1, p0, Ldgx;->X:Ldhd;

    invoke-interface {v0, v1}, Ldgi;->b(Ldgl;)V

    .line 136
    return-void
.end method
