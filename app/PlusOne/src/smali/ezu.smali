.class public final Lezu;
.super Lezk;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljuf;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 24
    invoke-static {}, Ljvj;->b()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lezk;-><init>(Landroid/content/Context;Ljava/util/List;ILizu;Ljava/lang/String;Landroid/net/Uri;)V

    .line 26
    return-void
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 34
    invoke-super {p0}, Lezk;->C()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lhyx;

    .line 36
    invoke-virtual {v0}, Lhyx;->b()[Landroid/database/Cursor;

    move-result-object v6

    array-length v7, v6

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_5

    aget-object v8, v6, v4

    .line 37
    if-eqz v8, :cond_0

    move v4, v2

    .line 42
    :goto_1
    if-nez v4, :cond_1

    move-object v0, v1

    .line 84
    :goto_2
    return-object v0

    .line 36
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 46
    :cond_1
    new-instance v4, Lezh;

    invoke-virtual {p0}, Lezu;->n()Landroid/content/Context;

    move-result-object v6

    .line 47
    invoke-virtual {p0}, Lezu;->n()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Ljem;->a(Landroid/content/Context;)Ljem;

    move-result-object v7

    invoke-direct {v4, v6, v0, v9, v7}, Lezh;-><init>(Landroid/content/Context;Lhyx;ILjem;)V

    .line 49
    invoke-virtual {v4}, Lezh;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v2

    .line 52
    :goto_3
    if-eqz v0, :cond_2

    .line 53
    sget-object v0, Levl;->b:[Ljava/lang/String;

    const/16 v0, 0x16

    new-array v0, v0, [Ljava/lang/Object;

    .line 54
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v3

    .line 55
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v9

    .line 56
    const/4 v6, 0x4

    invoke-static {}, Ljvj;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v0, v6

    .line 57
    const-string v6, "~local"

    aput-object v6, v0, v2

    .line 58
    const/4 v2, 0x5

    const-string v6, "camera"

    aput-object v6, v0, v2

    .line 59
    const/16 v2, 0x9

    invoke-virtual {v4}, Lezh;->getCount()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v2

    .line 60
    const/16 v2, 0xd

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    .line 61
    const/16 v2, 0xf

    const-wide/32 v6, 0x40000

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v0, v2

    .line 63
    new-instance v2, Lhym;

    sget-object v3, Levl;->b:[Ljava/lang/String;

    invoke-direct {v2, v3}, Lhym;-><init>([Ljava/lang/String;)V

    .line 64
    invoke-virtual {v2, v0}, Lhym;->a([Ljava/lang/Object;)V

    .line 65
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    :cond_2
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 72
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 73
    new-instance v1, Lezv;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/database/Cursor;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/database/Cursor;

    invoke-direct {v1, v0, v2}, Lezv;-><init>([Landroid/database/Cursor;Landroid/os/Bundle;)V

    move-object v0, v1

    goto/16 :goto_2

    :cond_3
    move v0, v3

    .line 49
    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 84
    goto/16 :goto_2

    :cond_5
    move v4, v3

    goto/16 :goto_1
.end method
