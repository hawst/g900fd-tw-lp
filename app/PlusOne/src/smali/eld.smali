.class final Leld;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ldvw;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lelc;


# direct methods
.method constructor <init>(Lelc;)V
    .locals 0

    .prologue
    .line 332
    iput-object p1, p0, Leld;->a:Lelc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)Lnqx;
    .locals 2

    .prologue
    .line 382
    new-instance v0, Lnqx;

    invoke-direct {v0}, Lnqx;-><init>()V

    .line 383
    iput p1, v0, Lnqx;->b:I

    .line 384
    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lnqx;->c:Ljava/lang/Integer;

    .line 385
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnqx;->e:Ljava/lang/Boolean;

    .line 386
    return-object v0
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 336
    new-instance v6, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 337
    const/16 v0, 0xf

    invoke-direct {p0, v0}, Leld;->a(I)Lnqx;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Leld;->a(I)Lnqx;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    new-instance v1, Lemi;

    iget-object v0, p0, Leld;->a:Lelc;

    invoke-static {v0}, Lelc;->b(Lelc;)Llnl;

    move-result-object v2

    iget-object v0, p0, Leld;->a:Lelc;

    iget-object v0, v0, Lelc;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v3

    const-wide/32 v4, 0x7fffffff

    invoke-direct/range {v1 .. v6}, Lemi;-><init>(Landroid/content/Context;IJLjava/util/List;)V

    return-object v1
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 379
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 332
    check-cast p2, Ljava/util/ArrayList;

    invoke-virtual {p0, p2}, Leld;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ldvw;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 346
    iget-object v0, p0, Leld;->a:Lelc;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lelc;->R:Z

    .line 347
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 350
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvw;

    .line 351
    iget v4, v0, Ldvw;->a:I

    const/16 v5, 0xf

    if-ne v4, v5, :cond_0

    .line 352
    iget-object v0, v0, Ldvw;->c:Ljava/util/ArrayList;

    move-object v2, v0

    goto :goto_0

    .line 353
    :cond_0
    iget v4, v0, Ldvw;->a:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 354
    iget-object v0, v0, Ldvw;->c:Ljava/util/ArrayList;

    :goto_1
    move-object v1, v0

    .line 356
    goto :goto_0

    .line 357
    :cond_1
    iget-object v0, p0, Leld;->a:Lelc;

    invoke-static {v0, v2, v1}, Lelc;->a(Lelc;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 359
    iget-object v0, p0, Leld;->a:Lelc;

    .line 360
    invoke-static {v0}, Lelc;->c(Lelc;)Llnl;

    move-result-object v0

    const v1, 0x7f0a05d5

    .line 359
    invoke-static {v0, v1}, Lfuw;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 361
    new-instance v0, Lele;

    invoke-direct {v0, p0}, Lele;-><init>(Leld;)V

    const-wide/16 v2, 0x3e8

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 368
    :cond_2
    new-instance v0, Lkoe;

    const/16 v1, 0x5d

    iget-object v2, p0, Leld;->a:Lelc;

    .line 369
    invoke-static {v2}, Lelc;->f(Lelc;)J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lkoe;-><init>(IJ)V

    iget-object v1, p0, Leld;->a:Lelc;

    invoke-static {v1}, Lelc;->e(Lelc;)Llnl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 370
    new-instance v0, Lkoe;

    const/16 v1, 0x5e

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    iget-object v1, p0, Leld;->a:Lelc;

    invoke-static {v1}, Lelc;->g(Lelc;)Llnl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 375
    :goto_2
    return-void

    .line 373
    :cond_3
    iget-object v0, p0, Leld;->a:Lelc;

    invoke-static {v0, v1, v1}, Lelc;->a(Lelc;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
