.class public final Ldrf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liad;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;IZ)V
    .locals 6

    .prologue
    .line 29
    invoke-static {p1, p2}, Lhzt;->a(Landroid/content/Context;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 30
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 32
    if-nez p3, :cond_0

    const-wide/32 v4, 0xf4240

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    .line 54
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 38
    :try_start_0
    invoke-static {v1}, Ldrm;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 39
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 41
    sget-object v2, Ldxd;->i:Lief;

    invoke-interface {v0, v2, p2}, Lieh;->b(Lief;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 42
    invoke-static {v1}, Ldsf;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 44
    :cond_1
    invoke-static {v1}, Ldrl;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 45
    invoke-static {}, Ldsc;->a()V

    .line 46
    invoke-static {v1}, Llbn;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 47
    invoke-static {p1, v1, p2}, Ldsm;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 48
    invoke-static {p1, v1, p2}, Ldtc;->a(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 49
    const-class v0, Ldhu;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldhu;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Ldhu;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 51
    :cond_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method
