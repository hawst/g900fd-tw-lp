.class public abstract Ldor;
.super Lhny;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:[Ljava/lang/String;

.field private h:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p2, p1}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 41
    iput p3, p0, Ldor;->h:I

    .line 42
    iput-object p4, p0, Ldor;->a:Ljava/lang/String;

    .line 43
    iput-object p5, p0, Ldor;->b:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Ldor;->c:Ljava/lang/String;

    .line 45
    iput-object p7, p0, Ldor;->d:Ljava/lang/String;

    .line 46
    iput-object p8, p0, Ldor;->e:Ljava/lang/String;

    .line 47
    iput-object p9, p0, Ldor;->f:[Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 7

    .prologue
    .line 52
    invoke-virtual {p0}, Ldor;->c()Ldji;

    move-result-object v2

    .line 53
    invoke-virtual {v2}, Ldji;->l()V

    .line 54
    invoke-virtual {v2}, Ldji;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {v2}, Ldji;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmah;

    iget-object v1, v0, Lmah;->a:Lnfi;

    .line 56
    invoke-virtual {p0}, Ldor;->f()Landroid/content/Context;

    move-result-object v0

    iget v3, p0, Ldor;->h:I

    iget-object v4, p0, Ldor;->e:Ljava/lang/String;

    iget-object v5, p0, Ldor;->d:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-static {v0, v3, v4, v5, v6}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v0

    .line 58
    if-eqz v0, :cond_0

    .line 59
    :try_start_0
    new-instance v3, Lnym;

    invoke-direct {v3}, Lnym;-><init>()V

    invoke-static {v3, v0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    iget-object v1, v1, Lnfi;->a:[Lnyi;

    iput-object v1, v0, Lnym;->O:[Lnyi;

    .line 67
    new-instance v1, Lnzx;

    invoke-direct {v1}, Lnzx;-><init>()V

    .line 68
    iget-object v3, p0, Ldor;->e:Ljava/lang/String;

    iput-object v3, v1, Lnzx;->b:Ljava/lang/String;

    .line 69
    new-instance v3, Lnzu;

    invoke-direct {v3}, Lnzu;-><init>()V

    .line 70
    iput-object v0, v3, Lnzu;->b:Lnym;

    .line 71
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {v1, v0, v3}, Lnzx;->a(Loxr;Ljava/lang/Object;)V

    .line 73
    invoke-virtual {p0}, Ldor;->f()Landroid/content/Context;

    move-result-object v0

    iget v3, p0, Ldor;->h:I

    const/4 v4, 0x0

    .line 72
    invoke-static {v0, v3, v1, v4}, Ljvj;->a(Landroid/content/Context;ILnzx;I)V

    .line 76
    :cond_0
    new-instance v1, Lhoz;

    iget v3, v2, Lkff;->i:I

    iget-object v4, v2, Lkff;->k:Ljava/lang/Exception;

    .line 77
    invoke-virtual {v2}, Ldji;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ldor;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v3, v4, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    :goto_1
    return-object v0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    const-string v1, "EditPhotoHashtagsTask"

    const-string v3, "Unable to parse Photo from byte array."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 64
    new-instance v0, Lhoz;

    iget v1, v2, Lkff;->i:I

    iget-object v2, v2, Lkff;->k:Ljava/lang/Exception;

    invoke-virtual {p0}, Ldor;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    goto :goto_1

    .line 77
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract c()Ldji;
.end method

.method protected abstract d()Ljava/lang/String;
.end method
