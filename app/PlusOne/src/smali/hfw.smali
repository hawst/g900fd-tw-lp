.class public final Lhfw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lhfw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:I

.field private b:Lhxc;

.field private c:Ljqs;

.field private d:Lkxr;

.field private e:Lhhe;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    new-instance v0, Lhfx;

    invoke-direct {v0}, Lhfx;-><init>()V

    sput-object v0, Lhfw;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p0}, Lhfw;->i()Lhfw;

    .line 24
    return-void
.end method


# virtual methods
.method public a(Lhxc;I)Lhfw;
    .locals 0

    .prologue
    .line 78
    invoke-virtual {p0}, Lhfw;->i()Lhfw;

    .line 79
    iput p2, p0, Lhfw;->a:I

    .line 80
    iput-object p1, p0, Lhfw;->b:Lhxc;

    .line 81
    return-object p0
.end method

.method public a(Ljqs;I)Lhfw;
    .locals 0

    .prologue
    .line 85
    invoke-virtual {p0}, Lhfw;->i()Lhfw;

    .line 86
    iput p2, p0, Lhfw;->a:I

    .line 87
    iput-object p1, p0, Lhfw;->c:Ljqs;

    .line 88
    return-object p0
.end method

.method public a(Lkxr;)Lhfw;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lhfw;->i()Lhfw;

    .line 93
    const/4 v0, 0x2

    iput v0, p0, Lhfw;->a:I

    .line 94
    iput-object p1, p0, Lhfw;->d:Lkxr;

    .line 95
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lhfw;->b:Lhxc;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lhfw;->b:Lhxc;

    invoke-virtual {v0}, Lhxc;->a()Ljava/lang/String;

    move-result-object v0

    .line 35
    :goto_0
    return-object v0

    .line 29
    :cond_0
    iget-object v0, p0, Lhfw;->c:Ljqs;

    if-eqz v0, :cond_2

    .line 30
    iget-object v0, p0, Lhfw;->c:Ljqs;

    invoke-virtual {v0}, Ljqs;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhfw;->c:Ljqs;

    .line 31
    invoke-virtual {v0}, Ljqs;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhfw;->c:Ljqs;

    invoke-virtual {v0}, Ljqs;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 32
    :cond_2
    iget-object v0, p0, Lhfw;->d:Lkxr;

    if-eqz v0, :cond_3

    .line 33
    iget-object v0, p0, Lhfw;->d:Lkxr;

    invoke-virtual {v0}, Lkxr;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 34
    :cond_3
    iget-object v0, p0, Lhfw;->e:Lhhe;

    if-eqz v0, :cond_4

    .line 35
    iget-object v0, p0, Lhfw;->e:Lhhe;

    invoke-virtual {v0}, Lhhe;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 37
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No data set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lhfw;)V
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lhfw;->a:I

    iput v0, p1, Lhfw;->a:I

    .line 122
    iget-object v0, p0, Lhfw;->b:Lhxc;

    iput-object v0, p1, Lhfw;->b:Lhxc;

    .line 123
    iget-object v0, p0, Lhfw;->c:Ljqs;

    iput-object v0, p1, Lhfw;->c:Ljqs;

    .line 124
    iget-object v0, p0, Lhfw;->d:Lkxr;

    iput-object v0, p1, Lhfw;->d:Lkxr;

    .line 125
    iget-object v0, p0, Lhfw;->e:Lhhe;

    iput-object v0, p1, Lhfw;->e:Lhhe;

    .line 126
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lhfw;->a:I

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lhfw;->b:Lhxc;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lhfw;->b:Lhxc;

    invoke-virtual {v0}, Lhxc;->b()Ljava/lang/String;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    .line 47
    :cond_0
    iget-object v0, p0, Lhfw;->c:Ljqs;

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lhfw;->c:Ljqs;

    invoke-virtual {v0}, Ljqs;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lhfw;->d:Lkxr;

    if-eqz v0, :cond_2

    .line 50
    iget-object v0, p0, Lhfw;->d:Lkxr;

    invoke-virtual {v0}, Lkxr;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_2
    iget-object v0, p0, Lhfw;->e:Lhhe;

    if-eqz v0, :cond_3

    .line 52
    iget-object v0, p0, Lhfw;->e:Lhhe;

    invoke-virtual {v0}, Lhhe;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No data set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lhfw;->j()Lhfw;

    move-result-object v0

    return-object v0
.end method

.method public d()Lhxc;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lhfw;->b:Lhxc;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public e()Ljqs;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lhfw;->c:Ljqs;

    return-object v0
.end method

.method public f()Lkxr;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lhfw;->d:Lkxr;

    return-object v0
.end method

.method public g()Lhhe;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lhfw;->e:Lhhe;

    return-object v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lhfw;->c:Ljqs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhfw;->c:Ljqs;

    invoke-virtual {v0}, Ljqs;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Lhfw;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 106
    const/4 v0, 0x4

    iput v0, p0, Lhfw;->a:I

    .line 107
    iput-object v1, p0, Lhfw;->b:Lhxc;

    .line 108
    iput-object v1, p0, Lhfw;->c:Ljqs;

    .line 109
    iput-object v1, p0, Lhfw;->d:Lkxr;

    .line 110
    iput-object v1, p0, Lhfw;->e:Lhhe;

    .line 111
    return-object p0
.end method

.method public j()Lhfw;
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lhfw;

    invoke-direct {v0}, Lhfw;-><init>()V

    .line 116
    invoke-virtual {p0, v0}, Lhfw;->a(Lhfw;)V

    .line 117
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lhfw;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    iget-object v0, p0, Lhfw;->b:Lhxc;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 132
    iget-object v0, p0, Lhfw;->c:Ljqs;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 133
    iget-object v0, p0, Lhfw;->d:Lkxr;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 135
    return-void
.end method
