.class final Liex;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lijm;


# instance fields
.field private synthetic a:Landroid/app/Activity;

.field private synthetic b:Lifb;

.field private synthetic c:Landroid/content/Context;

.field private synthetic d:Lijk;


# direct methods
.method constructor <init>(Landroid/app/Activity;Lifb;Landroid/content/Context;Lijk;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Liex;->a:Landroid/app/Activity;

    iput-object p2, p0, Liex;->b:Lifb;

    iput-object p3, p0, Liex;->c:Landroid/content/Context;

    iput-object p4, p0, Liex;->d:Lijk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 204
    .line 210
    :try_start_0
    iget-object v1, p0, Liex;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 211
    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 214
    :try_start_1
    invoke-virtual {v1}, Landroid/view/View;->isDrawingCacheEnabled()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v4

    .line 215
    if-nez v4, :cond_5

    .line 216
    const/4 v5, 0x1

    :try_start_2
    invoke-virtual {v1, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 221
    :goto_0
    :try_start_3
    invoke-virtual {v1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v5

    .line 224
    if-eqz v5, :cond_4

    .line 225
    const v6, 0xbfa64    # 1.100014E-39f

    invoke-static {v5, v6}, Liew;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v0

    .line 232
    :goto_1
    if-eqz v3, :cond_3

    if-nez v4, :cond_3

    if-eqz v1, :cond_3

    .line 233
    invoke-virtual {v1, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    move-object v1, v0

    .line 238
    :goto_2
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 240
    :try_start_4
    iget-object v0, p0, Liex;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v4, "com.google.android.gms"

    const/4 v5, 0x0

    .line 241
    invoke-virtual {v0, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_1

    .line 245
    :goto_3
    const-string v2, "GmsVersion"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Liex;->b:Lifb;

    sget-object v2, Lifb;->a:Lifb;

    if-eq v0, v2, :cond_0

    .line 248
    const-string v0, "Subproduct"

    iget-object v2, p0, Liex;->b:Lifb;

    invoke-virtual {v2}, Lifb;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    :cond_0
    iget-object v0, p0, Liex;->c:Landroid/content/Context;

    const-class v2, Likn;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Likn;

    iget-object v2, p0, Liex;->d:Lijk;

    invoke-interface {v0, v2}, Likn;->a(Lijk;)Likm;

    move-result-object v2

    .line 252
    iget-object v0, p0, Liex;->c:Landroid/content/Context;

    const-class v4, Likv;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Likv;

    .line 253
    invoke-interface {v0, v1}, Likv;->a(Landroid/graphics/Bitmap;)Likv;

    move-result-object v0

    .line 254
    invoke-interface {v0, v3}, Likv;->a(Landroid/os/Bundle;)Likv;

    move-result-object v0

    .line 255
    invoke-interface {v0}, Likv;->b()Liku;

    move-result-object v0

    .line 256
    invoke-interface {v2, v0}, Likm;->b(Liku;)Lijq;

    move-result-object v0

    new-instance v1, Liez;

    iget-object v2, p0, Liex;->d:Lijk;

    iget-object v3, p0, Liex;->c:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Liez;-><init>(Lijk;Landroid/content/Context;)V

    .line 257
    invoke-interface {v0, v1}, Lijq;->a(Lijt;)V

    .line 258
    return-void

    .line 229
    :catch_0
    move-exception v1

    move-object v1, v0

    move v4, v3

    move v3, v2

    .line 232
    :goto_4
    if-eqz v3, :cond_2

    if-nez v4, :cond_2

    if-eqz v1, :cond_2

    .line 233
    invoke-virtual {v1, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    move-object v1, v0

    goto :goto_2

    .line 232
    :catchall_0
    move-exception v1

    move v4, v3

    move v3, v2

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    :goto_5
    if-eqz v3, :cond_1

    if-nez v4, :cond_1

    if-eqz v1, :cond_1

    .line 233
    invoke-virtual {v1, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    :cond_1
    throw v0

    .line 243
    :catch_1
    move-exception v0

    move v0, v2

    goto :goto_3

    .line 232
    :catchall_1
    move-exception v0

    move v4, v3

    move v3, v2

    goto :goto_5

    :catchall_2
    move-exception v0

    move v3, v2

    goto :goto_5

    :catchall_3
    move-exception v0

    goto :goto_5

    .line 229
    :catch_2
    move-exception v4

    move v4, v3

    move v3, v2

    goto :goto_4

    :catch_3
    move-exception v3

    move v3, v2

    goto :goto_4

    :catch_4
    move-exception v5

    goto :goto_4

    :cond_2
    move-object v1, v0

    goto/16 :goto_2

    :cond_3
    move-object v1, v0

    goto/16 :goto_2

    :cond_4
    move-object v0, v5

    goto/16 :goto_1

    :cond_5
    move v3, v2

    goto/16 :goto_0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 262
    return-void
.end method
