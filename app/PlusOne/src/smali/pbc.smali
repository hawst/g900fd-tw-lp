.class public final Lpbc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1612
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1638
    const/4 v0, 0x0

    .line 1639
    iget-object v1, p0, Lpbc;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 1640
    const/4 v0, 0x1

    iget-object v1, p0, Lpbc;->a:Ljava/lang/Boolean;

    .line 1641
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1643
    :cond_0
    iget-object v1, p0, Lpbc;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1644
    const/4 v1, 0x2

    iget-object v2, p0, Lpbc;->b:Ljava/lang/Boolean;

    .line 1645
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1647
    :cond_1
    iget-object v1, p0, Lpbc;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1648
    const/4 v1, 0x3

    iget-object v2, p0, Lpbc;->c:Ljava/lang/Boolean;

    .line 1649
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1651
    :cond_2
    iget-object v1, p0, Lpbc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1652
    iput v0, p0, Lpbc;->ai:I

    .line 1653
    return v0
.end method

.method public a(Loxn;)Lpbc;
    .locals 2

    .prologue
    .line 1661
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1662
    sparse-switch v0, :sswitch_data_0

    .line 1666
    iget-object v1, p0, Lpbc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1667
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpbc;->ah:Ljava/util/List;

    .line 1670
    :cond_1
    iget-object v1, p0, Lpbc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1672
    :sswitch_0
    return-object p0

    .line 1677
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbc;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 1681
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbc;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1685
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpbc;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 1662
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1623
    iget-object v0, p0, Lpbc;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1624
    const/4 v0, 0x1

    iget-object v1, p0, Lpbc;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1626
    :cond_0
    iget-object v0, p0, Lpbc;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1627
    const/4 v0, 0x2

    iget-object v1, p0, Lpbc;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1629
    :cond_1
    iget-object v0, p0, Lpbc;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1630
    const/4 v0, 0x3

    iget-object v1, p0, Lpbc;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1632
    :cond_2
    iget-object v0, p0, Lpbc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1634
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1608
    invoke-virtual {p0, p1}, Lpbc;->a(Loxn;)Lpbc;

    move-result-object v0

    return-object v0
.end method
