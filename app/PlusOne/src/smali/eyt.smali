.class public final Leyt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Intent;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Integer;

.field private i:Ljava/lang/Integer;

.field private j:Ljava/lang/Integer;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/Integer;

.field private n:Ljava/lang/Integer;

.field private final o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 3452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3453
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Leyt;->a:Landroid/content/Intent;

    .line 3454
    iput p3, p0, Leyt;->o:I

    .line 3455
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 3557
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3558
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "account_id"

    iget v2, p0, Leyt;->o:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3560
    iget-object v0, p0, Leyt;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3561
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "photo_picker_mode"

    iget-object v2, p0, Leyt;->b:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3564
    :cond_0
    iget-object v0, p0, Leyt;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3565
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "filter"

    iget-object v2, p0, Leyt;->c:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3568
    :cond_1
    iget-object v0, p0, Leyt;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3573
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "external"

    iget-object v2, p0, Leyt;->d:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3576
    :cond_2
    iget-object v0, p0, Leyt;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 3577
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "is_for_get_content"

    iget-object v2, p0, Leyt;->e:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3580
    :cond_3
    iget-object v0, p0, Leyt;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 3585
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "force_return_edit_list"

    iget-object v2, p0, Leyt;->f:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3588
    :cond_4
    iget-object v0, p0, Leyt;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 3593
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "tabs"

    iget-object v2, p0, Leyt;->g:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3596
    :cond_5
    iget-object v0, p0, Leyt;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 3609
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "photo_picker_crop_mode"

    iget-object v2, p0, Leyt;->h:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3612
    :cond_6
    iget-object v0, p0, Leyt;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 3613
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "photo_min_width"

    iget-object v2, p0, Leyt;->i:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3616
    :cond_7
    iget-object v0, p0, Leyt;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 3617
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "photo_min_height"

    iget-object v2, p0, Leyt;->j:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3620
    :cond_8
    iget-object v0, p0, Leyt;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 3621
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "disable_up_button"

    iget-object v2, p0, Leyt;->k:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3624
    :cond_9
    iget-object v0, p0, Leyt;->l:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 3625
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "button_title_res_id"

    iget-object v2, p0, Leyt;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3628
    :cond_a
    iget-object v0, p0, Leyt;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 3629
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "min_selection_count"

    iget-object v2, p0, Leyt;->m:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3632
    :cond_b
    iget-object v0, p0, Leyt;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 3633
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    const-string v1, "max_selection_count"

    iget-object v2, p0, Leyt;->n:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 3636
    :cond_c
    iget-object v0, p0, Leyt;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a(I)Leyt;
    .locals 1

    .prologue
    .line 3506
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyt;->g:Ljava/lang/Integer;

    .line 3507
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Leyt;
    .locals 0

    .prologue
    .line 3491
    iput-object p1, p0, Leyt;->f:Ljava/lang/Boolean;

    .line 3492
    return-object p0
.end method

.method public a(Ljava/lang/Integer;)Leyt;
    .locals 0

    .prologue
    .line 3459
    iput-object p1, p0, Leyt;->b:Ljava/lang/Integer;

    .line 3460
    return-object p0
.end method

.method public a(Ljava/lang/String;)Leyt;
    .locals 0

    .prologue
    .line 3541
    iput-object p1, p0, Leyt;->l:Ljava/lang/String;

    .line 3542
    return-object p0
.end method

.method public a(Z)Leyt;
    .locals 1

    .prologue
    .line 3476
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leyt;->d:Ljava/lang/Boolean;

    .line 3477
    return-object p0
.end method

.method public b(I)Leyt;
    .locals 1

    .prologue
    .line 3521
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyt;->h:Ljava/lang/Integer;

    .line 3522
    return-object p0
.end method

.method public b(Ljava/lang/Integer;)Leyt;
    .locals 0

    .prologue
    .line 3465
    iput-object p1, p0, Leyt;->c:Ljava/lang/Integer;

    .line 3466
    return-object p0
.end method

.method public b(Z)Leyt;
    .locals 1

    .prologue
    .line 3481
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leyt;->e:Ljava/lang/Boolean;

    .line 3482
    return-object p0
.end method

.method public c(I)Leyt;
    .locals 1

    .prologue
    .line 3526
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyt;->i:Ljava/lang/Integer;

    .line 3527
    return-object p0
.end method

.method public c(Z)Leyt;
    .locals 1

    .prologue
    .line 3536
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Leyt;->k:Ljava/lang/Boolean;

    .line 3537
    return-object p0
.end method

.method public d(I)Leyt;
    .locals 1

    .prologue
    .line 3531
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyt;->j:Ljava/lang/Integer;

    .line 3532
    return-object p0
.end method

.method public e(I)Leyt;
    .locals 1

    .prologue
    .line 3546
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyt;->m:Ljava/lang/Integer;

    .line 3547
    return-object p0
.end method

.method public f(I)Leyt;
    .locals 1

    .prologue
    .line 3551
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leyt;->n:Ljava/lang/Integer;

    .line 3552
    return-object p0
.end method
