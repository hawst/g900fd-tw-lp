.class public final Lcof;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Landroid/widget/SectionIndexer;
.implements Lcry;
.implements Lenf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/BaseAdapter;",
        "Landroid/widget/SectionIndexer;",
        "Lcry",
        "<",
        "Lctm;",
        ">;",
        "Lenf",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private c:Z

.field private d:Z

.field private final e:I

.field private final f:Lizs;

.field private g:I

.field private h:Lcoq;

.field private final i:Lfcd;

.field private j:I

.field private k:Lene;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lene",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/concurrent/Future;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lene;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lene",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lctm;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lctm;

.field private o:Lctm;

.field private p:Lctm;

.field private q:Lcrp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcrp",
            "<",
            "Lctm;",
            ">;"
        }
    .end annotation
.end field

.field private r:Z

.field private s:Lcow;

.field private t:Lcom;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/apps/plus/views/FastScrollContainer;)V
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcof;->g:I

    .line 73
    new-instance v0, Lcoi;

    invoke-direct {v0, p0}, Lcoi;-><init>(Lcof;)V

    iput-object v0, p0, Lcof;->k:Lene;

    .line 74
    new-instance v0, Lcol;

    invoke-direct {v0, p0}, Lcol;-><init>(Lcof;)V

    iput-object v0, p0, Lcof;->l:Lene;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcof;->m:Ljava/util/List;

    .line 87
    iput-object p1, p0, Lcof;->a:Landroid/content/Context;

    .line 88
    iput p2, p0, Lcof;->b:I

    .line 89
    new-instance v0, Ljvl;

    invoke-direct {v0, p1}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v0, v0, Ljvl;->a:I

    iput v0, p0, Lcof;->e:I

    .line 90
    const-class v0, Lizs;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iput-object v0, p0, Lcof;->f:Lizs;

    .line 91
    new-instance v0, Lfcd;

    new-instance v1, Lcog;

    invoke-direct {v1, p0}, Lcog;-><init>(Lcof;)V

    invoke-direct {v0, v1}, Lfcd;-><init>(Lfcf;)V

    iput-object v0, p0, Lcof;->i:Lfcd;

    .line 98
    iget-object v0, p0, Lcof;->i:Lfcd;

    invoke-virtual {v0, p3}, Lfcd;->a(Lcom/google/android/apps/plus/views/FastScrollContainer;)V

    .line 99
    return-void
.end method

.method static synthetic a(Lcof;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcof;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(Lcoq;I)Lctm;
    .locals 4

    .prologue
    .line 213
    sget-object v0, Lcoq;->d:Lcoq;

    if-ne p1, v0, :cond_0

    .line 214
    new-instance v0, Lcos;

    invoke-direct {v0, p2}, Lcos;-><init>(I)V

    .line 219
    :goto_0
    return-object v0

    .line 216
    :cond_0
    new-instance v0, Lcor;

    iget-object v1, p0, Lcof;->a:Landroid/content/Context;

    sget-object v2, Lcoq;->e:Lcoq;

    invoke-virtual {v2}, Lcoq;->ordinal()I

    move-result v2

    iget-object v3, p0, Lcof;->q:Lcrp;

    invoke-direct {v0, v1, v2, v3}, Lcor;-><init>(Landroid/content/Context;ILcrp;)V

    goto :goto_0
.end method

.method static synthetic b(Lcof;)Lcom;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcof;->t:Lcom;

    return-object v0
.end method

.method static synthetic c(Lcof;)Lizs;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcof;->f:Lizs;

    return-object v0
.end method

.method private f()I
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcof;->n:Lctm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v1, p0, Lcof;->n:Lctm;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Lctm;
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcof;->d:Z

    if-eqz v0, :cond_0

    .line 269
    new-instance v0, Lcoz;

    invoke-direct {v0}, Lcoz;-><init>()V

    .line 271
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcot;

    invoke-direct {v0}, Lcot;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcof;->j:I

    return v0
.end method

.method public a(Lcoo;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 384
    iget-object v1, p0, Lcof;->m:Ljava/util/List;

    if-nez v1, :cond_0

    move v1, v0

    .line 385
    :goto_0
    const/4 v4, 0x0

    .line 387
    new-instance v5, Lcok;

    invoke-direct {v5, p1}, Lcok;-><init>(Lcoo;)V

    move v2, v0

    move v3, v0

    .line 389
    :goto_1
    if-ge v2, v1, :cond_2

    .line 390
    invoke-virtual {v5}, Lcok;->a()V

    .line 391
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, v5}, Lctm;->a(Lctn;)V

    .line 393
    iget v0, v5, Lcok;->a:F

    cmpg-float v0, v0, v4

    if-ltz v0, :cond_2

    .line 394
    iget v0, v5, Lcok;->a:F

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_3

    .line 396
    iget v0, v5, Lcok;->a:F

    .line 399
    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, v5, Lcok;->a:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v4, 0x33d6bf95    # 1.0E-7f

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_1

    move v3, v0

    move v0, v2

    .line 400
    :goto_2
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    move v3, v0

    goto :goto_1

    .line 384
    :cond_0
    iget-object v1, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_1
    move v3, v2

    .line 405
    :cond_2
    return v3

    :cond_3
    move v0, v3

    move v3, v4

    goto :goto_2
.end method

.method public a(I)Lctm;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    return-object v0
.end method

.method public a(II)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 417
    new-instance v2, Lcoh;

    invoke-direct {v2}, Lcoh;-><init>()V

    .line 419
    :goto_0
    if-ge p1, p2, :cond_1

    .line 420
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    .line 421
    invoke-interface {v0, v2}, Lctm;->a(Lctn;)V

    .line 423
    iget-object v0, v2, Lcoh;->a:Lcsp;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, v2, Lcoh;->a:Lcsp;

    invoke-virtual {v0}, Lcsp;->a()Ljava/util/List;

    move-result-object v0

    .line 425
    if-eqz v0, :cond_0

    .line 426
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 419
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 431
    :cond_1
    return-object v1
.end method

.method public a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 117
    new-instance v1, Lcpb;

    sget-object v0, Lcoq;->h:Lcoq;

    invoke-virtual {v0}, Lcoq;->ordinal()I

    move-result v0

    invoke-direct {v1, p1, v0}, Lcpb;-><init>(Landroid/view/View;I)V

    .line 118
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 120
    iget-object v0, p0, Lcof;->p:Lctm;

    if-eqz v0, :cond_4

    .line 121
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v2, p0, Lcof;->p:Lctm;

    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 123
    if-eqz p1, :cond_3

    .line 124
    if-gez v0, :cond_0

    invoke-direct {p0}, Lcof;->f()I

    move-result v0

    .line 125
    :cond_0
    iget-object v2, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 127
    iget-object v2, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v2, v0, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 144
    :cond_1
    :goto_0
    if-eqz p1, :cond_5

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcof;->p:Lctm;

    .line 145
    invoke-virtual {p0}, Lcof;->notifyDataSetChanged()V

    .line 146
    return-void

    .line 130
    :cond_2
    iget-object v2, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v2, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 132
    :cond_3
    if-ltz v0, :cond_1

    .line 134
    iget-object v2, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 136
    :cond_4
    if-eqz p1, :cond_1

    .line 138
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-direct {p0}, Lcof;->f()I

    move-result v2

    invoke-interface {v0, v2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 144
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Lcom;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcof;->t:Lcom;

    .line 103
    return-void
.end method

.method public a(Lcoq;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 233
    if-eqz p1, :cond_1

    sget-object v1, Lcoq;->e:Lcoq;

    invoke-virtual {v1, p1}, Lcoq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcoq;->d:Lcoq;

    .line 234
    invoke-virtual {v1, p1}, Lcoq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iput-object p1, p0, Lcof;->h:Lcoq;

    .line 241
    iget-object v1, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 245
    iget-object v1, p0, Lcof;->n:Lctm;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcof;->h:Lcoq;

    if-eqz v1, :cond_2

    .line 247
    iget-object v1, p0, Lcof;->h:Lcoq;

    invoke-direct {p0, v1, v4}, Lcof;->a(Lcoq;I)Lctm;

    move-result-object v1

    iput-object v1, p0, Lcof;->n:Lctm;

    .line 248
    iget-object v1, p0, Lcof;->m:Ljava/util/List;

    iget-object v2, p0, Lcof;->n:Lctm;

    invoke-interface {v1, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 249
    invoke-virtual {p0}, Lcof;->notifyDataSetChanged()V

    goto :goto_0

    .line 250
    :cond_2
    iget-object v1, p0, Lcof;->n:Lctm;

    if-eqz v1, :cond_0

    .line 251
    iget-object v1, p0, Lcof;->m:Ljava/util/List;

    iget-object v2, p0, Lcof;->n:Lctm;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 252
    iget-object v2, p0, Lcof;->h:Lcoq;

    if-nez v2, :cond_3

    if-ltz v1, :cond_3

    .line 254
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcof;->n:Lctm;

    .line 256
    invoke-virtual {p0}, Lcof;->notifyDataSetChanged()V

    goto :goto_0

    .line 257
    :cond_3
    iget-object v2, p0, Lcof;->n:Lctm;

    invoke-interface {v2}, Lctm;->a()I

    move-result v2

    iget-object v3, p0, Lcof;->h:Lcoq;

    invoke-virtual {v3}, Lcoq;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 259
    iget-object v2, p0, Lcof;->h:Lcoq;

    invoke-direct {p0, v2, v4}, Lcof;->a(Lcoq;I)Lctm;

    move-result-object v2

    iput-object v2, p0, Lcof;->n:Lctm;

    .line 260
    iget-object v2, p0, Lcof;->m:Ljava/util/List;

    if-gez v1, :cond_4

    :goto_1
    iget-object v1, p0, Lcof;->n:Lctm;

    invoke-interface {v2, v0, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 261
    invoke-virtual {p0}, Lcof;->notifyDataSetChanged()V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 260
    goto :goto_1
.end method

.method public a(Lcpx;IZZ)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 110
    iput-boolean p4, p0, Lcof;->r:Z

    .line 111
    iput-boolean p3, p0, Lcof;->c:Z

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v0, p0, Lcof;->h:Lcoq;

    invoke-direct {p0, v0, p2}, Lcof;->a(Lcoq;I)Lctm;

    move-result-object v0

    iput-object v0, p0, Lcof;->n:Lctm;

    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v1, p0, Lcof;->n:Lctm;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcpx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcof;->j:I

    iput-object v2, p0, Lcof;->o:Lctm;

    iput-object v2, p0, Lcof;->s:Lcow;

    .line 113
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcof;->notifyDataSetChanged()V

    .line 114
    return-void

    .line 112
    :cond_2
    iget-object v0, p1, Lcpx;->c:Lcpu;

    invoke-virtual {v0}, Lcpu;->a()I

    move-result v0

    iput v0, p0, Lcof;->j:I

    iget-object v0, p0, Lcof;->p:Lctm;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v1, p0, Lcof;->p:Lctm;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-boolean v0, p0, Lcof;->r:Z

    if-eqz v0, :cond_4

    new-instance v0, Lcow;

    invoke-direct {v0}, Lcow;-><init>()V

    iput-object v0, p0, Lcof;->s:Lcow;

    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v1, p0, Lcof;->s:Lcow;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v2, p1, Lcpx;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iget-object v0, p1, Lcpx;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_5

    iget-object v3, p1, Lcpx;->b:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p1, Lcpx;->b:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcof;->i:Lfcd;

    invoke-virtual {v0, v2}, Lfcd;->a(Landroid/util/SparseArray;)V

    iget-boolean v0, p0, Lcof;->c:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcof;->g()Lctm;

    move-result-object v0

    iput-object v0, p0, Lcof;->o:Lctm;

    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v1, p0, Lcof;->o:Lctm;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v0, p1, Lcpx;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcpx;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcof;->g:I

    goto :goto_0
.end method

.method public a(Lcrp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcrp",
            "<",
            "Lctm;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 448
    iput-object p1, p0, Lcof;->q:Lcrp;

    .line 449
    return-void
.end method

.method public a(Lctm;)V
    .locals 3

    .prologue
    .line 329
    invoke-interface {p1}, Lctm;->a()I

    move-result v0

    sget-object v1, Lcoq;->e:Lcoq;

    invoke-virtual {v1}, Lcoq;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 330
    iget-object v0, p0, Lcof;->a:Landroid/content/Context;

    iget v1, p0, Lcof;->b:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Ldhv;->h(Landroid/content/Context;IZ)V

    .line 331
    invoke-virtual {p0}, Lcof;->notifyDataSetChanged()V

    .line 333
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 40
    check-cast p1, Lctm;

    invoke-virtual {p0, p1}, Lcof;->a(Lctm;)V

    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 279
    iput-boolean p1, p0, Lcof;->d:Z

    .line 282
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 310
    :goto_0
    return-void

    .line 286
    :cond_0
    iget-object v0, p0, Lcof;->s:Lcow;

    if-eqz v0, :cond_1

    .line 287
    iget-object v0, p0, Lcof;->s:Lcow;

    invoke-virtual {v0, p1}, Lcow;->a(Z)V

    .line 291
    :cond_1
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 292
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    .line 293
    iget-boolean v2, p0, Lcof;->c:Z

    if-eqz v2, :cond_4

    .line 294
    iget-object v2, p0, Lcof;->o:Lctm;

    .line 295
    invoke-direct {p0}, Lcof;->g()Lctm;

    move-result-object v3

    iput-object v3, p0, Lcof;->o:Lctm;

    .line 297
    if-ne v0, v2, :cond_3

    .line 298
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v2, p0, Lcof;->o:Lctm;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 309
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcof;->notifyDataSetChanged()V

    goto :goto_0

    .line 300
    :cond_3
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    iget-object v1, p0, Lcof;->o:Lctm;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 303
    :cond_4
    iget-object v2, p0, Lcof;->o:Lctm;

    if-ne v0, v2, :cond_2

    .line 304
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 305
    const/4 v0, 0x0

    iput-object v0, p0, Lcof;->o:Lctm;

    goto :goto_1
.end method

.method public b()I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Lcof;->g:I

    return v0
.end method

.method public b(I)Lcoo;
    .locals 2

    .prologue
    .line 369
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 370
    :cond_0
    const/4 v0, 0x0

    .line 376
    :goto_0
    return-object v0

    .line 373
    :cond_1
    new-instance v1, Lcoj;

    invoke-direct {v1}, Lcoj;-><init>()V

    .line 374
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, v1}, Lctm;->a(Lctn;)V

    .line 376
    iget-object v0, v1, Lcoj;->a:Lcoo;

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcof;->e:I

    return v0
.end method

.method public d()Lene;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lene",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/concurrent/Future;",
            ">;"
        }
    .end annotation

    .prologue
    .line 435
    iget-object v0, p0, Lcof;->k:Lene;

    return-object v0
.end method

.method public e()Lene;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lene",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 439
    iget-object v0, p0, Lcof;->l:Lene;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcof;->a(I)Lctm;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 337
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0}, Lctm;->a()I

    move-result v0

    return v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcof;->i:Lfcd;

    invoke-virtual {v0, p1}, Lfcd;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcof;->i:Lfcd;

    invoke-virtual {v0, p1}, Lfcd;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcof;->i:Lfcd;

    invoke-virtual {v0}, Lfcd;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcof;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctm;

    invoke-interface {v0, p2, p3}, Lctm;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 352
    invoke-static {}, Lcoq;->values()[Lcoq;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    return v0
.end method
