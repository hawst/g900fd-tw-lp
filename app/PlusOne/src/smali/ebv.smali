.class public final Lebv;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lbc;
.implements Lcov;
.implements Lcoy;
.implements Lcpt;
.implements Lcqc;
.implements Lept;
.implements Leqz;
.implements Lfyl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Landroid/widget/AbsListView$OnScrollListener;",
        "Lbc",
        "<",
        "Lcpu;",
        ">;",
        "Lcov;",
        "Lcoy;",
        "Lcpt;",
        "Lcqc;",
        "Lept;",
        "Leqz;",
        "Lfyl;"
    }
.end annotation


# static fields
.field public static final N:Lloz;


# instance fields
.field private aA:Z

.field private aB:Z

.field private aC:Lcqa;

.field private aD:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcqd;",
            "Lcpx;",
            ">;"
        }
    .end annotation
.end field

.field private aE:Lcqd;

.field private aF:Lcom;

.field private aG:Lcqf;

.field private final aH:Licq;

.field private final aI:Lfhh;

.field private final aJ:Ljava/lang/Runnable;

.field private aK:Z

.field private final aL:Ljava/lang/Runnable;

.field private ah:Lequ;

.field private ai:Lcom/google/android/apps/plus/views/FastScrollListView;

.field private aj:Lcom/google/android/apps/plus/views/FastScrollContainer;

.field private ak:Lcof;

.field private al:Lend;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lend",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/concurrent/Future;",
            ">;"
        }
    .end annotation
.end field

.field private am:Lend;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lend",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/android/libraries/social/media/MediaResource;",
            ">;"
        }
    .end annotation
.end field

.field private an:Ljava/lang/Integer;

.field private ao:Ljava/lang/Integer;

.field private ap:Lcoo;

.field private aq:Landroid/net/Uri;

.field private ar:Z

.field private as:I

.field private aw:Ldwg;

.field private ax:Lepn;

.field private ay:[Lepm;

.field private az:Lebz;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 107
    new-instance v0, Lloz;

    const-string v1, "debug.plus.all_photos_extended"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lebv;->N:Lloz;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 93
    invoke-direct {p0}, Legi;-><init>()V

    .line 144
    new-instance v0, Lequ;

    iget-object v1, p0, Lebv;->av:Llqm;

    invoke-direct {v0, p0, v1, p0, v2}, Lequ;-><init>(Lu;Llqr;Leqz;I)V

    .line 146
    invoke-virtual {v0, v2}, Lequ;->a(Z)Lequ;

    move-result-object v0

    iput-object v0, p0, Lebv;->ah:Lequ;

    .line 172
    new-instance v0, Licq;

    iget-object v1, p0, Lebv;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064c

    .line 173
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lebv;->aH:Licq;

    .line 175
    new-instance v0, Lebw;

    invoke-direct {v0, p0}, Lebw;-><init>(Lebv;)V

    iput-object v0, p0, Lebv;->aI:Lfhh;

    .line 187
    new-instance v0, Lebx;

    invoke-direct {v0, p0}, Lebx;-><init>(Lebv;)V

    iput-object v0, p0, Lebv;->aJ:Ljava/lang/Runnable;

    .line 198
    new-instance v0, Leby;

    invoke-direct {v0, p0}, Leby;-><init>(Lebv;)V

    iput-object v0, p0, Lebv;->aL:Ljava/lang/Runnable;

    .line 209
    new-instance v0, Lcsc;

    iget-object v1, p0, Lebv;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcsc;-><init>(Lu;Llqr;I)V

    .line 809
    return-void
.end method

.method static synthetic a(Lebv;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lebv;->au()V

    return-void
.end method

.method static synthetic a(Lebv;Ljava/lang/Integer;Lfib;)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Lebv;->a(Ljava/lang/Integer;Lfib;)V

    return-void
.end method

.method private a(Ljava/lang/Integer;Lfib;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 406
    iget-object v1, p0, Lebv;->an:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lebv;->an:Ljava/lang/Integer;

    if-eq v1, p1, :cond_1

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iput-object v4, p0, Lebv;->an:Ljava/lang/Integer;

    .line 412
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 413
    invoke-virtual {p0}, Lebv;->o()Landroid/content/res/Resources;

    move-result-object v1

    .line 414
    const v2, 0x7f0a07ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-virtual {p0}, Lebv;->n()Lz;

    move-result-object v2

    invoke-static {v2, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 417
    const-string v1, "AllPhotosFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 418
    const-string v1, "AllPhotosFragment"

    const-string v2, "unable to refresh photos"

    invoke-virtual {p2}, Lfib;->d()Ljava/lang/Exception;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 426
    :cond_2
    :goto_1
    iput-object v4, p0, Lebv;->ao:Ljava/lang/Integer;

    .line 428
    invoke-virtual {p0}, Lebv;->x()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lebv;->d(Landroid/view/View;)V

    .line 430
    iget-object v1, p0, Lebv;->ak:Lcof;

    invoke-virtual {v1, v0}, Lcof;->a(Z)V

    goto :goto_0

    .line 420
    :cond_3
    iget-object v1, p0, Lebv;->ao:Ljava/lang/Integer;

    if-ne p1, v1, :cond_2

    .line 421
    const/4 v1, 0x1

    .line 422
    sget-object v2, Lebz;->a:Lebz;

    iput-object v2, p0, Lebv;->az:Lebz;

    .line 423
    iget v2, p0, Lebv;->as:I

    add-int/lit16 v2, v2, 0x2710

    iput v2, p0, Lebv;->as:I

    .line 424
    invoke-virtual {p0}, Lebv;->w()Lbb;

    move-result-object v2

    invoke-virtual {v2, v0, v4, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lebv;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lebv;->ar:Z

    return p1
.end method

.method private au()V
    .locals 2

    .prologue
    .line 836
    iget-object v0, p0, Lebv;->ak:Lcof;

    sget-object v1, Lcoq;->e:Lcoq;

    invoke-virtual {v0, v1}, Lcof;->a(Lcoq;)V

    .line 837
    iget-object v0, p0, Lebv;->ah:Lequ;

    invoke-virtual {v0}, Lequ;->a()V

    .line 838
    return-void
.end method

.method static synthetic b(Lebv;)Lcof;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lebv;->ak:Lcof;

    return-object v0
.end method

.method static synthetic b(Lebv;Z)Z
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lebv;->aK:Z

    return p1
.end method

.method private d(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 699
    if-nez p1, :cond_0

    .line 720
    :goto_0
    return-void

    .line 703
    :cond_0
    invoke-virtual {p0}, Lebv;->U()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 704
    iget-boolean v0, p0, Lebv;->aA:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lebv;->aB:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 705
    :cond_1
    iget-object v0, p0, Lebv;->aH:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 713
    :goto_1
    iget-boolean v0, p0, Lebv;->aA:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lebv;->aB:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lebv;->ap:Lcoo;

    if-eqz v0, :cond_2

    .line 714
    iget-object v0, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v1, p0, Lebv;->ak:Lcof;

    iget-object v2, p0, Lebv;->ap:Lcoo;

    invoke-virtual {v1, v2}, Lcof;->a(Lcoo;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    .line 715
    const/4 v0, 0x0

    iput-object v0, p0, Lebv;->ap:Lcoo;

    .line 718
    :cond_2
    invoke-virtual {p0}, Lebv;->Z_()V

    .line 719
    invoke-virtual {p0}, Lebv;->ag()V

    goto :goto_0

    .line 707
    :cond_3
    iget-object v0, p0, Lebv;->aH:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 710
    :cond_4
    iget-object v0, p0, Lebv;->aH:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 842
    iget-object v0, p0, Lebv;->aC:Lcqa;

    invoke-virtual {v0}, Lcqa;->a()V

    .line 843
    iget-object v0, p0, Lebv;->at:Llnl;

    const-class v1, Lcpj;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpj;

    invoke-virtual {v0}, Lcpj;->a()V

    .line 844
    invoke-super {p0}, Legi;->A()V

    .line 845
    return-void
.end method

.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 466
    sget-object v0, Lhmw;->ae:Lhmw;

    return-object v0
.end method

.method public J_()Z
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 481
    invoke-super {p0}, Legi;->L_()V

    .line 482
    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 499
    :goto_0
    return-void

    .line 485
    :cond_0
    invoke-virtual {p0}, Lebv;->at()Z

    move-result v3

    .line 486
    invoke-virtual {p0}, Lebv;->n()Lz;

    move-result-object v4

    iget-object v0, p0, Lebv;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v5

    if-nez v3, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    .line 489
    if-nez v3, :cond_1

    .line 491
    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0, v6, v1, v2, v2}, Lcof;->a(Lcpx;IZZ)V

    .line 493
    iget-object v0, p0, Lebv;->ak:Lcof;

    sget-object v2, Lcoq;->d:Lcoq;

    invoke-virtual {v0, v2}, Lcof;->a(Lcoq;)V

    .line 495
    :cond_1
    iput-object v6, p0, Lebv;->ao:Ljava/lang/Integer;

    .line 496
    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0, v1}, Lcof;->a(Z)V

    .line 498
    invoke-virtual {p0}, Lebv;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebv;->d(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 486
    goto :goto_1
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lebv;->ak:Lcof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0}, Lcof;->a()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public X_()V
    .locals 4

    .prologue
    .line 523
    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 535
    :goto_0
    return-void

    .line 527
    :cond_0
    iget-object v0, p0, Lebv;->at:Llnl;

    iget-object v1, p0, Lebv;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget v2, p0, Lebv;->as:I

    add-int/lit16 v2, v2, 0x4e20

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IJ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    .line 530
    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    iput-object v0, p0, Lebv;->ao:Ljava/lang/Integer;

    .line 531
    iget-object v0, p0, Lebv;->ak:Lcof;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcof;->a(Z)V

    .line 532
    invoke-virtual {p0}, Lebv;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebv;->d(Landroid/view/View;)V

    .line 533
    iget-object v0, p0, Lebv;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lebv;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eD:Lhmv;

    .line 534
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 533
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method public Y()Z
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-static {v0}, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;->a(Landroid/widget/AbsListView;)Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 471
    invoke-super {p0}, Legi;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebv;->P:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const v10, 0x7f0d0291

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 261
    const v0, 0x7f0400d3

    invoke-super {p0, p1, p2, p3, v0}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v7

    .line 264
    invoke-virtual {p0}, Lebv;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_0

    .line 266
    const-string v3, "scroll_to_uri"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lebv;->aq:Landroid/net/Uri;

    .line 269
    :cond_0
    const v0, 0x7f100306

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/FastScrollListView;

    iput-object v0, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 270
    const v0, 0x7f100305

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/FastScrollContainer;

    iput-object v0, p0, Lebv;->aj:Lcom/google/android/apps/plus/views/FastScrollContainer;

    .line 271
    iget-object v0, p0, Lebv;->aj:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a(Lfyl;)V

    .line 272
    iget-object v0, p0, Lebv;->ah:Lequ;

    invoke-virtual {v0, v1}, Lequ;->b(Z)V

    .line 273
    new-instance v0, Lcof;

    iget-object v3, p0, Lebv;->at:Llnl;

    iget-object v5, p0, Lebv;->P:Lhee;

    invoke-interface {v5}, Lhee;->d()I

    move-result v5

    iget-object v6, p0, Lebv;->aj:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-direct {v0, v3, v5, v6}, Lcof;-><init>(Landroid/content/Context;ILcom/google/android/apps/plus/views/FastScrollContainer;)V

    iput-object v0, p0, Lebv;->ak:Lcof;

    .line 274
    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0}, Lcof;->notifyDataSetChanged()V

    .line 275
    iget-object v0, p0, Lebv;->ak:Lcof;

    sget-object v3, Lcoq;->d:Lcoq;

    invoke-virtual {v0, v3}, Lcof;->a(Lcoq;)V

    .line 276
    iget-object v3, p0, Lebv;->ak:Lcof;

    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcof;->a(Z)V

    .line 277
    iget-object v0, p0, Lebv;->ak:Lcof;

    iget-object v3, p0, Lebv;->aF:Lcom;

    invoke-virtual {v0, v3}, Lcof;->a(Lcom;)V

    .line 278
    new-instance v0, Lend;

    const/16 v3, 0xfa

    iget-object v5, p0, Lebv;->ak:Lcof;

    invoke-virtual {v5}, Lcof;->c()I

    move-result v5

    div-int/2addr v3, v5

    iget-object v5, p0, Lebv;->ak:Lcof;

    iget-object v6, p0, Lebv;->ak:Lcof;

    .line 279
    invoke-virtual {v6}, Lcof;->d()Lene;

    move-result-object v6

    invoke-direct {v0, v3, v5, v6}, Lend;-><init>(ILenf;Lene;)V

    iput-object v0, p0, Lebv;->al:Lend;

    .line 280
    new-instance v0, Lend;

    iget-object v3, p0, Lebv;->ak:Lcof;

    iget-object v5, p0, Lebv;->ak:Lcof;

    .line 281
    invoke-virtual {v5}, Lcof;->e()Lene;

    move-result-object v5

    invoke-direct {v0, v4, v3, v5}, Lend;-><init>(ILenf;Lene;)V

    iput-object v0, p0, Lebv;->am:Lend;

    .line 282
    const v0, 0x7f10005b

    .line 283
    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/pulltorefresh/PullToRefreshSwipeView;

    .line 284
    sget-object v3, Lfvc;->n:Lfvc;

    invoke-virtual {v3}, Lfvc;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 285
    new-instance v3, Lcrp;

    iget-object v5, p0, Lebv;->at:Llnl;

    iget-object v6, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    new-array v8, v1, [Landroid/view/View;

    aput-object v0, v8, v2

    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-direct {v3, v5, v6, v8, v0}, Lcrp;-><init>(Landroid/content/Context;Landroid/widget/ListView;[Landroid/view/View;Lcry;)V

    .line 287
    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0, v3}, Lcof;->a(Lcrp;)V

    .line 290
    :cond_1
    iput-boolean v2, p0, Lebv;->ar:Z

    .line 292
    iget-object v0, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v3, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/FastScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 293
    iget-object v0, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    new-instance v3, Leca;

    invoke-direct {v3}, Leca;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/FastScrollListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 296
    iget-object v0, p0, Lebv;->aC:Lcqa;

    sget-object v3, Lcqd;->b:Lcqd;

    new-instance v5, Lcpy;

    iget-object v6, p0, Lebv;->at:Llnl;

    iget-object v8, p0, Lebv;->ak:Lcof;

    .line 297
    invoke-virtual {v8}, Lcof;->c()I

    move-result v8

    iget-object v9, p0, Lebv;->at:Llnl;

    .line 298
    invoke-virtual {v9}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v9

    invoke-direct {v5, v6, v8, v9, v1}, Lcpy;-><init>(Landroid/content/Context;IIZ)V

    .line 296
    invoke-virtual {v0, v3, v5}, Lcqa;->a(Lcqd;Lcqe;)V

    .line 301
    iget-object v0, p0, Lebv;->aC:Lcqa;

    sget-object v1, Lcqd;->a:Lcqd;

    new-instance v3, Lcpy;

    iget-object v5, p0, Lebv;->at:Llnl;

    iget-object v6, p0, Lebv;->ak:Lcof;

    .line 302
    invoke-virtual {v6}, Lcof;->c()I

    move-result v6

    iget-object v8, p0, Lebv;->at:Llnl;

    .line 303
    invoke-virtual {v8}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v8

    invoke-direct {v3, v5, v6, v8, v2}, Lcpy;-><init>(Landroid/content/Context;IIZ)V

    .line 301
    invoke-virtual {v0, v1, v3}, Lcqa;->a(Lcqd;Lcqe;)V

    .line 306
    iget-object v0, p0, Lebv;->aC:Lcqa;

    sget-object v1, Lcqd;->c:Lcqd;

    new-instance v3, Lcpz;

    iget-object v5, p0, Lebv;->at:Llnl;

    invoke-direct {v3, v5}, Lcpz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v3}, Lcqa;->a(Lcqd;Lcqe;)V

    .line 309
    invoke-virtual {p0}, Lebv;->as()Lepx;

    move-result-object v0

    .line 310
    iget-object v1, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v3, p0, Lebv;->aj:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0, v1, p0, v3}, Lepx;->a(Landroid/widget/AbsListView;Landroid/widget/AbsListView$OnScrollListener;Lcom/google/android/apps/plus/views/FastScrollContainer;)V

    .line 314
    if-nez p3, :cond_2

    .line 315
    invoke-virtual {p0}, Lebv;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbb;->a(I)V

    .line 318
    :cond_2
    invoke-virtual {p0}, Lebv;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 320
    invoke-direct {p0, v7}, Lebv;->d(Landroid/view/View;)V

    .line 322
    invoke-virtual {p0}, Lebv;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lebv;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 321
    invoke-static {v0, v1, p0, p0}, Ldwg;->a(Landroid/content/Context;ILept;Legi;)[Lepm;

    move-result-object v0

    iput-object v0, p0, Lebv;->ay:[Lepm;

    .line 324
    new-instance v0, Ldwg;

    iget-object v1, p0, Lebv;->at:Llnl;

    .line 325
    invoke-virtual {p0}, Lebv;->w()Lbb;

    move-result-object v3

    iget-object v2, p0, Lebv;->P:Lhee;

    .line 326
    invoke-interface {v2}, Lhee;->d()I

    move-result v5

    iget-object v2, p0, Lebv;->Y:Lctq;

    .line 327
    invoke-virtual {v2}, Lctq;->c()I

    move-result v6

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Ldwg;-><init>(Landroid/content/Context;Lept;Lbb;III)V

    iput-object v0, p0, Lebv;->aw:Ldwg;

    .line 329
    iget-object v0, p0, Lebv;->aj:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a()V

    .line 331
    return-object v7

    :cond_3
    move v0, v2

    .line 276
    goto/16 :goto_0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lcpu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 611
    new-instance v0, Lcpw;

    iget-object v1, p0, Lebv;->at:Llnl;

    iget-object v2, p0, Lebv;->P:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lebv;->aq:Landroid/net/Uri;

    const/4 v4, 0x0

    iget v5, p0, Lebv;->as:I

    const/16 v6, 0x2710

    .line 613
    invoke-virtual {p0}, Lebv;->k()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "filter"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcpw;-><init>(Landroid/content/Context;ILandroid/net/Uri;Ljava/lang/Long;III)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const v3, 0x10040

    .line 214
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 216
    if-eqz p1, :cond_4

    .line 217
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    .line 220
    :cond_0
    const-string v0, "scroll_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    const-string v0, "scroll_position"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcoo;

    iput-object v0, p0, Lebv;->ap:Lcoo;

    .line 223
    :cond_1
    const-string v0, "load_more_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 224
    const-string v0, "load_more_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebv;->ao:Ljava/lang/Integer;

    .line 226
    :cond_2
    const-string v0, "current_offset"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 227
    const-string v0, "current_offset"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lebv;->as:I

    .line 229
    :cond_3
    const-string v0, "waiting_for_loader"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 230
    const-string v0, "waiting_for_loader"

    .line 231
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 230
    invoke-static {v0}, Lebz;->valueOf(Ljava/lang/String;)Lebz;

    move-result-object v0

    iput-object v0, p0, Lebv;->az:Lebz;

    .line 235
    :cond_4
    invoke-virtual {p0}, Lebv;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "grid_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 252
    :goto_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "all-photos-list-transform"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 254
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 255
    new-instance v1, Lcqa;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Lcqa;-><init>(Landroid/os/Looper;Lcqc;)V

    iput-object v1, p0, Lebv;->aC:Lcqa;

    .line 256
    return-void

    .line 237
    :pswitch_0
    sget-object v0, Lcqd;->b:Lcqd;

    iput-object v0, p0, Lebv;->aE:Lcqd;

    .line 238
    new-instance v0, Lcom;

    const/4 v1, 0x2

    invoke-direct {v0, v3, v1}, Lcom;-><init>(II)V

    iput-object v0, p0, Lebv;->aF:Lcom;

    goto :goto_0

    .line 243
    :pswitch_1
    sget-object v0, Lcqd;->c:Lcqd;

    iput-object v0, p0, Lebv;->aE:Lcqd;

    .line 244
    invoke-virtual {p0}, Lebv;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0386

    .line 245
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 246
    new-instance v1, Lcom;

    invoke-direct {v1, v3, v0, v0}, Lcom;-><init>(III)V

    iput-object v1, p0, Lebv;->aF:Lcom;

    goto :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/plus/views/PhotoTileView;J)V
    .locals 6

    .prologue
    .line 540
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v1

    .line 542
    invoke-virtual {p0, v1}, Lebv;->a(Lizu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    :goto_0
    return-void

    .line 548
    :cond_0
    iget-object v0, p0, Lebv;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 549
    new-instance v2, Ldew;

    invoke-virtual {p0}, Lebv;->n()Lz;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 550
    invoke-virtual {v2, v1}, Ldew;->a(Lizu;)Ldew;

    move-result-object v0

    .line 551
    invoke-virtual {v0, p2, p3}, Ldew;->a(J)Ldew;

    move-result-object v0

    iget v2, p0, Lebv;->as:I

    .line 552
    invoke-virtual {v0, v2}, Ldew;->a(I)Ldew;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    .line 555
    invoke-static {v2, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v2, p0, Lebv;->Y:Lctq;

    .line 556
    invoke-virtual {v2}, Lctq;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Ldew;->b(I)Ldew;

    move-result-object v0

    iget-object v2, p0, Lebv;->X:Lctz;

    .line 557
    invoke-virtual {v2}, Lctz;->a()Ljcn;

    move-result-object v2

    invoke-virtual {v0, v2}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-boolean v2, p0, Lebv;->ag:Z

    .line 558
    invoke-virtual {v0, v2}, Ldew;->i(Z)Ldew;

    move-result-object v0

    iget v2, p0, Lebv;->aa:I

    .line 559
    invoke-virtual {v0, v2}, Ldew;->c(I)Ldew;

    move-result-object v0

    .line 560
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v2

    .line 561
    if-eqz v1, :cond_1

    .line 562
    iget-object v0, p0, Lebv;->at:Llnl;

    const-class v3, Lizs;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    const/4 v3, 0x5

    const/16 v4, 0x1040

    invoke-virtual {v0, v1, v3, v4}, Lizs;->c(Lizu;II)Lcom/google/android/libraries/social/media/MediaResource;

    .line 568
    :cond_1
    iget-object v0, p0, Lebv;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v3, p0, Lebv;->at:Llnl;

    invoke-direct {v1, v3}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v3, Lhmv;->dZ:Lhmv;

    .line 569
    invoke-virtual {v1, v3}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 568
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 570
    invoke-virtual {p0, v2}, Lebv;->b(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Lcpu;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcpu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 618
    iget-object v0, p0, Lebv;->aC:Lcqa;

    invoke-virtual {v0, p1}, Lcqa;->a(Lcpu;)V

    .line 619
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lcpu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 696
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 93
    check-cast p2, Lcpu;

    invoke-virtual {p0, p2}, Lebv;->a(Lcpu;)V

    return-void
.end method

.method public a(Lepm;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 785
    const/4 v0, 0x1

    iput-boolean v0, p0, Lebv;->aB:Z

    .line 786
    if-eqz p1, :cond_1

    .line 787
    invoke-interface {p1}, Lepm;->e()Lepn;

    move-result-object v0

    iput-object v0, p0, Lebv;->ax:Lepn;

    .line 788
    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-interface {p1}, Lepm;->j()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcof;->a(Landroid/view/View;)V

    .line 793
    :goto_0
    invoke-virtual {p0}, Lebv;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebv;->d(Landroid/view/View;)V

    .line 794
    if-eqz p1, :cond_0

    .line 795
    invoke-interface {p1}, Lepm;->i()V

    .line 797
    :cond_0
    return-void

    .line 790
    :cond_1
    iput-object v1, p0, Lebv;->ax:Lepn;

    .line 791
    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0, v1}, Lcof;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a(Ljava/util/Map;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcqd;",
            "Lcpx;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 623
    iput-object p1, p0, Lebv;->aD:Ljava/util/Map;

    .line 624
    iget-object v0, p0, Lebv;->aD:Ljava/util/Map;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lebv;->aD:Ljava/util/Map;

    iget-object v1, p0, Lebv;->aE:Lcqd;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcpx;

    move-object v7, v0

    :goto_0
    if-eqz v7, :cond_3

    iget-object v0, v7, Lcpx;->c:Lcpu;

    move-object v1, v0

    :goto_1
    iput-boolean v3, p0, Lebv;->aA:Z

    iget-object v0, p0, Lebv;->au:Llnh;

    const-class v4, Lfew;

    invoke-virtual {v0, v4}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    iget-object v4, p0, Lebv;->P:Lhee;

    invoke-interface {v4}, Lhee;->f()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lebv;->P:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-virtual {v0, v4}, Lfew;->b(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lebv;->ar:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    iget-object v4, p0, Lebv;->aJ:Ljava/lang/Runnable;

    const-wide/16 v8, 0x1388

    invoke-static {v4, v8, v9}, Llsx;->a(Ljava/lang/Runnable;J)V

    move v4, v5

    :goto_2
    const-string v6, "AllPhotosFragment"

    const/4 v8, 0x4

    invoke-static {v6, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    if-nez v1, :cond_6

    const-string v6, "null"

    :goto_3
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, Lebv;->P:Lhee;

    invoke-interface {v8}, Lhee;->d()I

    move-result v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x38

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "Loaded "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " photos. Processing: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " AccountId: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcpu;->d()Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v4}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v4

    invoke-virtual {v1}, Lcpu;->c()Ljava/lang/Integer;

    move-result-object v6

    if-eqz v6, :cond_b

    invoke-virtual {v1}, Lcpu;->c()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lebv;->as:I

    move v1, v4

    :goto_4
    iget-object v4, p0, Lebv;->ak:Lcof;

    iget v6, p0, Lebv;->as:I

    if-eqz v6, :cond_8

    :goto_5
    invoke-virtual {v4, v7, v0, v1, v3}, Lcof;->a(Lcpx;IZZ)V

    iget-object v0, p0, Lebv;->az:Lebz;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0, v5}, Lcof;->a(Z)V

    iget-object v0, p0, Lebv;->az:Lebz;

    sget-object v1, Lebz;->a:Lebz;

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    :goto_6
    iput-object v2, p0, Lebv;->az:Lebz;

    :cond_1
    :goto_7
    invoke-virtual {p0}, Lebv;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lebv;->d(Landroid/view/View;)V

    .line 625
    return-void

    :cond_2
    move-object v7, v2

    .line 624
    goto/16 :goto_0

    :cond_3
    move-object v1, v2

    goto/16 :goto_1

    :cond_4
    invoke-direct {p0}, Lebv;->au()V

    move v0, v3

    move v4, v5

    goto/16 :goto_2

    :cond_5
    iput-boolean v3, p0, Lebv;->ar:Z

    iget-object v0, p0, Lebv;->aJ:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    move v0, v3

    move v4, v3

    goto/16 :goto_2

    :cond_6
    invoke-virtual {v1}, Lcpu;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/16 :goto_3

    :cond_7
    iput v5, p0, Lebv;->as:I

    move v1, v5

    goto :goto_4

    :cond_8
    move v3, v5

    goto :goto_5

    :cond_9
    iget-object v0, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    iget-object v1, p0, Lebv;->ak:Lcof;

    invoke-virtual {v1}, Lcof;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    goto :goto_6

    :cond_a
    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0}, Lcof;->b()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/FastScrollListView;->setSelection(I)V

    goto :goto_7

    :cond_b
    move v1, v4

    goto :goto_4
.end method

.method public a(Ljcl;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 585
    instance-of v0, p1, Ldqm;

    if-nez v0, :cond_0

    .line 607
    :goto_0
    return-void

    .line 590
    :cond_0
    check-cast p1, Ldqm;

    .line 591
    iget-object v0, p0, Lebv;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 592
    new-instance v1, Ldew;

    invoke-virtual {p0}, Lebv;->n()Lz;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ldew;-><init>(Landroid/content/Context;I)V

    .line 593
    invoke-virtual {p1}, Ldqm;->f()Lizu;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldew;->a(Lizu;)Ldew;

    move-result-object v0

    .line 594
    invoke-virtual {p1}, Ldqm;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ldew;->a(J)Ldew;

    move-result-object v0

    iget v1, p0, Lebv;->as:I

    .line 595
    invoke-virtual {v0, v1}, Ldew;->a(I)Ldew;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v4, [Ljava/lang/String;

    .line 596
    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lebv;->Y:Lctq;

    .line 597
    invoke-virtual {v1}, Lctq;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->b(I)Ldew;

    move-result-object v0

    iget-object v1, p0, Lebv;->X:Lctz;

    .line 598
    invoke-virtual {v1}, Lctz;->a()Ljcn;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v1, p0, Lebv;->Y:Lctq;

    .line 599
    invoke-virtual {v1}, Lctq;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Ldew;->a(Z)Ldew;

    move-result-object v0

    .line 600
    invoke-virtual {v0, v4}, Ldew;->i(Z)Ldew;

    move-result-object v0

    iget-boolean v1, p0, Lebv;->ac:Z

    .line 601
    invoke-virtual {v0, v1}, Ldew;->b(Z)Ldew;

    move-result-object v0

    iget-boolean v1, p0, Lebv;->ae:Z

    .line 602
    invoke-virtual {v0, v1}, Ldew;->c(Z)Ldew;

    move-result-object v0

    iget-object v1, p0, Lebv;->af:Ljava/lang/String;

    .line 603
    invoke-virtual {v0, v1}, Ldew;->e(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-boolean v1, p0, Lebv;->ad:Z

    .line 604
    invoke-virtual {v0, v1}, Ldew;->g(Z)Ldew;

    move-result-object v0

    .line 605
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v0

    .line 606
    invoke-virtual {p0, v0}, Lebv;->b(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/plus/views/PhotoTileView;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 575
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 576
    invoke-virtual {p0, p1}, Lebv;->b(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    .line 577
    invoke-virtual {p0, v0}, Lebv;->e(I)V

    .line 578
    const/4 v0, 0x1

    .line 580
    :cond_0
    return v0
.end method

.method public aO_()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 357
    invoke-super {p0}, Legi;->aO_()V

    .line 359
    iget-object v0, p0, Lebv;->aI:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 360
    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 361
    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    invoke-virtual {p0}, Lebv;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lebv;->aH:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 365
    :cond_0
    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0, v3}, Lcof;->a(Z)V

    .line 384
    :cond_1
    :goto_0
    iget-object v0, p0, Lebv;->aw:Ldwg;

    invoke-virtual {v0}, Ldwg;->a()V

    .line 386
    iget-object v0, p0, Lebv;->aj:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->a()V

    .line 387
    return-void

    .line 367
    :cond_2
    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 368
    iget-object v1, p0, Lebv;->an:Ljava/lang/Integer;

    invoke-direct {p0, v1, v0}, Lebv;->a(Ljava/lang/Integer;Lfib;)V

    goto :goto_0

    .line 370
    :cond_3
    iget-object v0, p0, Lebv;->ao:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 371
    iget-object v0, p0, Lebv;->at:Llnl;

    const-class v1, Lfew;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfew;

    .line 374
    iget-object v1, p0, Lebv;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lfew;->b(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 375
    invoke-virtual {p0}, Lebv;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lebv;->P:Lhee;

    .line 376
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    const/4 v2, 0x0

    .line 375
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;IZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    .line 377
    iget-object v0, p0, Lebv;->ak:Lcof;

    invoke-virtual {v0, v3}, Lcof;->a(Z)V

    goto :goto_0

    .line 378
    :cond_4
    iget-object v1, p0, Lebv;->at:Llnl;

    invoke-virtual {v0, v1}, Lfew;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381
    iget-object v0, p0, Lebv;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    new-instance v1, Lcoe;

    iget-object v2, p0, Lebv;->at:Llnl;

    invoke-direct {v1, v2}, Lcoe;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    goto :goto_0
.end method

.method public aa()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 747
    invoke-virtual {p0}, Lebv;->k()Landroid/os/Bundle;

    move-result-object v3

    .line 749
    iget-object v0, p0, Lebv;->at:Llnl;

    iget-object v4, p0, Lebv;->P:Lhee;

    .line 750
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    .line 749
    invoke-static {v0, v4}, Ldhv;->y(Landroid/content/Context;I)Z

    move-result v4

    .line 751
    if-eqz v3, :cond_2

    iget-object v0, p0, Lebv;->ax:Lepn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lebv;->ax:Lepn;

    sget-object v5, Lepn;->d:Lepn;

    if-ne v0, v5, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    if-nez v4, :cond_2

    .line 752
    const-string v0, "show_autobackup_status"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 754
    :goto_1
    iget-object v3, p0, Lebv;->P:Lhee;

    invoke-interface {v3}, Lhee;->f()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lebv;->P:Lhee;

    .line 755
    invoke-interface {v3}, Lhee;->g()Lhej;

    move-result-object v3

    const-string v4, "is_plus_page"

    invoke-interface {v3, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_0

    move v1, v2

    :cond_0
    return v1

    :cond_1
    move v0, v1

    .line 751
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public ab()V
    .locals 4

    .prologue
    .line 765
    iget-object v0, p0, Lebv;->ak:Lcof;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lebv;->aK:Z

    if-nez v0, :cond_0

    .line 766
    const/4 v0, 0x1

    iput-boolean v0, p0, Lebv;->aK:Z

    .line 767
    iget-object v0, p0, Lebv;->aL:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 770
    :cond_0
    return-void
.end method

.method public ac()V
    .locals 1

    .prologue
    .line 801
    const/4 v0, 0x0

    iput-object v0, p0, Lebv;->ax:Lepn;

    .line 802
    return-void
.end method

.method public ad()[Lepm;
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lebv;->ay:[Lepm;

    return-object v0
.end method

.method public b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 510
    iget-object v0, p0, Lebv;->ao:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebv;->ao:Ljava/lang/Integer;

    iget-object v1, p0, Lebv;->an:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    iput-object v2, p0, Lebv;->an:Ljava/lang/Integer;

    .line 513
    :cond_0
    iput-object v2, p0, Lebv;->ao:Ljava/lang/Integer;

    .line 515
    iget v0, p0, Lebv;->as:I

    add-int/lit16 v0, v0, -0x2710

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lebv;->as:I

    .line 516
    sget-object v0, Lebz;->b:Lebz;

    iput-object v0, p0, Lebv;->az:Lebz;

    .line 517
    invoke-virtual {p0}, Lebv;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v3, v2, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 518
    iget-object v0, p0, Lebv;->ak:Lcof;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcof;->a(Z)V

    .line 519
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 336
    invoke-super {p0, p1}, Legi;->c(Landroid/os/Bundle;)V

    .line 337
    iget-object v0, p0, Lebv;->P:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 338
    iget-object v1, p0, Lebv;->au:Llnh;

    const-class v2, Lcpi;

    .line 339
    invoke-static {v0}, Lcpi;->a(I)Lcpi;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v1

    const-class v2, Lcpj;

    new-instance v3, Lcpj;

    iget-object v4, p0, Lebv;->at:Llnl;

    invoke-direct {v3, v4, v0}, Lcpj;-><init>(Landroid/content/Context;I)V

    .line 340
    invoke-virtual {v1, v2, v3}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Leqt;

    iget-object v2, p0, Lebv;->ah:Lequ;

    .line 341
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcpt;

    .line 342
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcov;

    .line 343
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Lcoy;

    .line 344
    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    move-result-object v0

    const-class v1, Ldxq;

    iget-object v2, p0, Lebv;->ah:Lequ;

    .line 345
    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 348
    sget-object v0, Lfvc;->k:Lfvc;

    invoke-virtual {v0}, Lfvc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lebv;->au:Llnh;

    const-class v1, Levy;

    new-instance v2, Levy;

    iget-object v3, p0, Lebv;->at:Llnl;

    invoke-direct {v2, v3}, Levy;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 352
    :cond_0
    iget-object v0, p0, Lebv;->au:Llnh;

    const-class v1, Lcqf;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqf;

    iput-object v0, p0, Lebv;->aG:Lcqf;

    .line 353
    return-void
.end method

.method protected c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 503
    const/4 v0, 0x0

    return v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 435
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 436
    iget-object v0, p0, Lebv;->an:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 437
    const-string v0, "refresh_request"

    iget-object v1, p0, Lebv;->an:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 439
    :cond_0
    iget-object v0, p0, Lebv;->ao:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 440
    const-string v0, "load_more_request"

    iget-object v1, p0, Lebv;->ao:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 442
    :cond_1
    iget-object v0, p0, Lebv;->az:Lebz;

    if-eqz v0, :cond_2

    .line 443
    const-string v0, "waiting_for_loader"

    iget-object v1, p0, Lebv;->az:Lebz;

    .line 444
    invoke-virtual {v1}, Lebz;->name()Ljava/lang/String;

    move-result-object v1

    .line 443
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :cond_2
    iget-object v0, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lebv;->ak:Lcof;

    if-eqz v0, :cond_3

    .line 447
    const-string v0, "scroll_position"

    iget-object v1, p0, Lebv;->ak:Lcof;

    iget-object v2, p0, Lebv;->ai:Lcom/google/android/apps/plus/views/FastScrollListView;

    .line 448
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/FastScrollListView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcof;->b(I)Lcoo;

    move-result-object v1

    .line 447
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 451
    :cond_3
    const-string v0, "current_offset"

    iget v1, p0, Lebv;->as:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 452
    return-void
.end method

.method public h_(I)V
    .locals 3

    .prologue
    .line 774
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 775
    iget-object v0, p0, Lebv;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lebv;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eF:Lhmv;

    .line 776
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 775
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    .line 781
    :cond_0
    :goto_0
    return-void

    .line 777
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 778
    iget-object v0, p0, Lebv;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lebv;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->eE:Lhmv;

    .line 779
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 778
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lebv;->am:Lend;

    invoke-virtual {v0, p1, p2, p3, p4}, Lend;->onScroll(Landroid/widget/AbsListView;III)V

    .line 737
    iget-object v0, p0, Lebv;->al:Lend;

    invoke-virtual {v0, p1, p2, p3, p4}, Lend;->onScroll(Landroid/widget/AbsListView;III)V

    .line 738
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 724
    iget-object v0, p0, Lebv;->aG:Lcqf;

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 726
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    .line 727
    iget-object v1, p0, Lebv;->ak:Lcof;

    invoke-virtual {v1, v0}, Lcof;->b(I)Lcoo;

    move-result-object v0

    .line 728
    if-eqz v0, :cond_0

    .line 729
    iget-object v0, p0, Lebv;->aG:Lcqf;

    .line 732
    :cond_0
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 391
    invoke-super {p0}, Legi;->z()V

    .line 393
    iget-object v0, p0, Lebv;->al:Lend;

    invoke-virtual {v0}, Lend;->a()V

    .line 394
    iget-object v0, p0, Lebv;->am:Lend;

    invoke-virtual {v0}, Lend;->a()V

    .line 395
    iget-object v0, p0, Lebv;->aj:Lcom/google/android/apps/plus/views/FastScrollContainer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/FastScrollContainer;->b()V

    .line 397
    iget-object v0, p0, Lebv;->aJ:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 398
    iget-object v0, p0, Lebv;->aI:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 399
    return-void
.end method
