.class final Lfok;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lmms;


# direct methods
.method constructor <init>(Lmms;)V
    .locals 2

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    if-nez p1, :cond_0

    .line 166
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot wrap a null StoryElementRef"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_0
    iput-object p1, p0, Lfok;->a:Lmms;

    .line 169
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 177
    instance-of v0, p1, Lfok;

    if-nez v0, :cond_0

    .line 178
    const/4 v0, 0x0

    .line 180
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfok;->a:Lmms;

    check-cast p1, Lfok;

    .line 181
    iget-object v1, p1, Lfok;->a:Lmms;

    .line 180
    invoke-static {v0, v1}, Loxu;->a(Loxu;Loxu;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 186
    const/16 v0, 0x11

    .line 187
    iget-object v1, p0, Lfok;->a:Lmms;

    iget-object v1, v1, Lmms;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 188
    iget-object v0, p0, Lfok;->a:Lmms;

    iget-object v0, v0, Lmms;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 190
    :cond_0
    iget-object v1, p0, Lfok;->a:Lmms;

    iget-object v1, v1, Lmms;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 191
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lfok;->a:Lmms;

    iget-object v1, v1, Lmms;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_1
    iget-object v1, p0, Lfok;->a:Lmms;

    iget-object v1, v1, Lmms;->d:Lmmm;

    if-eqz v1, :cond_2

    .line 195
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lfok;->a:Lmms;

    iget-object v1, v1, Lmms;->d:Lmmm;

    iget-object v1, v1, Lmmm;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    :cond_2
    iget-object v1, p0, Lfok;->a:Lmms;

    iget-object v1, v1, Lmms;->e:Lmmd;

    if-eqz v1, :cond_3

    .line 198
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lfok;->a:Lmms;

    iget-object v1, v1, Lmms;->e:Lmmd;

    iget-object v1, v1, Lmmd;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_3
    return v0
.end method
