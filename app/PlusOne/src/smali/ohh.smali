.class public final Lohh;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lohh;


# instance fields
.field public b:Lohp;

.field public c:Lohi;

.field private d:Lohj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6799
    const/4 v0, 0x0

    new-array v0, v0, [Lohh;

    sput-object v0, Lohh;->a:[Lohh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6800
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6803
    iput-object v0, p0, Lohh;->b:Lohp;

    .line 6806
    iput-object v0, p0, Lohh;->c:Lohi;

    .line 6809
    iput-object v0, p0, Lohh;->d:Lohj;

    .line 6800
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6829
    const/4 v0, 0x0

    .line 6830
    iget-object v1, p0, Lohh;->b:Lohp;

    if-eqz v1, :cond_0

    .line 6831
    const/4 v0, 0x1

    iget-object v1, p0, Lohh;->b:Lohp;

    .line 6832
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6834
    :cond_0
    iget-object v1, p0, Lohh;->c:Lohi;

    if-eqz v1, :cond_1

    .line 6835
    const/4 v1, 0x2

    iget-object v2, p0, Lohh;->c:Lohi;

    .line 6836
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6838
    :cond_1
    iget-object v1, p0, Lohh;->d:Lohj;

    if-eqz v1, :cond_2

    .line 6839
    const/4 v1, 0x3

    iget-object v2, p0, Lohh;->d:Lohj;

    .line 6840
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6842
    :cond_2
    iget-object v1, p0, Lohh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6843
    iput v0, p0, Lohh;->ai:I

    .line 6844
    return v0
.end method

.method public a(Loxn;)Lohh;
    .locals 2

    .prologue
    .line 6852
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6853
    sparse-switch v0, :sswitch_data_0

    .line 6857
    iget-object v1, p0, Lohh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6858
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lohh;->ah:Ljava/util/List;

    .line 6861
    :cond_1
    iget-object v1, p0, Lohh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6863
    :sswitch_0
    return-object p0

    .line 6868
    :sswitch_1
    iget-object v0, p0, Lohh;->b:Lohp;

    if-nez v0, :cond_2

    .line 6869
    new-instance v0, Lohp;

    invoke-direct {v0}, Lohp;-><init>()V

    iput-object v0, p0, Lohh;->b:Lohp;

    .line 6871
    :cond_2
    iget-object v0, p0, Lohh;->b:Lohp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6875
    :sswitch_2
    iget-object v0, p0, Lohh;->c:Lohi;

    if-nez v0, :cond_3

    .line 6876
    new-instance v0, Lohi;

    invoke-direct {v0}, Lohi;-><init>()V

    iput-object v0, p0, Lohh;->c:Lohi;

    .line 6878
    :cond_3
    iget-object v0, p0, Lohh;->c:Lohi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6882
    :sswitch_3
    iget-object v0, p0, Lohh;->d:Lohj;

    if-nez v0, :cond_4

    .line 6883
    new-instance v0, Lohj;

    invoke-direct {v0}, Lohj;-><init>()V

    iput-object v0, p0, Lohh;->d:Lohj;

    .line 6885
    :cond_4
    iget-object v0, p0, Lohh;->d:Lohj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6853
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6814
    iget-object v0, p0, Lohh;->b:Lohp;

    if-eqz v0, :cond_0

    .line 6815
    const/4 v0, 0x1

    iget-object v1, p0, Lohh;->b:Lohp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6817
    :cond_0
    iget-object v0, p0, Lohh;->c:Lohi;

    if-eqz v0, :cond_1

    .line 6818
    const/4 v0, 0x2

    iget-object v1, p0, Lohh;->c:Lohi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6820
    :cond_1
    iget-object v0, p0, Lohh;->d:Lohj;

    if-eqz v0, :cond_2

    .line 6821
    const/4 v0, 0x3

    iget-object v1, p0, Lohh;->d:Lohj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6823
    :cond_2
    iget-object v0, p0, Lohh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6825
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6796
    invoke-virtual {p0, p1}, Lohh;->a(Loxn;)Lohh;

    move-result-object v0

    return-object v0
.end method
