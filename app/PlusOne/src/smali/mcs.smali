.class public final Lmcs;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnld;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 565
    invoke-direct {p0}, Loxq;-><init>()V

    .line 568
    iput-object v0, p0, Lmcs;->apiHeader:Llyq;

    .line 571
    iput-object v0, p0, Lmcs;->a:Lnld;

    .line 565
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 588
    const/4 v0, 0x0

    .line 589
    iget-object v1, p0, Lmcs;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 590
    const/4 v0, 0x1

    iget-object v1, p0, Lmcs;->apiHeader:Llyq;

    .line 591
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 593
    :cond_0
    iget-object v1, p0, Lmcs;->a:Lnld;

    if-eqz v1, :cond_1

    .line 594
    const/4 v1, 0x2

    iget-object v2, p0, Lmcs;->a:Lnld;

    .line 595
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 597
    :cond_1
    iget-object v1, p0, Lmcs;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 598
    iput v0, p0, Lmcs;->ai:I

    .line 599
    return v0
.end method

.method public a(Loxn;)Lmcs;
    .locals 2

    .prologue
    .line 607
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 608
    sparse-switch v0, :sswitch_data_0

    .line 612
    iget-object v1, p0, Lmcs;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 613
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcs;->ah:Ljava/util/List;

    .line 616
    :cond_1
    iget-object v1, p0, Lmcs;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 618
    :sswitch_0
    return-object p0

    .line 623
    :sswitch_1
    iget-object v0, p0, Lmcs;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 624
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmcs;->apiHeader:Llyq;

    .line 626
    :cond_2
    iget-object v0, p0, Lmcs;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 630
    :sswitch_2
    iget-object v0, p0, Lmcs;->a:Lnld;

    if-nez v0, :cond_3

    .line 631
    new-instance v0, Lnld;

    invoke-direct {v0}, Lnld;-><init>()V

    iput-object v0, p0, Lmcs;->a:Lnld;

    .line 633
    :cond_3
    iget-object v0, p0, Lmcs;->a:Lnld;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 608
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, Lmcs;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 577
    const/4 v0, 0x1

    iget-object v1, p0, Lmcs;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 579
    :cond_0
    iget-object v0, p0, Lmcs;->a:Lnld;

    if-eqz v0, :cond_1

    .line 580
    const/4 v0, 0x2

    iget-object v1, p0, Lmcs;->a:Lnld;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 582
    :cond_1
    iget-object v0, p0, Lmcs;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 584
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 561
    invoke-virtual {p0, p1}, Lmcs;->a(Loxn;)Lmcs;

    move-result-object v0

    return-object v0
.end method
