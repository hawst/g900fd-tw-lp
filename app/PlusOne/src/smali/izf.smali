.class public final Lizf;
.super Llol;
.source "PG"

# interfaces
.implements Lhep;
.implements Lkhf;


# instance fields
.field private final N:Lhkd;

.field private final O:Lkhe;

.field private P:Landroid/content/Intent;

.field private Q:Lkhr;

.field private R:Lizi;

.field private S:Lhee;

.field private T:Lhei;

.field private U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

.field private V:I

.field private W:Lhke;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 83
    invoke-direct {p0}, Llol;-><init>()V

    .line 40
    new-instance v0, Lizg;

    invoke-direct {v0, p0}, Lizg;-><init>(Lizf;)V

    iput-object v0, p0, Lizf;->N:Lhkd;

    .line 55
    new-instance v0, Lkhe;

    iget-object v1, p0, Lizf;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lkhe;-><init>(Lkhf;Llqr;)V

    iput-object v0, p0, Lizf;->O:Lkhe;

    .line 63
    const/4 v0, 0x0

    iput v0, p0, Lizf;->V:I

    .line 64
    new-instance v0, Lhke;

    iget-object v1, p0, Lizf;->av:Llqm;

    invoke-direct {v0, v1}, Lhke;-><init>(Llqr;)V

    iget-object v1, p0, Lizf;->au:Llnh;

    .line 65
    invoke-virtual {v0, v1}, Lhke;->a(Llnh;)Lhke;

    move-result-object v0

    const v1, 0x7f100051

    iget-object v2, p0, Lizf;->N:Lhkd;

    .line 66
    invoke-virtual {v0, v1, v2}, Lhke;->a(ILhkd;)Lhke;

    move-result-object v0

    iput-object v0, p0, Lizf;->W:Lhke;

    .line 83
    return-void
.end method

.method static synthetic a(Lizf;I)I
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lizf;->V:I

    return p1
.end method

.method static synthetic a(Lizf;)Lizi;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lizf;->R:Lizi;

    return-object v0
.end method

.method private a(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V
    .locals 7

    .prologue
    .line 173
    iget-object v0, p0, Lizf;->at:Llnl;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 174
    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v1

    .line 176
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 177
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v0, v2}, Lhei;->a(I)Lhej;

    move-result-object v4

    .line 178
    invoke-interface {v4}, Lhej;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 179
    iget-object v2, p0, Lizf;->P:Landroid/content/Intent;

    if-eqz v2, :cond_1

    .line 180
    iget-object v2, p0, Lizf;->P:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 181
    const-string v5, "account_id"

    invoke-virtual {v2, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 182
    iget-object v1, p0, Lizf;->Q:Lkhr;

    const-string v5, "display_name"

    .line 183
    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "account_name"

    invoke-interface {v4, v6}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 182
    invoke-virtual {v1, v5, v4, v2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    goto :goto_0

    .line 186
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must set intent for accounts to be visible"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_2
    return-void
.end method

.method static synthetic b(Lizf;)Lcom/google/android/libraries/social/settings/PreferenceCategory;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    return-object v0
.end method

.method static synthetic c(Lizf;)Llnl;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lizf;->at:Llnl;

    return-object v0
.end method

.method static synthetic d(Lizf;)Lhke;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lizf;->W:Lhke;

    return-object v0
.end method

.method private d()Lkhl;
    .locals 3

    .prologue
    .line 156
    iget-object v0, p0, Lizf;->Q:Lkhr;

    const v1, 0x7f0a03d8

    .line 157
    invoke-virtual {p0, v1}, Lizf;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 156
    invoke-virtual {v0, v1, v2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    .line 158
    new-instance v1, Lizh;

    invoke-direct {v1, p0}, Lizh;-><init>(Lizf;)V

    invoke-virtual {v0, v1}, Lkhl;->a(Lkhq;)V

    .line 169
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 194
    new-instance v0, Lkhr;

    iget-object v1, p0, Lizf;->at:Llnl;

    invoke-direct {v0, v1}, Lkhr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lizf;->Q:Lkhr;

    .line 195
    iget-object v0, p0, Lizf;->Q:Lkhr;

    const v1, 0x7f0a03d7

    invoke-virtual {p0, v1}, Lizf;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkhr;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    iput-object v0, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    iget-object v0, p0, Lizf;->O:Lkhe;

    iget-object v1, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-virtual {v0, v1}, Lkhe;->a(Lkhl;)Lkhl;

    iget-object v0, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-direct {p0, v0}, Lizf;->a(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    iget-object v0, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->d()I

    move-result v0

    iput v0, p0, Lizf;->V:I

    iget-object v0, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-direct {p0}, Lizf;->d()Lkhl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    .line 196
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 98
    if-eqz p1, :cond_0

    .line 99
    const-string v0, "intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lizf;->P:Landroid/content/Intent;

    .line 100
    const-string v0, "state_account_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lizf;->V:I

    .line 102
    :cond_0
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Llol;->aO_()V

    .line 116
    invoke-virtual {p0}, Lizf;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lizf;->S:Lhee;

    .line 117
    invoke-interface {v0}, Lhee;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lizf;->S:Lhee;

    .line 118
    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lizf;->R:Lizi;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lizf;->R:Lizi;

    invoke-interface {v0}, Lizi;->k()V

    .line 123
    :cond_0
    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lizf;->P:Landroid/content/Intent;

    .line 92
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 107
    iget-object v0, p0, Lizf;->au:Llnh;

    const-class v1, Lizi;

    invoke-virtual {v0, v1}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizi;

    iput-object v0, p0, Lizf;->R:Lizi;

    .line 108
    iget-object v0, p0, Lizf;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lizf;->S:Lhee;

    .line 109
    iget-object v0, p0, Lizf;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lizf;->T:Lhei;

    .line 110
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 141
    const-string v0, "intent"

    iget-object v1, p0, Lizf;->P:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 142
    const-string v0, "state_account_count"

    iget v1, p0, Lizf;->V:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0}, Llol;->g()V

    .line 128
    iget-object v0, p0, Lizf;->T:Lhei;

    invoke-interface {v0, p0}, Lhei;->a(Lhep;)V

    .line 129
    invoke-virtual {p0}, Lizf;->l()V

    .line 130
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Llol;->h()V

    .line 135
    iget-object v0, p0, Lizf;->T:Lhei;

    invoke-interface {v0, p0}, Lhei;->b(Lhep;)V

    .line 136
    return-void
.end method

.method public l()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 201
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lizf;->T:Lhei;

    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, Lizf;->T:Lhei;

    invoke-interface {v4, v0}, Lhei;->a(I)Lhej;

    move-result-object v4

    invoke-interface {v4}, Lhej;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lhed;

    iget-object v3, p0, Lizf;->T:Lhei;

    invoke-direct {v0, v3}, Lhed;-><init>(Lhei;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    invoke-virtual {p0}, Lizf;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 220
    :goto_1
    return-void

    .line 207
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 208
    :goto_2
    iget-object v3, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->d()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 209
    iget-object v3, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->a(I)Lkhl;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 213
    :cond_3
    :goto_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 214
    iget-object v3, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkhl;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->d(Lkhl;)Z

    .line 213
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 218
    :cond_4
    iget-object v0, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-direct {p0, v0}, Lizf;->a(Lcom/google/android/libraries/social/settings/PreferenceCategory;)V

    .line 219
    iget-object v0, p0, Lizf;->U:Lcom/google/android/libraries/social/settings/PreferenceCategory;

    invoke-direct {p0}, Lizf;->d()Lkhl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->c(Lkhl;)Z

    goto :goto_1
.end method
