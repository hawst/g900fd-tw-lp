.class public final Lows;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lows;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Lowt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    new-array v0, v0, [Lows;

    sput-object v0, Lows;->a:[Lows;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 156
    invoke-direct {p0}, Loxq;-><init>()V

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lows;->c:Lowt;

    .line 156
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 264
    const/4 v0, 0x0

    .line 265
    iget-object v1, p0, Lows;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 266
    const/4 v0, 0x1

    iget-object v1, p0, Lows;->b:Ljava/lang/String;

    .line 267
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 269
    :cond_0
    iget-object v1, p0, Lows;->c:Lowt;

    if-eqz v1, :cond_1

    .line 270
    const/4 v1, 0x2

    iget-object v2, p0, Lows;->c:Lowt;

    .line 271
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_1
    iget-object v1, p0, Lows;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    iput v0, p0, Lows;->ai:I

    .line 275
    return v0
.end method

.method public a(Loxn;)Lows;
    .locals 2

    .prologue
    .line 283
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 284
    sparse-switch v0, :sswitch_data_0

    .line 288
    iget-object v1, p0, Lows;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 289
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lows;->ah:Ljava/util/List;

    .line 292
    :cond_1
    iget-object v1, p0, Lows;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    :sswitch_0
    return-object p0

    .line 299
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lows;->b:Ljava/lang/String;

    goto :goto_0

    .line 303
    :sswitch_2
    iget-object v0, p0, Lows;->c:Lowt;

    if-nez v0, :cond_2

    .line 304
    new-instance v0, Lowt;

    invoke-direct {v0}, Lowt;-><init>()V

    iput-object v0, p0, Lows;->c:Lowt;

    .line 306
    :cond_2
    iget-object v0, p0, Lows;->c:Lowt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 284
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lows;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 253
    const/4 v0, 0x1

    iget-object v1, p0, Lows;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 255
    :cond_0
    iget-object v0, p0, Lows;->c:Lowt;

    if-eqz v0, :cond_1

    .line 256
    const/4 v0, 0x2

    iget-object v1, p0, Lows;->c:Lowt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 258
    :cond_1
    iget-object v0, p0, Lows;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 260
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0, p1}, Lows;->a(Loxn;)Lows;

    move-result-object v0

    return-object v0
.end method
