.class Liz;
.super Liy;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 752
    invoke-direct {p0}, Liy;-><init>()V

    return-void
.end method


# virtual methods
.method public a(III)I
    .locals 1

    .prologue
    .line 779
    invoke-static {p1, p2, p3}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v0

    return v0
.end method

.method a()J
    .locals 2

    .prologue
    .line 755
    invoke-static {}, Landroid/animation/ValueAnimator;->getFrameDelay()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 763
    invoke-virtual {p1, p2, p3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 764
    return-void
.end method

.method public b(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 803
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 804
    return-void
.end method

.method public c(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 807
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    .line 808
    return-void
.end method

.method public d(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 811
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    .line 812
    return-void
.end method

.method public e(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 835
    invoke-virtual {p1, p2}, Landroid/view/View;->setScaleX(F)V

    .line 836
    return-void
.end method

.method public f(Landroid/view/View;F)V
    .locals 0

    .prologue
    .line 839
    invoke-virtual {p1, p2}, Landroid/view/View;->setScaleY(F)V

    .line 840
    return-void
.end method

.method public h(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 791
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredState()I

    move-result v0

    return v0
.end method

.method public i(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 799
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    return v0
.end method

.method public n(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 894
    invoke-virtual {p1}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 895
    return-void
.end method
