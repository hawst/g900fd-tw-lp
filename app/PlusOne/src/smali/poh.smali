.class public final Lpoh;
.super Loxq;
.source "PG"


# instance fields
.field private a:[Lpog;

.field private b:[Lpog;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Loxq;-><init>()V

    .line 183
    sget-object v0, Lpog;->a:[Lpog;

    iput-object v0, p0, Lpoh;->a:[Lpog;

    .line 186
    sget-object v0, Lpog;->a:[Lpog;

    iput-object v0, p0, Lpoh;->b:[Lpog;

    .line 180
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 216
    .line 217
    iget-object v0, p0, Lpoh;->a:[Lpog;

    if-eqz v0, :cond_1

    .line 218
    iget-object v3, p0, Lpoh;->a:[Lpog;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 219
    if-eqz v5, :cond_0

    .line 220
    const/4 v6, 0x1

    .line 221
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 218
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 225
    :cond_2
    iget-object v2, p0, Lpoh;->b:[Lpog;

    if-eqz v2, :cond_4

    .line 226
    iget-object v2, p0, Lpoh;->b:[Lpog;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 227
    if-eqz v4, :cond_3

    .line 228
    const/4 v5, 0x2

    .line 229
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 226
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 233
    :cond_4
    iget-object v1, p0, Lpoh;->c:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 234
    const/4 v1, 0x3

    iget-object v2, p0, Lpoh;->c:Ljava/lang/String;

    .line 235
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_5
    iget-object v1, p0, Lpoh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    iput v0, p0, Lpoh;->ai:I

    .line 239
    return v0
.end method

.method public a(Loxn;)Lpoh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 247
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 248
    sparse-switch v0, :sswitch_data_0

    .line 252
    iget-object v2, p0, Lpoh;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 253
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lpoh;->ah:Ljava/util/List;

    .line 256
    :cond_1
    iget-object v2, p0, Lpoh;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    :sswitch_0
    return-object p0

    .line 263
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 264
    iget-object v0, p0, Lpoh;->a:[Lpog;

    if-nez v0, :cond_3

    move v0, v1

    .line 265
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpog;

    .line 266
    iget-object v3, p0, Lpoh;->a:[Lpog;

    if-eqz v3, :cond_2

    .line 267
    iget-object v3, p0, Lpoh;->a:[Lpog;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 269
    :cond_2
    iput-object v2, p0, Lpoh;->a:[Lpog;

    .line 270
    :goto_2
    iget-object v2, p0, Lpoh;->a:[Lpog;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 271
    iget-object v2, p0, Lpoh;->a:[Lpog;

    new-instance v3, Lpog;

    invoke-direct {v3}, Lpog;-><init>()V

    aput-object v3, v2, v0

    .line 272
    iget-object v2, p0, Lpoh;->a:[Lpog;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 273
    invoke-virtual {p1}, Loxn;->a()I

    .line 270
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 264
    :cond_3
    iget-object v0, p0, Lpoh;->a:[Lpog;

    array-length v0, v0

    goto :goto_1

    .line 276
    :cond_4
    iget-object v2, p0, Lpoh;->a:[Lpog;

    new-instance v3, Lpog;

    invoke-direct {v3}, Lpog;-><init>()V

    aput-object v3, v2, v0

    .line 277
    iget-object v2, p0, Lpoh;->a:[Lpog;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 281
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 282
    iget-object v0, p0, Lpoh;->b:[Lpog;

    if-nez v0, :cond_6

    move v0, v1

    .line 283
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lpog;

    .line 284
    iget-object v3, p0, Lpoh;->b:[Lpog;

    if-eqz v3, :cond_5

    .line 285
    iget-object v3, p0, Lpoh;->b:[Lpog;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 287
    :cond_5
    iput-object v2, p0, Lpoh;->b:[Lpog;

    .line 288
    :goto_4
    iget-object v2, p0, Lpoh;->b:[Lpog;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    .line 289
    iget-object v2, p0, Lpoh;->b:[Lpog;

    new-instance v3, Lpog;

    invoke-direct {v3}, Lpog;-><init>()V

    aput-object v3, v2, v0

    .line 290
    iget-object v2, p0, Lpoh;->b:[Lpog;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 291
    invoke-virtual {p1}, Loxn;->a()I

    .line 288
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 282
    :cond_6
    iget-object v0, p0, Lpoh;->b:[Lpog;

    array-length v0, v0

    goto :goto_3

    .line 294
    :cond_7
    iget-object v2, p0, Lpoh;->b:[Lpog;

    new-instance v3, Lpog;

    invoke-direct {v3}, Lpog;-><init>()V

    aput-object v3, v2, v0

    .line 295
    iget-object v2, p0, Lpoh;->b:[Lpog;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 299
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpoh;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 248
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 193
    iget-object v1, p0, Lpoh;->a:[Lpog;

    if-eqz v1, :cond_1

    .line 194
    iget-object v2, p0, Lpoh;->a:[Lpog;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 195
    if-eqz v4, :cond_0

    .line 196
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 194
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 200
    :cond_1
    iget-object v1, p0, Lpoh;->b:[Lpog;

    if-eqz v1, :cond_3

    .line 201
    iget-object v1, p0, Lpoh;->b:[Lpog;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 202
    if-eqz v3, :cond_2

    .line 203
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 201
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 207
    :cond_3
    iget-object v0, p0, Lpoh;->c:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 208
    const/4 v0, 0x3

    iget-object v1, p0, Lpoh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 210
    :cond_4
    iget-object v0, p0, Lpoh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 212
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0, p1}, Lpoh;->a(Loxn;)Lpoh;

    move-result-object v0

    return-object v0
.end method
