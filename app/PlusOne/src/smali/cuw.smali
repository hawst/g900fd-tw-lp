.class final Lcuw;
.super Ljava/lang/Thread;
.source "PG"


# instance fields
.field private final a:Lcut;

.field private final b:Lcut;

.field private final c:Landroid/content/Context;

.field private final d:Lcus;

.field private final e:Z

.field private volatile f:Z

.field private volatile g:Lcuy;

.field private volatile h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcus;Lcut;Lcut;)V
    .locals 1

    .prologue
    .line 138
    const-string v0, "allphotos_local_media_sync"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 139
    iput-object p1, p0, Lcuw;->c:Landroid/content/Context;

    .line 140
    iput-boolean p2, p0, Lcuw;->e:Z

    .line 141
    iput-object p3, p0, Lcuw;->d:Lcus;

    .line 142
    iput-object p4, p0, Lcuw;->a:Lcut;

    .line 143
    iput-object p5, p0, Lcuw;->b:Lcut;

    .line 144
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lfhq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v12, 0x3

    const/4 v6, 0x0

    .line 207
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v8

    .line 209
    iget-boolean v0, p0, Lcuw;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcuw;->c:Landroid/content/Context;

    invoke-static {v0}, Lhrx;->a(Landroid/content/Context;)Lhrx;

    move-result-object v4

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v1, v6

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhq;

    invoke-virtual {v0}, Lfhq;->a()I

    move-result v2

    invoke-virtual {v0}, Lfhq;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v7, v1

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    iget-boolean v0, p0, Lcuw;->f:Z

    if-eqz v0, :cond_2

    const-string v0, "MediaSync"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x2c

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "cancelled, returning, total run: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 211
    :cond_0
    const-string v0, "MediaSync"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 212
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 213
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhq;

    .line 214
    invoke-virtual {v0}, Lfhq;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 209
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    new-instance v0, Lcuy;

    iget-object v1, p0, Lcuw;->c:Landroid/content/Context;

    iget-object v5, p0, Lcuw;->d:Lcus;

    invoke-direct/range {v0 .. v5}, Lcuy;-><init>(Landroid/content/Context;ILandroid/net/Uri;Lhrx;Lcus;)V

    iput-object v0, p0, Lcuw;->g:Lcuy;

    iget-object v0, p0, Lcuw;->g:Lcuy;

    invoke-virtual {v0}, Lcuy;->b()V

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_3
    move v1, v7

    goto/16 :goto_1

    .line 216
    :cond_4
    const-string v0, "processNewLocalMediaAndAccounts, duration: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 217
    invoke-static {v8, v9}, Llse;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcuw;->f:Z

    .line 219
    invoke-static {v1}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2f

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v5, v7

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v5, v7

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", cancelled: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", updated uris and accounts: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    :cond_5
    iget-boolean v0, p0, Lcuw;->f:Z

    if-nez v0, :cond_6

    const/4 v6, 0x1

    :cond_6
    iput-boolean v6, p0, Lcuw;->h:Z

    .line 223
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcuw;->e:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcuw;->h:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 177
    iget-boolean v0, p0, Lcuw;->f:Z

    return v0
.end method

.method public d()Lcut;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcuw;->b:Lcut;

    return-object v0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcuw;->f:Z

    .line 192
    iget-object v0, p0, Lcuw;->g:Lcuy;

    .line 193
    if-eqz v0, :cond_0

    .line 194
    invoke-virtual {v0}, Lcuy;->a()V

    .line 196
    :cond_0
    const-string v1, "MediaSync"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Cancelling media store synchronizer current uri synchronizer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    :cond_1
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 148
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcuw;->setPriority(I)V

    .line 149
    iget-boolean v0, p0, Lcuw;->f:Z

    if-eqz v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    iget-object v0, p0, Lcuw;->b:Lcut;

    iget-object v1, p0, Lcuw;->a:Lcut;

    .line 153
    invoke-virtual {v0, v1}, Lcut;->a(Lcut;)Ljava/util/List;

    move-result-object v0

    .line 155
    invoke-virtual {p0, v0}, Lcuw;->a(Ljava/util/List;)V

    .line 157
    iget-boolean v1, p0, Lcuw;->f:Z

    if-nez v1, :cond_0

    .line 158
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 159
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhq;

    .line 160
    invoke-virtual {v0}, Lfhq;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 163
    :cond_2
    iget-object v0, p0, Lcuw;->b:Lcut;

    invoke-virtual {v0}, Lcut;->a()V

    .line 164
    iget-object v0, p0, Lcuw;->d:Lcus;

    invoke-interface {v0, v1}, Lcus;->a(Ljava/util/List;)V

    goto :goto_0
.end method
