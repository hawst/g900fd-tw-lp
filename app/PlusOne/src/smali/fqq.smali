.class public final Lfqq;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/view/View$OnClickListener;

.field private final c:I

.field private final d:I

.field private final e:Lmms;

.field private final f:Lfsu;

.field private g:Lmmq;

.field private h:Lfod;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lfqr;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lfoi;

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Lmms;Lfsu;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lfqq;->k:I

    .line 67
    iput-object p1, p0, Lfqq;->a:Landroid/content/Context;

    .line 68
    iput-object p2, p0, Lfqq;->b:Landroid/view/View$OnClickListener;

    .line 69
    new-instance v0, Ljvl;

    iget-object v1, p0, Lfqq;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v0, v0, Ljvl;->a:I

    iput v0, p0, Lfqq;->c:I

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00d4

    .line 71
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lfqq;->d:I

    .line 72
    iput-object p3, p0, Lfqq;->e:Lmms;

    .line 73
    iput-object p4, p0, Lfqq;->f:Lfsu;

    .line 74
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)I
    .locals 3

    .prologue
    .line 274
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lfqq;->d:I

    iget v2, p0, Lfqq;->c:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    iget v1, p0, Lfqq;->c:I

    div-int/2addr v0, v1

    return v0
.end method

.method private a(Landroid/view/ViewGroup;Lfqr;)V
    .locals 4

    .prologue
    .line 302
    const/4 v0, 0x0

    .line 303
    iget-object v1, p2, Lfqr;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoh;

    .line 304
    add-int/lit8 v2, v1, 0x1

    .line 305
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;

    .line 306
    invoke-direct {p0, v1, v0}, Lfqq;->a(Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;Lfoh;)V

    move v1, v2

    .line 307
    goto :goto_0

    .line 308
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;Lfoh;)V
    .locals 16

    .prologue
    .line 311
    invoke-virtual/range {p2 .. p2}, Lfoh;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 362
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Unsupported story element type"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 313
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Lfoh;->c()Lmmc;

    move-result-object v2

    .line 314
    iget v3, v2, Lmmc;->b:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 315
    iget-object v2, v2, Lmmc;->d:Lmme;

    iget-object v2, v2, Lmme;->b:Ljava/lang/String;

    .line 316
    move-object/from16 v0, p0

    iget-object v3, v0, Lfqq;->a:Landroid/content/Context;

    sget-object v4, Ljac;->a:Ljac;

    invoke-static {v3, v2, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Lizu;)V

    .line 364
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lfqq;->j:Lfoi;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lfoi;->a(Lfoh;)Z

    move-result v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->setSelected(Z)V

    .line 365
    const v2, 0x7f10003e

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->setTag(ILjava/lang/Object;)V

    .line 366
    move-object/from16 v0, p0

    iget-object v2, v0, Lfqq;->b:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 367
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->setVisibility(I)V

    .line 368
    return-void

    .line 317
    :cond_1
    iget v3, v2, Lmmc;->b:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6

    .line 318
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 319
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 320
    move-object/from16 v0, p0

    iget-object v5, v0, Lfqq;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    .line 321
    move-object/from16 v0, p0

    iget-object v6, v0, Lfqq;->g:Lmmq;

    iget-object v2, v2, Lmmc;->e:Lmmj;

    iget-object v2, v2, Lmmj;->a:Lmon;

    iget-object v7, v2, Lmon;->a:Lmmh;

    iget-object v7, v7, Lmmh;->a:Ljava/lang/Long;

    iget-object v2, v2, Lmon;->b:Lmmh;

    iget-object v8, v2, Lmmh;->a:Ljava/lang/Long;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v6, Lmmq;->d:Lmmi;

    iget-object v6, v2, Lmmi;->a:[Lmmk;

    array-length v10, v6

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v10, :cond_3

    aget-object v11, v6, v2

    iget-object v12, v11, Lmmk;->c:Lmon;

    iget-object v12, v12, Lmon;->b:Lmmh;

    iget-object v12, v12, Lmmh;->a:Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    cmp-long v12, v12, v14

    if-gtz v12, :cond_3

    iget-object v12, v11, Lmmk;->c:Lmon;

    iget-object v12, v12, Lmon;->a:Lmmh;

    iget-object v12, v12, Lmmh;->a:Ljava/lang/Long;

    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    cmp-long v12, v12, v14

    if-ltz v12, :cond_2

    invoke-interface {v9, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-static {v2, v7}, Lfsp;->a(II)Ling;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    const/4 v7, 0x1

    if-le v2, v7, :cond_4

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmmk;

    iget-object v2, v2, Lmmk;->b:Lmmf;

    iget-object v2, v2, Lmmf;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmmk;

    iget-object v2, v2, Lmmk;->b:Lmmf;

    iget-object v2, v2, Lmmf;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v7, v2}, Lfsp;->a(II)Ling;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-static {v6, v3, v4, v5}, Lfsp;->a(Ljava/util/List;IIF)Ljava/lang/String;

    move-result-object v2

    .line 323
    move-object/from16 v0, p0

    iget-object v3, v0, Lfqq;->a:Landroid/content/Context;

    sget-object v4, Ljac;->a:Ljac;

    invoke-static {v3, v2, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Lizu;)V

    goto/16 :goto_0

    .line 321
    :cond_5
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmmk;

    iget-object v2, v2, Lmmk;->b:Lmmf;

    iget-object v2, v2, Lmmf;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v2, 0x0

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmmk;

    iget-object v2, v2, Lmmk;->b:Lmmf;

    iget-object v2, v2, Lmmf;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v7, v2}, Lfsp;->a(II)Ling;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 325
    :cond_6
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Unsupported enrichment type"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 330
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Lfoh;->b()Lmml;

    move-result-object v3

    .line 331
    iget v2, v3, Lmml;->c:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_8

    .line 332
    iget-object v2, v3, Lmml;->d:Lnzx;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfqq;->h:Lfod;

    invoke-static {v2, v4}, Lfss;->a(Lnzx;Lfod;)Ljava/lang/String;

    move-result-object v4

    .line 333
    iget-object v2, v3, Lmml;->d:Lnzx;

    sget-object v5, Lnzu;->a:Loxr;

    invoke-virtual {v2, v5}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnzu;

    iget-object v5, v2, Lnzu;->b:Lnym;

    .line 334
    if-nez v5, :cond_7

    const/4 v2, 0x0

    .line 335
    :goto_3
    move-object/from16 v0, p0

    iget-object v6, v0, Lfqq;->a:Landroid/content/Context;

    sget-object v7, Ljac;->a:Ljac;

    .line 336
    invoke-static {v6, v4, v7}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v6

    .line 335
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Lizu;)V

    .line 337
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Ljac;)V

    .line 338
    iget-object v5, v5, Lnym;->K:Ljava/lang/Boolean;

    invoke-static {v5}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->b(Z)V

    .line 340
    if-eqz v3, :cond_0

    iget-object v5, v3, Lmml;->d:Lnzx;

    if-eqz v5, :cond_0

    iget-object v5, v3, Lmml;->d:Lnzx;

    iget-object v5, v5, Lnzx;->b:Ljava/lang/String;

    if-eqz v5, :cond_0

    if-eqz v4, :cond_0

    if-eqz v2, :cond_0

    .line 342
    const v5, 0x7f100042

    iget-object v3, v3, Lmml;->d:Lnzx;

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v3}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->setTag(ILjava/lang/Object;)V

    .line 343
    const v3, 0x7f100091

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->setTag(ILjava/lang/Object;)V

    .line 344
    const v3, 0x7f100041

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->setTag(ILjava/lang/Object;)V

    .line 345
    move-object/from16 v0, p0

    iget-object v2, v0, Lfqq;->f:Lfsu;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Lfsu;)V

    goto/16 :goto_0

    .line 334
    :cond_7
    invoke-static {v5}, Ljvd;->b(Lnym;)Ljac;

    move-result-object v2

    goto :goto_3

    .line 347
    :cond_8
    iget v2, v3, Lmml;->c:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_9

    .line 348
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 349
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 350
    move-object/from16 v0, p0

    iget-object v5, v0, Lfqq;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    .line 351
    iget-object v3, v3, Lmml;->e:Lmmn;

    iget-object v3, v3, Lmmn;->a:Lmmo;

    const/4 v6, 0x0

    .line 352
    invoke-static {v3, v6}, Lfsp;->a(Lmmo;Z)Lfof;

    move-result-object v3

    .line 351
    invoke-static {v3, v2, v4, v5}, Lfsp;->a(Lfof;IIF)Ljava/lang/String;

    move-result-object v2

    .line 355
    move-object/from16 v0, p0

    iget-object v3, v0, Lfqq;->a:Landroid/content/Context;

    sget-object v4, Ljac;->a:Ljac;

    invoke-static {v3, v2, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Lizu;)V

    goto/16 :goto_0

    .line 357
    :cond_9
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Unsupported moment type"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 311
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private d(I)I
    .locals 2

    .prologue
    .line 487
    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    .line 488
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqr;

    iget v0, v0, Lfqr;->a:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    .line 489
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqr;

    iget v0, v0, Lfqr;->a:I

    if-nez v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    :cond_0
    return p1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Lfqq;->k:I

    return v0
.end method

.method public a(I)Lfqr;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqr;

    return-object v0
.end method

.method public a(Lfob;)V
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v1, 0x0

    .line 80
    .line 82
    iget-object v0, p0, Lfqq;->g:Lmmq;

    iget-object v2, p1, Lfob;->a:Lmmq;

    if-eq v0, v2, :cond_11

    .line 83
    iget-object v0, p1, Lfob;->a:Lmmq;

    iput-object v0, p0, Lfqq;->g:Lmmq;

    .line 84
    iget-object v0, p0, Lfqq;->g:Lmmq;

    if-nez v0, :cond_4

    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput v6, p0, Lfqq;->k:I

    iget-object v0, p0, Lfqq;->j:Lfoi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqq;->j:Lfoi;

    invoke-virtual {v0, v1}, Lfoi;->a(I)V

    :cond_0
    :goto_0
    move v0, v7

    .line 88
    :goto_1
    iget-object v2, p1, Lfob;->b:Lfod;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lfob;->b:Lfod;

    iget-object v3, p0, Lfqq;->h:Lfod;

    .line 89
    invoke-virtual {v2, v3}, Lfod;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 90
    iget-object v1, p1, Lfob;->b:Lfod;

    iput-object v1, p0, Lfqq;->h:Lfod;

    move v1, v7

    .line 94
    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    .line 95
    :cond_2
    invoke-virtual {p0}, Lfqq;->notifyDataSetChanged()V

    .line 97
    :cond_3
    return-void

    .line 84
    :cond_4
    new-instance v8, Lfqs;

    iget v0, p0, Lfqq;->c:I

    invoke-direct {v8, v0}, Lfqs;-><init>(I)V

    move v0, v1

    move v2, v1

    move v3, v1

    :goto_2
    iget-object v4, p0, Lfqq;->g:Lmmq;

    iget-object v4, v4, Lmmq;->e:[Lmma;

    array-length v4, v4

    if-ge v0, v4, :cond_b

    iget-object v4, p0, Lfqq;->g:Lmmq;

    iget-object v4, v4, Lmmq;->e:[Lmma;

    aget-object v4, v4, v0

    iget-object v4, v4, Lmma;->c:Lmmb;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lfqq;->g:Lmmq;

    iget-object v4, v4, Lmmq;->e:[Lmma;

    aget-object v4, v4, v0

    iget-object v4, v4, Lmma;->c:Lmmb;

    iget-object v4, v4, Lmmb;->a:Ljava/lang/String;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lfqq;->g:Lmmq;

    iget-object v4, v4, Lmmq;->e:[Lmma;

    aget-object v4, v4, v0

    iget-object v4, v4, Lmma;->c:Lmmb;

    iget-object v4, v4, Lmmb;->a:Ljava/lang/String;

    :goto_3
    invoke-virtual {v8, v4}, Lfqs;->a(Ljava/lang/String;)V

    move v4, v1

    :goto_4
    iget-object v5, p0, Lfqq;->g:Lmmq;

    iget-object v5, v5, Lmmq;->e:[Lmma;

    aget-object v5, v5, v0

    iget-object v5, v5, Lmma;->b:[Lmmp;

    array-length v5, v5

    if-ge v4, v5, :cond_a

    iget-object v5, p0, Lfqq;->g:Lmmq;

    iget-object v5, v5, Lmmq;->e:[Lmma;

    aget-object v5, v5, v0

    iget-object v5, v5, Lmma;->b:[Lmmp;

    aget-object v5, v5, v4

    iget-object v5, v5, Lmmp;->c:[Lmmc;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lfqq;->g:Lmmq;

    iget-object v5, v5, Lmmq;->e:[Lmma;

    aget-object v5, v5, v0

    iget-object v5, v5, Lmma;->b:[Lmmp;

    aget-object v5, v5, v4

    iget-object v9, v5, Lmmp;->c:[Lmmc;

    array-length v10, v9

    move v5, v1

    :goto_5
    if-ge v5, v10, :cond_7

    aget-object v11, v9, v5

    invoke-static {v11}, Lfss;->a(Lmmc;)Z

    move-result v12

    if-eqz v12, :cond_5

    new-instance v12, Lfoh;

    invoke-direct {v12, v11, v0, v4}, Lfoh;-><init>(Lmmc;II)V

    invoke-virtual {v8, v12}, Lfqs;->a(Lfoh;)V

    iget-object v11, p0, Lfqq;->j:Lfoi;

    invoke-virtual {v11, v12}, Lfoi;->a(Lfoh;)Z

    move-result v11

    if-eqz v11, :cond_5

    add-int/lit8 v2, v2, 0x1

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_6
    const-string v4, ""

    goto :goto_3

    :cond_7
    iget-object v5, p0, Lfqq;->g:Lmmq;

    iget-object v5, v5, Lmmq;->e:[Lmma;

    aget-object v5, v5, v0

    iget-object v5, v5, Lmma;->b:[Lmmp;

    aget-object v5, v5, v4

    iget-object v5, v5, Lmmp;->b:[Lmml;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lfqq;->g:Lmmq;

    iget-object v5, v5, Lmmq;->e:[Lmma;

    aget-object v5, v5, v0

    iget-object v5, v5, Lmma;->b:[Lmmp;

    aget-object v5, v5, v4

    iget-object v9, v5, Lmmp;->b:[Lmml;

    array-length v10, v9

    move v5, v1

    :goto_6
    if-ge v5, v10, :cond_9

    aget-object v11, v9, v5

    invoke-static {v11}, Lfss;->a(Lmml;)Z

    move-result v12

    if-eqz v12, :cond_8

    new-instance v12, Lfoh;

    invoke-direct {v12, v11}, Lfoh;-><init>(Lmml;)V

    invoke-virtual {v8, v12}, Lfqs;->a(Lfoh;)V

    iget-object v11, p0, Lfqq;->j:Lfoi;

    invoke-virtual {v11, v12}, Lfoi;->a(Lfoh;)Z

    move-result v11

    if-eqz v11, :cond_8

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v12}, Lfoh;->h()Z

    move-result v11

    if-eqz v11, :cond_8

    add-int/lit8 v3, v3, 0x1

    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_9
    move v5, v3

    move v3, v2

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v3

    move v3, v5

    goto/16 :goto_4

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    :cond_b
    invoke-virtual {v8}, Lfqs;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfqq;->i:Ljava/util/List;

    iget-object v0, p0, Lfqq;->j:Lfoi;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lfqq;->j:Lfoi;

    invoke-virtual {v0, v3}, Lfoi;->a(I)V

    iget-object v0, p0, Lfqq;->j:Lfoi;

    invoke-virtual {v0, v2}, Lfoi;->b(I)V

    :cond_c
    iget-object v0, p0, Lfqq;->e:Lmms;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v6

    :cond_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqr;

    add-int/lit8 v2, v2, 0x1

    iget-object v4, v0, Lfqr;->c:Ljava/util/List;

    if-eqz v4, :cond_d

    iget-object v0, v0, Lfqr;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoh;

    invoke-virtual {v0}, Lfoh;->i()Lmms;

    move-result-object v5

    if-eqz v5, :cond_e

    invoke-virtual {v0}, Lfoh;->i()Lmms;

    move-result-object v5

    iget-object v5, v5, Lmms;->d:Lmmm;

    iget-object v6, p0, Lfqq;->e:Lmms;

    iget-object v6, v6, Lmms;->d:Lmmm;

    invoke-static {v5, v6}, Loxu;->a(Loxu;Loxu;)Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-virtual {v0}, Lfoh;->i()Lmms;

    move-result-object v0

    iget-object v0, v0, Lmms;->e:Lmmd;

    iget-object v5, p0, Lfqq;->e:Lmms;

    iget-object v5, v5, Lmms;->e:Lmmd;

    invoke-static {v0, v5}, Loxu;->a(Loxu;Loxu;)Z

    move-result v0

    if-eqz v0, :cond_e

    iput v2, p0, Lfqq;->k:I

    :cond_f
    iget v0, p0, Lfqq;->k:I

    invoke-direct {p0, v0}, Lfqq;->d(I)I

    move-result v0

    iput v0, p0, Lfqq;->k:I

    goto/16 :goto_0

    :cond_10
    iput v1, p0, Lfqq;->k:I

    goto/16 :goto_0

    :cond_11
    move v0, v1

    goto/16 :goto_1
.end method

.method public a(Lfoi;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lfqq;->j:Lfoi;

    .line 101
    return-void
.end method

.method public b(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 242
    iget-object v1, p0, Lfqq;->i:Ljava/util/List;

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 249
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v2, v0

    move v1, v0

    .line 246
    :goto_1
    if-ge v2, p1, :cond_2

    .line 247
    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqr;

    invoke-static {v0}, Lfqr;->a(Lfqr;)I

    move-result v0

    add-int/2addr v1, v0

    .line 246
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 249
    goto :goto_0
.end method

.method public c(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 258
    iget-object v1, p0, Lfqq;->i:Ljava/util/List;

    if-nez v1, :cond_0

    .line 269
    :goto_0
    return v0

    :cond_0
    move v1, v0

    move v2, v0

    .line 263
    :goto_1
    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 264
    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqr;

    invoke-static {v0}, Lfqr;->a(Lfqr;)I

    move-result v0

    add-int/2addr v0, v1

    .line 265
    if-gt v0, p1, :cond_1

    .line 266
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 269
    :cond_1
    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, v0}, Lfqq;->d(I)I

    move-result v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfqq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lfqq;->a(I)Lfqr;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 115
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Lfqq;->a(I)Lfqr;

    move-result-object v0

    iget v0, v0, Lfqr;->a:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v3, 0x0

    const v8, 0x7f1005e4

    const/4 v4, 0x1

    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 131
    iget-object v0, p0, Lfqq;->g:Lmmq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfqq;->j:Lfoi;

    if-nez v0, :cond_1

    :cond_0
    move-object v1, v3

    .line 222
    :goto_0
    return-object v1

    .line 135
    :cond_1
    invoke-virtual {p0, p1}, Lfqq;->a(I)Lfqr;

    move-result-object v5

    .line 136
    invoke-virtual {p0, p1}, Lfqq;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 226
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unrecognized story element row type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :pswitch_0
    if-nez p2, :cond_12

    .line 140
    iget-object v0, p0, Lfqq;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04020a

    .line 141
    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_1
    move-object v0, v1

    .line 143
    check-cast v0, Landroid/widget/TextView;

    iget-object v2, v5, Lfqr;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 149
    :pswitch_1
    if-nez p2, :cond_6

    .line 150
    iget-object v0, p0, Lfqq;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04020c

    .line 151
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 153
    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;

    .line 154
    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Z)V

    .line 155
    const/4 v6, 0x3

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->b(I)V

    move-object p2, v0

    .line 166
    :goto_2
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_2

    .line 167
    invoke-virtual {p2, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gtz v0, :cond_2

    .line 168
    invoke-direct {p0, p3}, Lfqq;->a(Landroid/view/ViewGroup;)I

    move-result v1

    .line 170
    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    .line 171
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    invoke-direct {v6, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 172
    iget v0, p0, Lfqq;->d:I

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 174
    invoke-virtual {p2, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;

    .line 175
    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(I)V

    .line 177
    const v0, 0x7f1005e1

    .line 178
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 179
    iget-object v1, p0, Lfqq;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f0d00d5

    .line 180
    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 181
    invoke-direct {p0, p3}, Lfqq;->a(Landroid/view/ViewGroup;)I

    move-result v6

    add-int/2addr v1, v6

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 183
    :cond_2
    iget-object v0, v5, Lfqr;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfoh;

    invoke-virtual {p2, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;

    invoke-direct {p0, v1, v0}, Lfqq;->a(Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;Lfoh;)V

    const v1, 0x7f1005e1

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f1005e2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v0}, Lfoh;->a()I

    move-result v2

    if-ne v2, v7, :cond_7

    invoke-virtual {v0}, Lfoh;->c()Lmmc;

    move-result-object v2

    iget v2, v2, Lmmc;->b:I

    if-ne v2, v7, :cond_7

    const v2, 0x7f020535

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_3
    :goto_3
    const v1, 0x7f1005e1

    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f1005e3

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lfoh;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :cond_4
    move-object v0, v3

    :goto_4
    if-nez v0, :cond_5

    const-string v0, ""

    :cond_5
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, p2

    .line 184
    goto/16 :goto_0

    .line 157
    :cond_6
    check-cast p2, Landroid/view/ViewGroup;

    goto/16 :goto_2

    .line 183
    :cond_7
    invoke-virtual {v0}, Lfoh;->a()I

    move-result v2

    if-ne v2, v7, :cond_8

    invoke-virtual {v0}, Lfoh;->c()Lmmc;

    move-result-object v2

    iget v2, v2, Lmmc;->b:I

    const/4 v5, 0x3

    if-ne v2, v5, :cond_8

    const v2, 0x7f02051d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :cond_8
    invoke-virtual {v0}, Lfoh;->a()I

    move-result v2

    if-ne v2, v4, :cond_3

    invoke-virtual {v0}, Lfoh;->b()Lmml;

    move-result-object v2

    iget v2, v2, Lmml;->c:I

    if-ne v2, v7, :cond_3

    const v2, 0x7f020535

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :pswitch_2
    invoke-virtual {v0}, Lfoh;->c()Lmmc;

    move-result-object v0

    iget v2, v0, Lmmc;->b:I

    if-ne v2, v7, :cond_9

    iget-object v0, v0, Lmmc;->d:Lmme;

    iget-object v3, v0, Lmme;->a:Ljava/lang/String;

    move-object v0, v3

    goto :goto_4

    :cond_9
    iget v2, v0, Lmmc;->b:I

    const/4 v4, 0x3

    if-ne v2, v4, :cond_4

    iget-object v2, v0, Lmmc;->e:Lmmj;

    iget-object v2, v2, Lmmj;->b:Lmme;

    if-eqz v2, :cond_a

    iget-object v2, v0, Lmmc;->e:Lmmj;

    iget-object v2, v2, Lmmj;->b:Lmme;

    iget-object v2, v2, Lmme;->a:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v0, v0, Lmmc;->e:Lmmj;

    iget-object v0, v0, Lmmj;->b:Lmme;

    iget-object v0, v0, Lmme;->a:Ljava/lang/String;

    goto :goto_4

    :cond_a
    const-string v0, ""

    goto :goto_4

    :pswitch_3
    invoke-virtual {v0}, Lfoh;->b()Lmml;

    move-result-object v0

    iget v2, v0, Lmml;->c:I

    if-ne v2, v7, :cond_4

    iget-object v2, v0, Lmml;->e:Lmmn;

    iget-object v2, v2, Lmmn;->a:Lmmo;

    iget-object v2, v2, Lmmo;->b:Lofq;

    if-eqz v2, :cond_4

    iget-object v0, v0, Lmml;->e:Lmmn;

    iget-object v0, v0, Lmmn;->a:Lmmo;

    iget-object v0, v0, Lmmo;->b:Lofq;

    iget-object v3, v0, Lofq;->e:Ljava/lang/String;

    move-object v0, v3

    goto :goto_4

    .line 189
    :pswitch_4
    if-nez p2, :cond_b

    .line 190
    iget-object v0, p0, Lfqq;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04020b

    .line 191
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 193
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v1, v2

    .line 194
    :goto_5
    iget v3, p0, Lfqq;->c:I

    if-ge v1, v3, :cond_c

    .line 195
    iget-object v3, p0, Lfqq;->a:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v6, 0x7f04020d

    .line 196
    invoke-virtual {v3, v6, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 195
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 199
    :cond_b
    check-cast p2, Landroid/view/ViewGroup;

    move-object v0, p2

    :cond_c
    move v1, v2

    .line 201
    :goto_6
    iget v3, p0, Lfqq;->c:I

    if-ge v1, v3, :cond_d

    .line 203
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 201
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 207
    :cond_d
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    if-lez v1, :cond_11

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gtz v1, :cond_11

    .line 208
    invoke-direct {p0, p3}, Lfqq;->a(Landroid/view/ViewGroup;)I

    move-result v6

    .line 210
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    iget v3, p0, Lfqq;->c:I

    mul-int/2addr v3, v6

    sub-int/2addr v1, v3

    iget v3, p0, Lfqq;->c:I

    add-int/lit8 v3, v3, -0x1

    iget v7, p0, Lfqq;->d:I

    mul-int/2addr v3, v7

    sub-int v7, v1, v3

    move v1, v2

    .line 212
    :goto_7
    iget v3, p0, Lfqq;->c:I

    if-ge v1, v3, :cond_11

    .line 214
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    if-ge v1, v7, :cond_e

    move v3, v4

    :goto_8
    add-int v9, v6, v3

    if-ge v1, v7, :cond_f

    move v3, v4

    :goto_9
    add-int/2addr v3, v6

    invoke-direct {v8, v9, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 216
    iget v3, p0, Lfqq;->c:I

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_10

    move v3, v2

    :goto_a
    iput v3, v8, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 217
    iget v3, p0, Lfqq;->d:I

    iput v3, v8, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 218
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_e
    move v3, v2

    .line 214
    goto :goto_8

    :cond_f
    move v3, v2

    goto :goto_9

    .line 216
    :cond_10
    iget v3, p0, Lfqq;->d:I

    goto :goto_a

    .line 221
    :cond_11
    invoke-direct {p0, v0, v5}, Lfqq;->a(Landroid/view/ViewGroup;Lfqr;)V

    move-object v1, v0

    .line 222
    goto/16 :goto_0

    :cond_12
    move-object v1, p2

    goto/16 :goto_1

    .line 136
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 183
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x3

    return v0
.end method
