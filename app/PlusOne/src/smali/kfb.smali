.class public final Lkfb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkfd;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:[Lkfi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lkfb;->a:Landroid/content/Context;

    .line 19
    return-void
.end method

.method private declared-synchronized a()V
    .locals 2

    .prologue
    .line 33
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lkfb;->a:Landroid/content/Context;

    const-class v1, Lkfi;

    .line 34
    invoke-static {v0, v1}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 35
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lkfi;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lkfi;

    iput-object v0, p0, Lkfb;->b:[Lkfi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    monitor-exit p0

    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Lkff;)V
    .locals 6

    .prologue
    .line 23
    iget-object v0, p0, Lkfb;->b:[Lkfi;

    if-nez v0, :cond_0

    .line 24
    invoke-direct {p0}, Lkfb;->a()V

    .line 26
    :cond_0
    iget-object v1, p0, Lkfb;->b:[Lkfi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 27
    iget-object v4, p1, Lkff;->f:Landroid/content/Context;

    invoke-interface {v3, v4, p1}, Lkfi;->a(Landroid/content/Context;Lkff;)Lkff;

    move-result-object p1

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 29
    :cond_1
    iget-object v0, p1, Lkff;->g:Lkfo;

    invoke-virtual {v0}, Lkfo;->e()Lkfg;

    move-result-object v0

    iput-object v0, p1, Lkff;->h:Lkfg;

    iget-object v0, p1, Lkff;->h:Lkfg;

    if-nez v0, :cond_2

    sget-object v0, Lkff;->e:Lloy;

    :cond_2
    const-string v0, "HttpOperation"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Starting op: "

    invoke-virtual {p1}, Lkff;->Q_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :cond_3
    :goto_1
    iget-object v0, p1, Lkff;->g:Lkfo;

    invoke-virtual {v0}, Lkfo;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    iput v0, p1, Lkff;->l:I

    :cond_4
    invoke-virtual {p1}, Lkff;->M_()V

    iget-object v0, p1, Lkff;->h:Lkfg;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lkff;->h:Lkfg;

    invoke-virtual {p1}, Lkff;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lkff;->r()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lkfg;->a(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lkff;->u()V

    invoke-virtual {p1}, Lkff;->v()V

    iget-object v0, p1, Lkff;->h:Lkfg;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lkff;->h:Lkfg;

    iget-object v1, p1, Lkff;->o:Lkfk;

    invoke-virtual {v0, v1}, Lkfg;->a(Lkfk;)V

    iget-object v0, p1, Lkff;->o:Lkfk;

    invoke-virtual {v0}, Lkfk;->f()V

    iget-object v0, p1, Lkff;->h:Lkfg;

    invoke-virtual {v0}, Lkfg;->e()V

    invoke-virtual {p1}, Lkff;->y()V

    iget-object v0, p1, Lkff;->g:Lkfo;

    invoke-virtual {v0}, Lkfo;->e()Lkfg;

    move-result-object v0

    if-nez v0, :cond_6

    iget-object v0, p1, Lkff;->h:Lkfg;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lkfg;->a(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "HttpOperation"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lkff;->o()Ljava/lang/String;

    move-result-object v0

    iget v1, p1, Lkff;->i:I

    iget-object v2, p1, Lkff;->k:Ljava/lang/Exception;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x24

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "] failed due to error: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    :cond_7
    return-void

    .line 29
    :cond_8
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1
.end method
