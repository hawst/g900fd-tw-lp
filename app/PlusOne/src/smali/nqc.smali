.class public final Lnqc;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Boolean;

.field public c:Lnpv;

.field public d:Lnpw;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1031
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1038
    iput-object v0, p0, Lnqc;->c:Lnpv;

    .line 1041
    iput-object v0, p0, Lnqc;->d:Lnpw;

    .line 1031
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1064
    const/4 v0, 0x0

    .line 1065
    iget-object v1, p0, Lnqc;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1066
    const/4 v0, 0x1

    iget-object v1, p0, Lnqc;->a:Ljava/lang/String;

    .line 1067
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1069
    :cond_0
    iget-object v1, p0, Lnqc;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1070
    const/4 v1, 0x2

    iget-object v2, p0, Lnqc;->b:Ljava/lang/Boolean;

    .line 1071
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1073
    :cond_1
    iget-object v1, p0, Lnqc;->c:Lnpv;

    if-eqz v1, :cond_2

    .line 1074
    const/4 v1, 0x3

    iget-object v2, p0, Lnqc;->c:Lnpv;

    .line 1075
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1077
    :cond_2
    iget-object v1, p0, Lnqc;->d:Lnpw;

    if-eqz v1, :cond_3

    .line 1078
    const/4 v1, 0x4

    iget-object v2, p0, Lnqc;->d:Lnpw;

    .line 1079
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1081
    :cond_3
    iget-object v1, p0, Lnqc;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1082
    iput v0, p0, Lnqc;->ai:I

    .line 1083
    return v0
.end method

.method public a(Loxn;)Lnqc;
    .locals 2

    .prologue
    .line 1091
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1092
    sparse-switch v0, :sswitch_data_0

    .line 1096
    iget-object v1, p0, Lnqc;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1097
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnqc;->ah:Ljava/util/List;

    .line 1100
    :cond_1
    iget-object v1, p0, Lnqc;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1102
    :sswitch_0
    return-object p0

    .line 1107
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnqc;->a:Ljava/lang/String;

    goto :goto_0

    .line 1111
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnqc;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 1115
    :sswitch_3
    iget-object v0, p0, Lnqc;->c:Lnpv;

    if-nez v0, :cond_2

    .line 1116
    new-instance v0, Lnpv;

    invoke-direct {v0}, Lnpv;-><init>()V

    iput-object v0, p0, Lnqc;->c:Lnpv;

    .line 1118
    :cond_2
    iget-object v0, p0, Lnqc;->c:Lnpv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1122
    :sswitch_4
    iget-object v0, p0, Lnqc;->d:Lnpw;

    if-nez v0, :cond_3

    .line 1123
    new-instance v0, Lnpw;

    invoke-direct {v0}, Lnpw;-><init>()V

    iput-object v0, p0, Lnqc;->d:Lnpw;

    .line 1125
    :cond_3
    iget-object v0, p0, Lnqc;->d:Lnpw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1092
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1046
    iget-object v0, p0, Lnqc;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1047
    const/4 v0, 0x1

    iget-object v1, p0, Lnqc;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1049
    :cond_0
    iget-object v0, p0, Lnqc;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1050
    const/4 v0, 0x2

    iget-object v1, p0, Lnqc;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1052
    :cond_1
    iget-object v0, p0, Lnqc;->c:Lnpv;

    if-eqz v0, :cond_2

    .line 1053
    const/4 v0, 0x3

    iget-object v1, p0, Lnqc;->c:Lnpv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1055
    :cond_2
    iget-object v0, p0, Lnqc;->d:Lnpw;

    if-eqz v0, :cond_3

    .line 1056
    const/4 v0, 0x4

    iget-object v1, p0, Lnqc;->d:Lnpw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1058
    :cond_3
    iget-object v0, p0, Lnqc;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1060
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1027
    invoke-virtual {p0, p1}, Lnqc;->a(Loxn;)Lnqc;

    move-result-object v0

    return-object v0
.end method
