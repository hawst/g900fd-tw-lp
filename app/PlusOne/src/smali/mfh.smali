.class public final Lmfh;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnbq;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27700
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27703
    iput-object v0, p0, Lmfh;->apiHeader:Llyr;

    .line 27706
    iput-object v0, p0, Lmfh;->a:Lnbq;

    .line 27700
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27723
    const/4 v0, 0x0

    .line 27724
    iget-object v1, p0, Lmfh;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 27725
    const/4 v0, 0x1

    iget-object v1, p0, Lmfh;->apiHeader:Llyr;

    .line 27726
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27728
    :cond_0
    iget-object v1, p0, Lmfh;->a:Lnbq;

    if-eqz v1, :cond_1

    .line 27729
    const/4 v1, 0x2

    iget-object v2, p0, Lmfh;->a:Lnbq;

    .line 27730
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27732
    :cond_1
    iget-object v1, p0, Lmfh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27733
    iput v0, p0, Lmfh;->ai:I

    .line 27734
    return v0
.end method

.method public a(Loxn;)Lmfh;
    .locals 2

    .prologue
    .line 27742
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27743
    sparse-switch v0, :sswitch_data_0

    .line 27747
    iget-object v1, p0, Lmfh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27748
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfh;->ah:Ljava/util/List;

    .line 27751
    :cond_1
    iget-object v1, p0, Lmfh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27753
    :sswitch_0
    return-object p0

    .line 27758
    :sswitch_1
    iget-object v0, p0, Lmfh;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 27759
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfh;->apiHeader:Llyr;

    .line 27761
    :cond_2
    iget-object v0, p0, Lmfh;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27765
    :sswitch_2
    iget-object v0, p0, Lmfh;->a:Lnbq;

    if-nez v0, :cond_3

    .line 27766
    new-instance v0, Lnbq;

    invoke-direct {v0}, Lnbq;-><init>()V

    iput-object v0, p0, Lmfh;->a:Lnbq;

    .line 27768
    :cond_3
    iget-object v0, p0, Lmfh;->a:Lnbq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27743
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27711
    iget-object v0, p0, Lmfh;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 27712
    const/4 v0, 0x1

    iget-object v1, p0, Lmfh;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27714
    :cond_0
    iget-object v0, p0, Lmfh;->a:Lnbq;

    if-eqz v0, :cond_1

    .line 27715
    const/4 v0, 0x2

    iget-object v1, p0, Lmfh;->a:Lnbq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27717
    :cond_1
    iget-object v0, p0, Lmfh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27719
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 27696
    invoke-virtual {p0, p1}, Lmfh;->a(Loxn;)Lmfh;

    move-result-object v0

    return-object v0
.end method
