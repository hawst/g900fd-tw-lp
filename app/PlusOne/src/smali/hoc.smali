.class public Lhoc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llqi;
.implements Llqz;
.implements Llra;
.implements Llrb;
.implements Llrc;
.implements Llrd;
.implements Llrg;


# static fields
.field private static final a:Lhos;


# instance fields
.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lhob;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/app/Activity;

.field private d:Lhor;

.field private e:Lhog;

.field private f:Lhos;

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lhof;

    invoke-direct {v0}, Lhof;-><init>()V

    sput-object v0, Lhoc;->a:Lhos;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhoc;->b:Ljava/util/ArrayList;

    .line 55
    sget-object v0, Lhoc;->a:Lhos;

    iput-object v0, p0, Lhoc;->f:Lhos;

    .line 67
    iput-object p1, p0, Lhoc;->c:Landroid/app/Activity;

    .line 68
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 69
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 70
    return-void
.end method

.method public constructor <init>(Lu;Llqr;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhoc;->b:Ljava/util/ArrayList;

    .line 55
    sget-object v0, Lhoc;->a:Lhos;

    iput-object v0, p0, Lhoc;->f:Lhos;

    .line 81
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 82
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 84
    return-void
.end method

.method public static a(Lhny;)Lhoz;
    .locals 1

    .prologue
    .line 169
    :try_start_0
    invoke-virtual {p0}, Lhny;->i()V

    .line 170
    invoke-virtual {p0}, Lhny;->m()Lhoz;

    move-result-object v0

    .line 172
    invoke-virtual {p0, v0}, Lhny;->a_(Lhoz;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    invoke-virtual {p0}, Lhny;->j()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lhny;->j()V

    throw v0
.end method

.method public static a(Landroid/content/Context;Lhny;)V
    .locals 2

    .prologue
    .line 137
    const-class v0, Lhor;

    .line 138
    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhor;

    .line 140
    invoke-virtual {p1}, Lhny;->i()V

    .line 141
    invoke-static {}, Llsx;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lhor;->a(Lhny;Lhoc;)V

    .line 151
    :goto_0
    return-void

    .line 144
    :cond_0
    new-instance v1, Lhod;

    invoke-direct {v1, v0, p1}, Lhod;-><init>(Lhor;Lhny;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public E_()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lhoc;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lhoc;->d:Lhor;

    invoke-virtual {v0, p0}, Lhor;->d(Lhoc;)V

    .line 222
    :cond_0
    return-void
.end method

.method public a(Lhob;)Lhoc;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lhoc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    return-object p0
.end method

.method public a(Llnh;)Lhoc;
    .locals 1

    .prologue
    .line 111
    const-class v0, Lhoc;

    invoke-virtual {p1, v0, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 112
    return-object p0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lhoc;->c:Landroid/app/Activity;

    .line 182
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lhoc;->c:Landroid/app/Activity;

    const-class v1, Lhor;

    .line 187
    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhor;

    iput-object v0, p0, Lhoc;->d:Lhor;

    .line 189
    if-eqz p1, :cond_1

    .line 190
    const-string v0, "boc_background_tasks"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhog;

    iput-object v0, p0, Lhoc;->e:Lhog;

    .line 195
    :goto_0
    iget-boolean v0, p0, Lhoc;->g:Z

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lhoc;->c:Landroid/app/Activity;

    const-class v1, Lhot;

    .line 197
    invoke-static {v0, v1}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhot;

    .line 198
    if-eqz v0, :cond_0

    iget-object v1, p0, Lhoc;->c:Landroid/app/Activity;

    instance-of v1, v1, Lz;

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lhoc;->c:Landroid/app/Activity;

    check-cast v1, Lz;

    invoke-virtual {v1}, Lz;->f()Lae;

    move-result-object v1

    .line 200
    iget-object v2, p0, Lhoc;->c:Landroid/app/Activity;

    invoke-interface {v0, v2, v1}, Lhot;->a(Landroid/content/Context;Lae;)Lhos;

    move-result-object v0

    iput-object v0, p0, Lhoc;->f:Lhos;

    .line 203
    :cond_0
    return-void

    .line 192
    :cond_1
    new-instance v0, Lhog;

    iget-object v1, p0, Lhoc;->d:Lhor;

    invoke-direct {v0, v1}, Lhog;-><init>(Lhor;)V

    iput-object v0, p0, Lhoc;->e:Lhog;

    goto :goto_0
.end method

.method public a(Lhos;)V
    .locals 1

    .prologue
    .line 119
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lhoc;->f:Lhos;

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhoc;->g:Z

    .line 121
    return-void

    .line 119
    :cond_0
    sget-object p1, Lhoc;->a:Lhos;

    goto :goto_0
.end method

.method a(Ljava/lang/String;Lhoz;)V
    .locals 3

    .prologue
    .line 296
    iget-object v0, p0, Lhoc;->e:Lhog;

    invoke-virtual {v0, p1}, Lhog;->b(Ljava/lang/String;)V

    .line 298
    iget-object v0, p0, Lhoc;->f:Lhos;

    invoke-virtual {v0, p1}, Lhos;->a(Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lhoc;->f:Lhos;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhos;->a(Z)V

    .line 304
    iget-object v0, p0, Lhoc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 305
    iget-object v0, p0, Lhoc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhob;

    iget-object v2, p0, Lhoc;->f:Lhos;

    invoke-interface {v0, p1, p2, v2}, Lhob;->a(Ljava/lang/String;Lhoz;Lhos;)V

    .line 304
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 308
    :cond_0
    iget-object v0, p0, Lhoc;->f:Lhos;

    invoke-virtual {v0}, Lhos;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 309
    iget-object v0, p0, Lhoc;->f:Lhos;

    invoke-virtual {v0, p2}, Lhos;->a(Lhoz;)Z

    .line 311
    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lhoc;->e:Lhog;

    if-nez v0, :cond_0

    .line 245
    const/4 v0, 0x0

    .line 247
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhoc;->d:Lhor;

    invoke-virtual {v0, p0, p1}, Lhor;->a(Lhoc;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(Lhob;)Lhoc;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lhoc;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 107
    return-object p0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lhoc;->d:Lhor;

    invoke-virtual {v0, p0}, Lhor;->b(Lhoc;)V

    .line 208
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 226
    const-string v0, "boc_background_tasks"

    iget-object v1, p0, Lhoc;->e:Lhog;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 227
    return-void
.end method

.method public b(Lhny;)V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lhoc;->e:Lhog;

    invoke-virtual {v0, p1}, Lhog;->a(Lhny;)V

    .line 266
    invoke-virtual {p1}, Lhny;->i()V

    .line 267
    iget-object v0, p0, Lhoc;->d:Lhor;

    invoke-virtual {v0, p1, p0}, Lhor;->a(Lhny;Lhoc;)V

    .line 268
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lhoc;->d:Lhor;

    invoke-virtual {v0, p0, p1}, Lhor;->c(Lhoc;Ljava/lang/String;)V

    .line 289
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lhoc;->d:Lhor;

    invoke-virtual {v0, p0}, Lhor;->c(Lhoc;)V

    .line 213
    return-void
.end method

.method public c(Lhny;)V
    .locals 1

    .prologue
    .line 275
    invoke-virtual {p0, p1}, Lhoc;->b(Lhny;)V

    .line 276
    iget-object v0, p0, Lhoc;->f:Lhos;

    invoke-virtual {v0, p1}, Lhos;->a(Lhny;)V

    .line 277
    return-void
.end method

.method c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lhoc;->e:Lhog;

    invoke-virtual {v0, p1}, Lhog;->b(Ljava/lang/String;)V

    .line 293
    return-void
.end method

.method d(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lhoc;->e:Lhog;

    invoke-virtual {v0, p1}, Lhog;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public d()Lhos;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lhoc;->f:Lhos;

    return-object v0
.end method

.method public d(Lhny;)V
    .locals 2

    .prologue
    .line 280
    invoke-virtual {p0, p1}, Lhoc;->b(Lhny;)V

    .line 281
    iget-object v0, p0, Lhoc;->f:Lhos;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lhos;->a(Lhny;Z)V

    .line 282
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lhoc;->e:Lhog;

    if-nez v0, :cond_0

    .line 234
    const/4 v0, 0x0

    .line 236
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lhoc;->d:Lhor;

    invoke-virtual {v0, p0}, Lhor;->a(Lhoc;)Z

    move-result v0

    goto :goto_0
.end method

.method f()I
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lhoc;->e:Lhog;

    invoke-virtual {v0}, Lhog;->a()I

    move-result v0

    return v0
.end method

.method g()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lhoc;->e:Lhog;

    invoke-virtual {v0}, Lhog;->b()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
