.class public final Lole;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lole;


# instance fields
.field private b:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    new-array v0, v0, [Lole;

    sput-object v0, Lole;->a:[Lole;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 334
    const/4 v0, 0x0

    .line 335
    iget-object v1, p0, Lole;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 336
    const/4 v0, 0x1

    iget-object v1, p0, Lole;->b:Ljava/lang/Integer;

    .line 337
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 339
    :cond_0
    iget-object v1, p0, Lole;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 340
    iput v0, p0, Lole;->ai:I

    .line 341
    return v0
.end method

.method public a(Loxn;)Lole;
    .locals 2

    .prologue
    .line 349
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 350
    sparse-switch v0, :sswitch_data_0

    .line 354
    iget-object v1, p0, Lole;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 355
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lole;->ah:Ljava/util/List;

    .line 358
    :cond_1
    iget-object v1, p0, Lole;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    :sswitch_0
    return-object p0

    .line 365
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lole;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 350
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lole;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 326
    const/4 v0, 0x1

    iget-object v1, p0, Lole;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 328
    :cond_0
    iget-object v0, p0, Lole;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 330
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 314
    invoke-virtual {p0, p1}, Lole;->a(Loxn;)Lole;

    move-result-object v0

    return-object v0
.end method
