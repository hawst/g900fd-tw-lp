.class public final Lqbb;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/Float;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 1695
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1698
    iput v0, p0, Lqbb;->a:I

    .line 1703
    iput v0, p0, Lqbb;->c:I

    .line 1695
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 1723
    const/4 v0, 0x0

    .line 1724
    iget v1, p0, Lqbb;->a:I

    if-eq v1, v3, :cond_0

    .line 1725
    const/4 v0, 0x1

    iget v1, p0, Lqbb;->a:I

    .line 1726
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1728
    :cond_0
    iget-object v1, p0, Lqbb;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 1729
    const/4 v1, 0x2

    iget-object v2, p0, Lqbb;->b:Ljava/lang/Float;

    .line 1730
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1732
    :cond_1
    iget v1, p0, Lqbb;->c:I

    if-eq v1, v3, :cond_2

    .line 1733
    const/4 v1, 0x3

    iget v2, p0, Lqbb;->c:I

    .line 1734
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1736
    :cond_2
    iget-object v1, p0, Lqbb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1737
    iput v0, p0, Lqbb;->ai:I

    .line 1738
    return v0
.end method

.method public a(Loxn;)Lqbb;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1746
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1747
    sparse-switch v0, :sswitch_data_0

    .line 1751
    iget-object v1, p0, Lqbb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1752
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqbb;->ah:Ljava/util/List;

    .line 1755
    :cond_1
    iget-object v1, p0, Lqbb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1757
    :sswitch_0
    return-object p0

    .line 1762
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1763
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-eq v0, v1, :cond_2

    const/16 v1, 0x13

    if-ne v0, v1, :cond_3

    .line 1783
    :cond_2
    iput v0, p0, Lqbb;->a:I

    goto :goto_0

    .line 1785
    :cond_3
    iput v2, p0, Lqbb;->a:I

    goto :goto_0

    .line 1790
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lqbb;->b:Ljava/lang/Float;

    goto :goto_0

    .line 1794
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1795
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-ne v0, v6, :cond_5

    .line 1800
    :cond_4
    iput v0, p0, Lqbb;->c:I

    goto/16 :goto_0

    .line 1802
    :cond_5
    iput v2, p0, Lqbb;->c:I

    goto/16 :goto_0

    .line 1747
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 1708
    iget v0, p0, Lqbb;->a:I

    if-eq v0, v2, :cond_0

    .line 1709
    const/4 v0, 0x1

    iget v1, p0, Lqbb;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1711
    :cond_0
    iget-object v0, p0, Lqbb;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 1712
    const/4 v0, 0x2

    iget-object v1, p0, Lqbb;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1714
    :cond_1
    iget v0, p0, Lqbb;->c:I

    if-eq v0, v2, :cond_2

    .line 1715
    const/4 v0, 0x3

    iget v1, p0, Lqbb;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1717
    :cond_2
    iget-object v0, p0, Lqbb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1719
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1691
    invoke-virtual {p0, p1}, Lqbb;->a(Loxn;)Lqbb;

    move-result-object v0

    return-object v0
.end method
