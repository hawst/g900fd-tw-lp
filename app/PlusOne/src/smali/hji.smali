.class final Lhji;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhjk;


# instance fields
.field a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lhjx;",
            ">;>;"
        }
    .end annotation
.end field

.field private b:Landroid/view/Menu;

.field private synthetic c:Lhjg;


# direct methods
.method constructor <init>(Lhjg;Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 203
    iput-object p1, p0, Lhji;->c:Lhjg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lhji;->a:Landroid/util/SparseArray;

    .line 204
    iput-object p2, p0, Lhji;->b:Landroid/view/Menu;

    .line 205
    return-void
.end method


# virtual methods
.method public a(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lhji;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public a(ILhjx;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 245
    invoke-virtual {p0, p1}, Lhji;->b(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 246
    if-eqz v0, :cond_0

    .line 247
    invoke-virtual {p0, p1, p2}, Lhji;->b(ILhjx;)V

    .line 249
    :cond_0
    return-object v0
.end method

.method public a(I)Lhjc;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lhji;->c:Lhjg;

    invoke-static {v0}, Lhjg;->b(Lhjg;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjc;

    return-object v0
.end method

.method a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208
    move v1, v2

    :goto_0
    iget-object v0, p0, Lhji;->c:Lhjg;

    invoke-static {v0}, Lhjg;->b(Lhjg;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 209
    iget-object v0, p0, Lhji;->c:Lhjg;

    invoke-static {v0}, Lhjg;->b(Lhjg;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjc;

    .line 210
    invoke-interface {v0}, Lhjc;->a()V

    .line 208
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 212
    :goto_1
    iget-object v1, p0, Lhji;->b:Landroid/view/Menu;

    invoke-interface {v1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 213
    iget-object v1, p0, Lhji;->b:Landroid/view/Menu;

    invoke-interface {v1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 215
    :cond_1
    iget-object v0, p0, Lhji;->c:Lhjg;

    invoke-static {v0}, Lhjg;->c(Lhjg;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_2

    .line 216
    iget-object v0, p0, Lhji;->c:Lhjg;

    invoke-static {v0}, Lhjg;->c(Lhjg;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjj;

    .line 217
    invoke-interface {v0, p0}, Lhjj;->a(Lhjk;)V

    .line 215
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 219
    :cond_2
    :goto_3
    iget-object v0, p0, Lhji;->c:Lhjg;

    invoke-static {v0}, Lhjg;->b(Lhjg;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 220
    iget-object v0, p0, Lhji;->c:Lhjg;

    invoke-static {v0}, Lhjg;->b(Lhjg;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjc;

    .line 221
    iget-object v1, p0, Lhji;->b:Landroid/view/Menu;

    invoke-interface {v0, v1}, Lhjc;->a(Landroid/view/Menu;)V

    .line 219
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 223
    :cond_3
    return-void
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lhji;->b:Landroid/view/Menu;

    invoke-interface {v0, p1, p2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 271
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lhji;->c:Lhjg;

    invoke-static {v0}, Lhjg;->d(Lhjg;)Loo;

    move-result-object v0

    invoke-virtual {v0, p1}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 281
    return-void
.end method

.method public b(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 238
    invoke-virtual {p0, p1}, Lhji;->c(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 239
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 240
    return-object v0
.end method

.method public b(ILhjx;)V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lhji;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 255
    if-nez v0, :cond_0

    .line 256
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 257
    iget-object v1, p0, Lhji;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 259
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    return-void
.end method

.method public c(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lhji;->b:Landroid/view/Menu;

    invoke-interface {v0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 265
    return-object v0
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lhji;->c:Lhjg;

    invoke-static {v0}, Lhjg;->d(Lhjg;)Loo;

    move-result-object v0

    invoke-virtual {v0, p1}, Loo;->c(I)V

    .line 276
    return-void
.end method
