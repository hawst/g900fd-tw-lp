.class public final Lnag;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lnag;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 920
    const/4 v0, 0x0

    new-array v0, v0, [Lnag;

    sput-object v0, Lnag;->a:[Lnag;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 921
    invoke-direct {p0}, Loxq;-><init>()V

    .line 930
    const/high16 v0, -0x80000000

    iput v0, p0, Lnag;->b:I

    .line 921
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 964
    const/4 v0, 0x0

    .line 965
    iget v1, p0, Lnag;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 966
    const/4 v0, 0x1

    iget v1, p0, Lnag;->b:I

    .line 967
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 969
    :cond_0
    iget-object v1, p0, Lnag;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 970
    const/4 v1, 0x2

    iget-object v2, p0, Lnag;->c:Ljava/lang/String;

    .line 971
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 973
    :cond_1
    iget-object v1, p0, Lnag;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 974
    const/4 v1, 0x3

    iget-object v2, p0, Lnag;->d:Ljava/lang/String;

    .line 975
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 977
    :cond_2
    iget-object v1, p0, Lnag;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 978
    const/4 v1, 0x4

    iget-object v2, p0, Lnag;->e:Ljava/lang/String;

    .line 979
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 981
    :cond_3
    iget-object v1, p0, Lnag;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 982
    const/4 v1, 0x5

    iget-object v2, p0, Lnag;->f:Ljava/lang/String;

    .line 983
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 985
    :cond_4
    iget-object v1, p0, Lnag;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 986
    iput v0, p0, Lnag;->ai:I

    .line 987
    return v0
.end method

.method public a(Loxn;)Lnag;
    .locals 2

    .prologue
    .line 995
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 996
    sparse-switch v0, :sswitch_data_0

    .line 1000
    iget-object v1, p0, Lnag;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1001
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnag;->ah:Ljava/util/List;

    .line 1004
    :cond_1
    iget-object v1, p0, Lnag;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1006
    :sswitch_0
    return-object p0

    .line 1011
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1012
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1015
    :cond_2
    iput v0, p0, Lnag;->b:I

    goto :goto_0

    .line 1017
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnag;->b:I

    goto :goto_0

    .line 1022
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnag;->c:Ljava/lang/String;

    goto :goto_0

    .line 1026
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnag;->d:Ljava/lang/String;

    goto :goto_0

    .line 1030
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnag;->e:Ljava/lang/String;

    goto :goto_0

    .line 1034
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnag;->f:Ljava/lang/String;

    goto :goto_0

    .line 996
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 943
    iget v0, p0, Lnag;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 944
    const/4 v0, 0x1

    iget v1, p0, Lnag;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 946
    :cond_0
    iget-object v0, p0, Lnag;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 947
    const/4 v0, 0x2

    iget-object v1, p0, Lnag;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 949
    :cond_1
    iget-object v0, p0, Lnag;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 950
    const/4 v0, 0x3

    iget-object v1, p0, Lnag;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 952
    :cond_2
    iget-object v0, p0, Lnag;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 953
    const/4 v0, 0x4

    iget-object v1, p0, Lnag;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 955
    :cond_3
    iget-object v0, p0, Lnag;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 956
    const/4 v0, 0x5

    iget-object v1, p0, Lnag;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 958
    :cond_4
    iget-object v0, p0, Lnag;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 960
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 917
    invoke-virtual {p0, p1}, Lnag;->a(Loxn;)Lnag;

    move-result-object v0

    return-object v0
.end method
