.class public final Ldnf;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmje;",
        "Lmjf;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:[Lmym;

.field private final b:I

.field private final p:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;[JILjava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[JI",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 37
    const-string v3, "setnotificationsreadstates"

    new-instance v4, Lmje;

    invoke-direct {v4}, Lmje;-><init>()V

    new-instance v5, Lmjf;

    invoke-direct {v5}, Lmjf;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 42
    iput p5, p0, Ldnf;->b:I

    .line 43
    iput-object p6, p0, Ldnf;->p:Ljava/lang/String;

    .line 45
    if-eqz p3, :cond_1

    if-eqz p4, :cond_1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    array-length v1, p4

    if-ne v0, v1, :cond_1

    .line 46
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    .line 47
    new-array v3, v2, [Lmym;

    .line 48
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 49
    new-instance v4, Lmym;

    invoke-direct {v4}, Lmym;-><init>()V

    .line 50
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lmym;->b:Ljava/lang/String;

    .line 51
    aget-wide v6, p4, v1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lmym;->c:Ljava/lang/Long;

    .line 52
    aput-object v4, v3, v1

    .line 48
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 54
    :cond_0
    iput-object v3, p0, Ldnf;->a:[Lmym;

    .line 58
    :goto_1
    return-void

    .line 56
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Ldnf;->a:[Lmym;

    goto :goto_1
.end method


# virtual methods
.method protected a(Lmje;)V
    .locals 3

    .prologue
    .line 62
    new-instance v0, Lmyn;

    invoke-direct {v0}, Lmyn;-><init>()V

    .line 65
    new-instance v1, Lmyk;

    invoke-direct {v1}, Lmyk;-><init>()V

    .line 66
    const-string v2, "android_gplus"

    iput-object v2, v1, Lmyk;->a:Ljava/lang/String;

    .line 67
    iput-object v1, v0, Lmyn;->a:Lmyk;

    .line 69
    new-instance v1, Lmyl;

    invoke-direct {v1}, Lmyl;-><init>()V

    .line 70
    iget v2, p0, Ldnf;->b:I

    iput v2, v1, Lmyl;->b:I

    .line 71
    iget-object v2, p0, Ldnf;->a:[Lmym;

    iput-object v2, v1, Lmyl;->a:[Lmym;

    .line 72
    iget-object v2, p0, Ldnf;->p:Ljava/lang/String;

    iput-object v2, v1, Lmyl;->c:Ljava/lang/String;

    .line 73
    iput-object v1, v0, Lmyn;->b:Lmyl;

    .line 75
    iput-object v0, p1, Lmje;->a:Lmyn;

    .line 76
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lmje;

    invoke-virtual {p0, p1}, Ldnf;->a(Lmje;)V

    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 19
    return-void
.end method
