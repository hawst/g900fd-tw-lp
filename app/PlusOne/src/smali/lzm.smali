.class public final Llzm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnuv;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7045
    invoke-direct {p0}, Loxq;-><init>()V

    .line 7048
    iput-object v0, p0, Llzm;->apiHeader:Llyq;

    .line 7051
    iput-object v0, p0, Llzm;->a:Lnuv;

    .line 7045
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 7068
    const/4 v0, 0x0

    .line 7069
    iget-object v1, p0, Llzm;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 7070
    const/4 v0, 0x1

    iget-object v1, p0, Llzm;->apiHeader:Llyq;

    .line 7071
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7073
    :cond_0
    iget-object v1, p0, Llzm;->a:Lnuv;

    if-eqz v1, :cond_1

    .line 7074
    const/4 v1, 0x2

    iget-object v2, p0, Llzm;->a:Lnuv;

    .line 7075
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7077
    :cond_1
    iget-object v1, p0, Llzm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7078
    iput v0, p0, Llzm;->ai:I

    .line 7079
    return v0
.end method

.method public a(Loxn;)Llzm;
    .locals 2

    .prologue
    .line 7087
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7088
    sparse-switch v0, :sswitch_data_0

    .line 7092
    iget-object v1, p0, Llzm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7093
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llzm;->ah:Ljava/util/List;

    .line 7096
    :cond_1
    iget-object v1, p0, Llzm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7098
    :sswitch_0
    return-object p0

    .line 7103
    :sswitch_1
    iget-object v0, p0, Llzm;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 7104
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Llzm;->apiHeader:Llyq;

    .line 7106
    :cond_2
    iget-object v0, p0, Llzm;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7110
    :sswitch_2
    iget-object v0, p0, Llzm;->a:Lnuv;

    if-nez v0, :cond_3

    .line 7111
    new-instance v0, Lnuv;

    invoke-direct {v0}, Lnuv;-><init>()V

    iput-object v0, p0, Llzm;->a:Lnuv;

    .line 7113
    :cond_3
    iget-object v0, p0, Llzm;->a:Lnuv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7088
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 7056
    iget-object v0, p0, Llzm;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 7057
    const/4 v0, 0x1

    iget-object v1, p0, Llzm;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7059
    :cond_0
    iget-object v0, p0, Llzm;->a:Lnuv;

    if-eqz v0, :cond_1

    .line 7060
    const/4 v0, 0x2

    iget-object v1, p0, Llzm;->a:Lnuv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 7062
    :cond_1
    iget-object v0, p0, Llzm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 7064
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 7041
    invoke-virtual {p0, p1}, Llzm;->a(Loxn;)Llzm;

    move-result-object v0

    return-object v0
.end method
