.class final Lqc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lqb;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lqb;)V
    .locals 1

    .prologue
    .line 144
    iput-object p1, p0, Lqc;->a:Lqb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    new-instance v0, Lqd;

    invoke-direct {v0, p0}, Lqd;-><init>(Lqc;)V

    iput-object v0, p0, Lqc;->b:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    .prologue
    .line 174
    if-eqz p3, :cond_0

    .line 175
    iget-object v0, p0, Lqc;->a:Lqb;

    invoke-static {v0}, Lqb;->d(Lqb;)Lvy;

    move-result-object v0

    invoke-virtual {v0, p2}, Lvy;->a(I)V

    .line 177
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lqc;->a:Lqb;

    invoke-static {v0}, Lqb;->a(Lqb;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lqc;->a:Lqb;

    invoke-static {v0}, Lqb;->c(Lqb;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lqc;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Lqc;->a:Lqb;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lqb;->a(Lqb;Z)Z

    goto :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Lqc;->a:Lqb;

    invoke-static {v0}, Lqb;->c(Lqb;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lqc;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/SeekBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 170
    return-void
.end method
