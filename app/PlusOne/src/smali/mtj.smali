.class public final Lmtj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmtj;


# instance fields
.field public b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1904
    const/4 v0, 0x0

    new-array v0, v0, [Lmtj;

    sput-object v0, Lmtj;->a:[Lmtj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1905
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1910
    const/high16 v0, -0x80000000

    iput v0, p0, Lmtj;->c:I

    .line 1905
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1937
    const/4 v0, 0x0

    .line 1938
    iget-object v1, p0, Lmtj;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1939
    const/4 v0, 0x1

    iget-object v1, p0, Lmtj;->b:Ljava/lang/String;

    .line 1940
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1942
    :cond_0
    iget v1, p0, Lmtj;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 1943
    const/4 v1, 0x2

    iget v2, p0, Lmtj;->c:I

    .line 1944
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1946
    :cond_1
    iget-object v1, p0, Lmtj;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1947
    const/4 v1, 0x3

    iget-object v2, p0, Lmtj;->d:Ljava/lang/Integer;

    .line 1948
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1950
    :cond_2
    iget-object v1, p0, Lmtj;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 1951
    const/4 v1, 0x4

    iget-object v2, p0, Lmtj;->e:Ljava/lang/Integer;

    .line 1952
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1954
    :cond_3
    iget-object v1, p0, Lmtj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1955
    iput v0, p0, Lmtj;->ai:I

    .line 1956
    return v0
.end method

.method public a(Loxn;)Lmtj;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1964
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1965
    sparse-switch v0, :sswitch_data_0

    .line 1969
    iget-object v1, p0, Lmtj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1970
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtj;->ah:Ljava/util/List;

    .line 1973
    :cond_1
    iget-object v1, p0, Lmtj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1975
    :sswitch_0
    return-object p0

    .line 1980
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtj;->b:Ljava/lang/String;

    goto :goto_0

    .line 1984
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1985
    if-eq v0, v2, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 1989
    :cond_2
    iput v0, p0, Lmtj;->c:I

    goto :goto_0

    .line 1991
    :cond_3
    iput v2, p0, Lmtj;->c:I

    goto :goto_0

    .line 1996
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtj;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 2000
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmtj;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 1965
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1919
    iget-object v0, p0, Lmtj;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1920
    const/4 v0, 0x1

    iget-object v1, p0, Lmtj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1922
    :cond_0
    iget v0, p0, Lmtj;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 1923
    const/4 v0, 0x2

    iget v1, p0, Lmtj;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1925
    :cond_1
    iget-object v0, p0, Lmtj;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1926
    const/4 v0, 0x3

    iget-object v1, p0, Lmtj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1928
    :cond_2
    iget-object v0, p0, Lmtj;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 1929
    const/4 v0, 0x4

    iget-object v1, p0, Lmtj;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1931
    :cond_3
    iget-object v0, p0, Lmtj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1933
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1901
    invoke-virtual {p0, p1}, Lmtj;->a(Loxn;)Lmtj;

    move-result-object v0

    return-object v0
.end method
