.class public final Loem;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:[Loen;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 419
    invoke-direct {p0}, Loxq;-><init>()V

    .line 428
    const/high16 v0, -0x80000000

    iput v0, p0, Loem;->a:I

    .line 431
    sget-object v0, Loen;->a:[Loen;

    iput-object v0, p0, Loem;->b:[Loen;

    .line 419
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 452
    .line 453
    iget v0, p0, Loem;->a:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_2

    .line 454
    const/4 v0, 0x1

    iget v2, p0, Loem;->a:I

    .line 455
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 457
    :goto_0
    iget-object v2, p0, Loem;->b:[Loen;

    if-eqz v2, :cond_1

    .line 458
    iget-object v2, p0, Loem;->b:[Loen;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 459
    if-eqz v4, :cond_0

    .line 460
    const/4 v5, 0x2

    .line 461
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 458
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 465
    :cond_1
    iget-object v1, p0, Loem;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 466
    iput v0, p0, Loem;->ai:I

    .line 467
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Loem;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 475
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 476
    sparse-switch v0, :sswitch_data_0

    .line 480
    iget-object v2, p0, Loem;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 481
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Loem;->ah:Ljava/util/List;

    .line 484
    :cond_1
    iget-object v2, p0, Loem;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 486
    :sswitch_0
    return-object p0

    .line 491
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 492
    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 495
    :cond_2
    iput v0, p0, Loem;->a:I

    goto :goto_0

    .line 497
    :cond_3
    iput v4, p0, Loem;->a:I

    goto :goto_0

    .line 502
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 503
    iget-object v0, p0, Loem;->b:[Loen;

    if-nez v0, :cond_5

    move v0, v1

    .line 504
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Loen;

    .line 505
    iget-object v3, p0, Loem;->b:[Loen;

    if-eqz v3, :cond_4

    .line 506
    iget-object v3, p0, Loem;->b:[Loen;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 508
    :cond_4
    iput-object v2, p0, Loem;->b:[Loen;

    .line 509
    :goto_2
    iget-object v2, p0, Loem;->b:[Loen;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 510
    iget-object v2, p0, Loem;->b:[Loen;

    new-instance v3, Loen;

    invoke-direct {v3}, Loen;-><init>()V

    aput-object v3, v2, v0

    .line 511
    iget-object v2, p0, Loem;->b:[Loen;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 512
    invoke-virtual {p1}, Loxn;->a()I

    .line 509
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 503
    :cond_5
    iget-object v0, p0, Loem;->b:[Loen;

    array-length v0, v0

    goto :goto_1

    .line 515
    :cond_6
    iget-object v2, p0, Loem;->b:[Loen;

    new-instance v3, Loen;

    invoke-direct {v3}, Loen;-><init>()V

    aput-object v3, v2, v0

    .line 516
    iget-object v2, p0, Loem;->b:[Loen;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 476
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 436
    iget v0, p0, Loem;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 437
    const/4 v0, 0x1

    iget v1, p0, Loem;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 439
    :cond_0
    iget-object v0, p0, Loem;->b:[Loen;

    if-eqz v0, :cond_2

    .line 440
    iget-object v1, p0, Loem;->b:[Loen;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 441
    if-eqz v3, :cond_1

    .line 442
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 440
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 446
    :cond_2
    iget-object v0, p0, Loem;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 448
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 415
    invoke-virtual {p0, p1}, Loem;->a(Loxn;)Loem;

    move-result-object v0

    return-object v0
.end method
