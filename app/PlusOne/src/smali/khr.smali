.class public final Lkhr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/libraries/social/settings/PreferenceScreen;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lkhr;->a:Landroid/content/Context;

    .line 24
    const-class v0, Lkij;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkij;

    .line 25
    invoke-interface {v0}, Lkij;->a()Lcom/google/android/libraries/social/settings/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lkhr;->b:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    .line 26
    return-void
.end method


# virtual methods
.method public a(I)Lcom/google/android/libraries/social/settings/PreferenceCategory;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lkhr;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lkhr;->b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/libraries/social/settings/PreferenceCategory;

    iget-object v1, p0, Lkhr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 110
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/PreferenceCategory;->e(Ljava/lang/CharSequence;)V

    .line 111
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lkhl;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lkhl;

    iget-object v1, p0, Lkhr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkhl;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-virtual {v0, p1}, Lkhl;->e(Ljava/lang/CharSequence;)V

    .line 36
    invoke-virtual {v0, p2}, Lkhl;->d(Ljava/lang/CharSequence;)V

    .line 37
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0, p1, p2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;)Lkhl;

    move-result-object v0

    .line 48
    invoke-virtual {v0, p3}, Lkhl;->a(Landroid/content/Intent;)V

    .line 49
    return-object v0
.end method

.method public a(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lkhr;->b:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p2}, Lkhr;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lkhl;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->c(Lkhl;)Z

    .line 285
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/libraries/social/settings/LabelPreference;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Lcom/google/android/libraries/social/settings/LabelPreference;

    iget-object v1, p0, Lkhr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/settings/LabelPreference;-><init>(Landroid/content/Context;)V

    .line 98
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/LabelPreference;->e(Ljava/lang/CharSequence;)V

    .line 99
    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/CharSequence;)V

    .line 100
    invoke-virtual {v0, p3}, Lcom/google/android/libraries/social/settings/LabelPreference;->a(Landroid/content/Intent;)V

    .line 101
    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;
    .locals 2

    .prologue
    .line 239
    invoke-virtual {p0, p1}, Lkhr;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/settings/PreferenceCategory;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lkhr;->b:Lcom/google/android/libraries/social/settings/PreferenceScreen;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/settings/PreferenceScreen;->c(Lkhl;)Z

    .line 241
    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lkhl;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Lkhj;

    iget-object v1, p0, Lkhr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkhj;-><init>(Landroid/content/Context;)V

    .line 59
    invoke-virtual {v0, p1}, Lkhl;->e(Ljava/lang/CharSequence;)V

    .line 60
    invoke-virtual {v0, p2}, Lkhl;->d(Ljava/lang/CharSequence;)V

    .line 61
    return-object v0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/LabelPreference;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/libraries/social/settings/LabelPreference;

    iget-object v1, p0, Lkhr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/settings/LabelPreference;-><init>(Landroid/content/Context;)V

    .line 84
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/LabelPreference;->e(Ljava/lang/CharSequence;)V

    .line 85
    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/settings/LabelPreference;->d(Ljava/lang/CharSequence;)V

    .line 86
    return-object v0
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;
    .locals 2

    .prologue
    .line 120
    new-instance v0, Lcom/google/android/libraries/social/settings/CheckBoxPreference;

    iget-object v1, p0, Lkhr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 121
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->e(Ljava/lang/CharSequence;)V

    .line 122
    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/CharSequence;)V

    .line 123
    return-object v0
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/settings/CheckBoxPreference;
    .locals 2

    .prologue
    .line 132
    new-instance v0, Lkhi;

    iget-object v1, p0, Lkhr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkhi;-><init>(Landroid/content/Context;)V

    .line 133
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->e(Ljava/lang/CharSequence;)V

    .line 134
    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/settings/CheckBoxPreference;->d(Ljava/lang/CharSequence;)V

    .line 135
    return-object v0
.end method

.method public f(Ljava/lang/String;Ljava/lang/String;)Lkgw;
    .locals 2

    .prologue
    .line 162
    new-instance v0, Lkgw;

    iget-object v1, p0, Lkhr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkgw;-><init>(Landroid/content/Context;)V

    .line 163
    invoke-virtual {v0, p1}, Lkgw;->e(Ljava/lang/CharSequence;)V

    .line 167
    invoke-virtual {v0, p1}, Lkgw;->a(Ljava/lang/CharSequence;)V

    .line 168
    invoke-virtual {v0, p2}, Lkgw;->d(Ljava/lang/CharSequence;)V

    .line 169
    const v1, 0x7f0a0596

    invoke-virtual {v0, v1}, Lkgw;->a(I)V

    .line 170
    const v1, 0x7f0a0597

    invoke-virtual {v0, v1}, Lkgw;->b(I)V

    .line 171
    return-object v0
.end method

.method public g(Ljava/lang/String;Ljava/lang/String;)Lkha;
    .locals 2

    .prologue
    .line 202
    new-instance v0, Lkha;

    iget-object v1, p0, Lkhr;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkha;-><init>(Landroid/content/Context;)V

    .line 203
    invoke-virtual {v0, p1}, Lkha;->e(Ljava/lang/CharSequence;)V

    .line 207
    invoke-virtual {v0, p1}, Lkha;->a(Ljava/lang/CharSequence;)V

    .line 208
    invoke-virtual {v0, p2}, Lkha;->d(Ljava/lang/CharSequence;)V

    .line 209
    return-object v0
.end method
