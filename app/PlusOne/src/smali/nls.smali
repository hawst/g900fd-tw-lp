.class public final Lnls;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lnjt;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Loxq;-><init>()V

    .line 72
    const/high16 v0, -0x80000000

    iput v0, p0, Lnls;->c:I

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lnls;->b:Lnjt;

    .line 69
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 98
    iget v1, p0, Lnls;->c:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 99
    const/4 v0, 0x1

    iget v1, p0, Lnls;->c:I

    .line 100
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 102
    :cond_0
    iget-object v1, p0, Lnls;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 103
    const/4 v1, 0x2

    iget-object v2, p0, Lnls;->a:Ljava/lang/String;

    .line 104
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_1
    iget-object v1, p0, Lnls;->b:Lnjt;

    if-eqz v1, :cond_2

    .line 107
    const/4 v1, 0x3

    iget-object v2, p0, Lnls;->b:Lnjt;

    .line 108
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_2
    iget-object v1, p0, Lnls;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    iput v0, p0, Lnls;->ai:I

    .line 112
    return v0
.end method

.method public a(Loxn;)Lnls;
    .locals 2

    .prologue
    .line 120
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 121
    sparse-switch v0, :sswitch_data_0

    .line 125
    iget-object v1, p0, Lnls;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 126
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnls;->ah:Ljava/util/List;

    .line 129
    :cond_1
    iget-object v1, p0, Lnls;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    :sswitch_0
    return-object p0

    .line 136
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 137
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 154
    :cond_2
    iput v0, p0, Lnls;->c:I

    goto :goto_0

    .line 156
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lnls;->c:I

    goto :goto_0

    .line 161
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnls;->a:Ljava/lang/String;

    goto :goto_0

    .line 165
    :sswitch_3
    iget-object v0, p0, Lnls;->b:Lnjt;

    if-nez v0, :cond_4

    .line 166
    new-instance v0, Lnjt;

    invoke-direct {v0}, Lnjt;-><init>()V

    iput-object v0, p0, Lnls;->b:Lnjt;

    .line 168
    :cond_4
    iget-object v0, p0, Lnls;->b:Lnjt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 121
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 82
    iget v0, p0, Lnls;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 83
    const/4 v0, 0x1

    iget v1, p0, Lnls;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 85
    :cond_0
    iget-object v0, p0, Lnls;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 86
    const/4 v0, 0x2

    iget-object v1, p0, Lnls;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 88
    :cond_1
    iget-object v0, p0, Lnls;->b:Lnjt;

    if-eqz v0, :cond_2

    .line 89
    const/4 v0, 0x3

    iget-object v1, p0, Lnls;->b:Lnjt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 91
    :cond_2
    iget-object v0, p0, Lnls;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 93
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lnls;->a(Loxn;)Lnls;

    move-result-object v0

    return-object v0
.end method
