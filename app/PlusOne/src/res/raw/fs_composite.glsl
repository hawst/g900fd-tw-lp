/**
 * The theme fragment shader that is used mulitple times per frame to draw
 * textured quads.
 * Supports:
 *   Optional color correction
 *   Optional color adjustment via a look-up-table
 * Note this shader does not support (unlike fs_main):
 *   Variable barrel distortion
 *   Optional background sampling
 *   Cropping
 */
precision mediump float;

// Uniforms:
// Input frame A
uniform sampler2D sampler_primary_frame;

// Frame A - color correction
uniform sampler2D sampler_primary_color_correct;
uniform int color_correct_active_a;
uniform float color_correct_factor_a;

// 3D lut
uniform sampler2D sampler_lut;
uniform int lut_is_active;

// Vertex-stream inputs:
varying vec2 v_texcoord;
varying vec2 v_primary_texcoord;

// Constants:
const float third = 1.0 / 3.0;

vec3 colorCorrect(in sampler2D lookup_texture, in vec3 rgb, in float correction_factor) {
    float rgb_mean = (rgb.r + rgb.g + rgb.b) * third;
    rgb = ((rgb - rgb_mean) * correction_factor) + rgb_mean;
    rgb = rgb * third + third;
    rgb.r = texture2D(lookup_texture, vec2(rgb.r, 0.0)).r;
    rgb.g = texture2D(lookup_texture, vec2(rgb.g, 0.0)).g;
    rgb.b = texture2D(lookup_texture, vec2(rgb.b, 0.0)).b;
    return rgb;
}

vec4 texture3D(in sampler2D tex, in float size, in vec3 texCoord) {
    float zSliceWidth = 1.0 / size;
    float singlePixelWidth = zSliceWidth / size;
    // need to pad slices by 1/2 pixel on each side to avoid interpolation between slices
    float zSlicePaddedWidth = singlePixelWidth * (size - 1.0);
    float zSliceLower = min(floor(texCoord.z * size), size - 1.0);
    float zSliceUpper = min(zSliceLower + 1.0, size - 1.0);
    float xOffset = texCoord.x * zSlicePaddedWidth + singlePixelWidth / 2.0;
    float lowerSliceX = xOffset + (zSliceLower * zSliceWidth);
    float upperSliceX = xOffset + (zSliceUpper * zSliceWidth);
    vec4 lowerSliceColor = texture2D(tex, vec2(lowerSliceX, texCoord.y));
    vec4 upperSliceColor = texture2D(tex, vec2(upperSliceX, texCoord.y));
    float zOffset = mod(texCoord.z * size, 1.0);
    return mix(lowerSliceColor, upperSliceColor, zOffset);
}

void main() {
    vec2 coord = v_primary_texcoord;
    vec4 color = texture2D(sampler_primary_frame, coord);
    color = vec4(color.rgb * color.a, 1.0);

    if (color_correct_active_a != 0) {
        color.rgb = colorCorrect(sampler_primary_color_correct, color.rgb, color_correct_factor_a);
    }

    if (lut_is_active != 0) {
        // The color must be passed in as RBG because the luts have red on the x axis,
        // blue on the y axis, and green on the z axis.
        color = texture3D(sampler_lut, 16.0, color.rbg);
    }

    gl_FragColor = vec4(color.rgb, 1.0);
}
