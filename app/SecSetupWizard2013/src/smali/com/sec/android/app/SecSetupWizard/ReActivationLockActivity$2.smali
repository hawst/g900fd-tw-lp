.class Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;
.super Ljava/lang/Object;
.source "ReActivationLockActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    # getter for: Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->reactivateChecked:Z
    invoke-static {v1}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->access$000(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    # getter for: Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->access$200(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/SecSetupWizard/Utils;->setReactivationFlag(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v3, :cond_0

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "lock_my_mobile"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.reactivationlock_on"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    # getter for: Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;
    invoke-static {v1}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->access$100(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)Lcom/sec/android/app/SecSetupWizard/LogMsg;

    move-result-object v1

    const-string v2, "ReActivationLockActivity"

    const-string v3, " onActivityResult[reactivationlock_on]"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    # getter for: Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->access$200(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 95
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->setResult(I)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->finish()V

    .line 97
    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    # getter for: Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;
    invoke-static {v1}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->access$100(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)Lcom/sec/android/app/SecSetupWizard/LogMsg;

    move-result-object v1

    const-string v2, "ReActivationLockActivity"

    const-string v3, " onActivityResult[reactivationlock_fail]"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;->this$0:Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    # getter for: Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;
    invoke-static {v1}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->access$100(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)Lcom/sec/android/app/SecSetupWizard/LogMsg;

    move-result-object v1

    const-string v2, "ReActivationLockActivity"

    const-string v3, " onActivityResult[reactivationlock_skip]"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
