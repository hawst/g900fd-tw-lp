.class public Lcom/sec/android/app/SecSetupWizard/KorV3Activity;
.super Landroid/app/Activity;
.source "KorV3Activity.java"


# static fields
.field private static mCocktailBarEnabled:Z


# instance fields
.field final SAMSUNG:I

.field installKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->mCocktailBarEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->SAMSUNG:I

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->installKey:I

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 20
    sget-boolean v0, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->mCocktailBarEnabled:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/SecSetupWizard/KorV3Activity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/SecSetupWizard/KorV3Activity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->updateCocktailbarPanelForEachStep(Ljava/lang/String;)V

    return-void
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 51
    const v1, 0x7f0e0016

    invoke-virtual {p0, v1}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 52
    .local v0, "nextBtnArea":Landroid/widget/LinearLayout;
    new-instance v1, Lcom/sec/android/app/SecSetupWizard/KorV3Activity$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity$1;-><init>(Lcom/sec/android/app/SecSetupWizard/KorV3Activity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    return-void
.end method

.method private setIndicatorTransparency()V
    .locals 3

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 115
    .local v0, "wmLp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, -0x7c000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 116
    return-void
.end method

.method private updateCocktailbarPanelForEachStep(Ljava/lang/String;)V
    .locals 3
    .param p1, "step"    # Ljava/lang/String;

    .prologue
    .line 147
    const-string v0, "KorV3Activity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateCocktailbarPanelForEachStep, step: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 129
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 130
    const-string v0, "Setup_V3 Mobile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const-string v0, "Setup_V3 Mobile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resultCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    packed-switch p1, :pswitch_data_0

    .line 141
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->mCocktailBarEnabled:Z

    if-eqz v0, :cond_1

    .line 142
    const-string v0, "kor_v3_end"

    invoke-direct {p0, v0}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->updateCocktailbarPanelForEachStep(Ljava/lang/String;)V

    .line 144
    :cond_1
    return-void

    .line 134
    :pswitch_0
    if-ne p2, v3, :cond_0

    .line 135
    invoke-virtual {p0, v3}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->setResult(I)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->finish()V

    goto :goto_0

    .line 132
    :pswitch_data_0
    .packed-switch 0x3f2
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->hide()V

    .line 38
    :cond_0
    new-instance v0, Lcom/sec/android/app/SecSetupWizard/BasicForm;

    invoke-direct {v0, p0}, Lcom/sec/android/app/SecSetupWizard/BasicForm;-><init>(Landroid/content/Context;)V

    .line 39
    .local v0, "bf":Lcom/sec/android/app/SecSetupWizard/BasicForm;
    const v1, 0x7f03000d

    invoke-virtual {v0, v1}, Lcom/sec/android/app/SecSetupWizard/BasicForm;->changeScreen(I)V

    .line 40
    invoke-virtual {v0}, Lcom/sec/android/app/SecSetupWizard/BasicForm;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->setContentView(Landroid/view/View;)V

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->initView()V

    .line 43
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.cocktailbar"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->mCocktailBarEnabled:Z

    .line 45
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isVideoSequenceModel()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/KorV3Activity;->setIndicatorTransparency()V

    .line 48
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 123
    const/4 v0, 0x1

    return v0
.end method
