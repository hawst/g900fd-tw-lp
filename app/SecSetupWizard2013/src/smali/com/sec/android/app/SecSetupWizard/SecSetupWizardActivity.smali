.class public Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;
.super Landroid/app/Activity;
.source "SecSetupWizardActivity.java"


# static fields
.field private static final LARGE_FONT_SCALE:F

.field private static mCocktailBarEnabled:Z

.field private static mCocktailBarServiceStarted:Z

.field private static final mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

.field private static mListForMUM:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static mReactivationLock:Z

.field private static mSkipListForSubUsers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

.field private mChangeFont:Z

.field private final mCurConfig:Landroid/content/res/Configuration;

.field private mCurrentResult:I

.field private mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

.field private mIsOwner:Z

.field private mIsSettingsChangesAllowed:Z

.field private mIsSupportMUM:Z

.field private mRestPolicy:Landroid/app/enterprise/RestrictionPolicy;

.field mTalkbackOnOffObserver:Landroid/database/ContentObserver;

.field private objValue:Ljava/lang/Object;

.field private v_context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 87
    new-instance v0, Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-direct {v0}, Lcom/sec/android/app/SecSetupWizard/SequenceList;-><init>()V

    sput-object v0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    .line 90
    sput-boolean v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mReactivationLock:Z

    .line 113
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->has7StepsLargeFontScale()F

    move-result v0

    sput v0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->LARGE_FONT_SCALE:F

    .line 117
    sput-boolean v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCocktailBarEnabled:Z

    .line 118
    sput-boolean v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCocktailBarServiceStarted:Z

    .line 126
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "language_setup"

    aput-object v2, v1, v4

    const-string v2, "reactivation_lock_setup"

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string v3, "date_and_time_setup"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "video_intro"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "sec_backup_setup"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mSkipListForSubUsers:Ljava/util/Set;

    .line 131
    new-instance v0, Ljava/util/HashSet;

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, "welcome"

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mListForMUM:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 94
    iput-boolean v0, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSettingsChangesAllowed:Z

    .line 100
    iput-boolean v0, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsOwner:Z

    .line 101
    iput-boolean v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSupportMUM:Z

    .line 111
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurConfig:Landroid/content/res/Configuration;

    .line 114
    iput-boolean v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mChangeFont:Z

    .line 349
    new-instance v0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity$1;-><init>(Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mTalkbackOnOffObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method private disableNotifications()V
    .locals 4

    .prologue
    .line 1506
    const-string v2, "statusbar"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 1507
    .local v0, "mStatusBarManager":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 1508
    const-string v2, "SecSetupWizardActivity"

    const-string v3, "disableNotifications"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1510
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isCameraOnlyModel()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1511
    const/high16 v1, 0x3870000

    .line 1525
    .local v1, "statusBarFlag":I
    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 1528
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.cocktailbar"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1532
    .end local v1    # "statusBarFlag":I
    :cond_0
    return-void

    .line 1518
    :cond_1
    const/high16 v1, 0x3a70000

    .restart local v1    # "statusBarFlag":I
    goto :goto_0
.end method

.method private enableNotifications()V
    .locals 3

    .prologue
    .line 1493
    const-string v1, "statusbar"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 1494
    .local v0, "mStatusBarManager":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 1495
    const-string v1, "SecSetupWizardActivity"

    const-string v2, "enableNotifications"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 1499
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.cocktailbar"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1503
    :cond_0
    return-void
.end method

.method private isSkipAttWelcomeScreen()Z
    .locals 1

    .prologue
    .line 783
    const/4 v0, 0x1

    .line 786
    .local v0, "skipItem":Z
    return v0
.end method

.method private isSkipBackgroundTraffic()Z
    .locals 4

    .prologue
    .line 674
    const/4 v0, 0x1

    .line 675
    .local v0, "isSkip":Z
    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSimMissing(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 676
    const/4 v1, 0x1

    .line 686
    :goto_0
    return v1

    .line 678
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSktAccountStatus()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDomesticSKTModel()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 679
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v2, "SecSetupWizardActivity"

    const-string v3, "isSkipBackgroundTraffic: SKT operator "

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    const/4 v0, 0x0

    :goto_1
    move v1, v0

    .line 686
    goto :goto_0

    .line 682
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v2, "SecSetupWizardActivity"

    const-string v3, "isSkipBackgroundTraffic: Skip background traffic"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private isSkipBaiduLocation()Z
    .locals 3

    .prologue
    .line 711
    const/4 v0, 0x1

    .line 712
    .local v0, "isSkip":Z
    const-string v1, "CHU"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHN"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CTC"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHC"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 713
    :cond_0
    const/4 v0, 0x0

    .line 715
    :cond_1
    return v0
.end method

.method private isSkipCMCC()Z
    .locals 3

    .prologue
    .line 719
    const/4 v0, 0x1

    .line 720
    .local v0, "isSkip":Z
    const-string v1, "CHM"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 721
    const/4 v0, 0x0

    .line 723
    :cond_0
    return v0
.end method

.method private isSkipCTCregistration()Z
    .locals 3

    .prologue
    .line 735
    const/4 v0, 0x1

    .line 736
    .local v0, "isSkip":Z
    const-string v1, "CTC"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 737
    const/4 v0, 0x0

    .line 739
    :cond_0
    return v0
.end method

.method private isSkipCUregistration()Z
    .locals 3

    .prologue
    .line 727
    const/4 v0, 0x1

    .line 728
    .local v0, "isSkip":Z
    const-string v1, "CHU"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 729
    const/4 v0, 0x0

    .line 731
    :cond_0
    return v0
.end method

.method private isSkipDataConnection()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 653
    const/4 v0, 0x1

    .line 654
    .local v0, "isSkip":Z
    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSimMissing(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 669
    :cond_0
    :goto_0
    return v1

    .line 657
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 660
    const-string v1, "XEO"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "PLS"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "TPL"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "PRT"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 662
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v2, "SecSetupWizardActivity"

    const-string v3, "isSkipDataConnection: poland operator "

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    const/4 v0, 0x0

    :goto_1
    move v1, v0

    .line 669
    goto :goto_0

    .line 665
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v2, "SecSetupWizardActivity"

    const-string v3, "isSkipDataConnection: Skip data page"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private isSkipDateTime()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 690
    const-string v2, "ATT"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "XTC"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "SMA"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "GLB"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "XTE"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 706
    :cond_1
    :goto_0
    return v0

    .line 698
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 699
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v2, "SecSetupWizardActivity"

    const-string v3, "isSkipDateTime : wifiOnly models "

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 703
    :cond_3
    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSimMissing(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 706
    goto :goto_0
.end method

.method private isSkipHfa()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 762
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.sprint.samsung.OMADMExtension.HFA"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 763
    .local v1, "i":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 764
    .local v2, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v2, v1, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 766
    .local v0, "activities":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v0, :cond_1

    .line 770
    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    move v3, v4

    goto :goto_0
.end method

.method private isSkipKorV3()Z
    .locals 3

    .prologue
    .line 774
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDomesticModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v1, "SecSetupWizardActivity"

    const-string v2, "isSkipKorV3: Korea Operator"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    const/4 v0, 0x0

    .line 778
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSkipMyAccount()Z
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 831
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 833
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const-string v8, "com.tmobile.pr.mytmobile"

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    iget-object v4, v8, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 834
    .local v4, "rawVersion":Ljava/lang/String;
    const/4 v8, 0x0

    const/4 v9, 0x3

    invoke-virtual {v4, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 835
    .local v5, "stringVerson":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 837
    .local v2, "numVersion":F
    iget-object v8, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v9, "SecSetupWizardActivity"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isSkipMyAccount: rawVersion = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", stringVerson = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", numVersion = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 840
    const v8, 0x40666666    # 3.6f

    cmpg-float v8, v2, v8

    if-gez v8, :cond_0

    .line 846
    .end local v2    # "numVersion":F
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    .end local v4    # "rawVersion":Ljava/lang/String;
    .end local v5    # "stringVerson":Ljava/lang/String;
    :goto_0
    return v6

    .restart local v2    # "numVersion":F
    .restart local v3    # "pm":Landroid/content/pm/PackageManager;
    .restart local v4    # "rawVersion":Ljava/lang/String;
    .restart local v5    # "stringVerson":Ljava/lang/String;
    :cond_0
    move v6, v7

    .line 840
    goto :goto_0

    .line 841
    .end local v2    # "numVersion":F
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    .end local v4    # "rawVersion":Ljava/lang/String;
    .end local v5    # "stringVerson":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 842
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    iget-object v7, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v8, "SecSetupWizardActivity"

    const-string v9, "isSkipMyAccount: NameNotFoundException"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 844
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v1

    .line 845
    .local v1, "nfe":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v8, "SecSetupWizardActivity"

    const-string v9, "isSkipMyAccount: NumberFormatException"

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isSkipSamsungAccountScreen()Z
    .locals 1

    .prologue
    .line 790
    const/4 v0, 0x0

    .line 793
    .local v0, "skipItem":Z
    return v0
.end method

.method private isSkipSamsungHealthScreen()Z
    .locals 3

    .prologue
    .line 797
    const/4 v0, 0x0

    .line 798
    .local v0, "skipItem":Z
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v1

    const-string v2, "SEC_FLOATING_FEATURE_SHEALTH_SUPPORT_HEALTH_SHORTCUT"

    invoke-virtual {v1, v2}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 799
    const/4 v0, 0x1

    .line 803
    :cond_0
    :goto_0
    return v0

    .line 800
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSecHealthSkipState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 801
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSkipSecEasySignUp()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 743
    const/4 v0, 0x1

    .line 744
    .local v0, "isSkip":Z
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->v_context:Landroid/content/Context;

    const-string v4, "com.samsung.android.coreapps"

    invoke-static {v3, v4}, Lcom/sec/android/app/SecSetupWizard/Utils;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/samsung/android/coreapps/sdk/easysignup/EasySignUpManager;->getSupportedFeatures(Landroid/content/Context;I)I

    move-result v3

    if-ltz v3, :cond_1

    .line 747
    const-string v3, "setupwizard"

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 748
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v3, "sec_account_signup_in_setupwizard"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 750
    .local v2, "sec_account_signup_in_setupwizard":Z
    if-eqz v2, :cond_0

    .line 751
    const/4 v0, 0x1

    .line 757
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    .end local v2    # "sec_account_signup_in_setupwizard":Z
    :goto_0
    return v0

    .line 753
    .restart local v1    # "pref":Landroid/content/SharedPreferences;
    .restart local v2    # "sec_account_signup_in_setupwizard":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 755
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    .end local v2    # "sec_account_signup_in_setupwizard":Z
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v4, "SecSetupWizardActivity"

    const-string v5, "no easysignup."

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isSkipSktAccount()Z
    .locals 5

    .prologue
    .line 807
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 808
    .local v1, "mTelephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 809
    .local v0, "CheckNum":Ljava/lang/String;
    const-string v2, "SktAccount"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSktAccountStatus : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSktAccountStatus()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSktAccountStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 812
    iget-object v2, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v3, "SecSetupWizardActivity"

    const-string v4, "isSkipSktAccount: SKT Models"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const/4 v2, 0x0

    .line 815
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isSkipSktTphoneSetup()Z
    .locals 3

    .prologue
    .line 820
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSktTphoneSetupStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDomesticSKTModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v1, "SecSetupWizardActivity"

    const-string v2, "isSkipSktTphoneSetup: SKT Models"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    const/4 v0, 0x0

    .line 824
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSkipWifi()Z
    .locals 3

    .prologue
    .line 635
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mRestPolicy:Landroid/app/enterprise/RestrictionPolicy;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mRestPolicy:Landroid/app/enterprise/RestrictionPolicy;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/enterprise/RestrictionPolicy;->isSettingsChangesAllowed(Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 636
    const/4 v0, 0x1

    .line 648
    :cond_0
    :goto_0
    return v0

    .line 640
    :cond_1
    const/4 v0, 0x0

    .line 645
    .local v0, "isSkip":Z
    const-string v1, "ATT"

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->readSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 646
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private killSetupWizardProcess()V
    .locals 2

    .prologue
    .line 1535
    const-string v0, "SecSetupWizardActivity"

    const-string v1, "killSetupWizardProcess"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1536
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 1537
    return-void
.end method

.method private launchCameraApp()V
    .locals 5

    .prologue
    .line 535
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 538
    .local v0, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.android.app.firmwareupdateservice"

    const-string v4, "com.sec.android.app.firmwareupdateservice.FUDisclaimerActivity"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 540
    const-string v1, "com.sec.android.app.firmwareupdateservice"

    const-string v2, "com.sec.android.app.firmwareupdateservice.FUDisclaimerActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 545
    :goto_0
    const-string v1, "from-sw"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 546
    invoke-virtual {p0, v0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 549
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->finish()V

    .line 550
    return-void

    .line 542
    :cond_0
    :try_start_1
    const-string v1, "com.sec.android.app.camera"

    const-string v2, "com.sec.android.app.camera.Camera"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 543
    const-string v1, "from-am"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 547
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private makeCameraOnlySequenceList()V
    .locals 9

    .prologue
    .line 474
    const/4 v2, 0x0

    .line 476
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "fromCamera"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 477
    const-string v5, "persist.sys.setupwizard_part"

    const-string v6, "2"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    :cond_0
    const-string v5, "persist.sys.setupwizard_part"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 480
    .local v3, "part":Ljava/lang/String;
    const-string v5, "camera"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 481
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    const-string v7, "makeCameraOnlySequenceList - Camera"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :goto_0
    return-void

    .line 483
    :cond_1
    const-string v5, "2"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 484
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    const-string v7, "makeCameraOnlySequenceList - part 2"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070008

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 492
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 493
    .local v4, "s":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/SecSetupWizard/IntentMaker;

    invoke-direct {v1, v4}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;-><init>(Ljava/lang/String;)V

    .line 494
    .local v1, "im":Lcom/sec/android/app/SecSetupWizard/IntentMaker;
    invoke-virtual {v1, p0}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->makeIntent(Landroid/content/Context;)V

    .line 495
    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getIntent()Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/SecSetupWizard/Utils;->isShouldSkipActivity(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 496
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getRequestCode()I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->insert(Ljava/lang/String;Landroid/content/Intent;I)Z

    goto :goto_2

    .line 487
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "im":Lcom/sec/android/app/SecSetupWizard/IntentMaker;
    .end local v4    # "s":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    const-string v7, "makeCameraOnlySequenceList - part 1"

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    const-string v5, "persist.sys.setupwizard_part"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070007

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    goto :goto_1

    .line 499
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setSkipNetworkCharges()V

    goto :goto_0
.end method

.method private makeDefaultSequenceList()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 429
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070003

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 430
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 431
    .local v4, "s":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/SecSetupWizard/IntentMaker;

    invoke-direct {v1, v4}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;-><init>(Ljava/lang/String;)V

    .line 432
    .local v1, "im":Lcom/sec/android/app/SecSetupWizard/IntentMaker;
    invoke-virtual {v1, p0}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->makeIntent(Landroid/content/Context;)V

    .line 433
    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getIntent()Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/SecSetupWizard/Utils;->isShouldSkipActivity(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    sget-boolean v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mReactivationLock:Z

    if-ne v9, v5, :cond_0

    .line 434
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 435
    .local v2, "key":Ljava/lang/String;
    iget-boolean v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSupportMUM:Z

    if-nez v5, :cond_3

    .line 436
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mListForMUM:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 445
    :cond_2
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getRequestCode()I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->insert(Ljava/lang/String;Landroid/content/Intent;I)Z

    .line 446
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "insert in list : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 438
    :cond_3
    iget-boolean v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsOwner:Z

    if-nez v5, :cond_4

    .line 439
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mSkipListForSubUsers:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    .line 441
    :cond_4
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mListForMUM:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    .line 450
    .end local v1    # "im":Lcom/sec/android/app/SecSetupWizard/IntentMaker;
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "s":Ljava/lang/String;
    :cond_5
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getInsertedSimCount()I

    move-result v5

    if-le v5, v9, :cond_6

    .line 451
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->makeDsaSequenceList()V

    .line 454
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setSkipNetworkCharges()V

    .line 458
    return-void
.end method

.method private makeDsaSequenceList()V
    .locals 8

    .prologue
    .line 461
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 462
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 463
    .local v3, "s":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/SecSetupWizard/IntentMaker;

    invoke-direct {v1, v3}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;-><init>(Ljava/lang/String;)V

    .line 464
    .local v1, "im":Lcom/sec/android/app/SecSetupWizard/IntentMaker;
    invoke-virtual {v1, p0}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->makeIntent(Landroid/content/Context;)V

    .line 465
    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/SecSetupWizard/Utils;->isShouldSkipActivity(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 466
    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getRequestCode()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->insert(Ljava/lang/String;Landroid/content/Intent;I)Z

    .line 467
    iget-object v4, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v5, "SecSetupWizardActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "insert in list (dsa) : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 470
    .end local v1    # "im":Lcom/sec/android/app/SecSetupWizard/IntentMaker;
    .end local v3    # "s":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private makeVideoIntroSequenceList()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 391
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070005

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 392
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-boolean v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsOwner:Z

    if-nez v5, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070009

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 394
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 395
    .local v4, "s":Ljava/lang/String;
    new-instance v1, Lcom/sec/android/app/SecSetupWizard/IntentMaker;

    invoke-direct {v1, v4}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;-><init>(Ljava/lang/String;)V

    .line 396
    .local v1, "im":Lcom/sec/android/app/SecSetupWizard/IntentMaker;
    invoke-virtual {v1, p0}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->makeIntent(Landroid/content/Context;)V

    .line 397
    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getIntent()Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/SecSetupWizard/Utils;->isShouldSkipActivity(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    sget-boolean v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mReactivationLock:Z

    if-ne v9, v5, :cond_1

    .line 398
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 399
    .local v2, "key":Ljava/lang/String;
    const-string v5, "dropbox_setup"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSkipDropBoxModel()Z

    move-result v5

    if-nez v5, :cond_1

    .line 400
    :cond_3
    iget-boolean v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSupportMUM:Z

    if-nez v5, :cond_6

    .line 401
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mListForMUM:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 402
    const-string v5, "sec_account_setup"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDisallowAccount(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 403
    :cond_4
    const-string v5, "sec_backup_setup"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDisallowAccount(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 416
    :cond_5
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getRequestCode()I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->insert(Ljava/lang/String;Landroid/content/Intent;I)Z

    .line 417
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "insert in list : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/IntentMaker;->getKey()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 405
    :cond_6
    iget-boolean v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsOwner:Z

    if-nez v5, :cond_8

    .line 406
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mSkipListForSubUsers:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 407
    const-string v5, "sec_account_setup"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDisallowAccount(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 408
    :cond_7
    const-string v5, "sec_backup_setup"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDisallowAccount(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto/16 :goto_0

    .line 410
    :cond_8
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mListForMUM:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 411
    const-string v5, "sec_account_setup"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDisallowAccount(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 412
    :cond_9
    const-string v5, "sec_backup_setup"

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDisallowAccount(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    goto/16 :goto_0

    .line 421
    .end local v1    # "im":Lcom/sec/android/app/SecSetupWizard/IntentMaker;
    .end local v2    # "key":Ljava/lang/String;
    .end local v4    # "s":Ljava/lang/String;
    :cond_a
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getInsertedSimCount()I

    move-result v5

    if-le v5, v9, :cond_b

    .line 422
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->makeDsaSequenceList()V

    .line 425
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setSkipNetworkCharges()V

    .line 426
    return-void
.end method

.method public static secFileExist(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 200
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 202
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 203
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 204
    const/4 v1, 0x1

    .line 208
    :cond_0
    return v1
.end method

.method public static secFileRead(Ljava/lang/String;)[B
    .locals 14
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 156
    const/4 v1, 0x0

    .line 157
    .local v1, "file_fd":Ljava/io/File;
    const/4 v3, 0x0

    .line 158
    .local v3, "fin":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 161
    .local v7, "read_file":[B
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    .end local v1    # "file_fd":Ljava/io/File;
    .local v2, "file_fd":Ljava/io/File;
    :try_start_1
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/StreamCorruptedException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 164
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .local v4, "fin":Ljava/io/FileInputStream;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v8

    .line 165
    .local v8, "size":J
    const-wide/32 v10, 0x7fffffff

    cmp-long v10, v8, v10

    if-gtz v10, :cond_2

    .line 166
    long-to-int v10, v8

    new-array v7, v10, [B

    .line 168
    const/4 v6, 0x0

    .line 169
    .local v6, "offset":I
    const/4 v5, 0x0

    .line 171
    .local v5, "numRead":I
    :goto_0
    array-length v10, v7

    if-ge v6, v10, :cond_0

    array-length v10, v7

    sub-int/2addr v10, v6

    invoke-virtual {v4, v7, v6, v10}, Ljava/io/FileInputStream;->read([BII)I

    move-result v5

    if-ltz v5, :cond_0

    .line 172
    add-int/2addr v6, v5

    goto :goto_0

    .line 175
    :cond_0
    array-length v10, v7

    if-ge v6, v10, :cond_2

    .line 176
    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Could not completely read file."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_2
    .catch Ljava/io/StreamCorruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_d
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 179
    .end local v5    # "numRead":I
    .end local v6    # "offset":I
    .end local v8    # "size":J
    :catch_0
    move-exception v0

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    move-object v1, v2

    .line 180
    .end local v2    # "file_fd":Ljava/io/File;
    .local v0, "e":Ljava/io/StreamCorruptedException;
    .restart local v1    # "file_fd":Ljava/io/File;
    :goto_1
    :try_start_3
    const-string v10, "LostLock"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "secFileRead StreamCorruptedException = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/StreamCorruptedException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 189
    if-eqz v3, :cond_1

    .line 190
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 196
    .end local v0    # "e":Ljava/io/StreamCorruptedException;
    :cond_1
    :goto_2
    return-object v7

    .line 189
    .end local v1    # "file_fd":Ljava/io/File;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "file_fd":Ljava/io/File;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v8    # "size":J
    :cond_2
    if-eqz v4, :cond_3

    .line 190
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_3
    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    move-object v1, v2

    .line 194
    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto :goto_2

    .line 192
    .end local v1    # "file_fd":Ljava/io/File;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "file_fd":Ljava/io/File;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    .line 193
    .local v0, "e":Ljava/io/IOException;
    const-string v10, "LostLock"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "secFileRead  close Exception = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    move-object v1, v2

    .line 195
    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto :goto_2

    .line 192
    .end local v8    # "size":J
    .local v0, "e":Ljava/io/StreamCorruptedException;
    :catch_2
    move-exception v0

    .line 193
    .local v0, "e":Ljava/io/IOException;
    const-string v10, "LostLock"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "secFileRead  close Exception = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 181
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 182
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_6
    const-string v10, "LostLock"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "secFileRead FileNotFoundException = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 189
    if-eqz v3, :cond_1

    .line 190
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2

    .line 192
    :catch_4
    move-exception v0

    .line 193
    .local v0, "e":Ljava/io/IOException;
    const-string v10, "LostLock"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "secFileRead  close Exception = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 183
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 184
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_8
    const-string v10, "LostLock"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "secFileRead IOException = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 189
    if-eqz v3, :cond_1

    .line 190
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto/16 :goto_2

    .line 192
    :catch_6
    move-exception v0

    .line 193
    const-string v10, "LostLock"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "secFileRead  close Exception = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 185
    .end local v0    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_a
    const-string v10, "LostLock"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "secFileRead Exception = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 189
    if-eqz v3, :cond_1

    .line 190
    :try_start_b
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto/16 :goto_2

    .line 192
    :catch_8
    move-exception v0

    .line 193
    .local v0, "e":Ljava/io/IOException;
    const-string v10, "LostLock"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "secFileRead  close Exception = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 188
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 189
    :goto_6
    if-eqz v3, :cond_4

    .line 190
    :try_start_c
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 194
    :cond_4
    :goto_7
    throw v10

    .line 192
    :catch_9
    move-exception v0

    .line 193
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v11, "LostLock"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "secFileRead  close Exception = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 188
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "file_fd":Ljava/io/File;
    .restart local v2    # "file_fd":Ljava/io/File;
    :catchall_1
    move-exception v10

    move-object v1, v2

    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto :goto_6

    .end local v1    # "file_fd":Ljava/io/File;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "file_fd":Ljava/io/File;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v10

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    move-object v1, v2

    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto :goto_6

    .line 185
    .end local v1    # "file_fd":Ljava/io/File;
    .restart local v2    # "file_fd":Ljava/io/File;
    :catch_a
    move-exception v0

    move-object v1, v2

    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto :goto_5

    .end local v1    # "file_fd":Ljava/io/File;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "file_fd":Ljava/io/File;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_b
    move-exception v0

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    move-object v1, v2

    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto :goto_5

    .line 183
    .end local v1    # "file_fd":Ljava/io/File;
    .restart local v2    # "file_fd":Ljava/io/File;
    :catch_c
    move-exception v0

    move-object v1, v2

    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto/16 :goto_4

    .end local v1    # "file_fd":Ljava/io/File;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "file_fd":Ljava/io/File;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_d
    move-exception v0

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    move-object v1, v2

    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto/16 :goto_4

    .line 181
    .end local v1    # "file_fd":Ljava/io/File;
    .restart local v2    # "file_fd":Ljava/io/File;
    :catch_e
    move-exception v0

    move-object v1, v2

    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto/16 :goto_3

    .end local v1    # "file_fd":Ljava/io/File;
    .end local v3    # "fin":Ljava/io/FileInputStream;
    .restart local v2    # "file_fd":Ljava/io/File;
    .restart local v4    # "fin":Ljava/io/FileInputStream;
    :catch_f
    move-exception v0

    move-object v3, v4

    .end local v4    # "fin":Ljava/io/FileInputStream;
    .restart local v3    # "fin":Ljava/io/FileInputStream;
    move-object v1, v2

    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto/16 :goto_3

    .line 179
    :catch_10
    move-exception v0

    goto/16 :goto_1

    .end local v1    # "file_fd":Ljava/io/File;
    .restart local v2    # "file_fd":Ljava/io/File;
    :catch_11
    move-exception v0

    move-object v1, v2

    .end local v2    # "file_fd":Ljava/io/File;
    .restart local v1    # "file_fd":Ljava/io/File;
    goto/16 :goto_1
.end method

.method private sendBroadcastDeviceNameChanged()V
    .locals 2

    .prologue
    .line 1369
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.settings.DEVICE_NAME_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1370
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1371
    return-void
.end method

.method private sendBroadcastFindingLostPhone()V
    .locals 2

    .prologue
    .line 1379
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.FindingLostPhone.SUBSCRIBE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1380
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1381
    return-void
.end method

.method private sendBroadcastSetupWizardComplete()V
    .locals 2

    .prologue
    .line 1374
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1375
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1376
    return-void
.end method

.method private setEasyMode()V
    .locals 15

    .prologue
    .line 1405
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const-string v11, "com.sec.android.app.easylauncher"

    invoke-virtual {v10, v11}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    .line 1406
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const-string v11, "com.sec.android.app.launcher"

    invoke-virtual {v10, v11}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    .line 1408
    new-instance v6, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v6, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1409
    .local v6, "mIntentFilter":Landroid/content/IntentFilter;
    const-string v10, "android.intent.category.HOME"

    invoke-virtual {v6, v10}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 1410
    const-string v10, "android.intent.category.DEFAULT"

    invoke-virtual {v6, v10}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 1412
    new-instance v5, Landroid/content/Intent;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v5, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1413
    .local v5, "mHomeIntent":Landroid/content/Intent;
    const-string v10, "android.intent.category.HOME"

    invoke-virtual {v5, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1414
    const-string v10, "android.intent.category.DEFAULT"

    invoke-virtual {v5, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1415
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/high16 v11, 0x10000

    invoke-virtual {v10, v5, v11}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v9

    .line 1416
    .local v9, "rList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1418
    .local v7, "mNoPriortyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    if-ge v1, v10, :cond_1

    .line 1419
    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    iget v10, v10, Landroid/content/pm/ResolveInfo;->priority:I

    if-lez v10, :cond_0

    .line 1418
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1421
    :cond_0
    invoke-interface {v9, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1424
    :cond_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v0, v10, [Landroid/content/ComponentName;

    .line 1425
    .local v0, "components":[Landroid/content/ComponentName;
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v1, v10, :cond_2

    .line 1426
    iget-object v11, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v12, "TAG"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    invoke-virtual {v10, v14}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v13, " Package name : "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    iget-object v10, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v12, v10}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    new-instance v11, Landroid/content/ComponentName;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    iget-object v10, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v12, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    iget-object v10, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v11, v12, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v11, v0, v1

    .line 1425
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1429
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "easy_mode_switch"

    const/4 v12, 0x1

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    .line 1430
    .local v8, "mStandardMode":I
    const/4 v4, 0x0

    .line 1431
    .local v4, "mDefaultCN":Landroid/content/ComponentName;
    const/4 v10, 0x1

    if-ne v8, v10, :cond_4

    .line 1432
    new-instance v4, Landroid/content/ComponentName;

    .end local v4    # "mDefaultCN":Landroid/content/ComponentName;
    const-string v10, "com.sec.android.app.launcher"

    const-string v11, "com.android.launcher2.Launcher"

    invoke-direct {v4, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    .restart local v4    # "mDefaultCN":Landroid/content/ComponentName;
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/high16 v11, 0x100000

    invoke-virtual {v10, v6, v11, v0, v4}, Landroid/content/pm/PackageManager;->addPreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    .line 1439
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "easy_mode_switch"

    const/4 v12, 0x1

    invoke-static {v10, v11, v12}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 1440
    .local v3, "isEasy":I
    new-instance v2, Landroid/content/Intent;

    const-string v10, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-direct {v2, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1441
    .local v2, "intent":Landroid/content/Intent;
    const-string v10, "easymode_from"

    const-string v11, "setupwizard"

    invoke-virtual {v2, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1443
    const/4 v10, 0x1

    if-ne v3, v10, :cond_5

    .line 1444
    const-string v10, "easymode"

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1451
    :cond_3
    :goto_4
    invoke-virtual {p0, v2}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1452
    return-void

    .line 1434
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "isEasy":I
    :cond_4
    new-instance v4, Landroid/content/ComponentName;

    .end local v4    # "mDefaultCN":Landroid/content/ComponentName;
    const-string v10, "com.sec.android.app.easylauncher"

    const-string v11, "com.sec.android.app.easylauncher.Launcher"

    invoke-direct {v4, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v4    # "mDefaultCN":Landroid/content/ComponentName;
    goto :goto_3

    .line 1445
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "isEasy":I
    :cond_5
    if-nez v3, :cond_3

    .line 1446
    const-string v10, "easymode"

    const/4 v11, 0x1

    invoke-virtual {v2, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1447
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSupportEasyMode30()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1448
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setLargeFont()V

    goto :goto_4
.end method

.method private setSkipNetworkCharges()V
    .locals 3

    .prologue
    .line 1473
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isChinaModel()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isWifiOnly(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1474
    sget-object v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v2, "network_charges"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 1476
    .local v0, "networkCharges":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v0, :cond_0

    .line 1477
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1480
    .end local v0    # "networkCharges":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_0
    return-void
.end method

.method private setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V
    .locals 7
    .param p1, "item"    # Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    .prologue
    const/high16 v6, 0x800000

    .line 852
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isCameraOnlyModel()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 853
    const-string v3, "complete"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 854
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v4, "SecSetupWizardActivity"

    const-string v5, "setupStart - launchCameraApp"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    const-string v3, "persist.sys.setupwizard_part"

    const-string v4, "camera"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->launchCameraApp()V

    .line 935
    :cond_0
    :goto_0
    return-void

    .line 873
    :cond_1
    const-string v3, "att_locker_setup"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 874
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "launchSource"

    const-string v5, "Samsung"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 877
    :cond_2
    const-string v3, "date_and_time_setup"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 878
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "extra_initial_auto_datetime_value"

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isNetConnected(Landroid/content/Context;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 881
    :cond_3
    const-string v3, "google_name_setup"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 882
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 883
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipDateTime()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 884
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "actionId"

    const-string v5, "user_name"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 891
    :cond_4
    :goto_1
    const-string v3, "google_account_setup"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 892
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 895
    :cond_5
    const-string v3, "sec_account_setup"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->v_context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSupportLMM(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-boolean v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsOwner:Z

    if-eqz v3, :cond_7

    .line 897
    const/4 v0, 0x0

    .line 898
    .local v0, "checkFlag":Z
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->v_context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/SecSetupWizard/Utils;->readReactivationFlag(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 899
    const/4 v0, 0x1

    .line 901
    :cond_6
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "required_auth"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 902
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v4, "SecSetupWizardActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LMM[getReqiredAuthFlag] = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    .end local v0    # "checkFlag":Z
    :cond_7
    const-string v3, "sec_health_setup"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 906
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "Shealth_setup_wizard"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 909
    :cond_8
    const-string v3, "google_subsuer_setup"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 910
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "actionId"

    const-string v5, "welcome"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 911
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "scriptUri"

    const-string v5, "android.resource://com.google.android.setupwizard/xml/wizard_script_user"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 915
    :cond_9
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v4, "SecSetupWizardActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startActivityForResult, current: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    const-string v3, "persist.sys.setupwizard"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getRequestCode()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 922
    :catch_0
    move-exception v2

    .line 923
    .local v2, "localActivityNotFoundException":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v4, "SecSetupWizardActivity"

    const-string v5, "ActivityNotFoundException!!"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v4, "SecSetupWizardActivity"

    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 886
    .end local v2    # "localActivityNotFoundException":Landroid/content/ActivityNotFoundException;
    :cond_a
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.android.setupwizard.DATE_TIME"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 887
    invoke-virtual {p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "actionId"

    const-string v5, "date_time"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 926
    :catch_1
    move-exception v1

    .line 927
    .local v1, "e":Ljava/lang/NullPointerException;
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v4, "SecSetupWizardActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setupwizard is not alive, current: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    const-string v3, "complete"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 929
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupComplete()V

    .line 931
    :cond_b
    const-string v3, "start"

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 932
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public checkNetwork()Z
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1553
    const-string v6, "connectivity"

    invoke-virtual {p0, v6}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1554
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1555
    .local v3, "ni":Landroid/net/NetworkInfo;
    if-nez v3, :cond_1

    .line 1567
    :cond_0
    :goto_0
    return v4

    .line 1558
    :cond_1
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    .line 1559
    .local v2, "isWifiAvail":Z
    invoke-virtual {v0, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1561
    if-nez v3, :cond_2

    move v4, v2

    .line 1562
    goto :goto_0

    .line 1565
    :cond_2
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    .line 1567
    .local v1, "isMobileAvail":Z
    if-nez v2, :cond_3

    if-eqz v1, :cond_0

    :cond_3
    move v4, v5

    goto :goto_0
.end method

.method public completeDropboxSetup(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1069
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 1070
    sget-object v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v2, "dropbox_setup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 1071
    .local v0, "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v0, :cond_0

    .line 1072
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1075
    .end local v0    # "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_0
    return-void
.end method

.method public completeFingerPrintSetup(I)V
    .locals 6
    .param p1, "resultCode"    # I

    .prologue
    .line 1154
    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v3, "finger_print_setup"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 1155
    .local v0, "fingerPrint":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    const/4 v2, -0x1

    if-ne v2, p1, :cond_1

    const/4 v1, 0x1

    .line 1156
    .local v1, "isSkip":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 1157
    invoke-virtual {v0, v1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1159
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v3, "SecSetupWizardActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "skip, finger print setup: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    return-void

    .line 1155
    .end local v1    # "isSkip":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public completeReactivationLock(Z)V
    .locals 5
    .param p1, "isSkip"    # Z

    .prologue
    .line 1135
    sget-object v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v2, "reactivation_lock_setup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 1136
    .local v0, "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v0, :cond_0

    .line 1137
    invoke-virtual {v0, p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1139
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v2, "SecSetupWizardActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "skip, reactivation setup: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1140
    return-void
.end method

.method public completeSecAccount(ILandroid/content/Intent;)V
    .locals 10
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1197
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v8, "sec_account_setup"

    invoke-virtual {v5, v8}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v4

    .line 1198
    .local v4, "secAccount":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    const/4 v5, -0x1

    if-ne v5, p1, :cond_3

    move v1, v6

    .line 1199
    .local v1, "isSkip":Z
    :goto_0
    if-eqz v4, :cond_0

    .line 1200
    invoke-virtual {v4, v1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1203
    :cond_0
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v8, "validation_email"

    invoke-virtual {v5, v8}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v2

    .line 1204
    .local v2, "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v2, :cond_1

    .line 1205
    if-nez v1, :cond_4

    move v5, v6

    :goto_1
    invoke-virtual {v2, v5}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1208
    :cond_1
    if-eqz v1, :cond_2

    .line 1209
    if-eqz p2, :cond_2

    .line 1210
    const-string v5, "signup_in_setupwizard"

    invoke-virtual {p2, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1211
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v8, "SecSetupWizardActivity"

    const-string v9, "signup_in_setupwizard true"

    invoke-virtual {v5, v8, v9}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    const-string v5, "setupwizard"

    invoke-virtual {p0, v5, v7}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 1213
    .local v3, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1214
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "sec_account_signup_in_setupwizard"

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1215
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1216
    const/4 v1, 0x0

    .line 1220
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    :goto_2
    const-string v5, "signin_without_email_verification"

    invoke-virtual {p2, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1221
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v8, "SecSetupWizardActivity"

    const-string v9, "signin_without_email_verification true"

    invoke-virtual {v5, v8, v9}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1222
    const-string v5, "setupwizard"

    invoke-virtual {p0, v5, v7}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 1223
    .restart local v3    # "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1224
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "signin_without_email_verification"

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1225
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1226
    const/4 v1, 0x0

    .line 1233
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    :cond_2
    :goto_3
    if-nez v1, :cond_7

    move v5, v6

    :goto_4
    invoke-virtual {p0, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeSecBackup(Z)V

    .line 1234
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->v_context:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSupportLMM(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isNetherlandsModel()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1235
    if-nez v1, :cond_8

    :goto_5
    invoke-virtual {p0, v6}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeReactivationLock(Z)V

    .line 1239
    :goto_6
    return-void

    .end local v1    # "isSkip":Z
    .end local v2    # "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_3
    move v1, v7

    .line 1198
    goto :goto_0

    .restart local v1    # "isSkip":Z
    .restart local v2    # "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_4
    move v5, v7

    .line 1205
    goto :goto_1

    .line 1218
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v8, "SecSetupWizardActivity"

    const-string v9, "signup_in_setupwizard false"

    invoke-virtual {v5, v8, v9}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1228
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v8, "SecSetupWizardActivity"

    const-string v9, "signin_without_email_verification false"

    invoke-virtual {v5, v8, v9}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_7
    move v5, v7

    .line 1233
    goto :goto_4

    :cond_8
    move v6, v7

    .line 1235
    goto :goto_5

    .line 1237
    :cond_9
    invoke-virtual {p0, v6}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeReactivationLock(Z)V

    goto :goto_6
.end method

.method public completeSecBackup(Z)V
    .locals 8
    .param p1, "isSkip"    # Z

    .prologue
    const/4 v6, 0x0

    .line 1118
    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v5, "sec_backup_setup"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v1

    .line 1119
    .local v1, "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    const-string v4, "setupwizard"

    invoke-virtual {p0, v4, v6}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1120
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v4, "backup_restore_done"

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 1121
    .local v0, "backup_completed":Z
    const-string v4, "signin_without_email_verification"

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 1123
    .local v3, "signin_without_email_verification":Z
    if-eqz v1, :cond_1

    .line 1124
    if-nez v0, :cond_0

    if-eqz v3, :cond_2

    .line 1125
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1130
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v5, "SecSetupWizardActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "skip, sec backup: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    return-void

    .line 1127
    :cond_2
    invoke-virtual {v1, p1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    goto :goto_0
.end method

.method public completeSecEasySignUp(I)V
    .locals 6
    .param p1, "resultCode"    # I

    .prologue
    .line 1144
    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v3, "sec_easy_signup"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v1

    .line 1145
    .local v1, "secEasySignUp":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    const/4 v2, -0x1

    if-ne v2, p1, :cond_1

    const/4 v0, 0x1

    .line 1147
    .local v0, "isSkip":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 1148
    invoke-virtual {v1, v0}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1150
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v3, "SecSetupWizardActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "skip, sec_easy_signup: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1151
    return-void

    .line 1145
    .end local v0    # "isSkip":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public completeSecHealthSetup(ILandroid/content/Intent;)V
    .locals 9
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1163
    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v7, "sec_health_setup"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v3

    .line 1164
    .local v3, "secHealth":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    const/4 v6, -0x1

    if-ne v6, p1, :cond_2

    move v1, v4

    .line 1166
    .local v1, "isSkip":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 1167
    if-eqz p2, :cond_0

    .line 1168
    const-string v6, "shealth_setup_wizard_complete"

    invoke-virtual {p2, v6, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1169
    iget-object v6, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v7, "SecSetupWizardActivity"

    const-string v8, "shealth_setup_wizard_complete true"

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1170
    const-string v6, "setupwizard"

    invoke-virtual {p0, v6, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1171
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1172
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "sec_health_setup_wizard_complete"

    invoke-interface {v0, v5, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1173
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1174
    const/4 v1, 0x1

    .line 1185
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    :goto_1
    if-eqz v3, :cond_1

    .line 1186
    invoke-virtual {v3, v1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1188
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v5, "SecSetupWizardActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "skip, completeSecHealthSetup: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    return-void

    .end local v1    # "isSkip":Z
    :cond_2
    move v1, v5

    .line 1164
    goto :goto_0

    .line 1176
    .restart local v1    # "isSkip":Z
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    const-string v7, "shealth_setup_wizard_complete false"

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1177
    const-string v4, "setupwizard"

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1178
    .restart local v2    # "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1179
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v4, "sec_health_setup_wizard_complete"

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1180
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1181
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public completeSktAccount(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1087
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 1088
    sget-object v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v2, "skt_account"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 1089
    .local v0, "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v0, :cond_0

    .line 1090
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1093
    .end local v0    # "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_0
    return-void
.end method

.method public completeSktTphoneSetup(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1096
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 1097
    sget-object v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v2, "skt_tphone_setup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 1098
    .local v0, "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v0, :cond_0

    .line 1099
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1102
    .end local v0    # "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_0
    return-void
.end method

.method public completeV3Setup(I)V
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 1078
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 1079
    sget-object v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v2, "kor_v3"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 1080
    .local v0, "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v0, :cond_0

    .line 1081
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1084
    .end local v0    # "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_0
    return-void
.end method

.method public completeValidationEmail(Landroid/content/Intent;)V
    .locals 6
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1105
    sget-object v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v4, "validation_email"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v1

    .line 1106
    .local v1, "item":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v1, :cond_0

    .line 1107
    invoke-virtual {v1, v0}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 1109
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v4, "SecSetupWizardActivity"

    const-string v5, "skip, sec validation email"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111
    const-string v3, "validation"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1112
    const-string v3, "validation"

    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1113
    .local v0, "isSkip":Z
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeSecBackup(Z)V

    .line 1115
    .end local v0    # "isSkip":Z
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1112
    goto :goto_0
.end method

.method public getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    .locals 3

    .prologue
    .line 555
    const/4 v0, 0x0

    .line 556
    .local v0, "nextItem":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    sget-object v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 559
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mRestPolicy:Landroid/app/enterprise/RestrictionPolicy;

    if-eqz v1, :cond_0

    .line 560
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mRestPolicy:Landroid/app/enterprise/RestrictionPolicy;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/enterprise/RestrictionPolicy;->isSettingsChangesAllowed(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSettingsChangesAllowed:Z

    .line 564
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->isSkip()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_1
    const-string v1, "hfa_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipHfa()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_2
    const-string v1, "cmcc_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipCMCC()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_3
    const-string v1, "wifi_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipWifi()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_4
    const-string v1, "data_connection"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipDataConnection()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_5
    const-string v1, "background_traffic"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipBackgroundTraffic()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_6
    const-string v1, "date_and_time_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipDateTime()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_7
    const-string v1, "cu_registration"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipCUregistration()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_8
    const-string v1, "ctc_registration"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipCTCregistration()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_9
    const-string v1, "baidu_location"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipBaiduLocation()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_a
    const-string v1, "sim_missing"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSimMissing(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_b
    const-string v1, "finger_print_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->hasFingerprintFeature(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_c
    const-string v1, "activation_progress"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSimMissing(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_18

    :cond_d
    const-string v1, "feature_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-boolean v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSettingsChangesAllowed:Z

    if-eqz v1, :cond_18

    :cond_e
    const-string v1, "kor_v3"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipKorV3()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_f
    const-string v1, "reactivation_lock_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, "sec_account_setup"

    invoke-static {v1}, Lcom/sec/android/app/SecSetupWizard/Utils;->isShouldSkipActivity(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    :cond_10
    const-string v1, "sec_easy_signup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSecEasySignUp()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_11
    const-string v1, "orangecloud_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isLinkOrangeCloud()Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_12
    const-string v1, "skt_account"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSktAccount()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_13
    const-string v1, "att_welcome_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipAttWelcomeScreen()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_14
    const-string v1, "sec_account_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSamsungAccountScreen()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_15
    const-string v1, "sec_health_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSamsungHealthScreen()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_16
    const-string v1, "my_account_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipMyAccount()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_17
    const-string v1, "skt_tphone_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSktTphoneSetup()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 588
    :cond_18
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 590
    :cond_19
    return-object v0
.end method

.method public getPrevious()Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    .locals 3

    .prologue
    .line 594
    const/4 v0, 0x0

    .line 595
    .local v0, "previousItem":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    sget-object v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getPrevious()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 598
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mRestPolicy:Landroid/app/enterprise/RestrictionPolicy;

    if-eqz v1, :cond_0

    .line 599
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mRestPolicy:Landroid/app/enterprise/RestrictionPolicy;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/enterprise/RestrictionPolicy;->isSettingsChangesAllowed(Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSettingsChangesAllowed:Z

    .line 603
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->isSkip()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_1
    const-string v1, "hfa_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipHfa()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_2
    const-string v1, "cmcc_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipCMCC()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_3
    const-string v1, "wifi_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipWifi()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_4
    const-string v1, "data_connection"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipDataConnection()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_5
    const-string v1, "background_traffic"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipBackgroundTraffic()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_6
    const-string v1, "date_and_time_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipDateTime()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_7
    const-string v1, "cu_registration"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipCUregistration()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_8
    const-string v1, "ctc_registration"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipCTCregistration()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_9
    const-string v1, "baidu_location"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipBaiduLocation()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_a
    const-string v1, "sim_missing"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSimMissing(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_b
    const-string v1, "finger_print_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->hasFingerprintFeature(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_c
    const-string v1, "activation_progress"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSimMissing(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_18

    :cond_d
    const-string v1, "feature_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-boolean v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSettingsChangesAllowed:Z

    if-eqz v1, :cond_18

    :cond_e
    const-string v1, "kor_v3"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipKorV3()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_f
    const-string v1, "reactivation_lock_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, "sec_account_setup"

    invoke-static {v1}, Lcom/sec/android/app/SecSetupWizard/Utils;->isShouldSkipActivity(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_18

    :cond_10
    const-string v1, "sec_easy_signup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSecEasySignUp()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_11
    const-string v1, "orangecloud_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isLinkOrangeCloud()Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_12
    const-string v1, "skt_account"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSktAccount()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_13
    const-string v1, "att_welcome_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipAttWelcomeScreen()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_14
    const-string v1, "sec_account_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSamsungAccountScreen()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_15
    const-string v1, "sec_health_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSamsungHealthScreen()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_16
    const-string v1, "my_account_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipMyAccount()Z

    move-result v1

    if-nez v1, :cond_18

    :cond_17
    const-string v1, "skt_tphone_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->isSkipSktTphoneSetup()Z

    move-result v1

    if-eqz v1, :cond_19

    .line 627
    :cond_18
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPrevious()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    .line 629
    :cond_19
    return-object v0
.end method

.method public getSecHealthSkipState()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1192
    const-string v1, "setupwizard"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1193
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "sec_health_setup_wizard_complete"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public getSktAccountStatus()Z
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDomesticSKTModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x1

    .line 140
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSktTphoneSetupStatus()Z
    .locals 4

    .prologue
    .line 145
    const-string v0, "SktTPhone"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature Test: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Setting_ConfigOperatorPhoneAppDuringSetupWizard"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Tphone"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Setting_ConfigOperatorPhoneAppDuringSetupWizard"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Tphone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    const/4 v0, 0x1

    .line 150
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 954
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 955
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "resultCode : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " of "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v8}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    const/4 v5, -0x1

    iput v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    .line 958
    packed-switch p1, :pswitch_data_0

    .line 1067
    :cond_0
    :goto_0
    return-void

    .line 960
    :pswitch_0
    const-string v5, "validation_email"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 961
    invoke-virtual {p0, p3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeValidationEmail(Landroid/content/Intent;)V

    .line 963
    :cond_1
    const-string v5, "sec_backup_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 964
    const-string v5, "setupwizard"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 965
    .local v4, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 966
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v5, "backup_restore_done"

    const/4 v6, 0x1

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 967
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 969
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeSecBackup(Z)V

    .line 971
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v4    # "pref":Landroid/content/SharedPreferences;
    :cond_2
    const-string v5, "sec_account_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 972
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeSecAccount(ILandroid/content/Intent;)V

    .line 974
    :cond_3
    const-string v5, "reactivation_lock_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 975
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeReactivationLock(Z)V

    .line 977
    :cond_4
    const-string v5, "terms_and_condition"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 978
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v6, "google_account_setup"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v1

    .line 979
    .local v1, "googleAccount":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v1, :cond_5

    .line 980
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->checkNetwork()Z

    move-result v5

    if-nez v5, :cond_7

    const/4 v5, 0x1

    :goto_1
    invoke-virtual {v1, v5}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 982
    :cond_5
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    const-string v6, "google_name_setup"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->findByKey(Ljava/lang/String;)Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v2

    .line 983
    .local v2, "googleName":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    if-eqz v2, :cond_6

    .line 984
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->checkNetwork()Z

    move-result v5

    invoke-virtual {v2, v5}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->setSkipState(Z)V

    .line 987
    .end local v1    # "googleAccount":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    .end local v2    # "googleName":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_6
    const-string v5, "google_account_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 989
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    goto/16 :goto_0

    .line 980
    .restart local v1    # "googleAccount":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_7
    const/4 v5, 0x0

    goto :goto_1

    .line 992
    .end local v1    # "googleAccount":Lcom/sec/android/app/SecSetupWizard/SequenceItem;
    :cond_8
    const-string v5, "google_name_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 993
    const/4 v5, 0x0

    iput v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    goto/16 :goto_0

    .line 996
    :cond_9
    const-string v5, "finger_print_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 997
    invoke-virtual {p0, p2}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeFingerPrintSetup(I)V

    .line 999
    :cond_a
    const-string v5, "sec_easy_signup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1000
    invoke-virtual {p0, p2}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeSecEasySignUp(I)V

    .line 1002
    :cond_b
    const-string v5, "hfa_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 1003
    const/4 p2, -0x1

    .line 1006
    :cond_c
    const-string v5, "sec_health_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1007
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeSecHealthSetup(ILandroid/content/Intent;)V

    .line 1010
    :cond_d
    const-string v5, "dropbox_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1011
    invoke-virtual {p0, p2}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeDropboxSetup(I)V

    .line 1013
    :cond_e
    const-string v5, "kor_v3"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 1014
    invoke-virtual {p0, p2}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeV3Setup(I)V

    .line 1016
    :cond_f
    const-string v5, "skt_account"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 1017
    invoke-virtual {p0, p2}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeSktAccount(I)V

    .line 1019
    :cond_10
    const-string v5, "skt_tphone_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1020
    invoke-virtual {p0, p2}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->completeSktTphoneSetup(I)V

    .line 1022
    :cond_11
    const-string v5, "google_subsuer_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 1023
    iput p2, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    goto/16 :goto_0

    .line 1027
    :cond_12
    if-eqz p2, :cond_13

    const/16 v5, 0xa

    if-ne p2, v5, :cond_15

    .line 1028
    :cond_13
    const-string v5, "att_locker_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1029
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupComplete()V

    goto/16 :goto_0

    .line 1031
    :cond_14
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPrevious()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto/16 :goto_0

    .line 1033
    :cond_15
    const/4 v5, -0x1

    if-eq p2, v5, :cond_16

    const/16 v5, 0xb

    if-eq p2, v5, :cond_16

    const/4 v5, 0x7

    if-eq p2, v5, :cond_16

    const/4 v5, 0x3

    if-eq p2, v5, :cond_16

    const/4 v5, 0x1

    if-ne p2, v5, :cond_18

    .line 1035
    :cond_16
    const-string v5, "att_welcome_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    const/4 v5, 0x7

    if-ne p2, v5, :cond_17

    sget-boolean v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mReactivationLock:Z

    if-eqz v5, :cond_17

    iget-boolean v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsOwner:Z

    if-eqz v5, :cond_17

    .line 1036
    new-instance v3, Landroid/content/Intent;

    const-string v5, "com.osp.app.signin.action.SAMSUNG_ACCOUNT_SETUPWIZARD"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1037
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "required_auth"

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1038
    const/16 v5, 0x3f3

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1039
    const-string v5, "SecSetupWizardActivity"

    const-string v6, "Lanching Samsung Account with extra information for reactivation"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1041
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_17
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto/16 :goto_0

    .line 1043
    :cond_18
    const/16 v5, 0xe

    if-ne p2, v5, :cond_0

    .line 1044
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    if-eqz v5, :cond_1a

    const-string v5, "complete"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrentItem()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getNext()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_19

    const-string v5, "dual_sim_always_on_phone_number_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrentItem()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getNext()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1046
    :cond_19
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupComplete()V

    goto/16 :goto_0

    .line 1048
    :cond_1a
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto/16 :goto_0

    .line 1054
    :pswitch_1
    const-string v5, "att_welcome_setup"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1b

    .line 1056
    const-string v5, "SecSetupWizardActivity"

    const-string v6, "Back to welcome screen"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1057
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    if-eqz v5, :cond_0

    .line 1058
    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v5}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrentItem()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto/16 :goto_0

    .line 1060
    :cond_1b
    const-string v5, "complete"

    sget-object v6, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrentItem()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/SecSetupWizard/SequenceItem;->getNext()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 1061
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupComplete()V

    goto/16 :goto_0

    .line 1063
    :cond_1c
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto/16 :goto_0

    .line 958
    :pswitch_data_0
    .packed-switch 0x3f2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 247
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->v_context:Landroid/content/Context;

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->hide()V

    .line 252
    :cond_0
    new-instance v3, Lcom/sec/android/app/SecSetupWizard/LogMsg;

    invoke-direct {v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    .line 253
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    const-string v7, "===== start ====="

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isShouldSkipActivity(SKIP_PACKAGE_NAME_SAMSUNGSETUPWIZARD): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "SecSetupWizard"

    invoke-static {v8}, Lcom/sec/android/app/SecSetupWizard/Utils;->isShouldSkipActivity(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "google"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v6, "user_setup_complete"

    invoke-static {v3, v6, v5}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_3

    move v1, v4

    .line 257
    .local v1, "isProvisionedGoogleB2BDevice":Z
    :goto_0
    const-string v3, "SecSetupWizard"

    invoke-static {v3}, Lcom/sec/android/app/SecSetupWizard/Utils;->isShouldSkipActivity(Ljava/lang/String;)Z

    move-result v3

    if-eq v4, v3, :cond_1

    invoke-static {p0}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/UserManager;->isLinkedUser()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v1, :cond_4

    .line 259
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupComplete()V

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->finish()V

    .line 346
    :cond_2
    :goto_1
    return-void

    .end local v1    # "isProvisionedGoogleB2BDevice":Z
    :cond_3
    move v1, v5

    .line 256
    goto :goto_0

    .line 264
    .restart local v1    # "isProvisionedGoogleB2BDevice":Z
    :cond_4
    const/4 v0, 0x0

    .line 265
    .local v0, "isCompleted":Z
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->v_context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/SecSetupWizard/Utils;->isSupportLMM(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 266
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->v_context:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/SecSetupWizard/Utils;->readReactivationFlag(Landroid/content/Context;)Z

    move-result v3

    sput-boolean v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mReactivationLock:Z

    .line 270
    :cond_5
    const-string v3, "enterprise_policy"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/EnterpriseDeviceManager;

    iput-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 272
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-eqz v3, :cond_6

    .line 273
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mRestPolicy:Landroid/app/enterprise/RestrictionPolicy;

    .line 276
    :cond_6
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    if-nez v3, :cond_7

    move v3, v4

    :goto_2
    iput-boolean v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsOwner:Z

    .line 277
    invoke-static {}, Landroid/os/UserManager;->supportsMultipleUsers()Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSupportMUM:Z

    .line 279
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "persist.sys.setupwizard: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "persist.sys.setupwizard"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "UserHandle.myUserId(): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mIsSupportMUM: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsSupportMUM:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string v3, "FINISH"

    const-string v6, "persist.sys.setupwizard"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    if-nez v3, :cond_8

    .line 284
    const/4 v0, 0x1

    .line 286
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v4, "SecSetupWizardActivity"

    const-string v5, "SetupWizard Complete again complete set"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupComplete()V

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->finish()V

    goto/16 :goto_1

    :cond_7
    move v3, v5

    .line 276
    goto/16 :goto_2

    .line 292
    :cond_8
    if-nez v0, :cond_a

    .line 293
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->disableNotifications()V

    .line 294
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isCameraOnlyModel()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v6, "fromCamera"

    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_9

    sget-object v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v3}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getLength()I

    move-result v3

    if-le v3, v9, :cond_9

    .line 296
    sget-object v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v3}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->initSequenceList()V

    .line 299
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v6, "SecSetupWizardActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getLength: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v8}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getLength()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    sget-object v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v3}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getLength()I

    move-result v3

    if-ne v3, v9, :cond_d

    .line 301
    const-string v3, "persist.sys.setupwizard"

    const-string v6, "START"

    invoke-static {v3, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v6, "user_setup_complete"

    invoke-static {v3, v6, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 304
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isCameraOnlyModel()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 305
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->makeCameraOnlySequenceList()V

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    .line 340
    :cond_a
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v5, "com.sec.feature.cocktailbar"

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    sput-boolean v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCocktailBarEnabled:Z

    .line 341
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v5, "SecSetupWizardActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mCocktailBarEnabled : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCocktailBarEnabled:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    sget-boolean v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCocktailBarEnabled:Z

    if-eqz v3, :cond_2

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v5, "accessibility_enabled"

    invoke-static {v5}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mTalkbackOnOffObserver:Landroid/database/ContentObserver;

    invoke-virtual {v3, v5, v4, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto/16 :goto_1

    .line 307
    :cond_b
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isVideoSequenceModel()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->makeVideoIntroSequenceList()V

    .line 309
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto :goto_3

    .line 311
    :cond_c
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->makeDefaultSequenceList()V

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto :goto_3

    .line 316
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v5, "SecSetupWizardActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate is called again, current: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v7}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", property: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "persist.sys.setupwizard"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string v3, "complete"

    sget-object v5, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v5}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 318
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v5, "SecSetupWizardActivity"

    const-string v6, "setupComplete in onCreate()"

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupComplete()V

    goto/16 :goto_3

    .line 320
    :cond_e
    sget-object v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v3}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v3

    const-string v5, "persist.sys.setupwizard"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v5, "SecSetupWizardActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setupStart in onCreate(): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v7}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    const-string v3, "persist.sys.setupwizard"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 323
    .local v2, "key":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v5, "SecSetupWizardActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "persist.sys.setupwizard: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v5, "actionId"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_f

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto/16 :goto_1

    .line 329
    :cond_f
    invoke-static {v2}, Lcom/sec/android/app/SecSetupWizard/Utils;->isActivityOfSetupWizard(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 330
    sget-object v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    if-eqz v3, :cond_a

    .line 331
    sget-object v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v3}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrentItem()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    goto/16 :goto_3
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 382
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 383
    const/4 v0, 0x1

    .line 387
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 939
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 941
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    .line 943
    iget-boolean v0, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsOwner:Z

    if-nez v0, :cond_0

    .line 944
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupComplete()V

    .line 950
    :goto_0
    return-void

    .line 948
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getNext()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    .line 949
    iget-object v0, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v1, "SecSetupWizardActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v3}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v0, 0x0

    .line 359
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 361
    const-string v1, "google_account_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "google_name_setup"

    sget-object v2, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v2}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 363
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v2, "SecSetupWizardActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onResume result : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v4}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    iget v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    if-ne v1, v5, :cond_2

    .line 365
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPrevious()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    .line 367
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mIsOwner:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurrentResult:I

    if-ne v1, v5, :cond_4

    .line 368
    :cond_3
    sget-object v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mList:Lcom/sec/android/app/SecSetupWizard/SequenceList;

    invoke-virtual {v1}, Lcom/sec/android/app/SecSetupWizard/SequenceList;->getCurrentItem()Lcom/sec/android/app/SecSetupWizard/SequenceItem;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupStart(Lcom/sec/android/app/SecSetupWizard/SequenceItem;)V

    .line 371
    :cond_4
    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "google"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "user_setup_complete"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    .line 372
    .local v0, "isProvisionedGoogleB2BDevice":Z
    :cond_5
    if-eqz v0, :cond_6

    .line 373
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v2, "SecSetupWizardActivity"

    const-string v3, "google b2b device and user setup is completed so finish setupwizard"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setupComplete()V

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->finish()V

    .line 379
    :cond_6
    return-void
.end method

.method public setLargeFont()V
    .locals 4

    .prologue
    .line 1541
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurConfig:Landroid/content/res/Configuration;

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1546
    :goto_0
    const-string v1, "SecSetupWizardActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KKK setLargeFont : mCurConfig.fontScale : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurConfig:Landroid/content/res/Configuration;

    iget v3, v3, Landroid/content/res/Configuration;->fontScale:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1547
    const-wide v2, 0x3ff30a3d70a3d70aL    # 1.19

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->objValue:Ljava/lang/Object;

    .line 1548
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "font_size"

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1549
    iget-object v1, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->objValue:Ljava/lang/Object;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->writeFontSizePreference(Ljava/lang/Object;)V

    .line 1550
    return-void

    .line 1542
    :catch_0
    move-exception v0

    .line 1543
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SecSetupWizardActivity"

    const-string v2, "Unable to retrieve font size"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setLockPhoneLock()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 218
    const-string v5, "/efs/sms/sktcarrierlockmode"

    invoke-static {v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->secFileExist(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 219
    const-string v5, "LostLock"

    const-string v6, "LostPhone Step : /efs/sms/sktcarrierlockmode"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const-string v5, "/efs/sms/sktcarrierlockmode"

    invoke-static {v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->secFileRead(Ljava/lang/String;)[B

    move-result-object v0

    .line 221
    .local v0, "bytes":[B
    new-instance v3, Ljava/lang/String;

    array-length v5, v0

    invoke-direct {v3, v0, v8, v5}, Ljava/lang/String;-><init>([BII)V

    .line 222
    .local v3, "readData":Ljava/lang/String;
    const-string v5, "LostLock"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "LostPhone Lock Status : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    if-eqz v3, :cond_0

    const-string v5, "ON"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 225
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.sec.android.FindingLostPhone.SUBSCRIBE"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    .local v1, "flpSubscribeIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->v_context:Landroid/content/Context;

    const-string v6, "android.permission.MASTER_CLEAR"

    invoke-virtual {v5, v1, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 231
    .end local v0    # "bytes":[B
    .end local v1    # "flpSubscribeIntent":Landroid/content/Intent;
    .end local v3    # "readData":Ljava/lang/String;
    :cond_0
    const-string v5, "/efs/sktdm_mem/lawlock.txt"

    invoke-static {v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->secFileExist(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 232
    const-string v5, "LostLock"

    const-string v6, "LostPhone Step : /efs/sktdm_mem/lawlock.txt"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    const-string v5, "/efs/sktdm_mem/lawlock.txt"

    invoke-static {v5}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->secFileRead(Ljava/lang/String;)[B

    move-result-object v0

    .line 234
    .restart local v0    # "bytes":[B
    new-instance v4, Ljava/lang/String;

    array-length v5, v0

    invoke-direct {v4, v0, v8, v5}, Ljava/lang/String;-><init>([BII)V

    .line 235
    .local v4, "readData_plus":Ljava/lang/String;
    const-string v5, "LostLock"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "LostPhonePlus Lock Status : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    if-eqz v4, :cond_1

    const-string v5, "ON"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 238
    new-instance v2, Landroid/content/Intent;

    const-string v5, "com.sec.android.FindingLostPhonePlus.SUBSCRIBE"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 240
    .local v2, "flpSubscribePlusIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->v_context:Landroid/content/Context;

    const-string v6, "android.permission.MASTER_CLEAR"

    invoke-virtual {v5, v2, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 243
    .end local v0    # "bytes":[B
    .end local v2    # "flpSubscribePlusIntent":Landroid/content/Intent;
    .end local v4    # "readData_plus":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected setupComplete()V
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/high16 v13, 0x10000000

    const/4 v12, 0x1

    .line 1260
    iget-object v9, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v10, "SecSetupWizardActivity"

    const-string v11, "setupComplete"

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSktAccountStatus()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1263
    new-instance v3, Landroid/content/Intent;

    const-string v9, "com.skt.apra.action.SETUP_WIZARD_HAS_RUN"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1264
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1265
    const-string v9, "SktAccount"

    const-string v10, "setupComplete Send"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1268
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "user_setup_complete"

    invoke-static {v9, v10, v12}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1278
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getSktTphoneSetupStatus()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDomesticSKTModel()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1279
    const-string v9, "SktTPhone"

    const-string v10, "Broadcast : skttphone_setupwizard"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1280
    new-instance v3, Landroid/content/Intent;

    const-string v9, "com.skt.prod.dialer.CHANGE_TPHONE_MODE_SETTING"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1281
    .restart local v3    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1285
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "device_provisioned"

    invoke-static {v9, v10, v12}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 1286
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->enableNotifications()V

    .line 1287
    invoke-static {p0}, Lcom/sec/android/app/SecSetupWizard/Utils;->updateLastSetupShown(Landroid/content/Context;)V

    .line 1288
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcastDeviceNameChanged()V

    .line 1289
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcastSetupWizardComplete()V

    .line 1290
    const-string v9, "persist.sys.setupwizard"

    const-string v10, "FINISH"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1292
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDomesticSKTModel()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1293
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setLockPhoneLock()V

    .line 1296
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->setEasyMode()V

    .line 1299
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 1300
    .local v8, "pm":Landroid/content/pm/PackageManager;
    new-instance v1, Landroid/content/ComponentName;

    const-class v9, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;

    invoke-direct {v1, p0, v9}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1301
    .local v1, "cn":Landroid/content/ComponentName;
    invoke-virtual {v8, v1, v14, v12}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 1303
    const-string v9, "persist.sys.userdata_flashed"

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    .line 1304
    .local v7, "mUserDataFlashed":Z
    const/4 v6, 0x1

    .line 1306
    .local v6, "mPreloadAppExist":Z
    :try_start_0
    const-string v9, "com.samsung.preloadapp"

    const/16 v10, 0x80

    invoke-virtual {v8, v9, v10}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1312
    :goto_0
    if-eqz v7, :cond_3

    .line 1313
    new-instance v5, Landroid/content/ComponentName;

    const-string v9, "com.samsung.preloadapp"

    const-string v10, "com.samsung.preloadapp.PreloadApp"

    invoke-direct {v5, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    .local v5, "mDisablePreload":Landroid/content/ComponentName;
    invoke-virtual {v8, v5, v14, v12}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 1317
    .end local v5    # "mDisablePreload":Landroid/content/ComponentName;
    :cond_3
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v9

    const-string v10, "CscFeature_Setting_SendTerminateSetupWizardBroadCast"

    invoke-virtual {v9, v10}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v9

    if-ne v9, v12, :cond_4

    if-nez v6, :cond_4

    .line 1318
    new-instance v4, Landroid/content/Intent;

    const-string v9, "com.vodafone.vodafone360updates.intent.action.OEM_SETUP_WIZARD_FINISHED"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1319
    .local v4, "intent_vf":Landroid/content/Intent;
    const-string v9, "SetupWizardLaunchedBy"

    const-string v10, "LaunchedBySystem"

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1320
    invoke-virtual {p0, v4}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1321
    iget-object v9, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v10, "SecSetupWizardActivity"

    const-string v11, "send terminate setupwizard broadcast message"

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 1324
    .end local v4    # "intent_vf":Landroid/content/Intent;
    :cond_4
    const-string v9, "jflteuc"

    const-string v10, "ro.product.name"

    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    const-string v9, "jactivelteuc"

    const-string v10, "ro.product.name"

    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 1326
    :cond_5
    :try_start_1
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1327
    .restart local v3    # "intent":Landroid/content/Intent;
    const/high16 v9, 0x10000000

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1328
    new-instance v0, Landroid/content/ComponentName;

    const-string v9, "com.att.android.digitallocker"

    const-string v10, "com.att.android.digitallocker.activities.SplashActivity"

    invoke-direct {v0, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    .local v0, "cName":Landroid/content/ComponentName;
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1330
    const-string v9, "launchSource"

    const-string v10, "Samsung"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1331
    invoke-virtual {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->startActivity(Landroid/content/Intent;)V

    .line 1332
    const-string v9, "SecSetupWizardActivity"

    const-string v10, "AT&T Locker Launched "

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1356
    .end local v0    # "cName":Landroid/content/ComponentName;
    .end local v3    # "intent":Landroid/content/Intent;
    :goto_1
    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isDomesticSKTModel()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-static {}, Lcom/sec/android/app/SecSetupWizard/Utils;->isCarrierLockEnabled()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1357
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->sendBroadcastFindingLostPhone()V

    .line 1360
    :cond_6
    sget-boolean v9, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCocktailBarEnabled:Z

    if-eqz v9, :cond_7

    .line 1361
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mTalkbackOnOffObserver:Landroid/database/ContentObserver;

    invoke-virtual {v9, v10}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1364
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->finish()V

    .line 1365
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->killSetupWizardProcess()V

    .line 1366
    return-void

    .line 1307
    :catch_0
    move-exception v2

    .line 1308
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v7, 0x0

    .line 1309
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 1333
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v2

    .line 1334
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 1335
    const-string v9, "SecSetupWizardActivity"

    const-string v10, "AT&T Locker not found"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1337
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    :cond_8
    const-string v9, "BMW"

    const-string v10, "ro.csc.sales_code"

    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1338
    new-instance v3, Landroid/content/Intent;

    const-string v9, "android.intent.action.MAIN"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1339
    .restart local v3    # "intent":Landroid/content/Intent;
    invoke-virtual {v3, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1340
    const-string v9, "com.sec.android.app.launcher"

    const-string v10, "com.android.launcher2.Launcher"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1341
    invoke-virtual {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 1344
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_9
    :try_start_2
    new-instance v3, Landroid/content/Intent;

    const-string v9, "com.android.setupwizard.EXIT"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1345
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v9, "actionId"

    const-string v10, "exit"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1346
    const-string v9, "scriptUri"

    const-string v10, "android.resource://com.google.android.setupwizard/xml/wizard_script"

    invoke-virtual {v3, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1347
    invoke-virtual {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 1348
    .end local v3    # "intent":Landroid/content/Intent;
    :catch_2
    move-exception v2

    .line 1349
    .restart local v2    # "e":Landroid/content/ActivityNotFoundException;
    new-instance v3, Landroid/content/Intent;

    const-string v9, "android.intent.action.MAIN"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1350
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v9, "android.intent.category.HOME"

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1351
    invoke-virtual {v3, v13}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1352
    invoke-virtual {p0, v3}, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method public writeFontSizePreference(Ljava/lang/Object;)V
    .locals 5
    .param p1, "objValue"    # Ljava/lang/Object;

    .prologue
    .line 1455
    const-string v2, "SecSetupWizardActivity"

    const-string v3, "KKK writeFontSizePreference"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1457
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 1458
    .local v1, "fontScale":F
    const-string v2, "SecSetupWizardActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KKK writeFontSizePreference : fontScale : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1459
    const/high16 v2, 0x3fc00000    # 1.5f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_0

    .line 1460
    sget v1, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->LARGE_FONT_SCALE:F

    .line 1461
    const-string v2, "SecSetupWizardActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KKK writeFontSizePreference : Extra large font. fontScale changed to : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1463
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurConfig:Landroid/content/res/Configuration;

    iput v1, v2, Landroid/content/res/Configuration;->fontScale:F

    .line 1465
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mCurConfig:Landroid/content/res/Configuration;

    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->updatePersistentConfiguration(Landroid/content/res/Configuration;)V

    .line 1466
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/SecSetupWizard/SecSetupWizardActivity;->mChangeFont:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1470
    .end local v1    # "fontScale":F
    :goto_0
    return-void

    .line 1467
    :catch_0
    move-exception v0

    .line 1468
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SecSetupWizardActivity"

    const-string v3, "KKK Unable to save font size"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
