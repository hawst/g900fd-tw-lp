.class public Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;
.super Landroid/app/Activity;
.source "ReActivationLockActivity.java"


# instance fields
.field private log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

.field private mContext:Landroid/content/Context;

.field private mLock:Landroid/widget/CheckBox;

.field private reactivateChecked:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->reactivateChecked:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->reactivateChecked:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)Lcom/sec/android/app/SecSetupWizard/LogMsg;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private initView()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 53
    const v5, 0x7f0e006c

    invoke-virtual {p0, v5}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 55
    .local v3, "tv":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0014

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "s":Ljava/lang/String;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    const v5, 0x7f0e006d

    invoke-virtual {p0, v5}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 59
    .local v4, "tv2":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0015

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 60
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iput-boolean v7, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->reactivateChecked:Z

    .line 64
    const v5, 0x7f0e006e

    invoke-virtual {p0, v5}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->mLock:Landroid/widget/CheckBox;

    .line 65
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->mLock:Landroid/widget/CheckBox;

    invoke-virtual {v5, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 66
    iget-object v5, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->mLock:Landroid/widget/CheckBox;

    new-instance v6, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$1;-><init>(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 76
    const v5, 0x7f0e0016

    invoke-virtual {p0, v5}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 77
    .local v1, "nextBtnArea":Landroid/widget/LinearLayout;
    new-instance v5, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity$2;-><init>(Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;)V

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0033

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0035

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "desc_tts":Ljava/lang/String;
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 103
    return-void
.end method

.method private setIndicatorTransparency()V
    .locals 3

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 107
    .local v0, "wmLp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v2, -0x7c000000

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 108
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->hide()V

    .line 40
    :cond_0
    new-instance v2, Lcom/sec/android/app/SecSetupWizard/LogMsg;

    invoke-direct {v2}, Lcom/sec/android/app/SecSetupWizard/LogMsg;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->log:Lcom/sec/android/app/SecSetupWizard/LogMsg;

    const-string v3, "ReActivationLockActivity"

    const-string v4, "ReActivationLockActivity Page Start"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/SecSetupWizard/LogMsg;->out(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->mContext:Landroid/content/Context;

    .line 43
    new-instance v0, Lcom/sec/android/app/SecSetupWizard/BasicForm;

    invoke-direct {v0, p0}, Lcom/sec/android/app/SecSetupWizard/BasicForm;-><init>(Landroid/content/Context;)V

    .line 44
    .local v0, "bf":Lcom/sec/android/app/SecSetupWizard/BasicForm;
    const v2, 0x7f030016

    const v3, 0x7f0c0012

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/SecSetupWizard/BasicForm;->changeScreen(II)V

    .line 45
    const v2, 0x7f0e0006

    invoke-virtual {v0, v2}, Lcom/sec/android/app/SecSetupWizard/BasicForm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 46
    .local v1, "v":Landroid/view/View;
    invoke-virtual {v0}, Lcom/sec/android/app/SecSetupWizard/BasicForm;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->setContentView(Landroid/view/View;)V

    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->initView()V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->setIndicatorTransparency()V

    .line 50
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 112
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 113
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->setResult(I)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/SecSetupWizard/ReActivationLockActivity;->finish()V

    .line 115
    const/4 v0, 0x0

    .line 117
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
