.class public Landroid/support/v7/app/d;
.super Landroid/support/v4/app/ab;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/bm;
.implements Landroid/support/v4/app/g;


# instance fields
.field private n:Landroid/support/v7/app/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/support/v4/app/ab;-><init>()V

    return-void
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 430
    invoke-static {p0}, Landroid/support/v4/app/bd;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    .line 432
    if-eqz v0, :cond_2

    .line 433
    invoke-static {p0, v0}, Landroid/support/v4/app/bd;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 434
    invoke-static {p0}, Landroid/support/v4/app/bl;->a(Landroid/content/Context;)Landroid/support/v4/app/bl;

    move-result-object v0

    .line 435
    invoke-virtual {v0, p0}, Landroid/support/v4/app/bl;->a(Landroid/app/Activity;)Landroid/support/v4/app/bl;

    .line 436
    invoke-virtual {v0}, Landroid/support/v4/app/bl;->a()V

    .line 440
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finishAffinity()V

    .line 451
    :goto_0
    const/4 v0, 0x1

    .line 453
    :goto_1
    return v0

    .line 440
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 444
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Landroid/support/v7/app/d;->finish()V

    goto :goto_0

    .line 449
    :cond_1
    invoke-static {p0, v0}, Landroid/support/v4/app/bd;->b(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    .line 453
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/Toolbar;)V
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/e;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 93
    return-void
.end method

.method final a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 264
    invoke-super {p0, p1}, Landroid/support/v4/app/ab;->setContentView(Landroid/view/View;)V

    .line 265
    return-void
.end method

.method final a(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 276
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ab;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method final a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 280
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ab;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected final a(Landroid/view/View;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 256
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    iget-object v0, v0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/d;

    invoke-virtual {v0, p2}, Landroid/support/v7/app/d;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/d;

    invoke-super {v0, p1, p2}, Landroid/support/v4/app/ab;->a(Landroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method public addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/e;->b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    return-void
.end method

.method final b(ILandroid/view/Menu;)V
    .locals 0

    .prologue
    .line 288
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ab;->onPanelClosed(ILandroid/view/Menu;)V

    .line 289
    return-void
.end method

.method final c(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 292
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ab;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final f()Landroid/support/v7/app/e;
    .locals 2

    .prologue
    .line 556
    iget-object v0, p0, Landroid/support/v7/app/d;->n:Landroid/support/v7/app/e;

    if-nez v0, :cond_0

    .line 557
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    new-instance v0, Landroid/support/v7/app/p;

    invoke-direct {v0, p0}, Landroid/support/v7/app/p;-><init>(Landroid/support/v7/app/d;)V

    :goto_0
    iput-object v0, p0, Landroid/support/v7/app/d;->n:Landroid/support/v7/app/e;

    .line 559
    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/d;->n:Landroid/support/v7/app/e;

    return-object v0

    .line 557
    :cond_1
    new-instance v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;

    invoke-direct {v0, p0}, Landroid/support/v7/app/ActionBarActivityDelegateBase;-><init>(Landroid/support/v7/app/d;)V

    goto :goto_0
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->c()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public invalidateOptionsMenu()V
    .locals 1

    .prologue
    .line 206
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->f()V

    .line 207
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    invoke-super {p0}, Landroid/support/v4/app/ab;->onBackPressed()V

    .line 300
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 128
    invoke-super {p0, p1}, Landroid/support/v4/app/ab;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 129
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/e;->a(Landroid/content/res/Configuration;)V

    .line 130
    return-void
.end method

.method public final onContentChanged()V
    .locals 0

    .prologue
    .line 534
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    .line 535
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/support/v4/app/ab;->onCreate(Landroid/os/Bundle;)V

    .line 123
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/e;->a(Landroid/os/Bundle;)V

    .line 124
    return-void
.end method

.method public onCreatePanelMenu(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 233
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/e;->c(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onCreatePanelView(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 146
    if-nez p1, :cond_0

    .line 147
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/e;->b(I)Landroid/view/View;

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/ab;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .prologue
    .line 547
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ab;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_0

    .line 552
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Landroid/support/v7/app/e;->a(Ljava/lang/String;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Landroid/support/v4/app/ab;->onDestroy()V

    .line 170
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/app/e;->i:Z

    .line 171
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 524
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ab;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    const/4 v0, 0x1

    .line 527
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/app/e;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 518
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/app/e;->b(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 155
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ab;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const/4 v0, 0x1

    .line 164
    :goto_0
    return v0

    .line 159
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 160
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/app/a;->a()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 162
    invoke-direct {p0}, Landroid/support/v7/app/d;->h()Z

    move-result v0

    goto :goto_0

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMenuOpened(ILandroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/e;->b(ILandroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onPanelClosed(ILandroid/view/Menu;)V
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/e;->a(ILandroid/view/Menu;)V

    .line 244
    return-void
.end method

.method public onPostResume()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Landroid/support/v4/app/ab;->onPostResume()V

    .line 141
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->e()V

    .line 142
    return-void
.end method

.method public onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 238
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/app/e;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Landroid/support/v4/app/ab;->onStop()V

    .line 135
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->d()V

    .line 136
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 175
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ab;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 176
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/e;->a(Ljava/lang/CharSequence;)V

    .line 177
    return-void
.end method

.method public s_()V
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->f()V

    .line 200
    return-void
.end method

.method public setContentView(I)V
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/e;->a(I)V

    .line 103
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/e;->a(Landroid/view/View;)V

    .line 108
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/e;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    return-void
.end method

.method public final t_()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 466
    invoke-static {p0}, Landroid/support/v4/app/bd;->a(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final u_()Landroid/support/v4/app/f;
    .locals 3

    .prologue
    .line 504
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    new-instance v1, Landroid/support/v7/app/g;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Landroid/support/v7/app/g;-><init>(Landroid/support/v7/app/e;B)V

    return-object v1
.end method

.method public final x_()Landroid/support/v7/app/a;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Landroid/support/v7/app/d;->f()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    return-object v0
.end method
