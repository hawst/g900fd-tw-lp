.class public final Landroid/support/v7/internal/view/menu/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/c/a/b;


# static fields
.field private static w:Ljava/lang/String;

.field private static x:Ljava/lang/String;

.field private static y:Ljava/lang/String;

.field private static z:Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/content/Intent;

.field private h:C

.field private i:C

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:I

.field private l:Landroid/support/v7/internal/view/menu/i;

.field private m:Landroid/support/v7/internal/view/menu/ad;

.field private n:Ljava/lang/Runnable;

.field private o:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private p:I

.field private q:I

.field private r:Landroid/view/View;

.field private s:Landroid/support/v4/view/n;

.field private t:Landroid/support/v4/view/ai;

.field private u:Z

.field private v:Landroid/view/ContextMenu$ContextMenuInfo;


# direct methods
.method constructor <init>(Landroid/support/v7/internal/view/menu/i;IIIILjava/lang/CharSequence;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput v1, p0, Landroid/support/v7/internal/view/menu/m;->k:I

    .line 76
    const/16 v0, 0x10

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 84
    iput v1, p0, Landroid/support/v7/internal/view/menu/m;->q:I

    .line 89
    iput-boolean v1, p0, Landroid/support/v7/internal/view/menu/m;->u:Z

    .line 133
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    .line 134
    iput p3, p0, Landroid/support/v7/internal/view/menu/m;->a:I

    .line 135
    iput p2, p0, Landroid/support/v7/internal/view/menu/m;->b:I

    .line 136
    iput p4, p0, Landroid/support/v7/internal/view/menu/m;->c:I

    .line 137
    iput p5, p0, Landroid/support/v7/internal/view/menu/m;->d:I

    .line 138
    iput-object p6, p0, Landroid/support/v7/internal/view/menu/m;->e:Ljava/lang/CharSequence;

    .line 139
    iput p7, p0, Landroid/support/v7/internal/view/menu/m;->q:I

    .line 140
    return-void
.end method

.method private a(Landroid/view/View;)Landroid/support/v4/c/a/b;
    .locals 2

    .prologue
    .line 597
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    .line 598
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    .line 599
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->a:I

    if-lez v0, :cond_0

    .line 600
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 602
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->j()V

    .line 603
    return-object p0
.end method


# virtual methods
.method public final a(Landroid/support/v4/view/n;)Landroid/support/v4/c/a/b;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 645
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    if-eqz v0, :cond_0

    .line 646
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/n;->a(Landroid/support/v4/view/p;)V

    .line 648
    :cond_0
    iput-object v1, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    .line 649
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    .line 650
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 651
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    if-eqz v0, :cond_1

    .line 652
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    new-instance v1, Landroid/support/v7/internal/view/menu/n;

    invoke-direct {v1, p0}, Landroid/support/v7/internal/view/menu/n;-><init>(Landroid/support/v7/internal/view/menu/m;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/n;->a(Landroid/support/v4/view/p;)V

    .line 659
    :cond_1
    return-object p0
.end method

.method final a(Landroid/support/v7/internal/view/menu/aa;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 362
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/support/v7/internal/view/menu/aa;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->getTitleCondensed()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Landroid/support/v7/internal/view/menu/ad;)V
    .locals 1

    .prologue
    .line 344
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->m:Landroid/support/v7/internal/view/menu/ad;

    .line 346
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/view/menu/ad;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 347
    return-void
.end method

.method final a(Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->v:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 535
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 457
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v1, v0, -0x5

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 458
    return-void

    .line 457
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 148
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->o:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->o:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v1, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v0

    .line 152
    :cond_1
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v2}, Landroid/support/v7/internal/view/menu/i;->r()Landroid/support/v7/internal/view/menu/i;

    move-result-object v2

    invoke-virtual {v1, v2, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 156
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->n:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 157
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->n:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 161
    :cond_2
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/content/Intent;

    if-eqz v1, :cond_3

    .line 163
    :try_start_0
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/i;->e()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    const-string v1, "MenuItemImpl"

    const-string v2, "Can\'t find activity to handle intent; ignoring"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 170
    :cond_3
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    .line 171
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->d:I

    return v0
.end method

.method final b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 483
    iget v2, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 484
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v3, v0, -0x3

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 485
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    if-eq v2, v0, :cond_0

    .line 486
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 488
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 484
    goto :goto_0
.end method

.method final c()C
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->i:C

    :goto_0
    return v0

    :cond_0
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->h:C

    goto :goto_0
.end method

.method final c(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 507
    iget v2, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 508
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v3, v0, -0x9

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 509
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    if-eq v2, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 508
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final collapseActionView()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 684
    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->q:I

    and-int/lit8 v1, v1, 0x8

    if-nez v1, :cond_1

    .line 697
    :cond_0
    :goto_0
    return v0

    .line 687
    :cond_1
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    if-nez v1, :cond_2

    .line 689
    const/4 v0, 0x1

    goto :goto_0

    .line 692
    :cond_2
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->t:Landroid/support/v4/view/ai;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->t:Landroid/support/v4/view/ai;

    invoke-interface {v1}, Landroid/support/v4/view/ai;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 694
    :cond_3
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/i;->b(Landroid/support/v7/internal/view/menu/m;)Z

    move-result v0

    goto :goto_0
.end method

.method final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 295
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->c()C

    move-result v0

    .line 296
    if-nez v0, :cond_0

    .line 297
    const-string v0, ""

    .line 320
    :goto_0
    return-object v0

    .line 300
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Landroid/support/v7/internal/view/menu/m;->w:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 301
    sparse-switch v0, :sswitch_data_0

    .line 316
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 320
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 304
    :sswitch_0
    sget-object v0, Landroid/support/v7/internal/view/menu/m;->x:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 308
    :sswitch_1
    sget-object v0, Landroid/support/v7/internal/view/menu/m;->y:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 312
    :sswitch_2
    sget-object v0, Landroid/support/v7/internal/view/menu/m;->z:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 301
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0xa -> :sswitch_0
        0x20 -> :sswitch_2
    .end sparse-switch
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 566
    if-eqz p1, :cond_0

    .line 567
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 571
    :goto_0
    return-void

    .line 569
    :cond_0
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 2

    .prologue
    .line 718
    iput-boolean p1, p0, Landroid/support/v7/internal/view/menu/m;->u:Z

    .line 719
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 720
    return-void
.end method

.method final e()Z
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->c()C

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final expandActionView()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 670
    invoke-virtual {p0}, Landroid/support/v7/internal/view/menu/m;->m()Z

    move-result v1

    if-nez v1, :cond_1

    .line 679
    :cond_0
    :goto_0
    return v0

    .line 674
    :cond_1
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->t:Landroid/support/v4/view/ai;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->t:Landroid/support/v4/view/ai;

    invoke-interface {v1}, Landroid/support/v4/view/ai;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 676
    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/m;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 461
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->s()Z

    move-result v0

    return v0
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
    .locals 2

    .prologue
    .line 634
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.getActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    .line 622
    :goto_0
    return-object v0

    .line 618
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    if-eqz v0, :cond_1

    .line 619
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    invoke-virtual {v0}, Landroid/support/v4/view/n;->a()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    .line 620
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    goto :goto_0

    .line 622
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 237
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->i:C

    return v0
.end method

.method public final getGroupId()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->b:I

    return v0
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->j:Landroid/graphics/drawable/Drawable;

    .line 417
    :goto_0
    return-object v0

    .line 410
    :cond_0
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->k:I

    if-eqz v0, :cond_1

    .line 411
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->e()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->k:I

    invoke-static {v0, v1}, Landroid/support/v7/internal/widget/av;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 412
    const/4 v1, 0x0

    iput v1, p0, Landroid/support/v7/internal/view/menu/m;->k:I

    .line 413
    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->j:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 417
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/content/Intent;

    return-object v0
.end method

.method public final getItemId()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 203
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->a:I

    return v0
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->v:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public final getNumericShortcut()C
    .locals 1

    .prologue
    .line 255
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->h:C

    return v0
.end method

.method public final getOrder()I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->c:I

    return v0
.end method

.method public final getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->m:Landroid/support/v7/internal/view/menu/ad;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 352
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->f:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->f:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->e:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 554
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSubMenu()Z
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->m:Landroid/support/v7/internal/view/menu/ad;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 558
    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->q:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 724
    iget-boolean v0, p0, Landroid/support/v7/internal/view/menu/m;->u:Z

    return v0
.end method

.method public final isCheckable()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 442
    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isChecked()Z
    .locals 2

    .prologue
    .line 466
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isVisible()Z
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    .line 493
    :cond_0
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 562
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->q:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 574
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->q:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Landroid/support/v4/view/n;
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 708
    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->q:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_1

    .line 709
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    if-eqz v1, :cond_0

    .line 710
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->s:Landroid/support/v4/view/n;

    invoke-virtual {v1}, Landroid/support/v4/view/n;->a()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    .line 712
    :cond_0
    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->r:Landroid/view/View;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 714
    :cond_1
    return v0
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 628
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setActionProvider()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic setActionView(I)Landroid/view/MenuItem;
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->e()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v1, p1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/view/View;)Landroid/support/v4/c/a/b;

    return-object p0
.end method

.method public final synthetic setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/view/View;)Landroid/support/v4/c/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 242
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->i:C

    if-ne v0, p1, :cond_0

    .line 250
    :goto_0
    return-object p0

    .line 246
    :cond_0
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/internal/view/menu/m;->i:C

    .line 248
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    goto :goto_0
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 447
    iget v2, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 448
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v3, v0, -0x2

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v3

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 449
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    if-eq v2, v0, :cond_0

    .line 450
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 453
    :cond_0
    return-object p0

    :cond_1
    move v0, v1

    .line 448
    goto :goto_0
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 471
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/view/MenuItem;)V

    .line 479
    :goto_0
    return-object p0

    .line 476
    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/m;->b(Z)V

    goto :goto_0
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 184
    if-eqz p1, :cond_0

    .line 185
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    .line 190
    :goto_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 192
    return-object p0

    .line 187
    :cond_0
    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Landroid/support/v7/internal/view/menu/m;->p:I

    goto :goto_0
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 431
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->j:Landroid/graphics/drawable/Drawable;

    .line 432
    iput p1, p0, Landroid/support/v7/internal/view/menu/m;->k:I

    .line 435
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 437
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 422
    iput v1, p0, Landroid/support/v7/internal/view/menu/m;->k:I

    .line 423
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->j:Landroid/graphics/drawable/Drawable;

    .line 424
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 426
    return-object p0
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/content/Intent;

    .line 223
    return-object p0
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 260
    iget-char v0, p0, Landroid/support/v7/internal/view/menu/m;->h:C

    if-ne v0, p1, :cond_0

    .line 268
    :goto_0
    return-object p0

    .line 264
    :cond_0
    iput-char p1, p0, Landroid/support/v7/internal/view/menu/m;->h:C

    .line 266
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    goto :goto_0
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 729
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This is not supported, use MenuItemCompat.setOnActionExpandListener()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->o:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 525
    return-object p0
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 273
    iput-char p1, p0, Landroid/support/v7/internal/view/menu/m;->h:C

    .line 274
    invoke-static {p2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    iput-char v0, p0, Landroid/support/v7/internal/view/menu/m;->i:C

    .line 276
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 278
    return-object p0
.end method

.method public final setShowAsAction(I)V
    .locals 2

    .prologue
    .line 579
    and-int/lit8 v0, p1, 0x3

    packed-switch v0, :pswitch_data_0

    .line 588
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 591
    :pswitch_0
    iput p1, p0, Landroid/support/v7/internal/view/menu/m;->q:I

    .line 592
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->j()V

    .line 593
    return-void

    .line 579
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final synthetic setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/m;->setShowAsAction(I)V

    return-object p0
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->e()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/view/menu/m;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 369
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->e:Ljava/lang/CharSequence;

    .line 371
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 373
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->m:Landroid/support/v7/internal/view/menu/ad;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->m:Landroid/support/v7/internal/view/menu/ad;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/view/menu/ad;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 377
    :cond_0
    return-object p0
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 392
    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->f:Ljava/lang/CharSequence;

    .line 395
    if-nez p1, :cond_0

    .line 396
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->e:Ljava/lang/CharSequence;

    .line 399
    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/i;->b(Z)V

    .line 401
    return-object p0
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 517
    invoke-virtual {p0, p1}, Landroid/support/v7/internal/view/menu/m;->c(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->l:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/i;->i()V

    .line 519
    :cond_0
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->e:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
