.class public final Landroid/support/v7/internal/view/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/c/b;


# instance fields
.field final a:Landroid/view/ActionMode$Callback;

.field final b:Landroid/content/Context;

.field final c:Landroid/support/v4/f/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ActionMode$Callback;)V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput-object p1, p0, Landroid/support/v7/internal/view/d;->b:Landroid/content/Context;

    .line 145
    iput-object p2, p0, Landroid/support/v7/internal/view/d;->a:Landroid/view/ActionMode$Callback;

    .line 146
    new-instance v0, Landroid/support/v4/f/p;

    invoke-direct {v0}, Landroid/support/v4/f/p;-><init>()V

    iput-object v0, p0, Landroid/support/v7/internal/view/d;->c:Landroid/support/v4/f/p;

    .line 147
    return-void
.end method

.method private b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Landroid/support/v7/internal/view/d;->c:Landroid/support/v4/f/p;

    invoke-virtual {v0, p1}, Landroid/support/v4/f/p;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/c;

    .line 176
    if-eqz v0, :cond_0

    .line 184
    :goto_0
    return-object v0

    .line 182
    :cond_0
    new-instance v0, Landroid/support/v7/internal/view/c;

    iget-object v1, p0, Landroid/support/v7/internal/view/d;->b:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Landroid/support/v7/internal/view/c;-><init>(Landroid/content/Context;Landroid/support/v7/c/a;)V

    .line 183
    iget-object v1, p0, Landroid/support/v7/internal/view/d;->c:Landroid/support/v4/f/p;

    invoke-virtual {v1, p1, v0}, Landroid/support/v4/f/p;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v7/c/a;)V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Landroid/support/v7/internal/view/d;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/d;->b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/ActionMode$Callback;->onDestroyActionMode(Landroid/view/ActionMode;)V

    .line 171
    return-void
.end method

.method public final a(Landroid/support/v7/c/a;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Landroid/support/v7/internal/view/d;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/d;->b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {p2}, Landroid/support/v7/internal/view/menu/ab;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v7/c/a;Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 164
    iget-object v1, p0, Landroid/support/v7/internal/view/d;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/d;->b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;

    move-result-object v2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_1

    new-instance v0, Landroid/support/v7/internal/view/menu/t;

    invoke-direct {v0, p2}, Landroid/support/v7/internal/view/menu/t;-><init>(Landroid/view/MenuItem;)V

    move-object p2, v0

    :cond_0
    :goto_0
    invoke-interface {v1, v2, p2}, Landroid/view/ActionMode$Callback;->onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_0

    new-instance v0, Landroid/support/v7/internal/view/menu/o;

    invoke-direct {v0, p2}, Landroid/support/v7/internal/view/menu/o;-><init>(Landroid/view/MenuItem;)V

    move-object p2, v0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/c/a;Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Landroid/support/v7/internal/view/d;->a:Landroid/view/ActionMode$Callback;

    invoke-direct {p0, p1}, Landroid/support/v7/internal/view/d;->b(Landroid/support/v7/c/a;)Landroid/view/ActionMode;

    move-result-object v1

    invoke-static {p2}, Landroid/support/v7/internal/view/menu/ab;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/ActionMode$Callback;->onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
