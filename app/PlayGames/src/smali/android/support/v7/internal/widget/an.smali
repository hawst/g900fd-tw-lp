.class final Landroid/support/v7/internal/widget/an;
.super Landroid/support/v7/widget/aw;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/widget/aq;


# instance fields
.field final synthetic a:Landroid/support/v7/internal/widget/SpinnerCompat;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/widget/ListAdapter;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/widget/SpinnerCompat;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 981
    iput-object p1, p0, Landroid/support/v7/internal/widget/an;->a:Landroid/support/v7/internal/widget/SpinnerCompat;

    .line 982
    invoke-direct {p0, p2, p3, p4}, Landroid/support/v7/widget/aw;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 984
    iput-object p1, p0, Landroid/support/v7/widget/aw;->d:Landroid/view/View;

    .line 985
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/an;->d()V

    .line 986
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/aw;->c:I

    .line 988
    new-instance v0, Landroid/support/v7/internal/widget/ao;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/internal/widget/ao;-><init>(Landroid/support/v7/internal/widget/an;Landroid/support/v7/internal/widget/SpinnerCompat;)V

    iput-object v0, p0, Landroid/support/v7/widget/aw;->e:Landroid/widget/AdapterView$OnItemClickListener;

    .line 999
    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/an;)Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 974
    iget-object v0, p0, Landroid/support/v7/internal/widget/an;->g:Landroid/widget/ListAdapter;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/widget/ListAdapter;)V
    .locals 0

    .prologue
    .line 1003
    invoke-super {p0, p1}, Landroid/support/v7/widget/aw;->a(Landroid/widget/ListAdapter;)V

    .line 1004
    iput-object p1, p0, Landroid/support/v7/internal/widget/an;->g:Landroid/widget/ListAdapter;

    .line 1005
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1013
    iput-object p1, p0, Landroid/support/v7/internal/widget/an;->f:Ljava/lang/CharSequence;

    .line 1014
    return-void
.end method
