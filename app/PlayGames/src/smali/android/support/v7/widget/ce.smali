.class public Landroid/support/v7/widget/ce;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SourceFile"


# instance fields
.field public a:Landroid/support/v7/widget/cr;

.field final b:Landroid/graphics/Rect;

.field c:Z

.field d:Z


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    const/4 v0, -0x2

    .line 6894
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 6882
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    .line 6883
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->c:Z

    .line 6887
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->d:Z

    .line 6895
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 6890
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 6882
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    .line 6883
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->c:Z

    .line 6887
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->d:Z

    .line 6891
    return-void
.end method

.method public constructor <init>(Landroid/support/v7/widget/ce;)V
    .locals 1

    .prologue
    .line 6906
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6882
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    .line 6883
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->c:Z

    .line 6887
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->d:Z

    .line 6907
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 6902
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 6882
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    .line 6883
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->c:Z

    .line 6887
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->d:Z

    .line 6903
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 1

    .prologue
    .line 6898
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 6882
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    .line 6883
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->c:Z

    .line 6887
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/ce;->d:Z

    .line 6899
    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 6916
    iget-object v0, p0, Landroid/support/v7/widget/ce;->a:Landroid/support/v7/widget/cr;

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->i()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 6937
    iget-object v0, p0, Landroid/support/v7/widget/ce;->a:Landroid/support/v7/widget/cr;

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->l()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 6948
    iget-object v0, p0, Landroid/support/v7/widget/ce;->a:Landroid/support/v7/widget/cr;

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->j()Z

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 6957
    iget-object v0, p0, Landroid/support/v7/widget/ce;->a:Landroid/support/v7/widget/cr;

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->c()I

    move-result v0

    return v0
.end method
