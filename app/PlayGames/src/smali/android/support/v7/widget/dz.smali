.class public final Landroid/support/v7/widget/dz;
.super Landroid/support/v7/app/b;
.source "SourceFile"


# instance fields
.field b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x2

    .line 1799
    invoke-direct {p0, v0, v0}, Landroid/support/v7/app/b;-><init>(II)V

    .line 1792
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/dz;->b:I

    .line 1800
    const v0, 0x800013

    iput v0, p0, Landroid/support/v7/widget/dz;->a:I

    .line 1801
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1795
    invoke-direct {p0, p1, p2}, Landroid/support/v7/app/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1792
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/dz;->b:I

    .line 1796
    return-void
.end method

.method public constructor <init>(Landroid/support/v7/app/b;)V
    .locals 1

    .prologue
    .line 1819
    invoke-direct {p0, p1}, Landroid/support/v7/app/b;-><init>(Landroid/support/v7/app/b;)V

    .line 1792
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/dz;->b:I

    .line 1820
    return-void
.end method

.method public constructor <init>(Landroid/support/v7/widget/dz;)V
    .locals 1

    .prologue
    .line 1813
    invoke-direct {p0, p1}, Landroid/support/v7/app/b;-><init>(Landroid/support/v7/app/b;)V

    .line 1792
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/dz;->b:I

    .line 1815
    iget v0, p1, Landroid/support/v7/widget/dz;->b:I

    iput v0, p0, Landroid/support/v7/widget/dz;->b:I

    .line 1816
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 1830
    invoke-direct {p0, p1}, Landroid/support/v7/app/b;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1792
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/dz;->b:I

    .line 1831
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 1

    .prologue
    .line 1823
    invoke-direct {p0, p1}, Landroid/support/v7/app/b;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1792
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/dz;->b:I

    .line 1826
    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v0, p0, Landroid/support/v7/widget/dz;->leftMargin:I

    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v0, p0, Landroid/support/v7/widget/dz;->topMargin:I

    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v0, p0, Landroid/support/v7/widget/dz;->rightMargin:I

    iget v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v0, p0, Landroid/support/v7/widget/dz;->bottomMargin:I

    .line 1827
    return-void
.end method
