.class final Landroid/support/v7/widget/ck;
.super Landroid/support/v7/widget/bx;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method private constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 2897
    iput-object p1, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Landroid/support/v7/widget/bx;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/widget/RecyclerView;B)V
    .locals 0

    .prologue
    .line 2897
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ck;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2949
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->n(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->o(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->p(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2950
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->q(Landroid/support/v7/widget/RecyclerView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 2955
    :goto_0
    return-void

    .line 2952
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->r(Landroid/support/v7/widget/RecyclerView;)Z

    .line 2953
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2900
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 2901
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v0

    iget-boolean v0, v0, Landroid/support/v7/widget/bv;->b:Z

    if-eqz v0, :cond_1

    .line 2905
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iput-boolean v2, v0, Landroid/support/v7/widget/co;->h:Z

    .line 2906
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->m(Landroid/support/v7/widget/RecyclerView;)Z

    .line 2911
    :goto_0
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0}, Landroid/support/v7/widget/n;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2912
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 2914
    :cond_0
    return-void

    .line 2908
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iput-boolean v2, v0, Landroid/support/v7/widget/co;->h:Z

    .line 2909
    iget-object v0, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->m(Landroid/support/v7/widget/RecyclerView;)Z

    goto :goto_0
.end method

.method public final a(II)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2918
    iget-object v1, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 2919
    iget-object v1, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    iget-object v2, v1, Landroid/support/v7/widget/n;->a:Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, p1, p2}, Landroid/support/v7/widget/n;->a(III)Landroid/support/v7/widget/p;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, v1, Landroid/support/v7/widget/n;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    .line 2920
    invoke-direct {p0}, Landroid/support/v7/widget/ck;->b()V

    .line 2922
    :cond_0
    return-void

    .line 2919
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(II)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2926
    iget-object v2, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 2927
    iget-object v2, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    iget-object v3, v2, Landroid/support/v7/widget/n;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, p1, p2}, Landroid/support/v7/widget/n;->a(III)Landroid/support/v7/widget/p;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v2, Landroid/support/v7/widget/n;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    .line 2928
    invoke-direct {p0}, Landroid/support/v7/widget/ck;->b()V

    .line 2930
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 2927
    goto :goto_0
.end method

.method public final c(II)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2934
    iget-object v1, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 2935
    iget-object v1, p0, Landroid/support/v7/widget/ck;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    iget-object v2, v1, Landroid/support/v7/widget/n;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1, p2}, Landroid/support/v7/widget/n;->a(III)Landroid/support/v7/widget/p;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, v1, Landroid/support/v7/widget/n;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    .line 2936
    invoke-direct {p0}, Landroid/support/v7/widget/ck;->b()V

    .line 2938
    :cond_0
    return-void

    .line 2935
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
