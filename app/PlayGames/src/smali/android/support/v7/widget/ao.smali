.class public final Landroid/support/v7/widget/ao;
.super Landroid/support/v7/widget/ce;
.source "SourceFile"


# instance fields
.field private e:I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x2

    .line 773
    invoke-direct {p0, v0, v0}, Landroid/support/v7/widget/ce;-><init>(II)V

    .line 764
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/ao;->e:I

    .line 766
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ao;->f:I

    .line 774
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 769
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ce;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 764
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/ao;->e:I

    .line 766
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ao;->f:I

    .line 770
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    .prologue
    .line 781
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ce;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 764
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/ao;->e:I

    .line 766
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ao;->f:I

    .line 782
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 1

    .prologue
    .line 777
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ce;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 764
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/ao;->e:I

    .line 766
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/ao;->f:I

    .line 778
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/ao;)I
    .locals 1

    .prologue
    .line 757
    iget v0, p0, Landroid/support/v7/widget/ao;->e:I

    return v0
.end method

.method static synthetic a(Landroid/support/v7/widget/ao;I)I
    .locals 0

    .prologue
    .line 757
    iput p1, p0, Landroid/support/v7/widget/ao;->f:I

    return p1
.end method

.method static synthetic b(Landroid/support/v7/widget/ao;)I
    .locals 1

    .prologue
    .line 757
    iget v0, p0, Landroid/support/v7/widget/ao;->f:I

    return v0
.end method

.method static synthetic b(Landroid/support/v7/widget/ao;I)I
    .locals 0

    .prologue
    .line 757
    iput p1, p0, Landroid/support/v7/widget/ao;->e:I

    return p1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 802
    iget v0, p0, Landroid/support/v7/widget/ao;->e:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 812
    iget v0, p0, Landroid/support/v7/widget/ao;->f:I

    return v0
.end method
