.class public abstract Landroid/support/v7/widget/cd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public p:Landroid/support/v7/widget/x;

.field public q:Landroid/support/v7/widget/RecyclerView;

.field r:Landroid/support/v7/widget/cm;

.field s:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4378
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/cd;->s:Z

    return-void
.end method

.method private static a(IIIZ)I
    .locals 4

    .prologue
    const/high16 v0, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 5462
    sub-int v2, p0, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 5466
    if-eqz p3, :cond_2

    .line 5467
    if-ltz p2, :cond_1

    .line 5488
    :cond_0
    :goto_0
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0

    :cond_1
    move v0, v1

    move p2, v1

    .line 5474
    goto :goto_0

    .line 5477
    :cond_2
    if-gez p2, :cond_0

    .line 5480
    const/4 v3, -0x1

    if-ne p2, v3, :cond_3

    move p2, v2

    .line 5482
    goto :goto_0

    .line 5483
    :cond_3
    const/4 v0, -0x2

    if-ne p2, v0, :cond_4

    .line 5485
    const/high16 v0, -0x80000000

    move p2, v2

    goto :goto_0

    :cond_4
    move v0, v1

    move p2, v1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 4907
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->f()I

    move-result v0

    return v0
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 4881
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/cd;->d(I)Landroid/view/View;

    move-result-object v0

    .line 4882
    if-eqz v0, :cond_0

    .line 4883
    iget-object v0, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/x;->a(I)I

    move-result v1

    iget-object v2, v0, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v2, v1}, Landroid/support/v7/widget/z;->b(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, v0, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v3, v1}, Landroid/support/v7/widget/z;->a(I)V

    iget-object v3, v0, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/y;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/y;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/x;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4885
    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/cd;Landroid/support/v7/widget/cm;)V
    .locals 1

    .prologue
    .line 4371
    iget-object v0, p0, Landroid/support/v7/widget/cd;->r:Landroid/support/v7/widget/cm;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/cd;->r:Landroid/support/v7/widget/cm;

    :cond_0
    return-void
.end method

.method public static a(Landroid/view/View;IIII)V
    .locals 4

    .prologue
    .line 5547
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    .line 5548
    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    sub-int v3, p3, v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, p4, v0

    invoke-virtual {p0, v1, v2, v3, v0}, Landroid/view/View;->layout(IIII)V

    .line 5550
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/cd;)Z
    .locals 1

    .prologue
    .line 4371
    iget-boolean v0, p0, Landroid/support/v7/widget/cd;->s:Z

    return v0
.end method

.method public static b(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 5501
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    .line 5502
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    return v0
.end method

.method static synthetic b(Landroid/support/v7/widget/cd;)Z
    .locals 1

    .prologue
    .line 4371
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/cd;->s:Z

    return v0
.end method

.method public static c(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 5515
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    .line 5516
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static d(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 5561
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v1, v0

    return v0
.end method

.method public static e(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 5573
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-static {p0}, Landroid/support/v7/widget/cd;->h(Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public static f(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 5585
    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static g(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 5597
    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static h(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 5638
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v0, v0, Landroid/support/v7/widget/ce;->b:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method private i(I)V
    .locals 3

    .prologue
    .line 4988
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/cd;->d(I)Landroid/view/View;

    iget-object v0, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/x;->a(I)I

    move-result v1

    iget-object v2, v0, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v2, v1}, Landroid/support/v7/widget/z;->c(I)V

    iget-object v0, v0, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/y;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/y;->c(I)Z

    .line 4989
    return-void
.end method


# virtual methods
.method public a(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 4636
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6299
    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v1

    if-nez v1, :cond_1

    .line 6302
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/bv;->a()I

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 5941
    const/4 v0, 0x0

    return v0
.end method

.method public abstract a()Landroid/support/v7/widget/ce;
.end method

.method public a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/ce;
    .locals 1

    .prologue
    .line 4619
    new-instance v0, Landroid/support/v7/widget/ce;

    invoke-direct {v0, p1, p2}, Landroid/support/v7/widget/ce;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ce;
    .locals 1

    .prologue
    .line 4595
    instance-of v0, p1, Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_0

    .line 4596
    new-instance v0, Landroid/support/v7/widget/ce;

    check-cast p1, Landroid/support/v7/widget/ce;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/ce;-><init>(Landroid/support/v7/widget/ce;)V

    .line 4600
    :goto_0
    return-object v0

    .line 4597
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    .line 4598
    new-instance v0, Landroid/support/v7/widget/ce;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/ce;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    .line 4600
    :cond_1
    new-instance v0, Landroid/support/v7/widget/ce;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/ce;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public a(II)V
    .locals 4

    .prologue
    .line 6019
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 6020
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 6021
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 6022
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 6024
    sparse-switch v2, :sswitch_data_0

    .line 6034
    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v4/view/at;->q(Landroid/view/View;)I

    move-result v1

    .line 6038
    :sswitch_0
    sparse-switch v3, :sswitch_data_1

    .line 6045
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/at;->r(Landroid/view/View;)I

    move-result v0

    .line 6049
    :sswitch_1
    invoke-virtual {p0, v1, v0}, Landroid/support/v7/widget/cd;->b(II)V

    .line 6050
    return-void

    .line 6024
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x40000000 -> :sswitch_0
    .end sparse-switch

    .line 6038
    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(ILandroid/support/v7/widget/ci;)V
    .locals 1

    .prologue
    .line 5115
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/cd;->d(I)Landroid/view/View;

    move-result-object v0

    .line 5116
    invoke-direct {p0, p1}, Landroid/support/v7/widget/cd;->a(I)V

    .line 5117
    invoke-virtual {p2, v0}, Landroid/support/v7/widget/ci;->a(Landroid/view/View;)V

    .line 5118
    return-void
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 6093
    return-void
.end method

.method final a(Landroid/support/v4/view/a/e;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 6138
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/ci;

    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    const-class v2, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/support/v4/view/a/e;->b(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v4}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v4}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/16 v2, 0x2000

    invoke-virtual {p1, v2}, Landroid/support/v4/view/a/e;->a(I)V

    invoke-virtual {p1, v3}, Landroid/support/v4/view/a/e;->i(Z)V

    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/16 v2, 0x1000

    invoke-virtual {p1, v2}, Landroid/support/v4/view/a/e;->a(I)V

    invoke-virtual {p1, v3}, Landroid/support/v4/view/a/e;->i(Z)V

    :cond_3
    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/cd;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/widget/cd;->b(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v0

    new-instance v1, Landroid/support/v4/view/a/m;

    invoke-static {}, Landroid/support/v4/view/a/e;->o()Landroid/support/v4/view/a/h;

    move-result-object v3

    invoke-interface {v3, v2, v0, v5, v5}, Landroid/support/v4/view/a/h;->a(IIZI)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/support/v4/view/a/m;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, v1}, Landroid/support/v4/view/a/e;->a(Ljava/lang/Object;)V

    .line 6140
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 5863
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 5875
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;III)V
    .locals 0

    .prologue
    .line 5911
    return-void
.end method

.method public a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/ci;)V
    .locals 0

    .prologue
    .line 4483
    return-void
.end method

.method public a(Landroid/support/v7/widget/bv;Landroid/support/v7/widget/bv;)V
    .locals 0

    .prologue
    .line 5825
    return-void
.end method

.method public final a(Landroid/support/v7/widget/ci;)V
    .locals 2

    .prologue
    .line 5337
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->j()I

    move-result v0

    .line 5338
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 5339
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cd;->d(I)Landroid/view/View;

    move-result-object v1

    .line 5340
    invoke-virtual {p0, p1, v0, v1}, Landroid/support/v7/widget/cd;->a(Landroid/support/v7/widget/ci;ILandroid/view/View;)V

    .line 5338
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 5342
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/ci;ILandroid/view/View;)V
    .locals 2

    .prologue
    .line 5345
    invoke-static {p3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v0

    .line 5346
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5360
    :goto_0
    return-void

    .line 5352
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->l()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->j()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v1

    iget-boolean v1, v1, Landroid/support/v7/widget/bv;->b:Z

    if-nez v1, :cond_1

    .line 5354
    invoke-direct {p0, p2}, Landroid/support/v7/widget/cd;->a(I)V

    .line 5355
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ci;->a(Landroid/support/v7/widget/cr;)V

    goto :goto_0

    .line 5357
    :cond_1
    invoke-direct {p0, p2}, Landroid/support/v7/widget/cd;->i(I)V

    .line 5358
    invoke-static {p3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v0

    iput-object p1, v0, Landroid/support/v7/widget/cr;->j:Landroid/support/v7/widget/ci;

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->h(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->l()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p1, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v1

    iget-boolean v1, v1, Landroid/support/v7/widget/bv;->b:Z

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Called scrap view with an invalid view. Invalid views cannot be reused from scrap, they should rebound from recycler pool."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v1, p1, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-object v1, p1, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    if-nez v1, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    :cond_5
    iget-object v1, p1, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Landroid/view/View;Landroid/support/v4/view/a/e;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 6245
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;)I

    move-result v0

    .line 6246
    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p3}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;)I

    move-result v2

    .line 6247
    :goto_1
    invoke-static {v0, v3, v2, v3, v1}, Landroid/support/v4/view/a/n;->a(IIIIZ)Landroid/support/v4/view/a/n;

    move-result-object v0

    .line 6250
    invoke-virtual {p4, v0}, Landroid/support/v4/view/a/e;->b(Ljava/lang/Object;)V

    .line 6251
    return-void

    :cond_0
    move v0, v1

    .line 6245
    goto :goto_0

    :cond_1
    move v2, v1

    .line 6246
    goto :goto_1
.end method

.method final a(Landroid/support/v7/widget/ci;Z)V
    .locals 4

    .prologue
    .line 5374
    iget-object v0, p1, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 5375
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 5376
    iget-object v0, p1, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cr;

    iget-object v0, v0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    .line 5377
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/widget/cr;->b()Z

    move-result v3

    if-nez v3, :cond_1

    .line 5378
    if-eqz p2, :cond_0

    .line 5381
    iget-object v3, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 5383
    :cond_0
    invoke-virtual {p1, v0}, Landroid/support/v7/widget/ci;->b(Landroid/view/View;)V

    .line 5375
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 5385
    :cond_2
    iget-object v0, p1, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 5386
    if-eqz p2, :cond_3

    if-lez v2, :cond_3

    .line 5387
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 5389
    :cond_3
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 4803
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;IZ)V

    .line 4804
    return-void
.end method

.method public a(Landroid/view/View;II)V
    .locals 6

    .prologue
    .line 5432
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 5434
    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 5435
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    add-int/2addr v2, p2

    .line 5436
    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v3

    add-int/2addr v1, p3

    .line 5438
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->k()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->m()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->o()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/ce;->leftMargin:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/ce;->rightMargin:I

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    iget v4, v0, Landroid/support/v7/widget/ce;->width:I

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->e()Z

    move-result v5

    invoke-static {v3, v2, v4, v5}, Landroid/support/v7/widget/cd;->a(IIIZ)I

    move-result v2

    .line 5442
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->l()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->n()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->p()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/ce;->topMargin:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/support/v7/widget/ce;->bottomMargin:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    iget v0, v0, Landroid/support/v7/widget/ce;->height:I

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->f()Z

    move-result v4

    invoke-static {v3, v1, v0, v4}, Landroid/support/v7/widget/cd;->a(IIIZ)I

    move-result v0

    .line 5446
    invoke-virtual {p1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 5447
    return-void
.end method

.method a(Landroid/view/View;IZ)V
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/4 v7, 0x0

    .line 4807
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v2

    .line 4808
    if-nez p3, :cond_0

    invoke-virtual {v2}, Landroid/support/v7/widget/cr;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4810
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p1}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 4819
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 4820
    invoke-virtual {v2}, Landroid/support/v7/widget/cr;->f()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v2}, Landroid/support/v7/widget/cr;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 4821
    :cond_1
    invoke-virtual {v2}, Landroid/support/v7/widget/cr;->d()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4822
    invoke-virtual {v2}, Landroid/support/v7/widget/cr;->e()V

    .line 4826
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v1, p1, p2, v3, v7}, Landroid/support/v7/widget/x;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    .line 4851
    :cond_2
    :goto_2
    iget-boolean v1, v0, Landroid/support/v7/widget/ce;->d:Z

    if-eqz v1, :cond_3

    .line 4855
    iget-object v1, v2, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    .line 4856
    iput-boolean v7, v0, Landroid/support/v7/widget/ce;->d:Z

    .line 4858
    :cond_3
    return-void

    .line 4817
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p1}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    goto :goto_0

    .line 4824
    :cond_5
    invoke-virtual {v2}, Landroid/support/v7/widget/cr;->g()V

    goto :goto_1

    .line 4830
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v3, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-ne v1, v3, :cond_b

    .line 4832
    iget-object v1, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/x;->a(Landroid/view/View;)I

    move-result v1

    .line 4833
    if-ne p2, v4, :cond_7

    .line 4834
    iget-object v3, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    invoke-virtual {v3}, Landroid/support/v7/widget/x;->a()I

    move-result p2

    .line 4836
    :cond_7
    if-ne v1, v4, :cond_8

    .line 4837
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Added View has RecyclerView as parent but view is not a real child. Unfiltered index:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4841
    :cond_8
    if-eq v1, p2, :cond_2

    .line 4842
    iget-object v3, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/cd;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/cd;->d(I)Landroid/view/View;

    move-result-object v4

    if-nez v4, :cond_9

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot move a child from non-existing index:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    invoke-direct {v3, v1}, Landroid/support/v7/widget/cd;->i(I)V

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/ce;

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/widget/cr;->l()Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v6, v3, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v6, v4}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    :goto_3
    iget-object v3, v3, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    invoke-virtual {v5}, Landroid/support/v7/widget/cr;->l()Z

    move-result v5

    invoke-virtual {v3, v4, p2, v1, v5}, Landroid/support/v7/widget/x;->a(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)V

    goto/16 :goto_2

    :cond_a
    iget-object v6, v3, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v6, v4}, Landroid/support/v7/widget/RecyclerView;->f(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    goto :goto_3

    .line 4845
    :cond_b
    iget-object v1, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    invoke-virtual {v1, p1, p2, v7}, Landroid/support/v7/widget/x;->a(Landroid/view/View;IZ)V

    .line 4846
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/ce;->c:Z

    .line 4847
    iget-object v1, p0, Landroid/support/v7/widget/cd;->r:Landroid/support/v7/widget/cm;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/cd;->r:Landroid/support/v7/widget/cm;

    iget-boolean v1, v1, Landroid/support/v7/widget/cm;->e:Z

    if-eqz v1, :cond_2

    .line 4848
    iget-object v1, p0, Landroid/support/v7/widget/cd;->r:Landroid/support/v7/widget/cm;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/cm;->a(Landroid/view/View;)I

    move-result v3

    iget v4, v1, Landroid/support/v7/widget/cm;->a:I

    if-ne v3, v4, :cond_2

    iput-object p1, v1, Landroid/support/v7/widget/cm;->f:Landroid/view/View;

    goto/16 :goto_2
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5618
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_0

    .line 5619
    invoke-virtual {p2, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 5624
    :goto_0
    return-void

    .line 5622
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 5623
    invoke-virtual {p2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method final a(Landroid/view/View;Landroid/support/v4/view/a/e;)V
    .locals 2

    .prologue
    .line 6225
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/ci;

    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    invoke-virtual {p0, v0, v1, p1, p2}, Landroid/support/v7/widget/cd;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Landroid/view/View;Landroid/support/v4/view/a/e;)V

    .line 6227
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/support/v7/widget/ci;)V
    .locals 3

    .prologue
    .line 5104
    iget-object v0, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    iget-object v1, v0, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v1, p1}, Landroid/support/v7/widget/z;->a(Landroid/view/View;)I

    move-result v1

    if-ltz v1, :cond_0

    iget-object v2, v0, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v2, v1}, Landroid/support/v7/widget/z;->a(I)V

    iget-object v2, v0, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/y;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/y;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/x;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 5105
    :cond_0
    invoke-virtual {p2, p1}, Landroid/support/v7/widget/ci;->a(Landroid/view/View;)V

    .line 5106
    return-void
.end method

.method public a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x1

    .line 6191
    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/ci;

    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    invoke-static {p1}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/ab;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_0

    if-nez v1, :cond_1

    .line 6192
    :cond_0
    :goto_0
    return-void

    .line 6191
    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v0}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v3}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v2, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    :goto_1
    invoke-virtual {v1, v0}, Landroid/support/v4/view/a/ab;->a(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/bv;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/support/v4/view/a/ab;->a(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 4421
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 4422
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Ljava/lang/String;)V

    .line 4424
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5745
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->m()I

    move-result v0

    .line 5746
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->n()I

    move-result v4

    .line 5747
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->k()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->o()I

    move-result v5

    sub-int v5, v1, v5

    .line 5748
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->l()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->p()I

    move-result v6

    sub-int v6, v1, v6

    .line 5749
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v7, p3, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v7

    .line 5750
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v7

    iget v8, p3, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v8

    .line 5751
    iget v8, p3, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v1

    .line 5752
    iget v9, p3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v7

    .line 5754
    sub-int v0, v1, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 5755
    sub-int v0, v7, v4

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 5756
    sub-int v0, v8, v5

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 5757
    sub-int v5, v9, v6

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 5762
    invoke-static {p1}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v6

    if-ne v6, v3, :cond_3

    .line 5763
    if-eqz v0, :cond_2

    :goto_0
    move v1, v0

    .line 5769
    :cond_0
    :goto_1
    if-eqz v4, :cond_4

    move v0, v4

    .line 5771
    :goto_2
    if-nez v1, :cond_1

    if-eqz v0, :cond_6

    .line 5772
    :cond_1
    if-eqz p4, :cond_5

    .line 5773
    invoke-virtual {p1, v1, v0}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    :goto_3
    move v0, v3

    .line 5779
    :goto_4
    return v0

    :cond_2
    move v0, v1

    .line 5763
    goto :goto_0

    .line 5765
    :cond_3
    if-nez v1, :cond_0

    move v1, v0

    goto :goto_1

    :cond_4
    move v0, v5

    .line 5769
    goto :goto_2

    .line 5775
    :cond_5
    invoke-virtual {p1, v1, v0}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    goto :goto_3

    :cond_6
    move v0, v2

    .line 5779
    goto :goto_4
.end method

.method public a(Landroid/support/v7/widget/ce;)Z
    .locals 1

    .prologue
    .line 4578
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 4653
    const/4 v0, 0x0

    return v0
.end method

.method public b(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 6318
    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v1

    if-nez v1, :cond_1

    .line 6321
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/bv;->a()I

    move-result v0

    goto :goto_0
.end method

.method public b(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 5986
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)Landroid/view/View;
    .locals 5

    .prologue
    .line 4935
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->j()I

    move-result v2

    .line 4936
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    .line 4937
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/cd;->d(I)Landroid/view/View;

    move-result-object v0

    .line 4938
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v3

    .line 4939
    if-eqz v3, :cond_1

    .line 4940
    invoke-virtual {v3}, Landroid/support/v7/widget/cr;->c()I

    move-result v4

    if-ne v4, p1, :cond_1

    invoke-virtual {v3}, Landroid/support/v7/widget/cr;->b()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-boolean v4, v4, Landroid/support/v7/widget/co;->i:Z

    if-nez v4, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/cr;->l()Z

    move-result v3

    if-nez v3, :cond_1

    .line 4947
    :cond_0
    :goto_1
    return-object v0

    .line 4936
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 4947
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 6060
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/support/v7/widget/RecyclerView;II)V

    .line 6061
    return-void
.end method

.method final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4381
    if-nez p1, :cond_0

    .line 4382
    iput-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    .line 4383
    iput-object v0, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    .line 4389
    :goto_0
    return-void

    .line 4385
    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    .line 4386
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    goto :goto_0
.end method

.method public b(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 5885
    return-void
.end method

.method public final b(Landroid/support/v7/widget/ci;)V
    .locals 2

    .prologue
    .line 6128
    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->j()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 6129
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cd;->d(I)Landroid/view/View;

    move-result-object v1

    .line 6130
    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/widget/cr;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6131
    invoke-virtual {p0, v0, p1}, Landroid/support/v7/widget/cd;->a(ILandroid/support/v7/widget/ci;)V

    .line 6128
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 6134
    :cond_1
    return-void
.end method

.method public c(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 5926
    const/4 v0, 0x0

    return v0
.end method

.method public c(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)Landroid/view/View;
    .locals 1

    .prologue
    .line 5705
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 4686
    return-void
.end method

.method public c(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 5895
    return-void
.end method

.method public c(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)V
    .locals 2

    .prologue
    .line 4547
    const-string v0, "RecyclerView"

    const-string v1, "You must override onLayoutChildren(Recycler recycler, State state) "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4548
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 4447
    const/4 v0, 0x0

    return v0
.end method

.method public d(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 5971
    const/4 v0, 0x0

    return v0
.end method

.method public d()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 6087
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 5136
    iget-object v0, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 5956
    const/4 v0, 0x0

    return v0
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 5269
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 5270
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->d(I)V

    .line 5272
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 4663
    const/4 v0, 0x0

    return v0
.end method

.method public f(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 6001
    const/4 v0, 0x0

    return v0
.end method

.method public f(I)V
    .locals 1

    .prologue
    .line 5281
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 5282
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->c(I)V

    .line 5284
    :cond_0
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 4673
    const/4 v0, 0x0

    return v0
.end method

.method public g(I)V
    .locals 0

    .prologue
    .line 6113
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 4395
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    .line 4396
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 4398
    :cond_0
    return-void
.end method

.method final h(I)Z
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 6340
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/ci;

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sparse-switch p1, :sswitch_data_0

    move v0, v1

    move v3, v1

    :goto_1
    if-nez v3, :cond_2

    if-eqz v0, :cond_0

    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0, v3}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    move v1, v2

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v4}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->l()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->n()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->p()I

    move-result v3

    sub-int/2addr v0, v3

    neg-int v0, v0

    :goto_2
    iget-object v3, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v4}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->k()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->m()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->o()I

    move-result v4

    sub-int/2addr v3, v4

    neg-int v3, v3

    move v5, v3

    move v3, v0

    move v0, v5

    goto :goto_1

    :sswitch_1
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v2}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->l()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->n()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->p()I

    move-result v3

    sub-int/2addr v0, v3

    :goto_3
    iget-object v3, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3, v2}, Landroid/support/v4/view/at;->a(Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->k()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->m()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/cd;->o()I

    move-result v4

    sub-int/2addr v3, v4

    move v5, v3

    move v3, v0

    move v0, v5

    goto :goto_1

    :cond_3
    move v3, v0

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1000 -> :sswitch_1
        0x2000 -> :sswitch_0
    .end sparse-switch
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 4720
    iget-object v0, p0, Landroid/support/v7/widget/cd;->r:Landroid/support/v7/widget/cm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->r:Landroid/support/v7/widget/cm;

    iget-boolean v0, v0, Landroid/support/v7/widget/cm;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 5127
    iget-object v0, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 5145
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 5154
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 5163
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 5172
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 5181
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 5190
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 5199
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v4/view/at;->m(Landroid/view/View;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final r()V
    .locals 1

    .prologue
    .line 6096
    iget-object v0, p0, Landroid/support/v7/widget/cd;->r:Landroid/support/v7/widget/cm;

    if-eqz v0, :cond_0

    .line 6097
    iget-object v0, p0, Landroid/support/v7/widget/cd;->r:Landroid/support/v7/widget/cm;

    invoke-virtual {v0}, Landroid/support/v7/widget/cm;->a()V

    .line 6099
    :cond_0
    return-void
.end method

.method final s()Z
    .locals 1

    .prologue
    .line 6387
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/ci;

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    const/4 v0, 0x0

    return v0
.end method
