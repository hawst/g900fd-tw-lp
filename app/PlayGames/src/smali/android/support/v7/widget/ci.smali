.class public final Landroid/support/v7/widget/ci;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/ArrayList;

.field b:Ljava/util/ArrayList;

.field final c:Ljava/util/ArrayList;

.field final d:Ljava/util/List;

.field final synthetic e:Landroid/support/v7/widget/RecyclerView;

.field private f:I

.field private g:Landroid/support/v7/widget/ch;

.field private h:Landroid/support/v7/widget/cp;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 3080
    iput-object p1, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3081
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    .line 3082
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    .line 3084
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    .line 3086
    iget-object v0, p0, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/ci;->d:Ljava/util/List;

    .line 3089
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v7/widget/ci;->f:I

    return-void
.end method

.method private a(IZ)Landroid/support/v7/widget/cr;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 3649
    iget-object v0, p0, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 3652
    :goto_0
    if-ge v3, v4, :cond_2

    .line 3653
    iget-object v0, p0, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cr;

    .line 3654
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->f()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->c()I

    move-result v5

    if-ne v5, p1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->h()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v5, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-boolean v5, v5, Landroid/support/v7/widget/co;->i:Z

    if-nez v5, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->l()Z

    move-result v5

    if-nez v5, :cond_1

    .line 3656
    :cond_0
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cr;->a(I)V

    .line 3692
    :goto_1
    return-object v0

    .line 3652
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 3667
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    iget-object v0, v4, Landroid/support/v7/widget/x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_5

    iget-object v0, v4, Landroid/support/v7/widget/x;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v6, v4, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v6, v0}, Landroid/support/v7/widget/z;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v7/widget/cr;->c()I

    move-result v7

    if-ne v7, p1, :cond_4

    invoke-virtual {v6}, Landroid/support/v7/widget/cr;->h()Z

    move-result v6

    if-nez v6, :cond_4

    .line 3669
    :goto_3
    if-eqz v0, :cond_3

    .line 3671
    iget-object v3, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v3, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    iget-object v4, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/by;->c(Landroid/support/v7/widget/cr;)V

    .line 3676
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 3677
    :goto_4
    if-ge v2, v3, :cond_7

    .line 3678
    iget-object v0, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cr;

    .line 3681
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->h()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->c()I

    move-result v4

    if-ne v4, p1, :cond_6

    .line 3682
    iget-object v1, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 3667
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_3

    .line 3677
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_7
    move-object v0, v1

    .line 3692
    goto :goto_1
.end method

.method private a(JIZ)Landroid/support/v7/widget/cr;
    .locals 5

    .prologue
    .line 3697
    iget-object v0, p0, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3698
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_3

    .line 3699
    iget-object v0, p0, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cr;

    .line 3700
    iget-wide v2, v0, Landroid/support/v7/widget/cr;->d:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->f()Z

    move-result v2

    if-nez v2, :cond_2

    .line 3701
    iget v2, v0, Landroid/support/v7/widget/cr;->e:I

    if-ne p3, v2, :cond_1

    .line 3702
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cr;->a(I)V

    .line 3703
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3712
    iget-object v1, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-boolean v1, v1, Landroid/support/v7/widget/co;->i:Z

    if-nez v1, :cond_0

    .line 3713
    const/4 v1, 0x2

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/cr;->a(II)V

    .line 3742
    :cond_0
    :goto_1
    return-object v0

    .line 3718
    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 3721
    iget-object v2, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, v0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-static {v2, v3}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 3722
    iget-object v0, v0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ci;->b(Landroid/view/View;)V

    .line 3698
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 3728
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3729
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_6

    .line 3730
    iget-object v0, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cr;

    .line 3731
    iget-wide v2, v0, Landroid/support/v7/widget/cr;->d:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_5

    .line 3732
    iget v2, v0, Landroid/support/v7/widget/cr;->e:I

    if-ne p3, v2, :cond_4

    .line 3733
    iget-object v2, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 3737
    :cond_4
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/ci;->c(I)Z

    .line 3729
    :cond_5
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 3742
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/view/ViewGroup;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 3411
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 3412
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3413
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    .line 3414
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Landroid/support/v7/widget/ci;->a(Landroid/view/ViewGroup;Z)V

    .line 3411
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 3417
    :cond_1
    if-nez p2, :cond_2

    .line 3429
    :goto_1
    return-void

    .line 3421
    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 3422
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3423
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 3425
    :cond_3
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    .line 3426
    invoke-virtual {p1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 3427
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method private c(Landroid/support/v7/widget/cr;)V
    .locals 1

    .prologue
    .line 3746
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->u(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/cj;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3747
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->u(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/cj;

    .line 3749
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3750
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    .line 3752
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    if-eqz v0, :cond_2

    .line 3753
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/co;->a(Landroid/support/v7/widget/cr;)V

    .line 3756
    :cond_2
    return-void
.end method

.method private d(I)Landroid/support/v7/widget/cr;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/16 v10, 0x20

    const/4 v2, 0x0

    .line 3611
    iget-object v0, p0, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v0, v1

    .line 3636
    :goto_0
    return-object v0

    :cond_1
    move v3, v2

    .line 3615
    :goto_1
    if-ge v3, v4, :cond_3

    .line 3616
    iget-object v0, p0, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cr;

    .line 3617
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->f()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->c()I

    move-result v5

    if-ne v5, p1, :cond_2

    .line 3618
    invoke-virtual {v0, v10}, Landroid/support/v7/widget/cr;->a(I)V

    goto :goto_0

    .line 3615
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 3623
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v0

    iget-boolean v0, v0, Landroid/support/v7/widget/bv;->b:Z

    if-eqz v0, :cond_5

    .line 3624
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0, p1, v2}, Landroid/support/v7/widget/n;->a(II)I

    move-result v0

    .line 3625
    if-lez v0, :cond_5

    iget-object v3, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/widget/bv;->a()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 3626
    iget-object v3, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/bv;->b(I)J

    move-result-wide v6

    .line 3627
    :goto_2
    if-ge v2, v4, :cond_5

    .line 3628
    iget-object v0, p0, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cr;

    .line 3629
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->f()Z

    move-result v3

    if-nez v3, :cond_4

    iget-wide v8, v0, Landroid/support/v7/widget/cr;->d:J

    cmp-long v3, v8, v6

    if-nez v3, :cond_4

    .line 3630
    invoke-virtual {v0, v10}, Landroid/support/v7/widget/cr;->a(I)V

    goto :goto_0

    .line 3627
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_5
    move-object v0, v1

    .line 3636
    goto :goto_0
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    .line 3233
    if-ltz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    invoke-virtual {v0}, Landroid/support/v7/widget/co;->a()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 3234
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". State item count is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    invoke-virtual {v2}, Landroid/support/v7/widget/co;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3237
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-boolean v0, v0, Landroid/support/v7/widget/co;->i:Z

    if-nez v0, :cond_2

    .line 3240
    :goto_0
    return p1

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/n;->a(I)I

    move-result p1

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 3102
    iget-object v0, p0, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3103
    iget-object v0, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ci;->c(I)Z

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 3104
    return-void
.end method

.method final a(Landroid/support/v7/widget/cr;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3500
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->d()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 3501
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Scrapped or attached views may not be recycled. isScrap:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->d()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isAttached:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    move v0, v1

    goto :goto_0

    .line 3507
    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3508
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Trying to recycle an ignored view holder. You should first call stopIgnoringView(view) before calling recycle."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3511
    :cond_3
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->n()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 3513
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->h()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-boolean v2, v2, Landroid/support/v7/widget/co;->i:Z

    if-nez v2, :cond_4

    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->l()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->j()Z

    move-result v2

    if-nez v2, :cond_6

    .line 3516
    iget-object v2, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Landroid/support/v7/widget/ci;->f:I

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v1

    .line 3517
    :goto_1
    iget-object v3, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 3518
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/ci;->c(I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 3519
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3523
    :cond_5
    iget-object v2, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Landroid/support/v7/widget/ci;->f:I

    if-ge v2, v3, :cond_6

    .line 3524
    iget-object v1, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v0

    .line 3528
    :cond_6
    if-nez v1, :cond_7

    .line 3529
    invoke-virtual {p0}, Landroid/support/v7/widget/ci;->b()Landroid/support/v7/widget/ch;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ch;->a(Landroid/support/v7/widget/cr;)V

    .line 3530
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ci;->c(Landroid/support/v7/widget/cr;)V

    .line 3538
    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/co;->a(Landroid/support/v7/widget/cr;)V

    .line 3539
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 3444
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v0

    .line 3445
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3446
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->e()V

    .line 3450
    :cond_0
    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ci;->a(Landroid/support/v7/widget/cr;)V

    .line 3451
    return-void

    .line 3447
    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3448
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->g()V

    goto :goto_0
.end method

.method final b()Landroid/support/v7/widget/ch;
    .locals 1

    .prologue
    .line 3861
    iget-object v0, p0, Landroid/support/v7/widget/ci;->g:Landroid/support/v7/widget/ch;

    if-nez v0, :cond_0

    .line 3862
    new-instance v0, Landroid/support/v7/widget/ch;

    invoke-direct {v0}, Landroid/support/v7/widget/ch;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/ci;->g:Landroid/support/v7/widget/ch;

    .line 3864
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ci;->g:Landroid/support/v7/widget/ch;

    return-object v0
.end method

.method public final b(I)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3258
    if-ltz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    invoke-virtual {v0}, Landroid/support/v7/widget/co;->a()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid item position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "). Item count:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    invoke-virtual {v2}, Landroid/support/v7/widget/co;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-boolean v0, v0, Landroid/support/v7/widget/co;->i:Z

    if-eqz v0, :cond_1e

    invoke-direct {p0, p1}, Landroid/support/v7/widget/ci;->d(I)Landroid/support/v7/widget/cr;

    move-result-object v1

    if-eqz v1, :cond_3

    move v0, v2

    :goto_0
    move-object v10, v1

    move v1, v0

    move-object v0, v10

    :goto_1
    if-nez v0, :cond_6

    invoke-direct {p0, p1, v3}, Landroid/support/v7/widget/ci;->a(IZ)Landroid/support/v7/widget/cr;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->l()Z

    move-result v4

    if-nez v4, :cond_9

    iget v4, v0, Landroid/support/v7/widget/cr;->b:I

    if-ltz v4, :cond_2

    iget v4, v0, Landroid/support/v7/widget/cr;->b:I

    iget-object v6, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v6}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v7/widget/bv;->a()I

    move-result v6

    if-lt v4, v6, :cond_4

    :cond_2
    new-instance v1, Ljava/lang/IndexOutOfBoundsException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Inconsistency detected. Invalid view holder adapter position"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    move v0, v3

    goto :goto_0

    :cond_4
    iget-object v4, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-boolean v4, v4, Landroid/support/v7/widget/co;->i:Z

    if-nez v4, :cond_8

    iget-object v4, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v4

    iget v6, v0, Landroid/support/v7/widget/cr;->b:I

    invoke-virtual {v4, v6}, Landroid/support/v7/widget/bv;->a(I)I

    move-result v4

    iget v6, v0, Landroid/support/v7/widget/cr;->e:I

    if-eq v4, v6, :cond_8

    move v4, v3

    :goto_2
    if-nez v4, :cond_b

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/cr;->a(I)V

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->d()Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v4, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v6, v0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-static {v4, v6}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->e()V

    :cond_5
    :goto_3
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ci;->a(Landroid/support/v7/widget/cr;)V

    move-object v0, v5

    :cond_6
    :goto_4
    if-nez v0, :cond_1d

    iget-object v4, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v4, v4, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v4, p1}, Landroid/support/v7/widget/n;->a(I)I

    move-result v6

    if-ltz v6, :cond_7

    iget-object v4, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/widget/bv;->a()I

    move-result v4

    if-lt v6, v4, :cond_c

    :cond_7
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inconsistency detected. Invalid item position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "(offset:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ").state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, v2, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    invoke-virtual {v2}, Landroid/support/v7/widget/co;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget-object v4, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v4

    iget-boolean v4, v4, Landroid/support/v7/widget/bv;->b:Z

    if-eqz v4, :cond_9

    iget-wide v6, v0, Landroid/support/v7/widget/cr;->d:J

    iget-object v4, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v4

    iget v8, v0, Landroid/support/v7/widget/cr;->b:I

    invoke-virtual {v4, v8}, Landroid/support/v7/widget/bv;->b(I)J

    move-result-wide v8

    cmp-long v4, v6, v8

    if-eqz v4, :cond_9

    move v4, v3

    goto :goto_2

    :cond_9
    move v4, v2

    goto/16 :goto_2

    :cond_a
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->f()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->g()V

    goto :goto_3

    :cond_b
    move v1, v2

    goto :goto_4

    :cond_c
    iget-object v4, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/support/v7/widget/bv;->a(I)I

    move-result v4

    iget-object v7, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v7}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v7

    iget-boolean v7, v7, Landroid/support/v7/widget/bv;->b:Z

    if-eqz v7, :cond_1c

    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/bv;->b(I)J

    move-result-wide v8

    invoke-direct {p0, v8, v9, v4, v3}, Landroid/support/v7/widget/ci;->a(JIZ)Landroid/support/v7/widget/cr;

    move-result-object v0

    if-eqz v0, :cond_1c

    iput v6, v0, Landroid/support/v7/widget/cr;->b:I

    move v4, v2

    :goto_5
    if-nez v0, :cond_e

    iget-object v1, p0, Landroid/support/v7/widget/ci;->h:Landroid/support/v7/widget/cp;

    if-eqz v1, :cond_e

    iget-object v1, p0, Landroid/support/v7/widget/ci;->h:Landroid/support/v7/widget/cp;

    invoke-virtual {v1}, Landroid/support/v7/widget/cp;->a()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v0

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "getViewForPositionAndType returned a view which does not have a ViewHolder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->b()Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "getViewForPositionAndType returned a view that is ignored. You must call stopIgnoring before returning this view."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    if-nez v0, :cond_10

    invoke-virtual {p0}, Landroid/support/v7/widget/ci;->b()Landroid/support/v7/widget/ch;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/support/v7/widget/bv;->a(I)I

    move-result v1

    iget-object v0, v0, Landroid/support/v7/widget/ch;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_11

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/cr;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_6
    if-eqz v1, :cond_f

    invoke-virtual {v1}, Landroid/support/v7/widget/cr;->m()V

    invoke-static {}, Landroid/support/v7/widget/RecyclerView;->h()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, v1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_f

    iget-object v0, v1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/ci;->a(Landroid/view/ViewGroup;Z)V

    :cond_f
    move-object v0, v1

    :cond_10
    if-nez v0, :cond_1b

    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/bv;->a(I)I

    move-result v5

    invoke-virtual {v0, v1, v5}, Landroid/support/v7/widget/bv;->a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cr;

    move-result-object v0

    iput v5, v0, Landroid/support/v7/widget/cr;->e:I

    move-object v1, v0

    :goto_7
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-boolean v0, v0, Landroid/support/v7/widget/co;->i:Z

    if-eqz v0, :cond_12

    invoke-virtual {v1}, Landroid/support/v7/widget/cr;->k()Z

    move-result v0

    if-eqz v0, :cond_12

    iput p1, v1, Landroid/support/v7/widget/cr;->f:I

    move v5, v3

    :goto_8
    iget-object v0, v1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_17

    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v6, v1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_9
    iput-object v1, v0, Landroid/support/v7/widget/ce;->a:Landroid/support/v7/widget/cr;

    if-eqz v4, :cond_19

    if-eqz v5, :cond_19

    :goto_a
    iput-boolean v2, v0, Landroid/support/v7/widget/ce;->d:Z

    iget-object v0, v1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    return-object v0

    :cond_11
    move-object v1, v5

    goto :goto_6

    :cond_12
    invoke-virtual {v1}, Landroid/support/v7/widget/cr;->k()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-virtual {v1}, Landroid/support/v7/widget/cr;->i()Z

    move-result v0

    if-nez v0, :cond_13

    invoke-virtual {v1}, Landroid/support/v7/widget/cr;->h()Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_13
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/n;->a(I)I

    move-result v0

    iget-object v5, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView;->g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;

    move-result-object v5

    invoke-virtual {v5, v1, v0}, Landroid/support/v7/widget/bv;->b(Landroid/support/v7/widget/cr;I)V

    iget-object v0, v1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    iget-object v5, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView;->s(Landroid/support/v7/widget/RecyclerView;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v5

    if-eqz v5, :cond_15

    iget-object v5, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView;->s(Landroid/support/v7/widget/RecyclerView;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-static {v0}, Landroid/support/v4/view/at;->e(Landroid/view/View;)I

    move-result v5

    if-nez v5, :cond_14

    invoke-static {v0, v2}, Landroid/support/v4/view/at;->c(Landroid/view/View;I)V

    :cond_14
    invoke-static {v0}, Landroid/support/v4/view/at;->b(Landroid/view/View;)Z

    move-result v5

    if-nez v5, :cond_15

    iget-object v5, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView;->t(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/cs;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/widget/cs;->b()Landroid/support/v4/view/a;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/support/v4/view/at;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    :cond_15
    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/co;

    iget-boolean v0, v0, Landroid/support/v7/widget/co;->i:Z

    if-eqz v0, :cond_16

    iput p1, v1, Landroid/support/v7/widget/cr;->f:I

    :cond_16
    move v5, v2

    goto/16 :goto_8

    :cond_17
    iget-object v6, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v6

    if-nez v6, :cond_18

    iget-object v6, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    iget-object v6, v1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_9

    :cond_18
    check-cast v0, Landroid/support/v7/widget/ce;

    goto/16 :goto_9

    :cond_19
    move v2, v3

    goto/16 :goto_a

    :cond_1a
    move v5, v3

    goto/16 :goto_8

    :cond_1b
    move-object v1, v0

    goto/16 :goto_7

    :cond_1c
    move v4, v1

    goto/16 :goto_5

    :cond_1d
    move v4, v1

    move-object v1, v0

    goto/16 :goto_7

    :cond_1e
    move-object v0, v5

    move v1, v3

    goto/16 :goto_1
.end method

.method final b(Landroid/support/v7/widget/cr;)V
    .locals 1

    .prologue
    .line 3587
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ci;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->h(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 3588
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/ci;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 3592
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p1, Landroid/support/v7/widget/cr;->j:Landroid/support/v7/widget/ci;

    .line 3593
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->g()V

    .line 3594
    return-void

    .line 3590
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/ci;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method final b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 3547
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cr;

    move-result-object v0

    .line 3548
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/cr;->j:Landroid/support/v7/widget/ci;

    .line 3549
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->g()V

    .line 3550
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/ci;->a(Landroid/support/v7/widget/cr;)V

    .line 3551
    return-void
.end method

.method final c(I)Z
    .locals 2

    .prologue
    .line 3481
    iget-object v0, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cr;

    .line 3485
    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3486
    invoke-virtual {p0}, Landroid/support/v7/widget/ci;->b()Landroid/support/v7/widget/ch;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/ch;->a(Landroid/support/v7/widget/cr;)V

    .line 3487
    invoke-direct {p0, v0}, Landroid/support/v7/widget/ci;->c(Landroid/support/v7/widget/cr;)V

    .line 3488
    iget-object v0, p0, Landroid/support/v7/widget/ci;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 3489
    const/4 v0, 0x1

    .line 3491
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
