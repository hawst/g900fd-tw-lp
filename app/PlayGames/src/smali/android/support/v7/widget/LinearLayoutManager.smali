.class public Landroid/support/v7/widget/LinearLayoutManager;
.super Landroid/support/v7/widget/cd;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/v7/widget/au;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field i:I

.field j:Landroid/support/v7/widget/bk;

.field k:Z

.field l:I

.field m:I

.field n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

.field final o:Landroid/support/v7/widget/as;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(B)V

    .line 142
    return-void
.end method

.method private constructor <init>(B)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 150
    invoke-direct {p0}, Landroid/support/v7/widget/cd;-><init>()V

    .line 91
    iput-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->c:Z

    .line 98
    iput-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    .line 105
    iput-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->d:Z

    .line 111
    iput-boolean v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->e:Z

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    .line 123
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    .line 127
    iput-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    .line 151
    new-instance v0, Landroid/support/v7/widget/as;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/as;-><init>(Landroid/support/v7/widget/LinearLayoutManager;)V

    iput-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    .line 152
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Ljava/lang/String;)V

    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-eq v3, v0, :cond_0

    iput v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    iput-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->h()V

    .line 153
    :cond_0
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Ljava/lang/String;)V

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->c:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->c:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->h()V

    .line 154
    :cond_1
    return-void
.end method

.method private a(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)I
    .locals 3

    .prologue
    .line 794
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    sub-int/2addr v0, p1

    .line 795
    if-lez v0, :cond_1

    .line 797
    neg-int v0, v0

    invoke-direct {p0, v0, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;->d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v0

    neg-int v0, v0

    .line 802
    add-int v1, p1, v0

    .line 803
    if-eqz p4, :cond_0

    .line 805
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->c()I

    move-result v2

    sub-int v1, v2, v1

    .line 806
    if-lez v1, :cond_0

    .line 807
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/bk;->a(I)V

    .line 808
    add-int/2addr v0, v1

    .line 811
    :cond_0
    :goto_0
    return v0

    .line 799
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;Landroid/support/v7/widget/co;Z)I
    .locals 8

    .prologue
    const/high16 v7, -0x80000000

    const/4 v6, 0x0

    .line 1253
    iget v1, p2, Landroid/support/v7/widget/au;->c:I

    .line 1254
    iget v0, p2, Landroid/support/v7/widget/au;->g:I

    if-eq v0, v7, :cond_1

    .line 1256
    iget v0, p2, Landroid/support/v7/widget/au;->c:I

    if-gez v0, :cond_0

    .line 1257
    iget v0, p2, Landroid/support/v7/widget/au;->g:I

    iget v2, p2, Landroid/support/v7/widget/au;->c:I

    add-int/2addr v0, v2

    iput v0, p2, Landroid/support/v7/widget/au;->g:I

    .line 1259
    :cond_0
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;)V

    .line 1261
    :cond_1
    iget v0, p2, Landroid/support/v7/widget/au;->c:I

    iget v2, p2, Landroid/support/v7/widget/au;->h:I

    add-int/2addr v0, v2

    .line 1262
    new-instance v2, Landroid/support/v7/widget/at;

    invoke-direct {v2}, Landroid/support/v7/widget/at;-><init>()V

    .line 1263
    :cond_2
    if-lez v0, :cond_7

    invoke-virtual {p2, p3}, Landroid/support/v7/widget/au;->a(Landroid/support/v7/widget/co;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1264
    iput v6, v2, Landroid/support/v7/widget/at;->a:I

    iput-boolean v6, v2, Landroid/support/v7/widget/at;->b:Z

    iput-boolean v6, v2, Landroid/support/v7/widget/at;->c:Z

    iput-boolean v6, v2, Landroid/support/v7/widget/at;->d:Z

    .line 1265
    invoke-virtual {p0, p1, p3, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Landroid/support/v7/widget/au;Landroid/support/v7/widget/at;)V

    .line 1266
    iget-boolean v3, v2, Landroid/support/v7/widget/at;->b:Z

    if-nez v3, :cond_7

    .line 1267
    iget v3, p2, Landroid/support/v7/widget/au;->b:I

    iget v4, v2, Landroid/support/v7/widget/at;->a:I

    iget v5, p2, Landroid/support/v7/widget/au;->f:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/au;->b:I

    .line 1276
    iget-boolean v3, v2, Landroid/support/v7/widget/at;->c:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-object v3, v3, Landroid/support/v7/widget/au;->j:Ljava/util/List;

    if-nez v3, :cond_3

    iget-boolean v3, p3, Landroid/support/v7/widget/co;->i:Z

    if-nez v3, :cond_4

    .line 1278
    :cond_3
    iget v3, p2, Landroid/support/v7/widget/au;->c:I

    iget v4, v2, Landroid/support/v7/widget/at;->a:I

    sub-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/au;->c:I

    .line 1280
    iget v3, v2, Landroid/support/v7/widget/at;->a:I

    sub-int/2addr v0, v3

    .line 1283
    :cond_4
    iget v3, p2, Landroid/support/v7/widget/au;->g:I

    if-eq v3, v7, :cond_6

    .line 1284
    iget v3, p2, Landroid/support/v7/widget/au;->g:I

    iget v4, v2, Landroid/support/v7/widget/at;->a:I

    add-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/au;->g:I

    .line 1285
    iget v3, p2, Landroid/support/v7/widget/au;->c:I

    if-gez v3, :cond_5

    .line 1286
    iget v3, p2, Landroid/support/v7/widget/au;->g:I

    iget v4, p2, Landroid/support/v7/widget/au;->c:I

    add-int/2addr v3, v4

    iput v3, p2, Landroid/support/v7/widget/au;->g:I

    .line 1288
    :cond_5
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;)V

    .line 1290
    :cond_6
    if-eqz p4, :cond_2

    iget-boolean v3, v2, Landroid/support/v7/widget/at;->d:Z

    if-eqz v3, :cond_2

    .line 1291
    :cond_7
    iget v0, p2, Landroid/support/v7/widget/au;->c:I

    sub-int v0, v1, v0

    return v0
.end method

.method private a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 1464
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v1

    invoke-direct {p0, v0, v1, p1}, Landroid/support/v7/widget/LinearLayoutManager;->a(III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(III)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1472
    .line 1474
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v5

    .line 1475
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v6

    .line 1476
    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    move-object v4, v2

    .line 1477
    :goto_1
    if-eq p1, p2, :cond_3

    .line 1478
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v3

    .line 1479
    invoke-static {v3}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    .line 1480
    if-ltz v0, :cond_6

    if-ge v0, p3, :cond_6

    .line 1481
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1482
    if-nez v4, :cond_6

    move-object v0, v2

    .line 1477
    :goto_2
    add-int/2addr p1, v1

    move-object v2, v0

    move-object v4, v3

    goto :goto_1

    .line 1476
    :cond_0
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 1485
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v0

    if-ge v0, v6, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v0

    if-ge v0, v5, :cond_4

    .line 1487
    :cond_2
    if-nez v2, :cond_6

    move-object v0, v3

    move-object v3, v4

    .line 1488
    goto :goto_2

    .line 1495
    :cond_3
    if-eqz v2, :cond_5

    move-object v3, v2

    :cond_4
    :goto_3
    return-object v3

    :cond_5
    move-object v3, v4

    goto :goto_3

    :cond_6
    move-object v0, v2

    move-object v3, v4

    goto :goto_2
.end method

.method private a(IIZLandroid/support/v7/widget/co;)V
    .locals 6

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x1

    .line 1050
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    invoke-direct {p0, p4}, Landroid/support/v7/widget/LinearLayoutManager;->g(Landroid/support/v7/widget/co;)I

    move-result v3

    iput v3, v2, Landroid/support/v7/widget/au;->h:I

    .line 1051
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput p1, v2, Landroid/support/v7/widget/au;->f:I

    .line 1053
    if-ne p1, v1, :cond_2

    .line 1054
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v3, v2, Landroid/support/v7/widget/au;->h:I

    iget-object v4, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v4}, Landroid/support/v7/widget/bk;->f()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v2, Landroid/support/v7/widget/au;->h:I

    .line 1056
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->v()Landroid/view/View;

    move-result-object v2

    .line 1058
    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-boolean v4, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v4, :cond_1

    :goto_0
    iput v0, v3, Landroid/support/v7/widget/au;->e:I

    .line 1060
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    invoke-static {v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v3, v3, Landroid/support/v7/widget/au;->e:I

    add-int/2addr v1, v3

    iput v1, v0, Landroid/support/v7/widget/au;->d:I

    .line 1061
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/au;->b:I

    .line 1063
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->c()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1076
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput p2, v1, Landroid/support/v7/widget/au;->c:I

    .line 1077
    if-eqz p3, :cond_0

    .line 1078
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v2, v1, Landroid/support/v7/widget/au;->c:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/support/v7/widget/au;->c:I

    .line 1080
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v0, v1, Landroid/support/v7/widget/au;->g:I

    .line 1081
    return-void

    :cond_1
    move v0, v1

    .line 1058
    goto :goto_0

    .line 1067
    :cond_2
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->u()Landroid/view/View;

    move-result-object v2

    .line 1068
    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v4, v3, Landroid/support/v7/widget/au;->h:I

    iget-object v5, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v5}, Landroid/support/v7/widget/bk;->b()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v3, Landroid/support/v7/widget/au;->h:I

    .line 1069
    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-boolean v4, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v4, :cond_3

    :goto_2
    iput v1, v3, Landroid/support/v7/widget/au;->e:I

    .line 1071
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    invoke-static {v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v3, v3, Landroid/support/v7/widget/au;->e:I

    add-int/2addr v1, v3

    iput v1, v0, Landroid/support/v7/widget/au;->d:I

    .line 1072
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/au;->b:I

    .line 1073
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->b()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_3
    move v1, v0

    .line 1069
    goto :goto_2
.end method

.method private a(Landroid/support/v7/widget/as;)V
    .locals 2

    .prologue
    .line 840
    iget v0, p1, Landroid/support/v7/widget/as;->a:I

    iget v1, p1, Landroid/support/v7/widget/as;->b:I

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->c(II)V

    .line 841
    return-void
.end method

.method private a(Landroid/support/v7/widget/ci;II)V
    .locals 1

    .prologue
    .line 1122
    if-ne p2, p3, :cond_1

    .line 1137
    :cond_0
    return-void

    .line 1128
    :cond_1
    if-le p3, p2, :cond_2

    .line 1129
    add-int/lit8 v0, p3, -0x1

    :goto_0
    if-lt v0, p2, :cond_0

    .line 1130
    invoke-virtual {p0, v0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->a(ILandroid/support/v7/widget/ci;)V

    .line 1129
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1133
    :cond_2
    :goto_1
    if-le p2, p3, :cond_0

    .line 1134
    invoke-virtual {p0, p2, p1}, Landroid/support/v7/widget/LinearLayoutManager;->a(ILandroid/support/v7/widget/ci;)V

    .line 1133
    add-int/lit8 p2, p2, -0x1

    goto :goto_1
.end method

.method private a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1229
    iget-boolean v0, p2, Landroid/support/v7/widget/au;->a:Z

    if-nez v0, :cond_1

    .line 1237
    :cond_0
    :goto_0
    return-void

    .line 1232
    :cond_1
    iget v0, p2, Landroid/support/v7/widget/au;->f:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_5

    .line 1233
    iget v0, p2, Landroid/support/v7/widget/au;->g:I

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v2

    if-ltz v0, :cond_0

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v3}, Landroid/support/v7/widget/bk;->d()I

    move-result v3

    sub-int/2addr v3, v0

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v5, v4}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v4

    if-ge v4, v3, :cond_2

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;II)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v2, -0x1

    :goto_2
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v1

    if-ge v1, v3, :cond_4

    add-int/lit8 v1, v2, -0x1

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;II)V

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 1235
    :cond_5
    iget v2, p2, Landroid/support/v7/widget/au;->g:I

    if-ltz v2, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v3

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_7

    add-int/lit8 v0, v3, -0x1

    :goto_3
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v1

    if-le v1, v2, :cond_6

    add-int/lit8 v1, v3, -0x1

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;II)V

    goto :goto_0

    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_7
    move v0, v1

    :goto_4
    if-ge v0, v3, :cond_0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v5, v4}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v4

    if-le v4, v2, :cond_8

    invoke-direct {p0, p1, v1, v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;II)V

    goto :goto_0

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method private b(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)I
    .locals 4

    .prologue
    .line 819
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v0

    sub-int v0, p1, v0

    .line 820
    if-lez v0, :cond_1

    .line 823
    invoke-direct {p0, v0, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;->d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v0

    neg-int v0, v0

    .line 827
    add-int v1, p1, v0

    .line 828
    if-eqz p4, :cond_0

    .line 830
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    sub-int/2addr v1, v2

    .line 831
    if-lez v1, :cond_0

    .line 832
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    neg-int v3, v1

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/bk;->a(I)V

    .line 833
    sub-int/2addr v0, v1

    .line 836
    :cond_0
    :goto_0
    return v0

    .line 825
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 321
    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-eq v1, v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->g()Z

    move-result v1

    if-nez v1, :cond_2

    .line 322
    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->c:Z

    .line 324
    :cond_1
    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    .line 326
    return-void

    .line 324
    :cond_2
    iget-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->c:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/support/v7/widget/as;)V
    .locals 2

    .prologue
    .line 854
    iget v0, p1, Landroid/support/v7/widget/as;->a:I

    iget v1, p1, Landroid/support/v7/widget/as;->b:I

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->d(II)V

    .line 855
    return-void
.end method

.method private c(II)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 844
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->c()I

    move-result v2

    sub-int/2addr v2, p2

    iput v2, v0, Landroid/support/v7/widget/au;->c:I

    .line 845
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, v2, Landroid/support/v7/widget/au;->e:I

    .line 847
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput p1, v0, Landroid/support/v7/widget/au;->d:I

    .line 848
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v1, v0, Landroid/support/v7/widget/au;->f:I

    .line 849
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput p2, v0, Landroid/support/v7/widget/au;->b:I

    .line 850
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/au;->g:I

    .line 851
    return-void

    :cond_0
    move v0, v1

    .line 845
    goto :goto_0
.end method

.method private d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1084
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move p1, v2

    .line 1105
    :goto_0
    return p1

    .line 1087
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput-boolean v1, v0, Landroid/support/v7/widget/au;->a:Z

    .line 1088
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->t()V

    .line 1089
    if-lez p1, :cond_2

    move v0, v1

    .line 1090
    :goto_1
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 1091
    invoke-direct {p0, v0, v3, v1, p3}, Landroid/support/v7/widget/LinearLayoutManager;->a(IIZLandroid/support/v7/widget/co;)V

    .line 1092
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v1, v1, Landroid/support/v7/widget/au;->g:I

    .line 1093
    iget-object v4, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    invoke-direct {p0, p2, v4, p3, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;Landroid/support/v7/widget/co;Z)I

    move-result v4

    add-int/2addr v1, v4

    .line 1094
    if-gez v1, :cond_3

    move p1, v2

    .line 1098
    goto :goto_0

    .line 1089
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 1100
    :cond_3
    if-le v3, v1, :cond_4

    mul-int p1, v0, v1

    .line 1101
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bk;->a(I)V

    goto :goto_0
.end method

.method private d(II)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 858
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    sub-int v2, p2, v2

    iput v2, v0, Landroid/support/v7/widget/au;->c:I

    .line 859
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput p1, v0, Landroid/support/v7/widget/au;->d:I

    .line 860
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, v2, Landroid/support/v7/widget/au;->e:I

    .line 862
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v1, v0, Landroid/support/v7/widget/au;->f:I

    .line 863
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput p2, v0, Landroid/support/v7/widget/au;->b:I

    .line 864
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/au;->g:I

    .line 866
    return-void

    :cond_0
    move v0, v1

    .line 860
    goto :goto_0
.end method

.method private e(II)Landroid/view/View;
    .locals 6

    .prologue
    .line 1575
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    .line 1576
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v3

    .line 1577
    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    .line 1578
    :goto_0
    if-eq p1, p2, :cond_2

    .line 1579
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v1

    .line 1580
    iget-object v4, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v4

    .line 1581
    iget-object v5, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v5, v1}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v5

    .line 1582
    if-ge v4, v3, :cond_1

    if-le v5, v2, :cond_1

    move-object v0, v1

    .line 1592
    :goto_1
    return-object v0

    .line 1577
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 1578
    :cond_1
    add-int/2addr p1, v0

    goto :goto_0

    .line 1592
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private g(Landroid/support/v7/widget/co;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 391
    iget v1, p1, Landroid/support/v7/widget/co;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    .line 392
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->e()I

    move-result v0

    .line 394
    :cond_0
    return v0

    :cond_1
    move v1, v0

    .line 391
    goto :goto_0
.end method

.method private h(Landroid/support/v7/widget/co;)I
    .locals 7

    .prologue
    .line 990
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    if-nez v0, :cond_0

    .line 991
    const/4 v0, 0x0

    .line 993
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->u()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->v()Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/LinearLayoutManager;->e:Z

    iget-boolean v6, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v7/widget/cx;->a(Landroid/support/v7/widget/co;Landroid/support/v7/widget/bk;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/cd;ZZ)I

    move-result v0

    goto :goto_0
.end method

.method private i(Landroid/support/v7/widget/co;)I
    .locals 6

    .prologue
    .line 999
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    if-nez v0, :cond_0

    .line 1000
    const/4 v0, 0x0

    .line 1002
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->u()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->v()Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/LinearLayoutManager;->e:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/cx;->a(Landroid/support/v7/widget/co;Landroid/support/v7/widget/bk;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/cd;Z)I

    move-result v0

    goto :goto_0
.end method

.method private i(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 1468
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1, p1}, Landroid/support/v7/widget/LinearLayoutManager;->a(III)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private j(Landroid/support/v7/widget/co;)I
    .locals 6

    .prologue
    .line 1008
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    if-nez v0, :cond_0

    .line 1009
    const/4 v0, 0x0

    .line 1011
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->u()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->v()Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/LinearLayoutManager;->e:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/cx;->b(Landroid/support/v7/widget/co;Landroid/support/v7/widget/bk;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/cd;Z)I

    move-result v0

    goto :goto_0
.end method

.method private k(Landroid/support/v7/widget/co;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1443
    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/co;->a()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/widget/co;->a()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->i(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private l(Landroid/support/v7/widget/co;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1459
    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/widget/co;->a()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->i(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/support/v7/widget/co;->a()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private t()V
    .locals 1

    .prologue
    .line 873
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    if-nez v0, :cond_0

    .line 874
    new-instance v0, Landroid/support/v7/widget/au;

    invoke-direct {v0}, Landroid/support/v7/widget/au;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    .line 876
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    if-nez v0, :cond_1

    .line 877
    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    invoke-static {p0, v0}, Landroid/support/v7/widget/bk;->a(Landroid/support/v7/widget/cd;I)Landroid/support/v7/widget/bk;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    .line 879
    :cond_1
    return-void
.end method

.method private u()Landroid/view/View;
    .locals 1

    .prologue
    .line 1418
    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()Landroid/view/View;
    .locals 1

    .prologue
    .line 1428
    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 2

    .prologue
    .line 941
    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 942
    const/4 v0, 0x0

    .line 944
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;->d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 961
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->h(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public a()Landroid/support/v7/widget/ce;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 161
    new-instance v0, Landroid/support/v7/widget/ce;

    invoke-direct {v0, v1, v1}, Landroid/support/v7/widget/ce;-><init>(II)V

    return-object v0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 241
    instance-of v0, p1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 242
    check-cast p1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iput-object p1, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    .line 243
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->h()V

    .line 250
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/ci;)V
    .locals 1

    .prologue
    .line 195
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/cd;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/ci;)V

    .line 196
    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->f:Z

    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/LinearLayoutManager;->b(Landroid/support/v7/widget/ci;)V

    .line 198
    invoke-virtual {p2}, Landroid/support/v7/widget/ci;->a()V

    .line 200
    :cond_0
    return-void
.end method

.method a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Landroid/support/v7/widget/au;Landroid/support/v7/widget/at;)V
    .locals 9

    .prologue
    const/4 v4, -0x1

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1302
    invoke-virtual {p3, p1}, Landroid/support/v7/widget/au;->a(Landroid/support/v7/widget/ci;)Landroid/view/View;

    move-result-object v6

    .line 1303
    if-nez v6, :cond_0

    .line 1309
    iput-boolean v5, p4, Landroid/support/v7/widget/at;->b:Z

    .line 1372
    :goto_0
    return-void

    .line 1312
    :cond_0
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    .line 1313
    iget-object v1, p3, Landroid/support/v7/widget/au;->j:Ljava/util/List;

    if-nez v1, :cond_5

    .line 1314
    iget-boolean v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    iget v1, p3, Landroid/support/v7/widget/au;->f:I

    if-ne v1, v4, :cond_3

    move v1, v5

    :goto_1
    if-ne v3, v1, :cond_4

    .line 1316
    invoke-super {p0, v6, v4, v2}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;IZ)V

    .line 1328
    :goto_2
    invoke-virtual {p0, v6, v2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;II)V

    .line 1329
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v6}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v1

    iput v1, p4, Landroid/support/v7/widget/at;->a:I

    .line 1331
    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-ne v1, v5, :cond_a

    .line 1332
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->g()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1333
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->k()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->o()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1334
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2, v6}, Landroid/support/v7/widget/bk;->d(Landroid/view/View;)I

    move-result v2

    sub-int v2, v1, v2

    .line 1339
    :goto_3
    iget v3, p3, Landroid/support/v7/widget/au;->f:I

    if-ne v3, v4, :cond_9

    .line 1340
    iget v3, p3, Landroid/support/v7/widget/au;->b:I

    .line 1341
    iget v4, p3, Landroid/support/v7/widget/au;->b:I

    iget v7, p4, Landroid/support/v7/widget/at;->a:I

    sub-int/2addr v4, v7

    move v8, v3

    move v3, v4

    move v4, v2

    move v2, v1

    move v1, v8

    .line 1360
    :goto_4
    iget v7, v0, Landroid/support/v7/widget/ce;->leftMargin:I

    add-int/2addr v4, v7

    iget v7, v0, Landroid/support/v7/widget/ce;->topMargin:I

    add-int/2addr v3, v7

    iget v7, v0, Landroid/support/v7/widget/ce;->rightMargin:I

    sub-int/2addr v2, v7

    iget v7, v0, Landroid/support/v7/widget/ce;->bottomMargin:I

    sub-int/2addr v1, v7

    invoke-static {v6, v4, v3, v2, v1}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;IIII)V

    .line 1368
    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1369
    :cond_1
    iput-boolean v5, p4, Landroid/support/v7/widget/at;->c:Z

    .line 1371
    :cond_2
    invoke-virtual {v6}, Landroid/view/View;->isFocusable()Z

    move-result v0

    iput-boolean v0, p4, Landroid/support/v7/widget/at;->d:Z

    goto :goto_0

    :cond_3
    move v1, v2

    .line 1314
    goto :goto_1

    .line 1318
    :cond_4
    invoke-super {p0, v6, v2, v2}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;IZ)V

    goto :goto_2

    .line 1321
    :cond_5
    iget-boolean v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    iget v1, p3, Landroid/support/v7/widget/au;->f:I

    if-ne v1, v4, :cond_6

    move v1, v5

    :goto_5
    if-ne v3, v1, :cond_7

    .line 1323
    invoke-super {p0, v6, v4, v5}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;IZ)V

    goto :goto_2

    :cond_6
    move v1, v2

    .line 1321
    goto :goto_5

    .line 1325
    :cond_7
    invoke-super {p0, v6, v2, v5}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;IZ)V

    goto :goto_2

    .line 1336
    :cond_8
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->m()I

    move-result v2

    .line 1337
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v6}, Landroid/support/v7/widget/bk;->d(Landroid/view/View;)I

    move-result v1

    add-int/2addr v1, v2

    goto :goto_3

    .line 1343
    :cond_9
    iget v4, p3, Landroid/support/v7/widget/au;->b:I

    .line 1344
    iget v3, p3, Landroid/support/v7/widget/au;->b:I

    iget v7, p4, Landroid/support/v7/widget/at;->a:I

    add-int/2addr v3, v7

    move v8, v3

    move v3, v4

    move v4, v2

    move v2, v1

    move v1, v8

    goto :goto_4

    .line 1347
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->n()I

    move-result v3

    .line 1348
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v6}, Landroid/support/v7/widget/bk;->d(Landroid/view/View;)I

    move-result v1

    add-int/2addr v1, v3

    .line 1350
    iget v2, p3, Landroid/support/v7/widget/au;->f:I

    if-ne v2, v4, :cond_b

    .line 1351
    iget v2, p3, Landroid/support/v7/widget/au;->b:I

    .line 1352
    iget v4, p3, Landroid/support/v7/widget/au;->b:I

    iget v7, p4, Landroid/support/v7/widget/at;->a:I

    sub-int/2addr v4, v7

    goto :goto_4

    .line 1354
    :cond_b
    iget v4, p3, Landroid/support/v7/widget/au;->b:I

    .line 1355
    iget v2, p3, Landroid/support/v7/widget/au;->b:I

    iget v7, p4, Landroid/support/v7/widget/at;->a:I

    add-int/2addr v2, v7

    goto :goto_4
.end method

.method a(Landroid/support/v7/widget/co;Landroid/support/v7/widget/as;)V
    .locals 0

    .prologue
    .line 579
    return-void
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 204
    invoke-super {p0, p1}, Landroid/support/v7/widget/cd;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 205
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    if-lez v0, :cond_0

    .line 206
    invoke-static {p1}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/ab;

    move-result-object v2

    .line 208
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v3

    invoke-direct {p0, v0, v3}, Landroid/support/v7/widget/LinearLayoutManager;->e(II)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v4/view/a/ab;->b(I)V

    .line 209
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->e(II)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Landroid/support/v4/view/a/ab;->c(I)V

    .line 211
    :cond_0
    return-void

    .line 208
    :cond_1
    invoke-static {v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_0

    .line 209
    :cond_2
    invoke-static {v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1110
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v0, :cond_0

    .line 1111
    invoke-super {p0, p1}, Landroid/support/v7/widget/cd;->a(Ljava/lang/String;)V

    .line 1113
    :cond_0
    return-void
.end method

.method public final b(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 953
    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-nez v0, :cond_0

    .line 954
    const/4 v0, 0x0

    .line 956
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutManager;->d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 966
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->h(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final b(I)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 366
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v1

    .line 367
    if-nez v1, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-object v0

    .line 370
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v2

    .line 371
    sub-int v2, p1, v2

    .line 372
    if-ltz v2, :cond_0

    if-ge v2, v1, :cond_0

    .line 373
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 971
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->i(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final c(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v0, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x1

    const/high16 v6, -0x80000000

    .line 1598
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->b()V

    .line 1599
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v1

    if-nez v1, :cond_1

    .line 1635
    :cond_0
    :goto_0
    return-object v0

    .line 1603
    :cond_1
    sparse-switch p1, :sswitch_data_0

    move v3, v6

    .line 1604
    :goto_1
    if-eq v3, v6, :cond_0

    .line 1608
    if-ne v3, v4, :cond_6

    .line 1609
    invoke-direct {p0, p3}, Landroid/support/v7/widget/LinearLayoutManager;->l(Landroid/support/v7/widget/co;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 1613
    :goto_2
    if-eqz v2, :cond_0

    .line 1620
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->t()V

    .line 1621
    const v1, 0x3ea8f5c3    # 0.33f

    iget-object v7, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v7}, Landroid/support/v7/widget/bk;->e()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v1, v7

    float-to-int v1, v1

    .line 1622
    invoke-direct {p0, v3, v1, v8, p3}, Landroid/support/v7/widget/LinearLayoutManager;->a(IIZLandroid/support/v7/widget/co;)V

    .line 1623
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v6, v1, Landroid/support/v7/widget/au;->g:I

    .line 1624
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput-boolean v8, v1, Landroid/support/v7/widget/au;->a:Z

    .line 1625
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    invoke-direct {p0, p2, v1, p3, v5}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;Landroid/support/v7/widget/co;Z)I

    .line 1627
    if-ne v3, v4, :cond_7

    .line 1628
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->u()Landroid/view/View;

    move-result-object v1

    .line 1632
    :goto_3
    if-eq v1, v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1635
    goto :goto_0

    :sswitch_0
    move v3, v4

    .line 1603
    goto :goto_1

    :sswitch_1
    move v3, v5

    goto :goto_1

    :sswitch_2
    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-ne v1, v5, :cond_2

    move v3, v4

    goto :goto_1

    :cond_2
    move v3, v6

    goto :goto_1

    :sswitch_3
    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-ne v1, v5, :cond_3

    move v3, v5

    goto :goto_1

    :cond_3
    move v3, v6

    goto :goto_1

    :sswitch_4
    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-nez v1, :cond_4

    move v3, v4

    goto :goto_1

    :cond_4
    move v3, v6

    goto :goto_1

    :sswitch_5
    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-nez v1, :cond_5

    move v3, v5

    goto :goto_1

    :cond_5
    move v3, v6

    goto :goto_1

    .line 1611
    :cond_6
    invoke-direct {p0, p3}, Landroid/support/v7/widget/LinearLayoutManager;->k(Landroid/support/v7/widget/co;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    goto :goto_2

    .line 1630
    :cond_7
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->v()Landroid/view/View;

    move-result-object v1

    goto :goto_3

    .line 1603
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x11 -> :sswitch_4
        0x21 -> :sswitch_2
        0x42 -> :sswitch_5
        0x82 -> :sswitch_3
    .end sparse-switch
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 897
    iput p1, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    .line 898
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    .line 899
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 900
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b()V

    .line 902
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->h()V

    .line 903
    return-void
.end method

.method public c(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)V
    .locals 12

    .prologue
    .line 441
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a:I

    iput v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    .line 445
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->t()V

    .line 446
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/au;->a:Z

    .line 448
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->b()V

    .line 450
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    const/4 v1, -0x1

    iput v1, v0, Landroid/support/v7/widget/as;->a:I

    const/high16 v1, -0x80000000

    iput v1, v0, Landroid/support/v7/widget/as;->b:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v7/widget/as;->c:Z

    .line 451
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    iget-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    iget-boolean v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->d:Z

    xor-int/2addr v1, v2

    iput-boolean v1, v0, Landroid/support/v7/widget/as;->c:Z

    .line 453
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    iget-boolean v0, p2, Landroid/support/v7/widget/co;->i:Z

    if-nez v0, :cond_1

    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_16

    const/4 v0, 0x0

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->d()Z

    move-result v3

    if-nez v3, :cond_19

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->f()I

    move-result v3

    if-ltz v3, :cond_19

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->f()I

    move-result v0

    invoke-virtual {p2}, Landroid/support/v7/widget/co;->a()I

    move-result v3

    if-ge v0, v3, :cond_19

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/as;->a(Landroid/view/View;)V

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_3
    if-nez v0, :cond_2

    invoke-virtual {v2}, Landroid/support/v7/widget/as;->a()V

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->d:Z

    if-eqz v0, :cond_21

    invoke-virtual {p2}, Landroid/support/v7/widget/co;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_4
    iput v0, v2, Landroid/support/v7/widget/as;->a:I

    .line 462
    :cond_2
    invoke-direct {p0, p2}, Landroid/support/v7/widget/LinearLayoutManager;->g(Landroid/support/v7/widget/co;)I

    move-result v1

    .line 463
    iget v0, p2, Landroid/support/v7/widget/co;->a:I

    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    iget v2, v2, Landroid/support/v7/widget/as;->a:I

    if-ge v0, v2, :cond_22

    const/4 v0, 0x1

    .line 464
    :goto_5
    iget-boolean v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-ne v0, v2, :cond_23

    .line 466
    const/4 v0, 0x0

    move v11, v1

    move v1, v0

    move v0, v11

    .line 471
    :goto_6
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    add-int/2addr v1, v2

    .line 472
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->f()I

    move-result v2

    add-int/2addr v0, v2

    .line 473
    iget-boolean v2, p2, Landroid/support/v7/widget/co;->i:Z

    if-eqz v2, :cond_3

    iget v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    iget v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    const/high16 v3, -0x80000000

    if-eq v2, v3, :cond_3

    .line 478
    iget v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->b(I)Landroid/view/View;

    move-result-object v2

    .line 479
    if-eqz v2, :cond_3

    .line 482
    iget-boolean v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v3, :cond_24

    .line 483
    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v3}, Landroid/support/v7/widget/bk;->c()I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v4, v2}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v2

    sub-int v2, v3, v2

    .line 485
    iget v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    sub-int/2addr v2, v3

    .line 491
    :goto_7
    if-lez v2, :cond_25

    .line 492
    add-int/2addr v1, v2

    .line 500
    :cond_3
    :goto_8
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    invoke-virtual {p0, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/co;Landroid/support/v7/widget/as;)V

    .line 501
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;)V

    .line 502
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget-boolean v3, p2, Landroid/support/v7/widget/co;->i:Z

    iput-boolean v3, v2, Landroid/support/v7/widget/au;->i:Z

    .line 503
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    iget-boolean v2, v2, Landroid/support/v7/widget/as;->c:Z

    if-eqz v2, :cond_26

    .line 505
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    invoke-direct {p0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->b(Landroid/support/v7/widget/as;)V

    .line 506
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v1, v2, Landroid/support/v7/widget/au;->h:I

    .line 507
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;Landroid/support/v7/widget/co;Z)I

    .line 508
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v1, v1, Landroid/support/v7/widget/au;->b:I

    .line 509
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v2, v2, Landroid/support/v7/widget/au;->c:I

    if-lez v2, :cond_4

    .line 510
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v2, v2, Landroid/support/v7/widget/au;->c:I

    add-int/2addr v0, v2

    .line 513
    :cond_4
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    invoke-direct {p0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/as;)V

    .line 514
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v0, v2, Landroid/support/v7/widget/au;->h:I

    .line 515
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v2, v0, Landroid/support/v7/widget/au;->d:I

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v3, v3, Landroid/support/v7/widget/au;->e:I

    add-int/2addr v2, v3

    iput v2, v0, Landroid/support/v7/widget/au;->d:I

    .line 516
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;Landroid/support/v7/widget/co;Z)I

    .line 517
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v0, v0, Landroid/support/v7/widget/au;->b:I

    .line 538
    :goto_9
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v2

    if-lez v2, :cond_32

    .line 542
    iget-boolean v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    iget-boolean v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->d:Z

    xor-int/2addr v2, v3

    if-eqz v2, :cond_28

    .line 543
    const/4 v2, 0x1

    invoke-direct {p0, v0, p1, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)I

    move-result v2

    .line 544
    add-int/2addr v1, v2

    .line 545
    add-int/2addr v0, v2

    .line 546
    const/4 v2, 0x0

    invoke-direct {p0, v1, p1, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->b(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)I

    move-result v2

    .line 547
    add-int/2addr v1, v2

    .line 548
    add-int/2addr v0, v2

    move v2, v1

    move v1, v0

    .line 558
    :goto_a
    iget-boolean v0, p2, Landroid/support/v7/widget/co;->k:Z

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p2, Landroid/support/v7/widget/co;->i:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->c()Z

    move-result v0

    if-nez v0, :cond_29

    .line 559
    :cond_5
    :goto_b
    iget-boolean v0, p2, Landroid/support/v7/widget/co;->i:Z

    if-nez v0, :cond_6

    .line 560
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    .line 561
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    .line 562
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->e()I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/bk;->b:I

    .line 564
    :cond_6
    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->d:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->b:Z

    .line 565
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    .line 569
    return-void

    .line 453
    :cond_7
    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    if-ltz v0, :cond_8

    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    invoke-virtual {p2}, Landroid/support/v7/widget/co;->a()I

    move-result v1

    if-lt v0, v1, :cond_9

    :cond_8
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_9
    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    iput v0, v2, Landroid/support/v7/widget/as;->a:I

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget-boolean v0, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->c:Z

    iput-boolean v0, v2, Landroid/support/v7/widget/as;->c:Z

    iget-boolean v0, v2, Landroid/support/v7/widget/as;->c:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v1, v1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b:I

    sub-int/2addr v0, v1

    iput v0, v2, Landroid/support/v7/widget/as;->b:I

    :goto_c
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget v1, v1, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b:I

    add-int/2addr v0, v1

    iput v0, v2, Landroid/support/v7/widget/as;->b:I

    goto :goto_c

    :cond_b
    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_14

    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->b(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v3}, Landroid/support/v7/widget/bk;->e()I

    move-result v3

    if-le v1, v3, :cond_c

    invoke-virtual {v2}, Landroid/support/v7/widget/as;->a()V

    :goto_d
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_c
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v3}, Landroid/support/v7/widget/bk;->b()I

    move-result v3

    sub-int/2addr v1, v3

    if-gez v1, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v0

    iput v0, v2, Landroid/support/v7/widget/as;->b:I

    const/4 v0, 0x0

    iput-boolean v0, v2, Landroid/support/v7/widget/as;->c:Z

    goto :goto_d

    :cond_d
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->c()I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v1, v3

    if-gez v1, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    iput v0, v2, Landroid/support/v7/widget/as;->b:I

    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/v7/widget/as;->c:Z

    goto :goto_d

    :cond_e
    iget-boolean v1, v2, Landroid/support/v7/widget/as;->c:Z

    if-eqz v1, :cond_f

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->a()I

    move-result v1

    add-int/2addr v0, v1

    :goto_e
    iput v0, v2, Landroid/support/v7/widget/as;->b:I

    :goto_f
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_f
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_e

    :cond_10
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v0

    if-lez v0, :cond_11

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->l:I

    if-ge v1, v0, :cond_12

    const/4 v0, 0x1

    :goto_10
    iget-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-ne v0, v1, :cond_13

    const/4 v0, 0x1

    :goto_11
    iput-boolean v0, v2, Landroid/support/v7/widget/as;->c:Z

    :cond_11
    invoke-virtual {v2}, Landroid/support/v7/widget/as;->a()V

    goto :goto_f

    :cond_12
    const/4 v0, 0x0

    goto :goto_10

    :cond_13
    const/4 v0, 0x0

    goto :goto_11

    :cond_14
    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    iput-boolean v0, v2, Landroid/support/v7/widget/as;->c:Z

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    sub-int/2addr v0, v1

    iput v0, v2, Landroid/support/v7/widget/as;->b:I

    goto/16 :goto_d

    :cond_15
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    add-int/2addr v0, v1

    iput v0, v2, Landroid/support/v7/widget/as;->b:I

    goto/16 :goto_d

    :cond_16
    iget-object v0, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_17

    iget-object v1, p0, Landroid/support/v7/widget/cd;->p:Landroid/support/v7/widget/x;

    iget-object v1, v1, Landroid/support/v7/widget/x;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    :cond_17
    const/4 v0, 0x0

    move-object v1, v0

    goto/16 :goto_1

    :cond_18
    move-object v1, v0

    goto/16 :goto_1

    :cond_19
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_1a
    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->b:Z

    iget-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->d:Z

    if-ne v0, v1, :cond_20

    iget-boolean v0, v2, Landroid/support/v7/widget/as;->c:Z

    if-eqz v0, :cond_1d

    invoke-direct {p0, p2}, Landroid/support/v7/widget/LinearLayoutManager;->k(Landroid/support/v7/widget/co;)Landroid/view/View;

    move-result-object v0

    :goto_12
    if-eqz v0, :cond_20

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/as;->a(Landroid/view/View;)V

    iget-boolean v1, p2, Landroid/support/v7/widget/co;->i:Z

    if-nez v1, :cond_1c

    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->c()Z

    move-result v1

    if-eqz v1, :cond_1c

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v1

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v3}, Landroid/support/v7/widget/bk;->c()I

    move-result v3

    if-ge v1, v3, :cond_1b

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->b()I

    move-result v1

    if-ge v0, v1, :cond_1e

    :cond_1b
    const/4 v0, 0x1

    :goto_13
    if-eqz v0, :cond_1c

    iget-boolean v0, v2, Landroid/support/v7/widget/as;->c:Z

    if-eqz v0, :cond_1f

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    :goto_14
    iput v0, v2, Landroid/support/v7/widget/as;->b:I

    :cond_1c
    const/4 v0, 0x1

    goto/16 :goto_3

    :cond_1d
    invoke-direct {p0, p2}, Landroid/support/v7/widget/LinearLayoutManager;->l(Landroid/support/v7/widget/co;)Landroid/view/View;

    move-result-object v0

    goto :goto_12

    :cond_1e
    const/4 v0, 0x0

    goto :goto_13

    :cond_1f
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v0

    goto :goto_14

    :cond_20
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_21
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 463
    :cond_22
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 469
    :cond_23
    const/4 v0, 0x0

    goto/16 :goto_6

    .line 487
    :cond_24
    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v3, v2}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v3}, Landroid/support/v7/widget/bk;->b()I

    move-result v3

    sub-int/2addr v2, v3

    .line 489
    iget v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->m:I

    sub-int v2, v3, v2

    goto/16 :goto_7

    .line 494
    :cond_25
    sub-int/2addr v0, v2

    goto/16 :goto_8

    .line 520
    :cond_26
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    invoke-direct {p0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/as;)V

    .line 521
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v0, v2, Landroid/support/v7/widget/au;->h:I

    .line 522
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;Landroid/support/v7/widget/co;Z)I

    .line 523
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v0, v0, Landroid/support/v7/widget/au;->b:I

    .line 524
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v2, v2, Landroid/support/v7/widget/au;->c:I

    if-lez v2, :cond_27

    .line 525
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v2, v2, Landroid/support/v7/widget/au;->c:I

    add-int/2addr v1, v2

    .line 528
    :cond_27
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->o:Landroid/support/v7/widget/as;

    invoke-direct {p0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->b(Landroid/support/v7/widget/as;)V

    .line 529
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v1, v2, Landroid/support/v7/widget/au;->h:I

    .line 530
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v2, v1, Landroid/support/v7/widget/au;->d:I

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v3, v3, Landroid/support/v7/widget/au;->e:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/support/v7/widget/au;->d:I

    .line 531
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;Landroid/support/v7/widget/co;Z)I

    .line 532
    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v1, v1, Landroid/support/v7/widget/au;->b:I

    goto/16 :goto_9

    .line 550
    :cond_28
    const/4 v2, 0x1

    invoke-direct {p0, v1, p1, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->b(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)I

    move-result v2

    .line 551
    add-int/2addr v1, v2

    .line 552
    add-int/2addr v0, v2

    .line 553
    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)I

    move-result v2

    .line 554
    add-int/2addr v1, v2

    .line 555
    add-int/2addr v0, v2

    move v2, v1

    move v1, v0

    goto/16 :goto_a

    .line 558
    :cond_29
    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v7, p1, Landroid/support/v7/widget/ci;->d:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/LinearLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v9

    const/4 v0, 0x0

    move v6, v0

    :goto_15
    if-ge v6, v8, :cond_2d

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cr;

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->c()I

    move-result v3

    if-ge v3, v9, :cond_2a

    const/4 v3, 0x1

    :goto_16
    iget-boolean v10, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eq v3, v10, :cond_2b

    const/4 v3, -0x1

    :goto_17
    const/4 v10, -0x1

    if-ne v3, v10, :cond_2c

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    iget-object v0, v0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v5

    move v3, v0

    move v0, v4

    :goto_18
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v5, v3

    move v4, v0

    goto :goto_15

    :cond_2a
    const/4 v3, 0x0

    goto :goto_16

    :cond_2b
    const/4 v3, 0x1

    goto :goto_17

    :cond_2c
    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    iget-object v0, v0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v4

    move v3, v5

    goto :goto_18

    :cond_2d
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput-object v7, v0, Landroid/support/v7/widget/au;->j:Ljava/util/List;

    if-lez v5, :cond_2e

    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->u()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0, v2}, Landroid/support/v7/widget/LinearLayoutManager;->d(II)V

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v5, v0, Landroid/support/v7/widget/au;->h:I

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v2, 0x0

    iput v2, v0, Landroid/support/v7/widget/au;->c:I

    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v3, v2, Landroid/support/v7/widget/au;->d:I

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_30

    const/4 v0, 0x1

    :goto_19
    add-int/2addr v0, v3

    iput v0, v2, Landroid/support/v7/widget/au;->d:I

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, p2, v2}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;Landroid/support/v7/widget/co;Z)I

    :cond_2e
    if-lez v4, :cond_2f

    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->v()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;->c(II)V

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iput v4, v0, Landroid/support/v7/widget/au;->h:I

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v1, 0x0

    iput v1, v0, Landroid/support/v7/widget/au;->c:I

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    iget v2, v1, Landroid/support/v7/widget/au;->d:I

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    if-eqz v0, :cond_31

    const/4 v0, -0x1

    :goto_1a
    add-int/2addr v0, v2

    iput v0, v1, Landroid/support/v7/widget/au;->d:I

    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/au;Landroid/support/v7/widget/co;Z)I

    :cond_2f
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->a:Landroid/support/v7/widget/au;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/au;->j:Ljava/util/List;

    goto/16 :goto_b

    :cond_30
    const/4 v0, -0x1

    goto :goto_19

    :cond_31
    const/4 v0, 0x1

    goto :goto_1a

    :cond_32
    move v2, v1

    move v1, v0

    goto/16 :goto_a
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 1704
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->b:Z

    iget-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->d:Z

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 976
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->i(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final d()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 215
    iget-object v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 216
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    iget-object v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->n:Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;-><init>(Landroid/support/v7/widget/LinearLayoutManager$SavedState;)V

    .line 236
    :goto_0
    return-object v0

    .line 218
    :cond_0
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;

    invoke-direct {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;-><init>()V

    .line 219
    invoke-virtual {p0}, Landroid/support/v7/widget/LinearLayoutManager;->j()I

    move-result v1

    if-lez v1, :cond_2

    .line 220
    iget-boolean v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->b:Z

    iget-boolean v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->k:Z

    xor-int/2addr v1, v2

    .line 221
    iput-boolean v1, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->c:Z

    .line 222
    if-eqz v1, :cond_1

    .line 223
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->v()Landroid/view/View;

    move-result-object v1

    .line 224
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->c()I

    move-result v2

    iget-object v3, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b:I

    .line 226
    invoke-static {v1}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a:I

    goto :goto_0

    .line 228
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/LinearLayoutManager;->u()Landroid/view/View;

    move-result-object v1

    .line 229
    invoke-static {v1}, Landroid/support/v7/widget/LinearLayoutManager;->a(Landroid/view/View;)I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->a:I

    .line 230
    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/widget/LinearLayoutManager;->j:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b:I

    goto :goto_0

    .line 234
    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager$SavedState;->b()V

    goto :goto_0
.end method

.method public final e(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 981
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->j(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 986
    invoke-direct {p0, p1}, Landroid/support/v7/widget/LinearLayoutManager;->j(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 265
    iget v1, p0, Landroid/support/v7/widget/LinearLayoutManager;->i:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 869
    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
