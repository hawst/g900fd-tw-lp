.class Landroid/support/v7/widget/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/u;


# instance fields
.field final a:Landroid/graphics/RectF;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/s;->a:Landroid/graphics/RectF;

    return-void
.end method

.method private static e(Landroid/support/v7/widget/r;)Landroid/support/v7/widget/cv;
    .locals 1

    .prologue
    .line 148
    invoke-interface {p0}, Landroid/support/v7/widget/r;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cv;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/r;)F
    .locals 1

    .prologue
    .line 134
    invoke-static {p1}, Landroid/support/v7/widget/s;->e(Landroid/support/v7/widget/r;)Landroid/support/v7/widget/cv;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/cv;->b()F

    move-result v0

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Landroid/support/v7/widget/t;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/t;-><init>(Landroid/support/v7/widget/s;)V

    sput-object v0, Landroid/support/v7/widget/cv;->c:Landroid/support/v7/widget/cw;

    .line 66
    return-void
.end method

.method public final a(Landroid/support/v7/widget/r;Landroid/content/Context;IFFF)V
    .locals 6

    .prologue
    .line 71
    new-instance v0, Landroid/support/v7/widget/cv;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/cv;-><init>(Landroid/content/res/Resources;IFFF)V

    .line 73
    invoke-interface {p1}, Landroid/support/v7/widget/r;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cv;->a(Z)V

    .line 74
    invoke-interface {p1, v0}, Landroid/support/v7/widget/r;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 75
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-static {p1}, Landroid/support/v7/widget/s;->e(Landroid/support/v7/widget/r;)Landroid/support/v7/widget/cv;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cv;->a(Landroid/graphics/Rect;)V

    move-object v0, p1

    check-cast v0, Landroid/view/View;

    invoke-static {p1}, Landroid/support/v7/widget/s;->e(Landroid/support/v7/widget/r;)Landroid/support/v7/widget/cv;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/cv;->d()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumHeight(I)V

    move-object v0, p1

    check-cast v0, Landroid/view/View;

    invoke-static {p1}, Landroid/support/v7/widget/s;->e(Landroid/support/v7/widget/r;)Landroid/support/v7/widget/cv;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/cv;->c()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumWidth(I)V

    iget v0, v1, Landroid/graphics/Rect;->left:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, v1, Landroid/graphics/Rect;->right:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-interface {p1, v0, v2, v3, v1}, Landroid/support/v7/widget/r;->a_(IIII)V

    .line 76
    return-void
.end method

.method public final b(Landroid/support/v7/widget/r;)F
    .locals 1

    .prologue
    .line 139
    invoke-static {p1}, Landroid/support/v7/widget/s;->e(Landroid/support/v7/widget/r;)Landroid/support/v7/widget/cv;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/cv;->c()F

    move-result v0

    return v0
.end method

.method public final c(Landroid/support/v7/widget/r;)F
    .locals 1

    .prologue
    .line 144
    invoke-static {p1}, Landroid/support/v7/widget/s;->e(Landroid/support/v7/widget/r;)Landroid/support/v7/widget/cv;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/cv;->d()F

    move-result v0

    return v0
.end method

.method public final d(Landroid/support/v7/widget/r;)F
    .locals 1

    .prologue
    .line 113
    invoke-static {p1}, Landroid/support/v7/widget/s;->e(Landroid/support/v7/widget/r;)Landroid/support/v7/widget/cv;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/cv;->a()F

    move-result v0

    return v0
.end method
