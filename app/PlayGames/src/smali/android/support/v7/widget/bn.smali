.class public final Landroid/support/v7/widget/bn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/j;
.implements Landroid/support/v7/internal/view/menu/y;


# instance fields
.field public a:Landroid/support/v7/internal/view/menu/v;

.field public b:Landroid/support/v7/widget/bp;

.field public c:Landroid/support/v7/widget/bo;

.field private d:Landroid/content/Context;

.field private e:Landroid/support/v7/internal/view/menu/i;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/bn;-><init>(Landroid/content/Context;Landroid/view/View;B)V

    .line 70
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/View;B)V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Landroid/support/v7/widget/bn;->d:Landroid/content/Context;

    .line 83
    new-instance v0, Landroid/support/v7/internal/view/menu/i;

    invoke-direct {v0, p1}, Landroid/support/v7/internal/view/menu/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/bn;->e:Landroid/support/v7/internal/view/menu/i;

    .line 84
    iget-object v0, p0, Landroid/support/v7/widget/bn;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/i;->a(Landroid/support/v7/internal/view/menu/j;)V

    .line 85
    iput-object p2, p0, Landroid/support/v7/widget/bn;->f:Landroid/view/View;

    .line 86
    new-instance v0, Landroid/support/v7/internal/view/menu/v;

    iget-object v1, p0, Landroid/support/v7/widget/bn;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-direct {v0, p1, v1, p2}, Landroid/support/v7/internal/view/menu/v;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;)V

    iput-object v0, p0, Landroid/support/v7/widget/bn;->a:Landroid/support/v7/internal/view/menu/v;

    .line 87
    iget-object v0, p0, Landroid/support/v7/widget/bn;->a:Landroid/support/v7/internal/view/menu/v;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/v;->a(I)V

    .line 88
    iget-object v0, p0, Landroid/support/v7/widget/bn;->a:Landroid/support/v7/internal/view/menu/v;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/v;->a(Landroid/support/v7/internal/view/menu/y;)V

    .line 89
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 160
    new-instance v0, Landroid/support/v7/internal/view/e;

    iget-object v1, p0, Landroid/support/v7/widget/bn;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/internal/view/e;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Landroid/support/v7/widget/bn;->e:Landroid/support/v7/internal/view/menu/i;

    invoke-virtual {v0, p1, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 161
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;)V
    .locals 0

    .prologue
    .line 241
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Z)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Landroid/support/v7/widget/bn;->c:Landroid/support/v7/widget/bo;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Landroid/support/v7/widget/bn;->c:Landroid/support/v7/widget/bo;

    invoke-interface {v0}, Landroid/support/v7/widget/bo;->a()V

    .line 214
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/i;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Landroid/support/v7/widget/bn;->b:Landroid/support/v7/widget/bp;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Landroid/support/v7/widget/bn;->b:Landroid/support/v7/widget/bp;

    invoke-interface {v0, p2}, Landroid/support/v7/widget/bp;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 204
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(Landroid/support/v7/internal/view/menu/i;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 220
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 228
    :cond_0
    :goto_0
    return v0

    .line 222
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/i;->hasVisibleItems()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 227
    new-instance v1, Landroid/support/v7/internal/view/menu/v;

    iget-object v2, p0, Landroid/support/v7/widget/bn;->d:Landroid/content/Context;

    iget-object v3, p0, Landroid/support/v7/widget/bn;->f:Landroid/view/View;

    invoke-direct {v1, v2, p1, v3}, Landroid/support/v7/internal/view/menu/v;-><init>(Landroid/content/Context;Landroid/support/v7/internal/view/menu/i;Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/v;->b()V

    goto :goto_0
.end method
