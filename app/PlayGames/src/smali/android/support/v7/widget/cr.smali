.class public abstract Landroid/support/v7/widget/cr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/view/View;

.field b:I

.field c:I

.field d:J

.field e:I

.field f:I

.field g:Landroid/support/v7/widget/cr;

.field h:Landroid/support/v7/widget/cr;

.field i:I

.field j:Landroid/support/v7/widget/ci;

.field private k:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 6671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6600
    iput v2, p0, Landroid/support/v7/widget/cr;->b:I

    .line 6601
    iput v2, p0, Landroid/support/v7/widget/cr;->c:I

    .line 6602
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v7/widget/cr;->d:J

    .line 6603
    iput v2, p0, Landroid/support/v7/widget/cr;->e:I

    .line 6604
    iput v2, p0, Landroid/support/v7/widget/cr;->f:I

    .line 6607
    iput-object v3, p0, Landroid/support/v7/widget/cr;->g:Landroid/support/v7/widget/cr;

    .line 6609
    iput-object v3, p0, Landroid/support/v7/widget/cr;->h:Landroid/support/v7/widget/cr;

    .line 6665
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/cr;->k:I

    .line 6669
    iput-object v3, p0, Landroid/support/v7/widget/cr;->j:Landroid/support/v7/widget/ci;

    .line 6672
    if-nez p1, :cond_0

    .line 6673
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "itemView may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 6675
    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    .line 6676
    return-void
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6701
    iput v0, p0, Landroid/support/v7/widget/cr;->c:I

    .line 6702
    iput v0, p0, Landroid/support/v7/widget/cr;->f:I

    .line 6703
    return-void
.end method

.method final a(I)V
    .locals 1

    .prologue
    .line 6800
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    or-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/cr;->i:I

    .line 6801
    return-void
.end method

.method final a(II)V
    .locals 2

    .prologue
    .line 6796
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    xor-int/lit8 v1, p2, -0x1

    and-int/2addr v0, v1

    and-int v1, p1, p2

    or-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/cr;->i:I

    .line 6797
    return-void
.end method

.method final a(IZ)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 6685
    iget v0, p0, Landroid/support/v7/widget/cr;->c:I

    if-ne v0, v1, :cond_0

    .line 6686
    iget v0, p0, Landroid/support/v7/widget/cr;->b:I

    iput v0, p0, Landroid/support/v7/widget/cr;->c:I

    .line 6688
    :cond_0
    iget v0, p0, Landroid/support/v7/widget/cr;->f:I

    if-ne v0, v1, :cond_1

    .line 6689
    iget v0, p0, Landroid/support/v7/widget/cr;->b:I

    iput v0, p0, Landroid/support/v7/widget/cr;->f:I

    .line 6691
    :cond_1
    if-eqz p2, :cond_2

    .line 6692
    iget v0, p0, Landroid/support/v7/widget/cr;->f:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/cr;->f:I

    .line 6694
    :cond_2
    iget v0, p0, Landroid/support/v7/widget/cr;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/cr;->b:I

    .line 6695
    iget-object v0, p0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 6696
    iget-object v0, p0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ce;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/ce;->c:Z

    .line 6698
    :cond_3
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 6844
    if-eqz p1, :cond_1

    iget v0, p0, Landroid/support/v7/widget/cr;->k:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iput v0, p0, Landroid/support/v7/widget/cr;->k:I

    .line 6845
    iget v0, p0, Landroid/support/v7/widget/cr;->k:I

    if-gez v0, :cond_2

    .line 6846
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/cr;->k:I

    .line 6851
    const-string v0, "View"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isRecyclable decremented below 0: unmatched pair of setIsRecyable() calls for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 6861
    :cond_0
    :goto_1
    return-void

    .line 6844
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/cr;->k:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6853
    :cond_2
    if-nez p1, :cond_3

    iget v0, p0, Landroid/support/v7/widget/cr;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 6854
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Landroid/support/v7/widget/cr;->i:I

    goto :goto_1

    .line 6855
    :cond_3
    if-eqz p1, :cond_0

    iget v0, p0, Landroid/support/v7/widget/cr;->k:I

    if-nez v0, :cond_0

    .line 6856
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Landroid/support/v7/widget/cr;->i:I

    goto :goto_1
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 6712
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 6716
    iget v0, p0, Landroid/support/v7/widget/cr;->f:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/support/v7/widget/cr;->b:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v7/widget/cr;->f:I

    goto :goto_0
.end method

.method final d()Z
    .locals 1

    .prologue
    .line 6752
    iget-object v0, p0, Landroid/support/v7/widget/cr;->j:Landroid/support/v7/widget/ci;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()V
    .locals 1

    .prologue
    .line 6756
    iget-object v0, p0, Landroid/support/v7/widget/cr;->j:Landroid/support/v7/widget/ci;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ci;->b(Landroid/support/v7/widget/cr;)V

    .line 6757
    return-void
.end method

.method final f()Z
    .locals 1

    .prologue
    .line 6760
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final g()V
    .locals 1

    .prologue
    .line 6764
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Landroid/support/v7/widget/cr;->i:I

    .line 6765
    return-void
.end method

.method final h()Z
    .locals 1

    .prologue
    .line 6776
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final i()Z
    .locals 1

    .prologue
    .line 6780
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final j()Z
    .locals 1

    .prologue
    .line 6784
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final k()Z
    .locals 1

    .prologue
    .line 6788
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final l()Z
    .locals 1

    .prologue
    .line 6792
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final m()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 6804
    iput v3, p0, Landroid/support/v7/widget/cr;->i:I

    .line 6805
    iput v2, p0, Landroid/support/v7/widget/cr;->b:I

    .line 6806
    iput v2, p0, Landroid/support/v7/widget/cr;->c:I

    .line 6807
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v7/widget/cr;->d:J

    .line 6808
    iput v2, p0, Landroid/support/v7/widget/cr;->f:I

    .line 6809
    iput v3, p0, Landroid/support/v7/widget/cr;->k:I

    .line 6810
    iput-object v4, p0, Landroid/support/v7/widget/cr;->g:Landroid/support/v7/widget/cr;

    .line 6811
    iput-object v4, p0, Landroid/support/v7/widget/cr;->h:Landroid/support/v7/widget/cr;

    .line 6812
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 6869
    iget v0, p0, Landroid/support/v7/widget/cr;->i:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/at;->c(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 6816
    new-instance v0, Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ViewHolder{"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/widget/cr;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Landroid/support/v7/widget/cr;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", oldPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/widget/cr;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pLpos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/widget/cr;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 6819
    invoke-virtual {p0}, Landroid/support/v7/widget/cr;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, " scrap"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6820
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/cr;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, " invalid"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6821
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/cr;->k()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, " unbound"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6822
    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/widget/cr;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, " update"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6823
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/widget/cr;->l()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, " removed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6824
    :cond_4
    invoke-virtual {p0}, Landroid/support/v7/widget/cr;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, " ignored"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6825
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/widget/cr;->j()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, " changed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6826
    :cond_6
    invoke-virtual {p0}, Landroid/support/v7/widget/cr;->n()Z

    move-result v1

    if-nez v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " not recyclable("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroid/support/v7/widget/cr;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6827
    :cond_7
    iget-object v1, p0, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_8

    const-string v1, " no parent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6828
    :cond_8
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6829
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
