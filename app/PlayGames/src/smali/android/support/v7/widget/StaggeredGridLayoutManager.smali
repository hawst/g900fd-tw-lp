.class public final Landroid/support/v7/widget/StaggeredGridLayoutManager;
.super Landroid/support/v7/widget/cd;
.source "SourceFile"


# instance fields
.field private A:Z

.field private final B:Ljava/lang/Runnable;

.field a:Landroid/support/v7/widget/bk;

.field b:Landroid/support/v7/widget/bk;

.field c:Z

.field d:I

.field e:I

.field f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

.field private g:I

.field private h:[Landroid/support/v7/widget/ds;

.field private i:I

.field private j:I

.field private k:Landroid/support/v7/widget/aq;

.field private l:Z

.field private m:Ljava/util/BitSet;

.field private n:I

.field private o:Z

.field private t:Z

.field private u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

.field private v:I

.field private w:I

.field private x:I

.field private final y:Landroid/support/v7/widget/do;

.field private z:Z


# direct methods
.method private a(I)I
    .locals 3

    .prologue
    .line 1532
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ds;->a(I)I

    move-result v1

    .line 1533
    const/4 v0, 0x1

    :goto_0
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v0, v2, :cond_1

    .line 1534
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/ds;->a(I)I

    move-result v2

    .line 1535
    if-ge v2, v1, :cond_0

    move v1, v2

    .line 1533
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1539
    :cond_1
    return v1
.end method

.method private static a(III)I
    .locals 2

    .prologue
    .line 991
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 999
    :cond_0
    :goto_0
    return p0

    .line 994
    :cond_1
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 995
    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_2

    const/high16 v1, 0x40000000    # 2.0f

    if-ne v0, v1, :cond_0

    .line 996
    :cond_2
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v1, p1

    sub-int/2addr v1, p2

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p0

    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/aq;Landroid/support/v7/widget/co;)I
    .locals 17

    .prologue
    .line 1304
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->m:Ljava/util/BitSet;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/BitSet;->set(IIZ)V

    .line 1314
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/aq;->d:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 1316
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->c()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v3, v3, Landroid/support/v7/widget/aq;->a:I

    add-int/2addr v2, v3

    .line 1317
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v3, v3, Landroid/support/v7/widget/aq;->e:I

    add-int/2addr v3, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v4}, Landroid/support/v7/widget/bk;->f()I

    move-result v4

    add-int/2addr v3, v4

    move v4, v3

    move v3, v2

    .line 1325
    :goto_0
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/aq;->d:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c(II)V

    .line 1328
    move-object/from16 v0, p0

    iget-boolean v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->c()I

    move-result v2

    move v5, v2

    .line 1332
    :cond_0
    :goto_1
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/aq;->b:I

    if-ltz v2, :cond_5

    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/aq;->b:I

    invoke-virtual/range {p3 .. p3}, Landroid/support/v7/widget/co;->a()I

    move-result v6

    if-ge v2, v6, :cond_5

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->m:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2c

    .line 1333
    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/aq;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/ci;->b(I)Landroid/view/View;

    move-result-object v14

    move-object/from16 v0, p2

    iget v2, v0, Landroid/support/v7/widget/aq;->b:I

    move-object/from16 v0, p2

    iget v6, v0, Landroid/support/v7/widget/aq;->c:I

    add-int/2addr v2, v6

    move-object/from16 v0, p2

    iput v2, v0, Landroid/support/v7/widget/aq;->b:I

    .line 1334
    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/dp;

    .line 1335
    move-object/from16 v0, p2

    iget v6, v0, Landroid/support/v7/widget/aq;->d:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_6

    .line 1336
    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-super {v0, v14, v6, v7}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;IZ)V

    .line 1340
    :goto_3
    iget-boolean v6, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v6, :cond_8

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_7

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v:I

    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->x:I

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v6, v7}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;II)V

    .line 1342
    :goto_4
    invoke-virtual {v2}, Landroid/support/v7/widget/dp;->f()I

    move-result v15

    .line 1343
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    iget-object v7, v6, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a:[I

    if-eqz v7, :cond_1

    iget-object v7, v6, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a:[I

    array-length v7, v7

    if-lt v15, v7, :cond_9

    :cond_1
    const/4 v6, -0x1

    move v7, v6

    .line 1345
    :goto_5
    const/4 v6, -0x1

    if-ne v7, v6, :cond_a

    const/4 v6, 0x1

    move v13, v6

    .line 1346
    :goto_6
    if-eqz v13, :cond_14

    .line 1347
    iget-boolean v6, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v6, :cond_b

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    const/4 v7, 0x0

    aget-object v11, v6, v7

    .line 1348
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v6, v15}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->c(I)V

    iget-object v6, v6, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a:[I

    iget v7, v11, Landroid/support/v7/widget/ds;->e:I

    aput v7, v6, v15

    .line 1361
    :goto_7
    move-object/from16 v0, p2

    iget v6, v0, Landroid/support/v7/widget/aq;->d:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_18

    .line 1362
    iget-boolean v6, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v6, :cond_15

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i(I)I

    move-result v6

    .line 1364
    :goto_8
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v7, v14}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v7

    add-int v8, v6, v7

    .line 1365
    if-eqz v13, :cond_2e

    iget-boolean v7, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v7, :cond_2e

    .line 1367
    new-instance v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    invoke-direct {v9}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;-><init>()V

    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    new-array v7, v7, [I

    iput-object v7, v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->c:[I

    const/4 v7, 0x0

    :goto_9
    move-object/from16 v0, p0

    iget v10, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v7, v10, :cond_16

    iget-object v10, v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->c:[I

    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v12, v12, v7

    invoke-virtual {v12, v6}, Landroid/support/v7/widget/ds;->b(I)I

    move-result v12

    sub-int v12, v6, v12

    aput v12, v10, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    .line 1321
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v3, v3, Landroid/support/v7/widget/aq;->a:I

    sub-int/2addr v2, v3

    .line 1322
    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v3, v3, Landroid/support/v7/widget/aq;->e:I

    sub-int v3, v2, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v4}, Landroid/support/v7/widget/bk;->b()I

    move-result v4

    sub-int/2addr v3, v4

    move v4, v3

    move v3, v2

    goto/16 :goto_0

    .line 1328
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    move v5, v2

    goto/16 :goto_1

    .line 1332
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1338
    :cond_6
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-super {v0, v14, v6, v7}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;IZ)V

    goto/16 :goto_3

    .line 1340
    :cond_7
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->w:I

    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v:I

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v6, v7}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;II)V

    goto/16 :goto_4

    :cond_8
    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->w:I

    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->x:I

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v6, v7}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;II)V

    goto/16 :goto_4

    .line 1343
    :cond_9
    iget-object v6, v6, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a:[I

    aget v6, v6, v15

    move v7, v6

    goto/16 :goto_5

    .line 1345
    :cond_a
    const/4 v6, 0x0

    move v13, v6

    goto/16 :goto_6

    .line 1347
    :cond_b
    move-object/from16 v0, p2

    iget v6, v0, Landroid/support/v7/widget/aq;->d:I

    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    if-nez v7, :cond_e

    const/4 v7, -0x1

    if-ne v6, v7, :cond_c

    const/4 v6, 0x1

    :goto_a
    move-object/from16 v0, p0

    iget-boolean v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eq v6, v7, :cond_d

    const/4 v6, 0x1

    :goto_b
    if-eqz v6, :cond_12

    move-object/from16 v0, p0

    iget v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    add-int/lit8 v8, v6, -0x1

    const/4 v7, -0x1

    const/4 v6, -0x1

    :goto_c
    move-object/from16 v0, p2

    iget v9, v0, Landroid/support/v7/widget/aq;->d:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_13

    const/4 v11, 0x0

    const v9, 0x7fffffff

    move-object/from16 v0, p0

    iget-object v10, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v10}, Landroid/support/v7/widget/bk;->b()I

    move-result v16

    move v12, v8

    :goto_d
    if-eq v12, v7, :cond_2

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v10, v8, v12

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/support/v7/widget/ds;->b(I)I

    move-result v8

    if-ge v8, v9, :cond_30

    move-object v9, v10

    :goto_e
    add-int v10, v12, v6

    move v12, v10

    move-object v11, v9

    move v9, v8

    goto :goto_d

    :cond_c
    const/4 v6, 0x0

    goto :goto_a

    :cond_d
    const/4 v6, 0x0

    goto :goto_b

    :cond_e
    const/4 v7, -0x1

    if-ne v6, v7, :cond_f

    const/4 v6, 0x1

    :goto_f
    move-object/from16 v0, p0

    iget-boolean v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-ne v6, v7, :cond_10

    const/4 v6, 0x1

    :goto_10
    invoke-direct/range {p0 .. p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t()Z

    move-result v7

    if-ne v6, v7, :cond_11

    const/4 v6, 0x1

    goto :goto_b

    :cond_f
    const/4 v6, 0x0

    goto :goto_f

    :cond_10
    const/4 v6, 0x0

    goto :goto_10

    :cond_11
    const/4 v6, 0x0

    goto :goto_b

    :cond_12
    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    const/4 v6, 0x1

    goto :goto_c

    :cond_13
    const/4 v11, 0x0

    const/high16 v9, -0x80000000

    move-object/from16 v0, p0

    iget-object v10, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v10}, Landroid/support/v7/widget/bk;->c()I

    move-result v16

    move v12, v8

    :goto_11
    if-eq v12, v7, :cond_2

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v10, v8, v12

    move/from16 v0, v16

    invoke-virtual {v10, v0}, Landroid/support/v7/widget/ds;->a(I)I

    move-result v8

    if-le v8, v9, :cond_2f

    move-object v9, v10

    :goto_12
    add-int v10, v12, v6

    move v12, v10

    move-object v11, v9

    move v9, v8

    goto :goto_11

    .line 1356
    :cond_14
    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v11, v6, v7

    goto/16 :goto_7

    .line 1362
    :cond_15
    invoke-virtual {v11, v5}, Landroid/support/v7/widget/ds;->b(I)I

    move-result v6

    goto/16 :goto_8

    .line 1368
    :cond_16
    const/4 v7, -0x1

    iput v7, v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->b:I

    .line 1369
    iput v15, v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    .line 1370
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v7, v9}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a(Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;)V

    move v7, v6

    move v6, v8

    .line 1386
    :goto_13
    iget-boolean v8, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v8, :cond_17

    move-object/from16 v0, p2

    iget v8, v0, Landroid/support/v7/widget/aq;->c:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_17

    if-eqz v13, :cond_17

    .line 1387
    const/4 v8, 0x1

    move-object/from16 v0, p0

    iput-boolean v8, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    .line 1390
    :cond_17
    iput-object v11, v2, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    .line 1391
    move-object/from16 v0, p2

    iget v8, v0, Landroid/support/v7/widget/aq;->d:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1f

    iget-boolean v8, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v8, :cond_1c

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    add-int/lit8 v8, v8, -0x1

    :goto_14
    if-ltz v8, :cond_1d

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v9, v9, v8

    invoke-virtual {v9, v14}, Landroid/support/v7/widget/ds;->b(Landroid/view/View;)V

    add-int/lit8 v8, v8, -0x1

    goto :goto_14

    .line 1373
    :cond_18
    iget-boolean v6, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v6, :cond_19

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(I)I

    move-result v6

    .line 1375
    :goto_15
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v7, v14}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v7

    sub-int v8, v6, v7

    .line 1376
    if-eqz v13, :cond_1b

    iget-boolean v7, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v7, :cond_1b

    .line 1378
    new-instance v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    invoke-direct {v9}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;-><init>()V

    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    new-array v7, v7, [I

    iput-object v7, v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->c:[I

    const/4 v7, 0x0

    :goto_16
    move-object/from16 v0, p0

    iget v10, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v7, v10, :cond_1a

    iget-object v10, v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->c:[I

    move-object/from16 v0, p0

    iget-object v12, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v12, v12, v7

    invoke-virtual {v12, v6}, Landroid/support/v7/widget/ds;->a(I)I

    move-result v12

    sub-int/2addr v12, v6

    aput v12, v10, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_16

    .line 1373
    :cond_19
    invoke-virtual {v11, v5}, Landroid/support/v7/widget/ds;->a(I)I

    move-result v6

    goto :goto_15

    .line 1379
    :cond_1a
    const/4 v7, 0x1

    iput v7, v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->b:I

    .line 1380
    iput v15, v9, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    .line 1381
    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v7, v9}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a(Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;)V

    :cond_1b
    move v7, v8

    goto/16 :goto_13

    .line 1391
    :cond_1c
    iget-object v8, v2, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    invoke-virtual {v8, v14}, Landroid/support/v7/widget/ds;->b(Landroid/view/View;)V

    .line 1392
    :cond_1d
    :goto_17
    iget-boolean v8, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v8, :cond_21

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bk;

    invoke-virtual {v8}, Landroid/support/v7/widget/bk;->b()I

    move-result v8

    .line 1395
    :goto_18
    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bk;

    invoke-virtual {v9, v14}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v9

    add-int/2addr v9, v8

    .line 1396
    move-object/from16 v0, p0

    iget v10, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    const/4 v12, 0x1

    if-ne v10, v12, :cond_22

    .line 1397
    invoke-static {v14, v8, v7, v9, v6}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;IIII)V

    .line 1402
    :goto_19
    iget-boolean v2, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v2, :cond_23

    .line 1403
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v2, v2, Landroid/support/v7/widget/aq;->d:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c(II)V

    .line 1407
    :goto_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v2, v2, Landroid/support/v7/widget/aq;->d:I

    const/4 v6, -0x1

    if-ne v2, v6, :cond_27

    invoke-virtual {v11}, Landroid/support/v7/widget/ds;->a()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    const/4 v6, 0x0

    aget-object v2, v2, v6

    invoke-virtual {v2, v8}, Landroid/support/v7/widget/ds;->a(I)I

    move-result v6

    const/4 v2, 0x1

    :goto_1b
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v2, v7, :cond_24

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v7, v7, v2

    invoke-virtual {v7, v8}, Landroid/support/v7/widget/ds;->a(I)I

    move-result v7

    if-le v7, v6, :cond_1e

    move v6, v7

    :cond_1e
    add-int/lit8 v2, v2, 0x1

    goto :goto_1b

    .line 1391
    :cond_1f
    iget-boolean v8, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v8, :cond_20

    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    add-int/lit8 v8, v8, -0x1

    :goto_1c
    if-ltz v8, :cond_1d

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v9, v9, v8

    invoke-virtual {v9, v14}, Landroid/support/v7/widget/ds;->a(Landroid/view/View;)V

    add-int/lit8 v8, v8, -0x1

    goto :goto_1c

    :cond_20
    iget-object v8, v2, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    invoke-virtual {v8, v14}, Landroid/support/v7/widget/ds;->a(Landroid/view/View;)V

    goto :goto_17

    .line 1392
    :cond_21
    iget v8, v11, Landroid/support/v7/widget/ds;->e:I

    move-object/from16 v0, p0

    iget v9, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j:I

    mul-int/2addr v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bk;

    invoke-virtual {v9}, Landroid/support/v7/widget/bk;->b()I

    move-result v9

    add-int/2addr v8, v9

    goto :goto_18

    .line 1399
    :cond_22
    invoke-static {v14, v7, v8, v6, v9}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/view/View;IIII)V

    goto :goto_19

    .line 1405
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v2, v2, Landroid/support/v7/widget/aq;->d:I

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v2, v4}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ds;II)V

    goto :goto_1a

    .line 1407
    :cond_24
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v6}, Landroid/support/v7/widget/bk;->d()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v7}, Landroid/support/v7/widget/bk;->b()I

    move-result v7

    sub-int/2addr v6, v7

    add-int v7, v2, v6

    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v6, v2

    :goto_1d
    if-ltz v6, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2, v8}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v2

    if-le v2, v7, :cond_0

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/dp;

    iget-boolean v9, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v9, :cond_25

    const/4 v2, 0x0

    :goto_1e
    move-object/from16 v0, p0

    iget v9, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v2, v9, :cond_26

    move-object/from16 v0, p0

    iget-object v9, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v9, v9, v2

    invoke-virtual {v9}, Landroid/support/v7/widget/ds;->d()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1e

    :cond_25
    iget-object v2, v2, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    invoke-virtual {v2}, Landroid/support/v7/widget/ds;->d()V

    :cond_26
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v8, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;Landroid/support/v7/widget/ci;)V

    add-int/lit8 v2, v6, -0x1

    move v6, v2

    goto :goto_1d

    :cond_27
    invoke-virtual {v11}, Landroid/support/v7/widget/ds;->b()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    const/4 v6, 0x0

    aget-object v2, v2, v6

    invoke-virtual {v2, v8}, Landroid/support/v7/widget/ds;->b(I)I

    move-result v6

    const/4 v2, 0x1

    :goto_1f
    move-object/from16 v0, p0

    iget v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v2, v7, :cond_29

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v7, v7, v2

    invoke-virtual {v7, v8}, Landroid/support/v7/widget/ds;->b(I)I

    move-result v7

    if-ge v7, v6, :cond_28

    move v6, v7

    :cond_28
    add-int/lit8 v2, v2, 0x1

    goto :goto_1f

    :cond_29
    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v6, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v6}, Landroid/support/v7/widget/bk;->d()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v7}, Landroid/support/v7/widget/bk;->b()I

    move-result v7

    sub-int/2addr v6, v7

    sub-int v6, v2, v6

    :goto_20
    invoke-virtual/range {p0 .. p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2, v7}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v2

    if-ge v2, v6, :cond_0

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/dp;

    iget-boolean v8, v2, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v8, :cond_2a

    const/4 v2, 0x0

    :goto_21
    move-object/from16 v0, p0

    iget v8, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v2, v8, :cond_2b

    move-object/from16 v0, p0

    iget-object v8, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v8, v8, v2

    invoke-virtual {v8}, Landroid/support/v7/widget/ds;->e()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_21

    :cond_2a
    iget-object v2, v2, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    invoke-virtual {v2}, Landroid/support/v7/widget/ds;->e()V

    :cond_2b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v7, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;Landroid/support/v7/widget/ci;)V

    goto :goto_20

    .line 1412
    :cond_2c
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v2, v2, Landroid/support/v7/widget/aq;->d:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_2d

    .line 1413
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(I)I

    move-result v2

    .line 1414
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v5, v5, Landroid/support/v7/widget/aq;->a:I

    sub-int v2, v3, v2

    add-int/2addr v2, v5

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1417
    :goto_22
    return v2

    .line 1416
    :cond_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->c()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i(I)I

    move-result v2

    .line 1417
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v5, v5, Landroid/support/v7/widget/aq;->a:I

    sub-int/2addr v2, v3

    add-int/2addr v2, v5

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_22

    :cond_2e
    move v7, v6

    move v6, v8

    goto/16 :goto_13

    :cond_2f
    move v8, v9

    move-object v9, v11

    goto/16 :goto_12

    :cond_30
    move v8, v9

    move-object v9, v11

    goto/16 :goto_e
.end method

.method private a(Z)Landroid/view/View;
    .locals 6

    .prologue
    .line 1136
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    .line 1137
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v3

    .line 1138
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v4

    .line 1139
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    .line 1140
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    .line 1141
    if-eqz p1, :cond_0

    iget-object v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v5

    if-lt v5, v2, :cond_1

    :cond_0
    iget-object v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v5, v0}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v5

    if-gt v5, v3, :cond_1

    .line 1146
    :goto_1
    return-object v0

    .line 1139
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1146
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(ILandroid/support/v7/widget/co;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 1195
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput v3, v2, Landroid/support/v7/widget/aq;->a:I

    .line 1196
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput p1, v2, Landroid/support/v7/widget/aq;->b:I

    .line 1197
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1198
    iget v2, p2, Landroid/support/v7/widget/co;->a:I

    .line 1199
    iget-boolean v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-ge v2, p1, :cond_0

    move v2, v0

    :goto_0
    if-eq v4, v2, :cond_1

    .line 1200
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget-object v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v3}, Landroid/support/v7/widget/bk;->e()I

    move-result v3

    iput v3, v2, Landroid/support/v7/widget/aq;->e:I

    .line 1207
    :goto_1
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput v1, v2, Landroid/support/v7/widget/aq;->d:I

    .line 1208
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget-boolean v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v3, :cond_2

    :goto_2
    iput v0, v2, Landroid/support/v7/widget/aq;->c:I

    .line 1210
    return-void

    :cond_0
    move v2, v3

    .line 1199
    goto :goto_0

    .line 1205
    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput v3, v2, Landroid/support/v7/widget/aq;->e:I

    goto :goto_1

    :cond_2
    move v0, v1

    .line 1208
    goto :goto_2
.end method

.method private a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)V
    .locals 2

    .prologue
    .line 1164
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i(I)I

    move-result v0

    .line 1165
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->c()I

    move-result v1

    sub-int v0, v1, v0

    .line 1167
    if-lez v0, :cond_0

    .line 1168
    neg-int v1, v0

    invoke-direct {p0, v1, p1, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v1

    neg-int v1, v1

    .line 1172
    sub-int/2addr v0, v1

    .line 1173
    if-eqz p3, :cond_0

    if-lez v0, :cond_0

    .line 1174
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bk;->a(I)V

    .line 1176
    :cond_0
    return-void
.end method

.method private a(Landroid/support/v7/widget/ds;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1506
    iget v0, p1, Landroid/support/v7/widget/ds;->d:I

    .line 1507
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    .line 1508
    invoke-virtual {p1}, Landroid/support/v7/widget/ds;->a()I

    move-result v1

    .line 1509
    add-int/2addr v0, v1

    if-ge v0, p3, :cond_0

    .line 1510
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->m:Ljava/util/BitSet;

    iget v1, p1, Landroid/support/v7/widget/ds;->e:I

    invoke-virtual {v0, v1, v2}, Ljava/util/BitSet;->set(IZ)V

    .line 1518
    :cond_0
    :goto_0
    return-void

    .line 1513
    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/widget/ds;->b()I

    move-result v1

    .line 1514
    sub-int v0, v1, v0

    if-le v0, p3, :cond_0

    .line 1515
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->m:Ljava/util/BitSet;

    iget v1, p1, Landroid/support/v7/widget/ds;->e:I

    invoke-virtual {v0, v1, v2}, Ljava/util/BitSet;->set(IZ)V

    goto :goto_0
.end method

.method private b(Z)Landroid/view/View;
    .locals 5

    .prologue
    .line 1150
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    .line 1151
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v3

    .line 1152
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1153
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    .line 1154
    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v4

    if-lt v4, v2, :cond_1

    if-eqz p1, :cond_0

    iget-object v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v4

    if-gt v4, v3, :cond_1

    .line 1159
    :cond_0
    :goto_1
    return-object v0

    .line 1152
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1159
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b()V
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    if-nez v0, :cond_0

    .line 493
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    invoke-static {p0, v0}, Landroid/support/v7/widget/bk;->a(Landroid/support/v7/widget/cd;I)Landroid/support/v7/widget/bk;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    .line 494
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    rsub-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Landroid/support/v7/widget/bk;->a(Landroid/support/v7/widget/cd;I)Landroid/support/v7/widget/bk;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bk;

    .line 496
    new-instance v0, Landroid/support/v7/widget/aq;

    invoke-direct {v0}, Landroid/support/v7/widget/aq;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    .line 498
    :cond_0
    return-void
.end method

.method private b(III)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1276
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u()I

    move-result v0

    .line 1277
    :goto_0
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->b(I)I

    .line 1278
    packed-switch p3, :pswitch_data_0

    .line 1292
    :goto_1
    :pswitch_0
    add-int v1, p1, p2

    if-gt v1, v0, :cond_2

    .line 1300
    :cond_0
    :goto_2
    return-void

    .line 1276
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v()I

    move-result v0

    goto :goto_0

    .line 1280
    :pswitch_1
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v1, p1, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->b(II)V

    goto :goto_1

    .line 1283
    :pswitch_2
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v1, p1, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a(II)V

    goto :goto_1

    .line 1287
    :pswitch_3
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v1, p1, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a(II)V

    .line 1288
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v1, p2, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->b(II)V

    goto :goto_1

    .line 1296
    :cond_2
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v()I

    move-result v0

    .line 1297
    :goto_3
    if-gt p1, v0, :cond_0

    .line 1298
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h()V

    goto :goto_2

    .line 1296
    :cond_3
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u()I

    move-result v0

    goto :goto_3

    .line 1278
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private b(ILandroid/support/v7/widget/co;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1213
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput v2, v1, Landroid/support/v7/widget/aq;->a:I

    .line 1214
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput p1, v1, Landroid/support/v7/widget/aq;->b:I

    .line 1215
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1216
    iget v1, p2, Landroid/support/v7/widget/co;->a:I

    .line 1217
    iget-boolean v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-le v1, p1, :cond_1

    move v1, v0

    :goto_0
    if-eq v3, v1, :cond_2

    .line 1218
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->e()I

    move-result v2

    iput v2, v1, Landroid/support/v7/widget/aq;->e:I

    .line 1225
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput v0, v1, Landroid/support/v7/widget/aq;->d:I

    .line 1226
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget-boolean v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v2, :cond_0

    const/4 v0, -0x1

    :cond_0
    iput v0, v1, Landroid/support/v7/widget/aq;->c:I

    .line 1228
    return-void

    :cond_1
    move v1, v2

    .line 1217
    goto :goto_0

    .line 1223
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput v2, v1, Landroid/support/v7/widget/aq;->e:I

    goto :goto_1
.end method

.method private b(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)V
    .locals 2

    .prologue
    .line 1180
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(I)I

    move-result v0

    .line 1181
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->b()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1183
    if-lez v0, :cond_0

    .line 1184
    invoke-direct {p0, v0, p1, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v1

    .line 1188
    sub-int/2addr v0, v1

    .line 1189
    if-eqz p3, :cond_0

    if-lez v0, :cond_0

    .line 1190
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    neg-int v0, v0

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/bk;->a(I)V

    .line 1192
    :cond_0
    return-void
.end method

.method private b(Landroid/view/View;II)V
    .locals 5

    .prologue
    .line 981
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v1

    .line 982
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dp;

    .line 983
    iget v2, v0, Landroid/support/v7/widget/dp;->leftMargin:I

    iget v3, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/support/v7/widget/dp;->rightMargin:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    invoke-static {p2, v2, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(III)I

    move-result v2

    .line 985
    iget v3, v0, Landroid/support/v7/widget/dp;->topMargin:I

    iget v4, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iget v0, v0, Landroid/support/v7/widget/dp;->bottomMargin:I

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    invoke-static {p3, v3, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(III)I

    move-result v0

    .line 987
    invoke-virtual {p1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 988
    return-void
.end method

.method private static b(Landroid/view/View;IIII)V
    .locals 4

    .prologue
    .line 1485
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dp;

    .line 1492
    iget v1, v0, Landroid/support/v7/widget/dp;->leftMargin:I

    add-int/2addr v1, p1

    iget v2, v0, Landroid/support/v7/widget/dp;->topMargin:I

    add-int/2addr v2, p2

    iget v3, v0, Landroid/support/v7/widget/dp;->rightMargin:I

    sub-int v3, p3, v3

    iget v0, v0, Landroid/support/v7/widget/dp;->bottomMargin:I

    sub-int v0, p4, v0

    invoke-static {p0, v1, v2, v3, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;IIII)V

    .line 1494
    return-void
.end method

.method private c(II)V
    .locals 2

    .prologue
    .line 1497
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v0, v1, :cond_1

    .line 1498
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v1, v1, v0

    iget-object v1, v1, Landroid/support/v7/widget/ds;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1499
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v1, v1, v0

    invoke-direct {p0, v1, p1, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ds;II)V

    .line 1497
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1503
    :cond_1
    return-void
.end method

.method private d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 1745
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b()V

    .line 1747
    if-lez p1, :cond_1

    .line 1748
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput v1, v2, Landroid/support/v7/widget/aq;->d:I

    .line 1749
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget-boolean v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v3, :cond_0

    :goto_0
    iput v0, v2, Landroid/support/v7/widget/aq;->c:I

    .line 1751
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u()I

    move-result v0

    .line 1758
    :goto_1
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v2, v2, Landroid/support/v7/widget/aq;->c:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/support/v7/widget/aq;->b:I

    .line 1759
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1760
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput v1, v0, Landroid/support/v7/widget/aq;->a:I

    .line 1761
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->e()I

    move-result v0

    :goto_2
    iput v0, v2, Landroid/support/v7/widget/aq;->e:I

    .line 1762
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    invoke-direct {p0, p2, v0, p3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/aq;Landroid/support/v7/widget/co;)I

    move-result v0

    .line 1764
    if-ge v1, v0, :cond_4

    .line 1775
    :goto_3
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bk;->a(I)V

    .line 1777
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->o:Z

    .line 1778
    return p1

    :cond_0
    move v0, v1

    .line 1749
    goto :goto_0

    .line 1753
    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iput v0, v2, Landroid/support/v7/widget/aq;->d:I

    .line 1754
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget-boolean v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v3, :cond_2

    :goto_4
    iput v1, v2, Landroid/support/v7/widget/aq;->c:I

    .line 1756
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v()I

    move-result v0

    goto :goto_1

    :cond_2
    move v1, v0

    .line 1754
    goto :goto_4

    .line 1761
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1766
    :cond_4
    if-gez p1, :cond_5

    .line 1767
    neg-int p1, v0

    goto :goto_3

    :cond_5
    move p1, v0

    .line 1769
    goto :goto_3
.end method

.method private g(Landroid/support/v7/widget/co;)I
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 913
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    if-nez v0, :cond_0

    .line 916
    :goto_0
    return v4

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_2

    :goto_2
    invoke-direct {p0, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    iget-boolean v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Landroid/support/v7/widget/cx;->a(Landroid/support/v7/widget/co;Landroid/support/v7/widget/bk;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/cd;ZZ)I

    move-result v4

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2
.end method

.method private g()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 507
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    if-eq v1, v0, :cond_0

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t()Z

    move-result v1

    if-nez v1, :cond_2

    .line 508
    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l:Z

    .line 510
    :cond_1
    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    .line 512
    return-void

    .line 510
    :cond_2
    iget-boolean v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h(Landroid/support/v7/widget/co;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 933
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    if-nez v0, :cond_0

    .line 936
    :goto_0
    return v4

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_2

    :goto_2
    invoke-direct {p0, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/cx;->a(Landroid/support/v7/widget/co;Landroid/support/v7/widget/bk;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/cd;Z)I

    move-result v4

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2
.end method

.method private i(I)I
    .locals 3

    .prologue
    .line 1543
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ds;->b(I)I

    move-result v1

    .line 1544
    const/4 v0, 0x1

    :goto_0
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v0, v2, :cond_1

    .line 1545
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/ds;->b(I)I

    move-result v2

    .line 1546
    if-le v2, v1, :cond_0

    move v1, v2

    .line 1544
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1550
    :cond_1
    return v1
.end method

.method private i(Landroid/support/v7/widget/co;)I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 953
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    if-nez v0, :cond_0

    .line 956
    :goto_0
    return v4

    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    invoke-direct {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    if-nez v0, :cond_2

    :goto_2
    invoke-direct {p0, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v3

    iget-boolean v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->A:Z

    move-object v0, p1

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Landroid/support/v7/widget/cx;->b(Landroid/support/v7/widget/co;Landroid/support/v7/widget/bk;Landroid/view/View;Landroid/view/View;Landroid/support/v7/widget/cd;Z)I

    move-result v4

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2
.end method

.method private t()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 515
    iget-object v1, p0, Landroid/support/v7/widget/cd;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v1}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()I
    .locals 1

    .prologue
    .line 1782
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    .line 1783
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method private v()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1787
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v1

    .line 1788
    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 1674
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 1120
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    if-nez v0, :cond_0

    .line 1121
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    .line 1123
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/cd;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 909
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final a()Landroid/support/v7/widget/ce;
    .locals 1

    .prologue
    .line 1826
    new-instance v0, Landroid/support/v7/widget/dp;

    invoke-direct {v0}, Landroid/support/v7/widget/dp;-><init>()V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/ce;
    .locals 1

    .prologue
    .line 1832
    new-instance v0, Landroid/support/v7/widget/dp;

    invoke-direct {v0, p1, p2}, Landroid/support/v7/widget/dp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/ce;
    .locals 1

    .prologue
    .line 1837
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_0

    .line 1838
    new-instance v0, Landroid/support/v7/widget/dp;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/dp;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 1840
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v7/widget/dp;

    invoke-direct {v0, p1}, Landroid/support/v7/widget/dp;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1004
    instance-of v0, p1, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 1005
    check-cast p1, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iput-object p1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    .line 1006
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h()V

    .line 1010
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 1258
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a()V

    .line 1259
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h()V

    .line 1260
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 1

    .prologue
    .line 1253
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(III)V

    .line 1254
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;III)V
    .locals 1

    .prologue
    .line 1264
    const/4 v0, 0x3

    invoke-direct {p0, p2, p3, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(III)V

    .line 1265
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/ci;)V
    .locals 2

    .prologue
    .line 269
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v0, v1, :cond_0

    .line 270
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/support/v7/widget/ds;->c()V

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 272
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Landroid/view/View;Landroid/support/v4/view/a/e;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 1065
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1066
    instance-of v3, v0, Landroid/support/v7/widget/dp;

    if-nez v3, :cond_0

    .line 1067
    invoke-super {p0, p3, p4}, Landroid/support/v7/widget/cd;->a(Landroid/view/View;Landroid/support/v4/view/a/e;)V

    .line 1082
    :goto_0
    return-void

    .line 1070
    :cond_0
    check-cast v0, Landroid/support/v7/widget/dp;

    .line 1071
    iget v3, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    if-nez v3, :cond_2

    .line 1072
    invoke-virtual {v0}, Landroid/support/v7/widget/dp;->a()I

    move-result v3

    iget-boolean v4, v0, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v4, :cond_1

    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    :cond_1
    move v4, v3

    move v3, v1

    move v1, v2

    .line 1077
    :goto_1
    iget-boolean v0, v0, Landroid/support/v7/widget/dp;->f:Z

    invoke-static {v4, v3, v2, v1, v0}, Landroid/support/v4/view/a/n;->a(IIIIZ)Landroid/support/v4/view/a/n;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/support/v4/view/a/e;->b(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/support/v7/widget/dp;->a()I

    move-result v3

    iget-boolean v4, v0, Landroid/support/v7/widget/dp;->f:Z

    if-eqz v4, :cond_3

    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    move v4, v2

    move v5, v2

    move v2, v3

    move v3, v5

    goto :goto_1

    :cond_3
    move v4, v2

    move v5, v2

    move v2, v3

    move v3, v5

    goto :goto_1
.end method

.method public final a(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1086
    invoke-super {p0, p1}, Landroid/support/v7/widget/cd;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1087
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    if-lez v0, :cond_0

    .line 1088
    invoke-static {p1}, Landroid/support/v4/view/a/a;->a(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/a/ab;

    move-result-object v0

    .line 1090
    invoke-direct {p0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v1

    .line 1091
    invoke-direct {p0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v2

    .line 1092
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 1105
    :cond_0
    :goto_0
    return-void

    .line 1095
    :cond_1
    invoke-static {v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;)I

    move-result v1

    .line 1096
    invoke-static {v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;)I

    move-result v2

    .line 1097
    if-ge v1, v2, :cond_2

    .line 1098
    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/ab;->b(I)V

    .line 1099
    invoke-virtual {v0, v2}, Landroid/support/v4/view/a/ab;->c(I)V

    goto :goto_0

    .line 1101
    :cond_2
    invoke-virtual {v0, v2}, Landroid/support/v4/view/a/ab;->b(I)V

    .line 1102
    invoke-virtual {v0, v1}, Landroid/support/v4/view/a/ab;->c(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-nez v0, :cond_0

    .line 467
    invoke-super {p0, p1}, Landroid/support/v7/widget/cd;->a(Ljava/lang/String;)V

    .line 469
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/ce;)Z
    .locals 1

    .prologue
    .line 1846
    instance-of v0, p1, Landroid/support/v7/widget/dp;

    return v0
.end method

.method public final b(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 1680
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(ILandroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final b(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I
    .locals 2

    .prologue
    .line 1129
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1130
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    .line 1132
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/cd;->b(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 924
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 1

    .prologue
    .line 1248
    const/4 v0, 0x1

    invoke-direct {p0, p2, p3, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(III)V

    .line 1249
    return-void
.end method

.method public final c(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 929
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 1714
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    if-eq v0, p1, :cond_0

    .line 1715
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->b()V

    .line 1717
    :cond_0
    iput p1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    .line 1718
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    .line 1719
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h()V

    .line 1720
    return-void
.end method

.method public final c(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 1

    .prologue
    .line 1269
    const/4 v0, 0x2

    invoke-direct {p0, p2, p3, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(III)V

    .line 1270
    return-void
.end method

.method public final c(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;)V
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/high16 v10, -0x80000000

    const/4 v1, 0x0

    .line 532
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b()V

    .line 534
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {p2}, Landroid/support/v7/widget/co;->a()I

    move-result v2

    iput v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->b:I

    .line 536
    iget-object v5, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->y:Landroid/support/v7/widget/do;

    .line 537
    iput v4, v5, Landroid/support/v7/widget/do;->a:I

    iput v10, v5, Landroid/support/v7/widget/do;->b:I

    iput-boolean v1, v5, Landroid/support/v7/widget/do;->c:Z

    iput-boolean v1, v5, Landroid/support/v7/widget/do;->d:Z

    .line 539
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_e

    .line 540
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    if-lez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ne v0, v2, :cond_2

    move v0, v1

    :goto_0
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/support/v7/widget/ds;->c()V

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->d:[I

    aget v2, v2, v0

    if-eq v2, v10, :cond_0

    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v6, v6, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->i:Z

    if-eqz v6, :cond_1

    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v6}, Landroid/support/v7/widget/bk;->c()I

    move-result v6

    add-int/2addr v2, v6

    :cond_0
    :goto_1
    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v6, v6, v0

    invoke-virtual {v6, v2}, Landroid/support/v7/widget/ds;->c(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v6}, Landroid/support/v7/widget/bk;->b()I

    move-result v6

    add-int/2addr v2, v6

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a()V

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->b:I

    iput v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->j:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t:Z

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->h:Z

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v2, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->h:Z

    if-eq v2, v0, :cond_4

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iput-boolean v0, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->h:Z

    :cond_4
    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h()V

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g()V

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    if-eq v0, v4, :cond_d

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-boolean v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->i:Z

    iput-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    :goto_2
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->e:I

    if-le v0, v3, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->f:[I

    iput-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a:[I

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->g:Ljava/util/List;

    iput-object v2, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->c:Ljava/util/List;

    .line 546
    :cond_5
    :goto_3
    iget-boolean v0, p2, Landroid/support/v7/widget/co;->i:Z

    if-nez v0, :cond_6

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    if-ne v0, v4, :cond_f

    :cond_6
    move v0, v1

    :goto_4
    if-nez v0, :cond_8

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->o:Z

    if-eqz v0, :cond_25

    invoke-virtual {p2}, Landroid/support/v7/widget/co;->a()I

    move-result v6

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_5
    if-ltz v2, :cond_24

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_23

    if-ge v0, v6, :cond_23

    :cond_7
    :goto_6
    iput v0, v5, Landroid/support/v7/widget/do;->a:I

    iput v10, v5, Landroid/support/v7/widget/do;->b:I

    .line 548
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-nez v0, :cond_a

    .line 549
    iget-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    iget-boolean v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->o:Z

    if-ne v0, v2, :cond_9

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t()Z

    move-result v0

    iget-boolean v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t:Z

    if-eq v0, v2, :cond_a

    .line 551
    :cond_9
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a()V

    .line 552
    iput-boolean v3, v5, Landroid/support/v7/widget/do;->d:Z

    .line 556
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    if-lez v0, :cond_2e

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_b

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    if-gtz v0, :cond_2e

    .line 558
    :cond_b
    iget-boolean v0, v5, Landroid/support/v7/widget/do;->d:Z

    if-eqz v0, :cond_28

    move v0, v1

    .line 559
    :goto_7
    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v0, v2, :cond_2e

    .line 561
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/support/v7/widget/ds;->c()V

    .line 562
    iget v2, v5, Landroid/support/v7/widget/do;->b:I

    if-eq v2, v10, :cond_c

    .line 563
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v2, v2, v0

    iget v6, v5, Landroid/support/v7/widget/do;->b:I

    invoke-virtual {v2, v6}, Landroid/support/v7/widget/ds;->c(I)V

    .line 559
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 540
    :cond_d
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    iput-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    goto/16 :goto_2

    .line 542
    :cond_e
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g()V

    .line 543
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    iput-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    goto/16 :goto_3

    .line 546
    :cond_f
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    if-ltz v0, :cond_10

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    invoke-virtual {p2}, Landroid/support/v7/widget/co;->a()I

    move-result v2

    if-lt v0, v2, :cond_11

    :cond_10
    iput v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    iput v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    move v0, v1

    goto/16 :goto_4

    :cond_11
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_12

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    if-eq v0, v4, :cond_12

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    if-gtz v0, :cond_22

    :cond_12
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1a

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v0, :cond_13

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u()I

    move-result v0

    :goto_8
    iput v0, v5, Landroid/support/v7/widget/do;->a:I

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    if-eq v0, v10, :cond_15

    iget-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    if-eqz v0, :cond_14

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    iget v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    sub-int/2addr v0, v6

    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v6, v2}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, v5, Landroid/support/v7/widget/do;->b:I

    :goto_9
    move v0, v3

    goto/16 :goto_4

    :cond_13
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v()I

    move-result v0

    goto :goto_8

    :cond_14
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v0

    iget v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    add-int/2addr v0, v6

    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v6, v2}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, v5, Landroid/support/v7/widget/do;->b:I

    goto :goto_9

    :cond_15
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v0

    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v6}, Landroid/support/v7/widget/bk;->e()I

    move-result v6

    if-le v0, v6, :cond_17

    iget-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    if-eqz v0, :cond_16

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    :goto_a
    iput v0, v5, Landroid/support/v7/widget/do;->b:I

    :goto_b
    move v0, v3

    goto/16 :goto_4

    :cond_16
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v0

    goto :goto_a

    :cond_17
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v0

    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v6}, Landroid/support/v7/widget/bk;->b()I

    move-result v6

    sub-int/2addr v0, v6

    if-gez v0, :cond_18

    neg-int v0, v0

    iput v0, v5, Landroid/support/v7/widget/do;->b:I

    goto :goto_b

    :cond_18
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v6, v2}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v2

    sub-int/2addr v0, v2

    if-gez v0, :cond_19

    iput v0, v5, Landroid/support/v7/widget/do;->b:I

    goto :goto_b

    :cond_19
    iput v10, v5, Landroid/support/v7/widget/do;->b:I

    goto :goto_b

    :cond_1a
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    iput v0, v5, Landroid/support/v7/widget/do;->a:I

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    if-ne v0, v10, :cond_20

    iget v0, v5, Landroid/support/v7/widget/do;->a:I

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v2

    if-nez v2, :cond_1b

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-nez v0, :cond_1d

    move v0, v4

    :goto_c
    if-ne v0, v3, :cond_1e

    move v0, v3

    :goto_d
    iput-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    iget-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    if-eqz v0, :cond_1f

    iget-object v0, v5, Landroid/support/v7/widget/do;->e:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->c()I

    move-result v0

    :goto_e
    iput v0, v5, Landroid/support/v7/widget/do;->b:I

    :goto_f
    iput-boolean v3, v5, Landroid/support/v7/widget/do;->d:Z

    goto :goto_b

    :cond_1b
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v()I

    move-result v2

    if-ge v0, v2, :cond_1c

    move v0, v3

    :goto_10
    iget-boolean v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eq v0, v2, :cond_1d

    move v0, v4

    goto :goto_c

    :cond_1c
    move v0, v1

    goto :goto_10

    :cond_1d
    move v0, v3

    goto :goto_c

    :cond_1e
    move v0, v1

    goto :goto_d

    :cond_1f
    iget-object v0, v5, Landroid/support/v7/widget/do;->e:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->b()I

    move-result v0

    goto :goto_e

    :cond_20
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    iget-boolean v2, v5, Landroid/support/v7/widget/do;->c:Z

    if-eqz v2, :cond_21

    iget-object v2, v5, Landroid/support/v7/widget/do;->e:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->c()I

    move-result v2

    sub-int v0, v2, v0

    iput v0, v5, Landroid/support/v7/widget/do;->b:I

    goto :goto_f

    :cond_21
    iget-object v2, v5, Landroid/support/v7/widget/do;->e:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v5, Landroid/support/v7/widget/do;->b:I

    goto :goto_f

    :cond_22
    iput v10, v5, Landroid/support/v7/widget/do;->b:I

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    iput v0, v5, Landroid/support/v7/widget/do;->a:I

    goto/16 :goto_b

    :cond_23
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto/16 :goto_5

    :cond_24
    move v0, v1

    goto/16 :goto_6

    :cond_25
    invoke-virtual {p2}, Landroid/support/v7/widget/co;->a()I

    move-result v6

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v7

    move v2, v1

    :goto_11
    if-ge v2, v7, :cond_27

    invoke-virtual {p0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_26

    if-lt v0, v6, :cond_7

    :cond_26
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_11

    :cond_27
    move v0, v1

    goto/16 :goto_6

    :cond_28
    move v2, v1

    .line 567
    :goto_12
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v2, v0, :cond_2e

    .line 568
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v6, v0, v2

    iget-boolean v7, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    iget v8, v5, Landroid/support/v7/widget/do;->b:I

    if-eqz v7, :cond_2b

    invoke-virtual {v6, v10}, Landroid/support/v7/widget/ds;->b(I)I

    move-result v0

    :goto_13
    invoke-virtual {v6}, Landroid/support/v7/widget/ds;->c()V

    if-eq v0, v10, :cond_2a

    if-eqz v7, :cond_29

    iget-object v9, v6, Landroid/support/v7/widget/ds;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v9, v9, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v9}, Landroid/support/v7/widget/bk;->c()I

    move-result v9

    if-lt v0, v9, :cond_2a

    :cond_29
    if-nez v7, :cond_2c

    iget-object v7, v6, Landroid/support/v7/widget/ds;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v7, v7, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v7}, Landroid/support/v7/widget/bk;->b()I

    move-result v7

    if-le v0, v7, :cond_2c

    .line 567
    :cond_2a
    :goto_14
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_12

    .line 568
    :cond_2b
    invoke-virtual {v6, v10}, Landroid/support/v7/widget/ds;->a(I)I

    move-result v0

    goto :goto_13

    :cond_2c
    if-eq v8, v10, :cond_2d

    add-int/2addr v0, v8

    :cond_2d
    iput v0, v6, Landroid/support/v7/widget/ds;->c:I

    iput v0, v6, Landroid/support/v7/widget/ds;->b:I

    goto :goto_14

    .line 572
    :cond_2e
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ci;)V

    .line 573
    iput-boolean v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    .line 574
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->e()I

    move-result v0

    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    div-int/2addr v0, v2

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j:I

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->e()I

    move-result v0

    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v:I

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    if-ne v0, v3, :cond_32

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j:I

    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->w:I

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->x:I

    .line 575
    :goto_15
    iget-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    if-eqz v0, :cond_33

    .line 577
    iget v0, v5, Landroid/support/v7/widget/do;->a:I

    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(ILandroid/support/v7/widget/co;)V

    .line 578
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/aq;Landroid/support/v7/widget/co;)I

    .line 580
    iget v0, v5, Landroid/support/v7/widget/do;->a:I

    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(ILandroid/support/v7/widget/co;)V

    .line 581
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v2, v0, Landroid/support/v7/widget/aq;->b:I

    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v6, v6, Landroid/support/v7/widget/aq;->c:I

    add-int/2addr v2, v6

    iput v2, v0, Landroid/support/v7/widget/aq;->b:I

    .line 582
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/aq;Landroid/support/v7/widget/co;)I

    .line 593
    :goto_16
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    if-lez v0, :cond_2f

    .line 594
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v0, :cond_34

    .line 595
    invoke-direct {p0, p1, p2, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)V

    .line 596
    invoke-direct {p0, p1, p2, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)V

    .line 603
    :cond_2f
    :goto_17
    iget-boolean v0, p2, Landroid/support/v7/widget/co;->i:Z

    if-nez v0, :cond_31

    .line 604
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    if-lez v0, :cond_30

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    if-eq v0, v4, :cond_30

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    if-eqz v0, :cond_30

    .line 606
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->B:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/at;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 608
    :cond_30
    iput v4, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d:I

    .line 609
    iput v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->e:I

    .line 611
    :cond_31
    iget-boolean v0, v5, Landroid/support/v7/widget/do;->c:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->o:Z

    .line 612
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t()Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t:Z

    .line 613
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    .line 614
    return-void

    .line 574
    :cond_32
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j:I

    invoke-static {v0, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->x:I

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->w:I

    goto :goto_15

    .line 585
    :cond_33
    iget v0, v5, Landroid/support/v7/widget/do;->a:I

    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(ILandroid/support/v7/widget/co;)V

    .line 586
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/aq;Landroid/support/v7/widget/co;)I

    .line 588
    iget v0, v5, Landroid/support/v7/widget/do;->a:I

    invoke-direct {p0, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(ILandroid/support/v7/widget/co;)V

    .line 589
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v2, v0, Landroid/support/v7/widget/aq;->b:I

    iget-object v6, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    iget v6, v6, Landroid/support/v7/widget/aq;->c:I

    add-int/2addr v2, v6

    iput v2, v0, Landroid/support/v7/widget/aq;->b:I

    .line 590
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->k:Landroid/support/v7/widget/aq;

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/aq;Landroid/support/v7/widget/co;)I

    goto :goto_16

    .line 598
    :cond_34
    invoke-direct {p0, p1, p2, v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)V

    .line 599
    invoke-direct {p0, p1, p2, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/support/v7/widget/ci;Landroid/support/v7/widget/co;Z)V

    goto :goto_17
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 944
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final d()Landroid/os/Parcelable;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/high16 v4, -0x80000000

    .line 1014
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    if-eqz v0, :cond_0

    .line 1015
    new-instance v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u:Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;-><init>(Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;)V

    .line 1059
    :goto_0
    return-object v0

    .line 1017
    :cond_0
    new-instance v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;

    invoke-direct {v3}, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;-><init>()V

    .line 1018
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->l:Z

    iput-boolean v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->h:Z

    .line 1019
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->o:Z

    iput-boolean v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->i:Z

    .line 1020
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t:Z

    iput-boolean v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->j:Z

    .line 1022
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a:[I

    if-eqz v0, :cond_2

    .line 1023
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a:[I

    iput-object v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->f:[I

    .line 1024
    iget-object v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->f:[I

    array-length v0, v0

    iput v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->e:I

    .line 1025
    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->c:Ljava/util/List;

    iput-object v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->g:Ljava/util/List;

    .line 1030
    :goto_1
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    if-lez v0, :cond_7

    .line 1031
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->o:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u()I

    move-result v0

    :goto_2
    iput v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    .line 1033
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v0, :cond_4

    invoke-direct {p0, v5}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->b(Z)Landroid/view/View;

    move-result-object v0

    :goto_3
    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    iput v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->b:I

    .line 1034
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    iput v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    .line 1035
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    new-array v0, v0, [I

    iput-object v0, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->d:[I

    move v0, v2

    .line 1036
    :goto_5
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v0, v1, :cond_8

    .line 1038
    iget-boolean v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->o:Z

    if-eqz v1, :cond_6

    .line 1039
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/ds;->b(I)I

    move-result v1

    .line 1040
    if-eq v1, v4, :cond_1

    .line 1041
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->c()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1049
    :cond_1
    :goto_6
    iget-object v2, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->d:[I

    aput v1, v2, v0

    .line 1036
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1027
    :cond_2
    iput v2, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->e:I

    goto :goto_1

    .line 1031
    :cond_3
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v()I

    move-result v0

    goto :goto_2

    .line 1033
    :cond_4
    invoke-direct {p0, v5}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Z)Landroid/view/View;

    move-result-object v0

    goto :goto_3

    :cond_5
    invoke-static {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a(Landroid/view/View;)I

    move-result v0

    goto :goto_4

    .line 1044
    :cond_6
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/ds;->a(I)I

    move-result v1

    .line 1045
    if-eq v1, v4, :cond_1

    .line 1046
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->b()I

    move-result v2

    sub-int/2addr v1, v2

    goto :goto_6

    .line 1052
    :cond_7
    iput v1, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->a:I

    .line 1053
    iput v1, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->b:I

    .line 1054
    iput v2, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager$SavedState;->c:I

    :cond_8
    move-object v0, v3

    .line 1059
    goto/16 :goto_0
.end method

.method public final e(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 949
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final e(I)V
    .locals 2

    .prologue
    .line 1232
    invoke-super {p0, p1}, Landroid/support/v7/widget/cd;->e(I)V

    .line 1233
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v0, v1, :cond_0

    .line 1234
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/ds;->d(I)V

    .line 1233
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1236
    :cond_0
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1668
    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/support/v7/widget/co;)I
    .locals 1

    .prologue
    .line 964
    invoke-direct {p0, p1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i(Landroid/support/v7/widget/co;)I

    move-result v0

    return v0
.end method

.method public final f(I)V
    .locals 2

    .prologue
    .line 1240
    invoke-super {p0, p1}, Landroid/support/v7/widget/cd;->f(I)V

    .line 1241
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    if-ge v0, v1, :cond_0

    .line 1242
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h:[Landroid/support/v7/widget/ds;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/ds;->d(I)V

    .line 1241
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1244
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1663
    iget v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(I)V
    .locals 13

    .prologue
    .line 262
    if-nez p1, :cond_0

    .line 263
    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->n:I

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u()I

    move-result v1

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v()I

    move-result v0

    move v7, v0

    move v8, v1

    :goto_1
    if-nez v8, :cond_12

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->j()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    new-instance v9, Ljava/util/BitSet;

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    invoke-direct {v9, v0}, Ljava/util/BitSet;-><init>(I)V

    const/4 v0, 0x0

    iget v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->g:I

    const/4 v3, 0x1

    invoke-virtual {v9, v0, v2, v3}, Ljava/util/BitSet;->set(IIZ)V

    iget v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->i:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->t()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v2, v0

    :goto_2
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v0, :cond_4

    add-int/lit8 v1, v1, -0x1

    const/4 v0, -0x1

    move v6, v0

    :goto_3
    if-ge v1, v6, :cond_5

    const/4 v0, 0x1

    move v3, v0

    :goto_4
    move v5, v1

    :goto_5
    if-eq v5, v6, :cond_11

    invoke-virtual {p0, v5}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dp;

    iget-object v1, v0, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    iget v1, v1, Landroid/support/v7/widget/ds;->e:I

    invoke-virtual {v9, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, v0, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    iget-boolean v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v10, :cond_6

    invoke-virtual {v1}, Landroid/support/v7/widget/ds;->b()I

    move-result v1

    iget-object v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v10}, Landroid/support/v7/widget/bk;->c()I

    move-result v10

    if-ge v1, v10, :cond_7

    const/4 v1, 0x1

    :goto_6
    if-eqz v1, :cond_8

    move-object v0, v4

    :goto_7
    if-eqz v0, :cond_12

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a()V

    :goto_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/cd;->s:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->h()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->v()I

    move-result v1

    invoke-direct {p0}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->u()I

    move-result v0

    move v7, v0

    move v8, v1

    goto :goto_1

    :cond_3
    const/4 v0, -0x1

    move v2, v0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    move v6, v1

    move v1, v0

    goto :goto_3

    :cond_5
    const/4 v0, -0x1

    move v3, v0

    goto :goto_4

    :cond_6
    invoke-virtual {v1}, Landroid/support/v7/widget/ds;->a()I

    move-result v1

    iget-object v10, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v10}, Landroid/support/v7/widget/bk;->b()I

    move-result v10

    if-le v1, v10, :cond_7

    const/4 v1, 0x1

    goto :goto_6

    :cond_7
    const/4 v1, 0x0

    goto :goto_6

    :cond_8
    iget-object v1, v0, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    iget v1, v1, Landroid/support/v7/widget/ds;->e:I

    invoke-virtual {v9, v1}, Ljava/util/BitSet;->clear(I)V

    :cond_9
    iget-boolean v1, v0, Landroid/support/v7/widget/dp;->f:Z

    if-nez v1, :cond_10

    add-int v1, v5, v3

    if-eq v1, v6, :cond_10

    add-int v1, v5, v3

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager;->d(I)Landroid/view/View;

    move-result-object v10

    const/4 v1, 0x0

    iget-boolean v11, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v11, :cond_c

    iget-object v11, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v11, v4}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v11

    iget-object v12, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v12, v10}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v12

    if-ge v11, v12, :cond_a

    move-object v0, v4

    goto :goto_7

    :cond_a
    if-ne v11, v12, :cond_b

    const/4 v1, 0x1

    :cond_b
    :goto_9
    if-eqz v1, :cond_10

    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/dp;

    iget-object v0, v0, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    iget v0, v0, Landroid/support/v7/widget/ds;->e:I

    iget-object v1, v1, Landroid/support/v7/widget/dp;->e:Landroid/support/v7/widget/ds;

    iget v1, v1, Landroid/support/v7/widget/ds;->e:I

    sub-int/2addr v0, v1

    if-gez v0, :cond_e

    const/4 v0, 0x1

    move v1, v0

    :goto_a
    if-gez v2, :cond_f

    const/4 v0, 0x1

    :goto_b
    if-eq v1, v0, :cond_10

    move-object v0, v4

    goto :goto_7

    :cond_c
    iget-object v11, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v11, v4}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v11

    iget-object v12, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v12, v10}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v12

    if-le v11, v12, :cond_d

    move-object v0, v4

    goto/16 :goto_7

    :cond_d
    if-ne v11, v12, :cond_b

    const/4 v1, 0x1

    goto :goto_9

    :cond_e
    const/4 v0, 0x0

    move v1, v0

    goto :goto_a

    :cond_f
    const/4 v0, 0x0

    goto :goto_b

    :cond_10
    add-int v0, v5, v3

    move v5, v0

    goto/16 :goto_5

    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_12
    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->c:Z

    if-eqz v0, :cond_13

    const/4 v0, -0x1

    :goto_c
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    add-int/lit8 v2, v7, 0x1

    invoke-virtual {v1, v8, v2, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a(III)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v1

    if-nez v1, :cond_14

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->z:Z

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    add-int/lit8 v1, v7, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a(I)I

    goto/16 :goto_0

    :cond_13
    const/4 v0, 0x1

    goto :goto_c

    :cond_14
    iget-object v2, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    iget v3, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    mul-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v8, v3, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a(III)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v0

    if-nez v0, :cond_15

    iget-object v0, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    iget v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a(I)I

    goto/16 :goto_8

    :cond_15
    iget-object v1, p0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    iget v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->a(I)I

    goto/16 :goto_8
.end method
