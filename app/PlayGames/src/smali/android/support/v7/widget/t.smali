.class final Landroid/support/v7/widget/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/cw;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/s;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/s;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/graphics/RectF;FLandroid/graphics/Paint;)V
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/high16 v3, 0x42b40000    # 90.0f

    .line 39
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, p3

    .line 40
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    sub-float v6, v1, v0

    .line 41
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float v7, v1, v0

    .line 42
    iget-object v1, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/s;

    iget-object v1, v1, Landroid/support/v7/widget/s;->a:Landroid/graphics/RectF;

    iget v2, p2, Landroid/graphics/RectF;->left:F

    iget v5, p2, Landroid/graphics/RectF;->top:F

    iget v8, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v8, v0

    iget v9, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v9

    invoke-virtual {v1, v2, v5, v8, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 45
    iget-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/s;

    iget-object v1, v0, Landroid/support/v7/widget/s;->a:Landroid/graphics/RectF;

    const/high16 v2, 0x43340000    # 180.0f

    move-object v0, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 46
    iget-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/s;

    iget-object v0, v0, Landroid/support/v7/widget/s;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, v6, v10}, Landroid/graphics/RectF;->offset(FF)V

    .line 47
    iget-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/s;

    iget-object v1, v0, Landroid/support/v7/widget/s;->a:Landroid/graphics/RectF;

    const/high16 v2, 0x43870000    # 270.0f

    move-object v0, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 48
    iget-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/s;

    iget-object v0, v0, Landroid/support/v7/widget/s;->a:Landroid/graphics/RectF;

    invoke-virtual {v0, v10, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 49
    iget-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/s;

    iget-object v1, v0, Landroid/support/v7/widget/s;->a:Landroid/graphics/RectF;

    move-object v0, p1

    move v2, v10

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 50
    iget-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/s;

    iget-object v0, v0, Landroid/support/v7/widget/s;->a:Landroid/graphics/RectF;

    neg-float v1, v6

    invoke-virtual {v0, v1, v10}, Landroid/graphics/RectF;->offset(FF)V

    .line 51
    iget-object v0, p0, Landroid/support/v7/widget/t;->a:Landroid/support/v7/widget/s;

    iget-object v1, v0, Landroid/support/v7/widget/s;->a:Landroid/graphics/RectF;

    move-object v0, p1

    move v2, v3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 54
    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v1, v0, p3

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float v3, v0, p3

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v4, v0, p3

    move-object v0, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 57
    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v1, v0, p3

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v0, p3

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float v3, v0, p3

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    move-object v0, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 62
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v2, v0, p3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v0, p3

    move-object v0, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 64
    return-void
.end method
