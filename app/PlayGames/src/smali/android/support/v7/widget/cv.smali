.class final Landroid/support/v7/widget/cv;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# static fields
.field static final a:D

.field static c:Landroid/support/v7/widget/cw;


# instance fields
.field final b:F

.field d:Landroid/graphics/Paint;

.field e:Landroid/graphics/Paint;

.field f:Landroid/graphics/Paint;

.field final g:Landroid/graphics/RectF;

.field h:F

.field i:Landroid/graphics/Path;

.field j:F

.field k:F

.field l:F

.field m:F

.field private n:Z

.field private final o:I

.field private final p:I

.field private q:Z

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    const-wide v0, 0x4046800000000000L    # 45.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    sput-wide v0, Landroid/support/v7/widget/cv;->a:D

    return-void
.end method

.method constructor <init>(Landroid/content/res/Resources;IFFF)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 90
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 76
    iput-boolean v2, p0, Landroid/support/v7/widget/cv;->n:Z

    .line 82
    iput-boolean v2, p0, Landroid/support/v7/widget/cv;->q:Z

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/cv;->r:Z

    .line 91
    sget v0, Landroid/support/v7/b/b;->b:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/cv;->o:I

    .line 92
    sget v0, Landroid/support/v7/b/b;->a:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/cv;->p:I

    .line 93
    sget v0, Landroid/support/v7/b/c;->a:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/cv;->b:F

    .line 94
    cmpg-float v0, p4, v1

    if-ltz v0, :cond_0

    cmpg-float v0, p5, v1

    if-gez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid shadow size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    cmpl-float v0, p4, p5

    if-lez v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v7/widget/cv;->r:Z

    if-nez v0, :cond_2

    const-string v0, "CardView"

    const-string v1, "Shadow size is being clipped by the max shadow size. See {CardView#setMaxCardElevation}."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v2, p0, Landroid/support/v7/widget/cv;->r:Z

    :cond_2
    move p4, p5

    :cond_3
    iget v0, p0, Landroid/support/v7/widget/cv;->m:F

    cmpl-float v0, v0, p4

    if-nez v0, :cond_4

    iget v0, p0, Landroid/support/v7/widget/cv;->k:F

    cmpl-float v0, v0, p5

    if-eqz v0, :cond_5

    :cond_4
    iput p4, p0, Landroid/support/v7/widget/cv;->m:F

    iput p5, p0, Landroid/support/v7/widget/cv;->k:F

    const/high16 v0, 0x3fc00000    # 1.5f

    mul-float/2addr v0, p4

    iget v1, p0, Landroid/support/v7/widget/cv;->b:F

    add-float/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/cv;->l:F

    iget v0, p0, Landroid/support/v7/widget/cv;->b:F

    add-float/2addr v0, p5

    iput v0, p0, Landroid/support/v7/widget/cv;->j:F

    iput-boolean v2, p0, Landroid/support/v7/widget/cv;->n:Z

    invoke-virtual {p0}, Landroid/support/v7/widget/cv;->invalidateSelf()V

    .line 95
    :cond_5
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Landroid/support/v7/widget/cv;->d:Landroid/graphics/Paint;

    .line 96
    iget-object v0, p0, Landroid/support/v7/widget/cv;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 97
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    .line 98
    iget-object v0, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 99
    iget-object v0, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 100
    iput p3, p0, Landroid/support/v7/widget/cv;->h:F

    .line 101
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    .line 102
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Landroid/support/v7/widget/cv;->f:Landroid/graphics/Paint;

    .line 103
    return-void
.end method

.method static a(FFZ)F
    .locals 6

    .prologue
    const/high16 v0, 0x3fc00000    # 1.5f

    .line 158
    if-eqz p2, :cond_0

    .line 159
    mul-float/2addr v0, p0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sget-wide v4, Landroid/support/v7/widget/cv;->a:D

    sub-double/2addr v2, v4

    float-to-double v4, p1

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    .line 161
    :goto_0
    return v0

    :cond_0
    mul-float/2addr v0, p0

    goto :goto_0
.end method

.method static b(FFZ)F
    .locals 6

    .prologue
    .line 167
    if-eqz p2, :cond_0

    .line 168
    float-to-double v0, p0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sget-wide v4, Landroid/support/v7/widget/cv;->a:D

    sub-double/2addr v2, v4

    float-to-double v4, p1

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float p0, v0

    .line 170
    :cond_0
    return p0
.end method


# virtual methods
.method final a()F
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Landroid/support/v7/widget/cv;->h:F

    return v0
.end method

.method final a(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 304
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/cv;->getPadding(Landroid/graphics/Rect;)Z

    .line 305
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 106
    iput-boolean p1, p0, Landroid/support/v7/widget/cv;->q:Z

    .line 107
    invoke-virtual {p0}, Landroid/support/v7/widget/cv;->invalidateSelf()V

    .line 108
    return-void
.end method

.method final b()F
    .locals 1

    .prologue
    .line 320
    iget v0, p0, Landroid/support/v7/widget/cv;->k:F

    return v0
.end method

.method final c()F
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 324
    iget v0, p0, Landroid/support/v7/widget/cv;->k:F

    iget v1, p0, Landroid/support/v7/widget/cv;->h:F

    iget v2, p0, Landroid/support/v7/widget/cv;->b:F

    add-float/2addr v1, v2

    iget v2, p0, Landroid/support/v7/widget/cv;->k:F

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, v3

    .line 326
    iget v1, p0, Landroid/support/v7/widget/cv;->k:F

    iget v2, p0, Landroid/support/v7/widget/cv;->b:F

    add-float/2addr v1, v2

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    return v0
.end method

.method final d()F
    .locals 5

    .prologue
    const/high16 v4, 0x3fc00000    # 1.5f

    const/high16 v3, 0x40000000    # 2.0f

    .line 330
    iget v0, p0, Landroid/support/v7/widget/cv;->k:F

    iget v1, p0, Landroid/support/v7/widget/cv;->h:F

    iget v2, p0, Landroid/support/v7/widget/cv;->b:F

    add-float/2addr v1, v2

    iget v2, p0, Landroid/support/v7/widget/cv;->k:F

    mul-float/2addr v2, v4

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, v3

    .line 332
    iget v1, p0, Landroid/support/v7/widget/cv;->k:F

    mul-float/2addr v1, v4

    iget v2, p0, Landroid/support/v7/widget/cv;->b:F

    add-float/2addr v1, v2

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 197
    iget-boolean v0, p0, Landroid/support/v7/widget/cv;->n:Z

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p0}, Landroid/support/v7/widget/cv;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v2, p0, Landroid/support/v7/widget/cv;->j:F

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v2, v3

    iget-object v3, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    iget v5, p0, Landroid/support/v7/widget/cv;->j:F

    add-float/2addr v4, v5

    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    add-float/2addr v5, v2

    iget v6, v0, Landroid/graphics/Rect;->right:I

    int-to-float v6, v6

    iget v7, p0, Landroid/support/v7/widget/cv;->j:F

    sub-float/2addr v6, v7

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    sub-float/2addr v0, v2

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/graphics/RectF;->set(FFFF)V

    new-instance v0, Landroid/graphics/RectF;

    iget v2, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v2, v2

    iget v3, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v3, v3

    iget v4, p0, Landroid/support/v7/widget/cv;->h:F

    iget v5, p0, Landroid/support/v7/widget/cv;->h:F

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iget v3, p0, Landroid/support/v7/widget/cv;->l:F

    neg-float v3, v3

    iget v4, p0, Landroid/support/v7/widget/cv;->l:F

    neg-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->inset(FF)V

    iget-object v3, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    if-nez v3, :cond_5

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    iput-object v3, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    :goto_0
    iget-object v3, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    sget-object v4, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v3, v4}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    iget-object v3, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    iget v4, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v4, v4

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v3, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    iget v4, p0, Landroid/support/v7/widget/cv;->l:F

    neg-float v4, v4

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    iget-object v3, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    const/high16 v4, 0x43340000    # 180.0f

    const/high16 v5, 0x42b40000    # 90.0f

    invoke-virtual {v3, v2, v4, v5, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    iget-object v2, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    const/high16 v3, 0x43870000    # 270.0f

    const/high16 v4, -0x3d4c0000    # -90.0f

    invoke-virtual {v2, v0, v3, v4, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    iget-object v0, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    iget v0, p0, Landroid/support/v7/widget/cv;->h:F

    iget v2, p0, Landroid/support/v7/widget/cv;->h:F

    iget v3, p0, Landroid/support/v7/widget/cv;->l:F

    add-float/2addr v2, v3

    div-float v2, v0, v2

    iget-object v7, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RadialGradient;

    iget v3, p0, Landroid/support/v7/widget/cv;->h:F

    iget v4, p0, Landroid/support/v7/widget/cv;->l:F

    add-float/2addr v3, v4

    new-array v4, v12, [I

    iget v5, p0, Landroid/support/v7/widget/cv;->o:I

    aput v5, v4, v9

    iget v5, p0, Landroid/support/v7/widget/cv;->o:I

    aput v5, v4, v8

    const/4 v5, 0x2

    iget v6, p0, Landroid/support/v7/widget/cv;->p:I

    aput v6, v4, v5

    new-array v5, v12, [F

    aput v1, v5, v9

    aput v2, v5, v8

    const/4 v2, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v5, v2

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v10, p0, Landroid/support/v7/widget/cv;->f:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/LinearGradient;

    iget v2, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v2, v2

    iget v3, p0, Landroid/support/v7/widget/cv;->l:F

    add-float/2addr v2, v3

    iget v3, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v3, v3

    iget v4, p0, Landroid/support/v7/widget/cv;->l:F

    sub-float v4, v3, v4

    new-array v5, v12, [I

    iget v3, p0, Landroid/support/v7/widget/cv;->o:I

    aput v3, v5, v9

    iget v3, p0, Landroid/support/v7/widget/cv;->o:I

    aput v3, v5, v8

    const/4 v3, 0x2

    iget v6, p0, Landroid/support/v7/widget/cv;->p:I

    aput v6, v5, v3

    new-array v6, v12, [F

    fill-array-data v6, :array_0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 199
    iput-boolean v9, p0, Landroid/support/v7/widget/cv;->n:Z

    .line 201
    :cond_0
    iget v0, p0, Landroid/support/v7/widget/cv;->m:F

    div-float/2addr v0, v11

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 202
    iget v0, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v0, v0

    iget v2, p0, Landroid/support/v7/widget/cv;->l:F

    sub-float v2, v0, v2

    iget v0, p0, Landroid/support/v7/widget/cv;->h:F

    iget v3, p0, Landroid/support/v7/widget/cv;->b:F

    add-float/2addr v0, v3

    iget v3, p0, Landroid/support/v7/widget/cv;->m:F

    div-float/2addr v3, v11

    add-float v7, v0, v3

    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float v3, v11, v7

    sub-float/2addr v0, v3

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    move v6, v8

    :goto_1
    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float v3, v11, v7

    sub-float/2addr v0, v3

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v9

    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v7

    iget-object v3, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v7

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    iget-object v3, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz v6, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float v3, v11, v7

    sub-float v3, v0, v3

    iget v0, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v4, v0

    iget-object v5, p0, Landroid/support/v7/widget/cv;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_1
    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v9

    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v7

    iget-object v3, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v7

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v0, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    iget-object v3, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz v6, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float v3, v11, v7

    sub-float v3, v0, v3

    iget v0, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v0, v0

    iget v4, p0, Landroid/support/v7/widget/cv;->l:F

    add-float/2addr v4, v0

    iget-object v5, p0, Landroid/support/v7/widget/cv;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_2
    invoke-virtual {p1, v9}, Landroid/graphics/Canvas;->restoreToCount(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v6

    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v7

    iget-object v3, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v7

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v0, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    iget-object v3, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz v8, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float v3, v11, v7

    sub-float v3, v0, v3

    iget v0, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v4, v0

    iget-object v5, p0, Landroid/support/v7/widget/cv;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v6

    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v7

    iget-object v3, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v7

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v0, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    iget-object v3, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz v8, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float v3, v11, v7

    sub-float v3, v0, v3

    iget v0, p0, Landroid/support/v7/widget/cv;->h:F

    neg-float v4, v0

    iget-object v5, p0, Landroid/support/v7/widget/cv;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_4
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 203
    iget v0, p0, Landroid/support/v7/widget/cv;->m:F

    neg-float v0, v0

    div-float/2addr v0, v11

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 204
    sget-object v0, Landroid/support/v7/widget/cv;->c:Landroid/support/v7/widget/cw;

    iget-object v1, p0, Landroid/support/v7/widget/cv;->g:Landroid/graphics/RectF;

    iget v2, p0, Landroid/support/v7/widget/cv;->h:F

    iget-object v3, p0, Landroid/support/v7/widget/cv;->d:Landroid/graphics/Paint;

    invoke-interface {v0, p1, v1, v2, v3}, Landroid/support/v7/widget/cw;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;FLandroid/graphics/Paint;)V

    .line 205
    return-void

    .line 198
    :cond_5
    iget-object v3, p0, Landroid/support/v7/widget/cv;->i:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    goto/16 :goto_0

    :cond_6
    move v6, v9

    .line 202
    goto/16 :goto_1

    :cond_7
    move v8, v9

    goto/16 :goto_2

    .line 198
    nop

    :array_0
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 183
    const/4 v0, -0x1

    return v0
.end method

.method public final getPadding(Landroid/graphics/Rect;)Z
    .locals 4

    .prologue
    .line 148
    iget v0, p0, Landroid/support/v7/widget/cv;->k:F

    iget v1, p0, Landroid/support/v7/widget/cv;->h:F

    iget-boolean v2, p0, Landroid/support/v7/widget/cv;->q:Z

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/cv;->a(FFZ)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 150
    iget v1, p0, Landroid/support/v7/widget/cv;->k:F

    iget v2, p0, Landroid/support/v7/widget/cv;->h:F

    iget-boolean v3, p0, Landroid/support/v7/widget/cv;->q:Z

    invoke-static {v1, v2, v3}, Landroid/support/v7/widget/cv;->b(FFZ)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 152
    invoke-virtual {p1, v1, v0, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 119
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/cv;->n:Z

    .line 121
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Landroid/support/v7/widget/cv;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 113
    iget-object v0, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 114
    iget-object v0, p0, Landroid/support/v7/widget/cv;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 115
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Landroid/support/v7/widget/cv;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 177
    iget-object v0, p0, Landroid/support/v7/widget/cv;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 178
    iget-object v0, p0, Landroid/support/v7/widget/cv;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 179
    return-void
.end method
