.class public abstract Landroid/support/v7/widget/by;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/ArrayList;

.field h:Landroid/support/v7/widget/bz;

.field i:J

.field j:J

.field k:J

.field l:J

.field m:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0xfa

    const-wide/16 v2, 0x78

    .line 7765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7767
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/bz;

    .line 7768
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/by;->a:Ljava/util/ArrayList;

    .line 7771
    iput-wide v2, p0, Landroid/support/v7/widget/by;->i:J

    .line 7772
    iput-wide v2, p0, Landroid/support/v7/widget/by;->j:J

    .line 7773
    iput-wide v4, p0, Landroid/support/v7/widget/by;->k:J

    .line 7774
    iput-wide v4, p0, Landroid/support/v7/widget/by;->l:J

    .line 7776
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/by;->m:Z

    .line 8191
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Landroid/support/v7/widget/cr;)Z
.end method

.method public abstract a(Landroid/support/v7/widget/cr;IIII)Z
.end method

.method public abstract a(Landroid/support/v7/widget/cr;Landroid/support/v7/widget/cr;IIII)Z
.end method

.method public abstract b()Z
.end method

.method public abstract b(Landroid/support/v7/widget/cr;)Z
.end method

.method public abstract c(Landroid/support/v7/widget/cr;)V
.end method

.method public abstract d()V
.end method

.method public final d(Landroid/support/v7/widget/cr;)V
    .locals 1

    .prologue
    .line 8010
    iget-object v0, p0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/bz;

    if-eqz v0, :cond_0

    .line 8012
    iget-object v0, p0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/bz;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/bz;->a(Landroid/support/v7/widget/cr;)V

    .line 8014
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 8177
    iget-object v0, p0, Landroid/support/v7/widget/by;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 8178
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 8179
    iget-object v2, p0, Landroid/support/v7/widget/by;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 8178
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8181
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/by;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 8182
    return-void
.end method

.method public final e(Landroid/support/v7/widget/cr;)V
    .locals 1

    .prologue
    .line 8022
    iget-object v0, p0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/bz;

    if-eqz v0, :cond_0

    .line 8024
    iget-object v0, p0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/bz;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/bz;->c(Landroid/support/v7/widget/cr;)V

    .line 8026
    :cond_0
    return-void
.end method

.method public final f(Landroid/support/v7/widget/cr;)V
    .locals 1

    .prologue
    .line 8034
    iget-object v0, p0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/bz;

    if-eqz v0, :cond_0

    .line 8036
    iget-object v0, p0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/bz;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/bz;->b(Landroid/support/v7/widget/cr;)V

    .line 8038
    :cond_0
    return-void
.end method

.method public final g(Landroid/support/v7/widget/cr;)V
    .locals 1

    .prologue
    .line 8051
    iget-object v0, p0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/bz;

    if-eqz v0, :cond_0

    .line 8053
    iget-object v0, p0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/bz;

    invoke-interface {v0, p1}, Landroid/support/v7/widget/bz;->d(Landroid/support/v7/widget/cr;)V

    .line 8055
    :cond_0
    return-void
.end method
