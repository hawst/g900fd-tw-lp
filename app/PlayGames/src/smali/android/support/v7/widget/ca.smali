.class final Landroid/support/v7/widget/ca;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/bz;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method private constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 7677
    iput-object p1, p0, Landroid/support/v7/widget/ca;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/widget/RecyclerView;B)V
    .locals 0

    .prologue
    .line 7677
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ca;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/cr;)V
    .locals 2

    .prologue
    .line 7681
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/cr;->a(Z)V

    .line 7682
    iget-object v0, p0, Landroid/support/v7/widget/ca;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->h(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 7683
    iget-object v0, p0, Landroid/support/v7/widget/ca;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->i(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 7684
    return-void
.end method

.method public final b(Landroid/support/v7/widget/cr;)V
    .locals 2

    .prologue
    .line 7688
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/cr;->a(Z)V

    .line 7689
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7690
    iget-object v0, p0, Landroid/support/v7/widget/ca;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->h(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 7692
    :cond_0
    return-void
.end method

.method public final c(Landroid/support/v7/widget/cr;)V
    .locals 2

    .prologue
    .line 7696
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/cr;->a(Z)V

    .line 7697
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7698
    iget-object v0, p0, Landroid/support/v7/widget/ca;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->h(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 7700
    :cond_0
    return-void
.end method

.method public final d(Landroid/support/v7/widget/cr;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 7704
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/cr;->a(Z)V

    .line 7732
    iget-object v0, p1, Landroid/support/v7/widget/cr;->g:Landroid/support/v7/widget/cr;

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/support/v7/widget/cr;->h:Landroid/support/v7/widget/cr;

    if-nez v0, :cond_0

    .line 7733
    iput-object v2, p1, Landroid/support/v7/widget/cr;->g:Landroid/support/v7/widget/cr;

    .line 7734
    const/16 v0, -0x41

    iget v1, p1, Landroid/support/v7/widget/cr;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/widget/cr;->a(II)V

    .line 7738
    :cond_0
    iput-object v2, p1, Landroid/support/v7/widget/cr;->h:Landroid/support/v7/widget/cr;

    .line 7739
    invoke-virtual {p1}, Landroid/support/v7/widget/cr;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7740
    iget-object v0, p0, Landroid/support/v7/widget/ca;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p1, Landroid/support/v7/widget/cr;->a:Landroid/view/View;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView;->h(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V

    .line 7742
    :cond_1
    return-void
.end method
