.class final Landroid/support/v7/widget/ah;
.super Landroid/support/v7/widget/al;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/widget/aj;

.field final synthetic b:Landroid/support/v4/view/cj;

.field final synthetic c:Landroid/support/v7/widget/aa;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/aa;Landroid/support/v7/widget/aj;Landroid/support/v4/view/cj;)V
    .locals 1

    .prologue
    .line 343
    iput-object p1, p0, Landroid/support/v7/widget/ah;->c:Landroid/support/v7/widget/aa;

    iput-object p2, p0, Landroid/support/v7/widget/ah;->a:Landroid/support/v7/widget/aj;

    iput-object p3, p0, Landroid/support/v7/widget/ah;->b:Landroid/support/v4/view/cj;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/al;-><init>(B)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Landroid/support/v7/widget/ah;->c:Landroid/support/v7/widget/aa;

    iget-object v0, p0, Landroid/support/v7/widget/ah;->a:Landroid/support/v7/widget/aj;

    iget-object v0, v0, Landroid/support/v7/widget/aj;->a:Landroid/support/v7/widget/cr;

    .line 347
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 350
    iget-object v0, p0, Landroid/support/v7/widget/ah;->b:Landroid/support/v4/view/cj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/cj;->a(Landroid/support/v4/view/cy;)Landroid/support/v4/view/cj;

    .line 351
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Landroid/support/v4/view/at;->c(Landroid/view/View;F)V

    .line 352
    invoke-static {p1, v2}, Landroid/support/v4/view/at;->a(Landroid/view/View;F)V

    .line 353
    invoke-static {p1, v2}, Landroid/support/v4/view/at;->b(Landroid/view/View;F)V

    .line 354
    iget-object v0, p0, Landroid/support/v7/widget/ah;->c:Landroid/support/v7/widget/aa;

    iget-object v1, p0, Landroid/support/v7/widget/ah;->a:Landroid/support/v7/widget/aj;

    iget-object v1, v1, Landroid/support/v7/widget/aj;->a:Landroid/support/v7/widget/cr;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/aa;->g(Landroid/support/v7/widget/cr;)V

    .line 355
    iget-object v0, p0, Landroid/support/v7/widget/ah;->c:Landroid/support/v7/widget/aa;

    iget-object v0, v0, Landroid/support/v7/widget/aa;->g:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/ah;->a:Landroid/support/v7/widget/aj;

    iget-object v1, v1, Landroid/support/v7/widget/aj;->a:Landroid/support/v7/widget/cr;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 356
    iget-object v0, p0, Landroid/support/v7/widget/ah;->c:Landroid/support/v7/widget/aa;

    invoke-virtual {v0}, Landroid/support/v7/widget/aa;->c()V

    .line 357
    return-void
.end method
