.class final Landroid/support/v4/widget/ad;
.super Landroid/view/animation/Animation;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v4/widget/aj;

.field final synthetic b:Landroid/support/v4/widget/ac;


# direct methods
.method constructor <init>(Landroid/support/v4/widget/ac;Landroid/support/v4/widget/aj;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Landroid/support/v4/widget/ad;->b:Landroid/support/v4/widget/ac;

    iput-object p2, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4

    .prologue
    .line 299
    iget-object v0, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    iget v0, v0, Landroid/support/v4/widget/aj;->m:F

    const v1, 0x3f4ccccd    # 0.8f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    .line 301
    iget-object v1, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    iget v1, v1, Landroid/support/v4/widget/aj;->k:F

    iget-object v2, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    iget v2, v2, Landroid/support/v4/widget/aj;->l:F

    iget-object v3, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    iget v3, v3, Landroid/support/v4/widget/aj;->k:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    .line 304
    iget-object v2, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/aj;->a(F)V

    .line 305
    iget-object v1, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    iget v1, v1, Landroid/support/v4/widget/aj;->m:F

    iget-object v2, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    iget v2, v2, Landroid/support/v4/widget/aj;->m:F

    sub-float/2addr v0, v2

    mul-float/2addr v0, p1

    add-float/2addr v0, v1

    .line 307
    iget-object v1, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/aj;->c(F)V

    .line 308
    iget-object v0, p0, Landroid/support/v4/widget/ad;->a:Landroid/support/v4/widget/aj;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/aj;->d(F)V

    .line 309
    return-void
.end method
