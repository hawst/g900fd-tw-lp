.class public final Lcom/google/a/a/a/a/a/d;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public f:Z

.field public g:[Lcom/google/a/a/a/a/a/e;

.field public h:Lcom/google/a/a/a/a/a/c;

.field public i:[B

.field public j:[B

.field public k:[B

.field public l:Lcom/google/a/a/a/a/a/b;

.field public m:Ljava/lang/String;

.field public n:J


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 453
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 454
    iput-wide v4, p0, Lcom/google/a/a/a/a/a/d;->a:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/a/a/a/a/a/d;->c:I

    iput v1, p0, Lcom/google/a/a/a/a/a/d;->d:I

    iput-boolean v1, p0, Lcom/google/a/a/a/a/a/d;->f:Z

    invoke-static {}, Lcom/google/a/a/a/a/a/e;->e()[Lcom/google/a/a/a/a/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    iput-object v2, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->i:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->j:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->k:[B

    iput-object v2, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    iput-wide v4, p0, Lcom/google/a/a/a/a/a/d;->n:J

    iput-object v2, p0, Lcom/google/a/a/a/a/a/d;->e:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/a/a/a/a/a/d;->G:I

    .line 455
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 627
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->a()I

    move-result v0

    .line 628
    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 629
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 632
    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 633
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 636
    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 637
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 638
    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    aget-object v2, v2, v0

    .line 639
    if-eqz v2, :cond_2

    .line 640
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 637
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 645
    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->i:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 646
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->i:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 649
    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    if-eqz v1, :cond_6

    .line 650
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 653
    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->j:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    .line 654
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->j:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 657
    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    if-eqz v1, :cond_8

    .line 658
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 661
    :cond_8
    iget-boolean v1, p0, Lcom/google/a/a/a/a/a/d;->f:Z

    if-eqz v1, :cond_9

    .line 662
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/a/a/a/a/a/d;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 665
    :cond_9
    iget v1, p0, Lcom/google/a/a/a/a/a/d;->c:I

    if-eqz v1, :cond_a

    .line 666
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/a/a/a/a/a/d;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 669
    :cond_a
    iget v1, p0, Lcom/google/a/a/a/a/a/d;->d:I

    if-eqz v1, :cond_b

    .line 670
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/a/a/a/a/a/d;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 673
    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->k:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_c

    .line 674
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->k:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 677
    :cond_c
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 678
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 681
    :cond_d
    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->n:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_e

    .line 682
    const/16 v1, 0xf

    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->n:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 685
    :cond_e
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 397
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/a/a/a/a/a/d;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/a/a/a/a/a/d;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/a/e;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/a/a/a/a/a/e;

    invoke-direct {v3}, Lcom/google/a/a/a/a/a/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/a/a/a/a/a/e;

    invoke-direct {v3}, Lcom/google/a/a/a/a/a/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->i:[B

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/a/a/a/a/a/b;

    invoke-direct {v0}, Lcom/google/a/a/a/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->j:[B

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/a/a/a/a/a/c;

    invoke-direct {v0}, Lcom/google/a/a/a/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/a/d;->f:Z

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/a/d;->c:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/a/d;->d:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->k:[B

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/a/a/a/a/a/d;->n:J

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
        0x58 -> :sswitch_9
        0x60 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x78 -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 578
    iget-wide v0, p0, Lcom/google/a/a/a/a/a/d;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 579
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 581
    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 582
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 584
    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 585
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 586
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    aget-object v1, v1, v0

    .line 587
    if-eqz v1, :cond_2

    .line 588
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 585
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 592
    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->i:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 593
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->i:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 595
    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    if-eqz v0, :cond_5

    .line 596
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 598
    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->j:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    .line 599
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->j:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 601
    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    if-eqz v0, :cond_7

    .line 602
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 604
    :cond_7
    iget-boolean v0, p0, Lcom/google/a/a/a/a/a/d;->f:Z

    if-eqz v0, :cond_8

    .line 605
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/a/a/a/a/a/d;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 607
    :cond_8
    iget v0, p0, Lcom/google/a/a/a/a/a/d;->c:I

    if-eqz v0, :cond_9

    .line 608
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/a/a/a/a/a/d;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 610
    :cond_9
    iget v0, p0, Lcom/google/a/a/a/a/a/d;->d:I

    if-eqz v0, :cond_a

    .line 611
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/a/a/a/a/a/d;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 613
    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->k:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_b

    .line 614
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->k:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 616
    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 617
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 619
    :cond_c
    iget-wide v0, p0, Lcom/google/a/a/a/a/a/d;->n:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_d

    .line 620
    const/16 v0, 0xf

    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->n:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 622
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->a(Lcom/google/protobuf/nano/b;)V

    .line 623
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 478
    if-ne p1, p0, :cond_1

    .line 479
    const/4 v0, 0x1

    .line 545
    :cond_0
    :goto_0
    return v0

    .line 481
    :cond_1
    instance-of v1, p1, Lcom/google/a/a/a/a/a/d;

    if-eqz v1, :cond_0

    .line 484
    check-cast p1, Lcom/google/a/a/a/a/a/d;

    .line 485
    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->a:J

    iget-wide v4, p1, Lcom/google/a/a/a/a/a/d;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 488
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 489
    iget-object v1, p1, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 495
    :cond_2
    iget v1, p0, Lcom/google/a/a/a/a/a/d;->c:I

    iget v2, p1, Lcom/google/a/a/a/a/a/d;->c:I

    if-ne v1, v2, :cond_0

    .line 498
    iget v1, p0, Lcom/google/a/a/a/a/a/d;->d:I

    iget v2, p1, Lcom/google/a/a/a/a/a/d;->d:I

    if-ne v1, v2, :cond_0

    .line 501
    iget-boolean v1, p0, Lcom/google/a/a/a/a/a/d;->f:Z

    iget-boolean v2, p1, Lcom/google/a/a/a/a/a/d;->f:Z

    if-ne v1, v2, :cond_0

    .line 504
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    iget-object v2, p1, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 508
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    if-nez v1, :cond_7

    .line 509
    iget-object v1, p1, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    if-nez v1, :cond_0

    .line 517
    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->i:[B

    iget-object v2, p1, Lcom/google/a/a/a/a/a/d;->i:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 520
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->j:[B

    iget-object v2, p1, Lcom/google/a/a/a/a/a/d;->j:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 523
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->k:[B

    iget-object v2, p1, Lcom/google/a/a/a/a/a/d;->k:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 526
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    if-nez v1, :cond_8

    .line 527
    iget-object v1, p1, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    if-nez v1, :cond_0

    .line 535
    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 536
    iget-object v1, p1, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 542
    :cond_5
    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->n:J

    iget-wide v4, p1, Lcom/google/a/a/a/a/a/d;->n:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 545
    invoke-virtual {p0, p1}, Lcom/google/a/a/a/a/a/d;->a(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 492
    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 513
    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    iget-object v2, p1, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    invoke-virtual {v1, v2}, Lcom/google/a/a/a/a/a/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 531
    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    iget-object v2, p1, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    invoke-virtual {v1, v2}, Lcom/google/a/a/a/a/a/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 539
    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 550
    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->a:J

    iget-wide v4, p0, Lcom/google/a/a/a/a/a/d;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    .line 553
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 555
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/a/a/a/a/a/d;->c:I

    add-int/2addr v0, v2

    .line 556
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/a/a/a/a/a/d;->d:I

    add-int/2addr v0, v2

    .line 557
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/a/a/a/a/a/d;->f:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    .line 558
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->g:[Lcom/google/a/a/a/a/a/e;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 560
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 562
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->i:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 563
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->j:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 564
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->k:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 565
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 567
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 569
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/a/a/a/a/a/d;->n:J

    iget-wide v4, p0, Lcom/google/a/a/a/a/a/d;->n:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 571
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/a/a/a/a/a/d;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 572
    return v0

    .line 553
    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 557
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    .line 560
    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->h:Lcom/google/a/a/a/a/a/c;

    invoke-virtual {v0}, Lcom/google/a/a/a/a/a/c;->hashCode()I

    move-result v0

    goto :goto_2

    .line 565
    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/a/d;->l:Lcom/google/a/a/a/a/a/b;

    invoke-virtual {v0}, Lcom/google/a/a/a/a/a/b;->hashCode()I

    move-result v0

    goto :goto_3

    .line 567
    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/a/d;->m:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method
