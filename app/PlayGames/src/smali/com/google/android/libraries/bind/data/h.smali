.class public Lcom/google/android/libraries/bind/data/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final k:Lcom/google/android/libraries/bind/c/b;


# instance fields
.field public final a:Ljava/lang/Integer;

.field public final b:Ljava/lang/Integer;

.field public final c:Ljava/lang/Integer;

.field public final d:Ljava/lang/Integer;

.field public final e:Ljava/lang/Integer;

.field public final f:Ljava/lang/Integer;

.field public final g:Ljava/lang/Integer;

.field public final h:Ljava/lang/Integer;

.field public final i:Ljava/lang/Integer;

.field protected final j:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/libraries/bind/data/h;

    invoke-static {v0}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Class;)Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/data/h;->k:Lcom/google/android/libraries/bind/c/b;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p3, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    .line 36
    sget-object v0, Lcom/google/android/libraries/bind/e;->o:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 38
    sget v1, Lcom/google/android/libraries/bind/e;->p:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/h;->a:Ljava/lang/Integer;

    .line 39
    sget v1, Lcom/google/android/libraries/bind/e;->q:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/h;->b:Ljava/lang/Integer;

    .line 40
    sget v1, Lcom/google/android/libraries/bind/e;->r:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/h;->c:Ljava/lang/Integer;

    .line 41
    sget v1, Lcom/google/android/libraries/bind/e;->u:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/h;->f:Ljava/lang/Integer;

    .line 42
    sget v1, Lcom/google/android/libraries/bind/e;->s:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/h;->d:Ljava/lang/Integer;

    .line 43
    sget v1, Lcom/google/android/libraries/bind/e;->t:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/h;->e:Ljava/lang/Integer;

    .line 44
    sget v1, Lcom/google/android/libraries/bind/e;->v:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/h;->g:Ljava/lang/Integer;

    .line 45
    sget v1, Lcom/google/android/libraries/bind/e;->x:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/h;->i:Ljava/lang/Integer;

    .line 46
    sget v1, Lcom/google/android/libraries/bind/e;->w:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/h;->h:Ljava/lang/Integer;

    .line 48
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    return-void
.end method

.method public static a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;
    .locals 2

    .prologue
    const v1, 0x7fffffff

    .line 66
    invoke-virtual {p0, p1, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 67
    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 166
    if-eqz p1, :cond_1

    .line 168
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    .line 176
    :goto_0
    if-eqz p3, :cond_6

    .line 177
    if-nez v0, :cond_5

    .line 179
    :goto_1
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 181
    :cond_1
    return-void

    .line 170
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 171
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 172
    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    .line 174
    goto :goto_0

    :cond_5
    move v2, v1

    .line 177
    goto :goto_1

    :cond_6
    move v2, v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 52
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/h;->a:Ljava/lang/Integer;

    if-eqz v5, :cond_0

    if-nez p1, :cond_7

    move-object v0, v1

    :goto_0
    if-nez v0, :cond_8

    invoke-virtual {v4, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 53
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/h;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    if-nez p1, :cond_b

    move-object v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 54
    :cond_1
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/h;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_c

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/libraries/bind/data/Data;->a(I)Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v2

    :goto_3
    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 55
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/h;->d:Ljava/lang/Integer;

    invoke-static {v0, v4, p1, v2}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;Z)V

    .line 56
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/h;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    if-nez p1, :cond_d

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_4
    if-nez v0, :cond_e

    move v0, v3

    :goto_5
    invoke-virtual {v2, v0}, Landroid/view/View;->setMinimumHeight(I)V

    .line 57
    :cond_3
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/h;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    if-nez p1, :cond_f

    move-object v0, v1

    :goto_6
    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    :cond_4
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/h;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    if-nez p1, :cond_10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_7
    if-nez v0, :cond_11

    move v0, v3

    :goto_8
    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v5, v6, :cond_12

    invoke-virtual {v2}, Landroid/view/View;->isPaddingRelative()Z

    move-result v5

    if-eqz v5, :cond_12

    invoke-virtual {v2}, Landroid/view/View;->getPaddingStart()I

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getPaddingEnd()I

    move-result v6

    invoke-virtual {v2, v5, v0, v6, v4}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 59
    :cond_5
    :goto_9
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/h;->h:Ljava/lang/Integer;

    if-eqz v2, :cond_6

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x15

    if-lt v4, v5, :cond_6

    if-nez p1, :cond_13

    :goto_a
    invoke-virtual {v0, v1}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 60
    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/h;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/h;->i:Ljava/lang/Integer;

    invoke-static {v0, v1, p1, v3}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/view/View;Ljava/lang/Integer;Lcom/google/android/libraries/bind/data/Data;Z)V

    .line 61
    return-void

    .line 52
    :cond_7
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    instance-of v6, v0, Ljava/lang/Integer;

    if-eqz v6, :cond_9

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_1

    :cond_9
    instance-of v6, v0, Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_a

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    :cond_a
    sget-object v0, Lcom/google/android/libraries/bind/data/h;->k:Lcom/google/android/libraries/bind/c/b;

    const-string v4, "Unrecognized bound background for key: %s"

    new-array v6, v2, [Ljava/lang/Object;

    aput-object v5, v6, v3

    invoke-virtual {v0, v4, v6}, Lcom/google/android/libraries/bind/c/b;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 53
    :cond_b
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    goto/16 :goto_2

    :cond_c
    move v0, v3

    .line 54
    goto/16 :goto_3

    .line 56
    :cond_d
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    goto/16 :goto_4

    :cond_e
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto/16 :goto_5

    .line 57
    :cond_f
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View$OnClickListener;

    goto/16 :goto_6

    .line 58
    :cond_10
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    goto/16 :goto_7

    :cond_11
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto/16 :goto_8

    :cond_12
    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    invoke-virtual {v2, v5, v0, v6, v4}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_9

    .line 59
    :cond_13
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/android/libraries/bind/data/Data;->c(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_a
.end method
