.class public final Lcom/google/android/libraries/bind/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Executor;


# static fields
.field protected static final a:Ljava/util/List;

.field private static final f:Lcom/google/android/libraries/bind/c/b;


# instance fields
.field protected b:Ljava/util/concurrent/Executor;

.field public final c:I

.field protected final d:Ljava/lang/ThreadGroup;

.field public final e:Ljava/util/concurrent/Executor;

.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/libraries/bind/a/o;

    invoke-static {v0}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Class;)Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/a/j;->f:Lcom/google/android/libraries/bind/c/b;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/a/j;->a:Ljava/util/List;

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/libraries/bind/a/k;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/a/k;-><init>(Lcom/google/android/libraries/bind/a/j;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/j;->e:Ljava/util/concurrent/Executor;

    .line 70
    iput-object p1, p0, Lcom/google/android/libraries/bind/a/j;->g:Ljava/lang/String;

    .line 71
    sget-object v0, Lcom/google/android/libraries/bind/a/j;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/libraries/bind/a/j;->c:I

    .line 73
    new-instance v0, Ljava/lang/ThreadGroup;

    invoke-direct {v0, p1}, Ljava/lang/ThreadGroup;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/j;->d:Ljava/lang/ThreadGroup;

    .line 74
    new-instance v6, Lcom/google/android/libraries/bind/a/l;

    invoke-direct {v6, p0}, Lcom/google/android/libraries/bind/a/l;-><init>(Lcom/google/android/libraries/bind/a/j;)V

    new-instance v0, Lcom/google/android/libraries/bind/a/n;

    iget v2, p0, Lcom/google/android/libraries/bind/a/j;->c:I

    iget v3, p0, Lcom/google/android/libraries/bind/a/j;->c:I

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v5, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v5}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const/4 v7, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/libraries/bind/a/n;-><init>(Lcom/google/android/libraries/bind/a/j;IILjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Z)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/j;->b:Ljava/util/concurrent/Executor;

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/libraries/bind/a/k;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/a/k;-><init>(Lcom/google/android/libraries/bind/a/j;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/j;->e:Ljava/util/concurrent/Executor;

    .line 78
    iput-object p1, p0, Lcom/google/android/libraries/bind/a/j;->g:Ljava/lang/String;

    .line 79
    sget-object v0, Lcom/google/android/libraries/bind/a/j;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/a/j;->c:I

    .line 81
    new-instance v0, Ljava/lang/ThreadGroup;

    invoke-direct {v0, p1}, Ljava/lang/ThreadGroup;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/j;->d:Ljava/lang/ThreadGroup;

    .line 82
    iput-object p2, p0, Lcom/google/android/libraries/bind/a/j;->b:Ljava/util/concurrent/Executor;

    .line 83
    return-void
.end method


# virtual methods
.method public final execute(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/j;->b:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 88
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/j;->g:Ljava/lang/String;

    return-object v0
.end method
