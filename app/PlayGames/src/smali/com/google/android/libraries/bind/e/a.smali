.class public Lcom/google/android/libraries/bind/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/libraries/bind/c/b;

.field private static final c:Ljava/util/WeakHashMap;


# instance fields
.field public final a:Landroid/content/Context;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Ljava/util/Map;

.field private final f:Ljava/util/List;

.field private g:I

.field private h:I

.field private i:I

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/libraries/bind/e/a;

    invoke-static {v0}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Class;)Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/e/a;->b:Lcom/google/android/libraries/bind/c/b;

    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/libraries/bind/e/a;->c:Ljava/util/WeakHashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/e/a;->e:Ljava/util/Map;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/e/a;->f:Ljava/util/List;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/e/a;->j:Z

    .line 51
    iput-object p1, p0, Lcom/google/android/libraries/bind/e/a;->a:Landroid/content/Context;

    .line 52
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/libraries/bind/e/a;->d:Landroid/view/LayoutInflater;

    .line 63
    return-void
.end method

.method private a()I
    .locals 3

    .prologue
    .line 134
    iget v0, p0, Lcom/google/android/libraries/bind/e/a;->g:I

    mul-int/lit8 v0, v0, 0x64

    iget v1, p0, Lcom/google/android/libraries/bind/e/a;->g:I

    iget v2, p0, Lcom/google/android/libraries/bind/e/a;->h:I

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    return v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    sget-object v0, Lcom/google/android/libraries/bind/e/a;->b:Lcom/google/android/libraries/bind/c/b;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/c/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    invoke-static {p0}, Lcom/google/android/libraries/bind/d/b;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 226
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(IZ)V
    .locals 5

    .prologue
    .line 119
    iget v0, p0, Lcom/google/android/libraries/bind/e/a;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/e/a;->g:I

    .line 120
    if-nez p2, :cond_0

    .line 121
    iget v0, p0, Lcom/google/android/libraries/bind/e/a;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/e/a;->i:I

    .line 123
    :cond_0
    sget-object v0, Lcom/google/android/libraries/bind/e/a;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v1, "Reusing view of type %s, efficiency: %d %%"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/libraries/bind/e/a;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/google/android/libraries/bind/e/a;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 184
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    :goto_0
    if-ltz v5, :cond_b

    .line 185
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 186
    instance-of v0, v1, Lcom/google/android/libraries/bind/data/e;

    if-eqz v0, :cond_a

    move-object v0, v1

    check-cast v0, Lcom/google/android/libraries/bind/data/e;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/e;->o()Z

    move-result v0

    if-nez v0, :cond_a

    .line 188
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 189
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_4

    instance-of v0, v1, Lcom/google/android/libraries/bind/data/e;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/libraries/bind/data/e;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/e;->o()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/libraries/bind/data/e;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/e;->m()V

    :cond_0
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/e/a;->a(Landroid/view/ViewGroup;)V

    :cond_1
    instance-of v0, v1, Lcom/google/android/libraries/bind/data/e;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Lcom/google/android/libraries/bind/data/e;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/e;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-virtual {v1, v4, v4}, Landroid/view/View;->measure(II)V

    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_5

    move v0, v3

    :goto_1
    invoke-static {v0}, Lcom/google/android/libraries/bind/d/b;->a(Z)V

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-boolean v2, p0, Lcom/google/android/libraries/bind/e/a;->j:Z

    if-eqz v2, :cond_6

    sget-object v1, Lcom/google/android/libraries/bind/e/a;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v2, "The heap is temporarily disabled after being cleared, not recycling view of type %s"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/bind/e/a;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v4

    invoke-virtual {v1, v2, v6}, Lcom/google/android/libraries/bind/c/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 184
    :cond_4
    :goto_2
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_0

    :cond_5
    move v0, v4

    .line 189
    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/google/android/libraries/bind/e/a;->e:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez v2, :cond_7

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_7
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/google/android/libraries/bind/e/a;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v7, "Recycled view of type %s, got %d in heap"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v9}, Lcom/google/android/libraries/bind/e/a;->a(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v3

    invoke-virtual {v6, v7, v8}, Lcom/google/android/libraries/bind/c/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v6, p0, Lcom/google/android/libraries/bind/e/a;->e:Ljava/util/Map;

    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/libraries/bind/e/a;->c:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/google/android/libraries/bind/e/a;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_9

    move v0, v3

    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Recycling a view we didn\'t create: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/d/b;->a(ZLjava/lang/String;)V

    :cond_8
    iget v0, p0, Lcom/google/android/libraries/bind/e/a;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/bind/e/a;->i:I

    goto :goto_2

    :cond_9
    move v0, v4

    goto :goto_3

    .line 190
    :cond_a
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 191
    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {p0, v1}, Lcom/google/android/libraries/bind/e/a;->a(Landroid/view/ViewGroup;)V

    goto :goto_2

    .line 194
    :cond_b
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 71
    if-eqz p3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/d/b;->a(Z)V

    .line 72
    iput-boolean v2, p0, Lcom/google/android/libraries/bind/e/a;->j:Z

    .line 73
    if-nez p2, :cond_3

    move-object v3, v4

    .line 74
    :goto_1
    sget-object v5, Lcom/google/android/libraries/bind/e/a;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v6, "convertViewResId: %s"

    new-array v7, v1, [Ljava/lang/Object;

    if-nez v3, :cond_4

    const-string v0, "null"

    :goto_2
    aput-object v0, v7, v2

    invoke-virtual {v5, v6, v7}, Lcom/google/android/libraries/bind/c/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 76
    if-eqz v3, :cond_5

    .line 78
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 79
    sget-object v0, Lcom/google/android/libraries/bind/e/a;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v5, "recycling children"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v5, v6}, Lcom/google/android/libraries/bind/c/b;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, p2

    .line 80
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/e/a;->a(Landroid/view/ViewGroup;)V

    .line 83
    :cond_0
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_5

    .line 84
    invoke-direct {p0, p1, v2}, Lcom/google/android/libraries/bind/e/a;->a(IZ)V

    .line 85
    invoke-virtual {p2, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 86
    sget-object v0, Lcom/google/android/libraries/bind/e/a;->c:Ljava/util/WeakHashMap;

    if-eqz v0, :cond_1

    .line 87
    sget-object v0, Lcom/google/android/libraries/bind/e/a;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p2}, Ljava/util/WeakHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/bind/d/b;->a(Z)V

    .line 115
    :cond_1
    :goto_3
    return-object p2

    :cond_2
    move v0, v2

    .line 71
    goto :goto_0

    .line 73
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    move-object v3, v0

    goto :goto_1

    .line 74
    :cond_4
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/bind/e/a;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 93
    :cond_5
    iget-object v0, p0, Lcom/google/android/libraries/bind/e/a;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 95
    if-eqz v0, :cond_9

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_9

    .line 96
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 98
    :goto_4
    if-nez v0, :cond_7

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/e/a;->d:Landroid/view/LayoutInflater;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, p1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 106
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 107
    iget v3, p0, Lcom/google/android/libraries/bind/e/a;->h:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/libraries/bind/e/a;->h:I

    sget-object v3, Lcom/google/android/libraries/bind/e/a;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v4, "Inflating view of type %s, efficiency: %d %%"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/libraries/bind/e/a;->a(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-direct {p0}, Lcom/google/android/libraries/bind/e/a;->a()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 111
    :goto_5
    invoke-virtual {v0, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    sget-object v3, Lcom/google/android/libraries/bind/e/a;->c:Ljava/util/WeakHashMap;

    if-eqz v3, :cond_6

    .line 113
    sget-object v3, Lcom/google/android/libraries/bind/e/a;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v3, v0, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_8

    :goto_6
    invoke-static {v1}, Lcom/google/android/libraries/bind/d/b;->a(Z)V

    :cond_6
    move-object p2, v0

    .line 115
    goto :goto_3

    .line 101
    :catch_0
    move-exception v0

    .line 103
    sget-object v3, Lcom/google/android/libraries/bind/e/a;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v4, "Failed to inflate view resource: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/libraries/bind/d/b;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Lcom/google/android/libraries/bind/c/b;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    throw v0

    .line 109
    :cond_7
    invoke-direct {p0, p1, v1}, Lcom/google/android/libraries/bind/e/a;->a(IZ)V

    goto :goto_5

    :cond_8
    move v1, v2

    .line 113
    goto :goto_6

    :cond_9
    move-object v0, v4

    goto :goto_4
.end method
