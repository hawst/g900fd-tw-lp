.class public final Lcom/google/android/libraries/bind/data/d;
.super Lcom/google/android/libraries/bind/data/j;
.source "SourceFile"


# static fields
.field private static final f:[I

.field private static g:Landroid/util/SparseIntArray;


# instance fields
.field private h:Lcom/google/android/libraries/bind/data/m;

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/libraries/bind/d;->a:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/libraries/bind/d;->c:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/bind/data/d;->f:[I

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 159
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/d;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 160
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/data/d;->c(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 161
    if-eqz v0, :cond_2

    .line 162
    iget v2, p0, Lcom/google/android/libraries/bind/data/d;->i:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/d;->b:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/m;->b(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 177
    :goto_1
    return v1

    .line 167
    :cond_0
    iget v2, p0, Lcom/google/android/libraries/bind/data/d;->j:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/card/g;

    .line 168
    invoke-interface {v0}, Lcom/google/android/libraries/bind/card/g;->b()Ljava/util/List;

    move-result-object v0

    .line 169
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 170
    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_1

    .line 159
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 177
    :cond_3
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final a(ILandroid/view/View;Lcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
    .locals 8

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/libraries/bind/data/d;->i:I

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 90
    if-nez v0, :cond_2

    .line 93
    iget v0, p0, Lcom/google/android/libraries/bind/data/d;->j:I

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/card/g;

    .line 96
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Missing both view resource ID and view generator"

    invoke-static {v1, v2}, Lcom/google/android/libraries/bind/d/b;->a(ZLjava/lang/String;)V

    .line 98
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/d;->a:Lcom/google/android/libraries/bind/e/a;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/card/g;->a()Landroid/view/View;

    move-result-object v3

    .line 110
    :cond_0
    :goto_1
    return-object v3

    .line 96
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 101
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/d;->a:Lcom/google/android/libraries/bind/e/a;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, p2, v3}, Lcom/google/android/libraries/bind/e/a;->a(ILandroid/view/View;Landroid/view/ViewGroup$LayoutParams;)Landroid/view/View;

    move-result-object v3

    .line 102
    iget v1, p0, Lcom/google/android/libraries/bind/data/d;->k:I

    invoke-virtual {p3, v1}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    .line 103
    instance-of v2, v3, Lcom/google/android/libraries/bind/data/t;

    if-eqz v2, :cond_0

    move-object v2, v3

    .line 104
    check-cast v2, Lcom/google/android/libraries/bind/data/t;

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/d;->h:Lcom/google/android/libraries/bind/data/m;

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/d;->h:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v5, p1}, Lcom/google/android/libraries/bind/data/m;->b(I)Ljava/lang/Object;

    move-result-object v5

    new-instance v6, Lcom/google/android/libraries/bind/data/ae;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v7, p0, Lcom/google/android/libraries/bind/data/d;->i:I

    invoke-direct {v6, v0, v7}, Lcom/google/android/libraries/bind/data/ae;-><init>(II)V

    invoke-virtual {v4, v5, v6, v1}, Lcom/google/android/libraries/bind/data/m;->a(Ljava/lang/Object;Lcom/google/android/libraries/bind/data/v;[I)Lcom/google/android/libraries/bind/data/z;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/android/libraries/bind/data/t;->a(Lcom/google/android/libraries/bind/data/m;)V

    goto :goto_1
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 181
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/d;->c(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 182
    if-nez v0, :cond_0

    move-object v0, v1

    .line 193
    :goto_0
    return-object v0

    .line 185
    :cond_0
    iget v2, p0, Lcom/google/android/libraries/bind/data/d;->i:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 186
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/d;->b:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/m;->b(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 188
    :cond_1
    iget v2, p0, Lcom/google/android/libraries/bind/data/d;->j:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/card/g;

    .line 189
    invoke-interface {v0}, Lcom/google/android/libraries/bind/card/g;->b()Ljava/util/List;

    move-result-object v0

    .line 190
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 191
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 193
    goto :goto_0
.end method

.method public final b(I)I
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 198
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/d;->c(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 200
    if-nez v0, :cond_0

    move v0, v1

    .line 204
    :goto_0
    return v0

    .line 203
    :cond_0
    iget v2, p0, Lcom/google/android/libraries/bind/data/d;->l:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 204
    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/d;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/d;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/google/android/libraries/bind/data/d;->g:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    return v0
.end method
