.class public final Lcom/google/android/libraries/bind/data/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/List;

.field private static final d:Ljava/util/Map;


# instance fields
.field public final a:Ljava/util/List;

.field public final b:I

.field private final e:Ljava/util/Map;

.field private final f:Lcom/google/android/libraries/bind/data/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/data/ak;->c:Ljava/util/List;

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/data/ak;->d:Ljava/util/Map;

    return-void
.end method

.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput p1, p0, Lcom/google/android/libraries/bind/data/ak;->b:I

    .line 78
    sget-object v0, Lcom/google/android/libraries/bind/data/ak;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->a:Ljava/util/List;

    .line 79
    sget-object v0, Lcom/google/android/libraries/bind/data/ak;->d:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->e:Ljava/util/Map;

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->f:Lcom/google/android/libraries/bind/data/l;

    .line 81
    return-void
.end method

.method public constructor <init>(ILcom/google/android/libraries/bind/data/l;)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Lcom/google/android/libraries/bind/data/ak;->b:I

    .line 70
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/ak;->f:Lcom/google/android/libraries/bind/data/l;

    .line 71
    sget-object v0, Lcom/google/android/libraries/bind/data/ak;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->a:Ljava/util/List;

    .line 72
    sget-object v0, Lcom/google/android/libraries/bind/data/ak;->d:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->e:Ljava/util/Map;

    .line 73
    return-void
.end method

.method public constructor <init>(ILjava/util/List;)V
    .locals 1

    .prologue
    .line 40
    invoke-static {p2, p1}, Lcom/google/android/libraries/bind/data/ak;->a(Ljava/util/List;I)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/data/ak;-><init>(ILjava/util/List;Ljava/util/Map;)V

    .line 41
    return-void
.end method

.method private constructor <init>(ILjava/util/List;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/libraries/bind/data/ak;->b:I

    .line 54
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->a:Ljava/util/List;

    .line 55
    iput-object p3, p0, Lcom/google/android/libraries/bind/data/ak;->e:Ljava/util/Map;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->f:Lcom/google/android/libraries/bind/data/l;

    .line 57
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 58
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Data;->a()V

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/List;I)Ljava/util/Map;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 88
    if-nez p0, :cond_0

    .line 89
    sget-object v0, Lcom/google/android/libraries/bind/data/ak;->d:Ljava/util/Map;

    .line 117
    :goto_0
    return-object v0

    .line 91
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 92
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    move v1, v2

    .line 95
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 96
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 97
    if-nez v0, :cond_2

    .line 98
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "Entry %d has no data"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_2
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 101
    if-nez v0, :cond_3

    .line 102
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Entry %d has an empty primary key %s - %s"

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-static {p1}, Lcom/google/android/libraries/bind/data/Data;->f(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/Data;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 105
    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 106
    if-eqz v4, :cond_4

    .line 107
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v5, "Duplicate entries for primary key %s, value %s (class %s), positions %s and %s"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/libraries/bind/data/Data;->f(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    aput-object v0, v6, v8

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v9

    aput-object v4, v6, v10

    const/4 v0, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 95
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_5
    move-object v0, v3

    .line 117
    goto/16 :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 194
    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 155
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/ak;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->f:Lcom/google/android/libraries/bind/data/l;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/libraries/bind/data/l;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->f:Lcom/google/android/libraries/bind/data/l;

    return-object v0
.end method

.method public final c(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/ak;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    const/4 v0, 0x0

    .line 181
    :goto_0
    return-object v0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 181
    iget v1, p0, Lcom/google/android/libraries/bind/data/ak;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->a:Ljava/util/List;

    sget-object v1, Lcom/google/android/libraries/bind/data/ak;->c:Ljava/util/List;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 122
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s - primaryKey: %s, size: %d, exception: %s"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget v4, p0, Lcom/google/android/libraries/bind/data/ak;->b:I

    invoke-static {v4}, Lcom/google/android/libraries/bind/data/Data;->f(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/ak;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/ak;->f:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/l;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "no"

    goto :goto_0
.end method
