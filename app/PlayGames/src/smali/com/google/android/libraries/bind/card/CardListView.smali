.class public Lcom/google/android/libraries/bind/card/CardListView;
.super Landroid/widget/ListView;
.source "SourceFile"


# static fields
.field protected static final a:Z

.field private static final b:Lcom/google/android/libraries/bind/c/b;

.field private static final c:Landroid/view/animation/Interpolator;

.field private static final d:Landroid/view/animation/Interpolator;

.field private static e:Landroid/graphics/Bitmap;

.field private static final p:[I


# instance fields
.field private final f:Landroid/database/DataSetObserver;

.field private final g:Landroid/database/DataSetObserver;

.field private final h:Lcom/google/android/libraries/bind/widget/c;

.field private i:Z

.field private final j:Ljava/util/Map;

.field private final k:Landroid/graphics/RectF;

.field private l:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

.field private m:Z

.field private n:I

.field private o:Landroid/widget/AbsListView$OnScrollListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-class v0, Lcom/google/android/libraries/bind/card/CardListView;

    invoke-static {v0}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Class;)Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/libraries/bind/card/CardListView;->a:Z

    .line 60
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->c:Landroid/view/animation/Interpolator;

    .line 62
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->d:Landroid/view/animation/Interpolator;

    .line 412
    const/4 v0, 0x3

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->p:[I

    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 76
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/card/CardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/card/CardListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    new-instance v0, Lcom/google/android/libraries/bind/widget/c;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/widget/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->h:Lcom/google/android/libraries/bind/widget/c;

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->i:Z

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->j:Ljava/util/Map;

    .line 70
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->k:Landroid/graphics/RectF;

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->n:I

    .line 86
    const v0, 0x106000d

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->setSelector(I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->h:Lcom/google/android/libraries/bind/widget/c;

    invoke-super {p0, v0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 88
    new-instance v0, Lcom/google/android/libraries/bind/card/c;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/card/c;-><init>(Lcom/google/android/libraries/bind/card/CardListView;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->g:Landroid/database/DataSetObserver;

    .line 95
    new-instance v0, Lcom/google/android/libraries/bind/card/d;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/card/d;-><init>(Lcom/google/android/libraries/bind/card/CardListView;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->f:Landroid/database/DataSetObserver;

    .line 107
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 274
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    move-object v1, p2

    .line 275
    :goto_0
    if-eq p0, p1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->offset(FF)V

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p0, v0

    goto :goto_0

    .line 276
    :cond_0
    return-object p2
.end method

.method private a(I)Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    .locals 6

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 514
    instance-of v1, v0, Lcom/google/android/libraries/bind/data/d;

    if-eqz v1, :cond_0

    .line 515
    check-cast v0, Lcom/google/android/libraries/bind/data/d;

    .line 516
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, v1, p1}, Lcom/google/android/libraries/bind/card/CardListView;->pointToPosition(II)I

    move-result v1

    .line 517
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 518
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/d;->a(I)Ljava/lang/Object;

    move-result-object v2

    .line 519
    if-eqz v2, :cond_0

    .line 520
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v3, "Saving state - cardId: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 521
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 522
    new-instance v0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    invoke-super {p0}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-direct {v0, v3, v2, v1}, Lcom/google/android/libraries/bind/card/CardListView$SavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/Object;I)V

    .line 526
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/libraries/bind/card/CardListView;)Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->l:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    return-object v0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/high16 v4, 0x40a00000    # 5.0f

    const-wide/16 v6, 0xfa

    const/4 v2, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    .line 289
    instance-of v0, p1, Lcom/google/android/libraries/bind/data/t;

    if-eqz v0, :cond_9

    move-object v0, p1

    .line 290
    check-cast v0, Lcom/google/android/libraries/bind/data/t;

    .line 291
    invoke-interface {v0}, Lcom/google/android/libraries/bind/data/t;->r()Lcom/google/android/libraries/bind/data/m;

    move-result-object v0

    .line 292
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/m;->c()I

    move-result v1

    if-lez v1, :cond_1

    .line 293
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/bind/data/m;->b(I)Ljava/lang/Object;

    move-result-object v3

    .line 294
    if-eqz p2, :cond_4

    .line 296
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->m:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/libraries/bind/data/f;->a:Lcom/google/android/libraries/bind/data/f;

    move-object v1, v0

    :goto_0
    new-instance v4, Lcom/google/android/libraries/bind/card/e;

    invoke-direct {v4, v2}, Lcom/google/android/libraries/bind/card/e;-><init>(B)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    invoke-static {p1, p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->a(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget v5, v0, Landroid/graphics/RectF;->left:F

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iput v5, v2, Landroid/graphics/Rect;->left:I

    iget v5, v0, Landroid/graphics/RectF;->top:F

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iput v5, v2, Landroid/graphics/Rect;->top:I

    iget v5, v0, Landroid/graphics/RectF;->right:F

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iput v5, v2, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    iput-object v2, v4, Lcom/google/android/libraries/bind/card/e;->a:Landroid/graphics/Rect;

    iput-wide v6, v4, Lcom/google/android/libraries/bind/card/e;->c:J

    instance-of v0, p1, Lcom/google/android/libraries/bind/data/e;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListView;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/bind/data/e;

    sget-object v2, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    iget-object v5, v4, Lcom/google/android/libraries/bind/card/e;->a:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget-object v6, v4, Lcom/google/android/libraries/bind/card/e;->a:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-interface {v0, v2, v5, v6}, Lcom/google/android/libraries/bind/data/e;->a(Landroid/graphics/Bitmap;FF)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    iput-object v1, v4, Lcom/google/android/libraries/bind/card/e;->b:Lcom/google/android/libraries/bind/data/f;

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->j:Ljava/util/Map;

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 302
    invoke-virtual {p1, v11}, Landroid/view/View;->setAlpha(F)V

    .line 303
    invoke-virtual {p1, v10}, Landroid/view/View;->setTranslationX(F)V

    .line 304
    invoke-virtual {p1, v10}, Landroid/view/View;->setTranslationY(F)V

    .line 305
    invoke-virtual {p1, v10}, Landroid/view/View;->setRotation(F)V

    .line 363
    :cond_1
    :goto_2
    return-void

    .line 296
    :cond_2
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 309
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->j:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/card/e;

    .line 310
    if-eqz v0, :cond_8

    .line 311
    iget-object v7, v0, Lcom/google/android/libraries/bind/card/e;->a:Landroid/graphics/Rect;

    .line 314
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardListView;->k:Landroid/graphics/RectF;

    invoke-static {p1, p0, v1}, Lcom/google/android/libraries/bind/card/CardListView;->a(Landroid/view/View;Landroid/view/View;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    .line 315
    invoke-virtual {v7}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardListView;->k:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    sub-float v8, v1, v3

    .line 316
    invoke-virtual {v7}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/libraries/bind/card/CardListView;->k:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    sub-float v9, v1, v3

    .line 317
    iget-object v1, v0, Lcom/google/android/libraries/bind/card/e;->b:Lcom/google/android/libraries/bind/data/f;

    if-eqz v1, :cond_7

    instance-of v1, p1, Lcom/google/android/libraries/bind/data/e;

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    .line 318
    :goto_3
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_5

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_5

    if-eqz v1, :cond_1

    .line 321
    :cond_5
    if-eqz v1, :cond_6

    move-object v1, p1

    .line 322
    check-cast v1, Lcom/google/android/libraries/bind/data/e;

    sget-object v2, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    iget-object v3, v0, Lcom/google/android/libraries/bind/card/e;->a:Landroid/graphics/Rect;

    iget-wide v4, v0, Lcom/google/android/libraries/bind/card/e;->c:J

    iget-object v6, v0, Lcom/google/android/libraries/bind/card/e;->b:Lcom/google/android/libraries/bind/data/f;

    invoke-interface/range {v1 .. v6}, Lcom/google/android/libraries/bind/data/e;->a(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/f;)V

    .line 324
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    .line 325
    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    .line 327
    :cond_6
    invoke-virtual {p1, v8}, Landroid/view/View;->setTranslationX(F)V

    .line 328
    invoke-virtual {p1, v9}, Landroid/view/View;->setTranslationY(F)V

    .line 329
    invoke-virtual {p1, v10}, Landroid/view/View;->setRotation(F)V

    .line 330
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v10}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v11}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    iget-wide v2, v0, Lcom/google/android/libraries/bind/card/e;->c:J

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/bind/card/CardListView;->c:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 338
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->a(Landroid/view/ViewParent;)V

    goto/16 :goto_2

    :cond_7
    move v1, v2

    .line 317
    goto :goto_3

    .line 342
    :cond_8
    invoke-virtual {p1, v10}, Landroid/view/View;->setAlpha(F)V

    .line 343
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/bind/card/CardListView;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 350
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 351
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_2

    .line 356
    :cond_9
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 357
    check-cast p1, Landroid/view/ViewGroup;

    .line 358
    :goto_4
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 359
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 360
    invoke-direct {p0, v0, p2}, Lcom/google/android/libraries/bind/card/CardListView;->a(Landroid/view/View;Z)V

    .line 358
    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method

.method private a(Landroid/view/ViewParent;)V
    .locals 3

    .prologue
    .line 389
    move-object v1, p1

    :goto_0
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 390
    check-cast v0, Landroid/view/ViewGroup;

    .line 391
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 392
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    .line 393
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    if-eq v1, p0, :cond_0

    move-object v1, p1

    .line 394
    goto :goto_0

    .line 397
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/libraries/bind/card/CardListView$SavedState;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 531
    iget-object v1, p1, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->a:Ljava/lang/Object;

    .line 532
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 533
    instance-of v2, v0, Lcom/google/android/libraries/bind/data/d;

    if-eqz v2, :cond_0

    .line 534
    check-cast v0, Lcom/google/android/libraries/bind/data/d;

    .line 535
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/d;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 536
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/d;->a(Ljava/lang/Object;)I

    move-result v0

    .line 537
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 538
    iget v2, p1, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->b:I

    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/bind/card/CardListView;->setSelectionFromTop(II)V

    .line 539
    sget-object v2, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v3, "Restoring for cardId %s to position %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 546
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardListView;->l:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 543
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v1, "Stashing restore state"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private d()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 225
    invoke-static {}, Lcom/google/android/libraries/bind/d/b;->a()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 226
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 227
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 229
    :cond_0
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    .line 232
    :cond_1
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 233
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    .line 236
    :cond_2
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->e:Landroid/graphics/Bitmap;

    return-object v0

    .line 225
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Lcom/google/android/libraries/bind/card/CardListView$SavedState;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 496
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 497
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/card/CardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-nez v0, :cond_0

    .line 499
    invoke-direct {p0, v1}, Lcom/google/android/libraries/bind/card/CardListView;->a(I)Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    .line 507
    :goto_0
    return-object v0

    .line 501
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getHeight()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 504
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->a(I)Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    goto :goto_0

    .line 507
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->a(I)Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 147
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v1, "captureCardPositions"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v0, Lcom/google/android/libraries/bind/card/CardListView;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/libraries/bind/widget/d;->a(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v1, "Skipping capture since we\'re offscreen"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v1, "Skipping capture since we\'re offscreen"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v1, "capturing"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->m:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListView;->d()Landroid/graphics/Bitmap;

    :cond_3
    const/4 v0, 0x1

    invoke-direct {p0, p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->a(Landroid/view/View;Z)V

    iput-boolean v3, p0, Lcom/google/android/libraries/bind/card/CardListView;->m:Z

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 480
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListView;->e()Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->l:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 481
    return-void
.end method

.method final c()V
    .locals 4

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->l:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->l:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 552
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/libraries/bind/card/CardListView;->l:Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 553
    sget-object v1, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v2, "Trying to restore stashed state"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 554
    invoke-direct {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->a(Lcom/google/android/libraries/bind/card/CardListView$SavedState;)V

    .line 556
    :cond_0
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 216
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v1, "animateTransition"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v0, Lcom/google/android/libraries/bind/card/CardListView;->a:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p0, v3}, Lcom/google/android/libraries/bind/card/CardListView;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    sget-object v0, Lcom/google/android/libraries/bind/a/f;->a:Lcom/google/android/libraries/bind/a/f;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/a/f;->d()V

    .line 217
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 218
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Landroid/widget/ListView;->onDetachedFromWindow()V

    .line 210
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/card/CardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->h:Lcom/google/android/libraries/bind/widget/c;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/c;->a()Lcom/google/android/libraries/bind/widget/c;

    .line 212
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v10, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 447
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 448
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/libraries/bind/data/d;

    if-eqz v1, :cond_5

    check-cast v0, Lcom/google/android/libraries/bind/data/d;

    move v1, v2

    move v3, v2

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v5

    if-ge v1, v5, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/d;->b(I)I

    move-result v5

    sget-object v6, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v7, "position %d, count: %d"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-virtual {v6, v7, v8}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    add-int/2addr v3, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/google/android/libraries/bind/card/CardListView;->p:[I

    aput v3, v1, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getFirstVisiblePosition()I

    move-result v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getLastVisiblePosition()I

    move-result v5

    if-ge v1, v5, :cond_1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/d;->b(I)I

    move-result v5

    sget-object v6, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v7, "position %d, count: %d"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-virtual {v6, v7, v8}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    add-int/2addr v3, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/google/android/libraries/bind/card/CardListView;->p:[I

    aput v3, v1, v4

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getLastVisiblePosition()I

    move-result v1

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/d;->getCount()I

    move-result v5

    if-ge v1, v5, :cond_2

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/d;->b(I)I

    move-result v5

    sget-object v6, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v7, "position %d, count: %d"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-virtual {v6, v7, v8}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    add-int/2addr v3, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->p:[I

    aput v3, v0, v10

    move v0, v4

    :goto_3
    if-eqz v0, :cond_4

    .line 449
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->p:[I

    aget v0, v0, v2

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 450
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    .line 451
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->p:[I

    aget v0, v0, v4

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 453
    :cond_3
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->p:[I

    aget v0, v0, v10

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 455
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 448
    goto :goto_3
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 465
    sget-object v0, Lcom/google/android/libraries/bind/card/CardListView;->b:Lcom/google/android/libraries/bind/c/b;

    const-string v1, "onRestoreInstanceState"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 466
    instance-of v0, p1, Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    if-eqz v0, :cond_0

    .line 467
    check-cast p1, Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    .line 468
    invoke-virtual {p1}, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 469
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/card/CardListView;->a(Lcom/google/android/libraries/bind/card/CardListView$SavedState;)V

    .line 473
    :goto_0
    return-void

    .line 471
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 459
    invoke-direct {p0}, Lcom/google/android/libraries/bind/card/CardListView;->e()Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    move-result-object v0

    .line 460
    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 47
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/card/CardListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/card/CardListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_0

    .line 158
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardListView;->g:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 159
    iget-boolean v1, p0, Lcom/google/android/libraries/bind/card/CardListView;->i:Z

    if-eqz v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardListView;->f:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 163
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 164
    if-eqz p1, :cond_1

    .line 165
    check-cast p1, Lcom/google/android/libraries/bind/data/j;

    .line 168
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->g:Landroid/database/DataSetObserver;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/j;->a(Landroid/database/DataSetObserver;I)V

    .line 170
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->i:Z

    if-eqz v0, :cond_1

    .line 173
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->f:Landroid/database/DataSetObserver;

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/libraries/bind/data/j;->a(Landroid/database/DataSetObserver;I)V

    .line 177
    :cond_1
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 2

    .prologue
    .line 183
    if-nez p1, :cond_1

    .line 184
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->o:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->o:Landroid/widget/AbsListView$OnScrollListener;

    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardListView;->h:Lcom/google/android/libraries/bind/widget/c;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/widget/c;->b(Landroid/widget/AbsListView$OnScrollListener;)Lcom/google/android/libraries/bind/widget/c;

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->o:Landroid/widget/AbsListView$OnScrollListener;

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->o:Landroid/widget/AbsListView$OnScrollListener;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/libraries/bind/d/b;->a(Z)V

    .line 190
    iput-object p1, p0, Lcom/google/android/libraries/bind/card/CardListView;->o:Landroid/widget/AbsListView$OnScrollListener;

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView;->o:Landroid/widget/AbsListView$OnScrollListener;

    iget-object v1, p0, Lcom/google/android/libraries/bind/card/CardListView;->h:Lcom/google/android/libraries/bind/widget/c;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/widget/c;->a(Landroid/widget/AbsListView$OnScrollListener;)Lcom/google/android/libraries/bind/widget/c;

    goto :goto_0

    .line 189
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
