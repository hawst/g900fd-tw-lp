.class final Lcom/google/android/libraries/bind/data/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 132
    const-class v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/libraries/bind/d/a;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    const-class v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment$SavedState;

    new-instance v2, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    invoke-direct {v2, v1, v0}, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;-><init>(Ljava/lang/Object;Landroid/support/v4/app/Fragment$SavedState;)V

    return-object v2
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132
    new-array v0, p1, [Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    return-object v0
.end method
