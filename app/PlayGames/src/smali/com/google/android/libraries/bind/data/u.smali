.class public Lcom/google/android/libraries/bind/data/u;
.super Lcom/google/android/libraries/bind/data/o;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/libraries/bind/data/m;

.field public b:Z

.field public c:Z

.field private final d:Lcom/google/android/libraries/bind/data/t;

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/t;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/o;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/u;->e:Z

    .line 26
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/t;

    .line 27
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/bind/data/m;->b(Lcom/google/android/libraries/bind/data/o;)V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/u;->f:Z

    .line 141
    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/libraries/bind/data/Data;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/m;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/m;->a(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/libraries/bind/data/k;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 67
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/m;->c()I

    move-result v1

    if-gt v1, v0, :cond_1

    :cond_0
    :goto_0
    const-string v1, "Passed DataList with more than one row."

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/d/b;->a(ZLjava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/t;

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/u;->a()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/bind/data/t;->b(Lcom/google/android/libraries/bind/data/Data;)V

    .line 70
    return-void

    .line 67
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/libraries/bind/data/m;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    if-ne p1, v0, :cond_0

    .line 42
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/u;->f:Z

    if-eqz v0, :cond_1

    .line 37
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/u;->d()V

    .line 39
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    .line 40
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/u;->c()V

    .line 41
    sget-object v0, Lcom/google/android/libraries/bind/data/k;->a:Lcom/google/android/libraries/bind/data/k;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/u;->a(Lcom/google/android/libraries/bind/data/k;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/u;->c:Z

    .line 106
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/u;->c()V

    .line 107
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 121
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/u;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/u;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/u;->e:Z

    if-nez v0, :cond_3

    :cond_1
    move v0, v1

    .line 122
    :goto_0
    if-eqz v0, :cond_4

    .line 123
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/u;->f:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    if-eqz v0, :cond_2

    .line 124
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/bind/data/m;->a(Lcom/google/android/libraries/bind/data/o;)V

    iput-boolean v1, p0, Lcom/google/android/libraries/bind/data/u;->f:Z

    .line 131
    :cond_2
    :goto_1
    return-void

    .line 121
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 127
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/u;->f:Z

    if-eqz v0, :cond_2

    .line 128
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/u;->d()V

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 54
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "View type: %s, hasData: %b, registered: %b, attached: %b,temporarilyDetached: %b, clearDataOnDetach: %b"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/t;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    if-eqz v5, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/u;->f:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/u;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/u;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/u;->e:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
