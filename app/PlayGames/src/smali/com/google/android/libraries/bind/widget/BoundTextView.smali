.class public Lcom/google/android/libraries/bind/widget/BoundTextView;
.super Landroid/widget/TextView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/bind/data/g;


# instance fields
.field private final a:Lcom/google/android/libraries/bind/data/h;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/widget/BoundTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/widget/BoundTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/libraries/bind/widget/BoundTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Lcom/google/android/libraries/bind/data/h;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/libraries/bind/data/h;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->a:Lcom/google/android/libraries/bind/data/h;

    .line 45
    sget-object v0, Lcom/google/android/libraries/bind/e;->i:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 47
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->isInEditMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    sget v1, Lcom/google/android/libraries/bind/e;->n:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 50
    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    :cond_0
    sget v1, Lcom/google/android/libraries/bind/e;->l:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->b:Ljava/lang/Integer;

    .line 55
    sget v1, Lcom/google/android/libraries/bind/e;->m:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->c:Ljava/lang/Integer;

    .line 56
    sget v1, Lcom/google/android/libraries/bind/e;->k:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->d:Ljava/lang/Integer;

    .line 57
    sget v1, Lcom/google/android/libraries/bind/e;->j:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/h;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->e:Ljava/lang/Integer;

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    return-void
.end method

.method private a([Landroid/graphics/drawable/Drawable;Lcom/google/android/libraries/bind/data/Data;Ljava/lang/Integer;Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 111
    if-eqz p3, :cond_0

    .line 112
    if-nez p2, :cond_1

    move-object v0, v1

    .line 114
    :goto_0
    invoke-static {p0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v2

    if-nez v2, :cond_2

    if-eqz p4, :cond_2

    const/4 v2, 0x0

    .line 116
    :goto_1
    if-nez v0, :cond_3

    .line 118
    :goto_2
    aput-object v1, p1, v2

    .line 120
    :cond_0
    return-void

    .line 112
    :cond_1
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/bind/data/Data;->d(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_2
    const/4 v2, 0x2

    goto :goto_1

    .line 116
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_2
.end method


# virtual methods
.method public final a_(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 68
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->a:Lcom/google/android/libraries/bind/data/h;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/h;->a(Lcom/google/android/libraries/bind/data/Data;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 71
    if-nez p1, :cond_4

    move-object v0, v1

    move-object v2, p0

    .line 80
    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 86
    if-nez p1, :cond_7

    move-object v0, v1

    .line 87
    :goto_2
    if-nez v0, :cond_8

    .line 88
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setTextColor(I)V

    .line 100
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->d:Ljava/lang/Integer;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 101
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->d:Ljava/lang/Integer;

    invoke-direct {p0, v0, p1, v1, v4}, Lcom/google/android/libraries/bind/widget/BoundTextView;->a([Landroid/graphics/drawable/Drawable;Lcom/google/android/libraries/bind/data/Data;Ljava/lang/Integer;Z)V

    .line 103
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->e:Ljava/lang/Integer;

    invoke-direct {p0, v0, p1, v1, v3}, Lcom/google/android/libraries/bind/widget/BoundTextView;->a([Landroid/graphics/drawable/Drawable;Lcom/google/android/libraries/bind/data/Data;Ljava/lang/Integer;Z)V

    .line 104
    aget-object v1, v0, v3

    aget-object v2, v0, v4

    const/4 v3, 0x2

    aget-object v3, v0, v3

    const/4 v4, 0x3

    aget-object v0, v0, v4

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 107
    :cond_3
    return-void

    .line 74
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 77
    instance-of v2, v0, Landroid/text/SpannableString;

    if-eqz v2, :cond_5

    .line 78
    check-cast v0, Landroid/text/SpannableString;

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    goto :goto_1

    .line 80
    :cond_5
    if-nez v0, :cond_6

    move-object v0, v1

    move-object v2, p0

    goto :goto_0

    :cond_6
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, p0

    goto :goto_0

    .line 86
    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundTextView;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_2

    .line 89
    :cond_8
    instance-of v1, v0, Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_9

    .line 90
    check-cast v0, Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_3

    .line 92
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/widget/BoundTextView;->setTextColor(I)V

    goto :goto_3
.end method
