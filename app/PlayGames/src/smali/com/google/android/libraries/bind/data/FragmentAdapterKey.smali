.class public Lcom/google/android/libraries/bind/data/FragmentAdapterKey;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/libraries/bind/data/ab;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/ab;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->a:Ljava/lang/Object;

    .line 17
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 21
    instance-of v0, p1, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    if-eqz v0, :cond_0

    .line 22
    check-cast p1, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;

    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->a:Ljava/lang/Object;

    iget-object v1, p1, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->a:Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/d/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 25
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 40
    const-string v0, "key: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->a:Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentAdapterKey;->a:Ljava/lang/Object;

    invoke-static {v0, p1, p2}, Lcom/google/android/libraries/bind/d/a;->a(Ljava/lang/Object;Landroid/os/Parcel;I)V

    .line 46
    return-void
.end method
