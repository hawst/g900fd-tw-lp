.class Lcom/google/android/libraries/bind/card/CardListView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 584
    new-instance v0, Lcom/google/android/libraries/bind/card/f;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/card/f;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 598
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 599
    const-class v0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/libraries/bind/d/a;->a(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->a:Ljava/lang/Object;

    .line 600
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->b:I

    .line 601
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 561
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/card/CardListView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 566
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 567
    iput-object p2, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->a:Ljava/lang/Object;

    .line 568
    iput p3, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->b:I

    .line 569
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 573
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 574
    iget-object v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->a:Ljava/lang/Object;

    invoke-static {v0, p1, p2}, Lcom/google/android/libraries/bind/d/a;->a(Ljava/lang/Object;Landroid/os/Parcel;I)V

    .line 575
    iget v0, p0, Lcom/google/android/libraries/bind/card/CardListView$SavedState;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 576
    return-void
.end method
