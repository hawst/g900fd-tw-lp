.class public final Lcom/google/android/libraries/bind/data/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/libraries/bind/data/k;

.field public static final b:Lcom/google/android/libraries/bind/data/k;

.field public static final c:Lcom/google/android/libraries/bind/data/k;


# instance fields
.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Lcom/google/android/libraries/bind/data/l;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 7
    new-instance v0, Lcom/google/android/libraries/bind/data/k;

    invoke-direct {v0, v2, v2}, Lcom/google/android/libraries/bind/data/k;-><init>(ZZ)V

    sput-object v0, Lcom/google/android/libraries/bind/data/k;->a:Lcom/google/android/libraries/bind/data/k;

    .line 8
    new-instance v0, Lcom/google/android/libraries/bind/data/k;

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/data/k;-><init>(ZZ)V

    sput-object v0, Lcom/google/android/libraries/bind/data/k;->b:Lcom/google/android/libraries/bind/data/k;

    .line 9
    new-instance v0, Lcom/google/android/libraries/bind/data/k;

    invoke-direct {v0, v1, v1}, Lcom/google/android/libraries/bind/data/k;-><init>(ZZ)V

    sput-object v0, Lcom/google/android/libraries/bind/data/k;->c:Lcom/google/android/libraries/bind/data/k;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/data/l;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/k;->d:Z

    .line 32
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/k;->e:Z

    .line 33
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/k;->f:Z

    .line 34
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/k;->g:Lcom/google/android/libraries/bind/data/l;

    .line 35
    return-void
.end method

.method private constructor <init>(ZZ)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/data/k;->d:Z

    .line 39
    iput-boolean p2, p0, Lcom/google/android/libraries/bind/data/k;->e:Z

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/k;->f:Z

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/k;->g:Lcom/google/android/libraries/bind/data/l;

    .line 42
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 46
    const-string v0, "isInvalidation: %b, affectsPrimaryKey: %b, exception: %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/libraries/bind/data/k;->d:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/libraries/bind/data/k;->e:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/k;->g:Lcom/google/android/libraries/bind/data/l;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
