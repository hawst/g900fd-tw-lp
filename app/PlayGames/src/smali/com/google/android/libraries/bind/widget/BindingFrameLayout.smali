.class public Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.super Lcom/google/android/libraries/bind/widget/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/bind/data/e;


# instance fields
.field protected final c:Lcom/google/android/libraries/bind/widget/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 25
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    new-instance v0, Lcom/google/android/libraries/bind/widget/a;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/widget/a;-><init>(Lcom/google/android/libraries/bind/data/t;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    .line 35
    sget-object v0, Lcom/google/android/libraries/bind/e;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    sget v2, Lcom/google/android/libraries/bind/e;->b:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/widget/a;->a(Z)V

    .line 38
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    sget v2, Lcom/google/android/libraries/bind/e;->c:I

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/widget/a;->b(Z)V

    .line 40
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/f;)V
    .locals 7

    .prologue
    .line 142
    iget-object v1, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/libraries/bind/widget/a;->a(Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/google/android/libraries/bind/data/f;)V

    .line 143
    return-void
.end method

.method public final a(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/widget/b;->draw(Landroid/graphics/Canvas;)V

    .line 161
    return-void
.end method

.method public final a(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 0

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->a_(Lcom/google/android/libraries/bind/data/Data;)V

    .line 156
    return-void
.end method

.method public final a(Lcom/google/android/libraries/bind/data/m;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/a;->a(Lcom/google/android/libraries/bind/data/m;)V

    .line 51
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;FF)Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/a;->a(Landroid/graphics/Bitmap;FF)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/a;->a(Lcom/google/android/libraries/bind/data/Data;)V

    .line 122
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/widget/a;->a(Landroid/graphics/Canvas;)V

    .line 127
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/a;->d()V

    .line 77
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/a;->f()Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/a;->e()Z

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/b;->onAttachedToWindow()V

    .line 104
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/libraries/bind/data/u;->b:Z

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/libraries/bind/data/u;->c:Z

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/u;->c()V

    .line 105
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/b;->onDetachedFromWindow()V

    .line 98
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    iput-boolean v1, v0, Lcom/google/android/libraries/bind/data/u;->b:Z

    iput-boolean v1, v0, Lcom/google/android/libraries/bind/data/u;->c:Z

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/u;->c()V

    .line 99
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/b;->onFinishInflate()V

    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/a;->g()V

    .line 93
    return-void
.end method

.method public onFinishTemporaryDetach()V
    .locals 1

    .prologue
    .line 115
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/b;->onFinishTemporaryDetach()V

    .line 116
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/a;->b()V

    .line 117
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    iget-boolean v1, v0, Lcom/google/android/libraries/bind/data/u;->c:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/u;->b()V

    .line 171
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/widget/b;->onMeasure(II)V

    .line 172
    return-void
.end method

.method public onStartTemporaryDetach()V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/android/libraries/bind/widget/b;->onStartTemporaryDetach()V

    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/libraries/bind/data/u;->c:Z

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/u;->c()V

    .line 111
    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/widget/a;->a(Z)V

    .line 132
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 165
    invoke-virtual {p0, v0, v0}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->setMeasuredDimension(II)V

    .line 166
    return-void
.end method

.method public final r()Lcom/google/android/libraries/bind/data/m;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/m;

    return-object v0
.end method

.method public final s()Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;->c:Lcom/google/android/libraries/bind/widget/a;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/widget/a;->a()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method
