.class public final Lcom/google/android/libraries/bind/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Executor;


# static fields
.field public static final a:Lcom/google/android/libraries/bind/a/f;

.field private static final b:Z


# instance fields
.field private final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private final d:Ljava/util/concurrent/locks/Condition;

.field private e:Z

.field private final f:Lcom/google/android/libraries/bind/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/libraries/bind/a/f;->b:Z

    .line 20
    new-instance v0, Lcom/google/android/libraries/bind/a/f;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/a/f;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/a/f;->a:Lcom/google/android/libraries/bind/a/f;

    return-void

    .line 17
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 23
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/f;->d:Ljava/util/concurrent/locks/Condition;

    .line 25
    new-instance v0, Lcom/google/android/libraries/bind/a/d;

    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->c()Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/bind/a/g;

    invoke-direct {v2, p0}, Lcom/google/android/libraries/bind/a/g;-><init>(Lcom/google/android/libraries/bind/a/f;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/a/d;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/f;->f:Lcom/google/android/libraries/bind/a/d;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 41
    sget-boolean v0, Lcom/google/android/libraries/bind/a/f;->b:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 46
    :goto_1
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/a/f;->e:Z

    if-eqz v0, :cond_2

    .line 47
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->awaitUninterruptibly()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 50
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 74
    sget-boolean v0, Lcom/google/android/libraries/bind/a/f;->b:Z

    if-eqz v0, :cond_0

    .line 84
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 80
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/a/f;->e:Z

    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 88
    sget-boolean v0, Lcom/google/android/libraries/bind/a/f;->b:Z

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 93
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/a/f;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    iget-object v1, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 103
    sget-boolean v0, Lcom/google/android/libraries/bind/a/f;->b:Z

    if-eqz v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    sget-boolean v0, Lcom/google/android/libraries/bind/a/f;->b:Z

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/a/f;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/f;->f:Lcom/google/android/libraries/bind/a/d;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/a/d;->a(J)Z

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/a/f;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final execute(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 163
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->d()Ljava/util/concurrent/Executor;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/bind/a/o;->a:Lcom/google/android/libraries/bind/a/j;

    new-instance v2, Lcom/google/android/libraries/bind/a/h;

    invoke-direct {v2, p0, v1, p1, v0}, Lcom/google/android/libraries/bind/a/h;-><init>(Lcom/google/android/libraries/bind/a/f;Ljava/util/concurrent/Executor;Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/a/f;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 164
    :goto_0
    return-void

    .line 163
    :cond_0
    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
