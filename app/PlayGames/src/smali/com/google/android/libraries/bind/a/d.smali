.class public final Lcom/google/android/libraries/bind/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Ljava/lang/Runnable;

.field c:J

.field private final d:Landroid/os/Handler;

.field private final e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p2, p0, Lcom/google/android/libraries/bind/a/d;->b:Ljava/lang/Runnable;

    .line 28
    iput-object p1, p0, Lcom/google/android/libraries/bind/a/d;->d:Landroid/os/Handler;

    .line 29
    new-instance v0, Lcom/google/android/libraries/bind/a/e;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/a/e;-><init>(Lcom/google/android/libraries/bind/a/d;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/d;->e:Ljava/lang/Runnable;

    .line 38
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/d;->e:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/google/android/libraries/bind/a/d;->a:Ljava/lang/Object;

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/libraries/bind/a/d;->c:J

    .line 40
    return-void
.end method

.method private b(J)V
    .locals 5

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/google/android/libraries/bind/a/d;->c:J

    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/d;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/bind/a/d;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/bind/a/d;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/libraries/bind/a/d;->e:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/google/android/libraries/bind/a/d;->c:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/libraries/bind/a/d;->c:J

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(J)Z
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 46
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    add-long v4, v2, p1

    .line 47
    iget-object v3, p0, Lcom/google/android/libraries/bind/a/d;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 48
    :try_start_0
    iget-wide v6, p0, Lcom/google/android/libraries/bind/a/d;->c:J

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 49
    invoke-direct {p0, v4, v5}, Lcom/google/android/libraries/bind/a/d;->b(J)V

    .line 50
    monitor-exit v3

    .line 57
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 48
    goto :goto_0

    .line 52
    :cond_1
    iget-wide v6, p0, Lcom/google/android/libraries/bind/a/d;->c:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    .line 55
    invoke-direct {p0, v4, v5}, Lcom/google/android/libraries/bind/a/d;->b(J)V

    .line 57
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    goto :goto_1

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method
