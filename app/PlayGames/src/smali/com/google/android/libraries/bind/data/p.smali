.class public abstract Lcom/google/android/libraries/bind/data/p;
.super Landroid/support/v4/view/ao;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/bind/b/a;


# instance fields
.field protected final a:Lcom/google/android/libraries/bind/e/a;

.field public final b:Lcom/google/android/libraries/bind/data/o;

.field public c:Lcom/google/android/libraries/bind/data/m;

.field private final d:Ljava/util/Map;

.field private final e:Lcom/google/android/libraries/bind/data/r;

.field private final f:Lcom/google/android/libraries/bind/data/r;

.field private g:Ljava/lang/Integer;

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/e/a;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v4/view/ao;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/p;->d:Ljava/util/Map;

    .line 27
    new-instance v0, Lcom/google/android/libraries/bind/data/r;

    sget-object v1, Lcom/google/android/libraries/bind/data/al;->b:Lcom/google/android/libraries/bind/data/al;

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/bind/data/r;-><init>(Lcom/google/android/libraries/bind/data/p;Lcom/google/android/libraries/bind/data/al;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/p;->e:Lcom/google/android/libraries/bind/data/r;

    .line 29
    new-instance v0, Lcom/google/android/libraries/bind/data/r;

    sget-object v1, Lcom/google/android/libraries/bind/data/al;->c:Lcom/google/android/libraries/bind/data/al;

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/bind/data/r;-><init>(Lcom/google/android/libraries/bind/data/p;Lcom/google/android/libraries/bind/data/al;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/p;->f:Lcom/google/android/libraries/bind/data/r;

    .line 37
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/p;->a:Lcom/google/android/libraries/bind/e/a;

    .line 38
    new-instance v0, Lcom/google/android/libraries/bind/data/q;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/bind/data/q;-><init>(Lcom/google/android/libraries/bind/data/p;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/p;->b:Lcom/google/android/libraries/bind/data/o;

    .line 46
    return-void
.end method

.method private c(I)Z
    .locals 1

    .prologue
    .line 106
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/m;->c()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final J_()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/p;->h:Z

    return v0
.end method

.method public final a(I)Landroid/view/View;
    .locals 2

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/p;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 206
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    if-nez v0, :cond_0

    .line 207
    const/4 v0, 0x0

    .line 215
    :goto_0
    return-object v0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/m;->e:Lcom/google/android/libraries/bind/data/ak;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->f:Lcom/google/android/libraries/bind/data/r;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/r;->a:Landroid/view/View;

    goto :goto_0

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->e:Lcom/google/android/libraries/bind/data/r;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/r;->a:Landroid/view/View;

    goto :goto_0

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/m;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/p;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method public abstract a(Landroid/view/ViewGroup;ILcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 111
    invoke-direct {p0, p2}, Lcom/google/android/libraries/bind/data/p;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/m;->e:Lcom/google/android/libraries/bind/data/ak;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ak;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->f:Lcom/google/android/libraries/bind/data/r;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/data/r;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->e:Lcom/google/android/libraries/bind/data/r;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/bind/data/r;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_2
    invoke-static {p0, p2}, Lcom/google/android/libraries/bind/b/c;->a(Landroid/support/v4/view/ao;I)I

    move-result v0

    .line 121
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/data/m;->a(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/libraries/bind/data/p;->a(Landroid/view/ViewGroup;ILcom/google/android/libraries/bind/data/Data;)Landroid/view/View;

    move-result-object v1

    .line 122
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 123
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/bind/data/m;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 124
    sget v2, Lcom/google/android/libraries/bind/c;->g:I

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 125
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/p;->d:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 86
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->e:Lcom/google/android/libraries/bind/data/r;

    if-ne p2, v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->e:Lcom/google/android/libraries/bind/data/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/r;->a(Landroid/view/ViewGroup;)V

    .line 142
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->f:Lcom/google/android/libraries/bind/data/r;

    if-ne p2, v0, :cond_1

    .line 136
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->f:Lcom/google/android/libraries/bind/data/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/r;->a(Landroid/view/ViewGroup;)V

    goto :goto_0

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->d:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 140
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 141
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/p;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/p;->h:Z

    if-eq v0, p1, :cond_0

    .line 56
    iput-boolean p1, p0, Lcom/google/android/libraries/bind/data/p;->h:Z

    .line 57
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/p;->d()V

    .line 59
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->e:Lcom/google/android/libraries/bind/data/r;

    if-ne p2, v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->e:Lcom/google/android/libraries/bind/data/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/r;->a(Landroid/view/View;)Z

    move-result v0

    .line 102
    :goto_0
    return v0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->f:Lcom/google/android/libraries/bind/data/r;

    if-ne p2, v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->f:Lcom/google/android/libraries/bind/data/r;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/r;->a(Landroid/view/View;)Z

    move-result v0

    goto :goto_0

    .line 102
    :cond_1
    sget v0, Lcom/google/android/libraries/bind/c;->g:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/libraries/bind/d/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/p;->c(Ljava/lang/Object;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/google/android/libraries/bind/b/c;->b(Landroid/support/v4/view/ao;I)I

    move-result v0

    return v0
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/p;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 224
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ao;->b(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    .line 226
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/libraries/bind/b/c;->a(Landroid/support/v4/view/ao;I)I

    move-result v0

    .line 227
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/data/m;->a(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/p;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->c(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 91
    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/m;->c()I

    move-result v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x2

    .line 187
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    if-nez v2, :cond_1

    .line 201
    :cond_0
    :goto_0
    return v0

    .line 190
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/p;->e:Lcom/google/android/libraries/bind/data/r;

    if-ne p1, v2, :cond_2

    .line 191
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/m;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    iget-object v2, v2, Lcom/google/android/libraries/bind/data/m;->e:Lcom/google/android/libraries/bind/data/ak;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/ak;->b()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 193
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/p;->f:Lcom/google/android/libraries/bind/data/r;

    if-ne p1, v2, :cond_3

    .line 194
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/m;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    iget-object v2, v2, Lcom/google/android/libraries/bind/data/m;->e:Lcom/google/android/libraries/bind/data/ak;

    invoke-virtual {v2}, Lcom/google/android/libraries/bind/data/ak;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 196
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v1, p1}, Lcom/google/android/libraries/bind/data/m;->a(Ljava/lang/Object;)I

    move-result v1

    .line 197
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    move v0, v1

    .line 201
    goto :goto_0
.end method
