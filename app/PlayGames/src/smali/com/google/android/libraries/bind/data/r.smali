.class public final Lcom/google/android/libraries/bind/data/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/view/View;

.field b:Lcom/google/android/libraries/bind/data/al;

.field final synthetic c:Lcom/google/android/libraries/bind/data/p;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/bind/data/p;Lcom/google/android/libraries/bind/data/al;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/r;->c:Lcom/google/android/libraries/bind/data/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/r;->b:Lcom/google/android/libraries/bind/data/al;

    .line 317
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 320
    if-lez p2, :cond_0

    .line 321
    const/4 p0, 0x0

    .line 326
    :goto_0
    return-object p0

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/r;->b:Lcom/google/android/libraries/bind/data/al;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/r;->c:Lcom/google/android/libraries/bind/data/p;

    iget-object v1, v1, Lcom/google/android/libraries/bind/data/p;->a:Lcom/google/android/libraries/bind/e/a;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/bind/data/al;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/r;->a:Landroid/view/View;

    .line 324
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/r;->a:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/r;->a:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/r;->a:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 331
    return-void
.end method

.method public final a(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/r;->a:Landroid/view/View;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
