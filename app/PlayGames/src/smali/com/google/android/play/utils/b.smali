.class public final Lcom/google/android/play/utils/b;
.super Landroid/animation/Animator;
.source "SourceFile"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field a:Ljava/util/ArrayList;

.field private b:Landroid/animation/Animator;


# direct methods
.method public constructor <init>(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/animation/Animator;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    .line 28
    iput-object p1, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    .line 29
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0, p0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 30
    return-void
.end method

.method private a()Lcom/google/android/play/utils/b;
    .locals 6

    .prologue
    .line 78
    invoke-super {p0}, Landroid/animation/Animator;->clone()Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/utils/b;

    .line 79
    iget-object v1, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 80
    iget-object v2, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    .line 82
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 83
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 84
    iget-object v4, v0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 87
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final addListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method

.method public final addPauseListener(Landroid/animation/Animator$AnimatorPauseListener;)V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public final cancel()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 103
    return-void
.end method

.method public final synthetic clone()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/play/utils/b;->a()Lcom/google/android/play/utils/b;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/play/utils/b;->a()Lcom/google/android/play/utils/b;

    move-result-object v0

    return-object v0
.end method

.method public final end()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 98
    return-void
.end method

.method public final getDuration()J
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getInterpolator()Landroid/animation/TimeInterpolator;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v0

    return-object v0
.end method

.method public final getListeners()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getStartDelay()J
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->getStartDelay()J

    move-result-wide v0

    return-wide v0
.end method

.method public final isPaused()Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isPaused()Z

    move-result v0

    return v0
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    return v0
.end method

.method public final isStarted()Z
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    return v0
.end method

.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 183
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 184
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 185
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 184
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 187
    :cond_0
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 194
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 195
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 196
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 195
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 198
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 205
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 206
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 207
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationRepeat(Landroid/animation/Animator;)V

    .line 206
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 209
    :cond_0
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 216
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 217
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 218
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/animation/Animator$AnimatorListener;

    invoke-interface {v1, p0}, Landroid/animation/Animator$AnimatorListener;->onAnimationStart(Landroid/animation/Animator;)V

    .line 217
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 220
    :cond_0
    return-void
.end method

.method public final pause()V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method public final removeAllListeners()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    .line 141
    :cond_0
    return-void
.end method

.method public final removeListener(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 149
    iget-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/utils/b;->a:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public final removePauseListener(Landroid/animation/Animator$AnimatorPauseListener;)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method public final resume()V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public final setDuration(J)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 50
    return-object p0
.end method

.method public final setInterpolator(Landroid/animation/TimeInterpolator;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0, p1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 56
    return-void
.end method

.method public final setStartDelay(J)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 61
    return-void
.end method

.method public final setTarget(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0, p1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method public final setupEndValues()V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->setupEndValues()V

    .line 172
    return-void
.end method

.method public final setupStartValues()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->setupStartValues()V

    .line 167
    return-void
.end method

.method public final start()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/play/utils/b;->b:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 93
    return-void
.end method
