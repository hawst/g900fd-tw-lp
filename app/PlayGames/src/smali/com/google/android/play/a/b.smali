.class public final Lcom/google/android/play/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static a:Landroid/view/animation/Interpolator;

.field private static b:Landroid/view/animation/Interpolator;


# direct methods
.method public static a(Landroid/content/Context;)Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/play/a/b;->a:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_0

    .line 23
    const v0, 0x10c000d

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/a/b;->a:Landroid/view/animation/Interpolator;

    .line 26
    :cond_0
    sget-object v0, Lcom/google/android/play/a/b;->a:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/play/a/b;->b:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_0

    .line 31
    const v0, 0x10c000f

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/a/b;->b:Landroid/view/animation/Interpolator;

    .line 34
    :cond_0
    sget-object v0, Lcom/google/android/play/a/b;->b:Landroid/view/animation/Interpolator;

    return-object v0
.end method
