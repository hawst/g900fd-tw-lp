.class public final Lcom/google/android/play/c/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# direct methods
.method public static a(Landroid/content/Context;Landroid/view/View;Landroid/view/View;ZZ)Landroid/transition/TransitionSet;
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 56
    new-instance v0, Lcom/google/android/play/c/j;

    invoke-direct {v0, p4}, Lcom/google/android/play/c/j;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/c/j;->a(Z)Lcom/google/android/play/c/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/play/c/j;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v3

    .line 59
    new-instance v0, Landroid/transition/ArcMotion;

    invoke-direct {v0}, Landroid/transition/ArcMotion;-><init>()V

    invoke-virtual {v3, v0}, Landroid/transition/Transition;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 61
    new-instance v4, Lcom/google/android/play/c/c;

    if-eqz p4, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {v4, v0}, Lcom/google/android/play/c/c;-><init>(I)V

    invoke-virtual {v4, p2}, Lcom/google/android/play/c/c;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 65
    new-instance v4, Landroid/transition/ChangeBounds;

    invoke-direct {v4}, Landroid/transition/ChangeBounds;-><init>()V

    invoke-virtual {v4, p2}, Landroid/transition/ChangeBounds;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v4

    .line 69
    const/4 v5, 0x3

    new-array v5, v5, [Landroid/transition/Transition;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    aput-object v0, v5, v1

    aput-object v4, v5, v2

    invoke-static {v5}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v1

    if-eqz p4, :cond_1

    invoke-static {p0}, Lcom/google/android/play/a/b;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v2

    if-eqz p4, :cond_2

    const-wide/16 v0, 0x1c2

    :goto_2
    invoke-virtual {v2, v0, v1}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 61
    goto :goto_0

    .line 69
    :cond_1
    invoke-static {p0}, Lcom/google/android/play/a/b;->b(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v0

    goto :goto_1

    :cond_2
    const-wide/16 v0, 0x15e

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;[Ljava/lang/Class;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/TransitionSet;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 99
    invoke-virtual {p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->k()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    sget v1, Lcom/google/android/play/f;->at:I

    invoke-virtual {p2, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    new-instance v2, Landroid/transition/Fade;

    invoke-direct {v2}, Landroid/transition/Fade;-><init>()V

    invoke-virtual {v2, v0}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    .line 101
    if-eqz p3, :cond_0

    .line 102
    new-array v1, v3, [Landroid/transition/Transition;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/play/a/b;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v0

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    .line 106
    :cond_0
    new-instance v1, Landroid/transition/TransitionSet;

    invoke-direct {v1}, Landroid/transition/TransitionSet;-><init>()V

    .line 107
    invoke-virtual {v1, v0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/play/a/b;->b(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v0

    goto :goto_0
.end method
