.class public Lcom/google/android/play/search/PlaySearchPlate;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchPlate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/play/search/d;)V
    .locals 3

    .prologue
    .line 32
    sget v0, Lcom/google/android/play/f;->I:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/PlaySearchNavigationButton;

    .line 34
    sget v1, Lcom/google/android/play/f;->av:I

    invoke-virtual {p0, v1}, Lcom/google/android/play/search/PlaySearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/search/PlaySearchPlateTextContainer;

    .line 36
    sget v2, Lcom/google/android/play/f;->e:I

    invoke-virtual {p0, v2}, Lcom/google/android/play/search/PlaySearchPlate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/search/PlaySearchActionButton;

    .line 38
    invoke-virtual {v0, p1}, Lcom/google/android/play/search/PlaySearchNavigationButton;->a(Lcom/google/android/play/search/d;)V

    .line 39
    invoke-virtual {v1, p1}, Lcom/google/android/play/search/PlaySearchPlateTextContainer;->a(Lcom/google/android/play/search/d;)V

    .line 40
    invoke-virtual {v2, p1}, Lcom/google/android/play/search/PlaySearchActionButton;->a(Lcom/google/android/play/search/d;)V

    .line 41
    return-void
.end method
