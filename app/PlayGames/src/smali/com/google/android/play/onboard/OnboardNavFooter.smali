.class public Lcom/google/android/play/onboard/OnboardNavFooter;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/widget/TextView;

.field protected final b:Landroid/widget/TextView;

.field protected final c:Lcom/google/android/play/widget/PageIndicator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/play/h;->f:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/c;->t:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->setBackgroundColor(I)V

    .line 48
    sget v0, Lcom/google/android/play/f;->am:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->a:Landroid/widget/TextView;

    .line 49
    sget v0, Lcom/google/android/play/f;->n:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->b:Landroid/widget/TextView;

    .line 50
    sget v0, Lcom/google/android/play/f;->J:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/widget/PageIndicator;

    iput-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->c:Lcom/google/android/play/widget/PageIndicator;

    .line 51
    return-void
.end method

.method private a(Landroid/widget/TextView;Lcom/google/android/play/onboard/n;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-virtual {p2}, Lcom/google/android/play/onboard/n;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 69
    invoke-virtual {p2}, Lcom/google/android/play/onboard/n;->c()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 70
    invoke-virtual {p2}, Lcom/google/android/play/onboard/n;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    invoke-virtual {p2}, Lcom/google/android/play/onboard/n;->e()I

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v2

    .line 73
    :goto_1
    iget-object v3, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->a:Landroid/widget/TextView;

    if-ne p1, v3, :cond_2

    .line 74
    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/google/android/play/utils/d;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V

    .line 78
    :goto_2
    invoke-virtual {p2}, Lcom/google/android/play/onboard/n;->f()Ljava/lang/Runnable;

    move-result-object v0

    .line 79
    if-nez v0, :cond_3

    .line 86
    :goto_3
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardNavFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/play/onboard/n;->e()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 76
    :cond_2
    invoke-static {p1, v0, v1}, Lcom/google/android/play/utils/d;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_2

    .line 79
    :cond_3
    new-instance v2, Lcom/google/android/play/onboard/m;

    invoke-direct {v2, p0, v0}, Lcom/google/android/play/onboard/m;-><init>(Lcom/google/android/play/onboard/OnboardNavFooter;Ljava/lang/Runnable;)V

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/google/android/play/onboard/d;Lcom/google/android/play/onboard/p;)V
    .locals 2

    .prologue
    .line 58
    invoke-interface {p2}, Lcom/google/android/play/onboard/p;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/OnboardNavFooter;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->c:Lcom/google/android/play/widget/PageIndicator;

    invoke-interface {p2}, Lcom/google/android/play/onboard/p;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/widget/PageIndicator;->a(I)V

    .line 61
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->c:Lcom/google/android/play/widget/PageIndicator;

    invoke-interface {p2}, Lcom/google/android/play/onboard/p;->j()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/widget/PageIndicator;->b(I)V

    .line 63
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->a:Landroid/widget/TextView;

    invoke-interface {p2, p1}, Lcom/google/android/play/onboard/p;->b(Lcom/google/android/play/onboard/d;)Lcom/google/android/play/onboard/n;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooter;->a(Landroid/widget/TextView;Lcom/google/android/play/onboard/n;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/play/onboard/OnboardNavFooter;->b:Landroid/widget/TextView;

    invoke-interface {p2, p1}, Lcom/google/android/play/onboard/p;->c(Lcom/google/android/play/onboard/d;)Lcom/google/android/play/onboard/n;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooter;->a(Landroid/widget/TextView;Lcom/google/android/play/onboard/n;)V

    .line 65
    return-void

    .line 58
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
