.class public final Lcom/google/android/play/image/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:I

.field private static c:I


# instance fields
.field private final a:Lcom/android/b/f;

.field private final d:Lcom/google/android/play/image/b;

.field private final e:Ljava/util/HashMap;

.field private final f:Ljava/util/HashMap;

.field private g:Lcom/google/android/play/image/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/high16 v0, 0x300000

    sput v0, Lcom/google/android/play/image/e;->b:I

    .line 51
    const/4 v0, 0x6

    sput v0, Lcom/google/android/play/image/e;->c:I

    return-void
.end method

.method protected static a(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Lcom/android/b/i;Lcom/android/b/h;)Lcom/google/android/play/image/k;
    .locals 7

    .prologue
    .line 326
    new-instance v0, Lcom/google/android/play/image/k;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/play/image/k;-><init>(Ljava/lang/String;IILandroid/graphics/Bitmap$Config;Lcom/android/b/i;Lcom/android/b/h;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/play/image/e;)Lcom/google/android/play/image/s;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/e;->g:Lcom/google/android/play/image/s;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/play/image/e;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/e;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/play/image/e;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/play/image/e;->f:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;IILcom/google/android/play/image/j;)Lcom/google/android/play/image/i;
    .locals 14

    .prologue
    .line 336
    if-gtz p2, :cond_0

    if-lez p3, :cond_f

    :cond_0
    invoke-static/range {p1 .. p3}, Lcom/google/android/play/image/q;->a(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v5

    :goto_0
    new-instance v1, Lcom/google/android/play/image/f;

    move-object v2, p0

    move/from16 v3, p2

    move/from16 v4, p3

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/play/image/f;-><init>(Lcom/google/android/play/image/e;IILjava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v1, Lcom/google/android/play/image/i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v2, p0

    move/from16 v6, p2

    move/from16 v7, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/play/image/i;-><init>(Lcom/google/android/play/image/e;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/j;)V

    :goto_1
    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/play/image/e;->d:Lcom/google/android/play/image/b;

    iget-object v2, v2, Lcom/google/android/play/image/b;->a:Lcom/google/android/play/image/d;

    invoke-virtual {v2, p1}, Lcom/google/android/play/image/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez v2, :cond_3

    const/4 v3, 0x0

    :cond_2
    :goto_2
    if-eqz v3, :cond_b

    iget v2, v3, Lcom/google/android/play/image/c;->b:I

    move/from16 v0, p2

    if-ne v2, v0, :cond_b

    iget v2, v3, Lcom/google/android/play/image/c;->c:I

    move/from16 v0, p3

    if-ne v2, v0, :cond_b

    new-instance v1, Lcom/google/android/play/image/i;

    iget-object v3, v3, Lcom/google/android/play/image/c;->a:Landroid/graphics/Bitmap;

    const/4 v8, 0x0

    move-object v2, p0

    move-object v4, p1

    move/from16 v6, p2

    move/from16 v7, p3

    invoke-direct/range {v1 .. v8}, Lcom/google/android/play/image/i;-><init>(Lcom/google/android/play/image/e;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/j;)V

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_7

    const/4 v3, 0x1

    move v8, v3

    :goto_3
    if-eqz p2, :cond_8

    const/4 v3, 0x1

    move v4, v3

    :goto_4
    const/4 v6, 0x0

    const/4 v3, 0x0

    move v7, v3

    :goto_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v7, v3, :cond_9

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/image/c;

    iget v9, v3, Lcom/google/android/play/image/c;->b:I

    move/from16 v0, p2

    if-ne v9, v0, :cond_4

    iget v9, v3, Lcom/google/android/play/image/c;->c:I

    move/from16 v0, p3

    if-eq v9, v0, :cond_2

    :cond_4
    if-nez v6, :cond_e

    if-eqz v8, :cond_5

    iget-object v9, v3, Lcom/google/android/play/image/c;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    move/from16 v0, p2

    if-lt v9, v0, :cond_e

    :cond_5
    if-eqz v4, :cond_6

    iget-object v9, v3, Lcom/google/android/play/image/c;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    move/from16 v0, p3

    if-lt v9, v0, :cond_e

    :cond_6
    :goto_6
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    move-object v6, v3

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    move v8, v3

    goto :goto_3

    :cond_8
    const/4 v3, 0x0

    move v4, v3

    goto :goto_4

    :cond_9
    if-eqz v6, :cond_a

    move-object v3, v6

    goto :goto_2

    :cond_a
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/image/c;

    move-object v3, v2

    goto :goto_2

    :cond_b
    const/4 v8, 0x0

    if-eqz v3, :cond_c

    iget-object v8, v3, Lcom/google/android/play/image/c;->a:Landroid/graphics/Bitmap;

    :cond_c
    new-instance v6, Lcom/google/android/play/image/i;

    move-object v7, p0

    move-object v9, p1

    move-object v10, v5

    move/from16 v11, p2

    move/from16 v12, p3

    move-object/from16 v13, p4

    invoke-direct/range {v6 .. v13}, Lcom/google/android/play/image/i;-><init>(Lcom/google/android/play/image/e;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/j;)V

    iget-object v2, p0, Lcom/google/android/play/image/e;->e:Ljava/util/HashMap;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/image/m;

    if-eqz v2, :cond_d

    iget-object v1, v2, Lcom/google/android/play/image/m;->a:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v6

    goto/16 :goto_1

    :cond_d
    invoke-interface {v1}, Lcom/google/android/play/image/l;->a()Lcom/android/b/d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/play/image/e;->a:Lcom/android/b/f;

    invoke-virtual {v2, v1}, Lcom/android/b/f;->a(Lcom/android/b/d;)Lcom/android/b/d;

    iget-object v2, p0, Lcom/google/android/play/image/e;->e:Ljava/util/HashMap;

    new-instance v3, Lcom/google/android/play/image/m;

    invoke-direct {v3, p0, v1, v6}, Lcom/google/android/play/image/m;-><init>(Lcom/google/android/play/image/e;Lcom/android/b/d;Lcom/google/android/play/image/i;)V

    invoke-virtual {v2, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v6

    goto/16 :goto_1

    :cond_e
    move-object v3, v6

    goto :goto_6

    :cond_f
    move-object v5, p1

    goto/16 :goto_0
.end method
