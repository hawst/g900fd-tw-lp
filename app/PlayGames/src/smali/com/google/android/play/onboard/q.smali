.class public Lcom/google/android/play/onboard/q;
.super Lcom/google/android/libraries/bind/data/p;
.source "SourceFile"


# static fields
.field public static final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/google/android/play/f;->U:I

    sput v0, Lcom/google/android/play/onboard/q;->e:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/bind/e/a;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/data/p;-><init>(Lcom/google/android/libraries/bind/e/a;)V

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;ILcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
    .locals 8

    .prologue
    .line 38
    sget v0, Lcom/google/android/libraries/bind/data/c;->a:I

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/bind/data/Data;->d(I)Ljava/lang/Integer;

    move-result-object v3

    .line 39
    if-nez v3, :cond_2

    .line 41
    sget v0, Lcom/google/android/play/onboard/q;->e:I

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/onboard/r;

    .line 42
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Missing both view resource ID and page generator"

    invoke-static {v1, v2}, Lcom/google/android/libraries/bind/d/b;->a(ZLjava/lang/String;)V

    .line 44
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/play/onboard/r;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v2

    .line 56
    :cond_0
    :goto_1
    return-object v2

    .line 42
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 47
    :cond_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 48
    instance-of v0, v2, Lcom/google/android/libraries/bind/data/t;

    if-eqz v0, :cond_0

    .line 49
    sget v0, Lcom/google/android/libraries/bind/data/c;->c:I

    invoke-virtual {p3, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    move-object v1, v2

    .line 50
    check-cast v1, Lcom/google/android/libraries/bind/data/t;

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    invoke-virtual {v5, p2}, Lcom/google/android/libraries/bind/data/m;->b(I)Ljava/lang/Object;

    move-result-object v5

    new-instance v6, Lcom/google/android/libraries/bind/data/ae;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget v7, Lcom/google/android/libraries/bind/data/c;->a:I

    invoke-direct {v6, v3, v7}, Lcom/google/android/libraries/bind/data/ae;-><init>(II)V

    invoke-virtual {v4, v5, v6, v0}, Lcom/google/android/libraries/bind/data/m;->a(Ljava/lang/Object;Lcom/google/android/libraries/bind/data/v;[I)Lcom/google/android/libraries/bind/data/z;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/libraries/bind/data/t;->a(Lcom/google/android/libraries/bind/data/m;)V

    goto :goto_1
.end method

.method public final c(I)Lcom/google/android/play/onboard/p;
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/google/android/play/onboard/q;->a(I)Landroid/view/View;

    move-result-object v0

    .line 61
    instance-of v1, v0, Lcom/google/android/play/onboard/o;

    if-eqz v1, :cond_0

    .line 62
    check-cast v0, Lcom/google/android/play/onboard/o;

    invoke-interface {v0}, Lcom/google/android/play/onboard/o;->b()Lcom/google/android/play/onboard/p;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
