.class public Lcom/google/android/play/image/FifeImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/image/j;


# static fields
.field private static c:Z

.field private static d:Z


# instance fields
.field a:Lcom/google/android/play/image/e;

.field b:Z

.field private e:Lcom/google/android/play/image/p;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Landroid/graphics/drawable/Drawable;

.field private final i:Landroid/graphics/Rect;

.field private j:Z

.field private k:I

.field private l:I

.field private m:Z

.field private n:F

.field private final o:Landroid/graphics/PointF;

.field private final p:Landroid/graphics/Matrix;

.field private q:Z

.field private r:I

.field private s:F

.field private t:Lcom/google/android/play/image/n;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/image/FifeImageView;->c:Z

    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/play/image/FifeImageView;->d:Z

    return-void

    :cond_0
    move v0, v2

    .line 43
    goto :goto_0

    :cond_1
    move v1, v2

    .line 45
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/image/FifeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/image/FifeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 93
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/FifeImageView;->i:Landroid/graphics/Rect;

    .line 56
    iput-boolean v6, p0, Lcom/google/android/play/image/FifeImageView;->j:Z

    .line 64
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/play/image/FifeImageView;->o:Landroid/graphics/PointF;

    .line 65
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/image/FifeImageView;->p:Landroid/graphics/Matrix;

    .line 95
    sget-object v0, Lcom/google/android/play/k;->p:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 97
    sget v1, Lcom/google/android/play/k;->q:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/image/FifeImageView;->q:Z

    .line 99
    sget v1, Lcom/google/android/play/k;->r:I

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/image/FifeImageView;->v:Z

    .line 101
    sget v1, Lcom/google/android/play/k;->u:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/play/image/FifeImageView;->m:Z

    .line 102
    iget-boolean v1, p0, Lcom/google/android/play/image/FifeImageView;->m:Z

    if-eqz v1, :cond_4

    .line 103
    sget v1, Lcom/google/android/play/k;->u:I

    invoke-virtual {v0, v1, v5, v5, v3}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v1

    iput v1, p0, Lcom/google/android/play/image/FifeImageView;->n:F

    .line 104
    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-super {p0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 109
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 110
    sget v2, Lcom/google/android/play/k;->s:I

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 111
    invoke-static {v1}, Lcom/google/android/play/image/a;->a(Landroid/content/res/Resources;)Lcom/google/android/play/image/a;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/play/image/FifeImageView;->t:Lcom/google/android/play/image/n;

    .line 114
    :cond_0
    sget v2, Lcom/google/android/play/k;->t:I

    invoke-virtual {v0, v2, v5, v5, v3}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v2

    iput v2, p0, Lcom/google/android/play/image/FifeImageView;->s:F

    .line 117
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 119
    new-array v0, v5, [I

    const v2, 0x1010109

    aput v2, v0, v6

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 121
    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 122
    if-eqz v2, :cond_3

    .line 123
    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    if-eq v3, v2, :cond_3

    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v3}, Lcom/google/android/play/image/FifeImageView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iput-object v2, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_5

    invoke-virtual {p0, v6}, Lcom/google/android/play/image/FifeImageView;->setWillNotDraw(Z)V

    invoke-virtual {v2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getDrawableState()[I

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->invalidate()V

    .line 125
    :cond_3
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 127
    const/high16 v0, 0x10e0000

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/FifeImageView;->r:I

    .line 129
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/play/image/FifeImageView;->x:Landroid/os/Handler;

    .line 130
    return-void

    .line 106
    :cond_4
    iput v3, p0, Lcom/google/android/play/image/FifeImageView;->n:F

    goto :goto_0

    .line 123
    :cond_5
    invoke-virtual {p0, v5}, Lcom/google/android/play/image/FifeImageView;->setWillNotDraw(Z)V

    goto :goto_1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 642
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->v:Z

    if-eqz v0, :cond_0

    .line 643
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->u:Z

    .line 645
    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;Z)V
    .locals 1

    .prologue
    .line 435
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->w:Z

    if-eqz v0, :cond_0

    .line 440
    :goto_0
    return-void

    .line 438
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 439
    invoke-direct {p0, p2}, Lcom/google/android/play/image/FifeImageView;->a(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;Z)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/image/FifeImageView;->a(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/play/image/FifeImageView;->b:Z

    .line 200
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->e:Lcom/google/android/play/image/p;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->e:Lcom/google/android/play/image/p;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    :cond_0
    monitor-exit p0

    return-void

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b()V
    .locals 14

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v13, 0x40000000    # 2.0f

    const/4 v12, 0x0

    .line 648
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 649
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    .line 650
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    .line 651
    if-eqz v0, :cond_0

    cmpl-float v3, v1, v12

    if-eqz v3, :cond_0

    cmpl-float v3, v2, v12

    if-nez v3, :cond_1

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 655
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    int-to-float v3, v3

    .line 656
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    .line 657
    cmpg-float v4, v3, v12

    if-lez v4, :cond_2

    cmpg-float v4, v0, v12

    if-gtz v4, :cond_3

    .line 660
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->p:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 688
    :goto_1
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->p:Landroid/graphics/Matrix;

    invoke-super {p0, v0}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 664
    :cond_3
    div-float v4, v1, v3

    div-float v5, v2, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 669
    div-float v5, v1, v4

    sub-float v5, v3, v5

    invoke-static {v5, v12}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 670
    div-float v4, v2, v4

    sub-float v4, v0, v4

    invoke-static {v4, v12}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 673
    iget-object v6, p0, Lcom/google/android/play/image/FifeImageView;->o:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    mul-float/2addr v6, v5

    .line 674
    sub-float/2addr v5, v6

    .line 675
    iget-object v7, p0, Lcom/google/android/play/image/FifeImageView;->o:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    mul-float/2addr v7, v4

    .line 676
    sub-float/2addr v4, v7

    .line 679
    iget v8, p0, Lcom/google/android/play/image/FifeImageView;->n:F

    sub-float/2addr v8, v10

    mul-float/2addr v8, v3

    .line 680
    iget v9, p0, Lcom/google/android/play/image/FifeImageView;->n:F

    sub-float/2addr v9, v10

    mul-float/2addr v9, v0

    .line 681
    new-instance v10, Landroid/graphics/RectF;

    div-float v11, v8, v13

    add-float/2addr v6, v11

    div-float v11, v9, v13

    add-float/2addr v7, v11

    sub-float/2addr v3, v5

    div-float v5, v8, v13

    sub-float/2addr v3, v5

    sub-float/2addr v0, v4

    div-float v4, v9, v13

    sub-float/2addr v0, v4

    invoke-direct {v10, v6, v7, v3, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 684
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v12, v12, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 686
    iget-object v1, p0, Lcom/google/android/play/image/FifeImageView;->p:Landroid/graphics/Matrix;

    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v10, v0, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    goto :goto_1
.end method


# virtual methods
.method public drawableHotspotChanged(FF)V
    .locals 1

    .prologue
    .line 525
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->drawableHotspotChanged(FF)V

    .line 527
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/Drawable;->setHotspot(FF)V

    .line 530
    :cond_0
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 514
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 515
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->t:Lcom/google/android/play/image/n;

    if-eqz v0, :cond_1

    .line 519
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->invalidate()V

    .line 521
    :cond_1
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 506
    invoke-super {p0}, Landroid/widget/ImageView;->jumpDrawablesToCurrentState()V

    .line 507
    sget-boolean v0, Lcom/google/android/play/image/FifeImageView;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 510
    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 544
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 546
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v1

    .line 547
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v2

    .line 549
    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->t:Lcom/google/android/play/image/n;

    if-nez v3, :cond_2

    .line 550
    iget-object v1, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/play/image/FifeImageView;->j:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->j:Z

    iget-object v1, p0, Lcom/google/android/play/image/FifeImageView;->i:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 562
    :cond_1
    :goto_0
    return-void

    .line 554
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->isDuplicateParentStateEnabled()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->isClickable()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 555
    :cond_4
    if-eqz v0, :cond_5

    .line 556
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->t:Lcom/google/android/play/image/n;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/play/image/n;->b(Landroid/graphics/Canvas;II)V

    .line 559
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 560
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->t:Lcom/google/android/play/image/n;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/play/image/n;->a(Landroid/graphics/Canvas;II)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 492
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 493
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->w:Z

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v3

    if-nez v0, :cond_0

    if-eqz v3, :cond_6

    :cond_0
    iput v2, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    iput v2, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v0, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v0, v5, :cond_7

    move v0, v1

    :goto_0
    iget v3, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v3, v5, :cond_8

    move v3, v1

    :goto_1
    if-eqz v0, :cond_1

    if-nez v3, :cond_b

    :cond_1
    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->t:Lcom/google/android/play/image/n;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->t:Lcom/google/android/play/image/n;

    iget v3, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    iget v4, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    invoke-interface {v0, v3, v4}, Lcom/google/android/play/image/n;->a(II)I

    move-result v0

    :goto_3
    iget v3, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    if-lez v3, :cond_3

    iget v3, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    sub-int/2addr v3, v0

    iput v3, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    :cond_3
    iget v3, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    if-lez v3, :cond_4

    iget v3, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    sub-int v0, v3, v0

    iput v0, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/play/image/q;->a(Landroid/content/Context;)F

    move-result v0

    iget v3, p0, Lcom/google/android/play/image/FifeImageView;->s:F

    mul-float/2addr v3, v0

    iget v4, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    iget v3, p0, Lcom/google/android/play/image/FifeImageView;->s:F

    mul-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    int-to-float v3, v3

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    if-gtz v0, :cond_d

    iget v0, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    if-gtz v0, :cond_d

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/i;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/google/android/play/image/i;->a()V

    invoke-virtual {p0, v7}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 495
    :cond_6
    :goto_4
    iput-boolean v1, p0, Lcom/google/android/play/image/FifeImageView;->j:Z

    .line 496
    return-void

    :cond_7
    move v0, v2

    .line 493
    goto :goto_0

    :cond_8
    move v3, v2

    goto :goto_1

    :cond_9
    iget v0, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lez v0, :cond_a

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    iget v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v0, :cond_2

    iget v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v0, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    goto :goto_2

    :cond_a
    if-eqz v3, :cond_b

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    goto :goto_2

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    goto/16 :goto_2

    :cond_c
    move v0, v2

    goto :goto_3

    :cond_d
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->g:Z

    if-eqz v0, :cond_14

    iget v3, p0, Lcom/google/android/play/image/FifeImageView;->k:I

    iget v0, p0, Lcom/google/android/play/image/FifeImageView;->l:I

    move v4, v3

    move v3, v0

    :goto_5
    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/i;

    if-eqz v0, :cond_f

    iget-object v5, v0, Lcom/google/android/play/image/i;->b:Ljava/lang/String;

    if-eqz v5, :cond_f

    iget-object v5, v0, Lcom/google/android/play/image/i;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/play/image/FifeImageView;->f:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    iget v5, v0, Lcom/google/android/play/image/i;->c:I

    if-ne v5, v4, :cond_e

    iget v5, v0, Lcom/google/android/play/image/i;->d:I

    if-eq v5, v3, :cond_6

    :cond_e
    invoke-virtual {v0}, Lcom/google/android/play/image/i;->a()V

    :cond_f
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->a:Lcom/google/android/play/image/e;

    iget-object v5, p0, Lcom/google/android/play/image/FifeImageView;->f:Ljava/lang/String;

    invoke-virtual {v0, v5, v4, v3, p0}, Lcom/google/android/play/image/e;->a(Ljava/lang/String;IILcom/google/android/play/image/j;)Lcom/google/android/play/image/i;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/image/FifeImageView;->setTag(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/google/android/play/image/i;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_13

    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->t:Lcom/google/android/play/image/n;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->t:Lcom/google/android/play/image/n;

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v5

    invoke-interface {v3, v0, v4, v5}, Lcom/google/android/play/image/n;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_10
    if-eqz v0, :cond_11

    move v2, v1

    :cond_11
    iget-boolean v3, p0, Lcom/google/android/play/image/FifeImageView;->v:Z

    if-eqz v3, :cond_12

    invoke-direct {p0, v0, v2}, Lcom/google/android/play/image/FifeImageView;->a(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_4

    :cond_12
    iget-object v3, p0, Lcom/google/android/play/image/FifeImageView;->x:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/play/image/o;

    invoke-direct {v4, p0, v0, v2}, Lcom/google/android/play/image/o;-><init>(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;Z)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_4

    :cond_13
    invoke-virtual {p0, v7}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_4

    :cond_14
    move v3, v2

    move v4, v2

    goto :goto_5
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 606
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 607
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->m:Z

    if-eqz v0, :cond_0

    .line 608
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->b()V

    .line 611
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->j:Z

    .line 612
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 623
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->u:Z

    if-nez v0, :cond_0

    .line 624
    invoke-super {p0}, Landroid/widget/ImageView;->requestLayout()V

    .line 626
    :cond_0
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 586
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->a()V

    .line 587
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 588
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->u:Z

    .line 589
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->m:Z

    if-eqz v0, :cond_0

    .line 590
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->b()V

    .line 592
    :cond_0
    return-void
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->m:Z

    if-eqz v0, :cond_0

    .line 309
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t mix scale type and custom zoom"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 311
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 312
    return-void
.end method

.method public setImageResource(I)V
    .locals 1

    .prologue
    .line 596
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->a()V

    .line 597
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 598
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->u:Z

    .line 599
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->m:Z

    if-eqz v0, :cond_0

    .line 600
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->b()V

    .line 602
    :cond_0
    return-void
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 616
    invoke-direct {p0}, Lcom/google/android/play/image/FifeImageView;->a()V

    .line 617
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 618
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->u:Z

    .line 619
    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 2

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/google/android/play/image/FifeImageView;->m:Z

    if-eqz v0, :cond_0

    .line 301
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can\'t mix scale type and custom zoom"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 304
    return-void
.end method

.method public setVisibility(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 534
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 536
    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 537
    if-nez p1, :cond_1

    const/4 v0, 0x1

    .line 538
    :goto_0
    iget-object v2, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 540
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 537
    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 500
    invoke-super {p0, p1}, Landroid/widget/ImageView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/image/FifeImageView;->h:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
