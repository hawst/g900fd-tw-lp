.class public final Lcom/google/android/play/image/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/graphics/Bitmap;

.field final b:Ljava/lang/String;

.field final c:I

.field final d:I

.field final synthetic e:Lcom/google/android/play/image/e;

.field private f:Lcom/google/android/play/image/j;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/play/image/e;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/play/image/j;)V
    .locals 0

    .prologue
    .line 409
    iput-object p1, p0, Lcom/google/android/play/image/i;->e:Lcom/google/android/play/image/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410
    iput-object p2, p0, Lcom/google/android/play/image/i;->a:Landroid/graphics/Bitmap;

    .line 411
    iput-object p3, p0, Lcom/google/android/play/image/i;->b:Ljava/lang/String;

    .line 412
    iput-object p4, p0, Lcom/google/android/play/image/i;->g:Ljava/lang/String;

    .line 413
    iput p5, p0, Lcom/google/android/play/image/i;->c:I

    .line 414
    iput p6, p0, Lcom/google/android/play/image/i;->d:I

    .line 415
    iput-object p7, p0, Lcom/google/android/play/image/i;->f:Lcom/google/android/play/image/j;

    .line 416
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/play/image/i;->f:Lcom/google/android/play/image/j;

    if-nez v0, :cond_1

    .line 442
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/image/i;->e:Lcom/google/android/play/image/e;

    invoke-static {v0}, Lcom/google/android/play/image/e;->b(Lcom/google/android/play/image/e;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/image/i;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/m;

    .line 427
    if-eqz v0, :cond_2

    .line 428
    invoke-virtual {v0, p0}, Lcom/google/android/play/image/m;->a(Lcom/google/android/play/image/i;)Z

    move-result v0

    .line 429
    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/google/android/play/image/i;->e:Lcom/google/android/play/image/e;

    invoke-static {v0}, Lcom/google/android/play/image/e;->b(Lcom/google/android/play/image/e;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/image/i;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 434
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/image/i;->e:Lcom/google/android/play/image/e;

    invoke-static {v0}, Lcom/google/android/play/image/e;->c(Lcom/google/android/play/image/e;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/image/i;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/m;

    .line 435
    if-eqz v0, :cond_0

    .line 436
    invoke-virtual {v0, p0}, Lcom/google/android/play/image/m;->a(Lcom/google/android/play/image/i;)Z

    .line 437
    iget-object v0, v0, Lcom/google/android/play/image/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/google/android/play/image/i;->e:Lcom/google/android/play/image/e;

    invoke-static {v0}, Lcom/google/android/play/image/e;->c(Lcom/google/android/play/image/e;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/image/i;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
