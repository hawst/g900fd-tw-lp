.class Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/play/image/p;


# instance fields
.field private a:Lcom/google/android/play/image/FifeImageView;

.field private b:Lcom/google/android/play/image/FifeImageView;

.field private c:Lcom/google/android/play/image/FifeImageView;

.field private d:Lcom/google/android/play/image/FifeImageView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/view/View;

.field private i:Landroid/accounts/Account;

.field private j:Landroid/accounts/Account;

.field private k:Landroid/accounts/Account;

.field private l:Lcom/google/android/play/drawer/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 254
    invoke-super {p0}, Landroid/widget/FrameLayout;->drawableStateChanged()V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->invalidate()V

    .line 256
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->l:Lcom/google/android/play/drawer/c;

    if-nez v0, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->b:Lcom/google/android/play/image/FifeImageView;

    if-ne p1, v0, :cond_2

    .line 202
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->l:Lcom/google/android/play/drawer/c;

    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->i:Landroid/accounts/Account;

    goto :goto_0

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->c:Lcom/google/android/play/image/FifeImageView;

    if-ne p1, v0, :cond_3

    .line 204
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->l:Lcom/google/android/play/drawer/c;

    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->j:Landroid/accounts/Account;

    goto :goto_0

    .line 205
    :cond_3
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->d:Lcom/google/android/play/image/FifeImageView;

    if-ne p1, v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->l:Lcom/google/android/play/drawer/c;

    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->k:Landroid/accounts/Account;

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 76
    sget v0, Lcom/google/android/play/f;->l:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->a:Lcom/google/android/play/image/FifeImageView;

    .line 77
    sget v0, Lcom/google/android/play/f;->h:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->b:Lcom/google/android/play/image/FifeImageView;

    .line 78
    sget v0, Lcom/google/android/play/f;->aj:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->c:Lcom/google/android/play/image/FifeImageView;

    .line 79
    sget v0, Lcom/google/android/play/f;->ak:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->d:Lcom/google/android/play/image/FifeImageView;

    .line 80
    sget v0, Lcom/google/android/play/f;->m:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->e:Landroid/widget/TextView;

    .line 81
    sget v0, Lcom/google/android/play/f;->b:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->f:Landroid/widget/TextView;

    .line 82
    sget v0, Lcom/google/android/play/f;->aw:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->g:Landroid/widget/ImageView;

    .line 83
    sget v0, Lcom/google/android/play/f;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->h:Landroid/view/View;

    .line 85
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->b:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->c:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/play/drawer/PlayDrawerProfileInfoView;->d:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    return-void
.end method
