.class public Lcom/google/android/play/layout/StarRatingBar;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private a:F

.field private b:F

.field private c:F

.field private d:F

.field private final e:D

.field private f:D

.field private g:D

.field private h:[Landroid/graphics/PointF;

.field private i:Landroid/graphics/Path;

.field private j:Landroid/graphics/Path;

.field private k:Landroid/graphics/Path;

.field private l:Landroid/graphics/Paint;

.field private m:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/StarRatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 132
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    .line 135
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 137
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 138
    sget-object v1, Lcom/google/android/play/k;->aI:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 140
    sget v2, Lcom/google/android/play/k;->aJ:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/play/layout/StarRatingBar;->c:F

    .line 142
    sget v2, Lcom/google/android/play/k;->aL:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/StarRatingBar;->a:F

    .line 143
    sget v2, Lcom/google/android/play/k;->aO:I

    sget v3, Lcom/google/android/play/d;->F:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    int-to-double v2, v2

    iput-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->e:D

    .line 145
    sget v2, Lcom/google/android/play/k;->aK:I

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/play/layout/StarRatingBar;->b:F

    .line 146
    sget v2, Lcom/google/android/play/k;->aN:I

    sget v3, Lcom/google/android/play/c;->y:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 148
    sget v3, Lcom/google/android/play/k;->aM:I

    sget v4, Lcom/google/android/play/c;->x:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v3, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 150
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 152
    new-instance v1, Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->l:Landroid/graphics/Paint;

    .line 153
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 154
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->l:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 155
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->m:Landroid/graphics/Paint;

    .line 156
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->m:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 159
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->i:Landroid/graphics/Path;

    .line 160
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->i:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 161
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->j:Landroid/graphics/Path;

    .line 162
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->j:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 163
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->k:Landroid/graphics/Path;

    .line 164
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->k:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 166
    iget-wide v0, p0, Lcom/google/android/play/layout/StarRatingBar;->e:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide v4, 0x3fee28c731eb6950L    # 0.9424777960769379

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    add-double/2addr v2, v4

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/play/layout/StarRatingBar;->f:D

    .line 167
    const-wide v0, 0x3fd921fb54442d18L    # 0.39269908169872414

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->f:D

    mul-double/2addr v0, v2

    const-wide v2, 0x4000f6f00c146b3dL    # 2.1205750411731104

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/play/layout/StarRatingBar;->g:D

    .line 170
    iget-wide v0, p0, Lcom/google/android/play/layout/StarRatingBar;->f:D

    const-wide v2, 0x3ff41b2f769cf0e0L    # 1.2566370614359172

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/play/layout/StarRatingBar;->d:F

    .line 172
    const/16 v0, 0xa

    new-array v0, v0, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    .line 173
    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    aput-object v2, v1, v0

    .line 173
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/high16 v1, -0x40800000    # -1.0f

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->f:D

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->g:D

    const-wide v4, 0x3fe41b2f769cf0e0L    # 0.6283185307179586

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const/high16 v1, -0x40800000    # -1.0f

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->g:D

    const-wide v4, 0x3fe41b2f769cf0e0L    # 0.6283185307179586

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->f:D

    const-wide v4, 0x3ff41b2f769cf0e0L    # 1.2566370614359172

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const/high16 v1, -0x40800000    # -1.0f

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->f:D

    const-wide v4, 0x3ff41b2f769cf0e0L    # 1.2566370614359172

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->g:D

    const-wide v4, 0x3ff41b2f769cf0e0L    # 1.2566370614359172

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->g:D

    const-wide v4, 0x3ff41b2f769cf0e0L    # 1.2566370614359172

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->f:D

    const-wide v4, 0x3fe41b2f769cf0e0L    # 0.6283185307179586

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->f:D

    double-to-float v1, v2

    float-to-double v2, v1

    const-wide v4, 0x3fe41b2f769cf0e0L    # 0.6283185307179586

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    const/4 v1, 0x0

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    iget-wide v2, p0, Lcom/google/android/play/layout/StarRatingBar;->g:D

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    const/high16 v1, -0x40800000    # -1.0f

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    const/high16 v1, -0x40800000    # -1.0f

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    const/high16 v1, -0x40800000    # -1.0f

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    const/high16 v1, -0x40800000    # -1.0f

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 178
    invoke-direct {p0}, Lcom/google/android/play/layout/StarRatingBar;->a()V

    .line 180
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/StarRatingBar;->setWillNotDraw(Z)V

    .line 181
    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 294
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->i:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 296
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->i:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v3, v3, v6

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v4, v4, v6

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    move v0, v1

    .line 298
    :goto_0
    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 299
    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->i:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v4, v4, v0

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 298
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->i:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 304
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->j:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 306
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->j:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v3, v3, v6

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v4, v4, v6

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    move v0, v2

    .line 308
    :goto_1
    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 309
    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->j:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v4, v4, v0

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 308
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 311
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->j:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 314
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->k:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 316
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->k:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v3, v3, v6

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v4, v4, v6

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 318
    :goto_2
    if-gt v1, v2, :cond_2

    .line 319
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->k:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v3, v3, v1

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/google/android/play/layout/StarRatingBar;->h:[Landroid/graphics/PointF;

    aget-object v4, v4, v1

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 318
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 321
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->k:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 322
    return-void
.end method


# virtual methods
.method public getBaseline()I
    .locals 1

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 199
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 200
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingTop()I

    move-result v3

    .line 201
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingLeft()I

    move-result v4

    .line 203
    iget v0, p0, Lcom/google/android/play/layout/StarRatingBar;->a:F

    float-to-int v5, v0

    .line 204
    iget v0, p0, Lcom/google/android/play/layout/StarRatingBar;->a:F

    const/high16 v6, 0x3f800000    # 1.0f

    rem-float/2addr v0, v6

    const/4 v6, 0x0

    invoke-static {v0, v6}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 207
    :goto_0
    int-to-float v4, v4

    iget v6, p0, Lcom/google/android/play/layout/StarRatingBar;->d:F

    add-float/2addr v4, v6

    .line 208
    int-to-float v3, v3

    iget-wide v6, p0, Lcom/google/android/play/layout/StarRatingBar;->f:D

    double-to-float v6, v6

    add-float/2addr v3, v6

    .line 211
    :goto_1
    if-ge v2, v5, :cond_1

    .line 212
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 213
    int-to-float v6, v2

    iget v7, p0, Lcom/google/android/play/layout/StarRatingBar;->d:F

    mul-float/2addr v7, v9

    iget v8, p0, Lcom/google/android/play/layout/StarRatingBar;->c:F

    add-float/2addr v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v6, v4

    invoke-virtual {p1, v6, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 214
    iget-object v6, p0, Lcom/google/android/play/layout/StarRatingBar;->i:Landroid/graphics/Path;

    iget-object v7, p0, Lcom/google/android/play/layout/StarRatingBar;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 215
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 211
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 204
    goto :goto_0

    .line 219
    :cond_1
    if-ne v0, v1, :cond_3

    .line 220
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 221
    int-to-float v0, v2

    iget v1, p0, Lcom/google/android/play/layout/StarRatingBar;->d:F

    mul-float/2addr v1, v9

    iget v5, p0, Lcom/google/android/play/layout/StarRatingBar;->c:F

    add-float/2addr v1, v5

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 222
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->j:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->l:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/play/layout/StarRatingBar;->k:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 224
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 225
    add-int/lit8 v0, v2, 0x1

    .line 229
    :goto_2
    int-to-float v1, v0

    iget v2, p0, Lcom/google/android/play/layout/StarRatingBar;->b:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 231
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 232
    int-to-float v1, v0

    iget v2, p0, Lcom/google/android/play/layout/StarRatingBar;->d:F

    mul-float/2addr v2, v9

    iget v5, p0, Lcom/google/android/play/layout/StarRatingBar;->c:F

    add-float/2addr v2, v5

    mul-float/2addr v1, v2

    add-float/2addr v1, v4

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 233
    iget-object v1, p0, Lcom/google/android/play/layout/StarRatingBar;->i:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/play/layout/StarRatingBar;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 234
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 236
    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 240
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 242
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/layout/StarRatingBar;->setMeasuredDimension(II)V

    .line 252
    :goto_0
    return-void

    .line 247
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/play/layout/StarRatingBar;->b:F

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/play/layout/StarRatingBar;->d:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/play/layout/StarRatingBar;->b:F

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/play/layout/StarRatingBar;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 249
    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/play/layout/StarRatingBar;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    int-to-double v2, v1

    iget-wide v4, p0, Lcom/google/android/play/layout/StarRatingBar;->e:D

    add-double/2addr v2, v4

    double-to-int v1, v2

    .line 251
    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/layout/StarRatingBar;->setMeasuredDimension(II)V

    goto :goto_0
.end method
