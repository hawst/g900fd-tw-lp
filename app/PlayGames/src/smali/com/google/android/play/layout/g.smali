.class abstract Lcom/google/android/play/layout/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public e:I

.field public f:I

.field public g:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 989
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 989
    invoke-direct {p0}, Lcom/google/android/play/layout/g;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(ZI)I
.end method

.method public abstract b()I
.end method

.method protected abstract b(I)Z
.end method

.method public final c(I)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 1034
    iget v0, p0, Lcom/google/android/play/layout/g;->e:I

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/play/layout/g;->b()I

    move-result v3

    if-ne v3, p1, :cond_3

    const/4 v3, 0x2

    :goto_1
    or-int/2addr v0, v3

    .line 1036
    iget v3, p0, Lcom/google/android/play/layout/g;->g:I

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/google/android/play/layout/g;->f:I

    if-eq v0, v3, :cond_1

    .line 1037
    :cond_0
    iget v3, p0, Lcom/google/android/play/layout/g;->g:I

    if-ne v3, v4, :cond_4

    :goto_2
    invoke-virtual {p0, v1, p1}, Lcom/google/android/play/layout/g;->a(ZI)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/g;->g:I

    .line 1038
    iput v0, p0, Lcom/google/android/play/layout/g;->f:I

    .line 1040
    :cond_1
    iget v0, p0, Lcom/google/android/play/layout/g;->g:I

    return v0

    :cond_2
    move v0, v2

    .line 1034
    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    move v1, v2

    .line 1037
    goto :goto_2
.end method

.method public final d(I)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1063
    iget v0, p0, Lcom/google/android/play/layout/g;->e:I

    if-gt p1, v0, :cond_0

    .line 1064
    iput v1, p0, Lcom/google/android/play/layout/g;->g:I

    .line 1065
    const/4 v0, 0x2

    .line 1071
    :goto_0
    return v0

    .line 1067
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/g;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1068
    iput v1, p0, Lcom/google/android/play/layout/g;->g:I

    .line 1069
    const/4 v0, 0x1

    goto :goto_0

    .line 1071
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 1090
    iput v0, p0, Lcom/google/android/play/layout/g;->g:I

    .line 1091
    iput v0, p0, Lcom/google/android/play/layout/g;->e:I

    .line 1092
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/g;->f:I

    .line 1093
    return-void
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 1086
    iget v0, p0, Lcom/google/android/play/layout/g;->e:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/play/layout/g;->e:I

    .line 1087
    return-void
.end method
