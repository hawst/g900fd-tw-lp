.class final Lcom/google/android/play/c/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;

.field public c:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>(FF)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    cmpg-float v0, p1, v1

    if-ltz v0, :cond_0

    cmpl-float v0, p1, v2

    if-lez v0, :cond_1

    .line 198
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Time value must be between [0,1]"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :cond_1
    cmpg-float v0, p2, v1

    if-ltz v0, :cond_2

    cmpl-float v0, p2, v2

    if-lez v0, :cond_3

    .line 200
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value percentage must be between [0,1]"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_3
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    .line 203
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/c/e;->b:Ljava/lang/Float;

    .line 204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/c/e;->c:Landroid/animation/TimeInterpolator;

    .line 205
    return-void
.end method


# virtual methods
.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 190
    check-cast p1, Lcom/google/android/play/c/e;

    iget-object v0, p0, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    iget-object v1, p1, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/Float;->compareTo(Ljava/lang/Float;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 214
    instance-of v0, p1, Lcom/google/android/play/c/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    check-cast p1, Lcom/google/android/play/c/e;

    iget-object v1, p1, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->hashCode()I

    move-result v0

    return v0
.end method
