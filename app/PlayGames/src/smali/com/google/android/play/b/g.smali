.class final Lcom/google/android/play/b/g;
.super Lcom/google/android/play/b/a;
.source "SourceFile"


# instance fields
.field private final e:Landroid/graphics/RectF;

.field private final f:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Landroid/content/res/ColorStateList;FF)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/b/a;-><init>(Landroid/content/res/ColorStateList;FF)V

    .line 25
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/b/g;->e:Landroid/graphics/RectF;

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/b/g;->f:Landroid/graphics/Rect;

    .line 27
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/play/b/g;->e:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/play/b/g;->c:F

    iget v2, p0, Lcom/google/android/play/b/g;->c:F

    iget-object v3, p0, Lcom/google/android/play/b/g;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 32
    return-void
.end method

.method public final getOutline(Landroid/graphics/Outline;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/play/b/g;->f:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/play/b/g;->c:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Outline;->setRoundRect(Landroid/graphics/Rect;F)V

    .line 50
    return-void
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/google/android/play/b/a;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/play/b/g;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/play/b/g;->f:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/play/b/g;->d:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p0, Lcom/google/android/play/b/g;->d:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    iget-object v0, p0, Lcom/google/android/play/b/g;->e:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/play/b/g;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 44
    return-void
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method
