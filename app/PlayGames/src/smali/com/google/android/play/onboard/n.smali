.class public final Lcom/google/android/play/onboard/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Z

.field protected b:Z

.field protected c:Ljava/lang/String;

.field protected d:I

.field protected e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-boolean v0, p0, Lcom/google/android/play/onboard/n;->a:Z

    .line 15
    iput-boolean v0, p0, Lcom/google/android/play/onboard/n;->b:Z

    return-void
.end method

.method private g()Lcom/google/android/play/onboard/n;
    .locals 2

    .prologue
    .line 83
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/onboard/n;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 84
    :catch_0
    move-exception v0

    .line 85
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()Lcom/google/android/play/onboard/n;
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/onboard/n;->a:Z

    .line 22
    return-object p0
.end method

.method public final a(I)Lcom/google/android/play/onboard/n;
    .locals 0

    .prologue
    .line 63
    iput p1, p0, Lcom/google/android/play/onboard/n;->d:I

    .line 64
    return-object p0
.end method

.method public final a(Landroid/content/Context;I)Lcom/google/android/play/onboard/n;
    .locals 1

    .prologue
    .line 50
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 51
    :goto_0
    iput-object v0, p0, Lcom/google/android/play/onboard/n;->c:Ljava/lang/String;

    return-object p0

    .line 50
    :cond_0
    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/n;
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/play/onboard/n;->e:Ljava/lang/Runnable;

    .line 73
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/play/onboard/n;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/play/onboard/n;->c:Ljava/lang/String;

    .line 46
    return-object p0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/play/onboard/n;->a:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/play/onboard/n;->b:Z

    return v0
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/play/onboard/n;->g()Lcom/google/android/play/onboard/n;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/play/onboard/n;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/play/onboard/n;->d:I

    return v0
.end method

.method public final f()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/play/onboard/n;->e:Ljava/lang/Runnable;

    return-object v0
.end method
