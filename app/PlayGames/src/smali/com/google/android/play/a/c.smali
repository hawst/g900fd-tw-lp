.class public final Lcom/google/android/play/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Z

.field private static final b:F

.field private static c:[I

.field private static d:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 21
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/a/c;->a:Z

    .line 27
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/google/android/play/a/c;->b:F

    .line 28
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/play/a/c;->c:[I

    .line 29
    new-array v0, v2, [I

    sput-object v0, Lcom/google/android/play/a/c;->d:[I

    return-void

    .line 21
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/ViewPropertyAnimator;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 123
    sget-boolean v0, Lcom/google/android/play/a/c;->a:Z

    if-nez v0, :cond_1

    .line 124
    const/4 v0, 0x0

    .line 154
    :cond_0
    :goto_0
    return-object v0

    .line 126
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 127
    sget-object v1, Lcom/google/android/play/a/c;->c:[I

    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 128
    sget-object v1, Lcom/google/android/play/a/c;->d:[I

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    .line 129
    sget-object v1, Lcom/google/android/play/a/c;->c:[I

    aget v1, v1, v3

    sget-object v2, Lcom/google/android/play/a/c;->d:[I

    aget v2, v2, v3

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 130
    sget-object v2, Lcom/google/android/play/a/c;->c:[I

    aget v2, v2, v4

    sget-object v3, Lcom/google/android/play/a/c;->d:[I

    aget v3, v3, v4

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 131
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    sget v2, Lcom/google/android/play/a/c;->b:F

    .line 137
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 138
    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {p0, v2}, Landroid/view/View;->setRotation(F)V

    .line 139
    const-wide/16 v2, 0x0

    const/high16 v4, 0x43af0000    # 350.0f

    mul-float/2addr v1, v4

    const/high16 v4, 0x43160000    # 150.0f

    mul-float/2addr v0, v4

    add-float/2addr v0, v1

    float-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 142
    const-wide/16 v2, 0xfa

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    .line 145
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 147
    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 150
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 151
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x10c000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method
