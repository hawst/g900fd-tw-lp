.class public Lcom/google/android/play/widget/d;
.super Lcom/google/android/libraries/bind/b/b;
.source "SourceFile"


# instance fields
.field private d:Z

.field protected g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/libraries/bind/b/b;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/d;->g:Z

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/b/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/d;->g:Z

    .line 30
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/d;->d:Z

    .line 53
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/b/b;->a(I)V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/d;->d:Z

    .line 55
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/d;->d:Z

    .line 60
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/bind/b/b;->a(IZ)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/d;->d:Z

    .line 62
    return-void
.end method

.method public final a(Landroid/support/v4/view/ao;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/d;->g:Z

    .line 41
    invoke-super {p0, p1}, Lcom/google/android/libraries/bind/b/b;->a(Landroid/support/v4/view/ao;)V

    .line 42
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/google/android/play/widget/d;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/widget/d;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/google/android/libraries/bind/b/b;->onAttachedToWindow()V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/d;->g:Z

    .line 36
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 46
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/bind/b/b;->onLayout(ZIIII)V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/d;->g:Z

    .line 48
    return-void
.end method
