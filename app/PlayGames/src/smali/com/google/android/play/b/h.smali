.class final Lcom/google/android/play/b/h;
.super Lcom/google/android/play/b/a;
.source "SourceFile"


# static fields
.field private static e:Landroid/graphics/RectF;


# instance fields
.field private f:Landroid/graphics/Paint;

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Path;

.field private final i:Landroid/graphics/RectF;

.field private j:F

.field private k:F

.field private l:Z

.field private final m:I

.field private final n:I


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/content/res/ColorStateList;FFF)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 45
    invoke-direct {p0, p2, p3, p5}, Lcom/google/android/play/b/a;-><init>(Landroid/content/res/ColorStateList;FF)V

    .line 38
    iput-boolean v2, p0, Lcom/google/android/play/b/h;->l:Z

    .line 46
    sget v0, Lcom/google/android/play/c;->f:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/b/h;->m:I

    .line 47
    sget v0, Lcom/google/android/play/c;->e:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/b/h;->n:I

    .line 48
    const/4 v0, 0x0

    cmpg-float v0, p4, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid shadow size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/play/b/h;->k:F

    cmpl-float v0, v0, p4

    if-eqz v0, :cond_1

    iput p4, p0, Lcom/google/android/play/b/h;->k:F

    const/high16 v0, 0x3fc00000    # 1.5f

    mul-float/2addr v0, p4

    iput v0, p0, Lcom/google/android/play/b/h;->j:F

    iput-boolean v2, p0, Lcom/google/android/play/b/h;->l:Z

    invoke-virtual {p0}, Lcom/google/android/play/b/h;->invalidateSelf()V

    .line 49
    :cond_1
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    .line 50
    iget-object v0, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setDither(Z)V

    .line 52
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    .line 53
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/google/android/play/b/h;->g:Landroid/graphics/Paint;

    .line 54
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 13

    .prologue
    const/high16 v12, 0x42b40000    # 90.0f

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 105
    iget-boolean v0, p0, Lcom/google/android/play/b/h;->l:Z

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/play/b/h;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/play/b/h;->d:F

    iget v3, p0, Lcom/google/android/play/b/h;->d:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/RectF;->inset(FF)V

    new-instance v0, Landroid/graphics/RectF;

    iget v2, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v2, v2

    iget v3, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v3, v3

    iget v4, p0, Lcom/google/android/play/b/h;->c:F

    iget v5, p0, Lcom/google/android/play/b/h;->c:F

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iget v3, p0, Lcom/google/android/play/b/h;->j:F

    neg-float v3, v3

    iget v4, p0, Lcom/google/android/play/b/h;->j:F

    neg-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->inset(FF)V

    iget-object v3, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    if-nez v3, :cond_6

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    iput-object v3, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    :goto_0
    iget-object v3, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    sget-object v4, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v3, v4}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    iget-object v3, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    iget v4, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v4, v4

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v3, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    iget v4, p0, Lcom/google/android/play/b/h;->j:F

    neg-float v4, v4

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    iget-object v3, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {v3, v2, v4, v12, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    iget-object v2, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    const/high16 v3, 0x43870000    # 270.0f

    const/high16 v4, -0x3d4c0000    # -90.0f

    invoke-virtual {v2, v0, v3, v4, v9}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FFZ)V

    iget-object v0, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    iget v0, p0, Lcom/google/android/play/b/h;->c:F

    iget v2, p0, Lcom/google/android/play/b/h;->c:F

    iget v3, p0, Lcom/google/android/play/b/h;->j:F

    add-float/2addr v2, v3

    div-float v2, v0, v2

    iget-object v7, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RadialGradient;

    iget v3, p0, Lcom/google/android/play/b/h;->c:F

    iget v4, p0, Lcom/google/android/play/b/h;->j:F

    add-float/2addr v3, v4

    const/4 v4, 0x3

    new-array v4, v4, [I

    iget v5, p0, Lcom/google/android/play/b/h;->m:I

    aput v5, v4, v9

    iget v5, p0, Lcom/google/android/play/b/h;->m:I

    aput v5, v4, v8

    const/4 v5, 0x2

    iget v6, p0, Lcom/google/android/play/b/h;->n:I

    aput v6, v4, v5

    const/4 v5, 0x3

    new-array v5, v5, [F

    aput v1, v5, v9

    aput v2, v5, v8

    const/4 v2, 0x2

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v5, v2

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    iget-object v10, p0, Lcom/google/android/play/b/h;->g:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/LinearGradient;

    iget v2, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v2, v2

    iget v3, p0, Lcom/google/android/play/b/h;->j:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v3, v3

    iget v4, p0, Lcom/google/android/play/b/h;->j:F

    sub-float v4, v3, v4

    const/4 v3, 0x3

    new-array v5, v3, [I

    iget v3, p0, Lcom/google/android/play/b/h;->m:I

    aput v3, v5, v9

    iget v3, p0, Lcom/google/android/play/b/h;->m:I

    aput v3, v5, v8

    const/4 v3, 0x2

    iget v6, p0, Lcom/google/android/play/b/h;->n:I

    aput v6, v5, v3

    const/4 v3, 0x3

    new-array v6, v3, [F

    fill-array-data v6, :array_0

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 107
    iput-boolean v9, p0, Lcom/google/android/play/b/h;->l:Z

    .line 109
    :cond_0
    iget v0, p0, Lcom/google/android/play/b/h;->k:F

    div-float/2addr v0, v11

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 110
    iget v0, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v0, v0

    iget v2, p0, Lcom/google/android/play/b/h;->j:F

    sub-float v2, v0, v2

    iget v0, p0, Lcom/google/android/play/b/h;->c:F

    iget v3, p0, Lcom/google/android/play/b/h;->k:F

    div-float/2addr v3, v11

    add-float v7, v0, v3

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float v3, v11, v7

    sub-float/2addr v0, v3

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    move v6, v8

    :goto_1
    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float v3, v11, v7

    sub-float/2addr v0, v3

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    move v9, v8

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v10

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v7

    iget-object v3, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v7

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz v6, :cond_2

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float v3, v11, v7

    sub-float v3, v0, v3

    iget v0, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v4, v0

    iget-object v5, p0, Lcom/google/android/play/b/h;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_2
    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->restoreToCount(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v10

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v7

    iget-object v3, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v7

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v0, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz v6, :cond_3

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float v3, v11, v7

    sub-float v3, v0, v3

    iget v0, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v0, v0

    iget v4, p0, Lcom/google/android/play/b/h;->j:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/play/b/h;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->restoreToCount(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v6

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v7

    iget-object v3, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, v7

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, 0x43870000    # 270.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v0, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz v9, :cond_4

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float v3, v11, v7

    sub-float v3, v0, v3

    iget v0, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v4, v0

    iget-object v5, p0, Lcom/google/android/play/b/h;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_4
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v6

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v7

    iget-object v3, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v7

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1, v12}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v0, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    if-eqz v9, :cond_5

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float v3, v11, v7

    sub-float v3, v0, v3

    iget v0, p0, Lcom/google/android/play/b/h;->c:F

    neg-float v4, v0

    iget-object v5, p0, Lcom/google/android/play/b/h;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_5
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 111
    iget v0, p0, Lcom/google/android/play/b/h;->k:F

    neg-float v0, v0

    div-float/2addr v0, v11

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 113
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_8

    .line 114
    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v1, p0, Lcom/google/android/play/b/h;->c:F

    iget v2, p0, Lcom/google/android/play/b/h;->c:F

    iget-object v3, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 145
    :goto_2
    return-void

    .line 106
    :cond_6
    iget-object v3, p0, Lcom/google/android/play/b/h;->h:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    goto/16 :goto_0

    :cond_7
    move v6, v9

    .line 110
    goto/16 :goto_1

    .line 119
    :cond_8
    sget-object v0, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    if-nez v0, :cond_9

    .line 120
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    .line 122
    :cond_9
    iget v0, p0, Lcom/google/android/play/b/h;->c:F

    mul-float/2addr v0, v11

    .line 123
    iget-object v2, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float v9, v2, v0

    .line 124
    iget-object v2, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float v0, v2, v0

    .line 125
    sget-object v2, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    iget v6, p0, Lcom/google/android/play/b/h;->c:F

    mul-float/2addr v6, v11

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/google/android/play/b/h;->c:F

    mul-float/2addr v7, v11

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 127
    sget-object v3, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    const/high16 v4, 0x43340000    # 180.0f

    iget-object v7, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    move-object v2, p1

    move v5, v12

    move v6, v8

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 128
    sget-object v2, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    invoke-virtual {v2, v9, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 129
    sget-object v3, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    const/high16 v4, 0x43870000    # 270.0f

    iget-object v7, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    move-object v2, p1

    move v5, v12

    move v6, v8

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 130
    sget-object v2, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/RectF;->offset(FF)V

    .line 131
    sget-object v3, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    move v5, v12

    move v6, v8

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 132
    sget-object v0, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    neg-float v2, v9

    invoke-virtual {v0, v2, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 133
    sget-object v1, Lcom/google/android/play/b/h;->e:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v12

    move v3, v12

    move v4, v8

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/google/android/play/b/h;->c:F

    add-float/2addr v1, v0

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/google/android/play/b/h;->c:F

    sub-float v3, v0, v3

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v4, p0, Lcom/google/android/play/b/h;->c:F

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/google/android/play/b/h;->c:F

    add-float/2addr v1, v0

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/google/android/play/b/h;->c:F

    sub-float v2, v0, v2

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget v3, p0, Lcom/google/android/play/b/h;->c:F

    sub-float v3, v0, v3

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/google/android/play/b/h;->c:F

    add-float/2addr v2, v0

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/google/android/play/b/h;->i:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/google/android/play/b/h;->c:F

    sub-float v4, v0, v4

    iget-object v5, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 106
    :array_0
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, -0x1

    return v0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/google/android/play/b/a;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/b/h;->l:Z

    .line 67
    return-void
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 59
    iget-object v0, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 60
    iget-object v0, p0, Lcom/google/android/play/b/h;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 61
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/play/b/h;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 85
    iget-object v0, p0, Lcom/google/android/play/b/h;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 86
    iget-object v0, p0, Lcom/google/android/play/b/h;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 87
    return-void
.end method
