.class public Lcom/google/android/play/onboard/InterstitialOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# static fields
.field public static final a:[I

.field private static final b:I


# instance fields
.field private c:Ljava/util/List;

.field private d:[I

.field private final e:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    sget v0, Lcom/google/android/play/h;->e:I

    sput v0, Lcom/google/android/play/onboard/InterstitialOverlay;->b:I

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/play/c;->o:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/play/c;->p:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/play/c;->q:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/play/c;->r:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/play/onboard/InterstitialOverlay;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/onboard/InterstitialOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/onboard/InterstitialOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->e:Ljava/util/Random;

    .line 53
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/widget/a;

    .line 113
    invoke-virtual {v0}, Lcom/google/android/play/widget/a;->start()V

    goto :goto_0

    .line 115
    :cond_0
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 58
    iget-object v0, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->c:Ljava/util/List;

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/play/i;->g:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/google/android/play/onboard/a;

    invoke-direct {v3, p0, v0}, Lcom/google/android/play/onboard/a;-><init>(Lcom/google/android/play/onboard/InterstitialOverlay;Ljava/lang/String;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v5, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v5, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v3, v0}, Lcom/google/android/play/onboard/w;->a(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->d:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->d:[I

    array-length v0, v0

    if-nez v0, :cond_5

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/google/android/play/onboard/InterstitialOverlay;->a:[I

    array-length v3, v2

    new-array v3, v3, [I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move v0, v1

    :goto_1
    array-length v1, v2

    if-ge v0, v1, :cond_4

    aget v1, v2, v0

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iput-object v3, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->d:[I

    :cond_5
    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v3, Lcom/google/android/play/widget/a;

    iget-object v4, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->d:[I

    iget-object v5, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->e:Ljava/util/Random;

    iget-object v6, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->d:[I

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    aget v4, v4, v5

    iget-object v5, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->e:Ljava/util/Random;

    const/16 v6, 0x320

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    int-to-long v6, v5

    invoke-direct {v3, v4, v6, v7}, Lcom/google/android/play/widget/a;-><init>(IJ)V

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-ge v4, v5, :cond_6

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_3
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_7
    iput-object v1, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->c:Ljava/util/List;

    .line 59
    :cond_8
    invoke-direct {p0}, Lcom/google/android/play/onboard/InterstitialOverlay;->a()V

    .line 60
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/play/onboard/InterstitialOverlay;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/widget/a;

    invoke-virtual {v0}, Lcom/google/android/play/widget/a;->stop()V

    goto :goto_0

    .line 65
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 66
    return-void
.end method
