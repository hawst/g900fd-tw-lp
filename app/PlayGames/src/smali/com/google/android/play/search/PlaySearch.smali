.class public Lcom/google/android/play/search/PlaySearch;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/search/e;


# instance fields
.field private a:Lcom/google/android/play/search/e;

.field private b:Lcom/google/android/play/search/PlaySearchPlate;

.field private c:Lcom/google/android/play/search/PlaySearchSuggestionsList;

.field private d:Lcom/google/android/play/search/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->a:Lcom/google/android/play/search/e;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->a:Lcom/google/android/play/search/e;

    invoke-interface {v0, p1}, Lcom/google/android/play/search/e;->a(I)V

    .line 81
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->a:Lcom/google/android/play/search/e;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->a:Lcom/google/android/play/search/e;

    invoke-interface {v0, p1}, Lcom/google/android/play/search/e;->a(Ljava/lang/String;)V

    .line 74
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 38
    sget v0, Lcom/google/android/play/f;->ad:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/PlaySearchPlate;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearch;->b:Lcom/google/android/play/search/PlaySearchPlate;

    .line 39
    sget v0, Lcom/google/android/play/f;->ae:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearch;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/PlaySearchSuggestionsList;

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearch;->c:Lcom/google/android/play/search/PlaySearchSuggestionsList;

    .line 42
    new-instance v0, Lcom/google/android/play/search/d;

    invoke-direct {v0}, Lcom/google/android/play/search/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearch;->d:Lcom/google/android/play/search/d;

    .line 43
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->d:Lcom/google/android/play/search/d;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/d;->a(Lcom/google/android/play/search/e;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->b:Lcom/google/android/play/search/PlaySearchPlate;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearch;->d:Lcom/google/android/play/search/d;

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchPlate;->a(Lcom/google/android/play/search/d;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearch;->c:Lcom/google/android/play/search/PlaySearchSuggestionsList;

    iget-object v1, p0, Lcom/google/android/play/search/PlaySearch;->d:Lcom/google/android/play/search/d;

    invoke-virtual {v0, v1}, Lcom/google/android/play/search/PlaySearchSuggestionsList;->a(Lcom/google/android/play/search/d;)V

    .line 46
    return-void
.end method
