.class public Lcom/google/android/play/layout/PlayTabContainer;
.super Landroid/widget/HorizontalScrollView;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/cc;


# instance fields
.field private a:Lcom/google/android/play/layout/PlayTabStrip;

.field private final b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTabContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTabContainer;->setHorizontalScrollBarEnabled(Z)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/d;->H:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->b:I

    .line 48
    return-void
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->a:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v0

    .line 146
    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->a:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 156
    add-int/2addr v0, p2

    .line 157
    if-gtz p1, :cond_2

    if-lez p2, :cond_3

    .line 158
    :cond_2
    iget v1, p0, Lcom/google/android/play/layout/PlayTabContainer;->b:I

    sub-int/2addr v0, v1

    .line 161
    :cond_3
    iget v1, p0, Lcom/google/android/play/layout/PlayTabContainer;->d:I

    if-eq v0, v1, :cond_0

    .line 165
    iput v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->d:I

    .line 166
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/play/layout/PlayTabContainer;->scrollTo(II)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->c:I

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->a:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/PlayTabStrip;->a(I)V

    .line 140
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTabContainer;->a(II)V

    .line 142
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->a:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayTabStrip;->getChildCount()I

    move-result v0

    .line 119
    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->a:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/play/layout/PlayTabStrip;->a(IF)V

    .line 125
    iget-object v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->a:Lcom/google/android/play/layout/PlayTabStrip;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/PlayTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 126
    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 128
    :goto_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/play/layout/PlayTabContainer;->a(II)V

    goto :goto_0

    .line 126
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    goto :goto_1
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 133
    iput p1, p0, Lcom/google/android/play/layout/PlayTabContainer;->c:I

    .line 134
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onFinishInflate()V

    .line 54
    sget v0, Lcom/google/android/play/f;->K:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayTabContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTabStrip;

    iput-object v0, p0, Lcom/google/android/play/layout/PlayTabContainer;->a:Lcom/google/android/play/layout/PlayTabStrip;

    .line 55
    return-void
.end method
