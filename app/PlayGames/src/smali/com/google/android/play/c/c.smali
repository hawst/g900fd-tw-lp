.class public final Lcom/google/android/play/c/c;
.super Lcom/google/android/play/c/f;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private a:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 46
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/c/c;-><init>(I)V

    .line 47
    const-string v0, "CircularReveal"

    const-string v1, "This constructor - CircularReveal(Rect) - is deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/c/c;-><init>(IB)V

    .line 56
    return-void
.end method

.method private constructor <init>(IB)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/play/c/f;-><init>(I)V

    .line 66
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    .line 67
    iget-object v0, p0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/play/c/e;

    invoke-direct {v1, v2, v2}, Lcom/google/android/play/c/e;-><init>(FF)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v0, p0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/play/c/e;

    invoke-direct {v1, v3, v3}, Lcom/google/android/play/c/e;-><init>(FF)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    return-void
.end method


# virtual methods
.method public final createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 16

    .prologue
    .line 107
    move-object/from16 v0, p3

    iget-object v6, v0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 108
    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float v7, v2, v3

    .line 109
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v8, v2, 0x2

    .line 110
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v9, v2, 0x2

    .line 113
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/c/c;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 118
    :cond_0
    new-instance v10, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 121
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/c/e;

    move-object v3, v2

    .line 123
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 124
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/c/e;

    .line 126
    iget-object v4, v3, Lcom/google/android/play/c/e;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    mul-float/2addr v4, v7

    iget-object v5, v2, Lcom/google/android/play/c/e;->b:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    mul-float/2addr v5, v7

    invoke-static {v6, v8, v9, v4, v5}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v12

    .line 128
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/c/c;->getDuration()J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/c/c;->a()Z

    move-result v13

    if-eqz v13, :cond_1

    iget-object v13, v2, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v13

    long-to-float v14, v4

    mul-float/2addr v13, v14

    iget-object v14, v3, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    invoke-virtual {v14}, Ljava/lang/Float;->floatValue()F

    move-result v14

    long-to-float v4, v4

    mul-float/2addr v4, v14

    sub-float v4, v13, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-long v4, v4

    :goto_1
    invoke-virtual {v12, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/c/c;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v3, v2, Lcom/google/android/play/c/e;->c:Landroid/animation/TimeInterpolator;

    :goto_2
    invoke-virtual {v12, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 131
    invoke-interface {v10, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v3, v2

    .line 134
    goto :goto_0

    .line 128
    :cond_1
    const/high16 v13, 0x3f800000    # 1.0f

    iget-object v14, v2, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    invoke-virtual {v14}, Ljava/lang/Float;->floatValue()F

    move-result v14

    sub-float/2addr v13, v14

    long-to-float v14, v4

    mul-float/2addr v13, v14

    const/high16 v14, 0x3f800000    # 1.0f

    iget-object v15, v3, Lcom/google/android/play/c/e;->a:Ljava/lang/Float;

    invoke-virtual {v15}, Ljava/lang/Float;->floatValue()F

    move-result v15

    sub-float/2addr v14, v15

    long-to-float v4, v4

    mul-float/2addr v4, v14

    sub-float v4, v13, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-long v4, v4

    goto :goto_1

    .line 129
    :cond_2
    iget-object v3, v3, Lcom/google/android/play/c/e;->c:Landroid/animation/TimeInterpolator;

    goto :goto_2

    .line 136
    :cond_3
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 137
    invoke-virtual {v2, v10}, Landroid/animation/AnimatorSet;->playSequentially(Ljava/util/List;)V

    .line 138
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/play/c/c;->getDuration()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 139
    new-instance v3, Lcom/google/android/play/c/d;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v6}, Lcom/google/android/play/c/d;-><init>(Lcom/google/android/play/c/c;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 149
    new-instance v3, Lcom/google/android/play/utils/b;

    invoke-direct {v3, v2}, Lcom/google/android/play/utils/b;-><init>(Landroid/animation/Animator;)V

    return-object v3
.end method

.method public final getInterpolator()Landroid/animation/TimeInterpolator;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/c/e;

    iget-object v0, v0, Lcom/google/android/play/c/e;->c:Landroid/animation/TimeInterpolator;

    return-object v0
.end method

.method public final setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/play/c/c;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/c/e;

    iput-object p1, v0, Lcom/google/android/play/c/e;->c:Landroid/animation/TimeInterpolator;

    .line 169
    return-object p0
.end method
