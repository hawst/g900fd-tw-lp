.class final Lcom/google/android/play/layout/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final p:Landroid/support/v4/f/n;


# instance fields
.field public a:I

.field public b:I

.field public c:F

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:Z

.field public o:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 857
    new-instance v0, Landroid/support/v4/f/o;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Landroid/support/v4/f/o;-><init>(I)V

    sput-object v0, Lcom/google/android/play/layout/d;->p:Landroid/support/v4/f/n;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 902
    invoke-direct {p0}, Lcom/google/android/play/layout/d;->c()V

    .line 903
    return-void
.end method

.method public static a()Lcom/google/android/play/layout/d;
    .locals 1

    .prologue
    .line 860
    sget-object v0, Lcom/google/android/play/layout/d;->p:Landroid/support/v4/f/n;

    invoke-interface {v0}, Landroid/support/v4/f/n;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/d;

    .line 861
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/play/layout/d;

    invoke-direct {v0}, Lcom/google/android/play/layout/d;-><init>()V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 966
    iput v0, p0, Lcom/google/android/play/layout/d;->k:I

    .line 967
    iput v0, p0, Lcom/google/android/play/layout/d;->l:I

    .line 968
    iput v1, p0, Lcom/google/android/play/layout/d;->d:I

    .line 969
    iput v1, p0, Lcom/google/android/play/layout/d;->e:I

    .line 970
    iput v1, p0, Lcom/google/android/play/layout/d;->f:I

    .line 971
    iput v1, p0, Lcom/google/android/play/layout/d;->g:I

    .line 972
    iput v1, p0, Lcom/google/android/play/layout/d;->h:I

    .line 973
    iput v1, p0, Lcom/google/android/play/layout/d;->i:I

    .line 974
    iput v1, p0, Lcom/google/android/play/layout/d;->m:I

    .line 975
    iput v1, p0, Lcom/google/android/play/layout/d;->j:I

    .line 976
    iput v1, p0, Lcom/google/android/play/layout/d;->a:I

    .line 977
    iput v1, p0, Lcom/google/android/play/layout/d;->b:I

    .line 978
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/google/android/play/layout/d;->c:F

    .line 979
    iput v1, p0, Lcom/google/android/play/layout/d;->o:I

    .line 980
    iput-boolean v1, p0, Lcom/google/android/play/layout/d;->n:Z

    .line 981
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Z)V
    .locals 5

    .prologue
    .line 934
    invoke-static {p1}, Lcom/google/android/play/layout/FlowLayoutManager;->b(Landroid/view/View;)I

    move-result v2

    .line 935
    invoke-static {p1}, Lcom/google/android/play/layout/FlowLayoutManager;->c(Landroid/view/View;)I

    move-result v1

    .line 936
    invoke-virtual {p1}, Landroid/view/View;->getBaseline()I

    move-result v0

    .line 938
    if-ltz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    if-le v0, v3, :cond_3

    :cond_0
    move v0, v1

    .line 943
    :goto_0
    if-eqz p2, :cond_2

    iget v3, p0, Lcom/google/android/play/layout/d;->k:I

    if-ne v2, v3, :cond_1

    iget v3, p0, Lcom/google/android/play/layout/d;->l:I

    if-ne v1, v3, :cond_1

    iget v3, p0, Lcom/google/android/play/layout/d;->m:I

    if-eq v0, v3, :cond_2

    .line 945
    :cond_1
    const-string v3, "FlowLayoutManager"

    const-string v4, "Child measurement changed without notifying from the adapter! Some layout may be incorrect."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    :cond_2
    iput v2, p0, Lcom/google/android/play/layout/d;->k:I

    .line 955
    iput v1, p0, Lcom/google/android/play/layout/d;->l:I

    .line 956
    iput v0, p0, Lcom/google/android/play/layout/d;->m:I

    .line 957
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/layout/d;->n:Z

    .line 958
    return-void

    .line 941
    :cond_3
    invoke-static {p1}, Lcom/google/android/play/layout/FlowLayoutManager;->h(Landroid/view/View;)I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method public final a(Lcom/google/android/play/layout/e;I)V
    .locals 1

    .prologue
    .line 909
    invoke-virtual {p1, p2, p0}, Lcom/google/android/play/layout/e;->a(ILcom/google/android/play/layout/d;)F

    .line 910
    iget v0, p0, Lcom/google/android/play/layout/d;->c:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/e;->a(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/d;->d:I

    .line 911
    iget v0, p0, Lcom/google/android/play/layout/d;->c:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/e;->b(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/d;->e:I

    .line 912
    iget v0, p0, Lcom/google/android/play/layout/d;->c:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/e;->c(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/d;->f:I

    .line 913
    iget v0, p0, Lcom/google/android/play/layout/d;->c:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/e;->d(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/d;->g:I

    .line 914
    iget v0, p0, Lcom/google/android/play/layout/d;->c:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/e;->e(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/d;->h:I

    .line 915
    iget v0, p0, Lcom/google/android/play/layout/d;->c:F

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/e;->f(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/d;->i:I

    .line 916
    iget v0, p1, Lcom/google/android/play/layout/e;->r:I

    iput v0, p0, Lcom/google/android/play/layout/d;->j:I

    .line 917
    return-void
.end method

.method public final a(Lcom/google/android/play/layout/d;)Z
    .locals 2

    .prologue
    .line 921
    iget v0, p0, Lcom/google/android/play/layout/d;->c:F

    iget v1, p1, Lcom/google/android/play/layout/d;->c:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/d;->a:I

    iget v1, p1, Lcom/google/android/play/layout/d;->a:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/d;->b:I

    iget v1, p1, Lcom/google/android/play/layout/d;->b:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 961
    invoke-direct {p0}, Lcom/google/android/play/layout/d;->c()V

    .line 962
    sget-object v0, Lcom/google/android/play/layout/d;->p:Landroid/support/v4/f/n;

    invoke-interface {v0, p0}, Landroid/support/v4/f/n;->a(Ljava/lang/Object;)Z

    .line 963
    return-void
.end method
