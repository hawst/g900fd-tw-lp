.class public final Lcom/google/android/play/search/i;
.super Landroid/support/v7/widget/bv;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v7/widget/bv;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/search/i;->c:Ljava/util/List;

    .line 22
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/play/search/i;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cr;
    .locals 3

    .prologue
    .line 18
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/play/h;->h:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/play/search/j;

    check-cast v0, Lcom/google/android/play/search/PlaySearchOneSuggestion;

    invoke-direct {v1, v0}, Lcom/google/android/play/search/j;-><init>(Lcom/google/android/play/search/PlaySearchOneSuggestion;)V

    return-object v1
.end method

.method public final synthetic a(Landroid/support/v7/widget/cr;I)V
    .locals 3

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/play/search/j;

    invoke-virtual {p0}, Lcom/google/android/play/search/i;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v2, p1, Lcom/google/android/play/search/j;->k:Lcom/google/android/play/search/PlaySearchOneSuggestion;

    iget-object v0, p0, Lcom/google/android/play/search/i;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/search/l;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/play/search/PlaySearchOneSuggestion;->a(Lcom/google/android/play/search/l;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method
