.class final Lcom/google/android/play/layout/c;
.super Lcom/google/android/play/layout/f;
.source "SourceFile"


# static fields
.field private static final h:Landroid/support/v4/f/n;


# instance fields
.field public a:I

.field public b:I

.field public final c:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1130
    new-instance v0, Landroid/support/v4/f/o;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/support/v4/f/o;-><init>(I)V

    sput-object v0, Lcom/google/android/play/layout/c;->h:Landroid/support/v4/f/n;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1162
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/layout/f;-><init>(B)V

    .line 1160
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    .line 1163
    invoke-virtual {p0}, Lcom/google/android/play/layout/c;->d()V

    .line 1164
    return-void
.end method

.method public static a(IIILcom/google/android/play/layout/d;)Lcom/google/android/play/layout/c;
    .locals 1

    .prologue
    .line 1135
    sget-object v0, Lcom/google/android/play/layout/c;->h:Landroid/support/v4/f/n;

    invoke-interface {v0}, Landroid/support/v4/f/n;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/c;

    .line 1136
    if-nez v0, :cond_0

    .line 1137
    new-instance v0, Lcom/google/android/play/layout/c;

    invoke-direct {v0}, Lcom/google/android/play/layout/c;-><init>()V

    .line 1139
    :cond_0
    iput p0, v0, Lcom/google/android/play/layout/c;->e:I

    .line 1140
    iput p2, v0, Lcom/google/android/play/layout/c;->d:I

    .line 1141
    iput p1, v0, Lcom/google/android/play/layout/c;->a:I

    .line 1142
    invoke-virtual {v0, p3}, Lcom/google/android/play/layout/c;->a(Lcom/google/android/play/layout/d;)V

    .line 1143
    return-object v0
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 1303
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    iget v1, p0, Lcom/google/android/play/layout/c;->e:I

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/d;

    iget v0, v0, Lcom/google/android/play/layout/d;->o:I

    return v0
.end method

.method protected final a(ZI)I
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/high16 v7, -0x80000000

    const/4 v5, 0x0

    .line 1189
    if-eqz p1, :cond_5

    .line 1190
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    move v8, v5

    move v4, v7

    move v3, v5

    move v1, v5

    :goto_0
    if-ge v8, v9, :cond_2

    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/d;

    iget v2, v0, Lcom/google/android/play/layout/d;->j:I

    if-nez v2, :cond_1

    iget v2, v0, Lcom/google/android/play/layout/d;->m:I

    neg-int v2, v2

    iput v2, v0, Lcom/google/android/play/layout/d;->o:I

    if-ge v2, v1, :cond_0

    move v1, v2

    :cond_0
    iget v0, v0, Lcom/google/android/play/layout/d;->l:I

    add-int/2addr v0, v2

    if-le v0, v3, :cond_11

    :goto_1
    move v3, v0

    move v0, v4

    :goto_2
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v4, v0

    goto :goto_0

    :cond_1
    iget v2, v0, Lcom/google/android/play/layout/d;->l:I

    if-le v2, v4, :cond_10

    iget v0, v0, Lcom/google/android/play/layout/d;->l:I

    goto :goto_2

    :cond_2
    if-eq v4, v7, :cond_5

    sub-int v0, v3, v1

    if-ge v0, v4, :cond_3

    if-nez v0, :cond_4

    move v0, v5

    :goto_3
    add-int v3, v0, v4

    move v1, v0

    :cond_3
    move v2, v5

    :goto_4
    if-ge v2, v9, :cond_5

    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/d;

    iget v4, v0, Lcom/google/android/play/layout/d;->j:I

    packed-switch v4, :pswitch_data_0

    :goto_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_4
    int-to-float v2, v4

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_3

    :pswitch_0
    iput v1, v0, Lcom/google/android/play/layout/d;->o:I

    goto :goto_5

    :pswitch_1
    iget v4, v0, Lcom/google/android/play/layout/d;->l:I

    sub-int v4, v3, v4

    iput v4, v0, Lcom/google/android/play/layout/d;->o:I

    goto :goto_5

    .line 1192
    :cond_5
    iget v0, p0, Lcom/google/android/play/layout/c;->e:I

    if-nez v0, :cond_6

    move v1, v6

    .line 1193
    :goto_6
    iget v0, p0, Lcom/google/android/play/layout/c;->e:I

    iget-object v2, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v0, v2

    if-ne v0, p2, :cond_7

    .line 1194
    :goto_7
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    if-nez v8, :cond_8

    :goto_8
    return v5

    :cond_6
    move v1, v5

    .line 1192
    goto :goto_6

    :cond_7
    move v6, v5

    .line 1193
    goto :goto_7

    .line 1194
    :cond_8
    const v3, 0x7fffffff

    move v4, v7

    move v7, v5

    :goto_9
    if-ge v7, v8, :cond_b

    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/d;

    iget v9, v0, Lcom/google/android/play/layout/d;->o:I

    if-eqz v1, :cond_9

    iget v2, v0, Lcom/google/android/play/layout/d;->e:I

    :goto_a
    sub-int v2, v9, v2

    if-ge v2, v3, :cond_f

    :goto_b
    iget v3, v0, Lcom/google/android/play/layout/d;->o:I

    iget v9, v0, Lcom/google/android/play/layout/d;->l:I

    add-int/2addr v3, v9

    if-eqz v6, :cond_a

    iget v0, v0, Lcom/google/android/play/layout/d;->i:I

    :goto_c
    add-int/2addr v0, v3

    if-le v0, v4, :cond_e

    :goto_d
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v4, v0

    move v3, v2

    goto :goto_9

    :cond_9
    iget v2, v0, Lcom/google/android/play/layout/d;->d:I

    goto :goto_a

    :cond_a
    iget v0, v0, Lcom/google/android/play/layout/d;->h:I

    goto :goto_c

    :cond_b
    if-eqz v3, :cond_d

    move v1, v5

    :goto_e
    if-ge v1, v8, :cond_c

    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/d;

    iget v2, v0, Lcom/google/android/play/layout/d;->o:I

    sub-int/2addr v2, v3

    iput v2, v0, Lcom/google/android/play/layout/d;->o:I

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_e

    :cond_c
    sub-int/2addr v4, v3

    :cond_d
    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto :goto_8

    :cond_e
    move v0, v4

    goto :goto_d

    :cond_f
    move v2, v3

    goto :goto_b

    :cond_10
    move v0, v4

    goto/16 :goto_2

    :cond_11
    move v0, v3

    goto/16 :goto_1

    .line 1190
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1169
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/d;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/play/layout/d;->n:Z

    .line 1168
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1171
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/play/layout/d;)V
    .locals 3

    .prologue
    .line 1174
    iget-boolean v0, p1, Lcom/google/android/play/layout/d;->n:Z

    if-nez v0, :cond_0

    .line 1175
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Item not measured"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1177
    :cond_0
    iget v0, p0, Lcom/google/android/play/layout/c;->b:I

    iget v1, p1, Lcom/google/android/play/layout/d;->k:I

    iget v2, p1, Lcom/google/android/play/layout/d;->f:I

    add-int/2addr v1, v2

    iget v2, p1, Lcom/google/android/play/layout/d;->g:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/layout/c;->b:I

    .line 1178
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/layout/g;->g:I

    .line 1180
    return-void
.end method

.method final a(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 1343
    const/16 v0, 0x40

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/layout/c;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/play/layout/c;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1344
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1184
    iget v0, p0, Lcom/google/android/play/layout/c;->e:I

    iget-object v1, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected final b(I)Z
    .locals 6

    .prologue
    .line 1308
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1309
    iget v1, p0, Lcom/google/android/play/layout/c;->e:I

    add-int/2addr v1, v0

    if-gt v1, p1, :cond_0

    .line 1311
    const/4 v0, 0x0

    .line 1321
    :goto_0
    return v0

    .line 1314
    :cond_0
    iget v1, p0, Lcom/google/android/play/layout/c;->e:I

    sub-int v2, p1, v1

    .line 1315
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-lt v1, v2, :cond_1

    .line 1316
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/d;

    .line 1318
    iget v3, p0, Lcom/google/android/play/layout/c;->b:I

    iget v4, v0, Lcom/google/android/play/layout/d;->k:I

    iget v5, v0, Lcom/google/android/play/layout/d;->f:I

    add-int/2addr v4, v5

    iget v5, v0, Lcom/google/android/play/layout/d;->g:I

    add-int/2addr v4, v5

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/play/layout/c;->b:I

    .line 1319
    invoke-virtual {v0}, Lcom/google/android/play/layout/d;->b()V

    .line 1315
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 1321
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1326
    invoke-virtual {p0}, Lcom/google/android/play/layout/c;->d()V

    .line 1327
    sget-object v0, Lcom/google/android/play/layout/c;->h:Landroid/support/v4/f/n;

    invoke-interface {v0, p0}, Landroid/support/v4/f/n;->a(Ljava/lang/Object;)Z

    .line 1328
    return-void
.end method

.method protected final d()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1332
    invoke-super {p0}, Lcom/google/android/play/layout/f;->d()V

    .line 1333
    iput v0, p0, Lcom/google/android/play/layout/c;->a:I

    .line 1334
    iput v0, p0, Lcom/google/android/play/layout/c;->b:I

    .line 1335
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1336
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/d;

    invoke-virtual {v0}, Lcom/google/android/play/layout/d;->b()V

    .line 1335
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1338
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1339
    return-void
.end method
