.class final Lcom/google/android/play/headerlist/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/view/View;

.field b:F

.field c:F

.field d:F

.field private e:F


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 3436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3431
    iput v1, p0, Lcom/google/android/play/headerlist/g;->b:F

    .line 3432
    iput v0, p0, Lcom/google/android/play/headerlist/g;->c:F

    .line 3433
    iput v0, p0, Lcom/google/android/play/headerlist/g;->e:F

    .line 3434
    iput v1, p0, Lcom/google/android/play/headerlist/g;->d:F

    .line 3437
    iput-object p1, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    .line 3438
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 3442
    iget-object v0, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    if-nez v0, :cond_1

    .line 3456
    :cond_0
    :goto_0
    return-void

    .line 3445
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 3446
    iget-object v0, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 3448
    :cond_2
    iget v0, p0, Lcom/google/android/play/headerlist/g;->b:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 3449
    iput p1, p0, Lcom/google/android/play/headerlist/g;->b:F

    .line 3450
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p1, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 3451
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 3452
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 3453
    iget-object v1, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final b(F)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const-wide/16 v4, 0xc8

    const/16 v2, 0xc

    .line 3472
    iget-object v0, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    if-nez v0, :cond_0

    .line 3500
    :goto_0
    return-void

    .line 3475
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v2, :cond_1

    .line 3476
    iget-object v0, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    .line 3478
    :cond_1
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    iget v1, p0, Lcom/google/android/play/headerlist/g;->b:F

    invoke-direct {v0, v1, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 3479
    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 3480
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 3482
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v2, :cond_2

    .line 3483
    new-instance v1, Lcom/google/android/play/headerlist/h;

    invoke-direct {v1, p0, p1}, Lcom/google/android/play/headerlist/h;-><init>(Lcom/google/android/play/headerlist/g;F)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3498
    :cond_2
    iget-object v1, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final c(F)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 3504
    invoke-static {}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3505
    iget-object v0, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setZ(F)V

    .line 3507
    :cond_0
    return-void
.end method

.method public final d(F)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 3523
    iget-object v0, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    if-nez v0, :cond_1

    .line 3524
    iput p1, p0, Lcom/google/android/play/headerlist/g;->c:F

    .line 3539
    :cond_0
    :goto_0
    return-void

    .line 3527
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 3528
    iget-object v0, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 3530
    :cond_2
    iget v0, p0, Lcom/google/android/play/headerlist/g;->c:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 3531
    iput p1, p0, Lcom/google/android/play/headerlist/g;->c:F

    .line 3532
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v2, v2, p1, p1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 3534
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 3535
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 3536
    iget-object v1, p0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method
