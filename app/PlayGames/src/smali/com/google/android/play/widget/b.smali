.class public final Lcom/google/android/play/widget/b;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"

# interfaces
.implements Landroid/graphics/drawable/Animatable;


# instance fields
.field protected final a:I

.field protected final b:J

.field protected final c:[Lcom/google/android/play/widget/c;

.field protected final d:Landroid/view/animation/Interpolator;

.field protected e:Z

.field protected f:I

.field protected g:J

.field protected h:Landroid/graphics/Paint;

.field private i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(IIJ)V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/play/widget/b;->f:I

    .line 54
    iput p2, p0, Lcom/google/android/play/widget/b;->a:I

    .line 55
    iput-wide p3, p0, Lcom/google/android/play/widget/b;->b:J

    .line 56
    new-array v0, p2, [Lcom/google/android/play/widget/c;

    iput-object v0, p0, Lcom/google/android/play/widget/b;->c:[Lcom/google/android/play/widget/c;

    .line 58
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/widget/b;->d:Landroid/view/animation/Interpolator;

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/widget/b;->h:Landroid/graphics/Paint;

    .line 61
    iget-object v0, p0, Lcom/google/android/play/widget/b;->h:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 62
    iget-object v0, p0, Lcom/google/android/play/widget/b;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/play/widget/b;->h:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 65
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/play/widget/b;->i:Ljava/lang/Runnable;

    .line 72
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 109
    iget-wide v2, p0, Lcom/google/android/play/widget/b;->g:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 111
    iget v0, p0, Lcom/google/android/play/widget/b;->f:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 134
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/play/widget/b;->g:J

    sub-long v10, v2, v4

    .line 116
    iget-wide v2, p0, Lcom/google/android/play/widget/b;->b:J

    cmp-long v0, v10, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    move v7, v0

    .line 119
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    const/16 v5, 0xff

    const/16 v6, 0x1f

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayerAlpha(FFFFII)I

    .line 122
    iget v0, p0, Lcom/google/android/play/widget/b;->f:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/play/widget/b;->c:[Lcom/google/android/play/widget/c;

    array-length v1, v0

    :goto_2
    if-ge v8, v1, :cond_2

    aget-object v2, v0, v8

    .line 124
    long-to-float v3, v10

    iget-wide v4, p0, Lcom/google/android/play/widget/b;->b:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/play/widget/b;->d:Landroid/view/animation/Interpolator;

    invoke-interface {v4, v3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/play/widget/b;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v3, v5

    iget v5, v2, Lcom/google/android/play/widget/c;->a:F

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget v2, v2, Lcom/google/android/play/widget/c;->b:F

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v2, v4

    iget-object v4, p0, Lcom/google/android/play/widget/b;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 123
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_1
    move v7, v8

    .line 116
    goto :goto_1

    .line 126
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 128
    if-eqz v7, :cond_3

    .line 129
    invoke-virtual {p0}, Lcom/google/android/play/widget/b;->stop()V

    goto :goto_0

    .line 132
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/play/widget/b;->invalidateSelf()V

    goto :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 168
    const/4 v0, -0x3

    return v0
.end method

.method public final isRunning()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/google/android/play/widget/b;->e:Z

    return v0
.end method

.method public final setAlpha(I)V
    .locals 3

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/play/widget/b;->f:I

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/play/widget/b;->f:I

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/play/widget/b;->f:I

    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/widget/b;->f:I

    .line 159
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 163
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final start()V
    .locals 10

    .prologue
    const/high16 v9, 0x3fc00000    # 1.5f

    const/high16 v8, 0x3e800000    # 0.25f

    .line 76
    invoke-virtual {p0}, Lcom/google/android/play/widget/b;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 77
    iget-wide v0, p0, Lcom/google/android/play/widget/b;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 78
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/play/widget/b;->g:J

    .line 79
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/play/widget/b;->a:I

    if-ge v0, v2, :cond_0

    new-instance v2, Lcom/google/android/play/widget/c;

    invoke-direct {v2}, Lcom/google/android/play/widget/c;-><init>()V

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v3

    mul-float/2addr v3, v9

    sub-float/2addr v3, v8

    iput v3, v2, Lcom/google/android/play/widget/c;->a:F

    invoke-virtual {v1}, Ljava/util/Random;->nextFloat()F

    move-result v3

    mul-float/2addr v3, v9

    sub-float/2addr v3, v8

    iput v3, v2, Lcom/google/android/play/widget/c;->b:F

    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/play/widget/b;->b:J

    long-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-long v4, v4

    iput-wide v4, v2, Lcom/google/android/play/widget/c;->c:J

    iget-object v3, p0, Lcom/google/android/play/widget/b;->c:[Lcom/google/android/play/widget/c;

    aput-object v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/play/widget/b;->e:Z

    .line 82
    invoke-virtual {p0}, Lcom/google/android/play/widget/b;->invalidateSelf()V

    .line 84
    :cond_1
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/play/widget/b;->e:Z

    .line 89
    iget-object v0, p0, Lcom/google/android/play/widget/b;->i:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/play/widget/b;->i:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 92
    :cond_0
    return-void
.end method
