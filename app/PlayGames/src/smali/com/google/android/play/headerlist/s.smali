.class public final Lcom/google/android/play/headerlist/s;
.super Landroid/support/v7/widget/cg;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field protected b:I

.field private c:I

.field private final d:Landroid/support/v7/widget/bx;

.field private e:Landroid/support/v7/widget/bv;


# direct methods
.method public constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/support/v7/widget/cg;-><init>()V

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/s;->c:I

    .line 27
    iput-object p1, p0, Lcom/google/android/play/headerlist/s;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 28
    new-instance v0, Lcom/google/android/play/headerlist/t;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/t;-><init>(Lcom/google/android/play/headerlist/s;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/s;->d:Landroid/support/v7/widget/bx;

    .line 60
    return-void
.end method

.method private a(Landroid/support/v7/widget/bv;)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->e:Landroid/support/v7/widget/bv;

    if-ne v0, p1, :cond_0

    .line 95
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->e:Landroid/support/v7/widget/bv;

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->e:Landroid/support/v7/widget/bv;

    iget-object v1, p0, Lcom/google/android/play/headerlist/s;->d:Landroid/support/v7/widget/bx;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bv;->b(Landroid/support/v7/widget/bx;)V

    .line 90
    :cond_1
    iput-object p1, p0, Lcom/google/android/play/headerlist/s;->e:Landroid/support/v7/widget/bv;

    .line 91
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->e:Landroid/support/v7/widget/bv;

    if-eqz v0, :cond_2

    .line 92
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->e:Landroid/support/v7/widget/bv;

    iget-object v1, p0, Lcom/google/android/play/headerlist/s;->d:Landroid/support/v7/widget/bx;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bv;->a(Landroid/support/v7/widget/bx;)V

    .line 94
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/s;->a(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/play/headerlist/s;)V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/s;->a(Z)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/s;->a(Landroid/support/v7/widget/bv;)V

    .line 100
    iput p2, p0, Lcom/google/android/play/headerlist/s;->b:I

    .line 101
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(I)V

    .line 102
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b:Landroid/support/v7/widget/cg;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b:Landroid/support/v7/widget/cg;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/cg;->a(Landroid/support/v7/widget/RecyclerView;I)V

    .line 106
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3

    .prologue
    .line 110
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/s;->a(Landroid/support/v7/widget/bv;)V

    .line 111
    iget v0, p0, Lcom/google/android/play/headerlist/s;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/view/ViewGroup;)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/s;->c:I

    .line 118
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/headerlist/s;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget v2, p0, Lcom/google/android/play/headerlist/s;->b:I

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v2, p3, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(III)V

    .line 119
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b:Landroid/support/v7/widget/cg;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/play/headerlist/s;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b:Landroid/support/v7/widget/cg;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/widget/cg;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 122
    :cond_0
    return-void

    .line 116
    :cond_1
    iget v0, p0, Lcom/google/android/play/headerlist/s;->c:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/play/headerlist/s;->c:I

    goto :goto_0

    .line 118
    :cond_2
    iget v0, p0, Lcom/google/android/play/headerlist/s;->c:I

    goto :goto_1
.end method

.method final a(Z)V
    .locals 1

    .prologue
    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/s;->c:I

    .line 76
    if-eqz p1, :cond_0

    .line 77
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/s;->a(Landroid/support/v7/widget/bv;)V

    .line 79
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/s;->b:I

    .line 80
    return-void
.end method
