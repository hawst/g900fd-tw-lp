.class public Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/bk;


# static fields
.field private static final e:Z

.field private static final f:Z

.field private static i:Ljava/util/Map;


# instance fields
.field private A:Landroid/view/View;

.field private B:Landroid/support/v7/widget/Toolbar;

.field private C:Lcom/google/android/play/headerlist/g;

.field private D:Landroid/support/v4/view/ViewPager;

.field private E:Landroid/view/ViewGroup;

.field private F:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private G:Lcom/google/android/play/headerlist/g;

.field private H:Lcom/google/android/play/widget/ScrollProxyView;

.field private I:I

.field private J:I

.field private K:I

.field private L:Z

.field private M:Z

.field private N:I

.field private O:Z

.field private P:I

.field private Q:F

.field private R:I

.field private S:I

.field private T:I

.field private U:Z

.field private V:I

.field private W:Z

.field a:Landroid/widget/AbsListView$OnScrollListener;

.field private aA:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

.field private aB:Ljava/util/Map;

.field private final aC:Ljava/lang/Runnable;

.field private final aD:Ljava/lang/Runnable;

.field private final aE:Ljava/lang/Runnable;

.field private aa:I

.field private ab:I

.field private ac:Landroid/support/v4/view/cc;

.field private ad:Landroid/graphics/drawable/Drawable;

.field private ae:Ljava/lang/CharSequence;

.field private af:Z

.field private ag:F

.field private ah:Z

.field private ai:F

.field private aj:I

.field private ak:Z

.field private al:I

.field private am:Z

.field private an:Z

.field private ao:Z

.field private ap:Z

.field private aq:F

.field private ar:Z

.field private as:Ljava/lang/Runnable;

.field private at:I

.field private au:Z

.field private av:Z

.field private final aw:Lcom/google/android/play/headerlist/q;

.field private final ax:Lcom/google/android/play/headerlist/s;

.field private final ay:Lcom/google/android/play/headerlist/x;

.field private final az:F

.field b:Landroid/support/v7/widget/cg;

.field c:Lcom/google/android/play/headerlist/m;

.field d:Lcom/google/android/play/headerlist/o;

.field private final g:Landroid/os/Handler;

.field private final h:Landroid/support/v4/view/cc;

.field private j:Landroid/widget/FrameLayout;

.field private k:Lcom/google/android/play/headerlist/g;

.field private l:Landroid/view/View;

.field private m:Lcom/google/android/play/headerlist/g;

.field private n:Landroid/view/View;

.field private o:Lcom/google/android/play/headerlist/g;

.field private p:Landroid/view/ViewGroup;

.field private q:Lcom/google/android/play/headerlist/g;

.field private r:Landroid/view/View;

.field private s:Lcom/google/android/play/headerlist/g;

.field private t:Landroid/widget/FrameLayout;

.field private u:Lcom/google/android/play/headerlist/g;

.field private v:Landroid/view/View;

.field private w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Lcom/google/android/play/headerlist/g;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xa

    if-le v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    .line 103
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->f:Z

    .line 427
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->i:Ljava/util/Map;

    return-void

    :cond_0
    move v0, v2

    .line 102
    goto :goto_0

    :cond_1
    move v1, v2

    .line 103
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1122
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1123
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1130
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 319
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g:Landroid/os/Handler;

    .line 325
    new-instance v0, Lcom/google/android/play/headerlist/a;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/a;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->h:Landroid/support/v4/view/cc;

    .line 515
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->T:I

    .line 571
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->al:I

    .line 580
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->an:Z

    .line 590
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aq:F

    .line 592
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ar:Z

    .line 616
    new-instance v0, Lcom/google/android/play/headerlist/q;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/q;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aw:Lcom/google/android/play/headerlist/q;

    .line 623
    new-instance v0, Lcom/google/android/play/headerlist/s;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/s;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ax:Lcom/google/android/play/headerlist/s;

    .line 631
    new-instance v0, Lcom/google/android/play/headerlist/x;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/x;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ay:Lcom/google/android/play/headerlist/x;

    .line 641
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aB:Ljava/util/Map;

    .line 644
    new-instance v0, Lcom/google/android/play/headerlist/b;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/b;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aC:Ljava/lang/Runnable;

    .line 652
    new-instance v0, Lcom/google/android/play/headerlist/c;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/c;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aD:Ljava/lang/Runnable;

    .line 660
    new-instance v0, Lcom/google/android/play/headerlist/d;

    invoke-direct {v0, p0}, Lcom/google/android/play/headerlist/d;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aE:Ljava/lang/Runnable;

    .line 1131
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x41a00000    # 20.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->az:F

    .line 1134
    return-void
.end method

.method private A()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 2975
    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ab:I

    packed-switch v2, :pswitch_data_0

    .line 2989
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->W:Z

    if-eqz v2, :cond_a

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v2

    cmpl-float v2, v2, v3

    if-lez v2, :cond_a

    sget-boolean v2, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    if-nez v2, :cond_5

    move v2, v1

    :goto_0
    if-nez v2, :cond_a

    .line 2995
    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ap:Z

    if-eq v2, v0, :cond_3

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ap:Z

    sget-boolean v2, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->f:Z

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->q:Lcom/google/android/play/headerlist/g;

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->s()F

    move-result v1

    :goto_2
    invoke-virtual {v2, v1}, Lcom/google/android/play/headerlist/g;->c(F)V

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->C:Lcom/google/android/play/headerlist/g;

    if-eqz v0, :cond_c

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->s()F

    move-result v1

    :goto_3
    invoke-virtual {v2, v1}, Lcom/google/android/play/headerlist/g;->c(F)V

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->y:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->s()F

    move-result v3

    :cond_2
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setZ(F)V

    .line 2996
    :cond_3
    :goto_4
    return-void

    :pswitch_0
    move v0, v1

    .line 2978
    goto :goto_1

    .line 2980
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v2

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_1

    move v0, v1

    goto :goto_1

    .line 2983
    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v2

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2989
    :cond_5
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_6

    move v2, v1

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMeasuredHeight()I

    move-result v2

    if-eqz v2, :cond_9

    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-nez v2, :cond_7

    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->W:Z

    if-eqz v2, :cond_9

    :cond_7
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v2

    int-to-float v4, v2

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->k:Lcom/google/android/play/headerlist/g;

    iget-object v5, v2, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    if-eqz v5, :cond_8

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_8

    iget-object v2, v2, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    :goto_5
    add-float/2addr v2, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v4

    sub-float/2addr v2, v4

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    cmpl-float v2, v2, v3

    if-gtz v2, :cond_9

    move v2, v1

    goto/16 :goto_0

    :cond_8
    iget v2, v2, Lcom/google/android/play/headerlist/g;->c:F

    goto :goto_5

    :cond_9
    move v2, v0

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto/16 :goto_1

    :cond_b
    move v1, v3

    .line 2995
    goto/16 :goto_2

    :cond_c
    move v1, v3

    goto :goto_3

    :cond_d
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r:Landroid/view/View;

    if-eqz v0, :cond_e

    :goto_6
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_e
    const/4 v1, 0x4

    goto :goto_6

    .line 2975
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 2129
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/d;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private static a(Landroid/content/Context;I)I
    .locals 2

    .prologue
    .line 2151
    packed-switch p1, :pswitch_data_0

    .line 2159
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2154
    :pswitch_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/d;->u:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2157
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2151
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;II)I
    .locals 2

    .prologue
    .line 1118
    invoke-static {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v0

    invoke-static {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;I)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    return v0
.end method

.method static synthetic a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;I)I
    .locals 0

    .prologue
    .line 99
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->al:I

    return p1
.end method

.method private a(Ljava/lang/String;FF)Landroid/animation/ObjectAnimator;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 3362
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aB:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    .line 3363
    if-eqz v0, :cond_0

    .line 3364
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 3366
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p2, v0, v1

    const/4 v1, 0x1

    aput p3, v0, v1

    invoke-static {p0, p1, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 3367
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aB:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3368
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/cc;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ac:Landroid/support/v4/view/cc;

    return-object v0
.end method

.method private static a(Landroid/view/View;)Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 2055
    if-eqz p0, :cond_0

    instance-of v0, p0, Landroid/widget/ListView;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/support/v7/widget/RecyclerView;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/play/headerlist/z;

    if-eqz v0, :cond_1

    .line 2057
    :cond_0
    check-cast p0, Landroid/view/ViewGroup;

    return-object p0

    .line 2059
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Found a view that isn\'t a ListView or a RecyclerView or a PlayScrollableContentView implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(FZ)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x64

    const/16 v4, 0xc

    const/4 v6, 0x1

    .line 2692
    if-eqz p2, :cond_4

    .line 2693
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->T:I

    packed-switch v0, :pswitch_data_0

    .line 2717
    :cond_0
    :goto_0
    return-void

    .line 2695
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->u:Lcom/google/android/play/headerlist/g;

    iget-object v1, v0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    if-nez v1, :cond_1

    iput p1, v0, Lcom/google/android/play/headerlist/g;->d:F

    goto :goto_0

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_2

    iget-object v0, v0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    :cond_2
    new-instance v1, Landroid/view/animation/ScaleAnimation;

    iget v2, v0, Lcom/google/android/play/headerlist/g;->d:F

    iget v3, v0, Lcom/google/android/play/headerlist/g;->d:F

    invoke-direct {v1, v2, p1, v3, p1}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    invoke-virtual {v1, v8, v9}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    invoke-virtual {v1, v6}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v4, :cond_3

    new-instance v2, Lcom/google/android/play/headerlist/i;

    invoke-direct {v2, v0, p1}, Lcom/google/android/play/headerlist/i;-><init>(Lcom/google/android/play/headerlist/g;F)V

    invoke-virtual {v1, v2}, Landroid/view/animation/ScaleAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    :cond_3
    iget-object v0, v0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 2701
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->u:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/g;->b(F)V

    goto :goto_0

    .line 2709
    :cond_4
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->T:I

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 2711
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->u:Lcom/google/android/play/headerlist/g;

    iget-object v1, v0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    if-nez v1, :cond_5

    iput p1, v0, Lcom/google/android/play/headerlist/g;->d:F

    goto :goto_0

    :cond_5
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_6

    iget-object v1, v0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setScaleX(F)V

    iget-object v0, v0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0

    :cond_6
    iget v1, v0, Lcom/google/android/play/headerlist/g;->d:F

    cmpl-float v1, v1, p1

    if-eqz v1, :cond_0

    iput p1, v0, Lcom/google/android/play/headerlist/g;->d:F

    new-instance v1, Landroid/view/animation/ScaleAnimation;

    iget v2, v0, Lcom/google/android/play/headerlist/g;->d:F

    iget v3, v0, Lcom/google/android/play/headerlist/g;->d:F

    iget v4, v0, Lcom/google/android/play/headerlist/g;->d:F

    iget v5, v0, Lcom/google/android/play/headerlist/g;->d:F

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    invoke-virtual {v1, v6}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    iget-object v0, v0, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 2716
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->u:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v0, p1}, Lcom/google/android/play/headerlist/g;->a(F)V

    goto/16 :goto_0

    .line 2693
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 2709
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3349
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 3351
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 3352
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3353
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2915
    if-eqz p2, :cond_3

    .line 2916
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2917
    if-nez v0, :cond_0

    .line 2918
    invoke-static {}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->y()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2920
    :cond_0
    if-nez p1, :cond_1

    invoke-static {}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->y()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 2921
    :cond_1
    if-ne v0, p1, :cond_2

    .line 2940
    :goto_0
    return-void

    .line 2924
    :cond_2
    new-instance v1, Lcom/google/android/play/headerlist/e;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    aput-object p1, v2, v4

    invoke-direct {v1, p0, v2}, Lcom/google/android/play/headerlist/e;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;[Landroid/graphics/drawable/Drawable;)V

    .line 2934
    invoke-virtual {v1, v4}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 2935
    const/16 v0, 0x12c

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 2936
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2938
    :cond_3
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;ZZ)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(ZZZ)V

    return-void
.end method

.method private a(ZZ)V
    .locals 8

    .prologue
    const-wide/16 v6, 0xc8

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 2828
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-ne v2, p1, :cond_0

    .line 2862
    :goto_0
    return-void

    .line 2832
    :cond_0
    if-eqz p1, :cond_4

    .line 2833
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v2

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p()F

    move-result v3

    sub-float/2addr v2, v3

    .line 2835
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->q()F

    move-result v3

    .line 2836
    cmpl-float v4, v3, v1

    if-nez v4, :cond_3

    .line 2837
    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    .line 2845
    :goto_1
    iput-boolean p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    .line 2848
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->W:Z

    if-nez v2, :cond_1

    .line 2849
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v2, :cond_5

    .line 2850
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ad:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v2, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 2856
    :cond_1
    :goto_2
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->U:Z

    if-eqz v2, :cond_2

    .line 2857
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v2, :cond_6

    :goto_3
    if-eqz p2, :cond_8

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aq:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_7

    const-string v1, "actionBarTitleAlpha"

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aq:F

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Ljava/lang/String;FF)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2859
    :cond_2
    :goto_4
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->A()V

    .line 2860
    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(Z)V

    .line 2861
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->z()V

    goto :goto_0

    .line 2839
    :cond_3
    div-float/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    goto :goto_1

    .line 2843
    :cond_4
    iput v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    goto :goto_1

    .line 2852
    :cond_5
    invoke-static {}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->y()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {p0, v2, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/graphics/drawable/Drawable;Z)V

    goto :goto_2

    :cond_6
    move v0, v1

    .line 2857
    goto :goto_3

    :cond_7
    new-instance v1, Lcom/google/android/play/headerlist/f;

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aq:F

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/play/headerlist/f;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;FF)V

    invoke-virtual {v1, v6, v7}, Lcom/google/android/play/headerlist/f;->setDuration(J)V

    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_4

    :cond_8
    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(F)V

    goto :goto_4
.end method

.method private a(ZZZ)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const-wide/16 v6, 0xc8

    const/4 v4, 0x1

    .line 2735
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->V:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 2780
    :cond_0
    :goto_0
    return-void

    .line 2738
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aC:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2739
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aD:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2741
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v1

    .line 2742
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p()F

    move-result v0

    .line 2743
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->o()F

    move-result v2

    .line 2744
    if-nez p2, :cond_2

    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    :cond_2
    cmpg-float v0, v1, v2

    if-gez v0, :cond_0

    .line 2746
    if-eqz p1, :cond_4

    const/high16 v0, 0x3f800000    # 1.0f

    .line 2748
    :goto_1
    iget-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-nez v3, :cond_3

    .line 2750
    cmpl-float v1, v1, v2

    if-gez v1, :cond_0

    .line 2761
    invoke-direct {p0, v4, p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(ZZ)V

    .line 2764
    :cond_3
    if-eqz p3, :cond_6

    .line 2765
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_5

    .line 2766
    const-string v1, "floatingFraction"

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->x()F

    move-result v2

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Ljava/lang/String;FF)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 2746
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 2770
    :cond_5
    new-instance v1, Lcom/google/android/play/headerlist/l;

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->x()F

    move-result v2

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/play/headerlist/l;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;FF)V

    .line 2773
    invoke-virtual {v1, v6, v7}, Lcom/google/android/play/headerlist/l;->setDuration(J)V

    .line 2774
    invoke-virtual {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 2777
    :cond_6
    invoke-direct {p0, v0, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(FZ)V

    goto :goto_0
.end method

.method private b(I)I
    .locals 2

    .prologue
    .line 1991
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v0

    .line 1992
    if-nez p1, :cond_0

    .line 1993
    add-int/lit8 v0, v0, -0x1

    .line 1995
    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 1996
    add-int/lit8 v0, v0, 0x1

    .line 1998
    :cond_1
    return v0
.end method

.method static synthetic b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->V:I

    return v0
.end method

.method private final b(FZ)V
    .locals 1

    .prologue
    .line 2793
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 2794
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    .line 2795
    if-eqz p2, :cond_0

    .line 2799
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->v()V

    .line 2800
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Z)V

    .line 2803
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1390
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->H:Lcom/google/android/play/widget/ScrollProxyView;

    invoke-virtual {v2}, Lcom/google/android/play/widget/ScrollProxyView;->getScrollY()I

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_0
    if-eq p1, v2, :cond_1

    .line 1395
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->H:Lcom/google/android/play/widget/ScrollProxyView;

    if-eqz p1, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v2, v1, v0}, Lcom/google/android/play/widget/ScrollProxyView;->scrollTo(II)V

    .line 1397
    :cond_1
    return-void

    :cond_2
    move v2, v1

    .line 1390
    goto :goto_0
.end method

.method private b(Landroid/view/ViewGroup;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1909
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    if-ne v0, p1, :cond_1

    .line 1910
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1971
    :goto_0
    return v2

    :cond_0
    move v2, v3

    .line 1910
    goto :goto_0

    .line 1912
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    if-eqz v0, :cond_8

    move v1, v2

    .line 1913
    :goto_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 1914
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_9

    .line 1915
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1916
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aw:Lcom/google/android/play/headerlist/q;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/q;->a()V

    .line 1927
    :cond_2
    :goto_2
    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    .line 1929
    :cond_3
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    .line 1930
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    if-eqz v0, :cond_e

    .line 1931
    iget-boolean v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    .line 1932
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    if-nez v0, :cond_4

    .line 1938
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isLayoutRequested()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    .line 1944
    :cond_4
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_b

    .line 1945
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aw:Lcom/google/android/play/headerlist/q;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1953
    :cond_5
    :goto_3
    iput-boolean v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    .line 1954
    if-eqz v1, :cond_6

    .line 1958
    invoke-direct {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Z)V

    .line 1962
    :cond_6
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    if-eqz v0, :cond_d

    .line 1963
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->M:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/play/headerlist/o;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->av:Z

    goto :goto_0

    :cond_8
    move v1, v3

    .line 1912
    goto :goto_1

    .line 1917
    :cond_9
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    instance-of v0, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_a

    .line 1918
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cg;)V

    .line 1919
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ax:Lcom/google/android/play/headerlist/s;

    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/s;->a(Z)V

    goto :goto_2

    .line 1920
    :cond_a
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    instance-of v0, v0, Lcom/google/android/play/headerlist/z;

    if-eqz v0, :cond_2

    .line 1921
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    .line 1922
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ay:Lcom/google/android/play/headerlist/x;

    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/x;->a(Z)V

    goto :goto_2

    .line 1946
    :cond_b
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    instance-of v0, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_c

    .line 1947
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ax:Lcom/google/android/play/headerlist/s;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cg;)V

    goto :goto_3

    .line 1949
    :cond_c
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    instance-of v0, v0, Lcom/google/android/play/headerlist/z;

    if-eqz v0, :cond_5

    .line 1950
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ay:Lcom/google/android/play/headerlist/x;

    goto :goto_3

    .line 1967
    :cond_d
    iput-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->av:Z

    goto/16 :goto_0

    :cond_e
    move v2, v3

    .line 1971
    goto/16 :goto_0
.end method

.method private c(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2253
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 2254
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2255
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v2

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->K:I

    if-ne v2, v3, :cond_0

    move-object v0, v1

    .line 2267
    :goto_1
    return-object v0

    .line 2253
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2267
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(I)Landroid/view/ViewGroup;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2040
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2041
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->J:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    .line 2047
    :goto_0
    return-object v0

    .line 2043
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ao;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ltz v4, :cond_1

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ao;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ao;->c()I

    move-result v0

    if-lt v4, v0, :cond_2

    :cond_1
    move-object v0, v1

    .line 2044
    :goto_1
    if-eqz v0, :cond_6

    .line 2045
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->J:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    goto :goto_0

    .line 2043
    :cond_2
    const/4 v0, 0x0

    :goto_2
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v2}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;)Landroid/support/v4/view/by;

    move-result-object v3

    if-nez v3, :cond_3

    move-object v3, v1

    :goto_3
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v4, :cond_4

    move-object v0, v2

    goto :goto_1

    :cond_3
    iget v3, v3, Landroid/support/v4/view/by;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_3

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_1

    :cond_6
    move-object v0, v1

    .line 2047
    goto :goto_0
.end method

.method private c(FZ)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 3296
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ag:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    .line 3311
    :goto_0
    return-void

    .line 3300
    :cond_0
    if-eqz p2, :cond_1

    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    if-eqz v0, :cond_1

    .line 3301
    const-string v0, "bannerFraction"

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ag:F

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Ljava/lang/String;FF)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 3305
    :cond_1
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ag:F

    .line 3309
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->v()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->m()V

    return-void
.end method

.method private c(Z)V
    .locals 1

    .prologue
    .line 2599
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Landroid/view/ViewGroup;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2621
    :cond_0
    :goto_0
    return-void

    .line 2605
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/view/ViewGroup;)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    .line 2606
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    if-eqz v0, :cond_2

    .line 2607
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(Z)V

    .line 2614
    :cond_2
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w()Z

    .line 2618
    if-eqz p1, :cond_0

    .line 2619
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->v()V

    goto :goto_0

    .line 2607
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private d(Landroid/view/ViewGroup;)I
    .locals 2

    .prologue
    .line 2303
    const/4 v0, -0x1

    .line 2304
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2305
    if-eqz v1, :cond_0

    .line 2306
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 2308
    :cond_0
    return v0
.end method

.method static synthetic d(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/widget/SwipeRefreshLayout;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method private d(Z)V
    .locals 2

    .prologue
    .line 2868
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->N:I

    if-nez v0, :cond_0

    .line 2870
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aa:I

    packed-switch v0, :pswitch_data_0

    .line 2879
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    .line 2882
    :goto_0
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(ZZ)V

    .line 2884
    :cond_0
    return-void

    .line 2872
    :pswitch_0
    const/4 v0, 0x1

    .line 2873
    goto :goto_0

    .line 2875
    :pswitch_1
    const/4 v0, 0x0

    .line 2876
    goto :goto_0

    .line 2870
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private d(I)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3103
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ao;

    move-result-object v0

    if-nez v0, :cond_1

    .line 3112
    :cond_0
    :goto_0
    return v3

    .line 3107
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(I)I

    move-result v0

    .line 3108
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ao;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ao;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 3111
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(I)Landroid/view/ViewGroup;

    move-result-object v0

    .line 3112
    if-ne p1, v2, :cond_2

    move v1, v2

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Landroid/view/ViewGroup;)Z

    move-result v4

    if-nez v4, :cond_3

    move v3, v2

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(Landroid/view/ViewGroup;)I

    move-result v5

    const/4 v4, -0x1

    if-ne v5, v4, :cond_6

    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-nez v1, :cond_0

    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    instance-of v1, v0, Landroid/widget/ListView;

    if-eqz v1, :cond_5

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v3, v3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_4
    :goto_2
    iput-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    move v3, v2

    goto :goto_0

    :cond_5
    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->a(I)V

    goto :goto_2

    :cond_6
    iget-boolean v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v4, :cond_a

    iget v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->V:I

    if-eqz v4, :cond_7

    iget v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->V:I

    const/4 v6, 0x2

    if-ne v4, v6, :cond_a

    :cond_7
    move v4, v2

    :goto_3
    if-eqz v4, :cond_b

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->o()F

    move-result v4

    :goto_4
    float-to-int v4, v4

    iget v6, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    add-int/2addr v4, v6

    sub-int v4, v5, v4

    iget-boolean v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v5, :cond_8

    if-ltz v4, :cond_0

    :cond_8
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-lez v5, :cond_e

    invoke-static {v0, v4}, Landroid/support/v4/view/at;->b(Landroid/view/View;I)Z

    move-result v5

    if-eqz v5, :cond_e

    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    instance-of v5, v0, Landroid/widget/ListView;

    if-eqz v5, :cond_d

    check-cast v0, Landroid/widget/ListView;

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x13

    if-lt v5, v6, :cond_c

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->scrollListBy(I)V

    :cond_9
    :goto_5
    iput-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    if-eqz v1, :cond_0

    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ak:Z

    goto/16 :goto_0

    :cond_a
    move v4, v3

    goto :goto_3

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v4

    goto :goto_4

    :cond_c
    invoke-virtual {v0, v4, v3}, Landroid/widget/ListView;->smoothScrollBy(II)V

    goto :goto_5

    :cond_d
    instance-of v5, v0, Landroid/support/v7/widget/RecyclerView;

    if-eqz v5, :cond_9

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3, v4}, Landroid/support/v7/widget/RecyclerView;->scrollBy(II)V

    goto :goto_5

    :cond_e
    if-eqz v1, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(Z)V

    goto/16 :goto_0
.end method

.method static synthetic e(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->al:I

    return v0
.end method

.method private e(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3069
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 3096
    :goto_0
    return-void

    .line 3074
    :cond_0
    if-eqz p1, :cond_4

    .line 3075
    invoke-direct {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(I)Z

    move-result v0

    .line 3076
    if-nez v0, :cond_1

    .line 3081
    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    .line 3085
    :cond_1
    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(I)Z

    move-result v4

    or-int/2addr v4, v0

    .line 3086
    invoke-direct {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(I)Z

    move-result v5

    or-int/2addr v4, v5

    .line 3087
    if-eqz v4, :cond_3

    .line 3092
    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->at:I

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_2

    .line 3094
    :cond_3
    iput v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->at:I

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private e(Landroid/view/ViewGroup;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2576
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/widget/ListView;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    if-gt v0, v1, :cond_7

    :cond_0
    move v0, v2

    .line 2591
    :goto_1
    return v0

    .line 2576
    :cond_1
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    goto :goto_0

    :cond_2
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bv;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Landroid/support/v7/widget/bv;->a()I

    move-result v0

    goto :goto_0

    :cond_4
    instance-of v0, p1, Lcom/google/android/play/headerlist/z;

    if-eqz v0, :cond_6

    move-object v0, p1

    check-cast v0, Lcom/google/android/play/headerlist/z;

    invoke-interface {v0}, Lcom/google/android/play/headerlist/z;->a()Landroid/widget/Adapter;

    move-result-object v0

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    goto :goto_0

    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected listview type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2581
    :cond_7
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-le v0, v1, :cond_8

    move v0, v1

    .line 2583
    goto :goto_1

    .line 2585
    :cond_8
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ne v0, v1, :cond_a

    .line 2588
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->K:I

    if-eq v0, v3, :cond_9

    move v0, v1

    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_1

    :cond_a
    move v0, v2

    .line 2591
    goto :goto_1
.end method

.method static synthetic f(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->as:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->as:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)F
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    return v0
.end method

.method static synthetic l()Z
    .locals 1

    .prologue
    .line 99
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->f:Z

    return v0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 1976
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(I)Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(Landroid/view/ViewGroup;)Z

    .line 1977
    return-void
.end method

.method private n()F
    .locals 3

    .prologue
    .line 2068
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private o()F
    .locals 2

    .prologue
    .line 2076
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r()F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method private p()F
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2081
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->af:Z

    if-eqz v1, :cond_0

    .line 2082
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 2098
    :goto_0
    return v0

    .line 2085
    :cond_0
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->V:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2087
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r()F

    move-result v1

    add-float/2addr v0, v1

    .line 2088
    goto :goto_0

    .line 2091
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 2092
    goto :goto_0

    .line 2095
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r()F

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_0

    .line 2085
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private q()F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2103
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->af:Z

    if-eqz v1, :cond_0

    .line 2114
    :goto_0
    :pswitch_0
    return v0

    .line 2106
    :cond_0
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->V:I

    packed-switch v1, :pswitch_data_0

    .line 2116
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2108
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r()F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    goto :goto_0

    .line 2110
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v0

    int-to-float v0, v0

    goto :goto_0

    .line 2112
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r()F

    move-result v0

    goto :goto_0

    .line 2106
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private r()F
    .locals 2

    .prologue
    .line 2121
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->N:I

    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method private s()F
    .locals 2

    .prologue
    .line 2144
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/d;->s:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method private t()I
    .locals 2

    .prologue
    .line 2171
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/d;->r:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private u()V
    .locals 2

    .prologue
    .line 2214
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->M:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    .line 2215
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->I:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    .line 2216
    if-eqz v0, :cond_0

    .line 2226
    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->D:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(Landroid/support/v4/view/ViewPager;)V

    .line 2229
    :cond_0
    return-void
.end method

.method private v()V
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2321
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    if-nez v0, :cond_1

    .line 2457
    :cond_0
    :goto_0
    return-void

    .line 2327
    :cond_1
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ag:F

    sub-float/2addr v0, v5

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->t()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 2328
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->z:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/g;->d(F)V

    .line 2330
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->t()I

    move-result v1

    int-to-float v1, v1

    add-float v2, v1, v0

    .line 2334
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->o:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/g;->d(F)V

    .line 2338
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n()F

    move-result v1

    .line 2340
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v0, :cond_6

    .line 2342
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    neg-int v0, v0

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p()F

    move-result v7

    add-float/2addr v0, v7

    iget v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->q()F

    move-result v8

    mul-float/2addr v7, v8

    add-float/2addr v0, v7

    iget v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    int-to-float v7, v7

    add-float/2addr v0, v7

    add-float/2addr v0, v2

    .line 2351
    :goto_1
    iget-object v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->q:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v7, v0}, Lcom/google/android/play/headerlist/g;->d(F)V

    .line 2354
    iget-object v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->s:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v7, v0}, Lcom/google/android/play/headerlist/g;->d(F)V

    .line 2360
    iget v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->V:I

    if-eqz v7, :cond_2

    iget v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->V:I

    if-ne v7, v3, :cond_e

    .line 2362
    :cond_2
    iget-boolean v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v7, :cond_7

    .line 2364
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    sub-float v1, v5, v1

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->q()F

    move-result v7

    mul-float/2addr v1, v7

    sub-float v1, v2, v1

    .line 2373
    :goto_2
    iget-object v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->C:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v7, v1}, Lcom/google/android/play/headerlist/g;->d(F)V

    .line 2376
    iget-object v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->t:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    .line 2377
    iget v8, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    int-to-float v8, v8

    sub-float/2addr v8, v7

    iget v9, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    int-to-float v9, v9

    sub-float/2addr v8, v9

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    .line 2378
    iget v9, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->T:I

    packed-switch v9, :pswitch_data_0

    .line 2405
    :cond_3
    :goto_3
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->O:Z

    if-eqz v0, :cond_4

    .line 2407
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    if-eqz v0, :cond_d

    .line 2408
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(Landroid/view/ViewGroup;)I

    move-result v0

    .line 2409
    if-eq v0, v10, :cond_d

    .line 2410
    int-to-float v0, v0

    add-float/2addr v0, v2

    .line 2413
    :goto_4
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->m:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/g;->d(F)V

    .line 2419
    :cond_4
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    if-ne v0, v10, :cond_b

    .line 2421
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2452
    :cond_5
    :goto_5
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->A()V

    .line 2454
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c:Lcom/google/android/play/headerlist/m;

    if-eqz v0, :cond_0

    .line 2455
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c:Lcom/google/android/play/headerlist/m;

    invoke-interface {v0}, Lcom/google/android/play/headerlist/m;->ak()V

    goto/16 :goto_0

    .line 2349
    :cond_6
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    int-to-float v0, v0

    sub-float v0, v1, v0

    add-float/2addr v0, v2

    goto :goto_1

    .line 2367
    :cond_7
    iget v7, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    int-to-float v7, v7

    sub-float/2addr v1, v7

    add-float/2addr v1, v2

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r()F

    move-result v7

    sub-float/2addr v1, v7

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v7

    int-to-float v7, v7

    sub-float/2addr v1, v7

    .line 2370
    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    goto :goto_2

    .line 2383
    :pswitch_0
    add-float/2addr v0, v8

    .line 2384
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v1, v7

    .line 2385
    cmpl-float v0, v0, v1

    if-ltz v0, :cond_8

    move v0, v3

    .line 2386
    :goto_6
    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->an:Z

    if-eq v1, v0, :cond_3

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->an:Z

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->an:Z

    if-eqz v0, :cond_9

    move v0, v5

    :goto_7
    invoke-direct {p0, v0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(FZ)V

    goto :goto_3

    :cond_8
    move v0, v4

    .line 2385
    goto :goto_6

    :cond_9
    move v0, v6

    .line 2386
    goto :goto_7

    .line 2390
    :pswitch_1
    add-float/2addr v0, v7

    div-float/2addr v0, v7

    invoke-static {v6, v0}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 2395
    cmpl-float v0, v1, v6

    if-lez v0, :cond_a

    move v0, v3

    :goto_8
    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->an:Z

    .line 2396
    invoke-direct {p0, v1, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(FZ)V

    goto/16 :goto_3

    :cond_a
    move v0, v4

    .line 2395
    goto :goto_8

    .line 2424
    :cond_b
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_c

    .line 2425
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2428
    :goto_9
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->Q:F

    div-float/2addr v0, v1

    .line 2430
    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    neg-int v1, v1

    int-to-float v1, v1

    iget v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->Q:F

    mul-float/2addr v1, v4

    add-float/2addr v1, v2

    .line 2431
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2432
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->k:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/g;->d(F)V

    .line 2435
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    sub-float/2addr v0, v2

    .line 2438
    if-eqz v3, :cond_5

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->az:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    .line 2439
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->k:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v0, v6}, Lcom/google/android/play/headerlist/g;->a(F)V

    .line 2440
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->k:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v0, v5}, Lcom/google/android/play/headerlist/g;->b(F)V

    goto/16 :goto_5

    :cond_c
    move v3, v4

    goto :goto_9

    :cond_d
    move v0, v2

    goto/16 :goto_4

    :cond_e
    move v1, v2

    goto/16 :goto_2

    .line 2378
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private w()Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2550
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->o()F

    move-result v3

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    int-to-float v3, v3

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    if-le v3, v0, :cond_1

    :cond_0
    move v0, v2

    .line 2551
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eq v0, v3, :cond_5

    .line 2554
    invoke-direct {p0, v0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(ZZ)V

    .line 2560
    :goto_1
    return v2

    :cond_1
    move v0, v1

    .line 2550
    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    sub-int/2addr v0, v3

    int-to-float v0, v0

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p()F

    move-result v3

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    if-ge v3, v0, :cond_3

    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    if-ne v0, v4, :cond_4

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v1

    .line 2560
    goto :goto_1
.end method

.method private x()F
    .locals 1

    .prologue
    .line 2809
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v0, :cond_0

    .line 2810
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    .line 2812
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static y()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 2819
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method private z()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2891
    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->R:I

    packed-switch v2, :pswitch_data_0

    move v1, v0

    .line 2906
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(Z)V

    .line 2907
    return-void

    .line 2896
    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->W:Z

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    move v1, v0

    .line 2897
    goto :goto_0

    .line 2899
    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->W:Z

    if-eqz v2, :cond_0

    :cond_3
    move v1, v0

    goto :goto_0

    .line 2891
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)I
    .locals 3

    .prologue
    .line 2277
    const/4 v0, -0x1

    .line 2278
    instance-of v1, p1, Landroid/widget/ListView;

    if-eqz v1, :cond_1

    .line 2281
    check-cast p1, Landroid/widget/ListView;

    .line 2282
    invoke-virtual {p1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 2283
    invoke-virtual {p1}, Landroid/widget/ListView;->getChildCount()I

    move-result v2

    .line 2284
    if-nez v1, :cond_0

    if-lez v2, :cond_0

    .line 2285
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    .line 2295
    :cond_0
    :goto_0
    return v0

    .line 2287
    :cond_1
    instance-of v1, p1, Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_2

    instance-of v1, p1, Lcom/google/android/play/headerlist/z;

    if-eqz v1, :cond_0

    .line 2290
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2291
    if-eqz v1, :cond_0

    .line 2292
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method protected final a(F)V
    .locals 4

    .prologue
    .line 1804
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aq:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 1805
    iput p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aq:F

    .line 1806
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->B:Landroid/support/v7/widget/Toolbar;

    const/4 v1, 0x0

    const/16 v2, 0xff

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v3, p1

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    const v2, 0xffffff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->b(I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->c(I)V

    .line 1808
    :cond_0
    return-void
.end method

.method final a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2631
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aC:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2632
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aD:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2633
    if-nez p1, :cond_1

    .line 2637
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n()F

    move-result v0

    iget v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    move v0, v1

    .line 2639
    :goto_0
    iget-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ar:Z

    if-eqz v3, :cond_4

    .line 2642
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    .line 2643
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e()F

    move-result v4

    .line 2644
    if-nez v0, :cond_0

    cmpl-float v0, v4, v3

    if-ltz v0, :cond_3

    .line 2652
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g:Landroid/os/Handler;

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aD:Ljava/lang/Runnable;

    :goto_2
    const-wide/16 v4, 0x32

    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2660
    invoke-direct {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Z)V

    .line 2662
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 2637
    goto :goto_0

    :cond_3
    move v1, v2

    .line 2644
    goto :goto_1

    :cond_4
    move v1, v0

    .line 2646
    goto :goto_1

    .line 2652
    :cond_5
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aC:Ljava/lang/Runnable;

    goto :goto_2
.end method

.method final a(III)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2470
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    if-eqz v0, :cond_0

    .line 2472
    if-nez p3, :cond_3

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(Z)V

    .line 2476
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    :cond_1
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    if-nez v0, :cond_4

    .line 2520
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v2

    .line 2472
    goto :goto_0

    .line 2486
    :cond_4
    iput p3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    .line 2487
    packed-switch p1, :pswitch_data_0

    .line 2493
    :goto_2
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2507
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v0, :cond_5

    .line 2508
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->q()F

    move-result v0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_7

    .line 2509
    iput v4, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    .line 2519
    :cond_5
    :goto_3
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->v()V

    goto :goto_1

    :pswitch_0
    move-object v0, p0

    .line 2492
    :goto_4
    iput-boolean v1, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ar:Z

    goto :goto_2

    :pswitch_1
    int-to-float v0, p2

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_6

    move-object v0, p0

    goto :goto_4

    :cond_6
    move v1, v2

    move-object v0, p0

    goto :goto_4

    .line 2511
    :cond_7
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    int-to-float v1, p2

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->q()F

    move-result v2

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    .line 2512
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    goto :goto_3

    .line 2487
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    .line 1463
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->al:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->M:Z

    if-nez v0, :cond_1

    .line 1464
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p2}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Z)V

    .line 1466
    :cond_1
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1483
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ad:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->W:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ad:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 1484
    :cond_2
    return-void
.end method

.method public final a(Landroid/support/v4/view/cc;)V
    .locals 0

    .prologue
    .line 1533
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ac:Landroid/support/v4/view/cc;

    .line 1534
    return-void
.end method

.method public final a(Landroid/support/v7/widget/cg;)V
    .locals 0

    .prologue
    .line 1549
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b:Landroid/support/v7/widget/cg;

    .line 1550
    return-void
.end method

.method public final a(Lcom/google/android/play/headerlist/j;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1142
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->a()F

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->Q:F

    .line 1143
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->J:I

    .line 1144
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->I:I

    .line 1145
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->K:I

    .line 1146
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->K:I

    if-nez v0, :cond_5

    sget v0, Lcom/google/android/play/f;->P:I

    :goto_0
    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->K:I

    .line 1147
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->L:Z

    .line 1148
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->M:Z

    .line 1149
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->g()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->N:I

    .line 1150
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->m()I

    iput-boolean v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->O:Z

    .line 1152
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->j()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->T:I

    .line 1153
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->k()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->U:Z

    .line 1155
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->n()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->V:I

    .line 1156
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->o()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ab:I

    .line 1157
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->l()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    if-nez v0, :cond_7

    :cond_0
    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->W:Z

    .line 1159
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->h()I

    move-result v0

    :goto_3
    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aa:I

    .line 1161
    iput v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->R:I

    .line 1162
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 1163
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/play/headerlist/j;->a(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    move-result-object v1

    .line 1167
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    if-eqz v0, :cond_9

    sget v0, Lcom/google/android/play/h;->a:I

    :goto_4
    invoke-virtual {v4, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1169
    sget v0, Lcom/google/android/play/f;->i:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    .line 1170
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    invoke-direct {v0, v5}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->k:Lcom/google/android/play/headerlist/g;

    .line 1171
    sget v0, Lcom/google/android/play/f;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->l:Landroid/view/View;

    .line 1172
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->l:Landroid/view/View;

    invoke-direct {v0, v5}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->m:Lcom/google/android/play/headerlist/g;

    .line 1173
    sget v0, Lcom/google/android/play/f;->j:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    .line 1174
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    invoke-direct {v0, v5}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->o:Lcom/google/android/play/headerlist/g;

    .line 1175
    sget v0, Lcom/google/android/play/f;->k:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p:Landroid/view/ViewGroup;

    .line 1176
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p:Landroid/view/ViewGroup;

    invoke-direct {v0, v5}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->q:Lcom/google/android/play/headerlist/g;

    .line 1177
    sget v0, Lcom/google/android/play/f;->r:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r:Landroid/view/View;

    .line 1178
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r:Landroid/view/View;

    invoke-direct {v0, v5}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->s:Lcom/google/android/play/headerlist/g;

    .line 1179
    sget v0, Lcom/google/android/play/f;->s:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->t:Landroid/widget/FrameLayout;

    .line 1180
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->t:Landroid/widget/FrameLayout;

    invoke-direct {v0, v5}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->u:Lcom/google/android/play/headerlist/g;

    .line 1181
    sget v0, Lcom/google/android/play/f;->at:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->v:Landroid/view/View;

    .line 1182
    sget v0, Lcom/google/android/play/f;->K:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .line 1183
    if-eqz v1, :cond_1

    .line 1184
    iget-object v5, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v5}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v7

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->removeViewAt(I)V

    invoke-virtual {v0, v1, v7, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5, v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v5, v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->removeViewAt(I)V

    invoke-virtual {v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->addView(Landroid/view/View;)V

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a()V

    .line 1185
    iput-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    .line 1187
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->h:Landroid/support/v4/view/cc;

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->a(Landroid/support/v4/view/cc;)V

    .line 1188
    sget v0, Lcom/google/android/play/f;->au:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->x:Landroid/widget/TextView;

    .line 1190
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    .line 1191
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/j;->p()I

    move-result v0

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    .line 1192
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    sub-int v1, v0, v1

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1193
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->f:Z

    if-nez v0, :cond_2

    .line 1197
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    sub-int/2addr v0, v1

    add-int/lit8 v1, v0, -0x1

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->r:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1199
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->L:Z

    if-eqz v0, :cond_a

    sget v0, Lcom/google/android/play/f;->Q:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/Toolbar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/support/v7/app/d;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/d;->a(Landroid/support/v7/widget/Toolbar;)V

    :goto_5
    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->B:Landroid/support/v7/widget/Toolbar;

    .line 1200
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->k()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->A:Landroid/view/View;

    .line 1201
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->A:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->C:Lcom/google/android/play/headerlist/g;

    .line 1202
    sget v0, Lcom/google/android/play/f;->L:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->y:Landroid/widget/TextView;

    .line 1203
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->y:Landroid/widget/TextView;

    invoke-direct {v0, v1}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->z:Lcom/google/android/play/headerlist/g;

    .line 1206
    sget v0, Lcom/google/android/play/f;->aq:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 1207
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-direct {v0, v1}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->G:Lcom/google/android/play/headerlist/g;

    .line 1208
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Landroid/support/v4/widget/bk;)V

    .line 1209
    sget v0, Lcom/google/android/play/f;->ag:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/widget/ScrollProxyView;

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->H:Lcom/google/android/play/widget/ScrollProxyView;

    .line 1211
    invoke-direct {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(Z)V

    .line 1214
    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    if-eqz v0, :cond_3

    .line 1215
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->j:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v4, v0}, Lcom/google/android/play/headerlist/j;->c(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 1216
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->t:Landroid/widget/FrameLayout;

    invoke-virtual {p1, v4, v0}, Lcom/google/android/play/headerlist/j;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 1218
    :cond_3
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    .line 1219
    invoke-virtual {p1, v4, v0}, Lcom/google/android/play/headerlist/j;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 1221
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ne v1, v2, :cond_4

    .line 1222
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1223
    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 1224
    invoke-virtual {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->removeViewAt(I)V

    .line 1225
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 1226
    invoke-virtual {p0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->addView(Landroid/view/View;I)V

    .line 1227
    iput-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    .line 1228
    new-instance v0, Lcom/google/android/play/headerlist/g;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/google/android/play/headerlist/g;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->o:Lcom/google/android/play/headerlist/g;

    .line 1232
    :cond_4
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->N:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected tab mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->N:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1146
    :cond_5
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->K:I

    goto/16 :goto_0

    :cond_6
    move v0, v3

    .line 1153
    goto/16 :goto_1

    :cond_7
    move v0, v3

    .line 1157
    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 1159
    goto/16 :goto_3

    .line 1167
    :cond_9
    sget v0, Lcom/google/android/play/h;->b:I

    goto/16 :goto_4

    .line 1199
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v1, Lcom/google/android/play/f;->c:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    goto/16 :goto_5

    .line 1232
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->v:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setVisibility(I)V

    .line 1233
    :goto_6
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->O:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->l:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/google/android/play/c;->l:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1236
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->W:Z

    if-eqz v0, :cond_c

    .line 1237
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ad:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/graphics/drawable/Drawable;Z)V

    .line 1240
    :cond_c
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->z:Lcom/google/android/play/headerlist/g;

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->t()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/g;->d(F)V

    .line 1243
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ag:F

    invoke-direct {p0, v0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(FZ)V

    .line 1244
    invoke-direct {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(Z)V

    .line 1245
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->z()V

    .line 1246
    return-void

    .line 1232
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->v:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0, v8}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setVisibility(I)V

    goto :goto_6

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->v:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->setVisibility(I)V

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/play/headerlist/m;)V
    .locals 0

    .prologue
    .line 1565
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c:Lcom/google/android/play/headerlist/m;

    .line 1566
    return-void
.end method

.method public final a(Lcom/google/android/play/headerlist/o;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1417
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    if-nez v0, :cond_0

    .line 1418
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot initialize pull to refresh before HeaderListLayout has been configured"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1421
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Z)V

    .line 1422
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->G:Lcom/google/android/play/headerlist/g;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/g;->a(F)V

    .line 1423
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->G:Lcom/google/android/play/headerlist/g;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/g;->d(F)V

    .line 1424
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    .line 1427
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->m()V

    .line 1429
    sget v0, Lcom/google/android/play/f;->ar:I

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1430
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1432
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    if-eqz v0, :cond_3

    .line 1434
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    .line 1436
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 1450
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aj:I

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(Z)V

    .line 1455
    :goto_1
    return-void

    .line 1430
    :cond_2
    const/16 v0, 0x8

    goto :goto_0

    .line 1452
    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(Z)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 1680
    invoke-direct {p0, v0, v0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(ZZZ)V

    .line 1681
    return-void
.end method

.method public final b()Landroid/support/v4/widget/SwipeRefreshLayout;
    .locals 1

    .prologue
    .line 1472
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    return-object v0
.end method

.method protected final b(F)V
    .locals 1

    .prologue
    .line 2786
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(FZ)V

    .line 2787
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1605
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aE:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->as:Ljava/lang/Runnable;

    iput-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ae:Ljava/lang/CharSequence;

    sget-boolean v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(FZ)V

    invoke-direct {p0, v2, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ah:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ah:Z

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->y:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0, v2, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(II)V

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1759
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->P:I

    return v0
.end method

.method public final e()F
    .locals 3

    .prologue
    .line 1770
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ao:Z

    if-eqz v0, :cond_0

    .line 1771
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->p()F

    move-result v0

    .line 1772
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->q()F

    move-result v1

    .line 1773
    iget v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ai:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 1777
    :goto_0
    return v0

    .line 1775
    :cond_0
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->n()F

    move-result v0

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->S:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1793
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->L:Z

    if-eqz v0, :cond_0

    .line 1794
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/d;

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->B:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/d;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 1796
    :cond_0
    return-void
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 2137
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 2164
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->N:I

    invoke-static {v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 2241
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->w:Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;->b()V

    .line 2242
    return-void
.end method

.method final j()V
    .locals 1

    .prologue
    .line 2313
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->at:I

    .line 2314
    return-void
.end method

.method public final k()Landroid/view/View;
    .locals 2

    .prologue
    .line 3018
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->A:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 3019
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->A:Landroid/view/View;

    .line 3024
    :goto_0
    return-object v0

    .line 3021
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->L:Z

    if-eqz v0, :cond_1

    .line 3022
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->B:Landroid/support/v7/widget/Toolbar;

    goto :goto_0

    .line 3024
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    sget v1, Lcom/google/android/play/f;->d:I

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1834
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 1835
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->au:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->au:Z

    sget-object v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->B:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->B:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->u()V

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->A()V

    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->U:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(F)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->m()V

    .line 1836
    :cond_0
    return-void

    .line 1835
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(F)V

    goto :goto_1
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 1868
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->au:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->au:Z

    sget-object v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->B:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->B:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    invoke-direct {p0, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(Landroid/view/ViewGroup;)Z

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->g:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-boolean v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->L:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->C:Lcom/google/android/play/headerlist/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/headerlist/g;->d(F)V

    :cond_0
    iput-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->as:Ljava/lang/Runnable;

    .line 1869
    :cond_1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 1870
    return-void

    .line 1868
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->i:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->B:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1359
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->av:Z

    if-eqz v0, :cond_2

    .line 1360
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 1361
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1362
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 1364
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->G:Lcom/google/android/play/headerlist/g;

    iget-object v2, v1, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    if-eqz v2, :cond_1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_1

    iget-object v1, v1, Lcom/google/android/play/headerlist/g;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getAlpha()F

    move-result v1

    :goto_0
    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    .line 1365
    iget-object v1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->G:Lcom/google/android/play/headerlist/g;

    invoke-virtual {v1, v4}, Lcom/google/android/play/headerlist/g;->a(F)V

    .line 1369
    :cond_0
    :goto_1
    return v0

    .line 1364
    :cond_1
    iget v1, v1, Lcom/google/android/play/headerlist/g;->b:F

    goto :goto_0

    .line 1369
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1312
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 1313
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->u()V

    .line 1316
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->m()V

    .line 1317
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aA:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    if-eqz v0, :cond_4

    move v0, v1

    .line 1318
    :goto_0
    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aA:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->E:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(Z)V

    iget-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aA:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    iget v3, v3, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;->a:F

    invoke-direct {p0, v3, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(FZ)V

    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->v()V

    invoke-direct {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Z)V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aA:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->am:Z

    .line 1319
    :cond_0
    if-nez v0, :cond_2

    .line 1320
    if-eqz p1, :cond_1

    .line 1324
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->at:I

    .line 1327
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ak:Z

    if-eqz v0, :cond_2

    .line 1331
    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(Z)V

    .line 1332
    iput-boolean v2, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->ak:Z

    .line 1335
    :cond_2
    iget v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->at:I

    packed-switch v0, :pswitch_data_0

    .line 1344
    :goto_1
    if-eqz p1, :cond_3

    .line 1352
    invoke-direct {p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->A()V

    .line 1354
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 1317
    goto :goto_0

    .line 1337
    :pswitch_0
    invoke-direct {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Z)V

    goto :goto_1

    .line 1343
    :pswitch_1
    invoke-direct {p0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Z)V

    goto :goto_1

    .line 1335
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1282
    instance-of v0, p1, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    if-eqz v0, :cond_0

    .line 1283
    check-cast p1, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    .line 1284
    invoke-virtual {p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1285
    iput-object p1, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->aA:Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    .line 1289
    :goto_0
    return-void

    .line 1287
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1277
    new-instance v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$SavedState;-><init>(Landroid/os/Parcelable;Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 1374
    iget-boolean v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->av:Z

    if-eqz v0, :cond_0

    .line 1375
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 1376
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->F:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 1377
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 1380
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final v_()V
    .locals 1

    .prologue
    .line 1404
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    if-eqz v0, :cond_0

    .line 1405
    iget-object v0, p0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/o;->a()V

    .line 1407
    :cond_0
    return-void
.end method
