.class public Lcom/google/android/play/widget/PageIndicator;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 29
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/play/widget/PageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/widget/PageIndicator;->a:I

    .line 37
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/play/widget/PageIndicator;->setGravity(I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/widget/PageIndicator;->a:I

    .line 42
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/play/widget/PageIndicator;->setGravity(I)V

    .line 43
    return-void
.end method

.method private a(Landroid/widget/ImageView;Z)Landroid/widget/ImageView;
    .locals 3

    .prologue
    .line 103
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 104
    if-eqz p2, :cond_0

    sget v1, Lcom/google/android/play/c;->m:I

    .line 107
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 108
    return-object p1

    .line 104
    :cond_0
    sget v1, Lcom/google/android/play/c;->n:I

    goto :goto_0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/i;->e:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/play/widget/PageIndicator;->a:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getChildCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/widget/PageIndicator;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, -0x2

    const/4 v2, 0x0

    .line 60
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "numPages must be >=0"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getChildCount()I

    move-result v0

    .line 62
    if-ge p1, v0, :cond_3

    .line 63
    sub-int/2addr v0, p1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/play/widget/PageIndicator;->removeViews(II)V

    .line 69
    :cond_2
    invoke-direct {p0}, Lcom/google/android/play/widget/PageIndicator;->a()V

    .line 70
    return-void

    .line 64
    :cond_3
    if-le p1, v0, :cond_2

    move v3, v0

    .line 65
    :goto_1
    if-ge v3, p1, :cond_2

    .line 66
    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    sget v0, Lcom/google/android/play/e;->e:I

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/play/d;->A:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    const/16 v6, 0x10

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {v0, v5, v2, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p0, v4, v0}, Lcom/google/android/play/widget/PageIndicator;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget v0, p0, Lcom/google/android/play/widget/PageIndicator;->a:I

    if-ne v3, v0, :cond_4

    move v0, v1

    :goto_2
    invoke-direct {p0, v4, v0}, Lcom/google/android/play/widget/PageIndicator;->a(Landroid/widget/ImageView;Z)Landroid/widget/ImageView;

    .line 65
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_4
    move v0, v2

    .line 66
    goto :goto_2
.end method

.method public final b(I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 77
    iget v0, p0, Lcom/google/android/play/widget/PageIndicator;->a:I

    if-eq v0, p1, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/google/android/play/widget/PageIndicator;->getChildCount()I

    move-result v4

    move v3, v2

    .line 79
    :goto_0
    if-ge v3, v4, :cond_1

    .line 80
    invoke-virtual {p0, v3}, Lcom/google/android/play/widget/PageIndicator;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-ne v3, p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/play/widget/PageIndicator;->a(Landroid/widget/ImageView;Z)Landroid/widget/ImageView;

    .line 79
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_0
    move v1, v2

    .line 80
    goto :goto_1

    .line 82
    :cond_1
    iput p1, p0, Lcom/google/android/play/widget/PageIndicator;->a:I

    .line 83
    invoke-direct {p0}, Lcom/google/android/play/widget/PageIndicator;->a()V

    .line 85
    :cond_2
    return-void
.end method
