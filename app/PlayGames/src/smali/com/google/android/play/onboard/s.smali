.class public final Lcom/google/android/play/onboard/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/android/libraries/bind/data/Data;

.field protected final b:Landroid/content/Context;

.field protected final c:I

.field protected final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    .line 169
    iput-object p1, p0, Lcom/google/android/play/onboard/s;->b:Landroid/content/Context;

    .line 170
    iput p2, p0, Lcom/google/android/play/onboard/s;->c:I

    .line 171
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/play/onboard/s;->d:I

    .line 172
    sget v0, Lcom/google/android/play/h;->g:I

    iget-object v1, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v2, Lcom/google/android/libraries/bind/data/c;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    .line 173
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/bind/data/Data;
    .locals 6

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/o;->a:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/o;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tutorial"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/play/onboard/s;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    .line 217
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/o;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/o;->b:I

    new-instance v2, Lcom/google/android/play/onboard/t;

    iget-object v3, p0, Lcom/google/android/play/onboard/s;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget v4, p0, Lcom/google/android/play/onboard/s;->c:I

    iget v5, p0, Lcom/google/android/play/onboard/s;->d:I

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/play/onboard/t;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/play/onboard/s;
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardTutorialPage;->d:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    .line 182
    return-object p0
.end method

.method public final a(Lcom/google/android/play/onboard/p;)Lcom/google/android/play/onboard/s;
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/o;->b:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    .line 210
    return-object p0
.end method

.method public final b(I)Lcom/google/android/play/onboard/s;
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v2, Lcom/google/android/play/onboard/OnboardTutorialPage;->e:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final c(I)Lcom/google/android/play/onboard/s;
    .locals 3

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v2, Lcom/google/android/play/onboard/OnboardTutorialPage;->f:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final d(I)Lcom/google/android/play/onboard/s;
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/play/onboard/s;->a:Lcom/google/android/libraries/bind/data/Data;

    sget v1, Lcom/google/android/play/onboard/OnboardTutorialPage;->g:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    .line 205
    return-object p0
.end method
