.class final Lcom/google/android/play/b/f;
.super Lcom/google/android/play/b/e;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/b/e;-><init>(B)V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/play/b/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 76
    invoke-static {p2, p3, p4}, Lcom/google/android/play/b/f;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 78
    new-instance v1, Lcom/google/android/play/b/g;

    sget v2, Lcom/google/android/play/k;->an:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/play/b/f;->a(Landroid/content/res/TypedArray;)F

    move-result v3

    invoke-static {v0}, Lcom/google/android/play/b/f;->c(Landroid/content/res/TypedArray;)I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/play/b/g;-><init>(Landroid/content/res/ColorStateList;FF)V

    .line 83
    invoke-virtual {p1, v5}, Landroid/view/View;->setClipToOutline(Z)V

    .line 84
    invoke-static {v0}, Lcom/google/android/play/b/f;->b(Landroid/content/res/TypedArray;)F

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/View;->setElevation(F)V

    .line 85
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 86
    sget v1, Lcom/google/android/play/k;->ao:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setClipToOutline(Z)V

    .line 88
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 89
    return-void
.end method
