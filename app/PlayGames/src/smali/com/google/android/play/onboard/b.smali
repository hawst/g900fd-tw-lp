.class public abstract Lcom/google/android/play/onboard/b;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/os/Handler;

.field private final b:I

.field private c:Landroid/view/View;


# direct methods
.method protected constructor <init>(I)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 17
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/onboard/b;->a:Landroid/os/Handler;

    .line 22
    iput p1, p0, Lcom/google/android/play/onboard/b;->b:I

    .line 23
    return-void
.end method

.method private b(Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/play/onboard/c;

    invoke-direct {v0, p0, p1}, Lcom/google/android/play/onboard/c;-><init>(Lcom/google/android/play/onboard/b;Ljava/lang/Runnable;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/play/onboard/b;->b:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/b;->c:Landroid/view/View;

    .line 29
    iget-object v0, p0, Lcom/google/android/play/onboard/b;->c:Landroid/view/View;

    return-object v0
.end method

.method protected final a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/play/onboard/b;->a:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/google/android/play/onboard/b;->b(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 73
    return-void
.end method

.method protected final a(Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/play/onboard/b;->a:Landroid/os/Handler;

    invoke-direct {p0, p1}, Lcom/google/android/play/onboard/b;->b(Ljava/lang/Runnable;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 65
    return-void
.end method
