.class public Lcom/google/android/play/search/PlaySearchActionButton;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/search/e;


# instance fields
.field private a:Lcom/google/android/play/search/d;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private final d:Z

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/search/PlaySearchActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/search/PlaySearchActionButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->e:I

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/e;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->b:Landroid/graphics/drawable/Drawable;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/e;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->c:Landroid/graphics/drawable/Drawable;

    .line 54
    invoke-static {p1}, Lcom/google/android/play/search/n;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->d:Z

    .line 55
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->b(I)V

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/google/android/play/search/PlaySearchActionButton;)Lcom/google/android/play/search/d;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->a:Lcom/google/android/play/search/d;

    return-object v0
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 98
    iget-object v2, p0, Lcom/google/android/play/search/PlaySearchActionButton;->a:Lcom/google/android/play/search/d;

    if-nez v2, :cond_0

    .line 111
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v2, p0, Lcom/google/android/play/search/PlaySearchActionButton;->a:Lcom/google/android/play/search/d;

    iget v2, v2, Lcom/google/android/play/search/d;->a:I

    .line 103
    iget-object v3, p0, Lcom/google/android/play/search/PlaySearchActionButton;->a:Lcom/google/android/play/search/d;

    iget-object v3, v3, Lcom/google/android/play/search/d;->b:Ljava/lang/String;

    .line 104
    if-eq v2, v4, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 105
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/play/search/PlaySearchActionButton;->d:Z

    if-eqz v2, :cond_2

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->b(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 106
    :cond_3
    if-eq v2, v0, :cond_4

    const/4 v0, 0x3

    if-ne v2, v0, :cond_5

    .line 107
    :cond_4
    invoke-direct {p0, v4}, Lcom/google/android/play/search/PlaySearchActionButton;->b(I)V

    goto :goto_0

    .line 109
    :cond_5
    invoke-direct {p0, v1}, Lcom/google/android/play/search/PlaySearchActionButton;->b(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/play/search/PlaySearchActionButton;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->e:I

    return v0
.end method

.method private b(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 114
    iput p1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->e:I

    .line 115
    if-nez p1, :cond_1

    .line 116
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->setVisibility(I)V

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    const/4 v0, 0x0

    .line 122
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    .line 123
    iget-object v2, p0, Lcom/google/android/play/search/PlaySearchActionButton;->b:Landroid/graphics/drawable/Drawable;

    .line 124
    sget v0, Lcom/google/android/play/i;->b:I

    .line 130
    :goto_1
    if-eqz v2, :cond_0

    .line 131
    invoke-virtual {p0, v2}, Lcom/google/android/play/search/PlaySearchActionButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/play/search/PlaySearchActionButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    invoke-virtual {p0, v1}, Lcom/google/android/play/search/PlaySearchActionButton;->setVisibility(I)V

    goto :goto_0

    .line 125
    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    .line 126
    iget-object v2, p0, Lcom/google/android/play/search/PlaySearchActionButton;->c:Landroid/graphics/drawable/Drawable;

    .line 127
    sget v0, Lcom/google/android/play/i;->d:I

    goto :goto_1

    :cond_3
    move-object v2, v0

    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchActionButton;->a()V

    .line 90
    return-void
.end method

.method public final a(Lcom/google/android/play/search/d;)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->a:Lcom/google/android/play/search/d;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->a:Lcom/google/android/play/search/d;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/d;->b(Lcom/google/android/play/search/e;)V

    .line 83
    :cond_0
    iput-object p1, p0, Lcom/google/android/play/search/PlaySearchActionButton;->a:Lcom/google/android/play/search/d;

    .line 84
    iget-object v0, p0, Lcom/google/android/play/search/PlaySearchActionButton;->a:Lcom/google/android/play/search/d;

    invoke-virtual {v0, p0}, Lcom/google/android/play/search/d;->a(Lcom/google/android/play/search/e;)V

    .line 85
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/google/android/play/search/PlaySearchActionButton;->a()V

    .line 95
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Landroid/widget/ImageView;->onFinishInflate()V

    .line 61
    new-instance v0, Lcom/google/android/play/search/c;

    invoke-direct {v0, p0}, Lcom/google/android/play/search/c;-><init>(Lcom/google/android/play/search/PlaySearchActionButton;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/search/PlaySearchActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void
.end method
