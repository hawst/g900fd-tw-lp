.class public final Lcom/google/android/play/layout/e;
.super Landroid/support/v7/widget/ce;
.source "SourceFile"


# static fields
.field static A:Ljava/lang/reflect/Field;


# instance fields
.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:F

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:I

.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, -0x2

    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 373
    invoke-direct {p0, v3, v3}, Landroid/support/v7/widget/ce;-><init>(II)V

    .line 185
    iput v1, p0, Lcom/google/android/play/layout/e;->g:I

    .line 189
    iput v1, p0, Lcom/google/android/play/layout/e;->h:I

    .line 193
    iput v1, p0, Lcom/google/android/play/layout/e;->i:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/e;->j:F

    .line 201
    iput v1, p0, Lcom/google/android/play/layout/e;->k:I

    .line 205
    iput v2, p0, Lcom/google/android/play/layout/e;->l:I

    .line 209
    iput v2, p0, Lcom/google/android/play/layout/e;->m:I

    .line 213
    iput v2, p0, Lcom/google/android/play/layout/e;->n:I

    .line 217
    iput v2, p0, Lcom/google/android/play/layout/e;->o:I

    .line 221
    iput v2, p0, Lcom/google/android/play/layout/e;->p:I

    .line 225
    iput v2, p0, Lcom/google/android/play/layout/e;->q:I

    .line 229
    iput v1, p0, Lcom/google/android/play/layout/e;->r:I

    .line 233
    iput v1, p0, Lcom/google/android/play/layout/e;->s:I

    .line 237
    iput v1, p0, Lcom/google/android/play/layout/e;->t:I

    .line 241
    iput v1, p0, Lcom/google/android/play/layout/e;->u:I

    .line 245
    iput v1, p0, Lcom/google/android/play/layout/e;->v:I

    .line 249
    iput v1, p0, Lcom/google/android/play/layout/e;->w:I

    .line 253
    iput v4, p0, Lcom/google/android/play/layout/e;->x:I

    .line 257
    iput v4, p0, Lcom/google/android/play/layout/e;->y:I

    .line 261
    iput v1, p0, Lcom/google/android/play/layout/e;->z:I

    .line 374
    iput v3, p0, Lcom/google/android/play/layout/e;->e:I

    .line 375
    iput v3, p0, Lcom/google/android/play/layout/e;->f:I

    .line 376
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, -0x1

    const/4 v5, 0x0

    const v0, 0x7fffffff

    const/4 v4, 0x0

    .line 264
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/ce;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 185
    iput v4, p0, Lcom/google/android/play/layout/e;->g:I

    .line 189
    iput v4, p0, Lcom/google/android/play/layout/e;->h:I

    .line 193
    iput v4, p0, Lcom/google/android/play/layout/e;->i:I

    .line 197
    iput v5, p0, Lcom/google/android/play/layout/e;->j:F

    .line 201
    iput v4, p0, Lcom/google/android/play/layout/e;->k:I

    .line 205
    iput v0, p0, Lcom/google/android/play/layout/e;->l:I

    .line 209
    iput v0, p0, Lcom/google/android/play/layout/e;->m:I

    .line 213
    iput v0, p0, Lcom/google/android/play/layout/e;->n:I

    .line 217
    iput v0, p0, Lcom/google/android/play/layout/e;->o:I

    .line 221
    iput v0, p0, Lcom/google/android/play/layout/e;->p:I

    .line 225
    iput v0, p0, Lcom/google/android/play/layout/e;->q:I

    .line 229
    iput v4, p0, Lcom/google/android/play/layout/e;->r:I

    .line 233
    iput v4, p0, Lcom/google/android/play/layout/e;->s:I

    .line 237
    iput v4, p0, Lcom/google/android/play/layout/e;->t:I

    .line 241
    iput v4, p0, Lcom/google/android/play/layout/e;->u:I

    .line 245
    iput v4, p0, Lcom/google/android/play/layout/e;->v:I

    .line 249
    iput v4, p0, Lcom/google/android/play/layout/e;->w:I

    .line 253
    iput v1, p0, Lcom/google/android/play/layout/e;->x:I

    .line 257
    iput v1, p0, Lcom/google/android/play/layout/e;->y:I

    .line 261
    iput v4, p0, Lcom/google/android/play/layout/e;->z:I

    .line 268
    sget-object v0, Lcom/google/android/play/k;->w:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 270
    sget v1, Lcom/google/android/play/k;->x:I

    sget v2, Lcom/google/android/play/j;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 273
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 276
    sget-object v0, Lcom/google/android/play/k;->v:[I

    invoke-virtual {p1, p2, v0, v4, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 279
    const-string v1, "layout_flmWidth"

    sget v2, Lcom/google/android/play/k;->U:I

    iget v3, p0, Lcom/google/android/play/layout/e;->width:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/play/layout/e;->a(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->e:I

    .line 281
    const-string v1, "layout_flmHeight"

    sget v2, Lcom/google/android/play/k;->J:I

    iget v3, p0, Lcom/google/android/play/layout/e;->height:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/play/layout/e;->a(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->f:I

    .line 283
    sget v1, Lcom/google/android/play/k;->H:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->g:I

    .line 285
    sget v1, Lcom/google/android/play/k;->G:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->h:I

    .line 287
    sget v1, Lcom/google/android/play/k;->S:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->i:I

    .line 289
    sget v1, Lcom/google/android/play/k;->F:I

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->j:F

    .line 291
    sget v1, Lcom/google/android/play/k;->I:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->k:I

    .line 294
    const-string v1, "layout_flmMargin"

    sget v2, Lcom/google/android/play/k;->L:I

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v1

    .line 296
    const-string v2, "layout_flmMarginTop"

    sget v3, Lcom/google/android/play/k;->Q:I

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/play/layout/e;->b(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/e;->l:I

    .line 298
    const-string v2, "layout_flmMarginStart"

    sget v3, Lcom/google/android/play/k;->P:I

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/play/layout/e;->b(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/e;->m:I

    .line 300
    const-string v2, "layout_flmMarginEnd"

    sget v3, Lcom/google/android/play/k;->O:I

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/play/layout/e;->b(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v2

    iput v2, p0, Lcom/google/android/play/layout/e;->n:I

    .line 302
    const-string v2, "layout_flmMarginBottom"

    sget v3, Lcom/google/android/play/k;->M:I

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/play/layout/e;->b(Landroid/content/res/TypedArray;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->o:I

    .line 304
    const-string v1, "layout_flmMarginTopForFirstLine"

    sget v2, Lcom/google/android/play/k;->R:I

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->p:I

    .line 307
    const-string v1, "layout_flmMarginBottomForLastLine"

    sget v2, Lcom/google/android/play/k;->N:I

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->q:I

    .line 311
    sget v1, Lcom/google/android/play/k;->T:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->r:I

    .line 314
    sget v1, Lcom/google/android/play/k;->y:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->s:I

    .line 316
    const-string v1, "layout_flmFlowInsetTop"

    sget v2, Lcom/google/android/play/k;->D:I

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->t:I

    .line 318
    const-string v1, "layout_flmFlowInsetStart"

    sget v2, Lcom/google/android/play/k;->C:I

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->u:I

    .line 320
    const-string v1, "layout_flmFlowInsetEnd"

    sget v2, Lcom/google/android/play/k;->B:I

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->v:I

    .line 322
    const-string v1, "layout_flmFlowInsetBottom"

    sget v2, Lcom/google/android/play/k;->A:I

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->w:I

    .line 324
    const-string v1, "layout_flmFlowWidth"

    sget v2, Lcom/google/android/play/k;->E:I

    invoke-static {v0, v1, v2, v6}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->x:I

    .line 326
    const-string v1, "layout_flmFlowHeight"

    sget v2, Lcom/google/android/play/k;->z:I

    invoke-static {v0, v1, v2, v6}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->y:I

    .line 329
    sget v1, Lcom/google/android/play/k;->K:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/play/layout/e;->z:I

    .line 331
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 332
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 367
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ce;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 185
    iput v1, p0, Lcom/google/android/play/layout/e;->g:I

    .line 189
    iput v1, p0, Lcom/google/android/play/layout/e;->h:I

    .line 193
    iput v1, p0, Lcom/google/android/play/layout/e;->i:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/e;->j:F

    .line 201
    iput v1, p0, Lcom/google/android/play/layout/e;->k:I

    .line 205
    iput v2, p0, Lcom/google/android/play/layout/e;->l:I

    .line 209
    iput v2, p0, Lcom/google/android/play/layout/e;->m:I

    .line 213
    iput v2, p0, Lcom/google/android/play/layout/e;->n:I

    .line 217
    iput v2, p0, Lcom/google/android/play/layout/e;->o:I

    .line 221
    iput v2, p0, Lcom/google/android/play/layout/e;->p:I

    .line 225
    iput v2, p0, Lcom/google/android/play/layout/e;->q:I

    .line 229
    iput v1, p0, Lcom/google/android/play/layout/e;->r:I

    .line 233
    iput v1, p0, Lcom/google/android/play/layout/e;->s:I

    .line 237
    iput v1, p0, Lcom/google/android/play/layout/e;->t:I

    .line 241
    iput v1, p0, Lcom/google/android/play/layout/e;->u:I

    .line 245
    iput v1, p0, Lcom/google/android/play/layout/e;->v:I

    .line 249
    iput v1, p0, Lcom/google/android/play/layout/e;->w:I

    .line 253
    iput v3, p0, Lcom/google/android/play/layout/e;->x:I

    .line 257
    iput v3, p0, Lcom/google/android/play/layout/e;->y:I

    .line 261
    iput v1, p0, Lcom/google/android/play/layout/e;->z:I

    .line 368
    iget v0, p0, Lcom/google/android/play/layout/e;->width:I

    iput v0, p0, Lcom/google/android/play/layout/e;->e:I

    .line 369
    iget v0, p0, Lcom/google/android/play/layout/e;->height:I

    iput v0, p0, Lcom/google/android/play/layout/e;->f:I

    .line 370
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 361
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ce;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 185
    iput v1, p0, Lcom/google/android/play/layout/e;->g:I

    .line 189
    iput v1, p0, Lcom/google/android/play/layout/e;->h:I

    .line 193
    iput v1, p0, Lcom/google/android/play/layout/e;->i:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/e;->j:F

    .line 201
    iput v1, p0, Lcom/google/android/play/layout/e;->k:I

    .line 205
    iput v2, p0, Lcom/google/android/play/layout/e;->l:I

    .line 209
    iput v2, p0, Lcom/google/android/play/layout/e;->m:I

    .line 213
    iput v2, p0, Lcom/google/android/play/layout/e;->n:I

    .line 217
    iput v2, p0, Lcom/google/android/play/layout/e;->o:I

    .line 221
    iput v2, p0, Lcom/google/android/play/layout/e;->p:I

    .line 225
    iput v2, p0, Lcom/google/android/play/layout/e;->q:I

    .line 229
    iput v1, p0, Lcom/google/android/play/layout/e;->r:I

    .line 233
    iput v1, p0, Lcom/google/android/play/layout/e;->s:I

    .line 237
    iput v1, p0, Lcom/google/android/play/layout/e;->t:I

    .line 241
    iput v1, p0, Lcom/google/android/play/layout/e;->u:I

    .line 245
    iput v1, p0, Lcom/google/android/play/layout/e;->v:I

    .line 249
    iput v1, p0, Lcom/google/android/play/layout/e;->w:I

    .line 253
    iput v3, p0, Lcom/google/android/play/layout/e;->x:I

    .line 257
    iput v3, p0, Lcom/google/android/play/layout/e;->y:I

    .line 261
    iput v1, p0, Lcom/google/android/play/layout/e;->z:I

    .line 362
    iget v0, p0, Lcom/google/android/play/layout/e;->width:I

    iput v0, p0, Lcom/google/android/play/layout/e;->e:I

    .line 363
    iget v0, p0, Lcom/google/android/play/layout/e;->height:I

    iput v0, p0, Lcom/google/android/play/layout/e;->f:I

    .line 364
    return-void
.end method

.method public constructor <init>(Lcom/google/android/play/layout/e;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const v2, 0x7fffffff

    const/4 v1, 0x0

    .line 335
    invoke-direct {p0, p1}, Landroid/support/v7/widget/ce;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 185
    iput v1, p0, Lcom/google/android/play/layout/e;->g:I

    .line 189
    iput v1, p0, Lcom/google/android/play/layout/e;->h:I

    .line 193
    iput v1, p0, Lcom/google/android/play/layout/e;->i:I

    .line 197
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/play/layout/e;->j:F

    .line 201
    iput v1, p0, Lcom/google/android/play/layout/e;->k:I

    .line 205
    iput v2, p0, Lcom/google/android/play/layout/e;->l:I

    .line 209
    iput v2, p0, Lcom/google/android/play/layout/e;->m:I

    .line 213
    iput v2, p0, Lcom/google/android/play/layout/e;->n:I

    .line 217
    iput v2, p0, Lcom/google/android/play/layout/e;->o:I

    .line 221
    iput v2, p0, Lcom/google/android/play/layout/e;->p:I

    .line 225
    iput v2, p0, Lcom/google/android/play/layout/e;->q:I

    .line 229
    iput v1, p0, Lcom/google/android/play/layout/e;->r:I

    .line 233
    iput v1, p0, Lcom/google/android/play/layout/e;->s:I

    .line 237
    iput v1, p0, Lcom/google/android/play/layout/e;->t:I

    .line 241
    iput v1, p0, Lcom/google/android/play/layout/e;->u:I

    .line 245
    iput v1, p0, Lcom/google/android/play/layout/e;->v:I

    .line 249
    iput v1, p0, Lcom/google/android/play/layout/e;->w:I

    .line 253
    iput v3, p0, Lcom/google/android/play/layout/e;->x:I

    .line 257
    iput v3, p0, Lcom/google/android/play/layout/e;->y:I

    .line 261
    iput v1, p0, Lcom/google/android/play/layout/e;->z:I

    .line 336
    iget v0, p1, Lcom/google/android/play/layout/e;->e:I

    iput v0, p0, Lcom/google/android/play/layout/e;->e:I

    .line 337
    iget v0, p1, Lcom/google/android/play/layout/e;->f:I

    iput v0, p0, Lcom/google/android/play/layout/e;->f:I

    .line 338
    iget v0, p1, Lcom/google/android/play/layout/e;->i:I

    iput v0, p0, Lcom/google/android/play/layout/e;->i:I

    .line 339
    iget v0, p1, Lcom/google/android/play/layout/e;->g:I

    iput v0, p0, Lcom/google/android/play/layout/e;->g:I

    .line 340
    iget v0, p1, Lcom/google/android/play/layout/e;->h:I

    iput v0, p0, Lcom/google/android/play/layout/e;->h:I

    .line 341
    iget v0, p1, Lcom/google/android/play/layout/e;->j:F

    iput v0, p0, Lcom/google/android/play/layout/e;->j:F

    .line 342
    iget v0, p1, Lcom/google/android/play/layout/e;->k:I

    iput v0, p0, Lcom/google/android/play/layout/e;->k:I

    .line 343
    iget v0, p1, Lcom/google/android/play/layout/e;->l:I

    iput v0, p0, Lcom/google/android/play/layout/e;->l:I

    .line 344
    iget v0, p1, Lcom/google/android/play/layout/e;->m:I

    iput v0, p0, Lcom/google/android/play/layout/e;->m:I

    .line 345
    iget v0, p1, Lcom/google/android/play/layout/e;->n:I

    iput v0, p0, Lcom/google/android/play/layout/e;->n:I

    .line 346
    iget v0, p1, Lcom/google/android/play/layout/e;->o:I

    iput v0, p0, Lcom/google/android/play/layout/e;->o:I

    .line 347
    iget v0, p1, Lcom/google/android/play/layout/e;->p:I

    iput v0, p0, Lcom/google/android/play/layout/e;->p:I

    .line 348
    iget v0, p1, Lcom/google/android/play/layout/e;->q:I

    iput v0, p0, Lcom/google/android/play/layout/e;->q:I

    .line 349
    iget v0, p1, Lcom/google/android/play/layout/e;->r:I

    iput v0, p0, Lcom/google/android/play/layout/e;->r:I

    .line 350
    iget v0, p1, Lcom/google/android/play/layout/e;->s:I

    iput v0, p0, Lcom/google/android/play/layout/e;->s:I

    .line 351
    iget v0, p1, Lcom/google/android/play/layout/e;->t:I

    iput v0, p0, Lcom/google/android/play/layout/e;->t:I

    .line 352
    iget v0, p1, Lcom/google/android/play/layout/e;->u:I

    iput v0, p0, Lcom/google/android/play/layout/e;->u:I

    .line 353
    iget v0, p1, Lcom/google/android/play/layout/e;->v:I

    iput v0, p0, Lcom/google/android/play/layout/e;->v:I

    .line 354
    iget v0, p1, Lcom/google/android/play/layout/e;->w:I

    iput v0, p0, Lcom/google/android/play/layout/e;->w:I

    .line 355
    iget v0, p1, Lcom/google/android/play/layout/e;->x:I

    iput v0, p0, Lcom/google/android/play/layout/e;->x:I

    .line 356
    iget v0, p1, Lcom/google/android/play/layout/e;->y:I

    iput v0, p0, Lcom/google/android/play/layout/e;->y:I

    .line 357
    iget v0, p1, Lcom/google/android/play/layout/e;->z:I

    iput v0, p0, Lcom/google/android/play/layout/e;->z:I

    .line 358
    return-void
.end method

.method private static a(Landroid/content/res/TypedArray;Ljava/lang/String;II)I
    .locals 3

    .prologue
    .line 400
    invoke-virtual {p0, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7fffffff

    if-ne p3, v0, :cond_2

    .line 401
    :cond_0
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result p3

    .line 407
    :cond_1
    return p3

    .line 405
    :cond_2
    const/4 v0, -0x2

    if-gt v0, p3, :cond_3

    const v0, 0xffffff

    if-le p3, v0, :cond_1

    .line 409
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/res/TypedArray;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": out-of-range dimension length for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/lang/String;IFZ)I
    .locals 3

    .prologue
    .line 608
    invoke-static {p1}, Lcom/google/android/play/utils/a;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 618
    :cond_0
    :goto_0
    return p1

    .line 611
    :cond_1
    if-eqz p3, :cond_2

    if-ltz p1, :cond_0

    .line 614
    :cond_2
    invoke-static {p2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 615
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uses grid-based measurement, which is disabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 618
    :cond_3
    invoke-static {p1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    mul-float/2addr v0, p2

    float-to-int p1, v0

    goto :goto_0
.end method

.method private static b(Landroid/content/res/TypedArray;Ljava/lang/String;II)I
    .locals 1

    .prologue
    .line 423
    invoke-virtual {p0, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/play/utils/a;->a(Landroid/content/res/TypedArray;Ljava/lang/String;IZ)I

    move-result p3

    .line 426
    :cond_0
    return p3
.end method


# virtual methods
.method final a(ILcom/google/android/play/layout/d;)F
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 482
    iget v2, p0, Lcom/google/android/play/layout/e;->g:I

    .line 483
    iget v1, p0, Lcom/google/android/play/layout/e;->h:I

    .line 484
    sub-int v0, p1, v2

    sub-int/2addr v0, v1

    .line 486
    iget v3, p0, Lcom/google/android/play/layout/e;->i:I

    if-lez v3, :cond_0

    iget v3, p0, Lcom/google/android/play/layout/e;->i:I

    if-le v0, v3, :cond_0

    .line 487
    iget v3, p0, Lcom/google/android/play/layout/e;->i:I

    sub-int v3, v0, v3

    .line 488
    iget v0, p0, Lcom/google/android/play/layout/e;->i:I

    .line 491
    shr-int/lit8 v4, v3, 0x1

    add-int/2addr v2, v4

    .line 492
    shr-int/lit8 v4, v3, 0x1

    and-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 496
    :cond_0
    iget v3, p0, Lcom/google/android/play/layout/e;->k:I

    if-gtz v3, :cond_3

    .line 498
    iget v3, p0, Lcom/google/android/play/layout/e;->j:F

    cmpg-float v3, v3, v5

    if-gtz v3, :cond_2

    const/high16 v3, 0x7fc00000    # NaNf

    .line 509
    :goto_0
    if-eqz p2, :cond_1

    .line 510
    const/4 v4, 0x0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v3

    iput v0, p2, Lcom/google/android/play/layout/d;->c:F

    .line 511
    iput v2, p2, Lcom/google/android/play/layout/d;->a:I

    .line 512
    iput v1, p2, Lcom/google/android/play/layout/d;->b:I

    .line 514
    :cond_1
    return v3

    .line 498
    :cond_2
    iget v3, p0, Lcom/google/android/play/layout/e;->j:F

    goto :goto_0

    .line 501
    :cond_3
    int-to-float v3, v0

    iget v4, p0, Lcom/google/android/play/layout/e;->k:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 502
    iget v4, p0, Lcom/google/android/play/layout/e;->j:F

    cmpg-float v4, v5, v4

    if-gez v4, :cond_4

    iget v4, p0, Lcom/google/android/play/layout/e;->j:F

    cmpg-float v4, v4, v3

    if-gtz v4, :cond_4

    .line 503
    iget v3, p0, Lcom/google/android/play/layout/e;->j:F

    goto :goto_0

    .line 505
    :cond_4
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    double-to-float v3, v4

    goto :goto_0
.end method

.method final a()I
    .locals 2

    .prologue
    .line 437
    iget v0, p0, Lcom/google/android/play/layout/e;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/e;->s:I

    if-eqz v0, :cond_1

    .line 438
    :cond_0
    const/4 v0, 0x2

    .line 440
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/google/android/play/layout/e;->z:I

    and-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method final a(F)I
    .locals 3

    .prologue
    .line 531
    iget v0, p0, Lcom/google/android/play/layout/e;->l:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 532
    iget v0, p0, Lcom/google/android/play/layout/e;->topMargin:I

    .line 534
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginTop"

    iget v1, p0, Lcom/google/android/play/layout/e;->l:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method final a(FII)I
    .locals 4

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    .line 632
    const-string v0, "layout_flmWidth"

    iget v2, p0, Lcom/google/android/play/layout/e;->e:I

    const/4 v3, 0x1

    invoke-static {v0, v2, p1, v3}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    .line 634
    packed-switch v0, :pswitch_data_0

    .line 651
    iget v2, p0, Lcom/google/android/play/layout/e;->e:I

    invoke-static {v2}, Lcom/google/android/play/utils/a;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 653
    const/4 v2, 0x0

    sub-int/2addr v0, p3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 659
    :cond_0
    iput v0, p0, Lcom/google/android/play/layout/e;->width:I

    move p2, v0

    move v0, v1

    .line 662
    :goto_0
    invoke-static {p2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0

    .line 638
    :pswitch_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/layout/e;->width:I

    move v0, v1

    .line 639
    goto :goto_0

    .line 643
    :pswitch_1
    iput p2, p0, Lcom/google/android/play/layout/e;->width:I

    move v0, v1

    .line 644
    goto :goto_0

    .line 647
    :pswitch_2
    const/high16 v0, -0x80000000

    .line 648
    const/4 v1, -0x2

    iput v1, p0, Lcom/google/android/play/layout/e;->width:I

    goto :goto_0

    .line 654
    :cond_1
    if-gez v0, :cond_0

    .line 655
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown enum value for layout_flmWidth: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 634
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method final a(FILcom/google/android/play/layout/FlowLayoutManager;)I
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/high16 v0, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 676
    const-string v2, "layout_flmHeight"

    iget v3, p0, Lcom/google/android/play/layout/e;->f:I

    const/4 v4, 0x1

    invoke-static {v2, v3, p1, v4}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v2

    .line 678
    packed-switch v2, :pswitch_data_0

    .line 707
    :pswitch_0
    iget v3, p0, Lcom/google/android/play/layout/e;->f:I

    invoke-static {v3}, Lcom/google/android/play/utils/a;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 709
    sub-int/2addr v2, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 715
    :goto_0
    iput v1, p0, Lcom/google/android/play/layout/e;->height:I

    .line 718
    :goto_1
    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    return v0

    .line 680
    :pswitch_1
    invoke-virtual {p3}, Lcom/google/android/play/layout/FlowLayoutManager;->l()I

    move-result v1

    invoke-virtual {p3}, Lcom/google/android/play/layout/FlowLayoutManager;->n()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p3}, Lcom/google/android/play/layout/FlowLayoutManager;->p()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v1, p2

    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/e;->a(F)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/e;->e(F)I

    move-result v2

    sub-int/2addr v1, v2

    .line 684
    iput v5, p0, Lcom/google/android/play/layout/e;->height:I

    goto :goto_1

    .line 687
    :pswitch_2
    iget v2, p0, Lcom/google/android/play/layout/e;->s:I

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/play/layout/e;->y:I

    if-ltz v2, :cond_0

    .line 689
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/e;->h(F)I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/e;->i(F)I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/e;->l(F)I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 699
    :goto_2
    iput v5, p0, Lcom/google/android/play/layout/e;->height:I

    goto :goto_1

    :cond_0
    move v0, v1

    .line 695
    goto :goto_2

    .line 704
    :pswitch_3
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/play/layout/e;->height:I

    move v0, v1

    .line 705
    goto :goto_1

    .line 710
    :cond_1
    if-gez v2, :cond_2

    .line 711
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown value for layout_flmHeight: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v1, v2

    goto :goto_0

    .line 678
    nop

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method final b()I
    .locals 1

    .prologue
    .line 451
    iget v0, p0, Lcom/google/android/play/layout/e;->z:I

    and-int/lit8 v0, v0, 0xc

    return v0
.end method

.method final b(F)I
    .locals 3

    .prologue
    .line 539
    iget v0, p0, Lcom/google/android/play/layout/e;->p:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 540
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/e;->a(F)I

    move-result v0

    .line 542
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginTopForFirstLine"

    iget v1, p0, Lcom/google/android/play/layout/e;->p:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method final c(F)I
    .locals 3

    .prologue
    .line 548
    iget v0, p0, Lcom/google/android/play/layout/e;->m:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 549
    invoke-static {p0}, Landroid/support/v4/view/z;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    .line 551
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginStart"

    iget v1, p0, Lcom/google/android/play/layout/e;->m:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method final d(F)I
    .locals 3

    .prologue
    .line 556
    iget v0, p0, Lcom/google/android/play/layout/e;->n:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 557
    invoke-static {p0}, Landroid/support/v4/view/z;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v0

    .line 559
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginEnd"

    iget v1, p0, Lcom/google/android/play/layout/e;->n:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method final e(F)I
    .locals 3

    .prologue
    .line 564
    iget v0, p0, Lcom/google/android/play/layout/e;->o:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 565
    iget v0, p0, Lcom/google/android/play/layout/e;->bottomMargin:I

    .line 567
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginBottom"

    iget v1, p0, Lcom/google/android/play/layout/e;->o:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method final f(F)I
    .locals 3

    .prologue
    .line 573
    iget v0, p0, Lcom/google/android/play/layout/e;->q:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 574
    invoke-virtual {p0, p1}, Lcom/google/android/play/layout/e;->e(F)I

    move-result v0

    .line 576
    :goto_0
    return v0

    :cond_0
    const-string v0, "layout_flmMarginBottomForLastLine"

    iget v1, p0, Lcom/google/android/play/layout/e;->q:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    goto :goto_0
.end method

.method final g(F)I
    .locals 3

    .prologue
    .line 582
    const-string v0, "layout_flmFlowWidth"

    iget v1, p0, Lcom/google/android/play/layout/e;->x:I

    const/4 v2, 0x1

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method final g()Z
    .locals 2

    .prologue
    .line 459
    invoke-virtual {p0}, Lcom/google/android/play/layout/e;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/e;->z:I

    and-int/lit8 v0, v0, 0xc

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final h(F)I
    .locals 3

    .prologue
    .line 586
    const-string v0, "layout_flmFlowHeight"

    iget v1, p0, Lcom/google/android/play/layout/e;->y:I

    const/4 v2, 0x1

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method final h()Ljava/lang/String;
    .locals 3

    .prologue
    .line 762
    sget-object v0, Lcom/google/android/play/layout/e;->A:Ljava/lang/reflect/Field;

    if-nez v0, :cond_0

    .line 764
    :try_start_0
    const-class v0, Landroid/support/v7/widget/ce;

    const-string v1, "a"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 765
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 766
    sput-object v0, Lcom/google/android/play/layout/e;->A:Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 773
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/play/layout/e;->A:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 775
    :goto_0
    return-object v0

    .line 767
    :catch_0
    move-exception v0

    .line 769
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 774
    :catch_1
    move-exception v0

    .line 775
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method final i(F)I
    .locals 3

    .prologue
    .line 590
    const-string v0, "layout_flmFlowInsetTop"

    iget v1, p0, Lcom/google/android/play/layout/e;->t:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method final j(F)I
    .locals 3

    .prologue
    .line 594
    const-string v0, "layout_flmFlowInsetStart"

    iget v1, p0, Lcom/google/android/play/layout/e;->u:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method final k(F)I
    .locals 3

    .prologue
    .line 599
    const-string v0, "layout_flmFlowInsetEnd"

    iget v1, p0, Lcom/google/android/play/layout/e;->v:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method final l(F)I
    .locals 3

    .prologue
    .line 603
    const-string v0, "layout_flmFlowInsetBottom"

    iget v1, p0, Lcom/google/android/play/layout/e;->w:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/play/layout/e;->a(Ljava/lang/String;IFZ)I

    move-result v0

    return v0
.end method

.method protected final setBaseAttributes(Landroid/content/res/TypedArray;II)V
    .locals 2

    .prologue
    const v1, 0x7fffffff

    .line 384
    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/e;->width:I

    .line 385
    invoke-virtual {p1, p3, v1}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/play/layout/e;->height:I

    .line 386
    return-void
.end method
