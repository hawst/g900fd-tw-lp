.class public Lcom/google/android/play/onboard/OnboardPager;
.super Lcom/google/android/play/widget/d;
.source "SourceFile"


# instance fields
.field protected d:Z

.field protected e:Z

.field protected f:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/play/widget/d;-><init>(Landroid/content/Context;)V

    .line 20
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->d:Z

    .line 21
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->e:Z

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/widget/d;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->d:Z

    .line 21
    iput-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->e:Z

    .line 34
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 104
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v1, :cond_2

    move v0, v1

    .line 105
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    move v2, v1

    .line 125
    :cond_1
    :goto_2
    :pswitch_1
    return v2

    :cond_2
    move v0, v2

    .line 104
    goto :goto_0

    .line 111
    :pswitch_2
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/play/onboard/OnboardPager;->f:F

    goto :goto_1

    .line 115
    :pswitch_3
    if-nez v0, :cond_1

    .line 118
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    .line 119
    iget v3, p0, Lcom/google/android/play/onboard/OnboardPager;->f:F

    sub-float v3, v0, v3

    .line 120
    iput v0, p0, Lcom/google/android/play/onboard/OnboardPager;->f:F

    .line 121
    neg-float v0, v3

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v0}, Lcom/google/android/play/onboard/OnboardPager;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_2

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private c(I)Z
    .locals 2

    .prologue
    .line 132
    iget v1, p0, Landroid/support/v4/view/ViewPager;->b:I

    .line 133
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Landroid/support/v4/view/ao;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 134
    :goto_0
    if-gez p1, :cond_2

    if-lez v1, :cond_2

    .line 135
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPager;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->d:Z

    .line 140
    :goto_1
    return v0

    .line 133
    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Landroid/support/v4/view/ao;

    invoke-virtual {v0}, Landroid/support/v4/view/ao;->c()I

    move-result v0

    goto :goto_0

    .line 135
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->e:Z

    goto :goto_1

    .line 136
    :cond_2
    if-lez p1, :cond_4

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_4

    .line 137
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardPager;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->e:Z

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/play/onboard/OnboardPager;->d:Z

    goto :goto_1

    .line 140
    :cond_4
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Z)V
    .locals 0

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/play/onboard/OnboardPager;->d:Z

    .line 38
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/play/onboard/OnboardPager;->e:Z

    .line 42
    return-void
.end method

.method protected final e()Z
    .locals 1

    .prologue
    .line 47
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/onboard/OnboardPager;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/play/widget/d;->e()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final f()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/play/onboard/OnboardPager;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/play/widget/d;->f()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/play/onboard/OnboardPager;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/play/widget/d;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 67
    :catch_0
    move-exception v0

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 68
    invoke-super {p0, p1}, Lcom/google/android/play/widget/d;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/play/onboard/OnboardPager;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    :try_start_0
    invoke-super {p0, p1}, Lcom/google/android/play/widget/d;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 87
    :goto_0
    return v0

    .line 82
    :catch_0
    move-exception v0

    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 83
    invoke-super {p0, p1}, Lcom/google/android/play/widget/d;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 87
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
