.class abstract Lcom/google/android/play/headerlist/k;
.super Landroid/view/animation/Animation;
.source "SourceFile"


# instance fields
.field private final a:F

.field final synthetic b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field private final c:F


# direct methods
.method protected constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;FF)V
    .locals 0

    .prologue
    .line 3378
    iput-object p1, p0, Lcom/google/android/play/headerlist/k;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 3379
    iput p2, p0, Lcom/google/android/play/headerlist/k;->a:F

    .line 3380
    iput p3, p0, Lcom/google/android/play/headerlist/k;->c:F

    .line 3381
    return-void
.end method


# virtual methods
.method protected abstract a(F)V
.end method

.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 2

    .prologue
    .line 3385
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    .line 3387
    iget v0, p0, Lcom/google/android/play/headerlist/k;->c:F

    iget v1, p0, Lcom/google/android/play/headerlist/k;->a:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, p1

    iget v1, p0, Lcom/google/android/play/headerlist/k;->a:F

    add-float/2addr v0, v1

    .line 3388
    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/k;->a(F)V

    .line 3389
    return-void
.end method
