.class public abstract Lcom/google/android/play/onboard/e;
.super Lcom/google/android/play/onboard/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/onboard/d;


# static fields
.field private static final ap:Lcom/google/android/libraries/bind/c/b;

.field private static final aq:Ljava/lang/String;

.field protected static final b:Ljava/lang/String;

.field protected static final c:Ljava/lang/String;

.field protected static final d:Ljava/lang/String;

.field protected static final e:Ljava/lang/String;

.field protected static final f:Ljava/lang/String;


# instance fields
.field protected aj:Lcom/google/android/play/onboard/OnboardNavFooter;

.field protected ak:Landroid/view/View;

.field protected al:Landroid/widget/ImageView;

.field protected am:Landroid/os/Bundle;

.field protected an:Ljava/lang/String;

.field protected ao:Z

.field private final ar:Lcom/google/android/libraries/bind/a/d;

.field protected g:Landroid/widget/FrameLayout;

.field protected h:Lcom/google/android/play/onboard/q;

.field protected i:Lcom/google/android/play/onboard/OnboardPager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 105
    const-class v0, Lcom/google/android/play/onboard/e;

    invoke-static {v0}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Class;)Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/e;->ap:Lcom/google/android/libraries/bind/c/b;

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/play/onboard/e;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/e;->aq:Ljava/lang/String;

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/e;->aq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "onboardBundle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/e;->b:Ljava/lang/String;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/e;->aq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "showedSplash"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/e;->c:Ljava/lang/String;

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/e;->aq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "selectedPageId"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/e;->d:Ljava/lang/String;

    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/e;->aq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "showingLoadingOverlay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/e;->e:Ljava/lang/String;

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/play/onboard/e;->aq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "showingInterstitialOverlay"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/onboard/e;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 149
    sget v0, Lcom/google/android/play/h;->d:I

    invoke-direct {p0, v0}, Lcom/google/android/play/onboard/b;-><init>(I)V

    .line 140
    new-instance v0, Lcom/google/android/libraries/bind/a/d;

    iget-object v1, p0, Lcom/google/android/play/onboard/e;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/play/onboard/f;

    invoke-direct {v2, p0}, Lcom/google/android/play/onboard/f;-><init>(Lcom/google/android/play/onboard/e;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/a/d;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/play/onboard/e;->ar:Lcom/google/android/libraries/bind/a/d;

    .line 150
    return-void
.end method

.method protected static U()V
    .locals 0

    .prologue
    .line 415
    return-void
.end method

.method static synthetic Z()Lcom/google/android/libraries/bind/c/b;
    .locals 1

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/play/onboard/e;->ap:Lcom/google/android/libraries/bind/c/b;

    return-object v0
.end method

.method private a(IZ)V
    .locals 2

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    iget-object v1, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    invoke-static {v1, p1}, Lcom/google/android/libraries/bind/b/c;->b(Landroid/support/v4/view/ao;I)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/play/onboard/OnboardPager;->a(IZ)V

    .line 497
    return-void
.end method


# virtual methods
.method public Q()V
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->am:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/e;->c(Landroid/os/Bundle;)V

    .line 517
    return-void
.end method

.method protected final S()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 304
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->am:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/play/onboard/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 305
    if-nez v0, :cond_0

    .line 306
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 308
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 309
    iget-object v1, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    invoke-virtual {v1, v0}, Lcom/google/android/play/onboard/q;->c(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    invoke-direct {p0, v0, v2}, Lcom/google/android/play/onboard/e;->a(IZ)V

    .line 315
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->V()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/e;->a(I)V

    .line 316
    return-void

    .line 306
    :cond_2
    const-string v1, "arg_initialPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 311
    :cond_3
    invoke-direct {p0, v2, v2}, Lcom/google/android/play/onboard/e;->a(IZ)V

    goto :goto_1
.end method

.method public final T()I
    .locals 2

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->j()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/c;->s:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0
.end method

.method public final V()I
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    invoke-virtual {v0}, Lcom/google/android/play/onboard/OnboardPager;->i()I

    move-result v0

    .line 443
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected final W()V
    .locals 2

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->V()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 485
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    invoke-virtual {v1}, Lcom/google/android/play/onboard/q;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 486
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/play/onboard/e;->a(IZ)V

    .line 488
    :cond_0
    return-void
.end method

.method public final X()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 535
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->V()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/play/onboard/q;->a(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    instance-of v2, v0, Lcom/google/android/play/onboard/o;

    if-eqz v2, :cond_2

    :goto_1
    check-cast v0, Lcom/google/android/play/onboard/o;

    .line 536
    if-eqz v0, :cond_0

    .line 537
    invoke-interface {v0}, Lcom/google/android/play/onboard/o;->b()Lcom/google/android/play/onboard/p;

    .line 539
    :cond_0
    return v3

    :cond_1
    move-object v0, v1

    .line 535
    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method protected final Y()V
    .locals 2

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->V()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/q;->c(I)Lcom/google/android/play/onboard/p;

    move-result-object v0

    .line 560
    if-eqz v0, :cond_0

    .line 561
    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/e;->a(Lcom/google/android/play/onboard/p;)V

    .line 564
    :cond_0
    return-void
.end method

.method public abstract a()Lcom/google/android/libraries/bind/data/m;
.end method

.method protected final a(I)V
    .locals 4

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->a()Lcom/google/android/libraries/bind/data/m;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/m;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 380
    const/4 v2, 0x0

    .line 381
    iget-object v1, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    invoke-virtual {v1, p1}, Lcom/google/android/play/onboard/q;->a(I)Landroid/view/View;

    move-result-object v1

    .line 382
    instance-of v3, v1, Lcom/google/android/play/onboard/o;

    if-eqz v3, :cond_0

    .line 383
    check-cast v1, Lcom/google/android/play/onboard/o;

    move-object v2, v1

    .line 386
    :cond_0
    iget-object v1, p0, Lcom/google/android/play/onboard/e;->an:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/d/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 387
    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->a()Lcom/google/android/libraries/bind/data/m;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/play/onboard/e;->an:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/bind/data/m;->a(Ljava/lang/Object;)I

    move-result v1

    .line 389
    iget-object v3, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    invoke-virtual {v3, v1}, Lcom/google/android/play/onboard/q;->a(I)Landroid/view/View;

    move-result-object v1

    .line 392
    instance-of v3, v1, Lcom/google/android/play/onboard/o;

    if-eqz v3, :cond_1

    .line 393
    check-cast v1, Lcom/google/android/play/onboard/o;

    invoke-interface {v1}, Lcom/google/android/play/onboard/o;->f()V

    .line 395
    :cond_1
    iput-object v0, p0, Lcom/google/android/play/onboard/e;->an:Ljava/lang/String;

    .line 396
    if-eqz v2, :cond_2

    .line 397
    invoke-interface {v2}, Lcom/google/android/play/onboard/o;->e()V

    .line 401
    :cond_2
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->ar:Lcom/google/android/libraries/bind/a/d;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/a/d;->a(J)Z

    .line 402
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 587
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 588
    if-eq v0, p1, :cond_1

    .line 589
    if-eqz v0, :cond_0

    .line 590
    iget-object v1, p0, Lcom/google/android/play/onboard/e;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 592
    :cond_0
    if-eqz p1, :cond_1

    .line 593
    invoke-virtual {p1, p2}, Landroid/view/View;->setId(I)V

    .line 594
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->g:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 600
    :cond_1
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 154
    invoke-super {p0, p1, p2}, Lcom/google/android/play/onboard/b;->a(Landroid/view/View;Landroid/os/Bundle;)V

    move-object v0, p1

    .line 155
    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/play/onboard/e;->g:Landroid/widget/FrameLayout;

    .line 156
    sget v0, Lcom/google/android/play/f;->ac:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/onboard/OnboardPager;

    iput-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    .line 157
    sget v0, Lcom/google/android/play/f;->ab:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/onboard/OnboardNavFooter;

    iput-object v0, p0, Lcom/google/android/play/onboard/e;->aj:Lcom/google/android/play/onboard/OnboardNavFooter;

    .line 159
    sget v0, Lcom/google/android/play/f;->al:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/e;->ak:Landroid/view/View;

    .line 160
    sget v0, Lcom/google/android/play/f;->aa:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/play/onboard/e;->al:Landroid/widget/ImageView;

    .line 162
    if-eqz p2, :cond_0

    .line 163
    sget-object v0, Lcom/google/android/play/onboard/e;->b:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/play/onboard/e;->am:Landroid/os/Bundle;

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->am:Landroid/os/Bundle;

    if-nez v0, :cond_1

    .line 166
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/onboard/e;->am:Landroid/os/Bundle;

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->am:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/e;->l(Landroid/os/Bundle;)V

    .line 170
    new-instance v0, Lcom/google/android/play/onboard/h;

    new-instance v1, Lcom/google/android/libraries/bind/e/a;

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v1, v2}, Lcom/google/android/libraries/bind/e/a;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/play/onboard/h;-><init>(Lcom/google/android/play/onboard/e;Lcom/google/android/libraries/bind/e/a;)V

    iput-object v0, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    iget-object v0, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->a()Lcom/google/android/libraries/bind/data/m;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    iget-object v3, v0, Lcom/google/android/libraries/bind/data/p;->b:Lcom/google/android/libraries/bind/data/o;

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/m;->b(Lcom/google/android/libraries/bind/data/o;)V

    :cond_2
    iput-object v1, v0, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    if-eqz v1, :cond_3

    iget-object v2, v0, Lcom/google/android/libraries/bind/data/p;->b:Lcom/google/android/libraries/bind/data/o;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/bind/data/m;->a(Lcom/google/android/libraries/bind/data/o;)V

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/p;->d()V

    iget-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    new-instance v1, Lcom/google/android/play/onboard/i;

    iget-object v2, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    invoke-direct {v1, p0, v2}, Lcom/google/android/play/onboard/i;-><init>(Lcom/google/android/play/onboard/e;Lcom/google/android/play/widget/d;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPager;->a(Landroid/support/v4/view/cc;)V

    iget-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    iget-object v1, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPager;->a(Landroid/support/v4/view/ao;)V

    .line 172
    new-instance v0, Lcom/google/android/play/onboard/g;

    invoke-direct {v0, p0}, Lcom/google/android/play/onboard/g;-><init>(Lcom/google/android/play/onboard/e;)V

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/e;->a(Ljava/lang/Runnable;)V

    .line 178
    return-void
.end method

.method protected final a(Landroid/view/View;Z)V
    .locals 4

    .prologue
    .line 276
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->j()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/play/d;->z:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 280
    :goto_0
    invoke-static {p1}, Landroid/support/v4/view/at;->m(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-static {p1}, Landroid/support/v4/view/at;->n(Landroid/view/View;)I

    move-result v3

    invoke-static {p1, v1, v2, v3, v0}, Landroid/support/v4/view/at;->b(Landroid/view/View;IIII)V

    .line 282
    return-void

    .line 276
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/play/onboard/p;)V
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->aj:Lcom/google/android/play/onboard/OnboardNavFooter;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/play/onboard/OnboardNavFooter;->a(Lcom/google/android/play/onboard/d;Lcom/google/android/play/onboard/p;)V

    .line 573
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    invoke-interface {p1}, Lcom/google/android/play/onboard/p;->l()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPager;->a(Z)V

    .line 574
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    invoke-interface {p1}, Lcom/google/android/play/onboard/p;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPager;->b(Z)V

    .line 575
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->W()V

    .line 470
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 197
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    invoke-virtual {v0}, Lcom/google/android/play/onboard/OnboardPager;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->i:Lcom/google/android/play/onboard/OnboardPager;

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 199
    instance-of v2, v0, Lcom/google/android/play/onboard/o;

    if-eqz v2, :cond_0

    .line 200
    check-cast v0, Lcom/google/android/play/onboard/o;

    invoke-interface {v0}, Lcom/google/android/play/onboard/o;->d()V

    .line 197
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 203
    :cond_1
    sget-object v1, Lcom/google/android/play/onboard/e;->d:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->V()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/play/onboard/e;->h:Lcom/google/android/play/onboard/q;

    iget-object v3, v3, Lcom/google/android/libraries/bind/data/p;->c:Lcom/google/android/libraries/bind/data/m;

    if-eqz v3, :cond_2

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/bind/data/m;->a(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/m;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/bind/data/Data;->c(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    sget-object v0, Lcom/google/android/play/onboard/e;->c:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/play/onboard/e;->ao:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 205
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 188
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/b;->e(Landroid/os/Bundle;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/play/onboard/e;->am:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/google/android/play/onboard/e;->c(Landroid/os/Bundle;)V

    .line 190
    sget-object v0, Lcom/google/android/play/onboard/e;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/play/onboard/e;->am:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 191
    return-void
.end method

.method public l(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 221
    sget-object v0, Lcom/google/android/play/onboard/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/play/onboard/e;->ao:Z

    .line 222
    return-void
.end method

.method public final v()V
    .locals 6

    .prologue
    const/16 v1, 0x8

    .line 182
    invoke-super {p0}, Lcom/google/android/play/onboard/b;->v()V

    .line 183
    iget-boolean v0, p0, Lcom/google/android/play/onboard/e;->ao:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/play/onboard/e;->ak:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/play/onboard/e;->al:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 184
    :goto_0
    return-void

    .line 183
    :cond_0
    new-instance v0, Lcom/google/android/play/widget/b;

    invoke-virtual {p0}, Lcom/google/android/play/onboard/e;->T()I

    move-result v1

    iget-object v2, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-virtual {v2}, Landroid/support/v4/app/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/play/g;->a:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-virtual {v3}, Landroid/support/v4/app/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/play/g;->b:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v4, v3

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/play/widget/b;-><init>(IIJ)V

    new-instance v1, Lcom/google/android/play/onboard/j;

    invoke-direct {v1, p0}, Lcom/google/android/play/onboard/j;-><init>(Lcom/google/android/play/onboard/e;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/widget/b;->a(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/play/onboard/e;->al:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget-object v1, Lcom/google/android/play/onboard/e;->ap:Lcom/google/android/libraries/bind/c/b;

    const-string v2, "Started showing splash"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Lcom/google/android/play/onboard/k;

    invoke-direct {v1, p0, v0}, Lcom/google/android/play/onboard/k;-><init>(Lcom/google/android/play/onboard/e;Lcom/google/android/play/widget/b;)V

    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-virtual {v0}, Landroid/support/v4/app/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/play/g;->c:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/play/onboard/e;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method
