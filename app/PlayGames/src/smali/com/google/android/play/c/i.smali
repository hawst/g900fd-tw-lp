.class public final Lcom/google/android/play/c/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 68
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public static varargs a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 56
    new-instance v1, Landroid/transition/TransitionSet;

    invoke-direct {v1}, Landroid/transition/TransitionSet;-><init>()V

    .line 57
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 58
    invoke-virtual {v1, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 38
    invoke-virtual {p0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setClipChildren(Z)V

    .line 39
    const/4 v0, 0x4

    new-array v3, v0, [I

    sget v0, Lcom/google/android/play/f;->s:I

    aput v0, v3, v2

    const/4 v0, 0x1

    sget v1, Lcom/google/android/play/f;->k:I

    aput v1, v3, v0

    const/4 v0, 0x2

    sget v1, Lcom/google/android/play/f;->i:I

    aput v1, v3, v0

    const/4 v0, 0x3

    sget v1, Lcom/google/android/play/f;->g:I

    aput v1, v3, v0

    .line 45
    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    aget v0, v3, v1

    .line 46
    invoke-virtual {p0, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 47
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 45
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 49
    :cond_0
    return-void
.end method
