.class final Lcom/google/android/play/layout/j;
.super Lcom/google/android/play/layout/g;
.source "SourceFile"


# static fields
.field private static final b:Landroid/support/v4/f/n;


# instance fields
.field public final a:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1624
    new-instance v0, Landroid/support/v4/f/o;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Landroid/support/v4/f/o;-><init>(I)V

    sput-object v0, Lcom/google/android/play/layout/j;->b:Landroid/support/v4/f/n;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1636
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/play/layout/g;-><init>(B)V

    .line 1643
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    .line 1637
    invoke-virtual {p0}, Lcom/google/android/play/layout/j;->d()V

    .line 1638
    return-void
.end method

.method public static a(I)Lcom/google/android/play/layout/j;
    .locals 1

    .prologue
    .line 1628
    sget-object v0, Lcom/google/android/play/layout/j;->b:Landroid/support/v4/f/n;

    invoke-interface {v0}, Landroid/support/v4/f/n;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/j;

    .line 1629
    if-nez v0, :cond_0

    .line 1630
    new-instance v0, Lcom/google/android/play/layout/j;

    invoke-direct {v0}, Lcom/google/android/play/layout/j;-><init>()V

    .line 1632
    :cond_0
    iput p0, v0, Lcom/google/android/play/layout/j;->e:I

    .line 1633
    return-object v0
.end method


# virtual methods
.method protected final a(ZI)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1668
    .line 1669
    iget-object v1, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    .line 1670
    :goto_0
    if-ge v1, v3, :cond_0

    .line 1671
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    invoke-virtual {v0, p2}, Lcom/google/android/play/layout/f;->c(I)I

    move-result v0

    add-int/2addr v2, v0

    .line 1670
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1673
    :cond_0
    return v2
.end method

.method public final a()Lcom/google/android/play/layout/f;
    .locals 2

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/play/layout/f;)V
    .locals 1

    .prologue
    .line 1650
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1651
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/play/layout/g;->g:I

    .line 1652
    return-void
.end method

.method final a(Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 1749
    const/16 v0, 0x7b

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1750
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1751
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/f;->a(Ljava/lang/StringBuilder;)V

    .line 1752
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1753
    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1754
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/f;->a(Ljava/lang/StringBuilder;)V

    .line 1752
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1757
    :cond_0
    const/16 v0, 0x7d

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1758
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1662
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/play/layout/j;->e:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    invoke-virtual {v0}, Lcom/google/android/play/layout/f;->b()I

    move-result v0

    goto :goto_0
.end method

.method protected final b(I)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1700
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 1701
    if-nez v4, :cond_0

    move v0, v1

    .line 1716
    :goto_0
    return v0

    .line 1706
    :cond_0
    add-int/lit8 v0, v4, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_2

    .line 1707
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    .line 1708
    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/f;->d(I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 1706
    :goto_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    .line 1712
    :pswitch_0
    add-int/lit8 v0, v4, -0x1

    if-eq v3, v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_1
    move v0, v2

    .line 1716
    goto :goto_0

    .line 1718
    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/play/layout/f;->c()V

    .line 1719
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 1723
    :cond_2
    new-instance v0, Ljava/lang/Error;

    const-string v1, "Should not reach here"

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1708
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1656
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    invoke-virtual {v0}, Lcom/google/android/play/layout/f;->a()V

    .line 1655
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1658
    :cond_0
    return-void
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 1741
    invoke-super {p0}, Lcom/google/android/play/layout/g;->d()V

    .line 1742
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1743
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    invoke-virtual {v0}, Lcom/google/android/play/layout/f;->c()V

    .line 1742
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1745
    :cond_0
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1746
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1735
    invoke-virtual {p0}, Lcom/google/android/play/layout/j;->d()V

    .line 1736
    sget-object v0, Lcom/google/android/play/layout/j;->b:Landroid/support/v4/f/n;

    invoke-interface {v0, p0}, Landroid/support/v4/f/n;->a(Ljava/lang/Object;)Z

    .line 1737
    return-void
.end method

.method public final e(I)V
    .locals 2

    .prologue
    .line 1728
    invoke-super {p0, p1}, Lcom/google/android/play/layout/g;->e(I)V

    .line 1729
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1730
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/f;->e(I)V

    .line 1729
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1732
    :cond_0
    return-void
.end method

.method public final f(I)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1682
    .line 1686
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v2, v1

    :goto_0
    if-ltz v3, :cond_1

    .line 1687
    iget-object v0, p0, Lcom/google/android/play/layout/j;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/f;

    .line 1688
    if-eqz v1, :cond_0

    .line 1689
    iget v0, v0, Lcom/google/android/play/layout/f;->g:I

    add-int/2addr v0, v2

    move v5, v1

    move v1, v0

    move v0, v5

    .line 1686
    :goto_1
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1690
    :cond_0
    iget v4, v0, Lcom/google/android/play/layout/f;->e:I

    if-gt v4, p1, :cond_2

    .line 1691
    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/f;->a(I)I

    move-result v1

    .line 1692
    const/4 v0, 0x1

    goto :goto_1

    .line 1695
    :cond_1
    return v2

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1
.end method
