.class public Lcom/google/android/play/onboard/OnboardTutorialPage;
.super Lcom/google/android/libraries/bind/widget/BindingFrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/onboard/o;


# static fields
.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I


# instance fields
.field protected h:Lcom/google/android/play/onboard/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget v0, Lcom/google/android/play/f;->V:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->d:I

    .line 27
    sget v0, Lcom/google/android/play/f;->Y:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->e:I

    .line 31
    sget v0, Lcom/google/android/play/f;->W:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->f:I

    .line 35
    sget v0, Lcom/google/android/play/f;->X:I

    sput v0, Lcom/google/android/play/onboard/OnboardTutorialPage;->g:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/play/onboard/OnboardTutorialPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/play/onboard/OnboardTutorialPage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/widget/BindingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/play/onboard/d;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/play/onboard/OnboardTutorialPage;->h:Lcom/google/android/play/onboard/d;

    .line 55
    return-void
.end method

.method public final b()Lcom/google/android/play/onboard/p;
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/play/onboard/OnboardTutorialPage;->s()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    .line 60
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Lcom/google/android/play/onboard/p;

    return-object v0

    :cond_0
    sget v1, Lcom/google/android/play/onboard/o;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 72
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method
