.class public final Lcom/google/android/play/utils/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final A:Lcom/google/android/play/utils/a/a;

.field public static B:Lcom/google/android/play/utils/a/a;

.field public static C:Lcom/google/android/play/utils/a/a;

.field public static final D:Lcom/google/android/play/utils/a/a;

.field public static final a:[Ljava/lang/String;

.field public static final b:Lcom/google/android/play/utils/a/a;

.field public static final c:Lcom/google/android/play/utils/a/a;

.field public static final d:Lcom/google/android/play/utils/a/a;

.field public static final e:Lcom/google/android/play/utils/a/a;

.field public static final f:Lcom/google/android/play/utils/a/a;

.field public static final g:Lcom/google/android/play/utils/a/a;

.field public static final h:Lcom/google/android/play/utils/a/a;

.field public static final i:Lcom/google/android/play/utils/a/a;

.field public static final j:Lcom/google/android/play/utils/a/a;

.field public static final k:Lcom/google/android/play/utils/a/a;

.field public static final l:Lcom/google/android/play/utils/a/a;

.field public static final m:Lcom/google/android/play/utils/a/a;

.field public static final n:Lcom/google/android/play/utils/a/a;

.field public static final o:Lcom/google/android/play/utils/a/a;

.field public static final p:Lcom/google/android/play/utils/a/a;

.field public static final q:Lcom/google/android/play/utils/a/a;

.field public static final r:Lcom/google/android/play/utils/a/a;

.field public static s:Lcom/google/android/play/utils/a/a;

.field public static t:Lcom/google/android/play/utils/a/a;

.field public static final u:Lcom/google/android/play/utils/a/a;

.field public static v:Lcom/google/android/play/utils/a/a;

.field public static final w:Lcom/google/android/play/utils/a/a;

.field public static final x:Lcom/google/android/play/utils/a/a;

.field public static final y:Lcom/google/android/play/utils/a/a;

.field public static final z:Lcom/google/android/play/utils/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 22
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "playcommon"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/play/utils/a/i;->a:[Ljava/lang/String;

    .line 29
    const-string v0, "playcommon.mcc_mnc_override"

    invoke-static {v0, v2}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->b:Lcom/google/android/play/utils/a/a;

    .line 35
    const-string v0, "playcommon.proto_log_url_regexp"

    const-string v1, ".*"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->c:Lcom/google/android/play/utils/a/a;

    .line 41
    const-string v0, "playcommon.dfe_request_timeout_ms"

    const/16 v1, 0x9c4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->d:Lcom/google/android/play/utils/a/a;

    .line 47
    const-string v0, "playcommon.dfe_max_retries"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->e:Lcom/google/android/play/utils/a/a;

    .line 53
    const-string v0, "playcommon.dfe_backoff_multiplier"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->f:Lcom/google/android/play/utils/a/a;

    .line 59
    const-string v0, "playcommon.plus_profile_bg_timeout_ms"

    const/16 v1, 0x1f40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->g:Lcom/google/android/play/utils/a/a;

    .line 65
    const-string v0, "playcommon.plus_profile_bg_max_retries"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->h:Lcom/google/android/play/utils/a/a;

    .line 71
    const-string v0, "playcommon.plus_profile_bg_backoff_mult"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->i:Lcom/google/android/play/utils/a/a;

    .line 77
    const-string v0, "playcommon.ip_country_override"

    invoke-static {v0, v2}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->j:Lcom/google/android/play/utils/a/a;

    .line 80
    const-string v0, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->k:Lcom/google/android/play/utils/a/a;

    .line 82
    const-string v0, "playcommon.auth_token_type"

    const-string v1, "androidmarket"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->l:Lcom/google/android/play/utils/a/a;

    .line 85
    const-string v0, "logging_id2"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->m:Lcom/google/android/play/utils/a/a;

    .line 88
    const-string v0, "market_client_id"

    const-string v1, "am-google"

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->n:Lcom/google/android/play/utils/a/a;

    .line 95
    const-string v0, "playcommon.skip_all_caches"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->o:Lcom/google/android/play/utils/a/a;

    .line 99
    const-string v0, "playcommon.show_staging_data"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->p:Lcom/google/android/play/utils/a/a;

    .line 103
    const-string v0, "playcommon.prex_disabled"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->q:Lcom/google/android/play/utils/a/a;

    .line 110
    const-string v0, "playcommon.tentative_gc_runner_enabled"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->r:Lcom/google/android/play/utils/a/a;

    .line 116
    const-string v0, "playcommon.bitmap_loader_cache_size_mb"

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->s:Lcom/google/android/play/utils/a/a;

    .line 119
    const-string v0, "playcommon.bitmap_loader_cache_size_ratio_to_screen"

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->t:Lcom/google/android/play/utils/a/a;

    .line 126
    const-string v0, "playcommon.min_image_size_limit_in_lru_cache_bytes"

    const/high16 v1, 0x80000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->u:Lcom/google/android/play/utils/a/a;

    .line 129
    const-string v0, "playcommon.debug_display_image_sizes"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->v:Lcom/google/android/play/utils/a/a;

    .line 133
    const-string v0, "playcommon.webp_fife_images_enabled"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->w:Lcom/google/android/play/utils/a/a;

    .line 137
    const-string v0, "playcommon.percent_of_image_size_wifi"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->x:Lcom/google/android/play/utils/a/a;

    .line 141
    const-string v0, "playcommon.percent_of_image_size_4g"

    const v1, 0x3f666666    # 0.9f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->y:Lcom/google/android/play/utils/a/a;

    .line 145
    const-string v0, "playcommon.percent_of_image_size_3g"

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->z:Lcom/google/android/play/utils/a/a;

    .line 149
    const-string v0, "playcommon.percent_of_image_size_2g"

    const v1, 0x3ee66666    # 0.45f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->A:Lcom/google/android/play/utils/a/a;

    .line 155
    const-string v0, "playcommon.main_cache_size_mb"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->B:Lcom/google/android/play/utils/a/a;

    .line 161
    const-string v0, "playcommon.image_cache_size_mb"

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->C:Lcom/google/android/play/utils/a/a;

    .line 167
    const-string v0, "playcommon.volley_buffer_pool_size_kb"

    const/16 v1, 0x100

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/play/utils/a/a;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/play/utils/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/play/utils/a/i;->D:Lcom/google/android/play/utils/a/a;

    return-void
.end method
