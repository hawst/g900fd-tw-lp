.class public final Lcom/google/android/play/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public static a(Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/play/headerlist/PlayHeaderListLayout;Z)Landroid/transition/Transition;
    .locals 6

    .prologue
    const/16 v3, 0x30

    const/4 v5, 0x0

    .line 85
    sget v0, Lcom/google/android/play/f;->i:I

    invoke-virtual {p3, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p4, :cond_0

    new-instance v1, Lcom/google/android/play/c/c;

    invoke-static {p1}, Lcom/google/android/play/c/i;->a(Landroid/view/View;)Landroid/graphics/Rect;

    invoke-direct {v1}, Lcom/google/android/play/c/c;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/play/c/c;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    new-instance v1, Landroid/transition/ArcMotion;

    invoke-direct {v1}, Landroid/transition/ArcMotion;-><init>()V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 86
    :goto_0
    invoke-virtual {p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->k()Landroid/view/View;

    move-result-object v2

    if-eqz p4, :cond_2

    new-instance v1, Landroid/transition/Fade;

    invoke-direct {v1}, Landroid/transition/Fade;-><init>()V

    :goto_1
    invoke-virtual {v1, v2}, Landroid/transition/Visibility;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    .line 87
    sget v2, Lcom/google/android/play/f;->at:I

    invoke-virtual {p3, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Landroid/transition/Fade;

    invoke-direct {v3}, Landroid/transition/Fade;-><init>()V

    invoke-virtual {v3, v2}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v2

    .line 88
    new-instance v3, Landroid/transition/Slide;

    invoke-direct {v3}, Landroid/transition/Slide;-><init>()V

    invoke-virtual {v3, p2}, Landroid/transition/Slide;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v3

    .line 90
    const/4 v4, 0x4

    new-array v4, v4, [Landroid/transition/Transition;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v2, v4, v0

    const/4 v0, 0x3

    aput-object v3, v4, v0

    invoke-static {v4}, Lcom/google/android/play/c/i;->a([Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/play/a/b;->a(Landroid/content/Context;)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/TransitionSet;

    move-result-object v0

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    move-result-object v0

    return-object v0

    .line 85
    :cond_0
    new-instance v1, Landroid/transition/Slide;

    invoke-direct {v1, v3}, Landroid/transition/Slide;-><init>(I)V

    invoke-virtual {v1, v0}, Landroid/transition/Slide;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 86
    :cond_2
    new-instance v1, Landroid/transition/Slide;

    invoke-direct {v1, v3}, Landroid/transition/Slide;-><init>(I)V

    goto :goto_1
.end method
