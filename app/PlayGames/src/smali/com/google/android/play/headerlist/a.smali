.class final Lcom/google/android/play/headerlist/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/cc;


# instance fields
.field final synthetic a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;


# direct methods
.method constructor <init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->c(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 357
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/cc;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/cc;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/cc;->a(I)V

    .line 360
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a()Z

    iget-object v1, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v1, v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;I)I

    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    iget-object v1, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->e(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/o;->c()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/SwipeRefreshLayout;->a(Z)V

    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/widget/SwipeRefreshLayout;

    :cond_0
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iget-object v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d:Lcom/google/android/play/headerlist/o;

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/cc;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 346
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/cc;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/cc;->a(IFI)V

    .line 349
    :cond_2
    return-void

    .line 344
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 329
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/cc;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)Landroid/support/v4/view/cc;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/support/v4/view/cc;->b(I)V

    .line 332
    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/google/android/play/headerlist/a;->a:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0, v2, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;ZZ)V

    .line 340
    :cond_2
    return-void
.end method
