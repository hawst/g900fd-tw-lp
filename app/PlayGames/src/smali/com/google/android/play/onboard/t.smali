.class public final Lcom/google/android/play/onboard/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/play/onboard/p;


# instance fields
.field protected final a:Landroid/content/Context;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/google/android/play/onboard/t;->a:Landroid/content/Context;

    .line 93
    iput p2, p0, Lcom/google/android/play/onboard/t;->b:I

    .line 94
    iput p3, p0, Lcom/google/android/play/onboard/t;->c:I

    .line 95
    return-void
.end method


# virtual methods
.method public final b(Lcom/google/android/play/onboard/d;)Lcom/google/android/play/onboard/n;
    .locals 1

    .prologue
    .line 119
    new-instance v0, Lcom/google/android/play/onboard/n;

    invoke-direct {v0}, Lcom/google/android/play/onboard/n;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/play/onboard/n;->a()Lcom/google/android/play/onboard/n;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/play/onboard/d;)Lcom/google/android/play/onboard/n;
    .locals 3

    .prologue
    .line 124
    new-instance v0, Lcom/google/android/play/onboard/n;

    invoke-direct {v0}, Lcom/google/android/play/onboard/n;-><init>()V

    sget v1, Lcom/google/android/play/e;->a:I

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/n;->a(I)Lcom/google/android/play/onboard/n;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/play/onboard/t;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/play/i;->f:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/n;->a(Landroid/content/Context;I)Lcom/google/android/play/onboard/n;

    move-result-object v0

    new-instance v1, Lcom/google/android/play/onboard/u;

    invoke-direct {v1, p0, p1}, Lcom/google/android/play/onboard/u;-><init>(Lcom/google/android/play/onboard/t;Lcom/google/android/play/onboard/d;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/n;->a(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/n;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x1

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/google/android/play/onboard/t;->c:I

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/google/android/play/onboard/t;->b:I

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x1

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x1

    return v0
.end method
