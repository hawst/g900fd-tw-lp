.class public Lcom/google/android/gms/drive/internal/DisconnectRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/drive/internal/m;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/DisconnectRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/internal/DisconnectRequest;-><init>(I)V

    .line 53
    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lcom/google/android/gms/drive/internal/DisconnectRequest;->a:I

    .line 48
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 36
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/m;->a(Lcom/google/android/gms/drive/internal/DisconnectRequest;Landroid/os/Parcel;)V

    .line 37
    return-void
.end method
