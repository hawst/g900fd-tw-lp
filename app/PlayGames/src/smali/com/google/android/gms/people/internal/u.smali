.class final Lcom/google/android/gms/people/internal/u;
.super Lcom/google/android/gms/common/internal/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/g;


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/internal/m;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Lcom/google/android/gms/people/model/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/f;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/google/android/gms/people/internal/u;->b:Lcom/google/android/gms/people/internal/m;

    .line 419
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/g;-><init>(Lcom/google/android/gms/common/internal/e;Ljava/lang/Object;)V

    .line 420
    iput-object p3, p0, Lcom/google/android/gms/people/internal/u;->c:Lcom/google/android/gms/common/api/Status;

    .line 421
    iput-object p4, p0, Lcom/google/android/gms/people/internal/u;->d:Lcom/google/android/gms/people/model/f;

    .line 422
    return-void
.end method


# virtual methods
.method public final E_()Lcom/google/android/gms/people/model/f;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/gms/people/internal/u;->d:Lcom/google/android/gms/people/model/f;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/gms/people/internal/u;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 412
    check-cast p1, Lcom/google/android/gms/common/api/m;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 0

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/google/android/gms/people/internal/u;->f_()V

    .line 434
    return-void
.end method

.method public final f_()V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/gms/people/internal/u;->d:Lcom/google/android/gms/people/model/f;

    if-eqz v0, :cond_0

    .line 449
    iget-object v0, p0, Lcom/google/android/gms/people/internal/u;->d:Lcom/google/android/gms/people/model/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/f;->c()V

    .line 451
    :cond_0
    return-void
.end method
