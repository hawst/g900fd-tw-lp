.class public final Lcom/google/android/gms/games/ui/destination/onboarding/d;
.super Lcom/google/android/play/onboard/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/destination/onboarding/c;
.implements Lcom/google/android/play/onboard/r;


# instance fields
.field private ap:Z

.field private aq:Ljava/lang/String;

.field private ar:Lcom/google/android/libraries/bind/data/m;

.field private as:Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

.field private at:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/play/onboard/e;-><init>()V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->at:Z

    return-void
.end method


# virtual methods
.method public final P()V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->at:Z

    .line 194
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/d;->Q()V

    .line 195
    return-void
.end method

.method public final Q()V
    .locals 2

    .prologue
    .line 199
    invoke-super {p0}, Lcom/google/android/play/onboard/e;->Q()V

    .line 201
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->at:Z

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->a()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 208
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->ar:Lcom/google/android/libraries/bind/data/m;

    .line 211
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->aq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->b(Ljava/lang/String;)V

    .line 212
    return-void

    .line 204
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->a()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(I)V

    goto :goto_0
.end method

.method public final R()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->as:Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->as:Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->a()V

    .line 242
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->as:Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->as:Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->a(Lcom/google/android/gms/games/ui/destination/onboarding/c;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->as:Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;->a(Landroid/app/Activity;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->as:Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardAccountsPage;

    return-object v0
.end method

.method public final a()Lcom/google/android/libraries/bind/data/m;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 91
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 92
    new-instance v3, Lcom/google/android/play/onboard/s;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v3, v4, v0}, Lcom/google/android/play/onboard/s;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/d;->T()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->a(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    const v4, 0x7f0f009d

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->b(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    const v4, 0x7f0f009c

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->c(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    const v4, 0x7f0200ff

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->d(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/games/ui/destination/onboarding/e;

    iget-object v5, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v4, v5, v0}, Lcom/google/android/gms/games/ui/destination/onboarding/e;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->a(Lcom/google/android/play/onboard/p;)Lcom/google/android/play/onboard/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/play/onboard/s;->a()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/play/onboard/s;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v3, v4, v1}, Lcom/google/android/play/onboard/s;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/d;->T()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->a(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    const v4, 0x7f0f009f

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->b(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    const v4, 0x7f0f009e

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->c(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    const v4, 0x7f020100

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->d(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/games/ui/destination/onboarding/e;

    iget-object v5, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v4, v5, v1}, Lcom/google/android/gms/games/ui/destination/onboarding/e;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->a(Lcom/google/android/play/onboard/p;)Lcom/google/android/play/onboard/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/play/onboard/s;->a()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/play/onboard/s;

    iget-object v4, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v3, v4, v6}, Lcom/google/android/play/onboard/s;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/d;->T()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->a(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    const v4, 0x7f0f00a1

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->b(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    const v4, 0x7f0f00a0

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->c(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    const v4, 0x7f020101

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->d(I)Lcom/google/android/play/onboard/s;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/games/ui/destination/onboarding/e;

    iget-object v5, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/games/ui/destination/onboarding/e;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3, v4}, Lcom/google/android/play/onboard/s;->a(Lcom/google/android/play/onboard/p;)Lcom/google/android/play/onboard/s;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/play/onboard/s;->a()Lcom/google/android/libraries/bind/data/Data;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-static {v3}, Lcom/google/android/gms/games/app/b;->d(Landroid/content/Context;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v1, :cond_0

    if-nez v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->ap:Z

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->ap:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/Data;-><init>()V

    sget v1, Lcom/google/android/play/onboard/o;->a:I

    const-string v3, "accounts"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    sget v1, Lcom/google/android/play/onboard/q;->e:I

    invoke-virtual {v0, v1, p0}, Lcom/google/android/libraries/bind/data/Data;->a(ILjava/lang/Object;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_2
    new-instance v0, Lcom/google/android/libraries/bind/data/m;

    sget v1, Lcom/google/android/play/onboard/o;->a:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/bind/data/m;-><init>(ILjava/util/List;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->ar:Lcom/google/android/libraries/bind/data/m;

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->ar:Lcom/google/android/libraries/bind/data/m;

    return-object v0
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0, p1, p2}, Lcom/google/android/play/onboard/e;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 66
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04007b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 68
    sget v1, Lcom/google/android/play/f;->Z:I

    invoke-super {p0, v0, v1}, Lcom/google/android/play/onboard/e;->a(Landroid/view/View;I)V

    .line 69
    return-void
.end method

.method public final a(Lcom/google/android/play/onboard/p;)V
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 162
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/e;->a(Lcom/google/android/play/onboard/p;)V

    .line 165
    if-eqz p1, :cond_1

    .line 166
    invoke-interface {p1}, Lcom/google/android/play/onboard/p;->j()I

    move-result v1

    .line 167
    packed-switch v1, :pswitch_data_0

    move v4, v0

    move v0, v1

    move v1, v4

    .line 183
    :goto_0
    if-ltz v1, :cond_0

    .line 185
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->a()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 189
    :goto_1
    return-void

    .line 169
    :pswitch_0
    const/4 v0, 0x2

    move v4, v0

    move v0, v1

    move v1, v4

    .line 170
    goto :goto_0

    .line 172
    :pswitch_1
    const/4 v0, 0x3

    move v4, v0

    move v0, v1

    move v1, v4

    .line 173
    goto :goto_0

    .line 175
    :pswitch_2
    const/4 v0, 0x4

    move v4, v0

    move v0, v1

    move v1, v4

    .line 176
    goto :goto_0

    .line 178
    :pswitch_3
    const/4 v0, 0x5

    move v4, v0

    move v0, v1

    move v1, v4

    goto :goto_0

    .line 187
    :cond_0
    const-string v1, "OnboardHostFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "goToPage - trying to go to invalid page: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move v1, v0

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/onboarding/GamesOnboardHostActivity;->a()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/app/a;->a(I)V

    .line 232
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->aq:Ljava/lang/String;

    .line 233
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->ap:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    .line 151
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/d;->V()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/d;->Q()V

    .line 158
    :goto_1
    return-void

    .line 150
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 156
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/onboarding/d;->W()V

    goto :goto_1
.end method

.method protected final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/e;->c(Landroid/os/Bundle;)V

    .line 78
    const-string v0, "showAccountsState"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->ap:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 79
    const-string v0, "selectedAccountState"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->aq:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method protected final l(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/google/android/play/onboard/e;->l(Landroid/os/Bundle;)V

    .line 85
    const-string v0, "showAccountsState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->ap:Z

    .line 86
    const-string v0, "selectedAccountState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/onboarding/d;->aq:Ljava/lang/String;

    .line 87
    return-void
.end method
