.class final Lcom/google/android/gms/games/internal/aq;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/ag;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/ag;)V
    .locals 0

    .prologue
    .line 845
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 846
    iput-object p1, p0, Lcom/google/android/gms/games/internal/aq;->a:Lcom/google/android/gms/common/api/ag;

    .line 847
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 870
    iget-object v0, p0, Lcom/google/android/gms/games/internal/aq;->a:Lcom/google/android/gms/common/api/ag;

    new-instance v1, Lcom/google/android/gms/games/internal/as;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/as;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/ag;->a(Lcom/google/android/gms/common/api/ai;)V

    .line 871
    return-void
.end method

.method public final m(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 852
    new-instance v1, Lcom/google/android/gms/games/request/a;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/request/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 853
    const/4 v0, 0x0

    .line 855
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->a()I

    move-result v2

    if-lez v2, :cond_0

    .line 856
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/request/a;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 859
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->f_()V

    .line 863
    if-eqz v0, :cond_1

    .line 864
    iget-object v1, p0, Lcom/google/android/gms/games/internal/aq;->a:Lcom/google/android/gms/common/api/ag;

    new-instance v2, Lcom/google/android/gms/games/internal/ar;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/internal/ar;-><init>(Lcom/google/android/gms/games/request/GameRequest;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/ag;->a(Lcom/google/android/gms/common/api/ai;)V

    .line 866
    :cond_1
    return-void

    .line 859
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->f_()V

    throw v0
.end method
