.class final Lcom/google/android/gms/games/internal/bg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/internal/be;


# instance fields
.field private a:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 2726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2727
    iput-object p1, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    .line 2728
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/internal/bb;[BLjava/lang/String;Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 3352
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3353
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3356
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3357
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3358
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 3359
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3360
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3361
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13a9

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3362
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 3363
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3366
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3367
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3369
    return v0

    .line 3357
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3366
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3367
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a([BLjava/lang/String;[Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 3373
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3374
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3377
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3378
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 3379
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3380
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 3381
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13aa

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3382
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 3383
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3386
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3387
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3389
    return v0

    .line 3386
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3387
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(IIZ)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4915
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4916
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4919
    :try_start_0
    const-string v3, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4920
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 4921
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4922
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4923
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2330

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4924
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4925
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 4926
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4933
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4934
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4936
    return-object v0

    .line 4929
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 4933
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4934
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(I[BILjava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 5382
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5383
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5386
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5387
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 5388
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 5389
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5390
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5391
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x271c

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5392
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5393
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 5394
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5401
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5402
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5404
    return-object v0

    .line 5397
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5401
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5402
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/achievement/AchievementEntity;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 6417
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6418
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6421
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6422
    if-eqz p1, :cond_0

    .line 6423
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6424
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/games/achievement/AchievementEntity;->writeToParcel(Landroid/os/Parcel;I)V

    .line 6429
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x32cd

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6430
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 6431
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 6432
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6439
    :goto_1
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6440
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6442
    return-object v0

    .line 6427
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 6439
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6440
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 6435
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 5548
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5549
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5552
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5553
    if-eqz p1, :cond_0

    .line 5554
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5555
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5560
    :goto_0
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5561
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5562
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2725

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5563
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5564
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 5565
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5572
    :goto_1
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5573
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5575
    return-object v0

    .line 5558
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 5572
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5573
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 5568
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 5464
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5465
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5468
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5469
    if-eqz p1, :cond_0

    .line 5470
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5471
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5476
    :goto_0
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5477
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2726

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5478
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5479
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 5480
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5487
    :goto_1
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5488
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5490
    return-object v0

    .line 5474
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 5487
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5488
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 5483
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;I)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 4987
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4988
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4991
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4992
    if-eqz p1, :cond_0

    .line 4993
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4994
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;->writeToParcel(Landroid/os/Parcel;I)V

    .line 4999
    :goto_0
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5000
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2333

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5001
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5002
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 5003
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5010
    :goto_1
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5011
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5013
    return-object v0

    .line 4997
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 5010
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5011
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 5006
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;ZZI)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5675
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 5676
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 5679
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5680
    invoke-virtual {v3, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5681
    if-eqz p2, :cond_0

    move v2, v0

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5682
    if-eqz p3, :cond_1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5683
    invoke-virtual {v3, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 5684
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2ee1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5685
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V

    .line 5686
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 5687
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v4}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5694
    :goto_2
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5695
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5697
    return-object v0

    :cond_0
    move v2, v1

    .line 5681
    goto :goto_0

    :cond_1
    move v0, v1

    .line 5682
    goto :goto_1

    .line 5690
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 5694
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5695
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a([I)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 6267
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6268
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6271
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6272
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 6273
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2efe

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6274
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 6275
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 6276
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6283
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6284
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6286
    return-object v0

    .line 6279
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6283
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6284
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 5061
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5062
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5065
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5066
    const/4 v0, 0x0

    invoke-virtual {v1, p1, v0}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 5067
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5068
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5069
    if-eqz p4, :cond_0

    .line 5070
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5071
    const/4 v0, 0x0

    invoke-virtual {p4, v1, v0}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5076
    :goto_0
    if-eqz p5, :cond_1

    .line 5077
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5078
    const/4 v0, 0x0

    invoke-virtual {p5, v1, v0}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5083
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2347

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5084
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5085
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 5086
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5093
    :goto_2
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5094
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5096
    return-object v0

    .line 5074
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 5093
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5094
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 5081
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 5089
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 6518
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6519
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6522
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6523
    const/4 v0, 0x0

    invoke-virtual {v1, p1, v0}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 6524
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6525
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6526
    if-eqz p4, :cond_0

    .line 6527
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6528
    const/4 v0, 0x0

    invoke-virtual {p4, v1, v0}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    .line 6533
    :goto_0
    if-eqz p5, :cond_1

    .line 6534
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6535
    const/4 v0, 0x0

    invoke-virtual {p5, v1, v0}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    .line 6540
    :goto_1
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6541
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x36b3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6542
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 6543
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 6544
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6551
    :goto_2
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6552
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6554
    return-object v0

    .line 6531
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 6551
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6552
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 6538
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 6547
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    .locals 5

    .prologue
    .line 4186
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4187
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4190
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4191
    if-eqz p1, :cond_0

    .line 4192
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4193
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    .line 4198
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x196b

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4199
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4200
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 4201
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4208
    :goto_1
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4209
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4211
    return-object v0

    .line 4196
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 4208
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4209
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 4204
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2774
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2775
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2778
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2779
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x138b

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2780
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 2781
    invoke-virtual {v2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2784
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2785
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2787
    return-object v0

    .line 2784
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2785
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2870
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2871
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2874
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2875
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2876
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13c8

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2877
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 2878
    invoke-virtual {v2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2881
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2882
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2884
    return-object v0

    .line 2881
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2882
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(I)V
    .locals 5

    .prologue
    .line 3413
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3414
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3416
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3417
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 3418
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13ac

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3419
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3422
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3423
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3424
    return-void

    .line 3422
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3423
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 2744
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2745
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2747
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2748
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 2749
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1389

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2750
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2753
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2754
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2755
    return-void

    .line 2753
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2754
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 4658
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4659
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4661
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4662
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 4663
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4664
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f53

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4665
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4668
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4669
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4670
    return-void

    .line 4668
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4669
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2815
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2816
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2818
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2819
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 2820
    if-eqz p2, :cond_0

    .line 2821
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2822
    const/4 v0, 0x0

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2827
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x138d

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2828
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2831
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2832
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2833
    return-void

    .line 2825
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2831
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2832
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/Contents;)V
    .locals 5

    .prologue
    .line 5782
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5783
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5785
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5786
    if-eqz p1, :cond_0

    .line 5787
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5788
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/drive/Contents;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5793
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2ef3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5794
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5797
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5798
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5799
    return-void

    .line 5791
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 5797
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5798
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 2759
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2760
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2762
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2763
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 2764
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x138a

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2765
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2768
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2769
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2770
    return-void

    .line 2763
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2768
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2769
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;I)V
    .locals 5

    .prologue
    .line 5514
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5515
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5517
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5518
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5519
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5520
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2720

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5521
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5524
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5525
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5526
    return-void

    .line 5518
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5524
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5525
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;III)V
    .locals 5

    .prologue
    .line 5327
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5328
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5330
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5331
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5332
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5333
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5334
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 5335
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2719

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5336
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5339
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5340
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5341
    return-void

    .line 5331
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5339
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5340
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;IIZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3558
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3559
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 3561
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3562
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3563
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3564
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3565
    if-eqz p4, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3566
    if-eqz p5, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3567
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13b4

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3568
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3571
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3572
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3573
    return-void

    .line 3562
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 3565
    goto :goto_1

    :cond_2
    move v0, v1

    .line 3566
    goto :goto_2

    .line 3571
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3572
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;II[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 4344
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4345
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4347
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4348
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4349
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4350
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 4351
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 4352
    if-eqz p5, :cond_1

    .line 4353
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4354
    const/4 v0, 0x0

    invoke-virtual {p5, v1, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 4359
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f44

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4360
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4363
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4364
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4365
    return-void

    .line 4348
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4357
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 4363
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4364
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;ILjava/lang/String;[Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6497
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6498
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6500
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6501
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6502
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 6503
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6504
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 6505
    if-eqz p5, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6506
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x36b2

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6507
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6510
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6511
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6512
    return-void

    .line 6501
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6510
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6511
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2969
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 2970
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 2972
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2973
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 2974
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2975
    if-eqz p3, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2976
    if-eqz p4, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2977
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1397

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2978
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2981
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 2982
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 2983
    return-void

    .line 2973
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 2975
    goto :goto_1

    :cond_2
    move v0, v1

    .line 2976
    goto :goto_2

    .line 2981
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 2982
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;I[I)V
    .locals 5

    .prologue
    .line 5581
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5582
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5584
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5585
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5586
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5587
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 5588
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2722

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5589
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5592
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5593
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5594
    return-void

    .line 5585
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5592
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5593
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;J)V
    .locals 6

    .prologue
    .line 3257
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3258
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3260
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3261
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3262
    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    .line 3263
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13c2

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3264
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3267
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3268
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3269
    return-void

    .line 3261
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3267
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3268
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;JLjava/lang/String;)V
    .locals 6

    .prologue
    .line 4641
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4642
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4644
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4645
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4646
    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    .line 4647
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4648
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f52

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4649
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4652
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4653
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4654
    return-void

    .line 4645
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4652
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4653
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Landroid/os/Bundle;II)V
    .locals 5

    .prologue
    .line 3077
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3078
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3080
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3081
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3082
    if-eqz p2, :cond_1

    .line 3083
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3084
    const/4 v0, 0x0

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 3089
    :goto_1
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3090
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 3091
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x139d

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3092
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3095
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3096
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3097
    return-void

    .line 3081
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3087
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3095
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3096
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Landroid/os/IBinder;I[Ljava/lang/String;Landroid/os/Bundle;ZJ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3290
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3291
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 3293
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3294
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3295
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3296
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3297
    invoke-virtual {v3, p4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 3298
    if-eqz p5, :cond_1

    .line 3299
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3300
    const/4 v2, 0x0

    invoke-virtual {p5, v3, v2}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 3305
    :goto_1
    if-eqz p6, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3306
    invoke-virtual {v3, p7, p8}, Landroid/os/Parcel;->writeLong(J)V

    .line 3307
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13a6

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3308
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3311
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3312
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3313
    return-void

    .line 3294
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 3303
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3311
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3312
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0

    :cond_2
    move v0, v1

    .line 3305
    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Landroid/os/IBinder;Ljava/lang/String;ZJ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3317
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3318
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3320
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3321
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3322
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3323
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3324
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3325
    invoke-virtual {v2, p5, p6}, Landroid/os/Parcel;->writeLong(J)V

    .line 3326
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13a7

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3327
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3330
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3331
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3332
    return-void

    .line 3321
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 3330
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3331
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2953
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2954
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2956
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2957
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 2958
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2959
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1396

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2960
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2963
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2964
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2965
    return-void

    .line 2957
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2963
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2964
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 5365
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5366
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5368
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5369
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5370
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5371
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5372
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x271b

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5373
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5376
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5377
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5378
    return-void

    .line 5369
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5376
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5377
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IIIZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3037
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3038
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3040
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3041
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3042
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3043
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3044
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 3045
    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 3046
    if-eqz p6, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3047
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x139b

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3048
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3051
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3052
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3053
    return-void

    .line 3041
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 3051
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3052
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 3166
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3167
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3169
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3170
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3171
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3172
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3173
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3174
    if-eqz p5, :cond_1

    .line 3175
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3176
    const/4 v0, 0x0

    invoke-virtual {p5, v1, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 3181
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13a1

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3182
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3185
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3186
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3187
    return-void

    .line 3170
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3179
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3185
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3186
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4725
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4726
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4728
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4729
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4730
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4731
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 4732
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4733
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1f57

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4734
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4737
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4738
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4739
    return-void

    .line 4729
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4737
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4738
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3577
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3578
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 3580
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3581
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3582
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3583
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3584
    if-eqz p4, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3585
    if-eqz p5, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3586
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13b5

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3587
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3590
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3591
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3592
    return-void

    .line 3581
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 3584
    goto :goto_1

    :cond_2
    move v0, v1

    .line 3585
    goto :goto_2

    .line 3590
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3591
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4072
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4073
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 4075
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4076
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4077
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4078
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 4079
    if-eqz p4, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4080
    if-eqz p5, :cond_2

    move v2, v0

    :goto_2
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4081
    if-eqz p6, :cond_3

    move v2, v0

    :goto_3
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4082
    if-eqz p7, :cond_4

    :goto_4
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4083
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1965

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4084
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4087
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 4088
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4089
    return-void

    .line 4076
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 4079
    goto :goto_1

    :cond_2
    move v2, v1

    .line 4080
    goto :goto_2

    :cond_3
    move v2, v1

    .line 4081
    goto :goto_3

    :cond_4
    move v0, v1

    .line 4082
    goto :goto_4

    .line 4087
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 4088
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;I[I)V
    .locals 5

    .prologue
    .line 5598
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5599
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5601
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5602
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5603
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5604
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5605
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 5606
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2723

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5607
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5610
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5611
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5612
    return-void

    .line 5602
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5610
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5611
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 2989
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2990
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2992
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2993
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 2994
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2995
    invoke-virtual {v1, p3, p4}, Landroid/os/Parcel;->writeLong(J)V

    .line 2996
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1398

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2997
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3000
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3001
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3002
    return-void

    .line 2993
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3000
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3001
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 4239
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4240
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4242
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4243
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4244
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4245
    invoke-virtual {v1, p3, p4}, Landroid/os/Parcel;->writeLong(J)V

    .line 4246
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4247
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1b5a

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4248
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4251
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4252
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4253
    return-void

    .line 4243
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4251
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4252
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 3118
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3119
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3121
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3122
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3123
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3124
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3125
    if-eqz p4, :cond_1

    .line 3126
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3127
    const/4 v0, 0x0

    invoke-virtual {p4, v1, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 3132
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x139f

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3133
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3136
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3137
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3138
    return-void

    .line 3122
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3130
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3136
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3137
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/SnapshotMetadataChangeEntity;Lcom/google/android/gms/drive/Contents;)V
    .locals 5

    .prologue
    .line 5752
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5753
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5755
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5756
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5757
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5758
    if-eqz p3, :cond_1

    .line 5759
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5760
    const/4 v0, 0x0

    invoke-virtual {p3, v1, v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadataChangeEntity;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5765
    :goto_1
    if-eqz p4, :cond_2

    .line 5766
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5767
    const/4 v0, 0x0

    invoke-virtual {p4, v1, v0}, Lcom/google/android/gms/drive/Contents;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5772
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2ee7

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5773
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5776
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5777
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5778
    return-void

    .line 5756
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5763
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 5776
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5777
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 5770
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3446
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3447
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3449
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3450
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3451
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3452
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3453
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13ae

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3454
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3457
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3458
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3459
    return-void

    .line 3450
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3457
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3458
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 5

    .prologue
    .line 4290
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4291
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4293
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4294
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4295
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4296
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4297
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 4298
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 4299
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f41

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4300
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4303
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4304
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4305
    return-void

    .line 4294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4303
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4304
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 5

    .prologue
    .line 5345
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5346
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5348
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5349
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5350
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5351
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5352
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 5353
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 5354
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeInt(I)V

    .line 5355
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x271a

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5356
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5359
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5360
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5361
    return-void

    .line 5349
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5359
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5360
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3463
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3464
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3466
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3467
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3468
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3469
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3470
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 3471
    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 3472
    invoke-virtual {v2, p6}, Landroid/os/Parcel;->writeInt(I)V

    .line 3473
    if-eqz p7, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3474
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13af

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3475
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3478
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3479
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3480
    return-void

    .line 3467
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 3478
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3479
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5142
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 5143
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 5145
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5146
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5147
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5148
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5149
    invoke-virtual {v3, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 5150
    if-eqz p5, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5151
    if-eqz p6, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5152
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2344

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5153
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5156
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5157
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5158
    return-void

    .line 5146
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 5150
    goto :goto_1

    :cond_2
    move v0, v1

    .line 5151
    goto :goto_2

    .line 5156
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5157
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/SnapshotMetadataChangeEntity;Lcom/google/android/gms/drive/Contents;)V
    .locals 5

    .prologue
    .line 5819
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5820
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5822
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5823
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5824
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5825
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5826
    if-eqz p4, :cond_1

    .line 5827
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5828
    const/4 v0, 0x0

    invoke-virtual {p4, v1, v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadataChangeEntity;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5833
    :goto_1
    if-eqz p5, :cond_2

    .line 5834
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5835
    const/4 v0, 0x0

    invoke-virtual {p5, v1, v0}, Lcom/google/android/gms/drive/Contents;->writeToParcel(Landroid/os/Parcel;I)V

    .line 5840
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2f01

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5841
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5844
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5845
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5846
    return-void

    .line 5823
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5831
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 5844
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5845
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0

    .line 5838
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4010
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4011
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4013
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4014
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4015
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4016
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4017
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4018
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1772

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4019
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4022
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4023
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4024
    return-void

    .line 4014
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4022
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4023
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;[IIZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6164
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6165
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6167
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6168
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6169
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6170
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6171
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 6172
    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 6173
    if-eqz p6, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6174
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2eef

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6175
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6178
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6179
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6180
    return-void

    .line 6168
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6178
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6179
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 5309
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5310
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5312
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5313
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5314
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5315
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5316
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 5317
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2718

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5318
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5321
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5322
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5323
    return-void

    .line 5313
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5321
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5322
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6184
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6185
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6187
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6188
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6189
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6190
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6191
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 6192
    if-eqz p5, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6193
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2efc

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6194
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6197
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6198
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6199
    return-void

    .line 6188
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6197
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6198
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3758
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3759
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3761
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3762
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3763
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3764
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3765
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13be

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3766
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3769
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3770
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3771
    return-void

    .line 3762
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 3769
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3770
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;[BLjava/lang/String;[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 5

    .prologue
    .line 4401
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4402
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4404
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4405
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4406
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4407
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 4408
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4409
    const/4 v0, 0x0

    invoke-virtual {v1, p5, v0}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 4410
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f47

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4411
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4414
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4415
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4416
    return-void

    .line 4405
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4414
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4415
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 5

    .prologue
    .line 4420
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4421
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4423
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4424
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4425
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4426
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 4427
    const/4 v0, 0x0

    invoke-virtual {v1, p4, v0}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 4428
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f48

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4429
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4432
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4433
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4434
    return-void

    .line 4424
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4432
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4433
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;[I)V
    .locals 5

    .prologue
    .line 4603
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4604
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4606
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4607
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4608
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4609
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 4610
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f51

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4611
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4614
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4615
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4616
    return-void

    .line 4607
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4614
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4615
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;[Ljava/lang/String;I[BI)V
    .locals 5

    .prologue
    .line 5257
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5258
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5260
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5261
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5262
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5263
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 5264
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 5265
    invoke-virtual {v1, p5}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 5266
    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeInt(I)V

    .line 5267
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2715

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5268
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5271
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5272
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5273
    return-void

    .line 5261
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5271
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5272
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3992
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3993
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3995
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3996
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3997
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3998
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1771

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3999
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4002
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4003
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4004
    return-void

    .line 3996
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4002
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4003
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;ZLandroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3887
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3888
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 3890
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3891
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3892
    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3893
    if-eqz p3, :cond_2

    .line 3894
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3895
    const/4 v0, 0x0

    invoke-virtual {p3, v3, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 3900
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13c7

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3901
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3904
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3905
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3906
    return-void

    .line 3891
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 3892
    goto :goto_1

    .line 3898
    :cond_2
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 3904
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3905
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;Z[Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6061
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6062
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6064
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6065
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6066
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6067
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 6068
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2eff

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6069
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6072
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6073
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6074
    return-void

    .line 6065
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6072
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6073
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;[I)V
    .locals 5

    .prologue
    .line 4328
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4329
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4331
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4332
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4333
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 4334
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f43

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4335
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4338
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4339
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4340
    return-void

    .line 4332
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4338
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4339
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;[IIZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6129
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6130
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6132
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6133
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6134
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 6135
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 6136
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6137
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2eea

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6138
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6141
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6142
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6143
    return-void

    .line 6133
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6141
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6142
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 5277
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5278
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5280
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5281
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5282
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 5283
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2716

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5284
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5287
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5288
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5289
    return-void

    .line 5281
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5287
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5288
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/internal/bb;[Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6147
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6148
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6150
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6151
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6152
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 6153
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6154
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2efd

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6155
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6158
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6159
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6160
    return-void

    .line 6151
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6158
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6159
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 3225
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3226
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3228
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3229
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3230
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3231
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13a4

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3232
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3235
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3236
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3237
    return-void

    .line 3235
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3236
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 6358
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6359
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6361
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6362
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6363
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6364
    if-eqz p3, :cond_0

    .line 6365
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6366
    const/4 v0, 0x0

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 6371
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x32ca

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6372
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6375
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6376
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6377
    return-void

    .line 6369
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 6375
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6376
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2888
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2889
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2891
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2892
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2893
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2894
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13c9

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2895
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2898
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2899
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2900
    return-void

    .line 2898
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2899
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 3681
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3682
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3684
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3685
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3686
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3687
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3688
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13bb

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3689
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3692
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3693
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3694
    return-void

    .line 3692
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3693
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3824
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3825
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3827
    :try_start_0
    const-string v3, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3828
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3829
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13cc

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3830
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3833
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3834
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3835
    return-void

    .line 3833
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3834
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 2731
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    return-object v0
.end method

.method public final b(IIZ)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4940
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4941
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4944
    :try_start_0
    const-string v3, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4945
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 4946
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4947
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4948
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2331

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4949
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4950
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 4951
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4958
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4959
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4961
    return-object v0

    .line 4954
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 4958
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4959
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b()Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 2791
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2792
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2795
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2796
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x138c

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2797
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 2798
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2799
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2806
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2807
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2809
    return-object v0

    .line 2802
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2806
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2807
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 3393
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3394
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3397
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3398
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3399
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13ab

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3400
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 3401
    invoke-virtual {v2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3404
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3405
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3407
    return-object v0

    .line 3404
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3405
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(J)V
    .locals 5

    .prologue
    .line 3273
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3274
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3276
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3277
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 3278
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13c3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3279
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3282
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3283
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3284
    return-void

    .line 3282
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3283
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 4691
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4692
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4694
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4695
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 4696
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4697
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f55

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4698
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4701
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4702
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4703
    return-void

    .line 4701
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4702
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 3006
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3007
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3009
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3010
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3011
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1399

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3012
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3015
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3016
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3017
    return-void

    .line 3010
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3015
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3016
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3598
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3599
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 3601
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3602
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3603
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3604
    if-eqz p3, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3605
    if-eqz p4, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3606
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13b6

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3607
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3610
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3611
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3612
    return-void

    .line 3602
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 3604
    goto :goto_1

    :cond_2
    move v0, v1

    .line 3605
    goto :goto_2

    .line 3610
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3611
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;J)V
    .locals 6

    .prologue
    .line 4487
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4488
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4490
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4491
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4492
    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    .line 4493
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f4c

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4494
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4497
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4498
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4499
    return-void

    .line 4491
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4497
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4498
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;JLjava/lang/String;)V
    .locals 6

    .prologue
    .line 4674
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4675
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4677
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4678
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4679
    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    .line 4680
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4681
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f54

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4682
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4685
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4686
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4687
    return-void

    .line 4678
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4685
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4686
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3021
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3022
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3024
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3025
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3026
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3027
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x139a

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3028
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3031
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3032
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3033
    return-void

    .line 3025
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3031
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3032
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 5905
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5906
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5908
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5909
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5910
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5911
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5912
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2ef7

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5913
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5916
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5917
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5918
    return-void

    .line 5909
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5916
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5917
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IIIZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3057
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3058
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3060
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3061
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3062
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3063
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3064
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 3065
    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 3066
    if-eqz p6, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3067
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x139c

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3068
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3071
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3072
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3073
    return-void

    .line 3061
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 3071
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3072
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 4259
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4260
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4262
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4263
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4264
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4265
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 4266
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4267
    if-eqz p5, :cond_1

    .line 4268
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4269
    const/4 v0, 0x0

    invoke-virtual {p5, v1, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 4274
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1b5b

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4275
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4278
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4279
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4280
    return-void

    .line 4263
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4272
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 4278
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4279
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 5530
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5531
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 5533
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5534
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5535
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5536
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5537
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5538
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2721

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5539
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5542
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5543
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5544
    return-void

    .line 5534
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 5542
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5543
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3943
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3944
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 3946
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3947
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3948
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3949
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 3950
    if-eqz p4, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3951
    if-eqz p5, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3952
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x157d

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3953
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3956
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3957
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3958
    return-void

    .line 3947
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 3950
    goto :goto_1

    :cond_2
    move v0, v1

    .line 3951
    goto :goto_2

    .line 3956
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3957
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 3142
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3143
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3145
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3146
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3147
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3148
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3149
    if-eqz p4, :cond_1

    .line 3150
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3151
    const/4 v0, 0x0

    invoke-virtual {p4, v1, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 3156
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13a0

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3157
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3160
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3161
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3162
    return-void

    .line 3146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3154
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 3160
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3161
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3507
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3508
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3510
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3511
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3512
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3513
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3514
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13b1

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3515
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3518
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3519
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3520
    return-void

    .line 3511
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3518
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3519
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3484
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3485
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3487
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3488
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3489
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3490
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3491
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 3492
    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 3493
    invoke-virtual {v2, p6}, Landroid/os/Parcel;->writeInt(I)V

    .line 3494
    if-eqz p7, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3495
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13b0

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3496
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3499
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3500
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3501
    return-void

    .line 3488
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 3499
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3500
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6315
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6316
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 6318
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6319
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6320
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6321
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6322
    invoke-virtual {v3, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 6323
    if-eqz p5, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 6324
    if-eqz p6, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6325
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2ef2

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6326
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6329
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 6330
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6331
    return-void

    .line 6319
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 6323
    goto :goto_1

    :cond_2
    move v0, v1

    .line 6324
    goto :goto_2

    .line 6329
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 6330
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4166
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4167
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4169
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4170
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4171
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4172
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4173
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4174
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x196a

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4175
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4178
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4179
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4180
    return-void

    .line 4170
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4178
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4179
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4095
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4096
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4098
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4099
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4100
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4101
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4102
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1966

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4103
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4106
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4107
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4108
    return-void

    .line 4099
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4106
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4107
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4114
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4115
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4117
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4118
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4119
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4120
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1967

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4121
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4124
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4125
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4126
    return-void

    .line 4118
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4124
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4125
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/bb;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 5293
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5294
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5296
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5297
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5298
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 5299
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2717

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5300
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5303
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5304
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5305
    return-void

    .line 5297
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5303
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5304
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 3241
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3242
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3244
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3245
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3246
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3247
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13a5

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3248
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3251
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3252
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3253
    return-void

    .line 3251
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3252
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4551
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4552
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4554
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4555
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4556
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4557
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f59

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4558
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4561
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4562
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4563
    return-void

    .line 4561
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4562
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 4622
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4623
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4625
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4626
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4627
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4628
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 4629
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f5a

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4630
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4633
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4634
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4635
    return-void

    .line 4633
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4634
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 5996
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5997
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5999
    :try_start_0
    const-string v3, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6000
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6001
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2efa

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6002
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6005
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6006
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6007
    return-void

    .line 6005
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6006
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2837
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2838
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2840
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2841
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x138e

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2842
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2845
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2846
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2847
    return-void

    .line 2845
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2846
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(J)V
    .locals 5

    .prologue
    .line 4503
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4504
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4506
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4507
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 4508
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f4d

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4509
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4512
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4513
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4514
    return-void

    .line 4512
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4513
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 5241
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5242
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5244
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5245
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 5246
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5247
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2714

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5248
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5251
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5252
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5253
    return-void

    .line 5251
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5252
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 3103
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3104
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3106
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3107
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3108
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x139e

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3109
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3112
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3113
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3114
    return-void

    .line 3107
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3112
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3113
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3631
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3632
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 3634
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3635
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3636
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3637
    if-eqz p3, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3638
    if-eqz p4, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 3639
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x13b8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3640
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3643
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3644
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3645
    return-void

    .line 3635
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 3637
    goto :goto_1

    :cond_2
    move v0, v1

    .line 3638
    goto :goto_2

    .line 3643
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 3644
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;J)V
    .locals 6

    .prologue
    .line 5193
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5194
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5196
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5197
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5198
    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    .line 5199
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2711

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5200
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5203
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5204
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5205
    return-void

    .line 5197
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5203
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5204
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;JLjava/lang/String;)V
    .locals 6

    .prologue
    .line 5224
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5225
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5227
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5228
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5229
    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    .line 5230
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5231
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2713

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5232
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5235
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5236
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5237
    return-void

    .line 5228
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5235
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5236
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3336
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3337
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3339
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3340
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3341
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3342
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13a8

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3343
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3346
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3347
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3348
    return-void

    .line 3340
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3346
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3347
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 5922
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5923
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5925
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5926
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5927
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5928
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5929
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2ef8

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5930
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5933
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5934
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5935
    return-void

    .line 5926
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5933
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5934
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4765
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4766
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 4768
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4769
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4770
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4771
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 4772
    if-eqz p4, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4773
    if-eqz p5, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4774
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2329

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4775
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4778
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 4779
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4780
    return-void

    .line 4769
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 4772
    goto :goto_1

    :cond_2
    move v0, v1

    .line 4773
    goto :goto_2

    .line 4778
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 4779
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4470
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4471
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4473
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4474
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4475
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4476
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4477
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f4b

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4478
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4481
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4482
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4483
    return-void

    .line 4474
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4481
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4482
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 5717
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5718
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 5720
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5721
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5722
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5723
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5724
    if-eqz p4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5725
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2ee3

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5726
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5729
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5730
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5731
    return-void

    .line 5721
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 5729
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5730
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4130
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4131
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4133
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4134
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4135
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4136
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4137
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1968

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4138
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4141
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4142
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4143
    return-void

    .line 4134
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4141
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4142
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4743
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4744
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4746
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4747
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4748
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4749
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1f5b

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4750
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4753
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4754
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4755
    return-void

    .line 4747
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4753
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4754
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/internal/bb;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 5618
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5619
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5621
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5622
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5623
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 5624
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2724

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5625
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5628
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5629
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5630
    return-void

    .line 5622
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5628
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5629
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3664
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3665
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3667
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3668
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3669
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13ba

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3670
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3673
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3674
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3675
    return-void

    .line 3673
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3674
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 3791
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3792
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3794
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3795
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3796
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 3797
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13bf

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3798
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3801
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3802
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3803
    return-void

    .line 3801
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3802
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final c(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6341
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6342
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6344
    :try_start_0
    const-string v3, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6345
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6346
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x32c9

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6347
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6350
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6351
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6352
    return-void

    .line 6350
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6351
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 3738
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3739
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3742
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3743
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3744
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13c4

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3745
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 3746
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3749
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3750
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3752
    return v0

    .line 3749
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3750
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2853
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2854
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2857
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2858
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x138f

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2859
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 2860
    invoke-virtual {v2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2863
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2864
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2866
    return-object v0

    .line 2863
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2864
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(J)V
    .locals 5

    .prologue
    .line 5209
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5210
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5212
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5213
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 5214
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2712

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5215
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5218
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5219
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5220
    return-void

    .line 5218
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5219
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(JLjava/lang/String;)V
    .locals 5

    .prologue
    .line 6251
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6252
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6254
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6255
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 6256
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6257
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2eee

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6258
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6261
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6262
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6263
    return-void

    .line 6261
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6262
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 3193
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3194
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3196
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3197
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3198
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13a2

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3199
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3202
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3203
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3204
    return-void

    .line 3197
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3202
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3203
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/internal/bb;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4030
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4031
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 4033
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4034
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4035
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4036
    if-eqz p3, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4037
    if-eqz p4, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4038
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1773

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4039
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4042
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 4043
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4044
    return-void

    .line 4034
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 4036
    goto :goto_1

    :cond_2
    move v0, v1

    .line 4037
    goto :goto_2

    .line 4042
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 4043
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/internal/bb;J)V
    .locals 6

    .prologue
    .line 6203
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6204
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6206
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6207
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6208
    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    .line 6209
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2eeb

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6210
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6213
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6214
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6215
    return-void

    .line 6207
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6213
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6214
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/internal/bb;JLjava/lang/String;)V
    .locals 6

    .prologue
    .line 6219
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6220
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6222
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6223
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6224
    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    .line 6225
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6226
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2eed

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6227
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6230
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6231
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6232
    return-void

    .line 6223
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6230
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6231
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3430
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3431
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3433
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3434
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3435
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3436
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13ad

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3437
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3440
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3441
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3442
    return-void

    .line 3434
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3440
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3441
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5123
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 5124
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 5126
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5127
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5128
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5129
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5130
    if-eqz p4, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5131
    if-eqz p5, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5132
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x233c

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5133
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5136
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5137
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5138
    return-void

    .line 5127
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 5130
    goto :goto_1

    :cond_2
    move v0, v1

    .line 5131
    goto :goto_2

    .line 5136
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5137
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4569
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4570
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4572
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4573
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4574
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4575
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4576
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f4f

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4577
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4580
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4581
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4582
    return-void

    .line 4573
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4580
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4581
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 4149
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4150
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4152
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4153
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4154
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4155
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4156
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1969

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4157
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4160
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4161
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4162
    return-void

    .line 4153
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4160
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4161
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/internal/bb;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 5701
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5702
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 5704
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5705
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5706
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5707
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2ee2

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5708
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5711
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5712
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5713
    return-void

    .line 5705
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 5711
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5712
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 5496
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5497
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5499
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5500
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5501
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5502
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x271e

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5503
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5506
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5507
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5508
    return-void

    .line 5506
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5507
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(Ljava/lang/String;)Landroid/net/Uri;
    .locals 5

    .prologue
    .line 3912
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3913
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3916
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3917
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3918
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13ca

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3919
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 3920
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3921
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3928
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3929
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3931
    return-object v0

    .line 3924
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3928
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3929
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2914
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2915
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2918
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2919
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1394

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2920
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 2921
    invoke-virtual {v2}, Landroid/os/Parcel;->readString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2924
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2925
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2927
    return-object v0

    .line 2924
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2925
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(J)V
    .locals 5

    .prologue
    .line 6236
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6237
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6239
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6240
    invoke-virtual {v1, p1, p2}, Landroid/os/Parcel;->writeLong(J)V

    .line 6241
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2eec

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6242
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6245
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6246
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6247
    return-void

    .line 6245
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6246
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 3210
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3211
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3213
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3214
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3215
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13a3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3216
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3219
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3220
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3221
    return-void

    .line 3214
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3219
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3220
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/internal/bb;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4048
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 4049
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 4051
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4052
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4053
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4054
    if-eqz p3, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 4055
    if-eqz p4, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 4056
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x1774

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4057
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4060
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 4061
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 4062
    return-void

    .line 4052
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 4054
    goto :goto_1

    :cond_2
    move v0, v1

    .line 4055
    goto :goto_2

    .line 4060
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 4061
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3526
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3527
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3529
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3530
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3531
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3532
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13b2

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3533
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3536
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3537
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3538
    return-void

    .line 3530
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3536
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3537
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5939
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 5940
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 5942
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5943
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5944
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5945
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5946
    if-eqz p4, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5947
    if-eqz p5, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5948
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2ef5

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5949
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5952
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5953
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5954
    return-void

    .line 5943
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 5946
    goto :goto_1

    :cond_2
    move v0, v1

    .line 5947
    goto :goto_2

    .line 5952
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5953
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4586
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4587
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4589
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4590
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4591
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4592
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4593
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f50

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4594
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4597
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4598
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4599
    return-void

    .line 4590
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4597
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4598
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 5735
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5736
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 5738
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5739
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5740
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5741
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5742
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2ee6

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5743
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5746
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5747
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5748
    return-void

    .line 5739
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 5746
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5747
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/internal/bb;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6027
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6028
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6030
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6031
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6032
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6033
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2f00

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6034
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6037
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6038
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6039
    return-void

    .line 6031
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6037
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6038
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final e(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 6078
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6079
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6081
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6082
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6083
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 6084
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2ef1

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6085
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6088
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6089
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6090
    return-void

    .line 6088
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6089
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final f(Ljava/lang/String;I)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 6471
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6472
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6475
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6476
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6477
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 6478
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x36b1

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6479
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 6480
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 6481
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6488
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6489
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6491
    return-object v0

    .line 6484
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6488
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6489
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final f()Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    .line 2931
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 2932
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 2935
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 2936
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1395

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 2937
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 2938
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2939
    sget-object v0, Lcom/google/android/gms/common/data/DataHolder;->CREATOR:Lcom/google/android/gms/common/data/j;

    invoke-static {v2}, Lcom/google/android/gms/common/data/j;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2946
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2947
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 2949
    return-object v0

    .line 2942
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2946
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 2947
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 3616
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3617
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3619
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3620
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3621
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13b7

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3622
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3625
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3626
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3627
    return-void

    .line 3620
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3625
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3626
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3542
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3543
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3545
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3546
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3547
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3548
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13b3

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3549
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3552
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3553
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3554
    return-void

    .line 3546
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3552
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3553
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;IZZ)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5958
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 5959
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    .line 5961
    :try_start_0
    const-string v2, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5962
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5963
    invoke-virtual {v3, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5964
    invoke-virtual {v3, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 5965
    if-eqz p4, :cond_1

    move v2, v0

    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 5966
    if-eqz p5, :cond_2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 5967
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2ef6

    const/4 v2, 0x0

    invoke-interface {v0, v1, v3, v4, v2}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5968
    invoke-virtual {v4}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5971
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5972
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 5973
    return-void

    .line 5962
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 5965
    goto :goto_1

    :cond_2
    move v0, v1

    .line 5966
    goto :goto_2

    .line 5971
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 5972
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 6112
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6113
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6115
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6116
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6117
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6118
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6119
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2ee9

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6120
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6123
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6124
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6125
    return-void

    .line 6116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6123
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6124
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6448
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6449
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6451
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6452
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6453
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6454
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6455
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x32ce

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6456
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6459
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6460
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6461
    return-void

    .line 6452
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6459
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6460
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/games/internal/bb;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6045
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6046
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6048
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6049
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6050
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6051
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x2ef0

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6052
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6055
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6056
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6057
    return-void

    .line 6049
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6055
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6056
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4311
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4312
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4314
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4315
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4316
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f42

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4317
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4320
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4321
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4322
    return-void

    .line 4320
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4321
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final g(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 4826
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4827
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4830
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4831
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4832
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x232c

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4833
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4834
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 4835
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4842
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4843
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4845
    return-object v0

    .line 4838
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4842
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4843
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final g(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 3649
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3650
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3652
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3653
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3654
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13b9

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3655
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3658
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3659
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3660
    return-void

    .line 3653
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3658
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3659
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final g(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3698
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3699
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3701
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3702
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3703
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3704
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13bc

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3705
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3708
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3709
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3710
    return-void

    .line 3702
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3708
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3709
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final g(Lcom/google/android/gms/games/internal/bb;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6383
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6384
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6386
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6387
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6388
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6389
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x32cb

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6390
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6393
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6394
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6395
    return-void

    .line 6387
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6393
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6394
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final g()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3807
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3808
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3811
    :try_start_0
    const-string v3, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3812
    iget-object v3, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v4, 0x13cb

    const/4 v5, 0x0

    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3813
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 3814
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    .line 3817
    :cond_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3818
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3820
    return v0

    .line 3817
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3818
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final h(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 5

    .prologue
    .line 5164
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5165
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5168
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5169
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5170
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2346

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5171
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5172
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 5173
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5180
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5181
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5183
    return-object v0

    .line 5176
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5180
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5181
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final h()Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    .line 3964
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3965
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3968
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3969
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x157e

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3970
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 3971
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3972
    sget-object v0, Lcom/google/android/gms/common/data/DataHolder;->CREATOR:Lcom/google/android/gms/common/data/j;

    invoke-static {v2}, Lcom/google/android/gms/common/data/j;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3979
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3980
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3982
    return-object v0

    .line 3975
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3979
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3980
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final h(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 3714
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3715
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 3718
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3719
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3720
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3721
    iget-object v1, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v4, 0x13bd

    const/4 v5, 0x0

    invoke-interface {v1, v4, v2, v3, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3722
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V

    .line 3723
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_0

    .line 3724
    sget-object v0, Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v3}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3731
    :cond_0
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3732
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3734
    return-object v0

    :cond_1
    move-object v1, v0

    .line 3719
    goto :goto_0

    .line 3731
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 3732
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final h(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 3841
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3842
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3844
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3845
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3846
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13c0

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3847
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3850
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3851
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3852
    return-void

    .line 3845
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3850
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3851
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final h(Lcom/google/android/gms/games/internal/bb;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 6399
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6400
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 6402
    :try_start_0
    const-string v1, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6403
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6404
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 6405
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v1, 0x32cc

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6406
    invoke-virtual {v3}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6409
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6410
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6411
    return-void

    .line 6403
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 6409
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 6410
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final i()I
    .locals 5

    .prologue
    .line 4534
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4535
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4538
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4539
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f58

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4540
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4541
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 4544
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4545
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4547
    return v0

    .line 4544
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4545
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final i(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 6290
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6291
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6294
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6295
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6296
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2f02

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6297
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 6298
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 6299
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6306
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6307
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6309
    return-object v0

    .line 6302
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6306
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6307
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final i(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 3872
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3873
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3875
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3876
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3877
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13c6

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3878
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3881
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3882
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3883
    return-void

    .line 3876
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3881
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3882
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final i(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3775
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3776
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3778
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3779
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3780
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3781
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13c5

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3782
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3785
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3786
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3787
    return-void

    .line 3779
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3785
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3786
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final j()V
    .locals 5

    .prologue
    .line 4709
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4710
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4712
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4713
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f56

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4714
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4717
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4718
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4719
    return-void

    .line 4717
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4718
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final j(Lcom/google/android/gms/games/internal/bb;)V
    .locals 5

    .prologue
    .line 5640
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5641
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5643
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5644
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5645
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2af9

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5646
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5649
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5650
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5651
    return-void

    .line 5644
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5649
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5650
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final j(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3856
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 3857
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 3859
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 3860
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 3861
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 3862
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x13c1

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 3863
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3866
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3867
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 3868
    return-void

    .line 3860
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 3866
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 3867
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final k()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 4804
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4805
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4808
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4809
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x232b

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4810
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4811
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 4812
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4819
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4820
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4822
    return-object v0

    .line 4815
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4819
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4820
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final k(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4221
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4222
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4224
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4225
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4226
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4227
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1b59

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4228
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4231
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4232
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4233
    return-void

    .line 4225
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4231
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4232
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final l()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 4849
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4850
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4853
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4854
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x232d

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4855
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4856
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 4857
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4864
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4865
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4867
    return-object v0

    .line 4860
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4864
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4865
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final l(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4369
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4370
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4372
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4373
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4374
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4375
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f45

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4376
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4379
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4380
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4381
    return-void

    .line 4373
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4379
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4380
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final m()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 4871
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4872
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4875
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4876
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x232e

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4877
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4878
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 4879
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4886
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4887
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4889
    return-object v0

    .line 4882
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4886
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4887
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final m(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4385
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4386
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4388
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4389
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4390
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4391
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f46

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4392
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4395
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4396
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4397
    return-void

    .line 4389
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4395
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4396
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final n()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 4893
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4894
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4897
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4898
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x232f

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4899
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4900
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 4901
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4908
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4909
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4911
    return-object v0

    .line 4904
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4908
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4909
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final n(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4438
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4439
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4441
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4442
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4443
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4444
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f49

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4445
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4448
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4449
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4450
    return-void

    .line 4442
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4448
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4449
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final o()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 4965
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4966
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4969
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4970
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2332

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4971
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 4972
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 4973
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4980
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4981
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4983
    return-object v0

    .line 4976
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4980
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4981
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final o(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4454
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4455
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4457
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4458
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4459
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4460
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f4a

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4461
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4464
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4465
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4466
    return-void

    .line 4458
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4464
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4465
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final p()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 5017
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5018
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5021
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5022
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2334

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5023
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5024
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 5025
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5032
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5033
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5035
    return-object v0

    .line 5028
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5032
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5033
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final p(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4518
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4519
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4521
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4522
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4523
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4524
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x1f4e

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4525
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4528
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4529
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4530
    return-void

    .line 4522
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4528
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4529
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final q()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 5039
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5040
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5043
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5044
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2335

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5045
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5046
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 5047
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5054
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5055
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5057
    return-object v0

    .line 5050
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5054
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5055
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final q(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 4786
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 4787
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 4789
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 4790
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 4791
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 4792
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x232a

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 4793
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4796
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4797
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 4798
    return-void

    .line 4790
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 4796
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 4797
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final r()I
    .locals 5

    .prologue
    .line 5104
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5105
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5108
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5109
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x233b

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5110
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5111
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 5114
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5115
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5117
    return v0

    .line 5114
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5115
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final r(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 5803
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5804
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5806
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5807
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5808
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5809
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2ef4

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5810
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5813
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5814
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5815
    return-void

    .line 5807
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5813
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5814
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final s()I
    .locals 5

    .prologue
    .line 5408
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5409
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5412
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5413
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x271d

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5414
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5415
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 5418
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5419
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5421
    return v0

    .line 5418
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5419
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final s(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 5889
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5890
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5892
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5893
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 5894
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 5895
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2ee5

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5896
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5899
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5900
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5901
    return-void

    .line 5893
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5899
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5900
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final t()I
    .locals 5

    .prologue
    .line 5425
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5426
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5429
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5430
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2727

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5431
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5432
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 5435
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5436
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5438
    return v0

    .line 5435
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5436
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final t(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 6011
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6012
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6014
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6015
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6016
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6017
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2efb

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6018
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6021
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6022
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6023
    return-void

    .line 6015
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6021
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6022
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final u()Landroid/content/Intent;
    .locals 5

    .prologue
    .line 5442
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5443
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5446
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5447
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x271f

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5448
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5449
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 5450
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5457
    :goto_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5458
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5460
    return-object v0

    .line 5453
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 5457
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5458
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final u(Lcom/google/android/gms/games/internal/bb;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 6096
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 6097
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 6099
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 6100
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/bb;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 6101
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 6102
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2ee8

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 6103
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6106
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6107
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 6108
    return-void

    .line 6100
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 6106
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 6107
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final v()V
    .locals 5

    .prologue
    .line 5655
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5656
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5658
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5659
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2afa

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5660
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5663
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5664
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5665
    return-void

    .line 5663
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5664
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final w()I
    .locals 5

    .prologue
    .line 5850
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5851
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5854
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5855
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2f03

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5856
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5857
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 5860
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5861
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5863
    return v0

    .line 5860
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5861
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final x()I
    .locals 5

    .prologue
    .line 5867
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5868
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5871
    :try_start_0
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5872
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v3, 0x2f04

    const/4 v4, 0x0

    invoke-interface {v0, v3, v1, v2, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5873
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5874
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 5877
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5878
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5880
    return v0

    .line 5877
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5878
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method public final y()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 5979
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 5980
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 5983
    :try_start_0
    const-string v3, "com.google.android.gms.games.internal.IGamesService"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 5984
    iget-object v3, p0, Lcom/google/android/gms/games/internal/bg;->a:Landroid/os/IBinder;

    const/16 v4, 0x2ef9

    const/4 v5, 0x0

    invoke-interface {v3, v4, v1, v2, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 5985
    invoke-virtual {v2}, Landroid/os/Parcel;->readException()V

    .line 5986
    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    .line 5989
    :cond_0
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5990
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 5992
    return v0

    .line 5989
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 5990
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method
