.class public final Lcom/google/android/gms/people/accountswitcherview/i;
.super Lcom/google/android/gms/people/accountswitcherview/o;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/accountswitcherview/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/t;Z)V

    .line 61
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/people/accountswitcherview/aa;->b:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/accountswitcherview/h;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/people/accountswitcherview/i;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/people/accountswitcherview/p;)V
    .locals 7

    .prologue
    const/4 v3, -0x1

    .line 25
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, v3

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;IIILcom/google/android/gms/people/accountswitcherview/p;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/j;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/accountswitcherview/j;-><init>(Lcom/google/android/gms/people/accountswitcherview/i;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/i;->a(Lcom/google/android/gms/people/accountswitcherview/p;)V

    .line 71
    return-void
.end method

.method protected final a(Lcom/google/android/gms/people/accountswitcherview/p;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 75
    if-nez p2, :cond_0

    .line 76
    iget-object v1, p1, Lcom/google/android/gms/people/accountswitcherview/p;->e:Landroid/widget/ImageView;

    move-object v0, p1

    .line 77
    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/j;

    .line 78
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/i;->b:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/people/accountswitcherview/p;->f:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/people/accountswitcherview/j;->b:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/gms/people/accountswitcherview/j;->a:I

    invoke-static {v2}, Lcom/google/android/gms/people/accountswitcherview/i;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/people/accountswitcherview/o;->a(Lcom/google/android/gms/people/accountswitcherview/p;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
