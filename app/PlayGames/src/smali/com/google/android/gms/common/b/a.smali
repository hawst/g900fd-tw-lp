.class public abstract Lcom/google/android/gms/common/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Lcom/google/android/gms/common/b/f;

.field private static final d:Ljava/lang/Object;


# instance fields
.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/b/a;->d:Ljava/lang/Object;

    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/common/b/a;->a:Lcom/google/android/gms/common/b/f;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/b/a;->e:Ljava/lang/Object;

    .line 101
    iput-object p1, p0, Lcom/google/android/gms/common/b/a;->b:Ljava/lang/String;

    .line 102
    iput-object p2, p0, Lcom/google/android/gms/common/b/a;->c:Ljava/lang/Object;

    .line 103
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/b/a;
    .locals 1

    .prologue
    .line 175
    new-instance v0, Lcom/google/android/gms/common/b/d;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/b/d;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/b/a;
    .locals 1

    .prologue
    .line 166
    new-instance v0, Lcom/google/android/gms/common/b/c;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/b/c;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/b/a;
    .locals 1

    .prologue
    .line 203
    new-instance v0, Lcom/google/android/gms/common/b/e;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/b/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lcom/google/android/gms/common/b/a;
    .locals 2

    .prologue
    .line 157
    new-instance v0, Lcom/google/android/gms/common/b/b;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/common/b/b;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 34
    sget-object v1, Lcom/google/android/gms/common/b/a;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 35
    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/b/a;->a:Lcom/google/android/gms/common/b/f;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/google/android/gms/common/b/g;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/b/g;-><init>(Landroid/content/ContentResolver;)V

    sput-object v0, Lcom/google/android/gms/common/b/a;->a:Lcom/google/android/gms/common/b/f;

    .line 38
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/common/b/a;->e:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/common/b/a;->e:Ljava/lang/Object;

    .line 135
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/b/a;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/common/b/a;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
