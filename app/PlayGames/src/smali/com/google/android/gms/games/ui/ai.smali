.class public final Lcom/google/android/gms/games/ui/ai;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget v0, Lcom/google/android/gms/i;->q:I

    sput v0, Lcom/google/android/gms/games/ui/ai;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/ai;->g:I

    .line 21
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/ai;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lcom/google/android/gms/games/ui/ai;->g:I

    return v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/games/ui/aj;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/ai;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/ai;->e:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/aj;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/google/android/gms/games/ui/ai;->e:I

    return v0
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lcom/google/android/gms/games/ui/ai;->g:I

    .line 25
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/ai;->d()V

    .line 26
    return-void
.end method
