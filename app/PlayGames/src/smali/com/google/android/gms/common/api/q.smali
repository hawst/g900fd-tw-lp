.class public final Lcom/google/android/gms/common/api/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/am;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:[Lcom/google/android/gms/common/api/aj;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/Status;[Lcom/google/android/gms/common/api/aj;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/gms/common/api/q;->a:Lcom/google/android/gms/common/api/Status;

    .line 17
    iput-object p2, p0, Lcom/google/android/gms/common/api/q;->b:[Lcom/google/android/gms/common/api/aj;

    .line 18
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/gms/common/api/q;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 34
    iget v0, p1, Lcom/google/android/gms/common/api/r;->a:I

    iget-object v1, p0, Lcom/google/android/gms/common/api/q;->b:[Lcom/google/android/gms/common/api/aj;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The result token does not belong to this batch"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ag;->b(ZLjava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/common/api/q;->b:[Lcom/google/android/gms/common/api/aj;

    iget v1, p1, Lcom/google/android/gms/common/api/r;->a:I

    aget-object v0, v0, v1

    .line 37
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Ljava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
