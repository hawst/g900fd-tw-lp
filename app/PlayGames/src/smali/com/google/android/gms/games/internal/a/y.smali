.class public final Lcom/google/android/gms/games/internal/a/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/multiplayer/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/gms/games/internal/a/aa;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/gms/games/internal/a/aa;-><init>(Lcom/google/android/gms/games/internal/a/y;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 83
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->n()V

    .line 84
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/multiplayer/g;)V
    .locals 2

    .prologue
    .line 77
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/t;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/ag;

    move-result-object v0

    .line 78
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/common/api/ag;)V

    .line 79
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/multiplayer/g;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 131
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/t;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/ag;

    move-result-object v0

    .line 132
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/common/api/ag;Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Lcom/google/android/gms/games/internal/a/z;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/games/internal/a/z;-><init>(Lcom/google/android/gms/games/internal/a/y;Lcom/google/android/gms/common/api/t;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 138
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->a(Ljava/lang/String;)V

    .line 139
    return-void
.end method
