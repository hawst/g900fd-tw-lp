.class final Lcom/google/android/gms/games/ui/common/matches/c;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final m:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 71
    sget v0, Lcom/google/android/gms/g;->aH:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/c;->m:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/c;->m:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 3

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 78
    check-cast p1, Lcom/google/android/gms/games/ui/common/matches/a;

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/c;->m:Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;

    invoke-static {p1}, Lcom/google/android/gms/games/ui/common/matches/a;->a(Lcom/google/android/gms/games/ui/common/matches/a;)Z

    move-result v1

    invoke-static {p1}, Lcom/google/android/gms/games/ui/common/matches/a;->b(Lcom/google/android/gms/games/ui/common/matches/a;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a(ZI)V

    .line 80
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/c;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/a;

    .line 85
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/a;->c(Lcom/google/android/gms/games/ui/common/matches/a;)Lcom/google/android/gms/games/ui/common/matches/b;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 86
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/a;->c(Lcom/google/android/gms/games/ui/common/matches/a;)Lcom/google/android/gms/games/ui/common/matches/b;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/a;->a(Lcom/google/android/gms/games/ui/common/matches/a;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/matches/b;->a(Z)V

    .line 88
    :cond_0
    return-void

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
