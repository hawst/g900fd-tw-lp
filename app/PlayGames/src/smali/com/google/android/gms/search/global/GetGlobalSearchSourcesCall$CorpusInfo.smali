.class public Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/c;


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lcom/google/android/gms/appdatasearch/Feature;

.field public c:Z

.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 259
    new-instance v0, Lcom/google/android/gms/search/global/c;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->CREATOR:Lcom/google/android/gms/search/global/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 252
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->d:I

    .line 253
    return-void
.end method

.method constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;Z)V
    .locals 0

    .prologue
    .line 269
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270
    iput p1, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->d:I

    .line 271
    iput-object p2, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->a:Ljava/lang/String;

    .line 272
    iput-object p3, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->b:[Lcom/google/android/gms/appdatasearch/Feature;

    .line 273
    iput-boolean p4, p0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->c:Z

    .line 274
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 279
    sget-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->CREATOR:Lcom/google/android/gms/search/global/c;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 285
    sget-object v0, Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;->CREATOR:Lcom/google/android/gms/search/global/c;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/global/c;->a(Lcom/google/android/gms/search/global/GetGlobalSearchSourcesCall$CorpusInfo;Landroid/os/Parcel;I)V

    .line 286
    return-void
.end method
