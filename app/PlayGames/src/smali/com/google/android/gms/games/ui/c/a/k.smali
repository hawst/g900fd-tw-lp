.class public final Lcom/google/android/gms/games/ui/c/a/k;
.super Lcom/google/android/gms/games/ui/c/a/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/a/a;-><init>()V

    .line 21
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/quest/Quest;)Lcom/google/android/gms/games/ui/c/a/k;
    .locals 4

    .prologue
    .line 39
    new-instance v1, Lcom/google/android/gms/games/ui/c/a/k;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/c/a/k;-><init>()V

    .line 42
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 43
    const-string v0, "signedInAccountName"

    invoke-virtual {v2, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v0, "newAccountName"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-interface {p2}, Lcom/google/android/gms/games/quest/Quest;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    .line 47
    const-string v3, "quests"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 48
    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/c/a/k;->g(Landroid/os/Bundle;)V

    .line 50
    return-object v1
.end method


# virtual methods
.method protected final P()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 56
    const-string v1, "quests"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    .line 57
    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final Q()V
    .locals 3

    .prologue
    .line 62
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 63
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 64
    const-string v2, "quests"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    .line 67
    instance-of v2, v1, Lcom/google/android/gms/games/ui/c/a/l;

    if-eqz v2, :cond_0

    .line 68
    check-cast v1, Lcom/google/android/gms/games/ui/c/a/l;

    .line 78
    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/c/a/l;->a(Lcom/google/android/gms/games/quest/Quest;)V

    .line 79
    return-void

    .line 69
    :cond_0
    instance-of v2, v1, Lcom/google/android/gms/games/ui/c/a/m;

    if-eqz v2, :cond_1

    .line 70
    check-cast v1, Lcom/google/android/gms/games/ui/c/a/m;

    .line 71
    invoke-interface {v1}, Lcom/google/android/gms/games/ui/c/a/m;->af()Lcom/google/android/gms/games/ui/c/a/l;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/c/a/l;

    goto :goto_0

    .line 73
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "QuestChangeAccountDialogFragment must be used with a parent Activity which implements QuestAccountSwitcher or QuestAccountSwitcherProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
