.class public final Lcom/google/android/gms/drive/internal/DeviceUsagePreferencesImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field b:I

.field c:I

.field d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/drive/internal/l;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/DeviceUsagePreferencesImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IIIZ)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/gms/drive/internal/DeviceUsagePreferencesImpl;->a:I

    .line 54
    iput p2, p0, Lcom/google/android/gms/drive/internal/DeviceUsagePreferencesImpl;->b:I

    .line 55
    iput p3, p0, Lcom/google/android/gms/drive/internal/DeviceUsagePreferencesImpl;->c:I

    .line 56
    iput-boolean p4, p0, Lcom/google/android/gms/drive/internal/DeviceUsagePreferencesImpl;->d:Z

    .line 57
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/l;->a(Lcom/google/android/gms/drive/internal/DeviceUsagePreferencesImpl;Landroid/os/Parcel;)V

    .line 43
    return-void
.end method
