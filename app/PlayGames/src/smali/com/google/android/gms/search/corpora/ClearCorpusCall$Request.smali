.class public Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/corpora/a;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/gms/search/corpora/a;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->c:I

    .line 60
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput p1, p0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->c:I

    .line 77
    iput-object p2, p0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->a:Ljava/lang/String;

    .line 78
    iput-object p3, p0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->b:Ljava/lang/String;

    .line 79
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/a;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->CREATOR:Lcom/google/android/gms/search/corpora/a;

    invoke-static {p0, p1}, Lcom/google/android/gms/search/corpora/a;->a(Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;Landroid/os/Parcel;)V

    .line 91
    return-void
.end method
