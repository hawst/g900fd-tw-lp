.class public final Lcom/google/android/gms/common/data/p;
.super Lcom/google/android/gms/common/data/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/e;
.implements Lcom/google/android/gms/common/data/q;


# instance fields
.field private final b:Ljava/util/List;

.field private final c:Ljava/util/HashSet;

.field private final d:Ljava/util/ArrayList;

.field private e:Lcom/google/android/gms/common/data/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/data/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 23
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->c:Ljava/util/HashSet;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    .line 32
    new-instance v0, Lcom/google/android/gms/common/data/f;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/common/data/p;->e()V

    .line 34
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/data/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 23
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->c:Ljava/util/HashSet;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    .line 47
    iput-object p1, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    .line 48
    new-instance v0, Lcom/google/android/gms/common/data/f;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/common/data/p;->e()V

    .line 50
    return-void
.end method

.method public varargs constructor <init>([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/data/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 23
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->c:Ljava/util/HashSet;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    .line 39
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    .line 40
    new-instance v0, Lcom/google/android/gms/common/data/f;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    .line 41
    invoke-direct {p0}, Lcom/google/android/gms/common/data/p;->e()V

    .line 42
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 144
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 145
    iget-object v2, p0, Lcom/google/android/gms/common/data/p;->c:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 146
    iget-object v2, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/data/p;->c:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 84
    iget-object v1, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/data/p;->a()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds for this buffer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/f;->a(Lcom/google/android/gms/common/data/d;)V

    .line 55
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/common/data/p;->e()V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->c:Ljava/util/HashSet;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 69
    if-lez v4, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    add-int/lit8 v5, v4, -0x1

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v3, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    add-int/lit8 v2, v4, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/common/data/f;->a_(II)V

    .line 75
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 69
    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/f;->b(Lcom/google/android/gms/common/data/d;)V

    .line 60
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 113
    .line 115
    iget-object v1, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 116
    iget-object v1, p0, Lcom/google/android/gms/common/data/p;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->c:Ljava/util/HashSet;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 124
    :cond_0
    const/4 v1, -0x1

    .line 125
    iget-object v3, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/f;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_1
    if-ltz v1, :cond_2

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 129
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 115
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 134
    invoke-direct {p0}, Lcom/google/android/gms/common/data/p;->e()V

    .line 137
    if-ltz v0, :cond_3

    .line 138
    iget-object v1, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/data/f;->b_(II)V

    .line 140
    :cond_3
    return-void
.end method

.method public final d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f_()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/common/data/p;->e:Lcom/google/android/gms/common/data/f;

    iget-object v0, v0, Lcom/google/android/gms/common/data/f;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 99
    return-void
.end method
