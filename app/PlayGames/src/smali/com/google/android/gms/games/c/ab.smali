.class public final Lcom/google/android/gms/games/c/ab;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/c/u;

.field public b:Lcom/google/android/gms/games/c/h;

.field public c:Lcom/google/android/gms/games/c/g;

.field public d:Lcom/google/android/gms/games/c/al;

.field public e:Lcom/google/android/gms/games/c/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3221
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3222
    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/ab;->G:I

    .line 3223
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 3331
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 3332
    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    if-eqz v1, :cond_0

    .line 3333
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3336
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    if-eqz v1, :cond_1

    .line 3337
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3340
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    if-eqz v1, :cond_2

    .line 3341
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3344
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    if-eqz v1, :cond_3

    .line 3345
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3348
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    if-eqz v1, :cond_4

    .line 3349
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3352
    :cond_4
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3189
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/c/u;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/c/h;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/games/c/g;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/games/c/al;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/games/c/i;

    invoke-direct {v0}, Lcom/google/android/gms/games/c/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3311
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    if-eqz v0, :cond_0

    .line 3312
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3314
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    if-eqz v0, :cond_1

    .line 3315
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3317
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    if-eqz v0, :cond_2

    .line 3318
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3320
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    if-eqz v0, :cond_3

    .line 3321
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3323
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    if-eqz v0, :cond_4

    .line 3324
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3326
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 3327
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3237
    if-ne p1, p0, :cond_1

    .line 3289
    :cond_0
    :goto_0
    return v0

    .line 3240
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/ab;

    if-nez v2, :cond_2

    move v0, v1

    .line 3241
    goto :goto_0

    .line 3243
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/ab;

    .line 3244
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    if-nez v2, :cond_3

    .line 3245
    iget-object v2, p1, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3246
    goto :goto_0

    .line 3249
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3250
    goto :goto_0

    .line 3253
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    if-nez v2, :cond_5

    .line 3254
    iget-object v2, p1, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    if-eqz v2, :cond_6

    move v0, v1

    .line 3255
    goto :goto_0

    .line 3258
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3259
    goto :goto_0

    .line 3262
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    if-nez v2, :cond_7

    .line 3263
    iget-object v2, p1, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    if-eqz v2, :cond_8

    move v0, v1

    .line 3264
    goto :goto_0

    .line 3267
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 3268
    goto :goto_0

    .line 3271
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    if-nez v2, :cond_9

    .line 3272
    iget-object v2, p1, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    if-eqz v2, :cond_a

    move v0, v1

    .line 3273
    goto :goto_0

    .line 3276
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/al;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 3277
    goto :goto_0

    .line 3280
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    if-nez v2, :cond_b

    .line 3281
    iget-object v2, p1, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    if-eqz v2, :cond_0

    move v0, v1

    .line 3282
    goto :goto_0

    .line 3285
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/c/i;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3286
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3294
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3297
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3299
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3301
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3303
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 3305
    return v0

    .line 3294
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->a:Lcom/google/android/gms/games/c/u;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/u;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3297
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->b:Lcom/google/android/gms/games/c/h;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/h;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3299
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->c:Lcom/google/android/gms/games/c/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/g;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3301
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/c/ab;->d:Lcom/google/android/gms/games/c/al;

    invoke-virtual {v0}, Lcom/google/android/gms/games/c/al;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3303
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/c/ab;->e:Lcom/google/android/gms/games/c/i;

    invoke-virtual {v1}, Lcom/google/android/gms/games/c/i;->hashCode()I

    move-result v1

    goto :goto_4
.end method
