.class final Lcom/google/android/gms/games/ui/destination/players/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/r;

.field final synthetic b:Lcom/google/android/gms/common/api/r;

.field final synthetic c:Lcom/google/android/gms/common/api/r;

.field final synthetic d:Lcom/google/android/gms/common/api/r;

.field final synthetic e:Lcom/google/android/gms/common/api/r;

.field final synthetic f:Lcom/google/android/gms/common/api/r;

.field final synthetic g:Lcom/google/android/gms/common/api/r;

.field final synthetic h:I

.field final synthetic i:I

.field final synthetic j:Lcom/google/android/gms/games/ui/destination/players/x;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/players/x;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;II)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/players/y;->a:Lcom/google/android/gms/common/api/r;

    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/players/y;->b:Lcom/google/android/gms/common/api/r;

    iput-object p4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->c:Lcom/google/android/gms/common/api/r;

    iput-object p5, p0, Lcom/google/android/gms/games/ui/destination/players/y;->d:Lcom/google/android/gms/common/api/r;

    iput-object p6, p0, Lcom/google/android/gms/games/ui/destination/players/y;->e:Lcom/google/android/gms/common/api/r;

    iput-object p7, p0, Lcom/google/android/gms/games/ui/destination/players/y;->f:Lcom/google/android/gms/common/api/r;

    iput-object p8, p0, Lcom/google/android/gms/games/ui/destination/players/y;->g:Lcom/google/android/gms/common/api/r;

    iput p9, p0, Lcom/google/android/gms/games/ui/destination/players/y;->h:I

    iput p10, p0, Lcom/google/android/gms/games/ui/destination/players/y;->i:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 13

    .prologue
    const/4 v7, 0x0

    .line 281
    check-cast p1, Lcom/google/android/gms/common/api/q;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/q;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v8

    invoke-static {v8}, Lcom/google/android/gms/games/ui/e/aj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/x;->a(Lcom/google/android/gms/games/ui/destination/players/x;)Lcom/google/android/gms/games/ui/e/o;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/y;->a:Lcom/google/android/gms/common/api/r;

    if-eqz v1, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/y;->a:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/u;

    invoke-interface {v0}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v0

    move-object v6, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/y;->b:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/w;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/y;->c:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/w;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/y;->d:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/j;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/y;->e:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v3}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/j;

    invoke-interface {v2}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v9

    invoke-interface {v3}, Lcom/google/android/gms/games/j;->c()Lcom/google/android/gms/games/internal/game/b;

    move-result-object v10

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->f:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v4}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/people/i;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/players/y;->g:Lcom/google/android/gms/common/api/r;

    invoke-virtual {p1, v5}, Lcom/google/android/gms/common/api/q;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/am;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/people/e;

    invoke-interface {v4}, Lcom/google/android/gms/people/i;->I_()Lcom/google/android/gms/people/model/i;

    move-result-object v11

    invoke-interface {v5}, Lcom/google/android/gms/people/e;->D_()Lcom/google/android/gms/people/model/c;

    move-result-object v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/players/x;->n()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/players/x;->o()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    iget-boolean v4, v4, Lcom/google/android/gms/games/ui/destination/players/x;->ap:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_5

    :cond_2
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/google/android/gms/games/o;->f_()V

    :cond_3
    invoke-virtual {v9}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    invoke-virtual {v10}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    if-eqz v11, :cond_4

    invoke-virtual {v11}, Lcom/google/android/gms/people/model/i;->f_()V

    :cond_4
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/people/model/c;->f_()V

    goto :goto_0

    :cond_5
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->a:Lcom/google/android/gms/common/api/r;

    if-eqz v4, :cond_6

    invoke-virtual {v6}, Lcom/google/android/gms/games/o;->a()I

    move-result v4

    if-lez v4, :cond_9

    iget-object v8, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    const/4 v4, 0x0

    invoke-virtual {v6, v4}, Lcom/google/android/gms/games/o;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/games/Player;

    iput-object v4, v8, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    :cond_6
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    iget-object v4, v4, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-static {v4}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/destination/players/x;->au()V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/games/ui/destination/players/x;->a(Lcom/google/android/gms/games/ui/destination/players/x;Lcom/google/android/gms/games/w;Lcom/google/android/gms/games/w;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/players/x;->ao:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->o()Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/players/x;->c(Lcom/google/android/gms/games/ui/destination/players/x;)Lcom/google/android/gms/games/ui/destination/players/j;

    move-result-object v1

    new-instance v4, Lcom/google/android/gms/common/data/p;

    const/4 v8, 0x1

    new-array v8, v8, [Lcom/google/android/gms/games/internal/player/MostRecentGameInfo;

    const/4 v12, 0x0

    aput-object v0, v8, v12

    invoke-direct {v4, v8}, Lcom/google/android/gms/common/data/p;-><init>([Ljava/lang/Object;)V

    invoke-virtual {v1, v4}, Lcom/google/android/gms/games/ui/destination/players/j;->a(Lcom/google/android/gms/common/data/b;)V

    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    iget v4, p0, Lcom/google/android/gms/games/ui/destination/players/y;->h:I

    invoke-static {v1, v2, v9, v4, v0}, Lcom/google/android/gms/games/ui/destination/players/x;->a(Lcom/google/android/gms/games/ui/destination/players/x;Lcom/google/android/gms/games/j;Lcom/google/android/gms/games/internal/game/b;ILcom/google/android/gms/games/internal/player/MostRecentGameInfo;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/players/y;->i:I

    invoke-static {v1, v3, v10, v2, v0}, Lcom/google/android/gms/games/ui/destination/players/x;->b(Lcom/google/android/gms/games/ui/destination/players/x;Lcom/google/android/gms/games/j;Lcom/google/android/gms/games/internal/game/b;ILcom/google/android/gms/games/internal/player/MostRecentGameInfo;)V

    if-eqz v11, :cond_8

    invoke-virtual {v11}, Lcom/google/android/gms/people/model/i;->a()I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Lcom/google/android/gms/people/model/i;->b(I)Lcom/google/android/gms/people/model/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/h;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/destination/players/x;->a(Lcom/google/android/gms/games/ui/destination/players/x;[Ljava/lang/String;)[Ljava/lang/String;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/destination/players/x;->a(Lcom/google/android/gms/games/ui/destination/players/x;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    if-nez v5, :cond_c

    move v0, v7

    :goto_2
    if-ge v7, v0, :cond_d

    invoke-virtual {v5, v7}, Lcom/google/android/gms/people/model/c;->b(I)Lcom/google/android/gms/people/model/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/people/model/b;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/gms/people/model/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/players/x;->d(Lcom/google/android/gms/games/ui/destination/players/x;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/x;->b(Lcom/google/android/gms/games/ui/destination/players/x;)Lcom/google/android/gms/games/ui/e/o;

    move-result-object v0

    invoke-virtual {v6}, Lcom/google/android/gms/games/o;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v8, v1, v2}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v6, :cond_a

    invoke-virtual {v6}, Lcom/google/android/gms/games/o;->f_()V

    :cond_a
    invoke-virtual {v9}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    invoke-virtual {v10}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    if-eqz v11, :cond_b

    invoke-virtual {v11}, Lcom/google/android/gms/people/model/i;->f_()V

    :cond_b
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/people/model/c;->f_()V

    goto/16 :goto_0

    :cond_c
    :try_start_2
    invoke-virtual {v5}, Lcom/google/android/gms/people/model/c;->a()I

    move-result v0

    goto :goto_2

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/x;->e(Lcom/google/android/gms/games/ui/destination/players/x;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/players/x;->f(Lcom/google/android/gms/games/ui/destination/players/x;)Lcom/google/android/gms/games/ui/e/o;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/y;->j:Lcom/google/android/gms/games/ui/destination/players/x;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/players/x;->as:Lcom/google/android/gms/games/ui/ai;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ai;->c(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v6, :cond_e

    invoke-virtual {v6}, Lcom/google/android/gms/games/o;->f_()V

    :cond_e
    invoke-virtual {v9}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    invoke-virtual {v10}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    if-eqz v11, :cond_f

    invoke-virtual {v11}, Lcom/google/android/gms/people/model/i;->f_()V

    :cond_f
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/google/android/gms/people/model/c;->f_()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_10

    invoke-virtual {v6}, Lcom/google/android/gms/games/o;->f_()V

    :cond_10
    invoke-virtual {v9}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    invoke-virtual {v10}, Lcom/google/android/gms/games/internal/game/b;->f_()V

    if-eqz v11, :cond_11

    invoke-virtual {v11}, Lcom/google/android/gms/people/model/i;->f_()V

    :cond_11
    if-eqz v5, :cond_12

    invoke-virtual {v5}, Lcom/google/android/gms/people/model/c;->f_()V

    :cond_12
    throw v0

    :cond_13
    move-object v6, v0

    goto/16 :goto_1
.end method
