.class public final Lcom/google/android/gms/games/ui/destination/u;
.super Lcom/google/android/gms/games/ui/destination/w;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/lang/String;

.field public c:Landroid/view/View$OnClickListener;

.field public final d:I

.field public final e:I

.field public final f:Z

.field public final g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIZZ)V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/w;-><init>()V

    .line 196
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/u;->a:Landroid/content/Context;

    .line 197
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/u;->b:Ljava/lang/String;

    .line 198
    iput p3, p0, Lcom/google/android/gms/games/ui/destination/u;->d:I

    .line 199
    iput p4, p0, Lcom/google/android/gms/games/ui/destination/u;->e:I

    .line 200
    iput-boolean p5, p0, Lcom/google/android/gms/games/ui/destination/u;->f:Z

    .line 201
    iput-boolean p6, p0, Lcom/google/android/gms/games/ui/destination/u;->g:Z

    .line 202
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/LayoutInflater;ZIZ)Landroid/view/View;
    .locals 7

    .prologue
    const v3, 0x7f0a00d3

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 208
    if-nez p1, :cond_2

    .line 209
    new-instance v1, Lcom/google/android/gms/games/ui/destination/y;

    invoke-direct {v1, v6}, Lcom/google/android/gms/games/ui/destination/y;-><init>(B)V

    .line 210
    if-eqz p3, :cond_1

    const v0, 0x7f040051

    .line 212
    :goto_0
    invoke-virtual {p2, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 213
    const v0, 0x7f0c015a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/games/ui/destination/y;->a:Landroid/widget/TextView;

    .line 214
    if-eqz p3, :cond_0

    .line 215
    iget-object v0, v1, Lcom/google/android/gms/games/ui/destination/y;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 217
    :cond_0
    const v0, 0x7f0c015b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/games/ui/destination/y;->b:Landroid/widget/TextView;

    .line 218
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 222
    :goto_1
    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/y;->a:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/destination/u;->b:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    if-eqz p3, :cond_3

    iget v1, p0, Lcom/google/android/gms/games/ui/destination/u;->e:I

    .line 227
    :goto_2
    if-lez v1, :cond_9

    .line 229
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 232
    :goto_3
    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/y;->a:Landroid/widget/TextView;

    invoke-virtual {v5, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 234
    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/y;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/u;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/y;->a:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 238
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/destination/u;->g:Z

    if-eqz v1, :cond_8

    if-lez p4, :cond_8

    .line 240
    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 242
    const/16 v1, 0xa

    if-lt p4, v1, :cond_4

    .line 244
    invoke-virtual {v2, v6}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 245
    const v1, 0x7f0b008f

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 252
    :goto_4
    if-eqz p5, :cond_5

    move v1, v3

    .line 254
    :goto_5
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 257
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 258
    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/y;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 264
    :goto_6
    const/16 v1, 0x63

    if-le p4, v1, :cond_7

    .line 265
    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/y;->b:Landroid/widget/TextView;

    const v2, 0x7f0f00e6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 269
    :goto_7
    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/y;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 274
    :goto_8
    return-object p1

    .line 210
    :cond_1
    const v0, 0x7f040050

    goto/16 :goto_0

    .line 220
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/y;

    goto :goto_1

    .line 226
    :cond_3
    iget v1, p0, Lcom/google/android/gms/games/ui/destination/u;->d:I

    goto :goto_2

    .line 248
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    goto :goto_4

    .line 252
    :cond_5
    const v1, 0x7f0a004d

    goto :goto_5

    .line 260
    :cond_6
    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/y;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_6

    .line 267
    :cond_7
    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/y;->b:Landroid/widget/TextView;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 271
    :cond_8
    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/y;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    :cond_9
    move-object v1, v2

    goto/16 :goto_3
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/u;->c:Landroid/view/View$OnClickListener;

    .line 279
    return-void
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz p2, :cond_0

    const v0, 0x7f0a0071

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 286
    return-void

    .line 283
    :cond_0
    const v0, 0x7f0a0070

    goto :goto_0
.end method
