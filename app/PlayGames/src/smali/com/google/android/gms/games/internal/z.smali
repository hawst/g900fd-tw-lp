.class final Lcom/google/android/gms/games/internal/z;
.super Lcom/google/android/gms/common/api/s;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/v;


# instance fields
.field private final c:Z

.field private final d:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 2522
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/s;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2524
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->g()I

    move-result v0

    if-lez v0, :cond_0

    .line 2525
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    .line 2526
    const-string v1, "profile_visible"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/games/internal/z;->c:Z

    .line 2528
    const-string v1, "profile_visibility_explicitly_set"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/z;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2536
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->i()V

    .line 2537
    return-void

    .line 2532
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/z;->c:Z

    .line 2533
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/z;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2536
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->i()V

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 2542
    iget-object v0, p0, Lcom/google/android/gms/games/internal/z;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2547
    iget-boolean v0, p0, Lcom/google/android/gms/games/internal/z;->c:Z

    return v0
.end method
