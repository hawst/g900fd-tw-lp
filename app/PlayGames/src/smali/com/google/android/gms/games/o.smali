.class public final Lcom/google/android/gms/games/o;
.super Lcom/google/android/gms/common/data/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/data/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/o;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/android/gms/games/Player;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/games/PlayerRef;

    iget-object v1, p0, Lcom/google/android/gms/games/o;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/games/PlayerRef;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-object v0
.end method

.method public final e()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/games/o;->d()Landroid/os/Bundle;

    move-result-object v1

    .line 39
    if-eqz v1, :cond_0

    .line 40
    const-string v2, "total_count"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 42
    :cond_0
    return v0
.end method
