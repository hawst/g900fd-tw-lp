.class public final Lcom/google/android/gms/games/ui/common/matches/ac;
.super Lcom/google/android/gms/games/ui/ak;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/ad;
.implements Lcom/google/android/gms/games/ui/common/matches/e;
.implements Lcom/google/android/gms/games/ui/common/matches/i;
.implements Lcom/google/android/gms/games/ui/common/matches/o;


# instance fields
.field private final c:Lcom/google/android/gms/games/ui/n;

.field private final d:Lcom/google/android/gms/games/ui/common/matches/e;

.field private final e:Lcom/google/android/gms/games/ui/common/matches/i;

.field private final g:Lcom/google/android/gms/games/ui/common/matches/o;

.field private final h:Lcom/google/android/gms/games/ui/ah;

.field private final i:Lcom/google/android/gms/games/ui/ad;

.field private final j:Lcom/google/android/gms/games/ui/common/matches/a;

.field private final k:Lcom/google/android/gms/games/ui/ac;

.field private final l:Lcom/google/android/gms/games/ui/common/matches/d;

.field private final m:Lcom/google/android/gms/games/ui/common/matches/h;

.field private final n:Lcom/google/android/gms/games/ui/ac;

.field private final o:Lcom/google/android/gms/games/ui/common/matches/n;

.field private final p:Lcom/google/android/gms/games/ui/ac;

.field private final q:Lcom/google/android/gms/games/ui/common/matches/n;

.field private final r:Lcom/google/android/gms/games/ui/ac;

.field private final s:Lcom/google/android/gms/games/ui/common/matches/n;

.field private t:Z

.field private u:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/e;Lcom/google/android/gms/games/ui/common/matches/i;Lcom/google/android/gms/games/ui/common/matches/o;Lcom/google/android/gms/games/ui/ah;Lcom/google/android/gms/games/ui/ad;Lcom/google/android/gms/games/ui/common/matches/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/ak;-><init>()V

    .line 82
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->t:Z

    .line 100
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->c:Lcom/google/android/gms/games/ui/n;

    .line 102
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/e;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->d:Lcom/google/android/gms/games/ui/common/matches/e;

    .line 103
    invoke-static {p3}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/i;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->e:Lcom/google/android/gms/games/ui/common/matches/i;

    .line 105
    invoke-static {p4}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/o;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->g:Lcom/google/android/gms/games/ui/common/matches/o;

    .line 106
    invoke-static {p5}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ah;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->h:Lcom/google/android/gms/games/ui/ah;

    .line 107
    invoke-static {p6}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ad;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->i:Lcom/google/android/gms/games/ui/ad;

    .line 109
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/a;

    invoke-direct {v0, p1, p7}, Lcom/google/android/gms/games/ui/common/matches/a;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->j:Lcom/google/android/gms/games/ui/common/matches/a;

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->j:Lcom/google/android/gms/games/ui/common/matches/a;

    sget-object v0, Lcom/google/android/gms/games/ui/l;->k:Lcom/google/android/gms/common/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b/a;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/matches/a;->c(Z)V

    .line 112
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->j:Lcom/google/android/gms/games/ui/common/matches/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/common/matches/a;->c(Z)V

    .line 117
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->k:Lcom/google/android/gms/games/ui/ac;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->k:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->W:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->k:Lcom/google/android/gms/games/ui/ac;

    const-string v1, "invitationsButton"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;)V

    .line 120
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/d;

    sget v1, Lcom/google/android/gms/h;->b:I

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/games/ui/common/matches/d;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/e;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->l:Lcom/google/android/gms/games/ui/common/matches/d;

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->l:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->d()V

    .line 123
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/h;

    sget v1, Lcom/google/android/gms/h;->b:I

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/games/ui/common/matches/h;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/i;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->m:Lcom/google/android/gms/games/ui/common/matches/h;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->m:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/h;->d()V

    .line 129
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->n:Lcom/google/android/gms/games/ui/ac;

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->n:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->X:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->n:Lcom/google/android/gms/games/ui/ac;

    const-string v1, "myTurnButton"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;)V

    .line 132
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/n;

    sget v1, Lcom/google/android/gms/h;->c:I

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/games/ui/common/matches/n;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/o;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->o:Lcom/google/android/gms/games/ui/common/matches/n;

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->o:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d()V

    .line 136
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->p:Lcom/google/android/gms/games/ui/ac;

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->p:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->aa:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->p:Lcom/google/android/gms/games/ui/ac;

    const-string v1, "theirTurnButton"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;)V

    .line 139
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/n;

    sget v1, Lcom/google/android/gms/h;->d:I

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/games/ui/common/matches/n;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/o;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->q:Lcom/google/android/gms/games/ui/common/matches/n;

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->q:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d()V

    .line 143
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->r:Lcom/google/android/gms/games/ui/ac;

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->r:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->V:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->r:Lcom/google/android/gms/games/ui/ac;

    const-string v1, "completedMatchesButton"

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;)V

    .line 146
    new-instance v0, Lcom/google/android/gms/games/ui/common/matches/n;

    sget v1, Lcom/google/android/gms/h;->a:I

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/games/ui/common/matches/n;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/matches/o;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->s:Lcom/google/android/gms/games/ui/common/matches/n;

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->s:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/n;->d()V

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 151
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->k:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->l:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->m:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->n:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->o:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->p:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->q:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->r:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->s:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/ac;->a(Ljava/util/ArrayList;)V

    .line 162
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/ac;->b()V

    .line 163
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->l:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->e()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->m:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/common/matches/h;->e()I

    move-result v3

    add-int/2addr v0, v3

    .line 289
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->k:Lcom/google/android/gms/games/ui/ac;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->l:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/common/matches/d;->c()I

    move-result v4

    invoke-virtual {v3, v0, v4}, Lcom/google/android/gms/games/ui/ac;->d(II)Z

    move-result v0

    .line 292
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->o:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/common/matches/n;->e()I

    move-result v3

    .line 293
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->n:Lcom/google/android/gms/games/ui/ac;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->o:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/common/matches/n;->c()I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lcom/google/android/gms/games/ui/ac;->d(II)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_5

    :cond_0
    move v0, v2

    .line 296
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->q:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/common/matches/n;->e()I

    move-result v3

    .line 297
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->p:Lcom/google/android/gms/games/ui/ac;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->q:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/common/matches/n;->c()I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lcom/google/android/gms/games/ui/ac;->d(II)Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v0, :cond_6

    :cond_1
    move v0, v2

    .line 300
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->s:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/common/matches/n;->e()I

    move-result v3

    .line 301
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->r:Lcom/google/android/gms/games/ui/ac;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->s:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/common/matches/n;->c()I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lcom/google/android/gms/games/ui/ac;->d(II)Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    move v1, v2

    .line 304
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->h:Lcom/google/android/gms/games/ui/ah;

    if-eqz v0, :cond_4

    if-nez v1, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->t:Z

    if-eqz v0, :cond_4

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->h:Lcom/google/android/gms/games/ui/ah;

    iget v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->u:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/ah;->c_(I)V

    .line 308
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0}, Landroid/support/v7/widget/bw;->a()V

    .line 309
    return-void

    :cond_5
    move v0, v1

    .line 293
    goto :goto_0

    :cond_6
    move v0, v1

    .line 297
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->c:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    const-string v1, "This method should only be called from a 3P context"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->c:Lcom/google/android/gms/games/ui/n;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    :goto_0
    return-void

    .line 185
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a:[I

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;I[I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/common/matches/ad;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/common/matches/ad;-><init>(Lcom/google/android/gms/games/ui/common/matches/ac;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 194
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {p1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 196
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->l:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->m:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/games/ui/common/matches/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->o:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->q:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->s:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    const/4 v1, 0x3

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->c:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/games/ui/z;->a:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, v0, Lcom/google/android/gms/games/ui/z;->a:I

    if-eq v1, v3, :cond_0

    iget v0, v0, Lcom/google/android/gms/games/ui/z;->a:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "This method should only be called from a 1P context"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->c:Lcom/google/android/gms/games/ui/n;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 236
    :goto_1
    return-void

    .line 208
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 213
    :cond_2
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/l;->b(Lcom/google/android/gms/common/api/t;)Z

    .line 218
    sget-object v0, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    sget-object v1, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a:[I

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;[I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/common/matches/ae;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/common/matches/ae;-><init>(Lcom/google/android/gms/games/ui/common/matches/ac;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->l:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->m:Lcom/google/android/gms/games/ui/common/matches/h;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/gms/games/ui/common/matches/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->o:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->q:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->s:Lcom/google/android/gms/games/ui/common/matches/n;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {v0, p1, p2, v3}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->d:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/Game;)V

    .line 358
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->e:Lcom/google/android/gms/games/ui/common/matches/i;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/i;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    .line 382
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/ac;->c()V

    .line 383
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->e:Lcom/google/android/gms/games/ui/common/matches/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/i;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->d:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 341
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->d:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->g:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 394
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->g:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/o;->a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/g;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 239
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/g;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->u:I

    .line 240
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/g;->c()Lcom/google/android/gms/games/multiplayer/turnbased/a;

    move-result-object v2

    .line 244
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->c:Lcom/google/android/gms/games/ui/n;

    iget v3, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->u:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->o()Z

    .line 276
    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a:Lcom/google/android/gms/games/multiplayer/a;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a:Lcom/google/android/gms/games/multiplayer/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/a;->a()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->h:Lcom/google/android/gms/games/ui/ah;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->h:Lcom/google/android/gms/games/ui/ah;

    iget v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->u:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/ah;->c_(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 276
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a()V

    .line 284
    :goto_1
    return-void

    .line 276
    :cond_1
    :try_start_1
    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->b:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    if-eqz v0, :cond_2

    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->b:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->a()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->c:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->c:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->a()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->d:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    if-eqz v0, :cond_4

    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->d:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->a()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :cond_5
    iget-object v0, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a:Lcom/google/android/gms/games/multiplayer/a;

    .line 259
    new-instance v3, Lcom/google/android/gms/games/ui/common/matches/g;

    invoke-direct {v3, v0}, Lcom/google/android/gms/games/ui/common/matches/g;-><init>(Lcom/google/android/gms/common/data/b;)V

    .line 260
    invoke-virtual {v0}, Lcom/google/android/gms/games/multiplayer/a;->f_()V

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->l:Lcom/google/android/gms/games/ui/common/matches/d;

    iget-object v4, v3, Lcom/google/android/gms/games/ui/common/matches/g;->a:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Lcom/google/android/gms/common/data/b;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->m:Lcom/google/android/gms/games/ui/common/matches/h;

    iget-object v3, v3, Lcom/google/android/gms/games/ui/common/matches/g;->b:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/matches/h;->a(Lcom/google/android/gms/common/data/b;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->o:Lcom/google/android/gms/games/ui/common/matches/n;

    iget-object v3, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->b:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/common/data/b;)V

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->q:Lcom/google/android/gms/games/ui/common/matches/n;

    iget-object v3, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->c:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/common/data/b;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->s:Lcom/google/android/gms/games/ui/common/matches/n;

    iget-object v3, v2, Lcom/google/android/gms/games/multiplayer/turnbased/a;->d:Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/common/matches/n;->a(Lcom/google/android/gms/common/data/b;)V

    .line 270
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->h:Lcom/google/android/gms/games/ui/ah;

    if-eqz v0, :cond_6

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->h:Lcom/google/android/gms/games/ui/ah;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ah;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 281
    :cond_6
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->t:Z

    .line 283
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/ac;->c()V

    goto :goto_1

    .line 277
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/multiplayer/turnbased/a;->a()V

    throw v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->j:Lcom/google/android/gms/games/ui/common/matches/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/a;->a(Z)V

    .line 332
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 167
    invoke-super {p0}, Lcom/google/android/gms/games/ui/ak;->b()V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->k:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->l:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->i()V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->n:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->p:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->r:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 175
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->d:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->b(Lcom/google/android/gms/games/Game;)V

    .line 363
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->e:Lcom/google/android/gms/games/ui/common/matches/i;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/i;->b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    .line 388
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/ac;->c()V

    .line 389
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->d:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->b(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 346
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/ac;->c()V

    .line 347
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->g:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/o;->b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 399
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/ac;->c()V

    .line 400
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->g:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/o;->c(Lcom/google/android/gms/games/Game;)V

    .line 410
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->d:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 352
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/ac;->c()V

    .line 353
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->g:Lcom/google/android/gms/games/ui/common/matches/o;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/o;->c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 405
    return-void
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 422
    const-string v0, "bannerNewPlayerTileTextTag"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->c:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->i()V

    .line 431
    :goto_0
    return-void

    .line 424
    :cond_0
    const-string v0, "bannerNewPlayerButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->c:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v0, "MultiplayerInboxAdapter"

    const-string v1, "setUseNewPlayerNotifications: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/l;->c(Lcom/google/android/gms/common/api/t;)V

    goto :goto_0

    .line 429
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->i:Lcom/google/android/gms/games/ui/ad;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/ad;->c_(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/ac;->j:Lcom/google/android/gms/games/ui/common/matches/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/a;->f(I)V

    .line 336
    return-void
.end method
