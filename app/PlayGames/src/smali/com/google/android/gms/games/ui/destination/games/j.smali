.class final Lcom/google/android/gms/games/ui/destination/games/j;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"


# instance fields
.field final synthetic m:Lcom/google/android/gms/games/ui/destination/games/i;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 326
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    .line 327
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 329
    const v0, 0x7f0c0106

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/destination/games/i;->a(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 330
    const v0, 0x7f0c00ff

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/destination/games/i;->a(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;

    .line 331
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/games/i;->a(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 332
    const v0, 0x7f0c0101

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/destination/games/i;->b(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;

    .line 335
    const v0, 0x7f0c0100

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/destination/games/i;->c(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;

    .line 336
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/games/i;->b(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    const v0, 0x7f0c0102

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/destination/games/i;->d(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;

    .line 339
    const v0, 0x7f0c0103

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/destination/games/i;->e(Lcom/google/android/gms/games/ui/destination/games/i;Landroid/view/View;)Landroid/view/View;

    .line 340
    iget-object v1, p1, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    const v0, 0x7f0c0104

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Lcom/google/android/gms/games/ui/destination/games/e;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 341
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/games/i;->c(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 342
    return-void
.end method

.method private a(Landroid/view/View;Lcom/google/android/gms/games/internal/game/GameBadge;)V
    .locals 3

    .prologue
    .line 475
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 476
    invoke-static {p2}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 478
    const v0, 0x7f0c00ed

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 479
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 481
    invoke-interface {p2}, Lcom/google/android/gms/games/internal/game/GameBadge;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 483
    const v0, 0x7f0c0117

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 484
    if-eqz v0, :cond_0

    .line 485
    invoke-interface {p2}, Lcom/google/android/gms/games/internal/game/GameBadge;->f()Landroid/net/Uri;

    move-result-object v1

    .line 486
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/e;->f(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/common/images/ImageManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/common/images/ImageManager;->a(Landroid/widget/ImageView;Landroid/net/Uri;)V

    .line 488
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 492
    const/16 v0, 0x8

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/i;->f(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v3, v3, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/e;->d(Lcom/google/android/gms/games/ui/destination/games/e;)Landroid/widget/LinearLayout;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(ZI[Landroid/view/View;)V

    .line 493
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 12

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Lcom/google/android/gms/games/ui/destination/games/e;Lcom/google/android/gms/games/Game;)Lcom/google/android/gms/games/Game;

    .line 348
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->b(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/i;->d(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->b(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->b(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/i;->a(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/i;->e(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 357
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ah()Lcom/google/android/gms/games/internal/game/ExtendedGame;

    move-result-object v0

    .line 358
    if-nez v0, :cond_4

    invoke-direct {p0, v3}, Lcom/google/android/gms/games/ui/destination/games/j;->b(Z)V

    .line 359
    :cond_1
    :goto_1
    return-void

    .line 349
    :cond_2
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    const-string v4, "\n"

    const-string v5, "<br />"

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0

    .line 358
    :cond_4
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    invoke-direct {p0, v3}, Lcom/google/android/gms/games/ui/destination/games/j;->b(Z)V

    goto :goto_1

    :cond_6
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/ui/destination/games/j;->b(Z)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v2, v0}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Lcom/google/android/gms/games/ui/destination/games/e;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->c(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/internal/game/GameBadge;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/i;->f(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/e;->c(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/internal/game/GameBadge;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/games/ui/destination/games/j;->a(Landroid/view/View;Lcom/google/android/gms/games/internal/game/GameBadge;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->j()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0005

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->d(Lcom/google/android/gms/games/ui/destination/games/e;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->e(Lcom/google/android/gms/games/ui/destination/games/e;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v3

    move v5, v3

    move-object v2, v1

    :goto_3
    if-ge v4, v7, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/e;->e(Lcom/google/android/gms/games/ui/destination/games/e;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/GameBadge;

    if-nez v2, :cond_7

    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v8, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v8, v8, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v8}, Lcom/google/android/gms/games/ui/destination/games/e;->a(Lcom/google/android/gms/games/ui/destination/games/e;)Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    move-result-object v8

    invoke-direct {v2, v8}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v9, -0x1

    const/4 v10, -0x2

    const/4 v11, 0x0

    invoke-direct {v8, v9, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v8, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    iget-object v8, v8, Lcom/google/android/gms/games/ui/destination/games/i;->e:Lcom/google/android/gms/games/ui/destination/games/e;

    invoke-static {v8}, Lcom/google/android/gms/games/ui/destination/games/e;->d(Lcom/google/android/gms/games/ui/destination/games/e;)Landroid/widget/LinearLayout;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_7
    iget-object v8, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    invoke-static {v8}, Lcom/google/android/gms/games/ui/destination/games/i;->g(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f040032

    invoke-virtual {v8, v9, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    invoke-direct {p0, v8, v0}, Lcom/google/android/gms/games/ui/destination/games/j;->a(Landroid/view/View;Lcom/google/android/gms/games/internal/game/GameBadge;)V

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v5, 0x1

    if-lt v0, v6, :cond_8

    move v0, v3

    move-object v2, v1

    :cond_8
    add-int/lit8 v4, v4, 0x1

    move v5, v0

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/j;->m:Lcom/google/android/gms/games/ui/destination/games/i;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/i;->f(Lcom/google/android/gms/games/ui/destination/games/i;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2
.end method
