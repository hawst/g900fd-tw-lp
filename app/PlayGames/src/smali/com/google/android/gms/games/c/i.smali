.class public final Lcom/google/android/gms/games/c/i;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2615
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2616
    iput v1, p0, Lcom/google/android/gms/games/c/i;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/c/i;->c:I

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/i;->d:Z

    iput-boolean v1, p0, Lcom/google/android/gms/games/c/i;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/i;->G:I

    .line 2617
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 2695
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 2696
    iget v1, p0, Lcom/google/android/gms/games/c/i;->a:I

    if-eqz v1, :cond_0

    .line 2697
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/i;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2700
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2701
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2704
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/c/i;->c:I

    if-eqz v1, :cond_2

    .line 2705
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/c/i;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2708
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/i;->d:Z

    if-eqz v1, :cond_3

    .line 2709
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/i;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2712
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/games/c/i;->e:Z

    if-eqz v1, :cond_4

    .line 2713
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/gms/games/c/i;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2716
    :cond_4
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2570
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/i;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/i;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/i;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/c/i;->e:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2675
    iget v0, p0, Lcom/google/android/gms/games/c/i;->a:I

    if-eqz v0, :cond_0

    .line 2676
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/i;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2678
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2679
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2681
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/c/i;->c:I

    if-eqz v0, :cond_2

    .line 2682
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/c/i;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2684
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/i;->d:Z

    if-eqz v0, :cond_3

    .line 2685
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/i;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2687
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/games/c/i;->e:Z

    if-eqz v0, :cond_4

    .line 2688
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/gms/games/c/i;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2690
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 2691
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2631
    if-ne p1, p0, :cond_1

    .line 2657
    :cond_0
    :goto_0
    return v0

    .line 2634
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/i;

    if-nez v2, :cond_2

    move v0, v1

    .line 2635
    goto :goto_0

    .line 2637
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/i;

    .line 2638
    iget v2, p0, Lcom/google/android/gms/games/c/i;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/i;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2639
    goto :goto_0

    .line 2641
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2642
    iget-object v2, p1, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 2643
    goto :goto_0

    .line 2645
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2646
    goto :goto_0

    .line 2648
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/c/i;->c:I

    iget v3, p1, Lcom/google/android/gms/games/c/i;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2649
    goto :goto_0

    .line 2651
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/i;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/i;->d:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2652
    goto :goto_0

    .line 2654
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/games/c/i;->e:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/c/i;->e:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2655
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 2662
    iget v0, p0, Lcom/google/android/gms/games/c/i;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2664
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v3

    .line 2666
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/android/gms/games/c/i;->c:I

    add-int/2addr v0, v3

    .line 2667
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/c/i;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 2668
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/gms/games/c/i;->e:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2669
    return v0

    .line 2664
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/i;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2667
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2668
    goto :goto_2
.end method
