.class public final Lcom/google/android/gms/games/ui/common/leaderboards/r;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/common/leaderboards/l;

.field private h:Lcom/google/android/gms/games/a/e;

.field private i:Lcom/google/android/gms/games/a/e;

.field private j:Lcom/google/android/gms/games/a/e;

.field private k:Ljava/lang/String;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/google/android/gms/i;->l:I

    sput v0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/leaderboards/l;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 47
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->g:Lcom/google/android/gms/games/ui/common/leaderboards/l;

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/a/e;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->j:Lcom/google/android/gms/games/a/e;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/a/e;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->i:Lcom/google/android/gms/games/a/e;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/a/e;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->h:Lcom/google/android/gms/games/a/e;

    return-object v0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->k:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->h:Lcom/google/android/gms/games/a/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->l:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/ui/common/leaderboards/l;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->g:Lcom/google/android/gms/games/ui/common/leaderboards/l;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/s;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/common/leaderboards/r;->e:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/s;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/support/v7/widget/cr;I)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lcom/google/android/gms/games/ui/bg;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->a(Lcom/google/android/gms/games/ui/bg;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/games/a/e;Lcom/google/android/gms/games/a/e;Lcom/google/android/gms/games/a/e;)V
    .locals 1

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->h:Lcom/google/android/gms/games/a/e;

    .line 60
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->i:Lcom/google/android/gms/games/a/e;

    .line 61
    iput-object p3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->j:Lcom/google/android/gms/games/a/e;

    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->d()V

    .line 65
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/bg;I)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->k:Ljava/lang/String;

    const-string v1, "Must set a player ID before binding views"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->h:Lcom/google/android/gms/games/a/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    const-string v1, "Must set at least the first score"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 104
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bf;->a(Lcom/google/android/gms/games/ui/bg;I)V

    .line 105
    return-void

    .line 103
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->k:Ljava/lang/String;

    .line 75
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->d()V

    .line 78
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->l:Z

    .line 88
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 96
    sget v0, Lcom/google/android/gms/games/ui/common/leaderboards/r;->e:I

    return v0
.end method
