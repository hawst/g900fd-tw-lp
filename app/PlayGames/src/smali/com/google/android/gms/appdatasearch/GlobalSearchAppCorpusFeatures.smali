.class public Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/k;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:[Lcom/google/android/gms/appdatasearch/Feature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/appdatasearch/k;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->CREATOR:Lcom/google/android/gms/appdatasearch/k;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->a:I

    .line 44
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->b:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->c:[Lcom/google/android/gms/appdatasearch/Feature;

    .line 46
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->CREATOR:Lcom/google/android/gms/appdatasearch/k;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;->CREATOR:Lcom/google/android/gms/appdatasearch/k;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/k;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchAppCorpusFeatures;Landroid/os/Parcel;I)V

    .line 62
    return-void
.end method
