.class final Lcom/google/android/gms/games/ui/destination/quests/d;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"


# instance fields
.field final synthetic m:Lcom/google/android/gms/games/ui/destination/quests/c;

.field private n:Landroid/view/View;

.field private final o:Lcom/google/android/gms/games/ui/destination/quests/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/destination/quests/c;Landroid/view/View;Lcom/google/android/gms/games/ui/destination/quests/e;)V
    .locals 3

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/quests/d;->m:Lcom/google/android/gms/games/ui/destination/quests/c;

    .line 92
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 93
    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/quests/d;->n:Landroid/view/View;

    .line 94
    const v0, 0x7f0c0154

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/destination/quests/c;->a(Lcom/google/android/gms/games/ui/destination/quests/c;Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/ViewPager;

    .line 95
    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/quests/d;->o:Lcom/google/android/gms/games/ui/destination/quests/e;

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/d;->n:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/view/View;)V

    .line 98
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/quests/c;->a(Lcom/google/android/gms/games/ui/destination/quests/c;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->b(Landroid/view/View;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/d;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 101
    const v0, 0x7f09000b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 102
    if-eqz v0, :cond_0

    .line 105
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/quests/c;->a(Lcom/google/android/gms/games/ui/destination/quests/c;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 107
    const v2, 0x7f0b015b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/d;->o:Lcom/google/android/gms/games/ui/destination/quests/e;

    iput-object p0, v0, Lcom/google/android/gms/games/ui/destination/quests/e;->b:Lcom/google/android/gms/games/ui/ag;

    .line 111
    invoke-static {p1}, Lcom/google/android/gms/games/ui/destination/quests/c;->a(Lcom/google/android/gms/games/ui/destination/quests/c;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/quests/d;->o:Lcom/google/android/gms/games/ui/destination/quests/e;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->a(Landroid/support/v4/view/ao;)V

    .line 112
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 3

    .prologue
    .line 116
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/quests/d;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/quests/c;

    .line 118
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/quests/c;->b(Lcom/google/android/gms/games/ui/destination/quests/c;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/quests/c;->c(Lcom/google/android/gms/games/ui/destination/quests/c;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/quests/c;->d(Lcom/google/android/gms/games/ui/destination/quests/c;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/quests/c;->a(Lcom/google/android/gms/games/ui/destination/quests/c;Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_0
    return-void
.end method
