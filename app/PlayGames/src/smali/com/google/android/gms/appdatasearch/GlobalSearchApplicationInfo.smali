.class public Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/m;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/appdatasearch/m;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/m;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a:I

    .line 94
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:Ljava/lang/String;

    .line 95
    iput p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    .line 96
    iput p4, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    .line 97
    iput p5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->e:I

    .line 98
    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->f:Ljava/lang/String;

    .line 99
    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->g:Ljava/lang/String;

    .line 100
    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->h:Ljava/lang/String;

    .line 101
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/m;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 161
    if-ne p0, p1, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v0

    .line 164
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    if-eqz v2, :cond_3

    .line 165
    check-cast p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    .line 166
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    iget v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    iget v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->e:I

    iget v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->e:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 174
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";labelId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";settingsDescriptionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";iconId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";defaultIntentAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";defaultIntentData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";defaultIntentActivity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/m;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/m;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;Landroid/os/Parcel;)V

    .line 151
    return-void
.end method
