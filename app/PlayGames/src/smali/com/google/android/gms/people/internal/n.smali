.class final Lcom/google/android/gms/people/internal/n;
.super Lcom/google/android/gms/common/internal/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/e;


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/internal/m;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Lcom/google/android/gms/people/model/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/internal/m;Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/c;)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/google/android/gms/people/internal/n;->b:Lcom/google/android/gms/people/internal/m;

    .line 501
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/g;-><init>(Lcom/google/android/gms/common/internal/e;Ljava/lang/Object;)V

    .line 502
    iput-object p3, p0, Lcom/google/android/gms/people/internal/n;->c:Lcom/google/android/gms/common/api/Status;

    .line 503
    iput-object p4, p0, Lcom/google/android/gms/people/internal/n;->d:Lcom/google/android/gms/people/model/c;

    .line 504
    return-void
.end method


# virtual methods
.method public final D_()Lcom/google/android/gms/people/model/c;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/gms/people/internal/n;->d:Lcom/google/android/gms/people/model/c;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/gms/people/internal/n;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 494
    check-cast p1, Lcom/google/android/gms/common/api/m;

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected final c()V
    .locals 0

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/google/android/gms/people/internal/n;->f_()V

    .line 516
    return-void
.end method

.method public final f_()V
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/google/android/gms/people/internal/n;->d:Lcom/google/android/gms/people/model/c;

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/google/android/gms/people/internal/n;->d:Lcom/google/android/gms/people/model/c;

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/c;->c()V

    .line 533
    :cond_0
    return-void
.end method
