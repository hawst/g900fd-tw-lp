.class public final Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field b:Ljava/lang/String;

.field c:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/wallet/shared/c;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/shared/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a:I

    .line 74
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Lcom/google/android/gms/wallet/shared/ApplicationParameters;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a:I

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    .line 67
    iput-object p4, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d:Ljava/lang/String;

    .line 68
    iput-object p5, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e:Ljava/lang/String;

    .line 69
    iput-object p6, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f:Ljava/lang/String;

    .line 70
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 154
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/shared/c;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/os/Parcel;I)V

    .line 155
    return-void
.end method
