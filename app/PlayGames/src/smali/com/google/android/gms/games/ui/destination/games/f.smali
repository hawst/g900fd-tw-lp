.class final Lcom/google/android/gms/games/ui/destination/games/f;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 202
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/f;->a:Landroid/view/LayoutInflater;

    .line 203
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 207
    if-nez p2, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/f;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f040028

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 212
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/g;

    .line 214
    if-nez v0, :cond_1

    .line 215
    new-instance v0, Lcom/google/android/gms/games/ui/destination/games/g;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/ui/destination/games/g;-><init>(Landroid/view/View;)V

    move-object v1, v0

    .line 218
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/f;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/GameBadge;

    .line 219
    iget-object v2, v1, Lcom/google/android/gms/games/ui/destination/games/g;->b:Landroid/widget/TextView;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/GameBadge;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v2, v1, Lcom/google/android/gms/games/ui/destination/games/g;->c:Landroid/widget/TextView;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/GameBadge;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v1, v1, Lcom/google/android/gms/games/ui/destination/games/g;->a:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/GameBadge;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;)V

    .line 223
    return-object p2

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
