.class final Lcom/google/android/gms/games/ui/destination/games/n;
.super Lcom/google/android/play/c/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/view/ViewGroup;

.field final synthetic b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/destination/games/n;->a:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/google/android/play/c/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTransitionEnd(Landroid/transition/Transition;)V
    .locals 8

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v7, 0x0

    .line 547
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591
    :goto_0
    return-void

    .line 559
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    const v3, 0x7f0c010a

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->j(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 562
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->k(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Z

    .line 565
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 567
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/games/ui/destination/games/t;->a:Z

    if-eqz v0, :cond_1

    .line 568
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->i(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 570
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v5, v3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v6, v3

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 572
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 573
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 574
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->l(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 575
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->c(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 578
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 579
    const v1, 0x7f0c010d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    const v1, 0x7f0c0110

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 581
    const v1, 0x7f0c0114

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 582
    const v1, 0x7f0c010b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    const v1, 0x7f0c0024

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 584
    const v1, 0x7f0c0026

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 585
    const v1, 0x7f0c0251

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->l(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)J

    move-result-wide v2

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;JLjava/util/ArrayList;)Landroid/view/animation/Animation;

    .line 588
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/n;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/n;->b:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->m(Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;)V

    goto/16 :goto_0
.end method
