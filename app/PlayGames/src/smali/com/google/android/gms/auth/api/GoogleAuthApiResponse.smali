.class public Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/api/b;


# instance fields
.field final a:I

.field final b:I

.field final c:Landroid/os/Bundle;

.field final d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/gms/auth/api/b;

    invoke-direct {v0}, Lcom/google/android/gms/auth/api/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;->CREATOR:Lcom/google/android/gms/auth/api/b;

    return-void
.end method

.method public constructor <init>(IILandroid/os/Bundle;[B)V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput p1, p0, Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;->a:I

    .line 95
    iput p2, p0, Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;->b:I

    .line 96
    iput-object p3, p0, Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;->c:Landroid/os/Bundle;

    .line 97
    iput-object p4, p0, Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;->d:[B

    .line 98
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 85
    invoke-static {p0, p1}, Lcom/google/android/gms/auth/api/b;->a(Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;Landroid/os/Parcel;)V

    .line 86
    return-void
.end method
