.class public final Lcom/google/android/gms/games/c/al;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2456
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2457
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/c/al;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/al;->G:I

    .line 2458
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 2512
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 2513
    iget v1, p0, Lcom/google/android/gms/games/c/al;->a:I

    if-eqz v1, :cond_0

    .line 2514
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/c/al;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2517
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2518
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2521
    :cond_1
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2428
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/c/al;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2501
    iget v0, p0, Lcom/google/android/gms/games/c/al;->a:I

    if-eqz v0, :cond_0

    .line 2502
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/c/al;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2504
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2505
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2507
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 2508
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2469
    if-ne p1, p0, :cond_1

    .line 2486
    :cond_0
    :goto_0
    return v0

    .line 2472
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/al;

    if-nez v2, :cond_2

    move v0, v1

    .line 2473
    goto :goto_0

    .line 2475
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/al;

    .line 2476
    iget v2, p0, Lcom/google/android/gms/games/c/al;->a:I

    iget v3, p1, Lcom/google/android/gms/games/c/al;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2477
    goto :goto_0

    .line 2479
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2480
    iget-object v2, p1, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2481
    goto :goto_0

    .line 2483
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2484
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2491
    iget v0, p0, Lcom/google/android/gms/games/c/al;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2493
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 2495
    return v0

    .line 2493
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/al;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
