.class public final Lcom/google/android/gms/games/ui/destination/games/y;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/common/players/b;
.implements Lcom/google/android/gms/games/ui/destination/r;
.implements Lcom/google/android/gms/games/ui/h;


# instance fields
.field private am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

.field private an:Lcom/google/android/gms/games/ui/common/players/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    return-void
.end method

.method private b(Lcom/google/android/gms/common/api/t;)V
    .locals 6

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    .line 103
    if-nez v0, :cond_0

    .line 105
    const-string v0, "GameDetailFriendFrag"

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->finish()V

    .line 114
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v3

    .line 111
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const-string v2, "circled"

    const/16 v4, 0xf

    const/4 v5, 0x0

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 4

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/gms/games/u;

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/u;->c()Lcom/google/android/gms/games/o;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/y;->Q()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/y;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->o()Z

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/y;->an:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/y;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->a()I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ai()Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/t;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/y;->b(Lcom/google/android/gms/common/api/t;)V

    .line 99
    :cond_0
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->d:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v0

    .line 174
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 176
    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 182
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/e/aj;->b(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/app/a;)V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/games/app/a;->a(Lcom/google/android/gms/games/Game;I)V

    .line 154
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/common/players/a;Lcom/google/android/gms/games/Player;I)V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method public final varargs b(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/app/b;->a(Landroid/app/Activity;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    .line 187
    return-void
.end method

.method public final b_(I)V
    .locals 5

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/y;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 139
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    const-string v0, "GameDetailFriendFrag"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/y;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v1

    .line 146
    sget-object v2, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    const-string v3, "circled"

    const/16 v4, 0xf

    invoke-interface {v2, v0, v3, v1, v4}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 50
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    .line 55
    const v0, 0x7f0200ed

    const v1, 0x7f0f00dd

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/gms/games/ui/destination/games/y;->a(III)V

    .line 58
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/y;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    .line 59
    const v1, 0x7f0f006c

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 62
    new-instance v1, Lcom/google/android/gms/games/ui/common/players/a;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/y;->d:Lcom/google/android/gms/games/ui/n;

    invoke-direct {v1, v2, p0, v3}, Lcom/google/android/gms/games/ui/common/players/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/b;I)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/y;->an:Lcom/google/android/gms/games/ui/common/players/a;

    .line 65
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/y;->an:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/games/ui/h;)V

    .line 67
    invoke-static {}, Lcom/google/android/gms/common/c/h;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    invoke-virtual {v1}, Landroid/support/v4/app/ab;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.games.ANIMATION"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v4, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/y;->ah()V

    .line 72
    :cond_0
    new-instance v1, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 73
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->an:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 75
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/y;->a(Landroid/support/v7/widget/bv;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ai()Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/t;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ai()Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/games/ui/destination/games/t;->c:Lcom/google/android/gms/games/o;

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->am:Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/games/GameDetailActivity;->ai()Lcom/google/android/gms/games/ui/destination/games/t;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/games/ui/destination/games/t;->d:I

    .line 85
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/y;->an:Lcom/google/android/gms/games/ui/common/players/a;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/common/players/a;->a(Lcom/google/android/gms/common/data/b;)V

    .line 88
    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/y;->h:Lcom/google/android/gms/games/ui/e/o;

    invoke-virtual {v1}, Lcom/google/android/gms/games/o;->a()I

    move-result v1

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/gms/games/ui/e/o;->a(IIZ)V

    .line 91
    :cond_1
    return-void
.end method

.method public final o_()V
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/y;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 163
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/destination/games/y;->b(Lcom/google/android/gms/common/api/t;)V

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/y;->h:Lcom/google/android/gms/games/ui/e/o;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/e/o;->b(I)V

    .line 169
    :goto_0
    return-void

    .line 167
    :cond_0
    const-string v0, "GameDetailFriendFrag"

    const-string v1, "onRetry(): client not connected yet..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
