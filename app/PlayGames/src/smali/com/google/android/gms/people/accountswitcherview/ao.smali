.class final Lcom/google/android/gms/people/accountswitcherview/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/accountswitcherview/at;


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V
    .locals 0

    .prologue
    .line 1162
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/ao;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;B)V
    .locals 0

    .prologue
    .line 1162
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/ao;-><init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Lcom/google/android/gms/people/accountswitcherview/as;
    .locals 3

    .prologue
    .line 1165
    new-instance v1, Lcom/google/android/gms/people/accountswitcherview/as;

    invoke-direct {v1}, Lcom/google/android/gms/people/accountswitcherview/as;-><init>()V

    .line 1166
    iput-object p1, v1, Lcom/google/android/gms/people/accountswitcherview/as;->b:Landroid/view/View;

    .line 1167
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->e:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->c:Landroid/view/View;

    .line 1168
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->f:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->e:Landroid/view/View;

    .line 1169
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->e:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->k:Landroid/widget/ImageView;

    .line 1170
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->b:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->f:Landroid/widget/TextView;

    .line 1172
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->g:Landroid/widget/TextView;

    .line 1173
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->i:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->j:Landroid/widget/ImageView;

    .line 1174
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->c:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->d:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    .line 1175
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->q:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->a:Landroid/view/View;

    .line 1176
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/ao;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    sget v2, Lcom/google/android/gms/people/accountswitcherview/ab;->d:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->x:Landroid/view/View;

    .line 1177
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/ao;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-static {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->h(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1178
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->g:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->h:Landroid/view/View;

    .line 1179
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->h:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->l:Landroid/widget/ImageView;

    .line 1180
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->h:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->i:Landroid/view/View;

    .line 1181
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->i:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->m:Landroid/widget/ImageView;

    .line 1182
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->n:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->q:Landroid/view/View;

    .line 1183
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->q:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->u:Landroid/widget/ImageView;

    .line 1184
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->o:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->r:Landroid/widget/ImageView;

    .line 1186
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->p:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->n:Landroid/view/View;

    .line 1187
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->m:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->o:Landroid/widget/TextView;

    .line 1189
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->l:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->p:Landroid/widget/TextView;

    .line 1191
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->j:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->s:Landroid/view/View;

    .line 1192
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->s:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->v:Landroid/widget/ImageView;

    .line 1193
    sget v0, Lcom/google/android/gms/people/accountswitcherview/ab;->k:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->t:Landroid/view/View;

    .line 1194
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->t:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/as;->w:Landroid/widget/ImageView;

    .line 1197
    :cond_0
    return-object v1
.end method
