.class public final Lcom/google/android/gms/games/internal/a/ch;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/multiplayer/turnbased/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;I[I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 360
    new-instance v0, Lcom/google/android/gms/games/internal/a/cl;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/cl;-><init>(Lcom/google/android/gms/games/internal/a/ch;Lcom/google/android/gms/common/api/t;I[I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 386
    new-instance v0, Lcom/google/android/gms/games/internal/a/ci;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/ci;-><init>(Lcom/google/android/gms/games/internal/a/ch;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->b(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;[I)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 428
    new-instance v0, Lcom/google/android/gms/games/internal/a/ck;

    const/4 v4, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/ck;-><init>(Lcom/google/android/gms/games/internal/a/ch;Lcom/google/android/gms/common/api/t;Ljava/lang/String;I[I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;[I)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/games/internal/a/ch;->a(Lcom/google/android/gms/common/api/t;I[I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 1

    .prologue
    .line 191
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->o()V

    .line 192
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/multiplayer/turnbased/b;)V
    .locals 2

    .prologue
    .line 184
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/t;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/ag;

    move-result-object v0

    .line 186
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/internal/c;->b(Lcom/google/android/gms/common/api/ag;)V

    .line 187
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/multiplayer/turnbased/b;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 440
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/t;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/ag;

    move-result-object v0

    .line 442
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/games/internal/c;->b(Lcom/google/android/gms/common/api/ag;Ljava/lang/String;)V

    .line 444
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 448
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->b(Ljava/lang/String;)V

    .line 449
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 1

    .prologue
    .line 397
    new-instance v0, Lcom/google/android/gms/games/internal/a/cj;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/cj;-><init>(Lcom/google/android/gms/games/internal/a/ch;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->b(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 408
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/gms/games/internal/c;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 410
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 415
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/gms/games/internal/c;->b(Ljava/lang/String;Ljava/lang/String;I)V

    .line 417
    return-void
.end method

.method public final e(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 421
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/games/internal/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    return-void
.end method
