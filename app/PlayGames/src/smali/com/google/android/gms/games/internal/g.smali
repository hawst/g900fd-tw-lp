.class final Lcom/google/android/gms/games/internal/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/m;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2693
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/g;->a:Lcom/google/android/gms/common/api/Status;

    .line 2694
    iput-object p2, p0, Lcom/google/android/gms/games/internal/g;->b:Ljava/lang/String;

    .line 2695
    iput-boolean p3, p0, Lcom/google/android/gms/games/internal/g;->c:Z

    .line 2696
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 2700
    iget-object v0, p0, Lcom/google/android/gms/games/internal/g;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2705
    iget-object v0, p0, Lcom/google/android/gms/games/internal/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2710
    iget-boolean v0, p0, Lcom/google/android/gms/games/internal/g;->c:Z

    return v0
.end method
