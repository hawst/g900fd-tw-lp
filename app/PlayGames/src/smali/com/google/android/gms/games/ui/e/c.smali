.class public Lcom/google/android/gms/games/ui/e/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/view/cc;


# instance fields
.field private final a:Landroid/support/v4/view/ViewPager;

.field private final b:Lcom/google/android/gms/games/ui/e/b;

.field private final c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;


# direct methods
.method public constructor <init>(Landroid/support/v4/view/ViewPager;Lcom/google/android/gms/games/ui/e/b;Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;)V
    .locals 0

    .prologue
    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 392
    iput-object p1, p0, Lcom/google/android/gms/games/ui/e/c;->a:Landroid/support/v4/view/ViewPager;

    .line 393
    iput-object p2, p0, Lcom/google/android/gms/games/ui/e/c;->b:Lcom/google/android/gms/games/ui/e/b;

    .line 394
    iput-object p3, p0, Lcom/google/android/gms/games/ui/e/c;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    .line 395
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/c;->b:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/e/b;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 426
    instance-of v1, v0, Lcom/google/android/gms/games/ui/e/e;

    if-eqz v1, :cond_0

    .line 427
    check-cast v0, Lcom/google/android/gms/games/ui/e/e;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/e;->aq()V

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/c;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(I)V

    .line 430
    return-void
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/c;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->a(IFI)V

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/c;->b:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/e/b;->f()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 420
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/c;->c:Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/finsky/PlayTabContainer;->b(I)V

    .line 435
    packed-switch p1, :pswitch_data_0

    .line 460
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/c;->b:Lcom/google/android/gms/games/ui/e/b;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/e/b;->b:Landroid/content/Context;

    const-string v1, "GFragmentPageChangeListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onPageScrollStateChanged(): unexpected state:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/internal/ba;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 439
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/c;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v0

    .line 444
    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/c;->b:Lcom/google/android/gms/games/ui/e/b;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/e/b;->c(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 446
    instance-of v1, v0, Lcom/google/android/gms/games/ui/e/e;

    if-eqz v1, :cond_0

    .line 449
    check-cast v0, Lcom/google/android/gms/games/ui/e/e;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/e;->ar()V

    goto :goto_0

    .line 435
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
