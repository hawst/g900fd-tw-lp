.class public final Lcom/google/android/gms/games/ui/destination/games/ac;
.super Lcom/google/android/gms/games/ui/destination/games/ab;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/destination/games/ao;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 0

    .prologue
    .line 171
    check-cast p1, Lcom/google/android/gms/games/j;

    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/games/ab;->a(Lcom/google/android/gms/games/j;)V

    return-void
.end method

.method protected final at()Lcom/google/android/gms/games/ui/w;
    .locals 3

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/am;-><init>()V

    .line 177
    new-instance v1, Lcom/google/android/gms/games/ui/destination/games/am;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/ac;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-direct {v1, v2, p0}, Lcom/google/android/gms/games/ui/destination/games/am;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/destination/games/ao;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 178
    invoke-super {p0}, Lcom/google/android/gms/games/ui/destination/games/ab;->at()Lcom/google/android/gms/games/ui/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/am;->a(Lcom/google/android/gms/games/ui/w;)Lcom/google/android/gms/games/ui/am;

    .line 179
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/am;->a()Lcom/google/android/gms/games/ui/ak;

    move-result-object v0

    return-object v0
.end method

.method public final au()I
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method public final av()V
    .locals 3

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/games/ac;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/s;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    :try_start_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    :goto_0
    return-void

    .line 211
    :catch_0
    move-exception v0

    const-string v1, "UiUtils"

    const-string v2, "Unable to launch play store intent"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/ba;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected final b(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 184
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/games/ac;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/e/aa;->e(Landroid/content/Context;)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/i;->c(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 187
    return-void
.end method

.method public final b_(I)V
    .locals 3

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/games/ac;->b()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 197
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 199
    const-string v0, "GameExploreFragment"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :goto_0
    return-void

    .line 203
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/i;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/games/ac;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/e/aa;->e(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/i;->d(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method
