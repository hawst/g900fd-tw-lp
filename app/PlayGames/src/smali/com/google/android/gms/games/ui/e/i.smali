.class public final Lcom/google/android/gms/games/ui/e/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/common/players/h;


# instance fields
.field private a:Z

.field private b:Lcom/google/android/gms/games/ui/e/j;

.field private c:Lcom/google/android/gms/games/ui/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/e/j;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/e/j;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/i;->b:Lcom/google/android/gms/games/ui/e/j;

    .line 54
    invoke-static {p1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/n;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/e/i;->c:Lcom/google/android/gms/games/ui/n;

    .line 55
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 24
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/i;->c:Lcom/google/android/gms/games/ui/n;

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogUpdatingProfileVisibility"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/i;->c:Lcom/google/android/gms/games/ui/n;

    sget v1, Lcom/google/android/gms/l;->bh:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/i;->b:Lcom/google/android/gms/games/ui/e/j;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/e/i;->a:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/e/j;->b_(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/i;->c:Lcom/google/android/gms/games/ui/n;

    sget v1, Lcom/google/android/gms/l;->bg:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/i;->c:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    const-string v0, "ProfileVisiHelp"

    const-string v1, "Trying to set visibility when not connected."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :goto_0
    return-void

    .line 70
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v1, v0, p1}, Lcom/google/android/gms/games/t;->b(Lcom/google/android/gms/common/api/t;Z)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 71
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/e/i;->a:Z

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/i;->c:Lcom/google/android/gms/games/ui/n;

    sget v1, Lcom/google/android/gms/l;->bi:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/n;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 75
    invoke-static {v0}, Lcom/google/android/gms/games/ui/c/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/c/f;

    move-result-object v0

    .line 76
    iget-object v1, p0, Lcom/google/android/gms/games/ui/e/i;->c:Lcom/google/android/gms/games/ui/n;

    const-string v2, "com.google.android.gms.games.ui.dialog.progressDialogUpdatingProfileVisibility"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/ui/e/i;->b:Lcom/google/android/gms/games/ui/e/j;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/e/j;->D()Z

    move-result v0

    return v0
.end method
