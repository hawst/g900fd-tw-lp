.class final Lcom/google/android/gms/games/internal/bj;
.super Lcom/google/android/gms/games/internal/bh;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field private c:Ljava/lang/ref/WeakReference;

.field private d:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/games/internal/c;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 85
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/internal/bh;-><init>(Lcom/google/android/gms/games/internal/c;IB)V

    .line 82
    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/bj;->d:Z

    .line 86
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, -0x1

    const/4 v6, 0x0

    .line 189
    .line 190
    const/16 v1, 0x11

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    invoke-virtual {p1}, Landroid/view/View;->getDisplay()Landroid/view/Display;

    move-result-object v1

    .line 192
    if-eqz v1, :cond_0

    .line 193
    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    .line 198
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 199
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 200
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 201
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 202
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 204
    iget-object v5, p0, Lcom/google/android/gms/games/internal/bj;->b:Lcom/google/android/gms/games/internal/bi;

    iput v0, v5, Lcom/google/android/gms/games/internal/bi;->c:I

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->b:Lcom/google/android/gms/games/internal/bi;

    iput-object v1, v0, Lcom/google/android/gms/games/internal/bi;->a:Landroid/os/IBinder;

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->b:Lcom/google/android/gms/games/internal/bi;

    aget v1, v2, v6

    iput v1, v0, Lcom/google/android/gms/games/internal/bi;->d:I

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->b:Lcom/google/android/gms/games/internal/bi;

    aget v1, v2, v7

    iput v1, v0, Lcom/google/android/gms/games/internal/bi;->e:I

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->b:Lcom/google/android/gms/games/internal/bi;

    aget v1, v2, v6

    add-int/2addr v1, v3

    iput v1, v0, Lcom/google/android/gms/games/internal/bi;->f:I

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->b:Lcom/google/android/gms/games/internal/bi;

    aget v1, v2, v7

    add-int/2addr v1, v4

    iput v1, v0, Lcom/google/android/gms/games/internal/bi;->g:I

    .line 211
    iget-boolean v0, p0, Lcom/google/android/gms/games/internal/bj;->d:Z

    if-eqz v0, :cond_1

    .line 212
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/bj;->a()V

    .line 213
    iput-boolean v6, p0, Lcom/google/android/gms/games/internal/bj;->d:Z

    .line 215
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->b:Lcom/google/android/gms/games/internal/bi;

    iget-object v0, v0, Lcom/google/android/gms/games/internal/bi;->a:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 153
    invoke-super {p0}, Lcom/google/android/gms/games/internal/bh;->a()V

    .line 161
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/bj;->d:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final a(I)V
    .locals 3

    .prologue
    .line 92
    new-instance v0, Lcom/google/android/gms/games/internal/bi;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/games/internal/bi;-><init>(ILandroid/os/IBinder;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/bj;->b:Lcom/google/android/gms/games/internal/bi;

    .line 93
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->u()V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->c:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 103
    iget-object v1, p0, Lcom/google/android/gms/games/internal/bj;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/c;->g()Landroid/content/Context;

    move-result-object v1

    .line 104
    if-nez v0, :cond_0

    instance-of v2, v1, Landroid/app/Activity;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 105
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 108
    :cond_0
    if-eqz v0, :cond_1

    .line 109
    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 110
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 111
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 112
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 118
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/internal/bj;->c:Ljava/lang/ref/WeakReference;

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->g()Landroid/content/Context;

    move-result-object v1

    .line 121
    if-nez p1, :cond_3

    instance-of v0, v1, Landroid/app/Activity;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 122
    check-cast v0, Landroid/app/Activity;

    .line 124
    const v2, 0x1020002

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 125
    if-nez v0, :cond_2

    .line 126
    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 129
    :cond_2
    const-string v1, "PopupManager"

    const-string v2, "You have not specified a View to use as content view for popups. Falling back to the Activity content view which may not work properly in future versions of the API. Use setViewForPopups() to set your content view."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    .line 135
    :cond_3
    if-eqz p1, :cond_5

    .line 136
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/internal/bj;->b(Landroid/view/View;)V

    .line 138
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/bj;->c:Ljava/lang/ref/WeakReference;

    .line 139
    invoke-virtual {p1, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 140
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 141
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 147
    :goto_1
    return-void

    .line 114
    :cond_4
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 143
    :cond_5
    const-string v0, "PopupManager"

    const-string v1, "No content view usable to display popups. Popups will not be displayed in response to this client\'s calls. Use setViewForPopups() to set your content view."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 181
    if-eqz v0, :cond_0

    .line 185
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/internal/bj;->b(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/internal/bj;->b(Landroid/view/View;)V

    .line 166
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bj;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->u()V

    .line 171
    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 172
    return-void
.end method
