.class public final Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;
.super Lcom/google/android/gms/games/ui/destination/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/c/a/p;
.implements Lcom/google/android/gms/games/ui/common/requests/m;


# instance fields
.field private A:I

.field private z:Lcom/google/android/gms/games/ui/destination/requests/a;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 42
    const v0, 0x7f04005e

    const v1, 0x7f11000c

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/destination/b;-><init>(IIZ)V

    .line 43
    return-void
.end method


# virtual methods
.method public final ai()Lcom/google/android/gms/games/ui/c/a/o;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->z:Lcom/google/android/gms/games/ui/destination/requests/a;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/games/ui/common/requests/l;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->z:Lcom/google/android/gms/games/ui/destination/requests/a;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 47
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/b;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 52
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/destination/requests/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/destination/requests/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->z:Lcom/google/android/gms/games/ui/destination/requests/a;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.FRAGMENT_INDEX"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->A:I

    .line 55
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->A:I

    if-ne v0, v2, :cond_1

    .line 56
    const-string v0, "DestRequestActivity"

    const-string v1, "Fragment Index not found in the Intent! Bailing!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->finish()V

    goto :goto_0

    .line 60
    :cond_1
    iget-object v1, p0, Landroid/support/v4/app/ab;->b:Landroid/support/v4/app/ah;

    iget v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->A:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCurrentFragment: unexpected index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->A:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/j;->b(I)Lcom/google/android/gms/games/ui/common/requests/j;

    move-result-object v0

    :goto_1
    invoke-virtual {v1}, Landroid/support/v4/app/ag;->a()Landroid/support/v4/app/ar;

    move-result-object v1

    const v2, 0x7f0c0122

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/ar;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/ar;

    invoke-virtual {v1}, Landroid/support/v4/app/ar;->a()I

    iget v0, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->A:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setCurrentTitle: unexpected index: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->A:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/j;->b(I)Lcom/google/android/gms/games/ui/common/requests/j;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const v0, 0x7f0f018a

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/requests/DestinationRequestListActivity;->setTitle(I)V

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0f018c

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
