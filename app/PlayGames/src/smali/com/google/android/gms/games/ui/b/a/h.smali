.class final Lcom/google/android/gms/games/ui/b/a/h;
.super Lcom/google/android/gms/games/ui/b/a/f;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field private d:Ljava/lang/ref/WeakReference;

.field private e:Z


# direct methods
.method protected constructor <init>(Landroid/app/Activity;ILcom/google/android/gms/games/ui/b/a/i;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    const/16 v0, 0x31

    invoke-direct {p0, p1, v0, p3, v1}, Lcom/google/android/gms/games/ui/b/a/f;-><init>(Landroid/app/Activity;ILcom/google/android/gms/games/ui/b/a/i;B)V

    .line 89
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b/a/h;->e:Z

    .line 95
    invoke-static {}, Lcom/google/android/gms/games/ui/b/a/b;->c()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->d:Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->a:Landroid/app/Activity;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    :cond_2
    if-eqz v0, :cond_4

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/b/a/h;->a(Landroid/view/View;)V

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b/a/h;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 96
    :goto_1
    return-void

    .line 95
    :cond_3
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    :cond_4
    const-string v0, "PopupManager"

    const-string v1, "No content view usable to display popups. Popups will not be displayed in response to this client\'s calls. Use setViewForPopups() to set your content view."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v0, -0x1

    const/4 v6, 0x0

    .line 187
    .line 188
    const/16 v1, 0x11

    invoke-static {v1}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 189
    invoke-virtual {p1}, Landroid/view/View;->getDisplay()Landroid/view/Display;

    move-result-object v1

    .line 190
    if-eqz v1, :cond_0

    .line 191
    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    .line 196
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    .line 197
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 198
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 199
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 200
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 202
    iget-object v5, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    iput v0, v5, Lcom/google/android/gms/games/ui/b/a/g;->c:I

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    iput-object v1, v0, Lcom/google/android/gms/games/ui/b/a/g;->a:Landroid/os/IBinder;

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    aget v1, v2, v6

    iput v1, v0, Lcom/google/android/gms/games/ui/b/a/g;->d:I

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    aget v1, v2, v7

    iput v1, v0, Lcom/google/android/gms/games/ui/b/a/g;->e:I

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    aget v1, v2, v6

    add-int/2addr v1, v3

    iput v1, v0, Lcom/google/android/gms/games/ui/b/a/g;->f:I

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    aget v1, v2, v7

    add-int/2addr v1, v4

    iput v1, v0, Lcom/google/android/gms/games/ui/b/a/g;->g:I

    .line 209
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->e:Z

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->c:Lcom/google/android/gms/games/ui/b/a/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/b/a/i;->a(Lcom/google/android/gms/games/ui/b/a/g;)V

    .line 211
    iput-boolean v6, p0, Lcom/google/android/gms/games/ui/b/a/h;->e:Z

    .line 213
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/b/a/g;->a:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->c:Lcom/google/android/gms/games/ui/b/a/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/b/a/i;->a(Lcom/google/android/gms/games/ui/b/a/g;)V

    .line 159
    :goto_0
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->e:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final a(I)V
    .locals 3

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/gms/games/ui/b/a/g;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/games/ui/b/a/g;-><init>(ILandroid/os/IBinder;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->b:Lcom/google/android/gms/games/ui/b/a/g;

    .line 103
    return-void
.end method

.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->d:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b/a/h;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 179
    if-eqz v0, :cond_0

    .line 183
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/b/a/h;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/b/a/h;->a(Landroid/view/View;)V

    .line 164
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 168
    invoke-static {}, Lcom/google/android/gms/games/ui/b/a/b;->c()V

    .line 169
    invoke-virtual {p1, p0}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 170
    return-void
.end method
