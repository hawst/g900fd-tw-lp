.class public final Lcom/google/android/gms/games/ui/c/a/b;
.super Lcom/google/android/gms/games/ui/c/a/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/a/a;-><init>()V

    .line 21
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/games/ui/c/a/b;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/gms/games/ui/c/a/b;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/a/b;-><init>()V

    .line 40
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 41
    const-string v2, "signedInAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v2, "newAccountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v2, "intent"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 44
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/a/b;->g(Landroid/os/Bundle;)V

    .line 46
    return-object v0
.end method


# virtual methods
.method protected final P()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    sget v1, Lcom/google/android/gms/l;->C:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/ab;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final Q()V
    .locals 3

    .prologue
    .line 56
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 57
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->r:Landroid/os/Bundle;

    .line 58
    const-string v2, "intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 61
    instance-of v2, v1, Lcom/google/android/gms/games/ui/c/a/d;

    if-eqz v2, :cond_0

    .line 62
    check-cast v1, Lcom/google/android/gms/games/ui/c/a/d;

    .line 63
    invoke-interface {v1}, Lcom/google/android/gms/games/ui/c/a/d;->t()Lcom/google/android/gms/games/ui/c/a/c;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/c/a/c;

    .line 69
    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/c/a/c;->c(Landroid/content/Intent;)V

    .line 70
    return-void

    .line 65
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "IntentChangeAccountDialogFragment must be used with a parent Activity which implements IntentAccountSwitcherProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
