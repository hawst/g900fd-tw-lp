.class public Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/people/identity/internal/models/e;

.field private static final J:Ljava/util/HashMap;


# instance fields
.field A:Ljava/util/List;

.field B:Ljava/lang/String;

.field C:Ljava/util/List;

.field D:Ljava/util/List;

.field E:Ljava/util/List;

.field F:Ljava/util/List;

.field G:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

.field H:Ljava/util/List;

.field I:Ljava/util/List;

.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/util/List;

.field d:Ljava/util/List;

.field e:Ljava/lang/String;

.field f:Ljava/util/List;

.field g:Ljava/util/List;

.field h:Ljava/util/List;

.field i:Ljava/util/List;

.field j:Ljava/util/List;

.field k:Ljava/lang/String;

.field l:Ljava/util/List;

.field m:Ljava/util/List;

.field n:Ljava/lang/String;

.field o:Ljava/util/List;

.field p:Ljava/util/List;

.field q:Ljava/lang/String;

.field r:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

.field s:Ljava/util/List;

.field t:Ljava/util/List;

.field u:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

.field v:Ljava/util/List;

.field w:Ljava/util/List;

.field x:Ljava/util/List;

.field y:Ljava/util/List;

.field z:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/e;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/e;

    .line 218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 221
    sput-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "abouts"

    const-string v2, "abouts"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Abouts;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "addresses"

    const-string v2, "addresses"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "ageRange"

    const-string v2, "ageRange"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "birthdays"

    const-string v2, "birthdays"

    const/4 v3, 0x5

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Birthdays;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "braggingRights"

    const-string v2, "braggingRights"

    const/4 v3, 0x6

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$BraggingRights;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "coverPhotos"

    const-string v2, "coverPhotos"

    const/4 v3, 0x7

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$CoverPhotos;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "customFields"

    const-string v2, "customFields"

    const/16 v3, 0x8

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$CustomFields;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "emails"

    const-string v2, "emails"

    const/16 v3, 0x9

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "etag"

    const-string v2, "etag"

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "events"

    const-string v2, "events"

    const/16 v3, 0xb

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "genders"

    const-string v2, "genders"

    const/16 v3, 0xc

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Genders;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "id"

    const/16 v3, 0xd

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "images"

    const-string v2, "images"

    const/16 v3, 0xe

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "instantMessaging"

    const-string v2, "instantMessaging"

    const/16 v3, 0xf

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "language"

    const-string v2, "language"

    const/16 v3, 0x11

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "legacyFields"

    const-string v2, "legacyFields"

    const/16 v3, 0x12

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "linkedPeople"

    const-string v2, "linkedPeople"

    const/16 v3, 0x13

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "memberships"

    const-string v2, "memberships"

    const/16 v3, 0x14

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "metadata"

    const-string v2, "metadata"

    const/16 v3, 0x15

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "names"

    const-string v2, "names"

    const/16 v3, 0x16

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "nicknames"

    const-string v2, "nicknames"

    const/16 v3, 0x17

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "occupations"

    const-string v2, "occupations"

    const/16 v3, 0x18

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Occupations;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "organizations"

    const-string v2, "organizations"

    const/16 v3, 0x19

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "phoneNumbers"

    const-string v2, "phoneNumbers"

    const/16 v3, 0x1a

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 267
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "placesLived"

    const-string v2, "placesLived"

    const/16 v3, 0x1b

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PlacesLived;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "profileUrl"

    const-string v2, "profileUrl"

    const/16 v3, 0x1c

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "relations"

    const-string v2, "relations"

    const/16 v3, 0x1d

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "relationshipInterests"

    const-string v2, "relationshipInterests"

    const/16 v3, 0x1e

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$RelationshipInterests;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "relationshipStatuses"

    const-string v2, "relationshipStatuses"

    const/16 v3, 0x1f

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$RelationshipStatuses;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "skills"

    const-string v2, "skills"

    const/16 v3, 0x20

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Skills;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "sortKeys"

    const-string v2, "sortKeys"

    const/16 v3, 0x21

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "taglines"

    const-string v2, "taglines"

    const/16 v3, 0x22

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Taglines;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    const-string v1, "urls"

    const-string v2, "urls"

    const/16 v3, 0x23

    const-class v4, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 509
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 510
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->b:I

    .line 511
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->a:Ljava/util/Set;

    .line 512
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 551
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 552
    iput-object p1, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->a:Ljava/util/Set;

    .line 553
    iput p2, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->b:I

    .line 554
    iput-object p3, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->c:Ljava/util/List;

    .line 555
    iput-object p4, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->d:Ljava/util/List;

    .line 556
    iput-object p5, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->e:Ljava/lang/String;

    .line 557
    iput-object p6, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->f:Ljava/util/List;

    .line 558
    iput-object p7, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->g:Ljava/util/List;

    .line 559
    iput-object p8, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->h:Ljava/util/List;

    .line 560
    iput-object p9, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->i:Ljava/util/List;

    .line 561
    iput-object p10, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->j:Ljava/util/List;

    .line 562
    iput-object p11, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->k:Ljava/lang/String;

    .line 563
    iput-object p12, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->l:Ljava/util/List;

    .line 564
    iput-object p13, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->m:Ljava/util/List;

    .line 565
    iput-object p14, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->n:Ljava/lang/String;

    .line 566
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->o:Ljava/util/List;

    .line 567
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->p:Ljava/util/List;

    .line 568
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->q:Ljava/lang/String;

    .line 569
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->r:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

    .line 570
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->s:Ljava/util/List;

    .line 571
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->t:Ljava/util/List;

    .line 572
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->u:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    .line 573
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->v:Ljava/util/List;

    .line 574
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->w:Ljava/util/List;

    .line 575
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->x:Ljava/util/List;

    .line 576
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->y:Ljava/util/List;

    .line 577
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->z:Ljava/util/List;

    .line 578
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->A:Ljava/util/List;

    .line 579
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->B:Ljava/lang/String;

    .line 580
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->C:Ljava/util/List;

    .line 581
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->D:Ljava/util/List;

    .line 582
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->E:Ljava/util/List;

    .line 583
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->F:Ljava/util/List;

    .line 584
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->G:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

    .line 585
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->H:Ljava/util/List;

    .line 586
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->I:Ljava/util/List;

    .line 587
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 14523
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 14528
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 14596
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 14530
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->c:Ljava/util/List;

    .line 14594
    :goto_0
    return-object v0

    .line 14532
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->d:Ljava/util/List;

    goto :goto_0

    .line 14534
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->e:Ljava/lang/String;

    goto :goto_0

    .line 14536
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->f:Ljava/util/List;

    goto :goto_0

    .line 14538
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->g:Ljava/util/List;

    goto :goto_0

    .line 14540
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->h:Ljava/util/List;

    goto :goto_0

    .line 14542
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->i:Ljava/util/List;

    goto :goto_0

    .line 14544
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->j:Ljava/util/List;

    goto :goto_0

    .line 14546
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->k:Ljava/lang/String;

    goto :goto_0

    .line 14548
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->l:Ljava/util/List;

    goto :goto_0

    .line 14550
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->m:Ljava/util/List;

    goto :goto_0

    .line 14552
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->n:Ljava/lang/String;

    goto :goto_0

    .line 14554
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->o:Ljava/util/List;

    goto :goto_0

    .line 14556
    :pswitch_e
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->p:Ljava/util/List;

    goto :goto_0

    .line 14558
    :pswitch_f
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->q:Ljava/lang/String;

    goto :goto_0

    .line 14560
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->r:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

    goto :goto_0

    .line 14562
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->s:Ljava/util/List;

    goto :goto_0

    .line 14564
    :pswitch_12
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->t:Ljava/util/List;

    goto :goto_0

    .line 14566
    :pswitch_13
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->u:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    goto :goto_0

    .line 14568
    :pswitch_14
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->v:Ljava/util/List;

    goto :goto_0

    .line 14570
    :pswitch_15
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->w:Ljava/util/List;

    goto :goto_0

    .line 14572
    :pswitch_16
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->x:Ljava/util/List;

    goto :goto_0

    .line 14574
    :pswitch_17
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->y:Ljava/util/List;

    goto :goto_0

    .line 14576
    :pswitch_18
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->z:Ljava/util/List;

    goto :goto_0

    .line 14578
    :pswitch_19
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->A:Ljava/util/List;

    goto :goto_0

    .line 14580
    :pswitch_1a
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->B:Ljava/lang/String;

    goto :goto_0

    .line 14582
    :pswitch_1b
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->C:Ljava/util/List;

    goto :goto_0

    .line 14584
    :pswitch_1c
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->D:Ljava/util/List;

    goto :goto_0

    .line 14586
    :pswitch_1d
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->E:Ljava/util/List;

    goto :goto_0

    .line 14588
    :pswitch_1e
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->F:Ljava/util/List;

    goto :goto_0

    .line 14590
    :pswitch_1f
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->G:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

    goto :goto_0

    .line 14592
    :pswitch_20
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->H:Ljava/util/List;

    goto :goto_0

    .line 14594
    :pswitch_21
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->I:Ljava/util/List;

    goto :goto_0

    .line 14528
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
    .end packed-switch
.end method

.method public final c()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 289
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14513
    const/4 v0, 0x0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 14502
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/e;

    const/4 v0, 0x0

    return v0
.end method

.method protected final e()Z
    .locals 1

    .prologue
    .line 14518
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 14776
    instance-of v0, p1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;

    if-nez v0, :cond_0

    move v0, v1

    .line 14807
    :goto_0
    return v0

    .line 14781
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 14782
    goto :goto_0

    .line 14785
    :cond_1
    check-cast p1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;

    .line 14786
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 14787
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 14788
    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 14790
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 14792
    goto :goto_0

    :cond_3
    move v0, v1

    .line 14797
    goto :goto_0

    .line 14800
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 14802
    goto :goto_0

    :cond_5
    move v0, v2

    .line 14807
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 14763
    const/4 v0, 0x0

    .line 14764
    sget-object v1, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 14765
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 14766
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g()I

    move-result v3

    add-int/2addr v1, v3

    .line 14767
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 14769
    goto :goto_0

    .line 14770
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 14507
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/e;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/people/identity/internal/models/e;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;Landroid/os/Parcel;I)V

    .line 14508
    return-void
.end method
