.class public final Lcom/google/android/gms/games/ui/d/p;
.super Landroid/transition/Transition;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/transition/Transition;-><init>()V

    .line 40
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/games/ui/d/p;->a:I

    .line 41
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/gms/games/ui/d/p;->a:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/d/p;)Z
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d/p;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public final captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method public final createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 4

    .prologue
    .line 47
    iget-object v1, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 48
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 49
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 51
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/d/p;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 54
    div-int/lit8 v0, v0, 0x2

    .line 60
    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v0, v2, v3

    .line 61
    new-instance v0, Lcom/google/android/gms/games/ui/d/r;

    const-string v3, "outline"

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/games/ui/d/r;-><init>(Lcom/google/android/gms/games/ui/d/p;Ljava/lang/String;)V

    .line 62
    invoke-static {v1, v0, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Landroid/util/Property;[I)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 63
    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/d/r;->a(Landroid/animation/ObjectAnimator;)V

    .line 65
    new-instance v0, Lcom/google/android/gms/games/ui/d/q;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/ui/d/q;-><init>(Lcom/google/android/gms/games/ui/d/p;Landroid/view/View;)V

    invoke-virtual {v2, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 86
    return-object v2

    .line 57
    :cond_0
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method
