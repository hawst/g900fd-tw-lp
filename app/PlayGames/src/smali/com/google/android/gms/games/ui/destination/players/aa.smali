.class public final Lcom/google/android/gms/games/ui/destination/players/aa;
.super Lcom/google/android/gms/games/ui/bg;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final A:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private final B:Ljava/lang/StringBuilder;

.field m:Z

.field private final n:Landroid/view/ViewGroup;

.field private final o:Landroid/view/ViewGroup;

.field private final p:Landroid/view/View;

.field private final q:Landroid/view/View;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Landroid/widget/TextView;

.field private final u:Landroid/widget/TextView;

.field private final v:Landroid/view/View;

.field private final w:Landroid/view/View;

.field private final x:Landroid/view/View;

.field private final y:Landroid/view/View;

.field private final z:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bg;-><init>(Landroid/view/View;)V

    .line 152
    const v0, 0x7f0c0139

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->n:Landroid/view/ViewGroup;

    .line 155
    const v0, 0x7f0c0137

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->q:Landroid/view/View;

    .line 157
    const v0, 0x7f0c0132

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->v:Landroid/view/View;

    .line 158
    const v0, 0x7f0c0134

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->w:Landroid/view/View;

    .line 161
    const v0, 0x7f0c0138

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->r:Landroid/widget/TextView;

    .line 162
    const v0, 0x7f0c013c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->s:Landroid/widget/TextView;

    .line 163
    const v0, 0x7f0c013d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->t:Landroid/widget/TextView;

    .line 164
    const v0, 0x7f0c013e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->u:Landroid/widget/TextView;

    .line 166
    const v0, 0x7f0c013a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 170
    const v0, 0x7f0c00f1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->p:Landroid/view/View;

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->p:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    const v0, 0x7f0c0131

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->x:Landroid/view/View;

    .line 174
    const v0, 0x7f0c0133

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->y:Landroid/view/View;

    .line 176
    const v0, 0x7f0c0135

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->z:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->z:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;)V

    .line 180
    const v0, 0x7f0c0136

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->A:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->A:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;)V

    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->B:Ljava/lang/StringBuilder;

    .line 185
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/aa;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private b(II)Landroid/animation/ValueAnimator;
    .locals 2

    .prologue
    .line 421
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 422
    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/ac;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/players/ac;-><init>(Lcom/google/android/gms/games/ui/destination/players/aa;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 431
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/w;I)V
    .locals 26

    .prologue
    .line 189
    invoke-super/range {p0 .. p2}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/ui/w;I)V

    .line 191
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v4, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;

    .line 192
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->k:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 194
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->q:Landroid/view/View;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 195
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->v:Landroid/view/View;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 196
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->w:Landroid/view/View;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 197
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->r:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->b(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 198
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->s:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->b(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 199
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->t:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->b(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 200
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->u:Landroid/widget/TextView;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->b(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 202
    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->c(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 203
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->p:Landroid/view/View;

    const v6, 0x7f020099

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 210
    :goto_0
    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->d(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Lcom/google/android/gms/games/Player;

    move-result-object v7

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->e(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Lcom/google/android/gms/games/Player;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->z:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v5, v7}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->A:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v5, v8}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-interface {v7}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-interface {v7}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v5

    :cond_0
    invoke-interface {v8}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-interface {v8}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v6

    :cond_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->x:Landroid/view/View;

    move-object/from16 v0, v16

    invoke-static {v0, v5}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setBackgroundColor(I)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->y:Landroid/view/View;

    move-object/from16 v0, v16

    invoke-static {v0, v6}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setBackgroundColor(I)V

    int-to-float v7, v5

    add-int/2addr v5, v6

    int-to-float v5, v5

    div-float v5, v7, v5

    const v6, 0x3e2e147b    # 0.17f

    const v7, 0x3f28f5c3    # 0.66f

    mul-float/2addr v5, v7

    add-float/2addr v6, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->x:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->x:Landroid/view/View;

    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->y:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v6, v7, v6

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->y:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 212
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->B:Ljava/lang/StringBuilder;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 213
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->m:Z

    if-nez v5, :cond_a

    .line 214
    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->d(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Lcom/google/android/gms/games/Player;

    move-result-object v5

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->e(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Lcom/google/android/gms/games/Player;

    move-result-object v6

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->f(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I

    move-result v7

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->g(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->h(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Landroid/view/LayoutInflater;

    move-result-object v17

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v5

    move-object/from16 v0, v16

    invoke-static {v0, v5}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v5

    move v9, v5

    :goto_1
    invoke-interface {v6}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v5

    move-object/from16 v0, v16

    invoke-static {v0, v5}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v13

    :goto_2
    if-nez v7, :cond_6

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v10, v5

    :goto_3
    const/4 v5, 0x0

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    move v11, v5

    :goto_4
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    if-ge v11, v10, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->n:Landroid/view/ViewGroup;

    move-object v12, v5

    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->l:Lcom/google/android/gms/games/ui/w;

    check-cast v6, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-static {v7}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v19

    const v7, 0x7f040044

    const/4 v8, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7, v8, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v20

    const v7, 0x7f0c013d

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iget-object v8, v5, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v6}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->i(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)I

    move-result v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setTextColor(I)V

    const v6, 0x7f0a004a

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    const v6, 0x7f0a004a

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    const v6, 0x7f0c013e

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const v7, 0x7f0c013c

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iget-wide v0, v5, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->b:J

    move-wide/from16 v22, v0

    move-object v8, v5

    check-cast v8, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;

    iget-wide v0, v8, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    move-wide/from16 v24, v0

    cmp-long v8, v24, v22

    if-lez v8, :cond_8

    invoke-virtual {v6}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v8

    const/4 v14, 0x1

    invoke-virtual {v6, v8, v14}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    move v8, v13

    move v14, v15

    :goto_6
    move-object/from16 v0, v19

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v6, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f00aa

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v14, 0x0

    iget-object v5, v5, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->c:Ljava/lang/String;

    aput-object v5, v8, v14

    const/4 v5, 0x1

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v8, v5

    const/4 v5, 0x2

    move-object/from16 v0, v19

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->B:Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v5, v11, 0x1

    move v11, v5

    goto/16 :goto_4

    .line 204
    :cond_2
    const/16 v5, 0x10

    invoke-static {v5}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 205
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->p:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 207
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->p:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 214
    :cond_4
    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v5}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v5

    move v9, v5

    goto/16 :goto_1

    :cond_5
    const/4 v5, 0x1

    move-object/from16 v0, v16

    invoke-static {v0, v5}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/res/Resources;I)I

    move-result v13

    goto/16 :goto_2

    :cond_6
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v5

    move v10, v5

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    move-object v12, v5

    goto/16 :goto_5

    :cond_8
    cmp-long v8, v24, v22

    if-gez v8, :cond_b

    invoke-virtual {v7}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v8

    const/4 v15, 0x1

    invoke-virtual {v7, v8, v15}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    move v8, v14

    move v14, v9

    goto/16 :goto_6

    :cond_9
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->m:Z

    .line 217
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->k:Landroid/content/Context;

    const v6, 0x7f0f00a8

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->k:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->d(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Lcom/google/android/gms/games/Player;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Player;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->e(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Lcom/google/android/gms/games/Player;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/gms/games/Player;->d()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->k:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;->e(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter;)Lcom/google/android/gms/games/Player;

    move-result-object v4

    invoke-static {v9, v4}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Player;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->B:Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 223
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/games/ui/destination/players/aa;->p:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 224
    return-void

    :cond_b
    move v8, v14

    move v14, v15

    goto/16 :goto_6
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v2, 0xb

    const/16 v1, 0x8

    const/4 v3, 0x0

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    invoke-static {v2}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewGroup;->measure(II)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/games/ui/destination/players/aa;->b(II)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    invoke-static {v2}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/games/ui/destination/players/aa;->b(II)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/ab;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/destination/players/ab;-><init>(Lcom/google/android/gms/games/ui/destination/players/aa;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/aa;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method
