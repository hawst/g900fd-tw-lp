.class public final Lcom/google/android/gms/games/ui/common/requests/n;
.super Lcom/google/android/gms/games/ui/ak;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/an;
.implements Lcom/google/android/gms/games/ui/ad;
.implements Lcom/google/android/gms/games/ui/common/requests/e;
.implements Lcom/google/android/gms/games/ui/common/requests/h;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:Lcom/google/android/gms/games/ui/n;

.field private final e:Lcom/google/android/gms/games/ui/common/requests/l;

.field private final g:Lcom/google/android/gms/games/ui/ah;

.field private final h:Lcom/google/android/gms/games/ui/ad;

.field private final i:Lcom/google/android/gms/games/ui/ac;

.field private final j:Lcom/google/android/gms/games/ui/common/requests/d;

.field private final k:Lcom/google/android/gms/games/ui/common/requests/g;

.field private final l:Lcom/google/android/gms/games/ui/ac;

.field private final m:Lcom/google/android/gms/games/ui/common/requests/d;

.field private final n:Lcom/google/android/gms/games/ui/common/requests/g;

.field private o:Z

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/gms/games/ui/common/requests/n;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/common/requests/n;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/l;Lcom/google/android/gms/games/ui/ad;Lcom/google/android/gms/games/ui/ah;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/ak;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->o:Z

    .line 78
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->d:Lcom/google/android/gms/games/ui/n;

    .line 80
    invoke-static {p2}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/l;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->e:Lcom/google/android/gms/games/ui/common/requests/l;

    .line 81
    invoke-static {p3}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ad;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->h:Lcom/google/android/gms/games/ui/ad;

    .line 82
    invoke-static {p4}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ah;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->g:Lcom/google/android/gms/games/ui/ah;

    .line 84
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->i:Lcom/google/android/gms/games/ui/ac;

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->i:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->bt:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 86
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/d;

    sget v1, Lcom/google/android/gms/h;->o:I

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/games/ui/common/requests/d;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/e;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->j:Lcom/google/android/gms/games/ui/common/requests/d;

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->j:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->d()V

    .line 91
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/g;

    sget v1, Lcom/google/android/gms/h;->o:I

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/games/ui/common/requests/g;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/h;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->k:Lcom/google/android/gms/games/ui/common/requests/g;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->k:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/requests/g;->d()V

    .line 95
    new-instance v0, Lcom/google/android/gms/games/ui/ac;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/ac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->l:Lcom/google/android/gms/games/ui/ac;

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->l:Lcom/google/android/gms/games/ui/ac;

    sget v1, Lcom/google/android/gms/l;->bv:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->f(I)V

    .line 97
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/d;

    sget v1, Lcom/google/android/gms/h;->p:I

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/games/ui/common/requests/d;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/e;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->m:Lcom/google/android/gms/games/ui/common/requests/d;

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->m:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->d()V

    .line 102
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/g;

    sget v1, Lcom/google/android/gms/h;->p:I

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/games/ui/common/requests/g;-><init>(Lcom/google/android/gms/games/ui/n;Lcom/google/android/gms/games/ui/common/requests/h;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->n:Lcom/google/android/gms/games/ui/common/requests/g;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->n:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/requests/g;->d()V

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->i:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->j:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->k:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->l:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->m:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->n:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/requests/n;->a(Ljava/util/ArrayList;)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/requests/n;->b()V

    .line 116
    return-void
.end method

.method private a(Lcom/google/android/gms/games/ui/ac;II)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 241
    if-lez p2, :cond_4

    .line 242
    invoke-virtual {p1, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 243
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/n;->i:Lcom/google/android/gms/games/ui/ac;

    if-ne p1, v2, :cond_2

    .line 245
    if-le p2, p3, :cond_1

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->h:Lcom/google/android/gms/games/ui/ad;

    const-string v2, "giftsButton"

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;)V

    .line 247
    sub-int v0, p2, p3

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/ac;->i(I)V

    .line 261
    :cond_0
    :goto_0
    return v1

    .line 249
    :cond_1
    sget v0, Lcom/google/android/gms/l;->bu:I

    const-string v2, "openAllButton"

    invoke-virtual {p1, p0, v0, v2}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;ILjava/lang/String;)V

    goto :goto_0

    .line 252
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/n;->l:Lcom/google/android/gms/games/ui/ac;

    if-ne p1, v2, :cond_0

    .line 254
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/n;->h:Lcom/google/android/gms/games/ui/ad;

    const-string v3, "wishesButton"

    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/games/ui/ac;->a(Lcom/google/android/gms/games/ui/ad;Ljava/lang/String;)V

    .line 255
    if-le p2, p3, :cond_3

    move v0, v1

    :cond_3
    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    .line 256
    sub-int v0, p2, p3

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/ac;->i(I)V

    goto :goto_0

    .line 260
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    move v1, v0

    .line 261
    goto :goto_0
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->j:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->e()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/n;->k:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/common/requests/g;->e()I

    move-result v3

    add-int/2addr v3, v0

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->i:Lcom/google/android/gms/games/ui/ac;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/requests/n;->j:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/common/requests/d;->c()I

    move-result v4

    invoke-direct {p0, v0, v3, v4}, Lcom/google/android/gms/games/ui/common/requests/n;->a(Lcom/google/android/gms/games/ui/ac;II)Z

    move-result v0

    .line 218
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/requests/n;->m:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/common/requests/d;->e()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/requests/n;->n:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/common/requests/g;->e()I

    move-result v5

    add-int/2addr v4, v5

    .line 220
    iget-object v5, p0, Lcom/google/android/gms/games/ui/common/requests/n;->l:Lcom/google/android/gms/games/ui/ac;

    iget-object v6, p0, Lcom/google/android/gms/games/ui/common/requests/n;->m:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v6}, Lcom/google/android/gms/games/ui/common/requests/d;->c()I

    move-result v6

    invoke-direct {p0, v5, v4, v6}, Lcom/google/android/gms/games/ui/common/requests/n;->a(Lcom/google/android/gms/games/ui/ac;II)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    .line 224
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/requests/n;->i:Lcom/google/android/gms/games/ui/ac;

    if-le v3, v2, :cond_3

    :goto_1
    invoke-virtual {v4, v2}, Lcom/google/android/gms/games/ui/ac;->a(Z)V

    .line 226
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->g:Lcom/google/android/gms/games/ui/ah;

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->o:Z

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->g:Lcom/google/android/gms/games/ui/ah;

    iget v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->p:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/ah;->c_(I)V

    .line 229
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 220
    goto :goto_0

    :cond_3
    move v2, v1

    .line 224
    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/am;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 37
    check-cast p1, Lcom/google/android/gms/games/request/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/request/e;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->g()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->p:I

    invoke-interface {p1, v5}, Lcom/google/android/gms/games/request/e;->a(I)Lcom/google/android/gms/games/request/a;

    move-result-object v1

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Lcom/google/android/gms/games/request/e;->a(I)Lcom/google/android/gms/games/request/a;

    move-result-object v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->d:Lcom/google/android/gms/games/ui/n;

    iget v3, p0, Lcom/google/android/gms/games/ui/common/requests/n;->p:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->o()Z

    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->a()I

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/request/a;->a()I

    move-result v3

    add-int/2addr v0, v3

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->g:Lcom/google/android/gms/games/ui/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->g:Lcom/google/android/gms/games/ui/ah;

    iget v3, p0, Lcom/google/android/gms/games/ui/common/requests/n;->p:I

    invoke-interface {v0, v3}, Lcom/google/android/gms/games/ui/ah;->c_(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->f_()V

    invoke-virtual {v2}, Lcom/google/android/gms/games/request/a;->f_()V

    :goto_0
    return-void

    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/a;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/requests/a;-><init>(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/n;->j:Lcom/google/android/gms/games/ui/common/requests/d;

    iget-object v4, v0, Lcom/google/android/gms/games/ui/common/requests/a;->a:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/n;->k:Lcom/google/android/gms/games/ui/common/requests/g;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/requests/a;->b:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/ui/common/requests/g;->a(Lcom/google/android/gms/common/data/b;)V

    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/a;

    invoke-direct {v0, v2}, Lcom/google/android/gms/games/ui/common/requests/a;-><init>(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/n;->m:Lcom/google/android/gms/games/ui/common/requests/d;

    iget-object v4, v0, Lcom/google/android/gms/games/ui/common/requests/a;->a:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/requests/n;->n:Lcom/google/android/gms/games/ui/common/requests/g;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/requests/a;->b:Lcom/google/android/gms/common/data/p;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/ui/common/requests/g;->a(Lcom/google/android/gms/common/data/b;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->g:Lcom/google/android/gms/games/ui/ah;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->g:Lcom/google/android/gms/games/ui/ah;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ah;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->f_()V

    invoke-virtual {v2}, Lcom/google/android/gms/games/request/a;->f_()V

    iput-boolean v5, p0, Lcom/google/android/gms/games/ui/common/requests/n;->o:Z

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/n;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/request/a;->f_()V

    invoke-virtual {v2}, Lcom/google/android/gms/games/request/a;->f_()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;)V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 133
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    const v1, 0xffff

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 137
    invoke-static {p1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->j:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Ljava/lang/String;)V

    .line 139
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->k:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/requests/g;->a(Ljava/lang/String;)V

    .line 140
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->m:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Ljava/lang/String;)V

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->n:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/requests/g;->a(Ljava/lang/String;)V

    .line 144
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    const/4 v1, 0x4

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->d:Lcom/google/android/gms/games/ui/n;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    :goto_0
    return-void

    .line 154
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    const v1, 0xffff

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->j:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0, p4}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->k:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v0, p4}, Lcom/google/android/gms/games/ui/common/requests/g;->a(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->m:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0, p4}, Lcom/google/android/gms/games/ui/common/requests/d;->a(Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->n:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v0, p4}, Lcom/google/android/gms/games/ui/common/requests/g;->a(Ljava/lang/String;)V

    .line 164
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/l;

    const/4 v1, 0x4

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/gms/games/l;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->e:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/l;->a(Lcom/google/android/gms/games/Game;)V

    .line 311
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->e:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/l;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V

    .line 323
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/n;->c()V

    .line 324
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->e:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/common/requests/l;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V

    .line 318
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->e:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/l;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    .line 305
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/n;->c()V

    .line 306
    return-void
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->e:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/l;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    .line 300
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-super {p0}, Lcom/google/android/gms/games/ui/ak;->b()V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->l:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->m:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/requests/d;->i()V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->i:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ac;->c(Z)V

    .line 125
    return-void
.end method

.method public final c_(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 288
    const-string v0, "openAllButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/common/data/b;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/n;->j:Lcom/google/android/gms/games/ui/common/requests/d;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/common/requests/d;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/requests/n;->k:Lcom/google/android/gms/games/ui/common/requests/g;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/common/requests/g;->f()Lcom/google/android/gms/common/data/b;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/requests/b;->a([Lcom/google/android/gms/common/data/b;)[Lcom/google/android/gms/games/request/GameRequest;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/requests/n;->e:Lcom/google/android/gms/games/ui/common/requests/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/requests/l;->a([Lcom/google/android/gms/games/request/GameRequest;)V

    .line 295
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/n;->h:Lcom/google/android/gms/games/ui/ad;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/ad;->c_(Ljava/lang/String;)V

    goto :goto_0
.end method
