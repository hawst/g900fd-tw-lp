.class public final Lcom/google/android/gms/games/ui/common/b/a;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# static fields
.field private static final e:I


# instance fields
.field private final g:Lcom/google/android/gms/games/ui/common/b/b;

.field private final h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget v0, Lcom/google/android/gms/i;->I:I

    sput v0, Lcom/google/android/gms/games/ui/common/b/a;->e:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/b/b;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/b/a;->g:Lcom/google/android/gms/games/ui/common/b/b;

    .line 30
    iput p3, p0, Lcom/google/android/gms/games/ui/common/b/a;->h:I

    .line 31
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/b/a;)I
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/google/android/gms/games/ui/common/b/a;->h:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/b/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/b/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/b/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/b/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/b/a;)Lcom/google/android/gms/games/ui/common/b/b;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/b/a;->g:Lcom/google/android/gms/games/ui/common/b/b;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 4

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/gms/games/ui/common/b/c;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/b/a;->d:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/common/b/a;->e:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/b/c;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/b/a;->i:Ljava/lang/String;

    .line 40
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/b/a;->j:Ljava/lang/String;

    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/b/a;->d()V

    .line 42
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 35
    sget v0, Lcom/google/android/gms/games/ui/common/b/a;->e:I

    return v0
.end method
