.class public final Lcom/google/android/gms/games/ui/destination/requests/a;
.super Lcom/google/android/gms/games/ui/common/requests/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/c/a/o;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/destination/g;

.field private final b:Lcom/google/android/gms/games/ui/destination/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/destination/g;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/requests/l;-><init>()V

    .line 43
    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    .line 44
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    .line 46
    new-instance v0, Lcom/google/android/gms/games/ui/destination/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/destination/a/a;-><init>(Lcom/google/android/gms/games/ui/destination/g;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->b:Lcom/google/android/gms/games/ui/destination/a/a;

    .line 47
    return-void
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v2

    .line 91
    invoke-interface {v2}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    const-string v0, "DestReqInboxHelper"

    const-string v1, "launchGameForRequest: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v3

    .line 100
    invoke-static {v2}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v2

    .line 103
    iget-object v4, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    invoke-static {v3}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    :goto_2
    if-ge v1, v5, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "requests"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/az;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-static {v4, v3, v0}, Lcom/google/android/gms/games/ui/e/aj;->a(Landroid/content/Context;Lcom/google/android/gms/games/Game;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->b:Lcom/google/android/gms/games/ui/destination/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/destination/a/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 129
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;)V
    .locals 4

    .prologue
    .line 142
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 143
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 144
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/requests/a;->a(Lcom/google/android/gms/games/request/GameRequest;)V

    .line 143
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 146
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 134
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.destination.SHOW_PUBLIC_REQUESTS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 136
    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/destination/g;->startActivity(Landroid/content/Intent;)V

    .line 138
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 5

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 109
    invoke-interface {v0}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    const-string v0, "DestReqInboxHelper"

    const-string v1, "onRequestDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :goto_0
    return-void

    .line 116
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/t;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/t;->a(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/String;

    move-result-object v2

    .line 120
    sget-object v3, Lcom/google/android/gms/games/d;->r:Lcom/google/android/gms/games/request/d;

    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->e()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v2, v1, v4}, Lcom/google/android/gms/games/request/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->u()V

    goto :goto_0
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v1

    .line 151
    invoke-interface {v1}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    const-string v0, "DestReqInboxHelper"

    const-string v1, "switchAccountForRequest: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :goto_0
    return-void

    .line 158
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v2

    .line 160
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 161
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/requests/a;->b(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final varargs a([Lcom/google/android/gms/games/request/GameRequest;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/g;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v2

    .line 52
    invoke-interface {v2}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    const-string v0, "DestReqInboxHelper"

    const-string v1, "onRequestClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :goto_0
    return-void

    .line 59
    :cond_0
    invoke-static {v2}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/t;)Ljava/lang/String;

    move-result-object v3

    .line 62
    new-instance v4, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    :goto_1
    array-length v5, p1

    if-ge v0, v5, :cond_1

    aget-object v5, p1, v0

    invoke-interface {v5}, Lcom/google/android/gms/common/data/n;->a()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_1
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v0

    .line 67
    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    if-nez v0, :cond_2

    .line 72
    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/destination/requests/a;->a(Ljava/util/ArrayList;)V

    goto :goto_0

    .line 73
    :cond_2
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 79
    invoke-static {v0, v3, v4}, Lcom/google/android/gms/games/ui/c/a/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Lcom/google/android/gms/games/ui/c/a/n;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/requests/a;->a:Lcom/google/android/gms/games/ui/destination/g;

    const-string v2, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :cond_3
    invoke-direct {p0, v4}, Lcom/google/android/gms/games/ui/destination/requests/a;->b(Ljava/util/ArrayList;)V

    goto :goto_0
.end method
