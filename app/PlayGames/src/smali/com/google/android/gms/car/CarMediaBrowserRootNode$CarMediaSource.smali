.class public Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/car/m;

    invoke-direct {v0}, Lcom/google/android/gms/car/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->a:I

    .line 63
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->a:I

    .line 56
    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->b:Ljava/lang/String;

    .line 57
    iput-object p3, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->c:Ljava/lang/String;

    .line 58
    iput-object p4, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->d:[B

    .line 59
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/google/android/gms/car/m;->a(Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;Landroid/os/Parcel;)V

    .line 73
    return-void
.end method
