.class public final Lcom/google/android/gms/games/c/ai;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/gms/games/c/ai;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6811
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6812
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/c/ai;->b:I

    iput v1, p0, Lcom/google/android/gms/games/c/ai;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/c/ai;->G:I

    .line 6813
    return-void
.end method

.method public static b()[Lcom/google/android/gms/games/c/ai;
    .locals 2

    .prologue
    .line 6791
    sget-object v0, Lcom/google/android/gms/games/c/ai;->d:[Lcom/google/android/gms/games/c/ai;

    if-nez v0, :cond_1

    .line 6792
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6794
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/c/ai;->d:[Lcom/google/android/gms/games/c/ai;

    if-nez v0, :cond_0

    .line 6795
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/games/c/ai;

    sput-object v0, Lcom/google/android/gms/games/c/ai;->d:[Lcom/google/android/gms/games/c/ai;

    .line 6797
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6799
    :cond_1
    sget-object v0, Lcom/google/android/gms/games/c/ai;->d:[Lcom/google/android/gms/games/c/ai;

    return-object v0

    .line 6797
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a()I
    .locals 3

    .prologue
    .line 6875
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->a()I

    move-result v0

    .line 6876
    iget-object v1, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6877
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6880
    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/c/ai;->b:I

    if-eqz v1, :cond_1

    .line 6881
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/games/c/ai;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6884
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/c/ai;->c:I

    if-eqz v1, :cond_2

    .line 6885
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/c/ai;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6888
    :cond_2
    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 6785
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/ai;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/c/ai;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 6861
    iget-object v0, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6862
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 6864
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/c/ai;->b:I

    if-eqz v0, :cond_1

    .line 6865
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/c/ai;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6867
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/c/ai;->c:I

    if-eqz v0, :cond_2

    .line 6868
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/c/ai;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6870
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->a(Lcom/google/protobuf/nano/b;)V

    .line 6871
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6825
    if-ne p1, p0, :cond_1

    .line 6845
    :cond_0
    :goto_0
    return v0

    .line 6828
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/c/ai;

    if-nez v2, :cond_2

    move v0, v1

    .line 6829
    goto :goto_0

    .line 6831
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/c/ai;

    .line 6832
    iget-object v2, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 6833
    iget-object v2, p1, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 6834
    goto :goto_0

    .line 6836
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 6837
    goto :goto_0

    .line 6839
    :cond_4
    iget v2, p0, Lcom/google/android/gms/games/c/ai;->b:I

    iget v3, p1, Lcom/google/android/gms/games/c/ai;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 6840
    goto :goto_0

    .line 6842
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/c/ai;->c:I

    iget v3, p1, Lcom/google/android/gms/games/c/ai;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 6843
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 6850
    iget-object v0, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 6853
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/c/ai;->b:I

    add-int/2addr v0, v1

    .line 6854
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/c/ai;->c:I

    add-int/2addr v0, v1

    .line 6855
    return v0

    .line 6850
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/c/ai;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
