.class public final Lcom/google/android/gms/games/ui/destination/players/u;
.super Lcom/google/android/gms/games/ui/bf;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/games/ui/destination/players/v;

.field private final g:Z

.field private final h:Z

.field private i:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private j:Lcom/google/android/gms/games/Player;

.field private k:Ljava/util/ArrayList;

.field private l:F


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcom/google/android/gms/games/ui/destination/players/v;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/bf;-><init>(Landroid/content/Context;Z)V

    .line 48
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->l:F

    .line 67
    iput-boolean p2, p0, Lcom/google/android/gms/games/ui/destination/players/u;->g:Z

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->h:Z

    .line 69
    iput-object p3, p0, Lcom/google/android/gms/games/ui/destination/players/u;->e:Lcom/google/android/gms/games/ui/destination/players/v;

    .line 70
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/destination/players/u;)F
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->l:F

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->j:Lcom/google/android/gms/games/Player;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/destination/players/u;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->h:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/destination/players/u;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->g:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/destination/players/u;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/destination/players/u;)Lcom/google/android/gms/games/ui/destination/players/v;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->e:Lcom/google/android/gms/games/ui/destination/players/v;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/bg;
    .locals 6

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f04008a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 99
    new-instance v1, Lcom/google/android/gms/games/ui/destination/players/w;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/destination/players/w;-><init>(Landroid/view/View;)V

    .line 102
    invoke-static {v1}, Lcom/google/android/gms/games/ui/destination/players/w;->a(Lcom/google/android/gms/games/ui/destination/players/w;)Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/ui/destination/players/u;->i:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 106
    const/16 v2, 0xb

    invoke-static {v2}, Lcom/google/android/gms/common/c/h;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 108
    iget-object v3, p0, Lcom/google/android/gms/games/ui/destination/players/u;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 109
    const v4, 0x7f0b0143

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 110
    iget v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    const v5, 0x7f0b0149

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v3, v4

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 111
    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 114
    :cond_0
    return-object v1
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 118
    iput p1, p0, Lcom/google/android/gms/games/ui/destination/players/u;->l:F

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/u;->d()V

    .line 120
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 1

    .prologue
    .line 73
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->j:Lcom/google/android/gms/games/Player;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/u;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/u;->d()V

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/destination/players/u;->c(Z)V

    goto :goto_0
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/gms/games/ui/destination/players/u;->k:Ljava/util/ArrayList;

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/destination/players/u;->d()V

    .line 84
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 92
    const v0, 0x7f04008a

    return v0
.end method

.method public final c()Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/u;->i:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    return-object v0
.end method
