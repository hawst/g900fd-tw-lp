.class public Lcom/google/android/gms/games/ui/common/a/h;
.super Lcom/google/android/gms/games/ui/common/a/a;
.source "SourceFile"


# instance fields
.field private an:Lcom/google/android/gms/games/ui/common/a/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/z;Z)V
    .locals 7

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/h;->an:Lcom/google/android/gms/games/ui/common/a/l;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/a/l;->c()[I

    move-result-object v4

    .line 37
    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/z;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v0, p1, v4, p3}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/t;[IZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    .line 47
    :goto_0
    return-void

    .line 42
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/z;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/z;->f()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    move-object v1, p1

    move v6, p3

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;[IIZ)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/an;)V

    goto :goto_0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/common/a/a;->d(Landroid/os/Bundle;)V

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/h;->d:Lcom/google/android/gms/games/ui/n;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/a/l;

    const-string v1, "Parent activity did not implement QuestMetadataProvider"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/h;->d:Lcom/google/android/gms/games/ui/n;

    check-cast v0, Lcom/google/android/gms/games/ui/common/a/l;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/h;->an:Lcom/google/android/gms/games/ui/common/a/l;

    .line 31
    return-void
.end method
