.class public Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;
.super Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 494
    new-instance v0, Lcom/google/android/gms/games/ui/destination/players/z;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/destination/players/z;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJLjava/lang/String;)V
    .locals 1

    .prologue
    .line 452
    invoke-direct {p0, p1, p2, p5}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;-><init>(JLjava/lang/String;)V

    .line 453
    iput-wide p3, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    .line 454
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 446
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;-><init>(Landroid/os/Parcel;)V

    .line 448
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    .line 449
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 459
    instance-of v2, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 460
    check-cast p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;

    .line 461
    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->b:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 471
    :cond_0
    :goto_0
    return v0

    .line 463
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->b:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    move v0, v1

    .line 464
    goto :goto_0

    .line 466
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 468
    iget-wide v2, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_3

    move v0, v1

    .line 469
    goto :goto_0

    .line 471
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 442
    check-cast p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a(Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 478
    instance-of v2, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;

    if-nez v2, :cond_1

    .line 485
    :cond_0
    :goto_0
    return v0

    .line 481
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 482
    goto :goto_0

    .line 484
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;

    .line 485
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p1, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 490
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpPerGenreAdapter$PlayerXpData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 491
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/destination/players/PlayerDetailXpComparisonAdapter$PlayerComparisonXpData;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 492
    return-void
.end method
