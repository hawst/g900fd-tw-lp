.class public Lcom/google/android/gms/location/copresence/internal/StrategyImpl;
.super Lcom/google/android/gms/location/copresence/k;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:Z

.field private final d:I

.field private final e:Z

.field private final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/i;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IZZIZZ)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/gms/location/copresence/k;-><init>()V

    .line 49
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->a:I

    .line 50
    iput-boolean p2, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b:Z

    .line 51
    iput-boolean p3, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->c:Z

    .line 52
    iput p4, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->d:I

    .line 53
    iput-boolean p5, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->e:Z

    .line 54
    iput-boolean p6, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->f:Z

    .line 55
    return-void
.end method

.method public constructor <init>(ZZIZZ)V
    .locals 7

    .prologue
    .line 60
    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;-><init>(IZZIZZ)V

    .line 62
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->a:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->c:Z

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->e:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 105
    if-ne p0, p1, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 108
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 109
    goto :goto_0

    .line 111
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 112
    goto :goto_0

    .line 114
    :cond_3
    check-cast p1, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    .line 115
    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->c:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->d:I

    iget v3, p1, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->d:I

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->e:Z

    iget-boolean v3, p1, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->e:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->f:Z

    iget-boolean v3, p1, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->f:Z

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->f:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 100
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 95
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/internal/i;->a(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;Landroid/os/Parcel;)V

    .line 96
    return-void
.end method
