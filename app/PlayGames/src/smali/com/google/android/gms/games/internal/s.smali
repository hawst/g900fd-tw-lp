.class final Lcom/google/android/gms/games/internal/s;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 1

    .prologue
    .line 673
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 674
    const-string v0, "Holder must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/ag;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/m;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/s;->a:Lcom/google/android/gms/common/api/m;

    .line 675
    return-void
.end method


# virtual methods
.method public final c(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/gms/games/internal/s;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/games/internal/q;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/q;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 680
    return-void
.end method
