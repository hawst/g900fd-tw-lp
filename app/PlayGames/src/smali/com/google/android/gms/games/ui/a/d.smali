.class public final Lcom/google/android/gms/games/ui/a/d;
.super Lcom/google/android/gms/games/ui/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/c/a/c;


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/n;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/a/a;-><init>(Landroid/support/v4/app/ab;)V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/games/ui/a/d;->a:Lcom/google/android/gms/games/ui/n;

    .line 35
    return-void
.end method


# virtual methods
.method protected final b(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/a/a;->b(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v0

    .line 44
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/a/d;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v2

    .line 45
    iget-object v3, p0, Lcom/google/android/gms/games/ui/a/d;->a:Lcom/google/android/gms/games/ui/n;

    invoke-static {v2, v3}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 47
    const-string v1, "GamesDestApiHelper"

    const-string v2, "startIntent: not connected; ignoring..."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :cond_2
    invoke-interface {v2}, Lcom/google/android/gms/common/api/t;->d()Z

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/a/d;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.google.android.gms"

    invoke-static {v2, v4}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    const-string v4, "com.google.android.gms"

    invoke-static {v2, v4, v3}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move v2, v0

    :goto_1
    if-nez v2, :cond_0

    move v0, v1

    .line 56
    goto :goto_0

    .line 51
    :cond_4
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v4, v3, p1}, Lcom/google/android/gms/games/ui/c/a/b;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/games/ui/c/a/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/a/d;->a:Lcom/google/android/gms/games/ui/n;

    const-string v4, "com.google.android.gms.games.ui.dialog.changeAccountDialog"

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/ui/e/a;->a(Landroid/support/v4/app/ab;Landroid/support/v4/app/x;Ljava/lang/String;)V

    move v2, v1

    goto :goto_1
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/ui/a/d;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/z;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "This method can only be called from client or headless UI"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/a;->a(ZLjava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/ui/a/d;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/n;->m()Lcom/google/android/gms/common/api/t;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/games/ui/a/d;->a:Lcom/google/android/gms/games/ui/n;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/e/aj;->a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/ui/n;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 103
    const-string v0, "GamesDestApiHelper"

    const-string v1, "switchAccountForIntent: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/ba;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :goto_1
    return-void

    .line 97
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 107
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/a/d;->a:Lcom/google/android/gms/games/ui/n;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/n;->p()Lcom/google/android/gms/games/ui/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/z;->g()Ljava/lang/String;

    move-result-object v1

    .line 109
    const-string v2, "com.google.android.gms"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/a/d;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method
