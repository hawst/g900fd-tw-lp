.class public abstract Lcom/google/android/gms/games/ui/destination/h;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"


# instance fields
.field protected am:Lcom/google/android/gms/games/ui/destination/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/p;-><init>()V

    .line 32
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/p;-><init>(I)V

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/p;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 55
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    .line 56
    instance-of v1, v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    if-eqz v1, :cond_0

    .line 57
    check-cast v0, Lcom/google/android/gms/games/ui/destination/main/MainActivity;

    .line 58
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/main/MainActivity;->aj()V

    .line 60
    :cond_0
    return-void
.end method

.method public final as()Lcom/google/android/gms/games/app/a;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/games/ui/destination/h;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/destination/b;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->d(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/destination/b;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/a;->a(Z)V

    .line 48
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->D:Landroid/support/v4/app/ab;

    check-cast v0, Lcom/google/android/gms/games/ui/destination/b;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/destination/h;->am:Lcom/google/android/gms/games/ui/destination/b;

    .line 49
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->v()V

    .line 65
    instance-of v0, p0, Lcom/google/android/gms/games/ui/destination/r;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 66
    check-cast v0, Lcom/google/android/gms/games/ui/destination/r;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/destination/h;->am:Lcom/google/android/gms/games/ui/destination/b;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/destination/b;->P()Lcom/google/android/gms/games/app/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/destination/r;->a(Lcom/google/android/gms/games/app/a;)V

    .line 68
    :cond_0
    return-void
.end method
