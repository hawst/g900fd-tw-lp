.class public final Lcom/google/android/gms/games/internal/a/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/m;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/t;)Lcom/google/android/gms/common/api/aj;
    .locals 2

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/gms/games/internal/a/ae;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/games/internal/a/ae;-><init>(Lcom/google/android/gms/games/internal/a/ad;Lcom/google/android/gms/common/api/t;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/a/f;II)Lcom/google/android/gms/common/api/aj;
    .locals 6

    .prologue
    .line 239
    new-instance v0, Lcom/google/android/gms/games/internal/a/ah;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/ah;-><init>(Lcom/google/android/gms/games/internal/a/ad;Lcom/google/android/gms/common/api/t;Lcom/google/android/gms/games/a/f;II)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;)Lcom/google/android/gms/common/api/aj;
    .locals 2

    .prologue
    .line 294
    new-instance v0, Lcom/google/android/gms/games/internal/a/ai;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/gms/games/internal/a/ai;-><init>(Lcom/google/android/gms/games/internal/a/ad;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/api/aj;
    .locals 8

    .prologue
    .line 205
    new-instance v0, Lcom/google/android/gms/games/internal/a/ag;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/internal/a/ag;-><init>(Lcom/google/android/gms/games/internal/a/ad;Lcom/google/android/gms/common/api/t;Ljava/lang/String;IIIZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/gms/common/api/aj;
    .locals 9

    .prologue
    .line 319
    new-instance v0, Lcom/google/android/gms/games/internal/a/af;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/games/internal/a/af;-><init>(Lcom/google/android/gms/games/internal/a/ad;Lcom/google/android/gms/common/api/t;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/t;->a(Lcom/google/android/gms/common/api/k;)Lcom/google/android/gms/common/api/k;

    move-result-object v0

    return-object v0
.end method
