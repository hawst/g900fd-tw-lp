.class public Lcom/google/android/gms/appdatasearch/DocumentResults;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/f;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:Landroid/os/Bundle;

.field final d:Landroid/os/Bundle;

.field final e:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/appdatasearch/f;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/DocumentResults;->CREATOR:Lcom/google/android/gms/appdatasearch/f;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput p1, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->a:I

    .line 47
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->b:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->c:Landroid/os/Bundle;

    .line 49
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->d:Landroid/os/Bundle;

    .line 50
    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/DocumentResults;->e:Landroid/os/Bundle;

    .line 51
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/gms/appdatasearch/DocumentResults;->CREATOR:Lcom/google/android/gms/appdatasearch/f;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/google/android/gms/appdatasearch/DocumentResults;->CREATOR:Lcom/google/android/gms/appdatasearch/f;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/f;->a(Lcom/google/android/gms/appdatasearch/DocumentResults;Landroid/os/Parcel;)V

    .line 144
    return-void
.end method
