.class public Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/android/gms/lockbox/internal/b;

    invoke-direct {v0}, Lcom/google/android/gms/lockbox/internal/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;->a:I

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;->b:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;->c:Ljava/lang/String;

    .line 41
    iput-wide p4, p0, Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;->d:J

    .line 42
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;->a:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;->d:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 72
    invoke-static {p0, p1}, Lcom/google/android/gms/lockbox/internal/b;->a(Lcom/google/android/gms/lockbox/internal/LockboxSignedInStatus;Landroid/os/Parcel;)V

    .line 73
    return-void
.end method
